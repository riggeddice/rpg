# Urszula Miłkowicz
## Identyfikator

Id: 9999-urszula-miłkowicz

## Sekcja Opowieści

### Stary kot a Trzęsawisko

* **uid:** 220814-stary-kot-a-trzesawisko, _numer względny_: 7
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Alan Bartozol, Ariela Lechot, Diana Tevalier, Iwan Zawtrak, Karolina Terienak, Kot Rozbójnik, Marian Lechot, Urszula Miłkowicz, Viirai d'Lechotka

Streszczenie:

Właściciele fortifarmy Lechotka kiedyś dali radę wyrwać ziemię Trzęsawisku. Trzęsawisko nie zapomniało. Teraz jak są starzy i sami, Trzęsawisko porwało Arielę. Terminusi nie pomogli Marianowi, więc poszedł by ją uratować i został porwany. Ich anty-anomalny kot Rozbójnik nie był w stanie im pomóc; rannego kota znalazła w rowie Karolina. Zawiozła do Majkłapca, ale kot im uciekł. Karo współpracując z Ulą opanowały Rozbójnika, dogadały się z fortifarmą i z pomocą Alana zniszczyły ludzi porwanych przez Trzęsawisko. A Rozbójnik i fortifarma wpadły do zaskoczonej Uli.

Aktor w Opowieści:

* Dokonanie:
    * po próbie porwania Daniela przyznała się przełożonym i przesunęli ją do dokumentów - aż zadzwoniła Karo poprosić o pomoc w uratowaniu kota. Ula pomogła Karo; dowiedziała się o visterminach, powiedziała, że Alan może pomóc i bardzo dużo wzięła na siebie przy osłanianiu vistermina Rozbójnika. By Alan nie zabił kota, wzięła go jako swojego i skończyła na fortifarmie Lechotka z potencjalnie szaloną TAI, groźnym kotem, jej własnym kotem i ryzykiem zagrożenia ze strony Trzęsawiska. Aha, i z konstruminusem.
* Progresja:
    * relatywnie szczęśliwa właścicielka vistermina (kota) Rozbójnika. Z rozkazu Alana Bartozola mieszka na Fortifarmie Lechotka i jest PRZERAŻONA.
    * zdjęta z dokumentacji i nie mieszania się w sprawy Aurum (gdzie wsadził ją Gabriel Ursus) przez Alana Bartozola.


### Jak wsadzić Ulę Alanowi?

* **uid:** 220816-jak-wsadzic-ule-alanowi, _numer względny_: 6
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Karolina Terienak, Marysia Sowińska, Mimoza Diakon, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Części do Hestii nadal nie dotarły do Marysi - okazuje się, że Mimoza ją sabotuje. Więc jest ryzyko, że części do Hestii dojdą i Jeremi Sowiński będzie miał dostęp do pełni mocy Hestii a Marysia nie XD. Dodatkowo, Karo poprosiła Marysię by ta wsadziła Ulę Alanowi, ale nie chcą zabijać kota. Ale JAK wsadzić Alanowi Ulę (z kotem) jako uczennicę, skoro vistermin jest morderczy, zbliża się audyt Marysi a w Tukanie obudził się terminus (i nie chce ryzykować Uli)? I do tego Alan nie może się dowiedzieć? Do tego okazuje się że części do Hestii konkurują z normalnymi częściami potrzebnymi na tym terenie i DLATEGO Mimoza - paladynka cholerna - blokuje Marysię...

Aktor w Opowieści:

* Dokonanie:
    * zdecydowanie woli życie w samotności na fortifarmie (choć chce być nieco bezpieczniejsza) niż w blokowisku. Ratuje swojego kota przez visterminem Rozbójnikiem.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 5
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * uciekinierka z Aurum i eks-protomag; porwała Daniela by poznać kto jest Rekinem działającym z mafią - ale nic z tego nie osiągnęła. Mieszka samotnie z kotem.


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 4
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * rozczarowana, że nie przyskrzyni żadnego Rekina. Uważa, że Rekiny pozwalają sobie na zdecydowanie za dużo. Ściągnęła Marysi Tukana - jeśli to może uratować akcję... ale się postawiła. Marysia musiała poprosić.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 3
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * niechętnie współpracująca z Marysią uczennica terminusa. Bardziej kompetentna niż się wydaje, choć kij w tyłku. Pokonana z zaskoczenia przez Arkadię (nie doceniła), potraktowała ją tazerem jak już była ranna i na ziemi. Umie współpracować jeśli chce.
* Progresja:
    * niechętny szacunek do Marysi Sowińskiej. Będzie z nią współpracować. Marysia nie jest ani zła ani głupia. Da się z nią sporo zrobić.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 2
* **daty:** 0111-04-22 - 0111-04-23
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * wezwana przez Napoleona, wyczuła, że coś jest nie tak z "porwanym Myrczkiem". Ale niewiele mogła zrobić. Po obserwowaniu domu Triany, dała się wymanewrować i wezwała wsparcie przeciw "czystej" grzyboprezie Rekinów i Myrczka.
* Progresja:
    * HAŃBA! Wezwała wsparcie do CZYSTEJ (zero alkoholu czy narkotyków) imprezy Rekinów. Oczywiście, to była prowokacja. Ale Ula nie powinna na to była się wpakować.
    * nie lubi arystokracji Aurum. A zwłaszcza Marysi Sowińskiej. Dodatkowo - nie lubi Ignacego Myrczka. Ma opinię, że się bała że on coś zrobi złego na imprezie...


### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 1
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * pierwszoroczna uczennica terminusa; świeżynka. Niezbyt bogata, bez wpływów, z ludzkiej rodziny. Naiwna. Wierzy, że robi misję szpiegowską; wykazała się wysoką inteligencją adaptując do porwania i planu Roberta i determinacją, walcząc i kończąc poparzoną przeciw efemerydzie. Będzie z niej coś przydatnego.
* Progresja:
    * BARDZO nieufna wobec Gabriela Ursusa; tyle jej obiecał a skończyła poparzona i z potężnym OPR od Tymona Grubosza, którego Ula podziwia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-10-13
    1. Primus    : 7, @: 0111-10-13
        1. Sektor Astoriański    : 7, @: 0111-10-13
            1. Astoria    : 7, @: 0111-10-13
                1. Sojusz Letejski    : 7, @: 0111-10-13
                    1. Szczeliniec    : 7, @: 0111-10-13
                        1. Powiat Pustogorski    : 7, @: 0111-10-13
                            1. Majkłapiec    : 1, @: 0111-10-13
                                1. Kociarnia Zawtrak    : 1, @: 0111-10-13
                            1. Podwiert, okolice    : 1, @: 0111-10-13
                                1. Fortifarma Lechotka    : 1, @: 0111-10-13
                            1. Podwiert    : 5, @: 0111-10-13
                                1. Dzielnica Luksusu Rekinów    : 4, @: 0111-10-13
                                    1. Obrzeża Biedy    : 3, @: 0111-06-16
                                        1. Domy Ubóstwa    : 1, @: 0111-05-31
                                        1. Stadion Lotników    : 2, @: 0111-06-16
                                    1. Sektor Brudu i Nudy    : 2, @: 0111-06-16
                                        1. Skrytki Czereśniaka    : 2, @: 0111-06-16
                                    1. Serce Luksusu    : 1, @: 0111-06-09
                                        1. Apartamentowce Elity    : 1, @: 0111-06-09
                                1. Kompleks Korporacyjny    : 1, @: 0111-10-13
                                    1. Dokumentarium Terminuskie    : 1, @: 0111-10-13
                                1. Las Trzęsawny    : 3, @: 0111-06-16
                            1. Pustogor    : 1, @: 0111-05-31
                                1. Rdzeń    : 1, @: 0111-05-31
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                            1. Trzęsawisko Zjawosztup    : 1, @: 0111-10-13
                            1. Zaczęstwo    : 4, @: 0111-06-16
                                1. Akademia Magii, kampus    : 1, @: 0111-04-23
                                    1. Akademik    : 1, @: 0111-04-23
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowiec Gorland    : 2, @: 0111-04-23
                                1. Dzielnica Kwiecista    : 2, @: 0111-04-23
                                    1. Rezydencja Porzeczników    : 2, @: 0111-04-23
                                        1. Podziemne Laboratorium    : 2, @: 0111-04-23
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 3, @: 0111-05-31
                                1. Osiedle Ptasie    : 1, @: 0111-06-16
                                1. Park Centralny    : 1, @: 0111-04-23
                                    1. Jezioro Gęsie    : 1, @: 0111-04-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Tomasz Tukan         | 4 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Julia Kardolin       | 3 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Kacper Bankierz      | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Karolina Terienak    | 3 | ((210720-porwanie-daniela-terienaka; 220814-stary-kot-a-trzesawisko; 220816-jak-wsadzic-ule-alanowi)) |
| Rafał Torszecki      | 3 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka)) |
| Triana Porzecznik    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Ignacy Myrczek       | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Liliana Bankierz     | 2 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach)) |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Daniel Terienak      | 1 | ((210720-porwanie-daniela-terienaka)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Napoleon Bankierz    | 1 | ((210323-grzybopreza)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |