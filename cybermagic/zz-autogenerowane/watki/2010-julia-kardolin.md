# Julia Kardolin
## Identyfikator

Id: 2010-julia-kardolin

## Sekcja Opowieści

### An Unfortunate Ratnapping

* **uid:** 230331-an-unfortunate-ratnapping, _numer względny_: 10
* **daty:** 0111-11-08 - 0111-11-10
* **obecni:** Alex Deverien, Carmen Deverien, Julia Kardolin, kot-pacyfikator Tobias, Paweł Szprotka, Radosław Turkamenin

Streszczenie:

In the city of Podwiert, magical rats cause chaos by draining electricity. Alex, Carmen, Julia, and their cat Tobias are sent by their magical school to solve the problem. They identify the rat nest in the warehouses and learn about the involvement of a mercenary company, the Guardians of Harmony, who have been secretly reducing the rat population.

Using a specially designed trap, the team captures the rats and encounters Radosław, the leader of the local Guardians of Harmony cell, who confronts them. They deny responsibility for the rat problem and continue their mission.

When Tobias goes missing, they find him in a Guardians' truck filled with caged rats. After a scuffle, Alex and Carmen steal the cages and accidentally swap Guardian's soul with that of a random rat. Earlier on, they had one problem. Now they have two.

Aktor w Opowieści:

* Dokonanie:
    * skilled in creating traps, she designs a trap to catch the magical rats and singlehandedly solves the main rat nest in the warehouses.


### The goose from hell

* **uid:** 230303-the-goose-from-hell, _numer względny_: 9
* **daty:** 0111-10-28 - 0111-10-30
* **obecni:** Alex Deverien, Alicja Trawlis, Carmen Deverien, Julia Kardolin, kot-pacyfikator Tobias, Paweł Szprotka, Teresa Mieralit

Streszczenie:

A cat-pacifier named Tobias belonging to Carmen and Alex got shot at by some random illegal hunters. Carmen, Alex, and Julia are tasked by their ethics teacher to deal with an anomalous goose created by Paweł that has become a menace (created to protect other animals). The group devises a plan to capture the goose using a cage and specially made food. Despite having to traverse a ruined building, they manage to trap the goose, and the story concludes with Alex investigating a mysterious girl who seemed to be somehow connected to the goose.

Aktor w Opowieści:

* Dokonanie:
    * Built a breakdancing mechanical monkey as a distraction, carried a cage, and set up the trap for the goose.


### Pierwszy tajemniczy wielbiciel Liliany

* **uid:** 230212-pierwszy-tajemniczy-wielbiciel-liliany, _numer względny_: 8
* **daty:** 0111-10-22 - 0111-10-24
* **obecni:** Julia Kardolin, Laura Tesinik, Liliana Bankierz, Mariusz Kupieczka, Triana Porzecznik

Streszczenie:

Liliana, Triana, Julia pracują nad formą integracji scutiera i lancera by Liliana mogła pokonać (lub choć wytrzymać) Arkadię Verlen. Triana dostała alarm ze swojej zaginiętej drony o nieprzytomnych ludziach na Nieużytkach. Okazuje się, że portret Liliany wyssał tych ludzi. Okazuje się, że student AMZ, Mariusz, zrobił portret by Lilianę poderwać, ale spartaczył; porter wysysa. Potem portret został ukradziony. Liliana dalej podejrzewa spiski. Gdy Liliana i Mariusz się konfrontują, okazuje się że były 3 portrety. Liliana zachowuje się nikczemnie wobec Mariusza, więc się szybko odkochał. Ona nie skojarzyła, że komuś się może podobać.

Aktor w Opowieści:

* Dokonanie:
    * AMZ. Gdy Liliana i Triana chcą lecieć ratować nastolatków, ona wezwała służby (SRZ). Negocjuje z Laurą by ta powiedziała co może, dowiaduje się od nastolatków skąd zwinęli niebezpieczny portret a potem deeskaluje Lilianę by nie skrzywdziła Mariusza. Uzmysławia Lilianie, że Mariuszowi się mogła podobać, że to nie musi być spisek.


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 7
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * uratowała Torszeckiego przed anomalią mentalną używając ścigacza Franka Bulteriera nad którym pracowała. Uciekła z nim wysoko w powietrze i zanim zapomniała o anomalii zawiadomiła Marysię.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 6
* **daty:** 0111-05-30 - 0111-05-31
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * wezwała terminusów (Ulę) jak była ostrzeliwana i odwróciła railgun Lancera Arkadii. Potem przejęła dowodzenie nad zespołem - muszą oddać robota na czas XD


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 5
* **daty:** 0111-05-13 - 0111-05-15
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * zaczyna od używania dron Triany (kiepski pomysł, wpadły w berserk), szuka danych o somnibelu w bibliotece AMZ i ogólnie pozyskuje informacje od uczniów AMZ.


### Porywaczka miragentów

* **uid:** 210518-porywaczka-miragentow, _numer względny_: 4
* **daty:** 0111-05-07 - 0111-05-08
* **obecni:** Diana Lemurczak, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Triana Porzecznik

Streszczenie:

Lorena chciała zaimponować Gerwazemu i wysłała na Dianę miragenta. Diana przejęła miragenta, ale ten w rozpaczy ostrzelał Marysię Sowińską. Zespół znalazł Lorenę, zestrzelił ją (berserkerskimi dronami Triany) i poznał prawdę o miragencie, po czym udał się do klubu Arkadia w Podwiercie i odebrał Dianie miragenta. Nie powiedzieli nikomu innemu, że wiedzą o jej Eidolonie.

Aktor w Opowieści:

* Dokonanie:
    * zbadała ścigacz Marysi (odkrywając Lorenę) i włamała się z Trianą do klubu Arkadia (Diana Lemurczak). Aha, i unieszkodliwiła magicznie ścigacz Loreny.
* Progresja:
    * ma ogromne ułatwienie do wkradania się i przełamywania zabezpieczeń Diany w Arkadii (i nie tylko). Neurosprzężenie Diany daje Julii przewagę przy łamaniu jej zabezpieczeń.


### Potencjalnie eksterytorialny seksbot

* **uid:** 210406-potencjalnie-eksterytorialny-seksbot, _numer względny_: 3
* **daty:** 0111-04-27 - 0111-04-28
* **obecni:** Barnaba Burgacz, Feliks Keksik, Franek Bulterier, Henryk Wkrąż, Julia Kardolin, Lorena Gwozdnik, Marysia Sowińska, Rupert Mysiokornik, Sabina Kazitan, Triana Porzecznik

Streszczenie:

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

Aktor w Opowieści:

* Dokonanie:
    * 3 mc temu ratuje Trianę przed kocimi uszkami ostrym kebabem, teraz świetnie się skrada po złomowisku i uruchamia na wpół martwe urządzenia by spowolnić szabrowników aż Franek przyleci.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 2
* **daty:** 0111-04-22 - 0111-04-23
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * wymyśliła Myrczka do odzyskania techno-psa, po czym namówiła Myrczka do pomocy. Docelowo wymknęła się z Trianą i przechwyciła psa - kosztem zatruflowania okolic biurowców w Zaczęstwie.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 1
* **daty:** 0110-10-14 - 0110-10-22
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * technomantka i psychotroniczka; uczennica Akademii Magicznej Zaczęstwa. Postawiła ścigacz Remor340D i pomogła Napoleonowi wygrać turniej Rekinów. Rekiny często u niej zamawiają tuning ścigaczy i ich naprawę, specjalizuje się w ścigaczach.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0111-11-10
    1. Primus    : 10, @: 0111-11-10
        1. Sektor Astoriański    : 10, @: 0111-11-10
            1. Astoria    : 10, @: 0111-11-10
                1. Sojusz Letejski    : 10, @: 0111-11-10
                    1. Szczeliniec    : 10, @: 0111-11-10
                        1. Powiat Pustogorski    : 10, @: 0111-11-10
                            1. Czarnopalec    : 1, @: 0111-05-15
                                1. Pusta Wieś    : 1, @: 0111-05-15
                            1. Podwiert    : 7, @: 0111-11-10
                                1. Dzielnica Luksusu Rekinów    : 5, @: 0111-06-09
                                    1. Obrzeża Biedy    : 4, @: 0111-06-09
                                        1. Domy Ubóstwa    : 1, @: 0111-05-31
                                        1. Hotel Milord    : 1, @: 0111-05-15
                                        1. Stadion Lotników    : 2, @: 0111-06-09
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-06-09
                                        1. Skrytki Czereśniaka    : 1, @: 0111-06-09
                                    1. Serce Luksusu    : 2, @: 0111-06-09
                                        1. Apartamentowce Elity    : 2, @: 0111-06-09
                                1. Las Trzęsawny    : 6, @: 0111-11-10
                                    1. Schron TRZ-17    : 1, @: 0111-05-15
                                1. Magazyny Sprzętu Ciężkiego    : 1, @: 0111-11-10
                                1. Osiedle Leszczynowe    : 1, @: 0111-05-08
                                    1. Klub Arkadia    : 1, @: 0111-05-08
                                1. Sensoplex    : 1, @: 0110-10-22
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-22
                            1. Pustogor    : 1, @: 0111-05-31
                                1. Rdzeń    : 1, @: 0111-05-31
                                    1. Barbakan    : 1, @: 0111-05-31
                                        1. Kazamaty    : 1, @: 0111-05-31
                            1. Zaczęstwo    : 8, @: 0111-10-30
                                1. Akademia Magii, kampus    : 3, @: 0111-10-24
                                    1. Akademik    : 1, @: 0111-04-23
                                    1. Artefaktorium    : 2, @: 0111-10-24
                                    1. Audytorium    : 1, @: 0110-10-22
                                1. Akademia Magii    : 1, @: 0111-10-30
                                1. Biurowiec Gorland    : 1, @: 0111-04-23
                                1. Dzielnica Kwiecista    : 4, @: 0111-10-24
                                    1. Rezydencja Porzeczników    : 4, @: 0111-10-24
                                        1. Garaż Groźnych Eksperymentów    : 1, @: 0111-10-24
                                        1. Podziemne Laboratorium    : 1, @: 0111-04-23
                                1. Dzielnica Ogrodów    : 1, @: 0111-10-24
                                    1. Wielki Szpital Magiczny    : 1, @: 0111-10-24
                                1. Las Trzęsawny    : 1, @: 0111-05-31
                                1. Nieużytki Staszka    : 6, @: 0111-10-30
                                1. Park Centralny    : 1, @: 0111-04-23
                                    1. Jezioro Gęsie    : 1, @: 0111-04-23
                                1. Złomowisko    : 1, @: 0111-04-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 6 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Triana Porzecznik    | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Liliana Bankierz     | 4 | ((201013-pojedynek-akademia-rekiny; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Ignacy Myrczek       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Paweł Szprotka       | 3 | ((210615-skradziony-kot-olgi; 230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Urszula Miłkowicz    | 3 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Arkadia Verlen       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Lorena Gwozdnik      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Napoleon Bankierz    | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Teresa Mieralit      | 2 | ((201013-pojedynek-akademia-rekiny; 230303-the-goose-from-hell)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tomasz Tukan         | 1 | ((210713-rekin-wspiera-mafie)) |
| Wiktor Satarail      | 1 | ((210615-skradziony-kot-olgi)) |