# Karolina Terienak
## Identyfikator

Id: 2109-karolina-terienak

## Sekcja Opowieści

### Lea, strażniczka lasu

* **uid:** 240305-lea-strazniczka-lasu, _numer względny_: 22
* **daty:** 0111-10-21 - 0111-10-25
* **obecni:** Arkadia Verlen, Bogdan Gwiazdocisz, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lea Samszar, Malena Barandis, Marek Samszar, Mariusz Kupieczka, Marysia Sowińska, Michał Klabacz, Triana Porzecznik

Streszczenie:

Marysia zatrzymała Leę przed naruszaniem kulturowego tabu eternijskiego (stroju) i przy okazji dowiedziała się, że Lea próbuje pomóc innym, ale nikt nie może o tym wiedzieć. Tymczasem jej EfemeHorror poranił grupę Rekinów i AMZ współpracujących ze sobą w celu pomocy staremu 'archeologowi' szukającemu prawdy o ofiarach wojny.

Aktor w Opowieści:

* Dokonanie:
    * pomaga rannym w lesie i szybko transportuje wszystkich, po czym montuje oddział do walki z EfemeHorrorem który pociął AMZ i Rekiny.


### Ten nawiedzany i ta ukryta

* **uid:** 230325-ten-nawiedzany-i-ta-ukryta, _numer względny_: 21
* **daty:** 0111-10-16 - 0111-10-17
* **obecni:** Aleksandra Burgacz, Daniel Terienak, Franek Bulterier, Karolina Terienak, Lea Samszar, Michał Klabacz, Nadia Uprewien, Oliwier Czepek, Rupert Mysiokornik

Streszczenie:

Ola Burgacz, poślednia tienka zacnego rodu zorganizowała wyścig mający drażnić lokalsów Podwiertu, który skończył się: zwycięstwem Daniela, rozbiciem Mysiokornika (ktoś zatruł jego paliwo) i całusem Oli do Bulteriera. Tymczasem do Karo przyszedł centuś Oliwier prosić o pomoc dla pracownika, Michała, który jest "przeklęty". Karo i Michał weszli mu na chatę, znaleźli ślady magii ale co ważne nie na samym Michale. By pozbyć się wścibskiej sąsiadki, podłożyli jej szczura.

Okazuje się, że Ola dokuczała Michałowi i ów zrobił artefakt który podobno szedł z planów Triany (nonsens) i który nie miał prawa działać - a działa. Ola chciała mu potem pomóc, ale nie umiała. Za wszystkim stoi Lea Samszar, która lubiła Michała ale dla utrzymania swojej reputacji przed bardziej podłymi Rekinami poświęciła Michała w rytuale. Gdy nie wiedzący o tym Daniel i Karo dotarli do Lei i przywieźli jej nieprzytomnego Michała po ataku efemerycznego horroru, Lea podziękowała i powiedziała że mu pomoże. Jak inne REKINY poprosiły ją o pomoc, miała pretekst by mu pomóc.

A przy okazji, przypadkiem, Karo i Daniel sprowadzili na Podwiert plagę magicznych szczurów za które obwiniona jest AMZ...

Aktor w Opowieści:

* Dokonanie:
    * Rekin; przygotowała lepsze paliwo dla Daniela by ten wygrał wyścig, obiecała Oliwierowi że pomoże Michałowi, potem pozbyła się Nadii podkładając jej szczura. Rozmawiała z Olą by pozyskać info o historii jej i Michała a potem, niestety, porozmawiała z Leą by ta pomogła Michałowi i powiedziała prawdę. Lea, oczywiście, żąda protokołu komunikacyjnego więc tienki się pożarły - ale Lea pomoże Michałowi.


### Stary kot a Trzęsawisko

* **uid:** 220814-stary-kot-a-trzesawisko, _numer względny_: 20
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Alan Bartozol, Ariela Lechot, Diana Tevalier, Iwan Zawtrak, Karolina Terienak, Kot Rozbójnik, Marian Lechot, Urszula Miłkowicz, Viirai d'Lechotka

Streszczenie:

Właściciele fortifarmy Lechotka kiedyś dali radę wyrwać ziemię Trzęsawisku. Trzęsawisko nie zapomniało. Teraz jak są starzy i sami, Trzęsawisko porwało Arielę. Terminusi nie pomogli Marianowi, więc poszedł by ją uratować i został porwany. Ich anty-anomalny kot Rozbójnik nie był w stanie im pomóc; rannego kota znalazła w rowie Karolina. Zawiozła do Majkłapca, ale kot im uciekł. Karo współpracując z Ulą opanowały Rozbójnika, dogadały się z fortifarmą i z pomocą Alana zniszczyły ludzi porwanych przez Trzęsawisko. A Rozbójnik i fortifarma wpadły do zaskoczonej Uli.

Aktor w Opowieści:

* Dokonanie:
    * uratowała rannego kota z okolic Trzęsawiska i dała go do kociarni Zawtrak; potem szukawszy gdzie ów kot jest pomogła Uli wydostać się z self-inflicted roboty papierkowej. Po przekonaniu TAI Viirai, że naprawdę CHCĄ pomóc wezwała Alana i po rozwiązaniu misji chroniła życie kota przed terminusami. Wsadziła go w końcu Uli :D.


### Jak wsadzić Ulę Alanowi?

* **uid:** 220816-jak-wsadzic-ule-alanowi, _numer względny_: 19
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Karolina Terienak, Marysia Sowińska, Mimoza Diakon, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Części do Hestii nadal nie dotarły do Marysi - okazuje się, że Mimoza ją sabotuje. Więc jest ryzyko, że części do Hestii dojdą i Jeremi Sowiński będzie miał dostęp do pełni mocy Hestii a Marysia nie XD. Dodatkowo, Karo poprosiła Marysię by ta wsadziła Ulę Alanowi, ale nie chcą zabijać kota. Ale JAK wsadzić Alanowi Ulę (z kotem) jako uczennicę, skoro vistermin jest morderczy, zbliża się audyt Marysi a w Tukanie obudził się terminus (i nie chce ryzykować Uli)? I do tego Alan nie może się dowiedzieć? Do tego okazuje się że części do Hestii konkurują z normalnymi częściami potrzebnymi na tym terenie i DLATEGO Mimoza - paladynka cholerna - blokuje Marysię...

Aktor w Opowieści:

* Dokonanie:
    * po raz pierwszy w życiu poprosiła o coś Marysię - niech wsadzi Ulę Alanowi. Chroni informację o TAI i chroni fortifarmę przed Tukanem. Ostro kłóci się z Marysią XD.


### Supersupertajny plan Loreny

* **uid:** 220730-supersupertajny-plan-loreny, _numer względny_: 18
* **daty:** 0111-09-30 - 0111-10-04
* **obecni:** Arkadia Verlen, Daniel Terienak, Karolina Terienak, Liliana Bankierz, Marsen Gwozdnik, Władysław Owczarek, Żorż d'Namertel

Streszczenie:

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

Aktor w Opowieści:

* Dokonanie:
    * by ochronić lokalny biznes przed problemami z kontraktem z Ernestem poszła do Żorża i doszła do tego, że ktoś się podle podszywa. Karo zastawiła pułapkę z pomocą Arkadii i Daniela, złapała Marsena Gwozdnika i miała zwis - koleś zupełnie źle widzi Lorenę (swoją kuzynkę). Wmówiła mu, że Karo jest agentką Loreny i Marsen ma nie szkodzić terenowi bo plan Loreny.


### Płaszcz ochronny Mimozy

* **uid:** 220222-plaszcz-ochronny-mimozy, _numer względny_: 17
* **daty:** 0111-09-21 - 0111-09-25
* **obecni:** Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Lorena Gwozdnik, Marysia Sowińska, Mimoza Elegancja Diakon

Streszczenie:

Mimoza weszła do akcji - wzięła Lorenę do azylu i ochroniła część kultystek przed Ernestem. Konflikt Ernest - Mimoza się rozpalił, Marysia + Karo poszły się z Mimozą spotkać by zdeeskalować problem. Lorena podsłuchiwała, więc Mimoza straciła twarz. Cóż. Mimoza wynegocjowała przekazanie kultystek Sensacjuszowi, Marysia wynegocjowała by Lorena zrobiła ten akt. ALE W DOMU MIMOZY. Zaczyna się ciekawie...

Aktor w Opowieści:

* Dokonanie:
    * brutalnie wyciąga kiepsko ukrytą Lorenę i pokazuje ją grupie, wzmacniając ruchy Marysi. Przeraża Lorenę samą swoją obecnością.


### Marysiowa Hestia Rekinów

* **uid:** 220111-marysiowa-hestia-rekinow, _numer względny_: 16
* **daty:** 0111-09-16 - 0111-09-19
* **obecni:** Diana Tevalier, Ernest Namertel, Hestia d'Rekiny, Jeremi Sowiński, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Marysia Sowińska

Streszczenie:

Konflikt Liliana - Marysia się zaostrza. Marysia skupia się na wzmocnieniu Hestii i uzyskaniu kontroli nad Rekinami - przejęła Hestię jako sojuszniczkę. Chevaleresse poprosiła Karolinę o uratowanie Melissy i Karo faktycznie pomogła - Ernest wysłał Keirę i odbili Melissę. To sprawiło, że Mimoza Diakon poczuła się zagrożona i weszła do akcji. Jakby tego było mało, Sowińscy wysyłają Jeremiego by opanował kuzynkę.

Aktor w Opowieści:

* Dokonanie:
    * pod namową Chevaleresse zwalcza Kult Ośmiornicy wśród Rekinów; zlokalizowała gdzie jest Melissa (u Santino) i współpracując z Ernestem przechwyciła Melissę.


### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 15
* **daty:** 0111-09-09 - 0111-09-12
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * zmierzyła się z Napoleonem Bankierzem na arenie i go pokonała z łatwością; podniosła mu reputację. Ściągnęła brata, by ten odzyskał akt od Torszeckiego.


### Chevaleresse infiltruje Rekiny

* **uid:** 211221-chevaleresse-infiltruje-rekiny, _numer względny_: 14
* **daty:** 0111-09-06 - 0111-09-07
* **obecni:** Alan Bartozol, Barnaba Burgacz, Diana Tevalier, Hestia d'Rekiny, Justynian Diakon, Karolina Terienak, Marysia Sowińska, Melissa Durszenko, Rupert Mysiokornik, Santino Mysiokornik, Staś Arienik, Żorż d'Namertel

Streszczenie:

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

Aktor w Opowieści:

* Dokonanie:
    * złapała Chevaleresse (i tyci się z nią dogadała), dogoniła Stasia Arienika i przechwyciła go, po czym zastraszyła i oddała Alanowi Bartozolowi zrzucając ze ścigacza.


### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 13
* **daty:** 0111-08-30 - 0111-08-31
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * wyciągnęła z Arnolda czemu nie ma prądu, uratowała ludzi pracujących w magitrowni Rekinów przed pobiciem i wciągnęła Arkadię w opiernicz Natalii za mafię.


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 12
* **daty:** 0111-08-19 - 0111-08-24
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * uznała, że chce pomóc Iwanowi i rozwiązać problem lokalnego oddziału mafii; zebrała drużynę (Torszecki i Arkadia), po wyłapaniu kotów z Danielem manewrowała ścigaczem by uratować od mafii Arkadię i ewakuować Daniela.
* Progresja:
    * w Majkłapcu jest lubiana przez Iwana Zawtraka z Kociarni i ma jego zaufanie.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 11
* **daty:** 0111-08-11 - 0111-08-20
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * tymczasowo przeprowadziła się do Ernesta; zorientowała się, że przez brak Marysi zrobił się _power vacuum_ który przejął Justynian Diakon (XD). Zdobyła info i ostrzegła Marysię.


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 10
* **daty:** 0111-08-09 - 0111-08-10
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * pokazała, że jest świetna w walce - wrzuciła Sensacjusza (i Marysię) do glisty, staranowała Lorenę, zagrała Owadem w baseball. Skutecznie sklupała wszystko co ją chciało skrzywdzić. Don't mess with her.
* Progresja:
    * za atak Loreny pod wpływem na nią, Karolina połamała jej nogi. Opinia absolutnie bezwzględnej i niebezpieczniej. Don't EVER mess with her. Terror works.


### Torszecki pokazał kręgosłup

* **uid:** 211012-torszecki-pokazal-kregoslup, _numer względny_: 9
* **daty:** 0111-07-29 - 0111-07-30
* **obecni:** Amelia Sowińska, Jolanta Sowińska, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon

Streszczenie:

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

Aktor w Opowieści:

* Dokonanie:
    * poszła do Torszeckiego by ten przestał być dupą wołową i się ośmielił; wydobyła od niego, że to ON stoi za zniszczeniem ścigacza. Poszła z tym potem do Marysi... to skomplikowana sprawa. Wygarnęła Marysi, że traktowanie Torszeckiego częściowo do tego doprowadziło.


### Wysadzony żywy ścigacz

* **uid:** 210928-wysadzony-zywy-scigacz, _numer względny_: 8
* **daty:** 0111-07-26 - 0111-07-27
* **obecni:** Azalia Sernat d'Namertel, Ernest Namertel, Franek Bulterier, Karolina Terienak, Keira Amarco d'Namertel, Marysia Sowińska, Rafał Torszecki, Żorż d'Namertel

Streszczenie:

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

Aktor w Opowieści:

* Dokonanie:
    * zaprzyjaźniła się z Ernestem, ignorując bariery tienowatych. Zapoznała go z Marysią, podroczyła się odnośnie ścigaczy a potem by ratować Bulteriera ostrzelała go ścigaczem, wygrała z nim w powietrzu i niestety nie udało jej się uratować ścigacza Ernesta. Mimo wsparcia jego Krwi i Sieci.
* Progresja:
    * ma przyjaźń i zaufanie Ernesta Namertela. Ernest czuje z nią duży kontakt. Jest flow.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 7
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * połączyła Pustaka (osobę wiedzącą wszystko o Royalsach) z Marysią. De facto Pustak Karolinie teraz trochę wisi.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 6
* **daty:** 0111-07-07 - 0111-07-10
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * przesłuchała Lorenę i doszła do tego jakie informacje są w ścigaczu jej brata; odkryła, że istnieje grupa Wydry polująca na Rekiny.
* Progresja:
    * ma wroga w Lorenie Gwozdnik. Zbyt twardo weszła i rozerwała związek jej i jej brata.


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 5
* **daty:** 0111-07-03 - 0111-07-05
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * wow factor - zaimponowała Laurze i Ekaterinie przez manewrowanie mimo dopalonego Esuriit pnączoszpona. Potem, mimo uszkodzeń ścigacza, wpakowała go w jezioro by ratować Zespół przed duchami. I ścigacz umarł.
* Progresja:
    * jej ścigacz jest nieaktywny następne 3 tygodnie. Jest nie tylko popsuty (co jest spoko), ale też zalany (co nie jest spoko).
    * wywołała ogromne WOW u Laury i Ekateriny - za pnączoszpona, manewry i genialny ruch ze ścigaczem w jeziorze.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 4
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * by ratować porwanego brata skopała Torszeckiego, poszła na współpracę z Marysią Sowińską i nacisnęła na Ulę Miłkowicz (że użyje krwi) zmuszając ją do decyzji - chroni skórę czy ratuje innych.


### Karolina w Ciężkim Młocie

* **uid:** 220101-karolina-w-ciezkim-mlocie, _numer względny_: 3
* **daty:** 0111-04-25 - 0111-05-05
* **obecni:** Daniel Terienak, Henryk Murkot, Karolina Terienak, Mimoza Elegancja Diakon, Władysław Owczarek

Streszczenie:

Daniel chciał wygrać zakład z Mysiokornikiem o Mimozę Diakon i alkohol w barze Ciężki Młot. Karo chciała mu pomóc, więc poszła do tego baru rozeznać sprawę. Nie jest akceptowana przez ludzi pracy i próbuje ową akceptację uzyskać. Daniel chce jej pomóc więc robi artefaktyczną zaklątwioną lodówkę; Karo ratuje zakład recyklingowy i jest zaakceptowana w Młocie. Potem opieprza Daniela i Daniel rezygnuje z zakładu z Mysiokornikiem, nieszczęśliwy.

Aktor w Opowieści:

* Dokonanie:
    * wbiła się do knajpy Ciężki Młot i mimo niechęci do "AMZ / Rekina" została zaakceptowana po tym, jak pomogła w ogarnięciu anomalicznej awarii w zakładzie recyklingowym. Nie boi się ciężkiej i żmudnej roboty.
* Progresja:
    * zaakceptowana jako "swoja" w barze Ciężki Młot. Jest jedynym "nie-człowiekiem pracy" który jest tam traktowany jak swój.


### Dziewczyna i pies

* **uid:** 201215-dziewczyna-i-pies, _numer względny_: 2
* **daty:** 0110-11-15 - 0110-11-17
* **obecni:** Andrzej Kuncerzyk, Daniel Terienak, Franciszek Zygmunt, Grzegorz Terienak, Izydor Grumczewicz, Karolina Terienak, Patrycja Radniak, Paulina Mordoch, Tadeusz Łaśnic

Streszczenie:

Patrycja, coraz bardziej opętana przez Lucka/Esuriit pragnie odzyskać rodzinę. Znalazła "rodzinę" w Małopsie, gdzie "przygarnęła" Paulinę i Andrzeja, za cichym zezwoleniem tajemniczych władców Małopsa (którzy chcieli ją naprawić). Tymczasem trzy Rekiny rodu Terieniak poleciały do Małopsa odzyskać swojego kolegę, Izydora, i skończyło się na tym, że wyciągnęły "rodziców" Patrycji z jej mgły mentalnej i zmusiły Patrycję do ucieczki na mokradła.

Aktor w Opowieści:

* Dokonanie:
    * przemanewrowała dookoła gorathaula we mgle i porwała sobie Zygmunta (policjanta) by móc go przesłuchać w dogodnym momencie. Robi co chcą bracia, skupia się na ścigaczu.


### Impreza w Małopsie

* **uid:** 201201-impreza-w-malopsie, _numer względny_: 1
* **daty:** 0110-11-04 - 0110-11-06
* **obecni:** Andrzej Kuncerzyk, Barnaba Burgacz, Cezary Urmaszcz, Daniel Terienak, Franciszek Zygmunt, Izydor Grumczewicz, Karolina Terienak, Paulina Mordoch, Tadeusz Łaśnic

Streszczenie:

W Małopsie egzorcysta-amator zainfekowany demonem próbował zrobić doom-metalowy pesymistyczny koncert. To jest nielegalne (Pryzmat: tyrania optymizmu). Rekiny, które się pojawiły by to rozwiązać przeszły przez temat używając pięści - usunęły demona, zmieniły go w broń i oddały to Grzymościowcom (za to, że Wolny Uśmiech tu posprząta po nich). Ale skąd tu demon?

Aktor w Opowieści:

* Dokonanie:
    * Rekin. Miłośniczka swojego ścigacza i świetny pilot. Normalnie łagodna - ale brutalnie zdewastowała DJ Babu za uszkodzenie ścigacza.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 22, @: 0111-10-25
    1. Primus    : 22, @: 0111-10-25
        1. Sektor Astoriański    : 22, @: 0111-10-25
            1. Astoria    : 22, @: 0111-10-25
                1. Sojusz Letejski    : 22, @: 0111-10-25
                    1. Powiat Jastrzębski    : 2, @: 0110-11-17
                        1. Małopies, okolice    : 1, @: 0110-11-17
                            1. Mokradła    : 1, @: 0110-11-17
                        1. Małopies    : 2, @: 0110-11-17
                            1. Hodowla psów    : 1, @: 0110-11-06
                            1. Motel    : 1, @: 0110-11-06
                            1. Stadion sportowy    : 1, @: 0110-11-06
                    1. Szczeliniec    : 20, @: 0111-10-25
                        1. Powiat Pustogorski    : 20, @: 0111-10-25
                            1. Czółenko    : 1, @: 0111-08-31
                                1. Generatory Keriltorn    : 1, @: 0111-08-31
                            1. Majkłapiec    : 2, @: 0111-10-13
                                1. Farma Krecik    : 1, @: 0111-08-24
                                1. Kociarnia Zawtrak    : 2, @: 0111-10-13
                                1. Wegefarma Myriad    : 1, @: 0111-08-24
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-24
                            1. Podwiert, okolice    : 1, @: 0111-10-13
                                1. Fortifarma Lechotka    : 1, @: 0111-10-13
                            1. Podwiert    : 19, @: 0111-10-25
                                1. Dzielnica Luksusu Rekinów    : 15, @: 0111-10-25
                                    1. Fortyfikacje Rolanda    : 4, @: 0111-09-25
                                    1. Obrzeża Biedy    : 4, @: 0111-09-25
                                        1. Domy Ubóstwa    : 3, @: 0111-09-25
                                        1. Hotel Milord    : 3, @: 0111-09-25
                                        1. Stadion Lotników    : 4, @: 0111-09-25
                                        1. Stajnia Rumaków    : 3, @: 0111-09-25
                                    1. Sektor Brudu i Nudy    : 5, @: 0111-09-25
                                        1. Komputerownia    : 4, @: 0111-09-25
                                        1. Konwerter Magielektryczny    : 3, @: 0111-09-25
                                        1. Magitrownia Pogardy    : 3, @: 0111-09-25
                                        1. Skrytki Czereśniaka    : 4, @: 0111-09-25
                                    1. Serce Luksusu    : 11, @: 0111-09-25
                                        1. Apartamentowce Elity    : 7, @: 0111-09-25
                                        1. Arena Amelii    : 5, @: 0111-09-25
                                        1. Fontanna Królewska    : 3, @: 0111-09-25
                                        1. Kawiarenka Relaks    : 3, @: 0111-09-25
                                        1. Lecznica Rannej Rybki    : 7, @: 0111-09-25
                                1. Kompleks Korporacyjny    : 5, @: 0111-10-17
                                    1. Bar Ciężki Młot    : 2, @: 0111-10-17
                                    1. Dokumentarium Terminuskie    : 1, @: 0111-10-13
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-31
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-31
                                    1. Zakład Recyklingu Owczarek    : 2, @: 0111-10-04
                                1. Las Trzęsawny    : 5, @: 0111-10-25
                                    1. Jeziorko Mokre    : 2, @: 0111-07-10
                                1. Osiedle Rdzawych Dębów    : 1, @: 0111-10-17
                                    1. Sklep Eliksir Siekiery    : 1, @: 0111-10-17
                                1. Osiedle Tęczy    : 1, @: 0111-07-10
                            1. Pustogor    : 1, @: 0111-08-10
                                1. Rdzeń    : 1, @: 0111-08-10
                                    1. Szpital Terminuski    : 1, @: 0111-08-10
                            1. Trzęsawisko Zjawosztup    : 1, @: 0111-10-13
                            1. Zaczęstwo    : 4, @: 0111-09-25
                                1. Akademia Magii, kampus    : 3, @: 0111-09-25
                                    1. Akademik    : 3, @: 0111-09-25
                                    1. Arena Treningowa    : 3, @: 0111-09-25
                                    1. Las Trzęsawny    : 3, @: 0111-09-25
                                1. Osiedle Ptasie    : 1, @: 0111-06-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 15 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220816-jak-wsadzic-ule-alanowi; 240305-lea-strazniczka-lasu)) |
| Daniel Terienak      | 12 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies; 210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Rafał Torszecki      | 9 | ((210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211127-waśń-o-ryby-w-majklapcu; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 8 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Arkadia Verlen       | 5 | ((210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny; 240305-lea-strazniczka-lasu)) |
| Keira Amarco d'Namertel | 5 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 240305-lea-strazniczka-lasu)) |
| Lorena Gwozdnik      | 5 | ((210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Sensacjusz Diakon    | 4 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Tomasz Tukan         | 4 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Diana Tevalier       | 3 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow; 220814-stary-kot-a-trzesawisko)) |
| Hestia d'Rekiny      | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Liliana Bankierz     | 3 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220730-supersupertajny-plan-loreny)) |
| Urszula Miłkowicz    | 3 | ((210720-porwanie-daniela-terienaka; 220814-stary-kot-a-trzesawisko; 220816-jak-wsadzic-ule-alanowi)) |
| Żorż d'Namertel      | 3 | ((210928-wysadzony-zywy-scigacz; 211221-chevaleresse-infiltruje-rekiny; 220730-supersupertajny-plan-loreny)) |
| Alan Bartozol        | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Andrzej Kuncerzyk    | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 2 | ((201201-impreza-w-malopsie; 211221-chevaleresse-infiltruje-rekiny)) |
| Franciszek Zygmunt   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Franek Bulterier     | 2 | ((210928-wysadzony-zywy-scigacz; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Iwan Zawtrak         | 2 | ((211127-waśń-o-ryby-w-majklapcu; 220814-stary-kot-a-trzesawisko)) |
| Izydor Grumczewicz   | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Laura Tesinik        | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Lea Samszar          | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 2 | ((211102-satarail-pomaga-marysi; 240305-lea-strazniczka-lasu)) |
| Michał Klabacz       | 2 | ((230325-ten-nawiedzany-i-ta-ukryta; 240305-lea-strazniczka-lasu)) |
| Mimoza Elegancja Diakon | 2 | ((220101-karolina-w-ciezkim-mlocie; 220222-plaszcz-ochronny-mimozy)) |
| Paulina Mordoch      | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Rupert Mysiokornik   | 2 | ((211221-chevaleresse-infiltruje-rekiny; 230325-ten-nawiedzany-i-ta-ukryta)) |
| Tadeusz Łaśnic       | 2 | ((201201-impreza-w-malopsie; 201215-dziewczyna-i-pies)) |
| Władysław Owczarek   | 2 | ((220101-karolina-w-ciezkim-mlocie; 220730-supersupertajny-plan-loreny)) |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Bogdan Gwiazdocisz   | 1 | ((240305-lea-strazniczka-lasu)) |
| Cezary Urmaszcz      | 1 | ((201201-impreza-w-malopsie)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Terienak    | 1 | ((201215-dziewczyna-i-pies)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Murkot        | 1 | ((220101-karolina-w-ciezkim-mlocie)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Malena Barandis      | 1 | ((240305-lea-strazniczka-lasu)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Mariusz Kupieczka    | 1 | ((240305-lea-strazniczka-lasu)) |
| Mariusz Trzewń       | 1 | ((210720-porwanie-daniela-terienaka)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Patrycja Radniak     | 1 | ((201215-dziewczyna-i-pies)) |
| Paweł Szprotka       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Triana Porzecznik    | 1 | ((240305-lea-strazniczka-lasu)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |