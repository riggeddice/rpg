# Gabriel Ursus
## Identyfikator

Id: 2004-gabriel-ursus

## Sekcja Opowieści

### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 12
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * ściągnął Ulę by dać jej kontakty z Aurum jako "dziewczyna do porwania", załatwił papiery z Tukanem (!?), przedłożył Aurum i relacje nad Pustogor - ale stał dzielnie przeciw efemerydzie i nie zostawił Uli samej; nie uciekł przed Tymonem.
* Progresja:
    * poważnie nadwątlił zaufanie Pięknotki, Tymona i dużej ilości terminusów Pustogoru. Przedłożył interesy Aurum - dwóch smarkaczy - nad dobro regionu.


### Narodziny paladynki Saitaera

* **uid:** 201011-narodziny-paladynki-saitaera, _numer względny_: 11
* **daty:** 0110-10-12 - 0110-10-13
* **obecni:** Agaton Ociegor, Aleksander Muniakiewicz, Gabriel Ursus, Minerwa Metalia, Pięknotka Diakon, Sabina Kazitan, Saitaer

Streszczenie:

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

Aktor w Opowieści:

* Dokonanie:
    * nie potrafił zostawić Sabiny Kazitan samej w spokoju; Pięknotka zmusiła go do porzucenia wszelkich vendett. Rody Ursus i Kazitan nie są już w stanie wojny.
* Progresja:
    * zrezygnował z walki przeciw Sabinie Kazitan. Ani on ani jego ród nie będą z nią walczyć i są przed nią zabezpieczeni.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 10
* **daty:** 0110-10-07 - 0110-10-09
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * skonfliktowany między zemstą na Sabinie Kazitan a zrobienie tego co należy. W końcu wybrał ochronę Myrczka i opieprzenie Rekinów nad zemstę nad Sabiną.
* Progresja:
    * Lorena Gwozdnik, Rekin, zakochała się w nim na zabój z uwagi na dezinhibitor. Gorzej niż miłość, to "oddanie".


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 9
* **daty:** 0110-09-18 - 0110-09-21
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * arystokrata, który bardzo próbował rozbroić mysz Esuriit zanim ktokolwiek ucierpi. Ściągnął Laurę i katalitycznie chronił klatkę. Laura nad nim dominuje na akcji.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 8
* **daty:** 0110-09-07 - 0110-09-11
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * zauważył różnice między aurami, spotkał się z Natalią przy Fortifarmie Irrydii i został sklupany przez Kallistę. Stracił o tym pamięć i skończył w Szpitalu Terminuskim.


### Rekin z Aurum i fortifarma

* **uid:** 200509-rekin-z-aurum-i-fortifarma, _numer względny_: 7
* **daty:** 0110-08-30 - 0110-09-04
* **obecni:** Artur Kołczond, Artur Michasiewicz, Erwin Galilien, Gabriel Ursus, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Tadeusz Tessalon

Streszczenie:

Sabina Kazitan ostrzegła Pięknotkę, że Natalia Tessalon z Aurum zażądała od niej niebezpiecznego rytuału do hodowania potwora. Pięknotka doszła do tego z sygnatury energii magicznej, że problem jest w okolicy Studni Irrydiańskiej; tam jest też fortifarma w której stary były-terminus tępi Latające Rekiny. I faktycznie - arystokraci z Aurum stworzyli potwora (krwawego) a Pięknotka Cieniem go zniszczyła. Niestety, brat Natalii został ciężko porażony i nigdy nie będzie już taki jak kiedyś. Aha, kwatermistrz opieprza Pięknotkę.

Aktor w Opowieści:

* Dokonanie:
    * źródło informacji o arystokracji Aurum i jak myślą o "prowincji". Jako katalista, pomógł Pięknotce znaleźć miejsce związane z Krwawym Potworem. Załamany losem Tadeusza Tessalona.
* Progresja:
    * eks Natalii Tessalon. Lubi ją i jej brata. Niechętnie walczy / działa przeciwko nim. Kiedyś ją zdradził i z nim zerwała, potem uciekł "na prowincję".


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 6
* **daty:** 0110-08-24 - 0110-08-26
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * przywołany do pomocy przez Pięknotkę by przenieść uszkodzonego elektronicznego DJa do Erwina; tam odkryli obecność Infiltratora Iniekcyjnego Aurum.


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 5
* **daty:** 0110-08-16 - 0110-08-21
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * walczył w obronie Czemerty przed bioformami Trzęsawiska; został ranny jako pilot awiana.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 4
* **daty:** 0110-08-12 - 0110-08-14
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * nienawidzi Sabiny Kazitan; wykrył dla Pięknotki jej sekret - płynnie kontroluje czarną magię. Przekonany, by ukryć ten sekret przed wszystkimi poza Karlą.


### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 3
* **daty:** 0110-06-30 - 0110-07-02
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * dowodził terminuskimi praktykantami ze średnią kompetencją do momentu pojawienia się Pięknotki.


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 2
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Nocnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * kuzyn Lilii, terminus-szperacz; idealista. Doskonale zauważa rzeczy (ale Amandy - nie). Dokładny, nieco biurokratyczny i trochę naiwny.


### Zagubiony efemerydyta

* **uid:** 190917-zagubiony-efemerydyta, _numer względny_: 1
* **daty:** 0110-06-21 - 0110-06-22
* **obecni:** Gabriel Ursus, Jan Uszczar, Marek Puszczok, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Młody efemerydyta uciekł z Eterni. Był przyciskany by dołączyć do mafii więc czasem w panice rzucał niewłaściwe zaklęcia w złych momentach. Pięknotka go znalazła i zajęła się nim - oddała go Orbiterowi, bo tam się może przydać. Mafia musi się obejść smakiem.

Aktor w Opowieści:

* Dokonanie:
    * katalista wspierający Pięknotkę. Dużo biega i zbiera informacji - z Fortu Mikado lub innych miejsc.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 12, @: 0110-10-27
    1. Primus    : 12, @: 0110-10-27
        1. Sektor Astoriański    : 12, @: 0110-10-27
            1. Astoria    : 12, @: 0110-10-27
                1. Sojusz Letejski    : 12, @: 0110-10-27
                    1. Szczeliniec    : 12, @: 0110-10-27
                        1. Powiat Jastrzębski    : 2, @: 0110-10-13
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-09
                                1. Blokhaus Widmo    : 1, @: 0110-10-09
                            1. Praszalek, okolice    : 1, @: 0110-10-13
                                1. Lasek Janor    : 1, @: 0110-10-13
                                1. Park rozrywki Janor    : 1, @: 0110-10-13
                        1. Powiat Pustogorski    : 11, @: 0110-10-27
                            1. Czemerta, okolice    : 2, @: 0110-09-11
                                1. Baza Irrydius    : 1, @: 0110-09-11
                                1. Fortifarma Irrydia    : 2, @: 0110-09-11
                                1. Studnia Irrydiańska    : 2, @: 0110-09-11
                            1. Czemerta    : 1, @: 0110-08-21
                            1. Czółenko    : 1, @: 0110-09-21
                                1. Bunkry    : 1, @: 0110-09-21
                                1. Pola Północne    : 1, @: 0110-09-21
                            1. Podwiert, okolice    : 2, @: 0110-07-02
                                1. Bioskładowisko podziemne    : 2, @: 0110-07-02
                            1. Podwiert    : 3, @: 0110-10-09
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0110-07-02
                                1. Osiedle Leszczynowe    : 1, @: 0110-09-21
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0110-09-21
                            1. Pustogor    : 4, @: 0110-09-11
                                1. Eksterior    : 3, @: 0110-09-11
                                    1. Dolina Uciech    : 1, @: 0110-06-22
                                    1. Fort Mikado    : 1, @: 0110-06-22
                                    1. Miasteczko    : 3, @: 0110-09-11
                                        1. Knajpa Górska Szalupa    : 3, @: 0110-09-11
                                    1. Zamek Weteranów    : 1, @: 0110-08-14
                                1. Gabinet Pięknotki    : 1, @: 0110-09-04
                                1. Knajpa Górska Szalupa    : 1, @: 0110-09-04
                                1. Rdzeń    : 2, @: 0110-09-11
                                    1. Szpital Terminuski    : 2, @: 0110-09-11
                            1. Zaczęstwo    : 4, @: 0110-10-27
                                1. Akademia Magii, kampus    : 2, @: 0110-10-09
                                    1. Arena Treningowa    : 1, @: 0110-10-09
                                    1. Budynek Centralny    : 1, @: 0110-08-26
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowiec Gorland    : 1, @: 0110-10-27
                                1. Cyberszkoła    : 1, @: 0110-08-26
                                1. Dzielnica Kwiecista    : 1, @: 0110-10-27
                                    1. Rezydencja Porzeczników    : 1, @: 0110-10-27
                                        1. Podziemne Laboratorium    : 1, @: 0110-10-27
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Nieużytki Staszka    : 3, @: 0110-10-27
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-26
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-08-21
                            1. Laboratorium W Drzewie    : 1, @: 0110-08-21

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy; 190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Sabina Kazitan       | 6 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 4 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Laura Tesinik        | 4 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200616-bardzo-straszna-mysz; 201006-dezinhibitor-dla-sabiny)) |
| Amanda Kajrat        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Erwin Galilien       | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Minerwa Metalia      | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201011-narodziny-paladynki-saitaera)) |
| Napoleon Bankierz    | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 2 | ((190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska)) |
| Saitaer              | 2 | ((190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Strażniczka Alair    | 2 | ((200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Tomasz Tukan         | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 2 | ((200510-tajna-baza-orbitera; 201020-przygoda-randka-i-porwanie)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Marek Puszczok       | 1 | ((190917-zagubiony-efemerydyta)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Wiktor Satarail      | 1 | ((200418-wojna-trzesawiska)) |