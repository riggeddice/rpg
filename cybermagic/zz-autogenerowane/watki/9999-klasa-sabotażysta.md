# Klasa Sabotażysta
## Identyfikator

Id: 9999-klasa-sabotażysta

## Sekcja Opowieści

### Relikwia z androida?!

* **uid:** 240214-relikwia-z-androida, _numer względny_: 7
* **daty:** 0107-05-16 - 0107-05-18
* **obecni:** Aerina Cavalis, Alina Mekran, Elwira Barknis, Kalista Surilik, Klasa Dyplomata, Klasa Hacker, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong, Tadeusz Mekran, Vanessa d'Cavalis

Streszczenie:

Zniknęły szczątki Vanessy - bohaterskiego androida, wydane trzem biologom. Niewyjaśnione polecenie wydania szczątków Vanessy i niespodziewane pojawienie się fanatyków walczących w jej imieniu na podziemnych arenach, zainfekowanych niemożliwymi do zrobienia na tej stacji hipnonarkotykami, wskazało na problem z anomaliami magicznymi. Okazało się, że przyczyną było morderstwo Aliny Mekran przez Elwirę Barknis, której plagiat i agresywne metody w pracy wyszły na jaw. Alina, próbująca wrócić do życia jako "resurrector" za pomocą roślin i hydroponiki, potrzebowała energii, krwi i ciała. Agencja była w stanie zamaskować sytuację, zniszczyć zainfekowany orbitujący dookoła Szernief hydrolab H4 i zabrali Vanessę jako influencerkę turystyczną poza Stację.

Aktor w Opowieści:

* Dokonanie:
    * skuteczne negocjacje z Kalistą, które odsłoniły kluczowe informacje dotyczące Elwiry i Aliny, umożliwiając Agencji głębsze zrozumienie sytuacji i dalsze kroki działania. Potem - współpraca z Inżynierem by zniszczyć H4 Hydrolab używając płomieni silników Szernief.


### Dla swych marzeń, warto!

* **uid:** 240117-dla-swych-marzen-warto, _numer względny_: 6
* **daty:** 0106-11-04 - 0106-11-06
* **obecni:** Aerina Cavalis, Artur Tavit, Estril Cavalis, Felina Amatanir, Kalista Surilik, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Larkus Talvinir, Maia Sakiran, Mawir Hong, Vanessa d'Cavalis

Streszczenie:

Na Stacji pojawił się pierścień Esuriit, który sprawia, że ludzie potrafią poświęcić energię do spełnienia obsesyjnych marzeń kosztem życia tych ludzi. Za pierścieniem przybył Szmaciarz, próbujący dodać pierścień do swojej kolekcji artefaktów. Zespołowi udało się zatrzymać Skażenie i uratować członka rady - stracili jednak Vanessę (biosyntkę) oraz Kalista została Aktywowana. Za to zespół zintegrował biosynty z resztą stacji.

Aktor w Opowieści:

* Dokonanie:
    * uratował Artura i ostrzelał Maię; zaproponował pomnik dla Vanessy by zintegrować biosynty z ludźmi


### Pan Skarpetek i Odratowany Ogród

* **uid:** 231221-pan-skarpetek-i-odratowany-ogrod, _numer względny_: 5
* **daty:** 0106-04-25 - 0106-04-27
* **obecni:** Felina Amatanir, Ignatius Sozyliw, Kalista Surilik, Klasa Inżynier, Klasa Oficer Naukowy, Klasa Sabotażysta, Mawir Hong

Streszczenie:

Rada wezwała Agencję, bo dzieciaki potrafią rozkazywać dorosłym używając savarańskiej postaci z bajek, 'Pana Skarpetka'. Agencja odkrywa, że dzieciaki próbują zrobić Rajski Ogród - miejsce z roślinami i radością. Gdy Agencja orientuje się, że to jest niemożliwe technicznie, odkrywają wpływ energii Alteris. Po przebijaniu się przez Alteris udało im się uratować dzieci, ale pomieszczenie jest Zniekształcone i trzeba było Kaliście coś powiedzieć o magii... za to, Agencja ma poprawioną reputację, choć ma opinię tych co promują współpracę ludów na stacji.

Aktor w Opowieści:

* Dokonanie:
    * porozmawiał z Ignatiusem i dowiedział się o wpływie Pana Skarpetka; potem załatwił sprawę i pozyskał dzieci ze stacji do lapisowania na Luminariusie, usuwając efemerydę Pana Skarpetka. Wyszedł na dziwnego łosia w oczach stacji, ale udało mu się usunąć Anomalię.


### Przeznaczeniem Szernief nie jest wojna domowa

* **uid:** 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa, _numer względny_: 4
* **daty:** 0105-12-11 - 0105-12-13
* **obecni:** Aerina Cavalis, Dorion Fughar, Felina Amatanir, Gabriel Septenas, Kaldor Czuk, Kalista Surilik, Klasa Dyplomata, Klasa Inżynier, Klasa Sabotażysta, Mawir Hong, Sia-03 Szernief

Streszczenie:

Na Stacji doszło do odprawienia savarańskiego rytuału Krwi dobrobytu w formie energii Fidetis. Problem w tym, że savaranie mają jedną wizję sukcesu a Szernief ma N grup. Więc każda grupa jest wzmacniana w _inny_ sposób, co powoduje ciekawe problemy (np. sabotaże by działali razem, lub savaranie atakujący Mawirowców by ci mogli być silniejsi). Agencja odkrywa obecność Łowcy (polującego na Radę) i go neutralizuje. Niestety, Tkaczka integruje się z Sią-03 tworząc nową frakcję.

Aktor w Opowieści:

* Dokonanie:
    * określił że Aerinie nic nie groziło przy sabotażu Sii; doszedł do jej motywu. Udowodnił obecność magii przez nienaturalne sabotaże. Doszedł do roli Toma-74 i że on był przyczyną.


### Polowanie na biosynty na Szernief

* **uid:** 231213-polowanie-na-biosynty-na-szernief, _numer względny_: 3
* **daty:** 0105-07-26 - 0105-07-31
* **obecni:** Artur Tavit, Delgado Vitriol, Felina Amatanir, Kalista Surilik, Klasa Biurokrata, Klasa Dyplomata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, Malik Darien, Sebastian-194, Vanessa d'Cavalis

Streszczenie:

Kalista wezwała Agencję bo biosynty są niszczone (ludzie zabijają je pierwsi) i dzieje się 'coś dziwnego' a 'człowiek Agencji' jest niekompetentny - okazało się, że jest Skażony Unumens (ma pasożyta). Agencja zrzuciła 'legalność' biosyntów na arystokratę Artura, zapewnia sobie współpracę z biosyntami i kieruje je do zniszczenia Parasekta Unumens. By odwrócić uwagę od swoich działań - zdradza Kalistę i Mawira (i wszystkie problemy zrzuca na nich). Udało się jednak większość uratować, mimo śmierci kilku osób w tak trudnej sytuacji.

Aktor w Opowieści:

* Dokonanie:
    * wbija do pomieszczenia z Malikiem eksplozją i zatrzymuje go przed samobójstwem; gdy idzie porozmawiać z Delgado to zabija na jego oczach savaranina by zyskać zaufanie. Porywa człowieka by testować antidotum na Parasekta i odwraca uwagę od ruchów biosyntów by te mogły wejść i zniszczyć Parasekta.


### Tajemnicze Tunele Sebirialis

* **uid:** 231119-tajemnicze-tunele-sebirialis, _numer względny_: 2
* **daty:** 0104-11-20 - 0104-11-24
* **obecni:** Aniela Kafantelas, Eryk Kawanicz, Felina Amatanir, Ignatius Sozyliw, Janvir Krassus, Kalista Surilik, Klasa Dyplomata, Klasa Sabotażysta, Larkus Talvinir, Leon Hurmniow, Marta Sarilit, OLU Luminarius

Streszczenie:

W tunelach kopalni pod CON Szernief znikają ludzie. Agencja dochodzi do tego, że winny jest protomag Dotknięty Alteris, który uciekł z tajnej (dla populacji Szernief) placówki NavirMed gdzie prowadzi się badania na ludziach. Agencja doszła do łańcucha logicznego i odkryła prawdę; by to rozwiązać, ewakuowali kopalnię i placówkę NavirMed na 3 miesiące by protomag umarł z głodu.

Aktor w Opowieści:

* Dokonanie:
    * Riana Sanesset z Lux Umbrarum; doszła do tego, że stacja jest finansowo zaniedbana i z pomocą Luminariusa wbiła się do systemów stacji. Dobra w zastraszaniu, skłoniła Martę z NavirMed do współpracy. Decyduje o ewakuacji stacji.


### Złomowanie legendarnej Anitikaplan

* **uid:** 240111-zlomowanie-legendarnej-anitikaplan, _numer względny_: 1
* **daty:** 0084-04-02 - 0084-04-06
* **obecni:** Klasa Biurokrata, Klasa Hacker, Klasa Oficer Naukowy, Klasa Sabotażysta, SN Anitikaplan, SN Varilen, Twaróg Worl, Wiktor Worl

Streszczenie:

Anitikaplan – legendarna jednostka od bajek z Tygryskiem została zniszczona podczas Wojny Deoriańskiej przez grupę fanatyków. Gdy zostały kupione prawa do zezłomowania Anitikaplan i gdy zaczęły dziać się anomalne rzeczy na pokładzie, Luminarius przybył, by usunąć anomalię. Anitikaplan próbowała przywrócić przeszłość do teraźniejszości; Sempitus i Alteris wspólnie kontynuowały zarówno Wojnę Deoriańską jak i ‘podaruj wszystkim dzieciom radość’. Agencji – mimo dużych strat w załodze – udało się wyczyścić Anitikaplan i zachować sekret magii przed populacją, zgodnie z misją.

Aktor w Opowieści:

* Dokonanie:
    * zniszczyła wolne TAI na pokładzie Anitikaplan a potem skutecznie rozmontowała Anomalię Alteris-Sempitus w Krypcie, czerpiąc z energii magicznej dookoła.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0107-05-18
    1. Primus    : 7, @: 0107-05-18
        1. Sektor Kalmantis    : 7, @: 0107-05-18
            1. Sebirialis, orbita    : 6, @: 0107-05-18
                1. CON Szernief    : 6, @: 0107-05-18
                    1. Orbitujące stacje hydroponiczne    : 1, @: 0107-05-18
                    1. Powłoka Wewnętrzna    : 1, @: 0105-07-31
                        1. Poziom Minus Dwa    : 1, @: 0105-07-31
                            1. Więzienie    : 1, @: 0105-07-31
                        1. Poziom Minus Jeden    : 1, @: 0105-07-31
                            1. Obszar Mieszkalny Savaran    : 1, @: 0105-07-31
                        1. Poziom Minus Trzy    : 1, @: 0105-07-31
                            1. Wielkie Obrady    : 1, @: 0105-07-31
            1. Sebirialis    : 1, @: 0104-11-24
                1. Krater Ablardius    : 1, @: 0104-11-24
                    1. Kopalnie Ablardius    : 1, @: 0104-11-24
                1. Płaskowyż Zaitrus    : 1, @: 0104-11-24
                    1. Ukryta Baza NavirMed    : 1, @: 0104-11-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kalista Surilik      | 6 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Felina Amatanir      | 5 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto)) |
| Klasa Oficer Naukowy | 5 | ((231213-polowanie-na-biosynty-na-szernief; 231221-pan-skarpetek-i-odratowany-ogrod; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Dyplomata      | 4 | ((231119-tajemnicze-tunele-sebirialis; 231213-polowanie-na-biosynty-na-szernief; 231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240214-relikwia-z-androida)) |
| Klasa Hacker         | 4 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Mawir Hong           | 4 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Aerina Cavalis       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Klasa Inżynier       | 3 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa; 231221-pan-skarpetek-i-odratowany-ogrod; 240214-relikwia-z-androida)) |
| Vanessa d'Cavalis    | 3 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto; 240214-relikwia-z-androida)) |
| Artur Tavit          | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240117-dla-swych-marzen-warto)) |
| Ignatius Sozyliw     | 2 | ((231119-tajemnicze-tunele-sebirialis; 231221-pan-skarpetek-i-odratowany-ogrod)) |
| Klasa Biurokrata     | 2 | ((231213-polowanie-na-biosynty-na-szernief; 240111-zlomowanie-legendarnej-anitikaplan)) |
| Larkus Talvinir      | 2 | ((231119-tajemnicze-tunele-sebirialis; 240117-dla-swych-marzen-warto)) |
| Alina Mekran         | 1 | ((240214-relikwia-z-androida)) |
| Aniela Kafantelas    | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Delgado Vitriol      | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Dorion Fughar        | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Elwira Barknis       | 1 | ((240214-relikwia-z-androida)) |
| Eryk Kawanicz        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Estril Cavalis       | 1 | ((240117-dla-swych-marzen-warto)) |
| Gabriel Septenas     | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Janvir Krassus       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Kaldor Czuk          | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| Leon Hurmniow        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Maia Sakiran         | 1 | ((240117-dla-swych-marzen-warto)) |
| Malik Darien         | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Marta Sarilit        | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| OLU Luminarius       | 1 | ((231119-tajemnicze-tunele-sebirialis)) |
| Sebastian-194        | 1 | ((231213-polowanie-na-biosynty-na-szernief)) |
| Sia-03 Szernief      | 1 | ((231216-przeznaczeniem-szernief-nie-jest-wojna-domowa)) |
| SN Anitikaplan       | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| SN Varilen           | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Tadeusz Mekran       | 1 | ((240214-relikwia-z-androida)) |
| Twaróg Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |
| Wiktor Worl          | 1 | ((240111-zlomowanie-legendarnej-anitikaplan)) |