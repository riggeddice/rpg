# Paweł Szprotka
## Identyfikator

Id: 9999-paweł-szprotka

## Sekcja Opowieści

### An Unfortunate Ratnapping

* **uid:** 230331-an-unfortunate-ratnapping, _numer względny_: 5
* **daty:** 0111-11-08 - 0111-11-10
* **obecni:** Alex Deverien, Carmen Deverien, Julia Kardolin, kot-pacyfikator Tobias, Paweł Szprotka, Radosław Turkamenin

Streszczenie:

In the city of Podwiert, magical rats cause chaos by draining electricity. Alex, Carmen, Julia, and their cat Tobias are sent by their magical school to solve the problem. They identify the rat nest in the warehouses and learn about the involvement of a mercenary company, the Guardians of Harmony, who have been secretly reducing the rat population.

Using a specially designed trap, the team captures the rats and encounters Radosław, the leader of the local Guardians of Harmony cell, who confronts them. They deny responsibility for the rat problem and continue their mission.

When Tobias goes missing, they find him in a Guardians' truck filled with caged rats. After a scuffle, Alex and Carmen steal the cages and accidentally swap Guardian's soul with that of a random rat. Earlier on, they had one problem. Now they have two.

Aktor w Opowieści:

* Dokonanie:
    * assists Julia in designing the perfect rat trap and lure. Not really present; more of advisory role.


### The goose from hell

* **uid:** 230303-the-goose-from-hell, _numer względny_: 4
* **daty:** 0111-10-28 - 0111-10-30
* **obecni:** Alex Deverien, Alicja Trawlis, Carmen Deverien, Julia Kardolin, kot-pacyfikator Tobias, Paweł Szprotka, Teresa Mieralit

Streszczenie:

A cat-pacifier named Tobias belonging to Carmen and Alex got shot at by some random illegal hunters. Carmen, Alex, and Julia are tasked by their ethics teacher to deal with an anomalous goose created by Paweł that has become a menace (created to protect other animals). The group devises a plan to capture the goose using a cage and specially made food. Despite having to traverse a ruined building, they manage to trap the goose, and the story concludes with Alex investigating a mysterious girl who seemed to be somehow connected to the goose.

Aktor w Opowieści:

* Dokonanie:
    * Created the anomalous goose to protect animals from stupid hunters, made the tastiest goose food, and provided guidance on setting the trap to capture a goose.


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 3
* **daty:** 0111-08-19 - 0111-08-24
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * zajmuje się leczeniem ryb i gęsi w Majkłapcu z ran kotów-pacyfikatorów (z ramienia farmy Krecik). Z samego nagrania zauważył, że koty były czymś naćpane - ktoś im coś podał. Karo poprowadziła go by z Danielem stworzył detektor kotów po sierści.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 2
* **daty:** 0111-08-01 - 0111-08-05
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * przyznał się Marysi, że jest zbiegłym eternianinem i zdobył zapewnienie, że ta będzie go chronić. Zorganizował Marysi spotkanie z Olgą. Jest weterynarzem. Chroni go też "Dama w Błękicie". Boi się tego, że tu jest Ernest z Eterni. Uważa, że Ernest jest tu go porwać (srs?).
* Progresja:
    * jest przekonany, że Marysia Sowińska (tienka) traktuje go tylko jak zasób. Ale obiecała, że będzie go chronić. Więc jest dobrze.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 1
* **daty:** 0111-05-13 - 0111-05-15
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * AMZ. Ma relację uczeń-mistrz z Olgą; jest uciekinierem eternijskim (fakt nieznany) którego Olga wyprowadziła z dziczy, nauczyła jak funkcjonować i dała mu rekomendację do AMZ. Próbuje odzyskać somnibela ukradzionego Oldze i za to Rekiny go tępią.
* Progresja:
    * Rekiny na niego polują by zemścić się za to, że Rekin nie jest całkowicie bezkarny na tym terenie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-11-10
    1. Primus    : 5, @: 0111-11-10
        1. Sektor Astoriański    : 5, @: 0111-11-10
            1. Astoria    : 5, @: 0111-11-10
                1. Sojusz Letejski    : 5, @: 0111-11-10
                    1. Szczeliniec    : 5, @: 0111-11-10
                        1. Powiat Pustogorski    : 5, @: 0111-11-10
                            1. Czarnopalec    : 2, @: 0111-08-05
                                1. Pusta Wieś    : 2, @: 0111-08-05
                            1. Majkłapiec    : 1, @: 0111-08-24
                                1. Farma Krecik    : 1, @: 0111-08-24
                                1. Kociarnia Zawtrak    : 1, @: 0111-08-24
                                1. Wegefarma Myriad    : 1, @: 0111-08-24
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-24
                            1. Podwiert    : 3, @: 0111-11-10
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-08-05
                                    1. Obrzeża Biedy    : 1, @: 0111-05-15
                                        1. Hotel Milord    : 1, @: 0111-05-15
                                    1. Serce Luksusu    : 1, @: 0111-08-05
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-08-05
                                1. Las Trzęsawny    : 2, @: 0111-11-10
                                    1. Schron TRZ-17    : 1, @: 0111-05-15
                                1. Magazyny Sprzętu Ciężkiego    : 1, @: 0111-11-10
                            1. Zaczęstwo    : 3, @: 0111-10-30
                                1. Akademia Magii, kampus    : 1, @: 0111-08-05
                                    1. Akademik    : 1, @: 0111-08-05
                                1. Akademia Magii    : 1, @: 0111-10-30
                                1. Nieużytki Staszka    : 2, @: 0111-10-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 3 | ((210615-skradziony-kot-olgi; 230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Arkadia Verlen       | 2 | ((210615-skradziony-kot-olgi; 211127-waśń-o-ryby-w-majklapcu)) |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Ksenia Kirallen      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Marysia Sowińska     | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Olga Myszeczka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Teresa Mieralit      | 2 | ((211026-koszt-ratowania-torszeckiego; 230303-the-goose-from-hell)) |
| Wiktor Satarail      | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Karolina Terienak    | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Marek Samszar        | 1 | ((210615-skradziony-kot-olgi)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |