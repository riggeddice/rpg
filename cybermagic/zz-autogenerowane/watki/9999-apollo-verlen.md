# Apollo Verlen
## Identyfikator

Id: 9999-apollo-verlen

## Sekcja Opowieści

### Miłość w rodzie Verlen

* **uid:** 210210-milosc-w-rodzie-verlen, _numer względny_: 9
* **daty:** 0111-08-21 - 0111-08-24
* **obecni:** Apollo Verlen, Arianna Verlen, Brunhilda Verlen, Elena Verlen, Franz Verlen, Krucjusz Verlen, Romeo Verlen, Seraf Verlen, Viorika Verlen

Streszczenie:

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

Aktor w Opowieści:

* Dokonanie:
    * chce wyjść za Milenę Blakenbauer (ze wzajemnością).


### Studenci u Verlenów

* **uid:** 210311-studenci-u-verlenow, _numer względny_: 8
* **daty:** 0097-01-16 - 0097-01-22
* **obecni:** Apollo Verlen, Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Maja Samszar, Michał Perikas, Rafał Perikas, Rufus Samszar, Sylwia Perikas, Viorika Verlen

Streszczenie:

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

Aktor w Opowieści:

* Dokonanie:
    * wykazał się kompetencją opanowując zniszczony Poniewierz po erupcji energii Eleny. Większość czasu jednak spędzał z Sylwią Perikas ;-). Na końcu wybrał rodzinę nad Sylwię - ona zdradziła jego zaufanie. Plus, mały research odnośnie lustra i Blakenbauera.


### Karolinka, nieokiełznana świnka

* **uid:** 230419-karolinka-nieokielznana-swinka, _numer względny_: 7
* **daty:** 0095-07-19 - 0095-07-23
* **obecni:** Aleksander Samszar, Apollo Verlen, Arianna Verlen, Elena Verlen, Fantazjusz Verlen, Marcinozaur Verlen, Tymek Samszar, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Zaprojektowano świnkę Karolinkę do pomszczenia Vioriki. Aria, Elena i Viorika ruszyły do Powiatu radząc sobie z figlarnym glukszwajnem. Po drodze Elena rozbiła vana w menhir, wintegrowała niewinnego Samszara w menhir, porzuciły tam świnkę i dotarły do Siewczyna. Tam Elena próbowała być słodka i współpracować z Samszarem. O dziwo, udało się przekonać Samszara by pozwolił Verlenkom zmierzyć się ze Strażnikiem. W wyniku Paradoksów, 'potwór został porwany' i uciekł do Verlenlandu a Elenie po raz pierwszy uruchomiła się moc Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * w tle współpracował z Vioriką i Arianną, by ukryć sytuację z Eleną. Wziął na siebie sprawę z podłożeniem świni i ze wtopieniem Tymka Samszara w menhir. Jako 'mszczenie się za Viorikę'.


### Dźwiedzie polują na ser

* **uid:** 230412-dzwiedzie-poluja-na-ser, _numer względny_: 6
* **daty:** 0095-07-12 - 0095-07-14
* **obecni:** Apollo Verlen, Arianna Verlen, Elena Verlen, Marcinozaur Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

Aktor w Opowieści:

* Dokonanie:
    * jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 5
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * przekonany przez Romeo, poszedł na zrobienie dowcipu Samszarom. Okup za Maję? "1001 gęsich piór o odpowiednim odcieniu". Oczywiście, "bo Karolinus".


### KJS - Stymulanty Szeptomandry

* **uid:** 230307-kjs-stymulanty-szeptomandry, _numer względny_: 4
* **daty:** 0095-06-26 - 0095-06-29
* **obecni:** Apollo Verlen, Franciszek Korel, KDN Kajis, Lidia Nemert, Lucas Oktromin, Marta Krissit, Michał Waczarek, Żaneta Krawędź

Streszczenie:

Młody chłopak, Michał, wykrył że KJS nie jest stąd; chce odlecieć z nimi. Lucas pyta go o narkotyki, ale Michał nic nie wie - więc jest wysłany do Marty by dowiedział się więcej. To dało otwarcie Żanecie która dowiedziała się od Marty o stymulantach.

Marta opowiada Michałowi o skutkach ubocznych stymulantów, jak np. senność i bóle głowy. Marta zdradza też, że substancje te nie są narkotykami ani nie są nielegalne, ale mają wpływ na umysł i ciało. Przedstawia Żanetę Franciszkowi, z którym ma romans (bo on zapewnia stymulanty). Żaneta udaje tępą fetyszystkę, a Franciszek planuje wykorzystać ją jako ofiarę. Dzięki temu Franciszek prowadzi ją do Szeptomandry (źródła stymulantów) a Lidia i Lucas proszą Apollo o interwencję dając mu całą sprawę na talerzu.

Ostatecznie, Żaneta i Franciszek spotykają się nocą, a Apollo i jego żołnierze pokonują potwora w jaskini.

Aktor w Opowieści:

* Dokonanie:
    * tien Verlen, który okazał się być całkiem normalny i dość rozumiał co jest grane. Nie chciał zrobić nikomu krzywdy i ogólnie dobrze przyjął Zespół. Zniszczył Szeptomandrę i ustabilizował teren. Zapłaci Kajis za zrobienie dowcipu komuś innemu. Nie pytał o "turystów", bo niekoniecznie chciał wiedzieć, acz swoje podejrzewał.


### KJS - Wygrać za wszelką cenę!

* **uid:** 230124-kjs-wygrac-za-wszelka-cene, _numer względny_: 3
* **daty:** 0095-06-23 - 0095-06-25
* **obecni:** Apollo Verlen, Elena Verlen, Emilia Lawendowiec, Karol Atenuatia, KDN Kajis, Lidia Nemert, Lucas Oktromin, Marta Krissit, Serena Krissit, Wojciech Namczak, Zuzanna Kraczamin, Żaneta Krawędź

Streszczenie:

Koszarów Chłopięcy jest miejscem, gdzie młodzi z Verlenlandu ćwiczą i rozwijają cnoty sportowe. Pojawiły się tam nietypowe narkotyki / środki, które nie są pochodzenia stricte chemicznego. Zespół Kajisa ma minimalny poziom energii i musi awaryjnie tam lądować by jak najszybciej uzyskać baterie irianium lub esencji. Na miejscu Lucas wdał się w pyskówkę z nastoletnią Eleną Verlen - nie pomoże jej rozwiązać problemu, bo nie współpracuje z gówniarami.

Aktor w Opowieści:

* Dokonanie:
    * młody kobieciarz (28). Jak tylko pojawił się na terenie Koszarowa, zaczął korzystać ze swojego statusu i umiejętności i dobrze planować czas. Acz przy okazji FAKTYCZNIE podnosi umiejętności Emilii specjalnymi treningami.


### Wiktoriata

* **uid:** 210306-wiktoriata, _numer względny_: 2
* **daty:** 0093-12-19 - 0093-12-28
* **obecni:** Apollo Verlen, Lucjusz Blakenbauer, Przemysław Czapurt, Viorika Verlen, Wiktor Blakenbauer

Streszczenie:

Po przegranym pojedynku na oddziały z Apollem, Viorika trafia do papierologii w Poniewierzy. Tam dowiaduje się, że Blakenbauerowie chyba porywają ludzi. Współpracując z Lucjuszem Blakenbauerem, odkrywa prawdę - to psychoza wywołana przez narkotyki bojowe sprzedawane Verlenom przez Blakenbauerów. Przypadkowo z Lucjuszem rozbija siatkę szpiegowską Blakenbauerów na terenie Verlenów. Plus, zalicza początek romansu z Lucjuszem.

Aktor w Opowieści:

* Dokonanie:
    * używając umiejętności społecznych i nieuczciwej przewagi zmiażdżył Viorikę w pojedynku oddziałów. Ona była lepsza taktycznie, jego żołnierze jego uwielbiali. Plus, wkręcił Viorikę.


### Sentiobsesja

* **uid:** 210224-sentiobsesja, _numer względny_: 1
* **daty:** 0092-10-01 - 0092-10-04
* **obecni:** Apollo Verlen, Arianna Verlen, Lucjusz Blakenbauer, Milena Blakenbauer, Mścigrom Verlen, Przemysław Czapurt, Viorika Verlen

Streszczenie:

Apollo Verlen, sam w Holdzie Bastion był jedynym sentitienem. Gdy uderzyły siły Mileny Blakenbauer, wściekł się i wpadł w sentiobsesję - chce atakować Blakenbauerów. Na szczęście na miejsce przybyły Arianna i Viorika, które rozwiązały sprawę, odkryły sekret Apolla (3 kochanki) i ściągnęły go pułapką, po czym... pokonały i przywróciły. Aha, potem odzyskały brakujący oddział kosztem śpiewu i tańca Apolla przed Mileną Blakenbauer.

Aktor w Opowieści:

* Dokonanie:
    * 24 lata, zawsze blisko żołnierskiej braci. BWR. Trzy kochanki które o sobie nie wiedzą. Poprosił, by inni sentitienowie opuścili ten teren, co Mścigrom zrobił; potem Apollo wpadł w sentiobsesję przez Milenę Blakenbauer. Uratowany przez Ariannę i Viorikę.
* Progresja:
    * dostał opiernicz od sierżanta za sentiobsesję. Dostał wpiernicz od kochanek. Śpiewał i tańczył w stroju maid-chan dla Mileny Blakenbauer. Bardzo, bardzo zły okres.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-08-24
    1. Primus    : 9, @: 0111-08-24
        1. Sektor Astoriański    : 9, @: 0111-08-24
            1. Astoria    : 9, @: 0111-08-24
                1. Sojusz Letejski    : 9, @: 0111-08-24
                    1. Aurum    : 9, @: 0111-08-24
                        1. Powiat Samszar    : 1, @: 0095-07-23
                            1. Siewczyn    : 1, @: 0095-07-23
                                1. Północne obrzeża    : 1, @: 0095-07-23
                                    1. Spichlerz Jedności    : 1, @: 0095-07-23
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-23
                                1. Menhir Centralny    : 1, @: 0095-07-23
                        1. Sentipustkowie Pierwotne    : 1, @: 0092-10-04
                        1. Świat Dżungli    : 1, @: 0092-10-04
                            1. Ostropnącz    : 1, @: 0092-10-04
                        1. Verlenland    : 9, @: 0111-08-24
                            1. Hold Bastion    : 1, @: 0092-10-04
                                1. Barbakan    : 1, @: 0092-10-04
                                1. Karcer    : 1, @: 0092-10-04
                                1. Karczma Szczur    : 1, @: 0092-10-04
                            1. Hold Karaan, obrzeża    : 1, @: 0111-08-24
                                1. Lądowisko    : 1, @: 0111-08-24
                            1. Hold Karaan    : 1, @: 0111-08-24
                                1. Barbakan Centralny    : 1, @: 0111-08-24
                                1. Krypta Plugastwa    : 1, @: 0111-08-24
                            1. Koszarów Chłopięcy, okolice    : 2, @: 0095-07-14
                                1. Las Wszystkich Niedźwiedzi    : 2, @: 0095-07-14
                                1. Skały Koszarowe    : 2, @: 0095-07-14
                            1. Koszarów Chłopięcy    : 3, @: 0095-07-14
                                1. Miasteczko    : 2, @: 0095-06-29
                                    1. Bar Krwawy Topór    : 2, @: 0095-06-29
                                1. Wielki Stadion    : 2, @: 0095-06-29
                            1. Poniewierz, obrzeża    : 1, @: 0097-01-22
                                1. Jaskinie Poniewierskie    : 1, @: 0097-01-22
                            1. Poniewierz, północ    : 1, @: 0097-01-22
                                1. Osiedle Nadziei    : 1, @: 0097-01-22
                            1. Poniewierz    : 2, @: 0097-01-22
                                1. Archiwum Poniewierskie    : 1, @: 0093-12-28
                                1. Dom Uciech Wszelakich    : 2, @: 0097-01-22
                                1. Garnizon    : 1, @: 0093-12-28
                                1. Magazyny Anomalii    : 1, @: 0093-12-28
                                1. Zamek Gościnny    : 1, @: 0097-01-22
                            1. Trójkąt Chaosu    : 1, @: 0093-12-28
                            1. VirtuFortis    : 2, @: 0095-07-23
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 2, @: 0095-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Viorika Verlen       | 7 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210306-wiktoriata; 210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Arianna Verlen       | 5 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 5 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 230124-kjs-wygrac-za-wszelka-cene; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| KDN Kajis            | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucjusz Blakenbauer  | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Maja Samszar         | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Marcinozaur Verlen   | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Marta Krissit        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Przemysław Czapurt   | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Romeo Verlen         | 2 | ((210210-milosc-w-rodzie-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Ula Blakenbauer      | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Żaneta Krawędź       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| AJA Szybka Strzała   | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karolinus Samszar    | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |