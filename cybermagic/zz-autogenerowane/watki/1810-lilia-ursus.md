# Lilia Ursus
## Identyfikator

Id: 1810-lilia-ursus

## Sekcja Opowieści

### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 7
* **daty:** 0110-06-24 - 0110-07-01
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * wejście Pięknotki do Aurum; sporo wie o tamtych terenach. Powiedziała Pięknotce jak działają rody Aurum, w jaki sposób kolekcjonuje się artefakty i jaką rolę ma Małmałaz w tym wszystkim.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 6
* **daty:** 0109-11-28 - 0109-12-01
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru. Nie dała rady rozdzielić Julii i Łysych Psów, niestety.

Aktor w Opowieści:

* Dokonanie:
    * porwana przez Walerię, jest w silnej depresji. Nie jest bezpieczna, nie ma celu. Zgodziła się na warunki Pięknotki by nie sprawiać kłopotu. Low point in her life.
* Progresja:
    * została nieszczęśliwą asystenką Ateny Sowińskiej. Niezbyt się szanują z Ateną.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 5
* **daty:** 0109-11-18 - 0109-11-27
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * porwana przez Walerię na prośbę Pięknotki; znajduje się w Colubrinus Psiarni, acz nikt jej nie krzywdzi.


### Swaty w cieniu potwora

* **uid:** 181125-swaty-w-cieniu-potwora, _numer względny_: 4
* **daty:** 0109-10-29 - 0109-11-01
* **obecni:** Lilia Ursus, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin, Waleria Cyklon, Wioletta Kalazar

Streszczenie:

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

Aktor w Opowieści:

* Dokonanie:
    * wyszukała informacje o nojrepach, wyszukała o Wioletcie, po czym wpadła w szpony Moktara i skończyła jako zabaweczka do uratowania przez Pietra. Chwilowo w Nukleonie.
* Progresja:
    * ośmieszona i pohańbiona przez Moktara; uwierzyła w jego słowa, że jest bezużytecznym śmieciem z pieniędzmi i nic nie może zrobić. Chwilowo w szpitalu na detoksie.
    * znalazła powiązanie pomiędzy nojrepami a Orbiterem Pierwszym i doprowadziła Pięknotkę do Wioletty.
    * jeszcze trzy dni musi spędzić w klinice Nukleon na regenerację. Moktar jej nie oszczędzał...


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 3
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * grzeczna panienka, naprawiła chatę Erwinowi, po czym - na prośbę Pięknotki - sobie poszła.


### Powrót Minerwy z terrorforma

* **uid:** 181021-powrot-minerwy-z-terrorforma, _numer względny_: 2
* **daty:** 0109-09-24 - 0109-09-29
* **obecni:** Adam Szarjan, Atena Sowińska, Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

Aktor w Opowieści:

* Dokonanie:
    * okazało się, że ma przeszłość powiązaną z Adamem. Nie lubi Adama, nie ma zamiaru z nim współpracować nawet jak ona by na tym zyskała.
* Progresja:
    * obwinia Adama Szarjana oraz Atenę Sowińską o stan Erwina Galiliena. Plus, dalej uważa, że Adam jest złym magiem z dobrym PRem.
    * ma w przeszłości wywalenie dobrej terminuski z Koszmaru oraz próbę wejścia do Koszmaru przez łóżko.


### Lilia na Trzęsawisku

* **uid:** 181003-lilia-na-trzesawisku, _numer względny_: 1
* **daty:** 0109-09-20 - 0109-09-22
* **obecni:** Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Erwin ucieka przed Lilią, bo nie chce wziąć jej do Szczeliny. Pięknotka nie była w stanie pogodzić Erwina i Lilii - wzięła ich więc na Trzęsawisko Zjawosztup polować na Anomalie. Niestety, servar Lili został uszkodzony i wszystkie zyski zjadła naprawa. Przy okazji, wyszła sprawa historii Erwina Galiliena oraz Nutki. I Lilii, która lekko zabujała się w Erwinie.

Aktor w Opowieści:

* Dokonanie:
    * chce pomóc Erwinowi przez co on wpada w kłopoty. Na Trzęsawisku niespecjalnie się wykazała, ale nie pogorszyła opinii.
* Progresja:
    * ma w swoich rękach długi Erwina Galiliena i mu nie odpuści - chce do Szczeliny
    * po akcji na Trzęsawisku jest wstrząśnięta i straciła pewność siebie. Zależy jej rozpaczliwie by się wykazać
    * ojciec wysłał oddział który ma ją chronić. Ona o nich nie wie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0110-07-01
    1. Primus    : 6, @: 0110-07-01
        1. Sektor Astoriański    : 6, @: 0110-07-01
            1. Astoria    : 6, @: 0110-07-01
                1. Sojusz Letejski, NW    : 2, @: 0109-12-01
                    1. Ruiniec    : 2, @: 0109-12-01
                        1. Colubrinus Psiarnia    : 1, @: 0109-12-01
                        1. Diamentowa Forteca    : 1, @: 0109-11-01
                        1. Świątynia Bez Dna    : 1, @: 0109-12-01
                1. Sojusz Letejski    : 6, @: 0110-07-01
                    1. Przelotyk    : 2, @: 0109-12-01
                        1. Przelotyk Wschodni    : 2, @: 0109-12-01
                            1. Cieniaszczyt    : 2, @: 0109-12-01
                                1. Arena Nadziei Tęczy    : 1, @: 0109-11-01
                                1. Bazar Wschodu Astorii    : 1, @: 0109-11-01
                                1. Knajpka Szkarłatny Szept    : 1, @: 0109-12-01
                                1. Kompleks Nukleon    : 1, @: 0109-12-01
                                1. Mordownia Czaszka Kralotha    : 1, @: 0109-11-01
                                1. Mrowisko    : 2, @: 0109-12-01
                    1. Szczeliniec    : 4, @: 0110-07-01
                        1. Powiat Pustogorski    : 3, @: 0109-10-04
                            1. Pustogor    : 3, @: 0109-10-04
                                1. Eksterior    : 1, @: 0109-10-04
                                    1. Miasteczko    : 1, @: 0109-10-04
                                1. Gabinet Pięknotki    : 2, @: 0109-09-29
                                1. Interior    : 1, @: 0109-10-04
                                    1. Inkubator Ekonomiczny Samszarów    : 1, @: 0109-10-04
                                1. Kompleks Testowy    : 1, @: 0109-09-29
                                1. Miasteczko    : 1, @: 0109-09-22
                                1. Pustułka    : 1, @: 0109-09-29
                            1. Trzęsawisko Zjawosztup    : 1, @: 0109-10-04
                        1. Pustogor    : 1, @: 0110-07-01
                        1. Trzęsawisko Zjawosztup    : 1, @: 0109-09-22
                            1. Głodna Ziemia    : 1, @: 0109-09-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 6 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy; 181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Erwin Galilien       | 3 | ((181003-lilia-na-trzesawisku; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Adam Szarjan         | 2 | ((181021-powrot-minerwy-z-terrorforma; 181024-decyzja-minerwy)) |
| Atena Sowińska       | 2 | ((181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Minerwa Metalia      | 2 | ((181024-decyzja-minerwy; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Moktar Gradon        | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Adela Kirys          | 1 | ((181024-decyzja-minerwy)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Romuald Czurukin     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |