# Karolinus Samszar
## Identyfikator

Id: 9999-karolinus-samszar

## Sekcja Opowieści

### Nauczmy młodego tiena jak żyć

* **uid:** 230808-nauczmy-mlodego-tiena-jak-zyc, _numer względny_: 16
* **daty:** 0095-09-14 - 0095-09-23
* **obecni:** Amanda Kajrat, Armin Samszar, Celina Maskiewnik, Elena Samszar, Florian Samszar, Karolinus Samszar, Kleopatra Trusiek, Rufus Bilgemener, Tadeusz Maskiewnik, Wiktor Blakenbauer

Streszczenie:

Florian chciał dać szansę Elenie i Karolinusowi i zrobił operację w której E+K mają wychować młodego Armina z pomocą rodu. Jednak E+K zadziałali w sposób pokazujący że są chętni do lekceważącego krzywdzenia ludzi i jakkolwiek osiągnęli cel z Arminem, oni sami "zawiedli". Florian dostał wpierdol. Amanda i Elena nawiązały pozytywne kontakty z Wiktorem Blakenbauerem.

Aktor w Opowieści:

* Dokonanie:
    * mercenary (co mam z tego mieć) a nie pomoc rodowi; pojawił się później by pomóc Elenie, chciał "położyć Kleopatrę do snu", ale Elena mu nie dała. Bez problemu magią biologiczną wpierw zranił kaprala a potem zmienił niewinnego człowieka w berserkera by pozbyć się strażniczek Armina. Bezwzględny, nie dba o swoich ludzi (podwładnych Samszarów).
* Progresja:
    * Samszarowie stwierdzili, że nie dba o ród. Wykonuje polecenia, ale nie ma tam 'serca'. Zdaniem Samszarów nie zależy mu na nikim poza nim samym.


### Amanda konsoliduje Złoty Cień

* **uid:** 230715-amanda-konsoliduje-zloty-cien, _numer względny_: 15
* **daty:** 0095-09-09 - 0095-09-12
* **obecni:** Amanda Kajrat, Bella Samszar, Emil Samszar, Kasimir Esilin, Rufus Bilgemener, Tadeusz Samszar, Wacław Samszar

Streszczenie:

Amanda dostaje wsparcie od Kajrata w formie Kasimira i żołnierzy; wykorzystuje to do obrony Bilgemenera i poszerzanie jego sił. Pozornie uległa Wacławowi Samszarowi, dając mu poczucie bezpieczeństwa i wciągając go w bycie słupem Amandy w Złotym Cieniu. Przekonali razem Tadeusza Samszara, by Lojalistów dodać do Złotego Cienia - a Secesjoniści i element Lojalistów to tymczasowo problem Belli. Klinika Oteriiel pozostaje nierozwiązana i 'neutralna', acz pracuje nad nią Bella.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * nagle stał się odpowiedzialny za powodzenie Złotego Cienia wraz z Wacławem Samszarem. Mają szansę do pierwszej poważnej wtopy Złotego Cienia.


### Zablokowana sentisieć w krainie makaronu

* **uid:** 230711-zablokowana-sentisiec-w-krainie-makaronu, _numer względny_: 14
* **daty:** 0095-09-05 - 0095-09-08
* **obecni:** Elena Samszar, Ignatus Blakenbauer, Irek Kraczownik, Karolinus Samszar, Petra Samszar, Wacław Samszar, Wiktor Blakenbauer

Streszczenie:

Rośnie napięcie między Hiperpsychotronikami i Eleną, bo Elena znalazła połączenie ich badań z Eternią. Karolinus nadał Wacława Amandzie... łącznie z Itrią. Tymczasem w Triticatusie (mieście makaronu) tienka mająca rozwiązać problem, Petra, weszła w głupi zakład z Wiktorem Blakenbauerem. Gdy Zespół tam trafił, Elena pokłóciła się ostro z Petrą, zestrzeliła jej drony i Paradoksem zablokowała sentisieć. Petra, przekonana, że Elena jest psychopatką przeprosiła Wiktora. A tymczasem Ożywiony Makaron stanowił kłopoty. W końcu Karolinus odblokował sentisieć, Wiktor zniszczył płaszczkę i wszystko JAKOŚ wróciło do normy, acz Petra została wrogiem Zespołu.

Aktor w Opowieści:

* Dokonanie:
    * wsadził Wacława (za regularne dziewczyny) Amandzie w Złotym Cieniu jako front, powiedział Irkowi że dołączy do Grupy Proludzkiej Aurum, ogólnie mediator i dyplomata. Nieskutecznie zastraszał Petrę, wraz z Eleną odblokował sentisieć - PERFEKCYJNIE ją odblokował i Petra aż była w szoku. Rozwiązał kryzys ^_^.
* Progresja:
    * Wacław jest LOJALNY Karolinusowi, bo Karolinus wielokrotnie udowodnił, że dba o niego. I zapewnia mu dziewczyny przez Złoty Cień.
    * Petra Samszar jest przekonana, że on tylko udaje pozytywnego. Chce wykazać jego wielkie zło.


### Maja chciała być dorosła

* **uid:** 230704-maja-chciala-byc-dorosla, _numer względny_: 13
* **daty:** 0095-08-29 - 0095-09-01
* **obecni:** Albert Samszar, Amanda Kajrat, Jonatan Lemurczak, Karolinus Samszar, Maja Samszar, Nadia Obiris, Vanessa Lemurczak, Wirgot Samszar

Streszczenie:

Karolinus namówiony przez Kajrata chce zrzucić swoje winy na współpracujący z handlarzami ludźmi Złoty Cień. By ich zniszczyć, manipuluje młodą Maję i wystawia ją jako ofiarę dla Lemurczaków zaproszonych przez Wirgota. Maja zostaje strasznie skrzywdzona, ale Złoty Cień jest złamany, Karolinus ma sojusz z Albertem, Lemurczakowie zostali odepchnięci a Wirgot ma w Albercie śmiertelnego wroga.

Aktor w Opowieści:

* Dokonanie:
    * wykorzystał Maję jako przynętę, manipulując ją by ona sama chciała być 'jak dorosła Vanessa' i zmieniając ją magią. Ale gdy Maja była u Lemurczaków odpowiednio długo, przekonał Alberta że ona potrzebuje pomocy i skupił wszystkich Samszarów do pomocy Mai. Udało mu się rozognić wojnę Albert - hiperpsychotronicy i odepchnąć Lemurczaków, ale jakim kosztem...
* Progresja:
    * sojusz z Albertem Samszarem, oparty o to, że oboje chcą jak najlepiej dla Mai.


### Ratuj młodzież dla Kajrata

* **uid:** 230627-ratuj-mlodziez-dla-kajrata, _numer względny_: 12
* **daty:** 0095-08-20 - 0095-08-25
* **obecni:** AJA Szybka Strzała, Amanda Kajrat, Elena Samszar, Ernest Kajrat, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Mitria Ira, Talia Aegis, Wacław Samszar, Wargun Ira

Streszczenie:

Herbert powiedział o lokacjach baz i że to prace na duchach. Strzała wie o tym że ma uszkodzoną psychotronikę - skontaktowała Zespół z Kajratem (S. potrzebuje pomocy Talii). Kajrat da dostęp do Talii, jeśli Zespół pomoże uratować noktiańskich młodych magów z niewoli Samszarów. Po znalezieniu bazy Karolinus wywabił na Itrię (którą dołączył do drużyny XD) Wacława (strażnika i ekstraktora) a Elena z Amandą Kajrat wydobyły nieszczęsnych noktian. Strzała odjechała do Kajrata. Kajrat zostawił Zespołowi Amandę do pomocy.

Aktor w Opowieści:

* Dokonanie:
    * dostarczył Strzale mapę inkubatora, ale ujawnił się przed Wacławem. Cóż - szuka dziewczyn. Potem skusił Wacława używając Itrii, POZYSKAŁ ową Itrię i przypadkiem dołączył ją do drużyny. Pomógł Strzale w ratowaniu noktian i ewakuacji do Talii Aegis.
* Progresja:
    * przypomniał sobie anty-amnestykami rzeczy które Fabian mu odebrał amnestykami.


### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 11
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * podbija do Itrii i przekonuje ją o planie - "musi się przespać z nim, Herbertem i Maksem by zrobić nirwanę kóz". Sam nie wierzy jak bardzo ta sprawa eskalowała i eksplodowała. Jak zawsze, ma sympatię prostych żołnierzy. Skupił się nie tylko na tajnych bazach ale też by pomóc w okolicy.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 10
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * Maja do niego wysłała SOS bo mu ufa; wciągnął Elenę i po analizie zapisu z kamer doszedł do tego, że Maja z Romeem coś robiła. Wyciągnął Elenę (zsynchronizowaną z dziwnym duchem) z piwnicy i robił za "dobrego wujka" Mai, uspokajając ją cały czas.
* Progresja:
    * zdaniem Alberta Samszara - zwykle niegodny uwagi. Zadaje się z Verlenami i marnuje czas.


### Zaginięcie psychotronika Cede

* **uid:** 230613-zaginiecie-psychotronika-cede, _numer względny_: 9
* **daty:** 0095-08-02 - 0095-08-05
* **obecni:** AJA Szybka Strzała, Aleksander Samszar, Cede Burian, Celina Burian, Fabian Samszar, Karolinus Samszar, Lara Ukraptin

Streszczenie:

Przyjaciel Karolinusa, Cede, zniknął. Siostra - Celina - będąca na terenie Sowińskich poprosiła go o pomoc (bo nikt inny nie pomaga). Karolinus i Strzała odkryli, że Cede robił coś w Siewczynie i Gwiazdoczach i ktoś udaje, że Cede miał problemy z mafią. Dalsze poszukiwania wykazały, że w sprawę zamieszana jest jednostka do walki psychotronicznej (???) oraz wysoko postawieni Samszarowie. Karolinus współpracując z Aleksandrem dotarł do swojego przeszłego nauczyciela, Fabiana, i zażył amnestyki. Ale Aleksander zostawił w Strzale środki naprawcze - które zadziałają po pewnym czasie.

Aktor w Opowieści:

* Dokonanie:
    * gdy jego przyjaciel, Cede, zniknął to się zainteresował - przegrzebał prawdę od fałszu i gdy zobaczył że sprawa jest dla niego za ostra, poprosił Aleksandra o pomoc. Skończył rozmawiając z Fabianem i wziął amnestyki.
* Progresja:
    * skończył z amnestykami, zażył od Fabiana. Zapomniał o wszystkim odkąd Celina się z nim skontaktowała.


### Karolinka - raciczki zemsty Verlenów

* **uid:** 230516-karolinka-raciczki-zemsty-verlenow, _numer względny_: 8
* **daty:** 0095-07-29 - 0095-07-31
* **obecni:** Aleksander Samszar, Amara Zegarzec, Elena Samszar, Franciszek Chartowiec, Karolinus Samszar, Ludmiła Zegarzec

Streszczenie:

Karolinka, świnka podłożona przez Verlenów na Wielkie Kwiatowisko zaczęła polować na lokalne duchy a przedsiębiorcza Ludmiła z pobliskiego hotelu zorganizowała dziennikarza Paktu i okazję do zarobienia. Karolinus i Elena przebili się przez problematycznego dziennikarza Paktu, zwabili świnkę do pojazdu i uśpili oraz podrzucili ją (rękami żołnierzy) do Verlenlandu. A Strzała jest naprawiana.

Aktor w Opowieści:

* Dokonanie:
    * kłóci się z Eleną kto zajmie się świnką, udaje asystenta Eleny (przed Paktem), uśpionego glukszwajna wysyła do Verlenlandu rękami dwóch żołnierzy.
* Progresja:
    * na wideo Paktu gdy zwalczali glukszwajna jako "asystent Eleny Samszar". Popularność wśród Paktu rośnie.
    * zdaniem Verlenów, jest z nimi kwita jak chodzi o podłe rzeczy które się sobie robi.


### Samszarowie, Lemurczak i fortel Strzały

* **uid:** 230509-samszarowie-lemurczak-i-fortel-strzaly, _numer względny_: 7
* **daty:** 0095-07-24 - 0095-07-26
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Jonatan Lemurczak, Karolinus Samszar, Roland Samszar

Streszczenie:

Strzała w ruinie, ale dała radę dotrzeć do w miarę bezpiecznego miejsca pod grzmotoptakami. Gdy dwójka nastolatków na które poluje ich zmieniona przez Lemurczaka matka się pojawili blisko, Elena ją unieruchomiła a Karolinus przekształcił w normalną formę. Acz Paradoksem wysłał sygnaturę Verlenopodobną. Gdy Karolinus i Elena się kłócą czy pomóc czy czekać, Strzała pojedynczą droną wymanewrowała Stegozaur-class support hovertank i przestraszyła Lemurczaka hintując, że Verlenowie polujący na ptaki są w pobliżu. Elena zmanipulowała Irka, więc E+K wyszli na osoby pozytywne które chcą dobrze, acz nie zawsze mają idealne plany (bo są młodzi). A Roland zajmie się Sanktuarium Kazitan.

Aktor w Opowieści:

* Dokonanie:
    * próbuje zrozumieć Irka i jego motywacje; naprawił magią przekształconą przez Lemurczaka matkę nastolatków. W ten sposób pokazuje Irkowi, że może nie jest całkiem zły. Irek mu zaufał. Karolinusowi dziękują w okolicach Lancatim; tam jest bohaterem (mimo, że to Strzała zrobiła robotę).


### Egzorcysta z Sanktuarium

* **uid:** 230411-egzorcysta-z-sanktuarium, _numer względny_: 6
* **daty:** 0095-07-21 - 0095-07-23
* **obecni:** AJA Szybka Strzała, Arnold Kazitan, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Tadeusz Dzwańczak

Streszczenie:

Egzorcysta Irek poszukiwany przez Samszarów jest w Sanktuarium Kazitan w Przelotyku Zachodnim Dzikim. Na miejscu Sanktuarium ulega Emisji - katastroficzne elementalne działania. Zespół ratuje dzieciaka od Emisji (najpierw mu zagrażając XD), ale gdy dociera do egzorcysty - ostatniego maga który próbuje pomóc Sanktuarium, to go porywają. Przy próbie porwania Strzała zostaje ciężko uszkodzona i nie daje rady wrócić do Powiatu Samszar - crashlanduje w bezpiecznej części Przelotyka.

Aktor w Opowieści:

* Dokonanie:
    * wpierw magią unieszkodliwił Tadeusza strażaka myśląc, że to porywacz a potem powerupował się by ratować dziecko i został lokalnym bohaterem na moment - tylko po to, by wyeksploatować tą inwestycję w reputację i porwać Irka. Skupiony na celu i wystarczająco bezwzględny wobec miasta.
* Progresja:
    * przez moment był bohaterem Sanktuarium Kazitan, ale teraz jest tam traktowany jak najgorszy z najgorszych po porwaniu Irka.


### Wszystkie duchy Siewczyna

* **uid:** 230404-wszystkie-duchy-siewczyna, _numer względny_: 5
* **daty:** 0095-07-18 - 0095-07-20
* **obecni:** AJA Szybka Strzała, Elena Samszar, Irek Kraczownik, Karolinus Samszar, Maksymilian Sforzeczok

Streszczenie:

Karolinus, wraz z kuzynką Eleną Samszar zostali wysłani do Siewczyna gdzie podobno są problemy (by uniknąć dalszego antagonizowania Verlenów). Na miejscu okazało się, że niekompetentny egzorcysta Irek sprowadził tu Hybrydę noktiańskiej TAI i ducha oraz ten byt próbował zemścić się za śmierć swoich ludzi. Elena Paradoksem zniszczyła Ducha Opiekuńczego Spichlerza a Karolinus Paradoksem stworzył strażniczego ducha w kształcie Vioriki w bikini. A w tle - spory między ludźmi i duchami (podsycane przez Hybrydę) oraz między podejściem 'ekonomia vs harmonia'.

Aktor w Opowieści:

* Dokonanie:
    * integrował się z normalnymi ludźmi przez picie, ale jak doszło co do czego to magią osłonił Strzałę by ta mogła walczyć z Hybrydą. Zrobił kolejną trwałą manifestację Vioriki...
* Progresja:
    * stworzył lokalnego Strażnika, ducha w formie Vioriki w bikini z karabinem. W Siewczynie i w Verlenlandzie mu tego nie zapomną...


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 4
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * odkrył prawdę stojącą za zniknięciem Mai oraz przekonanie jej do wzięcia udziału w turnieju Supreme Missionforce, jednocześnie dbając o jej bezpieczeństwo i ustalając plan na jej powrót do domu po zakończeniu imprezy.


### Brudna konkurencja w Arachnoziem

* **uid:** 230314-brudna-konkurencja-w-arachnoziem, _numer względny_: 3
* **daty:** 0095-06-20 - 0095-06-22
* **obecni:** AJA Szybka Strzała, Ania Turabnik, Fiona Szarstasz, Fircjusz Szarstasz, Julita Mopsarin, Kacper Aczramin, Karolinus Samszar, Laura Turabnik

Streszczenie:

Karolinus Samszar przybywa na prośbę swej guwernantki Fiony by pomóc w jej rodzinnej firmie ArachnoBuild, która ryzykuje utratę swojej pozycji na rynku i która jest w Verlenlandzie. Pojawił się Potwór przez błędy w działaniach ArachnoBuild. Karolinus dowiaduje się od barmanki Laury o konkurencji - firmie EnMilStrukt, finansowanej przez Brunhildę Verlen. EnMilStrukt ma inne podejście do prowadzenia biznesu (nie harmonia z naturą a dominacja natury) i zyskuje poparcie części mieszkańców.

Fiona i Karolinus dochodzą do wniosku, że jedynymi osobami, które mogły wyciec dane, są Kacper i Ania. Dane wskazują na odchylenia w lokalizacji jaszczurów od czasu pojawienia się EnMilStrukt. Fiona rozmawia z bratem Fircjuszem, który ujawnia swoje uczucia do Ani, a Kacper okazuje się być starym przyjacielem rodziny. Ania ma kompleks, że nie jest wystarczająco dobra, a Kacper ma długi.

Pod wpływem magii puryfikacji Karolinusa, Ania przyznaje, że spotkała kogoś, kto twierdził, że jest kuzynem Fircjusza, i przekazywała mu dane (magia mentalna). Miejsce spotkań to kopalnia, a numer telefonu jest przypisany do ludzi zatrudnionych przez EnMilStrukt.Strzała próbuje zlokalizować telefon, ale nie udaje się zdobyć wystarczających dowodów, aby udowodnić, że EnMilStrukt miało wiedzę o spisku. Za to Karolinusowi udało się dotrzeć do wrogiej czarodziejki i Paradoksem zmienił populację większości miasta w Fionę XD. Viorika Verlen próbowała złapać Karolinusa, ale Strzała może lecieć w stratosferę XD.

Aktor w Opowieści:

* Dokonanie:
    * (młody (20) panicz który chce pomóc rodzinie Fiony, bo miłość jest ślepa. Biomagia + magia Samszarów. Tien.) Poszukuje informacji w knajpie "Łeb jaszczura", rozmawia z barmanką Laurą i spuryfikował Anię, dzięki czemu poznał info o tajemniczej czarodziejce w służbie EnMilStrukt. Potem wszedł z nią w starcie i jakkolwiek ją miał, to rzucił zaklęcie i Paradoksem doprowadził do tego, że większość miasta wygląda jak Fiona i jest w nim zakochana. Skutecznie zwiał Viorice używając Strzały.
* Progresja:
    * niesamowicie zdrażnił Viorikę Verlen i spalony w miasteczku Arachnoziem. Specjalnie dla niego, Viorika zasponsorowała upokarzający pomnik.


### Kłótnie sąsiadów w Wańczarku

* **uid:** 230425-klotnie-sasiadow-w-wanczarku, _numer względny_: 2
* **daty:** 0095-05-16 - 0095-05-19
* **obecni:** AJA Szybka Strzała, Artemis Lawellan, Artur Lawellan, Damian Fenekis, Elea Brzozecka, Ilfons Lawellan, Karolinus Samszar, Olga Fenekis

Streszczenie:

Karolinus został wysłany do Wańczarka, by rozsądzać spory między sąsiadami odnośnie sadów, ale wpakował się w starą intrygę ciemniejszej strony Samszarów i Blakenbauerów odnośnie robienia Koncentratu Oświecenia z soku mandragory. Gdy pojawiła się próba utopienia Ilfonsa przez Eleę, Strzała wyciągnęła topielca a Karolinus go uratował. Karolinus zmienił jednego Lawellana w świnię, a potem ze Strzałą przyszpilili Artemis która powiedziała im prawdę o tym terenie - jak powstaje Koncentrat Oświecenia. Aha, miesiąc temu zniknął tu dziennikarz z Verlenlandu.

Aktor w Opowieści:

* Dokonanie:
    * zaczął od sprawdzania zbyt miłego sołtysa, przeszedł przez nocną imprezę z dzieciakami i ich brutalnie zastraszył (zmieniając jednego w świnię co go przewrócił w błoto za flirt z jego dziewczyną), uratował magią życie topielcowi go przekształcając w roślino-człowieka a na końcu przekonuje do siebie Artemis. Może pomóc i chce uratować ludzi z mrocznych eksperymentów Blakenbauerów.


### Karolinus, sędzia Mirkali

* **uid:** 230620-karolinus-sedzia-mirkali, _numer względny_: 1
* **daty:** 0095-04-15 - 0095-04-18
* **obecni:** Agnieszka Klirpin, AJA Szybka Strzała, Filip Klirpin, Juanita Derwisz, Karolinus Samszar

Streszczenie:

Mirkala - miasteczko, gdzie stare (zioła) zwalcza się z nowym (Juanita pragnie budować ekologiczne prefab-domy na wzgórzach). Karolinus przekonał Starszyznę, że muszą dać młodym odskocznię (i znaleźć pieniądze) by Mirkala nie była całkowitym kołchozem, a potem z pomocą Strzały odepchnął Juanitę z tego terenu; niech nie mści się na Starszyźnie za śmierć rodziny jej przyjaciela.

Aktor w Opowieści:

* Dokonanie:
    * ŚWIETNIE zapowiadający się 'rozsądca Samszarów'. W Mirkali znalazł świetne sposoby na to jak odwrócić pewną śmierć miasta i przyciągać młodych i emerytów oraz poszerzyć wartość Mirkali o coś więcj niż tylko zioła. Z pomocą Strzały odepchnął Juanitę, która chciała pomścić przyjaciela którego rodzinę zniszczyła Starszyzna Mirkali.
* Progresja:
    * bardzo mile widziany w Mirkali, uważany za bohatera - pozbył się Juanity i pomógł młodym. Duży bonus zarówno w subfrakcji Samszarów jak i w samym miasteczku.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 15, @: 0095-09-23
    1. Primus    : 15, @: 0095-09-23
        1. Sektor Astoriański    : 15, @: 0095-09-23
            1. Astoria    : 15, @: 0095-09-23
                1. Sojusz Letejski    : 15, @: 0095-09-23
                    1. Aurum    : 13, @: 0095-09-23
                        1. Powiat Blakenbauer    : 1, @: 0095-09-23
                            1. Gęstwina Duchów    : 1, @: 0095-09-23
                        1. Powiat Samszar    : 11, @: 0095-09-23
                            1. Fort Tawalizer    : 1, @: 0095-08-18
                                1. Obserwatorium Potworów i Fort (E)    : 1, @: 0095-08-18
                                    1. Areszt    : 1, @: 0095-08-18
                                    1. Koszary garnizonu    : 1, @: 0095-08-18
                                1. Wzgórza Potworów (N)    : 1, @: 0095-08-18
                                    1. Farmy kóz    : 1, @: 0095-08-18
                            1. Gwiazdoczy, okolice    : 1, @: 0095-08-25
                                1. Technopark Jutra    : 1, @: 0095-08-25
                                    1. Inkubatory małego biznesu    : 1, @: 0095-08-25
                            1. Gwiazdoczy    : 2, @: 0095-08-25
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 2, @: 0095-09-01
                                1. Centralny Park Harmonii    : 1, @: 0095-09-01
                                    1. Gwiezdna Rozkosz    : 1, @: 0095-09-01
                                    1. Pałac Harmonii    : 1, @: 0095-09-01
                            1. Mirkala    : 1, @: 0095-04-18
                                1. Wielka Zielarnia (E)    : 1, @: 0095-04-18
                                1. Wzgórza Bezduszne (W)    : 1, @: 0095-04-18
                                    1. Fabryka Domów    : 1, @: 0095-04-18
                                    1. Technopark    : 1, @: 0095-04-18
                            1. Siewczyn    : 2, @: 0095-08-05
                                1. Astralne Ogrody (ES)    : 1, @: 0095-07-20
                                    1. Centralne Biura Rolnicze    : 1, @: 0095-07-20
                                    1. Centrum R&D dla zrównoważonego rolnictwa    : 1, @: 0095-07-20
                                    1. Dom Szamana    : 1, @: 0095-07-20
                                    1. Fabryka neutralizatorów astralnych    : 1, @: 0095-07-20
                                    1. Gaj Duchów    : 1, @: 0095-07-20
                                    1. Rezydencje Harmonii    : 1, @: 0095-07-20
                                1. Centrum Jedności Mieszka (Center)    : 1, @: 0095-07-20
                                    1. Bazar rękodzieła    : 1, @: 0095-07-20
                                    1. Drzewo Jedności    : 1, @: 0095-07-20
                                    1. Most jedności    : 1, @: 0095-07-20
                                    1. Park miejski    : 1, @: 0095-07-20
                                    1. Ratusz miejski    : 1, @: 0095-07-20
                                1. Północne obrzeża    : 1, @: 0095-07-20
                                    1. Spichlerz Jedności    : 1, @: 0095-07-20
                                1. Wzgórza Industrialnej Harmonii (NW)    : 1, @: 0095-07-20
                                    1. Fabryka siewników i sadzarek    : 1, @: 0095-07-20
                                    1. Fabryka traktorów    : 1, @: 0095-07-20
                                    1. Przestrzeń mieszkalna    : 1, @: 0095-07-20
                                    1. Sklepy i centra handlowe    : 1, @: 0095-07-20
                            1. Triticatus, północ    : 2, @: 0095-09-23
                                1. Dopływ Strumienia Pszenicznego    : 1, @: 0095-09-08
                                1. Stare magazyny    : 1, @: 0095-09-23
                            1. Triticatus    : 2, @: 0095-09-23
                                1. Makaroniarnia (NW)    : 2, @: 0095-09-23
                                    1. Formatornia    : 1, @: 0095-09-08
                                    1. Pola Pszenicy    : 2, @: 0095-09-23
                                    1. Semolinatorium    : 1, @: 0095-09-08
                                    1. Strumień Pszeniczny    : 1, @: 0095-09-08
                                    1. Systemy Irygacyjne    : 1, @: 0095-09-08
                                    1. Wielka Suszarnia    : 1, @: 0095-09-08
                                    1. Wielki Młyn    : 1, @: 0095-09-08
                                1. Pszenicznik (SE)    : 1, @: 0095-09-08
                                    1. Dworzec Maglev    : 1, @: 0095-09-08
                                    1. Magazyny    : 1, @: 0095-09-08
                                    1. Obszar administracyjny    : 1, @: 0095-09-08
                                    1. Rezydencje mieszkalne    : 1, @: 0095-09-08
                            1. Wańczarek    : 1, @: 0095-05-19
                                1. Wańczarek    : 1, @: 0095-05-19
                                    1. Dzielnica Sadowa (NW, W)    : 1, @: 0095-05-19
                                        1. Dom sołtysa    : 1, @: 0095-05-19
                                        1. Sady owocowe (NW)    : 1, @: 0095-05-19
                                    1. Dzielnica Smutku (E)    : 1, @: 0095-05-19
                                        1. Szpital Jabłoni    : 1, @: 0095-05-19
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-31
                                1. Hotel Odpoczynek Pszczół    : 1, @: 0095-07-31
                                1. Menhir Centralny    : 1, @: 0095-07-31
                        1. Verlenland    : 3, @: 0095-08-25
                            1. Arachnoziem    : 1, @: 0095-06-22
                                1. Bar Łeb Jaszczura    : 1, @: 0095-06-22
                                1. Kopalnia    : 1, @: 0095-06-22
                            1. Duchowiec Śmieszny, okolice    : 1, @: 0095-08-25
                            1. Duchowiec Śmieszny    : 1, @: 0095-08-25
                            1. VirtuFortis    : 1, @: 0095-07-02
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 1, @: 0095-07-02
                    1. Przelotyk    : 2, @: 0095-07-26
                        1. Przelotyk Zachodni Dziki    : 2, @: 0095-07-26
                            1. Lancatim, okolice    : 1, @: 0095-07-26
                            1. Lancatim    : 1, @: 0095-07-26
                            1. Sanktuarium Kazitan    : 1, @: 0095-07-23
                                1. Dystrykt Szafir    : 1, @: 0095-07-23
                                    1. Komnata lecznicza    : 1, @: 0095-07-23
                                    1. Komnata mieszkalna    : 1, @: 0095-07-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 11 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai; 230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230425-klotnie-sasiadow-w-wanczarku; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230613-zaginiecie-psychotronika-cede; 230620-karolinus-sedzia-mirkali; 230627-ratuj-mlodziez-dla-kajrata)) |
| Elena Samszar        | 9 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230516-karolinka-raciczki-zemsty-verlenow; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Irek Kraczownik      | 4 | ((230404-wszystkie-duchy-siewczyna; 230411-egzorcysta-z-sanktuarium; 230509-samszarowie-lemurczak-i-fortel-strzaly; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Amanda Kajrat        | 3 | ((230627-ratuj-mlodziez-dla-kajrata; 230704-maja-chciala-byc-dorosla; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Maja Samszar         | 3 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| Albert Samszar       | 2 | ((230523-romeo-dyskretny-instalator-supreme-missionforce; 230704-maja-chciala-byc-dorosla)) |
| Aleksander Samszar   | 2 | ((230516-karolinka-raciczki-zemsty-verlenow; 230613-zaginiecie-psychotronika-cede)) |
| Fiona Szarstasz      | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Herbert Samszar      | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Impresja Ignicja Incydencja Diakon | 2 | ((230606-piekna-diakonka-i-rytual-nirwany-koz; 230627-ratuj-mlodziez-dla-kajrata)) |
| Jonatan Lemurczak    | 2 | ((230509-samszarowie-lemurczak-i-fortel-strzaly; 230704-maja-chciala-byc-dorosla)) |
| Romeo Verlen         | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Wacław Samszar       | 2 | ((230627-ratuj-mlodziez-dla-kajrata; 230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Wiktor Blakenbauer   | 2 | ((230711-zablokowana-sentisiec-w-krainie-makaronu; 230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Apollo Verlen        | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Armin Samszar        | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Arnold Kazitan       | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Maskiewnik    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Damian Fenekis       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ernest Kajrat        | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Florian Samszar      | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Ignatus Blakenbauer  | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Juanita Derwisz      | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kleopatra Trusiek    | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Maks Samszar         | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Maksymilian Sforzeczok | 1 | ((230404-wszystkie-duchy-siewczyna)) |
| Mitria Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Nadia Obiris         | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Petra Samszar        | 1 | ((230711-zablokowana-sentisiec-w-krainie-makaronu)) |
| Roland Samszar       | 1 | ((230509-samszarowie-lemurczak-i-fortel-strzaly)) |
| Rufus Bilgemener     | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Tadeusz Dzwańczak    | 1 | ((230411-egzorcysta-z-sanktuarium)) |
| Tadeusz Maskiewnik   | 1 | ((230808-nauczmy-mlodego-tiena-jak-zyc)) |
| Talia Aegis          | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Vanessa Lemurczak    | 1 | ((230704-maja-chciala-byc-dorosla)) |
| Viorika Verlen       | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Wargun Ira           | 1 | ((230627-ratuj-mlodziez-dla-kajrata)) |
| Wirgot Samszar       | 1 | ((230704-maja-chciala-byc-dorosla)) |