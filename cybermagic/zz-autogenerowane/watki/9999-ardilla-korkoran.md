# Ardilla Korkoran
## Identyfikator

Id: 9999-ardilla-korkoran

## Sekcja Opowieści

### Orbiter, Nihilus i ruch oporu w Nativis

* **uid:** 230816-orbiter-nihilus-i-ruch-oporu-w-nativis, _numer względny_: 16
* **daty:** 0093-03-30 - 0093-03-31
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Kalia Awiter, Marcel Draglin, Marzena Marius

Streszczenie:

Przed Nativis problem - nie są w stanie zniszczyć ani odeprzeć Syndykatu. Więc pozostaje im wezwać Orbiter, ale jak? Albo przez Izabellę Skażoną Nihilusem albo przez Cognitio Nexus (gdzie jest komunikacja). A tymczasem Marzena Marius, kiedyś z Orbitera przejęła dowodzenie nad ruchem oporu anty-Kidironowym i niestety rozgrywa rewelacyjnie politykę. Draglin chroni za wszelką cenę reputację Ardilli i samą Ardillę, też przeciw Eustachemu XD.

Aktor w Opowieści:

* Dokonanie:
    * jej pierwszy plan - udawać kult Nihilusa - sama złamała (zbyt niebezpieczne). Próbowała wybadać ruchy Aurory Nativis i rozmawiała z Marzeną Marius, ale okazało się, że eks-komodor Orbitera jest po prostu politycznie LEPSZA od Ardilli. Ardilla odroczyła działania ruchu oporu.


### Korkoran płaci cenę za Nativis

* **uid:** 230726-korkoran-placi-cene-za-nativis, _numer względny_: 15
* **daty:** 0093-03-29 - 0093-03-30
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Marcel Draglin, OO Infernia, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Draglin zapędził Czarnymi Hełmami Tymona Korkorana do Muzeum Kidirona. Używając sił swoich i Inferni (pod rozkazami Eustachego) uratował piratów pod mentalnym wpływem. Negocjacje Lobrak - Eustachy pokazały Eustachemu, że nie są w stanie pokonać Syndykatu. Ardilla nie pozwala mu dołączyć Arkologii do Syndykatu - to nie to co powinno być. W chwili, w której Eustachy skonsolidował pełnię sił i mocy Tymon eksploduje wysadzając siebie i Wujka. Eustachy zostaje bez swojego sumienia, sam z Ardillą, Draglinem, Kalią i uszkodzoną Arkologią. Naprzeciw Syndykatu i Lobrakowi, który chce sojuszu.

Aktor w Opowieści:

* Dokonanie:
    * przede wszystkim skupia się na Ralfie, monitoruje go i patrzy na Izabellę. Przez to przeoczyła sytuację z Wujkiem. Ale chroni Arkologię przed Eustachym, który próbuje wejść w sojusz z Syndykatem Aureliona. Pokazuje Eustachemu, że to nie jest dobra opcja. Jest ostatnim elementem sumienia Eustachego. Gdyby nie Ardilla to Lobrak by wygrał, bo Eustachy by oddał mu Arkologię.


### Wojna o Arkologię Nativis - nowa regentka

* **uid:** 230719-wojna-o-arkologie-nativis-nowa-regentka, _numer względny_: 14
* **daty:** 0093-03-28 - 0093-03-29
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Kalia Awiter, Marcel Draglin, OO Infernia, Ralf Tapszecz, Tobiasz Lobrak

Streszczenie:

Ardilla ma plan przejęcia kontroli nad Arkologią, w tle Kidiron przez nią kontrolowany. To też neutralizuje główne ataki L&T, że Kidiron taki zły. Z pomocą Kalii (ewakuowanej ze skrzydła medycznego) zbudowała linię propagandową i po odzyskaniu Radiowęzła, Kalia nadała wiadomość pokoju i pojednania. Eustachy zmiażdżył główne siły L&T koło Engineering, Kalia JAKIMŚ CUDEM została regentką przez przypadek a czarodziejka Syndykatu, Izabella została zmiażdżona w imię Nihilusa przez Ralfa chroniącego Ardillę.

Aktor w Opowieści:

* Dokonanie:
    * doszła, że Lobrak jest aktywnym agentem Syndykatu i za wszystkim stoi mag; chce przejąć władzę nad Arkologią i współpracuje z Kalią. Ewakuowała Kalię i wpadła pod zaklęcie czarodziejki Syndykatu; zanim ta przeformatowała Ardillę, Ralf uratował Ardillę niszcząc umysł czarodziejki Syndykatu w imię Nihilusa.
* Progresja:
    * ma potencjał przejęcia kontroli nad Arkologią Nativis w swoim imieniu, a w tle Kidiron.


### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 13
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * używając Szarego Ostrza ma listę farighanów; przekonała Lertysów do pomocy przeciw nim. Próbuje chronić Szczury Arkologii przed brutalnością Eustachego i jego piratów, ale nadaremno. Eustachy zrobił co chciał. Poświęca personalną reputację dla jutrzejszej żywej i sprawnej arkologii. Uważa, że Rafał Kidiron jest jedynym co faktycznie utrzyma arkologię Nativis...
* Progresja:
    * oficjalnie i powszechnie rozpoznawana jako sympatyczka i sojuszniczka Rafała Kidirona


### Infiltrator ucieka a Arkologia płonie

* **uid:** 230621-infiltrator-ucieka-a-arkologia-plonie, _numer względny_: 12
* **daty:** 0093-03-25 - 0093-03-26
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, BIA Prometeus, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Laurencjusz Kidiron, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

Aktor w Opowieści:

* Dokonanie:
    * utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami.
* Progresja:
    * dostęp do kodów i haseł Kidirona dających jej praktycznie władzę dyktatorki i autokraty


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 11
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * od Stanisława usłyszała o dziwnej arkologii Nox Aegis; potem broniła Ralfa przed wujkiem (tak chce się dalej spotykać). Gdy był zamach na Kidirona, skupiła się na samym Kidironie i go z Ralfem uratowała, ku swym mieszanym uczuciom. Ale dostała dostęp do jego mrocznych planów.
* Progresja:
    * ma dostęp do wszystkich planów operacyjnych Kidirona. Sekrety farighanów, trianai, bazy Nox Aegis... wszystko.
    * w sprawie Ralfa musi Wujkowi wszystko donosić - została konfidentem XD.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 10
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * zajmowała się komunikacją Infernia - arkologia by znaleźć Skorpiona Szczepana, poprosiła Amelię o shackowanie pamiętnika Eweliny, potem wciągnęła Kalię do sprawy Eweliny, znalazła dowód na obecność maga Interis który uratował Ewelinę i zdecydowała się pomóc Ewelinie. Musimy uratować jej reputację i odwrócić sprawę.


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 9
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * koordynowała akcję ratunkową, wyłączyła Infernię by napastnicy nie mogli jej zdobyć, złośliwie uszkodziła klimatyzację Czarnym Hełmom, schowała się w kanałach Inferni gdy mostek napadli i uszkodziła dla Eustachego generatory Memoriam Inferni. Gwarant bezpieczeństwa piratów podczas ich przesłuchania przez Kidirona.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 8
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * weszła do Ambasadorki razem z Kalią; została pojmana przez Misterię, ale przekonała ją do wymiany rannych na wujka. Próbowała negocjować ewakuację Misterii i innych osób. Przekonała Misterię by wymienić rannych na wujka i stanowiła serce zespołu; bez niej Misteria zostałaby zestrzelona lub zabita. Współpracując z Kalią doprowadziła do tego, że nikomu nic się nie stało. Współpracując z Misterią i Kalią doprowadziła do ruchu oporu przeciw Kidironowi.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 7
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * oswaja Ralfa i z nim lekko flirtuje (chcesz sobie znaleźć dziewczynę?). Pokazuje Ralfowi, że Nihilus nie jest wszechmocny, że jest po co żyć i działać. Ratuje Marcinka i wyjaśnia Ralfowi, że wszyscy ludzie mają potencjał. Ma do niego cierpliwość. Potem - śledzi Eustachego na randce z Kalią (i kibicuje Kalii). Zdobywa dowody, że Eustachy ma autokontrolę nad Infernią. Końsko zalotuje Eustachego i Kalię ;-). Gdy Eustachy zostawił Kalię w dziwnych okolicznościach, idzie do niej.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 6
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * wzięła pod opiekę Ralfa i pokazała mu teren arkologii, dając mu namiastkę czegoś dla czego warto żyć; potem zastraszyła kilka lowlifeów z arkologii którzy chcieli sklupać noktianina. Okazuje się, że ma ostry język. Dostała dowodzenie operacją pomocy noktianom i twardo wybrała Wujka nad Kidirona. Ma dobre serce i zbiera informacje odnośnie używaniu Trianai jako broni biologicznej Kidironów.
* Progresja:
    * w Nativis uznana za przerażającą. Zamiast przestraszyć nastolatków, woli ich skrzywdzić. "Element podły" się jej boi, Mroczne Hełmy niechętnie z nią walczą.
    * uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną.


### Osy w CES Purdont

* **uid:** 220817-osy-w-ces-purdont, _numer względny_: 5
* **daty:** 0093-01-23 - 0093-01-24
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Joachim Puriur, Kamil Wraczok, Kordian Olgator, VN Karglondel

Streszczenie:

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

Aktor w Opowieści:

* Dokonanie:
    * wpierw z pomocą Janka zyskała kluczowe do antidotum rzeczy dla Celiny, potem porozkładała szybko próbki po bazie by Trianai vs komandosi i zrobić dywersję. I zdążyła uratować Darię i dostarczyć jej antidotum w ostatnim momencie.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 4
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * CONCEPT: badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze. Eustachy składa maszyny, ja kradnę znaleziska ze zrujnowanych struktur. Rywalizujemy na polu Mój jeszcze-nie-wiem-co-to-inator jest lepszy, niż twój! PAST: wiewiórcza rozrabiaka, próbowała ukraść keykartę i chciała wkraść się z Darią do laboratorium ekopoezy; Eustachy podkablował więc złapana. ACTUAL: przekonała wujka by mimo presji czasowej pomógł Jankowi spotkać się z Darią, skoczyła na rurę i w ten sposób dała okazję Eustachemu i Jankowi na zestrzelenie ainshkera i pełniła rolę głównego zwiadowcy.


### Ona chce dziecko Eustachego

* **uid:** 221006-ona-chce-dziecko-eustachego, _numer względny_: 3
* **daty:** 0092-09-20 - 0092-09-24
* **obecni:** Ardilla Korkoran, Ava Kieras, Emban Dolamor, Eustachy Korkoran, Lerten Kieras, Maks Selert, Michał Kervendal, OO Infernia, Staszek Zakraton, VN Exerinn

Streszczenie:

Rift - wujek vs Kidironowie w sprawie Robaków i Dziewczynki. Eustachy rozdarty, acz staje za wujkiem. Z woli Kidironów Eustachy wziął Infernię i poleciał do crawlera o nazwie Exerinn by odzyskać Robaki które uciekły. Tam Eustachy poznał jak żyją Sarderyci z Exerinna i jak radzą sobie z trudnościami (m.in. Farighanami - nekroborgami Neikatis). Po tym jak Infernia pomogła Exerinnowi i Eustachy oddał geny Avie, Robaki wróciły z Exerinna na Infernię i Eustachy ukrył ich życie przed Kidironami - fałszywka, że stali się Farighanami.

Aktor w Opowieści:

* Dokonanie:
    * zapewniła sobie współpracę Robaków którzy uciekli do Exerinna z Nativis, po czym nie dała sczeznąć Lertenowi zmienionemu w Farighana - wpadła do quada i go wyciągnęła unieruchamiając zanim on coś złego / głupiego zrobi pod wpływem implantu.


### Dziewczynka Trianai

* **uid:** 220914-dziewczynka-trianai, _numer względny_: 2
* **daty:** 0092-09-10 - 0092-09-11
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Karina Nezerin, Stanisław Uczantor

Streszczenie:

Po zniszczeniu Robaków okazało się, że w tunelach Starej Arkologii zniknęło dwóch nastolatków. Ardilla i Eustachy poszli znaleźć owych nastolatków - faktycznie, coś tam jest. Eustachy zastawił pułapkę i prawie zabił pietnastolatkę zmienioną w Trianai. Udało im się wydobyć ją i dostarczyć Kidironom, choć wykazali się dużą bezwzględnością. Większość zdominowanych przez dziewczynkę ludzi udało się uratować. Ale kto jej to zrobił i czemu?

Aktor w Opowieści:

* Dokonanie:
    * w imieniu Stanisława zaniosła jedzenie dla "Ducha" Arkologii, potem znalazła gdzie ów "Duch" się znajduje (piętnastolatka trianai) i oddała ją Kidironom.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 1
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * uratowała przemytnika i przekazała Eustachemu paczkę z analizatorem żywności; gdy chciała podłożyć pluskwę Stanisławowi to on ją opieprzył i będzie resocjalizował. Ale wyciągnęła go z Robaków i sprowadziła na dobrą drogę.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 16, @: 0093-03-31
    1. Primus    : 16, @: 0093-03-31
        1. Sektor Astoriański    : 16, @: 0093-03-31
            1. Neikatis    : 16, @: 0093-03-31
                1. Dystrykt Glairen    : 16, @: 0093-03-31
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis    : 14, @: 0093-03-31
                        1. Poziom 1 - Dolny    : 7, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Magazyny    : 1, @: 0093-03-28
                                1. Processing    : 1, @: 0093-03-28
                                1. Stacje Skorpionów    : 1, @: 0093-03-28
                            1. Północ - Stara Arkologia    : 7, @: 0093-03-28
                                1. Blokhaus E    : 2, @: 0093-03-28
                                1. Blokhaus F    : 5, @: 0093-03-28
                                    1. Szczurowisko    : 3, @: 0093-03-28
                                1. Stare Wejście Północne    : 3, @: 0093-03-28
                                1. Szczurowisko    : 1, @: 0093-02-21
                            1. Wschód    : 1, @: 0093-03-28
                                1. Farmy Wschodnie    : 1, @: 0093-03-28
                            1. Zachód    : 2, @: 0093-03-28
                                1. Centrala Prometeusa    : 2, @: 0093-03-28
                                1. Stare TechBunkry    : 1, @: 0093-03-28
                        1. Poziom 2 - Niższy Środkowy    : 3, @: 0093-03-30
                            1. Południe    : 2, @: 0093-03-29
                                1. Engineering    : 2, @: 0093-03-29
                                1. Power Core    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Bazar i sklepy    : 1, @: 0093-03-28
                            1. Wschód    : 2, @: 0093-03-30
                                1. Centrum Kultury i Rozrywki    : 2, @: 0093-03-30
                                    1. Bar Śrubka z Masła    : 1, @: 0093-03-28
                                    1. Muzeum Kidirona    : 2, @: 0093-03-30
                                    1. Ogrody Gurdacza    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Koszary Hełmów    : 1, @: 0093-03-28
                                1. Sektor Porządkowy    : 1, @: 0093-03-28
                        1. Poziom 3 - Górny Środkowy    : 6, @: 0093-03-29
                            1. Południe    : 2, @: 0093-03-29
                                1. Life Support    : 1, @: 0093-03-28
                                1. Medical    : 2, @: 0093-03-29
                            1. Wschód    : 5, @: 0093-03-28
                                1. Dzielnica Luksusu    : 5, @: 0093-03-28
                                    1. Ambasadorka Ukojenia    : 3, @: 0093-03-28
                                    1. Ogrody Wiecznej Zieleni    : 4, @: 0093-03-28
                                    1. Stacja holosymulacji    : 2, @: 0093-03-28
                        1. Poziom 4 - Górny    : 2, @: 0093-03-29
                            1. Wschód    : 2, @: 0093-03-29
                                1. Centrala dowodzenia    : 1, @: 0093-03-28
                                1. Obserwatorium Astronomiczne    : 1, @: 0093-03-28
                                1. Radiowęzeł    : 2, @: 0093-03-29
                            1. Zachód    : 1, @: 0093-03-28
                                1. Tereny Sportowe    : 1, @: 0093-03-28
                        1. Poziom 5 - Szczyt    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Port kosmiczny    : 1, @: 0093-03-28
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 2, @: 0093-01-24
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22
                    1. Ogród Zwłok Exerinna    : 1, @: 0092-09-24

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 16 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Bartłomiej Korkoran  | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 10 | ((220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 9 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 8 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Rafał Kidiron        | 8 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Celina Lertys        | 6 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tymon Korkoran       | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Jan Lertys           | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Marcel Draglin       | 4 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis; 230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Tobiasz Lobrak       | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Stanisław Uczantor   | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Małgorzata Maratelus | 2 | ((230329-zdrada-rozrywajaca-arkologie; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Marzena Marius       | 1 | ((230816-orbiter-nihilus-i-ruch-oporu-w-nativis)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |