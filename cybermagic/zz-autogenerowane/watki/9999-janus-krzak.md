# Janus Krzak
## Identyfikator

Id: 9999-janus-krzak

## Sekcja Opowieści

### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 5
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * duże wsparcie Klaudii w radzeniu sobie ze zrozumieniem tego co się stało z Bramą Kariańską i zdobywanie informacji o ON Catarinie.


### W cieniu Nocnej Krypty

* **uid:** 210728-w-cieniu-nocnej-krypty, _numer względny_: 4
* **daty:** 0111-05-20 - 0111-06-06
* **obecni:** AK Nocna Krypta, Arianna Verlen, Atrius Kurunen, Eustachy Korkoran, Finis Vitae, Gerard Adanor, Helena Adanor, Janus Krzak, Oliwia Karelan, Romana Arnatin, Romana Arnatin, Ulisses Kalidon

Streszczenie:

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

Aktor w Opowieści:

* Dokonanie:
    * z Eustachym zmontował sondę która wysłała sygnał udający, że w rzeczywistości Finis Vitae opanował bazę Sarairen. Jego wiedza o Eterze uprawdopodobniła ów sygnał.


### Pierwsza BIA mag

* **uid:** 210721-pierwsza-bia-mag, _numer względny_: 3
* **daty:** 0111-05-17 - 0111-05-19
* **obecni:** Arianna Verlen, BIA Solitaria d'Zona Tres, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Romana Arnatin

Streszczenie:

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * jedyny astoriański naukowiec ever jaki tłumaczył Bii zasady eteru i jak należy używać magii. Srs.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 2
* **daty:** 0111-05-14 - 0111-05-16
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * de facto ostrzegł, że powrót Bramą jest piekielnie niebezpieczny. Arianna czuje smutek.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 1
* **daty:** 0111-05-11 - 0111-05-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * ekspert od Eteru sponsorowany przez Sowińskich, chciał lecieć na konferencję (poza Sektorem Astoriańskim). Pomógł Zespołowi rozpracować co się mogło stać z Klaudią.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-02-12
    1. Nierzeczywistość    : 1, @: 0111-06-06
    1. Primus    : 5, @: 0112-02-12
        1. Sektor Astoriański    : 2, @: 0112-02-12
            1. Brama Kariańska    : 2, @: 0112-02-12
        1. Zagubieni w Kosmosie    : 4, @: 0111-06-06
            1. Crepuscula    : 4, @: 0111-06-06
                1. Pasmo Zmroku    : 4, @: 0111-06-06
                    1. Baza Noktiańska Zona Tres    : 4, @: 0111-06-06
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-05-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Eustachy Korkoran    | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Martyn Hiwasser      | 4 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Elena Verlen         | 3 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej)) |
| Klaudia Stryk        | 3 | ((210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Flawia Blakenbauer   | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gilbert Bloch        | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Leona Astrienko      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Raoul Lavanis        | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Tal Marczak          | 1 | ((210707-po-drugiej-stronie-bramy)) |