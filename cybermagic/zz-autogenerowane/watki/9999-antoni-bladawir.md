# Antoni Bladawir
## Identyfikator

Id: 9999-antoni-bladawir

## Sekcja Opowieści

### Anomalna Mavidiz?

* **uid:** 240131-anomalna-mavidiz, _numer względny_: 9
* **daty:** 0111-01-03 - 0111-01-08
* **obecni:** Antoni Bladawir, Arianna Verlen, Borys Kragin, Eustachy Korkoran, Grigor Tarnow, Igor Stratos, Karl Murnoff, Klaudia Stryk, Markus Wąż, ONS Mavidiz, OO Tivr, Raoul Lavanis, Rita Stratos, Tara Ogniczek

Streszczenie:

Bladawir skierował Ariannę na SCA Mavidiz używając OO Tivr. Tam - o czym nikt nie wie - jest Nihilosekt. Madiviz jest anomalnym statkiem - co gorsza coś jest nie tak z załogą wpadającą w obsesje i pętle; pojawiają się halucynacje i z ludźmi coś jest nie tak. Gdy Zespół zostaje zaatakowany, z trudem sabotują jednostkę i robią ostatni przyczółek przy silnikach. Eustachemu udaje się oddzielić 'zarażony' fragment jednostki i Mavidiz - co prawda ciężko zniszczona - przetrwała.

Aktor w Opowieści:

* Dokonanie:
    * wysłał Ariannę Tivrem (którego miała zdobyć) na Mavidiz, by przejąć Tarę (od Ernesta Bankierza) jako dowód, że Ernest jest zdrajcą.
* Progresja:
    * dzięki operacji Arianny na Mavidiz uzyskał eks-Aurelionowca (Karl Murnoff) i eks-agentkę Ernesta Bankierza (Tarę Ogniczek).


### Wszystkie sekrety Caralei

* **uid:** 240125-wszystkie-sekrety-caralei, _numer względny_: 8
* **daty:** 0110-12-29 - 0111-01-02
* **obecni:** Antoni Bladawir, Dobromir Misiak, Kazimierz Darbik, Olaf Rawen, Tog Worniacz

Streszczenie:

Załoga Actazir zatrzymała przemyt verminusów na Saltomak w brutalny sposób, który jednak nie był problematyczny dla Orbitera - za co dostali opierdol od kapitana Actazir. Następnie przeszli do badań Caralei, która jest bardziej problematyczna niż normalna jednostka (problemy z TAI, problemy ze skanowaniem). Podczas rozwikływania kłopotliwej sytuacji Caralei Zespół doszedł do tego, że kapitan Caralei jest noktianinem unikającym Orbitera i mającym dobre serce; przygarnął sporo osób które miały problem. Niestety, są osoby na pokładzie Caralei skłonne go zdradzić. I - żona kapitana zintegrowała się z BIA jednostki. Bladawir dostając takie informacje pomógł Caralei - robiąc z niej swoją "sprzymierzoną jednostkę", z własnymi oczami i uszami.

Aktor w Opowieści:

* Dokonanie:
    * gdy dostał problem Caralei - nie do końca prawidłowej jednostki z żywą BIA - włączył ją do swoich tajnych wspieranych jednostek i wprowadził tam Toga jako swojego człowieka.


### Mikiptur, zemsta Woltaren

* **uid:** 240124-mikiptur-zemsta-woltaren, _numer względny_: 7
* **daty:** 0110-12-29 - 0111-01-01
* **obecni:** Antoni Bladawir, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lars Kidironus, OA Mikiptur, OO Isratazir, Raoul Lavanis, Władawiec Diakon

Streszczenie:

Syndykat zdecydował się zaatakować Bladawira i wysłał Dewastatora przy użyciu Mikiptur jako pułapki. Skutecznie zniszczył Isratazir. Arianna śpi, więc Eustachy (nie spiesząc się) przyleciał ratować Isratazir. Infernia rozmontowała miny i wyciągnęła kogo się dało, mimo, że Mikiptur uszkodził Sarkalin. Klaudia i Arianna zrobiły co mogły, by ograniczyć polityczny fallout. Arianna ujawniła obecność Syndykatu, ratując co się da z reputacji Bladawira i jednocząc Orbiter.

Aktor w Opowieści:

* Dokonanie:
    * był w złym miejscu, wierząc w swoje plany i działania i został wymanewrowany przez Aureliona. Gdyby nie Arianna i Infernia, przestałby być komodorem.
* Progresja:
    * potężna strata autorytetu, opinii świetnego oficera itp. Gdyby nie Arianna, straciłby poziom komodora.


### Wieczna Wojna Bladawira

* **uid:** 240110-wieczna-wojna-bladawira, _numer względny_: 6
* **daty:** 0110-12-24 - 0110-12-27
* **obecni:** Antoni Bladawir, Arianna Verlen, Dormand Miraris, Ernest Bankierz, Kazimierz Darbik, Klaudia Stryk, Marian Witaczek, Samuel Fanszakt, Talia Miraris, Władawiec Diakon, Zaara Mieralit

Streszczenie:

Arianna planowała ściągnięcie Władawca i przy jego pomocy uwiedzenie Zaary by zniszczyć Bladawira. Z pomocą Klaudii załatwiła transfer Władawca na Infernię (i pewną roszadę załogi). Gdy okazało się, że kraloth został zniszczony przez kapitana Ernesta Bankierza, Bladawir jest jeszcze bardziej zagubiony. Przekierował swoją flotę by tam wejść i dowiedzieć się wszystkiego o konspiracji ("bo to niemożliwe"). Gdy Arianna połączyła się z tkanką kralotyczną z Klaudią, Zaarą i Samuelem udało jej się odkryć że kraloth współpracuje z większą organizacją (która chyba się go pozbyła XD). Co więcej - udało jej się ZROZUMIEĆ przeszłość Zaary i Bladawira. Bladawir stracił wszystko przez Syndykat Aureliona i walczy z nimi dalej, konspiracja w konspiracji. Ariannie udało się użyć kralotycznej mocy by Zdestabilizować Zaarę - niech jest możliwa do naprawienia, rekonstruowała ją integrując ją z tkanką kralotyczną i wyłączyła Zaarze możliwość powiedzenia o tym że cokolwiek jej się stało.

Czyli celem Bladawira jest Syndykat Aureliona. Siła, na którą Bladawir poluje obsesyjnie. Każdy może być członkiem Aureliona. I nagle Arianna nie chce już tak bardzo niszczyć Bladawira i kontrolować Zaary...

Aktor w Opowieści:

* Dokonanie:
    * grasping at straws. Próbuje łagodniej podejść do Inferni; tak zdewastowany, że aż PRAWIE powiedział Ariannie coś czego nie chciał. I tak pokazał kilka śladów które Arianna może znaleźć. Zaczyna robić błędy - chciał użyć magów bez obstawy ludzi, chciał Reanimować kralotha by go przesłuchać. Ale coś znalazł i dookoła kralotha i działań Ernesta Bankierza. Pytanie - co.
* Progresja:
    * całkowicie zagubiony pomiędzy dziesiątkami konspiracji i planów Orbitera. Bierze narkotyki, by się wzmocnić i by być niestrawnym dla magii. He Will Not Serve Arianna After Death.


### Perfekcyjne wykorzystanie unikalnych zasobów

* **uid:** 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow, _numer względny_: 5
* **daty:** 0110-12-03 - 0110-12-17
* **obecni:** Antoni Bladawir, Arianna Verlen, Klaudia Stryk, Lars Kidironus, Zaara Mieralit

Streszczenie:

Bladawir dostarcza dane z jego Teczek gdzie są jakieś anomalie. Klaudia znajduje anomalie świadczące o potworze - to nie to czego Bladawir szukał, ale też się tym zajmie. Po Dark Awakening Arianny wobec jednej z ofiar Bladawir postanowił zniszczyć potwora, bo Orbiter był za słaby i zdradził tych ludzi. Ale jego metody są nieakceptowalne dla Klaudii i powiedziała mu to w twarz. Gdy Arianna TEŻ się mu postawiła, Bladawir odsunął je od operacji i sam zapolował na potwora by go przejąć. Arianna zrobiła donos do Wydziału Wewnętrznego i Bladawir bardzo ucierpiał.

Aktor w Opowieści:

* Dokonanie:
    * czegoś szuka w okolicy Neotik i przekazał część Mrocznych Danych Klaudii i Zaarze do analizy. Po tym jak Klaudia znalazła anomalię w danych, pozyskał zwłoki dla Arianny by zrobić Dark Awakening. Gdy się okazało, że to nie to czego szuka a potwór, skupił się na jego usunięciu bo Orbiter zawiódł ludzi. Zagroził Ariannie zdrowiem jej bliskich jeśli stanie przeciw niemu. Co ważne - nie ukarał ani Arianny ani Klaudii bo uważa je za kompetentne i przydatne dla Orbitera.
* Progresja:
    * ma offlinowe dane w których ma Podłe Czyny wielu ludzi XD. Wielką Teczkę Czynów Złych i Podłych.
    * przez działania Arianny, dostał poważne uszkodzenie od Orbitera. Patrzą mu na ręce i unieszkodliwili jego działania.


### Bladawir kontra przemyt tienów

* **uid:** 231220-bladawir-kontra-przemyt-tienow, _numer względny_: 4
* **daty:** 0110-12-06 - 0110-12-11
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Ewa Razalis, Kazimierz Darbik, Klaudia Stryk, Marcinozaur Verlen, OO Infernia, OO Karsztarin, Raoul Lavanis, Szymon Orzesznik, Zaara Mieralit

Streszczenie:

Komodor Bladawir postanowił usunąć przemyt tienów z Karsztarina (najpewniej Program Kosmiczny Aurum), każąc swoim jednostkom robić wyczerpujące patrole i przeszukiwanie jednostek pod kątem owego przemytu. Tempo jest zabójcze nawet dla Arianny i Inferni - zwłaszcza, że Arianna nie wie po co to wszystko robi. Śledztwo Klaudii pokazuje, że Bladawir najpewniej umieścił potwora na Karsztarinie by zmusić przemytników do rozpaczliwego ruchu. Arianna nie może nikogo ostrzec, ale używa połączenia Krwi z Eleną - i w ten sposób dowiaduje się o roli Orzesznika. Klaudia dyskretnie przekazuje informacje Ewie Razalis. Operacja Bladawira się nie tylko nie udaje, ale trafił na dywanik i jego plan się rozsypał. Bladawir powiedział Ariannie, że ona ALBO będzie z nim współpracować albo on jej nie chce. Nadal nie wie, że to Arianna jest architektem jego porażki.

Aktor w Opowieści:

* Dokonanie:
    * tyran planujący zniszczenie 'zdrajcy Orbitera' przemycającego tienów z Aurum przez Karsztarin gdzieś poza Orbiter. Zaara na jego polecenie opracowała potwora wprowadzonego na Karsztarin (za czasów starcia z Ewą Razalis). Wszystko opracował - wygnanie zdrajcy z Karsztarina i przechwycenia go jego jednostkami które nawet nie wiedzą w jakiej operacji uczestniczą. Ale Arianna i Klaudia pokrzyżowały mu plany, jego działania się rozpadły. PRAWIE powiedział Ariannie o co chodzi, ale nie powie tego przecież 'wrogiej agentce'...
* Progresja:
    * Przegrał operację 'usunięcia zdrajcy z Orbitera', nad którą pracował od dawna. Największa porażka od bardzo dawna. Do tego ogromny OPR z Admiralicji za działania, sabotaż i niedopilnowanie Inferni. Ogólnie, poważny cios.
    * Dowiedział się, że Verlenowie wiedzą o 'przesuwaniu tienów z Aurum na Orbitę' ale w tym nie uczestniczą. I o 'Ariannie, księżniczce Verlenlandu' i części jej powiązań.


### Niemożliwe nieuczciwe ćwiczenia Bladawira

* **uid:** 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira, _numer względny_: 3
* **daty:** 0110-11-26 - 0110-11-30
* **obecni:** Antoni Bladawir, Arianna Verlen, Elena Verlen, Kamil Lyraczek, Klaudia Stryk, Lars Kidironus, Marta Keksik, OO Infernia, OO Paprykowiec, Patryk Samszar, Zaara Mieralit

Streszczenie:

Bladawir przedstawia Inferni Zaarę i informuje o ćwiczeniach, rozgrywając Zaarę przeciw Inferni. Klaudia odkryła że jednostki jakie Infernia ma eskortować mają _Pain Module_. Arianna prosząc przez Krew Elenę doprowadziła do neutralizacji tych modułów. Ćwiczenia okazują się prawie niemożliwe, ale Infernia poświęciła Paprykowiec, 'zniszczyła' jednostki w przewadze i uratowała załogę. Odwróciła ćwiczenia Bladawira przeciw niemu, przy okazji podnosząc swoją chwałę i pokazując że nie będzie grać w cudze gry.

Aktor w Opowieści:

* Dokonanie:
    * zrobił ćwiczenia by postawić Ariannę przed wyborem 'skuteczność' lub 'dobre serce i przyjaciele', wystawiając jako swoją namiestniczkę Zaarę. Jednocześnie testuje umiejętności i lojalność swoich pozostałych oficerów. Po tej operacji w której Arianna skutecznie ominęła wszystkie jego pułapki jest przekonany do jej kompetencji, acz zmartwiony jej pro-ludzkimi słabościami.


### Ćwiczenia komodora Bladawira

* **uid:** 231115-cwiczenia-komodora-bladawira, _numer względny_: 2
* **daty:** 0110-11-08 - 0110-11-15
* **obecni:** Antoni Bladawir, Arianna Verlen, Dorota Radraszew, Ewa Razalis, Izabela Zarantel, Klaudia Stryk, Leszek Kurzmin, OO Karsztarin, OO Paprykowiec

Streszczenie:

Komodorzy Bladawir i Razalis mają historię i przy wspólnych ćwiczeniach zrobili wszystko by to drugie nie wygrało. Bladawir ściągnął Ariannę jako dywersję, ale okazało się że to był plan wewnątrz planu. Gdy Klaudia doszła do tego, że atak na Karsztarin jest niemożliwy, Arianna wkradła się w łaski Bladawira i poznała plan - porwać Ewę Razalis jej lojalnymi ludźmi. Arianna poszła za planem (prosząc Kurzmina by ostrzegł Razalis); Bladawirowi Arianna się spodobała do tego stopnia, że dodał ją do swoich sił. Też dlatego, bo Kurzmin stanął przeciw niemu a Bladawir nie wybacza.

Aktor w Opowieści:

* Dokonanie:
    * były kochanek i przełożony kmdr Ewy Razalis, przez kilka lat podkładał jej ludzi do elitarnej gwardii by ostatecznie ją ZNISZCZYĆ i pokazać jej że każdego da się kupić. Ściągnął do siebie Ariannę jako dywersję, ale jak wykazała się skillsetem to dołączył ją bo kompetentna. Absolutnie okrutny. Jego słabością jest przekonanie o jego intelekcie i absolutny brak sympatii do ludzi.
* Progresja:
    * przez Izę, Bladawir spotyka się z publicznym potępieniem, a Razalis postrzegana jest jako ofiara.


### Komodor Bladawir i Korona Woltaren

* **uid:** 231109-komodor-bladawir-i-korona-woltaren, _numer względny_: 1
* **daty:** 0110-10-27 - 0110-11-03
* **obecni:** Aneta Woltaren, Antoni Bladawir, Arianna Verlen, Elena Verlen, Feliks Walrond, Klaudia Stryk, Krzysztof Woltaren, Leszek Kurzmin, Lidia Woltaren, Marta Keksik, OO Paprykowiec, Patryk Samszar, SC Korona Woltaren, Tomasz Rewernik, Wojciech Mykirło

Streszczenie:

Komodorowi Bladawirowi podpadła "Korona Waltaren" i wysłał Ariannę z zespołem na inspekcję. Podczas inspekcji Arianna i Klaudia odkrywają różne problemy, w tym zagrożenia środowiskowe i sanitarne. Mimo problemów, załoga broni Anety i staje za swoim statkiem; kolejna grzywna może "Koronę" utopić. Arianna opracowuje plan, który pozwoli statkowi uniknąć grzywny przez złożenie prośby o ratunek. Kilka miesięcy później jednak Elena szukając kralotha wykrywa coś dziwnego na tej jednostce, ale nie umie tego nazwać. Okazuje się, że na "Koronie" która była niedaleko Iorusa zalągł się terrorform ixioński i gdyby nie poświęcenie młodej tienki, Marty, Zespół mógłby zginąć. Zespół ewakuuje Koronę (która zostaje porzucona) i mimo wielu rannych udaje się większość uratować. Bladawir jest zadowolony - ałoga 'Korony' trafi w okolice Anomalii, 'gdzie ich miejsce'.

Aktor w Opowieści:

* Dokonanie:
    * komodor. Tępił 'Koronę Woltaren' dla prywaty i traktuje nie-Orbiter jak śmiecie. W końcu 'Korona' miała problem z terrorformem i wysławszy tam Ariannę - jego przypuszczenia się spełniły. Mściwy i niemiły.
* Progresja:
    * pełnia zasług za to, że skutecznie ograniczył problem ixiońskiego terrorforma na "Koronie Woltaren". Zgodnie z papierami, 'miał rację' doczepiając się do tej jednostki.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-01-08
    1. Primus    : 9, @: 0111-01-08
        1. Sektor Astoriański    : 9, @: 0111-01-08
            1. Astoria, Pierścień Zewnętrzny    : 8, @: 0111-01-08
                1. Stacja Medyczna Atropos    : 1, @: 0110-11-15
                1. Stacja przeładunkowa Saltomak    : 1, @: 0110-12-17

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Klaudia Stryk        | 8 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Zaara Mieralit       | 4 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240110-wieczna-wojna-bladawira)) |
| Elena Verlen         | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Eustachy Korkoran    | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Kazimierz Darbik     | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240110-wieczna-wojna-bladawira; 240125-wszystkie-sekrety-caralei)) |
| Lars Kidironus       | 3 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 240103-perfekcyjne-wykorzystanie-unikalnych-zasobow; 240124-mikiptur-zemsta-woltaren)) |
| OO Paprykowiec       | 3 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Raoul Lavanis        | 3 | ((231220-bladawir-kontra-przemyt-tienow; 240124-mikiptur-zemsta-woltaren; 240131-anomalna-mavidiz)) |
| Ewa Razalis          | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Leszek Kurzmin       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231115-cwiczenia-komodora-bladawira)) |
| Marta Keksik         | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| OO Infernia          | 2 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| OO Karsztarin        | 2 | ((231115-cwiczenia-komodora-bladawira; 231220-bladawir-kontra-przemyt-tienow)) |
| Patryk Samszar       | 2 | ((231109-komodor-bladawir-i-korona-woltaren; 231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Władawiec Diakon     | 2 | ((240110-wieczna-wojna-bladawira; 240124-mikiptur-zemsta-woltaren)) |
| Aneta Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Borys Kragin         | 1 | ((240131-anomalna-mavidiz)) |
| Dobromir Misiak      | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Dormand Miraris      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Dorota Radraszew     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Ernest Bankierz      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Feliks Walrond       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Grigor Tarnow        | 1 | ((240131-anomalna-mavidiz)) |
| Igor Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Izabela Zarantel     | 1 | ((231115-cwiczenia-komodora-bladawira)) |
| Kamil Lyraczek       | 1 | ((231129-niemozliwe-nieuczciwe-cwiczenia-bladawira)) |
| Karl Murnoff         | 1 | ((240131-anomalna-mavidiz)) |
| Krzysztof Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Lidia Woltaren       | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Marcinozaur Verlen   | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Marian Witaczek      | 1 | ((240110-wieczna-wojna-bladawira)) |
| Markus Wąż           | 1 | ((240131-anomalna-mavidiz)) |
| OA Mikiptur          | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| Olaf Rawen           | 1 | ((240125-wszystkie-sekrety-caralei)) |
| ONS Mavidiz          | 1 | ((240131-anomalna-mavidiz)) |
| OO Isratazir         | 1 | ((240124-mikiptur-zemsta-woltaren)) |
| OO Tivr              | 1 | ((240131-anomalna-mavidiz)) |
| Rita Stratos         | 1 | ((240131-anomalna-mavidiz)) |
| Samuel Fanszakt      | 1 | ((240110-wieczna-wojna-bladawira)) |
| SC Korona Woltaren   | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Szymon Orzesznik     | 1 | ((231220-bladawir-kontra-przemyt-tienow)) |
| Talia Miraris        | 1 | ((240110-wieczna-wojna-bladawira)) |
| Tara Ogniczek        | 1 | ((240131-anomalna-mavidiz)) |
| Tog Worniacz         | 1 | ((240125-wszystkie-sekrety-caralei)) |
| Tomasz Rewernik      | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |
| Wojciech Mykirło     | 1 | ((231109-komodor-bladawir-i-korona-woltaren)) |