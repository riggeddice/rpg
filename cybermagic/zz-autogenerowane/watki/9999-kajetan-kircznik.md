# Kajetan Kircznik
## Identyfikator

Id: 9999-kajetan-kircznik

## Sekcja Opowieści

### Anomalne awarie Athamarein?

* **uid:** 231018-anomalne-awarie-athamarein, _numer względny_: 7
* **daty:** 0111-12-22 - 0111-12-27
* **obecni:** Alicja Szadawir, Andrzej Gwozdnik, Arianna Verlen, Błażej Sowiński, Elena Verlen, Eustachy Korkoran, Hiacynt Samszar, Jakub Oroginiec, Kajetan Kircznik, Klaudia Stryk, Leona Astrienko, OO Athamarein, Remigiusz Alkarenit, Uśmiechniczka Konstrukcjonistka Aurora Diakon

Streszczenie:

Athamarein - bardzo stara korweta rakietowa, jeszcze sprzed czasu Astorii - miała spotkanie z rajderami Syndykatu. Sama obróciła się, by Syndykat nie zabił jej załogi (Persefona nic nie wie). Jednak załoga Athamarein jest skłócona, straumatyzowana i niekompetentna - nie poradziłaby sobie na akcji, więc Athamarein zaczęła się sama 'sabotować'. W rozpaczy, ściągnęli załogę Inferni zanim Athamarein pójdzie na żyletki - niech Infernianie odkryją i wyjaśnią, co się stało z Athamarein. W wyniku, Athamarein została oddelegowana jako jednostka szkoleniowa. Czas na emeryturę.

Aktor w Opowieści:

* Dokonanie:
    * niezmiennie szalony afro-czarny-paw, doradził Orogińcowi kontakt z Arianną; nic się nie zmienił. Nadal chodzi w "przebraniach" (fake mustache), nadal dba o ludzi przy sobie, nadal robi głupie dowcipy i lubi Ariannę :-).


### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 6
* **daty:** 0100-11-13 - 0100-11-16
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * nieco zbyt skutecznie przestraszył młodych infiltratorów; noktianie się boją Orbitera ale teraz też duchów.


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 5
* **daty:** 0100-11-07 - 0100-11-10
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * jako medyk szybko pomógł Elenie i Władawcowi po tym jak się pocięli podczas tańca; piorun obiecał Elenie że jej pomoże i nie da Ariannie z nią rozmawiać gdy jej pomaga a potem obiecał Ariannie, że jak najszybciej Elenę uwolni spod opieki lekarskiej.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 4
* **daty:** 0100-10-05 - 0100-10-08
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * upewnił się, że Kirea nie jest niebezpieczna, że nie jest chora i nie zagrozi Flarze.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 3
* **daty:** 0100-09-12 - 0100-09-15
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * Arianna znalazła go, gdy opowiadał w mesie załodze o przeszłych doświadczeniach z Egzotycznymi Pięknościami, jak to on i jego brat COŚTAM piraci i po ich stronie Piękność. Ulubieniec załogi. Świetny gawędziarz.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 2
* **daty:** 0100-09-08 - 0100-09-11
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * rozpuścił plotki o Egzotycznych Pięknościach na Nonarionie.


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 1
* **daty:** 0100-07-28 - 0100-08-03
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * medical officer Astralnej Flary, Orbiter (czarny, afro, paw, augmentacje bio) (ENCAO: +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny); podchodzi do Eleny z HELLO SŁONECZKO i ją odstraszył XD.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-12-27
    1. Primus    : 7, @: 0111-12-27
        1. Sektor Astoriański    : 7, @: 0111-12-27
            1. Astoria, Orbita    : 1, @: 0111-12-27
                1. Kontroler Pierwszy    : 1, @: 0111-12-27
            1. Libracja Lirańska    : 5, @: 0100-11-16
                1. Anomalia Kolapsu, orbita    : 5, @: 0100-11-16
                    1. Planetoida Kazmirian    : 1, @: 0100-09-11
                    1. SC Nonarion Nadziei    : 1, @: 0100-09-11
                        1. Moduł ExpanLuminis    : 1, @: 0100-09-11
                    1. Strefa Biur HR    : 1, @: 0100-11-16
                    1. Strefa Upiorów Orbitera    : 3, @: 0100-11-16
                        1. Planetoida Kazmirian    : 2, @: 0100-11-10
                        1. Planetoida Lodowca    : 3, @: 0100-11-16

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Daria Czarnewik      | 6 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Elena Verlen         | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 231018-anomalne-awarie-athamarein)) |
| Maja Samszar         | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Leszek Kurzmin       | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Grażyna Burgacz      | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szczepan Myrczek     | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alicja Szadawir      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Andrzej Gwozdnik     | 1 | ((231018-anomalne-awarie-athamarein)) |
| Błażej Sowiński      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Eustachy Korkoran    | 1 | ((231018-anomalne-awarie-athamarein)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hiacynt Samszar      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Jakub Oroginiec      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudia Stryk        | 1 | ((231018-anomalne-awarie-athamarein)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leona Astrienko      | 1 | ((231018-anomalne-awarie-athamarein)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Remigiusz Alkarenit  | 1 | ((231018-anomalne-awarie-athamarein)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Uśmiechniczka Konstrukcjonistka Aurora Diakon | 1 | ((231018-anomalne-awarie-athamarein)) |