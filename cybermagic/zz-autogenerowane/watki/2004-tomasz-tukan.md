# Tomasz Tukan
## Identyfikator

Id: 2004-tomasz-tukan

## Sekcja Opowieści

### Jak wsadzić Ulę Alanowi?

* **uid:** 220816-jak-wsadzic-ule-alanowi, _numer względny_: 15
* **daty:** 0111-10-12 - 0111-10-13
* **obecni:** Karolina Terienak, Marysia Sowińska, Mimoza Diakon, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Części do Hestii nadal nie dotarły do Marysi - okazuje się, że Mimoza ją sabotuje. Więc jest ryzyko, że części do Hestii dojdą i Jeremi Sowiński będzie miał dostęp do pełni mocy Hestii a Marysia nie XD. Dodatkowo, Karo poprosiła Marysię by ta wsadziła Ulę Alanowi, ale nie chcą zabijać kota. Ale JAK wsadzić Alanowi Ulę (z kotem) jako uczennicę, skoro vistermin jest morderczy, zbliża się audyt Marysi a w Tukanie obudził się terminus (i nie chce ryzykować Uli)? I do tego Alan nie może się dowiedzieć? Do tego okazuje się że części do Hestii konkurują z normalnymi częściami potrzebnymi na tym terenie i DLATEGO Mimoza - paladynka cholerna - blokuje Marysię...

Aktor w Opowieści:

* Dokonanie:
    * jakkolwiek spełnia prośby Marysi, zmartwiła go prośba by wsadzić Ulę (z visterminem!) pod Alana bo się o Ulę boi. Może gardzić subterminuskami, ale nie chce ich krzywdy - więc się postawił Marysi.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 14
* **daty:** 0111-07-20 - 0111-07-25
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * zamiast wsadzić Arkadię za niewinność do więzienia (by usunąć ją jako zagrożenie dla Ernesta Namartela) wkręcił ją w program dla uczniów terminusa. I tak zakręcił, że mu podziękowali XD.
* Progresja:
    * zadowolenie ze strony zarówno Arkadii Verlen jak i Pustogoru - znalazł dla niej coś fajnego: fuszkę "uczennicy terminusa".


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 13
* **daty:** 0111-07-03 - 0111-07-05
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * gdy Marysia powiedziała o mandragorze, natychmiast wysłał Marysi do pomocy Laurę i Ekaterinę.


### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 12
* **daty:** 0111-06-22 - 0111-06-24
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * sprawa z ko-matrycą Kuratorów była tak poważna, że zajął się nią sam a nie delegował Laurze. Przekonał Marysię, by oddała ją Pustogorowi i dał ją Alanowi, by zastawić pułapkę na mafię. Plus, wślizgnął się na emeryturę do Sowińskich. Wielki wygrany. I postąpił właściwie.
* Progresja:
    * znalazł sobie złoty bilet - gdy Pustogor go usunie lub gdy Marysia Sowińska opuści ten teren ma gwarantowane miejsce w Aurum, przy Marysi Sowińskiej.


### Porwanie Daniela Terienaka

* **uid:** 210720-porwanie-daniela-terienaka, _numer względny_: 11
* **daty:** 0111-06-14 - 0111-06-16
* **obecni:** Daniel Terienak, Karolina Terienak, Mariusz Trzewń, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

Aktor w Opowieści:

* Dokonanie:
    * powiedział Marysi, że Ula nie ma autoryzowanej żadnej akcji terminusów. Był skłonny sprzedać Marysi sekrety przeszłości Uli za przysługę. Pies na luksusy w Aurum z ładnymi kobietami.
* Progresja:
    * w Pustogorze już nikt nie ma wątpliwości, że się sprzedał Aurum/Sowińskim. Przynajmniej nie mafii, nie?


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 10
* **daty:** 0111-06-06 - 0111-06-09
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * pomógł Marysi Sowińskiej uwolnić Arkadię by rozwiązać problemy Rekinów w świecie Rekinów, za co dostał dobry weekend z piękną damą w luksusowych hotelach Sowińskich w Aurum.


### Przygoda, randka i porwanie

* **uid:** 201020-przygoda-randka-i-porwanie, _numer względny_: 9
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Daniel Sowiński, Gabriel Ursus, Henryk Sowiński, Kacper Bankierz, Laurencjusz Sorbian, Liliana Bankierz, Robert Pakiszon, Robinson Porzecznik, Tomasz Tukan, Triana Porzecznik, Tymon Grubosz, Urszula Miłkowicz

Streszczenie:

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

Aktor w Opowieści:

* Dokonanie:
    * Gabriel się doń zwrócił by pomógł przekonać opiekuna Uli. Tak wymanewrował w rozmowie, że wygrał WSZYSTKO - dostał kontakt do Sowińskich oraz chwałę za sukces. Ula nie dostała nic a Gabriel dostał straszny opiernicz.
* Progresja:
    * uznanie Laurencjusza Sorbiana i kontakty z Sowińskimi; przechwycił lwią część sławy i zasług za akcję Gabriela i Roberta.


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 8
* **daty:** 0110-09-18 - 0110-09-21
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Dostał LEGENDARNY opiernicz od Ksenii za to, że puszcza Laurę na akcje z potworami w terenie Esuriit I NAWET O TYM NIE WIE. Poszło do papierów.


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 7
* **daty:** 0110-08-24 - 0110-08-26
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * neuronauta-terminus; nie cierpi Minerwy i jej "chorej" magii, nie cierpi Pięknotki. Chce koniecznie udowodnić winę Minerwy; upokorzony przed studentką przez Pięknotkę.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 6
* **daty:** 0110-07-24 - 0110-07-27
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * chroni interesy mafii przeciw Pustogorowi. Ale nie chce szkody terminusów. Zwrócił się do Pięknotki; Malictrix spadnie na niego jako jego "zasługa". Nie ma za co.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 5
* **daty:** 0110-06-07 - 0110-06-09
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * chciał rozmontować kult i się o nim wszystkiego dowiedzieć; zainfekował się Esuriit i podpadł Kajratowi. Uratowany przez Pięknotkę.


### Somnibel uciekł Arienikom

* **uid:** 190709-somnibel-uciekl-arienikom, _numer względny_: 4
* **daty:** 0110-06-05 - 0110-06-07
* **obecni:** Ernest Kajrat, Jan Revlen, Ksawery Wojnicki, Pięknotka Diakon, Staś Arienik, Tomasz Tukan, Urszula Arienik

Streszczenie:

Bogatemu rodowi Arieników powiązanemu z Luxuritias uciekł somnibel. Pięknotka znalazła go u Kajrata - który dla odmiany nie porwał kociego viciniusa; dostał go od podwładnego. Kajrat oddał somnibela Pięknotce po wykonaniu pewnego testu a napięcie między Arienikami a "plebsem" z Podwiertu się podniosło.

Aktor w Opowieści:

* Dokonanie:
    * nieprzekonany do pomocy Pięknotce, ale mógł pomóc dzieciakom Dotkniętym przez energię somnibela; znalazł coś u jednej z dziewczynek.


### Anomalna Serafina

* **uid:** 190616-anomalna-serafina, _numer względny_: 3
* **daty:** 0110-05-12 - 0110-05-15
* **obecni:** Antoni Żuwaczka, Ernest Kajrat, Krystian Namałłek, Pięknotka Diakon, Ronald Grzymość, Serafina Ira, Tomasz Tukan

Streszczenie:

W Podwiercie pojawiła się Serafina, piosenkarka zbierająca i asymilująca anomalie. Wyraźnie pomaga jej Kajrat, który ją dodatkowo chroni. Pięknotka dostała zadanie odzyskać te niestabilne anomalie - ale Serafina jest lubiana przez kilku cieniaszczyckich bonzów; nie można odebrać jej anomalii siłą. Dodatkowo, Serafina ma w sobie stary gniew na Pustogor za coś, co się zdarzyło w Pacyfice. Wszystko zbliża się w kierunku na wojnę lub konflikt zbrojny.

Aktor w Opowieści:

* Dokonanie:
    * niezły neuronauta, katalista i terminus ale nie ma kręgosłupa i nie radzi sobie z konfliktem. Poniewierany i upokarzany przez mafię, mimo, że powinien słuchać Pięknotkę..
* Progresja:
    * Upokorzony i pogardzany przez mafię. Ma jeszcze mniejszy kręgosłup niż kiedykolwiek.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 2
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * neuronauta Pustogoru, terminus, który nie chce podpadać Ernestowi. Nienawidzi seksbotów, całkowicie. Najchętniej zadowoliłby mafię nawet kosztem żywej istoty.


### Anomalna figurka Żabboga

* **uid:** 200507-anomalna-figurka-zabboga, _numer względny_: 1
* **daty:** 0109-09-19 - 0109-09-26
* **obecni:** Damian Polwonien, Gustaf Profnos, Jan Łowicz, Kinga Kruk, Melinda Teilert, Natasza Aniel, Tomasz Tukan

Streszczenie:

Próba kradzieży figurki z Fantasmagorii doprowadziła Zespół do Melindy, która chce pomóc delikwentowi który stracił majątek. Janek decyduje się usunąć swoją stalkerkę raz na zawsze - ale jak poznał ją bliżej, zrobiło mu się jej żal. Okazało się, że owa figurka jest magiczną anomalią zdolną do niszczenia dokumentów. Kinga stworzyła jej fałszywki by nikt nie był w stanie ich użyć, a Natasza sprzedała Tukanowi możliwość użycia za ochronę i kontakty w Aurum.

Aktor w Opowieści:

* Dokonanie:
    * stworzył nietrywialny plan zdobycia w ukryciu przed Pustogorem figurki niszczącej dane i dokumenty, by ostatecznie ją otrzymać w handlu z Fantasmagorią.
* Progresja:
    * zobowiązany do ochrony Fantasmagorii, jej członków oraz do pomocy w odbudowaniu pozycji i bogactwa Melindy Teilert (plus ochrony).
    * uzyskał Anomalną Figurkę Żabboga, dzięki której może zniszczyć dowolne dokumenty. Ale użyć może jej tylko ktoś kto nie wie jak działa i kto ją ukradł.
    * jest absolutnie przekonany, że na terenie Podwiertu jest potężna ukryta organizacja (najpewniej z Aurum), która stoi za Fantasmagorią.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 14, @: 0111-10-13
    1. Primus    : 14, @: 0111-10-13
        1. Sektor Astoriański    : 14, @: 0111-10-13
            1. Astoria    : 14, @: 0111-10-13
                1. Sojusz Letejski    : 14, @: 0111-10-13
                    1. Szczeliniec    : 14, @: 0111-10-13
                        1. Powiat Jastrzębski    : 1, @: 0110-07-27
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-27
                                1. Klinika Iglica    : 1, @: 0110-07-27
                                    1. Kompleks Itaran    : 1, @: 0110-07-27
                                1. TechBunkier Sarrat    : 1, @: 0110-07-27
                            1. Kalbark    : 1, @: 0110-07-27
                                1. Escape Room Lustereczko    : 1, @: 0110-07-27
                        1. Powiat Pustogorski    : 14, @: 0111-10-13
                            1. Czółenko    : 2, @: 0111-06-24
                            1. Podwiert    : 10, @: 0111-10-13
                                1. Dolina Biurowa    : 1, @: 0110-05-15
                                1. Dzielnica Luksusu Rekinów    : 6, @: 0111-10-13
                                    1. Obrzeża Biedy    : 2, @: 0111-06-16
                                        1. Stadion Lotników    : 2, @: 0111-06-16
                                    1. Sektor Brudu i Nudy    : 2, @: 0111-06-16
                                        1. Skrytki Czereśniaka    : 2, @: 0111-06-16
                                    1. Serce Luksusu    : 3, @: 0111-07-25
                                        1. Apartamentowce Elity    : 2, @: 0111-07-25
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-07-05
                                1. Iglice Nadziei    : 1, @: 0110-06-07
                                    1. Posiadłość Arieników    : 1, @: 0110-06-07
                                1. Las Trzęsawny    : 4, @: 0111-07-05
                                    1. Jeziorko Mokre    : 1, @: 0111-07-05
                                1. Osiedle Leszczynowe    : 3, @: 0110-06-09
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0109-09-26
                                    1. Szkoła Nowa    : 2, @: 0110-06-09
                            1. Zaczęstwo    : 5, @: 0111-06-16
                                1. Akademia Magii, kampus    : 2, @: 0110-08-26
                                    1. Budynek Centralny    : 2, @: 0110-08-26
                                        1. Skrzydło Loris    : 1, @: 0110-04-26
                                1. Bazar Różności    : 1, @: 0110-10-27
                                1. Biurowiec Gorland    : 1, @: 0110-10-27
                                1. Cyberszkoła    : 1, @: 0110-08-26
                                1. Dzielnica Kwiecista    : 1, @: 0110-10-27
                                    1. Rezydencja Porzeczników    : 1, @: 0110-10-27
                                        1. Podziemne Laboratorium    : 1, @: 0110-10-27
                                1. Kawiarenka Leopold    : 1, @: 0110-10-27
                                1. Nieużytki Staszka    : 2, @: 0110-10-27
                                1. Osiedle Ptasie    : 1, @: 0111-06-16
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 6 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Pięknotka Diakon     | 6 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Ernest Kajrat        | 4 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit)) |
| Karolina Terienak    | 4 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Urszula Miłkowicz    | 4 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Kacper Bankierz      | 3 | ((201020-przygoda-randka-i-porwanie; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow)) |
| Liliana Bankierz     | 3 | ((190519-uciekajacy-seksbot; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Mariusz Trzewń       | 3 | ((200202-krucjata-chevaleresse; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Rafał Torszecki      | 3 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210921-przybycie-rekina-z-eterni)) |
| Amelia Sowińska      | 2 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni)) |
| Daniel Terienak      | 2 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201020-przygoda-randka-i-porwanie)) |
| Minerwa Metalia      | 2 | ((200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Amanda Kajrat        | 1 | ((190714-kult-choroba-esuriit)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Jan Łowicz           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Julia Kardolin       | 1 | ((210713-rekin-wspiera-mafie)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kinga Kruk           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Krystian Namałłek    | 1 | ((190616-anomalna-serafina)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Melinda Teilert      | 1 | ((200507-anomalna-figurka-zabboga)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Natasza Aniel        | 1 | ((200507-anomalna-figurka-zabboga)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Serafina Ira         | 1 | ((190616-anomalna-serafina)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |