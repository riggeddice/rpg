# Jan Lertys
## Identyfikator

Id: 9999-jan-lertys

## Sekcja Opowieści

### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 6
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * wspiera Bartłomieja Korkorana i tylko jego z rodziny Korkoranów. Wszyscy inni są jego zdaniem lojalistami Kidirona.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 5
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * cichy i spokojny Janek po tym jak Tymon Korkoran wzgardził jego siostrą dał mu TAK SOLIDNIE W MORDĘ że Tymon stracił przytomność i do końca sesji nie odzyskał.


### Osy w CES Purdont

* **uid:** 220817-osy-w-ces-purdont, _numer względny_: 4
* **daty:** 0093-01-23 - 0093-01-24
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Joachim Puriur, Kamil Wraczok, Kordian Olgator, VN Karglondel

Streszczenie:

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

Aktor w Opowieści:

* Dokonanie:
    * skupia się na ochronie Celiny gdy ta robi antidotum, na szybkim wspieraniu Ardilli gdy ta szuka składników i na strzelaniu do Trianai by chronić Zespół. Kompetentny ale nic super.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 3
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * silny i zdolny do bitki, choć nie najinteligentniejszy mat Inferni zakochany w Darii Raizis. Pisze dla Darii kompromitujące wiersze i nie aprobuje relacji Celiny w stronę Eustachego. Bardzo opiekuńczy wobec siostry i Zespołu. Rozstrzelał ainshkera z Eustachym.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 2
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * pomagał Ardilii w scoutowaniu przemytników i przesunął się na pozycję "Infernia" a nie "Nativis" w postrzeganiu;
* Progresja:
    * opierdol i nieufność ludzi z Nativis, że pozwala Ardilli z Inferni się "wałęsać na lewo i prawo". On jest z Inferni, nie z Nativis.


### Polowanie na szczury w Nativis

* **uid:** 220723-polowanie-na-szczury-w-nativis, _numer względny_: 1
* **daty:** 0087-05-03 - 0087-05-12
* **obecni:** Celina Lertys, Emilia d'Erozja, Jan Lertys, Karol Lertys, Laurencjusz Kidiron, OA Erozja Ego, Tymon Korkoran

Streszczenie:

Młodzi Lertysi - Celina i Janek - próbowali sobie dorobić polując na szczury w Nativis. Skonfliktowali się z Laurencjuszem Kidironem (i go sponiewierali skunksem), ale przez to musieli użyć wiedzy o kanałach serwisowych Nativis. To sprawiło że znaleźli ślady advancera polującego na ich dziadka. I to sprawiło, że poznali prawdę o swoim dziedzictwie - ich dziadek nie jest ich dziadkiem a oni pochodzą z martwej arkologii Aspiria.

Aktor w Opowieści:

* Dokonanie:
    * brat Celiny; świetnie porusza się po tunelach arkologii i umie pozyskać różne rzeczy (np. spray skunksowy). Raczej słucha się siostry, bardzo związany z dziadkiem.
* Progresja:
    * ogólna reputacja 'dziwnego' w Nativis; nikt nie chce z nim zadzierać. Wie, że pochodzi z Aspirii.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0093-02-12
    1. Primus    : 5, @: 0093-02-12
        1. Sektor Astoriański    : 5, @: 0093-02-12
            1. Neikatis    : 5, @: 0093-02-12
                1. Dystrykt Glairen    : 5, @: 0093-02-12
                    1. Arkologia Nativis    : 4, @: 0093-02-12
                        1. Poziom 1 - Dolny    : 2, @: 0092-08-27
                            1. Północ - Stara Arkologia    : 1, @: 0092-08-27
                                1. Blokhaus E    : 1, @: 0092-08-27
                            1. Zachód    : 1, @: 0087-05-12
                                1. Stare TechBunkry    : 1, @: 0087-05-12
                        1. Poziom 2 - Niższy Środkowy    : 1, @: 0087-05-12
                            1. Wschód    : 1, @: 0087-05-12
                                1. Centrum Kultury i Rozrywki    : 1, @: 0087-05-12
                                    1. Bar Śrubka z Masła    : 1, @: 0087-05-12
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 2, @: 0093-01-24
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 5 | ((220720-infernia-taksowka-dla-lycoris; 220723-polowanie-na-szczury-w-nativis; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Ardilla Korkoran     | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Tymon Korkoran       | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| OO Infernia          | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Rafał Kidiron        | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Kalia Awiter         | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karol Lertys         | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Lycoris Kidiron      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ralf Tapszecz        | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |