# Tymon Korkoran
## Identyfikator

Id: 9999-tymon-korkoran

## Sekcja Opowieści

### Korkoran płaci cenę za Nativis

* **uid:** 230726-korkoran-placi-cene-za-nativis, _numer względny_: 6
* **daty:** 0093-03-29 - 0093-03-30
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Marcel Draglin, OO Infernia, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Draglin zapędził Czarnymi Hełmami Tymona Korkorana do Muzeum Kidirona. Używając sił swoich i Inferni (pod rozkazami Eustachego) uratował piratów pod mentalnym wpływem. Negocjacje Lobrak - Eustachy pokazały Eustachemu, że nie są w stanie pokonać Syndykatu. Ardilla nie pozwala mu dołączyć Arkologii do Syndykatu - to nie to co powinno być. W chwili, w której Eustachy skonsolidował pełnię sił i mocy Tymon eksploduje wysadzając siebie i Wujka. Eustachy zostaje bez swojego sumienia, sam z Ardillą, Draglinem, Kalią i uszkodzoną Arkologią. Naprzeciw Syndykatu i Lobrakowi, który chce sojuszu.

Aktor w Opowieści:

* Dokonanie:
    * naciśnięty przez Eustachego poza zakres maksimum, został złamany. Powiedział Eustachemu o wszystkich planach KiKo, po czym wysadził się z Wujkiem (swoim ojcem). "Zabrałeś mi wszystko, ojca Ci nie oddam". KIA.


### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 5
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * zdradził Infernię i dołączył do Laurencjusza. Zależy mu na przekonaniu Eustachego do sojuszu. Wierzy, że Lobrak to tylko doradca i dał się zaślepić Izabelli.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 4
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * regularnie próbował się wkraść do Ambasadorki podglądać dziewczyny i znalazł ścieżkę którą powiedział Ralfowi. Nie wiadomo czy być z niego dumnym czy nim gardzić.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 3
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * jedyny świadek tego, że Eustachy COŚ ZROBIŁ z Infernią. Wszystko się mu rozpada - Kidiron skupia się na Eustachym, Infernia wypada z rąk. Eksplodował na Celinę nazywając ją znajdką i wujek nim wzgardził. Potem sklupany przez Janka. Stracił WSZYSTKO.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 2
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * kuzyn Eustachego; bardzo chce się przypodobać Kidironom; Eustachy wpakował go w pułapkę by Robaki go sklepały. Tak dąży do chwały i potęgi że wpadł w to jak śliwka w kompot. De facto został "tym przez kogo rozwiązano konflikt" w oczach Kidironów i Nativis.
* Progresja:
    * przesunięty na zupełnie inną pozycję, wielka chwała u Kidironów. PLUS nieufność ojca (Bartłomieja Korkorana) i Karola Lertysa (dziadka Celiny i Janka)


### Polowanie na szczury w Nativis

* **uid:** 220723-polowanie-na-szczury-w-nativis, _numer względny_: 1
* **daty:** 0087-05-03 - 0087-05-12
* **obecni:** Celina Lertys, Emilia d'Erozja, Jan Lertys, Karol Lertys, Laurencjusz Kidiron, OA Erozja Ego, Tymon Korkoran

Streszczenie:

Młodzi Lertysi - Celina i Janek - próbowali sobie dorobić polując na szczury w Nativis. Skonfliktowali się z Laurencjuszem Kidironem (i go sponiewierali skunksem), ale przez to musieli użyć wiedzy o kanałach serwisowych Nativis. To sprawiło że znaleźli ślady advancera polującego na ich dziadka. I to sprawiło, że poznali prawdę o swoim dziedzictwie - ich dziadek nie jest ich dziadkiem a oni pochodzą z martwej arkologii Aspiria.

Aktor w Opowieści:

* Dokonanie:
    * lekkoduch rozdarty między wujem Bartłomiejem i Laurencjuszem Kidironem (15); został słupem dla Celiny odnośnie polowania na szczury, bo chce zaimponować swojej eks-dziewczynie że robi coś przydatnego (i zarobić za nic).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0093-03-30
    1. Primus    : 6, @: 0093-03-30
        1. Sektor Astoriański    : 6, @: 0093-03-30
            1. Neikatis    : 6, @: 0093-03-30
                1. Dystrykt Glairen    : 6, @: 0093-03-30
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis    : 6, @: 0093-03-30
                        1. Poziom 1 - Dolny    : 3, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Magazyny    : 1, @: 0093-03-28
                                1. Processing    : 1, @: 0093-03-28
                                1. Stacje Skorpionów    : 1, @: 0093-03-28
                            1. Północ - Stara Arkologia    : 2, @: 0093-03-28
                                1. Blokhaus E    : 2, @: 0093-03-28
                                1. Blokhaus F    : 1, @: 0093-03-28
                                    1. Szczurowisko    : 1, @: 0093-03-28
                                1. Stare Wejście Północne    : 1, @: 0093-03-28
                            1. Wschód    : 1, @: 0093-03-28
                                1. Farmy Wschodnie    : 1, @: 0093-03-28
                            1. Zachód    : 2, @: 0093-03-28
                                1. Centrala Prometeusa    : 1, @: 0093-03-28
                                1. Stare TechBunkry    : 2, @: 0093-03-28
                        1. Poziom 2 - Niższy Środkowy    : 3, @: 0093-03-30
                            1. Południe    : 1, @: 0093-03-28
                                1. Engineering    : 1, @: 0093-03-28
                                1. Power Core    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Bazar i sklepy    : 1, @: 0093-03-28
                            1. Wschód    : 3, @: 0093-03-30
                                1. Centrum Kultury i Rozrywki    : 3, @: 0093-03-30
                                    1. Bar Śrubka z Masła    : 2, @: 0093-03-28
                                    1. Muzeum Kidirona    : 2, @: 0093-03-30
                                    1. Ogrody Gurdacza    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Koszary Hełmów    : 1, @: 0093-03-28
                                1. Sektor Porządkowy    : 1, @: 0093-03-28
                        1. Poziom 3 - Górny Środkowy    : 2, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Life Support    : 1, @: 0093-03-28
                                1. Medical    : 1, @: 0093-03-28
                            1. Wschód    : 2, @: 0093-03-28
                                1. Dzielnica Luksusu    : 2, @: 0093-03-28
                                    1. Ambasadorka Ukojenia    : 2, @: 0093-03-28
                                    1. Ogrody Wiecznej Zieleni    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                        1. Poziom 4 - Górny    : 1, @: 0093-03-28
                            1. Wschód    : 1, @: 0093-03-28
                                1. Centrala dowodzenia    : 1, @: 0093-03-28
                                1. Obserwatorium Astronomiczne    : 1, @: 0093-03-28
                                1. Radiowęzeł    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Tereny Sportowe    : 1, @: 0093-03-28
                        1. Poziom 5 - Szczyt    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Port kosmiczny    : 1, @: 0093-03-28
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 5 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 4 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Jan Lertys           | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Ralf Tapszecz        | 3 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Tobiasz Lobrak       | 3 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Izabella Saviripatel | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Kalia Awiter         | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Karol Lertys         | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Laurencjusz Kidiron  | 2 | ((220723-polowanie-na-szczury-w-nativis; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Marcel Draglin       | 2 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230726-korkoran-placi-cene-za-nativis)) |
| Rafał Kidiron        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |