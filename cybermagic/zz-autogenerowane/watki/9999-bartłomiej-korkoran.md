# Bartłomiej Korkoran
## Identyfikator

Id: 9999-bartłomiej-korkoran

## Sekcja Opowieści

### Korkoran płaci cenę za Nativis

* **uid:** 230726-korkoran-placi-cene-za-nativis, _numer względny_: 11
* **daty:** 0093-03-29 - 0093-03-30
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Marcel Draglin, OO Infernia, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Draglin zapędził Czarnymi Hełmami Tymona Korkorana do Muzeum Kidirona. Używając sił swoich i Inferni (pod rozkazami Eustachego) uratował piratów pod mentalnym wpływem. Negocjacje Lobrak - Eustachy pokazały Eustachemu, że nie są w stanie pokonać Syndykatu. Ardilla nie pozwala mu dołączyć Arkologii do Syndykatu - to nie to co powinno być. W chwili, w której Eustachy skonsolidował pełnię sił i mocy Tymon eksploduje wysadzając siebie i Wujka. Eustachy zostaje bez swojego sumienia, sam z Ardillą, Draglinem, Kalią i uszkodzoną Arkologią. Naprzeciw Syndykatu i Lobrakowi, który chce sojuszu.

Aktor w Opowieści:

* Dokonanie:
    * częściowo Dotknięty przez Izabellę, częściowo żałujący swoich decyzji odnośnie Tymona, poszedł go przekonać. Tymon się wysadził, zabijając siebie i Bartłomieja. Bartłomiej MÓGŁBY może go powstrzymać... ale nie miał do tego serca. Jeden raz gdy potrzebował rodziny, został sam. KIA.


### Wojna o Arkologię Nativis - nowa regentka

* **uid:** 230719-wojna-o-arkologie-nativis-nowa-regentka, _numer względny_: 10
* **daty:** 0093-03-28 - 0093-03-29
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Izabella Saviripatel, Kalia Awiter, Marcel Draglin, OO Infernia, Ralf Tapszecz, Tobiasz Lobrak

Streszczenie:

Ardilla ma plan przejęcia kontroli nad Arkologią, w tle Kidiron przez nią kontrolowany. To też neutralizuje główne ataki L&T, że Kidiron taki zły. Z pomocą Kalii (ewakuowanej ze skrzydła medycznego) zbudowała linię propagandową i po odzyskaniu Radiowęzła, Kalia nadała wiadomość pokoju i pojednania. Eustachy zmiażdżył główne siły L&T koło Engineering, Kalia JAKIMŚ CUDEM została regentką przez przypadek a czarodziejka Syndykatu, Izabella została zmiażdżona w imię Nihilusa przez Ralfa chroniącego Ardillę.

Aktor w Opowieści:

* Dokonanie:
    * z Draglinem wbił się i zdobył Radiowęzeł. Chce uratować syna, Tymona.


### Wojna o Arkologię Nativis - konsolidacja sił

* **uid:** 230628-wojna-o-arkologie-nativis-konsolidacja-sil, _numer względny_: 9
* **daty:** 0093-03-27 - 0093-03-28
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Eustachy Korkoran, Izabella Saviripatel, Karol Lertys, Laurencjusz Kidiron, Marcel Draglin, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

W Nativis panuje wojna domowa - Laurencjusz i Tymon próbują przejąć władzę jako 'nowi lepsi Kidiron + Korkoran'. Eustachy i Ardilla konsolidują lojalistów arkologii - Lertysów, Draglina, Szczury - tworząc potężną siłę pro-Rafał Kidiron. Gdy Laurencjusz i Tymon wspomagani przez Lobraka próbują Eustachego przekonać do współpracy, Eustachy odrzuca. Oddziały Inferni z jakiegoś powodu krzywdzą cywili (co dziwi Eustachego i martwi Ardillę) i zbrodnie są dobrze nagłośnione, spadając na ręce Eustachego. Farighanowie są odparci. Szczury są złamane. I wiemy, że L+T współpracowali z Infiltratorem...

Aktor w Opowieści:

* Dokonanie:
    * zajmuje główne komponenty Arkologii by zapewnić jej przetrwanie (i oba statki - Infernię i Szare Ostrze). Gdy Draglin obiecał współpracę pod Eustachym, oficjalnie oddał się do dyspozycji Eustachego. Ale robi swoje.


### Infiltrator ucieka a Arkologia płonie

* **uid:** 230621-infiltrator-ucieka-a-arkologia-plonie, _numer względny_: 8
* **daty:** 0093-03-25 - 0093-03-26
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, BIA Prometeus, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Laurencjusz Kidiron, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

Aktor w Opowieści:

* Dokonanie:
    * przejął część Hełmów i próbuje ochronić Arkologię oraz Prometeusa. Nie ma Lancerów, więc działa czym może. Walczy z młodym Laurencjuszem Kidironem o to kto będzie regentem Arkologii.


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 7
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Dalmjer Servart, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * wreszcie zregenerowany. Nie wiedział nic o teorii 'Ralf jest magiem Nihilusa'. Próbuje chronić Eustachego przed Infernią, Infernię przed memoriam, Ardillę przed Ralfem. Po raz pierwszy potraktował Eustachego i Ardillę jak dorosłych. Gdy był atak na Arkologię, przejął kontrolę tymczasowo. Z łóżka w szpitalu.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 6
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * chciał zastąpić Ardillę jako zakładnik, ale nie wyszło. Więc wymienił się na grupę rannych ludzi. W momencie w którym Misteria już nie była racjonalna zaatakował ją i obezwładnił, ciężko ranny przez wiły. Dzięki temu pomógł doprowadzić do ewakuacji wszystkich z Ambasadorki.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 5
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * próbuje pokazać Eustachemu, że E. jest lepszy i skuteczniejszy. Że nie musiał zabijać tych noktian. Próbuje przekonać Eustachego pod kątem honoru i prawości. Ale Kidiron się zgadza z Eustachym - Bartłomiej wygląda na starszego niż jest. Widzi, że przegrywa wojnę o Eustachego i nie wie czemu. DOWIADUJE SIĘ, że Infernia jest pod mentalną kontrolą Eustachego.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 4
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * moralny, chce pomóc noktianom i przygarnął młodego noktianina, Ralfa Tapszecza. Chce by Infernia czyniła dobro. Bardzo rozczarowany Tymonem, który wzgardził Celiną i Ardillą - spoliczkował go i oddał dowodzenie misją Ardilli.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 3
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * PAST: łagodny wujek opiekujący się Eustachym i Adrillą, który jednak rozdziela opierdol gdy zasłużyli; bardzo ważna jest dlań rodzina. ACTUAL: dba o to, by Janek mógł spotkać się z Darią i próbuje chronić swoich młodych podopiecznych. Jednak zaufał im i oddał im do działania zainfekowany przez Trianai CES Purdont (na usprawiedliwienie, nie wie z czym ma do czynienia).
* Progresja:
    * w przeszłości znał się z Wiktorem Turkalisem. Lubią się.


### Dziewczynka Trianai

* **uid:** 220914-dziewczynka-trianai, _numer względny_: 2
* **daty:** 0092-09-10 - 0092-09-11
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Karina Nezerin, Stanisław Uczantor

Streszczenie:

Po zniszczeniu Robaków okazało się, że w tunelach Starej Arkologii zniknęło dwóch nastolatków. Ardilla i Eustachy poszli znaleźć owych nastolatków - faktycznie, coś tam jest. Eustachy zastawił pułapkę i prawie zabił pietnastolatkę zmienioną w Trianai. Udało im się wydobyć ją i dostarczyć Kidironom, choć wykazali się dużą bezwzględnością. Większość zdominowanych przez dziewczynkę ludzi udało się uratować. Ale kto jej to zrobił i czemu?

Aktor w Opowieści:

* Dokonanie:
    * wujek poprosił Eustachego i Ardillę, by oni rozwiązali problem znikających nastolatków w Starej Arkologii. Nie wierzy w Ducha Arkologii.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 1
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0093-03-30
    1. Primus    : 10, @: 0093-03-30
        1. Sektor Astoriański    : 10, @: 0093-03-30
            1. Neikatis    : 10, @: 0093-03-30
                1. Dystrykt Glairen    : 10, @: 0093-03-30
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis    : 10, @: 0093-03-30
                        1. Poziom 1 - Dolny    : 5, @: 0093-03-28
                            1. Południe    : 1, @: 0093-03-28
                                1. Magazyny    : 1, @: 0093-03-28
                                1. Processing    : 1, @: 0093-03-28
                                1. Stacje Skorpionów    : 1, @: 0093-03-28
                            1. Północ - Stara Arkologia    : 5, @: 0093-03-28
                                1. Blokhaus E    : 1, @: 0093-03-28
                                1. Blokhaus F    : 4, @: 0093-03-28
                                    1. Szczurowisko    : 2, @: 0093-03-28
                                1. Stare Wejście Północne    : 3, @: 0093-03-28
                                1. Szczurowisko    : 1, @: 0093-02-21
                            1. Wschód    : 1, @: 0093-03-28
                                1. Farmy Wschodnie    : 1, @: 0093-03-28
                            1. Zachód    : 2, @: 0093-03-28
                                1. Centrala Prometeusa    : 2, @: 0093-03-28
                                1. Stare TechBunkry    : 1, @: 0093-03-28
                        1. Poziom 2 - Niższy Środkowy    : 3, @: 0093-03-30
                            1. Południe    : 2, @: 0093-03-29
                                1. Engineering    : 2, @: 0093-03-29
                                1. Power Core    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Bazar i sklepy    : 1, @: 0093-03-28
                            1. Wschód    : 2, @: 0093-03-30
                                1. Centrum Kultury i Rozrywki    : 2, @: 0093-03-30
                                    1. Bar Śrubka z Masła    : 1, @: 0093-03-28
                                    1. Muzeum Kidirona    : 2, @: 0093-03-30
                                    1. Ogrody Gurdacza    : 1, @: 0093-03-28
                                    1. Stacja holosymulacji    : 1, @: 0093-03-28
                            1. Zachód    : 1, @: 0093-03-28
                                1. Koszary Hełmów    : 1, @: 0093-03-28
                                1. Sektor Porządkowy    : 1, @: 0093-03-28
                        1. Poziom 3 - Górny Środkowy    : 5, @: 0093-03-29
                            1. Południe    : 2, @: 0093-03-29
                                1. Life Support    : 1, @: 0093-03-28
                                1. Medical    : 2, @: 0093-03-29
                            1. Wschód    : 4, @: 0093-03-28
                                1. Dzielnica Luksusu    : 4, @: 0093-03-28
                                    1. Ambasadorka Ukojenia    : 3, @: 0093-03-28
                                    1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-28
                                    1. Stacja holosymulacji    : 2, @: 0093-03-28
                        1. Poziom 4 - Górny    : 2, @: 0093-03-29
                            1. Wschód    : 2, @: 0093-03-29
                                1. Centrala dowodzenia    : 1, @: 0093-03-28
                                1. Obserwatorium Astronomiczne    : 1, @: 0093-03-28
                                1. Radiowęzeł    : 2, @: 0093-03-29
                            1. Zachód    : 1, @: 0093-03-28
                                1. Tereny Sportowe    : 1, @: 0093-03-28
                        1. Poziom 5 - Szczyt    : 1, @: 0093-03-28
                            1. Północ    : 1, @: 0093-03-28
                                1. Port kosmiczny    : 1, @: 0093-03-28
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 1, @: 0093-01-22
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Eustachy Korkoran    | 10 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| OO Infernia          | 7 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Ralf Tapszecz        | 7 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Kalia Awiter         | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie; 230719-wojna-o-arkologie-nativis-nowa-regentka)) |
| Rafał Kidiron        | 6 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Tobiasz Lobrak       | 4 | ((230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Tymon Korkoran       | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce; 230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230726-korkoran-placi-cene-za-nativis)) |
| Celina Lertys        | 3 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Izabella Saviripatel | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Marcel Draglin       | 3 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil; 230719-wojna-o-arkologie-nativis-nowa-regentka; 230726-korkoran-placi-cene-za-nativis)) |
| Dalmjer Servart      | 2 | ((230614-atak-na-kidirona; 230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 2 | ((230621-infiltrator-ucieka-a-arkologia-plonie; 230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 2 | ((220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| BIA Prometeus        | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Franciszek Pietraszczyk | 1 | ((230208-pierwsza-randka-eustachego)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Karol Lertys         | 1 | ((230628-wojna-o-arkologie-nativis-konsolidacja-sil)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230621-infiltrator-ucieka-a-arkologia-plonie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |