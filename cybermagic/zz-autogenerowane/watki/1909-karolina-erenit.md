# Karolina Erenit
## Identyfikator

Id: 1909-karolina-erenit

## Sekcja Opowieści

### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 9
* **daty:** 0110-10-07 - 0110-10-09
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * samotna, ciepła wobec Myrczka. Myrczek wymiauczał sobie jej pomoc przy dezinhibitorze. Wybitny taktyk, powiedziała Zespołowi czemu coś jej śmierdzi w planie Myrczka. Zintegrowana magią z Gabrielem, sama poczuła jak podły plan miał Kumczek i umysł Loreny Gwozdnik.


### Kto wrobił Alana

* **uid:** 190830-kto-wrobil-alana, _numer względny_: 8
* **daty:** 0110-06-03 - 0110-06-05
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Pięknotka Diakon, Talia Aegis, Wojciech Zermann

Streszczenie:

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

Aktor w Opowieści:

* Dokonanie:
    * zwinęła Chevaleresse artefakt Alana i oddała go magom Aurum by wpakować ich w katastrofalne kłopoty.


### Chevaleresse

* **uid:** 190217-chevaleresse, _numer względny_: 7
* **daty:** 0110-02-25 - 0110-02-27
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

Aktor w Opowieści:

* Dokonanie:
    * w Cyberszkole zaatakowana przez Dianę, potem pchnięta na ziemię przez Tymona gdy ów chciał przerwać jej czar. Ogólnie, nie jej dzień ;-).


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 6
* **daty:** 0110-02-17 - 0110-02-20
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Marlena Maja Leszczyńska, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Karolina źle reaguje na to, że stała się czarodziejką i chciała odrzucić moc. Jednak nie udało jej się, jedynie zdewastowała Cyberszkołę i zagroziła ludziom. Karolina sprowadziła Ixion do Cyberszkoły. Zespół ją powstrzymał i Pięknotka zaczęła reintegrować życie Karoliny, kierując ją do Marleny Mai i do AMZ.

Aktor w Opowieści:

* Dokonanie:
    * chciała zachować pamięć i nie stracić przyjaciół, więc za szeptami Saitaera otworzyła portal do Ixionu w Cyberszkole. Zatrzymana przez Pięknotkę, musi jakoś wejść w życie w społeczeństwo jako mag mimo swej nieufności
* Progresja:
    * dużo potężniejsza czarodziejka niż się wydawało; Saitaer zrobił dobrą robotę.


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 5
* **daty:** 0110-02-01 - 0110-02-05
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * przestała być ofiarą. Gdy stała się czarodziejką, zaczęła działać inaczej - pomiędzy strachem a agresją. Dotknął ją Karradrael, by wyczyścić Saitaera.
* Progresja:
    * stała się czarodziejką mocą Saitaera; odebrała moc Wojtkowi Kurczynosowi.
    * mocą Karradraela wyczyszczona z wpływów Saitaera - Władca Rekonstrukcji nie ma na niej holdu.
    * nielubiana przez wielu magów zwłaszcza w szkole z uwagi na korelację z jej uzyskaniem mocy i utratą mocy przez Wojtka.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 4
* **daty:** 0110-01-29 - 0110-01-30
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * stały target Wojtka Kurczynosa i kilku innych magów Szkoły Magów w Zaczęstwie. Jako, że jest człowiekiem, nic nie pamięta.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 3
* **daty:** 0110-01-05 - 0110-01-06
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * była źle traktowana przez magów (nie wie o tym), więc Napoleon Bankierz próbował ją chronić eliksirem Adeli Kirys. Stała się wektorem toksyny z Trzęsawiska. Tymczasowo.


### Wojna o uczciwe półfinały

* **uid:** 181101-wojna-o-uczciwe-polfinaly, _numer względny_: 2
* **daty:** 0109-10-17 - 0109-10-19
* **obecni:** Alan Bartozol, Damian Podpalnik, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Marlena przybyła do Zaczęstwa sprawdzić, czy Zaczęstwiacy oszukują w SupMis. Okazało się, że to ludzie - nie wiedzą o magii i tak, magia im pomaga. Marlena zaczęła próbować im pomóc i wyplątać ich z magii zanim yyizdis zabanuje tą drużynę (konkurencję Marleny). Spowodowała Efekt Skażenia i musiała ją uratować Pięknotka, która zaczęła się zastanawiać jak wplątała się w tą sprawę. Koniec końców się udało - ale Pięknotka dostała w odpowiedzialność zarządzanie Zaczęstwem jako terminuska...

Aktor w Opowieści:

* Dokonanie:
    * rozerwana magicznie ze studnią życzeń, podskoczyła jej uroda drastycznie plus w ciągu miesiąca będzie wybitnym graczem SupMis.
* Progresja:
    * utraciła połączenie ze Studnią Życzeń; nie działają w jej okolicy już dziwne magiczne efekty
    * ma nadnaturalne umiejętności grania w Supreme Missionforce; do tego jest naprawdę piękną dziewczyną przez zrost mocy Pięknotki i Tymona


### Zaczęstwiacy czy Karolina

* **uid:** 181030-zaczestwiacy-czy-karolina, _numer względny_: 1
* **daty:** 0109-10-15 - 0109-10-16
* **obecni:** Damian Podpalnik, Karolina Erenit, Kirisu Gero, Lia Sagabello, Mariusz Kozaczek, Michał Krutkiwąs

Streszczenie:

Esportowa gildia Zaczęstwiaków ma pewien problem - na Damiana polują Eteryczne Echa. Kilku członków gildii zostało w to wplątanych; rzucili się na pomoc Damianowi by nie tracić kluczowego gracza. Okazało się, że w to wszystko zamieszany był wewnętrzny konflikt Damiana - dziewczyna czy gra oraz czy jest nieudacznikiem. Pomogli mu znaleźć pracę i wciągnęli jego dziewczynę w granie. W ten sposób wyegzorcyzmowali duchy... znajdując pracę.

Aktor w Opowieści:

* Dokonanie:
    * młoda tsundere, studentka sieci i internetu. Uważa, że internet jest DZIWNY. Chce zniechęcić chłopaka (Damiana) do gry w Supreme Missionforce.
* Progresja:
    * polubiła Supreme Missionforce i z radością gra z Zaczęstwiakami - a zwłaszcza z Lią.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0110-10-09
    1. Primus    : 9, @: 0110-10-09
        1. Sektor Astoriański    : 9, @: 0110-10-09
            1. Astoria    : 9, @: 0110-10-09
                1. Sojusz Letejski    : 9, @: 0110-10-09
                    1. Szczeliniec    : 9, @: 0110-10-09
                        1. Powiat Jastrzębski    : 1, @: 0110-10-09
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-09
                                1. Blokhaus Widmo    : 1, @: 0110-10-09
                        1. Powiat Pustogorski    : 9, @: 0110-10-09
                            1. Podwiert    : 1, @: 0110-10-09
                            1. Pustogor    : 2, @: 0110-02-27
                                1. Barbakan    : 1, @: 0109-10-19
                                1. Miasteczko    : 1, @: 0110-02-27
                            1. Zaczęstwo    : 9, @: 0110-10-09
                                1. Akademia Magii, kampus    : 4, @: 0110-10-09
                                    1. Arena Treningowa    : 1, @: 0110-10-09
                                    1. Budynek Centralny    : 2, @: 0110-02-05
                                    1. Domek dyrektora    : 1, @: 0110-02-20
                                1. Cyberszkoła    : 6, @: 0110-02-27
                                1. Kwatera Terminusa    : 1, @: 0110-02-20
                                1. Nieużytki Staszka    : 2, @: 0109-10-19
                                1. Osiedle Ptasie    : 4, @: 0110-02-05
                                1. Sklep Komputrix    : 1, @: 0109-10-16
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-01-30
                            1. Głodna Ziemia    : 1, @: 0110-01-30
                            1. Laboratorium W Drzewie    : 1, @: 0110-01-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((181101-wojna-o-uczciwe-polfinaly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Tymon Grubosz        | 5 | ((181101-wojna-o-uczciwe-polfinaly; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Alan Bartozol        | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Marlena Maja Leszczyńska | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Minerwa Metalia      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy)) |
| Napoleon Bankierz    | 3 | ((190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 201006-dezinhibitor-dla-sabiny)) |
| Adela Kirys          | 2 | ((190113-chronmy-karoline-przed-uczniami; 190202-czarodziejka-z-woli-saitaera)) |
| Arnulf Poważny       | 2 | ((190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy)) |
| Damian Podpalnik     | 2 | ((181030-zaczestwiacy-czy-karolina; 181101-wojna-o-uczciwe-polfinaly)) |
| Diana Tevalier       | 2 | ((190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Saitaer              | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ignacy Myrczek       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kasjopea Maus        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Liliana Bankierz     | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Sabina Kazitan       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Talia Aegis          | 1 | ((190830-kto-wrobil-alana)) |
| Teresa Mieralit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |