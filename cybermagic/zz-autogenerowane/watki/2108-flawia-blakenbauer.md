# Flawia Blakenbauer
## Identyfikator

Id: 2108-flawia-blakenbauer

## Sekcja Opowieści

### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 8
* **daty:** 0112-03-28 - 0112-04-02
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * w wyniku Krwawej Emisji Eleny i działań simulacrum Martyna wyssało ją w kosmos z laboratorium przy K1. Jej los się na zawsze rozdzielił z Orbiterem.
* Progresja:
    * wyssana w kosmos. Nie zginęła, ale jej los i los Orbitera się na zawsze rozdzieliły.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 7
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * akcja infiltracyjna Kultu Esuriit z Eleną. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Elenie się uciec.
* Progresja:
    * TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 6
* **daty:** 0112-02-09 - 0112-02-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * uwiodła i przekonała Gilberta Blocha do przekazania Inferni tajnych danych sił specjalnych. Jest jej z tym źle.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 5
* **daty:** 0112-02-04 - 0112-02-07
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * dostała polecenie od Arianny by oczarować Blocha. Odmówiła, bo Orbiter nie ma intryg przeciw Orbiterowi. Arianna przykładami ją złamała do współpracy.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 4
* **daty:** 0112-01-27 - 0112-02-01
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * Elena kiedyś wpakowała ją w GIGANTYCZNE długi. Sowińscy ją wykupili i ona jest teraz infiltrating agent ("Bond"). Teraz miała zajmować się Rolandem, ale Roland chciał na Infernię, więc ona "zinfiltrowała" Infernię. Próbowała się wybronić lub uciec, ale nie była w stanie. Finalnie - dołączyła do Inferni pod pretekstem "infiltracji i szukania Anastazji".
* Progresja:
    * oddelegowana przez Anastazego Sowińskiego na Infernię jako, że "skutecznie ją zinfiltrowała a Infernia szuka Anastazji".
    * absolutna nienawiść do Eleny. Zemści się na niej. Zniszczy ją.


### Fecundatis w Domenie Barana

* **uid:** 210820-fecundatis-w-domenie-barana, _numer względny_: 3
* **daty:** 0109-03-06 - 0109-03-09
* **obecni:** Antonella Temaris, Antos Kuramin, Bruno Baran, Cień Brighton, Damian Szczugor, Deneb Ira, Elsa Kułak, Flawia Blakenbauer, Jolanta Sowińska, Kara Szamun, Leon Kantor, Rick Varias, Tamara Mardius

Streszczenie:

Fecundatis dotarł do Domeny Ukojenia w Pasie Kazimierza. Na miejscu - Blakvelowcy zmiażdżyli Mardiusowców; Strachy mają kolejną ofensywę i wszyscy się boją. Po przeanalizowaniu sytuacji Fecundatis zdecydował się na znalezienie i przejęcie miragenta, ale przeszkodziła im inwazja DUŻEJ ilości groźnych Strachów. Zespół skutecznie odparł Strachy (przy pewnych stratach) i wyniósł Antosa do roli lokalnego bohatera i jednoczyciela. Mieli jednak wsparcie Kuratorów... którzy m.in. Tamarę Mardius wpakowali już do Aleksandrii...

Aktor w Opowieści:

* Dokonanie:
    * zaczęła od flirtu zbierającego info co się dzieje w okolicy, potem psychotropowała Damiana i skończyła na tym, że magią (wspierana przez Antonellę i Esuriit) syntetyzowała skorpioidami pajęczynę do neutralizacji Strachów. Wymęczyło ją to, ale dała radę.
* Progresja:
    * zawsze nosi przy sobie psychotropy "na miło i wesoło" oraz środki unieszkodliwiające.


### Szmuglowanie Antonelli

* **uid:** 210813-szmuglowanie-antonelli, _numer względny_: 2
* **daty:** 0109-02-11 - 0109-02-23
* **obecni:** Antonella Temaris, Bruno Baran, Cień Brighton, Flawia Blakenbauer, Jolanta Sowińska, Lucjusz Blakenbauer, SC Fecundatis, Tomasz Sowiński

Streszczenie:

Antonella Temaris stała się problemem politycznym na linii Sowińscy - Nataniel Morlan. By nie została oddana, Tomasz, Jolanta i Flawia weszli we współpracę z Cieniem Brightonem, przemytnikiem. Przemycili Flawię na orbitę (Brighton skłonił Jolantę, by ta poleciała z nimi!), po czym zgubili ewentualny pościg na Valentinie. A drugą linią Flawia przekonała Lucjusza, by ten przygotował szpital terminuski w Pustogorze na zmianę Wzoru Antonelli, by ją naprawić...

Aktor w Opowieści:

* Dokonanie:
    * współpracując z Lucjuszem doprowadziła do tego, że Antonelli Pustogor naprawi Wzór. Wyflirtowała z sanepidowcem dostęp do wiewiórki i zintegrowała jej krew z krwią Antonelli, skutecznie gubiąc Morlana i jego łowców nagród.


### Porwanie na Gwiezdnym Motylu

* **uid:** 210810-porwanie-na-gwiezdnym-motylu, _numer względny_: 1
* **daty:** 0108-12-25 - 0108-12-30
* **obecni:** Antonella Temaris, Flawia Blakenbauer, Franek Kuparał, Jolanta Sowińska, Lena Fenatil, Nataniel Morlan, Renata Szarżun, SLX Gwiezdny Motyl, Tomasz Sowiński

Streszczenie:

Flawia Blakenbauer miała nadzieję, że Tomasz Sowiński pomoże jej z kłopotami, ale ów zniknął na Gwiezdnym Motylu. Flawia, Szmuglerka i Strażniczka skutecznie znalazły konspirację noktian którzy próbowali przehandlować Tomasza za innych noktian, oraz miragenta próbującego Tomasza usunąć. Do tego zaplątały się dookoła Ducha - eternijskiej szlachcianki uciekającej przed potężnym łowcą magów uważającym że jest jej ojcem. All in all, Tomasz odzyskany, łowca magów uśpiony a Duch uciekł.

Aktor w Opowieści:

* Dokonanie:
    * ma nadzieję, że Tomasz Sowiński pomoże jej w karierze/finansach, ale Tomasza porwano na Gwiezdnym Motylu. Przymiliła się do Jolanty Sowińskiej, zdobyła informacje od Nataniela Morlana i poświęciła reputację by chronić Ducha (Antonellę).
* Progresja:
    * poświęciła reputację i nawet trafiła do aresztu za dziką imprezę z ostrymi środkami psychoaktywnymi. Pikanteria - zrobiła to dla misji i by chronić Tomasza Sowińskiego.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0112-04-02
    1. Primus    : 8, @: 0112-04-02
        1. Sektor Astoriański    : 7, @: 0112-04-02
            1. Astoria, Orbita    : 2, @: 0112-04-02
                1. Kontroler Pierwszy    : 2, @: 0112-04-02
                    1. Hangary Alicantis    : 1, @: 0112-02-01
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-04-02
            1. Astoria    : 1, @: 0109-02-23
                1. Sojusz Letejski    : 1, @: 0109-02-23
                    1. Aurum    : 1, @: 0109-02-23
                        1. Imperium Sowińskich    : 1, @: 0109-02-23
                            1. Krystalitium    : 1, @: 0109-02-23
                                1. Klub Eksplozja    : 1, @: 0109-02-23
                                1. Pałac Świateł    : 1, @: 0109-02-23
                    1. Szczeliniec    : 1, @: 0109-02-23
                        1. Powiat Pustogorski    : 1, @: 0109-02-23
                            1. Pustogor    : 1, @: 0109-02-23
                                1. Rdzeń    : 1, @: 0109-02-23
                                    1. Szpital Terminuski    : 1, @: 0109-02-23
            1. Brama Kariańska    : 2, @: 0112-02-12
            1. Pas Teliriański    : 1, @: 0109-03-09
                1. Planetoidy Kazimierza    : 1, @: 0109-03-09
                    1. Domena Ukojenia    : 1, @: 0109-03-09
                        1. Planetoida Mirnas    : 1, @: 0109-03-09
                        1. Planetoida Talio    : 1, @: 0109-03-09
                        1. Stacja Ukojenie Barana    : 1, @: 0109-03-09
                        1. Szamunczak    : 1, @: 0109-03-09
                            1. Klub Korona    : 1, @: 0109-03-09
            1. Stacja Valentina    : 1, @: 0109-02-23
        1. Sektor Mevilig    : 1, @: 0112-03-26
            1. Planetoida Kalarfam    : 1, @: 0112-03-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Elena Verlen         | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Klaudia Stryk        | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Eustachy Korkoran    | 4 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia)) |
| Antonella Temaris    | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 3 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Martyn Hiwasser      | 3 | ((210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Bruno Baran          | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 2 | ((210813-szmuglowanie-antonelli; 210820-fecundatis-w-domenie-barana)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Kamil Lyraczek       | 2 | ((210616-nieudana-infiltracja-inferni; 211020-kurczakownia)) |
| Leona Astrienko      | 2 | ((210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej)) |
| Medea Sowińska       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Infernia          | 2 | ((210825-uszkodzona-brama-eteryczna; 211020-kurczakownia)) |
| Otto Azgorn          | 2 | ((211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Tomasz Sowiński      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Izabela Zarantel     | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Janus Krzak          | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Marian Tosen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Nataniel Morlan      | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Netrahina         | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Roland Sowiński      | 1 | ((210616-nieudana-infiltracja-inferni)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| TAI Rzieza d'K1      | 1 | ((211027-rzieza-niszczy-infernie)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Vigilus Mevilig      | 1 | ((211020-kurczakownia)) |