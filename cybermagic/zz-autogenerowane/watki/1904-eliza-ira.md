# Eliza Ira
## Identyfikator

Id: 1904-eliza-ira

## Sekcja Opowieści

### Dwa stare miragenty

* **uid:** 190521-dwa-stare-miragenty, _numer względny_: 11
* **daty:** 0111-07-26 - 0111-07-28
* **obecni:** Alojzy Wypyszcz, Eliza Ira, Fergus Salien, Fred Wypyszcz, Olga Leszcz

Streszczenie:

Pokój z Elizą Irą doprowadził do tego, że w okolicy pojawił się korespondent szukający swojego brata - podobno Eliza go zabiła. Tyle, że Eliza nic o tym nie wie. Zespół poszedł śladem nieszczęsnego brata i znaleźli dwa stare miragenty, które zostały skazane na cierpienie przez swojego poprzedniego dowódcę z Orbitera. Olga zwinęła bazę danych miragentów którą ów dowódca chciał zniszczyć 7 lat temu zanim miragenty zostały trwale i nieodwracalnie zniszczone. Ludzie zostali uratowani. Niestety, transportowiec (Tucznik Trzeci) został zestrzelony.

Aktor w Opowieści:

* Dokonanie:
    * nic nie wie o tym, by zniszczyła grupę archeologów. Pomogła Zespołowi tłumacząc zasady działania kryształów i krystalicznych struktur Noctis.


### Arcymag w Raju

* **uid:** 190326-arcymag-w-raju, _numer względny_: 10
* **daty:** 0111-07-18 - 0111-07-19
* **obecni:** Ataienne, Dawid Szardak, Eliza Ira, Fergus Salien, Grzegorz Kamczarnik, Olga Leszcz, OO Castigator

Streszczenie:

Eliza Ira zdecydowała się odzyskać swoich ludzi z czasów wojny przetrzymywanych w Trzecim Raju. Stworzyła Krystaliczną Plagę unieszkodliwiającą wpływ mindwarpującej AI, Ataienne. Jednak Fergus i Olga dali radę przekonać Elizę, że wojna się skończyła i ich wspólnym przeciwnikiem jest teraz Astoria. Arcymagini weszła w ostrożny sojusz z Orbiterem Pierwszym, odzyskując kilkanaście osób chcących z nią współpracować.

Aktor w Opowieści:

* Dokonanie:
    * zdecydowała się na uratowanie swoich ludzi albo śmierć. Skończyła w sojuszu z Orbiterem Pierwszym, by razem odzyskali jak najwięcej Astorii dla ludzkości.
* Progresja:
    * odzyskała kilkanaście osób chcących z nią współpracować. Dodatkowo, weszła w ostrożny sojusz z Orbiterem Pierwszym.
    * nienawiść wielu z jej dawnej frakcji do niej jest niesamowita. Tak samo jak nienawiść wielu z Orbitera. Nie jest niczyją ulubioną osobą.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 9
* **daty:** 0111-03-07 - 0111-03-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * przekonana przez Ariannę, że jeśli przejmie Trzeci Raj to nie ma szans na integrację noktian z astorianami. Zrezygnowała z aktualnej walki - ale będzie chronić Raj.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 8
* **daty:** 0111-03-02 - 0111-03-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * weszła do Trzeciego Raju po śmierci mindwarpowanych komandosów noktiańskich; zażądała oddania jej wszystkich noktian. Jak nie - wszyscy poza noktianami zginą.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 7
* **daty:** 0111-02-21 - 0111-02-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * osłabiła burzę magiczną, która spowodowałaby dużo większe zniszczenia w Ruinie Trzeciego Raju, acz tak, by nikt nie wiedział. Poluje na kompetentnych arcymagów.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 6
* **daty:** 0110-04-25 - 0110-04-26
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * mimo osłabionego połączenia robiła co była w stanie by móc pomóc seksbotowi, by ów mógł być wolny i nie musiał wracać do Ernesta. Utraciła połączenie przez Pięknotkę.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 5
* **daty:** 0110-04-10 - 0110-04-13
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * zakłada kulty i sieci komunikacyjne przez kryształy. Po akcji Zespołu, koszt używania tych kryształów i jej Matrycy znacząco wzrósł. Ale ma już jakieś kontakty.


### Spóźniona wojna Elizy

* **uid:** 190418-spozniona-wojna-elizy, _numer względny_: 4
* **daty:** 0110-03-20 - 0110-03-22
* **obecni:** Damian Drób, Eliza Ira, Robert Garwen

Streszczenie:

Po 10 latach od Inwazji z Noctis Eliza wyczuła, że Saitaer się rusza. Uderzyła w Trzeci Raj by odzyskać oddział. Jednak zasymilowane z Astorią siły Noctis stanęły w większości po stronie Astorii i przeciw Elizie. Udało się powstrzymać zamieszki a Zespół przekonał Elizę, że niekoniecznie tędy droga; nie trzeba niszczyć Astorii by uratować świat przed Saitaerem. Eliza uznała, że da im szansę. Zespół pomógł Astorianom ustabilizować sytuację, po czym Ataienne odebrała wszystkim - poza Zespołem - pamięć ponownie. Eliza powoli może odtajać z nienawiści.

Aktor w Opowieści:

* Dokonanie:
    * arcymagini kryształów, która odparła Saitaera i Ataienne krystalizując swą nienawiść. Po 10 latach uderzyła w Trzeci Raj by odzyskać swój oddział po uprzednim wyłączeniu z akcji Ataienne. Zespół ją odtajał i kupił szansę Trzeciemu Rajowi.
* Progresja:
    * arcymagini kryształów odtajała. Nie jest napędzana jedynie skrystalizowaną nienawiścią do Astorii i dawnej Elizy.


### Korporacyjna wojna w MMO

* **uid:** 190226-korporacyjna-wojna-w-mmo, _numer względny_: 3
* **daty:** 0110-03-14 - 0110-03-17
* **obecni:** Adrian Wężoskór, Alan Bartozol, Antoni Kotomin, Dariusz Bankierz, Eliza Kotlet, Kermit Szperacz, Mi Ruda, Rafał Królewski, Wojciech Słabizna

Streszczenie:

Dwie magiczne firmy - Sensus oraz Rexpapier - weszły w konflikt o Rosę Volant. Rozmowy pokojowe miały mieć miejsce w MMO. W wyniku tego Alan Bartozol znalazł aktualnego szefa EliSquid, "Sekatora Pierwszego", którego też zabanowali. EliSquid zostało przejęte przez niejaką "Iglicę" a rozmowy pokojowe się udały - wszyscy skupią się na Dariuszu Bankierzu i małym miasteczku na terenach Skażonych by nie pojawiła się większa konkurencja do rosy volant...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jako "Iglica" przejęła dowodzenie nad EliSquid. Kryształowy Pałac jest zagrożony jak nigdy ;-).


### Plaga jamników

* **uid:** 190313-plaga-jamnikow, _numer względny_: 2
* **daty:** 0110-02-28 - 0110-03-01
* **obecni:** Alsa Fryta, Eliza Ira, Olga Myszeczka, Tomek Żuchwiacz

Streszczenie:

By pozbyć się problemów z Czerwonymi Myszami raz na zawsze, Olga zrobiła Plagę Jamników (która przerodziła się w "Wyjący Zew"). Tymczasem archeolog zeszła w podziemia kopalni Terposzy i natrafiła na nieaktywne zabezpieczenia z czasów Wojny. Uruchomiła nową plagę i współpracując z Elizą Irą rozmontowała ją. Potem rozwiązali problem Olgi usuwając Myszy z tego terenu. Olga nie ma już kłopotów z tępiącymi ją magami.

Aktor w Opowieści:

* Dokonanie:
    * chciała zdobyć stare systemy bojowe. Zamiast tego dostała je zsabotowane plus dwóch górników (czego nie chciała). I jedno ciało martwego kompana.


### Lekarz dla Elizy

* **uid:** 200930-lekarz-dla-elizy, _numer względny_: 1
* **daty:** 0082-02-10 - 0082-02-14
* **obecni:** Autofort Imperatrix, Eliza Ira, Karmina Alarel, Szymon Szynek, Wanessa Pyszcz

Streszczenie:

Eliza Ira umiera w jaskini; jej nowe moce ją zabijają. Karmina i Wanessa przemierzają Skażone przez zniszczenie fizycznej formy Saitaera tereny prowadzące do Ortus-Conticium by porwać astoriańskiego medyka by jej pomógł. Udaje im się to, po drodze ratując noktiańskie siły porucznika Szynka (6 z 50 osób). Niestety, uległy silnemu Skażeniu w Ortus i zniszczyły astoriański oddział ewakuacyjny; przez to dostarczyły do astoriańskiego dowodzenia informacje, gdzie znajduje się baza Elizy.

Aktor w Opowieści:

* Dokonanie:
    * jeszcze nie arcymagini. Na wpół sparaliżowana, niweluje Skażenie i wątpi w swoje decyzje o nie niszczeniu Astorii. Pierwszy raz użyła magii by zamaskować ścigacz kryształami.
* Progresja:
    * dookoła niej jest bezpiecznie; anomalie się nie materializują. Jest neutralizatorem chorych energii.
    * silna zależność emocjonalna od Karminy i Wanessy. Nienawiść do siebie. Oraz - pragnienie ochrony noktian i zbudowanie dla nich bezpiecznego miejsca.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0111-07-28
    1. Primus    : 10, @: 0111-07-28
        1. Sektor Astoriański    : 10, @: 0111-07-28
            1. Astoria    : 10, @: 0111-07-28
                1. Sojusz Letejski, NW    : 7, @: 0111-07-28
                    1. Ruiniec    : 7, @: 0111-07-28
                        1. Kryształowa Forteca    : 1, @: 0111-07-19
                            1. Mauzoleum    : 1, @: 0111-07-19
                            1. Rajskie Peryferia    : 1, @: 0111-07-19
                        1. Ortus-Conticium, okolice    : 1, @: 0082-02-14
                            1. Rzeka Tirellia    : 1, @: 0082-02-14
                        1. Ortus-Conticium    : 2, @: 0111-02-25
                        1. Pustynia Kryształowa    : 3, @: 0111-07-28
                            1. Kryształowa Forteca    : 1, @: 0082-02-14
                        1. Trzeci Raj    : 6, @: 0111-07-28
                            1. Barbakan    : 1, @: 0110-03-22
                            1. Centrala Ataienne    : 1, @: 0111-07-19
                            1. Mały Kosmoport    : 1, @: 0110-03-22
                            1. Ratusz    : 1, @: 0110-03-22
                            1. Stacja Nadawcza    : 1, @: 0110-03-22
                1. Sojusz Letejski    : 4, @: 0111-03-10
                    1. Aurum    : 1, @: 0111-03-10
                        1. Powiat Niskowzgórza    : 1, @: 0111-03-10
                            1. Domena Arłacz    : 1, @: 0111-03-10
                                1. Posiadłość Arłacz    : 1, @: 0111-03-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-03-10
                                    1. Małe lądowisko    : 1, @: 0111-03-10
                    1. Szczeliniec    : 3, @: 0110-04-26
                        1. Powiat Pustogorski    : 3, @: 0110-04-26
                            1. Czarnopalec    : 1, @: 0110-03-01
                                1. Pusta Wieś    : 1, @: 0110-03-01
                            1. Podwiert    : 1, @: 0110-03-01
                                1. Kopalnia Terposzy    : 1, @: 0110-03-01
                            1. Pustogor    : 1, @: 0110-04-13
                                1. Eksterior    : 1, @: 0110-04-13
                                    1. Miasteczko    : 1, @: 0110-04-13
                                1. Interior    : 1, @: 0110-04-13
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-13
                                    1. Laboratorium Senetis    : 1, @: 0110-04-13
                                1. Rdzeń    : 1, @: 0110-04-13
                                    1. Szpital Terminuski    : 1, @: 0110-04-13
                            1. Zaczęstwo    : 1, @: 0110-04-26
                                1. Akademia Magii, kampus    : 1, @: 0110-04-26
                                    1. Budynek Centralny    : 1, @: 0110-04-26
                                        1. Skrzydło Loris    : 1, @: 0110-04-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Ataienne             | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Dariusz Krantak      | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Elena Verlen         | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Fergus Salien        | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| Izabela Zarantel     | 2 | ((200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Olga Leszcz          | 2 | ((190326-arcymag-w-raju; 190521-dwa-stare-miragenty)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Pięknotka Diakon     | 2 | ((190429-sabotaz-szeptow-elizy; 190519-uciekajacy-seksbot)) |
| Wanessa Pyszcz       | 2 | ((200930-lekarz-dla-elizy; 201021-noktianie-rodu-arlacz)) |
| Alan Bartozol        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Alojzy Wypyszcz      | 1 | ((190521-dwa-stare-miragenty)) |
| Alsa Fryta           | 1 | ((190313-plaga-jamnikow)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Autofort Imperatrix  | 1 | ((200930-lekarz-dla-elizy)) |
| Damian Drób          | 1 | ((190418-spozniona-wojna-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Fred Wypyszcz        | 1 | ((190521-dwa-stare-miragenty)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karmina Alarel       | 1 | ((200930-lekarz-dla-elizy)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Liliana Bankierz     | 1 | ((190519-uciekajacy-seksbot)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Minerwa Metalia      | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Olga Myszeczka       | 1 | ((190313-plaga-jamnikow)) |
| OO Castigator        | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((190418-spozniona-wojna-elizy)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Szynek        | 1 | ((200930-lekarz-dla-elizy)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Tomek Żuchwiacz      | 1 | ((190313-plaga-jamnikow)) |