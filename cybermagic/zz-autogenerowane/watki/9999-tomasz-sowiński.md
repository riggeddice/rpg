# Tomasz Sowiński
## Identyfikator

Id: 9999-tomasz-sowiński

## Sekcja Opowieści

### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 7
* **daty:** 0112-01-20 - 0112-01-24
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * chce uratować Ofelię i zwraca się z prośbą do Arianny Verlen. Podkochuje się w niej. Ma najgorsze poczucie spec ops w historii - zakłada dziwny strój i zwraca na siebie uwagę.
* Progresja:
    * podkochuje się w Ariannie Verlen. Uważa ją za mądrą i piękną. Nigdy jej tego nie powie.
    * wisi Ariannie Verlen spory dług wdzięczności za ratunek Ofelii.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 6
* **daty:** 0111-11-16 - 0111-11-19
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * chciał chronić kuzynkę przed Martynem (by ten jej nie uwiódł?). Uniemożliwił Martynowi uratowanie Jolanty, skończył przez Martyna unieszkodliwiony.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 5
* **daty:** 0111-11-03 - 0111-11-12
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * przerażony Arianną jak cholera, współpracuje z nią by nikt nie ucierpiał. Przekonał Mariusza Tubalona, by ten dał mu uprawnienia takie jak Jolancie.
* Progresja:
    * Opinia "Krwawy Lord Sowiński" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 4
* **daty:** 0111-10-18 - 0111-11-02
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * zorientował się, że z Jolantą jest coś bardzo poważnie nie tak - i przekazał tą wiadomość dyskretnie Ariannie (prowadząc do porwania Joli).


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 3
* **daty:** 0111-09-16 - 0111-10-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * udaje twardego i męskiego, ale to przerażony duży dzieciak skonfrontowany z wszechświatem. Zakochał się w Ariannie ("Monice"), bo jest pierwszą kobietą w jego życiu XD.
* Progresja:
    * odblokował przesyłkę dla Jolanty Sowińskiej. Będzie wiadomo, że to on. Solidny wpiernicz od Jolanty się będzie należał?
    * zakochał się w Ariannie jako "Monice", która uratowała go przed Leoną, dbała o niego i w ogóle była miła, inteligentna i kompetentna. I nie śmiała się z niego. Chce ją wyciągnąć.


### Szmuglowanie Antonelli

* **uid:** 210813-szmuglowanie-antonelli, _numer względny_: 2
* **daty:** 0109-02-11 - 0109-02-23
* **obecni:** Antonella Temaris, Bruno Baran, Cień Brighton, Flawia Blakenbauer, Jolanta Sowińska, Lucjusz Blakenbauer, SC Fecundatis, Tomasz Sowiński

Streszczenie:

Antonella Temaris stała się problemem politycznym na linii Sowińscy - Nataniel Morlan. By nie została oddana, Tomasz, Jolanta i Flawia weszli we współpracę z Cieniem Brightonem, przemytnikiem. Przemycili Flawię na orbitę (Brighton skłonił Jolantę, by ta poleciała z nimi!), po czym zgubili ewentualny pościg na Valentinie. A drugą linią Flawia przekonała Lucjusza, by ten przygotował szpital terminuski w Pustogorze na zmianę Wzoru Antonelli, by ją naprawić...

Aktor w Opowieści:

* Dokonanie:
    * twórca najgorszych planów ratowania Antonelli w historii, ale jego gorące serce zapaliło Jolantę do pomocy i przekonał do tego też Cienia. Dostarcza surowców Lucjuszowi Blakenbauerowi by zmienić Wzór Antonelli.


### Porwanie na Gwiezdnym Motylu

* **uid:** 210810-porwanie-na-gwiezdnym-motylu, _numer względny_: 1
* **daty:** 0108-12-25 - 0108-12-30
* **obecni:** Antonella Temaris, Flawia Blakenbauer, Franek Kuparał, Jolanta Sowińska, Lena Fenatil, Nataniel Morlan, Renata Szarżun, SLX Gwiezdny Motyl, Tomasz Sowiński

Streszczenie:

Flawia Blakenbauer miała nadzieję, że Tomasz Sowiński pomoże jej z kłopotami, ale ów zniknął na Gwiezdnym Motylu. Flawia, Szmuglerka i Strażniczka skutecznie znalazły konspirację noktian którzy próbowali przehandlować Tomasza za innych noktian, oraz miragenta próbującego Tomasza usunąć. Do tego zaplątały się dookoła Ducha - eternijskiej szlachcianki uciekającej przed potężnym łowcą magów uważającym że jest jej ojcem. All in all, Tomasz odzyskany, łowca magów uśpiony a Duch uciekł.

Aktor w Opowieści:

* Dokonanie:
    * żądny przygód młody mag o dobrym sercu; poszedł z niewłaściwą dziewczyną na stronę i został porwany przez miragenta. Pomógł Flawii i Duchowi (Antonelli) wyjść z trudnych sytuacji życiowych.
* Progresja:
    * zaprzyjaźnił się z Flawią Blakenbauer i Antonellą Tamaris.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0112-01-24
    1. Primus    : 7, @: 0112-01-24
        1. Sektor Astoriański    : 7, @: 0112-01-24
            1. Astoria, Orbita    : 3, @: 0112-01-24
                1. Kontroler Pierwszy    : 3, @: 0112-01-24
                    1. Sektor 57    : 1, @: 0112-01-24
                        1. Mordownia    : 1, @: 0112-01-24
            1. Astoria    : 1, @: 0109-02-23
                1. Sojusz Letejski    : 1, @: 0109-02-23
                    1. Aurum    : 1, @: 0109-02-23
                        1. Imperium Sowińskich    : 1, @: 0109-02-23
                            1. Krystalitium    : 1, @: 0109-02-23
                                1. Klub Eksplozja    : 1, @: 0109-02-23
                                1. Pałac Świateł    : 1, @: 0109-02-23
                    1. Szczeliniec    : 1, @: 0109-02-23
                        1. Powiat Pustogorski    : 1, @: 0109-02-23
                            1. Pustogor    : 1, @: 0109-02-23
                                1. Rdzeń    : 1, @: 0109-02-23
                                    1. Szpital Terminuski    : 1, @: 0109-02-23
            1. Pas Teliriański    : 3, @: 0111-11-12
                1. Planetoidy Kazimierza    : 3, @: 0111-11-12
                    1. Planetoida Asimear    : 3, @: 0111-11-12
                        1. Stacja Lazarin    : 2, @: 0111-11-12
            1. Stacja Valentina    : 1, @: 0109-02-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Eustachy Korkoran    | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Jolanta Sowińska     | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Klaudia Stryk        | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Elena Verlen         | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Leona Astrienko      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Martyn Hiwasser      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Antonella Temaris    | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Antoni Kramer        | 2 | ((210218-infernia-jako-goldarion; 210818-siostrzenica-morlana)) |
| Flawia Blakenbauer   | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210813-szmuglowanie-antonelli)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Nataniel Morlan      | 2 | ((210810-porwanie-na-gwiezdnym-motylu; 210818-siostrzenica-morlana)) |
| SC Fecundatis        | 2 | ((210813-szmuglowanie-antonelli; 210818-siostrzenica-morlana)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Bruno Baran          | 1 | ((210813-szmuglowanie-antonelli)) |
| Cień Brighton        | 1 | ((210813-szmuglowanie-antonelli)) |
| Franek Kuparał       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Kamil Lyraczek       | 1 | ((210414-dekralotyzacja-asimear)) |
| Lena Fenatil         | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lucjusz Blakenbauer  | 1 | ((210813-szmuglowanie-antonelli)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Infernia          | 1 | ((210218-infernia-jako-goldarion)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| Otto Azgorn          | 1 | ((210421-znudzona-zaloga-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Renata Szarżun       | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SLX Gwiezdny Motyl   | 1 | ((210810-porwanie-na-gwiezdnym-motylu)) |