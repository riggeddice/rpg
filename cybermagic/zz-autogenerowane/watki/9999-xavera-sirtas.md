# Xavera Sirtas
## Identyfikator

Id: 9999-xavera-sirtas

## Sekcja Opowieści

### Legenda o noktiańskiem mafii

* **uid:** 230920-legenda-o-noktianskiej-mafii, _numer względny_: 6
* **daty:** 0081-07-10 - 0081-07-13
* **obecni:** Amanda Kajrat, Bogdan Gwiazdocisz, Dmitri Karpov, Irelia Kairanolis, Patryk Majwuron, Petra Karpov, Xavera Sirtas

Streszczenie:

Furie z Karpovami opracowywały infiltrację i jak się zachowywać jako lokalsi. Jednak pewnej nocy pojawili się Grzymościowcy by torturować Karpovów (bez sensu). Furie jako Czerwone Myszy spróbowały porwać ścigacze - nie wyszło. Skończyło się na walce (masakrze), gdzie jako NOWA mafia, noktiańska, odparły Grzymościowców (i dowiedziały się, że jest tu inna Furia, Irelia, mindwarpowana). Po dotarciu do Zaczęstwa przez Podwiert Furie wpakowały się na gang porywający młode kobiety (który ich nie porwał) i schowały się w Przytułku. Nie wiedzą o tym, ale gang na nie dalej poluje, nie wiedząc kim są...

Aktor w Opowieści:

* Dokonanie:
    * gdy nie udało się cicho ukraść ścigacza, przechwyciła i złamała Patryka. Korzystając, że łowcy dziewczyn skupiają się na Amandzie, skrzywdziła faceta i się oderwały. Dotarła do Zaczęstwa.


### Operacja: spotkać się z Dmitrim

* **uid:** 230913-operacja-spotkac-sie-z-dmitrim, _numer względny_: 5
* **daty:** 0081-06-28 - 0081-06-30
* **obecni:** Amanda Kajrat, Arnulf Poważny, Dmitri Karpov, Petra Karpov, Xavera Sirtas

Streszczenie:

Furie po przejściu przez Mur nawiązały kontakt z Dmitrim (a to był Arnulf). Zostawiły sygnał, ale też zastawiły pułapkę - i skutecznie ominęły 5 servarów, acz się obie pochorowały i zapłaciły zdrowiem. Wyślizgnąwszy się z pułapki, napotkały Dmitriego i dotarły do jego fortifarmy, gdzie poznały też jego żonę, Petrę.

Aktor w Opowieści:

* Dokonanie:
    * ściągnęła żukowca i dała znać 'Dmitriemu' gdzie mniej więcej są; gdy już polowały na nie Lancery to ściągnęła potwora odpowiednimi dźwiękami. Gdy napotkały Dmitriego, ona była centrum dyplomacji ;-).


### Operacja: mag dla Symlotosu

* **uid:** 230906-operacja-mag-dla-symlotosu, _numer względny_: 4
* **daty:** 0081-06-26 - 0081-06-28
* **obecni:** Amanda Kajrat, Caelia Calaris, Dragan Halatis, Ernest Kajrat, Leon Varkas, Lestral Kirmanik, Xavera Sirtas

Streszczenie:

Kajrat odkrył, że to Wolny Uśmiech stoi za porwaniem Furii. Wysyła Xaverę i Amandę do Zaczęstwa, by pozyskały maga i zrobiły kanał przerzutowy. Furiom w Quispisie udało się dotrzeć do Wielkiego Muru Pustogorskiego i go przekroczyć, acz skończyły bez servarów. Mają jednak po drugiej stronie ukryte zapasy oraz kontakt do agenta po stronie Szczelińca...

Aktor w Opowieści:

* Dokonanie:
    * wysłana przez Kajrata do Zaczęstwa; żartownisia i flirciara, dokucza Amandzie. Szybko wymanewrowała gąsienicostwora i wymyśliła plotkę o orgiach Furii.


### Skażone schronienie w Fortecy

* **uid:** 230730-skazone-schronienie-w-fortecy, _numer względny_: 3
* **daty:** 0081-06-18 - 0081-06-21
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Isaura Velaska, Leira Euridis, Raab Navan, Xavera Sirtas

Streszczenie:

Po rozbiciu awiana Furie nie mają już zasobów ani możliwości. Mimo, że Czarne Czaszki na nie polowały, uciekły do Fortecy Symlotosu (nie wiedząc co to jest). Tam dostały pomoc i spotkały Leirę (kolejną Furię) która zaakceptowała Symlotos. Furie dzielnie broniły się przed Skażeniem, skupiając się na pomocy Symlotosowi i zwalczaniem Czaszek. Niestety, operacja zwabiania Czaszek w pułapkę skończyła się ciężką raną Xavery. Ale Kajrat wspierany przez kolejną Furię, Isaurę, pozyskał Amandę i Xaverę, po czym otworzył negocjacje z Symlotosem by odzyskać Leirę i Aynę za maga.

Aktor w Opowieści:

* Dokonanie:
    * ranna, ale przez zaciśnięte zęby prze do przodu. Zauważyła, że ludzie na obszarze Symlotosu nie zachowują się naturalnie, coś ich zmieniło. Skończyła ciężko ranna i w sztucznej śpiączce.
* Progresja:
    * bardzo ciężko ranna, wprowadzona w śpiączkę przez medyków Kajrata; za dwa miesiące będzie w stanie operacyjnym przy aktualnym poziomie technologicznym w obszarze Enklaw


### Furia poluje na Furie

* **uid:** 230729-furia-poluje-na-furie, _numer względny_: 2
* **daty:** 0081-06-17 - 0081-06-18
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Ralena Karimin, Xavera Sirtas

Streszczenie:

Servary przestają działać. Amanda pozyskała stash od Kajrata z gniazda latających jaszczurów, acz kosztem swojego servara. Jedyny sprawny - Xavery - został zmodowany jako jednostka transportowa. Niestety, podpalenie gniazda jaszczurów sprawiło, że Ralena - kontrolowana przez KOGOŚ Furia - zaatakowała siłą 8 servarów i 2 awianów. Zespół dał radę zdjąć 1 awiana a drugiego porwać i jakkolwiek wszystkie Furie są zatrute, Xavera ranna, Ayna bardzo ciężko ranna, ale mają kilkanaście kilometrów przewagi nad atakującymi. Furie dzielnie się bronią, ale zaczynają przegrywać...

Aktor w Opowieści:

* Dokonanie:
    * najlepsza w walce z trzech Furii; wyciągnęła Amandę z gniazda jaszczuroskrzydeł podpalając je, oddała Amandzie część energii i granatem uszkodziła servar wroga. Gdy Aynie groziło porwanie, zderzyła awianem w porywającą Aynę Ralenę by przechwycić Aynę i wiać
* Progresja:
    * ranna acz funkcjonalna i zatruta przez straszne korzenie w okolicach Enklaw; ma personal armour i sprzęt brawlera


### Crashlanding Furii Mataris

* **uid:** 230723-crashlanding-furii-mataris, _numer względny_: 1
* **daty:** 0081-06-15 - 0081-06-17
* **obecni:** Amanda Kajrat, Ayna Marialin, Ernest Kajrat, Lucia Veidril, Xavera Sirtas

Streszczenie:

Noktiańska jednostka przewożąca Furie Mataris została zestrzelona i część Furii spadło w okolice Pustogoru. Astorianie polują na Furie, Kajrat je ostrzega i formuje swoje Nocne Niebo. Amanda odzyskała kontakt z dwoma innymi Furiami i przechodzą przez tereny na południu Pustogoru, próbując dostać się do Pacyfiki. Tam czeka na nie Kajrat. A po drodze - rozbity dziwny noktiański statek, polujący na Furie itp.

Aktor w Opowieści:

* Dokonanie:
    * fierce and loyal; najlepsza wojowniczka Furii Mataris w okolicy. Ma dość agresywne plany - chce się zakraść, ustrzelić itp. Ale daje się przekonać, że to nie jest najlepszy pomysł. Opiekuje się Ayną.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0081-07-13
    1. Primus    : 6, @: 0081-07-13
        1. Sektor Astoriański    : 6, @: 0081-07-13
            1. Astoria    : 6, @: 0081-07-13
                1. Sojusz Letejski, SW    : 4, @: 0081-06-28
                    1. Granica Anomalii    : 4, @: 0081-06-28
                        1. Forteca Symlotosu    : 1, @: 0081-06-21
                        1. Las Pusty, okolice    : 1, @: 0081-06-17
                        1. Ruina miasteczka Kalterweiser    : 1, @: 0081-06-28
                        1. Ruiny Kaliritosa    : 1, @: 0081-06-17
                    1. Wielki Mur Pustogorski    : 1, @: 0081-06-28
                1. Sojusz Letejski    : 2, @: 0081-07-13
                    1. Szczeliniec    : 2, @: 0081-07-13
                        1. Powiat Pustogorski    : 2, @: 0081-07-13
                            1. Powiat Przymurski    : 2, @: 0081-07-13
                                1. Fortifarma Karpovska    : 2, @: 0081-07-13
                                1. Las Przymurski    : 1, @: 0081-06-30
                            1. Zaczęstwo, obrzeża    : 1, @: 0081-07-13
                                1. Przytułek Cicha Gwiazdka    : 1, @: 0081-07-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amanda Kajrat        | 6 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu; 230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Ernest Kajrat        | 4 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy; 230906-operacja-mag-dla-symlotosu)) |
| Ayna Marialin        | 3 | ((230723-crashlanding-furii-mataris; 230729-furia-poluje-na-furie; 230730-skazone-schronienie-w-fortecy)) |
| Dmitri Karpov        | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Petra Karpov         | 2 | ((230913-operacja-spotkac-sie-z-dmitrim; 230920-legenda-o-noktianskiej-mafii)) |
| Arnulf Poważny       | 1 | ((230913-operacja-spotkac-sie-z-dmitrim)) |
| Bogdan Gwiazdocisz   | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Caelia Calaris       | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Dragan Halatis       | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Irelia Kairanolis    | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Isaura Velaska       | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leira Euridis        | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Leon Varkas          | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Lestral Kirmanik     | 1 | ((230906-operacja-mag-dla-symlotosu)) |
| Lucia Veidril        | 1 | ((230723-crashlanding-furii-mataris)) |
| Patryk Majwuron      | 1 | ((230920-legenda-o-noktianskiej-mafii)) |
| Raab Navan           | 1 | ((230730-skazone-schronienie-w-fortecy)) |
| Ralena Karimin       | 1 | ((230729-furia-poluje-na-furie)) |