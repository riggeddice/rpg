# OO Castigator
## Identyfikator

Id: 9999-oo-castigator

## Sekcja Opowieści

### Arcymag w Raju

* **uid:** 190326-arcymag-w-raju, _numer względny_: 5
* **daty:** 0111-07-18 - 0111-07-19
* **obecni:** Ataienne, Dawid Szardak, Eliza Ira, Fergus Salien, Grzegorz Kamczarnik, Olga Leszcz, OO Castigator

Streszczenie:

Eliza Ira zdecydowała się odzyskać swoich ludzi z czasów wojny przetrzymywanych w Trzecim Raju. Stworzyła Krystaliczną Plagę unieszkodliwiającą wpływ mindwarpującej AI, Ataienne. Jednak Fergus i Olga dali radę przekonać Elizę, że wojna się skończyła i ich wspólnym przeciwnikiem jest teraz Astoria. Arcymagini weszła w ostrożny sojusz z Orbiterem Pierwszym, odzyskując kilkanaście osób chcących z nią współpracować.

Aktor w Opowieści:

* Dokonanie:
    * superciężka artyleria Orbitera gotowa do zestrzelenia na planecie Elizy Iry jeśli do niczego nie dojdzie w osi Trzeci Raj - Eliza Ira.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 4
* **daty:** 0111-01-14 - 0111-01-20
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * Kramer załatwił uprawnienia, Arianna przyzwała Kryptę, anihilator Castigatora wypalił Nocną Kryptę do zera, niszcząc zagrożenie Esuriit.


### Śpiew NieLalki na Castigatorze

* **uid:** 231025-spiew-nielalki-na-castigatorze, _numer względny_: 3
* **daty:** 0110-10-24 - 0110-10-26
* **obecni:** Anna Tessalon, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Igor Arłacz, Klaudia Stryk, Konstanty Keksik, Leona Astrienko, Leszek Kurzmin, Marta Keksik, OO Castigator, OO Infernia, Patryk Samszar, Raoul Lavanis, TAI Eszara d'Castigator

Streszczenie:

Na pokład Castigatora tieni przemycili NieLalkę Altarient i zaczęły mieszać się Znaczenia oraz Rzeczywistość. Zespół doprowadził do kontrolowanego opanowania i wygaszania Castigatora, po czym po zlokalizowaniu Anomalii Altarient Raoul został wysłany by ją zaizolować. Infernia ją zestrzeliła. A Arianna wykorzystała tą okazję by młodzi tieni wzięli się w garść i nie robili głupot. Szczęśliwie, Admiralicja nic nie wie, acz było nieciekawie przez moment.

Aktor w Opowieści:

* Dokonanie:
    * przez problemy z Saitaerem i awarię Memoriam doszło do manifestacji Altarient przez przemycony potężny artefakt źródłowy, 'NieLalkę', zmieniającą znaczenia, percepcję i rzeczywistość. Szczęśliwie udało się wszystko zachować 'w środku', by we flocie nikt niczego nie zauważył (przynajmniej, nie oficjalnie).


### Ekstaflos na Tezifeng

* **uid:** 231011-ekstaflos-na-tezifeng, _numer względny_: 2
* **daty:** 0110-10-20 - 0110-10-22
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudiusz Terienak, Lars Kidironus, Leona Astrienko, Leszek Kurzmin, Natalia Gwozdnik, OO Castigator, OO Infernia, OO Tezifeng, Raoul Lavanis

Streszczenie:

Jeszcze na Castigatorze, Arianna i Kurzmin z przyjemnością odbudowali znajomość i wspominki. Gdy Elena chcąc ratować Leonę 'zabiła' jej anomalię, Leona wyzwała ją na pojedynek. Elena przegrała, choć było blisko; będzie musiała być służką Leony przez pewien czas, co Castigator odnotował z radością. Kurzmin poprosił Ariannę o znalezienie i pomoc dwóm tienom w rutynowym patrolu (najpewniej przekroczyli paliwo). Niestety, Tezifeng, korweta Castigatora podczas operacji celnych natrafiła na coś kralotycznego. Klaudiusz Terienak został zmieniony w ekstaflos a Natalia Gwozdnik została porwana. Pułapka na Infernię została wykryta przez Ariannę (bo Klaudiusz zachowywał się nie tak jak powinien). Szybka operacja wejścia - wydobycia jednego jeńca (marionetki kralotycznej) i zbadanie sytuacji sprawiła, że Eustachy działkami unieszkodliwił Tezifeng i Arianna wezwała wsparcie - kraloth to coś bardzo wysokiej klasy.

Aktor w Opowieści:

* Dokonanie:
    * uszkodzony zewnętrznie i strukturalnie, lekko Skażony, ale ogólnie działa bez zarzutu. Centrum dowodzenia Kurzmina i tymczasowa platforma szkoleniowa...


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 1
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * potężny statek artyleryjski na orbicie Astorii. Opanowany przez naleśniczych arystokratów z Aurum. Po wizycie Arianny (i Leony) - wrócił do formy. Uszkodzony przez Rozalię (tak naprawdę Eustachego), po tym jak Wiktor Satarail Skaził ixionem Azalię d'Alkaris.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-07-19
    1. Primus    : 5, @: 0111-07-19
        1. Sektor Astoriański    : 5, @: 0111-07-19
            1. Astoria, Orbita    : 3, @: 0110-10-26
                1. Kontroler Pierwszy    : 1, @: 0110-10-19
                    1. Akademia Orbitera    : 1, @: 0110-10-19
            1. Astoria    : 1, @: 0111-07-19
                1. Sojusz Letejski, NW    : 1, @: 0111-07-19
                    1. Ruiniec    : 1, @: 0111-07-19
                        1. Kryształowa Forteca    : 1, @: 0111-07-19
                            1. Mauzoleum    : 1, @: 0111-07-19
                            1. Rajskie Peryferia    : 1, @: 0111-07-19
                        1. Trzeci Raj    : 1, @: 0111-07-19
                            1. Centrala Ataienne    : 1, @: 0111-07-19
            1. Libracja Lirańska    : 1, @: 0111-01-20
        1. Sektor Lacarin    : 1, @: 0111-01-20

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Eustachy Korkoran    | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Leona Astrienko      | 4 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Klaudia Stryk        | 3 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 231025-spiew-nielalki-na-castigatorze)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Elena Verlen         | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| OO Infernia          | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| Raoul Lavanis        | 2 | ((231011-ekstaflos-na-tezifeng; 231025-spiew-nielalki-na-castigatorze)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Anna Tessalon        | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Damian Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Eliza Ira            | 1 | ((190326-arcymag-w-raju)) |
| Feliks Walrond       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Igor Arłacz          | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudiusz Terienak   | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Konstanty Keksik     | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Lars Kidironus       | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Marta Keksik         | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Natalia Gwozdnik     | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Tezifeng          | 1 | ((231011-ekstaflos-na-tezifeng)) |
| Patryk Samszar       | 1 | ((231025-spiew-nielalki-na-castigatorze)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| TAI Eszara d'Castigator | 1 | ((231025-spiew-nielalki-na-castigatorze)) |