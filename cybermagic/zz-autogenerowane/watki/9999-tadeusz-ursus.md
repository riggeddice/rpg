# Tadeusz Ursus
## Identyfikator

Id: 9999-tadeusz-ursus

## Sekcja Opowieści

### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 5
* **daty:** 0111-11-22 - 0111-11-23
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * posiadacz statku "Piękna Elena". Prosi Eustachego, by ten nauczył go podrywać. Prosi Ariannę o pomoc dla OE Falołamacz i przekazuje jej wszystko co wie na temat Falołamacza (nawet czego nie powinien)


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 4
* **daty:** 0111-02-05 - 0111-02-11
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * jeszcze ranny i leży w łóżku, ale już wziął na siebie organizację buntu by osłonić Ariannę. Jest honorowy - chce jej pomóc, bo ona pomogła jemu.
* Progresja:
    * zgromadził na sobie nienawiść buntowników z Inferni i Orbitera, bo "jest prowodyrem" (wziął na siebie by czyścić Ariannę).
    * sytuacja beznadziejna; pokazał słabość i arystokraci Eterni są zdecydowani go zniszczyć. Bez sojusznika (Arianny?) nie ma szans.
    * delikatny sojusz i cień przyjaźni z Arianną Verlen.


### Gdy Arianna nie patrzy

* **uid:** 200822-gdy-arianna-nie-patrzy, _numer względny_: 3
* **daty:** 0111-02-02 - 0111-02-04
* **obecni:** Klaudia Stryk, Martyn Hiwasser, Sylwia Milarcz, Tadeusz Ursus

Streszczenie:

Klaudia i Martyn mieli dość tego, że nie wiedzą nic o tematach związanych z Esuriit a ostatnio mają z tym ciągle do czynienia. Podczas dojścia do tego jaką dawkę dostał Tadeusz gdy stworzył Simulacrum przypadkowo odkryli konspirację - lekarz Tadeusza próbował go (długoterminowo) zniszczyć. Po drodze reputacja Arianny ucierpiała a Martyn trafił do aresztu za okrutne użycie magii wobec thugów. Tadeusz jest jednak bardzo wdzięczny Ariannie która o niczym nie wie :-).

Aktor w Opowieści:

* Dokonanie:
    * arystokrata kiedyś Aurum, teraz Eterni. Niezbyt bystry acz potężny; nie rozumie co Klaudia próbuje mu pokazać odnośnie jego Skażenia. Jego własny lekarz zatruwał go Esuriit, by Tadeusz upadł.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 2
* **daty:** 0111-01-24 - 0111-02-01
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * podpuszczony przez Klaudię a potem Ariannę wszedł z Leoną na pojedynek o Elenę.
* Progresja:
    * niby champion Eterni, ale przegrał z Leoną. Eternia jest na niego zła - przez swoją chuć i słabość ośmieszył ich wszystkich.
    * odczepi się od Eleny Verlen. Elena nie będzie już jego żoną. Przegrał walkę z Leoną.
    * jego bezpośrednią a-tien jest Sabina Servatel. Ona jest gdzieś w kosmosie, ale nie wiadomo dokładnie gdzie (on wie).


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 1
* **daty:** 0111-01-10 - 0111-01-13
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * eternijski szlachcic i oficer Orbitera na Kontrolerze Pierwszym. Uważa się za męża Eleny. Elena go nie cierpi. Złapał Eustachego by wyjaśnić "nie dobiera się do Eleny" ale przyszła Leona. Teraz wierzy, że ELENA jest flirciarą i chroni reputację Arianny przeciw Elenie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-11-23
    1. Primus    : 5, @: 0111-11-23
        1. Sektor Astoriański    : 5, @: 0111-11-23
            1. Astoria, Orbita    : 4, @: 0111-02-11
                1. Kontroler Pierwszy    : 4, @: 0111-02-11
                    1. Arena Kalaternijska    : 1, @: 0111-02-01
                    1. Hangary Alicantis    : 1, @: 0111-01-13

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Eustachy Korkoran    | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Klaudia Stryk        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Martyn Hiwasser      | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Elena Verlen         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210428-infekcja-serenit)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin)) |
| Antoni Kramer        | 2 | ((200722-wielki-kosmiczny-romans; 200826-nienawisc-do-swin)) |
| Damian Orion         | 2 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa)) |
| AK Serenit           | 1 | ((210428-infekcja-serenit)) |
| Aleksandra Termia    | 1 | ((200826-nienawisc-do-swin)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kamil Lyraczek       | 1 | ((200826-nienawisc-do-swin)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| OE Falołamacz        | 1 | ((210428-infekcja-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Infernia          | 1 | ((210428-infekcja-serenit)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |