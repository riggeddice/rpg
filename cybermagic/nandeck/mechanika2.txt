[conflict]=1-9
[character]=10-18
FONT = Arial, 8, B, #000000
TEXT = [conflict],"TYPOWY", 3%, 15%, 100%, 15%, right, bottom, 90
TEXT = [conflict],"TRUDNY", 3%, 35%, 100%, 20%, left, bottom,90
TEXT = [conflict],"EKSTREMALNY", 3%, 52%, 100%, 25%, left, bottom,90
TEXT = [conflict],"HEROICZNY", 3%, 80%, 100%, 20%, center, bottom,90

FONT = Arial, 22, B, #000000

TEXT = [conflict],"5", 60%, 10%, 30%, 20%, right, bottom
TEXT = [conflict],"8", 60%, 28%, 30%, 20%, right, bottom
TEXT = [conflict],"11", 60%, 48%, 30%, 20%, right, bottom
TEXT = [conflict],"15", 60%, 75%, 30%, 20%, right, bottom

TEXT = [conflict],"0", 8%, 10%, 40%, 20%, right, bottom
TEXT = [conflict],"-3", 8%, 28%, 45%, 20%, right, bottom
TEXT = [conflict],"-6", 8%, 48%, 45%, 20%, right, bottom
TEXT = [conflict],"-11", 8%, 75%, 50%, 20%, right, bottom

IMAGE = [conflict],"success.png",40%,3%, 10%, 10%, 0, PN
IMAGE = [conflict],"failure.png",80%,3%, 10%, 10%, 0, PN

LINE = [conflict],0,15%,100%,15%,#000000,0.1
LINE = [conflict],0,30%,100%,30%,#000000,0.1
LINE = [conflict],0,50%,100%,50%,#000000,0.1
LINE = [conflict],0,80%,100%,80%,#000000,0.1
LINE = [conflict],30%,0%,30%,100%,#000000,0.1
LINE = [conflict],70%,0%,70%,100%,#000000,0.1

FONT = Arial, 10, B, #000000
TEXT = [character],"Co robi? (3)", 5%, 10%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Kim posta� jest? Co potrafi? Czym si� zajmuje? Jak j� widzisz?", 5%, 15%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Czego chce? (3)", 5%, 25%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Czego posta� chce? Przeciwko czemu stoi?", 5%, 30%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Jak dzia�a? (3)", 5%, 40%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Jak osi�ga sukcesy? Czego NIE zrobi?", 5%, 45%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Zasoby (3)", 5%, 55%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Do czego ma dost�p? Jaki sprz�t jest dla niej charaketerystyczny?", 5%, 60%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [character],"Magia (3)", 5%, 72%, 100%, 100%, left, top
FONT = Arial, 8, N, #000000
TEXT = [character],"Najcz�ciej wspiera dzia�ania postaci. Dzia�a w zgodzie z pragnieniami i wol�, w oparciu o wiedz�.", 5%, 77%, 95%, 100%, left, wordwrap