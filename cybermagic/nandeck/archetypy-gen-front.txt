CARDSIZE = 6, 9

LINK="archetypy-profil-gen-front.txt"
[all]=1-{(Nazwa)}
FONT = Arial, 12, B, #000000
TEXT = [all],[Nazwa], 1%, 0%, 100%, 100%, center, top

FONT = Arial, 10, B, #000000
TEXT = [all],"Co robi?", 3%, 7%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Pomysl], 5%, 12%, 90%, 95%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Czego chce?", 3%, 25%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Motywacja], 5%, 30%, 90%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Jak dzia�a?", 3%, 50%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Dzialanie], 5%, 55%, 95%, 100%, left, wordwrap
FONT = Arial, 10, B, #000000
TEXT = [all],"Zasoby", 3%, 75%, 100%, 100%, left, top
FONT = Arial, 9, N, #000000
TEXT = [all],[Zasoby], 5%, 80%, 95%, 100%, left, wordwrap