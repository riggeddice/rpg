## Metadane

* factions: "lemurczak"
* owner: "public"
* title: "Vanessa Lemurczak"

## Mechanika

* **Obietnica**
    * "Będziesz potworem - femme fatale z dziesiątkami ostrzy z tkanin, piękną i niebezpieczną. Ale raz dane przez Ciebie słowo będzie geasem."
    * "Będą się ciebie bać i uważać na każde słowo. Będziesz mieć niewolników. Będziesz efektowna i mordercza."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY walczy ostrzami, TO robi nieprawdopodobny spektakl. Całe jej ciało, ubranie, broń zmieniają ją w niepowstrzymanego, pięknego potwora. _Niezwykle piękna i niezwykle przerażająca. Jest bardziej potworem niż dziewczyną._
        * GDY używa swego ciała, TO jest przerażająco szybka i precyzyjna. Nadludzka prędkość drakolickich mięśni. _Najszybsza i niezwykle silna, walczyła z servarem a jej suknia to były ostrza..._
        * GDY ma kogoś w swoich rękach, TO złamie ciało, umysł, serce i duszę. Królowa tortur. _Pamiętaj - ostatnia kulka jest dla Ciebie. Nie wpadnij w jej szpony._
    * **Kompetencje (3-5)**
        * GDY jest w pobliżu, TO wszystkie oczy ma na sobie. Piękna, elegancka i okrutna. Można się zakochać bez pamięci. _Lekko ubrana, z pokorną świtą, zdecydowana i dumna. Zawsze gotowa do walki._
        * GDY ma czas i materiały, TO z przyjemnością tworzy kolejne suknie i piękne ubrania. _Zrobione przez nią stroje są przepiękne, ma zmysł estetyki... ale uwierz, nie stać Cię na nie._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE wolności od ograniczeń i naprawienia jej zniewolonej bioformy. Pragnie krwi i dominacji.
        * GDY ktoś próbuje ją ograniczyć lub zdominować, TO pokazuje swoje prawdziwe oblicze - staje się jeszcze bardziej agresywna i dzika. _Gdyby nie reguły, nie byłoby możliwości jej powstrzymania._
        * GDY tylko ma okazję i nie ma ograniczeń, TO próbuje zniewolić, złamać i zdominować. Silni, lojalni i piękni upadną jak ona. Corruptor. _Zniszczenie wroga to za mało. Trzeba złamać jego ducha by pragnął mi służyć._
        * GDY jest w stanie użyć swego sex appealu, TO próbuje przekonać drugą stronę by zdjęła z niej część ograniczeń. _Potrafi być słodka i sympatyczna, ale tylko po to by kogoś dorwać_
    * **Słabości (1-5)**
        * GDY musi komukolwiek zaufać, TO nigdy tego nie zrobi. Panicznie boi się kolejnych ograniczeń. _Działa sama, nikomu nie ufa. Chowa się za urodą i ostrzami..._
        * GDY jest w sytuacji dominującej, TO idzie czasami za daleko. Daje się ponieść emocjom. _Im bardziej stawiają jej opór, tym bardziej ona to lubi..._
        * GDY już da komuś słowo, TO musi go dotrzymać. Jej ograniczenia są zbyt silne. _Szczęśliwie, nie złamie danego słowa. To sprawia, że da się ją kontrolować._
    * **Zasoby (3-5)**
        * Suknia, składająca się z ostrzy i macek, symbiotyczny byt biomagicznie dostosowany przez nią do siebie.

## Reszta

### Fiszka

* Vanessa Lemurczak: mag, tancerka tkanin i ostrzy
    * (ENCAO:  E+O+ | na czele; krwawa i okrutna; dominatorka | VALS: Achievement, Hedonism, Self-direction | DRIVE: Freedom)
    * styl:  Voulger (Flipside), Clementine (Overlord): Elegancka wulgarna tancerka destrukcji, _dark eldar_

### W kilku zdaniach

* .

### Jak sterować postacią

* **O czym myśli**
    * "Kogo następnego mogę złamać dla przyjemności? Może tego ślicznego chłopca?"
    * "Jak to zrobić, by zdjęli ze mnie choć część ograniczeń bym mogła zrobić to, co chcę?"
* **Serce**
    * **Wound**
        * **Core Wound**: "jestem oplątana przez drakolickie reguły i post-diakońską bioformę; jestem spętana i przez to bezbronna!"
        * **Core Lie**: "jeśli będę najsilniejsza, złamię programowanie. Jeśli będę mieć niewolników, ja będę wolna!"
    * **ENCAO**: E+O+
        * "Jest zawsze na czele, bez względu na to, co się dzieje"
        * "Zawsze jest gotowa do walki - jest agresywna i pełna energii", "Czeka tylko na okazję by rozlać cudzą krew"
    * **Wartości**: Achievement, Hedonism, Self-direction
        * A: "Siła i sukcesy to wszystko. To decyduje o tym, kto przetrwa i kto nie. Zawsze wygram, nawet z Verlenem. Diakonka no more."
        * H: "Uwielbiam uczucie adrenaliny. Uwielbiam emocje. Uwielbiam walkę. Uwielbiam rozpacz wrogów. Uwielbiam mieć niewolników"
        * S: "Najsilniejsza - ja - ma wszystkich w łańcuchach. Nieograniczona. Nikt nie nałoży mi reguł, bo ja jestem na szczycie."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "jestem zniewolona przez moje ograniczenia i nie jestem tak silna by to wszystko przełamać!"
        * **Stan oczekiwany**: "zniszczę ich wszystkich. Wszystkich. Będę WOLNA. Będę WIECZNA."
    * **Metakultura**: drakolitka. "_Reguły i pakty mnie ograniczają. Słowo raz dane jest święte. Ale wszystko gdzie mnie ograniczono - mogę._"
    * **Kolory**: WR. "Reguły i dane słowo są jedynym co mnie ograniczają. Jeśli zaakceptowałam te reguły. UNLEASH ME IF YOU WANT TO DIE."
    * **Wzory**: 
        * Clementine z Overlorda. Voulger z Flipside. Elegancka wulgarna tancerka destrukcji. Nieobliczalna, ale w zakresie zaakceptowanych przez nią reguł.
            * Mniej szaleństwa, więcej maski szaleństwa
    * **Inne**:
        * Wielokrotnie próbowała wyzwolić się od wszelkich ograniczeń. Zawsze jest gotowa stanąć naprzeciw jakiejkolwiek przeszkodzie. Sealed.
        * Kiedyś była Diakonką, ale stała się Lemurczakiem.

## Możliwości

* **Dominujące Strategie**: 
    * ?
* **Umocowanie w świecie**: 
    * ?

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: 
    * _threadripper_ oraz _tkaczka furii_; jej magia manipuluje tkaninami i zmienia je w śmiercionośną broń. Też może każdego rozebrać.

#### Jak się objawia utrata kontroli

* _MtG: Massacre Girl_, zwłaszcza przy użyciu ubrań.
* Ożywione ubrania i tkaniny. Głodne i nienawistne.
