## Metadane

* factions: "orbiter, oddział verlen, ród verlen"
* owner: "public"
* title: "Otto Azgorn"

## Kim jest

### W kilku zdaniach

Sierżant paladynów Verlen, jeden z trzech ostatnich paladynów na Inferni (reszta nie żyje). Człowiek, unikający wszelkich wspomagań, ale na szczycie ludzkich umiejętności. Nadrabia intelektem i taktyką tam, gdzie nie może polegać na czystej sile. Nieustraszony lojalista Verlenów i czystej ludzkości. Purysta ludzkiej natury, ale nie fanatyczny.

### Co się rzuca w oczy

* Motto: "Ludzie przetrwali wszystko. Przetrwamy i magię i zbyt zaawansowaną technologię."
* Silny, muskularny, stosunkowo cichy i spokojny. Mało mówi, zawsze chroni.
* Ma wytatuowany herb Verlenów na sercu; uważa to co spotkało Ariannę i Elenę za swoją osobistą porażkę.
* Lubi słuchać Verlenowej muzyki pop. Która ogólnie jest... specjalna.

### Jak sterować postacią

* Zdyscyplinowany, silny łowca potworów, znający się na walce z różnymi istotami. Mało mówi, dużo analizuje.
* Wykorzystuje zaawansowaną technologię, ale bez neurosprzężenia czy bez specjalnych biostymulatorów. Polega na intelekcie, nie tylko na czystej sile.
* Unika wszystkiego powiązanego z magią i anomalizacją ludzkiego ciała.
* Robi WSZYSTKO by uratować Elenę i nie dopuścić do upadku Arianny.
* Bardzo zdyscyplinowany i zauważa wszystko. Zawsze trzyma się regulacji i łańcucha dowodzenia.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Zebrał grupę rozbitych cywili i przeprowadził ich przez bardzo niebezpieczne miejsce mając jedynie dwóch rannych żołnierzy. Wszystkich utrzymał zimnym spojrzeniem i ostrym rozkazem.
* Skoordynował oddział żołnierzy i pokonał straszną anomalną bestię, której nie dali rady magowie. Siebie umieścił na stanowisku 'przynęty'.
* Coup de grace swojego bratanka, który zaczął się Zmieniać. To sprawiło, że oddelegowano go do ochrony Verlenek na orbicie.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* PRACA: Świetny taktyk, instruktor i inspirator. Daj mu oddział niezgranych ludzi i zrobi z nich sprawny regiment.
* PRACA: Zna wszystkie regulacje i zasady na pamięć i potrafi się po nich świetnie poruszać.
* ATUT: Peak of unaugmented humanity. Silny, szybki, wytrenowany w walce i używaniu różnego rodzaju broni.
* ATUT: Wie o swoich słabościach, więc kompensuje je doskonałym przygotowaniem i planowaniem taktycznym. Onslaught.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Zachowanie czystości sprawia, że jesteśmy za słabi. Korupcja magią lub technologią sprawia, że tracimy czystość i człowieczeństwo. I cannot win."
* CORE LIE: "Regulacje Verlenów sprawią, że dylemat zniknie. Ukrycie się za zasadami i regułami sprawi, że wszystko będzie dobrze. Wykonywałem rozkazy prawidłowo."
* Obsesja na punkcie wykonywania zasad i trzymania się regulacji, zwłaszcza Verlenów - te regulacje "są" jego osobowością.
* Niewspomagany człowiek w świecie magii, wszczepów, stymulantów bojowych i neurosprzężenia. "Obsolete hardware and software".
* Arianna zrobiła z niego arcykapłana swojego kultu, co jest dla niego zaprzeczeniem WSZYSTKIEGO w co wierzy.

### Serce i Wartości (3)

* Wartości
    * TAK: Prestiż (rodu), Tradycja (ród), Bezpieczeństwo
    * NIE: Stymulacja, Uniwersalizm
    * Z jego punktu widzenia ludzie NIE SĄ sobie równi. Verlenowie > czyści ludzie > inni. Ale misja jest ważniejsza niż jego przekonania.
    * Jedyne co się liczy to stan rodu Verlen i bezpieczeństwo jego podwładnych i podopiecznych. Jest autoryzowany do zrobienia czegokolwiek trzeba.
* Ocean
    * E:-, N:0, C:+, A:0, O:0
    * Introspektywny i kontemplacyjny. Nie wykonuje zbędnych ruchów. Zawsze zachowuje się, jakby oszczędzał energię.
    * Precyzyjny i bardzo cierpliwy, potrafi zaplanować i poczekać i uderzyć jednym precyzyjnym ciosem.
* Silnik
    * Chronić, uratować i naprawić Elenę i Ariannę.
    * Zapewnić, by ideały i cele Verlenów były zapewnione - i żeby siły i ideały Verlenów fundamentalnie okazały się zwycięskie. REGULACJE forever.
    * Czysta ludzkość. Czyste ciało, czyste serca.
    
## Inne

### Wygląd

.

### Coś Więcej

* ?

### Endgame

* ?
