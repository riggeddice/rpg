## Metadane

* factions: "szczeliniec"
* owner: "public"
* title: "Lucjusz Blakenbauer"


## Kim jest

### Paradoksalny Koncept

Lekarz walczący o każde życie, stworzony w laboratoriach Blakenbauerów jako maszyna do zabijania. Lekarz nie dyskryminujący żadnego życia, zdolny do zabijania biologicznie lub swoim ciałem. Wspomagany biomagicznie, walczy przeciwko jakimkolwiek zbędnym wspomaganiom. Nie będąc człowiekiem, walczy o prawo ludzi do "czystości" struktury. Nie dyskryminuje władzy, mafii, "wrogów". Liczy się tylko misja - ratowanie życia.

### Motto

"Każde życie jest warte ratowania. Jeśli się nie zgadzasz - zejdź mi z drogi."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Wybitny lekarz i badacz biomantyczny. Specjalizuje się w Skażeniu i operowaniu energiami magicznymi.
* ATUT: Klątwa Blakenbauerów daje mu siłę i drugą formę - bojową. Gdy jest Bestią, jest niepowstrzymany.
* SŁABA: Nie kontroluje transformacji i drugiej formy. Jeśli będzie zmuszony, zmieni formę i zacznie eksterminację.
* SŁABA: Lucjusz jest absolutnie przewidywalny. Ma swoją misję i idzie do celu. Chroni życia i zdrowie - i tyle.

### O co walczy (3)

* ZA: Świat, w którym ludzie mogą być ludźmi, bez Skażania czy cyborgizacji.
* ZA: Miejsce bez "noktian" i "astorian", bez frakcji i wojen. To "my kontra świat".
* VS: Nie wolno poświęcać najsłabszych - będzie o nich walczył za cenę kariery i majątku.
* VS: Wzmocnienia organizmu - czy to bio/magi/techno - to błąd. Do niebezpiecznych rzeczy są TAI.

### Znaczące Czyny (3)

* Gdy w jego Rezydencji schroniła się ofiara pewnego gangu i ów gang zaatakował, Lucjusz ich pokonał - siłą fizyczną i bioformami. A potem mafia ich rozgromiła.
* Walczył o życie Ernesta Kajrata nawet, gdy nikt nie miał nadziei że to się uda. I wygrał - Kajrat przeżył.
* Opuścił stanowisko lekarza w Szpitalu Terminuskim by chronić Alicją przed przekształceniem jej w Strażniczkę Esuriit. Przegrał.

### Kluczowe Zasoby (3)

* COŚ: Supernowoczesna Rezydencja Blakenbauerów pod Pustogorem z zaawansowanym biolabem.
* KTOŚ: Dostęp do większości baz większości frakcji w Szczelińcu. Pomagał wszystkim.
* WIEM: Jak przekształcić większość owadów czy grzybów w straszliwą broń biologiczną.
* OPINIA: Apolityczny lekarz, który się nie poddaje i walczy o pacjenta / ofiarę Skażenia do samego końca.

## Inne

### Wygląd

Silny fizycznie i solidnie zbudowany wysoki mężczyzna; strasznie żylasty. Brązowe oczy, brak zarostu na twarzy, włosy bardzo krótko obcięte. Twarz pociągła, lekko zapadnięta. Ubrany zwykle w praktyczny, biały strój; często ma coś ochronnego.

### Coś Więcej

Karta postaci nie oddaje jak niesamowicie niebezpieczny jest w formie Bestii. Nie pokazuje też jednego - Lucjusz NIE jest pacyfistą. Unika krzywdzenia i szkodzenia, ale jeśli nie ma wyjścia... w końcu był zaprojektowany jako broń, prawda?
