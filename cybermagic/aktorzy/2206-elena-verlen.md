## Metadane

* factions: "orbiter, arystokracja aurum, oddział verlen, spustoszenie strain elena"
* owner: "public"
* title: "Elena Verlen"


## Kim jest

### W kilku zdaniach

Kiedyś - arystokratka Aurum rodu Verlen, uciekająca przed swoją przeszłością w powinność i zasady. Dzisiaj - Koordynator Spustoszenia strain Elena. Świetny pilot małych VTOLi. Bombardowana przez Ixion oraz Esuriit, bardzo wspomagana przez visiat, wiecznie próbuje zachować osobowość ale przegrywa tę walkę. Z zawodu - neurosprzężony advancer.

### Co się rzuca w oczy

* Transorganiczna istota, zintegrowana z senti-eidolonem. Wygląda jak człowiek, ale widać transorganikę.
* Absurdalnie skupiona na rywalizacji. Praktycznie nie wycofuje się ze starcia, niezależnie od jej stanu.
* Szanuje kompetencję i umiejętności, nawet jeśli nie lubi osoby. Nie cierpi pozerstwa.
* Arogancka, nie wierzy, że ktoś może być lepszy od niej. Jeśli jest - trenuje do padnięcia. Ale wygra.
* Nie potrafi kontrolować swojego temperamentu.
* Żąda bycia docenioną i walczy o jak najlepszą karierę - oraz o to, by wszyscy działali jak najlepiej.
* Ma kiepską opinię o nie-militarnych siłach ludzkości. Jest tu dla walki.

### Jak sterować postacią

* SPEC: Każdy test zawiera 3 Or -> czy Spustoszenie nie przejmie nad nią kontroli i czy nie objawi się przez Destrukcję czy przez Asymilację / Adaptację.
* Esuriit - nieskończony głód - objawia się u niej w kierunku na Eustachego i **bycie najlepszą**, **uratowanie wszystkich** oraz **kontrolę nad sobą**. Prosta do wyzwolenia.
* Elena stała się bardzo radykalna. Nie ceni życia innych. PRÓBUJE je cenić, ale straciła tą umiejętność.
* Próbuje, bardzo próbuje być lojalna Orbiterowi, Ariannie i przyjaciołom. Bardzo próbuje.
* Próbuje... ratować. Nie do końca pamięta CZEMU oraz JAK ale próbuje. Zwłaszcza dzieci. Anastazja...
* Nie potrafi kontrolować swojego temperamentu. Nie kontroluje swoich impulsów. Zapomina. Gubi. Traci... nie pamięta.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W pojedynku z Eustachym (naprawa-sabotaż) wspomagana visiatem utrzymała się naprawiając Infernię aż Persefona musiała ich odciąć.
* Jako oficer Castigatora była TERROREM arystokracji Aurum; niby jedna z nich, ale upokarzała i tępiła tworząc jakiś porządek.
* "Lot Walkirii" małą korwetką, gdy była sprowokowana przez Ariannę. Całkowicie szkodliwy lot dla statku, ale pobiła rekord tej korwety.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* SPEC: Każdy test zawiera 3 Or -> czy Spustoszenie nie przejmie nad nią kontroli i czy nie objawi się przez Destrukcję czy przez Asymilację / Adaptację.
* CECHA: Koordynator Spustoszenia. Potrafi przejąć kontrolę nad kimś / mechanizmem. Potrafi się integrować. Hive Queen.
* CECHA: Transorganiczny Terrorform. Zmiennokształtna, zdolna do asymilacji. Integrując się ze statkiem stanowi z nim jedność.
* PRACA: Wybitny pilot małych jednostek oraz servarów. Zwłaszcza teraz, przy specjalnych zdolnościach asymilacji.
* ATUT: Perfekcyjna kontrola przestrzeni: zawsze CZUJE gdzie jest i co jest dookoła niej.
* ATUT: Wspomaganie visiańskie: silne emocje (gniew, strach) i ból to uruchamiają - pilotaż, reakcje, walka. Dotyka Esuriit i Ixionu.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Tracę wszystko, na czym mi zależy. Niszczę wszystkich, których kocham. Zniszczyłam nawet siebie i nic nie zostało."
* CORE LIE: "Jestem w stanie poradzić sobie z tym wszystkim sama. Mam wszystko pod kontrolą. Nie zmieniam się bardziej."
* Łatwa do sprowokowania do działania - zwłaszcza do ratowania, stosunkowo przewidywalna.
* Absolutnie nie umie się opanować jak zaczyna działać, jest wirem destrukcji. I **musi** być najlepsza.
* SERCE: Rana związana z arystokracją i pasożytami; żąda wysokiej klasy zachowań i kompetencji.
* CIAŁO: Bardzo wrażliwa na wszystkie bronie anty-anomalne przez głęboką integrację visiańską.
* SERCE: Trywialna do sprowokowania - po prostu NIE UMIE zostawić żadnej urazy na boku.
* SERCE: Nie radzi sobie z konfliktami inaczej niż przez eskalację lub unikanie. Zwykle eskaluje.
* MAGIA: Jej energia manifestuje się instynktownie, często przez Ixion / Esuriit / Spustoszenie. Nie ma już magii dynamicznej.
* Wszystkie testy przez Eustachego mają stabilną, skuteczną kategorię wyżej. Kocha go. Chce się z nim zjednoczyć. Spustoszyć go, Ariannę...

### Serce i Wartości (3)

* Wartości
    * TAK: Tradycja (Powinność), Osiągnięcia, Samosterowność
    * NIE: Hedonizm, Pokora
    * Zawsze pragnęła być wolna, być najlepsza - wymagała od siebie i innych jak najwięcej. Działać w strukturach Orbitera, najlepszy advancer.
    * Nigdy nie rozumiała tych, którzy akceptują to co ich spotyka i tych którzy pragną najwięcej dla siebie. Zwalczała ich. WALCZ I WYGRYWAJ! Daj wszystko i WIĘCEJ!
* Ocean
    * E:0, N:+, C:-, A:0, O:0
    * Agresywna i impulsywna, mająca uczucia na dłoni i walcząca z negatywnymi uczuciami. Bardzo się wszystkim przejmuje.
    * Gorsza w planowaniu, lepsza w szybkim działaniu i reagowaniu. Wpierw działa, potem myśli.
* Silnik
    * Dorównać swojej kuzynce (Ariannie). Przekroczyć Ariannę. Znaleźć dla siebie miejsce w strukturach Orbitera.
    * Uratować i ochronić wszystkich swoich przyjaciół... i innych. Jest w stanie. By ich nie spotkało to co ją.
    * Dobrze zginąć. (DEATHWISH!)

## Inne

### Wygląd

.

### Coś Więcej

* Wiem, że ucieka od czegoś w Aurum. Nie wiem jeszcze czego. <-- przeszłość, Verlen / Blakenbauer
* Ma romans z Eustachym ;-). Tzn. nie ma. Ale są plotki. <-- nieaktualne

### Endgame

* Stanie się AI Nocnej Krypty
* Stanie się Koordynatorem Spustoszenia i będzie szerzyć mrok w galaktyce
* Stanie się Koordynatorem Spustoszenia po stronie Orbitera
