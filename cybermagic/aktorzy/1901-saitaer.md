## Metadane

* factions: "saitaer"
* owner: "public"
* title: "Saitaer"


## Postać

### Ogólny pomysł (3)

Terrorform. Godling. Bóg. Istota o **dwie kategorie** silniejsza niż powinna być. Pasożyt z wymarłego świata, który zainfekował Astorię. Kiedyś, bóg typu support - rekonstruktor. Mistrz techorganiki i magii krwi o aspekcie "Rekonstrukcja".

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* PERFECTION/ADAPTABILITY; wszelkie życie jest nieśmiertelne i adaptujące; rekonstrukcja życia do techorganiki
* mój świat (Erelis) jest martwy; mój świat ożyje; wyssanie energii z Primusa by oddać ją Erelisowi
* niekompatybilność rzeczywistości ze Wzorem; cała rzeczywistość jest spójna ze Wzorem; Aspekt Rekonstrukcji

### Wyróżniki (3)

* Rekonstruktor. Potrafi przebudować i przetransformować wszystkie istoty w techorganiczne amalgamaty.
* Mistrz Magii Krwi. To, co my nazywamy "magią krwi" jest kanwą Saitaera - jego naturalną formą manipulacji rzeczywistości
* Skażenie. Boska energia potrafi przekształcić ciało i umysł jego oponentów.

### Zasoby i otoczenie (3)

* Nieliczni wyznawcy, ale zawsze wyznawcy

### Magia (3)

#### Gdy kontroluje energię

* otwiera portale
* transformuje istoty, tworzy życie
* Skaża magów i ludzi, jest w końcu bogiem

#### Gdy traci kontrolę

* nie dotyczy

### Powiązane frakcje

{{ page.factions }}

## Opis

.
