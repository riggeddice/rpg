## Metadane

* factions: "pustogor, rekin, terienak, aurum"
* owner: "kić"
* title: "Daria Czarnewik"


## Kim jest

### W kilku zdaniach



### Co się rzuca w oczy

* Około 40-tki
* No-nonsene
* Brak widocznych augumentacji

### Jak sterować postacią

No nonsense, jest problem, trzeba go rozwiązać. Inżynieria zwykle może pomóc, nawet jeśli to problem z morale.
Inżynierami dowodzi poprzez przykład i dając im szansę na sukcesy.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* 

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* AKCJA: 
* AKCJA: 
* AKCJA: 
* AKCJA: 
* CECHA: 
* COŚ: 
* COŚ: 

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: 
* CORE LIE: 
* 
* 
* 
* 

### Serce i Wartości (3)

* Wartości
    * TAK: Self-direction ()
    * NIE: 
* Ocean
    * E:, N:, C:, A:, O:
	* 
	* 
    * 
* Silnik
	* 
	* 

### Magia (3M)

#### W czym jest świetna

* Naprawa i utrzymanie wszelkich statków kosmicznych

#### Jak się objawia utrata kontroli

* 

## Inne

### Wygląd

.

### Coś Więcej

Urodzona na K1, tam się wychowała. Próżniowiec, świetnie czuje się w nieważkości. 
Ma pomniejsze wszczepy wspomagające pamięć (wszystkie możliwe blueprinty pod ręką), ale nie dała sobie wszczepić niczego bardziej zaawansowanego niż moduł pamięci i AR.
Choć urodziła się i wychowała na K1 i ma tam wielu znajomych, nie lubi tam przebywać i unika tego jak może.
Zna i lubi się z wieloma inżynierami na różnego rodzaju statkach.


### Endgame

* ?
