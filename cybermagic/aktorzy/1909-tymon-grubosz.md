## Metadane

* factions: "terminus, pustogor"
* owner: "public"
* title: "Tymon Grubosz"


## Paradoks

strasznie groźny i potężny terminus szczerze wierzący w Astorię - jednocześnie trochę naiwny

## Motywacja

### O co walczy

* o zachowanie ludzkości i cywilizacji astoriańskiej; optymizm, bezpieczeństwo, pomaganie sobie
* szczerze wierzy w Pustogor, Karlę oraz w misję terminusów na tym terenie
* integracja noktian, astorian - wszystkich w obszarze Szczelińca. Integracja ludzkości.

### Przeciw czemu walczy

* cokolwiek zagraża jego przyjaciołom i osobom pod jego opieką
* anomalie magiczne i Skażenie - włączając w to Saitaera czy Wiktora Sataraila
* chaos, szaleństwo, utrata kontroli - utrata cywilizacji i kultury

## Działania

### Specjalność

* terminus o ogromnej wręcz odporności na wszelkie ciosy, z hiperstabilnym Wzorem
* katalista zdolny do transferu, kontroli lub przerzucania ogromnych energii magicznych
* katai, wspomagany przez ciężki sprzęt terminuski.
* pokorny, o nieskończenie wielkiej wierze w Karlę, Pustogor i Zjednoczenie Astoriańskie - na co magia odpowiedziała

### Słabość

* gołębie serce; czasem zbyt łagodny lub daje o jedną szansę za dużo
* nie chce robić innym problemów, ma tendencje do brania za dużo na siebie
* zawsze próbuje pomóc przyjaciołom; nie odrzuca ich próśb - przez co często cierpi
* naiwny jak na terminusa

### Akcje

* Katai w superciężkim servarze Entropik, niszczący problemy wspomaganą bronią białą
* Zawsze próbuje pomóc i zawsze zakłada, że druga strona chce dobrze
* Przekierowuje i steruje energią magiczną jakby był istotą Vim
* Traktuje Pustogor czy Zjednoczenie Astoriańskie w sposób religijny i tak o tym mówi

### Znaczące czyny

* Potrafił samemu zogniskować i odepchnąć falę energii z Trzęsawiska. Ucierpiał, ale przetrwał.
* Swoją obecnością dał radę zdeeskalować niebezpieczną sytuację; czterech magów nie odważyło się z nim walczyć.
* Infekcja Sataraila. Fala Esuriit. Wszystkie takie ciosy energetyczne jego Wzór wytrzymał. Prawdziwy niezłomny paladyn. Wiara jest jego tarczą.
* Pod wpływem narkotyków czy innych środków, nie obrócił się przeciwko Karli czy Pustogorowi. Nieskończenie wierzy w swoją misję.
* Samotnie potrafił przebić się przez niewielką bandę Grzymościa, po czym ich rozbił i zmusił do ucieczki.

## Mechanika

### Archetypy

.

### Motywacje

.

## Inne

### Wygląd

Dwumetrowy i 120-kilogramowy terminus o strasznym obliczu; zwykle w swoim superciężkim servarze Entropik. Na ciele ma 84 tatuaże - imiona i nazwiska magów, którzy zginęli za Astorię / Szczeliniec i których znał za życia.
