## Metadane

* factions: "szczeliniec, terminus"
* owner: "public"
* title: "Laura Tesinik"


## Kim jest

### Paradoksalny Koncept

Uczennica terminusa, której bliżej do prawniczki niż do wojowniczki. Wykorzystuje drony i AI, by nie musieć działać osobiście. Potrafi każdego przeciwnika wystawić / wykurzyć z bezpiecznego miejsca, ale nie umie tego wykorzystać; jej magia jest za wolna. Uciekinierka z Cieniaszczytu (kralothy) i studentka Akademii Magii, która pragnie życia terminusa by nie musieć wracać. Wykorzystuje prawo by znaleźć w nim luki, które obróci na swoją korzyść.

### Motto

"Prawo nie jest celem - to kolejna broń w arsenale terminusa - dokładnie jak drony czy technologia."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zna absolutnie każdy paragraf i zasadę prawną jaka dotyczy sytuacji. Skutecznie paraliżuje praworządne osoby.
* ATUT: Psychotroniczka; edytuje i wchodzi w głąb AI. Świetnie zarządza dronami i wykorzystuje je jako wsparcie.
* ATUT: Potrafi każdego wykurzyć zza osłony / z fortecy kombinacją dron, gazów bojowych i superciężkich zaklęć destrukcyjnych. Wystawi każdy cel.
* SŁABA: W walce wręcz i krótkiego zasięgu; nadaje się tylko na średni / daleki zasięg. W krótkim zasięgu / walce wręcz słabsza niż nie-terminusi!
* SŁABA: Jej magia jest wolna; nie jest w stanie czarować szybko i w warunkach nie-artyleryjskich.

### O co walczy (3)

* ZA: Zrobi wszystko by osiągnąć status pełnoprawnej terminuski; to jej pozwoli się ochronić przed wrogami z Cieniaszczytu (kralotycznymi).
* ZA: Większa swoboda i wolność wyboru; kocha model cieniaszczycki i nie przepada za szczelińskim. Nie jest przeciwna mafii czy używkom.
* VS: Bardzo przeciwna kralothom i dominacji viciniusów. Boi się kralothów. By do żadnego się nie zbliżyć, wesprze diabła.
* VS: Nienawidzi hipokrytów, którzy tylko "wykonują rozkazy". Ona zaryzykowała i wygrała.

### Znaczące Czyny (3)

* W Cieniaszczycie z zimną krwią zastawiła pułapkę zabijając kralotha w odwecie za zniewolenie jej przyjaciół. Potem uciekła.
* Niejednokrotnie wybroniła Tukana z problemów prawnych przez jego współpracę z Grzymościem; została jego protegowaną.
* Zrobiła automatyczny monitoring obszaru dronami i elektroniką, gdy wymykała się na schadzkę z przyjacielem w szkole magów. 

### Kluczowe Zasoby (3)

* COŚ: Osiem dron zwiadowczo-bojowych klasy 'Żądłacz'; sama je odratowała i wstrzyknęła im poszerzoną psychotronikę.
* KTOŚ: Jej głównym kręgiem towarzyskim są jednak uczniowie Akademii Magii w Zaczęstwie. Napoleon ją zwalczał i nie wierzy, że została terminuską...
* WIEM: Wie bardzo dużo na temat tego jak szybko przejąć kontrolę / wykorzystywać typowe cywilne TAI.
* OPINIA: Bardzo dobrze wykorzystuje prawo dla własnych celów; nie stroni od zabawy i wolności typu cieniaszczyckiego.

## Inne

### Wygląd

.

### Coś Więcej

.
