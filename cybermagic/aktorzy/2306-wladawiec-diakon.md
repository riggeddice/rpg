## Metadane

* factions: "diakon"
* owner: "public"
* title: "Władawiec Diakon"

## Mechanika

* **Obietnica**
    * "Przyciągniesz wszystkie oczy i będziesz kompetentny w każdej roli na statku kosmicznym. Twoje zrozumienie innych i manipulowanie ich ruchami będzie niezwykłe. I genialnie walczysz w zwarciu."
    * "Nieważne gdzie się nie znajdziesz, będziesz mieć władzę nad ludźmi. Staniesz się szarą eminencją. Będą słuchać Twych słów a kobiety - zwłaszcza dumne i potężne - będą na Twoje skinienie."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY pojawi się dama która ma być Twoja, TO ją usidlisz - feromonami, słowami, zachowaniem. Dopasujesz się do jej marzeń. _Nie rozumiesz... one służąc z Władawcem robiły RZECZY... i tęsknią._
        * GDY wchodzisz w umysł drugiej osoby, TO rozumiesz ją dogłębnie; czy to magią czy empatią. _Nikt tak Cię nie zrozumie i nie wysłucha jak on. Zawsze czujesz się przy nim bezpieczna._
        * GDY walczysz w zwarciu poza servarem, TO wygrywasz starcia; walka wręcz, broń biała czy broń krótka. _Świetnie walczy i to w niesamowitym stylu._
    * **Kompetencje (3-5)**
        * GDY wchodzi na salę, TO wszystkie oczy ma na sobie. Elegancja, gravitas i styl. _Wygląda i zachowuje się jak prawdziwy dowódca, w stresie czy w akcji._ 
        * GDY jest na statku kosmicznym, TO potrafi pełnić praktycznie każdą rolę na mostku. _Świetny generalista, zawsze przydatny i pomocny._
        * GDY pojawia się panika, TO przejmuje kontrolę i kieruje wszystkich w odpowiednią stronę. _Jego ludzie są mu oddani i ich morale się nie łamie - zadowolą go za wszelką cenę._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE mieć wszystko co istotne pod kontrolą. Ciało, serce i duszę. Wszystko ma działać perfekcyjnie. Wszystko ma być piękne. _Zapewni Ci utopię - jeśli pragniesz szczęścia, on nim jest._
        * GDY pojawia się nowa osoba w jego domenie, TO znajdzie czego ta osoba pragnie i jej to zapewni - za nieskończoną lojalność. _Wszystko będzie kontrolował - od snów po najmniejszy szczegół._
        * GDY pojawia się dama która z nim konkuruje lub jest wyzwaniem, TO powoli ją usidli aż stanie się jego akolitką. _Naturalnym dla kobiet jest ulec silnemu mężczyźnie. Z ich własnej woli._
        * GDY widzi strach, TO pomoże Ci go przekroczyć i przesunąć się dalej. _Prowadzi do perfekcji we wszystkim co robi, jeszcze bardziej Cię usidlając._
    * **Słabości (1-5)**
        * GDY widzi damę w jego profilu, TO musi spróbować ją usidlić. _Jego słabością są silne kobiety. Nie zostają silne długo, ale dlatego nie będzie nigdy kapitanem._
        * GDY czegoś naprawdę pragnie lub się nudzi, TO wykorzystuje swoje umiejętności w mroczny sposób. _Nigdy nie zapisuj się na misję patrolową z nim na pokładzie. Zaufaj mi, to zły pomysł._
    * **Zasoby (3-5)**
        * Reputacja doskonałego XO, acz takiego któremu ulegają wszelkie damy. Przez to jego reputacja jest tyci toksyczna dla kobiet.
        * Reputacja - jak się pojawia na statku kosmicznym, statek w pewnym momencie zaczyna działać dużo lepiej.

## Reszta

### Fiszka

* Władawiec Diakon: tien, corruptor of ladies, duelist, second-in-command na statku kosmicznym (XO)
    * ENCAO: (E+C+) | intrygancki;; kontrolowany wulkan;; pająk w sieci | VALS: Hedonism, Power | DRIVE: Perfekcja Zaufania
    * styl: nieskazitelny oficer, delikatny uśmiech, niezmiennie pewny. Gabriel Durindal (Gundam Seed Destiny)
    * piosenka wiodąca: Epica "Obsessive Devotion"

### W kilku zdaniach

* .

### Jak sterować postacią

* **O czym myśli**
    * "Skuteczna organizacja i prawdziwa władza wymaga personalnego oddania personelu wobec przełożonego. I dostarczę im tego czego potrzebują."
    * "Pokażę Orbiterowi, w jaki sposób należy zmienić organizację. Jak to zrobić, by wszystko działało prawidłowo."
* **Serce**
    * **Wound**
        * **Core Wound**: ""
        * **Core Lie**: ""
    * **ENCAO**: E+C+
        * "Każda sytuacja jest możliwa do rozwiązania - wymaga tylko odpowiedniego podejścia i adaptacji."
        * "Każda dziewczyna pragnie mi służyć. To normalne prawo natury."
    * **Wartości**: Hedonism, Power
        * H: "Nie ma nic piękniejszego niż dumna dama, która w rzeczywistości nie może się doczekać aż zostaniemy sam na sam."
        * P: "Moim spadkiem będzie sprawny Orbiter, doskonałe załogi i zwycięstwo. Osiągnę to przez UWOLNIENIE załóg. Będą wdzięczni MNIE."
        * HP: "Prawdziwa władza i ekstaza to nie łóżko - to absolutna kontrola drugiej osoby, wszystkich elementów, każdego snu i ruchu."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Jestem doskonałym pierwszym oficerem na jednostce, na której jestem skuteczny."
        * **Stan oczekiwany**: "Statek kosmiczny - a potem Orbiter - jest jednym organizmem, który musi sobie idealnie ufać i w pełni współpracować ze sobą. Ze mną."
    * **Metakultura**: drakolita. "_Moje słowo jest święte i moja perfekcja musi być absolutna. Więc obiecam Ci że się Tobą zaopiekuję - jeśli ulegniesz._"
    * **Kolory**: UB "Każda istota ma prawo do ewolucji. Każda dziewczyna ma prawo do ewolucji. Ja mam obowiązek dążenia do władzy absolutnej. A to prowadzi do brania czego pragnę."
    * **Wzory**: 
        * (Gabriel Durindal) MIX (Altena) MIX (Kiryuu)
        * Próbuje być pozytywną siłą, ale żąda za to odpowiedniego zachowania w wymianie.
    * **Inne**:
        * .

## Możliwości

* **Dominujące Strategie**: 
    * Corruption of Angels. Mediacja. Kestra'chern
    * Magia mentalna, dominacja, The Spider
    * Inspiracja własnym przykładem
    * Mimikra do potrzeb i adaptacja, by druga strona za nim poszła. Użyje uczuć drugiej strony przeciw niej.
* **Umocowanie w świecie**: 
    * ?

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: 
    * _manipulation of souls_; perfekcyjna mimikra i wejście w umysł drugiej osoby by stopniowo ją zmieniać. Czytanie myśli, obecność w snach i feromony.
* AFFINITY
    * Esuriit, Praecis, elementy Ixion (z Rodu)
        * w jego wypadku Alucis -> Praecis, Esuriit. 
            * Nie chce koić, chce rządzić i mieć świat pod kontrolą
            * Skupia się na pożądaniu i głodzie, na uczuciach ROZPACZLIWEJ POTRZEBY
            * Praecis, Ixion ORAZ Esuriit działają tylko w domenie mentalnej i 'ultrafizycznej' (np. _ars amandi_)
        * Ixion dotyczy jedynie jego własnej bioformy
            * Zmiana i dostosowanie siebie do _ars amandi_, konstrukcja feromonów
            * Zmiana swojego zachowania pod kątem tego co jest potrzebne, perfekcyjny mimik-corruptor

#### Jak się objawia utrata kontroli

* 
