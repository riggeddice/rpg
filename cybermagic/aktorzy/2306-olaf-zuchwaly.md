## Metadane

* factions: "Pustogor"
* owner: "public"
* title: "Olaf Zuchwały"

## Funkcjonalna mechanika

* **Obietnica**
    * "Będziesz śledczym, który został barmanem. Zdeeskalujesz niejedną sytuację, przeprowadzisz śledztwo a jak trzeba - unieszkodliwisz nogą od krzesła."
    * Były agent biura spraw wewnętrznych klanu dekadiańskiego Noctis, który nie wierzył w magię. 
        * Postrzelony przez przyjaciół i zostawiony na śmierć zakochał się w Astorii i życiu planetarnym i jako śledczy został świetnym właścicielem baru.
        * Odrzucił swoje poprzednie nazwisko wraz ze "śmiercią".
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY sytuacja nie jest zbyt napięta, TO bez problemu wyciągnie z ludzi wszystkie sekrety. _Sympatyczny i współczujący, aż chce się z nim gadać._
        * GDY musi zrozumieć co się stało w scenie, TO składając fakty i robiąc robotę policyjną dojdzie do tego co się tu stało. _Dokładny i cierpliwy, ma czas._
    * **Kompetencje (3-5)**
        * GDY trzeba walczyć w zwarciu, TO jest mistrzem brudnej walki, zwłaszcza bronią improwizowaną. _Walczy agresywnie i do końca, jak dekadianin._
        * GDY wszyscy są zdenerwowani i przestraszeni, TO potrafi zdeeskalować śmiechem, żartem i łagodnym słowem. _Łagodny z natury, idzie za flow_.
        * GDY trzeba zmajstrować coś przy użyciu zwykłych narzędzi, TO bez problemu zrobi karmnik czy drabinę. _Lubi proste, spokojne życie na planecie._
        * GDY trzeba zająć czyjąś uwagę, TO potrafi opowiadać niezwykłe historie z czasów Noctis czy pracy śledczego. _Jest gadatliwy i ujmujący jak na dekadianina._
    * **Agenda i styl działania (3-5)**
        * PRAGNIE życia na planecie. Bar, domek, natura, majstrowanie. Wszystko, czego nie miał na statku kosmicznym. _To jest prawdziwe, dobre życie, nie te wszystkie wojny._
        * GDY sytuacja jest niejednoznaczna moralnie, TO nawet nie próbuje jej zrozumieć - tylko minimalizuje cierpienie. _Nie mieszam się, to nie na moją głowę._
        * GDY są różne nacje, style lub działania, TO próbuje integrować i pomagać wszystkim się dogadać. _Wszyscy jedziemy na tym samym wózku..._
    * **Słabości (1-5)**
        * GDY może osiągnąć sukces zabijając kogoś, TO próbuje tego uniknąć - on sam przeżył tylko dzięki szczęściu. _Nie jest pacyfistą, ale unika zabijania._
        * NAWET GDY ktoś jest niezaprzeczalnym potworem (Sabina Kazitan), TO próbuje pomóc i dać osobie drugą szansę. _Każdy ma jeszcze jedną szansę._
        
## Reszta

### W kilku zdaniach

* Były agent biura spraw wewnętrznych klanu dekadiańskiego Noctis, który nie wierzył w magię.
    * Skuteczny śledczy, usuwał problemy z morale i ścigał frakcję pro-magiczną
    * Gdy okazało się, że jego "brat" jest magiem i ma dokumenty o magii, miał go na muszce ale nie strzelił.
    * Postrzelony przez przyjaciół i zostawiony na śmierć. Wtedy "umarł".
* Dostał drugą szansę w Noctis. Uczestniczył w Inwazji jako ten co wie o magii.
* Podczas Inwazji zestrzelony.
    * Stoczył się, alkohol itp.
    * Odrzucił swoje poprzednie nazwisko, bo "zginął" zdradzony przez Noctis.
* Trzecie życie - na Astorii
    * Zakochał się w Astorii i życiu planetarnym i został świetnym właścicielem baru.

### Jak sterować postacią

* **O czym myśli**
    * "To wszystko nie na moją głowę. Proste, uczciwe życie wśród natury na planecie - to jest to."
    * "Przy wszystkich wadach Noctis niektóre rzeczy są świetne i warto je zachować i utrzymać."
    * "Po co się wszyscy kłócą, to wszystko i tak nie ma znaczenia. Nie poznamy prawdy, bo jak?"
* **Serce**
    * **Wound**
        * **Core Wound**: "W imię nieprawdy krzywdziłem innych. Wszystko w co wierzyłem obróciło się w proch. Nawet najbliżsi mnie opuścili. Próbowali mnie zastrzelić. Odrzucono mnie jak pionka."
        * **Core Lie**: "Da się żyć prostym, spokojnym życiem i się nie angażować."
    * **ENCAO**: +00++ | "Świetnie opowiada", "King of bad jokes"
    * **Wartości**: Humility, Tradition > Security
        * H: "Nie ogarnę tego co się dzieje. Miało nie być magii - jest magia. Miałem mieć rodzinę i sojuszników, zastrzelili mnie. To nie na moją głowę. Go with the flow."
        * T: "Dekadiańskie doktryny i strategie ogólnie działają, warto je rozprzestrzenić."
        * S: "Możesz zrobić wszystko dobrze a i tak wszystko stracisz. Just go with the flow."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Jestem nomadem, nie mam niczego, jestem w obcym świecie i muszę się angażować w rzeczy niemożliwe"
        * **Stan oczekiwany**: "Spokojne życie na Astorii wśród natury, domek, majsterkowanie, trochę bimbru i nie mieszanie się w za trudne dla mnie sprawy"
    * **Metakultura**: dekadianin. "_Nacja i Twoja grupa są wszystkim. Ja nie mam grupy ani nacji. Tylko spokojne życie na pięknej planecie._", "_Wolność jest nieistotna, tylko robienie dobrej roboty._"
    * **Kolory**: G>WR: pasja i pragnienie harmonii, pogodzenie z tym co jest
    * **Wzory**: 
        * przeciętny wesoły barman-wiking opiekujący się zbłąkanymi kotkami. Ale samotny.
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * nieokreślone
* **Zasoby**: 
    * nieznane
* **Umocowanie w świecie**: 
    * drifter, potem barman w Pustogorze.

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: nie czaruje
    * .

#### Jak się objawia utrata kontroli

* .
