## Metadane

* factions: "aurum"
* owner: "public"
* title: "Mariusz Grabarz"


## Kim jest

### Paradoksalny Koncept

Całkiem dobry reżyser o sporych aspiracjach, który robi niską propagandę dla możnych. Doskonały w korumpowaniu i kuszeniu, ale robi to tylko dla fabrykowania świetnej historii. Najlepiej poinformowany o prawie i zasadach w okolicy mag, używający giętkiego języka by mieć odpowiednie pozwolenia i nie dało się mu niczego zarzucić.

### Motto

"Świetna opowieść przetrwa wieki. Może i robię propagandę, ale to świetna propaganda."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Dobry w prawie i biurokracji - wie na co może sobie pozwolić a co jest całkowicie nielegalne. Zawsze ma wszystkie pozwolenia które są potrzebne, umie pracować z dokumentami.
* ATUT: Kusiciel - rozumie słabości i naturę innych. Potrafi im zaproponować to, czego pragną. Dobry w korumpowaniu i szantażowaniu.
* SŁABA: Tak skupia się na świetnej historii i odpowiednim show, że jest skłonny poświęcić cały plan by tylko opowieść była rewelacyjna.
* SŁABA: Brzydzi się przemocą. Nie jest w stanie walczyć nawet w obronie własnej. Unika krwi (chyba, że na ekranie).

### O co walczy (3)

* ZA: Chcę stworzyć coś pięknego, chcę zrobić prawdziwą sztukę, świetny film. Ale mnie nie stać...
* ZA: Silniejszy ma więcej praw niż słabszy - dlatego chce być doceniony przez arystokratów Aurum i chce się u nich zaczepić.
* VS: Okrutne tępienie dłużników. Sam ma straszne długi u inwestorów i musi je spłacić - więc poluje na pieniądze za robotę marną czy godną...
* VS: Nie chce być tylko nędznym, żałosnym szczurem ulicznym robiącym propagandę. Gardzi taką przyszłością i szuka lepszej.

### Znaczące Czyny (3)

* Stworzył fikcyjne love story między kilkoma ludźmi, dzięki czemu skomplikował im życie - ale zarobił parę groszy i znalazł inwestora w Aurum.
* Anonimowo wykupił kolegę z praktycznie niewolniczego kontraktu płącąc ogromną sumę. Uznał, że sztuka kolegi jest tego warta.
* Manipulując dokumentami oraz opinią publiczną doprowadził do tego, że pomniejszy arystokrata nie trafił do więzienia za spowodowanie poważnego uszczerbku na zdrowiu.

### Kluczowe Zasoby (3)

* COŚ: Pozwolenie, papiery i dokumenty na każdą okazję. Większość z tych papierów i dokumentów jest nawet prawdziwa.
* COŚ: Drony filmujące i infiltratory. Byty służące do odpowiedniego filmowania i zapisywania wspomnień podczas "reality show".
* KTOŚ: Szeroka populacja która była ofiarami jego "filmów" i którzy dyszą chęcią zemsty, ale nie są w stanie niczego zrobić.
* WIEM: W jaki sposób pokazywać "fakty" tak, by zadowolić odpowiednie osoby w różnych grupach społecznych.
* OPINIA: Śliski koleś, który próbuje się wzbogacić na cudzej krzywdzie i próbuje się podlizać potężniejszym od siebie.

## Inne

### Wygląd

.

### Coś Więcej

.
