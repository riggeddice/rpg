## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Kamil Lyraczek"


## Kim jest

### Paradoksalny Koncept

Ludzki kaznodzieja Arianny, traktujący ją jak boginię. Człowiek wierzący w czarodziejkę. Człowiek, który przemowami ma większy wpływ na Pryzmat niż większość magów. Przejął dowodzenie nad grupką Zbieraczy gdy ich statek został ciężko uszkodzony i patrzył jak jeden po drugim umierają - ale utrzymał ich w wysokim morale. Wtedy uratowała go Arianna. Ta trauma zdefiniowała całą jego przyszłość.

### Motto

"Kosmiczny porządek wymaga, by pojawiła się Arianna Verlen i wszystkich uratowała."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Żarliwa wiara i absolutne przekonanie w swoje racje. Wolą przekracza granice ciała. Jest niekorumpowalny i posunie się absurdalnie daleko by zapewnić sukces Sprawie.
* ATUT: Mistrz przemów, gadania i porywania za sobą tłumów. Manipuluje Pryzmatem, ciągnie za sobą tłumy, ale też potrafi przekonać większość osób indywidualnie do swojej racji.
* SŁABA: Jest absolutnym pacyfistą. Nie tylko odmawia ranienia kogokolwiek, ale w ogóle nie chce zadawać cierpienia.
* SŁABA: Uzależniony od pewnych środków uspokajających. Gdy ich nie ma, miesza mu się czasem rzeczywistość z demonami przeszłości.

### O co walczy (3)

* ZA: Optymizm. Dobro. Ciepło. Pozytywne uczucia. Rzeczy, które sprawiają, że życie jest "dobre". Propagowaniem tego zajmował się odkąd stracił przyjaciół Zbieraczy.
* ZA: Odpowiedni PORZĄDEK na świecie - magowie u góry, ludzie poniżej, wszyscy inni poniżej. Świat winien być uporządkowany i bezpieczny.
* VS: Nie dopuścić do tego, by ktokolwiek umierał samotnie w ciemności próżni, tracąc resztki tlenu. Chce uratować wszystkich. Uratować jak największą ilość osób. 
* VS: Nie chce być bohaterem. Nie chce być zapamiętany. Chce działać z drugiej linii, móc być dumnym z piękna świata który pomógł zbudować.

### Znaczące Czyny (3)

* Przejął dowodzenie nad grupką Zbieraczy i dawał im nadzieję aż poumierali zginęli w Anomalii Kolapsu. Do samego końca zachowywał wszystkich w nadziei oraz się nie złamał.
* Podczas procesu Arianny Verlen doprowadził do tego, że została uznana za "działającą w słusznej sprawie". Znalazł adwokata i obrócił opinię publiczną na jej korzyść.
* Zebrał załogę Inferni. Przekonał, by Infernia trafiła do Arianny. To on "stworzył" nowe życie Arianny Verlen. By to osiągnąć oddał wszystko, odrzucił wszystko.

### Kluczowe Zasoby (3)

* COŚ: Środki uspokajające dobrej klasy. Potrafią wyłączyć z akcji lub skupić umysł i usunąć strach.
* KTOŚ: Spora grupa zelotów Arianny Verlen, w zdecydowanej większości ludzi. Regularnie utrzymuje z nimi kontakty. Większość jest na Inferni.
* WIEM: Doskonale manipuluje Pryzmatem i wie jak wzmocnić lub osłabić maga używając grupy oddanych mu ludzi.
* OPINIA: Nawiedzony jak cholera, ale ma niesamowicie gadane. Warto z nim porozmawiać, zna świetne opowieści i historie.

## Inne

### Wygląd

.

### Coś Więcej

.
