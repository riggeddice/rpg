## Metadane

* factions: "cieniaszczyt, lyse_psy"
* owner: "public"
* title: "Moktar Gradon"


## Postać

### Ogólny pomysł (3)

Potężna góra mięśni i złości, częściowo techorganiczny. Ciało zbudowane przez Saitaera, umysł uwolniony przez Arazille. Kapłan dwóch bogów, niewolnik żadnego. Mistyk wykorzystujący energię bogów. Terminator. Nieskończona wola walki i zniszczenia. Szturmowiec i gladiator, zna się perfekcyjnie na różnych typach broni. Charyzmatyczny i przerażający przywódca Łysych Psów.

Z uwagi na specyfikę (techorganiczny awatar dwóch bogów) **+1 kategoria w sprawach powiązanych z konceptem**

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* ENJOYMENT/POWER; wszystko podda się jego woli i jego siła przewyższy wszystko; robienie tego, na co ma ochotę i dewastacja wrogów
* ADAPTABILITY/FREEDOM; perfekcja ciała i wolność umysłu dla każdego; zaczynając od Łysych Psów a kończąc na świecie - explore, expand, evolve
* odebrali mu Karradraela; będzie mieć WSZYSTKICH bogów; zintegrować się ze wszystkimi tak jak z Arazille i Saitaerem
* bezwzględny, okrutny, warczący, wszystko sprowadza do konfrontacji, pogardliwy

### Wyróżniki (3)

* Military freak: jeśli jest cokolwiek do wiedzenia na temat broni i jej efektów w różnych okolicznościach, on to wie.
* Zmuszanie i zastraszanie: zna język siły i przymusu i doskonale potrafi go używać by dostać czego chce.
* Technomagiczne implanty: silniejszy, szybszy i potężniejszy; jest żywym triumfem magitechnologii i determinacji nad dobrym smakiem i człowieczeństwem
* Używa mocy bogów - Saitaera i Arazille, nie będąc przez nie Skażony.
* Używa mocy bogów w sposób niemożliwy dla maga, by zrobić rzeczy niemożliwe. Najczęściej to wolność Arazille czy furia Saitaera.
* Używając swojej charyzmy, intelektu i okrutnych mocy sprzęga i łamie nawet najsilniejsze istoty.
* Niepowstrzymany. Niezależnie od siły ognia, obrażeń, trucizny itp. Nie da się go powstrzymać. Moktar nie padnie.

### Zasoby i otoczenie (3)

* Szantaże, długi i plotki: zawsze może zmusić drugą stronę do stawienia mu czoła w bezpośredniej walce.
* Nielegalne wspomagania i środki: narkotyki, stymulanty i inne środki wspomagające w walce lub łamiące innych.
* Dowódca Łysych Psów: dowodzi oddziałem straceńców których trzyma karnie i brutalnie - słuchają go ze strachu. 
* Pancerny Power Suit: ciężko opancerzony power suit o dużej sile ognia, silny i wytrzymały acz niezbyt szybki.
* Niezliczone typy broni: military freak, zawsze ma mniej lub bardziej legalną broń radzącą sobie z danym problemem.

### Magia (3)

#### Gdy kontroluje energię

* Manipulacja bronią i sprzętem: military freak zdolny do adaptacji i przekształcania broni w zależności od sytuacji.
* Mroczna strona natury: potrafi wszystkich wepchnąć w stan berserk czy strachu. Ciemna strona emocji to jego strona.
* Last Mage Standing: potrafi leczyć się kosztem innych i regenerować kosztem innych. Niekoniecznie sojuszników.
* Potęga Saitaera i Arazille: potrafi przywołać energię bóstw i się nimi wzmocnić.

#### Gdy traci kontrolę

* Eruptor: jego gniew i nienawiść wpływają na niego lub innych, zwykle realizując się w berserku lub terrorze.
* Krwawiec: jego magia kieruje się w formę Krwi, działając potężniej, acz ze straszliwymi efektami ubocznymi.
* Strzelba na ścianie: rzeczy zdolne do zadawania bólu i ran zaczynają zadawać ból i rany (np. broń sama wystrzeli).
* Uwolnieni Bogowie: Saitaer lub Arazille manifestują swoją moc w tym obszarze.

### Powiązane frakcje

{{ page.factions }}

## Opis

Chyba najgroźniejsza postać jaką potrafię stworzyć w tym systemie.
