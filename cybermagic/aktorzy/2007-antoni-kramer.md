## Metadane

* factions: "orbiter, admiralicja orbitera"
* owner: "public"
* title: "Antoni Kramer"


## Kim jest

### Koncept

Admirał Orbitera o twarzy zmęczonego mopsa, specjalizujący się w ocenie ludzi i logistyce. Idealista - pragnie bezpieczeństwa i jedności Zjednoczonej Ludzkości. Widzi Orbiter jako miejsce pokojowej, bezpiecznej eksploracji i nieskończonych możliwości. Gdy trzeba, twardo podejmie właściwą decyzję, acz z żalem, nie delegując tego na innych.

### Motto

"Jeżeli przetrwamy do jutra, ale stracimy wszystko co czyni nas ludźmi - czy naprawdę przetrwaliśmy?"

### Co się rzuca w oczy

* Starszy czarodziej o twarzy bardzo zmęczonego mopsa, z przerzedzonymi włosami.
* Mówi spokojnie i powoli, choć czasami się unosi. Wygląda, jakby miał mało energii.
* Zawsze ma mnóstwo serca do swoich podopiecznych i podwładnych. Staje za nimi w potrzebie.
* Wzmacnia tematy związane ze stabilizacją Orbitera i jego bezpieczeństwem militarnym, politycznym, ekonomicznym.
* Bardzo moralny. Bardzo dba o to, by Orbiter nie był siłą zła i nie czynił zła.

### Przekonania

* Pomaganie innym i dawanie drugiej szansy to nie jest nasza słabość - to nasza siła z której powinniśmy być dumni.
* Naszym obowiązkiem jest znaleźć najsilniejsze strony naszych podopiecznych i umieścić ich tam, gdzie są najskuteczniejsi.
* Nieważne, czy jesteśmy z Noctis w stanie wojny, czy nie - są ludźmi i powinniśmy im pomóc jeśli trzeba.
* Orbiter musi się ustabilizować - wzmocnić flotę, łańcuchy dostaw i łańcuch dowodzenia.
* Indywidualizm, rywalizacja - tak, ale nie kosztem moralności i podstawowego człowieczeństwa.
* Zakazane energie, zakazane technologie są zakazane nie przez przypadek.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Znalazł Leonę Astrienko, tak potrzaskaną jak była i znalazł dla niej miejsce w Orbiterze. Potem dał ją Kamilowi jako warunek wyciągnięcia Arianny do chwały.
* Zobaczył w Ataienne coś więcej niż tylko Skażoną przez Saitaera TAI i wynegocjował z Aleksandrą Termią użycie Ataienne a nie rozebranie jej na kawałeczki.

### Z czym przyjdą do niej o pomoc? (3)

* Doskonały w ocenie ludzi - gdzie umieścić kogoś, by pokazał swoje najlepsze cechy.
* Nienaturalnie więc dobry w pozyskiwaniu sprzętu i zasobów oraz logistyce - jego podwładni zwykle mają dobry sprzęt w odpowiednim miejscu.
* Potrafi pogodzić ogień z wodą; dobrze mediuje i godzi interesy wielu stron. Jeden z najlepszych dyplomatów w Admiralicji.

### Jaką ma nieuczciwą przewagę? (3)

* Zna sporo bardzo kompetentnych magów Orbitera. Jest doceniany i lubiany za swoje humanistyczne podejście i starania.

### Charakterystyczne zasoby (3)

* COŚ: Sprzęt ponadprzeciętnej jakości w miejscach, w których jest potrzebny. Jego podwładni są dobrze wyposażeni i mają wysokie morale.
* KTOŚ: Ataienne uznaje go za jedyną nadzieję Orbitera. Pomaga mu z ukrycia jak jest w stanie - a nie jest w stanie bardzo, niestety.
* WIEM: Sieć agentów, którzy mówią mu o aktualnościach i dziwnych rzeczach które się pojawiają w Orbiterze. Ma rękę na pulsie.
* OPINIA: solidny koń roboczy, taki biurokrata, który chroni swoich i konsoliduje swoją pozycję w Orbiterze.

### Typowe sytuacje z którymi sobie nie radzi (-3)

* Zbyt próbuje pomóc innym w potrzebie. Zbyt często próbuje dać drugą szansę. Jest przez to przewidywalny i da się go wrobić w coś.
* Jest to dużo gorszy taktyk niż przeciętny komodor. Bądźmy szczerzy - w taktyce i dowodzeniu statkiem jest beznadziejny.
* Wobec swoich wychowanków i podwładnych ma bardzo łagodne podejście. Daje sobie wejść na głowę. Osłania ich, bierze problemy na siebie, nie ma ogromnych konsekwencji.

### Specjalne

* brak

## Inne

### Wygląd

* Starszy czarodziej o twarzy bardzo zmęczonego mopsa, z przerzedzonymi włosami.
* Mówi spokojnie i powoli, choć czasami się unosi. Wygląda, jakby miał mało energii.

### Coś Więcej

* 

### Endgame

* On: Antoni Kramer widzi dla siebie przyszłość jako edukator; zbuduje swoich następców, którzy będą podzielać jego ideały. 
* Ataienne: jej zdaniem, Antoni powinien przejąć władzę nad Orbiterem. On lub osoby z nim powiązane. Ten punkt widzenia musi być dominujący.
* Aleksandra Termia: zdaniem Aleksandry, Antoni powinien zostać w K1 i robić to co robi dobrze - administrować i zarządzać ludźmi. I żadnej władzy ustawodawczej.
