## Metadane

* factions: "cieniaszczyt, lyse_psy"
* owner: "public"
* title: "Bogdan Szerl"


## Postać

### Ogólny pomysł (3)

Medyk i doktor Łysych Psów, też mindwarp i ogólnie - neuronauta. Pasjonuje się w kontroli umysłów i dominacji innych. Nie jest wyznawcą Saitaera, ale podziela jego ideały. Na krótkiej smyczy Moktara. Naukowiec wysokiej klasy. Potrafi walczyć, acz nie jest w tym świetny.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* KNOWLEDGE/ADAPTABILITY; osiągnąć perfekcję taką jak głosi Saitaer; rekonstruktor i mindwarp - częste testowanie ludzkich granic
* CONTROL/BEAUTY; posiadać piękne, perfekcyjnie kontrolowane kształty ludzkie; zdobywa jeńców i ofiary i przekształca ich w lalki
* uśmiechnięty, lekko nerwowy, okrutny, karmi się dominacją, często się oblizuje

### Wyróżniki (3)

* Mindwarp: potrafi każdego złamać, ulojalnić czy przekształcić.
* Lekarz i naukowiec: skupia się na szybkim postawieniu bardziej niż na ograniczeniu cierpienia. Bardzo skuteczny w odnajdowaniu nietypowych rozwiązań
* Jeśli zostaje sam na sam z kimś i ta osoba jest w jego mocy, to bardzo szybko robi z tej osoby swojego wiernego sługę
* Mając trudny problem medyczny potrafi znaleźć bardzo skuteczne rozwiązanie. Jest niemoralne, ale bardzo skuteczne

### Zasoby i otoczenie (3)

* liczne artefakty paraliżujące, unieruchamiające, dominacyjne
* kolekcja upiornych holokostek Mausów; doskonałych na różne okazje
* stary kompleks medyczny; na terenach Skażonych na wschód od Cieniaszczytu. W połowie zakopany pod ziemią, ma opcje portali.

### Magia (3)

#### Gdy kontroluje energię

* dostosowywanie ciała i umysłu do swojej woli: jest to wyjątkowo unikalny sposób magii medycznej...
* wszelkie formy unieruchomienia bądź spętywania swojej ofiary, czy to fizycznie czy mentalnie
* magia skupiająca się na efektach takich jak: dominacja, kontrola, uwielbienie

#### Gdy traci kontrolę

* przebłyski szaleństwa; czy to u siebie czy innych - prawdziwe pragnienia czy JEGO pragnienia manifestują się w formie niemożliwych do kontroli impulsów

### Powiązane frakcje

{{ page.factions }}

## Opis

.
