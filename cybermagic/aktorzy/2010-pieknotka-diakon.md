## Metadane

* factions: "Pustogor"
* owner: "public"
* title: "Pięknotka Diakon"


## Kim jest

### W kilku zdaniach

Chemiczna terminuska, kochająca trzęsawisko Zjawosztup. Kosmetyczka.

### Co się rzuca w oczy

* Często chodzi po Trzęsawisku.
* Stanowcze, trochę smutne spojrzenie.
* Wysportowana, trochę żylasta.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Uśpiła Finis Vitae, łącząc moc Arazille i Saitaera, osobiście wchodząc w paszczę lwa.
* Opanowała Cienia siłą woli i ogromnymi stratami we krwi własnej.
* Trochę oswoiła Chevaleresse i wpakowała ją Alanowi.
* Przetrwała w pojedynku snajperskim z Walerią oraz przetrwała atak z zaskoczenia Kirasjerki.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Biochemia - trucizny i kosmetyki. Potrafi syntetyzować różnego rodzaju substancje, zna się na środkach pochodzących z Trzęsawiska
* PRACA: Kontrola terenu, manipulacja sytuacją. Pięknotka zawsze stara się ustawić otoczenie i sytuację na swoją korzyść. Świadoma sytuacji dookoła.
* SERCE: Absolutnie nie godzi się na jakiekolwiek formy nadużycia siły i władzy.
* SERCE: Między Trzęsawiskiem a Pustogorem. Szuka balansu między przeciwnymi światami.
* ATUT: Transorganik. Zintegrowana z Cieniem. Szybsza, silniejsza, groźniejsza niż cokolwiek innego.
* ATUT: Doskonały zwiadowca. Dzięki poruszaniu się po Trzęsawisku wszędzie wejdzie niezauważona.

### Charakterystyczne zasoby (3)

* COŚ: Karabin snajperski. Po przemianie zintegrowany z nią.
* KTOŚ: Sieć wsparcia terminusów Pustogoru (w szczególności Mariusz Trzewń)
* WIEM: Znajomość terenu, Trzęsawiska, całej okolicy.
* OPINIA: Bezwzględna, okrutna terminuska, która nie zawaha się zrobić krzywdy.

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: Wyjątkowo wrażliwa na srebro i Esuriit.
* CIAŁO: Potrzebuje regularnych pełnych transfuzji.
* SERCE: Walczy do końca, nie potrafi zrezygnować.
* SERCE: Szepty Saitaera. Musi je odpychać, co jest trudne.
* STRACH: Samemu nie nadużyć władzy. 
* STRACH: Nigdy nie dać się złapać, że jest tym, czym jest. 
* MAGIA: Jej magia jest niestabilna przez Ixion - nie może jej ufać.
* INNI: Gdyby się dowiedzieli, czym się stała, to by na nią polowali.

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .
