## Metadane

* factions: "pustogor, rekin, sowinscy, aurum"
* owner: "anadia"
* title: "Marysia Sowińska"


## Kim jest

### W kilku zdaniach

Dość psotna arystokratka bardzo skupiona na tym, by zachować neutralność i niezależność. Bardzo chce wyjść z cienia swojej kuzynki Amelii i bardzo irytuje ją porównywanie do niej. 

Nie chce wychodzić za mąż i chce zachować równowagę między siłami - oraz być największą rybą w stawie, nieważne jak mały staw by nie był.

### Co się rzuca w oczy

* Bardzo elegancko ubrana.
* Nie jest fanką krzywdzenia kogokolwiek (poza Torszeckim).
* Gdy musi, jest bezwzględna. Gdy może, jest sympatyczna.
* Zadaje się z osobami różnych stanów, niespecjalnie dba o zasady i etykietę.
* Ma tendencje do bardzo nietypowych i momentami ryzykownych działań. Przekonana, że sobie sama poradzi (ku rozpaczy osób z Aurum którym na niej zależy).
* Nie jest przekonana, że komuś z Aurum na niej zależy.

### Jak sterować postacią

* wybiera raczej ogólne dobro niż opinię o swoim życiu prywatnym (plotki można uciszać ;) )
* chce by się z nią liczono (przynajmniej na poziomie Pustogoru)
* będzie się starała jak najdłużej utrzymać status "Szwajcarii" (nie wybierać stron i nie pokazać co uważa)
* Marysia jak była mała miała jako większość przyjaciół koty. Stąd upodobała sobie koty jako kochaną formę i przyjaciół.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W Aurum, zwykle nie traktowano jej poważnie. By podsłuchiwać o czym inni mówią, zmieniała się w kota i zakradała w miejsca pozornie niemożliwe.
* W Aurum kolega Marysi był oskarżony o coś czego nie zrobił. Marysia wiedziała że jest niewinny (bo była z nim wtedy). Wściekła się, zebrała dowody, po czym publicznie go uniewinniła. Potem dostała OPR od możnych rodu, bo przez to ucierpiał ktoś inny kto był jej lojalny.
* Rafael Bankierz chciał dobrze wyjść za Marysię i zrobić z niej kochankę. Marysi się nie podobało jego podejście. Więc nie powiedziała "nie", ale tak manewrowała, że on się zbłaźnił strasznie a ona wysłała sygnał. "Nie bawcie się tak ze mną."

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: świetnie radzi sobie w przestrzeni (czy na ścigaczu czy jako osa). Istota powietrza; świetnie czuje się w powietrzu.
* AKCJA: doskonale czyta innych. Świetnie wykrywa intencje - czemu ktoś coś mówi czy robi.
* AKCJA: imperatrix: władcza i rozkazująca, gdy chce, potrafi ZMUSIĆ lub KAZAĆ. Ludzie się jej słuchają bo za nią stoi potęga rodu.
* AKCJA: wybitna aktorka i kłamca pierwszej wody. Powie to, co inni chcą usłyszeć a oni powiedzią jej to, co ona chce usłyszeć. W czym oni powiedzą prawdę. Mistrzowska manipulatorka.
* AKCJA: zwierzę polityczne: wie skąd wieje wiatr i kto, z kim, dlaczego i za ile. Umie te rzeczy rozgrywać.
* COŚ: bogactwo i reputacja wynikająca z Rodu Sowińskich. Jest w rodzie lubiana, acz nie jest uważaną za ważną.

### Serce i Wartości (3)

* Samosterowność
    * Nikt nigdy nie będzie jej mówił co ma robić. Będzie na szczycie albo wcale.
    * Lepiej być królem wśród Rekinów niż szprotką wśród Sowińskich 
    * Nie wolno odrzucać nietypowych, może skandalicznych możliwości i metod
* Potęga
    * Będę miała taki wpływ że to ja decyduje o losie innych a nie oni o moim.
    * Dopóki nie mam odpowiedniej mocy, będę „neutralna”. Rozgrywam innych.
    * Chronię innych i mam prestiż, bo oni budują moją potęgę - i są warci ochrony
* Uniwersalizm
    * Nieważne jakiego ktoś jest stanu, ma znaczenie tylko co wnosi i jaką jest osobą
    * Ja decyduje kto ma wartość i z kim się zadaję a nie jakieś zasady lub ktoś inny
    * Żadna organizacja nie może zdominować jednostki. Równowaga pomiędzy siłami.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: ""
* CORE LIE: ""
* ma dużą słabość do zwierzaków, nie pozwoli by robione była zwierzakom krzywda
* ma dużą słabość do bliskich osób (Karolina, NIE Torszecki) i im też pomoże i nie da zrobić krzywdy.
* da się ją skusić na bardzo ryzykowne manewry samym faktem, że nikt tego się nie spodziewa

### Magia (3M)

#### W czym jest świetna

* mentalistka: czytanie pamięci, przerażanie, kontrola uczuć, dominacja (kontrola umysłów, rozkazy)
* morfka: transformowanie siebie i innych w zwierzaki. Musi mieć prototyp ("coś co istnieje").
* bioaktorka: potrafi zmieniać wygląd - inne włosy, postura, wielkość, twarz...
* elementalistka Powietrza: latanie, poruszanie się w przestrzeni, wysysanie powietrza (bąbel próżniowy)
* tarcze mentalne i ukrywanie własnych emocji

#### Jak się objawia utrata kontroli

* osoby postronne zauważają jak osoby / wydarzenia co do których Marysia nie chce by były znaczące stają się znaczące ("Torszecki x Marysia = WM" lub "Amelia dużo zrobiła")
* manifestacja Powietrza jako tornada, wichry, "rzeczy lecą w powietrze" itp.
* istoty dookoła przekształcają się w słodkie kotki. Czasem łącznie z Marysią.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* .

### Endgame

* .
