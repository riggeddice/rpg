## Metadane

* factions: "blakenbauer"
* owner: "public"
* title: "Ula Blakenbauer"

## Funkcjonalna mechanika

v1

* **Obietnica postaci**
    * Będziesz tworzyć niesamowite potwory ("Płaszczki") dotykiem, animujesz i przejmiesz kontrolę nad istotami żywymi i materią
    * Tak, jak animujesz potwory tak możesz przejąć kontrolę nad żywymi istotami poprzez senti-infuzję
    * Możesz traktować wszystkich z góry i muszą zaciskać zęby, bo jesteś potężna i kapryśna
    * Nie będą Cię kochali, ale wszyscy będą Cię szanować i traktować jak primadonnę z uwagi na Twoją moc i możliwości
    * Twoja krew jest toksyczna i jest wektorem Twojej magii - płaszczkowanie i dominacja
* **Słabości postaci**
    * Zupełnie nie umie walczyć, w żadnej formie i postaci
    * Śmieszność gorsza niż śmierć, strasznie wrażliwa na krytykę
    * Samotność - nie ma przyjaciół, nie wierzy w przyjaźnie, chowa się za formalnościami
    * Overconfidence - jest przekonana że wszystko jest w stanie zrobić i zalać potężną energią magiczną
    * Jej magia ma zasięg dotyku. Nie brudzi rąk, wszystko robi płaszczkami
    * Bez magii w sumie nie ma wiele do zaoferowania
* **Zasoby postaci**
    * Bardzo elegancki niebojowy Lancer z przedłużeniem sentisieci
    * Blakenbauerowie "w domu" jej dostarczą czego potrzebuje i pomogą - póki nie jest blisko nich
* **Klucz do zrozumienia postaci**
    * "ona jest lepsza" - nieważne co się nie dzieje, Ula zawsze spróbuje przebić innych
    * "unikać śmieszności i pokazania słabości za wszelką cenę"
    * "hiperspecjalizowana czarodziejka Płaszczek i rekonstrukcji"

v2

* **Aspekty charakteru**
    * Siły: bardzo precyzyjna, praktycznie nieomylna w swojej domenie, nieobliczalna i pomysłowa, samodzielna i wytrwała, elegancka jak cholera, perfekcyjna tienka
    * Słabości: OVERCONFIDENCE, Blakenbauer > all, nie trzyma języka za zębami, unika śmieszności bardziej niż ran, nie wierzy w relacje poza formalnymi, BARDZO WRAŻLIWA NA KRYTYKĘ
* **Aspekty strategii / podejścia**
    * Siły: "summons" (infuzje sentisieci w płaszczki), mimiki wszelakie, adaptacyjne płaszczki, "nawet kamień jest płaszczką"
    * Słabości: wszystko chce rozwiązywać płaszczkami, nie brudzi rąk sama, hiperspecjalizacja, nie umie walczyć, magia tylko w zasięgu dotyku
* **Aspekty umiejętności**
    * Siły: płaszczki Blakenbauerów, ARCYMAG SENTI-INFUZJI KRWIĄ, TOKSYCZNA KREW, DEVIATOR KRWIĄ
* **Aspekty zasobów**
    * Siły: senti-infusion ("wszystko jest płaszczką"), Bardzo elegancki niebojowy Lancer z przedłużeniem sentisieci

## Kim jest

### Fiszka

* Ula Blakenbauer: czarodziejka, arcymag płaszczek, senti-infuser
    * (ENCAO:  -0+0+ |Nie cierpi 'small talk';;;;Nieobliczalna, nie wiadomo co zrobi| VALS: Universalism, Face >> Power| DRIVE: Duma i bycie uznaną)
    * styl: Raven z DC x Suspiria

### W kilku zdaniach

Elegancka, dumna hiperspecjalizowana arcymag Blakenbauerów o bardzo specyficznej mocy (płaszczki) i bez cienia zsocjalizowania. Patrzy na nie-Blakenbauerów z lekkim pobłażaniem. Dba o swój wizerunek za wszelką cenę.

* Motto: "Każdy problem da się rozwiązać odpowiednią płaszczką, jeśli jesteś dość dobry."

### Jak sterować postacią

* Wizja
    * JA: Ula jest podziwiana, proszona o pomoc i pomaga. I jej dziękują. Jej płaszczki są super pomocne i Ula jest kochana i podziwiana.
    * ŚWIAT: Nie ma szerszej wizji, nie wie jak chce by świat wyglądał.
* Dominujące Strategie
    * hiperspecjalizowane płaszczki, senti-infuzja, zaawansowane przemyślane plany
* Charakter
    * arogancka i elegancka, zawsze chce działać płaszczkami i "bardziej", nie oszczędza surowców i siebie | śmieszność jest gorsza od ran | musi być LEPSZA niż inni i podziwiana
* Wzory 
    * Purvis Viper ze Swat Kats (styl walki i działania) x Raven z DC (elegancja, containment mocy) x Suspiria (overconfidence, samotność, siła)
* Inne
    * .

### Serce i Wartości (3)

* Ocean
    * ENCAO: -0+0+
    * Nie cierpi 'small talk' w żadnej postaci, działa formalnie i unika pokazywania jakichkolwiek form ludzkiego zachowania.
    * Nieobliczalna i pomysłowa, potrafi niesamowicie zaskoczyć
    * Bardzo wrażliwa na krytykę; przeżywa te same starcia dziesięciokrotnie
* Wartości
    * TAK: Universalism, Face
    * NIE: Power
    * U: Nieważne czy jesteś płaszczką, człowiekiem czy magiem - jeśli jesteś stowarzyszony z Blakenbauerami (lub z nią), jesteś tak samo ważny.
    * F: Najważniejszy jest nieskazitelny wygląd, pokazanie poziomu kompetencji oraz to, by nikt nie pomyślał że jest od Ciebie lepszy.
    * F: Śmieszność i brak szacunku jest najgorszym, co może Cię spotkać.
    * P: Dla Uli nie ma znaczenia potęga i wpływy. Im większe wpływy, tym bardziej się nawet irytuje. Bawią się w bogów Blakenbauerskim życiem...
* Silnik
    * Odpłacić (dobrem i pomocą) za to, że dostała szansę
* Marzenie (jeśli inne niż Silnik)
    * Przyjaciel
* CORE WOUND: "Skonstruowali mnie jako perfekcyjny deviator. Byłam superbronią. Potem odebrano mnie moim twórcom. I nie wiem jak żyć."
* CORE LIE: "Jeśli zachowasz poziom formalny i nie ujawnisz swojej prawdziwej mocy, możesz mieć poprawne relacje." 

### Magia

#### Dominująca moc

* JEDNYM ZDANIEM: "potworyzator płaszzek" (Lady Lifegiver) - jej magia zakłada użycie krwi i/lub sentisieci, jest o mocy arcymaga.
    * Spłaszczkowanie ofiary (agresywna inwazja sentiinfuzji)
    * Spłaszczkowanie istoty / materii (sentiinfuzja, potworyzacja)
    * Dominacja płaszczki ("Każda istota wpadnie pod moją kontrolę...")
    * Senti-evolution (evolve self)

#### Jak się objawia utrata kontroli

* Echo przeszłości. "I am not THAT anymore!". Berserk. Shouting. Defiance.
* Niekontrolowana transformacja krwi / płaszczki poza kontrolą / niekontrolowana potworyzacja.

### Znaczące Czyny i Osiągnięcia 

* W służbie Kreatorów (Blakenbauerów) spłaszczkowała innego Blakenbauera i go całkowicie zdominowała.
* Stworzyła widocznego, wielkiego potwora, ale pokonała przeciwnika niewielkim trującym mimikiem na którego wroga naprowadziła.
* Źle zrozumiała zachowanie innych magów swojego rodu i potraktowała zwykłe siłowanie się "na serio". Stworzyła płaszczkę bojową. W tym momencie straciła szansę na przyjaciół XD.

