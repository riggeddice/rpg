## Metadane

* factions: "pustogor, rekin, terienak, aurum"
* owner: "public"
* title: "Daniel Terienak"


## Kim jest

### W kilku zdaniach

Brat Karoliny. 

### Co się rzuca w oczy

* ?

### Jak sterować postacią

* TODO

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* TODO

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: ?
* AKCJA: ?
* SERCE: 
* COŚ: 

### Serce i Wartości (3)

* Tradycja
    * Rozpaczliwie próbuje znaleźć środki na odbudowę umierającego rodu
    * Kodeks rycerski - mogę oćwiczyć chłopa, ale dziewczyny respektowane będą.
    * W istotnych sprawach zawsze po właściwej stronie - jest tien Aurum.
* Twarz 
    * Tylko siostrze pozwala na brak szacunku. Walczy o reputacje pełną siłą.
    * Lepiej, by się mnie bali niż kochali. WYJĄTEK: dziewczyny. Im wolno więcej. 
    * Bardzo dba o reputację doskonałego zwadźcy i świetnego kuriera. Nie odmawia pojedynków, prowokuje do nich.
* Potęga
    * Zdobyć wpływy, środki itp - musi uratować rod i chronić jego członków.
    * Zero sympatii i empatii wobec ludzi i viciniusów. Gdzie Karo widzi potencjał on widzi zasoby.
    * Buduje sojusze na lewo i prawo. Nieważne z kim, ważne jak mocne.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Moja sentisieć umiera. Jestem lordem - powinienem coś z tym zrobić. Wszyscy na mnie patrzą a ja jestem bezradny."
* CORE LIE: "Niezależnie od czegokolwiek to na mnie i tylko na mnie leży uratowanie sentisieci. Bez sentisieci jestem nikim i mój Ród umrze."
* ?

### Magia (3M)

#### W czym jest świetna

* ?

#### Jak się objawia utrata kontroli

* ?

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* On ma affinity do sentisieci rodu Terienak. On by był następnym patriarchą. Dlatego tak ciężko przeżywa jej umieranie - on to czuje.

### Endgame

* .
