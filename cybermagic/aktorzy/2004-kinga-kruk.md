## Metadane

* factions: ""
* owner: "public"
* title: "Kinga Kruk"


## Kim jest

### Paradoksalny Koncept

Artystka - fałszerz, pałająca ogromnym szacunkiem do oryginałów.

### Motto

"Skoro chcesz mieć coś ładnego to ci to dam w sensownej cenie. Nie każdy potrzebuje w życiu oryginału."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Tworzenie wiarygodnych imitacji i wszelkiego rodzaju oszustwa. 
* ATUT: Praca z dokumentami, wyszukiwanie ukrytych faktów.
* SŁABA: Walka. Nie i już.
* SŁABA: 

### O co walczy (3)

* ZA: Każdy załuguje na coś pięknego w życiu, a ja przy tym mogę zarobić.
* ZA: 
* VS: Kradzieże i handel skradzionymi oryginałami.
* VS: 

### Znaczące Czyny (3)

* Jedno z jej "okultystycznych" dzieł wisi w miejscowym ratuszu.
* 

### Kluczowe Zasoby (3)

* COŚ: Terenówka - czasem żeby zdobyć składniki trzeba pojechać w dziwne miejsca.
* KTOŚ: Sieć zaprzyjaźnionych dostawców elementów, składników itp do produkcji fałszywek.
* WIEM: 
* OPINIA: Najbardziej praworządna obywatelka w historii.

## Inne

### Wygląd



### Coś Więcej

