## Metadane

* factions: "szczeliniec, terminus, aurum, Arystokracja Aurum"
* owner: "public"
* title: "Gabriel Ursus"


## Kim jest

### Paradoksalny Koncept

Uczeń terminusa, który bardzo lubi sprzęt ciężki i TAI - ale elektronika go zawsze zawodzi. Charyzmatyczny arystokrata, który zwiał przed swoją eks by zostać terminusem. Bezbłędnie znajduje prawdę - acz to, co znajduje sprawia mu ogromne zgryzoty.

### Motto

"Każdy ma swoje miejsce w którym osiągnie mistrzostwo - i pomogę Ci znaleźć Twoje."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Szperacz. Potrafi wszystko wywęszyć czy wykryć, nawet sekrety. Wspiera się magią katalityczną - świadomie lub instynktownie. Jakoś zawsze dojdzie do prawdy.
* ATUT: Naturalnie przejmuje dowodzenie i ma posłuch u innych; wyjątkowo charyzmatyczny. To, że jest arystokratą Aurum jedynie pomaga.
* SŁABA: Nie radzi sobie dobrze z pilotażem pojazdów. Owszem, potrafi - ale jakoś ZAWSZE ma pecha i jeśli on pilotuje to stanie się coś złego. Dotyczy też servarów.
* SŁABA: Jest trochę zbyt uczciwy i honorowy - nie radzi sobie z intrygami i z kłamaniem. Jeśli ma udawać kogoś kim nie jest, to mu to strasznie nie wychodzi.

### O co walczy (3)

* ZA: Porządek i harmonia. Każdy ma swoje miejsce w społeczeństwie, nikt nie powinien się obawiać zgłosić problemu wyżej.
* ZA: Zrozumieć, co sprawia, że tak kochana przez niego technologia go "zdradza" i regularnie zawodzi. To jego wstydliwy sekret.
* VS: Degeneraci z Aurum. Jedną rzeczą jest zdrowa arystokracja, ale plugastwo należy wytępić. Jest terminusem, by chronić kuzynkę i ród.
* VS: Jeśli jesteś w stanie coś zrobić, masz obowiązek to zrobić. Nie możesz stać z boku. Acz długoterminowe cele są ważniejsze niż krótkoterminowe.

### Znaczące Czyny (3)

* Nie chcąc by komukolwiek stała się krzywda poszedł sam uratować swoją eks - dostał solidny wpiernicz, ale uratował jej honor i godność.
* Wykrył z niemałej odległości, że Sabina Kazitan włada zakazanymi rytuałami. Tylko z uwagi na rozkaz nie powiedział o tym nikomu innemu.
* Miał możliwość zyskania władzy i wpływów jeśli dołączy do Lemurczaka. Odmówił - zasady są ważniejsze. Plus, Lilia powinna rządzić, nie on.

### Kluczowe Zasoby (3)

* COŚ: Najlepszej klasy uzbrojenie i sprzęt elektroniczny. Taki, który go zawodzi. Ale - jest co najmniej o klasę lepszy niż alternatywny.
* KTOŚ: Lilia, jego ulubiona (i nieco dzika) kuzynka, która wie absolutnie wszystko o funkcjonowaniu rodów Aurum i skupiła się na rozbudowie majątku.
* WIEM: Niekwestionowany ekspert (teoretyk) od różnego rodzaju broni i pojazdów. Naprawdę, kocha ten temat. Po prostu mu to nie wychodzi.
* OPINIA: Sztywny, acz można na nim polegać. Zrobi to, co jest słuszne, nieważne ile osób przy tym nie ucierpi.

## Inne

### Wygląd

.

### Coś Więcej

.
