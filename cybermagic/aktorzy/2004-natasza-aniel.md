## Metadane

* factions: ""
* owner: "public"
* title: "Natasza Aniel"


## Kim jest

### Paradoksalny Koncept

Okultystka nie wierząca w magię.

### Motto


## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zna i potrafi wykorzystać rytuały "magiczne" pochodzące z folkloru. Demony, istoty lasu, nic co nadnaturalne nie jest jej obce.
* ATUT: 
* SŁABA: Nie jest mistrzem sportów - wysiłek fizyczny jest dla innych.
* SŁABA: 

### O co walczy (3)

* ZA: Skusi się na każdą możliwość zarobku, nieważne jak bardzo szemraną. Chciwa. 
* ZA: 
* VS: 
* VS: 

### Znaczące Czyny (3)

* Przeprowadziła z bratem Edwardem rytuał, który poszedł nie po ich myśli. W wyniku tego Edward zagninął.

### Kluczowe Zasoby (3)

* COŚ: Sklep. Bunkier z kolekcją okultystycznych ksiąg, przekazywanych w rodzinie z pokolenia na pokolenie. Mieszkanie na poddaszu.
* KTOŚ: Felicja, złodziejka i włamywaczka. 
* WIEM: 
* OPINIA: Ktoś, kto może załatwić nawet najdziwniejsze przedmioty.

## Inne

### Wygląd

Niebieskie oczy, rude włosy. 160 cm wzrostu, drobna, prawie dziecięca budowa ciała. Ostre kości policzkowe i wyraźnie zarysowane obojczyki. Jasna cera z nielicznymi piegami. Chłopięcy ubiór, najczęściej nosi ciasne, skórzane spodnie i obszerną koszulę z dopasowaną do talii kamizelką.

### Coś Więcej

