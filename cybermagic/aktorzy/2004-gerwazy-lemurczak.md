## Metadane

* factions: "Arystokracja Aurum, Podwiert"
* owner: "public"
* title: "Gerwazy Lemurczak"


## Kim jest

### Motto

"Mrówki nie powstrzymają lwa - a lwy nie dbają o opinię mrówek."

### Paradoksalny Koncept

Arystokrata Aurum, który nad służących woli indywidualistyczne podejście. Drapieżnik i łowca, działający ze swojego ścigacza - unikający za wszelką cenę pająków. Kolekcjonuje artefakty i anomalie, próbując je zrozumieć - preferuje moc bezpośrednią nad bycie szanowanym arystokratą. Świetny w walce i zawsze szuka godnego wyzwania, ale nie podporządkuje się rozkazom. Nie dba o ludzi czy innych magów, jest samotnikiem.

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Bardzo silny i szybki, odporny na przeciążenia. Co gorsza, świetna percepcja. Ciało na szczycie możliwości człowieka, homo superior.
* ATUT: Doskonale rozumie i wyczuwa technologię; nie tylko świetny pilot ale i dobry hacker.
* SŁABA: "Lwy gardzą mrówkami". Nie rozmawia, wydaje rozkazy. Nie jest dobry w negocjowaniu czy zaprzyjaźnianiu się.
* SŁABA: Zabrzmi to idiotycznie, ale boi się pająków. Zwłaszcza tych małych. Na szczęście niewielu o tym wie.

### O co walczy (3)

* ZA: Być znanym. Zostać legendą. Nikt nie przejdzie koło niego obojętnie. Im bardziej efektowny czyn, tym lepiej.
* ZA: Pozyskać anomalie, artefakty, przedmioty nienaturalne i niebezpieczne. Zrozumieć różne anomalie i umieć je wykorzystać.
* VS: "Mrówki próbujące pokonać lwa" - grupy miernot, które próbują mieć znaczenie w świecie, w którym potęga oznacza władzę.
* VS: Polityka nad umiejętności. Szanuje osoby które coś umieją, lubi wystawiać tych, którzy są silni przyjaciółmi na ekstremalne wyzwania.

### Znaczące Czyny (3)

* Dostał się na teren strzeżonego laboratorium niepostrzeżenie swoim ścigaczem, tworząc dywersję obrazami z kamer - których nigdy nie było.
* Pokonał w walce trzech innych arystokratów z Rekinów, gdy ci obrazili jego ród. Skutecznie zastraszył innych, by nie próbowali.
* Pomógł niewielkiej firmie rozwiązać problem potwora, ale potem odmówił wypełniania papierów - przez co mieli kłopoty.

### Kluczowe Zasoby (3)

* COŚ: Ścigacz dobrej klasy, bardzo szybki i dyskretny. Całkiem nieźle uzbrojony i wyposażony w maskowanie.
* KTOŚ: Kuzynka, Diana Lauris, prowadząca klub Arkadia. Owa kuzynka go nienawidzi, ale on jest potężniejszy od niej.
* KTOŚ: Posłuch w grupie "Latających Rekinów". Uważany za grubą rybę w tym klubie arystokratów.
* WIEM: W jaki sposób przejąć kontrolę nad większością drobnej elektroniki tak, by nikt tego nie zauważył.
* OPINIA: Bardzo niebezpieczny i arogancki mag, któremu nie powinno się wchodzić w drogę.

## Inne

### Wygląd

.

### Coś Więcej

.
