## Metadane

* name: Nocne Niebo
* shortdesc: Mafia noktiańska zaprojektowana przez Kajrata do ochrony i wspierania noktian po przegranej Inwazji na Astorię

## Serce
### 1. Koncept wysokopoziomowy

* "Rodzina". Mafia noktiańska zaprojektowana do ochrony i wspierania noktian po Inwazji na Astorię.
    * Działa jak pierwsze mafie sycylijskie XIX wieku - państwo w państwie, które chroni swoich i brutalnie egzekwuje lojalność.
* Nocne Niebo postrzega się jako alternatywne państwo podziemne. Nie jest celem zniszczyć Sojusz Letejski a pomóc noktianom. Alternatywne rządy.
* "Nie jesteśmy przestępcami. Jesteśmy żołnierzami w wojnie, która się jeszcze nie skończyła. Walczymy o przetrwanie Noktian w świecie, który chce nas zniszczyć."
* "Jeśli oni wszyscy nas odrzucili, zbudujemy dla nas lepsze jutro alternatywnymi sposobami".
* Ratują i pomagają noktianom. Astorianie to zasoby. Możesz zostać "honorowym noktianinem" będąc astorianinem.

### 2. Agenda

* **Głód**: "Imperium Noctis na Astorii! We shall rise! Nikt nie zostanie pozostawiony bez pomocy! Odrzucili nas, więc my odrzucimy ich."
    * **Stan Aktualny**: noktianie są rozbici, wielu jest zniewolonych, są obywatelami drugiej kategorii, astorianie się na nich mszczą za Inwazję.
    * **Stan Pożądany**: podziemne państwo noktian pod flagą Nocnego Nieba. Noktianie są bezpieczni i astorianie nie odważą się ich krzywdzić.
* Pomóż noktianom w potrzebie, chyba, że są zdrajcami
* Ukaraj wszystkich, którzy krzywdzą noktian niezależnie od powodu: dla korzyści własnych, dlatego bo to łatwe...
* Rozbudowuj sieć państwa podziemnego, by Nocne Niebo było wszędzie gdzie może być noktianin
* Asymiluj grupy przestępcze i zdobywaj zasoby. Państwo jest tylko tak silne jak siła którą może zapewnić potęgę
* Asymiluj noktian. Nieważne czy chcą czy nie - należą do Nocnego Nieba.
* Niechętnie, ale wspieraj astorian którzy wspierając noktian się narażają i coś stracili.
* Karaj zdrajców i apostatów w najokrutniejszy możliwy sposób, by zniechęcić potencjalnych zdrajców i apostatów.

### 3. Informacje dodatkowe

* Obszar działania: Astoria, Sojusz Letejski oraz Tereny Skażone przy Sojuszu Letejskim
* Czas działania: od Inwazji Noctis
* Struktura organizacyjna:
    * Struktura organizacji terrorystycznej: luźna federacja komórek z dyktatorem determinującym wizję i wysokopoziomową strategię, z Oficerami dowodzącymi komórkami.
    * Rozproszone Komórki Funkcyjne w rękach zaufanych Oficerów: Inkwizycja (policja wewnętrzna), inne komórki specjalistyczne
    * Komórki Nieba nie wspierające Nieba zostaną okrutnie zniszczone.
    * Centrala jest w Szczelińcu, blisko Terenów Skażonych.
    * Oficerowie mają autonomię na swoim terenie, ale Niebo wyznacza ogólną strategię. Dopóki przynoszą korzyści i robią co powinni - ich inne działania są tolerowane.
* Ekonomia:
    * Działania przestępcze. Przemyt, haracze.
    * Podatki od noktian. Legalne firmy wykonujące rzeczy, które są Niebu potrzebne.
* Relacje i kluczowi partnerzy:
    * Sojusz Letejski: Wszelkie siły dowodzące, przywódcze, policyjne itp. nie są uznawane. Czasem jednak Niebo z nimi współpracuje chroniąc noktian czy przeciw Anomaliom. Zimna wojna, ale żadna strona nie chce zniszczyć drugiej.
    * Niestowarzyszeni noktianie: Niebo uważa ich za zasób i "swoich ludzi". Noktianie nie mają wiele do powiedzienia w tej dziedzinie. W większości noktianie się Nieba boją, niektórzy współpracują z chęcią.
* Reputacja i plotki
    * Nocne Niebo jest uznawane za bardzo niebezpieczną, okrutną organizację, ale taką, która nie interesuje się niczym poza pomocą i ochroną noktian.
    * Niebo nie zrobi Ci krzywdy, jeśli się nie sprzeciwiasz 

### 4. Zasoby i aspekty

* Zbrojne gangi: sporo noktian było uzbrojonych i wyszkolonych. Nadal ci ludzie są dostępni dla mafii.
* Ukryci agenci: nigdy nie wiesz, który noktianin jest agentem Nieba z własnej woli lub z przymusu.
* Szlaki przerzutowe: Nocne Niebo potrafi zdobyć i przerzucić nieprawdopodobnie dziwne rzeczy, dziwnymi trasami.
* Skrytki sprzętu noktiańskiego: po Inwazji poukrywano sporo sprzętu na różnych terenach.
* Poddane Biznesy: część to przykrywki, część płaci haracz, część dostarcza sprzętu.
* Eksperci noktiańscy: fałszerstwa, dokumenty, wiedza specyficzna. Duża populacja noktian na Astorii to kompetentni ludzie.
* Ogromne rozproszenie: co prawda głową jest Kajrat, ale działają jak grupy partyzanckie i komórki terrorystyczne.
    * "Każdy czyn dokonany w imieniu Nocnego Nieba jest czynem konstytuującym nowy porządek, nieważne kim jesteś."

### 5. Szczególne słabości

* Brak pozytywnej wizji przyszłości. "Podziemne państwo" to za mało. Co, jakby dało się zintegrować? Nie ma na to odpowiedzi. To powoduje rozwarstwienie Nieba i poważne konflikty wewnętrzne.
    * Kajrat jest genialnym oficerem logistyki działającym z musu chwili, nie wizjonerem. A nikt inny nie ośmieliłby się przedstawić takiej wizji.
* Ksenofobia: obustronna. 
    * Oni ufają tylko noktianom, ale noktianie przez istnienie Nieba nie mają łatwości integracji na Astorii.
    * Nikt nie będący noktianinem nie ma powodu wspierać Nocnego Nieba z własnej woli.
* Bardzo mało magów: mimo świetnego intelu i ogromnego rozproszenia, brak magów poważnie osłabia ich możliwości wobec anomalii.
* Starcia wewnętrzne: Czy Nocne Niebo chce Noktian zjednoczyć, czy podporządkować? Różne siły uważają inaczej.
* Zdrady w szeregach: astorianie i noktianie, którzy próbują infiltrować Niebo (lub np. magią mentalną wymusić info od Nieba). To wymusza OKRUCIEŃSTWO i BEZWZGLĘDNOŚĆ ze strony Nieba, co powoduje spiralę radykalizacji.
* Wielu noktian, zwłaszcza po roku 0110 nie chce wspierać Nocnego Nieba - nie tędy droga, lepiej się integrować.
* Brutalność jako spirala radykalizacji: ich taktyki zwiększają nienawiść astorian (oraz części noktian), co utrudnia im działanie na dłuższą metę.

### 6. Pokusy i domyślne Porażki
#### 6.1. Bonusy

* GDY potrzebny jest specjalistyczny sprzęt, TO Nocne Niebo może go przerzucić lub zapewnić noktiański.
* GDY potrzebna jest siła ognia, TO Nocne Niebo ma do dyspozycji siły: od lokalnych zbirów aż do komandosów.
* GDY trzeba odwrócić czyjąś uwagę, TO Nocne Niebo ma do dyspozycji wiele zasobów.
* GDY trzeba kogoś ukryć, TO Nocne Niebo ma kryjówki i zna ich większość.
* GDY trzeba zdobyć coś od noktianina, TO Nocne Niebo zapewni wiedzę lub 'przekona' noktianina do współpracy.
* GDY działasz na obcym terenie, TO Nocne Niebo może zbudować Ci odpowiednią reputację i zapewnić społeczne wsparcie i wiedzę.

#### 6.2. Malusy

* GDY współpracujesz z Niebem, TO zwracasz na siebie uwagę innych grup przestępczych, władz astoriańskich itp.
* GDY współpracujesz z Niebem, TO zaciągasz dług u organizacji przestępczej. Masz pewne zobowiązania - musisz spłacić.
* GDY agent Nieba ma kłopoty, TO jesteś zobowiązany mu pomóc. Nie zostawia się "swoich".
* GDY wykorzystujesz zasoby i wsparcie Nieba, TO możesz przypadkowo być wplątany w ich większe plany lub działania.
* GDY Niebo działa na terenie aktywnie, TO zaostrzają się konflikty astorianie - noktianie.

## Inne

.
