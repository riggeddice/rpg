## Metadane

* name: Kwietnicy Lotosu
* shortdesc: Sprzężeni symbiotycznie (?) z Kwiatem Lotosu ludzie dążący do harmonii dla wszystkich kosztem człowieczeństwa i ludzkiej natury.

## Serce
### 1. Koncept wysokopoziomowy

* Sprzężeni z kwiatami ludzie z różnych frakcji, którzy posłuchali pieśni kwiatów i dołączyli do Kwietników.
* Dominująca Lokalizacja: Wolne Enklawy przy Szczelińcu
* Ogólnie łagodni i pomocni, ale żądają wzmacniania ekosystemu w którym Kwiaty mają priorytet i mogą się rozwijać.
* "Niech Kwiat oplecie świat. Zero wojen. Zero cierpienia."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
