## Metadane

* name: Wolni Miasteczkowcy Pustogoru
* shortdesc: Grupa anarchistów, pionierów i przeciwników dekadiańskiej filozofii wypchniętych na południe od Pustogoru, żyjących jak na 'Dzikim Zachodzie'

## Serce
### 1. Koncept wysokopoziomowy

* Luźno rozproszona i skłócona wewnętrznie frakcja indywidualistów działająca wśród nomadów oraz Miasteczka Pustogoru.
* Zdecydowanie obróceni przeciwko 'tyranii terminusów' oraz Pustogorowi, chcą bardziej wolnego i niezależnego życia.
* Nie zgadzają się na to, by Pustogor decydował jak żyć. Bardzo pionierskie podejście. "Dziki Zachód".
* "Wolność i niezależność. Wy miejcie tą swoją 'cywilizację', my działamy po naszemu - zdrowiej."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
