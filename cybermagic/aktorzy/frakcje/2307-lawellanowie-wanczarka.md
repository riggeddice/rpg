## Metadane

* name: Lawellanowie Wańczarka
* shortdesc: Oddani Samszarom przez Blakenbauerów drakoliccy łowcy ludzi i hodowcy niebezpiecznych roślin

## Serce
### 1. Koncept wysokopoziomowy

* Stworzeni przez Blakenbauerów do hodowania mandragor przez Samszarów, oddani Samszarom, osadnicy Wańczarka.
* Hodowcy i Alchemicy Koncentratu Oświecenia - eliksiru łączącego wiedzę szamanów z naukową wiedzą o funkcjonowaniu mózgu, aby pobudzać kreatywność, inspirację i oświecenie umysłu
* "Jesteśmy zabawkami w rękach bogów, ale nadal jesteśmy lepsi od zwykłych ludzi."
* "My hodujemy mandragory dla bogów. Jaka jest Twoja rola w życiu, człowieku?"

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
