## Metadane

* name: Boronici Przelotyka
* shortdesc: Niekontrolowana agresywna grupa przelotyckich górali i pionierów, żyjących niezależnie i działających jak ośmiornica mafijna.

## Serce
### 1. Koncept wysokopoziomowy

* Grupa sybrian z korzeniami przelotyckimi, którzy żyją na własną rękę, współpracując ze 'swoimi' i ufając tylko swoim.
* Działają jak bardzo rozproszona organizacja przestępcza, współpracując ze swoimi subklanami. Bardzo cenią lojalność i tradycję.
* Bezwzględni w walce, wspierają swoich. Subgrupki są nomadyczne. Trudno się do nich dostać, trudno ich opuścić.
* "Rodzina nade wszystko. Rodzina pochodząca z zimnego wiatru."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
