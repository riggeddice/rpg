## Metadane

* name: Czarne Czaszki Enklaw
* shortdesc: Śmiertelnie niebezpieczni łowcy nagród, uzbrojeni i przystosowani do świata nomadów na niebezpiecznych terenach Szczeliny.

## Serce
### 1. Koncept wysokopoziomowy

* Łowcy nagród i nomadzi, 'roaming bands' działający na terenach gdzie żaden człowiek nie powinien działać. Salvagerzy zagłady.
* Dominująca Lokalizacja: Wolne Enklawy przy Szczelińcu
* Uzbrojeni i zaadaptowani do tego świata, doskonale sobie radzą niewidoczni, cisi i niewykrywalni.
* Odrzucili nadzieję, że da się osiągnąć coś cywilizacją i wracają do klasy hunter-gatherer.
* "Silni będą silni. Słabi będą służyć lub posłużą jako pożywienie."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
