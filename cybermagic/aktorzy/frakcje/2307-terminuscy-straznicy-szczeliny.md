## Metadane

* name: Terminuscy Strażnicy Szczeliny
* shortdesc: dekadianie astoriańscy, którzy przejęli kontrolę nad Pustogorem i większością Szczelińca, trzymający autorytarnie władzę i kulturę

## Serce
### 1. Koncept wysokopoziomowy

* Dekadianie dążący do maksymalnej defensywy i ostrożności, chroniący terenu przed anomaliami; z militarystycznymi rządami terminusów.
* Unikają współpracy z TAI wyższego stopnia, zupełnie im nie ufają ("wszystko może ulec Skażeniu"). Nie stoją po stronie noktian.
* Bezwzględni, ale sprawiedliwi i naprawdę chcą jak najlepiej dla Szczelińca. Ich ośrodkiem jest Pustogor.
* "Nieważne, że życie jest szare i nie masz swobód. Ważne, że jesteś bezpieczny i wszyscy przetrwali."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
