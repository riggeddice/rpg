## Metadane

* name: Agencja Terriletea
* shortdesc: Astoriańska agencja ubezpieczeniowa skupiająca się na odbudowie terenu po wojnie i dbająca proaktywnie o teren.

## Serce
### 1. Koncept wysokopoziomowy

* Astoriańska agencja skupiająca się na pomocy ludziom i odbudowie terenu, działająca po Inwazji Noctis. 
    * Terriletea specjalizuje się szczególnie w Anomaliach i waśniach sąsiedzkich.
* Jest to agencja ubezpieczeniowa, która ubezpiecza całość okolicznego terenu i przeznacza tam opiekuna. 
    * Funkcją opiekuna jest monitorowanie zagrożeń i usuwanie ich zanim coś się stanie.
* Terriletea działa przez luźne grupy konsultantów reagujących na prośby opiekunów terenu, by usuwać problemy uszkadzające naturalny proces powojennej odbudowy, jak konsultanci i opiekun uważają za właściwe.
* Ich misją nadrzędną jest prosperity lokalnego terenu i usuwanie problemów zakłócających odbudowę. Na tym zarabiają - jako kolejna warstwa "zabezpieczenia terenu".
* "Pomożemy Ci odbudować to, co wojna i Anomalie Magiczne zniszczyły. Z nami będziesz żyć w lepszym miejscu."

### 2. Agenda

* **Głód**: "Jeśli nie możemy żyć bezpiecznie i się bogacić, zbudujmy z pomocą Aurum świat, w którym ludzie są szczęśliwi i bezpieczni. Odzyskajmy świat od Anomalii."
    * **Stan Aktualny**: Nie wszędzie da się bezpiecznie mieszkać i często dobrych ludzi dotyka krzywda; mamy do czynienia z problemami i Anomaliami. Teren jest Skażony i niebezpieczny.
    * **Stan Pożądany**: Ludzie mogą mieszkać spokojnie i szczęśliwie. Biznes się kręci. Nie ma szczególnych problemów. Sojusz Letejski ma kolejny poziom prosperity. A Aurum jest chwalone za pomoc.
* Promuj odbudowę i współpracę lokalną, integrację i jedność. Jesteśmy świeżo po wojnie, najważniejsze jest bezpieczeństwo i dobrobyt.
* Promuj Aurum jako centralną siłę dzięki dobrej, uczciwej pracy i pomocy innym. Nie nachalnie, ale konsekwentnie.
* Akceptuj lokalną kulturę i zwyczaje nawet, jeśli nie są szczególnie pożądane czy moralnie słuszne. Mamy swoją misję. Krok po kroku.
* Akceptuj konieczność współpracy z kimkolwiek, kto w jakimś stopniu stabilizuje okolicę. Sytuacja w chwili obecnej jest zbyt delikatna.
* Wspieraj robienie tego, co dobre i tego, co należy robić. Mimo wszystko nie chodzi o stabilizację a o DOBRĄ stabilizację. Tymczasowa destabilizacja nie jest tak zła.
* Usuwaj nadmiar negatywnych emocji i przyczyny tych emocji z okolicy. Duże emocje i niestabilny teren spowoduje kolejne katastrofy magiczne.
* Usuwaj Anomalie Magiczne i problemy ze Skażeniem z okolicy. To są główne przyczyny tego, że ludzie są przecież zagrożeni.

### 3. Informacje dodatkowe

* Obszar działania: Astoria, Sojusz Letejski
* Czas działania: od Budowy Sojuszu Letejskiego
* Struktura organizacyjna:
    * Struktura Dywizjonowa: Centrala w Aurum (powiązana z rodem Sowińskich) - Dywizjony lokalne - Opiekunowie Terenów
    * Współdzielone zasoby: Grupy Agentów, współdzielone usługi (np. "logistyka", "eksperci od rzadkich Anomalii")
* Ekonomia:
    * Główne finansowanie pochodzi z Aurum.
    * Ubezpieczane są obszary, nie osoby. Pieniądze stamtąd są też wskaźnikiem dla Centrali jak sobie Opiekunowie Terenu radzą.
* Relacje i kluczowi partnerzy:
    * Aurum: fundatorzy oraz główne źródło ekspertów i sprzętu
    * Przelotyk: główny obszar działania, większość agentów w tej okolicy
    * Szczeliniec: opozycja polityczna; popierają cele, ale są przeciwni Aurum i potencjalnej hegemonii Aurum
    * Lokalne władze: główny partner Terriletei; często jest to też siła rywalizująca z Terrileteą (każdy chce pokazać "to dzięki mnie jest dobrze")
    * Lokalni destabilizatorzy status quo: nieważne, czy mają rację czy nie, przez sam fakt destabilizacji są w opozycji do Terriletei która jest siłą spajającą i harmonizującą.
* Reputacja i plotki
    * Ogólnie bardzo dobry standing. Terriletea jest wspierana i uważana za bardzo wartościową.
    * Szczeliniec (i nie tylko) rozpuszcza i podsyca plotki, że Terriletea to marionetka Aurum i próba zniewolenia Sojuszu Letejskiego przez Aurum

### 4. Zasoby i aspekty

* Pieniądze płynące z Aurum, ekspertyza grupy tienów (magów-arystokratów) których misją jest odbudowa i budowanie jedności całego Sojuszu Letejskiego.
* Bardzo duży kredyt zaufania i dobra reputacja. Terriletea nie przeszkadza siłom lokalnym, nie zmienia ich sposobu rządzenia. Celem Terriletei jest jedynie ochrona dobrobytu ludzi na danym terenie.

### 5. Szczególne słabości

* Technicznie, Terriletea niewiele MOŻE. Nie mają uprawnień formalnych, choć zwykle się z nimi współpracuje. To konsultanci i filantropowie, nie siła dominująca.
* Duże rozproszenie i zaufanie do swoich członków. Teoretycznie niewiele stoi na przeszkodzie, by ktoś wybrał sobie "bezpieczny teren" i z niego profitował. Terriletea nie ma dobrych mechanizmów kontrolingowych.
* Terriletea ma ludzi silnie zmotywowanych wewnętrznie. Wspieranie kultur czy grup "nieetycznych" może być dla nich moralnie szkodliwe. Agenci Terriletei mogą stanąć zgodnie ze swoją moralnością, nie tym, co należy.
* Aurum jest siłą generalnie pozytywną, ale jednocześnie sama obecność Aurum jest często problematyczna. Terriletea może być klasyfikowana jako narzędzie propagandy Aurum i tak traktowana.

### 6. Pokusy i domyślne Porażki
#### 6.1. Bonusy

* GDY działamy z lokalnymi strukturami, TO reputacja i historia Terriletei jest bardzo pomocna i jest silnym dowodem społecznym wartości agentów.
* GDY potrzebny jest typowy sprzęt średniej klasy, TO Terriletea ma do niego dostęp i go zawsze bez kłopotu zapewni.
* GDY potrzebna jest ekspertyza anty-anomalna lub wiedza lokalna, TO Terriletea wie z kim rozmawiać lub może ową ekspertyzę dostarczyć.

#### 6.2. Malusy

* GDY działamy z lokalnymi patriotami czy seperatystami, TO Terriletea jest uważana za próbę integrowania i hegemonii, utrzymująca status quo za cenę dobra.
* GDY lokalna społeczność ma wewnętrzne konflikty, TO Terriletea może być postrzegana jako narzędzie narzucania zewnętrznej kontroli.
* GDY pojawia się konieczność interwencji militarnej, TO Terriletea jest unikalnie kiepska w pomocy swoim agentom. Z definicji jest "rozbrojona".

## Inne

* .
