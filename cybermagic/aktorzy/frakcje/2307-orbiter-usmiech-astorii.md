## Metadane

* name: Orbiter Uśmiech Astorii
* shortdesc: Orbiterowa subfrakcja wierząca w ścisłą współpracę z Astorią przeciw Syndykatowi Aureliona. Dlatego współpracują z Wolnym Uśmiechem i Cieniaszczytem..?

## Serce
### 1. Koncept wysokopoziomowy

* Grupa pragmatycznych Orbiterowców pod kontrolą wielu admirałów; ukryta subfrakcja
* Współpracują przede wszystkim z Wolnym Uśmiechem, ale nie pogardzą też innymi grupami
* W większości atarieni, szukają przyjemności, zasobów i lepszego jutra dla wszystkich
* "Jeśli my nie wejdziemy w sojusz z Astorią, Syndykat to zrobi. Orbiter i Astoria powinny ściśle współpracować."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
