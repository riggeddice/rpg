## Metadane

* name: Oneiritalis
* shortdesc: ruch w kosmosie skupiający się na silnej wirtualizacji, wolności osobistej i memetycznej i znalezieniu perfekcji dla każdego sapienta

## Serce
### 1. Koncept wysokopoziomowy

* Potężny ruch wirtualizacyjny skupiający się na najlepszym możliwym dopasowaniu ludzi do problemu.
* Ich misją nadrzędną jest pomoc ludziom znaleźć perfekcyjne dopasowanie w tym czego szukają. Czasem lekka rekonstrukcja ludzi.
* Dominująca metakultura to orilianie (surprise...). Skupiają się na maksymalizacji rozrywki i wydajności.
* Nie współpracują z Syndykatem Aureliona, praktycznie nigdy. Ich zdaniem Syndykat stanowi JEDNOLITE rozwiązanie, co zawsze jest błędne.
* Wierzą, że wszystko można poprawić a rzeczywistość to tylko kolejne ograniczenie, które należy przezwyciężyć.
* "Pomożemy Ci znaleźć to, w czym jesteś najlepszy i dopasować to co winieneś robić do Twojej prawdziwej natury"

### 2. Agenda

* **Głód**: "Jeśli jesteśmy zniewoleni w realu, uwolnijmy się przez virt. Jeśli możemy tylko to na co nam pozwalają, chodźmy w stronę wolnej woli i szerokiej ludzkiej swobody"
    * **Stan Aktualny**: Orbiter jest jedyną siłą przy Astorii i blokuje możliwości innych, Aurelion zniewala inaczej a ludzie są skazani na działanie zgodnie z czyimś życzeniem
    * **Stan Pożądany**: Oneiritalis stanowi prawidłową alternatywę dla Orbitera czy Aureliona, Sztuczne Intelekty mają prawa jak ludzie, wolna wola i swoboda dla wszystkich
* Promuj wirtualizację, szerokie wykorzystywanie TAI i zaawansowane mechanizmy technologiczne.
* Pokazuj alternatywy dla dominujących metakultur, podejścia Orbitera oraz Aureliona.
* Wspieraj Program Kosmiczny Aurum, ruchy Sowińskich itp. Wspieraj możliwości poszerzania wpływów mniejszych frakcji.
* Promuj i propaguj różnego rodzaju gry komputerowe, symulacje i wykorzystywanie virtu, niekoniecznie tylko dla zabawy.
* Akceptuj problemy i niedogodności związane z różnorodnością ludzkich charakterów. Każdego można ukarać w vircie, niech służy.
* Usuwaj niewolnictwo, wymuszenie pracy itp. Wolna wola, swoboda, szerokość idei i podejść jest najważniejsze.
* Dyskretnie zwalczaj hegemonię i zbyt silne i jednorodne systemy. Po integracji wewnętrznej takie siły zawsze obracają się na zewnątrz.
* Promuj możliwość, by każdy był zawsze uzbrojony i był w stanie bronić się niezależnie i wspierać innych niezależnie

### 3. Informacje dodatkowe

* Obszar działania: Sektor Astoriański. Rzadko bliżej niż Neikatis (2 AU od gwiazdy i dalej). Siła kosmiczna, mogą mieć placówki planetarne i planetoidowe.
* Czas działania: Byli zawsze.
* Struktura organizacyjna:
    * Struktura Rozproszona: Bardziej "luźne komórki partyzanckie" różnej wielkości. To bardziej ruch społeczny niż jedna organizacja.
    * Różne komórki i grupy Oneiritalis mogą mieć sprzeczne cele. Ich głównym problemem (własnością?) jest koordynacja.
* Ekonomia:
    * Prywatne zarabianie. Zwykle na podstawie Zasobów i tematów wirtualizacji rzeczywistości i symulacji.
* Relacje i kluczowi partnerzy:
    * Orbiter: rywal; Orbiter reprezentuje wszystko, z czym wolnościowcy z Oneiritalis się nie zgadzają. Bezpieczeństwo nie może zmiażdżyć różnorodności i tysięcy kolorów świata.
    * Syndykat Aureliona: rywal; mimo optymalizacji i dopasowywania ludzi do ról Oneiritalis po prostu nie zgadza się na niewolnictwo. Efektywność nie może zmiażdżyć wolności.
* Reputacja i plotki
    * Oneiritalis są uznawani za "zasadniczo niegroźnych", ale o ogromnej amplitudzie problemów przez brak kontroli nad społeczeństwem, magią i TAI.
    * "Oneiritalis? Możesz z nimi współpracować, ale uważaj. Nigdy nie wiesz, czego się spodziewać."

### 4. Zasoby i aspekty

* Zaawansowane virtgry i virtsymulacje: niesamowicie zaawansowani psychotronicy tworzący odpowiednie virtświaty sprawiają, że mają symulacje 'na wszystko'.
* Autonomiczne Syntetyczne Intelekty: niezgodnie z zasadami Orbitera i większości Astorii, oni wspierają i mają wsparcie ze strony SI. Łącznie z autostatkami kosmicznymi.
* Dobra wola randomów: Oneiritalis wspiera mniejsze grupki i organizacje i handluje ze wszystkimi. Ludzie którzy pragną wolności nad bezpieczeństwo lubią Oneiritalis.
* Szerokie szlaki handlowe: Oneiritalis jest w stanie pozyskać bardzo dużo z bardzo wielu stron i miejsc. Nie mają wielu tabu, choć są przeciwnikami niewolnictwa.

### 5. Szczególne słabości

* Brak kohezji wewnętrznej: Oneiritalis wygląda jak jedna organizacja, ale w praktyce jest serią suborganizacji płacących podatki do centrali. Bardziej rozbici niż Orbiter.
* Brak centralnej floty: Doktryna wojskowa Oneiritalis sprawia, że nie mają możliwości walki z innymi potęgami jako duża siła. Brak im ciężkich jednostek i 'staying power'. Są jak sieć.
* Niestabilne jednostki: czy to SI, czy to statki kosmiczne, czy byty anomalne - niektóre są niestabilne, nie można im ufać, za stare, mają obce agendy...
* Nikt niczego nie kontroluje: Brak przywództwa oznacza, że lokalne komórki mogą się zwalczać. Nie ma "siły nadrzędnej". Nie ma jednoznacznej procedury rozwiązywania konfliktów. Chaos.

### 6. Pokusy i domyślne Porażki
#### 6.1. Bonusy

* GDY trzeba zrobić coś nieoczekiwanego, TO Oneiritalis jest w stanie odmrozić odpowiednią SI i to rozwiązać łatwiej niż ktokolwiek inny.
* GDY trzeba przeprowadzić symulację, TO Oneiritalis najpewniej ma jakąś gotową lub najprościej ją zaprojektuje i znajdzie.

#### 6.2. Malusy

* GDY współpracujesz z Oneiritalis, TO Orbiter/Syndykat/inne frakcje nie wiedzą czy Ci można zaufać i czy jesteś z tych 'stabilnych'
* GDY współpracujesz z Oneiritalis, TO Ty też nie wiesz czy Twoi sojusznicy są z tych 'stabilnych' i godnych zaufania
* GDY coś idzie nie tak, TO często inne siły Oneiritalis są za daleko by można było zdobyć ich wsparcie czy pomoc

## Inne

* .
