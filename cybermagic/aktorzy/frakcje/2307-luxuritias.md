## Metadane

* name: Luxuritias
* shortdesc: Potężna legalna firma skupiona na dostarczaniu wszelkich form przyjemności za wysoką cenę.

## Serce
### 1. Koncept wysokopoziomowy

* Firma franczyzowa mająca macki w różnych sektorach. Czasem pracują z Syndykatem, czasem z Kultem, czasem sami.
* Ich misją nadrzędną jest dostarczenie luksusowych rozwiązań na problem pustki egzystencji. Przyjemność i spełnienie.
* "Jeśli czegoś pragniesz, otrzymasz to, jeśli jesteś w stanie zapłacić."
* "Kult Ośmiorcnicy Cię zniewoli. Aurelion Cię oszuka. My faktycznie dostarczymy rozwiązanie Twych marzeń"

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
