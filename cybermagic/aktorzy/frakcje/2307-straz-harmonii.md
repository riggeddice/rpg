## Metadane

* name: Straż Harmonii Szczelińca
* shortdesc: Grupa szczelińskich najemników, niechętnie działająca z magami i skupiająca się na walczeniu z anomaliami.

## Serce
### 1. Koncept wysokopoziomowy

* Post-dekadiańska grupa samopomocowa która powstała w Podwiercie; miejscu pomiędzy Zaczęstwem (i AMZ) a Szczeliną Pustogorską.
* Uzbrojeni i mający wsparcie wśród okolicznych ludzi, bardzo silnie pomagają lokalsom przeciwko wszystkiemu co anomalne i magiczne.
* Grupa paramilitarna z ogromną ilością sympatyków, szczególnie ścierająca się z Latającymi Rekinami Aurum.
* "Magowie niech zajmują się magicznymi sprawami. My pomożemy ludziom i zajmiemy się tym wszystkim co magowie spieprzyli."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
