## Metadane

* name: Nox Aegis
* shortdesc: tarcza chroniąca noktian na Neikatis, ukryta arkologia, niesamowicie dobra technologicznie i koordynacyjnie. Ukryta.

## Serce
### 1. Koncept wysokopoziomowy

* Ukryta arkologia, działania noktiańskie na Neikatis mające dać im w miarę bezpieczne miejsce przed koszmarem astoriańskim.
* Ich misją nadrzędną jest przetrwać i zbudować lepsze życie dla noktian i syntetycznych intelektów.
* "Jeśli pragniesz wolności, dostaniesz ją. Ale wolność oznacza też rezygnację z niewolenia innych oraz dopasowanie się do Tarczy"

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
