## Metadane

* name: Astoriańskie Siły Defensywne
* shortdesc: Astoriańska frakcja łącząca większość państw i sojuszy, skupiająca się na ochronie planety przed... wszystkim nie-planetarnym.

## Serce
### 1. Koncept wysokopoziomowy

* Dość luźna grupa chroniąca Astorię przed siłami nie-Astoriańskimi. Grupa ochrony planety przed wszystkim innym.
* Dość rozpolitykowana, ale skupiona na defensywie Astorii frakcja niezależna od wszelkich sił Astorii.
* Kompetentne siły defensywne, nie mają najlepszych i najdoskonalszych (bo jest Orbiter), ale skupiają się na dobrej robocie.
* "Astorię jest w stanie ochronić tylko Astoria. Nie Orbiter, nie Syndykat, nie Sojusz Letejski, nie Eternia. Działamy razem."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
