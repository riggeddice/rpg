## Metadane

* name: Orbiter Aurora
* shortdesc: Orbiterowa subfrakcja skupiająca się na progresie technologicznym i dawaniu drugiej szansy i nadziei. Najbardziej optymistyczna i proaktywna siła Orbitera.

## Serce
### 1. Koncept wysokopoziomowy

* Dość duża i rosnąca grupa Orbitera, dążąca do maksymalizacji postępu i budowania lepszego świata i nadziei dla ludzi.
* Najbardziej skłonni do akceptacji żywych TAI, dania drugiej szansy i ogólnie HFY frakcja Orbitera. Symbol optymizmu, ale i naiwności w Orbiterze.
* "Nieważne co się stanie, uczymy się na swoich błędach i zbudujemy lepszy świat. Opłakujemy porażki, ale uczymy się i nie odrzucamy. Za lepsze jutro!"

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
