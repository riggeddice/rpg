## Metadane

* name: Sokoły Zmierzchu
* shortdesc: grupa astorian próbująca pomagać noktianom i dążąca do integracji astorian i noktian; działają z ukrycia.

## Serce
### 1. Koncept wysokopoziomowy

* Bardzo luźno rozproszona siatka w Sojuszu Letejskim dążąca do pomocy noktianom i do integracji ich z astorianami.
* Zwolennicy współpracy i harmonii, dzielenia się zasobami i integracji noktian z astorianami.
* Akceptują podejście typu "pewna statystyczna populacja ucierpi, ale długoterminowo się to opłaca."
* "Noktianie, astorianie - i tak naszym problemem jest cholerny świat z magią. A łącząc siły możemy więcej."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
