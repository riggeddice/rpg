## Metadane

* name: Czerwone Myszy Pustogorskie
* shortdesc: Alternatywny kulturowo wentyl poprzedniej kultury terenu Szczelińca przed Wielką Szczeliną 

## Serce
### 1. Koncept wysokopoziomowy

* Zwolennicy oryginalnej kultury sprzed dominacji dekadian na terenie Szczelińca
* Potrafią działać w relatywnej harmonii z Miasteczkowcami oraz z Trzęsawiskiem
* Odrzucają zimną dominację Pustogoru i militarny reżim
* Chcą wolnej, otwartej kultury i szerszej radości
* "To, że wszyscy wierzą w Pustogor nie oznacza, że tak jest lepiej. Kiedyś - przed Pustogorem - było lepiej. I stare powróci."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
