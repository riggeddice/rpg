## Metadane

* name: Trzy Ciernie Latiszy
* shortdesc: eks-oddział Trzykwiatu specjalizujący się w polowaniu na ludzi; niebezpieczna militarna banda powojenna próbująca zdobyć polityczną siłę

## Serce
### 1. Koncept wysokopoziomowy

* Stworzeni przez Trzykwiat przy Pustogorze by pomóc, ale gdy Pustogor przejął Trzykwiat to się ewakuowali.
* Ich misją jest znaleźć bezpieczne miejsce i lepszy dom. I zemsta na Pustogorze oraz na zdrajcach.
* Dominują siły powojenne (Astoria, nawet Noctis) którzy nie akceptują tego jak wojna się skończyła i jak się układają siły.
* Bardzo amoralni, mają doskonały korpus naukowy, niezły sprzęt i są 'lokalni' na terenie Szczelińca. Nie boją się Esuriit i 'paktów z diabłem'.
* "Próbowaliśmy działać w świecie zgodnie z zasadami i najlepszych z nas zdradzono i zniszczono dla głodu Pustogoru. Nasza kolej na lepszy świat."

### 2. Agenda

* **Głód**: "To jest nasza nagroda za obronę Astorii przed Saitaerem i Noctis, a potem za utrzymanie Trzykwiatu w dobrej formie?! Wygnanie i skazanie?"
    * **Stan Aktualny**: TCL są zmuszeni uciekać i unikać patroli astoriańskich, mają może dużą siłę ognia, ale są banitami.
    * **Stan Pożądany**: TCL mają więcej niż jedną miejscowość pod kontrolą, mają trybut i są w stanie obronić się przed wszystkimi i wszystkim. Legal and Powerful.
* Pomagaj ludziom i buduj polityczną presję na to, że TCL są korzystne z perspektywy okolicy.
* Pokazuj jak złym miejscem jest Pustogor i tyrania terminusów Pustogoru. Nie pozwól im po prostu zostać cicho i cicho przejmować kontroli nad terenem.
* Zgromadź jak najwięcej ludzi, materiel i potęgi politycznej. Spawnuj odnogi i działania dodatkowe.
* Promuj chwałę weteranów astoriańskich, zwłaszcza tych odrzuconych i wzgardzonych przez Astorię po tym jak wojna się skończyła. TAK, pomóż też noktiańskim weteranom.

### 3. Zasoby i aspekty

* Materiel i dyscyplina: dobry mit założycielski, niezłej klasy sprzęt wojskowy i niezła dyscyplina. Wojskowi, którzy zostali zdradzeni i trzymają się razem.
* Potencjał Esuriit: mają dużą wiedzę o Esuriit, liczne badania i potrafią wykorzystywać niektóre artefakty Esuriit. Jest to niebezpieczne, ale możliwe.
* Siły lokalne Szczelińca: mają sympatyków i wsparcie na obszarze Szczelińca, znają teren i wiedzą jak tam działać i jak się poruszać. Nie są 'zewnętrzną bandą', to 'partyzanci'.
* Sprawne fabrykatory: mają dostęp do fabrykatorów i mogą odbudować straty w sprzęcie, amunicji, dronach. Mogą też nimi handlować.

### 4. Szczególne słabości

* Zła opinia: nie tylko z uwagi na to że są 'bandami', ale też z uwagi na to, że nie mają nic przeciw niewolnictwu czy używaniu mrocznej magii.
* Magia dookoła Esuriit: Esuriit Zawsze Wygrywa. Skaża użytkowników. Nawet jak to tylko artefakty.
* Niewielkie wsparcie magiczne: nie mają dostępu do silnych magów czy wielu magów, co stanowi unikalną słabość.

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY operacja wymaga koordynacji, TO TCL jest w stanie uderzyć z wielu stron w tym samym czasie odpowiednimi siłami
* GDY potrzebna jest większa siła ognia, TO TCL może fabrykatorami dostosować się do problemu lub użyć sił Esuriit by przebić się w ten sposób.

#### Malusy

* GDY masz wsparcie TCL, TO jesteś postrzegany jako przeciwnik Pustogoru oraz jako zwolennik mrocznego podejścia, niewolnictwa itp.

## Inne

* .
