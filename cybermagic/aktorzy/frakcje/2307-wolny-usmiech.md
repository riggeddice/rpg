## Metadane

* name: Wolny Uśmiech
* shortdesc: Hedonistyczna mafia Grzymościa w Sojuszu Letejskim, pochodząca z Cieniaszczytu, rozpełzająca się nawet na Orbiter.

## Serce
### 1. Koncept wysokopoziomowy

* Skrzyżowanie mafii i sekty religijnej. Specjalizują się w hazardzie, rozrywkach, środkach psychoaktywnych, przemycie, badaniach itp. 
* Wspierani są pośrednio przez Cieniaszczyt i okolice z przyczyn kulturowych. Ogromne wsparcie wszelkich drakolitów.
* [Radość i Optymizm, Wolność i Niezależność, Eksperymenty i Adaptacja]
* "Techniki i podejście drakolickie nie muszą być spętane nieistotnymi zasadami."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
