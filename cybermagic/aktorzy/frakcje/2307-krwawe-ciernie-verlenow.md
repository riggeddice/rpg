## Metadane

* name: Krwawe Ciernie Verlenów
* shortdesc: Kult Verlenów dążący do osiągnięcia wielkości przez straszliwe wyzwania, nie patrzący na koszty

## Serce
### 1. Koncept wysokopoziomowy

* Niebezpieczni i niesamowicie wyszkoleni Verlenowie, którzy aranżują niebezpieczne sytuacje by sprawdzić siebie i akolitów.
* Coś pomiędzy kultem i organizacją, szanują skuteczność i efektywność. Odrzucają wszystko co nie jest niezbędne do osiągnięcia celu.
* Nie tylko Verlen może być Cierniem. Ale tylko Verlen może wprowadzić nową osobę do Cierni.
* "Stal hartuje się w ogniu. Verlen hartuje się w piekle."

### 2. Agenda

* **Głód**: ""
    * **Stan Aktualny**: 
    * **Stan Pożądany**: 
* .

### 3. Zasoby i aspekty

* X: .

### 4. Szczególne słabości

* .

### 5. Pokusy i domyślne Porażki
#### Bonusy

* GDY . TO .

#### Malusy

* GDY . TO .

## Inne

* .
