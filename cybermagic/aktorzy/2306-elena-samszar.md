## Metadane

* factions: "Pustogor"
* owner: "anadia"
* title: "Elena Samszar"

## Funkcjonalna mechanika

* **Obietnica**
    * ?
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY , TO 
    * **Kompetencje (3-5)**
        * .
    * **Agenda i styl działania (3-5)**
        * PRAGNIE .
        * GDY  TO 
    * **Słabości (1-5)**
        * NAWET GDY  TO  
        
## Reszta

### W kilku zdaniach

* Elena Samszar: czarodziejka origami i kinezy <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
    * A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.>
* Nauka i duchy
* Arystokratka, nie w stylu Torszeckiego. W Bibliotece raczej wygląda jak taka uczennica z anime, bo to wygodniejsze, żeby siedzieć w bibliotece i chodzić po regałach niż w długiej sukni, ale jak chce to potrafi "zmienić się" w 100% arystokratkę.

.

* Jej cel pośredni: najlepsza biblioteka na świecie (Gwiazdoczy?) 
* Cel główny: WIEDZIEĆ i umieć. 
    * Dlatego dużo czyta, kontaktuje się z duchami, bo chce wiedzy. 
    * Ma bdb pamięć. Ma dużą wiedzę, albo wie, gdzie coś znaleźć. 
    * Jest młoda, więc czasem wydaje jej się, że wszystko potrafi zrobić, bo o tym czytała, ale nie jest to prawdą (np. czytała o sztukach walki, więc umie kung-fu... no ale fizycznie nie ćwiczyła tej sztuki walki, więc lepiej, żeby walczyła papierem ;)) 
        * Chociaż zakładam, że fizycznie jest sprawna, bo np. nosi książki czy włazi na regały po stary skrypt "hujwieczego". 
* Myślę, że gdy słyszy o jakimś odkryciu np.zaginionego starodruku, to chce go chociaż przeczytać. Więc ma "subskrypcje" na nowinki ze świata archeologicznego, archiwistycznego itp. (Może jest tajną członkinią fanów papieru czerpanego - jeszcze przemyślę czego xD) 
* Chroni swoje ziemie i swoich ludzi i swoje duchy. Nie zawaha się, by poświęcić kogoś do ich obrony (kogoś kto nabroił np.tego kiepskiego maga z ostatniej sesji, swoich nie). Myślę, że bardziej kieruje się w stronę starszych, bo są mniej chaotyczni i "mądrzejsi" niż młodzi i tym mogę wyjaśnić jej irytację na Karolinusa ;)
* Stosunek do starszych: jeśli mają wiedzę to spoko np. Ojciec Eleny miał wiedzę, więc to był spoko. Niekoniecznie będzie szanować tych bez wiedzy lub jeśli już nic się nie może od nich nauczyć. Aczkolwiek nie będzie nimi jawnie pogardzać. * Jak wiemy z ostatniej sesji, manipulatorka.

### Jak sterować postacią

* **O czym myśli**
    * ?
* **Serce**
    * **Wound**
        * **Core Wound**: ?
        * **Core Lie**: ?
    * **ENCAO**: ?
    * **Wartości**: ?
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: ?
        * **Stan oczekiwany**: ?
    * **Metakultura**: ?
    * **Kolory**: ?
    * **Wzory**: 
        * ?
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * nieokreślone
* **Zasoby**: 
    * nieznane
* **Umocowanie w świecie**: 
    * tien Samszar, arystokratka

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: czarodziejka origami i kinezy
    * .

#### Jak się objawia utrata kontroli

* .
