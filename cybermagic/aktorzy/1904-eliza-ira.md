## Metadane

* factions: "pozostalosci eris"
* owner: "public"
* title: "Eliza Ira"


## Postać

### Ogólny pomysł (2)

* Dowódca TerrorShipa Eris
* Ekspert od krystaloform i hodowania kryształów; naukowiec

### Wyróżnik (2)

* Arcymag
* Silnie sprzężona z Multivirtem
* Kryształowe plagi i krystaloformy

### Misja (co dla innych) (2)

* Zwycięstwo ludzkości nad anomaliami i bogami
* Czystość i wolność wyboru dla wszystkich
* Znalezienie dobrego miejsca dla swoich ludzi

### Motywacja (co dla mnie) (2)

* Odkupienie tego co zrobiła (lub nie zrobiła)
* Przebaczenie od "swoich", z Eris lub kultury Eris
* Zapamiętanie przeszłości; zachowanie kultury i stylu życia Eris; legacy

### Zasoby i otoczenie (3)

* Powszechnie znienawidzona i uważana za przerażającą - zarówno przez Eris jak i przez Astorię
* Grupka lojalnych jej ludzi i magów chcących podążać jej ścieżką
* Kompleks Kryształowej Fortecy na wschodzie Cieniaszczytu
* Liczne technologie z czasów floty inwazyjnej Eris

### Magia (3)

#### Gdy kontroluje energię

* Arcymagini lodu i kryształów
* Kryształowe plagi oraz krystaloformy

#### Gdy traci kontrolę

* Anomalie krystaliczne, kryształowe plagi i inne takie

### Powiązane frakcje

{{ page.factions }}

## Opis

(10 min)
