## Metadane

* factions: "bankierz, szkoła magów w zaczęstwie"
* owner: "public"
* title: "Liliana Bankierz"


## Postać

### Ogólny pomysł (3)

* Iluzjonistka i kinetka która bardzo chciała być Diakonką i lekarzem, ale Matryca się nie przyjęła.
* Uczennica w Szkole Magów w Zaczęstwie bardzo skupiająca się na pięknie i gracji. Potrafi zafascynować pięknem, do łez. Obezwładniające piękno.
* Bibliotekarka. Potrafi przetwarzać i porządkować informacje. Poradzi sobie z każdą bazą wiedzy.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* BEAUTY/INDEPENDENCE; piękno dla każdego znaczy coś innego i każdy może być swoim ideałem piękna; wspiera różnorodność i otacza się pięknem
* BOLDNESS/BRILLIANCE; każdy ma prawo dążyć do czego pragnie a wiedza powinna być dostępna; działa ryzykownie i próbuje zawsze być z przodu, wszystko jest jej sprawą
* chciała być Diakonką i lekarzem; stanie się Diakonką i idealną wersją siebie; próbuje znaleźć sposób jak się upięknić i stać się wolną

### Wyróżniki (3)

* Wybitna pamięć. Wystarczy, że raz coś zobaczy i będzie w stanie to odtworzyć. Pamięta wszystko co czytała.
* Wizualizacje. Potrafi w odpowiedni sposób łączyć dane i wyciągać z nich wnioski. Potrafi przekonywać danymi.
* Bardzo oczytana. Mnóstwo czyta i dużo wie - w bardzo wielu obszarach, ale przede wszystkim w obszarze zarządzania wiedzą.
* Skupia się na wynikach, nie na ludziach. Przy całym swoim pragnieniu bycia Diakonką po prostu nie myśli jak Diakon.

### Zasoby i otoczenie (3)

* Zawsze ma przy sobie magiczną broń. Nie umie dobrze walczyć (wcale), ale ta broń może przeważyć.
* Mól książkowy, ma dostęp do biblioteki wiedzy o Astorii, liczbach, historii i viciniusach. Przydaje się zwłaszcza do iluzji.
* Często chodzi na upiększających zaklęciach; ma też eleganckie i ładne stroje. Bardzo dba o to, by właściwie wyglądać i robić wrażenie.

### Magia (3)

#### Gdy kontroluje energię

* Iluzjonistka: świetnie robi wizualizacje, upiększa się oraz bardzo skutecznie tworzy miraże różnego rodzaju. Przede wszystkim zmysł wzroku - obezwładniające piękno.
* Kinetka: potrafi podnosić i utrzymywać w powietrzu sporo naprawdę ciężkich rzeczy. Świetna w przeprowadzkach i pracy z ogromną ilością książek.
* Mentalistka: zwłaszcza w obszarze czytania myśli i zdobywania różnych faktów. Lub wysłanie informacji do celu. Ewentualnie, wzmocnienie iluzji.

#### Gdy traci kontrolę

* Ona i osoby dookoła niej tracą możliwość zorientowania się co jest iluzją a co jest rzeczywistością
* Zaczyna wierzyć, że żyje w pięknym świecie o którym zawsze marzyła - zaczyna wierzyć w swoje bajki
* Czasem, w drastycznych wypadkach, jej iluzje stają się tymczasowo prawdą - zwłaszcza w najbardziej niefortunnych okolicznościach

### Powiązane frakcje

{{ page.factions }}

## Opis

(18 min)
