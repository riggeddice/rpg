## Metadane

* factions: "rekin, diakon, aurum, wolny uśmiech"
* owner: "public"
* title: "Aranea Diakon"


## Kim jest

### W kilku zdaniach

Pajęcza Diakonka, czarodziejka, która walczy o ludzkie prawa i bycie traktowaną jak inni. Doskonała infiltratorka oraz mistrzyni trucizn. Niezła uwodzicielka. Mimo przerażającej natury, strasznie lubi robić dowcipy i nikomu nie życzy źle. Uwielbia imprezy i łażenie po dyskotekach - zwłaszcza takich, gdzie jej nie chcą.

Motto: "Wiesz, 'zabójczyni do wynajęcia' brzmi tak strasznie poważnie, no nie? Zwłaszcza, że jeszcze nikogo nie zabiłam. Ale przynajmniej wpuszczają mnie do zamkniętych lokali." 

### Co się rzuca w oczy

* Nigdy dwa razy nie wygląda tak samo. Plus, uwielbia szokować.
* Najbardziej lubi formę drobnej, młodziutkiej dziewczyny o długiej sukni w motywy pająków.
* UWIELBIA obserwować pojedynki magów, zwłaszcza gdy mają nagi tors.
* Wesoła, rozszczebiotana, wie wszystko o każdej imprezie i zawsze świetnie poinformowana.

### Jak sterować postacią

* Stworzenie społeczne, miejskie, leci do imprez jak ćma do ognia.
* Jeśli ma okazję zrobić komuś dowcip, na pewno to zrobi.
* Lubi się popisywać swoimi umiejętnościami.
* Strasznie źle reaguje na sytuacje w których ktoś źle traktuje innych z uwagi na klasę (AI, ludzie, viciniusy). Zwykle się wycofuje.
* Nie lubi walczyć. Nie chce walczyć. Jeśli ma walczyć, używa formy rozproszonej.
* Nie zdradza cudzych sekretów jeśli może tego uniknąć.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Z zaskoczenia zneutralizowała CAŁY gang Dużego Toma, rozpraszając się na rój pająków i wszystkich jednocześnie zatruwając jadem.
* Uwiodła mało ważnego polityka najeżdżającego na anomaliczne istoty, po czym wystawiła go w hotelu. Ale wpierw zatruła jego wino by wyszedł na idiotę.
* Odstraszyła dzieciaki od bazy mafii, pokazując im, że tam jest dużo pająków. Nie chciała ich krzywdy gdyby coś znaleźli.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Konstruktorka trucizn; jest w stanie syntetyzować różnego rodzaju trucizny i halucynogeny. Nieodzowna na niektórych imprezach ;-)
* AKCJA: Infiltratorka; z uwagi na umiejętności kameleona i rozproszenia strasznie trudno jest ukryć sekrety przed Araneą.
* AKCJA: Uwodzicielka i flirciara, uwielbia korzystać z życia. Zwykle się wyplącze z problemu dobrym słowem.
* AKCJA: Practical joker. Świetna w składaniu idiotycznych pułapek czy maszyn Rubego Goldberga.
* CECHA: Istota rozproszona; Aranea składa się z wielu małych pająków które stanowią jej ciało i ubranie. Jeśli pająki przejdą, cała Aranea jest w stanie się przedostać.
* CECHA: Klasyfikowana bardziej jako vicinius niż mag; przez to inni traktują ją jak nieistotny pyłek.
* COŚ: Trucizny i psychotropy, łącznie z wektorem wprowadzającym owe trucizny - jej własne pająki.
* COŚ: Opinia najsympatyczniejszej zabójczyni i osoby kompromitującej innych jaką znajdziesz. Ładna i zwariowana. Szkoda, że trochę za bardzo lubi głupie dowcipy.
* COŚ: Kolekcjonerka sekretów różnych osób na różne tematy.

### Serce i Wartości (3)

* Uniwersalizm
    * Nieważne kim lub czym jestem - jestem sobą. Nazywam się Aranea. Nie traktuj mnie jak jakieś dziwadło. Jestem Diakonką.
    * Nie chcę, by ludzie ze sobą walczyli. Nie chcę niczyjej krzywdy. Jeśli mogę coś powiedzieć czy coś zrobić, zatrzymam niepotrzebne konflikty.
    * Wolę przestawać z magami AMZ niż z Rekinami - tam przynajmniej nikt mnie nie ocenia jako "nieudaną Diakonkę".
* Stymulacja
    * Łóżko, psota... wszystko jest dla ludzi ;-). Tańcz, póki możesz.
    * Ważne, by wywoływać uśmiech na twarzy każdego. O to chodzi! Radość, uśmiech i dobra zabawa.
    * Nie ma klubu, do którego nie wejdę. Nie wpuszczą mnie to wejdę oknem :3.
* Bezpieczeństwo
    * Nie wiem, co będzie jutro. Muszę zapewnić sobie JAKIEŚ jutro, póki jeszcze mogę.
    * Cokolwiek się nie dzieje, chcę znać wszystkie sekrety na temat każdego. Jak nie ma innego wyjścia, zostaje szantaż. A faceci gadają w łóżku.
    * Dbam o reputację STRASZLIWEJ ZABÓJCZYNI. Nie jest to w pełni prawda, ale...

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Jestem OSOBĄ. Nie viciniusem. Nie eksperymentem. Na imię mam ARANEA. Choć nie wiem, co ze mną będzie i jestem nieudaną Diakonką..."
* CORE LIE: "Muszę żyć TERAZ, bo nie wiem co będzie jutro. Nie ma dla mnie przyszłości. Jeśli sama nie znajdę sobie szybko czegoś, oddadzą mnie na płaszczkę Blakenbauerów..."
* Niesamowicie wrażliwa na ogień i ataki gazowe. Jeśli coś skutecznie tępi rój i byty rojowe, tępi też ją.
* Niezdolna do korzystania z większości standardowej techniki zakładającej bardziej spójne ciało, linie papilarne itp.
* Nie jest w stanie utrzymać pełni umiejętności na deszczu. Musi też unikać pływania. Ogólnie, kąpiel jest dla niej wyzwaniem.

### Magia (3M)

#### W czym jest świetna

* Adaptacja ciała: Aranea umie zmieniać swój kształt i formę. Rozbić się w formę rojową.
* Zmiana wyglądu: Aranea zwykle nie ma na sobie ubrania; to wszystko jej pająki. Potrafi więc zmienić ich wygląd i kolorystykę - potrafi kameleonizować w dowolną osobę, strój, obiekt...
* Synteza trucizn: mistrzyni trucizn i psychodelików. Potrafi to syntetyzować i wprowadzać w różne miejsca...

#### Jak się objawia utrata kontroli

* Rozsypuje się na pająki i nie umie odzyskać formy XD.
* Przyzywa inne pająki (oczywiście).
* Psychodeliczny odjazd albo głębsza trucizna.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* .

### Endgame

* ?
