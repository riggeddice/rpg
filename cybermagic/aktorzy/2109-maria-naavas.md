## Metadane

* factions: "orbiter, żelazko"
* owner: "public"
* title: "Maria Naavas"


## Kim jest

### W kilku zdaniach

Eterniańska lekarka "Żelazka", która tak samo skutecznie leczy jak zabija. Nieprawdopodobnie wesoła i rozszczebiotana, jednocześnie jest bardzo 'unsettling'. Nigdy nie traci optymizmu i zawsze dąży ku najlepszego rozwiązania. Podrywaczka i kolekcjonerka facetów, namawiająca wszystkich do życia pełnią życia póki są w stanie. Sympatyczna i lojalna socjopatka nie mająca granic ani wyrzutów sumienia.

### Co się rzuca w oczy

* Wraz ze swoim dowódcą, Olgierdem, założyła "kapelę karaoke", z której jest piekielnie dumna. Ona jest na growlu...
* Uwielbia bawić się w swatkę; zwłaszcza chce zeswatać Olgierda z kimś jego godnym.
* Wzdycha za Martynem Hiwasserem. On jest jej głównym słabym punktem.
* DOTYK. Maria zawsze dotyka. W tym dotyku często nie ma podtekstów; po prostu taka jest.
* Niesamowicie dużo energii i pasji. Dąży do tej pasji. Próbuje spełnić pasję swoją i innych. Intensywna jak cholera.
* Unika wszelkiego typu holoprojekcji i virtu. Jej miejsce jest tu, w świecie realnym.
* Podrywaczka. Sama o sobie mówi "kolekcjonerka facetów". Ma sex appeal i nie waha się go używać.
* Całkowicie nie ma empatii. Zero. Nie współczuje. Jest "niekompletna".
* Nie ma w naturze rywalizacji, nie próbuje nikomu też nic udowodnić. Jest tym czym jest.
* Zawsze uśmiechnięta, często lekko rozmarzona, na twarzy wyraz lekkiego zadziwienia. Ekspresyjna.
* Uważa, że Orbiter może nie jest perfekcyjny, ale jest sprawny i potrzebny. Będzie Orbiter wspierać - bo widziała horror.

### Jak sterować postacią

* Perky & Chirpy. Wesoła. Dusza imprezy. Jednocześnie - Hollow Soul, Incomplete.
* Oddana Martynowi Hiwasserowi. Pragnie być jego partnerką. On ją dopełni. Jednocześnie - on ją lubi, nawet kocha - ale nie chce się związać.
* ZAWSZE bawi się w swatkę i jest w tym świetna.
* Nigdy nie traci optymizmu. Tak jak Martyn, wierzy w lepszy świat i w odróżnieniu od niego pragnie go zobaczyć.
* Nie ma w naturze rywalizacji, nie próbuje nikomu też nic udowodnić. Jest tym czym jest.
* NEOFORM: Wiktoria Diakon + Garak (DS9) | Abzan (BWG) | Profanity + Community + Tribalism | "Savage and unrestrained will to live and thrive and protect way of life"

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Dostała się do domu groźnego pirata przez łóżko, zabiła go gołymi rękami a następnie poszła pokój obok uspokoić jego dzieci, że nic się nie stało.
* Przekonała Olgierda do tego, by zrobił z nią kapelę karaoke. Po czym sprzedawała bilety a dochody przeznaczyła na program integracyjny Orbitera.
* Gdy Żelazko umierało w kosmosie i czekało na ratunek, utrzymała morale wszystkich w górze. Dzięki niej dało się podjąć trudne decyzje i statek przetrwał.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: Z uwagi na specyfikę jej visiatu jest nieprawdopodobnie odporna na Esuriit. Nie widzi, nie wchodzi w interakcję itp. Nie ma arkinu.
* AKCJA: Lekarz i anioł śmierci Żelazka. Doskonały lekarz i eksterminator bioagentami. Specjalizuje się w kierowanych plagach.
* AKCJA: Pierwszy oficer Żelazka i do tego świetna aktorka. Nie zmieniając tonu głosu umie strzelić w plecy komuś kogo przed chwilą całowała.
* AKCJA: Uwodzicielka i oficer morale. Świetnie śpiewa, ma ogromny sex appeal, doskonale podnosi morale innych i umie zainspirować.
* SERCE: Maria pragnie ŻYĆ z całego serca. Niewrażliwa na krytykę czy statusy społeczne. Niezłomne morale, zawsze zachowuje zimną krew.
* COŚ: Przenośne holoprojektory i amplifikatory soniczne; Maria niezależnie od okoliczności zorganizuje karaoke czy imprezę.
* COŚ: Jeden z najlepszych ekspertów od Esuriit jakich mamy na Orbiterze. Rytuały, sprzęt... i nic z tego nie może użyć sama.

### Serce i Wartości (3)

* Stymulacja
    * Intensywność u niej podpada pod hedonizm. Wszyscy powinni żyć pełnią swojego życia, zawsze i wszędzie!
    * Bardzo oparta na wszystkich zmysłach, zwłaszcza na dotyku.
    * "Head over heels" - nadmiar energii i pasji. Pierwsza rzuca się jak ćma w płomienie. Często płonie. Nie pyta o pozwolenie.
* Tradycja
    * Orbiter musi przetrwać. Orbiter musi być silny. Orbiter ma podejście, które działa - to podejście należy wspierać.
    * Nieprawdopodobna lojalność, zwłaszcza w kontekście braku empatii. Orbiter jest jej przyszywaną rodziną i musi być maksymalnie wzmocniony.
* Pokora
    * Pełni rolę pomocniczą. Jest bardzo żywą, ale aktorką drugoplanową wobec wszystkich "kompletnych" ludzi. Tylko Martyn może dać jej duszę.
    * Zdaje sobie sprawę ze swojej niekompletności i słabości. WIE, że jej percepcja świata jest uszkodzona. Oddaje dowodzenie innym.
    * Jestem cieniem czarodziejki. Jak w wypadku TAI, moją rolą jest służyć. Jak kiedyś Martyn. (synteza Tradycji i Pokory)

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Arkin eternijski mnie odrzucił. Nie jestem w stanie zjednoczyć się z innymi."
* CORE LIE: "Jestem niekompletna. Muszę być z kimś i muszę być bardzo głęboko. Sama nie stanowię osoby."
* Śmiertelnie zakochana w Martynie do poziomu obsesji. Dla jego uśmiechu skoczy z mostu.
* Brak empatii. Nie rozumie innych, nie wie kiedy posuwa się za daleko. Nie wie, kiedy krzywdzi - i nie czuje że to jest problem.
* "Prawda" i "moralność" są dla niej tylko słowem. Powie to, co najbardziej przybliży ją do celu. Zrobi to, co uzna za najskuteczniejsze.
* Proaktywna do bólu. Działa zgodnie z tym co rozumie i jej powiedziano. Nie przekracza rozkazów - czasem interpretuje je źle.

### Magia (3M)

#### W czym jest świetna

* Leczenie, utrzymywanie przy życiu, regeneracja. Maria jest lekarzem Orbitera i to widać.
* Plagi i pasożyty - dedykowane środki pod kątem konkretnej bioformy / konkretnej osoby.
* Psychokonstrukty / efemerydy intensyfikujące uczucia i emocje. Specjalizuje się w Różowej Pladze.

#### Jak się objawia utrata kontroli

* Utrata racjonalnego podejścia. 100% hiperoptymizmu i "Ryzyko? Uda się".
* Monotoniczne obsesje. Poczucie pustki którą trzeba zapełnić. Często wybuchy erotycznych orgii.
* Rozsianie plag / pasożytów.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* "Myślisz, że śmierć to zła opcja? Są rzeczy gorsze."
* Przyjęła nazwisko po anomalicznym statku kosmicznym, który należał do Kultu Ośmiornicy. Bo czemu nie? Chcieli nazwisko...
* Jej tożsamość nie jest powiązana z Eternią, więc nie jest powiązana z niczym. Poza, potencjalnie, Martynem.
* Teraz: 35 lat (na datę chronologii Inferni).
* Jej pierwszym przyjacielem i ogromną miłością był Martyn Hiwasser. Do dzisiaj są bardzo blisko.
* Jest lojalna Olgierdowi i "Żelazku"; oni ją wyciągnęli z Kultu Ośmiornicy. Ale jeszcze bardziej lojalna Martynowi.
* Theme Song: _Inkubus Sukkubus: "Vampire Erotica"_

### Endgame

* .
