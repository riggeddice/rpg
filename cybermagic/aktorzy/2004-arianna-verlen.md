## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Arianna Verlen"


## Kim jest

### Paradoksalny Koncept

Arcymag. Komodor Orbitera próbująca ratować wszystkich nawet kosztem swego zdrowia, w praktyce wyrządzająca sporo szkód sojusznikom. Świetna manipulatorka, zgrabnie porusza się po polityce i jest doskonałym taktykiem. Optymistka, wierząca w ludzkość jako ideę - i nie znosząca tysięcy niepotrzebnych frakcji. Dla uratowania członków swojej załogi czy dla jedności świata nie ma skrupułów by poświęcić dziesiątki innych osób.

### Motto

"By Federacja Ludzkich Planet mogła zaistnieć, potrzebny jest symbol - ktoś, komu zależy, dba o jedność, chroni i ratuje."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Manipulatorka i taktyk. Arianna potrafi zawsze zaleźć drugiej osobie za skórę - lub przewidzieć co druga osoba zrobi. Skuteczna w polityce i na polu bitwy.
* ATUT: Bardzo potężna czarodziejka; ze wsparciem załogi osiąga poziom Arcymaga. Arianna jest bardzo odporna na magię i włada potężną magią technomantyczną i kinetyczną.
* SŁABA: Arogancka i skłonna do ryzyka. Przecenia swoją moc i swoje umiejętności - idzie o krok za daleko, nie wycofuje się, zwykle raczej eskaluje - za daleko.
* SŁABA: Tak bardzo się martwi, że komuś na kim jej zależy stanie się krzywda, że pozycjonuje się na pierwszej linii ognia; próbuje prowadzić z frontu nawet, jak to bez wartości.

### O co walczy (3)

* ZA: Federacja ludzkich planet, chroniona przed Anomaliami przez Zjednoczone Siły Orbitera. Ze zdrowymi szlakami handlowymi i prosperującym światem.
* ZA: Zjednoczenie WSZYSTKICH ludzi i magów. Arianna NAPRAWDĘ chce ratować wszystkich. Noktianie, astorianie, Skażeni... to wszystko to nasza rasa, nasza przyszłość.
* ZA: Chce stać się symbolem, chce być czymś więcej niż tylko czarodziejką. Chce być symbolem jedności i bezpieczeństwa. Arcymagiem i awatarem.
* VS: Głupie dziesiątki frakcji w Orbiterze. Zamiast współpracować dla jednego celu porozdzieraliby się nawzajem. Arianna zatem ich zniszczy politycznie lub magicznie.
* VS: Nie pozwoli NIGDY komukolwiek się pokonać. Nie będzie nigdy upokorzona. Nie odda nikomu pokłonów. Nie utraci twarzy ani godności.
* VS: Aktywnie przeciwdziała zarówno próbom ogromnej centralizacji władzy ("monarchia, imperium") jak i za dużemu rozbiciu ("państwa-miasta"). To nie zadziała.

### Znaczące Czyny (3)

* Naraziła Infernię na straszliwe uszkodzenia i prawie zniszczenie - ale magią rozerwała osłony Anomalii Kosmicznej Leotis, by Infernia mogła wystrzelić.
* Wymanewrowała komodora Ogryza idąc przez plotki, przez jego żonę itp - by potem dać mu rozwiązanie problemu jaki stworzyła i zostać jego sojuszniczką.
* Zaufała Hiwasserowi i rozkazała sabotować Miecz Światła, który ich uratował - by tylko Miecz nie został pożarty przez Anomalię Kosmiczną Serenit.

### Kluczowe Zasoby (3)

* COŚ: Amplifikatory Eteryczne. Arianna jest w stanie użyć ich by jeszcze bardziej wzmocnić swoją magię, skierować swoją wolę i osiągnąć jeszcze więcej.
* KTOŚ: Ma pozycję komodora Orbitera, z czym wiąże się władza i uprawnienia, zarówno wśród magów jak i TAI.
* WIEM: Wie, w jaki sposób może wezwać niebezpieczną Anomalię - Nocną Kryptę - i jak przewidzieć jej zachowania.
* OPINIA: Pomocna, ale bezwzględna. Zadrzyj z jej załogą a ona Cię po prostu zniszczy. Nierozważnie działa z pierwszej linii. Nie umrze śmiercią naturalną.

## Inne

### Wygląd

.

### Coś Więcej

.
