## Metadane

* factions: "orbiter, oddział litaar"
* owner: "public"
* title: "Rufus Czamtark"


## Kim jest

### Paradoksalny Koncept

Bardzo lubiany oficer polityczny i porządkowy statku kosmicznego. Świetny i wygadany wojownik, przybył z arystokracji Aurum. Straszny hazardzista, który dba jednak o swoją załogę i jej morale. Arystokrata, który uciekł przed systemem hierarchicznym i ożenkiem dla pieniędzy. Pragnie pomóc swoim, ale nie kosztem powrotu na planetę. Hazardzista, który pragnie bogactwa dla innych.

### Motto

"Nieważne jak źle, poradzimy sobie - a żeby zabić czas, zagrasz w karty?"

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Bardzo szybki, silny i zdeterminowany - z perspektywy fizycznej i mentalnej jest świetnym okazem człowieka. Świetny w walce wręcz.
* ATUT: Ma gadane; potrafi pocieszyć, wyciągnąć sekrety czy podnieść morale, jeśli trzeba.
* SŁABA: Słabość do hazardu. Nie potrafi odmówić. Lubi się zakładać i im wyższe stawki tym lepsza zabawa.
* SŁABA: Jest ZBYT zdeterminowany - eskaluje za daleko, nie wie, kiedy się wycofać.

### O co walczy (3)

* ZA: Sława i bogactwo - by pomóc rodzinie (podupadły ród arystokratyczny). Tak jak jest są arystokracją tylko z nazwy.
* ZA: Zostać kapitanem statku - do tego celu musi brać odpowiedzialność i przynosić korzyści flocie Orbitera.
* VS: Ucieczka przed wysoką rangą i zobowiązaniami; nie chce pełnić roli arystokraty Aurum a na pewno nie chce się żenić dla pieniędzy!
* VS: Piractwo, arystokracja (...) i nadużywanie siły i władzy wobec słabszych.

### Znaczące Czyny (3)

* Przegrał w karty o wysoką stawkę; jako konsekwencja zaciągnął się do floty Orbitera. Spodobało mu się.
* Pod ostrzałem i narażając życie wyciągnął ciężko rannych kolegów używając siły fizycznej i granatu dymnego.
* Gdy dowodził pinasą wydzieloną z Miecza Światła, był w stanie tak długo przegadywać piratów chcących go abordażować, że doczekał przybycia Miecza Światła.

### Kluczowe Zasoby (3)

* COŚ: Różne sympatyczne środki chemiczne do unieszkodliwiania, uspokajania, ale też stymulanty itp. Na wszelki wypadek.
* KTOŚ: Ma dostęp do swojego rodu (m.in. kilku członków załogi pochodzą z jego majątku). Jako arystokrata ma szersze uprawnienia.
* WIEM: Jeżeli cokolwiek da się przemycić, Rufus dokładnie wie jak to przemycić i gdzie to będzie.
* OPINIA: Można mu zaufać, jak długo się z nim nie zakładasz. Dba o swoich i to co robi robi dobrze. Można przyjść z nim pogadać.

## Inne

### Wygląd

.

### Coś Więcej

.