## Metadane

* factions: "orbiter, oddział litaar"
* owner: "public"
* title: "Mateusz Sowiński"


## Kim jest

### Paradoksalny Koncept

Komodor statku ratunkowego polujący na kosmiczne anomalie. Dowódca statku ratunkowego lubiący pojedynki w słusznej sprawie. Dowódca nowoczesnego statku żyjący przeszłością oraz pragnieniem godnej śmierci w akcji. Dowódca nowoczesnego statku nie ufający TAI i BIA. 57-letni czarodziej który tak ma dość innych, że woli eksplorować odleglejsze fragmenty Astorii i ratować rozbitków. Zafascynowany bogami, a zwłaszcza Saitaerem - zbiera artefakty na jego temat i próbuje zrozumieć Władcę Adaptacji.

### Motto

"Kto nie ryzykuje osobiście - nie zasługuje na wygraną ani lojalność ludzi. Kapitan przodem."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Doskonały w pojedynkach; formalnych oraz faktycznej walce 1 vs 1.
* ATUT: Potrafi przewidzieć działania anomalii oraz wrogich okrętów wojennych tak, by wprowadzić je w niekorzystną sytuację.
* SŁABA: Arogancki i pewny siebie; przecenia siebie, swój statek i swoją pozycję. Łatwo go wciągnąć w pułapkę.
* SŁABA: Lekceważy zarówno TAI jak i BIA. Nie wierzy nieludzkim formom inteligencji. DRAMATYCZNIE je zaniża.

### O co walczy (3)

* ZA: Odnaleźć Nocną Kryptę - a z nią swojego syna, którego podobno "uratowała".
* ZA: Pokazać tym gryzipiórkom w admiralicji jaki powinien być PRAWDZIWY komodor Orbitera.
* VS: Głupcy i tchórze; należy ich usunąć ze stanowiska i wyrzucić.
* VS: Zniszczyć wszystkie kosmiczne anomalie zagrażające Orbiterowi i ludziom.

### Znaczące Czyny (3)

* Zniszczył najwięcej niebezpiecznych anomalii ze wszystkich komodorów Orbitera swoim Mieczem Światła.
* Osobiście szkoli członków swojej załogi w walce wręcz i bronią zasięgową.
* Gdy Miecz Światła miał awarię reaktora, wykorzystał Anomalię Saitaera by Miecz doleciał do Kontrolera Pierwszego.

### Kluczowe Zasoby (3)

* COŚ: krążownik ratunkowy "Miecz Światła", z bronią anty-anomalną i wierną załogą na pokładzie. I podłamaną TAI Persefoną.
* KTOŚ: zaprzyjaźniony z niejedną nielegalną baza noktiańską lub astoriańską w Sektorze Astoriańskim; mile tam widziany.
* WIEM: o swoich statkach, załodze itp. wie ABSOLUTNIE WSZYSTKO. Praktycznie się nie myli - wie jak bardzo daleko może się posunąć.
* OPINIA: lekko świrnięty i często nieprzyjemny kapitan, który jednak wie co robić i nie pozwoli nikomu zginąć marnie.

## Inne

### Wygląd

Nieskazitelnie ubrany 57-letni komodor, który roztacza wokół siebie aurę władzy i potęgi. Niski, mocny głos. Stalowe spojrzenie, szpakowate włosy i odpowiednie zmarszczki. Porusza się jak tygrys i zawsze łapie równowagę. Z czułością mówi o Mieczu Światła - jest jego domem i najbliższą mu "osobą".

### Coś Więcej

Kompetentny i arogancki dowódca Miecza Światła który reinwestował w ów krążownik większość pieniędzy. Najpewniej umrze wraz ze statkiem.
