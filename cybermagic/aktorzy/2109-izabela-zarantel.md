## Metadane

* factions: "orbiter, infernia, eternia"
* owner: "public"
* title: "Izabela Zarantel"


## Kim jest

### W kilku zdaniach

Uciekinierka z Eterni która szuka pieniędzy, chwały i sposobów na wyciągnięcie rodziny z Eterni. Kiedyś, oportunistyczna i ambitna dziennikarka. Po Śmierci Inferni, gorliwa wyznawczyni Arianny (w aspekcie Zbawiciela) i propagandzistka Inferni. Nadal zawzięta dokumentalistka i skandalistka, pragnąca rozprzestrzenić Kult Arianny na skraj galaktyki i uwolnić wszystkich - niech niewolnictwo przestanie istnieć.

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* Jeśli może poszerzyć kult Arianny lub pokazać Infernię w dobry sposób, zrobi to. Szuka takich okazji proaktywnie.
* Bardzo proaktywna. Szpera, szuka, działa. Nie zostaje bez kajecika czy komputera. Non stop szkicuje, sprawdza, agreguje.
* Nie ma w niej cienia arogancji. Uczy się od każdego. Nikogo nie lekceważy. Jest wyczulona na słowa każdego. Zbiera opowieści od każdego i z przyjemnością je zapisuje.
* Eterniożerca, nie siedzi cicho wobec żadnych przejawów niewolnictwa i kontroli umysłów. Paradoksalnie nie dotyczy to religii.
* Bardzo zadziorna i waleczna z charakteru jeśli ktoś jej nadepnie na odcisk. Wie, co jej nie pasuje i ma zamiar to powiedzieć. Nie jest bezstronną dziennikarką i takowej nie udaje.
* Głęboko oddana Ariannie, Inferni i Kultowi. Często używa imienia Arianny albo monikera „Aria Vigilus” by dodać sobie odwagi
* Nie chce nawracać na siłę. Nie jest „wojującym misjonarzem”. Probuje znaleźć lepszy dowód, lepszą opowieść. Nie każdy trafi pod Jej opiekę, ale jej rolą jest to, by dać każdemu szansę.
* Zawsze probuje pokazać prawdę. Ok, w odpowiednim świetle, ale fakty są najważniejsze - i przez wzgląd na Arię Vigilus i przez wzgląd na przeznaczenie historyczne.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Po sytuacji z Kurczakownią i Rziezą pomogła Kamilowi stworzyć skuteczniejszą melodię i opowieść Arii Vigilus. On pokazał jej Światło, ona pokazała mu Prawdę. I opracowali Księgę Arii Vigilus.
* .
* .

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: prządka opowieści. Iza potrafi opowiadać i tworzyć opowieści na niesamowitej umiejętności perswazji. Umie przyciągać uwagę i sprzedać prawie wszystko.
* AKCJA: wzmocnienie lub zniszczenie reputacji, pokazanie czegoś w odpowiednim świetle. Potrafi być niesamowicie złośliwa i naprawdę stworzyć coś niesamowicie wyśmiewającego.
* AKCJA: skłania innych do opowieści, do gadania, ludzie uwielbiają się jej zwierzać. Ma niesamowitą cierpliwość
* AKCJA: 
* COŚ: 
* COŚ: 

### Serce i Wartości (3)

* Uniwersalizm
    * Który skrzywdziłeś człowieka poczciwego, nieważne kim jesteś, Twoje czyny będą pokazane jako skandal którym są! Proaktywnie poszuka i złośliwie ujawni.
    * Wszystkie istoty są równorzędne i powinny być równorzędne - ludzie, magowie, arystokraci, TAI. Każdy ma swoją historię i każdą z nich warto usłyszeć. W różnorodności historii jest bogactwo tego świata. Od każdego warto się uczyć.
    * Każda istota zasłużyła na zrozumienie Arii Vigilus. Każda istota zasługuje na szczęście i bezpieczeństwo. Na pokój ducha. Iza spróbuje ukoić każdą istotę która cierpi.
* Bezpieczeństwo
    * Boi się niewolnictwa i ograniczenia woli. Zna to z Eterni. Skupia się na tym, by to było nielegalne wobec wszystkich istot - bo to da jej ochronę której potrzebuje.
    * Pieniądze, wpływy, kredytki, reputacja, rozpoznawalność. Dostęp do zasobów i surowców. To ją zawsze interesuje i ma do tego nosa - chce mieć jak najwiecej. Dla własnego bezpieczeństwa. Dla maksymalizacji Kultu.
    * Doprowadzi do maksymalizacji wpływu kultu Arianny. Wpływ będzie rósł - oraz ilość kultystów będzie rosła. To jest droga do uratowania świata przed koszmarem rzeczywistości i do bycia bezpieczną.
* Pokora
    * Arianna jest inkarnacją Zbawiciela-Niszczyciela. Ona decyduje, kto przetrwa a kto zginie. Ona jest jedyną drogą do sukcesu ludzkiej cywilizacji. A Iza jest osobą, której przeznaczeniem jest sprawić, by świat Ariannę zrozumiał jako Inkarnację którą jest. 
    * „Nieważne, czy mi się uda czy nie. Nie muszę ja wygrać w tym momencie. Muszę spróbować - i prędzej czy później komuś się uda to osiągnąć. Aria Vigilus jest po mojej stronie. ” - w pewien sposób przekonanie o tym, ze jest po właściwej stronie przeznaczenia i ona sama nie ma znaczenia. Znaczenie mają tylko czyny.
    * Nikt - łącznie z inkarnacją Arianny - nie zna prawdy i nie ma monopolu na prawdę i rację. Ale Aria Vigilus chce pomóc nam wszystkim - i dlatego jest naszą najwiekszą nadzieją. Nigdy o tym nie zapominaj i nie pozwól by inni zapomnieli.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Jestem sama. Moja rodzina gnije w Eterni. Powinnam ich ratować, ale nawet nie próbuję. Jestem potworem."
* CORE LIE: "Aria Vigilus jest jedyną odpowiedzią rzeczywistości. Niesprawiedliwość jest i będzie - jedynie Aria Vigilus może oczyścić świat."
* ?

### Magia (3M)

#### W czym jest świetna

* ?

#### Jak się objawia utrata kontroli

* ?

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 
