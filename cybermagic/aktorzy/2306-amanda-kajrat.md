## Metadane

* factions: "mafia kajrata"
* owner: "kić"
* title: "Amanda Kajrat"

## Funkcjonalna mechanika

* **Obietnica**
    * "Będziesz infiltratorką i snajperem, oficerem noktiańskiej mafii. Będziesz zabójczynią wysokiej klasy, budującą organizację przestępczą dla Kajrata pięścią i sprytem."
    * "Byłaś infiltratorką noktiańską, usuwającą przeciwników ciężką snajperką. Stałaś się prawą ręką i ulubienicą szefa mafii, budując nową przyszłość dla noktian na Astorii."
* **Rdzeń mechaniczny**
    * **Mistrzostwo (1-3)**
        * GDY musi się gdzieś przedostać, TO się tam dostanie niezauważona i cierpliwie przeczeka aż będzie mieć okazję do działania. _Gibka, zwinna i cierpliwa - infiltratorka najwyższej klasy_
        * GDY walczy na serio, TO się skutecznie przemieści, schowa i snajperką ustrzeli wroga. _Niezależnie czy to servar czy anomalia, zastrzeli bez cienia zawahania._
    * **Kompetencje (3-5)**
        * GDY trzeba walczyć w zwarciu, TO walczy brudno i świetnie 'robi' nożem. _Dekadiańskie szkolenie i naturalna bezwzględność zmieniły ją w rosomaka w walce_
        * GDY trzeba przejąć kontrolę, TO żelazną pięścią i strachem wymusi co jest potrzebne. _"Możecie protestować, ale zrobicie co Wam każę. Macie w końcu rodziny."_
        * GDY jest w neutralnej sytuacji społecznej w której jej nie znają, TO urokiem i dekoltem osiągnie co chce. _Ciało i uśmiech są jej bronią tak jak nóż, strach i snajperka._
        * GDY ma do czynienia ze sprzętem wojskowym / ochroniarskim, TO znajdzie słaby punkt i zrozumie co to. "_Każdy ma hobby. Tak się składa, że moim jest niszczenie TYCH rzeczy."_
    * **Agenda i styl działania (3-5)**
        * PRAGNIE zbudować nowe, stabilne życie dla noktian na Astorii, które nie zależy od nikogo poza noktianami na Astorii. _Noctis nas zdradziło. Astoria nas niszczy. Sami stworzymy nasz los._
        * GDY ma do czynienia z ludźmi niewłaściwie traktującymi noktian, TO nie wybaczy i ich okrutnie pomści, robiąc przykład z ofiary. _Nikt nie skrzywdzi moich. Nie odważą się._
        * GDY ma okazję na pozyskanie zasobów niewłaściwych moralnie, TO się nie zawaha i nie ma z tym problemów. _Przede wszystkim przetrwać i się wzmocnić. Potem wszystko naprawimy._
        * GDY ma do wyboru - reputację lub wykonać zadanie, TO skupia na tym, co jest najlepsze dla noktian i Kajrata; ona nie ma znaczenia. _Nie jestem tutaj, by zdobywać popularność. Kajrat mnie osłoni._
        * GDY może uratować lub pomóc noktianom nie naruszając zadania, TO niewiele ją przed tym powstrzyma. _Misja przede wszystkim, ale nie możemy zostawić NASZYCH._
        * GDY ma do czynienia ze zdrajcami, TO nimi gardzi, nie ufa, nie wybacza i najchętniej usunie. _Misja to misja. Ale zdrada jest niewybaczalna. Wszystko zaczyna się od tego, że nas zdradzono._
    * **Słabości (1-5)**
        * GDY ma do czynienia z normalną sytuacją społeczną, TO zupełnie nie ma poczucia humoru. _Nawet jak na dekadiankę, humor jest czymś jej obcym._
        * GDY sytuacja wymaga empatii i wrażliwości, TO się gubi w swym pragmatyzmie. _"Rozumiem ból i strach. Ale o co TERAZ chodzi? Nic się przecież nie stało"_
        * GDY ma zadanie zlecone przez Kajrata, TO ma tendencję do nadmiernej wiary w jego dbanie o nią. _"Kajrat mnie nie zdradzi. I tylko on mnie nie zdradzi."_
    * **Zasoby (3-5)**
        * Dane z mafii odnośnie świata przestępczego i okolicy działań.
        * Dostęp do narkotyków i pomniejszych grup przestępczych, z ramienia Kajrata i własnej żelaznej pięści
        * Snajperka. Na późniejszym etapie - Eidolon.

## Reszta

### W kilku zdaniach

* .

### Jak sterować postacią

* **O czym myśli**
    * "Noctis nie wróci. Ale naszą odpowiedzialnością jest zapewnić 'naszym' jak najwięcej szans".
    * "Ernest Kajrat jest naszą najlepszą szansą i nadzieją na lepsze jutro. On nas nie zdradził. On jeden stoi za ideałami Noctis."
    * "Nie da się działać w normalnych strukturach, bo jesteśmy traktowani jak śmieci. Zrobimy własne struktury. Buduj, nie narzekaj."
    * "Magia, anomalie, magowie, całe to zawracanie głowy. Mam rozkazy i snajperkę."
* **Serce**
    * **Wound**
        * **Core Wound**: "Jestem sama, w nowym świecie i wszystko co miałam i kochałam obróciło się w proch. Straciłam wszystko i mogę stracić więcej przez astorian."
        * **Core Lie**: "Ernest Kajrat jest jedyną siłą, która daje mi - i innym - nadzieję na jutro. Nie da się zasymilować na Astorii. Odrębność noktian i mafia to jedyna droga."
    * **ENCAO**: N-C+
        * "nie da się jej wyprowadzić z równowagi", "niezależnie od poziomu stresu w sytuacji, wykona plan na zimno"
        * "nie zawaha się zrobić tego co trzeba", "jest nieskończenie cierpliwa i pragmatyczna"
        * "raczej zachowuje swoje myśli dla siebie"
        * "zawsze jest przygotowana", "jest jak skała - stabilna, niezłomna i zawsze można na niej polegać."
    * **Wartości**: Power, Security, Family
        * F: "Zostało nam tak niewiele - jesteśmy rozproszeni i rozbici. Lojalność i odbudowa, dbanie o to, co nam zostało - to się liczy. Noctis nas zdradziło, ale Kajrat nie."
        * S: "Musimy przetrwać i zapewnić, że nie stracimy tego czym jesteśmy. Uratujemy i zintegrujemy noktian."
        * P: "Siła jest istotna. Wpływ na świat ma znaczenie. Bez dobrych sojuszy, zasobów i dalszych możliwości nie tylko nikomu nie pomożemy ale się sami rozpadniemy."
    * **DELTA_DRIVE**: 
        * **Stan aktualny**: "Nie wiem co nas czeka i tylko Kajrat ma wizję jutra. Nie wiem, co mnie czeka w przyszłości. A noktianie cierpią za zdradę ze strony innych noktian."
        * **Stan oczekiwany**: "Imperium Noctis na Astorii, pod dowództwem Kajrata. Niezależność od Astorian. A ja - egzekutor Jego woli."
    * **Metakultura**: dekadianin. "_Wilk nie poluje samotnie. Jestem członkiem stada. Jednostka jest mniej istotna niż cele grupowe._"
    * **Kolory**: BW. "Porządek i wspólnota Noctis przez poświęcenie i bezwzględność. Nie wszystko musi mi się podobać, ale grupa jest ważniejsza niż jednostka."
    * **Wzory**: 
        * Kerrigan w relacji do Mengska przed jej infestacją
    * **Inne**:
        * brak

## Możliwości

* **Dominujące Strategie**: 
    * infiltracja - przeczekanie - egzekucja - ewakuacja
    * używanie narkotyków i strachu by zmusić innych do działania
    * używanie pomniejszych grup przestępczych by mieć wsparcie w dowolnej sytuacji
* **Umocowanie w świecie**: 
    * prawa ręka Ernesta Kajrata, dowódcy pronoktiańskiej mafii w Szczelińcu

## Magia
### Dominująca moc

* JEDNYM ZDANIEM: nie czaruje
    * .

#### Jak się objawia utrata kontroli

* .
