## Metadane

* factions: "noktianka"
* owner: "public"
* title: "Talia Aegis"


## Paradoks

Humanistyczna i pro-ludzka biomantka - chroniąca AI jako formę życia.

## Motywacja

### O co walczy

- Saitaer musi zostać zniszczony lub opanowany
- prawa dla AI i BIA
- świat znowu będzie dobry dla magów i ludzi- pokój i kontrola

### Przeciw czemu walczy

- wszelkie przejawy niekontrolowanej energii: ixion, esuriit
- traktowanie AI jako zabawek czy narzędzia a nie istoty świadome
- wojny, konflikty militarne, przemoc, krzywdzenie innych
- nieodpowiedzialne igranie z siłami których nie rozumiemy

## Działania

### Specjalność

- pamięta jeszcze Erę Kosmiczną; ma ogromną wiedzę i doświadczenie
- konstruktorka TAI czy BIA. Psychotroniczka i neuronautka najwyższej klasy
- potrafi ukryć to co wie, może nawet na torturach (multi-twine)
- wyjątkowo nie wychodzi jej umieranie

### Słabość

- nie ma wiele siły ani energii i bardzo szybko się męczy
- jej moc magiczna jest słaba; wypadła poza Paradygmat
- Z uwagi na zdecydowane poglądy jest lekceważona i niezbyt ceniona

### Akcje

- ktokolwiek jej o coś nie poprosi, zwykle się zgadza - mniej roboty i konfliktów
- jest bezgranicznie uczciwa - nie intryguje przeciw innym, da się przekonać faktom i dowodom- jak długo może je zweryfikować
- oschła; trzyma ludzi na dystans. Preferuje kontakt z Al.
- ostrożność w ruchach i wiecznie odległe spojrzenie zdradzają jej wiek.
- podejście lekarza - nie krzywdź, nie rań, nie odbieraj życia. Też wobec Al.
- lubi pozostawać w ciszy i cieniu. To już nie jest jej świat.
- identyfikuje się z kulturą noktiańską

### Znaczące czyny

- sprzęgła Nikolę z Finis Vitae przez BIA 5 kategorii. Do dziś ma koszmary.
- wprowadziła BIA jako warunek konieczny przetrwania Noctis po Pęknięciu.
- pierwsza rzuciła temat 'Nie wolno nam zniszczyć Astorii' podczas Inwazji Noctis.

## Mechanika

### Archetypy

psychotronik, biomanta, naukowiec, medyk polowy

### Motywacje

?

## Inne

### Wygląd

?
