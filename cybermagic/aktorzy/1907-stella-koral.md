## Metadane

* factions: ""
* owner: "kić"
* title: "Stella Koral"


## Postać

### Ogólny pomysł (3)

Pilot i górnik. Sprzężona ze swoim statkiem, Mikado.
Scrapper, który zrobi laser górniczy z niczego.
Uwielbia ciężkie lasery górnicze.
Archertypy: Scrapper, Neurosprzężony pilot.

### Czego chce a nie ma (3)

* dla siebie: reputacja najlepszego statku górniczego
* dla siebie: wobody eksploracji
* dla sibie: niezależności i akcpetacji
* dla innych: wolności dla AI
* dla innych: bezpieczeństwa załogi

### Sposób działania (3)

* prowizorka - konstrukcje z byle czego, adaptacja sprzętu, ad-hoc elektronika, konstrukcja psychotroniki
* chaos - odwracanie uwagi, bomby, dywersja na odległość, chemikalia, pułapki
* opętane neuromaszyny - przejmowanie kontroli, zostawianie bomb logicznych, konstrukcja psychotroniki
* survival - ocena sytuacji, znajdowanie zasobów, S&R

### Zasoby i otoczenie (3)

* Mikado - statek górniczy z sarkastyczym AI 'Eva'

### Magia (3)

#### Gdy kontroluje energię

* Wspomagane konstrukcje z niczego
* Neuronautka eksplorująca bazy danych i umysły AI
* Skan i wykrywanie niebezpieczeństw

#### Gdy traci kontrolę

* Mściwe golemy, pół-świadome maszyny
* Złomowisko dookoła

### Powiązane frakcje

{{ page.factions }}

## Opis


