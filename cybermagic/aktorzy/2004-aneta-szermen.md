## Metadane

* factions: "orbiter, oddział litaar"
* owner: "public"
* title: "Aneta Szermen"


## Kim jest

### Paradoksalny Koncept

Badaczka pustki, anomalii i otchłani, która czuje się szczęśliwa w obliczu nicości. Lubi spacery w próżni, gdyż tam najłatwiej jej się koncentrować. Przyjacielska; lubiana zarówno przez załogę jak i przez sztuczne inteligencje. Uwielbia rozwiązywać zagadki i badać anomalie, pomaga też w potrzebie dziwnymi formami magii. Mimo, że jest na bardzo pro-ludzkim statku, chce harmonii z innymi bytami.

### Motto

"Gdy jest cicho i nic nie przeszkadza, jesteś w stanie zobaczyć prawdę. A poznanie prawdy jest bardzo ważne."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: W kosmosie, w próżni, w pustce czuje się swobodnie - jak w domu. Jest niewidoczna i ma pełną kontrolę.
* ATUT: Bardzo wyczulona na wszelkie anomalie magiczne i najdrobniejsze drgnięcia magii; postrzega je jak dysharmonię.
* SŁABA: Źle się czuje w ograniczonych, ciasnych pomieszczeniach; potrzebuje przestrzeni.
* SŁABA: Potrzebuje ciszy i spokoju. W pośpiechu i panice ciężko jej się pracuje.

### O co walczy (3)

* ZA: Każda zagadka musi zostać rozwiązana, każda anomalia dokładnie zbadana. Nie zostawi sytuacji nierozwiązanej.
* ZA: Użyje potęgi anomalii i magii by pomóc osobom w potrzebie; wiedza dla samej wiedzy to za mało - wiedza musi pomóc.
* VS: Zdrajcy, szpiedzy i sabotażyści - osoby działające przeciwko swojej organizacji muszą zapłacić.
* VS: Kłótnie i niezrozumienie są NUDNE. Wszelkie spory trzeba rozwiązać - zawsze KTOŚ ma rację.

### Znaczące Czyny (3)

* Zrobiła pantomimę w kosmosie by rozśmieszyć przerażone dzieciaki gdy statek kosmiczny był uszkodzony (ku przerażeniu kapitana).
* Przekonfigurowała torpedę taumiczną na szybkie pulsowanie, by zniszczyć niebezpieczną anomalię karmiącą się energią magiczną.
* Na urlopie odkryła przypadkiem kult niebezpiecznych istot po nienaturalnej sygnaturze energii magicznej z pobliskiej szkoły podstawowej.

### Kluczowe Zasoby (3)

* COŚ: Maskowany serwopancerz badawczy; świetne narzędzie badawcze i pancerz ochronny. Świetny do poruszania się w próżni.
* KTOŚ: TAI Persefona. Każda sztuczna inteligencja statku kosmicznego jest jej potencjalnym przyjacielem - mają podobny zasób wiedzy.
* WIEM: Anomalie Kosmiczne. Opowieści o anomaliach. Problemy z anomaliami. Jeśli to jest w kosmosie i powiązane z magią - najpewniej Aneta o tym słyszała.
* OPINIA: Bardzo lubiana przez załogę i bardzo kompetentna badaczka anomalii magicznych, która lubi szalone spacery w próżni. Krzywdy nie zrobi.

## Inne

### Wygląd

.

### Coś Więcej

.
