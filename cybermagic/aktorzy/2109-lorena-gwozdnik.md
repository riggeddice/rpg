## Metadane

* factions: "rekin, gwozdnik, aurum"
* owner: "public"
* title: "Lorena Gwozdnik"


## Kim jest

### W kilku zdaniach

Pochodząca z bardzo militarnego i silnie współpracującego z Orbiterem rodu czarodziejka, wygnana na Prowincję by "zrobiła się dzielna" i "dorosła". Rozpaczliwie próbuje gdzieś się wpasować i gdzieś należeć. Nie potrafi się odnaleźć wśród Rekinów i się trochę boi wszystkiego. I nienawidzi faktu, że się tego wszystkiego boi. Poczciwe maleństwo, acz zachowuje się jak GROŹNA OSTRA LASKA.

### Co się rzuca w oczy

* Trochę szara myszka, ale bardzo próbuje pasować. Trochę za bardzo.

### Jak sterować postacią

* Jeśli ktoś zrobił jej krzywdę, nie mści się. Unika. Chowa się.
* Udaje dzielną i bojową, ale tak naprawdę to jest łagodna i unika konfliktów. Chętniej pomaga / koi niż krzywdzi.
* Łatwo daje się porwać grupie. Bardzo socjalna. Życie każdej imprezy.
* Zwykle zgłasza się na ochotniczkę, jeśli myśli, że może pomóc.
* Potrafi być waleczna, choć jak się ją naciśnie to się natychmiast wycofuje.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy miała 14 lat, perfekcyjnie przyjęła niezapowiedzianą wizytę Sowińskich jako wiodąca arystokratka. Nie zrobiła ani jednego błędu w etykiecie i zachowała się jak młoda dama.
* Anonimowo wygrała konkurs na "strasznego potwora" dla młodych talentów w Domenie Gwozdników. Rodzice jej pogratulowali, ale spalili jej rysunki. Nie to jest jej przyszłością.
* Mimo ciągłego monitoringu, udało jej się złożyć niewielkią pracownię w Domenie Gwozdników. Do końca nikt nie doszedł do tego, że nie przestała rysować.
* Podczas jednej z operacji Justyniana do polowania na potwory z Trzęsawiska stanowiła ochronę ogniową jako snajper. Bardzo skutecznie.
* W Podwiercie nawiązała współpracę z Galerią Sztuki Makabra, oczywiście anonimowo. Pod pseudonimem "Kirasjer" dostarcza rysunku różnych niepokojących istot.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Życie każdej imprezy. Potrafi łagodzić konflikty, świetnie się bawić i rozchmurzyć każdego. Genialnie tańczy.
* AKCJA: Zna etykietę, ma doskonałe poczucie stylu i jest perfekcyjną "damą". Tyle, że tego nie używa bo to oznaki słabości XD.
* AKCJA: Cat burglar. Świetnie się chowa. Bardzo szybko biega. Umie się włamywać.
* AKCJA: Bardzo spostrzegawcza i ma świetną pamięć. Praktycznie sokole oko. Dobrze wykrywa zagrożenia - gdzie się NIE pojawiać.
* AKCJA: Nieźle pilotuje ścigacz i świetnie strzela. Zwłaszcza snajperką. Jest bardzo celna, ale jej walka wręcz SSIE.
* AKCJA: Przepięknie rysuje. To jest to, co najbardziej lubi robić. Jej rysunki są bardzo realistyczne i bardzo plastyczne. Wybitna w _eldritch horror_ i aktach.
* COŚ: Dostęp do arsenału broni, do którego zdecydowanie nie powinna mieć dostępu. Jej ulubiona broń? Karabin snajperski.

### Serce i Wartości (3)

* Konformizm
    * Wierzy, że ważną i cenną rzeczą jest to, by tkanka społeczna działała i ludzie się lubili. Bardzo jej zależy, by to się działo.
    * Chce być częścią czegoś większego. Chce być zaakceptowana przez ród i przez jakąś grupę Rekinów. Chce być sprawnym elementem większej maszyny.
    * Pójdzie za tym, co chcą inni; trochę pasywna. Chyba, że to zagrozi jej reputacji, jej Rodowi lub ogólnie Tradycji.
* Twarz
    * Zależy jej na opinii innych na jej temat. Zależy jej, by przed sobą samą wyglądać na godną i sensowną. Nie chce być "trashy girl". A boi się, że jest.
    * Bardzo zależy jej, by dorównać obrazowi jej rodu. Silnie militarystyczny, bojowy ród ze zdolnymi wojownikami. Będzie dbała o swoją reputację.
    * Bardzo próbuje się popisać przed innymi, pokazać, jaka jest skuteczna i dobra.
    * Ma w sobie bardzo dużo godności i dumy - mimo wszystkiego. Nie pozwoli sobą pomiatać. Może jest myszką, ale myszka TEŻ ugryzie.
* Tradycja
    * WIE jakie powinny być Rekiny, WIE jak działają Rody Aurum - ale ona po prostu nie pasuje do tego obrazu. Próbuje jednak utrzymać istniejące systemy. Bo są dobre.
    * Jest święcie przekonana, że jej ród jest najlepszym rodem Aurum jaki jest. Specjaliści od miragentów i uzbrojenia.
    * Odrzuca publicznie swoje umiejętności związane ze sztuką. Póki jest wśród Rekinów, obniża status umiejętności związanych z byciem damą. GWOZDNIK MUSI WALCZYĆ.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "W moim rodzie wszyscy są świetni. Poza mną. Nigdy nie zaakceptują tego, że ja chcę być artystką. Że nie chcę walczyć."
* CORE LIE: "Tylko bycie wojowniczką i ostrą laską ma jakiekolwiek znaczenie. To jest dla mnie jedyna droga by coś osiągnąć. Wszystko inne jest godne pogardy i muszę ukrywać."
* Stosunkowo niski próg strachu - potrafi być ODWAŻNA, ale łatwo ją przestraszyć. Tzn. łatwo się poddaje woli innych.
* Bardzo skonfliktowana wewnętrznie. Z jednej strony musi być agresywna, z drugiej jest łagodniutka i spolegliwa.
* Fatalna w walce wręcz. FATALNA. To jej tak całkowicie nie wychodzi. I się boi walki w zwarciu.

### Magia (3M)

#### W czym jest świetna

* Mentalistka: potrafi wprowadzić drugą osobę w stan prawdziwego Piękna. Ekstatyczne delirium. Ukojenie, piękno, wrażenia, emocje.
* Mentalistka: mindbreak. Terror. Złamanie serca. Uczucia, które czasem czuje i nie chce ich pokazywać. Inkarnuje swoje rysunki ;-).
* Biomancja: potrafi przenieść "oczy", zmienić kanały wizualne (ciepło itp), wzmocnić wzrok itp. Dotyczy tylko zmysłu wzroku.

#### Jak się objawia utrata kontroli

* Atak w zmysły i ciało - tracimy wzrok, tracimy precyzję ruchów, jesteśmy zmęczeni itp.
* Niewłaściwe stany emocjonalne.
* Aportacja jakichś jej rysunków (zwykle wielu) lub referencyjnych zdjęć (zwłaszcza aktów, bo się ich najbardziej wstydzi).

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 
