## Metadane

* factions: "elisquid, multivirt"
* owner: "public"
* title: "Diana Tevalier"


## Postać

### Ogólny pomysł (2)

* W multivirt gra supportem; nazwa kodowa Chevaleresse. Członkini EliSquid. Bezwzględnie oddana Alanowi i gildii.
* Scavenger i dziecko ulicy. Potrafi przetrwać w praktycznie każdym mieście. Świetnie się chowa.
* Aktorka. Potrafi grać dużo młodszą niż jest. Wykorzystuje swoje umiejętności do przebierania się i maskowania.
* Kucharka. Jest świetną kucharką, potrafi zrobić niesamowicie smaczne jedzenie przy posiadaniu prawie niczego.

### Misja (co dla innych) (2)

* Połączyć multivirt i świat rzeczywisty. Dla niej są jednym światem; niech inni też to zobaczą. Pokazać piękno multivirt i połączyć światy.
* Ochraniać słabszych od siebie. Sprawić, by nie mieli takich koszmarnych obrazów do emisji jak Chevaleresse.
* Stworzyć jak najsilniejsze "rodziny" czy "gildie". Grupy wspierające się nawzajem, z ludzi którym bezgranicznie ufa.

### Motywacja (co dla mnie) (2)

* Wolność. Całkowita autonomia, swoboda poruszania się i możliwość robienia co tylko chce.
* Chce zawsze mieć rację; strasznie uparta z niej dziewczyna i nie boi się stanąć naprzeciw większym siłom od siebie.
* Nieufna, ale jeśli zaufa - oddaje lojalność całą sobą, bezgraniczną. Oczekuje tego samego.

### Wyróżnik (2)

* Poszerzony hipernet; z każdego miejsca potrafi wejść w multivirt. W multivirt jest skuteczniejsza i lepsza niż w realu.
* Koszmary i strach. Diana jest świetna pod względem przerażania wszystkich. Ma wyjątkowo wiarygodne obrazy. Dużo widziała.
* Potrafi się schować. Ma duży talent do chowania się i pozostawania niezauważoną.

### Zasoby i otoczenie (3)

* Spokrewniona ze znaną oficer, Teresą Tevalier. Przez to niektórzy jej idą lekko na rękę.

### Magia (3)

#### Gdy kontroluje energię

* Ukrywanie się i maskowanie: ukrywanie się, maskowanie, makijaż, iluzje... wszystko, by nie dało się dostrzec drobnej Chevaleresse.
* Koszmary i odstraszanie: odwracanie uwagi, terror, zabezpieczenie terenu. Bonus: przed owadami i dziką zwierzyną.
* Stabilizacja i przetrwanie: modyfikacja metabolizmu, przetrwanie w trudnych warunkach, podtrzymanie życia.
* Przyprawy i gotowanie: potrafi robić świetne i bardzo smaczne jedzenie.

#### Gdy traci kontrolę

* Multivirt i świat rzeczywisty stają się jednością

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)
