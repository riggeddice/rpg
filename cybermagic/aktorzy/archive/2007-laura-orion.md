## Metadane

* factions: "orbiter, kirasjerka orbitera, orbiter neomil"
* owner: "public"
* title: "Laura Orion"


## Kim jest

### Koncept

Hiperkompetentna Emulatorka Kirasjerów, niezbyt lubiana i bardzo pragmatyczna. Pochodzi z Eterni, jej ciało i umysł zostały poważnie zniszczone i zostało tylko pragnienie zemsty - wtedy zmieniono ją w Emulatorkę. Jedna z najbardziej świadomych Emulatorek Damiana.

### Motto

""

### Co się rzuca w oczy

* Walka nie sprawia jej przyjemności. Bardziej taktyk niż bezpośrednia wojowniczka.
* Gdy coś robi, daje z siebie wszystko. Idzie za daleko. Pożre, zniszczy, zdewastuje. Jest dzika i niepowstrzymana.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Zjednoczona z Esuriit, ciężko ranna, pożerała żywcem swoich eterniańskich oprawców - aż zatrzymał ją Damian i pokonał (a potem, Emulował).
* Z braku TAI, przejęła rolę Persefony i doprowadziła uszkodzony statek do celu (acz zapłaciła zdrowiem). Potężny umysł zdolny do Emulacji niejednej rzeczy.
* Gdy Damian był ciężko ranny, poddała Kirasjerów by tylko go uratować. Zaufała, że Orbiter nie zostawi ich samych.

### Z czym przyjdą do niej o pomoc? (3)

* 

### Jaką ma nieuczciwą przewagę? (3)

* Emulatorka - potrafi przełączyć swój zestaw umiejętności na taki, jaki ma załadowany. Ma slot na 3 zestawy.
* Homo Superior - świetnie uzbrojona, zdolna do działania w próżni, otoczona polem magicznym. Katai.

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: 
* WIEM: 
* OPINIA: 

### Typowe sytuacje z którymi sobie nie radzi (-3)

* 

## Inne

### Wygląd

.

### Coś Więcej

* Skażona przez Kijarę - obudziła w sobie Obsesję Esuriit.
* Naprawiana przez Nocną Kryptę - fuzja Emulatorki i bytu Esuriit. Zeiram-class.

### Endgame

* ?
