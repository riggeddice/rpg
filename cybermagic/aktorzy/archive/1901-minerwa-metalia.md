## Metadane

* factions: "pustogor"
* owner: "public"
* title: "Minerwa Metalia"


## Postać

### Ogólny pomysł (3)

* Neuronautka i naukowiec, specjalizująca się w psychotronice. Niezła konstruktorka power suitów. Naukowiec z serca.
* Kiedyś, urocza i otwarta Diakonka owijająca wszystkich jednym paluszkiem.
* Zrekonstruowana przez Saitaera i zregenerowana przez Pustogor w zwykłą czarodziejkę.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* AMBITION/KNOWLEDGE; ludzkość jest najlepszą możliwą formą siebie; promocja ciekawości i patrzenia co jest po drugiej stronie światła
* ACCEPTANCE/HOPE; jest częścią normalnego świata i jest cenioną badaczką; wspieranie oficjalnych struktur oraz swoich przyjaciół
* HEALTH/MINDFULNESS; jest świadoma swojego ciała i tego wszystkiego; lubi luksusy i rozpieszczanie. Lubi dotyk ciała. Lubi swoje ciało. Mała obsesja na punkcie ciała.
* żyje dzięki morderstwu i szalonemu bogowi; chce naprawić zło które się stało, ale i tęskni za Dotykiem Saitaera; być tak pomocną jak tylko może
* zdeterminowana, łagodna z natury, ciekawska, raczej melancholijna, skłonna do działania, kiepskie żarty, sarkastyczna

### Wyróżniki (3)

* Wybitnej klasy neuronautka: specjalizuje się w konstrukcji sztucznych umysłów i osobowości, ale też poradzi sobie z analizą
* Badaczka anomalii, zwłaszcza technomantycznych: potrafi robić cuda związane z dziwnymi anomaliami technomantycznymi
* Urocza dusza towarzystwa: potrafi się zachowywać w odpowiedni, bardzo pociągający innych sposób
* Ciało Saitaera: przede wszystkim defensywnie. Jest w stanie funkcjonować w warunkach niemożliwych dla maga czy człowieka

### Zasoby i otoczenie (3)

* Black Technology: rozumie dziwne technomantyczne anomalie. Saitaer jej to "dał". Nie wie czemu to rozumie i jak je budować, ale potrafi...
* Black Stories: dziwne opowieści, z odmiennych światów i czasów. Znowu, dar Saitaera. Im bardziej się oddalamy od Primusa, tym lepiej ona poznaje świat.
* Koszmary: Minerwa ma nieprawdopodobnie chore koszmary i myśli oraz nieludzki umysł. Jej ciało i umysł nie radzą sobie ze wspomnieniami Saitaera i nieludzkiego ciała.
* Wskrzeszona: większość magów którzy o niej wiedzą wie też o jej historii. Sprawia to, że Minerwa jest fundamentalnie minusem społecznie.

### Magia (3)

#### Gdy kontroluje energię

* Mentalistka: zwłaszcza w dziedzinie konstrukcji i tworzenia/modyfikacji sygnału. Raczej pracuje na wzorach (modelach mentalnych) niż pojedynczych myślach i pamięci
* Technomantka: przede wszystkim analiza i konstrukcja. Dotyk Saitaera dał jej unikalną zdolność rekonstrukcji mechanicznej
* Golemy technomantyczne: wiedza o psychotronice i Dotyk Saitaera dały jej coś nowego - umiejętność łatwego tworzenia inteligentnych agentów. Nigdy nie jest sama...

#### Gdy traci kontrolę

* Pojawiają się anomalie, zwłaszcza technomantyczne. Bonus, jeśli pochodzą z martwego świata Saitaera.
* Saitaer widzi obszar w którym zaklęcie jest rzucone przez Minerwę.

### Powiązane frakcje

{{ page.factions }}

## Opis

(15 min)
