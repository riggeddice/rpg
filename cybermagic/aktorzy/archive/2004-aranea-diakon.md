## Metadane

* factions: "szczeliniec, wolny uśmiech"
* owner: "public"
* title: "Aranea Diakon"


## Kim jest

### Paradoksalny Koncept

Pajęcza Diakonka, czarodziejka, która walczy o ludzkie prawa i bycie traktowaną jak inni. Doskonała infiltratorka oraz mistrzyni trucizn. Niezła uwodzicielka. Mimo przerażającej natury, strasznie lubi robić dowcipy i nikomu nie życzy źle. Uwielbia imprezy i łażenie po dyskotekach - zwłaszcza takich, gdzie jej nie chcą.

### Motto

"Wiesz, 'zabójczyni do wynajęcia' brzmi tak strasznie poważnie, no nie? Zwłaszcza, że jeszcze nikogo nie zabiłam. Ale przynajmniej wpuszczają mnie do zamkniętych lokali." 

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Istota rozproszona; Aranea składa się z wielu małych pająków które stanowią jej ciało i ubranie. Jeśli pająki przejdą, cała Aranea jest w stanie się przedostać.
* ATUT: Mistrzyni trucizn; jest w stanie syntetyzować różnego rodzaju trucizny i halucynogeny.
* ATUT: Infiltratorka; z uwagi na umiejętności kameleona i rozproszenia strasznie trudno jest ukryć sekrety przed Araneą.
* SŁABA: Niesamowicie wrażliwa na ogień i ataki gazowe. Jeśli coś skutecznie tępi rój, tępi też ją.
* SŁABA: Niezdolna do korzystania z większości standardowej techniki zakładającej bardziej spójne ciało, linie papilarne itp.
* SŁABA: Nie jest w stanie utrzymać pełni umiejętności na deszczu. Musi też unikać pływania. Ogólnie, kąpiel jest dla niej wyzwaniem.

### O co walczy (3)

* ZA: Może to świat dla ludzi i magów, ale do licha, ona też ma prawo współegzystencji w spokoju jako coś więcej niż tylko ozdoba czy dziwadło.
* ZA: Być na KAŻDEJ imprezie, wejść do KAŻDEJ dyskoteki. Wleźć wszędzie - a już w ogóle tam, gdzie mnie nie chcą.
* VS: Traktowanie innych tylko jako narzędzia. Każdy ma prawo do bycia szanowanym jako osoba, nieważne, kim jest.
* VS: Sztywność. Zakaz dowcipów. Serio - życie jest jedno i jeśli się NIE zrobi kilku dowcipów to można iść się utopić.

### Znaczące Czyny (3)

* Z zaskoczenia zneutralizowała CAŁY gang Dużego Toma, rozpraszając się na rój pająków i wszystkich jednocześnie zatruwając jadem.
* Uwiodła mało ważnego polityka najeżdżającego na anomaliczne istoty, po czym wystawiła go w hotelu. Ale wpierw zatruła jego wino by wyszedł na idiotę.
* Odstraszyła dzieciaki od bazy mafii, pokazując im, że tam jest dużo pająków. Nie chciała ich krzywdy gdyby coś znaleźli.

### Kluczowe Zasoby (3)

* COŚ: Trucizny i psychotropy, łącznie z wektorem wprowadzającym owe trucizny - jej własne pająki.
* KTOŚ: Bywam we wszystkich klubach i imprezowniach w Szczelińcu. Im bardziej muszę się wślizgnąć - tym lepiej.
* WIEM: Jak się dostać zwłaszcza tam, gdzie mnie nie chcą - to brzmi jak fajne wyzwanie.
* OPINIA: Najsympatyczniejsza zabójczyni i osoba kompromitująca innych jaką znajdziesz. Ładna i zwariowana. Szkoda, że trochę za bardzo lubi głupie dowcipy.

## Inne

### Wygląd

.

### Coś Więcej

.
