## Metadane

* factions: "sowińscy, epirjon"
* owner: "public"
* title: "Atena Sowińska"


## Postać

### Ogólny pomysł (3)

Elegancka i opanowana komendant stacji orbitalnej Epirjon. Kiedyś medyk, która przetrwała zniszczenie załogi Epirjona i przejęła dowodzenie, ratując stację. Świetnej klasy technomantka i ogólnie rozumiana czarodziejka wsparcia. Nie jest terminusem. Medyk i mechanik.

### Motywacja (gniew, zmiana, sposób) (2)

* nie jestem dość dobra, oni też nie są; wszyscy musimy być tak dobrzy by poradzić sobie ze wszystkim; push herself and others beyond the limits
* Epirjon jest stacją traktowaną jako kąsek polityczny; Epirjon daje nadzieję kontroli świata po Zaćmieniach; być najlepszym komendantem i mieć najlepszych ludzi
* nie ma dobrych wzorców, idoli czy ideałów; ona jako Sowińska musi być takim wzorem; być światłem w ciemności i żywym przykładem dla innych jak żyć
* sztywna, arystokratyczna, elegancka, apodyktyczna, bezwzględna, bardzo pracowita, opanowana, śnieżnobiały mundur

### Wyróżniki (2)

* zimna elegancja i opanowanie: praktycznie nie da się jej wyprowadzić z równowagi i zawsze zachowuje pełen spokój
* okrutna charyzma: potrafi własnym przykładem pociągnąć innych w ogień
* mechanik ASD: doskonale potrafi poradzić sobie z większością pojazdów ASD oraz stacją Epirjon. Wie o nich bardzo dużo
* utrzymywanie przy życiu: jako lekarz ASD potrafi poradzić sobie ze stabilizacją i utrzymaniem przy życiu większości osób, też przez taktyczną cyborgizację
* anielskie skrzydła i światło: TAK. Przez Skażenie potrafi latać i funkcjonuje nawet w próżni. Ale wygląda jak, no, anioł.

### Epicki moment (2)

* Potrafi naprawić praktycznie każdy pojazd ASD i go utrzymać w gotowości bojowej. Im sytuacja jest bardziej krytyczna, tym jest skuteczniejsza.
* Dowodzi operacjami mającymi na celu zatrzymać rozlewające się Skażenie lub katastrofę naturalną. Uratuje, wyleczy, opanuje.
* Czy medycznie czy technomantycznie, ale jej ludzie BĘDĄ pracowali przy użyciu jej umiejętności technomantycznych i medycznych.

### Zasoby i otoczenie (3)

* Stacja Orbitalna Epirjon: oczy i uszy Ateny, oraz polityczne wzmocnienie jej pozycji. Ona JEST Epirjonem.
* Echa Eteru Katastrofy Epirjona: czasem nawiedzają ją duchy i echa tego, co się wydarzyło. Czasem ją chronią i jej pomagają. Martwa Świta.
* Wysoka pozycja: mimo jej niechęci do polityki była obiecującą arystokratką. Nadal jest chroniona, zwłaszcza z uwagi na jej czyny.
* Znajomości w ASD; nie jest popularna, ale jest szanowana i co do jej moralności nikt nie ma cienia uwag.

### Magia (3)

#### Gdy kontroluje energię

* wybitnej klasy technomantka i czarodziejka materii skupiona na naprawianiu i analizie słabych stron konstrukcji
* wybitnej klasy magiczny lekarz, skupiona na ratowaniu życia i przetrwaniu w trudnych warunkach; też specjalizuje się w cyborgizacji
* katalistka, wyczuwająca pływy energii i skupiająca się na zasilaniu i wspomaganiu magitechnicznych bytów

#### Gdy traci kontrolę

* manifestuje się echo katastrofy Epirjona: technologia i magia sprzęgają się w Skażone twory i sprowadzają zagładę
* Atena z natury jest raczej pacyfistką, więc reakcja utraty kontroli - komuś może stać się krzywda przez technomancję czy biomancję
* stara dobra eksplozja energii magicznej - prawdziwa utrata kontroli katalitycznej

### Powiązane frakcje

{{ page.factions }}

## Opis

(10 min)

ASD: Astoriańskie Siły Defensywne