## Metadane

* factions: "pustogor, diakon"
* owner: "kić"
* title: "Pięknotka Diakon"


## Postać

### Koncept (3)

* Strażnik miasta lub obszaru, oddany nie prawu ale ludziom i zasadom
* Mimik ma dla każdego taką twarz jaka jest najbardziej lubiana
* Niszczyciel anomalii chroniący ludzkość ogniem i mieczem
* Chemiczna terminuska, łącząca świat ludzi i magów.
* Profesjonalna kosmetyczka, prowadzi niewielki, ale znany gabinet kosmetyczny.
* Członek ludzkich sił obrony terytorialnej, snajper

### Motywacja (gniew, zmiana, sposób) (3)

* dla mnie: ustabilizować Pustogor, to MOJE miasto
* dla mnie: lubiany, podziw, adoracja
* dla mnie: swoboda działania dzięki skuteczności
* dla mnie: mnóstwo znajomych i przyjaciół, na których można liczyć
* dla mnie: sława kosmetyczki
* dla innych: bezpieczeństwo i ochrona, dobre traktowanie innych istot rozumnych
* dla innych: wspólnota, radość, poczucie przynależności do grupy
* dla innych: ludzie w zgodzie z naturą
* dla innych: niech wszyscy są bezpieczni, dobro ponad prawo
* dla innych: niech chronią i szanują Pustogor
* dla innych: piękno. Niech kochają piękno i otaczają się rzeczami pięknymi

### Wyróżniki (3)

* Terminuska wykorzystująca walkę chemiczną. Łowczyni potworów.
* Surwiwalistka, specjalizująca się w trzęsawisku Zjawosztup.
* rośliny, zioła, trucizny ( w szczególności Zjawosztup)
* natura moim domem 
* zastraszanie, wyciąganie zeznań
* szybki bieg, zna miasto na wylot
* wykorzystanie i ustawienie terenu na swoją korzyść
* czytanie innych, obserwacja, plotki i wiadomości o innych
* aktorstwo, kłamstwa, manipulacja; dla każdego właściwa twarz
* walka z potworami, tropienie, pułapki
* dostosowanie do przeciwnika, spontaniczne wykorzystanie dostępnych zasobów
* przekonaj, przekonaj i jeszcze raz przekonaj. Jak się nie da - zastrasz.
* Swoje ciało traktuje jak surowiec, nie ma oporów przed jego wykorzystaniem lub poświęceniem.
* Twórczyni bojowych kosmetyków oraz (al)chemiczka.

### Zasoby i otoczenie (3)

* salon piękności z małym laboratorium
* oddział sił obrony terytorialnej
* struktury terminusów Pustogoru
* pancerz w najlepszym stlu MMORPG, mocno zmodyfikowany
* zna większość ważnych osób w mieście (bo salon)
* snajperka
* potrafi wydzielać kontaktowo lub wziewnie działające substancje
* zaprzyjaźnione wiły na trzęsawisku Zjawosztup

### Magia (3)

#### Gdy kontroluje energię

* W boju elementalna jak również chemiczna.
* W gabinecie alchemia i magia upiększająca.
* Czytanie i modyfikacje pamięci.

#### Gdy traci kontrolę

* Ujawnia się jej ból ("Jestem kosmetyczką!")
* Przyzywa echa swoich przyjaciół i bliskich.
* Elementy szaleją.

#### Powiązane frakcje

{{ page.factions }}

## Opis