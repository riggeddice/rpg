## Metadane

* factions: "elisquid, multivirt"
* owner: "public"
* title: "Diana Tevalier"


## Postać

### Ogólny pomysł (3)

* W multivirt gra supportem; nazwa kodowa Chevaleresse. Członkini EliSquid. Bezwzględnie oddana Alanowi i gildii.
* Scavenger i dziecko ulicy. Potrafi przetrwać w praktycznie każdym mieście. Świetnie się chowa.
* Aktorka. Potrafi grać dużo młodszą niż jest. Wykorzystuje swoje umiejętności do przebierania się i maskowania.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* 

### Wyróżniki (3)

* Poszerzony hipernet; z każdego miejsca potrafi wejść w multivirt. W multivirt jest skuteczniejsza i lepsza niż w realu.
* Koszmary i strach. Diana jest świetna pod względem przerażania wszystkich. Ma wyjątkowo wiarygodne obrazy.
* Kucharka. Jest świetną kucharką, potrafi zrobić niesamowicie smaczne jedzenie przy posiadaniu prawie niczego.

### Zasoby i otoczenie (3)

* 

### Magia (3)

#### Gdy kontroluje energię

* Ukrywanie się i maskowanie: ukrywanie się, maskowanie, makijaż, iluzje... wszystko, by nie dało się dostrzec drobnej Chevaleresse.
* Koszmary i odstraszanie: odwracanie uwagi, terror, zabezpieczenie terenu. Bonus: przed owadami i dziką zwierzyną.
* Stabilizacja i przetrwanie: modyfikacja metabolizmu, przetrwanie w trudnych warunkach, podtrzymanie życia.
* Przyprawy i gotowanie: potrafi robić świetne i bardzo smaczne jedzenie.

#### Gdy traci kontrolę

* 

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)
