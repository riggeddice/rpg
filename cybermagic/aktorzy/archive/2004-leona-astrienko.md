## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Leona Astrienko"


## Kim jest

### Paradoksalny Koncept

Ludzki dowódca marine w świecie magów. Pozornie słodkie, drobne stworzenie, niezrównana w walce wręcz. Urocza, acz przerażająca. Używa antymagicznych wszczepów, ale boją się jej z uwagi na podejście. W okrutny sposób zdewastuje każdego, kto deprecjonuje ludzi lub ich gnębi, ale jest bardzo miła wobec przyjaciół. Anty-biurokratka, anty-arystokratka, która lubi się bić i wygrywać.

### Motto

"Ludzie to nie są słabsi magowie czy coś do ochrony. My - ludzie - jesteśmy co najmniej tak dobrzy jak magowie i sami dajemy sobie radę."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Zastraszanie i terror. Jako człowiek w świecie magów - i co gorsza drobna dziewczyna - nauczyła się nadrabiać podejściem i bojowością jako oficer marines.
* ATUT: Niezrównana w walce w zwarciu. Nie dość że wszystko jest bronią to jeszcze jej wszczepy sprawiają, że niełatwo ją zneutralizować.
* ATUT: Antymagiczne wszczepy i sprzęt. Jako, że jest człowiekiem, to nie ma problemów ze stosowaniem dwustronnie antymagicznych bytów (np. lapisowanych). Nie przeszkadzają jej.
* SŁABA: Za bardzo jej zależy. Za bardzo się stara. Jest za twarda, zbyt bezwzględna, zbyt ostra. Robi przez to katastrofalne czasami błędy - zbyt ryzykuje, zbyt odpycha, kończy w szpitalu.
* SŁABA: Jest bardzo odporna na magię wszelaką, prawda? Łącznie z leczniczą / wspomagającą...
* SŁABA: Jest tylko człowiekiem - wiele osób nie traktuje jej poważnie. Ona wtedy odpowiada złamaniem ręki czy coś.

### O co walczy (3)

* ZA: Nie tylko magowie mają znaczenie! Leona zdecydowanie walczy o miejsce ludzi w załodze Orbitera i o ich znaczenie w świecie, gdzie magowie są coraz silniejsi.
* ZA: Klasyczny achiever - musi być najlepsza, najsilniejsza, najgroźniejsza. Jej skrajnie rywalizująca natura sprawia, że bywa nie do zniesienia.
* VS: Każda osoba deprecjonująca ludzi lub ich wykorzystująca bo jest silniejsza - zostanie ukarana. Np. pięścią w twarz.
* VS: Nie cierpi arogancji i arystokracji - wejdzie w bezpośrednie zwarcie z każdym, co "gada nie mając umiejętności podpierających takie słowa".

### Znaczące Czyny (3)

* Trzecie miejsce w turnieju walki w zwarciu marines; pokonała magów katai w walce 1v1. Do dzisiaj paraduje z dumą pokazując odznaczenie.
* Zatrzymała walkę w barze pomiędzy magami - częściowo swoją siłą woli i rozkazami a częściowo wybijając strategiczne zęby. Nie musiała, ale mogła.
* Podczas wypadku w laboratorium na Kontrolerze Pierwszym zatrzymała Anomalię kupując wszystkim czas na ucieczkę. Wynik - miesiąc w szpitalu.

### Kluczowe Zasoby (3)

* COŚ: Liczne wszczepy, też antymagiczne; zmieniają jej ciało w prawdziwego homo superior z perspektywy walki. Co więcej, dają jej możliwość używania Entropika.
* KTOŚ: Leona zawsze wchodzi w środowisko marines kosmicznych. Jest jedną z nich - a żołnierze sobie pomagają i gadają.
* WIEM: Zna większość typowych taktyk magów, uzbrojenia magów itp. Jeśli walczy przeciwko magom, jest najgroźniejszą wersją siebie.
* OPINIA: Niebezpieczna, nie podchodzić! Nieobliczalna, groźna oraz ogólnie chodzące ryzyko. Najlepiej zejść jej z drogi.

## Inne

### Wygląd

Mała, drobna, słodko wyglądająca blondyneczka. Zawsze ubrana w coś pokazującego, że jest marine - szturmowcem kosmicznym.

### Coś Więcej

.
