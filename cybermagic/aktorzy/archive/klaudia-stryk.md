## Metadane

* factions: ""
* type: "NPC"
* owner: "kić"
* title: "Klaudia Stryk"
 

## Postać

### Co robi?
- bada, analizuje, używa anomalii
- przechwytuje każdą komunikację
- zauważa subtelne szczegóły
- konstruuje i wysyła drony
- nawiguje w gąszczu biurokracji

### Czego chce?

- TAK: dogłębnie zrozumieć magię
- TAK: zapewnić bezpieczeństwo sobie i otoczeniu
- TAK: dotrzeć do prawdy
- NIE: odrzucić wiedzę

### Jak działa?

- TAK: postawić na swoim
- NIE: głupi upór
- TAK: nie przebierać w środkach
- TAK: metodycznie

### Zasoby

- Laboratorium na statku
