## Metadane

* factions: "maus, luxuritias, cieniaszczyt"
* owner: "public"
* title: "Kasjopea Maus"


## Postać

### Ogólny pomysł (3)

* Kiedyś dziecko ulicy, odratowane przez Mausów. Trochę nożowniczka, trochę chemik, zdecydowanie włamywaczka i akrobatka.
* Dzisiaj reżyser holokostek - wywołuje silne emocje i wydarzenia, po czym je nagrywa. Kocha miejskie życie i ryzyko.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* INTENSITY/RECREATION; ludzie budzą w sobie prawdziwą naturę i przestają się bać życia; wymuszanie ryzyka i produkcja holokostek
* FAME/INFLUENCE; jest znana i podziwiana, zawsze w centrum uwagi; popisuje się jak cholera i zawsze spotlight musi być jej
* RISK/PASSION; chce zawsze czuć, że żyje na krawędzi; bierze na siebie najtrudniejsze i najdziwniejsze rzeczy, niezależnie od konsekwencji.

### Wyróżniki (3)

* Wtapia się w każde miasto, zwłaszcza w mroczniejsze fragmenty. Ma nienaturalny dar czucia pulsu miasta i znajdowania kłopotów.
* Cat burglar, włamywaczka i akrobatka. Bardzo zwinna i szybka; wyjątkowo trudno ją złapać i unieruchomić.
* Reżyser emocji, czy to przy użyciu środków psychoaktywnych, czy przez zbudowanie odpowiedniej "opowieści".

### Zasoby i otoczenie (3)

* Konstruktor holokostek pamięci, dzięki temu urządzeniu jest w stanie budować obraz cudzych wspomnień by przeżyć to co oni (z doświadczeniami i uczuciami)
* Różnego rodzaju nielegalne środki psychoaktywne, od wzmacniania i wywoływania pewnych uczuć po podkładanie by kompromitować inne osoby
* Pochowane noże, zarówno te do rzucania jak i te do walki wręcz. Zawsze jest uzbrojona a broń zatruta jest środkami psychoaktywnymi.
* Szacun w środowiskach niższych, zwykle jest w jakimś stopniu znana wśród lokalnego 'elementu' jako niebezpieczna daredevil.

### Magia (3)

#### Gdy kontroluje energię

* Stymulatory i narkotyki, forma magii leczniczej pozwalającej na wzmocnienie ciała i działanie DALEJ i MOCNIEJ. Też na wywołanie stanów emocjonalnych.
* Wzmocnienie odczuwanych emocji, forma magii mentalnych pozwalająca na zdecydowanie wzmocnienie uczuć odczuwanych przez osoby obecne i ich uskrajnienie.
* Ogólnie rozumiana magia ochronna, od działań mentalnych po fizyczne. Nie lubi tego używać (redukuje stawkę), ale może rzucić na kogoś innego kto nie wie.

#### Gdy traci kontrolę

* Zazwyczaj, poziom chaosu i ryzyka zdecydowanie rośnie - jej magia podnosi stawki i stopień trudności w formie Magii Chaosu - zbieg i splot okoliczności, który jest mało prawdopodobny ale AKURAT się to przytrafiło. Dodatkowo, wszyscy wiedzą, że to dzięki niej.

### Powiązane frakcje

{{ page.factions }}

## Opis

.
