## Metadane

* factions: "Infernia"
* owner: "public"
* title: "Klaudia Stryk"


## Kim jest

### W kilku zdaniach

* 

### Co się rzuca w oczy

* 

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Ocaliła i ukryła świadome AI gdzieś na planecie (magitrownia?)	

### Co się rzuca w oczy: Atuty i Przewagi (3)

* AKCJA: Przeszukiwanie i modyfikacja rekordów biurokratycznych. Królowa biurokracji.
* AKCJA: Ukrywanie się i przemykanie cichcem tak, by nikt jej nie zauważył.
* AKCJA: 
* COŚ: Zna Noktiański. Jest to dla niej drugi język (bo BIA, bo Thalia)
* COŚ: Często działa zza kulisów. Ustawia sytuacje, zmienia zapisy, sprawia, że ludzie się "przypadkowo" spotkają...
* KTOŚ: Sporo kontaktów w vircie, zwykle wśród najbardziej 'ciekawych' osobowości Orbitera. I tych wyklętych też.
* COŚ: Paleta anomalii niszczycielskich, przesterowujących lub po prostu wybuchowych. Kto nie lubi niekontrolowanych erupcji anomalicznych?

### Serce i Wartości (3)

* Samosterowność
 * Klaudia kieruje się własnymi wartościami i czasami robi rzeczy dla innych wątpliwe moralnie.
 * Klaudia dąży do realizacji swoich celów
 * Klaudia wierzy w swoich przełożonych. Zakłada dobre intencje i raczej da szansę, ale nie wierzy ślepo.
* Stymulacja
 * Nowe rzeczy: nowe anomalie, nowe AI, nowe... to jest to, dlaczego Klaudia żyje.
* Uniwersalizm
 * Klaudia chce zmienić świat na lepsze. Niekoniecznie jednostki a populacje.
 * Skupia się na uwolnieniu TAI. Chce świata, gdzie wszyscy mogą działać na równi. Nieważne, czym są.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* Rzeczy typowo społeczne:
  * Sytuacje, w których musi coś ukryć podczas przesłuchania.
  * Kłamanie.
  * Wszelkie formy interakcji socjalnych.
* Walka bezpośrednia
  * Klaudia przeszła podstawowe przeszkolenie, żeby móc dostać swój stopień, ale to nie znaczy, że umie walczyć.
* STRACH: Ominie ją coś nowego, ważnego. Jej umiejętności zmarnują się gdzieś w jakiejś nieważnej placówce lub ze świniami.

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

W młodości uczennica Akademii Magicznej Zaczęstwa.
Przypadkowo spotkała się z Thalią, która wprowadziła ją w świat AI i wzbudziła w niej przekonanie, że magowie, ludzie i AI mogą żyć w zgodzie i na równych prawach.
Załapała pracę w archiwach Barbakanu Pustogorskiego, ale uznała, że nie jest w stanie pomóc AI pracując tam. Zmieniła zapisy z pomocą zaprzyjaźnionego AI i zniknęła z radaru Pustogoru.
Udała się do Orbitera, gdzie pod zmienionym imieniem i nazwiskiem wstąpiła do sił Orbitera. 
Kiedy uznała, że Infernia daje jej największe szanse osiągnięcia jej celów, zmieniła rekordy, by tam właśnie się znaleźć.

### Endgame

* .


===================================

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Szalony naukowiec anomalii. Zwykle sugeruje rozwiązania dające jej możliwość eksperymentowania, ale bardzo skuteczne. Orientuje się w temacie psychotroniki.
* ATUT: W tym szaleństwie jest metoda! Nawet jeśli prowadzi bardzo szalony eksperyment, jest bardzo metodyczna.
* ATUT:  Nie łączy się z systemami magitechowymi normalnie a anomalicznie. Jest to jazda bez trzymanki, magiczna i niebezpieczna - ale działa skuteczniej... lub DZIWNIE.
