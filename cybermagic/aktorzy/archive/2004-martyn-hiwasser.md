## Metadane

* factions: "orbiter, oddział verlen"
* owner: "public"
* title: "Martyn Hiwasser"


## Kim jest

### Paradoksalny Koncept

Lekarz wojskowy odpowiadający agresją na agresję. Lekarz skupiający się na statku kosmicznym, nie tylko załodze. Kobieciarz, który nigdy nie nadużyłby pozycji by wykorzystać kobietę. Wybitny biokonstruktor, który kocha piękno niezmoderowanego ludzkiego ciała. Martyn jest jednym z lepszych biokonstruktorów i magów, lojalny Ariannie Verlen i skupiający wszystkie siły do redukowania cierpienia. Niestety, przez niewyparzoną gębę i ostre podejście do głupoty i zasad robi sobie sporo wrogów i nie ma wielu przyjaciół... płci męskiej. Specjalizuje się w działaniu "na miejscu", nie "po czasie".

### Motto

"Utrzymam wszystkich przy życiu - dla uroczej pani kapitan."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Wybitny lekarz i biokonstruktor, zachowuje zimną krew pod ostrzałem. Leczy i adaptuje ludzi do celu.
* ATUT: Czar i urok osobisty, zwłaszcza wobec kobiet. Bardzo społeczny, "doskonały kumpel".
* SŁABA: Słabość do płci niewieściej. Wpada w pułapki, nie powstrzyma żadnego sekretu, przerażająco ufny.
* SŁABA: Na agresję odpowiada agresją; ma kompleks paladyna. Ma niewyparzoną gębę i potrafi powiedzieć komuś co myśli.

### O co walczy (3)

* ZA: Podrywanie, flirt, dobra zabawa - dla siebie i innych.
* ZA: Statek na którym jest Martyn ma funkcjonować jak w zegarku. To jak ciało ludzkie, które ma działać.
* VS: Zwalcza ponuractwo, brak poczucia humoru i nadużywanie pozycji.
* VS: Nadpisze i zignoruje wszelkie polecenia mające prowadzić do cierpienia ludzi i magów.

### Znaczące Czyny (3)

* Wraz z Ateną Sowińską uratowali część załogi ASD 'Perceptor' przed Serenitem; on stymulował ludzi, ona maszyny. Uciekli, acz z ofiarami.
* Zatrzymał konflikt między dwoma frakcjami Orbitera, bo... zaprzyjaźnił się z damami dowodzącymi oboma frakcjami i zmusił je do rozmowy.
* Gdy 'Infernia' umierała po walce z Leotisem, przesunął wielu ludzi do działania w próżni magitechowo - by mogli utrzymać statek przy życiu.

### Kluczowe Zasoby (3)

* COŚ: Niezależnie od okoliczności, Martyn ZAWSZE ma pod ręką pasujący strój kobiecy na każdą okazję. No questions asked.
* KTOŚ: Na wielu statkach Orbitera ma przyjaciółki i kochanki. Na wielu statkach ma też wrogów.
* WIEM: Ogromna wiedza o ludzkim ciele i anomaliach magów; potrafi robić z ludzkim substratem niemożliwe rzeczy.
* OPINIA: Uroczy kobieciarz i jednocześnie świetny lekarz. W zasadzie niegroźny.

## Inne

### Wygląd

Brązowe włosy, szare oczy, ubrany w lekko wypłowiały mundur. Chłopięcy wygląd, z lekką nutką nieśmiałości i bezradności. Ma bardzo ładny głos.

### Coś Więcej

?