## Metadane

* factions: "cieniaszczyt"
* owner: "public"
* title: "Pietro Dwarczan"


## Postać

### Ogólny pomysł (3)

Silny i dobrej klasy atleta, opiekuńczy. Wygląda jak prawdziwy epicki wiking. Świetny DJ. Imprezowy zwierz, uwielbiający zabawę, rywalizację i przygody. Silny kręgosłup każdej ekipy. Kiedyś gladiator; do dzisiaj mistrz rzucania przedmiotami do celu.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* ENJOYMENT/FREEDOM; życie i tak ma wiele problemów, więc idźmy radością; czy przez muzykę czy przez imprezę, on to naprawi 
* ludzie dzielą wszystkich i szukają różnic; wszyscy powinniśmy dawać innym szanse; zawsze powita innego i stanie przeciw wykluczeniu
* ciągłe zastanawianie się co wolno a co nie; idź za sercem, mniej polityki; nie będzie pieprzył się w politykę a robił to co uważa za słuszne
* łagodny, kobieciarz, opiekuńczy, przyjazny 

### Wyróżniki (3)

* Miotanie przedmiotami: mało kto tak jak on potrafi rzucić czymś i trafić dokładnie tam gdzie chciał.
* Król imprez: DJ elektroniki, techno i hardstyle - sama jego obecność może zmienić imprezę w legendarną.
* Łagodzenie konfliktów: potrafi obniżyć poziom złości i problemów, dogada się z większością ludzi i ogólnie "chill out, man".
* Wiara w swoje ideały: incorruptible, a przynajmniej taki próbuje być, niezależnie od okoliczności.
* Walczy z przeciwnościami losu lub pogody, wspina się gdzieś czy próbuje przetrwać w dziczy. Ciało zawodzi, lecz on idzie dalej.

### Zasoby i otoczenie (3)

* Epicki sprzęt muzyczno-dźwiękowy: niezależnie od okoliczności, the rave can be started!
* Lokalna / internetowa sława: nie jest może bardzo sławny, ale jest lubiany i popularny w Cieniaszczycie i w internecie

### Magia (3)

#### Gdy kontroluje energię

* Magia soniczna: czy to dla muzyki czy do zniszczenia czegoś, jego moc skupia się na dźwiękach i muzyce
* Magia iluzji: skupiona przede wszystkim w kierunku imprez i efektowności. Impreza musi trwać!
* Magia lecznicza: zwłaszcza w obszarze kojenia bólu i usuwania niekorzystnych efektów narkotyków. Ale też - leczenie (acz średnio).

#### Gdy traci kontrolę

* Smutna muzyka: głośna, smutna muzyka niszcząca klimat i wszelkie próby 'stealth'...
* Aura miłości: ludzie zaczynają się mieć ku sobie. Przychodzi impreza lub przychodzi niekontrolowana miłość i radość

### Powiązane frakcje

{{ page.factions }}

## Opis

.
