# Praca nad ekstrakcją Streszczenia
## Spis treści

1. Wykorzystanie - aktualna metodyka
2. Idea
3. Konstrukcja
4. Checkpoints (tu znajdziecie Style Guide / Instructions)

## 1. Idea

Ekstrakcja TYLKO streszczeń. 

* Hipoteza 1: 
    * Metodologia
        * Wpierw opis danych
        * Potem pokazanie stylu (hodowla z 10 streszczeń)
        * Potem few-shots z 4-5 raportów
        * Potem one-shot dla istniejącego raportu celem kalibracji
        * Potem aplikacja do realnych sytuacji

## 2. Wykorzystanie
### UNLEASH

Otrzymasz tekst sesji RPG jako załączony plik. Twoim zadaniem będzie zbudowanie Streszczenia sesji RPG podobnym do przykładów (które otrzymasz w sekcji PRZYKŁADY STRESZCZEŃ), zgodnie z instrukcją konstrukcji Streszczeń (którą otrzymasz w sekcji KONSTRUKCJA STRESZCZEŃ).

Sesje RPGowe mają specyficzny opis danych, który otrzymasz w sekcji OPIS DANYCH

===
OPIS DANYCH:

Poniżej wycinek przykładowej sesji RPG:

```
Jak tylko udało się rozmontować barierę, Xavier i Ewelina przeszli przez śluzę. Dokładnie po drugiej stronie znajduje się straszliwy mackowy potwór, który czekał w zasadzce. Złapał Ewelinę by ją rozszarpać na kawałki. Ewelina próbuje się bronić używając swojej broni - zarówno karabinu laserowego jak i shotguna.

Tr +3: (oderwanie się)

* X: broń mało skuteczna, potwór bardzo silny; scutier Eweliny zostaje uszkodzony
* Vr: rzuciłaś się do tyłu - macki odpadają
* V: SUKCES. Zgodnie z rozkazem, Xavier zatrzasnął śluzę overridem. Udało się - macki są po tej stronie, potwór po tamtej, Ewelina jest wolna. Udało się Wam przynajmniej tymczasowo zatrzymać stwora - wszystkie macki są zniszczone.

Po drugiej stronie śluzy jest więcej pyłku niż po tej. (część z macek ruszała się KORZYSTNIEJ niż powinny z perspektywy Eweliny).

* Mara: Nie wiem co zrobiliście, ale w mojej okolicy jest cicho
* Ewelina: Gdzie się ukrywasz?
* Mara: W medycznym, zabarykadowana. Próbuję nie wychodzić.
```

Wyjaśnię, jak to czytać. Poniżej przykład dialogu:

```
* [Jedna osoba]: tekst tekst
* [Druga osoba]: tekst tekst
```

Poniżej przykład testu (testy występują w wariantach: Tp, Tr, Ex, Hr - Typowy, Trudny, Ekstremalny, Heroiczny). Testy są bardzo istotne i robią silną zmianę rzeczywistości.

```
[Ktoś] próbuje [coś osiągnąć]

Tr +3:

* X: [konsekwencje porażki]
* Vr: [konsekwencje sukcesu]
```

Jeśli chodzi o sam Raport, warto zaznaczyć istotne sekcje:

```
### Projekt Wizji Sesji   <-- ta sekcja pokazuje ideę sesji RPG i wysokopoziomowo do czego MG dąży. To wizja, a nie dokładnie co się tam stało. Pokazuje 'szkielet opowieści'.
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)   <-- to są fakty, które się wydarzyły przed sesją. Możesz tego użyć by zrozumieć kontekst sesji.
### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)   <-- zignoruj tę sekcję, ona nie ma znaczenia
### Scena Zero - impl           <-- kluczowa sekcja, na tym pracujesz przede wszystkim
### Sesja Właściwa - impl       <-- kluczowa sekcja, na tym pracujesz przede wszystkim
```
===

===
KONSTRUKCJA STRESZCZEŃ:

### Kompletny Style Guide dla Streszczeń Sesji RPG

#### 1. Zwięzłość i Celność
- **Skupienie na kluczowych problemach i ich rozwiązaniach**, z jasnym wskazaniem przyczyn i skutków. Streszczenie ma być precyzyjne, zawierając mniej niż 1000 znaków.

#### 2. Jasność Wypowiedzi
- **Bezpośrednie wskazanie na antagonistów, konflikty i metody ich rozwiązania**. Wyraźne określenie akcji i ich bezpośrednich rezultatów.

#### 3. Konkretyzacja Działania
- **Wyraźne opisanie działań podjętych przez postacie** i ich bezpośrednich skutków, z naciskiem na rozwiązanie problemu.

#### 4. Ograniczenie Detali
- Zachowanie tylko tych szczegółów dotyczących postaci i wydarzeń, **które bezpośrednio przyczyniają się do rozwoju akcji** i rozwiązania konfliktu.

#### 5. Zachowanie Tonu
- Utrzymanie charakterystycznego tonu narracji, z większym naciskiem na **klarowność i bezpośredniość wypowiedzi**.

#### 6. Struktura i Porządek
- Streszczenie powinno mieć **jasną strukturę**: wprowadzenie problemu, opis działań i rozwiązanie, podsumowanie skutków.

---
### Kompletna Instrukcja Ekstrakcji Streszczenia z Raportu

#### 1. Identyfikacja Głównego Problemu
- **Znajdź i wyraźnie określ kluczowy problem lub konflikt sesji**. Jest to punkt wyjścia dla całego streszczenia.

#### 2. Wskazanie Antagonisty
- **Jasno zidentyfikuj głównego antagonistę lub źródło konfliktu**. Wskazanie, kim lub czym jest przeszkoda dla postaci.

#### 3. Opisanie Działania Agencji/Grupy
- **Skup się na działaniach podjętych w celu rozwiązania problemu**, wskazując na konkretne metody i ich skutki.

#### 4. Podsumowanie Rozwiązania
- **Jasno opisz, jak problem został rozwiązany**. Zwróć uwagę na działania, które miały kluczowy wpływ na koniec konfliktu.

#### 5. Ograniczenie Informacji Pobocznych
- **Skoncentruj się na najważniejszych informacjach**. Eliminuj detale nieistotne dla głównego wątku konfliktu i jego rozwiązania.

#### 6. Struktura Streszczenia
- Streszczenie powinno być zorganizowane **chronologicznie lub logicznie**, z wyraźnym początkiem (problem), środkiem (działania) i końcem (rozwiązanie).

#### 7. Użycie Cytatów i Dialogów
- Nie używaj bezpośrednich cytatów ani dialogów

===

===
PRZYKŁADY STRESZCZEŃ:

1:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

2: 

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

3:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

4:

Sensacjusz został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

5:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

6:

Kaprys eternijskiej arystokratki, która chciała pozyskać broszkę i zażądała tego od swojej wygnanej służki splótł losy owej służki z Martynem Hiwasserem. Ten od razu zwąchał okazję do założenia HANDLU BIŻUTERIĄ EGZOTYCZNĄ! Plan był niezły - ale ktoś podrobił część biżuterii (krwawe rubiny), dzięki czemu sygnał poddaństwa wobec tien Kopiec stał się ozdobą. Gdy Martyn i Kalina się z tego wyplątywali, nie osiągnęli ogromnego majątku - ale przynajmniej nikomu nic się nie stało.

7:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

8:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.
===

Wykonaj instrukcję - zbuduj Streszczenie z załączonego dokumentu.



### Prompt 1 - seedowanie przykładu

Poniżej masz istniejące Streszczenia sesji RPG (osiem Streszczeń). Jako, że sesje są różne to Streszczenia czasami zwracają uwagę na różne aspekty, ale wszystkie mają pewien spójny wzór. Zwróć uwagę na styl, zwięzłość i charakter Streszczeń jakie Ci wklejam poniżej. To przykłady, które mają Cię prowadzić do tego czego szukam. Powiedz "rozumiem".

Streszczenia:

1:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

2: 

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

3:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

4:

Sensacjusz został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

5:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

6:

Kaprys eternijskiej arystokratki, która chciała pozyskać broszkę i zażądała tego od swojej wygnanej służki splótł losy owej służki z Martynem Hiwasserem. Ten od razu zwąchał okazję do założenia HANDLU BIŻUTERIĄ EGZOTYCZNĄ! Plan był niezły - ale ktoś podrobił część biżuterii (krwawe rubiny), dzięki czemu sygnał poddaństwa wobec tien Kopiec stał się ozdobą. Gdy Martyn i Kalina się z tego wyplątywali, nie osiągnęli ogromnego majątku - ale przynajmniej nikomu nic się nie stało.

7:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

8:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

### Prompt 2 - opis danych

W następnym prompcie otrzymasz ode mnie Raport w formie pliku tekstowego `xxx.txt`. Poniżej wycinek przykładowego raportu:

```
Jak tylko udało się rozmontować barierę, Xavier i Ewelina przeszli przez śluzę. Dokładnie po drugiej stronie znajduje się straszliwy mackowy potwór, który czekał w zasadzce. Złapał Ewelinę by ją rozszarpać na kawałki. Ewelina próbuje się bronić używając swojej broni - zarówno karabinu laserowego jak i shotguna.

Tr +3: (oderwanie się)

* X: broń mało skuteczna, potwór bardzo silny; scutier Eweliny zostaje uszkodzony
* Vr: rzuciłaś się do tyłu - macki odpadają
* V: SUKCES. Zgodnie z rozkazem, Xavier zatrzasnął śluzę overridem. Udało się - macki są po tej stronie, potwór po tamtej, Ewelina jest wolna. Udało się Wam przynajmniej tymczasowo zatrzymać stwora - wszystkie macki są zniszczone.

Po drugiej stronie śluzy jest więcej pyłku niż po tej. (część z macek ruszała się KORZYSTNIEJ niż powinny z perspektywy Eweliny).

* Mara: Nie wiem co zrobiliście, ale w mojej okolicy jest cicho
* Ewelina: Gdzie się ukrywasz?
* Mara: W medycznym, zabarykadowana. Próbuję nie wychodzić.
```

Wyjaśnię, jak to czytać. Poniżej przykład dialogu:

```
* [Jedna osoba]: tekst tekst
* [Druga osoba]: tekst tekst
```

Poniżej przykład testu (testy występują w wariantach: Tp, Tr, Ex, Hr - Typowy, Trudny, Ekstremalny, Heroiczny). Testy są bardzo istotne i robią silną zmianę rzeczywistości.

```
[Ktoś] próbuje [coś osiągnąć]

Tr +3:

* X: [konsekwencje porażki]
* Vr: [konsekwencje sukcesu]
```

Jeśli chodzi o sam Raport, warto zaznaczyć istotne sekcje:

```
### Projekt Wizji Sesji   <-- ta sekcja pokazuje ideę sesji RPG i wysokopoziomowo do czego MG dąży. To wizja, a nie dokładnie co się tam stało. Pokazuje 'szkielet opowieści'.
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)   <-- to są fakty, które się wydarzyły przed sesją. Możesz tego użyć by zrozumieć kontekst sesji.
### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)   <-- zignoruj tę sekcję, ona nie ma znaczenia
### Scena Zero - impl           <-- kluczowa sekcja, na tym pracujesz przede wszystkim
### Sesja Właściwa - impl       <-- kluczowa sekcja, na tym pracujesz przede wszystkim
```

Rozumiesz instrukcję?

### Prompt 3 - zrobienie Streszczenia (zmieniaj Style Guide / Instrukcję w zależności od typu raportu)

Podaję Ci Raport w formie pliku tekstowego `xxx.txt`. 

Zrób Streszczenie zgodnie z Instrukcją Ekstrakcji Streszczenia w taki sposób, by Streszczenie było zgodne ze Style Guide. Uwzględnij wytyczne powyżej.

===

### Kompletny Style Guide dla Streszczeń Sesji RPG

#### 1. Zwięzłość i Celność
- **Skupienie na kluczowych problemach i ich rozwiązaniach**, z jasnym wskazaniem przyczyn i skutków. Streszczenie ma być precyzyjne, zawierając mniej niż 1000 znaków.

#### 2. Jasność Wypowiedzi
- **Bezpośrednie wskazanie na antagonistów, konflikty i metody ich rozwiązania**. Wyraźne określenie akcji i ich bezpośrednich rezultatów.

#### 3. Konkretyzacja Działania
- **Wyraźne opisanie działań podjętych przez postacie** i ich bezpośrednich skutków, z naciskiem na rozwiązanie problemu.

#### 4. Ograniczenie Detali
- Zachowanie tylko tych szczegółów dotyczących postaci i wydarzeń, **które bezpośrednio przyczyniają się do rozwoju akcji** i rozwiązania konfliktu.

#### 5. Zachowanie Tonu
- Utrzymanie charakterystycznego tonu narracji, z większym naciskiem na **klarowność i bezpośredniość wypowiedzi**.

#### 6. Struktura i Porządek
- Streszczenie powinno mieć **jasną strukturę**: wprowadzenie problemu, opis działań i rozwiązanie, podsumowanie skutków.

#### 7. Unikalność Stylu
- Zachowanie **unikalnego stylu i osobowości narracji**, odpowiedniego dla świata przedstawionego, z zachowaniem zasad poprzednich punktów.

---
### Kompletna Instrukcja Ekstrakcji Streszczenia z Raportu

#### 1. Identyfikacja Głównego Problemu
- **Znajdź i wyraźnie określ kluczowy problem lub konflikt sesji**. Jest to punkt wyjścia dla całego streszczenia.

#### 2. Wskazanie Antagonisty
- **Jasno zidentyfikuj głównego antagonistę lub źródło konfliktu**. Wskazanie, kim lub czym jest przeszkoda dla postaci.

#### 3. Opisanie Działania Agencji/Grupy
- **Skup się na działaniach podjętych w celu rozwiązania problemu**, wskazując na konkretne metody i ich skutki.

#### 4. Podsumowanie Rozwiązania
- **Jasno opisz, jak problem został rozwiązany**. Zwróć uwagę na działania, które miały kluczowy wpływ na koniec konfliktu.

#### 5. Ograniczenie Informacji Pobocznych
- **Skoncentruj się na najważniejszych informacjach**. Eliminuj detale nieistotne dla głównego wątku konfliktu i jego rozwiązania.

#### 6. Struktura Streszczenia
- Streszczenie powinno być zorganizowane **chronologicznie lub logicznie**, z wyraźnym początkiem (problem), środkiem (działania) i końcem (rozwiązanie).

#### 7. Użycie Cytatów i Dialogów
- **Ogranicz użycie bezpośrednich cytatów i dialogów**, chyba że są one niezbędne do zilustrowania kluczowego momentu.

#### 8. Refleksja i Konsekwencje
- Zakończ streszczenie **wskazówkami na przyszłość lub refleksją** nad skutkami wydarzeń, jeśli jest to istotne dla kontynuacji historii.

Te dokumenty mają służyć jako wytyczne do tworzenia spójnych, zwięzłych i klarownych streszczeń sesji RPG, które są zarówno informacyjne, jak i angażujące dla czytelnika.


==============================
==============================
==============================
==============================


## 3. Prompty i parametry
### 3.1. Hipoteza 1, 2403121114
#### 3.1.1. Pierwsze podejście
##### Prompt 1 - wyjaśnienie

Zbudujemy razem sposób budowania dobrych Streszczeń sesji RPG z napisanych Raportów. Twoim zadaniem będzie konstruowanie Streszczeń zgodnych ze stylem Streszczeń które Ci zaraz zamieszczę. 

* Krok 1: podam Ci serię Streszczeń - z tego wyprowadzisz Style Guide
* Krok 2: podam Ci kilka Raportów Sesji RPG, opis kluczowych danych oraz jakie Streszczenia stworzyłem - z tego wyprowadzisz sposób ekstrakcji
* Krok 3: podam Ci prawdziwe Raporty Sesji RPG i napiszę jakie Streszczenie bym zrobił - to da Ci kalibrację

Czy rozumiesz instrukcję?

##### Prompt 2 - streszczenia do Style Guide

Poniżej masz istniejące Streszczenia sesji RPG (osiem Streszczeń). Jako, że sesje są różne to Streszczenia czasami zwracają uwagę na różne aspekty, ale wszystkie mają pewien spójny wzór. Zwróć uwagę na styl, zwięzłość i charakter Streszczeń jakie Ci wklejam poniżej. Jakie cechy mają te Streszczenia? Napisz to jako Style Guide dla siebie.

Streszczenia:

1:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

2: 

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

3:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

4:

Sensacjusz został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

5:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

6:

Kaprys eternijskiej arystokratki, która chciała pozyskać broszkę i zażądała tego od swojej wygnanej służki splótł losy owej służki z Martynem Hiwasserem. Ten od razu zwąchał okazję do założenia HANDLU BIŻUTERIĄ EGZOTYCZNĄ! Plan był niezły - ale ktoś podrobił część biżuterii (krwawe rubiny), dzięki czemu sygnał poddaństwa wobec tien Kopiec stał się ozdobą. Gdy Martyn i Kalina się z tego wyplątywali, nie osiągnęli ogromnego majątku - ale przynajmniej nikomu nic się nie stało.

7:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

8:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

##### Prompt 3 - Raport plus Streszczenie.
###### Initial

Przechodzimy do Kroku 2: Raport plus Streszczenie. Ten krok powtórzymy wielokrotnie jako few-shot learning.

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt` oraz pasujące do niego Streszczenie. 

NAJPIERW jeśli Style Guide musi się zmienić, napisz nowy uaktualniony Style Guide.
POTEM stwórz instrukcję dla siebie jak wyciągnąć takie Streszczenie z tego Raportu. 

Streszczenie do załączonego raportu:


<załącz *-INPUT.txt>
<wklej Streszczenie>

###### Evolving

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt` oraz pasujące do niego Streszczenie. 

NAJPIERW powtórz aktualny Style Guide i aktualną Instrukcję Ekstrakcji Streszczenia, by nie stracić danych.
POTEM jeśli Style Guide musi się zmienić, napisz nowy uaktualniony Style Guide.
POTEM jeśli Instrukcja Ekstrakcji Streszczenia musi się zmienić, napisz nową uaktualnioną Instrukcję Ekstrakcji Streszczenia.

Streszczenie do załączonego raportu:


<załącz *-INPUT.txt>
<wklej Streszczenie>

##### Prompt 3 - many-shots
###### Podaję instrukcję

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt`. 

Zrób Streszczenie zgodnie z Instrukcją Ekstrakcji Streszczenia w taki sposób, by Streszczenie było zgodne ze Style Guide.

###### Ocena i szukanie różnic

Zrobione przeze mnie Streszczenie wygląda w taki sposób:

```
<wklej Streszczenie>
```

Znajdź różnice - jak ja wyciągnąłem dane ze Streszczenia a jak Ty? Gdzie są różnice?

###### Adaptacja

NAJPIERW powtórz aktualny Style Guide i aktualną Instrukcję Ekstrakcji Streszczenia, by nie stracić danych.
POTEM jeśli Style Guide musi się zmienić, napisz nowy uaktualniony Style Guide.
POTEM jeśli Instrukcja Ekstrakcji Streszczenia musi się zmienić, napisz nową uaktualnioną Instrukcję Ekstrakcji Streszczenia.

## 4. Checkpoints
### 4.1. 2403121158

Ten działał bardzo dobrze.

```md
### Uaktualniony Style Guide dla Streszczeń Sesji RPG

1. **Zwięzłość i Precyzja**: Skupienie na najważniejszych wydarzeniach i decyzjach, streszczenie ma zawierać mniej niż 1000 znaków.
2. **Charakterystyczny Język i Humor**: Użycie kolokwializmów, żargonu specyficznego dla świata przedstawionego, oraz lekkiego humoru.
3. **Ważne Postacie i Wydarzenia**: Podkreślenie kluczowych postaci i ich działań, które miały istotny wpływ na przebieg sesji.
4. **Konflikty i Rozwiązania**: Opis głównych konfliktów lub problemów i sposobów ich rozwiązania.
5. **Tajemnice i Zagadki**: Wzmianki o tajemnicach, niewyjaśnionych zagadkach, lub elementach dodających głębi światu.
6. **Wsparcie i Relacje między Postaciami**: Znaczenie relacji między postaciami dla rozwoju wydarzeń.
7. **Specyficzne dla Świata Elementy**: Wprowadzenie elementów unikalnych dla świata gry, zachowując przy tym spójność świata przedstawionego.
8. **Zakończenie otwarte lub Hook**: Elementy wprowadzające napięcie lub zainteresowanie dalszymi losami bohaterów.
9. **Unikatowe Elementy Narracyjne**: Nietypowe zwroty, żartobliwe nazwy, kreatywność i niebanalne podejście do tworzenia świata gry.

### Aktualna Instrukcja Ekstrakcji Streszczenia z Raportu

1. **Identyfikacja głównych wydarzeń**: Znajdź kluczowe momenty i zwroty akcji, które mają znaczący wpływ na rozwój historii.
2. **Wyodrębnienie głównych postaci i ich roli**: Określ, które postacie odegrały kluczowe role w wydarzeniach i jakie były ich działania.
3. **Skupienie się na konflikcie i jego rozwiązaniu**: Zidentyfikuj główny konflikt sesji oraz sposób, w jaki został rozwiązany lub zaadresowany przez postacie.
4. **Ujęcie konsekwencji wydarzeń**: Opisz, jakie długoterminowe skutki mogą wynikać z rozwiązania konfliktu lub z wprowadzonych zmian.
5. **Uwzględnienie wskazówek na przyszłość**: Wskazówki, tajemnice lub zagadki, które mogą mieć wpływ na przyszłe sesje, należy krótko wspomnieć.
6. **Zachowanie spójności tonu**: Utrzymaj charakterystyczny ton i styl streszczenia, używając humoru, specyficznego języka, i kolokwializmów zgodnie z Style Guide.
7. **Pamiętaj o limicie znaków**: Streszczenie powinno być zwięzłe i skoncentrowane, zawierając mniej niż 1000 znaków, aby zachować precyzję i skupienie na najistotniejszych aspektach sesji.
```

### 4.2. 2403121241

Ten działa WYJĄTKOWO dobrze - do Mavidiz.

```md
### Kompletny Style Guide dla Streszczeń Sesji RPG

#### 1. Zwięzłość i Celność
- **Skupienie na kluczowych problemach i ich rozwiązaniach**, z jasnym wskazaniem przyczyn i skutków. Streszczenie ma być precyzyjne, zawierając mniej niż 1000 znaków.

#### 2. Jasność Wypowiedzi
- **Bezpośrednie wskazanie na antagonistów, konflikty i metody ich rozwiązania**. Wyraźne określenie akcji i ich bezpośrednich rezultatów.

#### 3. Konkretyzacja Działania
- **Wyraźne opisanie działań podjętych przez postacie** i ich bezpośrednich skutków, z naciskiem na rozwiązanie problemu.

#### 4. Ograniczenie Detali
- Zachowanie tylko tych szczegółów dotyczących postaci i wydarzeń, **które bezpośrednio przyczyniają się do rozwoju akcji** i rozwiązania konfliktu.

#### 5. Zachowanie Tonu
- Utrzymanie charakterystycznego tonu narracji, z większym naciskiem na **klarowność i bezpośredniość wypowiedzi**.

#### 6. Struktura i Porządek
- Streszczenie powinno mieć **jasną strukturę**: wprowadzenie problemu, opis działań i rozwiązanie, podsumowanie skutków.

#### 7. Unikalność Stylu
- Zachowanie **unikalnego stylu i osobowości narracji**, odpowiedniego dla świata przedstawionego, z zachowaniem zasad poprzednich punktów.

---
### Kompletna Instrukcja Ekstrakcji Streszczenia z Raportu

#### 1. Identyfikacja Głównego Problemu
- **Znajdź i wyraźnie określ kluczowy problem lub konflikt sesji**. Jest to punkt wyjścia dla całego streszczenia.

#### 2. Wskazanie Antagonisty
- **Jasno zidentyfikuj głównego antagonistę lub źródło konfliktu**. Wskazanie, kim lub czym jest przeszkoda dla postaci.

#### 3. Opisanie Działania Agencji/Grupy
- **Skup się na działaniach podjętych w celu rozwiązania problemu**, wskazując na konkretne metody i ich skutki.

#### 4. Podsumowanie Rozwiązania
- **Jasno opisz, jak problem został rozwiązany**. Zwróć uwagę na działania, które miały kluczowy wpływ na koniec konfliktu.

#### 5. Ograniczenie Informacji Pobocznych
- **Skoncentruj się na najważniejszych informacjach**. Eliminuj detale nieistotne dla głównego wątku konfliktu i jego rozwiązania.

#### 6. Struktura Streszczenia
- Streszczenie powinno być zorganizowane **chronologicznie lub logicznie**, z wyraźnym początkiem (problem), środkiem (działania) i końcem (rozwiązanie).

#### 7. Użycie Cytatów i Dialogów
- **Ogranicz użycie bezpośrednich cytatów i dialogów**, chyba że są one niezbędne do zilustrowania kluczowego momentu.

#### 8. Refleksja i Konsekwencje
- Zakończ streszczenie **wskazówkami na przyszłość lub refleksją** nad skutkami wydarzeń, jeśli jest to istotne dla kontynuacji historii.

Te dokumenty mają służyć jako wytyczne do tworzenia spójnych, zwięzłych i klarownych streszczeń sesji RPG, które są zarówno informacyjne, jak i angażujące dla czytelnika.
```

### 4.3. 2403121426

Post-Mavidiz i post-Caralea (fatalny zapis Caralei...):

```md
### Kompletny Style Guide dla Streszczeń Sesji RPG

#### Celność i Zwięzłość
- Streszczenia powinny być skoncentrowane, zawierając esencję sesji w mniej niż 1000 znaków, z jasnym wskazaniem na główne wydarzenia, konflikty i ich rozwiązania.

#### Jasność Wypowiedzi
- Bezpośrednio i wyraźnie wskazuj na kluczowe postacie, antagonistów, konflikty oraz metody ich rozwiązania. Opisuj akcje i ich rezultaty bez zbędnych szczegółów.

#### Konkretyzacja Działania
- Dokładnie opisz działania podjęte przez postacie, podkreślając, jak przyczyniły się one do rozwiązania problemu. Wyeksponuj dynamiczne i dramatyczne momenty.

#### Ograniczenie Detali
- Zachowaj tylko te detale, które są niezbędne do zrozumienia akcji i rozwoju fabuły. Pomijaj informacje poboczne, które nie wpływają bezpośrednio na główną narrację.

#### Zachowanie Tonu
- Utrzymuj charakterystyczny ton i styl narracji, odpowiedni dla świata przedstawionego, z naciskiem na klarowność i przystępność przekazu.

#### Struktura i Porządek
- Upewnij się, że streszczenie ma jasną i logiczną strukturę: wprowadzenie problemu, opis działań i rozwiązanie, podsumowanie skutków.

#### Unikalność Stylu
- Utrzymuj unikalny styl i osobowość narracji, uwzględniając specyfikę świata gry i głębię postaci.

### Kompletna Instrukcja Ekstrakcji Streszczenia z Raportu

#### Identyfikacja Głównego Problemu
- Rozpocznij od określenia centralnego problemu lub konfliktu, który stanowi oś narracyjną sesji.

#### Wskazanie Antagonisty i Protagonisty
- Jasno zidentyfikuj głównego antagonistę oraz postacie główne (protagonistów), ich role i motywacje.

#### Opisanie Kluczowych Działań
- Skoncentruj się na działaniach podjętych przez postacie w celu rozwiązania konfliktu, zwracając uwagę na innowacyjność i determinację.

#### Podsumowanie Rozwiązania i Konsekwencji
- Jasno przedstaw, jak rozwiązano problem i jakie są bezpośrednie oraz długoterminowe skutki tych działań dla postaci i świata gry.

#### Ograniczenie Informacji Pobocznych
- Skup się na najistotniejszych informacjach, eliminując zbędne szczegóły, które nie przyczyniają się do zrozumienia głównej fabuły.

#### Zintegrowanie Informacji w Spójną Narrację
- Uporządkuj zebrane informacje w logiczną i spójną narrację, przedstawiającą historię od problemu, przez działania, do rozwiązania i jego konsekwencji.

#### Użycie Cytatów i Dialogów
- Ogranicz użycie bezpośrednich cytatów i dialogów do ilustracji kluczowych momentów, które podkreślają dramatyczność sytuacji lub ważne decyzje.

#### Refleksja i Konsekwencje
- Zakończ streszczenie refleksją nad skutkami wydarzeń, uwzględniając zmiany w świecie gry i postaciach, które mogą wpłynąć na przyszłe sesje.

Te wytyczne mają na celu zapewnienie, że streszczenia sesji RPG będą spójne, zwięzłe, i pełne wglądu, oddając ducha przygody i wyzwania, przed którymi stanęły postacie, przy jednoczesnym zachowaniu unikalnego stylu i tonu świata przedstawionego.
```

