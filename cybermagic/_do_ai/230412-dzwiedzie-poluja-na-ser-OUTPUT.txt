
## Streszczenie

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

## Progresja

* Elena Verlen: okazuje się wyjątkowo podatna na Szeptomandrę i ataki tego typu

## Zasługi

* Arianna Verlen: dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami.
* Viorika Verlen: wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie.
* Marcinozaur Verlen: ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ.
* Ula Blakenbauer: ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii.
* Apollo Verlen: jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.
* Elena Verlen: (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.
