Załączam Ci INPUT i OUTPUT. OUTPUT jest wygenerowany z INPUT na podstawie Instrukcji poniżej.

```
## Instrukcja do Tworzenia OUTPUT z INPUT dla Sesji RPG

### 1. **Streszczenie**:
- Twoje streszczenie powinno być bezpośrednie, skoncentrowane na faktach i kluczowych wydarzeniach sesji bez dodawania niepotrzebnych ozdobników. Skup się na przedstawieniu wydarzeń w sposób zwięzły, bezpośrednio wpływających na przebieg sesji.

### 2. **Zasługi Postaci**:
- Opisz zasługi postaci w sposób szczegółowy, wskazując konkretne działania i ich bezpośredni wpływ na fabułę. Zasługi powinny odzwierciedlać indywidualne wkłady postaci, ich decyzje i konsekwencje tych decyzji dla dalszego rozwoju historii.

### 3. **Frakcje**:
- Uwzględnij frakcje działające w sesji, opisując ich cele, motywacje i wpływ na wydarzenia. Zwróć uwagę na ich relacje z głównymi postaciami oraz wpływ na konflikty i rozwiązania w narracji.

### 4. **Lokalizacje**:
- Dokładnie przedstaw lokalizacje, podkreślając ich znaczenie dla fabuły i wpływ na postaci oraz wydarzenia. Opisy lokalizacji powinny pomagać w zrozumieniu kontekstu i atmosfery sesji.

### Praktyczne Wskazówki:
- **Styl i Język**: Zachowaj prostotę i bezpośredniość w prezentacji informacji, unikając zbędnych ozdobników. Skup się na tym, co istotne dla zrozumienia fabuły i postaci.
- **Szczegółowość i Konkretność**: Podkreślaj konkretne działania postaci i ich znaczenie dla historii. Uwzględnij specyficzne momenty, które definiują sesję i jej wyjątkowość.
- **Struktura i Czytelność**: Zapewnij, aby OUTPUT był logicznie zbudowany i łatwy do śledzenia, z wyraźnym podziałem na sekcje: Streszczenie, Zasługi, Frakcje, Lokalizacje.
```

Czy potrafisz na podstawie tej Instrukcji zbudować ten OUTPUT z tego INPUTU?



===

## Wskazówki do Tworzenia OUTPUT z INPUT dla Sesji RPG Sci-Fi + Magia

### Ogólny Proces Analizy:
1. **Dokładnie przeczytaj INPUT**, aby zrozumieć fabułę, dynamikę postaci, kluczowe wydarzenia, oraz unikalne aspekty świata gry. Zwróć uwagę na emocje, motywacje postaci oraz sposób, w jaki wpływają one na rozwój wydarzeń.

2. **Identyfikuj i analizuj kluczowe elementy**, takie jak wydarzenia, dialogi, konflikty, a także działania i interakcje postaci. Szczególną uwagę zwróć na momenty przełomowe dla postaci i fabuły.

3. **Zapisz sobie** najważniejsze informacje, które pokazują działania głównych postaci i stanowią oś fabularną sesji RPG, uwzględniając ich emocjonalny i psychologiczny rozwój.

### Analiza i Synteza Informacji:
4. **Streszczenie**:
   - Stwórz szczegółowe streszczenie, podkreślając przebieg wydarzeń w logicznej kolejności. Skoncentruj się na kluczowych punktach fabuły i jak te wydarzenia wpłynęły na świat gry oraz postacie.
   - Zwróć uwagę na dynamikę między postaciami i ich rozwój w trakcie sesji, zachowując zwięzłość i klarowność.

5. **Zasługi**:
   - Szczegółowo opisz zasługi każdej postaci, wskazując konkretne działania i ich wpływ na sesję. Uwzględnij emocjonalne i psychologiczne aspekty decyzji postaci, a także konsekwencje tych decyzji.
   - Wyróżnij momenty, w których postacie musiały dokonać trudnych wyborów, pokazując, jak te wybory wpłynęły na fabułę i inne postacie.

6. **Progresja**:
   - Opisz, jak postaci rozwinęły się w trakcie sesji, uwzględniając zarówno ich wewnętrzny rozwój, jak i zmiany w ich umiejętnościach czy statusie. Podkreśl trwałe zmiany, które będą miały wpływ na dalszą grę.

7. **Frakcje i Lokalizacje**:
   - Dokładnie opisz frakcje i lokalizacje, uwzględniając ich znaczenie dla fabuły i atmosferę sesji. Opis powinien odzwierciedlać skomplikowaną strukturę świata gry i wpływ frakcji na postacie oraz wydarzenia.

### Prezentacja Wyników:
8. **Uporządkuj OUTPUT**, utrzymując jasny i czytelny podział na sekcje: Streszczenie, Zasługi, Progresja, Frakcje, Lokalizacje. Zadbaj o spójny styl prezentacji, który będzie przystępny i atrakcyjny dla czytelnika.

- **Styl i Język**: Zachowaj prostotę i bezpośredniość w prezentacji informacji, unikając zbędnych ozdobników. Skup się na tym, co istotne dla zrozumienia fabuły i postaci.

- **Szczegółowość i Konkretność**: Podkreślaj konkretne działania postaci i ich znaczenie dla historii. Uwzględnij specyficzne momenty, które definiują sesję i jej wyjątkowość.

- **Struktura i Czytelność**: Zapewnij, aby OUTPUT był logicznie zbudowany i łatwy do śledzenia, z wyraźnym podziałem na sekcje.





===

## Kompleksowe Wskazówki do Tworzenia OUTPUT z INPUT dla Sesji RPG Sci-Fi + Magia

### Ogólny Proces Analizy:
1. **Rozpocznij od dokładnej lektury INPUTu**, aby zrozumieć fabułę, kluczowe wydarzenia, dynamikę postaci i unikalne aspekty świata przedstawionego. Skup się na metadanych, które dostarczają kontekstu i kierunku dla sesji.

2. **Wyodrębnij kluczowe elementy fabuły**, w tym ważne wydarzenia, przełomowe momenty i konflikty. Zwróć szczególną uwagę na dialogi oraz sceny konfliktów, które są kluczowe dla zrozumienia dynamiki postaci i ich wpływu na rozwój wydarzeń.

### Analiza i Synteza Informacji:
3. **Streszczenie**: Podsumuj fabułę, podkreślając główne punkty i unikalne aspekty świata gry. Utrzymaj narracyjny, ale zwięzły styl, aby oddać atmosferę sesji.
4. **Zasługi Postaci**: Opisz szczegółowo działania i wybory postaci, ich rozwój i wpływ na fabułę. Wskazuj konkretne interakcje i ich konsekwencje.
5. **Frakcje**: Precyzyjnie przedstaw frakcje, ich cele, motywacje i wpływ na historię. Podkreślaj interakcje i konflikty między nimi.
6. **Lokalizacje**: Dokładnie opisuj miejsca akcji, ich znaczenie i wpływ na wydarzenia sesji.

### Specyficzne Aspekty i Szczegóły:
7. **Dialogi i Konflikty**: Analizuj dialogi i konflikty, zwracając uwagę na ich znaczenie dla fabuły i rozwój postaci.
8. **Energia i Motywy**: Uwzględniaj unikalne aspekty świata gry, takie jak magia czy technologia, i ich wpływ na sesję.
9. **Dynamika Postaci**: Skupiaj się na dynamice między postaciami, ich relacjach i zmianach w ich interakcjach.

### Prezentacja Wyników:
10. **Struktura OUTPUTu**: Utrzymaj jasny podział na sekcje: Streszczenie, Zasługi Postaci, Frakcje, Lokalizacje.
11. **Styl i Spójność**: Zadbaj o spójny styl prezentacji, który będzie klarowny i atrakcyjny dla czytelnika.

### Finalizacja:
12. **Weryfikacja i Dostosowanie**: Sprawdź, czy OUTPUT jest zgodny z INPUT i czy oddaje esencję sesji RPG. Bądź gotów na ewentualne dostosowania na podstawie otrzymanych danych.

Stosując te zintegrowane wskazówki, będziesz w stanie stworzyć OUTPUT, który nie tylko wiernie oddaje zawartość INPUTu, ale również podkreśla kluczowe aspekty sesji RPG, ułatwiając zrozumienie i docenienie jej przez czytelnika.



===

Given a detailed account of an RPG session, summarize the session to extract essential information including key events, character actions, outcomes of conflicts, notable dialogues, and character developments. Organize the summary into sections: Summary, Progression, Merits. Use specific vocabulary and structure as seen in previous summaries to maintain consistency.

[Insert RPG session details here]

1. **Summary**: Provide an overview of the session's main events, including any significant changes in settings or plots. Mention any critical discoveries or alterations in the storyline.

2. **Progression**: Detail how the session progressed, including any conflicts (e.g., Tr +3, Ex +2) and their outcomes (e.g., V: success, X: failure). Note any strategic decisions made by characters and their impacts.

3. **Merits**: Highlight individual characters' contributions, achievements, or developments. Include any specific actions taken by characters that significantly influenced the session's outcome or the storyline.

Ensure to incorporate the specific terms and structures used in the provided RPG session summaries for consistency and coherence.



===

## Opis danych
### Ogólny opis danych

Podaję Ci zapisaną sesję RPG, sci-fi + magia. Twoim celem jest streszczenie tej sesji i wyciągnięcie kluczowych danych: Streszczenia i Zasług postaci.

* Plik nazwany 'xxx-INPUT.txt' zawiera dokument, z którego należy wyciągnąć owe dane.
* Plik nazwany 'xxx-OUTPUT.txt' zawiera dokument, który ma wyciągnięte kluczowe dane. To przykład wyniku.

### Szczególne przypadki zapisu danych - Dialog

Poniższy zapis oznacza dialog:

```
* Ernest: Śniadanko?
* Marysia: Tak. Wszystko w porządku?
* Ernest: Tak...
* Marysia: Keira nie wyglądała na zadowoloną bo mnie widzi
* Ernest: Keira nie jest niezadowolona, ale uważa że powinnaś coś zrobić a nie robisz. Nie, nie to (uśmiechnął) - to wieczorem. Lub za 10 min, przed śniadankiem? (uśmiech)
```

### Szczególne przypadki zapisu danych - Test / Konflikt

Poniższy zapis oznacza konflikt:

```
Tr +3:

* V: coś się stało
* X: coś innego się stało
```

Konflikt może być Trudny (Tr), Typowy (Tp), Ekstremalny (Ex) lub Heroiczny (Hr). Po konflikcie mamy następującą notację:

* V: sukces, graczom udało się osiągnąć sukces w wyniku czego osiągnęli to o co im chodziło
* X: porażka, pojawiła się komplikacja lub przeciwnicy osiągnęli sukces
* O: działanie trzeciej strony, coś dziwnego się wydarzyło

W ramach konfliktu może też pojawić się dialog, wygląda to wtedy tak:

```
Ex +2 +3Ob:

* V:
    * Ania: coś ciekawego
    * Bartek: coś innego ciekawego
```

Zwykle rzeczy znajdujące się w konflikcie są ważne i bardzo wpływają na fabułę i wydarzenia.

## Polecenie

Czy potrafisz stworzyć rady w jaki sposób stworzyć OUTPUT z INPUT w formie wykorzystywalnej dla Ciebie?