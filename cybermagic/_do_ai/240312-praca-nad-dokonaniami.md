# Praca nad ekstrakcją Streszczenia
## Spis treści

1. Wykorzystanie - aktualna metodyka
2. Idea
3. Konstrukcja
4. Checkpoints (tu znajdziecie Style Guide / Instructions)

## 1. Idea

Ekstrakcja TYLKO Dokonań. 

* Hipoteza 1: 
    * Metodologia
        * Wpierw opis danych
        * Potem pokazanie stylu (hodowla z 10 streszczeń)
        * Potem few-shots z 4-5 raportów
        * Potem one-shot dla istniejącego raportu celem kalibracji
        * Potem aplikacja do realnych sytuacji

## 2. Wykorzystanie
### Prompt 1 - seed danych

Zbudujemy razem sposób budowania dobrych Zasług sesji RPG z napisanych Raportów. Twoim zadaniem będzie konstruowanie Zasług i znalezienie co zrobiły na sesji różne postacie zgodnie ze stylem Dokonań, które Ci zaraz zamieszczę. 

Poniżej masz istniejące Zasługi powiązane z sesjami RPG. Zwróć uwagę na kształt Zasług - bardzo zwięzły styl, skupienie na to, co dana osoba zrobiła i co się stało dzięki tej osobie. Gdyby tej osoby nie było na sesji, to poniżej by się nie stało. Czy potrafisz zrozumieć jak buduje się Zasługi? Stwórz dla siebie instrukcję jak Dokonanie ma wyglądać i jaki ma kształt.

Format Zasługi:

* [Imię Nazwisko]: [co zrobiła]

Przykładowe Zasługi:

## Zasługi

* Arianna Verlen: dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami.
* Viorika Verlen: wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie.
* Marcinozaur Verlen: ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ.
* Ula Blakenbauer: ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii.
* Apollo Verlen: jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.
* Elena Verlen: (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.
* Klasa Oficer Naukowy: Walter; odnalazł starcie Alucis-Esuriit, zbadał śpiących i stworzył antidotum by móc ludzi wyciągnąć ze Snu. Znajdował rzeczy które do siebie nie pasowały i wyciągał z nich dalsze wnioski.
* Klasa Dyplomata: Ola; wydobyła od Feliny że ta wie gdzie jest Rovis, ułagodziła napięcia na stacji i doprowadziła do miękkiej ewakuacji Rovisa i Joli ze stacji za recykler. Skutecznie zarządziła naprawą sytuacji.
* Klasa Hacker: Mery; zebrała informacje o przyczynie problemów (inwestorzy), wbiła się w dane Kalisty odkrywając jej hipotezy i potrafiła prześledzić ruchy Rovisa jak i nagrania z walki gdy savaranie odbijali Jolę-09.
* Rovis Skarun: przez swoją decyzję o porwaniu Joli-09 wplątał się z nią w relację romantyczną. Jego konflikt z Dorionem ją ujawnił, i gdy uciekał z ukochaną doszło do manifestacji Alucis. Opuścił stację przez Agencję.
* Jola-09 Szernief: savarańska inżynier solarna i artystka, porwana przez Rovisa się w nim zakochała. Gdy Rovis ją osłaniał przed swoimi eks-przyjaciółmi, jej krew posłużyła do ko-manifestacji Alucis. Opuściła stację za sprzęt do recyklingu.
* Klaudiusz Widar: Inwestor; zwolennik pójścia po 'industry', zmienił zasady Con Szernief na bardziej competitive (co zwiększyło zyski) i z NavirMed produkował stymulanty dla drakolitów (większe zyski).
* Damian Orczakin: Inwestor pro-turyzm; chce się pozbyć savaran (i kultystów) ze stacji. On wprowadził paranoizator do stymulantów z nadzieją, że zamieszki. Poszło za ostro, więc potem próbował to neutralizować.
* Felina Amatanir: wie gdzie schował się Rovis i próbuje opanować i utrzymać CON Szernief do kupy, ale ma problemy przez energie magiczne. Współpracuje z Agencją. Mimo problemów, dalej ma zaufanie populacji stacji.
* Kalista Surilik: dziennikarka skupiona na prawdzie i uczciwości, podejrzewa Inwestorów o dodanie czegoś do wody i skutecznie ujawniła niektóre ruchy Agencji. Świetnie zbiera informacje, ma je też spoza stacji.
* Mawir Hong: jeden z bardziej agresywnych drakolitów, pracujący w Energii, niepisany 'szef gangu'. Najmocniej dotknięty Esuriit.
* Dorion Fughar: przyjaciel i rywal Rovisa, który próbuje stać się prawą ręką Honga. Współuczestniczył w ataku na Jolę-09. Pierwszy stał za próbą rozwalenia Rovisa za 'zdradę rasy'. Skończył Uśpiony przez Alucis. Odratowany przez Agencję.
* Szymon Alifajrin: 'praktykant' Agencji, umieszczony w NavirMed; udostępnił dane o stymulantach (trzy strainy). Bardzo nie chce tu być.

Napisz "rozumiem".

### Prompt 2 - opis danych

W następnym prompcie otrzymasz ode mnie Raport w formie pliku tekstowego `xxx.txt`. Poniżej wycinek przykładowego raportu:

```
Jak tylko udało się rozmontować barierę, Xavier i Ewelina przeszli przez śluzę. Dokładnie po drugiej stronie znajduje się straszliwy mackowy potwór, który czekał w zasadzce. Złapał Ewelinę by ją rozszarpać na kawałki. Ewelina próbuje się bronić używając swojej broni - zarówno karabinu laserowego jak i shotguna.

Tr +3: (oderwanie się)

* X: broń mało skuteczna, potwór bardzo silny; scutier Eweliny zostaje uszkodzony
* Vr: rzuciłaś się do tyłu - macki odpadają
* V: SUKCES. Zgodnie z rozkazem, Xavier zatrzasnął śluzę overridem. Udało się - macki są po tej stronie, potwór po tamtej, Ewelina jest wolna. Udało się Wam przynajmniej tymczasowo zatrzymać stwora - wszystkie macki są zniszczone.

Po drugiej stronie śluzy jest więcej pyłku niż po tej. (część z macek ruszała się KORZYSTNIEJ niż powinny z perspektywy Eweliny).

* Mara: Nie wiem co zrobiliście, ale w mojej okolicy jest cicho
* Ewelina: Gdzie się ukrywasz?
* Mara: W medycznym, zabarykadowana. Próbuję nie wychodzić.
```

Wyjaśnię, jak to czytać. Poniżej przykład dialogu:

```
* [Jedna osoba]: tekst tekst
* [Druga osoba]: tekst tekst
```

Poniżej przykład testu (testy występują w wariantach: Tp, Tr, Ex, Hr - Typowy, Trudny, Ekstremalny, Heroiczny). Testy są bardzo istotne i robią silną zmianę rzeczywistości.

```
[Ktoś] próbuje [coś osiągnąć]

Tr +3:

* X: [konsekwencje porażki]
* Vr: [konsekwencje sukcesu]
```

Jeśli chodzi o sam Raport, warto zaznaczyć istotne sekcje:

```
### Projekt Wizji Sesji   <-- ta sekcja pokazuje ideę sesji RPG i wysokopoziomowo do czego MG dąży. To wizja, a nie dokładnie co się tam stało. Pokazuje 'szkielet opowieści'.
### Co się stało i co wiemy (tu są fakty i prawda, z przeszłości)   <-- to są fakty, które się wydarzyły przed sesją. Możesz tego użyć by zrozumieć kontekst sesji.
### Co się stanie jak nikt nic nie zrobi (tu NIE MA faktów i prawdy)   <-- zignoruj tę sekcję, ona nie ma znaczenia
### Scena Zero - impl           <-- kluczowa sekcja, na tym pracujesz przede wszystkim
### Sesja Właściwa - impl       <-- kluczowa sekcja, na tym pracujesz przede wszystkim
```

Rozumiesz instrukcję?


### Prompt 3 - ekstrakcja Zasług


Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt`. Stwórz z niego Zasługi, łącząc wszystkie wytyczne które stworzyłeś i te poniżej.

### Instrukcja Tworzenia Zasług

1. **Analiza działań**: Dokładnie przeanalizuj każde działanie postaci w kontekście raportu, zwracając uwagę na te, które miały bezpośredni wpływ na fabułę, rozwiązanie problemu lub rozwój sytuacji.
   
2. **Szczegóły i kontekst**: Opisuj dokładnie okoliczności i szczegółowe działania postaci, podając kontekst, który pomaga zrozumieć ich znaczenie i wpływ na historię.

3. **Dynamiczne interakcje**: Podkreślaj ważne interakcje między postaciami, które wpływają na przebieg wydarzeń i relacje. Włączaj dialogi lub konkretne wymiany, które ilustrują te interakcje.

4. **Motywacje i emocje**: Włączaj informacje o motywacjach postaci i ich emocjonalnej reakcji na wydarzenia. Jakie były ich przesłanki do podjęcia akcji? Jak się czuły?

5. **Rozwój postaci**: Zwróć uwagę na to, jak wydarzenia wpłynęły na rozwój postaci – zarówno pod względem umiejętności, jak i emocjonalnym. Jakie zmiany zaszły w postaciach?

6. **Techniczne i magiczne aspekty**: Uwzględniaj szczegółowe informacje na temat używanego sprzętu, technologii, magii czy innych kluczowych elementów, które miały wpływ na sukces misji.

7. **Konkretyzacja i personalizacja**: Każde Zasługi powinno być konkretnie związane z postacią, jasno i precyzyjnie formułowane, aby oddawało unikalny wkład i charakter postaci.

8. **Weryfikacja i dostosowanie**: Po stworzeniu Zasługi, sprawdź, czy odzwierciedla ono wszystkie kluczowe aspekty działań postaci i czy jest zgodne z tonem oraz duchem sesji.

Analizując podane przez Ciebie Zasługi w porównaniu do moich poprzednich propozycji, zauważyłem kilka kluczowych różnic:

1. **Osobiste dokonania i wpływ na fabułę**: Twoje Zasługi skupiają się bardziej na indywidualnych dokonaniach postaci i ich bezpośrednim wpływie na rozwój historii oraz na relacje między postaciami. Moje wcześniejsze propozycje były bardziej ogólne i mniej skupiały się na konkretnych wynikach działań postaci.

2. **Szczegółowy kontekst i motywacje**: Twoje Zasługi zawierają więcej szczegółów na temat kontekstu i motywacji postaci, jak np. motywacje polityczne Ślicznospada czy determinacja Tiani w obronie Mścizęba. W moich propozycjach brakowało tej głębi.

3. **Konkretyzacja działań**: W Twoich Zasługach dokładnie wskazujesz na konkretne działania i decyzje, jak np. projektowanie pułapki przez Viorikę czy wykorzystanie lokalnej wiedzy przez Mścizęba. W moich propozycjach brakowało takiego poziomu szczegółowości.

### Przekształcona instrukcja do tworzenia Zasług

Na podstawie Twoich uwag i porównania, oto przekształcona instrukcja do tworzenia Zasług:

1. **Zidentyfikuj indywidualne działania**: Skoncentruj się na konkretnych działaniach, które postać podjęła w trakcie sesji, i jak te działania wpłynęły na rozwój fabuły oraz rozwiązanie problemów.

2. **Włącz motywacje i kontekst**: Do każdej Zasługi dodaj informacje o motywacjach postaci oraz kontekście ich działań, by lepiej zrozumieć ich znaczenie i wpływ na historię.

3. **Podkreśl bezpośredni wpływ na fabułę**: Wskazuj, jak działania postaci miały bezpośredni wpływ na kluczowe momenty sesji, zmiany w relacjach między postaciami lub rozwiązanie centralnych konfliktów.

4. **Zaznacz rozwój i zmiany**: Zwróć uwagę na to, jak dane wydarzenia wpłynęły na rozwój postaci, zarówno pod względem umiejętności, jak i emocjonalnym.

5. **Używaj precyzyjnego i zwięzłego języka**: Formułując Zasługi, staraj się być precyzyjny i zwięzły, jednocześnie zachowując bogactwo detali i głębię analizy.







==================================
==================================

## 3. Prompty i parametry
### 3.1. Hipoteza 1, 2403121557
#### 3.1.1. Pierwsze podejście
##### Prompt 1 - wyjaśnienie

Zbudujemy razem sposób budowania dobrych Zasług sesji RPG z napisanych Raportów. Twoim zadaniem będzie konstruowanie Zasług i znalezienie co zrobiły na sesji różne postacie zgodnie ze stylem Dokonań, które Ci zaraz zamieszczę. 

Czy rozumiesz instrukcję?

##### Prompt 2 - streszczenia do Style Guide

Poniżej masz istniejące Zasługi powiązane z sesjami RPG. Zwróć uwagę na kształt Zasług - bardzo zwięzły styl, skupienie na to, co dana osoba zrobiła i co się stało dzięki tej osobie. Gdyby tej osoby nie było na sesji, to poniżej by się nie stało. Czy potrafisz zrozumieć jak buduje się Zasługi? Stwórz dla siebie instrukcję jak Dokonanie ma wyglądać i jaki ma kształt.

Format Zasługi:

* Imię Nazwisko: co zrobiła

Przykładowe Zasługa:

## Zasługi

* Arianna Verlen: dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami.
* Viorika Verlen: wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie.
* Marcinozaur Verlen: ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ.
* Ula Blakenbauer: ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii.
* Apollo Verlen: jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.
* Elena Verlen: (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.
* Klasa Oficer Naukowy: Walter; odnalazł starcie Alucis-Esuriit, zbadał śpiących i stworzył antidotum by móc ludzi wyciągnąć ze Snu. Znajdował rzeczy które do siebie nie pasowały i wyciągał z nich dalsze wnioski.
* Klasa Dyplomata: Ola; wydobyła od Feliny że ta wie gdzie jest Rovis, ułagodziła napięcia na stacji i doprowadziła do miękkiej ewakuacji Rovisa i Joli ze stacji za recykler. Skutecznie zarządziła naprawą sytuacji.
* Klasa Hacker: Mery; zebrała informacje o przyczynie problemów (inwestorzy), wbiła się w dane Kalisty odkrywając jej hipotezy i potrafiła prześledzić ruchy Rovisa jak i nagrania z walki gdy savaranie odbijali Jolę-09.
* Rovis Skarun: przez swoją decyzję o porwaniu Joli-09 wplątał się z nią w relację romantyczną. Jego konflikt z Dorionem ją ujawnił, i gdy uciekał z ukochaną doszło do manifestacji Alucis. Opuścił stację przez Agencję.
* Jola-09 Szernief: savarańska inżynier solarna i artystka, porwana przez Rovisa się w nim zakochała. Gdy Rovis ją osłaniał przed swoimi eks-przyjaciółmi, jej krew posłużyła do ko-manifestacji Alucis. Opuściła stację za sprzęt do recyklingu.
* Klaudiusz Widar: Inwestor; zwolennik pójścia po 'industry', zmienił zasady Con Szernief na bardziej competitive (co zwiększyło zyski) i z NavirMed produkował stymulanty dla drakolitów (większe zyski).
* Damian Orczakin: Inwestor pro-turyzm; chce się pozbyć savaran (i kultystów) ze stacji. On wprowadził paranoizator do stymulantów z nadzieją, że zamieszki. Poszło za ostro, więc potem próbował to neutralizować.
* Felina Amatanir: wie gdzie schował się Rovis i próbuje opanować i utrzymać CON Szernief do kupy, ale ma problemy przez energie magiczne. Współpracuje z Agencją. Mimo problemów, dalej ma zaufanie populacji stacji.
* Kalista Surilik: dziennikarka skupiona na prawdzie i uczciwości, podejrzewa Inwestorów o dodanie czegoś do wody i skutecznie ujawniła niektóre ruchy Agencji. Świetnie zbiera informacje, ma je też spoza stacji.
* Mawir Hong: jeden z bardziej agresywnych drakolitów, pracujący w Energii, niepisany 'szef gangu'. Najmocniej dotknięty Esuriit.
* Dorion Fughar: przyjaciel i rywal Rovisa, który próbuje stać się prawą ręką Honga. Współuczestniczył w ataku na Jolę-09. Pierwszy stał za próbą rozwalenia Rovisa za 'zdradę rasy'. Skończył Uśpiony przez Alucis. Odratowany przez Agencję.
* Szymon Alifajrin: 'praktykant' Agencji, umieszczony w NavirMed; udostępnił dane o stymulantach (trzy strainy). Bardzo nie chce tu być.


##### Prompt 3 - Raport plus Zasługi.
###### Initial

Przechodzimy do Kroku 2: Raport plus Zasługi. Ten krok powtórzymy wielokrotnie jako few-shot learning.

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt` oraz pasujące do niego Zasługi.

Czy widzisz, w jaki sposób powstały te Zasługi?
Jeśli Instrukcja Tworzenia Dokonań ma się zmienić, zmień ją.

Zasługi do załączonego Raportu:


<załącz *-INPUT.txt>
<wklej Zasługi>

###### Evolving

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt` oraz pasujące do niego Zasługi. 

Jeśli Instrukcja Ekstrakcji musi się zmienić, napisz nową uaktualnioną Instrukcję.

Zasługi do załączonego raportu:


<załącz *-INPUT.txt>
<wklej Zasługi>

##### Prompt 3 - many-shots
###### Podaję raport

Otrzymasz ode mnie Raport w formie pliku tekstowego `xxx-INPUT.txt`. Stwórz z niego Zasługi.  


## 4. Checkpoints
### 4.1. 2403122017

Ten działał bardzo dobrze.

```md
### Instrukcja Tworzenia Zasług

1. **Analiza działań**: Dokładnie przeanalizuj każde działanie postaci w kontekście raportu, zwracając uwagę na te, które miały bezpośredni wpływ na fabułę, rozwiązanie problemu lub rozwój sytuacji.
   
2. **Szczegóły i kontekst**: Opisuj dokładnie okoliczności i szczegółowe działania postaci, podając kontekst, który pomaga zrozumieć ich znaczenie i wpływ na historię.

3. **Dynamiczne interakcje**: Podkreślaj ważne interakcje między postaciami, które wpływają na przebieg wydarzeń i relacje. Włączaj dialogi lub konkretne wymiany, które ilustrują te interakcje.

4. **Motywacje i emocje**: Włączaj informacje o motywacjach postaci i ich emocjonalnej reakcji na wydarzenia. Jakie były ich przesłanki do podjęcia akcji? Jak się czuły?

5. **Rozwój postaci**: Zwróć uwagę na to, jak wydarzenia wpłynęły na rozwój postaci – zarówno pod względem umiejętności, jak i emocjonalnym. Jakie zmiany zaszły w postaciach?

6. **Techniczne i magiczne aspekty**: Uwzględniaj szczegółowe informacje na temat używanego sprzętu, technologii, magii czy innych kluczowych elementów, które miały wpływ na sukces misji.

7. **Konkretyzacja i personalizacja**: Każde Zasługi powinno być konkretnie związane z postacią, jasno i precyzyjnie formułowane, aby oddawało unikalny wkład i charakter postaci.

8. **Weryfikacja i dostosowanie**: Po stworzeniu Zasługi, sprawdź, czy odzwierciedla ono wszystkie kluczowe aspekty działań postaci i czy jest zgodne z tonem oraz duchem sesji.
```

### 4.2. 2403122018

```md
### Przekształcona Nowa Instrukcja Tworzenia Zasług

1. **Dokładna analiza i dokumentacja działań**: Skup się na szczegółowym opisie każdego działania postaci, uwzględniając użyte narzędzia, metody i technologie. Rozpisz krok po kroku, jakie działania zostały podjęte i jakie miały skutki.

2. **Wbudowanie kontekstu historycznego i emocjonalnego**: Włączaj w opisy Zasług nie tylko kontekst bezpośrednich wydarzeń, ale też historię i tło, które wpływają na motywacje postaci oraz ich postrzeganie sytuacji. Wskazuj, jak przeszłość wpływa na teraźniejsze decyzje.

3. **Szczegółowe interakcje i dynamika grupowa**: Rozwiń opisy interakcji między postaciami, wskazując, jak te interakcje wpływają na rozwiązanie problemów. Używaj dialogów i anegdot, aby pokazać charaktery postaci i ich relacje.

4. **Podkreślenie znaczenia emocji i motywacji**: Daj czytelnikom głębszy wgląd w to, co napędza postacie, opisując ich wewnętrzne konflikty, nadzieje, obawy i determinację. Uwydatnij momenty, w których emocje postaci miały kluczowe znaczenie dla ich działań.

5. **Rozwój postaci i ich ewolucja**: Uwydatnij, jak wydarzenia sesji przyczyniają się do rozwoju i zmian w postaciach. Opisz, jak postacie dorastały, uczyły się na swoich błędach lub zmieniały swoje podejście.

6. **Wykorzystanie magii i technologii jako narzędzi**: W każdym opisie Zasługi szczegółowo przedstawiaj, jak postaci używają magii, technologii czy innych unikalnych zasobów, aby osiągnąć swoje cele. Podaj konkretne przykłady wykorzystania tych narzędzi.

7. **Elementy humoru i lekkości**: Wplataj w opisy Zasług momenty lekkości i humoru, które złagodzą napięcie i dodadzą sesji charakteru. Pokaż, jak postaci radzą sobie z trudnościami, również poprzez humor.

8. **Szczegółowa personalizacja i dopasowanie do postaci**: Każde Zasługi powinno nie tylko opisywać działania, ale też odzwierciedlać głębię i osobowość postaci. Wskazuj, jak unikalne cechy postaci wpływają na ich sposoby działania.

9. **Ocena i dostosowanie według tonu narracji**: Po stworzeniu Zasług upewnij się, że pasują one do ogólnego tonu i ducha sesji, zachowując spójność narracyjną i głębię opowieści.

Przekształcona instrukcja ma na celu jeszcze bardziej szczegółowe podejście do tworzenia Zasług, które odzwierciedlają złożoność działań postaci, ich emocjonalne tło oraz dynamikę w grupie, przy jednoczesnym podkreśleniu znaczenia historii, magii i technologii w ich działaniach.
```
