---
layout: mechanika
title: "Notatka mechaniki, 230601 - karta postaci - koncept"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox

## 1. Purpose

1. Znamy przeszłe mechanizmy.
2. Widzimy jakie są interesujące mechanizmy kart postaci.

## 2. High-level overview

.

## 3. Co mamy

W [pierwszym dokumencie](230530-analiza-karty-postaci.md) przeanalizowaliśmy serię poprzednich kart postaci których używaliśmy, rozumowań itp.

W [drugim dokumencie](230531-analiza-karty-postaci-cele.md) wypracowaliśmy przydatne klocki:

1. niekompletna i średnia lista WARTOŚCI dla karty postaci (szybkość użycia akcji, łatwość użycia akcji...)
2. klocki karty postaci które mieliśmy i się jakoś sprawdziły (agenda, obietnica, zasoby...)
3. usecases / akcje jakich oczekujemy od karty postaci (tworzenie postaci, aktualizacja karty, "co moja postać może zrobić TERAZ w tej sytuacji...")
4. blokady i failmodes wypełniania karty postaci

Czas na trzeci dokument, tymczasowy, bo muszę skupić się najpierw na mechanice (czego się nie spodziewałem).

## 4. Główne przemyślenia

Przykład konceptu karty postaci, na przykładzie Martyna.

[Oryginalna karta Martyna, mechanika 2206](../karty-postaci/2206-martyn-hiwasser.md)

Koncept (szkielet, niesformatowany, nie 100% wypełnione):

* **Martyn Hiwasser**
    * Obietnica
        * "Będziesz starszym lekarzem, morderczym weteranem wojennym, który z łagodnością rozładuje konflikt, utrzyma wszystkich przy życiu lub usunie przeciwnika. Nikt nie będzie się Ciebie spodziewał."
        * Biokonstruktor, który był członkiem gangu, medykiem wojskowym, przez moment tienem Eterni a teraz jest głównym lekarzem na Inferni. Własnymi słowami: "Na emeryturze".
    * Rdzeń mechaniczny
        * Mistrzostwo (1-3)
            * GDY wszyscy panikują i jest chaos, TO Martyn zachowuje zimną krew i uspokaja optymizmem
            * GDY ktoś próbuje określić kto jest jak groźny, TO Martyn zostaje uznany za całkowicie niegroźnego
        * Kompetencje (3-5)
            * GDY jest trudniejsza sytuacja społeczna, TO czarem i urokiem osobistym zdeeskaluje sytuację
            * GDY spotyka się z nieznajomym, TO łatwo staje się 'poczciwym starszym lekarzem' z którym łatwo się gada o wszystkim 
            * GDY ludzie są za słabi by sobie poradzić z sytuacją, TO jako lekarz i biokonstruktor podniesie ich możliwości stymulantami
            * GDY jest trudna sytuacja medyczna, TO jest ekspertem od medycyny. Wiele widział i wiele zrobił. Każdego uratuje nawet w dziwny sposób."
            * GDY pojawiają się nietypowe sytuacje, TO jako stary weteran (20 lat służby na froncie) wszystko widział i wie jak z tym poradzić
            * GDY potrzebne są rozwiązania siłowe, TO jako eks-agent militarny sił specjalnych umie szybko i cicho zabić ofiarę nożem i magią
            * GDY jest trudna sytuacja poza statkiem w kosmosie, TO jako advancer próżniowy w kosmosie jest jak ryba w wodzie.
        * Agenda i styl działania (3-5)
            * GDY może pomóc wartościowej osobie w karierze lub życiu, TO NAWET jeśli jej nie lubi - pomoże, dla "lepszego jutra".
            * GDY może komuś pomóc w trudnej sytuacji nie ryzykując sojuszników, TO Martyn pomoże z uśmiechem, nawet ryzykując zdrowiem i karierą
            * GDY uważa, że komuś nie da się pomóc, TO ich zostawi lub zrobi _coup de grace_. Za dużo widział.
            * GDY może kogoś narazić lub zadziałać samemu, TO zrobi to sam nawet jeśli jego ryzyko jest większe.
            * GDY może coś osiągnąć mówiąc źle o innych, TO niczego złego nie powie. Nie mówi źle nawet o noktianach.
        * Słabości (1-5)
            * GDY sytuacja jest zbyt niebezpieczna, TO bierze ją na siebie nikogo nie włączając. Nie chce już tracić żadnych przyjaciół ani młodych z potencjałem.
            * GDY nie ma dostępu do swoich leków i środków stabilizujących jego, TO jego rzeczywistość się mu rozpada w rękach i nie można na nim polegać (agresja, paranoja, "they are here").
            * GDY wchodzi w interakcję z noktianami, TO zawsze zakłada że oni są wszystkiemu winni.
            * NAWET JEŚLI można nawet uratować życie kłamstwem, TO NAWET WTEDY Martyn bezpośrednio nie skłamie
    * Reszta / opis (niewypełnione, nieprzemyślane)
        * Ogólnie
            * O czym myśli
                * "Moja historia się już skończyła - czas na następną generację Orbitera, Astorii i Neikatis"
                * "Jeśli spojrzysz w gwiazdy, zobaczysz uśmiech tych ludzi których życie poprawiłeś"
                * "Samo przetrwanie nie wystarczy. Potrzebna jest też nadzieja i uśmiech."
            * Znaczące Dokonania
                * Zatrzymał konflikt między dwoma frakcjami Orbitera, bo... zaprzyjaźnił się z damami dowodzącymi oboma frakcjami i zmusił je do rozmowy.
                * ...
        * Serce (te rzeczy są w większości na starej karcie, więc nie wypełniam)
            * Core Wound - Core Lie
            * ENCAO, VALS
            * DRIVE jako enumeracja delt(nie ma, pragnie) (jak Obietnica itp.)
        * Możliwości
            * Zasoby
            * Magia
            
            * Umocowanie w świecie (nie, nie wiem co tu ma być)
        * Styl i Sterowanie


DEFINICJE:

* **postać persystentna** : Arianna Verlen, Eustachy Korkoran, Martyn Hiwasser, Klaudia Stryk. Postacie, które są "głównymi postaciami". Postacie graczy / NPC dookoła których powstają całe wątki.
* **tempka** : Celina, potencjalnie Ardilla Korkoran. postać którą obejmujemy jednorazowo / na kilka sesji. Może awansować na persystentną ;-).
* **fiszka** : salvagerzy z Seiren na Neikatis, postacie autogenerowane per sesja.
* **archetyp** : As Pilotażu, Inżynier Nanitek, Daredevil. Coś, co dajemy graczowi na konwencie. To, co robi Apocalypse World. Jest wielu "hardholderów" ale jest JEDEN "Hardholder". Wielu "lekarzy" ale jeden "Anioł".

Więc Martyn jest **postacią persystentną**. Skończone? Nope. Ale widzimy wektor.

1. Postać Persystentna musi mieć wypełniony **Rdzeń Mechaniczny** i tylko Rdzeń Mechaniczny
    * Choć silnie sugerowałbym "Obietnicę", by po przeczytaniu dało się zrozumieć _feel_ postaci
2. **Agenda i styl** sprawiają, że mamy jednoznaczne kierowanie postacią. 
    * Doprecyzowanie mamy w Opisie, ale już Agenda i Styl dają unikalne zachowania
3. **Mistrzostwo** niesie za sobą DODATKOWĄ AUTOMATYCZNĄ PRZEWAGĘ. To jest to, gdzie postać jest najlepsza. Wśród 5 żołnierzy wszyscy mogą mieć te same Kompetencje, ale Mistrzostwa będą mieć inne
    * każda postać ma swoją niszę
    * od razu widać, kiedy użyć danej postaci - kiedy będzie najlepsza
    * każda postać ma "specjalne ruchy" wynikające z Mistrzostwa, bo można grać swoim Mistrzostwem.
        * to plus _Agenda / Styl_ dają nam bardzo unikalne patterny ruchów różnych postaci
3. **Kompetencje** mogą być opisane jako Sytuacje (GDY -> TO), ale nadal pokazują szerzej jaki jest zakres możliwości postaci. Reszta pochodzi z Możliwości w _Reszcie_ - jeśli coś nie jest w Kompetencjach, to może jest tam. Chodzi o pokazanie idei postaci a nie pisanie wszystkiego.
    * Dlatego jest ograniczenie 3-5. Nie chodzi o "wszystko" a o pokazanie najczęstszych ruchów.
3. **Słabości** pokazują gdzie postać ma niedowagę i w co MG może uderzać by zwiększyć nisze innych postaci. Niektóre Słabości zdecydowanie uatrakcyjniają postać z perspektywy prowadzenia.

Żeby nie było, tak widzę Ariannę w tym modelu (-> Fox). Zauważcie, że to pokazuje "atrakcyjność" postaci, nawet w samym czystym Rdzeniu Mechanicznym. Od razu widać jak działa i co z nią robić.

[Oryginalna karta Arianny, mechanika 2004](../karty-postaci/2004-arianna-verlen.md)

* **Arianna Verlen**
    * Rdzeń mechaniczny
        * Mistrzostwo (1-3)
            * GDY jest jakikolwiek konflikt magiczny, TO Arianna będąc arcymagiem jest najpotężniejsza.
            * Dark Awakening: GDY używa magii do animacji / uruchomienia czegoś, TO nadaje mu nie-życie i przekształca lojalność tego bytu do siebie. Technicznie: "jak nekromantka ale wobec wszystkiego"
            * GDY jest jakikolwiek show lub sytuacja grupowa, TO Arianna ściąga światło reflektorów (cekinami ;-) )
        * Kompetencje (3-5)
            * GDY jest sytuacja w walce, TO specjalizuje się w walce krótkiego zasięgu i oszukaniu drugiej strony (deception). Świetna w Lancerze.
            * GDY trzeba pilotować ścigacz czy fregatę, TO bez problemu sobie poradzi - mimo, opinii na planecie że jest kiepskim pilotem
            * GDY dowodzi różnorodnymi osobami, TO wykorzysta ich umiejętności w optymalny sposób
            * GDY morale jest niskie lub sytuacja trudna, TO zrobi show i walnie mówkę - inspiruje "sztandarem"
            * GDY jest trudna sytuacja społeczna, TO wykrywa ją (empatia i odczytywanie emocji, ukrytych intencji), zmanipuluje i przekona
            * GDY potrzebny jest odpowiedni front czy zachowanie, TO wybitna aktorka przyjmie rolę jakiej potrzebuje.
        * Agenda i styl działania (3-5)
            * GDY pojawia się anty-noktianizm, anty-Aurumizm itp, TO integruje wszystkie grupy. Nie akceptuje -izmów.
            * GDY pojawia się okazja do pokazania się, TO próbuje zostać symbolem i wykorzystać media do integracji wszystkich pod swoim sztandarem.
            * GDY jej przyjaciele są narażeni, TO poświęci sporo bezimiennych sojuszników i zasobów by ich ratować
            * GDY sojusznicy jej zagrażają, świadomie lub nie, TO ich sabotuje lub wprowadzi ich na minę.
            * GDY już kogoś polubi i na kimś jej zależy, TO pomoże tej osobie nawet kosztem wsadzenia ją w Anomalię (Krypta). Nawet wbrew woli tej osoby i w sekrecie. "Ja wiem lepiej jak Ci pomóc".
            * GDY spotyka się z sadyzmem czy bezsensownym cierpieniem, TO zniszczy to z przyjemnością i determinacją.
        * Słabości (1-5)
            * GDY sytuacja robi się zbyt skomplikowana i wychodzi za daleko od jej planów, TO przestaje dowodzić, zaczyna działać osobiście i oddaje swoim kompetentnym oficerom niezależne działania. Nie daje _Commander's Intent_.
            * GDY uważa, że sytuacja jest pod kontrolą, TO często wchodzi zbyt agresywnie, nie wycofuje się odpowiednio wcześnie. Czasem wchodzi w pułapki wierząc, że je pokona.
            * GDY ryzykuje reputacją lub byciem "tą złą", TO deleguje sytuację ją na sojusznika lub unika działania.
            * GDY ma kompetentnych sojuszników, TO przeszacowuje ich umiejętności - nawet, jeśli się zarżną wykonując zadanie.
            
Z perspektywy tylko tego co powyżej widzę jak prowadzić Ariannę i jak robić jej skonfliktowane sceny, sytuacje itp. Kartę wypełniłem na podstawie poprzednich sesji i sesji-substratu (<3 RdButler).

_**Nie ma jeszcze zmian w karcie postaci, może da się to uprościć / czegoś brakuje. Do konsultacji z Zespołem.**_

## 5. Inne wartościowe obserwacje

Dzięki Darkenowi mam ważną modyfikację fiszki. Weźmy dowolne dwie postacie z Seiren (Fox: to ten horror z jaskiniami w Neikatis w burzy piaskowej z powracającym echem z komunikatora):

**Aktualne fiszki:**

* Michał Uszwon
    * (ENCAO: 00-0+ |Zakłóca spokój; nie toleruje spokoju i nudy| VALS: Face >> Achievement| DRIVE: Poznać sekret Rufusa - skąd ma Seiren?)
    * salvager Seiren (hired muscle / engineer) faeril: robota czeka.
    * "Jak masz czas gadać to masz czas robić" "Zamknij mordę."
    * styl: twardy, nie okazuje słabości, gardzi mamlasami
* Cyprian Kugrak
    * ENCAO:  0-0++ |Unika nieprzyjemnej roboty;;Lubi pochwały| VALS: Hedonism, Family| DRIVE: Bezpieczeństwo swego dziecka
    * salvager Serien (hired muscle / combat) faeril: tylko ludzkie formy
    * "Nienawidzę tych wszystkich anomalicznych gówien..."
    * styl: pomocny, dobrą rękę poda

Jak widać, oboje są salvagerami, oboje są niby "hired muscle / X" a inżynierstwo Michała nie było silnie zaakcentowane bo nie musiało być na sesji. Dało się te postacie odróżnić na sesji, ale przez styl (i umiejętności MG, mówiąc z charakterystyczną dla mnie skromnością) a nie tylko dzięki fiszkom.

A gdybyśmy mocniej zaakcentowali te różnice?

**Docelowe fiszki:**

* Michał Uszwon
    * (ENCAO: 00-0+ |Zakłóca spokój; nie toleruje spokoju i nudy| VALS: Face >> Achievement| DRIVE: Poznać sekret Rufusa - skąd ma Seiren?)
    * salvager Seiren (hired muscle / engineer) faeril: robota czeka.
    * "Jak masz czas gadać to masz czas robić" "Zamknij mordę."
    * styl: twardy, nie okazuje słabości, gardzi mamlasami
    * **RUCH AGENDY**: GDY Seiren nie ma nikogo na pokładzie, TO próbuje poznać sekrety Seiren i Rufusa
    * **RUCH UNIKALNY**: GDY ktokolwiek pokazuje choć cień słabości, TO opieprza i próbuje maksymalizować robotę wedle zasady "narzekasz to masz nadmiar czasu i energii".
* Cyprian Kugrak
    * ENCAO:  0-0++ |Unika nieprzyjemnej roboty;;Lubi pochwały| VALS: Hedonism, Family| DRIVE: Bezpieczeństwo swego dziecka
    * salvager Serien (hired muscle / combat) faeril: tylko ludzkie formy
    * "Nienawidzę tych wszystkich anomalicznych gówien..."
    * styl: pomocny, dobrą rękę poda
    * **RUCH AGENDY**: GDY pojawiają się sprawy z anomaliami, TO próbuje zrzucić robotę na innych > unikać > rozwalić
    * **RUCH UNIKALNY**: GDY ma na to dowolną okazję, TO próbuje pokazać się z jak najlepszej strony, popisać się i zdobyć pochwały.

Zauważcie co się dzieje - jeśli nie mam pomysłu jak skonsumować (X) to mogę go przesunąć na niewłaściwe / niepożądane użycie Ruchu jakiejś z postaci. Jeśli jest okazja, mogę tymi ruchami pokazać unikalność postaci. _Agenda / styl_ pokaże _kim jest i czego chce_ a _Unikalny_ pokaże _czym osiąga sukces lub na czym mu zależy_.

Co z tym dalej - jeszcze nie wiem, ale na 100% będę eksplorował. To wyjdzie eksperymentalnie, jak 999 rzeczy.

## 6. Bardzo ciekawa obserwacja odnośnie teatrów / wejść.

Spróbuję przerobić Martyna jeszcze raz. Teraz robimy coś takiego: po LEWEJ stronie stawiamy Teatr a nie konkretną sytuację. Czyli np. "walka", "społeczna", "medyczna" itp. To nam troszkę redukuje unikalność, ale da nam porównanie sytuacji dla dwóch różnych postaci oraz potencjalnie dużo łatwiej tego użyć.

(dzięki, Fox)

* **Martyn Hiwasser**
    * Obietnica
        * "Będziesz starszym lekarzem, morderczym weteranem wojennym, który z łagodnością rozładuje konflikt, utrzyma wszystkich przy życiu lub usunie przeciwnika. Nikt nie będzie się Ciebie spodziewał."
        * Biokonstruktor, który był członkiem gangu, medykiem wojskowym, przez moment tienem Eterni a teraz jest głównym lekarzem na Inferni. Własnymi słowami: "Na emeryturze".
    * Rdzeń mechaniczny
        * Mistrzostwo (1-3)
            * GDY wszyscy panikują i jest duża presja, TO Martyn zachowuje zimną krew i uspokaja optymizmem
            * GDY jest sytuacja **bojowa** lub **społeczna**, TO Martyn zostaje uznany za całkowicie niegroźnego
        * Kompetencje (3-5)
            * GDY jest sytuacja **społeczna** i nerwy, TO czarem i urokiem osobistym zdeeskaluje sytuację
            * GDY jest sytuacja **społeczna**, TO łatwo staje się 'poczciwym starszym lekarzem' z którym łatwo się gada o wszystkim 
            * GDY musi **wzmocnić sojuszników**, TO jako lekarz i biokonstruktor podniesie ich możliwości stymulantami
            * GDY jest sytuacja **medyczna**, TO jest ekspertem od medycyny. Wiele widział i wiele zrobił. Każdego uratuje nawet w dziwny sposób."
            * GDY jest sytuacja **naukowo-analityczna**, TO jako stary weteran (20 lat służby na froncie) wszystko widział i wie jak z tym poradzić
            * GDY jest sytuacja **bojowa**, TO jako eks-agent militarny sił specjalnych umie szybko i cicho zabić ofiarę nożem i magią
            * GDY jest sytuacja **w kosmosie**, TO jako advancer próżniowy w kosmosie jest jak ryba w wodzie.
        * Agenda i styl działania (3-5)
            * GDY może pomóc wartościowej osobie w karierze lub życiu, TO NAWET jeśli jej nie lubi - pomoże, dla "lepszego jutra".
            * GDY może komuś pomóc w trudnej sytuacji nie ryzykując sojuszników, TO Martyn pomoże z uśmiechem, nawet ryzykując zdrowiem i karierą
            * GDY uważa, że komuś nie da się pomóc, TO ich zostawi lub zrobi _coup de grace_. Za dużo widział.
            * GDY może kogoś narazić lub zadziałać samemu, TO zrobi to sam nawet jeśli jego ryzyko jest większe.
            * GDY może coś osiągnąć mówiąc źle o innych, TO niczego złego nie powie. Nie mówi źle nawet o noktianach.
        * Słabości (1-5)
            * GDY sytuacja **jest ryzykowna**, TO bierze ją na siebie nikogo nie włączając. Nie chce już tracić żadnych przyjaciół ani młodych z potencjałem.
            * GDY nie ma dostępu do swoich leków i środków stabilizujących jego, TO jego rzeczywistość się mu rozpada w rękach i nie można na nim polegać (agresja, paranoja, "they are here").
            * GDY wchodzi w interakcję z noktianami, TO zawsze zakłada że oni są wszystkiemu winni.
            * NAWET JEŚLI można nawet uratować życie kłamstwem, TO NAWET WTEDY Martyn bezpośrednio nie skłamie

To co tu ciekawe:

Na razie mamy niedoprecyzowane teatry / zakresy, ale patrząc na Martyna na samym dole i mając USTALONĄ listę teatrów łatwiej będzie złożyć postać. A tam gdzie np. "sytuacja z silną presją" (słabość Martyna) to jest aspekt sytuacji - tymczasowy lub trwały. Więc można go wygenerować krzyżykiem.

## 8. Problemy jakie zostały

* **Problem trudnego wypełnienia karty postaci / tworzenia postaci**
    * Formuła jest szalenie trudna i jeśli nie widzisz jak zadziałałeś postacią to nie wiesz co jej wpisać patrząc na sucho na kartę.
        * Jak siadam do postaci mam w głowie "to chcę zagrać inżynierem z dronami"
        * wymyślenie do tego kompetencji jest dość łatwe i wyjdzie, ale wymyślenie agendy i słabości od zera - auć.
            * rpgi uczą dumpować staty, a nie wymyślać słabości
    * --> jak mamy 5 sesji daną postacią, to jest to prostsze (potwierdzam)
        * --> v1: predefiniowane karty / archetypy (jak AW)
        * --> v2: generator szkieletu postaci, mix & match
        * --> v3: wyjdź od przeszłości postaci (generator lub enumerator)

## 7. Co dalej?

1. Najbliższy krok
    1. **Ustabilizować mechanikę CR która wymusza rzeczy na kartach postaci** (dzięki, Dzióbku T_T )
2. Najbliższy krok W kontekście kart postaci
    1. Uzupełnić wymagania niefunkcjonalne per akcja
    2. Uzupełnić Blokady i Failmodes
    3. Uzupełnić i uporządkować Usecases
    4. Uzupełnić i uporządkować Wymiary karty postaci
3. Następny krok
    1. Priorytetyzacja NFun per akcja
    2. Gdy mamy priorytety, projektować i testować pod kątem priorytetów
    3. Zrobić szkieletowe karty i sprawdzić jak działają
    4. ...
    5. Profit.

