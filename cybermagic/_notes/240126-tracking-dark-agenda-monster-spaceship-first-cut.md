# Tracking Dark Agenda, monster on the spaceship, first cut (240126)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Dark Agenda, tory, autoprzesuwanie ruchów Drugiej Strony do przodu.
* Wysokopoziomowa idea do przeanalizowania i sprawdzenia "kiedyś".

---
## 2. Spis treści i omówienie

1. Cztery fazy sesji z Potworem Na Statku
2. Konstrukcja sesji
    1. Potwór
    2. Progress Agendy potwora
    3. Dark Agenda
    4. Teren
3. Faza Inicjacji Sesji
    1. Scena Zero
    2. Scena pierwsza i dalsze działania
4. Faza Ekspansji Środków i Zajmowania Terenu
    1. Zajmowanie własnych torów / zasobów
    2. Zajmowanie wspólnych torów / zasobów
    3. Dark Agenda
5. Faza Kontrakcji / Payoff
6. Pytania i czego nie wiem

---
## 3. Cztery fazy sesji z Potworem Na Statku

Ciekawy koncept na strukturę sesji. Coś do sprawdzenia.

Konstrukcja sesji – faza inicjacji - faza ekspansji środków i zajmowania terenu - faza kontrakcji / payoff

## 4. Konstrukcja sesji

![Potwór, wizualizacja](Materials/240126/001-monster.webp)

Wyobraźmy sobie taką sesję - na statku kosmicznym jest potwór. Potwór ma następujące parametry defensywne i ofensywne (zgodnie z torami które były kiedyś):

* Defensywy:
    * odporność na ataki kinetyczne: 3
    * duża żywotność (wspierana bardzo szybką regeneracją) i niekształt antykinetyczny: 4
* Agenda:
    * przejęcie kontroli nad systemem podtrzymywania życia: 1
    * przejęcie kontroli nad pokładem: 3
    * przejęcie kontroli nad statkiem: 3
* Baza
    * Przeciwnik Ekstremalny
* Siły
    * Zmiennokształtność oraz infekcja ofiar
    * Ataki fizyczne o dużej mocy (kolcowęże)
    * Szybka regeneracja i asymilacja biomasy
    * Podzielność i pułapki
* Słabości:
    * Niskie temperatury

Widzimy jakie ruchy robi taki przeciwnik. Zaczyna z poziomu ‘małego bloba’, w jakiś sposób się rozprzestrzenia i powiększa i kończy jako potężny przeciwnik do rozwalenia zanim stacja kosmiczna ucierpi.

Załóżmy, że nałożymy na to agendę **The Devastator**. Jako statek kosmiczny biorę **supply ship klasy Arcadalian**.

Mam więc teren, podstawowy rozkład statku, Dark Future, Tracking (tory) oraz Agendę przeciwnika. Teraz muszę dodać grupę prostych NPC i faza konstrukcji sesji jest zakończona.

Cel graczy - zniszczyć przeciwnika zanim przeciwnik osiągnie Dark Agenda. Z uwagi na szybką regenerację, tor zadawanych obrażeń może ulec regeneracji.

## 5. Faza Inicjacji Sesji

W fazie pierwszej – inicjacji - często zaczynamy od sceny zero (mającej pokazać konkretne rzeczy) i wprowadzamy główne postacie graczy. Głównym celem tego jest poszerzenie wiedzy graczy o otoczeniu, demonstracja głównych kluczowych npc oraz pokazanie co tu się mniej więcej dzieje.

Faza inicjacji już posuwa mroczną agendę do przodu. Już tu coś się dzieje i Potwór działa. Faza inicjacji kończy się w momencie, w którym gracze dowiadują się o obecności potwora.

## 6. Faza Ekspansji Środków i Zajmowania Terenu

Ta faza zaczyna się zawsze od demonstracji potęgi potwora. Ta faza jest wyścigiem - postacie graczy próbują zabezpieczyć co się da, osłonić ze statku tyle ile się da i zbudować zasoby pozwalające na walkę z potworem. Przygotowanie do ostatecznej konfrontacji. Mogą występować formy dywersji, spowolnienia potwora, ataku i tymczasowego osłabienia, chowania się i pozyskiwania kluczowych zasobów.

Podczas tej fazy gracze budują własne tory. Przykłady takich torów w wypadku tej sesji (wymyślane przez graczy, nie MG):

* Zapasy ciekłego azotu: 0 -> 2
* ładunki wybuchowe dużej mocy: 0 -> 2
* przesunięcie potwora w miejsce bez możliwości ucieczki: 0 -> 2

Do tego występują też zasoby samego statku o które można walczyć (w większości wymyślane przez MG):

populacja statku kosmicznego: 5 (20% ea)
struktura statku kosmicznego: 3 (po przekroczeniu ‘3’ konieczność ewakuacji)
potwór nie dał rady się ewakuować: 2

W tej fazie obie strony - potwór oraz gracze - próbują zabezpieczyć pewne obszary i pewne zasoby. Każdy element populacji statku zajęty przez potwora staje się jego zasobem (infekcja). Każdy element uratowany podnosi chwałę i punkty graczy.

W końcu graczom udaje się zabezpieczyć odpowiednią ilość zasobów. Są przekonani, że są w stanie walczyć z potworem. Lub potwór jest za blisko mrocznej agendy. Przechodzimy do kolejnej fazy.

## 7. Faza Kontrakcji / Payoff

Postaci graczy konfrontują się z potworem (niekoniecznie bezpośrednio, acz zwykle). To jest faza, w której potwór uderza bezpośrednio w zasoby postaci i postacie próbują uderzyć w potwora.

Przykładowe sytuacje będą takie: 

* Gracz poświęca zasób zapasu ciekłego azotu (2 -> 1) by zadać niemożliwą do zregenerowania ranę. 
* Gracze pozycjonując potwora ptaszkami redukują jego tory przetrwania. 
* Potwór atakując graczy uderza albo w ich zasoby, albo w to co wypracowali, albo nadaje im odpowiednie negatywne statusy.

Jeżeli gracze są skłonni posunąć się odpowiednio daleko i im nie wychodzi, mogą nawet zginąć. Jeżeli graczom idzie dobrze, potwór może zdecydować się wycofać. Jeśli gracze nie są w stanie go zatrzymać, będzie w stanie docelowo się zregenerować. Ale jeżeli gracze dadzą radę go zatrzymać, mogą go zniszczyć.

Wyobrażam sobie, że gracze są w stanie się oderwać od potwora. Wtedy wracamy do poprzedniej fazy - potwór zacznie się regenerować i budować nowe zasoby,  gracze zaczną opracowywać jak najszybciej można do czegoś dojść, żeby potwora się pozbyć. Jakie zasoby i okoliczności.

Tory stanowią postęp do sukcesu. Potwór z wyzerowanymi torami życia jest zniszczony.

## 8. Pytania i czego nie wiem

* Jak robić automatyczny przydział torów - jak postępować i trackować mroczną agendę?
* Jak skalować siłę przeciwnika i jak podnosić presję cały czas?
* Jak pokazać jak daleko potwór się rozprzestrzenił z perspektywy Dark Agenda?
* To jest model tego jak to zrobić. A teraz trzeba zaprojektować subnarzędzia które to osiągną.

Do analizy: 

* Zeiram @ Iria, pierwszy odcinek
