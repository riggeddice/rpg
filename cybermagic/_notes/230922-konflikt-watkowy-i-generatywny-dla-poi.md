# Wyjaśnienie: Podejście do konfliktu wątkowego i generatywnego (dla PoI)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Darken

---
## 1. Purpose

* Odpowiedź na serię pytań
* Konflikt generatywny na przykładzie budowania PoI
* Konflikt wątkowy (overarching, kilka miesięcy)
    * Stosowanie złożonych mechanik

---
## 2. Spis treści i omówienie

1. Co rozwiązujemy
    1. Pytanie/Cel - Kontekst - Default Future
    2. Analiza wysokopoziomowa - wymiary celu
    3. Nadajemy imię Celowi - Celina
2. Pierwszy wymiar: Czy w ogóle można zrobić z tego jeden konflikt?
    1. Zacznijmy od tyłu - jak zrobić 'długoterminowy' / 'wątkowy' konflikt mechaniczny EZ?
    2. Dobrze - jak z TEGO konfliktu zrobić jeden konflikt i czy warto?
        1. Kiedy zrobić 'zoom-out' a kiedy 'zoom-in' w konflikcie?
        2. Co wiemy o tej sytuacji i co wybrać
        3. Czyli tu warto. Jak to zatem zrobić? 
            1. Anatomia konfliktu
            2. Przejdźmy zatem przez TEN KONKRETNY konflikt
3. Drugi wymiar: Jak zrobić, by konflikt odpowiedział nam na pytanie fabularne?
    1. Konflikt odpowiada na pytanie fabularne przez (V) i (X)
    2. Jak zrobić to szybko i minimalną inwestycją (minimum przygotowania)
4. Jak złożyć bardzo złożony konflikt, jak stawki, jak dark future?
6. Jak sprawić, by konflikt nam procentował "na potem"
    1. Jak zrobić, by konflikt zbudował nam ciekawe POI "na potem"
        1. Co to jest "ciekawe" PoI?
        2. Jak zrobić ciekawe PoI bez mechaniki (ogólnie)
        3. Mikropodsumowanie zanim przejdziemy do naszej sytuacji - "ciekawe PoI"
        4. Jak zrobić ciekawe PoI przy użyciu mechaniki
        5. To teraz zróbmy te góry gdzie uciekła Celina w ten sposób
        6. ...a teraz zróbmy to tanio z niskim przygotowaniem
    2. Jak zrobić, by konflikt dał nam ciekawe ramy fabularne na potem



---
## 3. Co rozwiązujemy
### 3.1. Pytanie/Cel - Kontekst - Default Future

* **pytanie i cel wysokopoziomowy**
    * jak zrobić w jednym konflikcie wyprawę poszukiwawczą/ratowniczą dla grupy 4 postaci która prowadzi przez kilka krajów i trwa miesiąc albo więcej
    * chcemy szybko skończyć wątek i jakoś podsumować
    * konflikt może wygenerować scenę lub dwie na uszczegółowienie potem sytuacji
        * w idealnym świecie, konflikt wygeneruje PoI (Point of Interest)
        * konflikt ma posłużyć jako ramy fabularne do "czegoś potem"
* **kontekst**
    * Cel zniknął podczas wyprawy wojennej i został uprowadzony
    * Cel uratował się sam ale oddalił od miejsca w którym mniej więcej spodziewają się go znaleźć
    * gracze wiedzieli gdzie zacząć pytać o Cel ale nie wiedzieli gdzie jest. Teraz ten Cel się przemieszcza i gracze nie wiedzą o tym
* **potencjalna przyszłość (default future)**
    * Cel WRÓCI
    * pytanie czy sam czy postacie go znajdą

### 3.2. Wysokopoziomowa analiza - wymiary celu

Mamy tu kilka problemów jednocześnie:

* 1: Czy w ogóle można zrobić z tego jeden konflikt?
    * 1.1.: a jeśli tak, jak to zrobić mechanicznie w EZ? W końcu mamy pule, żetony itp...
* 2: Jak zrobić, by konflikt odpowiedział nam na pytanie fabularne ("Cel wrócił sam, Cel uratowany...")
    * 2.1.: i jak zrobić to relatywnie szybko
    * 2.2.: i minimalną inwestycją (minimum przygotowuję)
* 3: Jak złożyć bardzo złożony konflikt (tor, łańcuch konfliktów) by był JEDNYM konfliktem
    * Jak ustawić stawki takiego cholerstwa
        * dark future, maximum success
        * potencjalne sukcesy i porażki, co można poświęcić, w ogóle "stożek możliwości" konfliktu
* 4: Jak sprawić, by konflikt nam procentował "na potem"    
    * 4.1.: Jak zrobić, by konflikt dał nam ciekawe ramy fabularne na potem
    * 4.2.: Jak zrobić, by konflikt zbudował nam ciekawe POI "na potem"

Wszystkie rozpatrzymy osobno.

### 3.3. Nadajemy imię Celowi - Celina

Jako, że mamy: 

* Cel (w rozumieniu, osobę do ratowania) 
* i będziemy rozpatrywać cele MG/Graczy (w rozumieniu: rzeczy do osiągnięcia)

to Cel (osobę) od tej pory nazywamy **Celina**.

### 4. Pierwszy wymiar: Czy w ogóle można zrobić z tego jeden konflikt?
#### 4.1. Zacznijmy od tyłu - jak zrobić 'długoterminowy' / 'wątkowy' konflikt mechaniczny EZ?

Przykłady takiego konfliktu:

* "Jak potoczyła się bitwa przy Czółenku? Trzy niezależne oddziały noktiańskie spotkały się z defensywą Pustogoru, a wtedy jeszcze zaatakowały potwory Trzęsawiska"
* "Jaki był stan Voyagera podczas 10-letniego przelotu przez niebezpieczny teren?" (Star Trek)

Jak to zrobić? Na pierwszy rzut oka, jest to trudne. 

* Konflikt ma co najmniej dwie strony
    * Tu mamy jakby... "serię konfliktów na przestrzeni lat"
* Strona aktywna ma jakiś stopień trudności (Trudne? Ekstremalne?), zasoby itp.
    * A tutaj mamy grupę bohaterów o różnych statystykach i różnych celach i różnych możliwościach

Ale! Cofnijmy się o krok i przypomnijmy sobie jedną z kluczowych zasad systemu:

* [Stabilizacja mechaniki 230603, oraz wynikający z niej Evergreen](230603-mechanika-2305-stabilizacja.md)
    * (1) **Każdy konflikt da się zamodelować**
    * (5) Poziomy mechaniki i problem "Wizard i Excel"
        * Istnieje zbiór podstawowych zasad, które zawsze działają i których użycie daje ci minimalną EZ.
        * Im bardziej zależy ci na modelowaniu złożonych konfliktów, tym więcej musisz użyć zasad bardziej zaawansowanych.
        * Czyli zasady możemy podzielić na kręgi. Rdzeń, w którym są zasady podstawowe i potem dodatkowe warstwy Które mogą zmieniać zasady Rdzenia.

To nie jest typowa sytuacja, więc musimy cofnąć się z poziomu "zasady nam mówią dokładnie jak to zrobić" (wizard) do poziomu "da się to zrobić jeśli określimy cel i użyjemy dostępnych klocków" (excel). Fundamentalnie, na czym nam zależy w tej sytuacji (przykład z Voyagerem)?

* Czy Voyager (ze Star Trek:Voyager) wróci do Floty Kosmicznej?
* Kto przetrwa a kto nie? 
* Jak daleko musi się posunąć załoga Voyagera, ile stracą swoich ideałów?
* ...itp

Fundamentalnie **nie ma znaczenia** jak dokładnie matematycznie ustawimy konflikt. Czy potraktujemy to jako łańcuch konfliktów z _podbiciami i manifestacjami_ per łańcuch zaczynając od konfliktu Ekstremalnego, czy jeszcze inaczej, ważne jest to, byśmy dostali odpowiedzi na pytania.

**ZAWSZE:**

* Mamy możliwość ustawienia łańcuchów konfliktów, jak demonstrowałem w [notatce o mapowaniu konfliktów (230828)](/230828-mapowanie-konfliktow.md). Czyli, tutaj:
    * POWRÓT: Voyager zostanie uratowany przez flotę kosmiczną (p) -> Voyager wróci, w stanie ciężkim (m) -> Voyager wróci z nową technologią, uratuje kogoś itp. (e)
        * krzyżyki: po powrocie na złom, fatalna reputacja gwiezdnej floty w sektorze
    * PRZETRWANIE: Przetrwały główne nazwane postacie (p) -> Przetrwała zdecydowana większość nazwanych postaci (m)
        * krzyżyki: ginie konkretna postać, konieczność zdobycia lokalnej załogi -> konieczność PORWANIA lokalnej załogi
    * IDEAŁY: Voyager złamał zasady floty kosmicznej, ale pozostał lojalny duchowi (s)
        * krzyżyki: część nazwanej załogi Voyagera złamały ducha zasad floty...

Widzicie? To powyżej jest proste, jeśli wiecie o co toczy się gra w fikcji

* **Strategia 1: arbitralny konflikt Ekstremalny**
    * Ustawiamy to jako konflikt `Ex +2`, czyli (`11X, 5V`)
        * pytamy graczy co są skłonni poświęcić. Np. "będziemy patrzyli pragmatycznie, wchodząc w sojusze z mrocznymi siłami, ale nie będziemy piratami". Dobrze więc, dam za to Przewagę. -> `Ex Z +2`, czyli (`11X 8V`).
* **Strategia 2: całkowicie arbitralna pula**
    * Dobra, nie wiem jaki jest konflikt i stopień trudności. 
        * Ale wiem, że `Tr +2` to `8V,  8X`. 
        * Wiem też, że będziemy ciągnąć około 5 razy
    * Więc wybieram pulę podstawową niekorzystną: 
        * `10X, 8V` (i wiem, że wyciągając 5 żetonów, będzie więcej `X` niż `V`)
        * Ważne, żeby operować WIĘKSZĄ ilością żetonów niż `8X, 8V`, bo przy za małej ilości żetonów po prostu wyczerpiemy pulę i będziemy musieli bazować na "pula nie może mieć mniej niż 1 żeton z typu", więc musimy zwracać i psuć prawdopodobieństwa.

Czemu to zadziała?

* Przy puli "Tr Z +3" (`8X, 12V`) dla 10 testów mamy średnio `4X 6V`.
* Przy puli "Ex +2" (`11X, 5V`) dla 10 testów mamy średnio `7X 3V`.

Czyli różni się średnia ilość sukcesów, ale to wszystko jest negocjowalne w fikcji (co poświęcamy, co bierzemy). Jeśli BARDZIEJ chcemy "więcej się uda", wychodzimy z pul gdzie `V` jest więcej niż `X`. Możemy też zawsze poddać konflikt jak wygramy wszystko na czym nam zależy.

I teraz, załóżmy, że mamy 10 testów, z czego: 

* 3 robione są przez eksperta (Tr Z+3 -> `8X, 12V`)
* 3 robione przez łosia (Ex+2 -> `11X, 5V`)
* 4 przez regularów (Tr+2 -> `8X, 8V`)
* średnia ilość X: `1.2 + 2 + 2` = `5X`, średnia ilość V: `5V`

Czyli, **nieważne** że różne postacie robią różne rzeczy na przestrzeni wyprawy. Jeśli traktujemy **całą drużynę jako kompetentny zbiór** i mamy różne trudności, możemy spłaszczyć prawdopodobieństwa do jednego rzutu. Tracimy na precyzji, dramaturgii itp, ale zyskujemy na prędkości i nadal mamy dostęp do odpowiedzi. Matematyka się uśredni.

Czyli: prawdopodobieństwo i modelowanie mają służyć MG i Graczom. Nie są kaftanem bezpieczeństwa. Są mechanizmem podjęcia negocjacji ("to tracimy", "tego nie poświęcamy") a nie narzędziem opresji MG i Graczy.

#### 4.2. Dobrze - jak z TEGO konfliktu zrobić jeden konflikt i czy warto?
#### 4.2.1. Kiedy zrobić 'zoom-out' a kiedy 'zoom-in' w konflikcie?
#### 4.2.1.1. Przykład zoom-out

Na sesji `230920-legenda-o-noktianskiej-mafii` mamy następującą sytuację:

* Amanda i Xavera, dwie postacie Graczy stają naprzeciw czterem uzbrojonym żołnierzom
    * Same są słabo uzbrojone i są w unieruchomionych ścigaczach
* Amanda i Xavera zostawiają granat w ścigaczu, uciekają do lasu i terroryzują czterech żołnierzy.

Cały konflikt był JEDNYM konfliktem, wyglądał mniej więcej tak:

```
* X: ścigacze ruszyły i się zablokowały. Kolesie wypadają z budynku i Wy jesteście w sumie przy lesie. Ale ścigacze mają broń.
* Vz: Furie wymieniły spojrzenie, Amanda szybko wyjęła zawleczkę z granatu w ścigaczu i Furie łapiąc broń lecą w długą. Kolesie biegną za nimi strzelając, ale nie trafiają.
    * "Łap je łap je!"
* V: Jeden ścigacz zniszczony granatem (ich własnym). Drugi ścigacz jest uszkodzony ale sprawny. Jeden koleś ma potrzaskane nogi. Ścigaczem. Drugi koleś nie żyje. Dwóch pozostałych takich... zamroczonych. A Furie w las.

Dwóch zamroczonych kolesi, jeden uszkodzony ścigacz. Jeden umierający pod zniszczonym ścigaczem. Amanda bezwzględnie zastrzeli jednego Grzymościowca. Jest jeden z wyraźną mową ciała "WTF CHCĘ DO DOMU" i drugi - a raczej druga - która szuka Was w lesie. Amanda strzela do laski.

* X: Laska skutecznie się wycofała i ewakuowała się do fortifarmy. Niestety, Amanda nie mając swojej broni i mając - dla odmiany - kompetentnego przeciwnika w pancerzu nie była w stanie jej zabić. Acz laska jest ranna i najpewniej nie ma ochoty na walkę.
* V: Xavera osłaniana przez Amandę szybkimi susami dotarła do faceta. Laska próbowała się wychylić z fortifarmy ale strzał Amandy ją bardzo zniechęcił. Laska zmieniła plan.

```

zauważcie, że ten konflikt jest bardzo wysokopoziomowy. Jest "płaski emocjonalnie". Amanda i Xavera nigdy nie są naprawdę zagrożone - pytanie nie brzmi "czy im się uda" a "ile osiągną". To nie jest walka - Amanda i Xavera są po prostu za dobre a przeciwnicy nie wiedzą co się dzieje i z czym mają do czynienia.

#### 4.2.1.2. Przykład zoom-in

Sesja `230729-furia-poluje-na-furie`, ten sam cykl (dla uproszczenia):

```
Tunele są wąskie, paskudne, rodem z horroru, korzenie próbujące wślizgnąć się pod ubranie i ssać krew. Trzeba momentami pełzać i poruszać się po fatalnym terenie. Amanda jest PEWNA, że są zakażone jakimś cholerstwem. Ale nie ma wyjścia.

Ex Z +3 +3Or:

* X: Furie są lekko pocięte, poranione, possane krwią i zakażone.
* Vr (p): Furie przejdą przez ten przeklęty labirynt korzeni i tuneli (+2Vg bo dziewczyny TEŻ znają Furie)
* X: Furie tracą wiekszość non-essential equipment. Nie przejdą z tym, nie w tym wypadku.
    * Ayna uruchamia sygnatury pozostałych servarów, by Ralenę wyprowadzić z równowagi i rozbić przeciwników trochę (+1Vg)
* Vg (m): Furie wypełzły jakimś uszkodzoną szczeliną ze wzgórza; jeden awian w powietrzu, drugi na ziemi. Żaden ich jeszcze nie zauważył. A Amanda ma snajperkę.
    * Xavera: "Amando, Ty celuj w to u góry, ja się zakradam, Ayno - osłaniaj. Jeśli oni zadziałają, STRZELAJ!"
    * (sekunda potem) Xavera: "servar pilnuje awiana..."
        * Xavera wykorzysta jedyny granat jaki ma, by uszkodzić servar. A Amanda zestrzeli awiana, uszkodzi go (ma servarową snajperkę). Ayna kryje... (+1Vr +2Or)
            * Ayna: "mamy jedną minę. Jedną"
            * Amanda: "skąd?"
            * Ayna: "jaszczury. Zabrałam jedną, Xavera osłaniała. Zajęło Ci chwilę (info)"
            * Ayna: "spowolnię Ralenę zrzucając aktywną minę w wejście do jaskini"
* X: Furie zostały zauważone gdy się rozdzieliły na powierzchni. Przeciwnicy NIE SĄ zaskoczeni.
* X (p): Zarówno Xavera jak i Ayna zostały poranione ogniem; pancerze przetrwały i uratowały im życie, ale są ranne.
* X: Ayna skutecznie zawaliła jaskinię, unieszkodliwiając osoby w środku. Ale Ralena zdążyła opuścić jaskinię i przechwycić Aynę.
* V: Amanda snajperką ppanc trafiła w awiana w powietrzu; ten się zdestabilizował i zaczyna crashlandować.
* Vr: Xavera, ranna, wykorzystała granat by ŁUPNĄĆ we wrogiego servara. Wybuch go uszkodził ale nie zniszczył. Xavera zignorowała servar by dostać się do awiana i zastrzeliła pilota.
    * Następnie obróciła awiana by przewrócić servar i ruszyła w stronę Amandy. (+3Vg)
        * Xavera: "Amando, łap się!"
* Vg: Xavera, jak to ona, wbiła się awianem. Ralena uskoczyła, Xavera przechwyciła Aynę (jeśli nie porwaną - bardzo ciężko ranną).
* X: Ralena dała radę trafić w awiana. Awian doleci kilkanaście kilometrów w stronę na Pacyfikę i się rozbije, pilotowany przez Xaverę.
* Vg: ranna Xavera, bardzo ciężko ranna Ayna, zatruta Amanda wylądowały DALEJ. Bliżej Pacyfiki.
```

Każdy ruch ma znaczenie. Każda decyzja. Określamy tak niewielkie rzeczy, jak "kto jest ranny" czy "jakie mają pozycje, jak są ustawione". Tu znaczenie ma wszystko, jak i widzimy różne wymiary:

* Zdrowie i stan Furii. Tylko Amanda jest operacyjna po akcji, a nawet ona jest ranna.
* Ayna jest zdjęta z akcji i bezużyteczna
* Xavera, Ayna i Amanda są praktycznie nieuzbrojone.
* Przeciwnik - Ralena - wie o nich i ma przewagę.
    * ALE jest od nich oddalona, więc Furie mogą się jakoś schować. Acz mają mało czasu i nie mogą zostać ukryte

Gdyby nie szybkie wsparcie Kajrata, nasze Furie by zostały pojmane po tym spotkaniu. Ale kupiły sobie jeszcze chwilę - a o to fundamentalnie im chodziło.

#### 4.2.1.3. Wyprowadzenie reguły

* Jeśli zależy nam tylko na wyniku, bez szczególnych okoliczności, robimy zoom-out.
    * Nieważne co dokładnie kto robił
    * Idziemy szybko, immersja i taktyka mają mniejsze znaczenie
    * WIĘC upraszczamy mechanicznie i idziemy dalej -> Zoom-out
* Jeśli zależy nam na demonstracji detali lub przeciwnicy są podobnej mocy, robimy zoom-in.
    * czyli: demonstracja postaci, ich możliwości i różnic I/LUB przeciwników, ich siły i stanu
    * chcemy pokazać coś zaawansowanego LUB wiarygodnie nałożyć jakiś stan (ranna, przerażona) na jakąś stronę
    * ważna jest taktyka, praca z terenem lub "jak oni to zrobili"
    * ważna jest głębsza immersja a nie prędkość
    * WIĘC wchodzimy w głąb, silniej dynamicznie zmieniamy pule i robimy Zoom-in

#### 4.2.2. Co wiemy o tej sytuacji i co wybrać?

Cytując specyfikację: "chcemy szybko skończyć wątek i jakoś podsumować"

To zdecydowanie krzyczy "zoom-out", bo: 

* Ważne są tylko **wyniki** a nie **kto co zrobił** lub **jak to było osiągnięte**. 
* Jesteśmy w trybie generatywnym a nie generatywno-eksploracyjno-taktycznym. Nieważny jest taktyczno-dramatyczny silnik a konkretny wynik.
* Chcemy "mieć to z głowy" i dostać szybką odpowiedź z silnika mechaniki

#### 4.2.3. Czyli tu warto. Jak to zatem zrobić? 
##### 4.2.3.1. Anatomia konfliktu

Każdy, ale to absolutnie **KAŻDY** konflikt ma kilka elementów:

* wbudowaną kolizję światów - poziom **Wizji**
    * Gracz Pierwszy chce "świat wygląda XXX", Gracz Drugi chce "świat wygląda YYY"
        * Więc wynik konfliktu ma powiedzieć: jaki zbiór okoliczności z XXX wchodzi a jaki z YYY. "Kto wygrał"
* **wspiera sesję**
    * nie jest nieistotną dygresją, tylko zbliża nas do końca jednego wątku sesji lub dodaje ważny subwątek (np. demonstruje coś ważnego, stawia opozycję, wbogaca świat...)
    * ma ważny cel: w jakiś sposób wzbogaca sesję: demonstruje postacie / sytuację / teren, rozwiązuje stawki, prowadzi do zmiany postaci graczy (emocjonalnej, społecznej...)
* ma **opozycję / przeszkodę** - są dwie (lub więcej) aktywne strony chcące różnych rzeczy
* jest **aktywny** - co najmniej jedna strona coś robi, druga na to reaguje (lub robi coś innego)
    * coś się dzieje, postacie coś robią...
* ma **stawki** - obie strony mają coś do stracenia i do wygrania
    * to jest tzw. zakres konfliktu - o co gramy i o co nie gramy
    * to sprawia, że konflikt i każda eskalacja ma **zmianę / wynik**
* ma **emocje lub jest szybki** - jeśli nie ma emocji, wejdź na zoom-out i zrób go szybko, usuń go z drogi i przejdź do czegoś ciekawszego.

.

Popatrzcie na przykład sesji `230729-furia-poluje-na-furie` o której już mówiłem:

![Anatomia konfliktu](Materials/230923/01-230923-anatomia-konfliktu.png)

Mamy sesję wysokopoziomową, w której Furie uciekają do jedynego sojusznika w pobliżu, Kajrata. I na drodze staje im Ralena (inna Furia, przyjaciółka) której wyprali mózg. Ralena próbuje porwać Furie i przekazać je swoim właścicielom.

* wsparcie sesji macierzystej
    * Furie chcą uciec przez trudny teren do Kajrata. Ralena i łowcy stanowią groźną przeszkodę.
        * powiązanie z sesją 1:1, bezpośrednie i liniowe
    * Demonstruje to, że Furie polują na Furie; pokazuje też Graczom los ich postaci jak przegrają.
        * smutek i determinacja "wrócimy po Ciebie, Raleno!"
    * Zdecydowana przewaga Raleny sprawia, że Furie nie mają szans jej pokonać uczciwie
        * demonstracja desperacji, intelektu i taktyki Furii (Graczy)
        * demonstracja, że Ralena nie chce ich krzywdzić a tylko pojmać, co daje Furiom przewagę
* kolizja wizji
    * MG chce, by Ralena pojmała Furie. Kajrat uratuje Amandę. Mogą być poranione i zatrute.
    * Gracze chcą, by Furie pokonały/przechytrzyły Ralenę, zdobyły sprzęt i szybciej dotarły do Kajrata
    * to rozpina nam potencjalne stawki reprezentowane przez łańcuchy konfliktów: 
        * Furie: zdrowe? wszystkie uciekły?
        * Sprzęt: zachowaliśmy? mamy dodatkowy? nie mamy nic?
        * Kajrat: jesteśmy bliżej niego? stracił żołnierzy nas ratując?
        * Ralena: zmniejszymy jej oddział? Oderwiemy się od niej?
* opozycja / przeszkoda
    * strona 1: Amanda, Xavera, Ayna
    * strona 2: Ralena i łowcy
    * strona 3: morderczy teren, który pragnie maksymalnie skrzywdzić wszystkich
* konflikt aktywny
    * aktywność na polu **fizycznym** i **taktycznym**. Skradanie, walka, przechytrzenie... ale też **emocjonalnym** - próby dotarcia, próby zniszczenia morale...
* stawki i zakres konfliktu
    * pokazane wyżej, wyprowadzone z kolizji wizji.

##### 4.2.3.2. Przejdźmy zatem przez TEN KONKRETNY konflikt

Najważniejszy jest kontekst i sukces/porażka. Kontekst jest jedynym co daje mi jakieś powiązanie z "macierzystą sesją".

* kontekst
    * Celina zniknęła podczas wyprawy wojennej i została uprowadzona
    * Celina uratowała się sama, ale oddaliła od miejsca w którym mniej więcej spodziewają się ją znaleźć
    * gracze wiedzieli gdzie zacząć pytać o Celinę, ale nie wiedzieli gdzie jest. Teraz Celina się przemieszcza i gracze nie wiedzą o tym
* potencjalna przyszłość (default future)
    * Celina WRÓCI
    * pytanie czy sama czy postacie ją znajdą

No dobrze. Czyli wiemy, o co **nie** toczy się stawka - stawka nie toczy się o losy Celiny (wiemy, że wróci) a o to, czy postacie graczy ją uratują.

Przejdźmy przez checklistę. _(uwaga, zmyślam - nie znam sytuacji)_:

* wsparcie sesji macierzystej 
    * postacie graczy chcą pozyskać fundatorów i pokazać się jako skuteczni eksploratorzy
        * wyprawa ratunkowa Graczy ma za zadanie znaleźć Celinę i doprowadzić ją do domu
        * wyprawa ma więc pokazać ich przedsiębiorczość i zapewnić im środki na przyszłość
    * dodatkowo: demonstracja / generacja ważnej lokalizacji
* kolizja wizji
    * wszyscy się zgadzamy, że: 
        * Celina wróci do domu
        * postacie graczy przeżyją i są dalej "sprawne"
    * wizja MG:
        * postacie graczy nie poradzą sobie z groźnymi górami i będą musiały być uratowane
        * postacie graczy uratowane a Celina w domu -> pośmiewisko, kwestionowanie kompetencji
        * do tego jeszcze utracone zasoby, śmierć wartościowych lokalsów, ogólnie - kaplica
    * wizja Graczy:
        * postacie graczy dotarły do Celiny i ją uratowały, bo wpadła w kłopoty mimo ucieczki
        * do tego, postacie zapewniły sobie fundatorów i dobrodziejów oraz finansowanie
        * postacie są popularne, bo uratowały Celinę
* opozycja / przeszkoda
    * Celina: próbuje się ukryć i wymanewrować tropiących
    * góry: morderczy teren, który próbuje wszystkich (poza Celiną) uciemiężyć
    * Tropiciele Celiny: którzy nie pogardzą zniszczeniem ekspedycji postaci graczy
* konflikt aktywny
    * postacie graczy wspinają się, planują trasy, rozbijają obóz, negocjują z tubylcami, szukają śladów, wymanewrowują tropicieli
    * góry są pasywną przeszkodą. Celina jest pasywną przeszkodą. Tropiciele w sumie też.
* stawki i zakres konfliktu
    * stan postaci graczy i zasobów
    * poziom wdzięczności Celiny / dobrodziei i fundatorów / popularność graczy
    * bogactwo i zyski postaci graczy, możliwość przyszłych ekspedycji
    * poziom dyplomacji z tubylcami, jakieś dodatkowe okazje na tamtym terenie?

Teraz zostaje tylko poustawiać (podbicia - manifestacje - eksplozje) i potencjalne ptaszki i krzyżyki do stawek i zakresu i w sumie mamy :-).

### 4. Drugi wymiar: Jak zrobić, by konflikt odpowiedział nam na pytanie fabularne?
#### 4.1. Konflikt odpowiada na pytanie fabularne przez (V) i (X)

Jak pokazałem w poprzednim punkcie. Stawiamy konflikt, np. na puli Ex+3 (`11X, 6V`). I ciągniemy.

* Wypada (`V`). Pytam gracza: 
    * "na czym zależy Ci najbardziej?"
    * "proponuję jako pierwsze 'wrócicie do domu bez pomocy' a jako drugie 'wróciliście z Celiną'. Dzięki temu nawet jak wszystko inne nie wyjdzie, będzie dało się zmyślać."
* Wypada (`X`). Proponuję graczom coś złego:
    * "może... ślady Celiny prowadziły do świętej dla tubylców góry. Musieliście ich zmusić, by tam poszli, ale oni Was teraz naprawdę nie lubią bo zmusiliście ich do splugawienia świętości..."
    * jak graczom pasuje, wezmą tą ZMIANĘ.
        * jak nie, zaproponują coś innego.

Ważne - każdy (V) lub (X) musi stanowić **ważną i obserwowalną** zmianę w obszarze istotnym dla graczy.

* Źle:
    * (V): Gracze są na tropie Celiny, znaleźli jej stare ślady
    * (V): Gracze PRAWIE dotarli do Celiny
* Dobrze:
    * (V): Gracze są na tropie Celiny, znaleźli jej stare ślady i obozowisko
    * (V): Gracze dotarli do celiny

Czemu tak:

* w Złym przykładzie: 
    * (1) i (2) są **dokładnie tym samym**. Nic się nie zmienia. Nieważne, czy jesteś 5 sekund po drugim miejscu czy 10 minut, i tak jesteś trzeci.
    * eskalacja z (1) do (2) nie ma nowych stawek, nie ma emocji. Utrzymujesz to, co jest już kupione.
* w Dobrym przykładzie
    * każda eskalacja zauważalnie zmienia stan gry. Są emocje. Są inne.

#### 4.2. Jak zrobić to szybko i minimalną inwestycją (minimum przygotowania)

Jeśli wiesz, że: 

* każdy konflikt jest powiązany z macierzystą sesją (_overarching goal_)
* jakie masz stawki i zakres konfliktu

To masz wszystko czego Ci trzeba. Resztę dogenerujesz dynamicznie. Idź według tej checklisty:

* wsparcie sesji macierzystej 
    * co w ramach tej opowieści chcą gracze?
    * czemu ten konflikt / scena istnieje? Co zmienia?
* kolizja wizji
    * jak wygląda świat według wizji MG?
    * jak wygląda świat według wizji Graczy?
* opozycja / przeszkoda
    * KTO chce CZEGO i CZEMU tego nie może dostać?
* konflikt aktywny
    * co robią poszczególne strony? 
    * co uważają za sukces i porażkę?
* stawki i zakres konfliktu
    * co można wygrać, co przegrać?
    * co jest ważne dla różnych stron?
    * czego strony nie chcą tracić?
    * co byłoby fajne zgodnie z opowieścią macierzystą? Co problematyczne?

I po prostu masz. Mi wystarcza na to 30 sekund do 2 minut. Nie muszę znać DOKŁADNYCH odpowiedzi, po prostu WYSTARCZAJĄCO dokładne.

### 5. Jak złożyć bardzo złożony konflikt, jak stawki, jak dark future?

Częściowo odpowiedziałem powyżej, częściowo tym zajmuje się notatka o podbiciach, manifestacjach, eskalacjach i łańcuchach. Czyli: [ta notatka, 230828-mapowanie-konfliktow](230828-mapowanie-konfliktow.md)

### 6. Jak sprawić, by konflikt nam procentował "na potem"
#### 6.1. Jak zrobić, by konflikt zbudował nam ciekawe POI "na potem"
#### 6.1.1. Co to jest "ciekawe" PoI?

Paradoksalnie, to jest łatwiejsze do wyjaśnienia.

Co odróżnia poi które jest ciekawe od takiego, które jest nudne? 

* Czy ciekawsze jest „pole”, czy też „pole, na którym w nocy pojawia się dziwny dźwięk”? 
    * coś doprecyzowanego
    * coś **zaskakującego**
* Ciekawsza jest „myjnia samochodowa”, czy też „Myjnia do której przyjeżdżają czasem 3 czarne wołgi”?
    * coś doprecyzowanego, ciekawego
    * dodatkowy **zbiór czynności**
* Ciekawsza jest "burgerownia" czy "burgerownia na której niczego nie płacisz jeżeli pokonasz na rękę kucharza"
    * coś doprecyzowanego, zaskakującego (nietypowego), dodatkowy zbiór czynności
    * dodatkowe **wyzwania**

Z tego wyprowadziliśmy kilka wymiarów które możemy rozpatrzeć by lokalizacja (PoI) była "ciekawa".

* doprecyzowane
* zaskakujące
* dodanie nowych czynności
* dodanie wyzwań

#### 6.1.2. Jak zrobić ciekawe PoI bez mechaniki (ogólnie)

Z warsztatów RPG jakie prowadziłem w obszarze budowania lokacji mamy następujące punkty:

* Co to jest lokacja: 
    * lokalizacje są tłem, nie są ważniejsze niż kto tam żyje (NPC + EVENTY)  
    * lokacje to zbiór czynności i możliwości (rozpadająca się świątynia „zabezpiecz by na łeb nie spadło”)
* Lokacja ma sens, gdy ma to dla graczy znaczenie
    * Nie każda lokacja musi być epicka. 
        * Lokacja zerowa, np: "karczma", "ciemna uliczka". Czyli nieciekawe lokacje.
        * Lokacja zerowa „karczma” i „ciemna uliczka” mają odmienne akcje, mimo że są "nudnymi" lokacjami (zerowymi)
    * Lokacja zapewnia NPCów i przedmioty (szpital ma apteczkę i lekarzy)
    * Lokacja nakręca historię – „nienawistna ósemka” niemożliwa w innym miejscu
    * Lokacja pomaga w zarządzaniu złożonością (wszyscy WIEDZĄ co jest w szpitalu)
    * By optymalizować czas i lokacje się dobrze zapamiętywały MUSISZ MÓWIĆ O RZECZACH CO SIĘ RÓŻNIĄ od typowej / zerowej.
* Demonstracja lokacji
    * Teren był niebezpieczny, ale nie jest to kłopot aż pojawia się presja
    * Różne afordancje i akcje – uszkodzony teren umożliwia użycie tego terenu do spowolnienia wroga
    * Mniej chodzi o opis bardziej o akcje
    * feel „gdzie jesteś” – odrębność, pokazanie fragmentu świata
    * dziedziczenie cech lokacji – "karczma w wysokich górach" dziedziczy część cech "wysokich gór". Itp.

Demonstracja dziedziczenia cech lokacji:

![Lokacje dziedziczą cechy po lokacjach](Materials/230923/02-230923-dziedziczenie-cech-lokacji.png)

Czyli, jak zrobić ciekawe PoI:

* w jakim jest **kontekście** (po czym dziedziczy i z czego się składa, jaką ma "rolę" w fikcji)?
    * Netrahina jest **dalekosiężnym statkiem**
        * więc ma więcej zapasów i więcej mechanizmów do samodzielnej naprawy
        * ma mniej luksusów, bo nie ma na nie miejsca
    * Netrahina jest **statkiem badawczym**
        * więc ma więcej naukowców, lokalizacji do składowania rzeczy niebezpiecznych
        * ma więcej zabezpieczeń, śluz itp
    * Netrahina jest **wysłana na dalekosiężną ekspedycję szukającą kosmicznych grzybów**
        * więc jest daleko od domu, nie ma kontaktu z bazą, ma wysoką autonomię...
    * Netrahina jest **starym statkiem, jednym z pierwszych, który wiele razy był przebudowywany**
        * więc jest istotnym bytem historycznie; jest w podręcznikach historii
            * WIĘC wszyscy chcą go uratować i są dumni ze służby na niej
* co unikalność PoI oznacza **z perspektywy czynności i afordancji graczy**?
    * Netrahina jest **statkiem badawczym**
        * więc postacie graczy mogą 
            * dowiedzieć się rzeczy od różnych naukowców w różnych dziedzinach
            * zobaczyć nietypowe próbki i badać własne tematy (pod nadzorem)
    * Netrahina jest **uszkodzona**
        * więc postacie graczy mogą
            * ratować ludzi / wyciągać spod gruzów
            * naprawiać uszkodzenia jednostki, robić spacery kosmiczne...
* co unikalność PoI mówi o **wydarzeniach i populacji PoI**?
    * Netrahina ma kilka grup populacji
        * arystokraci Aurum, którzy są na Orbiterze i dowodzą jednostką
            * trzymają się w kupie, współpracują ze sobą; taka 'klika'
        * puryści Orbitera
            * "tylko czysta ludzka forma i tylko Orbiter"
        * ekspansjoniści Orbitera
            * "musimy zająć wszystko, jak najwięcej, dowiedzieć się, poznać i przejąć"
        * ewolucjoniści
            * "ludzka forma jest nie dość dobra i nie dość silna, zaadaptujmy się"
    * To powyżej generuje niezwykłe źródło konfliktów i kolorytu - mamy np. _oficera naukowego purystę_ oraz jego podwładnych _ewolucjonistów_ i widać już co się będzie działo :-)
* co unikalność PoI mówi o **szerszym świecie**?
    * Netrahina pokazuje, że Orbiter ma co najmniej trzy nurty filozoficzne (puryści, ewolucjoniści, ekspansjoniści)
        * pokazuje też że w momencie awarii, oni współpracują. To nie jest nienawiść, to różnice poglądów
    * Netrahina pokazuje, że kapitanowie mają wysoką autonomię, nawet, jeśli nie pochodzą z Orbitera (bo oficerowie Netrahiny pochodzą z Aurum)
    * Netrahina pokazuje, że istnieją autonomiczne jednostki badawcze dalekiego zasięgu
    * ...

#### 6.1.3. Mikropodsumowanie zanim przejdziemy do naszej sytuacji - "ciekawe PoI"

Czyli, raz jeszcze:

* Ogólnie
    * lokacje są tłem dla osób tam żyjących i eventów
    * lokacje to zbiór czynności i możliwości
* "Ciekawe" PoI się różni od "nieciekawych" tym, że:
    * jest wystarczająco doprecyzowane
    * ma coś zaskakującego i unikalnego
    * ma nowe czynności i afordancje dla postaci graczy
    * ma ciekawe wyzwania
* By zbudować "ciekawe PoI", potrzebujemy odpowiedzi na pytania
    * w jakim **kontekście** jest PoI 
        * po czym dziedziczy i z czego się składa 
        * jaką ma "rolę" w fikcji?
        * jaką ma historię, geografię, ekonomię
        * kto tam żyje i czemu?
    * co unikalność PoI oznacza **z perspektywy czynności i afordancji graczy**?
    * co unikalność PoI mówi o **wydarzeniach i populacji PoI**?
    * co unikalność PoI mówi o **szerszym świecie**?

#### 6.1.4. Jak zrobić ciekawe PoI przy użyciu mechaniki?

Najprościej?

* Odpowiedz na powyższe pytania przy użyciu mechaniki (`V` lub `X` lub `O`)
    * W taki sposób, by maksymalizować afordancje i czynności
    * W taki sposób, by maksymalizować dramaturgię / atrakcyjność / zaskakiwalność

Łatwo się mówi, więc pokażę.

**Najprostszy przykład z minimalnymi elementami**

Mamy Anię i Bartka, oboje są 'homo superior', z rywalizujących rodzin. Dzieciaki rywalizują o to, który jest lepszy. Próbują zdobyć popularność wśród rówieśników i publicznie upokorzyć to drugie.

* wsparcie sesji macierzystej: 
    * sesja jest o tym, jak kłótnie rodów wpływają na dzieci. Trochę jak "Ronja, córka Zbójnika"
* kolizja wizji
    * MG chce, by Ania zdeptała Bartka, bo jest lepsza i bardziej popularna.
    * Gracze chcą, by Bartek wygrał z Anią, ale podał jej dłoń na zgodę. Nie muszą się nienawidzić.
        * Ania może odrzucić rękę Bartka, ale zapamięta, że on chce zgody - zmiana jej nastawienia (Progresja postaci)
* opozycja / przeszkoda: Ania i Bartek. Ale też naprawdę - widmo rodów.
* konflikt aktywny: tak, pole fizyczne i emocjonalne/społeczne
* stawki i zakres konfliktu: SPECJALNIE zostawiam nie zrobione

Dobra. Jedziemy z tym koksem. Naszym celem jest **zrobienie ciekawego PoI**.

* Bartek ma następujące łańcuchy:
    * Bartek wygrywa z nieznaczną przewagą ALE Ania jest zła (p) -> Bartek wygrywa z nieznaczną przewagą ORAZ Ania widzi, że to przez to że była zacietrzewiona (m) -> Ania wyciąga wniosek, że zacietrzewienie rodem szkodzi (e)
    * Ani Bartkowi ani Ani nie stało się nic poważnego podczas zabawy (s)
    * Ania widzi, że Bartek nie jest 'jak inni z rodu' (p) -> Ania zrozumie, że ona może się z nim zakumplować nawet jak tego jeszcze nie robi (m)
    * 3+1+2 = długość maksymalna '6'.
* Robimy konflikt
    * I teraz - gdy pojawia się (`X`), ja **nie muszę** wykupywać rannej Ani czy nienawiści Ani do Bartka. Mogę zamiast tego wykupić rzeczy, które dodadzą atrakcyjności PoI. Przykłady poniżej:
        * (+tło dla osób żyjących) w wyniku rywalizacji, Ania i Bartek coś uszkodzili, bo plac zabaw jest stary i nie jest specjalnie konserwowany
            * (tu od razu powiem "Bartek uszkodził" a gracz zaproponuje "nie, razem uszkodzili, ale Bartek bierze winę na siebie" by podnieść sobie bonusy dalej)
        * (+czynności i możliwości) w wyniku rywalizacji, Cyprian patrzy z pogardą na Bartka - próbuje na SERIO rywalizować z dziewczyną. Cyprian będzie Bartka zwalczał i wspierał Anię.
        * (+coś zaskakującego) trasa którą biegną będzie jutro używana na WFie, ale po stronie trasy Ani są rozbite butelki i szkło MIMO, że ktoś to niby sprzątał. Jeśli Ania wygra to MIMO tego, jeśli przegra to DLATEGO.
        * (+coś unikalnego) boisko ma tor przeszkód robiony przez prawdziwego żołnierza (który jest teraz nauczycielem WFu) - Bartek jest szybszy, ale Ania zwinniejsza, więc ma tu przewagę
        * (+nowe czynności i afordancje) mamy też dziury do mini-golfa, co publiczność nalega by było częścią konkurencji. Ale Ania jest dużo lepsza w mini-golfa niż Bartek.
        * (+ciekawe wyzwania) tor przeszkód przez nauczyciela WFu. Lub "pierwszym krokiem" jest w ogóle dostać się do boiska w nocy - a tam czasem starsze dzieciaki grają hazardowo
            * co też jest nową dodaną czynnością / afordancją "na kiedyś"
        * (+po czym dziedziczy) OD TERAZ wiemy, że szkoła jest bogata i pełna luksusu. Szkoła jest wspierana przez pieniądze rodu Ani. Ale boisko - rodu Bartka. I niezadbane boisko jest sygnałem dla innych że Bartek jest biedny / jego ród nie dba a Ani tak. Czyli "zbutwiały kiedyś-luksus"
        * ...

Widzicie? Krzyżyk może dodać do każdego kroku (pewnych sensowniej, pewnych mniej sensownie). To samo ptaszek. Możecie ingerować nie tylko w swoje postacie a też w rzeczywistość dookoła tych postaci.

#### 6.1.5. To teraz zróbmy te góry gdzie uciekła Celina w ten sposób

Dobrze. Przypomnijmy kontekst:

* **kontekst**
    * Cel zniknął podczas wyprawy wojennej i został uprowadzony
    * Cel uratował się sam ale oddalił od miejsca w którym mniej więcej spodziewają się go znaleźć
    * gracze wiedzieli gdzie zacząć pytać o Cel ale nie wiedzieli gdzie jest. Teraz ten Cel się przemieszcza i gracze nie wiedzą o tym
* **potencjalna przyszłość (default future)**
    * Cel WRÓCI
    * pytanie czy sam czy postacie go znajdą
* **moje dodatki umieszczające pytanie w kontekście, tylko minimum**
    * postacie graczy chcą pozyskać fundatorów i pokazać się jako skuteczni eksploratorzy
        * wyprawa ratunkowa Graczy ma za zadanie znaleźć Celinę i doprowadzić ją do domu
        * wyprawa ma więc pokazać ich przedsiębiorczość i zapewnić im środki na przyszłość
    * wizja MG:
        * postacie graczy nie poradzą sobie z groźnymi górami i będą musiały być uratowane
        * postacie graczy uratowane a Celina w domu -> pośmiewisko, kwestionowanie kompetencji
        * do tego jeszcze utracone zasoby, śmierć wartościowych lokalsów, ogólnie - kaplica
    * wizja Graczy:
        * postacie graczy dotarły do Celiny i ją uratowały, bo wpadła w kłopoty mimo ucieczki
        * do tego, postacie zapewniły sobie fundatorów i dobrodziejów oraz finansowanie
        * postacie są popularne, bo uratowały Celinę

Ale najpierw zacznijmy od drugiej strony: po kosztach. Bo wiecie, nie wiem zbyt dużo o górach, problemach górskich, potencjalnych "krzyżykach górskich" czy ekspedycjach ratunkowych w górach.

#### 6.1.6. ...zróbmy to tanio z niskim przygotowaniem

Dobrze. Używam darmowego ChatGPT3.5. Cel - inspiracja.

![Inspiracja najmniejszym kosztem](Materials/230923/03-230923-gilgit.png)

Tu szukamy potencjalnej historii i kontekstu miejsca:

* Mieszkańcy tych obszarów wierzą w bóstwa gór i prowadzą rytuały ku ich czci. 
* Łowcy potworów oraz alchemicy chętnie przybywają tu w poszukiwaniu unikalnych składników.
* Region obfituje w piękne wodospady, są miejscem pielgrzymek i rytuałów religijnych.
* Góry te słyną z rozległych jaskiń, w których można znaleźć rzadkie i magiczne kryształy.
* niebezpieczne przełęcze i wąwozy
* bandyci i kultyści

Mamy też jakieś sugestie jakiejś populacji:

* bandyci i kultyści, o których nic nie wiemy (ale mogę dogenerować)
* Twardzi górnicy górscy ("krasnoludy"), modelowałbym po mieszkańcach Appallachów w USA; te oddalone od cywilizacji miasteczka górnicze. Oni będą wysyłali karawany górskie z kruszcem i na to polują bandyci.
* Prosperujące miasteczka w dolinach; korzystają ze złota i kruszców z gór, stanowiąc ośrodek handlu oraz przesyłając to, czego górnicze miasteczka nie zmontują same. ("elfy")
* Koczownicy, którzy żyją jak górscy nomadzi, poza "cywilizacją". Mają własną kulturę i żyją raczej sami.

Dobrze. Nie potrzeba mi wiele więcej. Potencjalne krzyżyki mające dodać aspekty do gór i zmienić je w ciekawe PoI:

* Ogólnie
    * lokacje są tłem dla osób tam żyjących i eventów
        * X: by zdobyć informacje o lokalizacji Celiny i zaskarbić współpracę koczowników, musieli oddać im sporo zasobów (status: kosztowna wyprawa)
        * X: stracili bardzo dużo czasu przez to, że manewrowali między grupami bandytów i koczowników (status: zmarnowany czas, który w coś przełożę kolejnymi krzyżykami)
    * lokacje to zbiór czynności i możliwości
        * X: okazja: ranny koczownik. Ale jak postacie mu pomagają, wyłania się karawana na którą jego ludzie napadli. Komu postacie podpadają - karawanie czy koczownikom?
        * X: ślady Celiny prowadziły do krystalicznej jaskini. Ale to była przez nią zastawiona pułapka - krystaliczne potwory tam żyjące pokiereszowały członków ekspedycji
* "Ciekawe" PoI się różni od "nieciekawych" tym, że:
    * jest wystarczająco doprecyzowane
        * X: zdradliwy i okrutny teren sprawił, że mimo przygotowania musieli wejść w sojusz z grupką lokalnych bandytów - za część sprzętu zostali przeprowadzeni (co spowodowało katastrofalne dyplomatyczne konsekwencje z górniczym miasteczkiem)
        * X: Gorące Szczyty mają unikalne rośliny i piękną wodę. Okazuje się, że to przez wyziewy siarki. Straty w ludziach ekspedycji. Nie znając terenu - gaśniesz i umierasz.
        * X: Gracze targowali się solidnie z miasteczkiem górskim, dostali nie 3x więcej a 10x więcej - by na dole okazało się, że zostali oszukani i to jest nic nie warty piryt a nie złoto (pośmiewisko jeśli to zaniosą)
    * ma coś zaskakującego i unikalnego
        * X: Gracze pozyskali przeklęte figurki lokalnego kultu bogów gór. ONI nie wzięli, ale członkowie ekspedycji podwędzili. I potem wszyscy są przeklęci
        * X: Z uwagi na kult bogów gór, gracze musieli przejść przez Kryształową Jaskinię by ktoś chciał im pomóc (bo Celina przeszła). Wyszli poranieni na ciele i duszy. (status: "w tych jaskiniach jest coś bardzo nie w porządku")

...

Zauważcie, że każdy z krzyżyków powyżej:

* ALBO generował nowy fakt o górach
* ALBO ustawiał relacje postaci do gór / tubylców (co robią, komu podpadną, kto ich nie lubi)

Widzicie? 

* **Jakikolwiek** zaczyn / starter, nawet historia prawdziwego Gilgita
* Większy kontekst i jak to wspiera większą sesję
* Nie banie się naciskania na graczy i ich postacie

I nagle macie konflikty GENERUJĄCE poszczególne PoI.

#### 6.2. Jak zrobić, by konflikt dał nam ciekawe ramy fabularne na potem

Podobnie jak z PoI. Po prostu zamiast w PoI, inwestujesz krzyżyki w przeciwników, inne agendy, dogenerowujesz sceny / sytuacje / ruchy przeciwników...
