# Analiza: Jak konstruowałem sesje na przestrzeni 4 lat? (240115)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Analiza jak można budować sesje, jakie strategie działały, nie działały i w jaki sposób
* Wstęp do konstrukcji 'w ten sposób przygotowuj sesje w EZ, człowiek'. W końcu chcemy to wydać...

---
## 2. Spis treści

1. Przejście przez ciąg przeszłych konceptów
    1. Przykład '180701-dwa-rejsy-w-potrzebie'
        1. (dla każdego przykładu: {
        2. Fragment / wycinek fazy projektowania
        3. Jakich technik użyłem i dlaczego
        4. Komentarz ogólny
        5. })
    2. Przykład '180730-prasyrena-z-zemsty, 180808-kultystka-z-milosci'
    3. Przykład '180912-inkarnata-nienawisci'
    4. Przykład '181114-neutralizacja-artylerii-koszmarow'
    5. Przykład '181118-romuald-i-julia'
    6. Przykład '190113-chronmy-karoline-przed-uczniami'
    7. Przykład '190217-chevaleresse'
    8. Przykład '190330-polowanie-na-ataienne'
    9. Przykład '190928-ostatnia-misja-tarna'
    10. Przykład '191201-ukradziony-entropik'
    11. Przykład '191218-kijara-corka-szotaron'
    12. Przykład '200909-arystokratka-w-ladowni-na-swinie'
    13. Przykład '201021-noktianie-rodu-arlacz'
    14. Przykład '201210-pocalunek-aspirii'
    15. Przykład '210324-lustrzane-odbicie-eleny'
    16. Przykład '210519-osiemnascie-oczu'
    17. Przykład
    18. Przykład
    19. Przykład
    20. Przykład





2. Masterlista technik
    1. Hasła Uruchamiające
    2. Pytania Generacyjne
    3. O co gramy - jawne stawki
    4. Pogoda i kontekst
    5. Kim są gracze - pozycjonowanie
    6. Pozycjonowanie Domyślne
    7. Theme & Vision: Piosenka Wiodąca
    8. Literacka Struktura Sesji
    9. Trigger: Sin
    10. Motywacja przez Potrzebę
    11. Dark Future
    12. Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy
    13. Tor postępu / Chain
    14. Wizja Sceny: Scena, Niestabilność, Opozycja, Trigger
    15. Mroczna Agenda: Rola
    16. Pierwsza Scena Zawiązująca
    17. Dark Past into Dark Future
    18. Emergentne Działania Agentów
    19. Wykupowanie Torów za Punkty
    20. Definicja kierunku uczuciowego
    21. Scena Zero budująca Kontekst
    22. Archetypy / Role Postaci
    23. Handouty z Kontekstem
    24. Rozpięcie linku Gracz - Postać
    25. Mroczna Agenda: Aktor / Frakcja
    26. Ekstrakcja pomysłu: Film / Książka / Komiks -> Scena, Wizja, Postać
    27. Sesja musi być o czymś
    28. Default Dark Future: Licznik Niegodziwości
    29. Wizja, Strategia, Taktyka: nad czym gracz ma mieć kontrolę?
    30. Umocowanie postaci i drama międzyludzka


---
## 3. Przejście przez ciąg przeszłych konceptów
### 3.1. Przykład '180701-dwa-rejsy-w-potrzebie'
#### 3.1.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) Statki Rejsowe po Nieskończonym Eterze
* Ż: (element niepasujący) Walki Gladiatorów
* Ż: (trigger) "Miłość to druga strona nienawiści" - obudzenie potwora
* Ż: (przeszłość, kontekst) Władyka statku odszedł w niełasce - nie pozwolono mu przekazać wiedzy

### Analiza i poszerzenie

Ogólnie:

* Ż: Czemu Kalina jest jedyną ze swego oddziału terminuską eskortującą statek po podróży po Bałtyku a potem Eterze Nieskończonym?
* K: Rozdzielili się by dobrze zarobić na oddział.
* Ż: Jaka ekonomicznie jest funkcja tego statku?
* K: Transportowiec. Transportuje... COŚ co wymaga takiej formy transportu.
* Ż: Kogo (ze wzajemnością) polubiła Kalina na tym statku? Jaką pełni funkcję?
* K: Załogant. Mechanik. On. Kacper, twardy jak podeszwa.

Gramy o (zdania):

1. "Statek ochraniany przez Kalinę zostanie zniszczony i nie uda się nikogo uratować."
2. "Statek ratujący Kalinę zostanie zniszczony i większość ludzi i większość magów zginie."
3. "Statek ratujący Kalinę zostanie uratowany przez odpowiednik handlarzy niewolników."
4. "Kalina nie zostanie obciążona winą za całość."

Pogoda, otoczenie:

* Morze albo Nieskończony Eter. Na statkach.
* Wpierw transportowiec taki... roboczy, zwykłej klasy.
* Potem luksusowy statek rejsowy Luxuritias.
* Luxuritias nie ma doświadczonych oficerów na pokładzie.

Kim są gracze:

* Kalina, terminuska mająca zadanie uratować sytuację.
```

#### 3.1.2. Jakich technik użyłem

* Hasła i Uruchomienie
* Pytania Generacyjne
* O co gramy - zdania określające stawki
* Pogoda i kontekst
* Kim są gracze - pozycjonowanie

#### 3.1.3. Komentarz

To były czasy - zaczynamy nowy system, niewiele wiemy, wiemy, że dopiero konstruujemy system razem i zobaczymy, gdzie to wszystko trafi. Skupiamy się na poszerzeniu możliwości i robieniu sesji ‘wspólnie eksplorując stożek tego co daje nam system’.

Uruchomienie daje nam wspólny kontekst sesji (z czego oczywiście Trigger nie jest dla Kić jawny, ale pozostałe elementy Uruchomienia pojawiają się bardzo szybko). Pytanie generatywne oraz ‘o co gramy’ dają nam wspólny kontekst sesji. Pogoda i otoczenie oraz pozycjonowanie postaci pokazuje nam wspólne klocki startowe.

A potem po prostu gramy. Wszystko wyjdzie w praniu. Ja powołuję elementy, ona powołuje elementy (acz ona mechaniką). 


### 3.2. Przykład '180730-prasyrena-z-zemsty, 180808-kultystka-z-milosci'
#### 3.2.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji

### Struktura sesji: Monster in the House

* Sin: Lust. Pożądanie, wobec płci pięknej. Budowanie ekosystemu nadużywającego władzę. Z drugiej strony: miłość do siostry.
    * Who: popularny chłopak, "tak się powinno robić".
* Monster: Siren, who is hunting them.
    * Ruin: po przekroczeniu masy krytycznej, masowe zabójstwo. Przedtem, sexual drain.
* House: Stach nie opuści miejsca, bo to wyzwanie. Kacper, bo chce zdominować i opętać Syrenę.
* Players:
    * K: zawołana przez Stacha jako terminuska.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) Wieloświatowiec Kacper powrócił z Wysp Ochoczych i ma ze sobą coś nowego co może pomóc jego ekipie.
* Ż: (element niepasujący) Stach ściągnął Kalinę, bo nie podoba mu się bardzo to, że jego remedia mają tu ogromne branie. Coś mu nie gra.
* Ż: (trigger) Małgorzata Dotknięta przez Kasandrę.
* Ż: (przeszłość, kontekst) Małgorzata jest uśpionym viciniusem który się obudził.

### Analiza i poszerzenie

Ogólnie:

* Ż: null
* K: null

Gramy o (zdania):

1. "Ktoś (nie wiem jeszcze kto) wpadnie w sidła Kręgu Ośmiornicy"
2. "Dojdzie do serii samobójstw wzmacniających potwora"
3. "Potwór odleci gdzieś poza ten teren bezpiecznie"

Pogoda, otoczenie:

* Większe miasto (~100k ludzi), o mniejszym progu Skażenia i ogólnie przez to spokojniejsze
* Zwykle sesja ma miejsce w nocach; jedna scena to jedna noc.

```

#### 3.2.2. Jakich technik użyłem

Znane:

* Hasła i Uruchomienie
* O co gramy - zdania określające stawki
* Pogoda i kontekst

Nowe:

* Pozycjonowanie Domyślne
* Theme & Vision: Piosenka Wiodąca
* Literacka Struktura Sesji
* Trigger: Sin

#### 3.2.3. Komentarz

Tu już mamy ustabilizowany troszeczkę świat; mam konkretny pomysł na sesję i mamy konkretną ochotę na coś ciemniejszego. Z uwagi na Uruchomienie i kontekst sesji nie jesteśmy w stanie wykorzystać pytań generatywnych sensownie. Generacja jest moją odpowiedzialnością.

Ciekawostka - pierwszy raz pojawia się piosenka wiodąca w formie praktycznie jawnej. Jeszcze tu jej nie wprowadzam jako technikę, ale tu po raz pierwszy się pojawia (w tej iteracji systemu).

Popatrz na fragment konstrukcji sesji: 

```
* Monster: Siren, who is hunting them.
    * Ruin: po przekroczeniu masy krytycznej, masowe zabójstwo. Przedtem, sexual drain.
```

A teraz popatrz na tekst piosenki „Blutengel – The Siren”:

```
* on: Why don't you stay away?
* ona: I am the siren who is hunting you
* on: I don't belong to you
* ona: I sing my song of temptation to you
```

Ta piosenka w pełni oddaje motywację przeciwnika oraz w pełni pokazuje w jaki sposób prowadzić tą sesję. Zawiera wymianę pomiędzy dwoma stronami, wieczne starcie. W pewien sposób **piosenka pokazuje strukturę prowadzenia sesji**, dynamikę między postaciami oraz jak się opowieść skończy (czyli przyszłe Dark Future).

Pozycjonowanie na tej sesji było Domyślne – wiemy, że Kić gra terminuską i w przeszłości spotkała postać Stacha. Więc Stach mógł poprosić ją o pomoc i Kić nie odmówi.

Zwrócę uwagę na strukturę sesji – Monster in the House jest techniką wziętą z „Save the Cat”, bardzo znanej książki o budowaniu scenariuszy, która daje jawną strukturę. Przez to, że musiałem jawnie odpowiedzieć na te pytania - jaki Sin doprowadził do Triggera, kto to zrobił, co jest potworem, jak wygląda Ruina - miałem mniej możliwości się pomylić i coś spieprzyć.

---
### 3.3. Przykład '180912-inkarnata-nienawisci'
#### 3.3.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji
### Struktura sesji: Monster in the House

* **Czego brakuje przeciwnikowi?**
    * Being x Affection: respect, sense of humor...
    * Brygidę zostawił nawet Wąsacz policjant. Została całkowicie sama. Niekochana, niezaproszona.
* **Jaką ruinę sprowadza potwór?**
    * Having x Affection: friendship, family...
    * Inkarnata odbiera te uczucia i zastępuje je bólem i gniewem.
* **Czym przeciwnik zaleczył ból i jak to ściągnęło potwora?**
    * Brygida poszła na Trzęsawisko Zjawosztup.
    * Tam jej nienawiść, ból i niezgoda stworzyły Inkarnatę.
* **Kim jest przeciwnik i kim jest potwór?**
    * Brygida, dziewczyna do towarzystwa: jest Przeciwnikiem
    * Inkarnata, upiór pożerający ciepło i zastępujący je gniewem jest Potworem
* **Czemu gracze muszą w to wejść?**
    * Atena jest zbyt niebezpieczna i zbyt mało zna ten świat by zaufać jej przy Zjawosztup
* **Trigger?**
    * Atena pojawiła się w okolicy Zaczęstwa i to wykryła

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) Zaczęstwo, teren Skażony
* Ż: (element niepasujący) Atena w Zaczęstwie - przybyła posprzątać osobiście
* Ż: (trigger) Alarmy Pięknotki
* Ż: (przeszłość, kontekst) Atena ma kłopoty

### Dark Future

1. Brygida stanie się viciniusem, opętanym nienawiścią i bólem
2. Atena przejmie Brygidę jako swoją niewolnicę
3. Inkarnata zniszczy więź między dziećmi i rodzicami w Cyberszkole

Pogoda, otoczenie:

* Ładna pogoda, tereny raczej wiejskie, z pagórkami i górkami.
* Niedaleko jest niemożliwe do usunięcia trzęsawisko Zjawosztup.
* Sam Zjawosztup to Trudny Teren, na którym są endemiczne Echa i problemy.

## Potencjalne pytania historyczne

* brak
```

#### 3.3.2. Jakich technik użyłem

Znane:

* Hasła i Uruchomienie
* Literacka Struktura Sesji
* Pozycjonowanie (przez 'czemu gracze muszą w to wejść')

Nowe: 

* Motywacja przez Potrzebę
* Dark Future

#### 3.3.3. Komentarz

Coraz bardziej oddalam się od czystych struktur literackich; tutaj bardzo przekształciłem Monster in the House w pewien sposób budując stan emocjonalny postaci na sesji. To, co w przyszłości robię muzyką i motive-split tym razem zrobiłem przez rozpisanie historii.

Chcę zwrócić uwagę na wykorzystanie Piramidy Masłowa – „Being x Affection” oraz „Having x Affection”. Pierwszy raz skupiam się na prawdziwych potrzebach drugiej strony (w ramach tego świata) i po raz pierwszy mam mechanizm strukturalny który pomaga mi to prowadzić.

Z uwagi na to, że sesja jest zainicjowana przez NPC z zewnątrz (Atenę), musiałem rozdzielić sytuację na Trigger oraz „czemu gracze muszą w to wejść” (pozycjonowanie)

Pierwsze zastosowanie Dark Future jako enumeracji przyszłości.

---
### 3.4. Przykład '181114-neutralizacja-artylerii-koszmarow'
#### 3.4.1. Fragment / wycinek fazy projektowania

'181114-neutralizacja-artylerii-koszmarow'

```md
## Projektowanie sesji
### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * strona: Wiktor Satarail, uzyskujący morderczą broń
    * potrzeba: powstrzymać ciągłe ataki na Trzęsawisko
    * sukces: ma ciężką broń - Upiorną Skrzynię
    * porażka: niczego nie osiągnął, musi naładować
    * sposób: osobiste działanie z bioformami
    * silnik: 1k3 / 3, skok na 3
    * Tor:
        * 3: Wiktor naładował artefakt (vol 1) na Nieużytkach
        * 6: Wiktor zaatakował okoliczne tereny przy użyciu bioform i zalał teren siłami bojowymi
        * 9: Wiktor wycofał się na Trzęsawisko z artefaktem
        * 12: Wiktor jest na wzmocnionej pozycji na Trzęsawisku
        * 15: Wiktor ma uzbrojoną broń artyleryjską
        * 18: Wiktor Satarail jest bezpieczny; nie da się doń dostać

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) 
* Ż: (przeszłość, kontekst) 

### Dark Future

1. Na Trzęsawisku powstanie śmiertelnie niebezpieczna broń artyleryjska, zagrażająca Zaczęstwu i awianom
```

#### 3.4.2. Jakich technik użyłem

Znane:

* Struktura Sesji Literacka (zmodyfikowana)
* Dark Future

Nowe:

* Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy
* Tor postępu / Chain

#### 3.4.3. Komentarz

Pierwszy raz powstaje coś co nazywam w przyszłości ‘metastrukturą’ - miejscem projektowania, w którym mam wszystkie potrzebne informacje o sesji i bytach nie będących agentami (aż w tamtym momencie agenci także wchodzili w ten zakres).

Sposób automatycznego przydzielania punktów do Torów / Łańcuchów bardzo demotywował graczy przed wykonywaniem konfliktów. Każdy konflikt miał ogromne znaczenie. To sprawiało, że w pewnym momencie zacząłem automatycznie przesuwać łańcuchy zgodnie z czasem rzeczywistym - zbliżaliśmy się do Endgame. Powodowało to piski protestu, ale je ignorowałam. To była dobra decyzja.

---
### 3.5. Przykład '181118-romuald-i-julia'
#### 3.5.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji
### Struktura sesji: Eksploracja

* **Scena, Niestabilność, Opozycja, Trigger**
    * Romuald prosi ekipę o uratowanie Julii która samotnie poszła do czarnego sektora. Ona poszła tam z nadzieją spokoju...
    * Romuald chce znaleźć Julię, ona zrobi wszystko by się nie dać.
    * Czarny Sektor (efemeryda), Julia która się chowa
    * Pięknotka poproszona bezpośrednio o pomoc
* **Scena, Niestabilność, Opozycja, Trigger**
    * Panorama Światła
    * Strażnicy, Czarne Sektory, fatalna droga z niestabilnościami magicznymi
    * Romuald chce pokazać Pięknotce cudowne miejsce, ale ono wymaga siły fizycznej i umiejętności wspinaczkowej (tor:8)
    * Pięknotka spędza miło czas
* **Scena, Niestabilność, Opozycja, Trigger**
    * Knajpka, Romuald i Julia; Romuald chce poderwać Julię, ona "umiera". Pojedynek z Romualdem.
    * Romuald chce poderwać Julię, ona zrobi wszystko by się nie dać.
    * Niech Romuald nie zostanie totalnie zdewastowany prawnie i reputacjowo
    * Pięknotka oskarżona o to, że otruła Julię
* **Scena, Niestabilność, Opozycja, Trigger**
    * Cezary Zwierz węszy sensację!
    * Cezary chce poznać prawdę o Romualdzie i Julii, Romuald ukrywa co się da
    * Cezary Zwierz robi artykuł dewastujący Romualda
    * Pięknotka chce się pozbyć Cezarego bardziej niż Romualda - będzie zamieszana
```

#### 3.5.2. Jakich technik użyłem

Znane:
Nieznane:

* Wizja Sceny: Scena, Niestabilność, Opozycja, Trigger

#### 3.5.3. Komentarz

To jest zupełnie nowy _strain_, wynikający z tego, że nie mam struktur na sesje eksploracyjne. Dopiero później Kić wymyśliła działającą strukturę.

Ten _strain_ docelowo zmienił się w Fazy Sesji - widać wyraźnie 4 ‘sceny’, które mniej mówią o samej scenie a więcej mówią o warunkach tej sceny: co jest źródłem niestabilności, kto tam jest, jaki jest cel danej sceny i co startuje tą scenę.

Uważam, że ten _strain_ miał dużo większy potencjał niż się wydawało i nie powinien był zaniknąć w tak prosty sposób. Zgodnie z tym podejściem generowało się bardzo prosto i zawsze było wiadomo co należy robić.

Jego duża wada - nie jest powiązana z Vision&Purpose, więc nie było ram w ramach których kontrolowałem sesję. Mogła się rozjeżdżać. Ale np. mając 10 Wizji Scen mógłbym poprowadzić 3-5 sesji w ramach posiadanego stożka możliwości

---
### 3.6. Przykład '190113-chronmy-karoline-przed-uczniami'
#### 3.6.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji
### Pytania sesji

* Czy Pięknotka dotrze do źródła problemu - dziwne specyfiki od Adeli?
* Czy Napoleon Bankierz dostanie wpiernicz od dyrektora za użycie niewłaściwych narkotyków? (chroni Ignacego i Adelę)

### Struktura sesji: Frakcje

* Napoleon: PROTECTOR: bezpieczeństwo Karoliny, bezpieczeństwo Adeli, bezpieczeństwo swoje, znalezienie reszty środków, neutralizacja środków
* Adela: SCIENTIST: bezpieczeństwo swoje, antidotum
* Skażony Perfum: PLAGUE: zainfekować kolejną osobę, wprowadzić w berserk, skrzywdzić maga zła
* Arnulf: PROTECTOR: śledzić Pięknotkę, utrudnić śledztwo formalnie
```

#### 3.6.2. Jakich technik użyłem

Znane:

* O co gramy - jawne stawki
* Tor postępu / Chain

Nowe:

* Mroczna Agenda: Rola

#### 3.6.3. Komentarz

To aż niesamowite, że Dark Agenda pojawiła się już wtedy, w tak atroficznej formie. Ten _strain_ nie żył bardzo długo i szybko zaniknął, ale już wtedy pomagał mi określić co i dlaczego się dzieje. Jest to logiczne rozwinięcie Motywacji Przez Potrzebę, przez ustalenie określonej roli dla postaci lub frakcji.

Pytania sesji, czyli „o co gramy” z jednej strony pokazują kontekst sytuacji a z drugiej strony kierują i MG i graczy w określoną stronę. Czyli to jest ten wektor sesji.

Patrząc na te raporty stwierdzam, że zaniknięcie poprzednich technik było wielką stratą. Te sesje nie były złe, byłem w stanie generować ich dużo więcej, ale nie było w nich tej… ‘wielkości’, która była w sesjach silniej określonych np. z Uruchomieniem.

---
### 3.7. Przykład '190217-chevaleresse'
#### 3.7.1. Fragment / wycinek fazy projektowania

```md
## Projektowanie sesji
### Dark Past

* Alan stworzył gildię Elisquid w multivirt. Ale zabanował go Yyizdath dzięki Pięknotce. Alan stracił kontrolę nad gildią.
* Ktoś inny przejął kontrolę nad Elisquid i prowadzi ją w niewłaściwą stronę. Diana się mu postawiła i przegrała.
* Diana dotarła na Astorię do Alana dowiedzieć się co się stało.
* Starcie na linii Marlena - Diana - Karolina

### Dekompozycja pytaniami

* Czy Alan zostanie odbanowany?
* Czy Alan pozbędzie się (kłopotliwej) Diany?
* Czy Alan i Marlena wejdą w wojnę?
* Czy Alan i Pięknotka wejdą w wojnę?

### Struktura sesji: Frakcje

* Alan: chce odzyskać swoją gildię.
* Alan: chce pozbyć się kłopotliwej Diany.
* Diana: chce odzyskać Alana i sprawić, by on Coś z Tym Zrobił.
* Marlena: nie chce mieć z tym wszystkim nic wspólnego.

### Dark Future

1. Alan nie zostanie odbanowany - przejmie konto Diany.
2. Diana opuści Alana, ale będzie kręcić się koło Zaczęstwa.
3. Alan i Marlena wejdą w wojnę - Marlena doprowadziła do jego zabanowania.

### Pierwsza Scena

Akcja w Cyberszkole - Tymon vs Marlena, Diana i wezwanie Pięknotki
```

#### 3.7.2. Jakich technik użyłem

Znane

* O co gramy - jawne stawki
* Motywacja przez Potrzebę
* Dark Future

Nowe

* Pierwsza Scena Zawiązująca
* Dark Past into Dark Future
* Emergentne Działania Agentów

#### 3.7.3. Komentarz

Można powiedzieć, że w tej sesji pojawiła się nowoczesna EZ i mniej więcej w okolicach tej sesji EZ stało się tym czym prowadzę ją do dzisiaj. To rozgrywka pomiędzy N emergentnymi agentami, określenie o co gramy i skupienie na pierwszej scenie stały się dominującym _strainem_ który prowadzę.

Oczywiście, kierowanie tej sesji nie było szczególnie mocne. Była to sesja silnie ‘character-driven’, trochę się rozłaziła, wobec tego czego szukałem, ale ogólnie była bardzo udana. Ta struktura okazała się być dobra, ale nadal świat był za mało zbudowany; nie mieliśmy bardzo dużych korzyści z gry emergentnej.

To sprawiło, że ten _strain_ nie wygrał. Przeszliśmy na kolejny 2 sesje później.

---
### 3.8. Przykład '190330-polowanie-na-ataienne'
#### 3.8.1. Fragment / wycinek fazy projektowania

```md
## Budowa sesji

Pytania:

1. Czy Chevaleresse zrazi do siebie uczniów Szkoły Magów? (5)
2. Czy Negatech da radę zranić Ataienne? (5)
3. Czy ktoś ma poważne kłopoty za to co się stało? (5)

Wizja i Kontekst:

* Magowie ze szkoły są raczej pacyfistycznie nastawieni, a na pewno anty mind-control
* Szymon ORAZ Chevaleresse widzą w Ataienne więcej niż tylko AI
* Orbiter przebudził technovora by zniszczył Ataienne; wrabia w to Elizę Irę

Sceny:

* Scena 1: Zakłócony koncert Ataienne; Chevaleresse używa broni Alana. Mag Orbitera nieprzytomny.
* Scena 2: Znaleźć Ataienne... szybko
* Scena 3: Uratować Ataienne przed technovorem
```

#### 3.8.2. Jakich technik użyłem

Znane:

* O co gramy - jawne stawki
* Tor postępu / Chain
* Pogoda i kontekst
* Wizja Sceny (uproszczona)
* Dark Future (implicit)

Nowe:

* brak

#### 3.8.3. Komentarz

Bardzo zgrabna i ładna struktura, która generuje bardzo udane sesje i która udowadnia, że jestem skazany na wymyślanie tego samego 15 razy. Ta struktura zyskałaby bardzo dużo, gdyby wykorzystywała Hasła Uruchamiające, ale nie. Nie spojrzę do przeszłości.

Tak czy inaczej, była to sprawna sesja przeprowadzona poprawnie używając posiadanych już technik. Jest to jeden _typ sesji_; tu nie było wiele do kombinowania w tej sytuacji.

Ten _strain_ był wielokrotnie wykorzystywany w przyszłości, zwłaszcza po wydzieleniu torów tak jak poniżej (z '190616-anomalna-serafina'):

```md
### Tory

* Artefakty: drugi - trzeci - ukrycie - neutralizacja - wywiezienie
* Zrzucenie winy: poszlaki - dowody - niewinność
* Szkody: niewielkie - duże - katastrofa
* Zdrowie Zespołu: ranni - ciężko - nieaktywni
* Złapanie Serafiny: uszkodzony sprzęt - ranna - pokonana
```

Jest to bardzo dobry _strain_; warto się mu przyjrzeć jako przykład powodzenia.

---
### 3.9. Przykład '190928-ostatnia-misja-tarna' (bardzo dziwna sytuacja)
#### 3.9.1. Fragment / wycinek fazy projektowania

'190928-ostatnia-misja-tarna', dane z Discorda gdzie to omawiam:

```md
**Kontekst**: 
- zbliżające się warsztaty RPG
- test startu sesji

**Problem**:
- start sesji ustawia PĘD graczom na sesji - musi generować ruch
- start sesji musi graczy KIEROWAĆ - zamknięte konflikty o korzyściach by zachęcać
- nie ma nic nudniejszego niż GADAĆ o świecie, a sesja czasem wymaga kontekstu
- start sesji musi graczom pokazać klimat / nastrój; albo przez kontrast z przyszłą sceną albo wzmocnienie

**Propozycja Rozwiązania**
- scena zerowa, która buduje odpowiednie połączenie emocjonalne graczy z postaciami / historią
- scena zerowa, która prezentuje ten fragment świata który próbujemy graczom pokazać ("opowiedzieć")
- scena zerowa, która buduje pęd graczom i która buduje klimat

**Implementacja Faktyczna**
Sesja 190925 ("Ostatnia misja Sztucznej Inteligencji") będzie mieć nieco inny start:
- Scena Zero: grają Talią (NPC), która próbuje wykopać Tarna (Kolektywny Gracze) i go ukryć. Pytanie --> ile poświęci. Typ: akcja. Znany wynik: sukces. Nieznany koszt.
- Scena Zero: grają Talią (NPC) i Tarnem (Kolektywni Gracze) osiągającymi konkretny sukces. Pytanie --> czy uratują <kogoś/coś>. Typ: akcja. Grają o wynik.
- Sesja Właściwa: grają Tarnem. Ratują Talię. Tarn jest skazany na śmierć (niezmiennik).

**Oczekiwany wynik:**
- Zaczniemy sesję bez gadania o świecie; kontekst powstanie w scenach zero (maksymalny czas: 30 minut na obie)
- Będą mieli relację Talia - Tarn już na starcie; to będą sensowne decyzje
- Piękny kontrast "Tarn niszczyciel światów" a teraz "lol obudzili mnie pijani mafiozi bo chcą zobaczyć jakie mózg w słoiku daje kawały"
- Tutorial mechaniki zrobiony prostymi konfliktami bez ryzyka a nie "randomowym czymśtam"; ośmiela ich to
- Jest pęd i klimat
- Sesja właściwa jest bardzo otwarta ALE MAJĄ KONTEKST - mogę na to sobie pozwolić bo mieli elementy zamknięte + kontekst

**Ryzyka:**
- Ciągła zmiana postaci jest ryzykowna dla graczy lubiących immersję
- Mogą się pogubić mimo kontroli sytuacji
- Niekoniecznie wszyscy gracze poczują się dobrze przy kolektywnej kontroli postaci
```

Oraz opis wyniku:

```md
jak co - zadziałało perfekcyjnie; elementy tego wrzuciłem w warsztat na środę. TL;DR samego podsumowania:

* Ogromna ilość ekspozycji wygenerowana przez akcję. Serio - OGROMNA. Ekspozycja przez akcję forever.
* Gracze mnie pytali o świat. To nie ja im robiłem ekspozycję, to oni mnie pytali.
* Myślałem, że powinienem był zadać pytanie czemu Górnik chce pomóc Talii (jak ona mu pomogła). Ale bez kontekstu w scenie zero by nie zadziałało. Jednak zrobiłem dobrze.
* Gracze którzy na początku chcieli zabić Tarna "bo świat" pod koniec sesji walczyli o niego nawet chipsami
* Gracze wczuli się w Tarna - immersja w mózg w słoiku, 3 graczy 1 postać.
* Gracze byli zachwyceni możliwościami i potęgą Tarna. Mieli naprawdę potężną postać i to czuli - a jednocześnie czuli ból tego "leżę na platformie i mam tylko głośnik"
* Wszyscy gracze weszli w sesję. Przyjęli cele Tarna za swoje. --> sceny zero zbudowały im motywację i pokazały dlaczego warto o to walczyć
* Wszystkie tłumaczenia i trzymanie kontekstu było możliwe dzięki temu, że cały czas rysowałem im mapę w czasie rzeczywistym (diagram połączeń). Gdy coś pokazywałem lub tłumaczyłem lub gdy oni coś mówili, dodawałem ten byt na mapę.

Dominująca technika tej sesji to PĘD. Ani przez moment nie było chwili spokoju - ciągle się działo. Ten pęd wygenerował immersję oraz sprawiał, że przerzucali się między sobą.

Dlatego ta sesja by nie zadziałała dla 5-6 graczy. Nie byłoby pędu.
```

#### 3.9.2. Jakich technik użyłem

Znane:

* Nieistotne.

Nowe:

* Rozpięcie Linku Gracz - Postać

#### 3.9.3. Komentarz

Z perspektywy tego co stało się EZ była to jedna z najważniejszych sesji. Nie tylko gracze nie chcieli grać osobnymi i odrębnymi postaciami, wczuli się w co wczuć się ‘nie da’. Ta sesja i późniejsza ‘Kijara’ są jednymi z kluczowych sesji generujących to, co stało się EZ: od Sceny Zero demonstrującej ważną sprawę, przez zmiany punktów widzenia, przez prowadzenie filmowe aż do przesunięciu mnie do myślenia 'opowieści o CZYMŚ są ważne'.

Mimo, że ten _strain_ długo był nieaktywny, ten _strain_ fundamentalnie wygrał.

---
### 3.10. Przykład '191201-ukradziony-entropik'
#### 3.10.1. Fragment / wycinek fazy projektowania

```md
## Budowa sesji
### Stan aktualny

* Kompleks Itaran jest bezpiecznie ukryty niedaleko Jastrząbca
* W Kompleksie prowadzone są badania i próby ratowania ludzi używając Black Technology
* Jednym z pacjentów a jednocześnie strażnikiem interesów Grzymościa jest niestabilny Melwin Hlagor
* Ataienne wykryła z Jastrzębca sygnaturę wskazującą na zaginionego parę miesięcy entropika.
* Damian wysłał miragenta, by ten rozwiązał problem. Pięknotka jako wsparcie dla miragenta. Gdzie jest cholerny entropik?

### Dark Future

* Miragent Keraina wpada pod kontrolę Przeciwnika
* Entropik ginie wraz z Melwinem
* Hestia d'Itaran ginie
* Pacjenci są bezpiecznie ewakuowani

### Pytania

1. Czy Przeciwnik przejmie kontrolę nad Kerainą (miragentem)?
2. Czy uda się ewakuować rannych Grzymościowi?
3. Czy uda się ustabilizować Melwina?

### Dominujące uczucia

* "Zniszczyć to, co powinno być od dawna martwe? Co oni mu zrobili. Jak go uratować."
* "Grzymość, coś ty narobił..."

### Tory

* E1: koszt (3): ewakuacja części pacjentów
* E1: koszt (3): ewakuacja najdroższego sprzętu
* E2: koszt (4): ewakuacja całości pacjentów
* E2: koszt (4): ewakuacja reszty sprzętu
* E3: koszt (5): ewakuacja Entropika
* Z1: koszt (3): zniszczenie części dowodów
* Z2: koszt (3): zniszczenie dowodów
* Z3: koszt (4): zniszczenie śladów
* Z4: koszt (5): destrukcja kompleksu Itaran
* K1: koszt (3): przejęcie Kerainy
* K2: koszt (3): wymiana Kerainy
* D1: koszt (3): dewastacja sił Jastrzębca
* D2: koszt (4): dewastacja Jastrzębca
* O1: koszt (3): obrócenie opinii publicznej za Grzymościem
* O2: koszt (3): obrócenie opinii publicznej przeciw Pustogorowi
```

#### 3.10.2. Jakich technik użyłem

Znane:

* O co gramy - jawne stawki (przez znane Tory ze znanymi kosztami PLUS Pytania)
* Tor postępu / Chain
* Pogoda i kontekst (w formie czystego kontekstu)
* Pozycjonowanie Domyślne (postać gracza kontynuująca kampanię)
* Dark Future

Nowe:

* Wykupowanie Torów za Punkty
* Definicja kierunku uczuciowego

#### 3.10.3. Komentarz

Te sesje po prostu działały - wychodziliśmy od stanu aktualnego, od zbudowania kontekstu danej sesji, przechodziliśmy przez domyślny kontekst (poprzednie sesje), wybieraliśmy kierunek, dodawaliśmy Dark Future i określaliśmy o co gramy - z perspektywy jawnych Torów i co najmniej częściowo jawnych Pytań (nie wszystko mogło być jawne, bo spoilery).

Tym razem nie skupialiśmy się na kierowaniu z perspektywy wizji samej sesji - o czym sesja jest - skupialiśmy się bardziej na uczuciach jakie sesje ma wywołać. Innymi słowy, co gracz ma myśleć podczas sesji i po sesji. 

Działało, choć liczenie punktów do torów i kupowanie punktów wpływów torów było lekką irytującą buchalterią. Wyrwało nas to z immersji; jakkolwiek nie jest to wymiar, który silnie optymalizujemy, ale gdy masz kilka torów i na wszystkie musisz patrzeć i gracz decyduje w którą stronę jak idzie, to idealnie nie było. Z drugiej strony, nadal jest to lepsze niż zarządzanie ekwipunkiem (z mojego doświadczenia).

Nadal uważam, że to wykorzystanie torów było idealne do rzeczy typu ‘bossfight’, ‘HPki’, ograniczone surowce. Pytanie, czy powinno się za punkty kupować przesunięcie toru czy przesuwać tory powinniśmy przy określonych (V) i (X) zostaje nadal otwarte.

Jest powód czemu ten _strain_ przetrwał tak długo. I nie tylko dlatego że bardziej skupiliśmy się na stabilizacji mechaniki ;-).

---
### 3.11. Przykład '191218-kijara-corka-szotaron'
#### 3.11.1. Fragment / wycinek fazy projektowania

```md
## Uczucie sesji

* "Jak to pokonać? Co możemy zrobić?"
* "Co oni zrobili, ci przed nami?!"

## Scena zero

* Jako Persefona d'Szotaron (od teraz: Kijara) zaatakują statek kosmiczny.
* -> Pokazać im siłę Kijary, wszystko jest proste, są potworem z horroru
* --> będą widzieli moc tego z czym walczą

## Archetypy Postaci

* As Pilotażu
* Szaman
* Majster

## Tory (wyciągnięte z Worda)

```
![Kijara, tory](Materials/240116/240116-001-tory-kijara.png)	
```md
## Materiały wspomagające (wyciągnięte z Worda)
```
![Kijara, assist](Materials/240116/240116-002-tory-aktorzy-terminy.png)	

#### 3.10.2. Jakich technik użyłem

Znane:

* Definicja kierunku uczuciowego
* Theme & Vision: Piosenka Wiodąca ("Neotokio3 - The Red Gauna")

Nowe:

* Scena Zero budująca Kontekst
* Archetypy / Role Postaci
* Handouty z Kontekstem

#### 3.11.3. Komentarz

Ta sesja otworzyła zupełnie nowy _strain_. Tu po raz pierwszy stwierdziłem, że sesje dla randomów i sesje dla stałych zespołów muszą być robione zupełnie inaczej. 

Jest tu silna degeneracja z perspektywy technik (typowe przy nowych _strainach_) - nie mam wysokopoziomowego kierunku; mam cel w tej sesji (uratować kogoś tam ze statku), ale nie jest on dokładnie rozpisany. Wiedziałem mniej więcej co sesja ma osiągnąć, wiedziałem mniej więcej w jakim kontekście się znajdujemy, ale generowałem sesję całkowicie dynamicznie, na podstawie (V) i (X).

Ta sesja wyszła rewelacyjnie, jako horror.

Co tu się stało z perspektywy technik – kombinacja ‘dostarczę wam materiały wspomagające’ oraz ‘to są archetypy którymi będziecie grać’ sprawiła, że każdy gracz mógł zrobić coś przydatnego. To zadziałało perfekcyjnie. Co więcej, tu po raz pierwszy wiedząc jakie gracze mają archetypy mogłem przetestować sesję w izolacji - co zrobi gracz posiadający archetyp Szaman w tym punkcie sesji w takiej sytuacji.

Tak więc mimo atrofii większości technik, ten _strain_ jest bardzo wartościowy i ta sesja była innowacyjna z perspektywy EZ.

---
### 3.12. Przykład '200909-arystokratka-w-ladowni-na-swinie'
#### 3.12.1. Fragment / wycinek fazy projektowania

```md
## Inne

Komentarz MG:

* Kolejna sesja, która sama wyszła z mechaniki i której nie przewidziałem.
* Powstała mi Anastazja i zupełnie zmieniła się możliwość powiązań:
  * nagle na pokładzie jest Izabela (na stałe?)
  * nagle na miesiąc jest Anastazja, co zmienia możliwości Arianny w Trzecim Raju

Planowana sesja:

* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
* Zespół troszkę im tam pomaga, wykrywa ??????? ?? ???????? ???????.
* Ok, niezależnie od okoliczności - świnie na statek. Lecimy na orbitę.
```

#### 3.12.2. Jakich technik użyłem

Znane:

* Emergentne Działania Agentów

#### 3.12.3. Komentarz

Można powiedzieć, że rok 2000 jest silnie pod kątem generacji sesji a nie przygotowywania dobrych sesji. Większość starych dobrych technik całkowicie zaniknęła, za to - idąc za Kacprem, Kić i Fox - skupiłem się na emergentnej generacji sesji z działań różnych postaci i dużo bardziej skupiałem się na budowaniu modelu agentów. Kim jest agent, jakie ma potrzeby, jakie ma działania. Modele emocjonalne, modele osobowościowe…

Dobrze to się odzwierciedla w sesji takiej jak ta załączona - nie ma projektu sesji. Sesja ‘dzieje się sama’ na podstawie ruchów postaci oraz innych agentów na sesji.

Szczęśliwie, po opanowaniu technik generacji emergentnych agentów ten ¬_strain_ trafił tam, gdzie powinien – zaniknął. Bo jakkolwiek generował dobre sesje niewielkim kosztem, ale te sesje były mniej ciekawe niż np. dobre filmy. A np. sesje z komodorem Bladawirem - jakkolwiek dużo bardziej kosztowne czasowo i energetycznie – są dużo lepsze niż cokolwiek Hollywood dzisiaj produkuje.

---
### 3.13. Przykład '201021-noktianie-rodu-arlacz'
#### 3.13.1. Fragment / wycinek fazy projektowania

```md
## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Stan aktualny sytuacji:

* **AMEND: dodajmy Iok-sama, szlachcica który z oddziałem kilkudziesięcioosobowym spróbuje uratować Trzeci Raj przed Elizą Irą.** -> do przyszłej sesji.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* (...)
```

#### 3.13.2. Jakich technik użyłem

Znane:

* Emergentne Działania Agentów
* Dark Past into Dark Future

Nowe:

* Mroczna Agenda: Aktor / Frakcja

#### 3.13.3. Komentarz

Wróciliśmy do konstrukcji sesji - to wynika zarówno ze złożoności sesji, które znowu zaczęliśmy mieć, jak i z tego, że skończyły się główne eksperymenty na sesjach pod kątem warsztatu RPG.

Dalej widać wpływ działań emergentnych; sesje już nigdy nie wróciły do bycia ‘niezależnymi od agentów’ jak pierwsze, ale wracamy do integrowania starych technik z technikami nowymi.

---
### 3.14. Przykład '201210-pocalunek-aspirii'
#### 3.14.1. Fragment / wycinek fazy projektowania

```md
### Opis sytuacji

Statek Koloidowy poluje na Ariannę. Nie wie, że Arianna dowodzi Infernią - i nie wie, że Infernia ma zaawansowane sensory.
Arianna próbuje zniszczyć statek koloidowy, zanim on zniszczy ją.
```

Model podstawowy: [Balance of Terror, Star Trek, 1966](https://memory-alpha.fandom.com/wiki/Balance_of_Terror_(episode))

* Spock's sensors detect a moving object, but nothing is visible on the screen. He suggests that the Romulans have some sort of invisibility shield. 
* Sposób działania:
    * Following the Romulan ship's path towards a comet's tail, Kirk orders the Enterprise to jump forward and attack the Romulan ship when its trail becomes visible. 
    * The Romulan commander hopes to double back to intercept the Enterprise, but, on learning that his target is no longer following, orders an evasive maneuver. 
    * Each commander, having failed in his plans, reflects on the other's intelligence. 

Czyli 'dwie łodzie podwodne w kosmosie', z dwoma inteligentnymi kapitanami.

#### 3.14.2. Jakich technik użyłem

Nowe:

* Ekstrakcja pomysłu: Film / Książka / Komiks -> Scena, Wizja, Postać

#### 3.14.3. Komentarz

Absolutnie atroficzne techniki konstrukcji sesji, ale nie miało to znaczenia - sesja musiała się udać, bo materiał źródłowy był tak nieprawdopodobnie mocny oraz z perspektywy graczy polowanie na statek koloidowy było bardzo ciekawym wyzwaniem. Kapitan wrogiej jednostki też był inteligentny i czarujący. A interakcja na pokładzie Inferni była modelowana po Balance of Terror (poza aspektami ksenofobii i rasizmu)

---
### 3.15. Przykład '210324-lustrzane-odbicie-eleny'
#### 3.15.1. Fragment / wycinek fazy projektowania

```md
## Punkt zerowy
### Dark Past

* Elena nie zapomniała, że Arianna i Viorika ukryły przed nią informację o tym, kto stał za tym wszystkim. Wie o Blakenbauerze, nie wie o Michale i Rafale.
* Elena by utrzymać kontrolę nad swoją mocą zwróciła się ku wiktoriacie - potężnemu środkowi Blakenbauerów do stabilizacji. Zadziałało, ale Skaża jej wzór.
* Elena zabrała z magazynów "swoje" lustro i zrobiła z niego swój konstrukt.
* Elena, **zbyt** pewna siebie, skupiła moc na lustrach. Jej moc + lustra + wiktoriata i nienawiść stworzyły krystalicznego potwora kontrolowanego przez nią.
* Elena przekonana że robi właściwą rzecz i to jest to co POWINNA robić skupia się na czyszczeniu Verlenlandu ze zła.

### Opis sytuacji

* https://anime.goodfon.com/download/art-suzuya-alice-in-5712/1920x1360/ <-- Mirror Monster

### Postacie

* Elena: Skażona wiktoriatą, z rozechwianym wzorem i dotknięta Esuriit. Pragnie wyczyścić Verlenland ze zła. "Jesteśmy żołnierzami. My się poświęcamy dla nich wszystkich. Tak ma być".
```

#### 3.15.2. Jakich technik użyłem

Znane:

* Pozycjonowanie Domyślne
* Trigger: Sin (nawet jak nie jest opisany explicit, on tam jest)
* Motywacja przez Potrzebę
* Dark Past into Dark Future
* Definicja kierunku uczuciowego
* Mroczna Agenda: Aktor / Frakcja
* Emergentne Działania Agentów
* Wizja Sceny (atroficzna; walka Elena vs Arianna (skończyło się inaczej))

Nowe:

* Sesja musi być o czymś
* Default Dark Future: Licznik Niegodziwości
* Wizja, Strategia, Taktyka: nad czym gracz ma mieć kontrolę?
* Umocowanie postaci i drama międzyludzka

#### 3.15.3. Komentarz

Kolejna kluczowa sesja. Ta sesja wynikała z tego, że Arianna (postać Fox) nie miała umocowania. Fox nie do końca wiedziała, jak ją prowadzić, kim Arianna jest… więc cofnęliśmy się w czasie żeby zrozumieć te postacie.

Jednocześnie zrobiłem sesję ‘starego stylu’, nie w pełni emergentną. Ta sesja stanowiła spójną opowieść od początku do końca (technicznie, 3 sesje). Dostałem bardzo entuzjastyczną odpowiedź od wszystkich graczy. Co ważniejsze, od tej pory Arianna nigdy już nie była taka sama. Relacja pomiędzy Arianną i Eleną też nigdy już nie były takie same.

Okazało się, że dużo ważniejsze niż sama ‘drama międzyludzka’ jest ‘kierowana drama międzyludzka’, spójne opowieści, które opowiadają o czymś bliskim ludzkiej naturze. Komponent emergentny służy do tego by wygenerować co kto robi, ale wysokopoziomowe sterowanie przez Wizję oraz ‘O czym jest sesja’ budują coś na zupełnie innym poziomie.

---
### 3.16. Przykład '210519-osiemnascie-oczu'
#### 3.16.1. Fragment / wycinek fazy projektowania
#### 3.16.2. Jakich technik użyłem
#### 3.16.3. Komentarz













1. Hasła Uruchamiające
2. Pytania Generacyjne
3. O co gramy - jawne stawki
4. Pogoda i kontekst
5. Kim są gracze - pozycjonowanie
6. Pozycjonowanie Domyślne
7. Theme & Vision: Piosenka Wiodąca
8. Literacka Struktura Sesji
9. Trigger: Sin
10. Motywacja przez Potrzebę
11. Dark Future
12. Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy
13. Tor postępu / Chain
14. Wizja Sceny: Scena, Niestabilność, Opozycja, Trigger
15. Mroczna Agenda: Rola
16. Pierwsza Scena Zawiązująca
17. Dark Past into Dark Future
18. Emergentne Działania Agentów
19. Wykupowanie Torów za Punkty
20. Definicja kierunku uczuciowego
21. Scena Zero budująca Kontekst
22. Archetypy / Role Postaci
23. Handouty z Kontekstem
24. Rozpięcie linku Gracz - Postać
25. Mroczna Agenda: Aktor / Frakcja
26. Ekstrakcja pomysłu: Film / Książka / Komiks -> Scena, Wizja, Postać
27. Sesja musi być o czymś
28. Default Dark Future: Licznik Niegodziwości
29. Wizja, Strategia, Taktyka: nad czym gracz ma mieć kontrolę?
30. Umocowanie postaci i drama międzyludzka



---
### 3.1. XX
#### 3.1.1. Fragment / wycinek fazy projektowania
#### 3.1.2. Jakich technik użyłem
#### 3.1.3. Komentarz

---
### 3.1. XX
#### 3.1.1. Fragment / wycinek fazy projektowania
#### 3.1.2. Jakich technik użyłem
#### 3.1.3. Komentarz



---
## 4. Masterlista technik

---
### 4.1. Hasła Uruchamiające
#### 4.1.1. Przykład

'180701-dwa-rejsy-w-potrzebie'

```md
### Hasła i uruchomienie

* Ż: (kontekst otoczenia) Statki Rejsowe po Nieskończonym Eterze
* Ż: (element niepasujący) Walki Gladiatorów
* Ż: (trigger) "Miłość to druga strona nienawiści" - obudzenie potwora
* Ż: (przeszłość, kontekst) Władyka statku odszedł w niełasce - nie pozwolono mu przekazać wiedzy
```

#### 4.1.2. Komentarz i wyjaśnienie

Source: Nie pamiętam skąd dokładnie mam tą technikę, więc albo z książek albo od Literary Devil. Older Than Dirt dla mnie. Kojarzy mi się mocno z Hero's Journey czy tekstem Pixara jak robić opowieści

```
(Once upon a time there was ___. Every day, ___. One day ___. Because of that, ___. Because of that, ___. Until finally ___.)
```

Kluczem jest podział idei sesji na cztery zasadnicze obszary:

* (kontekst otoczenia)
* (element niepasujący)
* (trigger)
* (przeszłość, kontekst)

Każdy z tych obszarów ma inną funkcję:

* Kontekst otoczenia zarysowuje sytuację - coś ciekawego, coś co od razu gracze zobaczą.
* Element niepasujący jest czymś na co gracze od razu mają zwrócić uwagę i coś co od razu ma być punktem startowym sesji. Coś, czego by się nie spodziewali.
* Trigger jest czymś co destabilizuje sytuację. Sytuacja była stabilna (nic się nie działo lub siły się skutecznie zwalczały) ale coś się stało co sprawia, że sytuacja idzie w kierunku na katastrofę. A niestabilna sytuacja sprawia, że postacie graczy są potrzebne.
* Kontekst i przeszłość pokazują rzeczywistość. Czemu ta sytuacja mogła być niestabilna. Z czego MG i gracze mogą czerpać. Ciekawe wydarzenie. Coś, co warto wiedzieć dla uatrakcyjnienia sesji / zbudowania klocków.

Tak więc ten podział daje dobry zarys sesji - na co gracze mają zwrócić uwagę, jaka jest ogólna sytuacja, co sprawiło że sytuacja się zdestabilizowała i jak wygląda kontekst opowieści.

Samo w sobie nie jest to wystarczające do poprowadzenia sesji, ale jest to dobre **uruchomienie** MG.

---
### 4.2. Pytania Generacyjne
#### 4.2.1. Przykład

'180701-dwa-rejsy-w-potrzebie'

```md
* Ż: Czemu Kalina jest jedyną ze swego oddziału terminuską eskortującą statek po podróży po Bałtyku a potem Eterze Nieskończonym?
* K: Rozdzielili się by dobrze zarobić na oddział.
* Ż: Jaka ekonomicznie jest funkcja tego statku?
* K: Transportowiec. Transportuje... COŚ co wymaga takiej formy transportu.
* Ż: Kogo (ze wzajemnością) polubiła Kalina na tym statku? Jaką pełni funkcję?
* K: Załogant. Mechanik. On. Kacper, twardy jak podeszwa.
```

#### 4.2.2. Komentarz i wyjaśnienie

Pytania generacyjne pełnią 3 istotne role jednocześnie:

* wspólne budowanie rzeczywistości, by wszyscy wiedzieli co jest ważne, na co zwrócić uwagę i jak postacie są ze sobą połączone
* odciążenie MG od niepotrzebnych i skomplikowanych detali
* buy-in ze strony graczy w stronę zarówno opowieści jak i współkreacji świata

W ramach pytań generacyjnych możemy pytać o: 

* rzeczy, które łączą postacie graczy („czemu Ania i Bartek współpracują mimo że…”)
* o połączenie postaci z światem / umocowanie ich („czemu Celina koniecznie chce ratować stację?”, „jak Darek kiedyś pomógł NPC Ewelinie?”) 
* o przydatne fakty jest historii których MG nie chce definiować („dlaczego stacja jest w gorszym stanie niż rok temu - co się zmieniło?”)

Jest to jedna z najskuteczniejszych technik wspólnego budowania zrozumienia sytuacji nieznanej zarówno MG jak i graczom. Jedna z najlepszych technik do jednostrzałów, przekazująca kontekst jak i budująca wspólną wiedzę na jakiś temat. Co więcej, gracze bardzo doceniają możliwość wpływu na świat.

Wadą tej techniki jest to, że gracze podejmują decyzję najmniej wiedząc o świcie i systemie. To sprawia, że im spójniejszy jest świat tym trudniej jest graczom odpowiedzieć w sposób choćby akceptowalnie dobry przy zachowaniu tonu sesji i ‘realizmu’ rozumianego zgodnie z konwencją.

---
### 4.3. O co gramy - zdania określające stawki
#### 4.3.1. Przykład

'180701-dwa-rejsy-w-potrzebie'

```md
Gramy o (zdania):

1. "Statek ochraniany przez Kalinę zostanie zniszczony i nie uda się nikogo uratować."
2. "Statek ratujący Kalinę zostanie zniszczony i większość ludzi i większość magów zginie."
3. "Statek ratujący Kalinę zostanie uratowany przez odpowiednik handlarzy niewolników."
4. "Kalina nie zostanie obciążona winą za całość."
```

'190102-stalker-i-czerwone-myszy'

```md
### Pytania sesji

* Czy Adela zachowa wystarczającą ilość 'normalnych' klientów by przetrwać?
* Czy Adela zwróci się w kierunku mafii?
* Czy Adela zaatakuje Pięknotkę bezpośrednio : WIEMY ŻE NIE
```

#### 4.3.2. Komentarz i wyjaśnienie

Już tutaj budowaliśmy coś, co docelowo stało się różnicą (Dark Future, Light Future). 

Jeżeli gracze wiedzą o jakie stawki toczy się gra, nie tylko mają informacje o tonie sesji, ale też widzą wartość swoich działań. Wiedzą, że MG o coś gra i o coś walczy. To ich silnie ustawia przeciwko działaniom MG i po tej samej stronie. Daje im też Vision&Purpose - w którą stronę idą razem. Możliwość ewaluacji swoich działań „czy ten ruch zbliża mnie do zwalczenia podłych czynów MG”.

Ta konkretna implementacja pokazuje możliwe przyszłości, nie wartościujące ich i nie mówiąc co jest ciemne a co jest jasne. Pokazuje zakres tego o co toczy się gra.

---
### 4.4. Pogoda i kontekst
#### 4.4.1. Przykład

'180701-dwa-rejsy-w-potrzebie'

```md
Pogoda, otoczenie:

* Morze albo Nieskończony Eter. Na statkach.
* Wpierw transportowiec taki... roboczy, zwykłej klasy.
* Potem luksusowy statek rejsowy Luxuritias.
* Luxuritias nie ma doświadczonych oficerów na pokładzie.
```

#### 4.4.2. Komentarz i wyjaśnienie

Po raz kolejny jest to technika filmowa - musisz potrafić zwizualizować sobie w jakiej przestrzeni postacie będą operować. To bowiem pokazuje z czym mają do dyspozycji. Ta technika zakłada, za postacie są w pewien sposób zablokowane w pewnej przestrzeni i nie kieruje Mistrza Gry w żadną stronę. Docelowo ta technika została zastąpiona przez Fazy.

---
### 4.5. Pozycjonowanie graczy
#### 4.5.1. Przykład

'180701-dwa-rejsy-w-potrzebie'

```md
Kim są gracze:

* Kalina, terminuska mająca zadanie uratować sytuację.
```

#### 4.5.2. Komentarz i wyjaśnienie

Pozycjonowanie jest kluczową techniką - prawidłowe pozycjonowanie Graczy sprawia, że od razu mają motywację, chęć i sposób wejścia do sesji („jesteście policjantami na komendzie, było morderstwo, jesteście jak w CSI”).

---
### 4.6. Pozycjonowanie Domyślne
#### 4.6.1. Przykład

'230920-legenda-o-noktianskiej-mafii'

```md
_Ważne nie to co jest napisane a co NIE JEST napisane - nie ma nic o postaciach graczy!!!_

## Plan sesji
### Co się stało i co wiemy

* Dane
    * Kajrat wie, kto stoi za porwaniem Furii - Grzymość
    * Kajrat chce pozyskać maga, w końcu, by odzyskać Aynę
    * KONKLUZJA: porywamy maga

### Co się stanie (what will happen)

* F1: _Dotarcie do Zaczęstwa, do Przytułku Cichych Gwiazd_
    * stakes:
        * Dmitri wpadnie w kłopoty
        * Furie ściągną kłopoty na noktian
        * Grzymościowcy dorwą Furie
```

#### 4.6.2. Komentarz i wyjaśnienie

Czasami masz do czynienia np. z kampanią lub z trzecią sesją z rzędu. Postaci graczy robią to co robią, bo po prostu to robią. Kontynuuję to co robiły wcześniej. Nie istnieje ścieżka, w której zachowają się inaczej - już się zacommitowały na konkretny kierunek.

To jest właśnie pozycjonowanie domyślne. Fox i Kić grają agentkami Kajrata ("Furiami") i to jest trzecia sesja z cyklu, kontynuacja skomplikowanej drogi którą zaczęły dawno temu.

---
### 4.7. Theme & Vision: Piosenka Wiodąca
#### 4.7.1. Przykład

'230831-potwory-ktore-przetrwaly-eter'

```md
* PIOSENKA: 
    * "Delain - Creatures"
        * Our stories all've been told
        * But you survived, you are the night
        * When your back's against the wall, who will catch you when you fall...
    * "Equilibrium - Eternal Destination"
* THEME & VISION:
    * There are nightmares greater than nightmare...
    * Magic always wins
```

Dla sesji w której streszczenie brzmi (zaznaczam pogrubieniem kluczowe komponenty):

```md
Travistas był uszkodzoną jednostką próbującą się ukryć, zapolował na niego 'Śmiech Sępa'. Jednostki przeszły przez Eter i się rozbiły na Neikatis. **Członkowie 'Śmiechu' bez Generatorów Memoriam ulegli strasznej transformacji.** Noktiański Travistas objął 'arkologię' Damnos, post-astoriańskie terrorformy ze 'Śmiechu' zaczęły chronić noktian. **Niestety, COŚ jest w okolicach Damnos; Diana, czarodziejka, uległa i dołączyła do Mroku.** Gdy pojawiły się niedobitki Coruscatis, Terrorformy weszły z nimi w sojusz i wszystkie siły - noktianie z Coruscatis, noktianie z Damnos i resztki terrorformów oddaliły się w poszukiwaniu lepszego jutra. **Diana (corrupted) mentalnie odezwała się do męża, że wróci po niego i go uratuje**...
```

#### 4.7.2. Komentarz i wyjaśnienie

Przykład opisanej sesji, będzie łatwiej.

Sama piosenka niesie za sobą konkretny temat - nieważne jak bardzo nie walczysz, przegrasz wobec Ciemności. Wybrałem sobie cytaty, które pokazują „ich świat się już skończył” oraz „ upadek jest nieuchronny”. To możliwe i zbudowanie odpowiedniej _melodii_ sesji, wiedziałem w jaki sposób prowadzić i jaki zakres uczuć który generuję graczom jest akceptowalny.

W wypadku tej sesji na przykład nie ma dużo miejsca na komedię. Pokazuję sceny „nie jesteśmy tacy jak kiedyś”, „straciliśmy to wszystko” i żetonami entropicznymi oraz komunikację mentalną z Dianą buduję ciągły kontrast pomiędzy „kiedyś byliśmy wolni” lub „Diana była najłagodniejszą z nas wszystkich, uratowała nas wszystkich” i aktualnym stanem - "Diana jest koszmarnym terrorformem pragnącym wszystkich ściągnąć do siebie". I pokazuję graczom, że skoro Diana uległa - oni też ulegną, jeżeli czegoś nie zrobią.

Czyli dzięki tej technice steruje „sobą” - co na sesji mogę wprowadzić – i z perspektywy klimatu i z perspektywy scen – jak i mam wytyczną z perspektywy melodii.

---
### 4.8. Literacka Struktura Sesji
#### 4.8.1. Przykład

'180730-prasyrena-z-zemsty'

```md
* Sin: Lust. Pożądanie, wobec płci pięknej. Budowanie ekosystemu nadużywającego władzę. Z drugiej strony: miłość do siostry.
    * Who: popularny chłopak, "tak się powinno robić".
* Monster: Siren, who is hunting them.
    * Ruin: po przekroczeniu masy krytycznej, masowe zabójstwo. Przedtem, sexual drain.
* House: Stach nie opuści miejsca, bo to wyzwanie. Kacper, bo chce zdominować i opętać Syrenę.
* Players:
    * K: zawołana przez Stacha jako terminuska.
```

#### 4.8.2. Komentarz i wyjaśnienie

Ta technika na 100% pochodzi z książki „Save the Cat”, w której opisana jest seria istniejących struktur opowieści, które można wykorzystać, gdy piszesz książkę czy robisz opowiadanie. Dostarcza nam zbiór potencjalnych struktur, takich jak:

```md
MONSTER IN THE HOUSE

1) A monster that is supernatural in its powers - even if its strength derives from insanity - and 'evil' at its core.
2) A house, meaning an enclosed space that can include a family unit, an entire town, a boat - a place heroes cannot escape
3) A sin. Someone is guilty of bringing the monster in the house... a transgression that can include ignorance.
```

Struktur jest 10 (Monster in the House, Golden Fleece, Out of the Bottle...). Mimo starań, większości nie udało mi się przełożyć na sesje RPG (aczkolwiek "Whydunit", czyli detektywistyczne, tak). Szczęśliwie, książka niesie też za sobą konkretny **SPOSÓB** jak sesja powinna wyglądać - storybeats. Za linkiem [Selfpublishing Save the cat](https://selfpublishing.com/save-the-cat/):

```md
Opening Image - Theme stated - Set up - Catalyst (po naszemu: Trigger) - Debate - B Story - Fun and Games - Midpoint - Bad Guys Close In - All Is Lost - Dark Night of the Soul - Finale - Final Image
```

Kluczem do tej techniki jest to, że dostarcza MG pewnej kolejności tego co się powinno stać i pewnego zawężenia możliwości. Np. ‘jeśli robisz sesję detektywistyczną, nie wprowadzaj losowych absurdów’ czy ‘do horroru to jest zakres emocjonalny którego potrzebujesz’. 

Jednak to zdecydowanie w technika filmowa i uległa wielu przekształceniom zanim dało się jej wykorzystać 1:1 do sprawnych sesji. To co napisałem powyżej jako przykład, czyli kombinacja:

* **Sin**: co jest przyczyną pojawienia się potwora?
    * **Who**: kto doprowadził do Sin i sprowadził Potwora?
* **Monster**: czym jest potwór i czego chce?
    * **Ruin**: w jaki sposób rujnuje świat i/lub postacie?
* **House**: jak wygląda otoczenie?
    * **Why**: czemu nie da się go opuścić
* **Positioning**: kim są?

Jest wystarczające do wykorzystania i długo mi służyło dobrze w jakiejś formie

---
### 4.9. Trigger: Sin
#### 4.9.1. Przykład

'180724-skorpiony-spod-ziemi'

```md
* Sin: Murder (Passion, Neglect)
    * Who: Alicja, zabijając brata (Zdzisława)
```

#### 4.9.2. Komentarz i wyjaśnienie

Na tym etapie już wiemy, za każda sesja opiera się na stanie niestabilnym - jeżeli nikt nic nie zrobi, dojdzie do katastrofy (zaczynam się zastanawiać, czy nie powinienem zrobić z tego techniki). Jednak sytuacja była stabilna aż coś się zmieniło:

* Trzęsienie ziemi spowodowało, że dochodzi do wycieku radioaktywnego z elektrowni
* Duma i niechęć do zejścia z kursu spowodowały zatopienie Titanica

W wypadku tego pierwszego gracze nic nie mogą zrobić. Nie ma ‘lessons learnt’, nie ma wbudowanego w sesję morału. Jest tylko człowiek wobec natury. Jednak w tym drugim wypadku poziom emocjonalny jest wyższy. Zwłaszcza, jeśli widzimy, gdzie to wszystko idzie i wiemy że zakończy się katastrofą z uwagi na to jakie te postacie są (mistrzostwo serialu Arcane - to się nie mogło potoczyć inaczej, nie z tymi postaciami)

To sprawia, że możemy pokazać jaki pierwotny czyn - jaki mroczny aspekt ludzkiej natury - spowodował katastrofę. To jest ów „Sin”.

* Elena mogłaby uniknąć większości swoich problemów, ale przez (Sin::Duma) wpada w coraz większe tarapaty.
* Bladawir mógłby dostać wsparcie którego potrzebuje, ale przez (Sin::Cruelty, Hatred, Paranoia) samotnie przebija się przez mrok stając się częścią mroku.
* Katastrofie w arkologii można by zapobiec, ale przez (Sin:Chciwość) spróbują posunąć się trochę dalej tymi samymi narzędziami; powinny jeszcze wytrzymać.

To jest Sin.

---
### 4.10. Motywacja przez Potrzebę
#### 4.10.1. Przykład

'180912-inkarnata-nienawisci'

```md
* **Czego brakuje przeciwnikowi?**
    * Being x Affection: respect, sense of humor...
    * Brygidę zostawił nawet Wąsacz policjant. Została całkowicie sama. Niekochana, niezaproszona.
```

'190217-chevaleresse'

```md
### Czego chcą strony

* Alan: chce odzyskać swoją gildię.
* Alan: chce pozbyć się kłopotliwej Diany.
* Diana: chce odzyskać Alana i sprawić, by on Coś z Tym Zrobił.
* Marlena: nie chce mieć z tym wszystkim nic wspólnego.
```

#### 4.10.2. Komentarz i wyjaśnienie

Jest to jeden z głównych mechanizmów sterowania frakcjami oraz postaciami. Trzeba pamiętać, że z perspektywy każdej postaci – najlichszego NPC - to on jest bohaterem swojego życia. Więc żeby cokolwiek zrobił musi coś dostać.

Teraz - ktoś kto boi się o jutro i nie wie, jak utrzyma rodzinę będzie silnie skupiony na pieniądzach. Ktoś, kto został upokorzony w pracy będzie próbował odbudować szacunek lub udowodnić sobie i innym, że jest godnym człowiekiem. Ktoś, kto jest uwięziony na obcej planecie będzie próbował rozpaczliwie odbudować swój statek lub może nawet oddać się w niewolę by opuścić to miejsce.
To są potrzeby.

Jeśli rozumiesz jakie potrzeby mają Twoje postacie i frakcje, wiesz, jak działają. Zawsze wiesz jaki zrobić kolejny ruch - w kontekście który widzą zgodnie ze swoimi zasadami i potrzebami.

---
### 4.11. Dark Future
#### 4.11.1. Przykład

```md
### Dark Future

1. Brygida stanie się viciniusem, opętanym nienawiścią i bólem
2. Atena przejmie Brygidę jako swoją niewolnicę
3. Inkarnata zniszczy więź między dziećmi i rodzicami w Cyberszkole
```

#### 4.11.2. Komentarz i wyjaśnienie

Dark Future jest zbudowaniem poziomu Wizji. Co się stanie, jeżeli nikt nic nie zrobi. Jak będzie wyglądała historia, jakby nie było tam graczy. Jest to jeden z najlepszych testów sprawdzających czy obecność graczy w ogóle ma sens.

Zwykle, gdy planuje jakąkolwiek sesję, różnie pojawia mi się pomysł. Czasem to piosenka, czasem to ciekawa postać, czasem to scena. Ale jedną z pierwszych rzeczy które robię to stabilizacja Dark Future - bez niej nie mam pewności czy mam sesję czy opowiadanie.

---
### 4.12. Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy
#### 4.1.1. Przykład

'181114-neutralizacja-artylerii-koszmarow'

```md
* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * strona: Wiktor Satarail, uzyskujący morderczą broń
    * potrzeba: powstrzymać ciągłe ataki na Trzęsawisko
    * sukces: ma ciężką broń - Upiorną Skrzynię
    * porażka: niczego nie osiągnął, musi naładować
    * sposób: osobiste działanie z bioformami
    * silnik: 1k3 / 3, skok na 3
    * Tor:
        * 3: Wiktor naładował artefakt (vol 1) na Nieużytkach
        * 6: Wiktor zaatakował okoliczne tereny przy użyciu bioform i zalał teren siłami bojowymi
        * 9: Wiktor wycofał się na Trzęsawisko z artefaktem
        * 12: Wiktor jest na wzmocnionej pozycji na Trzęsawisku
        * 15: Wiktor ma uzbrojoną broń artyleryjską
        * 18: Wiktor Satarail jest bezpieczny; nie da się doń dostać
```

#### 4.1.2. Komentarz i wyjaśnienie

To jest jedna z pierwszych prób konstrukcji ‘metastruktury’. Jakkolwiek tę wersję osobiście uważam za nieudaną, ale widać w niej to czym się stała:

* Delta Future (Sukces – Porażka)
* Strony i Potrzeby, czyli Agendy
* Sposób działania
* Tory i Silniki (jak wypełniać Tory)

Zostawiam to pro forma, nie dlatego, bo jest najlepsza. Miała swoje miejsce, ale została całkowicie zastąpiona przez lepsze narzędzia.

---
### 4.13. Tor postępu / Chain
#### 4.13.1. Przykład

'181114-neutralizacja-artylerii-koszmarow'

```md
* silnik: 1k3 / 3, skok na 3
* Tor:
    * 3: Wiktor naładował artefakt (vol 1) na Nieużytkach
    * 6: Wiktor zaatakował okoliczne tereny przy użyciu bioform i zalał teren siłami bojowymi
    * 9: Wiktor wycofał się na Trzęsawisko z artefaktem
    * 12: Wiktor jest na wzmocnionej pozycji na Trzęsawisku
    * 15: Wiktor ma uzbrojoną broń artyleryjską
    * 18: Wiktor Satarail jest bezpieczny; nie da się doń dostać
```

'2002-alicja-kontra-esuriit' (docx) lub '200226-strazniczka-przez-lzy' (md); to ta sama sesja

```md
Stan startowy: (1-0-0-1-0-1) (NIE WYKLUCZAM ŻE W PRZENOSZENIU COŚ USZKODZIŁO)

* Alicja-Esuriit: (daleko)(strach)(nie!)(odrzucenie)
* Alicja-Podpięcie: (nie)(trochę)(tak)(kontrola)
* Alicja-Morale: (fajna)(ok)(niewarta)(do kitu)(pustka)
* Lucjusz: (pełnia)(zdrowy)(ranny)(słaby)(out)(Esuriit)	
* Zespół: (zdrowy)(ranny)(ciężko)(shutdown)
* Zdrowie Alicji: (zdrowa)(słabsza)(uszkodzona)(trwale)(nienaturalna)(gasnąca)(umiera)(Esuriit-1)(Esuriit-2)
```

'190616-anomalna-serafina'

```md
### Tory

* Artefakty: drugi - trzeci - ukrycie - neutralizacja - wywiezienie
* Zrzucenie winy: poszlaki - dowody - niewinność
* Szkody: niewielkie - duże - katastrofa
* Zdrowie Zespołu: ranni - ciężko - nieaktywni
* Złapanie Serafiny: uszkodzony sprzęt - ranna - pokonana
```

'240102-zaloga-vishaera-przezyje'

```md
* Delta future
    * DARK FUTURE:
        * chain 1: CORRUPTION: Talikazer i Vishaer zostają zniszczone
        * chain 2: TRUST: Anastazy i Helmut mają złamane morale; "ich trzeba zniszczyć"
        * chain 3: CIOTKA: Miranda nie życzy sobie by cokolwiek mogło się stać Anastazemu
    * LIGHT FUTURE: 
        * chain 1: CORRUPTION: uda się uratować załogę Talikazera i nawet część Vishaera
        * chain 2: TRUST: Anastazy i Helmut rzucają się sobie do gardeł. Fabian jakoś deeskaluje
        * chain 3: CIOTKA: Miranda nic nie wie
* CHAINS
    * Problemy i konflikty
        * (ranny Anastazy i Helmut, uszkodzenie Serbiniusa, Fabian ma problem)
        * (Anastazy i Helmut się żrą, Anastazy, Helmut vs Martyn i Klaudia)
        * (atak potworów, 'dead is alive', silniejszy stres)
        * (contamination)
    * Magia
        * (Klaudia / Martyn Skażeni, nie wszystkich da się uratować, konieczność operacji ratunkowych po Martyna/Klaudię)
        * (Transfer energii do Vishaera)
```

#### 4.13.2. Komentarz i wyjaśnienie

Kilka wariantów tego samego narzędzia. Fundamentalnie, łańcuchy pełnią tą samą rolę co zegary w AW, ale trochę inaczej. Łańcuch z jednej strony jest suwakiem (może iść w lewo i prawo), może być przesunięty z uwagi na konflikt lub może coś odzwierciedlać.

* Najstarszy wariant łańcucha to tor postępu - idzie tylko w jedną stronę, co N akcji graczy przeciwnicy dostają +1 do toru.
* Wariant z Alicją jest wielowymiarową serią łańcuchów idącą dwustronnie. Jak w simsach - pilnujesz by wszystkie mniej więcej jakoś działały.
* Wariant Vishaer łączy Delta Future z łańcuchami oraz pokazuje co w ramach poszczególnych łańcuchów może się spieprzyć.

Nadal nie mamy rozwiązanego problemu łańcuchów, ale te 3 przykłady pokazują jak wszechstronne narzędzie to jest.

Sposób automatycznego przydzielania punktów do Torów / Łańcuchów bardzo demotywował graczy przed wykonywaniem konfliktów. Każdy konflikt miał ogromne znaczenie. To sprawiało, że w pewnym momencie zacząłem automatycznie przesuwać łańcuchy zgodnie z czasem rzeczywistym - zbliżaliśmy się do Endgame. To był dobry ruch.

---
### 4.14. Wizja Sceny: Scena, Niestabilność, Opozycja, Trigger
#### 4.14.1. Przykład

'181118-romuald-i-julia'

```md
## Projektowanie sesji

* **Scena, Niestabilność, Opozycja, Trigger**
    * Romuald prosi ekipę o uratowanie Julii która samotnie poszła do czarnego sektora. Ona poszła tam z nadzieją spokoju...
    * Romuald chce znaleźć Julię, ona zrobi wszystko by się nie dać.
    * Czarny Sektor (efemeryda), Julia która się chowa
    * Pięknotka poproszona bezpośrednio o pomoc
* **Scena, Niestabilność, Opozycja, Trigger**
    * Panorama Światła
    * Strażnicy, Czarne Sektory, fatalna droga z niestabilnościami magicznymi
    * Romuald chce pokazać Pięknotce cudowne miejsce, ale ono wymaga siły fizycznej i umiejętności wspinaczkowej (tor:8)
    * Pięknotka spędza miło czas
* **Scena, Niestabilność, Opozycja, Trigger**
    * Knajpka, Romuald i Julia; Romuald chce poderwać Julię, ona "umiera". Pojedynek z Romualdem.
    * Romuald chce poderwać Julię, ona zrobi wszystko by się nie dać.
    * Niech Romuald nie zostanie totalnie zdewastowany prawnie i reputacjowo
    * Pięknotka oskarżona o to, że otruła Julię
* **Scena, Niestabilność, Opozycja, Trigger**
    * Cezary Zwierz węszy sensację!
    * Cezary chce poznać prawdę o Romualdzie i Julii, Romuald ukrywa co się da
    * Cezary Zwierz robi artykuł dewastujący Romualda
    * Pięknotka chce się pozbyć Cezarego bardziej niż Romualda - będzie zamieszana
```

#### 4.14.2. Komentarz i wyjaśnienie

Mamy do czynienia z 4 scenami, w kolejności sekwencyjnej. Ale potencjalnie nie do końca.

* Pierwsza linijka mówi po co ta scena jest i jaką pełni funkcję, czasami jaki ma kontekst.
* Druga linijka skupia się na źródle niestabilności i przeszkód. Kto czego chce i czemu tego nie może dostać.
* Trzecia linijka pokazuje czasem element Dark Future, czasem opozycję / przeciwnika.
* Czwarta linijka to Trigger.

W szczycie działania tej techniki mogłem mieć 6 takich scen i w zależności od ruchów graczy niektóre się realizowały a niektóre w ogóle nie były w stanie. Pojawiały się nowe, dynamiczne sceny a inne gasły. Na czym polega problem - nie ma spadającego je Vision&Purpose, więc fundamentalnie mamy ‘sceny’ a nie ‘opowieść o czymś’.

Czy to znaczy, że to narzędzie jest do niczego? Nie, da się elegancko połączyć je z bardziej wysokopoziomowymi narzędziami i zbudować ‘sesję probabilitystyczną’. Problem w tym, że potencjalnie nie dostaniemy dobrej opowieści z niepowiązanej serii dobrych scen. Dlatego narzędzie zaniknęło.

Jednak im bardziej prowadzę w kontekście sesji emergentnych, gdzie ta opowieść i tak jest wynikiem działań agendy różnych stron, to narzędzie mogłoby się przydać do przygotowania kluczowych scen. W jakiejś nowocześniejszej formie.

---
### 4.15. Mroczna Agenda: Rola
#### 4.15.1. Przykład

'190113-chronmy-karoline-przed-uczniami'

```md
* Napoleon: PROTECTOR: bezpieczeństwo Karoliny, bezpieczeństwo Adeli, bezpieczeństwo swoje, znalezienie reszty środków, neutralizacja środków
* Adela: SCIENTIST: bezpieczeństwo swoje, antidotum
* Skażony Perfum: PLAGUE: zainfekować kolejną osobę, wprowadzić w berserk, skrzywdzić maga zła
* Arnulf: PROTECTOR: śledzić Pięknotkę, utrudnić śledztwo formalnie
```

'240114-o-seksbotach-i-syntetycznych-intelektach'

```md
* the-inquisitor: Sasza, terminus jest przeciwny Tymonowi, Orbiterowi i samowolce. Ale nie Noctis. Jest przekonany o tym że Talia coś kombinuje ze Strażniczką / AMZ. Udowodni to i znajdzie ślady.
* the-devastator: Ralena, terminuska nienawidząca Noctis. Skupia się przeciwko Talii i jej działaniom. Dąży do destrukcji wszystkiego nad czym Talia pracuje.
* the-distractor: Talia, która udaje, że skupia się na seksbotach i skupia na sobie zainteresowanie i niechęć by tylko zresocjalizować i uratować maksymalną ilość Syntetycznych Intelektów z czasów wojny.
```

#### 4.15.2. Komentarz i wyjaśnienie

_Strain_ który pojawił się kilka razy w przeszłości i całkowicie zaniknął, aż Fox miała bardzo dobrą obserwację (231216):

* W ogóle, podoba mi się, że potwory zyskały teraz tytuły "The xyz". Bardzo fajnie i szybko da się zorientować, o co chodzi
* Sensownie by było w sumie, by stare potwory dostały przydomki w tym stylu (zwykli ludzie/żołnierze tak je kojarzą), a nowe dostały też łacińsko/mistyczne nazwy (tak nazywają je naukowcy, dowódcy, ci co się znają)

W pewien sposób to spowodowało, że zacząłem znowu skupiać się na interfejsach - jaką rolę ma dany byt - a nie na implementacjach - co to jest za byt. Bo znowu – ‘Monster’ z Monster in the House nie jest byle jakim potworem, jest manifestacją zapłaty za czyjś Sin. I teraz: Trianai na Neikatis które są gdzieś tam w oddali nie są tą wersją Monster. Ale Trianai które uwolniły się z laboratorium biologicznego Kidirona bo robił nad nimi badania zdecydowanie pełnią rolę Monster.

Więc jak działa ten _strain_? Nie skupiasz się na tym co znajduje się na sesji a jaką rolę to coś ma pełnić. Za każdą rolą idzie określony zbiór ruchów, zasobów, celów oraz agenda. Docelowo nie wykluczam, że powinien pojawić się też Tor/Chain.

Ten _strain_ już nie zaniknie. Zrobił się zbyt przydatny.

---
### 4.16. Pierwsza Scena Zawiązująca
#### 4.16.1. Przykład

'190217-chevaleresse'

```md
### Pierwsza Scena

Akcja w Cyberszkole - Tymon vs Marlena, Diana i wezwanie Pięknotki
```

#### 4.16.2. Komentarz i wyjaśnienie

Tu w sumie główną innowacją jest to, że absolutnie najważniejsza scena każdej sesji RPG to pierwsza scena - pierwszy punkt interakcji graczy ze światem.

Gdy robisz prezentację, nie poprowadzisz jej zgodnie z tym co masz na slajdach. Nie da się. Ale możesz nauczyć się i zrozumieć dwa pierwsze slajdy początkowe - przedstawienie się i agendę. Potem idziesz z marszu.

To samo tutaj - nie jesteś w stanie kontrolować co zrobią gracze, bo z definicji zrobią co chcą. Ale pierwszą scenę możesz zrobić odpowiednio Zamkniętą, dać graczom jasny silny cel, od razu konflikt, przy którym mają jakiś wpływ i zrobić im dobry sensowny tutorial.

W przyszłości ta technika została nadpisana przez Scenę Zero w wypadku sesji z randomami. Ale powinna być rozwinięta jeszcze w kontekście sesji ze stałymi zespołami.

---
### 4.17. Dark Past to Dark Future
#### 4.17.1. Przykład

```md
### Dark Past

* Alan stworzył gildię Elisquid w multivirt. Ale zabanował go Yyizdath dzięki Pięknotce. Alan stracił kontrolę nad gildią.
* Ktoś inny przejął kontrolę nad Elisquid i prowadzi ją w niewłaściwą stronę. Diana się mu postawiła i przegrała.
* Diana dotarła na Astorię do Alana dowiedzieć się co się stało.
* Starcie na linii Marlena - Diana - Karolina
```

#### 4.17.2. Komentarz i wyjaśnienie

W tej technice chodzi o zbudowanie sytuacji niestabilnej, która sama w sobie dąży do katastrofy. Symulujesz tą sytuację. Określasz Dark Future. A potem cofasz się w czasie, zatrzymujesz się w konkretnym punkcie i znasz już zarówno przeszłość jak i Dark Future.

Ta technika jest zwodniczo prosta. Ogólnie chodzi o to: 

* Masz jakieś postacie i masz jakąś sytuację. 
* Symulujesz ich działania i destabilizujesz sytuację. 
* Jeśli sytuacja nie jest dość niestabilna, modyfikujesz rzeczywistość by była jeszcze bardziej niestabilna.
* Wykonujesz serię iteracji tymi postaciami, żeby zachowywały się wiarygodnie.
* Patrzysz na katastrofę - masz swoje Dark Future, wiarygodne, pochodzące bezpośrednio z ruchów postaci
* Teraz cofasz się w czasie i znajdujesz taki punkt, w którym postacie są w stanie coś zrobić i zauważyć sytuację
    * To jest twój punkt ‘interakcji’ sesji. 
    * To niekoniecznie jest pierwsza scena. Ale tam postacie muszą dojść, by móc zapobiec Dark Future.

Innymi słowy, pierwszy raz mamy do czynienia z symulacją sesji (jak opowiadania) przez zbiór niezależnych agentów.

---
### 4.18. Emergentne Działania Agentów
#### 4.18.1. Przykład

'190217-chevaleresse'

```md
### Czego chcą strony

* Alan: chce odzyskać swoją gildię.
* Alan: chce pozbyć się kłopotliwej Diany.
* Diana: chce odzyskać Alana i sprawić, by on Coś z Tym Zrobił.
* Marlena: nie chce mieć z tym wszystkim nic wspólnego.
```

'231202-protest-przed-kultem-na-szernief'

```md
#### Strony

* Esurient
    * CZEGO: Eksterminacja wszystkich powiązanych arystokratów oraz przekształcenie Aeriny w wendigo
    * JAK: Opętywanie przyszłych morderców, zwiększanie krwawych czynów, szał arystokratów
* Prawdoposzukiwacze
    * KTO: Kalista, Felina...
    * CZEGO: Dojść do tego co się tu naprawdę dzieje i co za tym stoi
    * JAK: Przesłuchania, zbieranie danych, określenie co i jak
* Kult Sprawiedliwości
    * KTO: populacja zwykłych ludzi, naciskanych przez arystokrację
    * CZEGO: "Niech się boją", "niech nie mają nieskończonych praw"
    * JAK: Przypominanie co i jak się stało, ulotki
* Arystokraci
    * KTO: Aerina, Cassian, Tobiasz, Maximus, Livia, Larisa, Anastazja, 
    * CZEGO: Zmienić świat na lepsze, zrobić różnicę, świetnie się bawić przy okazji
    * JAK: Protesty, płomienne mowy, 'my jesteśmy ważniejsi to robimy co uważamy za słuszne'
* Drakolici Mawira
    * CZEGO: Nie dać się wpakować że to oni; serio nie mają powodu
    * JAK: Redukować kontakt z arystokratami ale NADAL sprzedawać im niewłaściwe środki i specyfiki
* Savaranie
    * CZEGO: Robić swoje, robić porządnie, nie dać się wciągnąć ALE chronić stację-dom
```

'231221-pan-skarpetek-i-odratowany-ogrod' (w połączeniu z Łańcuchami)

```md
* Pan Skarpetek: 
    * chain: (umieszcza kwiat, prosi Mawirowców o rekonstrukcję Miejsca, przygotowuje Alteris ogród, pierwsze krwawe ofiary dzieci (składamy krew z ręki i czytamy bajki))
* Kalista: 
    * chain: (dochodzi do sekretów bajek savarańskich, znajduje ludzi Mawira, znajduje miejsce Ogrodu)
* TAI: (NIE alertuje o kwiatach bo nie wie, ALE demonstruje inne problemy)
* Sia (Oszczędnościowcy): (współpracuje z Zespołem; ogród to marnotrawstwo. Oszczędność i redukcja a nie podnoszenie savaran)
    * chain: 
* Aerina (Popularyzatorzy): grajmy na współpracę (Ikta, Teriman). Popularyzujmy kulturę.
    * chain: Festiwal Pana Skarpetka, Baśń 'Pan Skarpetek i Inżynier Drakolicki' do napisania przez Kalistę
* Mawir (Separacjoniści): Dojdźmy do tego co się tu dzieje. Wspierajmy Kalistę.
    * chain: konstrukcja Pomieszczeń Toksycznej Adaptacji, rozchodzenie się i pytanie, polowanie na Pana Skarpetka
* Wit (Separacjoniści): savaranie mają być savaranami, nie powinniśmy się tak mieszać. Róbmy swoje i działajmy. Anty-Aerina.
    * chain: brutalna dewastacja każdego co chce współpracować z Integratorami LUB wygnanie
* Ikta i Teriman (Integratorzy): (podnieśmy savaran; czemu nie do poziomu drakolitów?)
    * chain: popularyzacja, proponowanie, współpraca z Aeriną
* Rada: chce zrozumieć co się tutaj dzieje i nie chce uszkodzeń dla stacji
    * -> Teraquid: mogą zaatakować i zniszczyć Ogród w imieniu Rady
```

#### 4.18.2. Komentarz i wyjaśnienie

Jeden z potężniejszych i prostszych sposobów budowania sesji. Tworzysz istniejącą sytuację, np. firmę informatyczną. Tworzysz kilku managerów. Oni walczą o wpływy i kluczowe zasoby (ten jeden senior dev oraz prawo do drukarki). Nakładasz na nich presję i stawiasz kto czego potrzebuje i dlaczego jak nic nie zrobi to jego frakcja umrze.

...sesja tworzy się sama.

Jest to ostateczna i najbardziej zaawansowana forma sesji emergentnej (sandbox?) - masz kilka stron, każda czegoś chce, każda z uwagi na swoje Zasoby i Serce jakoś się odnosi do aktualnej sytuacji i jedyne co ci zostaje to symulować co zrobią i jak się odnoszą do świata.

A jako że sytuacja jest niestabilna, są coraz bardziej zdesperowani. Robią coraz więcej błędów. Sytuacja dąży do katastrofy bez niczyjej pomocy.

---
### 4.19. Wykupowanie Torów za Punkty
#### 4.19.1. Przykład

'191201-ukradziony-entropik'

```md
* E1: koszt (3): ewakuacja części pacjentów
* E1: koszt (3): ewakuacja najdroższego sprzętu
* E2: koszt (4): ewakuacja całości pacjentów
* E2: koszt (4): ewakuacja reszty sprzętu
* E3: koszt (5): ewakuacja Entropika
* Z1: koszt (3): zniszczenie części dowodów
* Z2: koszt (3): zniszczenie dowodów
* Z3: koszt (4): zniszczenie śladów
* Z4: koszt (5): destrukcja kompleksu Itaran
* K1: koszt (3): przejęcie Kerainy
* K2: koszt (3): wymiana Kerainy
* D1: koszt (3): dewastacja sił Jastrzębca
* D2: koszt (4): dewastacja Jastrzębca
* O1: koszt (3): obrócenie opinii publicznej za Grzymościem
* O2: koszt (3): obrócenie opinii publicznej przeciw Pustogorowi
```

#### 4.19.2. Komentarz i wyjaśnienie

Tory pojawiły się wcześniej. Ale tu po raz pierwszy ptaszki i krzyżyki jako efekty uboczne mogą generować punkty. Te punkty pozwalają na kupowanie głębokości toru. Na przykład: jeśli mamy sekwencję „artefakt: znaleziony (3), uszkodzony (5), zniszczony (7)”, i wydam 8 punktów to artefakt będzie znaleziony oraz uszkodzony.

Jest to jeden ze sposobów zarówno kontrolowania postępu sesji, kontrolowania stanu rozgrywki oraz Dark Future, wykorzystywania mechaniki projektów oraz daje to rozpisanie poszczególnych łańcuchów na mniejsze komponenty. Czyli lepiej rozumiemy co w tej opowieści się dzieje.

To prowadzi do tego, że zamiast punktów niektóre (X) mogą od razu przesuwać tor w określone miejsce. Jest to kolejna technika która nie jest w pełni ustabilizowana z perspektywy projektowania sesji, ale okazała się być świetna w kontekście samej mechaniki projektów.

---
### 4.20. Definicja kierunku uczuciowego
#### 4.20.1. Przykład

'191201-ukradziony-entropik'

```md
### Dominujące uczucia

* "Zniszczyć to, co powinno być od dawna martwe? Co oni mu zrobili. Jak go uratować."
* "Grzymość, coś ty narobił..."
```

#### 4.20.2. Komentarz i wyjaśnienie

Technika zaprojektowana po to, by MG jednoznacznie wiedział jak **tonalnie** prowadzić sesję. Wszystkie (V) i (X) mają generować określony efekt wśród odbiorcy - nie samych postaci a graczy siedzących przy stole.

To jest forma pokazania w którą stronę tonalnie sesja powinna iść. Czy to w formie cytatów, czy to w formie dobrych porad.

---
### 4.21. Scena Zero budująca Kontekst
#### 4.21.1. Przykład

'191218-kijara-corka-szotaron'

```md
## Scena zero

* Jako Persefona d'Szotaron (od teraz: Kijara) zaatakują statek kosmiczny.
* -> Pokazać im siłę Kijary, wszystko jest proste, są potworem z horroru
* --> będą widzieli moc tego z czym walczą
```

'231122-sen-chroniacy-kochankow'

```md
### Co się stanie (what will happen)

* F0: TUTORIAL
    * (Rovis z Dorianem +1) porywają Jolę-09 (w okolicy 5 savaran)
    * Dorion próbuje być lepszy niż Rovis i jeszcze jednego savaranina
    * -> "i wtedy były największe zamieszki i najwięcej krwi popłynęło".
```

#### 4.21.2. Komentarz i wyjaśnienie

Jednym z głównych problemów jaki staje przed grą z nowymi ludźmi jest to, że nie wiedzą nic o świecie. Nie znają się na magii, nie wiedzą niczego o technologii EZ, nie wiedzą co można, nie rozumieją mechaniki.

Pierwsza scena każdej sesji rpg jednocześnie jest w tutorialem dla graczy - co wolno używać, jak sesja wygląda tonalnie, musi być niskoryzykowna i spełnić zasadę ‘show don’t tell’ - przekazać dość informacji o świecie by gracze mogli wybrać swoje postacie prawidłowo i wiedzieli, jak grać.

To jest specjalna rola Sceny Zero. Gracze nie grają swoimi postaciami - grają jakimiś postaciami, które są ważne z perspektywy fabuły albo które robią coś co gracze powinni wiedzieć.

* W sesji ‘Kijara’, gracze grali potworem kosmicznym. Wszystkie testy szły im bardzo łatwo. Generowali straszliwe moce dla potwora. Gdy potem ich główne postacie spotkały się z tym potworem, ‘they shit bricks’.
* W sesji ‘Sen chroniący kochanków’ gracze zapoznali się z kluczowymi aktorami rozgrywki – Rovisem i Jolą. To sprawiło, że gdy potem znaleźli ciała / osoby problematyczne z tej grupki, wiedzieli z kim mają do czynienia i dlaczego to co widzą jest takie dziwne.

Dodatkowo, scena Zero daje ludziom możliwość eksperymentowania z mechaniką w formie bezpiecznej. Zawsze pokazuję jeden (X) z konsekwencjami, zawsze pokazuję jedną eskalację.

Gdy robię sesję w nowym kontekście lub z nowymi ludźmi, ta technika stała się techniką dominującą.

_Ta scena poważnie zmusza mnie do przemyślenia co gracze **muszą zobaczyć** i jak uruchomić określone NPC w określonych miejscach._ Jeden z kluczy projektowania sesji moim zdaniem.

---
### 4.22. Archetypy / Role Postaci
#### 4.22.1. Przykład

Stacja Szernief, wszystkie sesje Agencji Lux Umbrarum (np. '2312-pajak-na-szernief')

![Archetypy ze 'stacji Szernief'](Materials/240116/240116-003-archetypy.png)

#### 4.22.2. Komentarz i wyjaśnienie

Jedno z najważniejszych odkryć - po co są klasy postaci. Nie chodzi o PDki, nie chodzi o wcielanie się czy wczuwanie. Chodzi o zabezpieczanie niszy – ‘każdy gracz jest w stanie gdzieś być świetny’.

Ma to odwrotną stronę z perspektywy projektowania sesji - jeśli wiem o czym jest sesja, Jestem w stanie umieścić tam odpowiednie archetypy. Jestem w stanie przewidzieć, w których miejscach dane archetypy są przydatne. Jestem w stanie przesymulować sesję pod kątem archetypów i pod tym kątem określić co dodać więcej a co usunąć.

Innymi słowy, projektuję sesję pod kątem archetypów lub przyjacielem archetypy pod kątem sesji. 

---
### 4.23. Handouty z Kontekstem
#### 4.23.1. Przykład

Stacja Szernief, wszystkie sesje Agencji Lux Umbrarum (np. '2312-pajak-na-szernief')

![Handouty ze stacji Szernief](Materials/240116/240116-004-handouts.png)

#### 4.23.2. Komentarz i wyjaśnienie

Bardzo ważny mechanizm z perspektywy gry z randomami, ale także z osobami doświadczonymi które są wrzucone w nowy kontekst. Jaka jest szansa, że normalny człowiek jest w stanie zapamiętać 20 NPCów i kilka wydarzeń, z przeszłości które miały miejsce?

Zero.

A jeśli są one potrzebne z perspektywy śledztwa? A jeśli gracze muszą wiedzieć do kogo mogą się odwołać lub kogo zapytać? Warto dostarczyć im takie coś – nie żeby to koniecznie przeczytali. Po to, że jeżeli będzie potrzebne mają coś do czego mogą się odwołać i przeczytać wtedy. 

Nie każdy gracz ZAPYTA. Ale każdy może PRZECZYTAĆ. Co więcej, daje to poczucie, że to jest OK.

---
### 4.24. Rozpięcie linku Gracz - Postać
#### 4.24.1. Przykład

Sesja 'Ostatnia misja Tarna'. Serio, przeczytajcie opis tej sesji tym razem.

#### 4.24.2. Komentarz i wyjaśnienie

Gracz gra tym czym gra Gracz. To nie musi być jego główna postać. To nie musi być tempka. To nie musi być jedna postać podczas jednej sesji. Te postacie nie muszą być zbalansowane.

To jest spojrzenie, które pokazuje, że RPG jest trochę jak film - ważne jest to na co patrzy kamera. Jeżeli kamera nie patrzy na postacie graczy, gracze powinni mieć wpływ na te klocki na które patrzy kamera.

To sprawia, że kilku graczy może grać tym samym jednym bytem (sesja z Tarnem), lub że w Scenie Zero grasz randomowymi postaciami które mają coś pokazać (Kijara, potem Szernief), lub niezależnie kto jest w scenie to wszyscy gracze mogą coś podpowiedzieć lub pomóc.

Gracz nie wchodzi immersyjnie w swoją postać jako w jedyny ‘leverage point’ na sesji. Postać jest jedynie głównym punktem interakcji pomiędzy graczem i SIS.

---
### 4.25. Mroczna Agenda: Aktor / Frakcja
#### 4.25.1. Przykład

'201021-noktianie-rodu-arlacz'

```md
### Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* (...)
```


'231202-protest-przed-kultem-na-szernief'

```md
#### Strony

* Esurient
    * CZEGO: Eksterminacja wszystkich powiązanych arystokratów oraz przekształcenie Aeriny w wendigo
    * JAK: Opętywanie przyszłych morderców, zwiększanie krwawych czynów, szał arystokratów
* Prawdoposzukiwacze
    * KTO: Kalista, Felina...
    * CZEGO: Dojść do tego co się tu naprawdę dzieje i co za tym stoi
    * JAK: Przesłuchania, zbieranie danych, określenie co i jak
* Kult Sprawiedliwości
    * KTO: populacja zwykłych ludzi, naciskanych przez arystokrację
    * CZEGO: "Niech się boją", "niech nie mają nieskończonych praw"
    * JAK: Przypominanie co i jak się stało, ulotki
* Arystokraci
    * KTO: Aerina, Cassian, Tobiasz, Maximus, Livia, Larisa, Anastazja, 
    * CZEGO: Zmienić świat na lepsze, zrobić różnicę, świetnie się bawić przy okazji
    * JAK: Protesty, płomienne mowy, 'my jesteśmy ważniejsi to robimy co uważamy za słuszne'
* Drakolici Mawira
    * CZEGO: Nie dać się wpakować że to oni; serio nie mają powodu
    * JAK: Redukować kontakt z arystokratami ale NADAL sprzedawać im niewłaściwe środki i specyfiki
* Savaranie
    * CZEGO: Robić swoje, robić porządnie, nie dać się wciągnąć ALE chronić stację-dom
```

'231025-spiew-nielalki-na-castigatorze'

```md
### Fiszki

* Lars Kidironus: asystent oficera naukowego, wierzy w Kidirona i jego wielkość i jest lojalny Eustachemu. Wszędzie widzi spiski. "Czemu amnestyki, hm?"
    * OCEAN: (N+C+): bardzo metodyczny i precyzyjny, wszystko kataloguje, mistrzowski archiwista. "Anomalia w danych jest znakiem potencjalnego zagrożenia"
    * VALS: (Stimulation, Conformity): dopasuje się do grupy i będzie udawał, że wszystko jest idealnie i w porządku. Ale _patrzy_. "Czemu mnie o to pytasz? Co chcesz usłyszeć?"
    * Core Wound - Lie: "wiedziałem i mnie wymazali, nie wiem CO wiedziałem" - "wszystko zapiszę, zobaczę, będę daleko i dojdę do PRAWDY"
    * Styl: coś między Starscreamem i Cyclonusem. Ostrożny, zawsze na baczności, analizujący każdy ruch i słowo innych. "Dlaczego pytasz? Co chcesz ukryć?"
```

'240111-zlomowanie-legendarnej-anitikaplan'

```md
### Co się stało i co wiemy

* Drakolici rządzący jednostką
    * oni PRAGNĄ pieniędzy i nadziei dla siebie (Wiktorianie)
        * Wiktor Worl (Megatron)
        * Twaróg Worl (Starscream)
        * Mirea Worl (Soundwave, oficer naukowy - kontraktowo zobligowana)
* Faerile, przesądni którzy nie chcą tu być ale ich długi są wykupione (Kompania Straceńców)
    * oni PRAGNĄ odkupienia długów i wolności
        * Antos Urak (Broken Kenobi)
        * Hanna Pireus (Starry-Eyed Novice) 
```

#### 4.25.2. Komentarz i wyjaśnienie

Bardzo uniwersalna technika, która ma różne możliwe adaptacje:

* Ma wariant ‘import / eksport’ (chce – ma)
* Ma wariant ‘wizja (kim są i czego chcą) - kto tam należy - jak to osiągają’
* Ma wariant pełnych zaawansowanych fiszek
* Ma wariant ‘wizja gangu - archetypy filmowe (by wiedzieć jak używać poszczególnych członków gangu)’

Za każdym razem chodzi o to samo - jakie strony występują podczas sesji, jak je prowadzić, jak będą zachowywać się w formie spójnej, do czego będą dążyć i jak będą działać. Pozwala to na zwiększenie poziomu ‘emergentnej kreacji sesji’ i na serio powinno być rozpatrywane jednocześnie z ‘emergentnymi agentami’

---
### 4.26. Ekstrakcja pomysłu: Film / Książka / Komiks -> Scena, Wizja, Postać
#### 4.26.1. Przykład

'201210-pocalunek-aspirii'

```md
### Opis sytuacji

Statek Koloidowy poluje na Ariannę. Nie wie, że Arianna dowodzi Infernią - i nie wie, że Infernia ma zaawansowane sensory.
Arianna próbuje zniszczyć statek koloidowy, zanim on zniszczy ją.
```

Model podstawowy: [Balance of Terror, Star Trek, 1966](https://memory-alpha.fandom.com/wiki/Balance_of_Terror_(episode))

```md
* Spock's sensors detect a moving object, but nothing is visible on the screen. He suggests that the Romulans have some sort of invisibility shield. 
* Sposób działania:
    * Following the Romulan ship's path towards a comet's tail, Kirk orders the Enterprise to jump forward and attack the Romulan ship when its trail becomes visible. 
    * The Romulan commander hopes to double back to intercept the Enterprise, but, on learning that his target is no longer following, orders an evasive maneuver. 
    * Each commander, having failed in his plans, reflects on the other's intelligence. 

Czyli 'dwie łodzie podwodne w kosmosie', z dwoma inteligentnymi kapitanami
```

#### 4.26.2. Komentarz i wyjaśnienie

Załóżmy, że widzisz film czy czytasz książkę, która zostawia w Tobie ślad. To może być pojedyncza scena. To może być konkretne wrażenie. To może być ogólna melodia danej opowieści.

Weź ją sobie. Przekształć pod sytuację i pod siebie. Wykorzystaj ją elegancko.

W wypadku przykładu jest rewelacyjny odcinek Star Trek, o niesamowitym napięciu, gdzie dochodzi do wystrzelenia jednego pocisku po bardzo emocjonujących 30 minutach patrzenia na sensory i gadania. Chciałem oddać dokładnie ten klimat - i udało mi się. Tym razem ekstraktowałem całą wizję sesji, ale równie dobrze mogłem wyciągnąć pojedynczą scenę lub ciekawą postać.

Pamiętaj - gracze i tak się nie zorientują.

---
### 4.27. Sesja musi być o czymś
#### 4.27.1. Przykład

'231228-ksiezniczka-arianna-ratuje-dzieci-w-lesie'

```md
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Tardigrade Inferno 'Little Princess'
    * "Little princess in the forest | With so strange hypnotic eyes | She will reverse the fairy tale | And in the end, she’ll have to die"
    * -> Wprowadzamy koncept 'Arianny' jako bajki, ratującej dzieci przed strasznym losem od lokalnej grupy przestępczej.
* **Opowieść o (Theme and vision)**: 
    * "Anomalia pochodząca z bajki trafia do jedynej istoty która potrafi ją odczytać przez rezonans podobnych sytuacji."
    * "Teresa ma traumę po wojnie, straciła rodziców i nadal za nimi tęskni i nie umie sobie poradzić sama."
* Detale
    * Crisis source: 
        * HEART 1: "Polowanie na dzieci na sprzedaż buduje w protomagu próbę czegoś do obrony - Księżniczka Arianna"
        * VISIBLE: "Dziwne zachowanie Teresy"
```

#### 4.27.2. Komentarz i wyjaśnienie

Można bez żadnego problemu zrobić sesję, w której mamy zbiór agentów, każdy próbuje osiągnąć to na czym mu zależy i sesja uda się prawidłowo. Jako przykład – „kura składa złote jajka”, „sołtys opiekuje się kurą i dzieli się pieniędzmi z wioską”, „kura żywi się ludźmi”, „wioska poluje na ludzi z zewnątrz”. W takiej sesji mamy przeciwnika, mamy sytuację startową i sesja na pewno będzie działać.

Ale ta sesja nie jest bardzo dobra. To po prostu jest wystarczająco dobra sesja. Oryginalna opowieść o kurze składającej złote jajka posiada wyższy koncept, morał - ludzie byli coraz bardziej chciwi i coraz więcej chcieli od tej kury i kura w końcu padła. Morał? „Nadmiar chciwości niszczy to co posiadasz.”

Takie zdanie czy koncept kieruje wizję sesji. Powinno połączyć sesję z czymś głębszym, bardziej ciekawym, „ sesja jest o czymś”. To generuje dużo głębsze emocje u graczy i sprawia że sesję lepiej zapamiętać.

Bardzo skomplikowany element Theme&Vision. **O czym jest ta opowieść?** Jaką zmianę powinna zrobić w graczach? Co powinni pomyśleć - jaki morał i co poczuć?

W wypadku sesji z przykładu - opowieść była o traumie powojennej, młodej dziewczynie, która nie pogodziła się z tym, że jej rodzice już nie żyją, ale nadal próbuje zrobić coś dobrego. Była o ludzkich drapieżnikach korzystających z okazji wojny. I sesja zadziałała - nie tylko z uwagi na interakcję aktorów, ale właśnie z uwagi na bardzo dużą **siłę** tematu tej opowieści.

---
### 4.28. Default Dark Future: Licznik Niegodziwości
#### 4.28.1. Przykład

sesje z Agencją Szernief:

![Licznik Niegodziwości](Materials/240116/240116-005-licznik-niegodziwosci.png)

#### 4.28.2. Komentarz i wyjaśnienie

Jeżeli mamy dostęp do sesji o otwartym zakończeniu - wiemy o jaki Dark Future walczymy, to sytuacja jest stosunkowo prosta. A co, jeśli jesteśmy w sesji achronologicznej? **Wiemy**, że niektórym postaciom nic złego się nie stało? Wiemy, że ogólnie nic nie może skończyć się katastrofalnie? To znaczy, że nie mamy stawek?

Tu do akcji wchodzi **Default Dark Future: Licznik XXX**. Popatrzmy na sesję detektywistyczną. **Wiemy**, że detektyw na końcu znajdzie mordercę, ale nie wiemy co mordercy uda się osiągnąć zanim zostanie znaleziony. Czyli mamy mechanizm typu **High Score**. Lepszy zespół graczy szybciej złapie mordercę (lub uniemożliwi mordercy skuteczne mordowanie) niż słabszy zespół graczy.

Jeżeli wiemy, jak wygląda przyszłość i mamy bardzo silne postacie graczy, musimy zbudować inną linię stawek. Przykładowo, w filmie katastroficznym - ile ludzi uda się uratować. Bo wiemy, że uda się jakichś ludzi uratować i że główny bohater jest bezpieczny.

---
### 4.29. Wizja, Strategia, Taktyka: nad czym gracz ma mieć kontrolę?
#### 4.29.1. Przykład

Porównanie trzech sytuacji:

```md
* Typ 'Wizja, Strategia, Taktyka w rękach Graczy'
    * Typowy OSRowy Sandbox (nie masz celu, nie masz presji, masz lokację i heksy)
    * Arianna jako komodor Orbitera mająca flotę gdzieś "daleko" z celem 'obecności Orbitera i ogólnie stabilizacji'
* Typ 'Wizja nadana, Strategia, Taktyka w rękach Graczy'
    * Arianna jako komodor Orbitera z flotą gdzieś "daleko", ma wytyczne jak ma wyglądać teren jak z nim skończy (np. pozbyć się Saitaerowców, zabezpieczyć pokój między drakolitami i savaranami...)
    * Większość gier komputerowych typu "Civilization 4" (MUSISZ wygrać, od Ciebie zależy jak)
    * Arianna jako kapitan Orbitera mająca przejąć kontrolę nad planetoidą Tekel.
* Typ 'Wizja i Strategia nadane, Taktyka w rękach Graczy'
    * Arianna jako kapitan Orbitera mająca przejąć kontrolę MILITARNIE nad planetoidą Tekel mając zasoby jakie ma i pozostając niezauważoną.
    * Arianna próbująca uratować ludzi z Serenit.
    * Typowy loch D&D.
```

#### 4.29.2. Komentarz i wyjaśnienie

To jest trudne.

Nikt z nas nie chce prowadzić ‘railroadingu’. Wszyscy chcemy, by gracze mieli maksymalną możliwą swobodę i bawili się jak najlepiej.

W praktyce, jest kilka poziomów które musimy rozpatrywać.

Popatrzmy na kampanię z komodorem Bladawirem. Wszystko zaczęło się od pojedynczej sesji, gdzie koleś okazał się być świnią. Potem w wyniku jednego krzyżyka Arianna dołączyła do sił Bladawira. I zespół w którym jest Arianna chce komodora Bladawira zabić bądź usunąć.

To jest poziom Wizji.

Na jednej z sesji okazało się, że Bladawir walczy przeciwko Syndykatowi. Jest świnią i potworem, ale da się to zrozumieć. Arianna teraz chce mu pomóc - chce z Bladawirem uderzyć w Syndykat. 

Poziom Wizji się zmienił. Gdybym jako MG powiedział, że nie może pomóc Bladawirowi, ja kontroluję poziom Wizji gry. To nie zawsze jest złe - zależy od tego o czym jest ta sesja bądź ten wątek.

Bladawir bardzo nie chce, żeby ktokolwiek wiedział co robi. Arianna jakoś musi zaskarbić sobie jego zaufanie. To jest poziom Strategii - jak to zrobić? Jak podejść do tego, by jej zaufał lub nawet czy w ogóle ma jej ufać – może ona wpakuje się w problemy z Syndykatem sama.

Jest jeszcze poziom Taktyki - masz określone surowce czy zasoby i Twoim celem jest sprawić, by Twój cel osiągnięty. Np. planetoida jest opanowana przez piratów i chcesz uratować ludzi na tej planetoidzie. Osobiście kwalifikuje to na taktykę a nie strategię choć możemy się nie zgodzić; zależy od kontekstu.

Teraz - jeśli prowadzimy na czystych klockach, gdzie MG nie posiada nawet poziomu Wizji, te sesje będą dobre, ale nie bardzo dobre. Bez poziomu Wizji te sesje nie mogą być „o czymś”. Chcemy jednak umożliwić graczom zaproponowanie własnej Wizji i walkę o nią. Niekoniecznie w ramach jednej i tej samej sesji - czasem tak. Wymaga wyczucia. Nie mam reguły.

Z doświadczenia w wypadku jednostrzałów gracze najlepiej się czują, jeśli posiadają poziom Wizji nadany odgórnie. Gracze dużo lepiej czują się modyfikując poziom Strategii oraz Taktyki. To doświadczeni gracze mający wiedzę o świecie powoli wchodzą na poziom Wizji.

Z perspektywy MG – MG musi być świadom istnienia 3 poziomów i musi świadomie wybrać które poziomy są modyfikowalne przez graczy w kontekście pojedynczej sesji.

---
### 4.30. Umocowanie postaci i drama międzyludzka
#### 4.30.1. Przykład

'210331-elena-z-rodu-verlen'

```md
### Opis sytuacji

Jesteśmy na takim etapie, że sytuacja z Eleną powoli grozi zniszczeniem delikatnej tkanki która się odbudowuje między rodami Blakenbauer i Verlen, bo Dariusz Blakenbauer tu jest...

### (Z SESJI)

Arianna poczekała jakąś godzinkę odkąd Romeo opuścił Kryptę, po czym sama poszła odwiedzić Elenę. Elena maskuje strach, coś kontemplowała w rekordach. Schowała to zanim Arianna zobaczyła co Elena oglądała. Arianna nie musi jednak wiedzieć co dokładnie Elena oglądała - znając ją, coś w stylu "wszystkie śmierci jakie spowodowałam" albo "kto tam przeze mnie umarł"... oczywiście, miała rację.

Arianna bezpośrednio skonfrontowała się z Eleną. W sposób jaki nigdy tego nie robiła z dorosłą Eleną - czystą akceptacją i miłością. "Zamknijmy przeszłość, wyrównajmy sytuację, razem".

TrZ+4:

* X: Elena jest święcie przekonana, że zostaje jej podejście "dwie dziewczyny Orbitera kontra cały świat". Arianna dodała do tej grupy jeszcze Viorikę ;-).
* V: Te 47 osób Blakenbauerów które zginęły to dla Eleny kolejny dowód, że niszczy wszystko czego dotknie. Verlenland to jej dom, nie chce robić nikomu krzywdy. Dlatego MUSI WYJECHAĆ!
...
```

'180701-dwa-rejsy-w-potrzebie'

```md
* Ż: Kogo (ze wzajemnością) polubiła Kalina na tym statku? Jaką pełni funkcję?
* K: Załogant. Mechanik. On. Kacper, twardy jak podeszwa.
```

#### 4.30.2. Komentarz i wyjaśnienie

W świecie rzeczywistym: dlaczego policjant ma angażować się w ratowanie porwanej influencerki? Ok - jest to jego praca. Ale jeżeli ma z nią głębszą relację, jest jego córka lub przyjaciółka jego córki - to jest to dużo głębsze i ma dużo większe znaczenie.

Istnieje bardzo duży problem z postaciami, które ‘przyszły znikąd, nikogo nie znają, na nikim im nie zależy, nie mają nic do stracenia’. Krzywdzi to nie tylko te postacie i potencjalne opowieści, ale odbiera też elegancji i radości graczom, którzy sterują takimi postaciami.

Jak najszybciej warto uwikłać postacie w tkankę świata. Można zrobić to pytaniami generatywnymi, można przez to, że gracze pomagają konkretnym NPC a te NPC pomagają tym graczom. Umocowujemy postacie dodając im wspólny kontekst, wspólną historię i wspólne przeżycia. Tu perfekcyjnie działa achronologia czy sceny flashbackowe.

Tak samo - widzimy 2 osoby, które się kłócą. Śmieszne i żałosne. Widzimy 2 bliskie nam osoby, które my się kłócą. Smutne i poważne.

Umocowane postacie pozwalają nam na budowanie rzeczy takich jak ‘Arcane’ - sensownych i wartościowych dram międzyludzkich, które faktycznie są przeżywane przez graczy.

Jak w przykładzie – Ariannie bardzo zależało, by pomóc Elenie. Na tamtym etapie, Elena była już wartościową, żywą osobą z uwagi na wspólną przeszłość i przeszłą chronologię. Arianna nie ryzykowałaby tych testów dla całkowicie losowej osoby.






---
### 4.31. X
#### 4.31.1. Przykład
#### 4.31.2. Komentarz i wyjaśnienie

---
### 4.32. X
#### 4.32.1. Przykład
#### 4.32.2. Komentarz i wyjaśnienie


---
### 4.3. X
#### 4.3.1. Przykład
#### 4.3.2. Komentarz i wyjaśnienie