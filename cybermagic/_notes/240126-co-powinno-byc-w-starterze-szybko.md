# Co powinno być w starterze? (240126)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Jak w pytaniu

---
## 2. Spis treści i omówienie

1. Szybka odpowiedź

---
## 3. Szybka odpowiedź

Poniżej odpowiedź o wyjątkowo niskiej, 40% pewności, że dobrze to przeanalizowałem. Bardziej punkt do dyskusji niż odpowiedź.

_Playtest/Starter służy do tego, by [losowy MG] mógł poprowadzić [udaną charakterystyczną sesję] dla tego systemu, [pokazującą siły tego systemu]._

* Zasady dla MG, które sprawią, że poprowadzi sesję **[pokazującą siły tego systemu]**
    * jeśli to inny SYSTEM (Warhammer - Blades in the Dark) a nie inny ŚWIAT DO SYSTEMU (Eberron - Ravenloft - Forgotten Realms) to ma inny _feel_ gdy grasz / prowadzisz. Tzn. jak różne gry komputerowe - one nie są wymienne między sobą. Niby _Unreal Tournament_, _Half-Life_, _F.E.A.R_ to FPSy, ale zupełnie inaczej w nie grasz i masz inne uczucia.
    * MG siadający do nowego systemu chcący poprowadzić w tym systemie nie wie jak go poprowadzić. Poprowadzi to co zna, jeśli nie wie, że ma zrobić inaczej.
    * im "dziwniejszy" jest Twój system, tym silniejsze zasady / sugestie muszą być wpisane w mechanikę lub rady playtesta.
    * SUKCES jest wtedy, gdy [losowy MG] poprowadzi sesję generując ten _feel_ jakiego szukał projektant 
* Sesję pokazującą siły tego systemu
    * każdy system ma "idealny typ sesji" (np. Esoterrorists -> śledztwo, InSpectres -> ghostbusters z zarządzaniem agencją...). 
        * Nawet systemy 'uniwersalne' mają sesje i akcje w których radzą sobie lepiej lub gorzej
    * zadaniem projektanta jest zaproponowanie w playteście takiej sesji, która pokaże, czemu _ten system_ jest lepszy niż wszystkie inne systemy (w kontekście tego typu sesji)
        * jaki _feel_ generuje, którego nie mam w innym systemie
        * typowa obiekcja: "czemu mam użyć tego systemu, skoro mogę prowadzić Warhammera? Znam Warhammera i podoba mi się jego _feel_"
        * żeby sesja była "udana" dla wszystkich obecnych, nawet niedoświadczonych, jak długo chcą tego _feelu_ jaki daje system i grają zgodnie z zasadami
* Skonstruowane predefiniowane postacie, z których każda przyda się na tej sesji i jest łatwa dla MG do uruchomienia w wypadku nieśmiałego gracza
* Jak MG ma pokazać graczowi jakie akcje są preferowane, jakie są zniechęcane, czyli "o czym jest gra". Interfejs komunikacyjny MG - Gracze.

To powyżej nie jest łatwe. Ale dodam punkt, który sprawia że to powyżej jest tak proste jak ukraść dziecku cukierka:

* Żeby MG i Gracze mogli wejść do playtestu z niewielkim kosztem czasu i energii. Np. 15-60 minut inwestycji i mogą grać i prowadzić.

A jeśli **którykolwiek** z pięciu punktów powyżej nie będzie spełniony, starter nie pokaże czemu system jest warty istnienia (lub mało kto do niego podejdzie).
