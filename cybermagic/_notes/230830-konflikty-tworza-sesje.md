# Wyjaśnienie: Konflikty tworzą sesję (230830)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Pokazanie, jak składać sesję z serii konfliktów
* Pokazanie, jak myśleć o przeszkodach
* Konkretny przykład

---
## 2. Spis treści i omówienie

1. Sesja to zbiór konfliktów
    1. Cel notatki - pokazanie konstrukcji sesji przez konflikty
    2. Przykład - Amanda vs uszkodzony cyberpotwór
    3. Co z tego warto wyjąć
        1. Każda sytuacja ma potencjał konfliktu nawet jak konfliktem nie jest
        2. Konflikt często nie toczy się o "oczywistą" rzecz a o okoliczności tej rzeczy
        3. Konflikty zmieniają opowieść - zamiast linii, rzeczywistość meandruje jak rzeka
2. Konstrukcja sesji od konfliktu rdzennego
    1. Kontekst - atak na 'Cognitio Nexus'
    2. Iteracja 01 - kontekst staje się grafem
        1. Dekompozycja sesji na etapy / fazy
        2. Określenie potencjalnych problemów (Przeszkód, opozycji)
        3. Określenie akcji jakie najpewniej postacie robią w tych fazach
        4. Mamy podstawową proto-strukturę sesji, ale bez emocji
    3. Iteracja 02 - gdzie są emocje?
        1. Krzywa emocjonalna, pożądana krzywa emocjonalna
        2. "Spawnery Problemów" / "Frakcje" z podziałem na (Impuls) - (Środki) - (Sukces) - (O co walczy)
        3. Fazy sesji, ze: (spawnerami), (decyzjami graczy), (akcjami graczy), (sukcesami graczy) i (porażkami graczy)
        4. Mapujemy krzywą emocjonalną na powyższe i patrzymy czy zadziała
    4. Iteracja 03 - kluczowe detale
        1. Mamy klocki, których możemy użyć by uprościć sobie życie
            1. Nawet jak klocek nie jest perfekcyjny, jest lepszy niż nic
        2. Warto rozrysować "dominujące typowe" byty i budować na ich podstawie kolejne
        3. Każdy nowy klocek daje nowe możliwości. Nie wiemy co zrobią gracze.
            1. Ale cokolwiek nie zrobią, będzie fajne

---
## 3. Sesja to zbiór konfliktów
### 3.1. Cel notatki - pokazanie konstrukcji sesji przez konflikty

Konflikty jako takie są dokładniej opisane w [230828 - Mapowanie konfliktów](230828-mapowanie-konfliktow). 

Powiedziałem, że konflikt oznacza, że **dwie osoby siedzące przy stole mają dwie różne wizje rzeczywistości**. Jak zatem połączyć tą definicję z "różnymi wizjami rzeczywistości"?

---
### 3.2. Przykład - Amanda vs uszkodzony cyberpotwór

* 1: Amanda (snajper) napotyka na swojej drodze na wpół zniszczonego cyberpotwora. 
    * Gracz Amandy deklaruje "zabijam go"
        * -> nie ma konfliktu. Zarówno MG jak i Gracz mają tą samą wersję rzeczywistości
        * pytanie brzmi jednak, gdzie jest napięcie? Gdzie są emocje? To była scena, ale ona demonstrowała Amandę i stan świata a nie konfliktowała.
* 2: Amanda (snajper) skrada się by dobić na wpół zniszczonego cyberpotwora zanim on wysadzi elektrownię.
    * Gracz Amandy deklaruje "zabijam go"
        * -> mamy potencjalnie konflikt:
            * MG: "Amanda zabija potwora, ale ów zdążył uszkodzić elektrownię"
            * Gracz: "Amanda zabija potwora zanim on uszkodził elektrownię"
        * emocje nie są w obszarze "czy się uda zabić potwora" a "czy Amanda uratuje elektrownię PRZEZ zabicie potwora ZANIM..."
    * Widząc powyższe, wróćmy do (1), ale inaczej.
* 1: Amanda (snajper) napotyka na swojej drodze na wpół zniszczonego cyberpotwora. Potwór kiedyś był burmistrzem miasta, ale nie zostało z niego już niczego.
    * Gracz Amandy deklaruje "zabijam go"
        * -> mamy potencjalnie konflikt:
            * MG: "Amanda zabija potwora, ale..."
                * 1. "Burmistrz był kochany w tym mieście; Amanda wyjdzie na potwora (może dało się go uratować?)"
                * 2. "Pojawi się opozycja wobec działań Amandy; co z tego że potwora zniszczy skoro jej tu nie chcą?"
            * Gracz: "To ja zabijam potwora tak by nikt nie widział"
            * MG: "Już są ludzie niedaleko potwora"
        * Właśnie wyłączyłem konflikt. Jeśli są TUTAJ a potwór jest niegroźny, po co Gracz Amandy ma konfliktować? Więc...
            * MG: "Sęk w tym, że potwór jest Skażony nanitkami; póki żyje, stanowi zagrożenie. Jak ktoś go dotknie, może się zarazić."
            * MG: "Dodatkowo pamiętasz - musisz wyjechać w ciągu 3 dni a dowódca kazał Ci rozwiązać problem w tym miejscu"
        * Właśnie włączyłem konflikt. Gracz ma 3 dni (to nie jest duża presja czasowa, ale oznacza, że musi COŚ z tym zrobić zamiast czekać) i teraz to jest problem tego Gracza.
            * -> Gracz musi działać proaktywnie 
                * zakraść się i zabić cyberpotwora w nocy? 
                * Montować opozycję przeciwko cyberpotworowi by ludzie sami go zabili? 
                * Przekonać przełożonego by dał więcej czasu?
                * ...a może po prostu zastrzelić i olać temat. W końcu i tak za 3 dni go tu nie będzie
            * Jak widzicie, **każda z tych rzeczy przesuwa historię w inne miejsce i demonstruje nam jakiego typu osobą jest Amanda**

Dlatego uważam, że dobre konflikty są dokładnie tym co tworzy nam sesję. A dobry konflikt może wyjść z kiepskiego substratu, za to z ciekawymi okolicznościami.

---
### 3.3. Co z tego warto wyjąć

1. Każda sytuacja ma potencjał konfliktu nawet jak konfliktem nie jest
2. Konflikt często nie toczy się o "oczywistą" rzecz a o okoliczności tej rzeczy
3. Konflikty zmieniają opowieść - zamiast linii, rzeczywistość meandruje jak rzeka

![Ilustracja splątanej mapy](Materials/230830/230830-01-tangled-map.png)

Żeby nie było - pokażę dokładnie jak takie rzeczy się robi.

---
## 4. Konstrukcja sesji zaczynając od konfliktu rdzennego
### 4.1. Kontekst sesji

Kontekst sesji o której mówimy:

* Arkologia Nativis ("miasto") jest atakowane przez Syndykat ("łowców niewolników i grabieżców")
* Postaciom graczy udało się odeprzeć politycznie i strachem Syndykat, ale mają mało czasu
* Syndykat siedzi na orbicie i zagłodzi Arkologię
* Gracze postanowili dostać się do znajdującej się niedaleko placówki 'Cognitio Nexus' i wysłać sygnał SOS do Orbitera ("potężnej siły militarnej zwalczającej Syndykat"), który została wymanewrowany i nie wie że Syndykat tu jest

Innymi słowy, sesja polega na tym:

* wejść do Cognitio Nexus zanim Syndykat się zorientuje
* nacisnąć Czerwony Guzik ("wiadomość do Orbitera")
* wrócić do domu

### 4.2. Iteracja 01 - kontekst staje się grafem

Co mamy teraz:

![Wizualizacja stanu startowego](Materials/230830/diagram-sesji-01.graphviz.svg)

Jak widać, mamy tu kilka redundancji:

* Faza 1 i Faza 3 (wejść do CN i opuścić CN) są identyczne w potencjalnych akcjach
* Faza 2 nie jest trudna. Przekonamy "Syndykat zagraża nam i Wam!" "Ojej masz rację!" i wysyłamy sygnał.

Jest to odpowiednio kiepska sesja. Musimy coś zmienić:

* Faza 3 jest redundantna. Leci. Nie robimy jej. Mamy dwie fazy - **dotarcie** i **wysłanie sygnału**
    * "Po co robić dwa razy to samo"
* Faza 2 jest za prosta, za nudna, za mała
    * Nie ma dość różnorodnych akcji
    * Jest to "no brainer" z perspektywy Cognitio Nexus by wezwać Orbiter jak jest Syndykat

Więc kończymy z czymś takim:

![Faza 2](Materials/230830/diagram-sesji-02.graphviz.svg)

**Ważne rzeczy jakie się pojawiły w tej fazie:**

1. Dekompozycja sesji na  etapy / fazy
    * dotarcie do CN
    * wysłanie sygnału do Orbitera
    * opuszczenie CN
2. Określenie potencjalnych problemów (Przeszkód, opozycji)
    * dzięki temu widzimy w którą stronę idą konflikty
        * mamy znowu układ "najlepiej - najgorzej" (jeszcze nie, ale nam daje to opcję)
3. Określenie akcji jakie najpewniej postacie robią w tych fazach
    * dzięki temu widzimy czy te postacie są w stanie w ogóle to zrobić
    * widzimy jaki jest _feel_ tej sesji
4. Mamy podstawową proto-strukturę sesji, ale bez emocji
    * dotrzeć GDZIEŚ i NACISNĄĆ CZERWONY GUZIK

### 4.2. Iteracja 02 - gdzie są emocje?

Jeśli w (Wysłaniu Wiadomości) nie ma Problemów, to mamy niesprawną sesję. Ogólnie rozumiana linia emocjonalna sesji powinno wyglądać jakoś tak:

![Krzywa emocjonalna sesji](Materials/230830/230830-02-krzywa-emocjonalna.png)

Oczywiście, nie DOKŁADNIE tak; to nie o tym notatka. Ale wzór widzimy:

* Im dalej w las ("w prawo"), tym większe emocje
* Zaczynamy od jakiejś akcji i działania, szybko coś ciekawego się dzieje
* Jest coś w stylu trudnej decyzji czy punktu kulminacyjnego czy bossa
* Ogólnie emocje **narastają** a nie maleją

To co mamy teraz ma złą krzywą - WPIERW jest ciekawie (Syndykat), POTEM jest nudno ("naciskamy klawisz"). Więc: 

* albo robimy sesję o wymanewrowaniu Syndykatu i wtedy _payoff_ to dotarcie do Cognitio Nexus
* albo dotarcie do CN to część pierwsza a prawdziwa "ostra" sesja będzie w części drugiej.

Idę w wariant drugi. Poniżej krzywa do której dążę:

![Docelowa krzywa emocjonalna sesji](Materials/230830/230830-03-krzywa-emocjonalna-docelowa.png)

Oki. **Nie mam pojęcia** jak to się stanie, ale wiem już do czego dążymy i jaką mapę emocjonalną chcemy mieć na sesji. I tu wchodzi **ŚWIAT** i zrozumienie świata.

* Syndykat Aureliona wie, że 'Cognitio Nexus' ma możliwość wezwania Orbiter na pomoc
* Syndykat wysłał zespół komandosów by złamać i przejąć CN
* Coś Poszło Nie Tak. CN się "spotworyzowało"
* Syndykat się wycofał, ale w CN nie ma normalnych ludzi. Są "potwory" (dalej: terrorformy, czyli 'formy terroru').

Co poszło nie tak - Flame i Kić mieli sesję ;-). Więc wiem, że w CN doszło do stworzenia Terrorformów Saitaera i Flame + Kić wycofali swoje oddziały zostawiając tam ruinę i Terrorformy.

I teraz zamapujmy sobie krzywą emocjonalną na plan:

* Dotarcie do CN
    * **spawnery problemów**: Syndykat Aureliona, Burza Magiczna + Trudny Teren
    * **akcje**: manewry polityczne, stealth pojazdów (czyli lecimy w burzy), pilotaż, unikanie i używanie trudnego terenu
    * **feel**: lekki horror (burza magiczna), próba manewrowania sensorów Syndykatu, wymanewrowywanie
    * **decyzje**: trudny teren CZY ryzyko wykrycia przez Syndykat? Okazja na coś ciekawego CZY skupienie na misji? Zostawiamy sojusznika z tyłu CZY ratujemy wszystkich ryzykując uszkodzenia i mniejsze siły? Infernia (superciężki statek) CZY mała jednostka by Syndykat myślał że Infernia jest w Arkologii?
* Problemy w CN
    * **spawnery problemów**: Terrorformy Saitaera, Skażona magicznie baza
    * **akcje**: taktyka, wymanewrowanie terrorformów, szukanie klucza do uruchomienia komunikacji, decydowanie jak daleko się posunąć
    * **feel**: akcja + horror. Terrorformy to jest coś pięknego. Jesteśmy we wrogiej bazie gdzie mechanizmy i ludzie spletli się w transorganiczne koszmary.
    * **decyzje**: "próbujemy to robić czy wracamy?", "ryzykujemy grupą dywersyjną czy wszyscy?", "pomagamy jakimś nędznym survivorom czy to ryzyko przeniesienia utajonych terrorformów?", "ryzykujemy zdrowiem i życiem nazwanych postaci czy ryzykujemy że zobaczy nas Syndykat?"

Czyli mamy teraz dwie fazy.

* Każda faza ma **spawner problemów** który ma Motywację / Impuls.
* Każdy problem posiada **najlepsze - najgorsze**, czyli jak daleko MG może się posunąć.
* Mamy to ułożone w krzywą emocjonalną

Czyli jesteśmy w stanie już to wziąć i prowadzić. Tzn. ja jestem. Ale tym razem to dokładniej rozpiszę.

Posiadana mapa sesji:

![Faza 3](Materials/230830/diagram-sesji-03.graphviz.svg)

Mało czytelne, ale good enough. Teraz mam już wszystko czego ja potrzebuję.

**Ważne rzeczy jakie się pojawiły w tej fazie:**

* Krzywa emocjonalna - analiza czy emocje będą takie jakich szukamy; w jaki feel idziemy
* "Spawnery Problemów" / "Frakcje" z podziałem na (Impuls) - (Środki) - (Sukces) - (O co walczy)
    * to im daje wszelkie narzędzia by wiedzieć jak tym sterować, co robić i po co to robię
* Fazy sesji mają: (spawnery), (decyzje graczy), (akcje graczy), (sukces graczy) i (maksymalną porażkę graczy)
    * Sukces i Porażka to "najgorzej - najlepiej"
    * Spawnery mówią na co mam patrzeć
    * Decyzje i Akcje pokazują mi czy Gracze mają co robić i czy wszyscy Gracze się będą dobrze bawić
* Mapujemy krzywą emocjonalną na powyższe i patrzymy czy zadziała
    * Tu ma duże szanse :-)

### 4.3. Iteracja 03 - kluczowe detale
#### 4.3.1. Gerard, dowódca taktyczny Syndykatu

Teraz odnoszę się do poprzedniej notatki, [230828-mapowanie-konfliktow](230828-mapowanie-konfliktow). Teraz wreszcie mogę zacząć patrzeć na poszczególne ciekawe przeszkody i detale.

Kto stoi na czele Syndykatu Aureliona jako oficer taktyczny? Z kim się zmierzy Zespół?

* Gerard Savaripatel
    * OCEAN: (C+A-): Wybuchowy ALE cierpliwy, lodowo-ognisty, lojalny wobec załogi i rodziny. Nie wybacza wrogom. Tendencje do fokusowania na jednej rzeczy na raz
    * VALS: (Benevolence, Hedonism): "Honor i zaufanie są wszystkim. Ale dobrze też korzystać z życia"
    * Core Wound - Lie: "Wygrzebałem się z nicości, wbijając nóż w plecy przyjacielowi" - "Jedyną drogą jest wieczna ekspansja i plany alternatywne. Przegrana to śmierć."
    * Metakultura: Atarien: "Zawłaszczę wszystko, czego nie dam rady - spalę."
    * Chce: odzyskać córkę.

Gerard jest zaprojektowany przy technice unikalności. "Wybuchowy ALE Cierpliwy". Dwie fazy. Lojalny i honorowy, ale ma na sumieniu zdradę. Zniszczy wszystko, ale posłucha rodziny i rozsądku. Więc Gerarda nie da się przeładować 1000 sygnałów, ale da się go wymanewrować by skupił się na czymś na czym nie powinien.

#### 4.3.2. Trudny Teren (czy to Neikatis czy baza)

Przygotowałem wcześniej w Narzędziach MG komponent "teren w ruinie". Przyda się do bazy.

![Teren w ruinie](Materials/230830/230830-04-teren-w-ruinie.png)

Nie wszystko pasuje, ale DOŚĆ pasuje bym miał z czego wybierać jak się pojawią (X) czy trudne dylematy.

#### 4.3.3. Terrorformy

Wychodzę z Trianai:

![Trianai](Materials/230830/230830-05-trianai.png)

I na ich podstawie mam dobre szkieletowe Terrorformy. Zmienię tylko nieco ich charakterystykę. Jako przykład, Klarvachnid jest formą śluzowatego pająka:

* .Klarvachnid
    * akcje: "wstrzykuję śluz", "rozkładam sieć", "punktowo niszczę pancerz śluzem"
    * siły: "silny i żrący śluz", "niezwykle wytrzymała sieć", "wszędzie wlezie", "przyczepny jak bluszcz"
    * defensywy: "ekstremalna odporność toksyczna", "szybka zmiana środka masy", "nieruchomy jest niewidoczny"
    * słabości: "wrażliwy na wysokie temperatury", "wszystko jest łatwopalne i wybucha"
    * zachowania: "rozpinanie pułapek", "unieruchamianie ofiar"

A to jest Terrorform wzorowany na powyższym:

* Terrorform pajęczy
    * akcje: "rzygam nanitkowym śluzem", "rozkładam sieć", "regeneruję się przez asymilację"
    * siły: "infekcja transorganiczna wiązką nanitek", "niezwykle wytrzymała sieć", "wszędzie wlezie", "przyczepny jak bluszcz", "przyczaja się", "kameleoniczna natura"
    * defensywy: "zniekształcony transorganiczny człowiek w pajęczej formie (strach)", "transorganiczny pancerz", "błaga o ratunek", "trzeba zniszczyć sporo masy by go zniszczyć"
    * słabości: jest wolniejszy niż biegnący człowiek
    * zachowania: "rozpinanie pułapek", "unieruchamianie ofiar", "transorganiczna infekcja"

Nieważne jak dobrze uzbrojony jest zespół. Kilkanaście obchodzących graczy z różnych stron Terrorformów tego typu to po prostu piekło na ziemi.

Jak wygląda próba zestrzelenia tego cholerstwa?

* defensywa 1: "transorganiczny pancerz"
* defensywa 2: "trzeba zniszczyć sporo masy"
* podbicie - manifestacja by zestrzelić

Innymi słowy - jeśli nie mamy naprawdę dobrej broni (np. obszarowa jak miotacz ognia czy wyrzutnia rakiet) to te terrorformy są koszmarnym przeciwnikiem, też przez ich umiejętność asymilacji Ciebie w terrorforma JEDNYM DOTKNIĘCIEM NANITEK.

Mam dziwne wrażenie, że gracze będą dobrze się bawić ;-).

#### 4.3.4. Rozkład bazy

Służę uprzejmie, miałem wcześniej gotowy.

![Mapka](Materials/230830/230830-06-mapa.png)

Jeśli córka jest możliwa do uratowania, jest:

* potencjalną zakładniczką, póki Matka jeszcze ma część swej osobowości aktywnej
* wartościową osobą, która może powiedzieć co i jak

Jeśli Terrorformy chcą zniszczyć Syndykat:

* da się zainfekować kogoś (np. "córkę Gerarda") i wysłać na orbitę
* może da się z nimi dogadać. Co poświęcisz? :>

Itp, itp.

#### 4.3.4. Ważne rzeczy jakie się pojawiły w tej fazie:

* Mamy klocki, których możemy użyć by uprościć sobie życie
    * Nawet jak klocek nie jest perfekcyjny, jest lepszy niż nic
* Warto rozrysować "dominujące typowe" byty i budować na ich podstawie kolejne
* Każdy nowy klocek daje nowe możliwości. Nie wiemy co zrobią gracze.
    * Ale cokolwiek nie zrobią, będzie fajne

## 5. Co mamy?
### 5.1. Krzywa emocjonalną sesji

![Docelowa krzywa emocjonalna sesji](Materials/230830/230830-03-krzywa-emocjonalna-docelowa.png)

### 5.2. Struktura faz sesji

![Faza 3](Materials/230830/diagram-sesji-03.graphviz.svg)

### 5.3. Kluczowe klocki sesji

Gerard, Teren, Terrorformy, cele, impulsy i możliwości

### 5.4. Czyli...

Mamy gotową sesję na 3h :-)
