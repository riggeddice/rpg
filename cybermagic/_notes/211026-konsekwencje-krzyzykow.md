---
layout: mechanika
title: "Notatka mechaniki, 211026 - krzyżyki i konsekwencje"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

## 1. Purpose

Ten dokument ma pokazać jak najlepiej dawać krzyżyki dla postaci w zależności od sytuacji.

## 2. High-level overview

* Po co jest mechanika konfliktu?
    * TAK (ptaszek) przesuwa postać Gracza tam, gdzie Gracz chce postawić klocki
    * NIE (krzyżyk) otwiera nowe wątki, jest nieblokujące – Gracz ponosi konsekwencje i traci zasoby / coś na czym Graczowi zależy
    * Niezależnie od okoliczności, test odpowiada na konkretne pytanie. Każdy wynik musi realnie zmienić stan gry.
        * KAŻDY test, KAŻDY ptaszek i krzyżyk musi być Obserwowalny i zmieniać stan gry.
    * To daje nam poczucie przeciągania liny i zarządzania ryzykiem "co poświęcę by dostać to na czym mi najbardziej zależy" i "jak daleko się posunę".
    * Późne krzyżyki mogą zniszczyć cel nadrzędny gracza i sprawić, że jest to nieosiągalne.
* Mamy cztery poziomy skali: X{1}, X{2-3}, X{4}, X{5}
    * X{1}: lekkie konsekwencje
    * X{2-3}: widoczne konsekwencje
    * X{4}: bardzo poważne konsekwencje.
    * X{5}: katastrofa, przerwanie konfliktu. Możliwość zniszczenia postaci / zabrania jej czegoś super-ważnego.
* Krzyżyki aplikujemy w kilku fazach. Jeśli poprzednia faza nie działa, to idziemy do następnej
    * Faza 1: poziom FIKCJI
        * Co pasuje z perspektywy fikcji w aktualnym kontekście?
        * Co pasuje do postaci i jest komplikacją?
            * Przykład: Eustachy pyta załogę, czy ktoś nie wie o śmierci ich członka załogi --> buduje paranoję i jego załoga zwiera szyki przeciw Innym (ale nie przeciw Eustachemu bo jest jednym z Nas)
        * Czy jest fajny obserwowalny krzyżyk odpowiedniej mocy?
        * Co byłoby fajne i posunęłoby opowieść do przodu?
        * Czy powinienem przesunąć cele przeciwnika o kolejny krok (siła przesunięcia -> głębokość X{n})?
    * Faza 2: poziom CELU NADRZĘDNEGO (INTENCJA - DESIGN - IMPLEMENTACJA) i OCZEKIWAŃ GRACZA
        * Czy mogę sensownie zmienić okoliczności celu nadrzędnego konfliktu, by gracz osiągnął cel ale _nie tak_?
            * Przykład: "Romeo i Elena się mają przestać zwalczać. Gracz oczekuje domyślnie szacunku, ale zależy graczowi by nie walczyli. W wyniku krzyżyków pojawia się pogarda Eleny do Romeo - cel osiągnięty (nie będą się zwalczać bo nie uważają się za godnych przeciwników), ale NIE TAK."
        * Czy mogę sprawić, że gracz osiągnie cel ale utrudni sobie życie na przyszłość / teraz?
            * Przykład: "Klaudia wbiła się do TAI Netrahiny. Gdzie jest Ofelia? Wie. Ale przeciwnik już ją ma."
        * Czego spodziewa się gracz? Czy mogę to złamać w atrakcyjny dla opowieści sposób?
    * Faza 3: DOMENY, ZASOBY, SPRAWCZOŚĆ: co mogę postaci gracza lub graczowi zabrać?
        * Czy mogę przesunąć front przeciwnika do przodu? Dodać mu zasoby?
            * "Nieuczciwy" hidden movement, przeciwnik / trzecia strona dowiaduje się coś, dopakowanie zasobów przeciwnika...
        * Czy mogę graczowi / postaci / zespołowi coś zabrać? Nałożyć negatywny status?
            * "Skończyły Ci się torpedy anihilacyjne"
            * "Infernia jest uszkodzona; nie wytrzymacie ponownie takiego samego trafienia"
* Mamy kilka domen, w których możemy zaaplikować konsekwencje:
    * Cel drugorzędny postaci gracza lub cel drugorzędny gracza - zamykamy ścieżkę / dodajemy komplikacje
    * Zasoby postaci gracza lub status postaci
        * Np. "wypoczęty" -> "zmęczony".
    * Okoliczności celu głównego postaci gracza
        * Np. "może Ci się udać, ALE"
    * Okoliczności świata lub sesji
    * Kolejna strona, która dołącza do akcji
    * Cel przeciwnika lub okoliczności przeciwnika - możemy wzmocnić Drugą Stronę (np. "dodajemy czas" zwykle wzmacnia proaktywnego przeciwnika)
* Czego nie powinniśmy robić aplikując krzyżyki?
    * Heurystyka: X{1-2} są "akceptowalne", X{3-4} to "duża cena", X{5} jest "nieakceptowalnym ryzykiem".
        * Co możemy
            * X{2-3}: wprowadzenie Trzeciej Strony
            * X{4-5}: zniszczyć cel nadrzędny Gracza. Cel / ten środek są nieosiągalne na tej sesji.
        * Czego nie powinniśmy
            * X{1-2}: **nie** powinniśmy uszkadzać poważnie celu głównego postaci. Możemy zrobić to skomplikowane, ale nie powinniśmy go zniszczyć.
            * X{1-4}: **nie** wolno nieodwracalnie uszkodzić postaci gracza ani kluczowych NPC / zasobów dla danego gracza. To musi być znana, jawna konsekwencja X{5}.

## 3. Istotne przykłady

### 3.1. 210331-elena-z-rodu-verlen (pierwszy gdzie zapisuję konflikty)
#### Kontekst

Romeo i Elena są rywalami "od zawsze". Elena Romeo nienawidzi, Romeo zachowuje się jakby jej nienawidził. Arianna (postać gracza) doszła do tego, że Romeo kocha się w Elenie od zawsze i nie umiał tego przekazać. Więc Arianna próbuje przekonać Romeo - niech wyzna Elenie swoją miłość. Niech jej wyjaśni skąd to wszystko się bierze. Skąd ta animozja. Bo ryzykuje, że oni wszyscy stracą Elenę z rodu Verlen na zawsze (tzn. że Elena opuści ród). A na to Romeo nie chce się zgodzić...

#### Komentarz

Celem najwyższego rzędu jest to, by Elena nie opuściła rodu Verlen. Celem drugorzędnym jest to, by Romeo i Elena się pogodzili. A przynajmniej, by nie było między nimi nienawiści. Innym celem drugorzędnym jest to, by Elena i Romeo umieli współpracować razem i uznali się za godnych partnerów - kuzynostwo.

#### Test

TrZ+2:

* X: jeśli Romeo wyzna miłość Elenie i wyjdzie źle, będzie winił Ariannę
* V: podczas rozmowy z Eleną Romeo się wymsknie, że on ją jednak kocha
* V: wymsknęło mu się, że jak śpi to czasem ma koszmary że Elena go bije. To sprawia, że Elena (i Arianna) nie mają jak traktować go wrogo. Jest... niegodny na wroga.
* X: "there is US! you and me!" Arianna tak powiedziała!
* V: litość i lekka pogarda ze strony Eleny, ale nie nienawiść czy próba pokonania. Romeo po prostu jest. Tym gorszym kuzynem, ale kuzynem...

#### Interpretacja

* Arianna wzięła na siebie jako konsekwencje pierwsze dwa krzyżyki - "ona wrobiła Romeo w wyznanie prawdy Elenie". To sprawiło, że Romeo się długo na Ariannę dąsa (X{1-2}).
* Romeo poszedł w "żałosność" - koszmary, kocha Elenę. Elena ma taki charakter, że jej to nie pasuje. Krzyżyki -> EFEKT osiągnięty, ale cel najwyższego rzędu się zmienił.
* Ariannie pasował taki wynik.

### 3.2. 210421-znudzona-zaloga-inferni
#### Kontekst

Eustachy to postać gracza. Elena to NPC. Eustachy jest bezpośrednim przełożonym Eleny. Jest też strasznym kobieciarzem. Elena jest z tych "służbistek" z gorącym sercem, ale umiłowaniem do zasad i reguł.

Eustachy wysyła sprzeczne sygnały Elenie, która ogólnie jest nim zainteresowana. W wyniku pewnej skomplikowanej sytuacji Elena uznała, że Eustachy CHYBA podrywa ją uszkadzając subsystemy Inferni - ich statku kosmicznego. "Czy Eustachy próbuje zaprosić mnie na RANDKĘ wysadzając podsystemy Inferni?". "Och wow, ojej... ojej... co ja mam teraz zrobić XD". 

Wielki Plan Eleny: Elena udaje, że próbuje NAPRAWIĆ systemy. Próbuje wpaść w kłopoty ale się nie zabić. I żeby nie było tego widać że robi to specjalnie. Jej cel - skonfrontować się z Eustachym i zmusić go do odpowiedzi / działania. Zakleszczyć postać gracza. Akcja rozpatrywana jak PVP, za zgodą gracza zgodnie z klasyczną mechaniką.

#### Komentarz 1

Niby NPC, ale Elena próbuje zrobić na Eustachego _compel_ - deklarujesz się. Albo "jesteśmy parą" albo "nie".

#### Test

Elena próbuje zakleszczyć Eustachego, by on MUSIAŁ z nią porozmawiać (ExZ+1):

* V: Skutecznie Wpada w Kłopoty i to wina Eustachego (jego sabotaż Inferni w przeszłości)
* O: ...i Eustachy się nie zorientował. TYLKO on. TYLKO on nie wie, że ona go podrywa po swojemu.
* V: Zostanie uratowana przez Eustachego... i epicki wyrzut Eleny. "Sprawdzam". Czyli _compel_

#### Komentarz 2

Elena dostała wszystko co chciała, acz kosztem reputacji (entropiczny). I teraz zaczyna się to co jest najciekawsze. **Gracz stał się zainteresowany tym romansem**, czyli cel nadrzędny gracza to "dobrze, niech będzie romans!". Ale gracz ORAZ postać są trochę zestresowani sytuacją. Więc dialog:

* Elena: "Serio, mogłeś mnie po prostu zaprosić na randkę. Mogłeś. Mogłeś poprosić. Nie musiałeś mnie wysadzać."
* Eustachy: <mówić jej prawdę o komandosach Verlenów, Leonie itp?>
* Eustachy: "...pani kapitan CO MAM ROBIĆ?!" 
* Arianna: "Leona nie będzie się nudzić jak będzie Was podglądać na randce :3"
* Eustachy: "JESTEM DOWÓDCĄ NIE MOGĘ! ...ale zaproszę jak pójdę na urlop."

#### Test

Eustachy -> Elena. CO ONA ODBIERZE!!! JAK ZAREAGUJE!!! Eustachy chce nie stracić przyjaźni Eleny, ale ODROCZYĆ RANDKĘ! NIE TERAZ. (ExZ+3):

* X: Elena zorientowała się, że EUSTACHY MA RACJĘ!!! Ona hańbi swój stopień podoficerski! Jak może wprowadzać dowódcę w pułapkę by się z nią umówił! Co ona robi ze swoim życiem XD
* O: Eustachy chce się z Eleną umówić. (compel do karty postaci gracza)

PRZEZ KRZYŻYK ELENA MÓWI NIE!

* X: Elena musi się z tym wszystkim zastanowić co ona robi i "na razie zostańmy przyjaciółmi". Nie umówi się z Eustachym, ale go lubi i rozumie, co on musi CZUĆ. Eustachy is like: "RLY!?". Teraz Elena UCIEKA (tak jak wcześniej Eustachy).

#### Interpretacja

* Eustachy chce **zachować** wysoki stan zainteresowania Eleny, ale niech poczeka. Czyli celem gracza staje się "randka tak, nie teraz, trochę wolniej".
* Pierwszy krzyżyk nie niszczy tego - więcej, wspiera cel nadrzędny gracza - ale dodaje KOMPLIKACJĘ. Elena jako podwładna hańbiłaby mundur.
    * Gracz eskaluje, nie chce takiego wyniku
* entropiczny (specjalna mechanika postaci "gorące serce"): Eustachy chce się z Eleną umówić. Ma to wpisane w kartę. Jest nią zainteresowany.
* KOLEJNY krzyżyk - Elena jest przestraszona całą tą sprawą i tym co się dzieje. Jeszcze większe komplikacje. Ona sama nie będzie już polować na Eustachego, teraz kolej gracza na zrobienie ruchu. Do końca tej sesji temat jest zamknięty, Elena ucieka i unika Eustachego.

Czyli cel gracza MOŻE być osiągnięty, ale musi być jawnie nazwany.

### 3.3. 210804-infernia-jest-nasza
#### Kontekst

Infernia - kochany statek kosmiczny - ma trafić na złom bo jest za drogi. Gracze konspirują jak go uratować. By nikt nie mógł rozłożyć go na części itp. Eustachy - główny inżynier (i piroman) - chce go sabotować. Ale musi wejść na statek, który pilnowany jest przez żandarmów.

Eustachy poprosił więc Leonę (psychopatyczna neurosprzężona komandoska Inferni), by ta weszła na Infernię. Bo potrzebuje odwrócić uwagę. Eustachy mówi, że żandarmi pilnują Inferni. A nie powinna. Leona się SKRAJNIE oburzyła, że Infernia na złom a Ariannie nie powiedzieli. Eustachy informuje Leonę że to się może dla niej źle skończyć więzieniem. Ta się uśmiechnęła.

#### Komentarz

Cel nadrzędny gracza (Eustachy) - dostać się na Infernię by móc neutralizować wszelkie plany Drugiej Strony. Do tego celu wykorzystuje agenta Inferni, Leonę. Działania Leony będą odbijać się na reputacji Inferni, ale wiemy, że Leona to Leona - jej krzyżyki idą w efektowną destrukcję i gracz jest tego świadomy. Ot, bierze żywą wyrzutnię rakiet na akcję z wyrzutnią rakiet :-).

#### Test

Leona idzie na Infernię jakby nigdy nic. Żandarmi próbują ją powstrzymać. A Eustachy wywołuje mały pożar na boku - by był dym. 

ExZ+3:

* X: Faktycznie, Leona trafi do więzienia na tydzień
* V: Eustachy się wślizgnie i ma okazję do sabotażu
* X: Eustachy łazi po kanałach serwisowych Inferni i nie wychodzi, ale sabotuje w trakcie.
* X: Leona dokonała bardzo poważnych obrażeń i uszkodzeń; musieli ją zestrzelić. Działała na ostro, jako neurosprzężony komandos.
* V: Eustachy sabotażem skutecznie unieszkodliwia jakichkolwiek planów by nie mieli.

#### Interpretacja

* X{1} - Leona na tydzień do więzienia - nie brzmi jak "pierwszy". Ale z perspektywy tej sesji i Leony nie jest to duży koszt. Plus patrząc kim ona jest, pasuje.
* X{2} - Eustachy nie wyjdzie - oznacza, że postać może być "duchem na Inferni", ale nie może z niej wyjść. Nie wyłącza to postaci - ma połączenie przez internet itp. Ale wyłącza urok osobisty i działania bezpośrednie, a ta postać ma bonusy do niewiast.
* X{3} - Leona ciężko poraniła ludzi i uszkodziła sprzęt. Silnie wpływa na to, jak postrzegana jest Leona, Infernia i załoga Inferni. "Ta psychopatka atakuje swoich".

### 3.4. 210526-morderstwo-na-inferni
#### Kontekst

Ktoś zabił członka załogi Inferni. Eustachy próbuje się dowiedzieć czy jego załoganci coś na ten temat wiedzą.

#### Komentarz

Gracz nie wie czego dokładnie szuka - czegoś powiązanego. Postać jest kiepska w tematach społecznych i raczej jest grubym toporem ciosana ;-).

#### Test

Eustachy równolegle gada ze swoimi technikami. Czy coś widzieli, coś wiedzą itp. Miał wypadek i szuka sprawców. TrZ+2

* XX: Eustachy dał radę doprowadzić do paniki. Ktoś poluje na członków Inferni na K1. Na razie "zewrzeć szyki".
* V: Jak tylko Tivr wylądował, Tal był zestresowany. Wkurzony. Łaził za tym Tivrem. Oglądał go. Niektórzy członkowie go "stary, daj spokój, nie idź tam".
    * Tivr wygląda trochę na projekt noktiański.

#### Interpretacja

* X{1-2}: zespoliłem dwa pierwsze krzyżyki. W kontekście śmierci członka załogi i tego, że Eustachy jako postać jest raczej bezpośredni uznaliśmy, że fakt, że Eustachy pyta czy ktoś nie wie o śmierci członka załogi buduje paranoję. Infernia ma przeżycia i członkowie załogi Inferni bywają specyficzni, więc to oznacza, że nie czują się na K1 jak w domu (a to najbliższe domu co mają).
    * długoterminowo, daje mi to wyciągnięcie i rozwinięcie sesji
    * krótkoterminowo, pokazuje to graczom jak bardzo Infernia jest specyficznym mikroklimatem
        * i jak Eustachy jest średni w komunikacji międzyludzkiej, co jest elementem postaci (wyjątek: dziewczyny)

### 3.5. 211013-szara-nawalnica

#### Kontekst

Bardzo stresująca sytuacja. Postacie graczy są w kosmosie, na Inferni. Dookoła mechaniczne potwory sterowane TAI wysokiej generacji. Gracze chcą wpaść, uratować ludzi ze śmieciowego uszkodzonego statku i wypaść oraz zostawić "prezencik" mechanicznym potworom w formie torpedy anihilacyjnej.

Plan graczy jest taki: Eustachy strzela by zrobić dziurę w statku a Elena i komandosi robią kosmiczne przechwycenie "śmieciarzy" (ludzi ze statku).

#### Komentarz

Sesja wysokostresowa w innym sektorze (nowe miejsce) dla postaci graczy i graczy. Ludzie na śmieciowym statku są "nieważni" dla graczy, ale ważni dla postaci. Uda się ich uratować? To tym razem drugorzędne dla celu sesji. Nie wiemy czym są te mechaniczne potwory, gracze chcą się dowiedzieć. Czyli cele nadrzędne: 

* z czym mamy do czynienia, czym są te potwory kosmiczne
* uratować kogo się da
* dowiedzieć się co tu się dzieje

#### Test

ExZ+4+5O:

* X: Eustachy nie przewidział jak delikatny jest ten statek. Bez sensu, pierwsi zabici.
* Vz: Udało się uratować kilkanaście osób.
* (+3Vg) Eustachy wprowadza torpedy. X: TAI zorientowały się w sytuacji - mogą użyć sejsmiki by znaleźć ludzi. Czas na fabrykację.
* V: Torpedy na miejscu.
* X: Inne potwory lecą w tą stronę.
* V: Wybuch oderwał pół potwora i cholerstwo "przestało żyć". Potwór nie żyje.
* X: musieliśmy stracić torpedę że niby UCIEKAMY. Torpeda je odciąga. ALE - w truchle potwora torpeda anihilacyjna. 
* Vg: Komandos poleciał wziąć bezpieczną próbkę.

#### Interpretacja

* X{1}: śmieciowy statek jest w kiepskim stanie i ma nietypową strukturę (zrobiony z wraków). Działka Inferni mające otworzyć statek doprowadziły do pierwszych śmierci
* V{1}: uratowanie kilkunastu osób spełnia cel postaci graczy i daje pójście "dalej" - da się dowiedzieć co i jak.

Tu cel graczy się zmienia. Wprowadzić torpedę anihilacyjną na pokład śmieciostatku by zniszczyć potwory i zdobyć próbkę potworów.

* V{2}: torpedy umieszczone na miejscu bez strat i problemów
* X{2}: inne potwory lecą w tą stronę -> nie niszczy to celu nadrzędnego (próbka), ale sprawia, że teren nie będzie bezpieczny dla postaci graczy. Utrudnienie.
* V{3-4}: zniszczenie potwora głównego, próbka pozyskana -> dwa ptaszki realizujące cele graczy
* X{3}: tracimy jeszcze jedną torpedę dywersyjną; zostają nam dwie i potem nie mamy jak wrócić (kluczowy zasób)

Czyli operacja high risk - high reward.
