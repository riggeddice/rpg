# Wyjaśnienie: Konflikty tworzą sesję (230830)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Kontynuacja [230611 - Komponentów sesji](/230611-struktura-klockow-sesji)
    * Tamte okazały się "za skomplikowane" i "za duże" do właściwego użycia
    * Przejście na "format karty" - małe i słodkie

---
## 2. Spis treści i omówienie

.

1. Sesja to zbiór konfliktów
    1. Cel notatki - pokazanie konstrukcji sesji przez konflikty
    2. Przykład - Amanda vs uszkodzony cyberpotwór
    3. Co z tego warto wyjąć
        1. Każda sytuacja ma potencjał konfliktu nawet jak konfliktem nie jest
        2. Konflikt często nie toczy się o "oczywistą" rzecz a o okoliczności tej rzeczy
        3. Konflikty zmieniają opowieść - zamiast linii, rzeczywistość meandruje jak rzeka
2. Konstrukcja sesji od konfliktu rdzennego
    1. Kontekst - atak na 'Cognitio Nexus'
    2. Iteracja 01 - kontekst staje się grafem
        1. Dekompozycja sesji na etapy / fazy
        2. Określenie potencjalnych problemów (Przeszkód, opozycji)
        3. Określenie akcji jakie najpewniej postacie robią w tych fazach
        4. Mamy podstawową proto-strukturę sesji, ale bez emocji
    3. Iteracja 02 - gdzie są emocje?
        1. Krzywa emocjonalna, pożądana krzywa emocjonalna
        2. "Spawnery Problemów" / "Frakcje" z podziałem na (Impuls) - (Środki) - (Sukces) - (O co walczy)
        3. Fazy sesji, ze: (spawnerami), (decyzjami graczy), (akcjami graczy), (sukcesami graczy) i (porażkami graczy)
        4. Mapujemy krzywą emocjonalną na powyższe i patrzymy czy zadziała
    4. Iteracja 03 - kluczowe detale
        1. Mamy klocki, których możemy użyć by uprościć sobie życie
            1. Nawet jak klocek nie jest perfekcyjny, jest lepszy niż nic
        2. Warto rozrysować "dominujące typowe" byty i budować na ich podstawie kolejne
        3. Każdy nowy klocek daje nowe możliwości. Nie wiemy co zrobią gracze.
            1. Ale cokolwiek nie zrobią, będzie fajne

---
## 3. Zasady dobrego storytellingu
### 3.1. LiteratureDevil, Storybuilding


[Storybuilding: This One Trick Will Unlock Your First Story!](https://www.youtube.com/watch?v=QPcjMiMz26Q) (mimo nazwy, bardzo sensowny film)

![Konflikt idzie pierwszy](Materials/230909/01-230909-conflict-first.png)

* Materializacja celu. Nie "feeling better" a "save the princess", tangible goals. **Tangible Conflict**.
    * Win something, Rescue, Escape, Stop someone - cztery opcje celu

### 3.2. Pixar 22 Rules

[The 22 PIXAR STORYTELLING RULES: Lessons for Screenwriters](https://industrialscripts.com/pixar-storytelling-rules/)

Te przydatne reguły:

* No.1: You Admire a Character More For Trying Than For Their Successes
    * Postacie AKTYWNE są promowane. Postacie COŚ robią. Postacie się STARAJĄ.
* No.2: You’ve Got to Keep in Mind What’s Interesting to You as an Audience. Not What’s Fun to Do as a Writer. The Two Can Be Very Different.
    * Fokus na Graczach (nawet jak nie na postaciach graczy)
* No.4: Once Upon a Time There Was______. Every day _______. One day________. Because of that,______. Because of That,_______. Until Finally______.
    * implikacyjność, przyczynowo-skutkowość
    * dobry starter sesji
* No.6: What’s Your Character Good At/Most Comfortable With? Throw the Polar Opposite at Them. Challenge Them. How Do They Deal?
    * Fish out of water ;-). Zmuś postacie do kombinowania. Zmuś graczy do wygrania zwycięstwa.
* No. 13: Give Your Characters Opinions. Passive/Malleable Might Seem Likeable as you Write, but It’s Poison to the Audience.
    * "Głód", motywacja, aktywator postaci i opowieści
* No. 14: Why Must You Tell THIS story? What’s the Belief Burning Within You That This Story Feeds Off Of? That’s the Heart of It.
    * Wyjdź od czegoś co lubisz lub co Ciebie poruszyło
* No. 15: If You Were Your Character in this Situation, How Would You Feel? Honesty Lends Credibility to Unbelievable Situations.
    * Inkarnacja postaci, nie 'robi bo musi' a zasymuluj.
* No. 16: What are the Stakes? Give us a Reason to Root for the Character. What Happens if They Don’t Succeed? Stack the Odds Against.
    * Muszą być stawki, obserwowalny sukces i obserwowalna porażka jako wynik sesji
    * Coś złego się stanie
* No. 17: No Work is Ever Wasted. If it Doesn’t Work, Let Go and Move On. It’ll Come Back Around and Be Useful Later.
    * Klocki sesji, narzędzia na przyszłość

Te mniej przydatne dla mnie:

* No. 3: Trying For Theme is Important, However You Won’t See What the Story is About Until You’re At the End of the Story. Got it? Now Rewrite
* No.5: Simplify. Focus. Hop Over Detours. You’ll Feel Like You’re Losing Valuable Stuff But it Sets You Free.
* No.7: Come Up With Your Ending Before You Figure Out Your Middle. Seriously, Endings Are Hard. Get Yours Working up Front.
* No.8: Finish Your Story. Let Go if It Isn’t Perfect. In An Ideal World You Have Both, But Move On. Do Better Next Time.
* No.9: When You’re Stuck, Make a List of What Would and Wouldn’t Happen Next. Material to Get You Unstuck Will Show Up.
* No.10: Pull Apart the Stories You Like. What You Like in Them is a Part of You; You’ve Got to Recognize it Before You Can Use it.
* No.11: Putting it On Paper Lets You Start Fixing it. If it Stays in Your Head, a Perfect Idea, You’ll Never Share it With Anyone.
* No. 12: Discount the First Idea That Comes to Mind. And the 2nd, and the 3rd and 4th and 5th. Get the Obvious Ones Out of the Way. Surprise Yourself.
* No. 18: You Have to Know Yourself: the Difference Between Being Yourself and Fussing. Story is Testing, Not Refining. 
* No. 19: Coincidences to Get Characters into Trouble are Great, Coincidences to Get Them Out of it Are Cheating. 
* No. 20: Exercise: Take the Building Blocks Out of a Movie You Dislike. How’d You Arrange Them into What You DO Like?
* No. 21: You Gotta Identify With Your Characters/Situations. You Can’t just Write “Cool”. What Would Make YOU Act That Way?
* No. 22: What’s the Essence of Your Story? The Most Economical Telling of It? If You Know That, You Can Build Out From There.

### 3.3. Literature Devil, Heart of Ice

![Story - Character - Theme](Materials/230909/02-230909-story-character-theme.png)

* Character: "This is how I will remember you. Surrounded by winter. Forever young, forever beautiful. Rest well, my love. The monster who took you from me will soon learn that revenge is a dish best served cold" (goal: WIN against Boyle, motivation: Nora's death -> Corrupted Virtue (Love))
* Theme: Humanity vs Inhumanity, Warm vs Cold

![Structure by Lee Child](Materials/230909/03-230909-story-structure.png)

* Structure: 
    * QUESTION: "Monster who took you from me" -> WHO is the monster? HOW took you from me?
    * Prologue -> Heist (show threat level) -> Hot chem bath foreshadows defeat -> Boyle presents Fries -> Freeze presents 'kill Boyle' -> Final confrontation -> Epilogue
* Theme, again: 
    * Opponent construction:
        * Corrupted Virtue, Core Wound (traumatic loss - traumatic loss, Dark Mirror)
        * Glimpse of Humanity
    * Core Theme: Warm vs Cold (where COLD is absence of humanity, WARMTH is humanity and compassion)
        * "Mr Freeze got humanity frozen off him, last glimpse is Nora and corrupted love"
        * Boyle is a humanitarian but a true monster, Fries is warm and has passionate warmth towards Nora

### 3.4. Literature Devil, Western vs Eastern Storytelling

[Western vs Eastern Storytelling](https://www.youtube.com/watch?v=ZGiajG2g-Nc)

![Western approach](Materials/230909/05-230909-western-approach.png)

* Western
    * EXTERNAL evil and source of conflict.
    * Central conflict, more aggressive (Greek Tragedy)

![Eastern approach](Materials/230909/05-230909-eastern-approach.png)

* Eastern
    * Problem which does not neccessarily need a conflict
    * Character-driven, slice-of-life, "the beach episode"
        * because if a Central Conflict exists, EVERYTHING needs to link to it
        * and if there is no Central Conflict, you can ask a QUESTION.

![Kishotenketsu answers a dominant question w/o conflict](Materials/230909/06-230909-kishotenketsu-does-not-need-conflict.png)

* Bez L, mamy opowieść o Light Yagami i nie wiemy co zrobi - dalej są pytania
    * Still **internal struggle**
* ...a Gwiezdne Wojny bez "Złego Imperium" -> nie ma powodu, by Luke Skywalker opuścił farmę, bo bez tyranii nie masz silnej rebelii.
    * **External struggle**, patrz na Pixar po raz kolejny: Once Upon a Time There Was...... Every day ....... **One day....... Because of that,.......** Because of That,....... Until Finally.......
        * Czyli coś ZEWNĘTRZNEGO się zmieniło co zdestabilizowało status

amended Kishotenketsu: [Kishotenketsu: East Asia's Formulaic Secret To Unique Stories | 4 act story arc](https://www.youtube.com/watch?v=2xGQoJ7kk8I)

* Structure which does not FOCUS on conflict
* not about overcoming a trial but about understanding and acceptance
* characters can be powerless, this conflict cannot be, usually, dominated; it can have no villain
    * character studies, deep exploration of experiences / biases
    * unreliability, fear, uncertainty
    * unconventional endings - it is about changing audiences, not characters in story

### 3.5. Film Courage - How to write a great story

[How To Write A Great Story (8 Years Of Wisdom Distilled Down To 17 Minutes)](https://www.youtube.com/watch?v=2-g1xYsgJ9s)

1. Every story must have a Problem/Obstacle/Goal 
2. The Problem, the Dramatic Question, the Transformation
    * Every story is about character trying to accomplish sth and having an obstacle
    * Every story makes a dramatic question - will they "get what they want" or not
    * Protagonist goes through a series of events which lead to transformation - THIS transformation is a purpose of a story
    * How a character who tries to accomplish sth runs into others who want to help or harm
3. Transformation/Growth through a personal inner struggle 
    * Characters are in trouble, conflict, struggling, suffering, punished, in the world they are underdogs
4. Measuring progress by the story yardstick 
    * Is he succeeding / failing, we hope the character will succeed
    * We want heroes to struggle, have strength we wish we had
5. Character flaws 
    * more important than strengths, flaws are interesting
    * mistakes -> conflict, understanding
    * no flaw -> hard to figure the journey
6. The story idea
    * newspapers, radios, inspirations
    * past 20 ideas you have some good ones, go DEEPER
    * remove flaws from ideas, make them sharper
7. The logline 
    * one-sentence way that encapsulates
    * whats it about
    * north star - it is about THIS
8. The craft of writing and your personal process
    * everyone has their own process, YOU CAN DO it relatively easily
9. The theme and story concept
    * what do I want to write about?
    * concept, story I want to tell, 3 words about the story, then character, what they do and want
    * emotional need (love, connection, power, revenge) and specific goal that satisfies it
10. Writing the first scene 
    * usually starts the whole process
    * I dont know beat-by-beat, I get to it, but one scene is needed
11. (& plotting vs pantsing)
12. Knowing the 5 Pillar Beats: 
    1. Ending/Resolution
    2. Beginning
    3. Inciting Incident
        * something changes, catalyst, call to adventure
        * Pixar "one day... because of that..."
    4. Midpoint
    5. All is Lost 
13. Writing a script through a series of questions:   
    1. Ask: Who, What do they want, Why, What do they do about it, How?
    2. How to make a scene work? clarity of Goals and Stakes
    3. What is the hero's wound?
13. Moving the story forward through: Stakes, Conflict and Tension
    * clarity of goals and stakes - stakes crucial they make tension, they will lose something
    * characters having victories and getting along - there is no drama
    * conflict ebbs and flows
14. Only include beats/scenes that serve the story and the character’s Goal
    * what character needs from this scene and how it leads towards the story
    * this character is changing, there is conflict, drive one way or other - THOSE are scenes I need to get to next beat
15. The importance of Conflict
    * not conflict faced right then? Then conflict which will come, tension
    * human feelings and desires
16. Building the emotional bedrock of the story
    * comes from your heart, needs to have emotional truth, human experience
17. Outro: the story you need to tell

EXTRA

* what does character want in the scene?
    * if character speaks / does something, it is towards what they want


### 3.5. If You Can't Answer These 6 Questions You Don't Have A Story - Glenn Gers

[If You Can't Answer These 6 Questions You Don't Have A Story - Glenn Gers](https://www.youtube.com/watch?v=uL0atQFZzL8)

* Every character thinks THEY are the main character - everyone tries to accomplish something
* Every story is about character trying to accomplish sth and having an obstacle and what they DO in face of obstacles is stories
    * obstacle can be internal - "I am afraid of train stations"
* Questions
    1. Who is it about? 
    2. What did they want?
    3. Why can't they get it?
    4. What did they do about it?
    5. Why doesn't that work?
    6. How does it end?
* And add more questions to answers "why, why, why..."


### 3.6. Piper’s Six-Stage Process for Writing Books

[Piper’s Six-Stage Process for Writing Books](https://www.youtube.com/watch?v=vngP0182D-k)

1. Seed Thought / Seed Idea - something which inspires me (and add questions), like "nobody sins out of duty"
2. The gathering step - aggregate, throw everything into one extremely large file
3. Organize, carve the structure out of everything aggregated, iteratively and slowly - like painter staring at colours
4. Just start writing anywhere, anything. From any place.
5. Avoid falling into warn-out jargon and familiar things. Present glorious reality in new beautiful steps.
6. The book takes life on its own, ideas come from the very writing, ideas we didn't have before.

### 3.7. Seven-point Story Structure, Reedsy and StudioBinder

* [7-Point Story Structure | Turn a Concept Into an Outline!](https://www.youtube.com/watch?v=vxEhlxTh07k)
* [Three Act Structure Explained — The Secret to Telling a Great Story](https://www.youtube.com/watch?v=tvqjp1CxxD8)

ORIGIN: 2013, Dan Wells, come from tabletop RPG, 7 plot beats

* The Hook (Initial Concept)
* Catalyst / Inciting Incident (First Plot Turn)
* Pinch Point - raise the stakes
* Midpoint - character becomes proactive
* Pinch Point - "all seems lost"
* Second Plot Turn - aha moment
* Resolution

.

* Example "Hunger Games"
    * You start from the END when you use this structure. Resolution -> Hook
* Resolution: "Katniss survives and wins Hunger Games"
* Hook (interesting things in character's life, in opposite end from resolution): "no influence, but can survive and helps family" (so high stakes and believable) (and it adds emotional stakes)
* Split story with Midpoint: REACT - PROACT
* Midpoint: "I cannot just survive, I need to do something - defy the rules of the place, more than one survivor, not play with Capitol rules anymore" -> games can hurt someone like her sister
* 4a: first plotturn: what do they have to lose if they don't respond to it?
    * "we KNOW she is motivated to care for family, so if her sister is selected, she goes in her place"
* 4b: second plotturn: symmetrical to first
    * "use all tools to go towards the end - outsmart, the bad guys is not OTHER contestants but Capitol, we threaten to kill ourselves together"
* 5a: pinchpoints - we miss the pressure points to increase tension and show antagonists
    * "oh this is not going to be cakewalk" + intro antagonist (capitol)
    * second: take all hopes. "having antagonists to trick Katniss - let her have it and at end force only one to survive"

An excellent approach if you think NON-LINEARLY and want to start from seed idea.

### 3.8. The 3 Act Structure: Writing an Engaging Middle, Reedsy

[The 3 Act Structure: Writing an Engaging Middle](https://www.youtube.com/watch?v=iPfQhMFN6k8)

* Second act takes 50%, more or less, of time
* Take a premise and capitalize on it, see transformation across character, story - the journey to get from NOW to SHOULD BE.
* Character
    * Challenge the protagonist, give opportunities to grow (even if won't take them)
    * Go deeper into who the character is, psychology and backstory, internal-external conflicts get complicated
    * Raising emotional stakes, what they lose if they dont succeed
    * Relationships, character's place in the world, character becomes a different version of themselves -> give meaning
* Plot
    * Most of plot events, cohesive series of events which imply each other, consequences and causality
    * Steady sense of pacing, stay relatively consistent
    * Central support beam, support point, change the stakes, key turning point
* Story
    * if moral, start to materialize it and show premise
    * keep in mind cause -> effect
    * do not resolve points of conflict, but keep complicating them, open new points of conflict, keep conflict going
* **You need STAKES and CONFLICT - they give the story energy**, keep track of stakes and raise them
* Let the different threads interact, lots of momentum comes from interacting, keep tying things together


The middle isn't "the stuff you have to go through to get to the good part at the end." The middle...IS the story. The first act is just the setup, and the 3rd act is the payoff.