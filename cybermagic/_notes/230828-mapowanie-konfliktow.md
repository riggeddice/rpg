---
layout: mechanika
title: "Wyjaśnienie, 230828, mapowanie konfliktów"

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox, Darken

---
## 1. Purpose

* Pokazanie jak myśleć o konfliktach
* Pokazanie jak mapować konflikty

---
## 2. High-level overview

1. Cel - wyjaśnienie konfliktów
    1. Konflikt - różnica wizji przyszłości dwóch osób przy stole
    2. Przykład konfliktu
    3. Co jest potrzebne do udanego konfliktu
        1. Dwie osoby przy stole widzą inaczej potencjalną przyszłość
        2. Osoby przy stole określają na czym im zależy i co są skłonne odpuścić
        3. Każdy konflikt jest po coś
        4. Osoby przy stole zabezpieczają warianty preferowanej przyszłości swoimi żetonami
        5. Konflikt się toczy tak długo jak mamy szczegóły ulegające konfliktowi
2. Zakres konfliktu - niezależne łańcuchy konfliktu
    1. Przykładowy rysunek na którym będziemy pracować.
    2. Łańcuchy niezależnych celów do osiągnięcia
    3. Jak podzielić łańcuch na ogniwa
    4. Trudniejszy przypadek - Wiesław
    5. Ogniwa łańcucha: podbicie, manifestacja, eksplozja
    6. Jeden konflikt, różne łańcuchy, różne trudności
3. Mechanika pojedynczego konfliktu
    1. Głębokość eskalacji
        1. Ile chcesz osiągnąć
        2. Przeszkody i niwelowanie aspektów
    2. Trudność eskalacji
        1. Podstawa, Przewagi, Shorthandy
        2. Zmiana trudności przez zmianę okoliczności i/lub strategii
    3. Jak szczegółowy powinien być konflikt?
4. Pytania
    1. Kiedy test jest Trudny a kiedy Ekstremalny lub Heroiczny?
    2. Jak Sygnalizować potencjalne zło? Kiedyś 2 pierwsze wyniki to było coś lekkiego...

---
## 3. Cel -  wyjaśnienie konfliktów
### 3.1. Konflikt - różnica wizji przyszłości dwóch osób przy stole

Konflikt oznacza, że **dwie osoby siedzące przy stole mają dwie różne wizje rzeczywistości**. Nie "dwie postacie chcą innych rzeczy". Dwie osoby siedzące przy stole nie zgadzają się co do przyszłości. I test ma odpowiedzieć na pytanie "czyja wersja trafia do kanonu".

---
### 3.2. Przykład konfliktu

* KONTEKST: Leona (neurosprzężony komandos) idzie przez korytarz. Napada ją czterech pijanych arystokratów, chcących dać jej łupnia.
* WIZJA GRACZA: Leona pokonuje tych czterech bez problemu
* WIZJA MG: zgadzam się
* == nie ma konfliktu

Ale rozbierzmy ten przykład dokładniej.

* KONTEKST: Leona (neurosprzężony komandos) idzie przez korytarz. Napada ją czterech pijanych arystokratów, chcąc dać jej łupnia.
    * (jak widzicie, kontekst jest bez zmian)
* WIZJA GRACZA:
    * Leona pokonuje tych czterech bez problemu
    * Leona robi to efektownie i krwawo, nikt więcej nie odważy się na nią napaść
    * Nie stało się nic fundamentalnie strasznego
* WIZJA MG:
    * Leona pokonuje tych czterech bez problemu (tu się zgadzamy)
    * Leona robi to efektownie i krwawo, _więc robi sobie wrogów do końca życia do poziomu vendetty_
    * _Leona (a zwłaszcza jej przełożona) mają problemy polityczne przez działania Leony_
    * _Leona robi komuś z nich ciężką krzywdę, przez co są potem poważne kłopoty_

Jak widać, nie konfliktujemy o to, czy Leona pokona tych czterech podpitych. To nie jest pytanie. Konfliktujemy o to, czy żądza walki w naszej ulubionej drobnej blondyneczce nie spowoduje poważnych kłopotów dalej.

Czyli nie testujemy **umiejętności walki** (czymkolwiek by nie była), bo to nie podlega konfliktowi. Testujemy jej **umiejętność opanowania się** i **zrozumienia kontekstu sytuacji**.

Najpewniej zrobiłbym to tak:

* Łańcuch "dalsze kłopoty wynikające z tej sytuacji"
    * V(1): (podbicie) Leonę da się wybronić z tego przed sądem polowym
    * V(2): (manifestacja) Leona się powstrzymała; jak rzadko, udało jej się nie zrobić nikomu poważnej krzywdy
    * V(3): Nikt nie ma vendetty z Leoną
* Łańcuch "nikt nie chce się bić z Leoną"
    * V(1): (podbicie) Leona odstraszyła większość "normalsów", ALE została uznana za 'wartościowego przeciwnika'
    * V(2): (manifestacja) Nikt nie chce zaczepiać Leony
    * V(3): Nie tylko nikt nie chce jej zaczepiać, ale też się jej boją i jest jej łatwiej wymuszać i zastraszać

Oczywiście, w wypadku (X) poszedłbym od razu w "szał bojowy opanował Leonę i zrobiła komuś krzywdę". Na przykład:

* X(1): (podbicie) Leona połamała kilka rączek i zrobiła krzywdę, ale nic czego tydzień w szpitalu nie naprawi
* X(2): (manifestacja) Leona skutecznie pokazała płaczącego arystokratę który z bólu i strachu "zrobił pod siebie". I to trafiło do internetu.

To jest owo nieszczęsne "przeciąganie liny". Każdy konflikt jest **mapą możliwości** i wraz z "zajmowaniem terenu" potencjalnych przyszłości przez poszczególne (V) i (X) tracimy pewne opcje i niektóre inne odblokowujemy.

W ten sposób ja widzę konflikty:

![W ten sposób ja widzę konflikty](Materials/230828/230828-01-plansza-konfliktu.png)

Innymi słowy, każdy (V) i (X) budują nowy kontekst sytuacji i konflikt się toczy nie o całość a tylko o obszary, w których różne strony myślą inaczej.

---
### 3.3. Co jest potrzebne do udanego konfliktu

1. Dwie osoby przy stole widzą inaczej potencjalną przyszłość
    * "Leona wyjdzie bez szwanku" vs "Leona zostaje ranna"
    * "Leona dostaje reputację 'cool girl'" vs "Leona dostaje reputację 'omg nie podchodź wściekły pies'"
2. Osoby przy stole określają na czym im zależy i co są skłonne odpuścić
    * "Leona nie zostaje ranna pokonując czterech podpitych arystokratów" -> podwójna zgoda, nie konfliktujemy
    * "Leona wychodzi na psychopatyczną berserkerkę" -> jedna strona się nie zgadza, więc to testujemy i sprawdzamy **w którą stronę** to się potoczyło.
        * wiedząc "w którą stronę" możemy określić "JAK to się stało"
3. Każdy konflikt jest **po coś**.
    * Jeśli dwie strony zgadzają się co do tego co się stanie, czemu konfliktujemy?
        * Jeśli się nie zgadzają, obie winne być w stanie nazwać wizję przyszłości
            * -> nie ma konfliktu typu "to zobaczę co się stanie" ze strony gracza. Bo nie ma wizji. Czyli akceptuje ruchy MG. Konflikt jest wtedy, gdy któraś strona **NIE** akceptuje narracji drugiej strony.
    * Może pojawić się konflikt typu "chaos", czyli 'chcę losowo wygenerować co się stanie'. Ale wtedy to jest tym celem.
4. Osoby przy stole zabezpieczają warianty preferowanej przyszłości swoimi żetonami `(X) / (V)`
    * Pasuje mi vendetta jako MG, więc zapewniłem to (X). Natomiast Gracz chce by Leona nie miała nikogo na sumieniu i nie było poważnych problemów, więc zapewnił to odpowiednimi (V).
5. Konflikt się toczy tak długo jak mamy szczegóły ulegające konfliktowi
    * "Say YES or roll the dice"
    * Jeśli coś Ci nie pasuje, pogłęb konflikt. Jeśli Ci nie zależy, oddaj bez konfliktu, zwłaszcza przy dłuższym konflikcie.

---
## 4. Zakres konfliktu - niezależne łańcuchy konfliktu
### 4.1. Przykładowy rysunek na którym będziemy pracować.

![Strzelanie do celu](Materials/230828/230828-02-dwa-konflikty.png)

---
#### 4.2. Łańcuchy niezależnych celów do osiągnięcia

Po lewej stronie rysunku widzicie Czesława. Czesław ma pistolet i strzela do tarczy. Czesław może przycelować. Chce zaimponować dziewczynom swoimi umiejętnościami strzelania.

To znaczy, że ten konflikt ma dwa **łańcuchy konfliktów** (zaakceptujcie tymczasowo to uproszczenie)

* Czesław trafił w tarczę -> Czesław trafił w środek tarczy
* Czesław zaimponował jakiejś dziewczynie -> Czesław zaimponował dziewczynom -> Czesław zaimponował wszystkim

Potrafię wyobrazić sobie sytuację, w której:

* Czesław trafił w sam środek ale ten jeden koleś, totalny nerd od broni krzyczy "WOW CZESŁAW JESTEŚ MISTRZEM!". Nie tylko Czesław nie zaimponował dziewczynom (które mają to gdzieś), ale jeszcze ów nerd broni je odstręczył od Czesława.
* Czesław zaimponował wszystkim obecnym, mimo, że ani razu nie trafił w tarczę.
    * np. "broń była uszkodzona i niesprawna; Czesław dał rady ją rozłożyć i jakkolwiek nie dało się tym w nic trafić ale jego umiejętności i tak sprawiły, że wszyscy byli pod wrażeniem"

Czyli każdy łańcuch jest **niezależny** i ma kroki (ogniwa). Co prowadzi do nowego pytania...

---
#### 4.3. Jak podzielić łańcuch na ogniwa

Pracujemy na konflikcie, który ma dwa łańcuchy:

1. CEL: Czesław trafia w sam środek tarczy
2. CEL: Czesław zaimponował dziewczynom

Jak bardzo szczegółowe powinny być te łańcuchy? Znowu, odpowiedź leży w tym **na czym nam zależy** jako osobom przy stole? Ja bym zrobił taką sugestię:

1. CEL: Czesław trafia w sam środek tarczy
    * Sukces (1): Czesław trafia w sam środek
2. CEL: Czesław zaimponował dziewczynom
    * Podbicie (1): Czesław zaimponował kilku dziewczynom, w tym tej na której mu najbardziej zależało
    * Manifestacja (2): Czesław zaimponował wszystkim dziewczynom
    * Eksplozja (3): Czesław zaimponował wszystkim obecnym

Czemu tak?

* W ramach celu "_trafienia w środek_" nie ma tu konieczności dużej precyzji.
    * pierwszy (X) oznacza "nie sam środek, ale dobry wynik" lub "zajęło kilka strzałów, ale się udało trafić w sam środek"
    * jeśli graczowi bardzo zależy "pierwszy strzał, sam środek", spytam "czemu Ci na tym zależy" i w zależności od tego określę odpowiednie (X)
    * **TAK**, patrzymy na łańcuch o potencjalnej długości 1. Czyli łańcuch składający się z 1 ogniwa. Nadal jest to łańcuch.
* W ramach celu "_Czesław zaimponował dziewczynom_" sytuacja jest ciekawsza
    * Moje pierwsze pytanie będzie "czemu chcesz im imponować?" Jeśli odpowiedź to "no bo tak", to traktuję to jako budowę reputacji, czyli zasobu.
        * Zasób tego typu (na potem) zwykle robię przy użyciu Manifestacji, czyli łańcuchu o długości (2).
        * -> w pierwszym (V) jest Podbicie, czyli tym razem okazja do zaimponowania nie wszystkim a części.
    * Więc... łańcuch o długości (2) z opcją kuszenia na (3)

Jeśli popatrzycie na każdy konflikt jak na zbiór **niezależnych subcelów** to sytuacja staje się dość prosta, tylko trzeba to zdekomponować.

---
#### 4.4. Trudniejszy przypadek - Wiesław

Prawy rysunek pokazuje Wiesława.

![Strzelanie do celu](Materials/230828/230828-02-dwa-konflikty.png)

Wiesław robi unik przed Mechanicznym Czaszkostworem, rzuca się szczupakiem oraz próbuje w locie trafić w tarczę. 

* Załóżmy, że trafienie w tarczę sprawi, że Czaszkostwór przestanie działać i symulacja treningowa zakończy się sukcesem. 
* Załóżmy też, że Wiesław jest synem kogoś ważnego i instruktor nim gardzi jako "kolesiem co tu jest bo nepotyzm". I Wiesław chce pokazać, że jest godny ćwiczeń i nie jest tylko "synem swego ojca".

Jeśli tak, mamy trzy następujące łańcuchy (rozpiszę z zakresami możliwości):

1. Wiesław nie zostaje ranny
    * najgorzej: Wiesław kończy ostro poparzony laserami, potrzebuje interwencji medycznej
    * najlepiej: Wiesławowi nic się nie stało
2. Wiesław trafia w tarczę i wyłącza symulację
    * najgorzej: Wiesław nie jest w stanie trafić w tarczę w tych warunkach
    * najlepiej: Wiesław trafił bez problemu w tarczę
3. Wiesław zakończył symulację treningową uzyskując szacunek instruktora
    * najgorzej: Wiesław jest upokorzony
    * najlepiej: Wiesław ma instruktora, który wierzy że Wiesław jest warty starań

Po raz kolejny zaznaczam - **NIGDZIE** nie ma tutaj "Wiesław umiera", bo żadna z osób przy stole tego nie chce. Ani MG ani Gracz. 

Tak - warto rozumieć jakie są konsekwencje Sukcesu i Porażki w ramach łańcucha konfliktu. Nie trzeba wiedzieć dokładnie, ale "najgorzej - najlepiej" jest dobrą techniką.

Mając zakres potencjalnych wyników, przyjrzyjmy się temu konfliktowi. Przy założeniu **samych ptaszków i sukcesów** zrobiłbym to tak:

* (V): podbicie: Wiesław wymanewrował lasery
* (V): manifestacja: ...i trafił w tarczę, kończąc symulację
* (V): ...nie zostając rannym poza lekkimi poparzeniami
* (V): podbicie: Instruktor jest zaskoczony skutecznością Wiesława
* (V): manifestacja: ...i stwierdził, że da mu szansę - warto próbować

![Strzelanie do celu](Materials/230828/230828-03-lancuchy.png)

Oki, a teraz załóżmy nieco bardziej pechowe rzuty:

* (X): (podbicie- łańcucha 1) Wiesław zostaje ranny, ale działa dalej
* (V): (podbicie+ łańcucha 2) Mimo ran, Wiesław przedostał się mimo lasery
* (X): (podbicie- łańcucha 2) Nie udało mu się trafić w tarczę. Musiał kilka razy przez to przechodzić. Niski poziom stylu i rozczarowanie kolegów. Po prostu nie jest dość dobry lub stres go zjadł.
* (X): (podbicie- łańcucha 3) Instruktor uważa go za niekompetentnego
    * (_to nie znaczy, że nie da mu szansy. To tylko znaczy, że uważa go za słabego TERAZ_)
* (X): (manifestacja- łańcucha 1) Wiesław po ćwiczeniach potrzebuje interwencji medycznej
* (X): (manifestacja- łańcucha 3) Instruktor dostał OPIERDOL za krzywdę Wiesława i za to, że nie przerwał w porę; personalna niechęć Instruktora do Wiesława; nie tylko nepotyzm
* (V): (manifestacja+ łańcucha 2) Mimo ran i cierpienia, Wiesław przetrwał dość by być w stanie trafić w tą cholerną tarczę
* (V): (wpływ+ na łańcuch 3) (kilka osób z klasy LUB inny instruktor) uznało, że Wiesław nie jest rozpieszczonym pieskiem salonowym i że warto dać mu szansę

A teraz, te same rzuty, ale gracz priorytetyzuje zdjęcie z siebie odium nepotyzmu i pokazanie że Wiesław jest wartościowy nad sukces na ćwiczeniu

* (X): (p- 1) Wiesław zostaje ranny, ale działa dalej
* (V): (p+ 3) Instruktor jest pod wrażeniem tego, że Wiesław sobie nie radzi ALE próbuje
* (X): (p- 2) Wiesław próbuje w różne sposoby, ale nie potrafi trafić w tarczę
* (X): (m- 2) Wiesław NIE TRAFI w tarczę. Nie da rady. Ma serce i odwagę, ale nie ma kompetencji
* (X): (m- 1) Wiesław po ćwiczeniach potrzebuje interwencji medycznej
* (X): (x- 1) Wiesław zrobił zbyt ambitny manewr i trzeba go wyciągać i ratować stamtąd; ma miesiąc w szpitalu i straci sporo ćwiczeń plus przeniosą go do grupy niżej
* (V): (m+ 3) Instruktor widzi, że Wiesławowi warto dać szansę - nie umie, ale się stara i jest twardy
* (V): (x+ 3) Czyny Wiesława się rozprzestrzeniły wśród kadry. Jest małe jądro osób, które chcą mu pomóc osiągnąć poziom. Instruktorzy, lekarze, niektórzy koledzy itp.

Czyli w tym wypadku mamy zupełnie inną sytuację - Wiesław przegrywa bitwę, ale udowadnia, że jest warty. I w ten sposób Gracze i MG mogą sterować przyszłością postaci i opowieści.

---
#### 4.5. Ogniwa łańcucha: podbicie, manifestacja, eksplozja

* Większość konfliktów ma [1..4] niezależnych łańcuchów (zwykle: 2)
* Większość łańcuchów ma długość od [1..3] (zwykle: 1 lub 2)
    * **podbicie**: osiągasz coś z tego co chcesz, ale nie wszystko
    * **manifestacja**: osiągasz to, na czym Ci zależy
    * **eksplozja**: idziesz o krok dalej, osiągasz więcej

Każda okoliczność, na której nam zależy stanowi osobny niezależny łańcuch.

* Przykład: "Robię obiad"
    * łańcuch: jest smaczny i się najedliśmy
        * sukces: jak wyżej. Jest smaczny i się najedliśmy
        * (ew. porażka "za wolno i czekaliśmy głodni, wykosztowałem się na składniki...)
        * (zwykle nie konfliktowałbym tego wymiaru, bo po co?)
    * łańcuch: "wow stary umiesz tak gotować? O_O"
        * podbicie: udowodniłem kolegom że umiem dobrze gotować
        * manifestacja: serio reputacja, do poziomu "on robi świetną rybę..."
    * łańcuch: dobrze ugościłem ludzi i przez to reputacja domu wzrosła
        * podbicie: dobrze ugościłem i się chcą odwdzięczyć, zapraszać itp
        * manifestacja: zbliżenie relacji, zaproszenie na ważne imprezy (bo uczta) itp...

Jeśli mamy łańcuch o długości (2) lub więcej, to "podbicie" oznacza też "announce badness" - jeśli zaeskaluję ten wymiar, **TO** się stanie. (przypominam, że Gracz może zaproponować inny (X) by zapobiec manifestacji podbicia; jeśli nie proponuje, sam sobie winien)

---
#### 4.6. Jeden konflikt, różne łańcuchy, różne trudności

"Chcę przebiec z garażu do domu w lesie poniżej 30 minut ORAZ nie zostać ugryzionym przez komary."

* Konflikt ma bazę "Trudny", jak każdy.
* Przebiegnięcie poniżej 30 minut -> Trudny
* Nie zostanie ugryzionym przez komary -> ma ekstra problemy (Przewagi przeciwko sobie)

Abstrahując od tego, że to powyżej nie jest dobrym konfliktem (nie ma sensownych stawek), pierwsza część jest Trudna a druga jest Ekstremalna.

Więc **albo** robimy z tego łańcuch "przebiec: 1" i "nieugryziony: 2" **albo** gdy testujemy drugi komponent to zmieniamy tymczasowo konflikt na Ex a potem wracamy do Tr.

---
### 5. Mechanika pojedynczego konfliktu
#### 5.1. Głębokość eskalacji
##### 5.1.1. O co chodzi

* "Chcę zaimponować koleżance"
* "Chcę zaimponować wszystkim dziewczynom które mnie widzą"

To jest ten sam konflikt, ale to drugie oznacza "więcej" niż to pierwsze. Tak samo:

* "Chcę go zranić, by walczył słabiej"
* "Chcę go zranić, by musiał oderwać się z walki"

Głębokość eskalacji przekłada się albo na ilość ogniw łańcucha albo na stopień trudności. Pomyślcie o tym tak:

![Wijące się schody w górach](Materials/230828/230828-04-mountains.png)

* Pokonanie tej ścieżki za jednym zamachem? _ojojojoj_
* Zrobienie jednego kroku? _zero problemu_

Czyli większość problemów z konfliktami da się rozwiązać:

* Dodając nowe ogniwo do łańcucha
* Podnosząc stopień trudności
* Redukując pewne okoliczności, by o nie nie grać.

Przykładowo, Wiesław z rysunku powyżej, unikający laserów:

* Jeśli Wiesławowi **nie zależy** by nie zostać rannym, może przyjąć to jako domyślną konsekwencję i podnieść stopień trudności
    * Detale w Master Notatce, [Stabilizacja mechaniki 2305](230603-mechanika-2305-stabilizacja), punkt `6.7. Automatyczny sukces (ostateczne poświęcenie)`

---
##### 5.1.2. Ile chcesz osiągnąć

![Kot siedzi na drzewie](Materials/230828/230828-05-cat-on-tree.png)

Kot siedzi na wysokim drzewie. Niedaleko jest gniazdo szerszeni. Na dole drzewa siedzą ludzie i krzyczą "RATUJCIE KOTKA!". Kot, jak to kot, ma wszystko gdzieś. Postać gracza jest strażakiem. Jak to rozwiązać?

Jaki cel ma gracz?

* Chcę zdjąć kota z drzewa...
* ...by nic się nikomu nie stało (łącznie z gniazdem szerszeni)...

Czyli mamy głębokość (2). Ale zmieńmy cel gracza:

* Chcę zdjąć kota z drzewa...
* ...by nic się nikomu nie stało (łącznie z gniazdem szerszeni)...
* ...i by do tego kuzyn burmistrza uznał, że trzeba naszą straż dofinansować...

Teraz mamy głębokość (3).

---
##### 5.1.3. Przeszkody i niwelowanie aspektów

Dobrze, chodźmy do czegoś trudniejszego. Amanda (snajper, postać gracza) celuje w cybernetycznego potwora który TEŻ ma broń zasięgową.

![Potwór na którego poluje Amanda](Materials/230828/230828-06-sniper-monster.png)

Przeciwnik jest praktycznie niewidoczny; nie da się go łatwo dorwać. Co gorsza, jest świetnie opancerzony. Jedyny sposób w jaki można go unieszkodliwić to... trafić go w oko. Amanda ma umiejętności jak to zrobić, ale polowanie na tego potwora jest koszmarnie trudne.

Rozrysujmy przeciwnika jako zbiór aspektów:

* Przeciwnik jest żywy
    * Nie da się go trafić ani zobaczyć póki się nie zatrzyma
        * Zatrzymuje się tylko gdy atakuje
    * Jest niesamowicie opancerzony; wrażliwy tylko na trafienie w oko
    * Używa broni dalekosiężnej; snajperskiej

Gdyby Amanda chciała rozwiązać tego przeciwnika używając jednego Konfliktu, musiałaby zrobić to tak:

* (V): JAKOŚ skłoniła przeciwnika do zaatakowania czegoś co nie jest nią
    * --> (V): skłoniła przeciwnika do zatrzymania się by stał się widoczny
* --> (V): przeciwnik jest ustawiony do niej frontem, więc może strzelić w oko
* (V): (podbicie) trafiła w przeciwnika (i go zraniła / odegnała)
* (V): (manifestacja) trafienie w przeciwnika go unieszkodliwiło / zniszczyło

Przeciwnik ma **pięć** aspektów defensywnych a ta strategia Amandy zmusiła ją do pokonania wszystkich pięciu. Jednak możemy zrobić to inaczej:

Amanda uruchomiła dronę, której zadaniem jest ostrzeliwywanie (nieskuteczne) potwora i świecenie weń ostrym światłem. Niech przeciwnik będzie zmuszony do zestrzelenia drony, co daje Amandzie szansę na lepsze pokonanie problemu.

Czyli:

* Przeciwnik jest żywy  <-- to rozwiąże podbicie i manifestacja
    * Nie da się go trafić ani zobaczyć póki się nie zatrzyma   <-- zmusimy go droną
        * Zatrzymuje się tylko gdy atakuje <-- zmusimy go droną
    * Jest niesamowicie opancerzony; wrażliwy tylko na trafienie w oko <-- to kwestia wystawienia przez dronę Amandy
    * Używa broni dalekosiężnej; snajperskiej <-- zniwelowane droną

Czyli:

* Łańcuch "prawidłowe używanie drony" (test na Taktykę i/lub Chowanie się)
    * podbicie: przeciwnik jest tam gdzie powinien być, ale jest aktywny
    * manifestacja: przeciwnik robi dokładnie to co Amanda chciała, odblokowuje drugi łańcuch na niższym stopniu trudności
* Łańcuch "zestrzelenie przeciwnika" (test na Snajpera)
    * (opcjonalne) Amanda trafia przeciwnika tak jak chciała: jeśli drona zawiodła manifestację.
    * podbicie: przeciwnik został trafiony przez Amandę i zmuszony do ucieczki
    * manifestacja: przeciwnik został zestrzelony / unieszkodliwiony

W skrócie:

* jeśli przeciwnik ma PANCERZ, musimy wpierw usunąć PANCERZ by móc zniszczyć przeciwnika
* jeśli gdzieś jest PRZESZKODA, musimy wpierw ją obejść by osiągnąć cel
* Manifestacja na torze daje "pełen sukces" i jeśli potrzebujemy Dużego Efektu, idziemy P->M.

---
#### 5.2. Trudność eskalacji
##### 5.2.1. Podstawa, Przewagi, Shorthandy

Wpierw: zakładam, że znacie [Master Notatkę (stabilizacja 2305)](230603-mechanika-2305-stabilizacja), punkt `6.2.3. Przewagi` i `6.4. Eskalacje`.

Czyli: 

* zawsze wychodzimy z testu Trudnego. (8(X) 6(V))
* Przewaga daje +3(V).
* Dwie Przewagi zmieniają kategorię. (+3(V), -3(X))

Mamy specjalne "skróty":

* Test Trudny to podstawa: `8(X) 6(V)`
* Test Typowy to Trudny z dwoma Przewagami, czyli `5(X) 9(V)`
* Test Ekstremalny to Trudny z dwoma Niedowagami: `11(X) 3(V)`
* Test Heroiczny to Ekstremalny z dwoma Niedowagami: `14(X) 0(V)`

Ale naprawdę, najważniejsze jest zapamiętanie testu Trudnego.

PRZYKŁAD tego po co to nam:

Trasa z Gliwic do Gdańska (300+ km) na piechotę może być zrobiona: 

* jednorazowo (konflikt niemożliwy)
* poniżej 5 dni (60 km/dzień; konflikt _Heroiczny_ dla większości z nas)
* w ciągu 30 dni (konflikt _Typowy z Przewagą_; około 10-15 km dziennie)

Mogę oczywiście liczyć Przewagi. Ale tu nie ma to sensu.

INNY PRZYKŁAD, z liczeniem Przewag:

Amanda celuje w oślepionego przez dronę cyberpotwora na którego Amanda poluje.

* Niedowaga: niesamowity pancerz przeciwnika
* Niedowaga: jedno malutkie oczko (drugie celuje swoją bronią w dronę)
* Przewaga: przeciwnik nie widzi Amandy i nie wie o niej
* Przewaga: Amanda zna wzory ruchu przeciwnika i ma go gdzie chce
* Przewaga: Amanda jest świetnym snajperem i ma dobrą pozycję a przeciwnik jest oślepiony
* (zniwelowana Niedowaga: przeciwnik się rusza i nie wiadomo gdzie jest; Amanda go zmusiła do pozostania)
* (zniwelowana Niedowaga: za mały cel; Amanda ma snajperkę)
* (zniwelowana Niedowaga: zestrzeli Amandę zanim ona go namierzy; drona oślepia)

NNPPP -> +1P, czyli Trudny z Przewagą, czyli pula startowa `8(X) 9(V)` plus Uznaniowe.

Ja po prostu bym zaczął "Ekstremalny" by nie bawić się w zliczanie Przewag i Niedowag i zacząłbym liczyć Przewagi: "oślepiony", "wymanewrowany i nie wie o niej", "Amanda jest mistrzowskim snajperem". `Ex +3P => Tr +P`. Dla mnie proste.

---
##### 5.2.2. Zmiana trudności przez zmianę okoliczności i/lub strategii

Jak na przykładzie Amandy - inne podejście zmienia stopnie trudności konfliktu oraz usuwa część Przeszkód / Aspektów. Generuje dodatkowe Przewagi.

* Kamienny Troll w płytówce vs Indiana Jones? NOPE!
* Kamienny Troll w płytówce próbuje biec przez most ZANIM Indiana Jones go odetnie w przepaść? YUP!

---
##### 5.2.3. Jak szczegółowy powinien być konflikt?

Tak szczegółowy jak jest to konieczne - jeśli dwie strony się zgadzają co do stanu gry, konflikt się zakończył. Nie trzeba też wszystkiego rozwiązywać jednym konfliktem, można kilkoma (przykład z Amandą i droną - jeden konflikt otwiera nam drugi).

Innymi słowy - zależy **o co toczy się dyskusja / konflikt między ludźmi przy stole**. Konflikt jest tak szczegółowy jak osoby przy stole go potrzebują.

---
### 6. Pytania
### 6.1. Kiedy test jest Trudny a kiedy Ekstremalny lub Heroiczny?

Im większą przewagę **w kontekście celu** ma Przeciwnik, tym więcej Niedowag ma gracz. Co dwie Niedowagi zmieniam skalę stopnia trudności.

Załóżmy, że prowadzicie partyzanta w górach, który próbuje pozbyć się najeźdźców z terenu Waszej wioski. Załóżmy też, że najeźdźcy panicznie boją się strat wśród swoich sił. Wszyscy wiedzą, że bojownicy używają tylko broni starej daty (AK-47); nie są w stanie poradzić sobie z pancerzami szturmowymi.

Jeśli przeprowadzicie atak swoimi partyzantami z CELEM zmuszenia najeźdźców do przegrupowania się i/lub zaprzestania działań wojskowych, mamy konflikt Ekstremalny lub Heroiczny. Czemu:

* N: oni wiedzą że nie macie dobrej broni do walki z wami
* N: nie jesteście w stanie zrobić im szkód
* N: oni są przyzwyczajeni
* --> Heroiczny z Przewagą

Nie mamy żadnych Przewag. Ale jeśli zrobimy coś takiego:

* zakopanie min przeciwpiechotnych
* atak pozorowany i ucieczka by za nami biegli
* wpadną na miny i mają rannych i zabitych

Mamy zupełnie inny wynik:

* P: oni MYŚLELI że nie macie dobrej broni A JEDNAK MACIE MINY
* P: mają rannych, potencjalnie zabitych
* P: są zaskoczeni, nie wiedzą co robić
* --> Typowy z Przewagą

Więc mamy dwa łańcuchy:

* Łańcuch "zastawić pułapkę": (najpewniej `Trudny`, lub `Trudny z Przewagą` bo się nie spodziewają)
    * zaminować niepostrzeżenie
    * atak z zaskoczenia (okoliczność: bez strat)
    * ściągnięcie ich na miny (okoliczność: mają straty śmiertelne; tu robię Podbicie - Manifestację)
        * (czyli długość tego łańcucha wynosi (5))
        * ((3) oczka łańcucha otwierają poniższy łańcuch)
* Łańcuch "zmusić ich do wycofania się / przegrupowania": `TpP`
    * (podbicie) złamane morale i zabunkrowanie w bazie
    * (manifestacja) zaprzestanie działań wojskowych, czekają na polecenia

Dokładnie rozpisanie możliwości poniżej:

![Akcja partyzancka, rozpisana](Materials/230828/230828-07-partyzantka.png)

---
### 6.2. Jak Sygnalizować potencjalne zło? Kiedyś 2 pierwsze wyniki to było coś lekkiego, 2 kolejne to mocniejsze i piąty to coś ekstremalnego. A teraz?

Patrz `4.5. Ogniwa łańcucha: podbicie, manifestacja, eksplozja`.

Podbicie to "Sygnalizacja". Z uwagi na wielowymiarowość Łańcuchów, każdy Łańcuch eskaluje sam dla siebie. Tak więc im więcej wymiarów, tym więcej może Gracz poświęcić. Patrz też przykład z `4.4. Trudniejszy przypadek - Wiesław`; tam pokazane jak to robimy z wymiarami.

Jednym słowem: Podbicie - Manifestacja.
