# Analiza: Czego potrzebują wszystkie elementy sesji (230925)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Co naprawdę potrzebują wszystkie elementy sesji by działać i by być ciekawe

---
## 2. Spis treści i omówienie

1. Czego potrzebuje KAŻDY element sesji?
    1. Hipoteza (produkcja-konsumpcja, agenda-sukces-porażka, rola, wyróżnik-kontekst, okazja-afordancje-akcje)
    2. W formie wizualnej, poszerzenie hipotezy
    3. Co z tego wynika?
    4. Przykłady testujące
        1. Jakie pola mamy do rozpatrzenia?
            * Kontekst Świata
            * Kontekst Sesji
            * Rola na tej sesji
            * Wizja i Agenda
            * Produkuje / Konsumuje
            * Akcje i Afordancje
            * Detale
            * Wyróżnik
            * Historia Elementu
        2. Element: Osoba
        3. Frakcja
        4. Lokacja
        5. Przedmiot
        6. Przeciwnik
        7. Domyślna Rola
        8. Agregacja całości

---
## 3. Czego potrzebuje KAŻDY element sesji?
### 3.1. Hipoteza: (produkcja-konsumpcja, agenda-sukces-porażka, rola, wyróżnik-kontekst, okazja-afordancje-akcje)

* Każdy element **coś konsumuje** (potrzebuje) i **coś produkuje** (tzn. dostarcza coś przydatnego innym)
    * Popeye konsumuje _szpinak_ i produkuje _ogromną siłę fizyczną_, czyli f(szpinak) -> ogromna siła fizyczna
        * konsumować można zasoby lub rzeczy niematerialne, np. uczucia czy czynności, czy IMPULS (pragnienia)
    * Wrogie pole minowe to f(naprawy, miny) -> (ogromne ryzyko, eksplozje, spowolnienie)
    * -> To determinuje relację bytu w świecie i jego interakcje ze światem -> możliwość manipulacji i wykorzystania bytu
* Każdy element ma **agendę** w formie **wyniku**, czyli Default Future / Sukces. Stan, do którego dąży. Różnica między (jest, ma być).
    * Sukcesem pola minowego jest "wszystko obecne na polu minowym ginie i jest zniszczone"
    * Sukcesem Ireny Essner jest "wszyscy moi przyjaciele są bezpieczni" oraz "ja jestem najlepszą wojowniczką, nikt mi nie jest w stanie stawić czoła"
* Każdy element ma **rolę** / pozycję w sesji, czyli przyczynę istnienia na sesji z przyczyn taktyczno-dramatycznych. Generyczne elementy mają też **potencjalną domyślną rolę**.
    * _Domyślną Rolą_ inteligentnego pola minowego jest bycie przeszkodą. Ma uniemożliwiać przejście przez pewien teren jednej ze stron lub skrajnie jej utrudniać lub pozbawić zasobów.
    * Rolą inteligentnego pola minowego _na tej sesji_ jest pokazanie graczom, jak bardzo terminusom zależy by potwory tu nie przeszły. Też ma zapewnić, że z TEJ strony są bezpieczni.
* Każdy _nazwany element_ ma **wyróżnik**, czyli aspekt unikalności; coś, co odróżnia go od elementu podobnego. To daje też szczególne siły / aspekty siły.
    * Wyróżnikiem Eleny _jako advancera_ jest jej omnidetekcja - Elena ma perfekcyjną percepcję na wszystkich zmysłach kilka metrów od siebie
    * Wyróżnikiem Netrahiny _jako statku kosmicznego / przestrzeni sesji_ jest to że jest dalekosiężnym statkiem badawczym. Implikacje: skład załogi, mamy konserwy itp.
* Każdy element ma **kontekst**: umiejscowienie w świecie w przestrzeni społeczno-politycznej i przestrzennej. Ma obszary wpływu.
    * Kontekst pola minowego przy Murze Pustogorskim: skalibrowany przeciw potworom (czyli: nie na ludzi, słabość) przez terminusów (przynależność), mechanizm samonaprawy przez roboty z muru (aspekt siły)
    * -> pamiętamy o zasadzie dziedziczenia z "poziomu wyżej". Kantyna na "barokowym badawczym statku kosmicznym" dziedziczy po "barokowym badawczym statku kosmicznym".

Można rozważyć:

* Element ma **Porażkę** - co jest najgorszego co się może stać jak element zawiedzie swoją agendę? To daje rozpiętość `H-101`
* Element ma **Okazje** - co prezentuje jako potencjalne (V) czy (X) dla graczy?
* Element ma **Afordancje i Akcje** - co można z nim zrobić, jakimi typowymi czynnościami osiąga swoją agendę?

### 3.2. W formie wizualnej, poszerzenie hipotezy

![Umieszczenie Elementu w fikcji](Materials/230925/01-230925.png)

Czyli, co tu jest ciekawego i ważnego:

* Gracz potrzebuje
    * Minimalnie 
        * **Potencjalne Akcje i Afordancje**
            * "Biolab na statku umożliwia zbadanie dziwnych śladów śluzu"
            * -> jakie ruchy da się wykonać na konkretnym Elemencie, do czego można go użyć
        * **Konsumpcję elementu**
            * "gang Miatalera bardzo potrzebuje nowych rekrutów i broni"
            * -> jaki słaby punkt ma element / czym możemy aktywować element
    * By mieć pole decyzyjne i możliwości
        * **Ogólny Kształt (detale)** danego elementu w fikcji
            * "Statek kosmiczny jest w kształcie pochodni, zainfekowany jest środkowy obszar"
            * "Kuratorzy przejęli fabrykatory, więc będą w stanie się wielokrotnie powielać"
            * -> kształt i detale dają graczom problemy i możliwości, plus konkretyzują sesję i ją urealniają.
        * **Produkcję elementu**
            * "ta wioska elfów słynie z bułek o własnościach leczniczych"
            * "pole minowe zapewnia śmierć i zniszczenie każdemu kto próbuje na nie wejść"
            * -> gracze mają możliwość wymyślenia rzeczy jakich MG nie przewidział, lub handlowania
        * **Unikalny wyróżnik**
            * "z moich pięciu żołnierzy Radek skrada się najlepiej"
            * "strażnik bramy jest prawie niezniszczalny, ale przepuści Cię jak masz czyste serce"
            * -> świat ożywa i różne byty są spójne i unikalne. Zwykle zapamiętujemy właśnie wyróżniki. Zwiększa to też stożek dramatyczno-taktyczny.
        * **Wizja i agenda** (czego pragnie: H-101)
            * "Krasnoludy z Caer Dhann nienawidzą obcych. Ale przegrywają wojnę z Głębiakami i jeśli coś się nie zmieni, Caer Dhann stanie się martwym fortem."
            * -> gracze mają możliwość manipulowania elementami w sposób niespodziewany dla wszystkich obecnych, bardzo poszerza to ich pole decyzyjne
        * **Relacje** (połączenie z innymi Elementami)
            * "Górscy Koczownicy boją się góry Arunczumalaj i tam się nie zbliżą. Mają pakt krwi z Wioską Kitapat, jak coś zagraża Kitapat, przybędą z odsieczą"
            * -> gracze mogą manipulować relacjami, przesuwać różne frakcje / pionki między sobą
* MG potrzebuje
    * Dla pojedynczej sesji
        * **Kontekst SESJI**
            * "Waszym celem jest uratowanie ludzi z płonącej arkologii zanim wszyscy zginą"
            * -> to prowadzi MG do prawidłowego umieszczenia elementu w fikcji; nie zepsujesz sobie sesji i nie zmarnujesz pracy
        * **Rolę**
            * "podczas burzy piaskowej nie da się przetrwać poza arkologią; jesteście odcięci"
            * -> po co ten element jest w tej sesji, czego od niego chcemy
                * to może być robione jako Agenda - Sukces - Porażka
            * -> Rola jest ŚCIŚLE powiązana z kontekstem sesji
        * **Okazje** (potencjalne krzyżyki i ptaszki)
            * "w gnieździe piaskostworów _ktoś_ znalazł szczątki, a tam - _coś przydatnego_"
            * -> jakie krzyżyki lub ptaszki są zapewnione przez dany element sesji
            * -> jak można wykorzystać dany element sesji jako Gracz lub MG, inspiracja do lepszych ptaszków / krzyżyków / entropicznych
        * **Potencjalne Akcje i Afordancje**
            * "śpiący ukryty gromojaszczur zaatakuje błyskawicami wszystko, co się doń zbliżyło"
            * -> co MG, gracze i inne elementy mogą zrobić z danym elementem
        * **Wizja i agenda** (czego pragnie: H-101)
            * "Bogaty Mikael stracił rodzinę do kosmitów. Teraz z radością skupuje wszystkich kosmitów, by ich torturować. Utrzymuje ich żywcem jak długo jest w stanie."
            * -> MG robi przewidywalny element, który w spójny sposób prawidłowo pełni swoją rolę na sesji.
        * **Relacje**
            * "Mikael regularnie odwiedza cmentarz, gdzie jest pochowana jego rodzina. Wtedy jest narażony na atak."
            * -> Nie tylko świat jest żywy i można wpływać na klocki przez dotykanie innych klocków i mamy efekt motyla, ale też łatwiej MG jest przewidzieć każdy kolejny ruch elementu
        * **Kształt i Detale**
            * -> bardzo przydatne by gracze mieli na czym i z czym pracować. Jak pokazuje Star Trek, ten element można potraktować abstrakcyjnie. Jak pokazuje wiele książek hard sci-fi, nie warto abstrakcyjnie.
    * Dla utrzymania świata
        * **Kontekst ŚWIATA**
            * -> jak działa świat, jak działa ten fragment terenu itp. Nie możesz zrobić np. na pustyni wielkiego miasta na niskim poziomie technologicznym, bo wymrą z głodu i braku wody.
        * **Historię elementu**
            * -> przewidywalność: co się może stać, budowa przyczynowo-skutkowości
        * **Produkcja** oraz **Konsumpcja**.
            * -> to wygeneruje nam import/eksport oraz Relacje. Z kim, jakie itp. Nakładasz na Historię Elementu i Kontekst Świata i powstaje Ci rzeczywistość. Wyprowadzasz z tego tymczasową Wizję i Agendę, potem z tego Rolę na tej sesji...

### 3.3. Co z tego wynika

Czy to znaczy, że musimy **mieć gotowe** wszystkie subelementy dla każdego elementu sesji? Nie. 

* Każdy subelement możemy "mieć" lub "wygenerować go dynamicznie"
* Każdy subelement zapewnia pewne możliwości graczom i MG. Jak czegoś nie mamy, z czegoś zrezygnowaliśmy / coś utrudniliśmy.

Czy to znaczy, że część rzeczy jest reużywalnych? Zdecydowanie tak.

Jeśli mamy 10 żołnierzy na sesji, możemy dać im po prostu po jednym wyróżniku i będzie dobrze. Jeśli mamy 2 skomplikowane byty, warto zainwestować w nie więcej.
        
### 3.4. Kilka przykładów testujących różne poziomy
#### 3.4.1. Jakie pola mamy do rozpatrzenia
##### 3.4.1.1. Pola

* Kontekst Świata
* Kontekst Sesji
* Rola na tej sesji
* Wizja i Agenda
* Produkuje / Konsumuje
* Akcje i Afordancje
* Detale
* Wyróżnik
* Historia Elementu

##### 3.4.1.2. Wspólny kontekst sesji - faza pierwsza (generacja sesji)

**Kontekst sesji** będzie wspólny i to niestety musimy zrobić tutaj.

Zgodnie z analizą budowania opowieści, każda opowieść polega na tym, że bohater ma jeden z celów do osiągnięcia. WIN - RESCUE - ESCAPE - STOP.

Mamy na sesji ciekawe miejsce: 

* Dolinę Jaskiń, gdzie niedawno odkryto dziwne kryształy

Mamy na sesji trzy główne frakcje:

* Arachnoziemcy, którzy wierzą, że da się wykorzystać energię tego miejsca do podniesienia standardu życia i zbudowanie czegoś wartościowego dla grupy.
* Samotnogórcy, którzy opuścili teren kontrolowany przez Verlenów i wolą działać niezależnie na swoim; nie chcą podnoszenia standardów i nie chcą opuszczać tego terenu. Nie chcą Verlenów.
* Lojaliści, którzy chcą by Verlenowie podjęli decyzje - to ich teren, ich sentisieć itp.

I celem postaci jest:

* STOP escalation between factions (ale tak że jak pójdą to też nie będzie problemu)
* STOP whatever is under earth; pokazujemy jak groźny jest Verlenland
    * RESCUE whoever went underground

##### 3.4.1.3. Wspólny kontekst sesji - faza druga (krystalizacja sesji)

Hmm... to prowadzi do innej sesji:

* FAZA 1: "gdzie są zaginięci ludzie i czemu zaginęli", "pod ziemią coś jest"
    * Arianna i Viorika wezwane, bo część Samotnogórców porwała ludzi co schodzą w podziemia
    * Okazuje się, że to nie Samotnogórcy. Kiedyś tak, ale oni zeszli tam sami.
    * Samotnogórcy nie chcą współpracy z Verlenami. Osiedlili się tu, bo tu jest groźnie
    * KONSEKWENCJE FAZY: ile czasu to zajmuje, jakie są relacje na powierzchni
* FAZA 2: "ratujemy ludzi pod ziemią"
    * naprawdę niebezpieczne jaskinie kryształów
    * encountery, trudne decyzje - schodzimy głębiej? KONSEKWENCJE FAZY:
        * ILE uratujemy a ILE zostanie pod ziemią?
        * czy trzeba przesiedlić Samotnogórców?
        * gdy poznamy prawdę - ratujemy pozostałych na dole CZY wychodzimy na powierzchnię?
* FAZA 3: "ratujemy ludzi NAD ziemią"
    * część ludzi zarażonych kryształami jest już na powierzchni
    * KONSEKWENCJE I FINAŁ:
        * czy Arianna i Viorika ściągną zaawansowane zasoby (Atraksjusz)? Czy nacisną Ulę za mocno?
        * czy Lojaliści i Arachnoziemcy nie zostaną zachwiani w wiarę w Ariannę i Viorikę?
        * czy konieczna będzie kwarantanna?

Potrzebuję pewien zbiór przydatnych klocków sesji:

* Osoba: doświadczony samotnogórzec, który nie lubi Verlenów; on wie najwięcej o sytuacji
* Frakcja: arachnoziemcy, którzy chcą wykorzystać moc tego terenu
* Lokacja: jaskinia kryształów, która jest _więzieniem_ i _corruptorem_ porwanych
* Przedmiot: crawler, pojazd do poruszania się pod ziemią
* Przeciwnik: rój witrezulitów (_great crysliver hive_), który próbuje się rozprzestrzenić
* Generyczny komponent: ofiara mentalnego pasożyta, czyli część osób na powierzchni

Tak. To poprowadziłbym w środę.

#### 3.4.2. Element: Osoba
#### 3.4.2.1. Osoba właściwa

#### 3.4.2.2. Ekstrakcja generycznego klocka

#### 3.4.3. Element: Frakcja
#### 3.4.3.1. Frakcja właściwa

#### 3.4.3.2. Ekstrakcja generycznego klocka


#### 3.4.4. Element: Lokacja
#### 3.4.3.1. Frakcja właściwa

#### 3.4.3.2. Ekstrakcja generycznego klocka


#### 3.4.5. Element: Przedmiot
#### 3.4.3.1. Frakcja właściwa

#### 3.4.3.2. Ekstrakcja generycznego klocka


#### 3.4.6. Element: Przeciwnik
#### 3.4.3.1. Frakcja właściwa

#### 3.4.3.2. Ekstrakcja generycznego klocka


#### 3.4.7. Element: Domyślna Rola
#### 3.4.3.1. Frakcja właściwa

#### 3.4.3.2. Ekstrakcja generycznego klocka


#### 3.4.8. Agregacja elementów



