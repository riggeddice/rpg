# Analiza: Jak działają sesje z randomami? Jak je stawiać? Kiedy nie działają? (240122)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox, Darken

---
## 1. Purpose

* Odpowiedź na kluczowe pytania o konstrukcję sesji dla randomów
* Konstrukcja sesji dla randomów

---
## 2. Spis treści i omówienie

1. Dlaczego (X) są nieatrakcyjne?
2. Jak przygotować zespół, który wcześniej grał w inne RPGi do grania w EZ?
3. Dlaczego gracze robią podłe czyny? Co robić w sytuacji PVP?
4. Jak zbudować buy-in graczy w świat i sesję?

---
## 3. Pytanie: dlaczego (X) są nieatrakcyjne?

Podczas sesji miał swój tor co się dzieje w poszczególnych pomieszczeniach. Np. w pralni zalewa się coraz bardziej. Postacie graczy znajdowały się powiedzmy w fosie. Gdy do przodu szły krzyżyki, Darken przesuwał do przodu tor w pralni.

Z perspektywy postaci graczy ich porażki nic nie robiły. Z perspektywy graczy to, w jakimś stopniu kolejny zespół ucierpi nie było istotne. Jedyne co miało znaczenie - oni nie czuli nacisku na siebie. Nie czuli, że cokolwiek ryzykują lub cokolwiek poświęcają.

A przecież gdy ja prowadzę EZ, to w tym momencie ludzie bardzo próbują nie uszkodzić chronologii dla następnych zespołów. Bardzo próbują zapewnić, by zostawić świat lepszym. Na czym polega różnica?

**Odpowiedź jest dość prosta**, acz rozwiązanie jest wielowymiarowe. 

Ja pozycjonuję postacie jako członków Agencji. Nie mają problemów z pieniędzmi, nie zależy im na praktycznie niczym. Są czterdziestoletnimi agentami w szczycie umiejętności, najlepszymi z najlepszych o najlepszym sprzęcie możliwym. Agenci są osobami którymi każdy chciałby być. Są osobami do których my aspirujemy w szczycie kariery.

I wszyscy dookoła dramatycznie potrzebują pomocy tych Agentów. NPC dookoła są kompetentnymi ludźmi na standardowym dla siebie poziomie kariery. Próbują rozwiązywać problemy w sytuacji magii, o której nic nie wiedzą. Mają swoje ludzkie zalety i wady. Agenci chcą im pomóc, bo Gracze widzą w nich swoje odbicie. Ci ludzie nie są ani szczególnie dobrzy ani szczególni źli. A agenci tak naprawdę niczego od nich nie chcą. Nie potrzebują pieniędzy czy zasobów. 

Dodajmy do tego ważny komponent – „High Score”. W wypadku agencji jest to licznik niegodziwości. Na początku sesji informuję graczy, że inne zespoły grały w tej samej lokalizacji i udało im się parę rzeczy naprawić a w ramach tego licznika są na poziomie 10 - 20. Nagle graczom włącza się rywalizacja. Ten zespół będzie lepszy niż poprzednie zespoły. Ten zespół zostawi świat troszeczkę lepszym niż poprzednie zespoły.

To generuje presję. Gdy pojawiają się (X), ja mocno naciskam w zabijanie ważnych postaci. Teraz - gracze mają możliwość odrzucenia (X) i możemy znaleźć i zaproponować inny krzyżyk. Ale sam fakt że zaproponowałem zabicie ważnego NPCa lub wygenerowanie punktów niegodziwości powoduje zwiększenie presji i uczucia ‘omg to się dzieje naprawdę’. Wszystkie krzyżyki są silnie powiązane z czymś na czym graczom zależy.

Wracamy do roli (X):

* dostarczenie zasobów MG 
    * przykład 1: (Kalista nie wiedziała o anomalii -> Kalista wie o anomalii i coś z tym robi, przeszkadzając graczom)
    * przykład 2: (Stacja kosmiczna jest uszkodzona -> Stacja się rozpada, WIĘC postacie mają trudny teren i mało czasu)
* przesunięcie toru przeciwnika
    * przykład 1 (niesprawny): (Potwór śpi -> Potwór się budzi)
    * przykład 2 (sprawny): (Potwór śpi -> agenci potwora atakują wioskę gdzie są postacie graczy by pozyskać figurkę sokoła CO BUDZI POTWORA)
    * -> tutaj krzyżyk daje coś co gracze **widzą**. Natychmiast dzieje się ruch który ich dotyka.
* Itp. itp. (wracam do notatki [211026 - Konsekwencje Krzyżyków (Cztery poziomy krzyżykowania)](211026-konsekwencje-krzyzykow.md) ); może muszę uaktualnić tą notatkę.

Teraz - tor jako taki jest OK. Ale przesunięcie toru musi sprawić, że **gracze widzą coś co się dzieje**. Co zapłacili. Tor musi aktywować coś po ich stronie. Kopiuję z notatki 211026:

```md
* Krzyżyki aplikujemy w kilku fazach. Jeśli poprzednia faza nie działa, to idziemy do następnej
    * Faza 1: poziom FIKCJI
        * Co pasuje z perspektywy fikcji w aktualnym kontekście?
        * Co pasuje do postaci i jest komplikacją?
            * Przykład: Eustachy pyta załogę, czy ktoś nie wie o śmierci ich członka załogi --> buduje paranoję i jego załoga zwiera szyki przeciw Innym (ale nie przeciw Eustachemu bo jest jednym z Nas)
        * Czy jest fajny obserwowalny krzyżyk odpowiedniej mocy?
        * Co byłoby fajne i posunęłoby opowieść do przodu?
        * Czy powinienem przesunąć cele przeciwnika o kolejny krok (siła przesunięcia -> głębokość X{n})?
    * Faza 2: poziom CELU NADRZĘDNEGO (INTENCJA - DESIGN - IMPLEMENTACJA) i OCZEKIWAŃ GRACZA
        * Czy mogę sensownie zmienić okoliczności celu nadrzędnego konfliktu, by gracz osiągnął cel ale _nie tak_?
            * Przykład: "Romeo i Elena się mają przestać zwalczać. Gracz oczekuje domyślnie szacunku, ale zależy graczowi by nie walczyli. W wyniku krzyżyków pojawia się pogarda Eleny do Romeo - cel osiągnięty (nie będą się zwalczać bo nie uważają się za godnych przeciwników), ale NIE TAK."
        * Czy mogę sprawić, że gracz osiągnie cel ale utrudni sobie życie na przyszłość / teraz?
            * Przykład: "Klaudia wbiła się do TAI Netrahiny. Gdzie jest Ofelia? Klaudia już wie. Ale przeciwnik już ją ma."
        * Czego spodziewa się gracz? Czy mogę to złamać w atrakcyjny dla opowieści sposób?
    * Faza 3: DOMENY, ZASOBY, SPRAWCZOŚĆ: co mogę postaci gracza lub graczowi zabrać?
        * Czy mogę przesunąć front przeciwnika do przodu? Dodać mu zasoby?
            * "Nieuczciwy" hidden movement, przeciwnik / trzecia strona dowiaduje się coś, dopakowanie zasobów przeciwnika...
            * WAŻNE: tu musi być coś widocznego dla graczy
        * Czy mogę graczowi / postaci / zespołowi coś zabrać? Nałożyć negatywny status?
            * "Skończyły Ci się torpedy anihilacyjne"
            * "Infernia jest uszkodzona; nie wytrzymacie ponownie takiego samego trafienia"
```

Kiedy możemy zmienić tor w sposób niewidoczny dla graczy? HIDDEN MOVEMENT bez danych? Powiedzmy, **raz na 3-5 (X)**. Czyli 4 (X) muszą boleć gracza tu-i-teraz a 1 (X) może być HIDDEN MOVEMENT.

Dla Graczy nie mają znaczenia rzeczy jakie nie dotykają ich lub ich postaci (zależy od typu gracza). Nagroda jest nagrodą jak jest odbierana jako nagroda, to samo dotyczy kary.

I nie bójmy się dawać graczom silnych konsekwencji i silnych bonusów - negocjacyjna mechanika to wyprostuje. Jeśli powiesz "przeciwnik odgryza Ci rękę" gracz może powiedzieć "nie akceptuję XD". I znajdziecie coś co Wam pasuje - razem, jako cały zespół.

---
## 4. Jak przygotować zespół, który wcześniej grał w inne RPGi do grania w EZ?

Randomy nie umieją grać w eztkę. To jest problem xd Bo przedstawić im "tu są żetony, tak się rozlicza konflikt" jest łatwo. Ale sama dziwna mechanika nie jest jedyną cechą sytlu gry w ez. Przede wszystkim gracze muszą inaczej podejść do rozgrywki. Nie gramy postaciami tak mocno, jak zwykle, gramy jako zespół, rzeczy z karty są by wam pomagać, ale jeśli coś wymyślicie to pewnie się zgodzę... cała intencyjność jest ciężka do złapania. Potknęłam się o to, że o tym zapomniałam XDDD Totalnie zapomniałam graczom wyjaśnić, czym się różni ez od innych systemów i jak powinni podejść do grania. 

Totalnie będziesz potrzebował do technik wrzucić to. Jak przygotować zespół, który wcześniej grał w inne rpgi [np. typowe dnd] do grania w ezetkę.

**Odpowiedź**

Słusznie zauważyłaś, że to nie mechanika stanowi o EZ a konkretne zastosowanie tej mechaniki. Że ten system działa zupełnie inaczej. Najprawdopodobniej poprowadziłaś sesję w taki sposób, jak normalnie prowadzi się sesję kampanijną - czyli to co ja prowadzę Ariannie czy Klaudii.

Teraz - dokładnie tak samo prowadziłem Elenie Samszar i Karolinusowi Samszarowi; oni zdecydowanie potrafili wykorzystać mechanizm mechaniki, ale wyniki i proponowane opowieści nie do końca były kompatybilne z tym czego oczekiwaliśmy od EZ. To implikuje, że kluczem jest mniej prawidłowe użycie mechaniki a bardziej konkretny mindset graczy, którzy próbują rozwiązać sesję w EZ.

**Podkreślam – mimo, że ja prowadziłem na moim poziomie umiejętności, Elena i Karolinus generowali niewłaściwe sesje. Mimo obecności Kić.** To pokazuje, że nie jesteś jedyna i kluczem nie jest użycie mechaniki a przekazanie kultury.

Wracamy do kluczowego pytania - jak przekazać kulturę rozgrywki. Odpowiedź: ‘co mierzysz i nagradzasz, to dostajesz’. Jest to jedyny sposób jaki znam który zapewnia odpowiedni wynik. Popatrzmy na SEF:

![Strategic Execution Framework](Materials/240116/240122-001-sef.png)

Kultura pochodzi z ‘tego czym jesteś’. Ale to czego nie pokazuje ten framework to to, że odpowiednio zbudowane procesy i mechanizmy oceny zmieniają kulturę. Jako przykład - masz 3 przyjaciół pracujących w tej samej firmie. Jednak system oceny polega na tym, że jedna osoba dostanie podwyżkę a najsłabsza zostanie zwolniona. W firmie pracuje 5 osób. Taki mechanizm powoduje, że przyjaźń tych 3 ludzi się rozpadnie - nie możesz nikomu pomóc, bo na serio pomagasz konkurencji. Tak zaprojektowane procesy zmieniają kulturę firmy na bardzo agresywną, ‘tylko jeden może wygrać’.

Jeżeli tak popatrzymy na sytuację, to dużo mniejsze znaczenie ma to z czym gracze weszli do gry a dużo większe co ja nagradzam i co zauważam. Nagrody mogą być zarówno na poziomie społecznym - powiem coś w stylu ‘świetny pomysł’ albo ‘Agencja byłaby z Ciebie dumna’ jak i na poziomie mechanicznym - czyli im lepiej odgrywasz tym więcej ptaszków uznaniowych dostajesz.

I teraz przeanalizujmy to w jaki sposób ja robię scenę zero. Każda scena Zero zawiera te same elementy:

* pokazanie potworności anomalii, z którą mają do czynienia
* pokazanie ludzkiego cierpienia wywołanego przez anomalię jak w każdym horrorze lub morderstwie
* pokazanie tonu gry – (X) może być wykorzystany do zabicia kluczowej osoby. Można negocjować (X).
* pokazanie eskalacji – (V) a potem kolejny (V)

A po scenie Zero podkreślam - jesteście agencją, macie zniszczyć problemy z magią i zabezpieczyć by nikt o magii nie wiedział. Chcecie pomóc ludziom (mówię to wprost) ale nie jest to Waszym priorytetem. Nie ma niczego czego od nich potrzebujecie poza działaniami doraźnymi. Takim pozycjonowaniem ustawiam ich jako osoby, które nie zyskują na tym, że krzywdzą stację czy jej mieszkańców.

I jakby tego było mało mówię im o tych innych zespołach, które coś zepsuły, coś poprawiły. Nagle gracze chcą być lepsi od poprzednich zespołów. A kto będzie to oceniał? Ja. Pod kątem jakich kryteriów? Pod kątem tego co powiedziałem z perspektywy Agencji. I jeszcze nakładam im licznik niegodziwości.

Nie zmienia to faktu, że praktycznie zawsze na jakiejś sesji ktoś powie coś w stylu ‘kurczę trzeba rozwalić tą dziennikarkę’. I ja wtedy pytam jako MG: ‘serio chce zrobić krzywdę kobiecie, która robi wszystko co może żeby pomóc tej stacji mając wiedzę jaką ma? Która Wam jeszcze pomagała? Nie znajdziecie lepszego rozwiązania jako obrońcy ludzi?’ Zwykle gracze się reflektują. Bo to nie byliby agenci którymi chcieliby być, gdyby to był prawdziwy świat. **To nie byłoby to to czego aspirują**.

W ten sposób próbuję przekazać kulturę EZ.

Gdybym znowu prowadził Karolinusowi i Elenie, co zrobiłbym inaczej?

* Dałbym im jawny tor reputacji każdej z osób – reputacja Karolinusa, reputacja Eleny.
* Dałbym im jawny i jednoznaczny cel. Coś co mają osiągnąć jako zespół. Jako tor.
* Pokazałbym im, jakie konsekwencje mają ich ruchy, że są jak superbohaterowie w świecie zwykłych ludzi.
* Dałbym im opiekuna silniejszego od nich, który by ich jawnie kierował czego od nich oczekuje. Taki ‘senior wśród juniorów’.
* Powiedziałbym im, jakie są oczekiwania wobec tienów. Co by się stało, gdyby ich nie było.
* Pokazałbym im osoby, którym pomogli - w formie pytań generatywnych. Wprowadziłbym te osoby.
* Silnie nagradzałbym za zachowywanie godne tiena, silnie zniechęcałbym zachowania niegodne tiena. (nie mam jeszcze silnych implementacji)

Ten problem jeszcze nie jest w pełni rozwiązany. Dlatego robię te wszystkie sesje z randomami. Ale takie pytania jak to i takie analizy zbliżają mnie do tego rozwiązania.

---
## 5. Dlaczego gracze robią podłe czyny? Co robić w sytuacji PVP?

Plus, rozwiązywanie konfliktów w drużynie - co jeśli pół drużyny chce x a pół y? Np część x chce zrobić coś niegodziwego czego część y by nie zrobiła. Normalnie bym im pozwoliła, każda postać działa niezależnie przez chwilę i robi co uważa, to może być ciekawe. Ale jak to rozwiązać przy eztce?

Wyjątkowo na tej sesji miałam igora, więc poszło dość gładko. Graczki chciały zrobić "nikczemną akcję" więc Igor się wycofał i im pozwolił, ale nie bierze jako postać w niej udziału. Ale np. jakby zdecydował się poinformować kapitana i im przeszkodzić/powstrzymać [kapitan byłby przeciwny]?

**Odpowiedź**

Popatrz - masz sytuację, w której jeden zespół chce zrobić zły czyn a drugi chce zrobić czyn dobry. Jeden chce uratować załogę stacji, drugi chce załogę stacji zniewolić. Dlaczego?

Jeżeli uważają, że dzięki temu zarobią więcej, to znaczy, że dla nich pieniądze są kluczowym komponentem. Ale popatrz na większość bogatych ludzi - ich celem nie jest zarabiać więcej pieniędzy tylko zmienić rzeczywistość w stronę, która ich interesuje.

Bo pieniądze to tylko środek. Kupienie cudzego czasu.

Dlatego powiedziałem, że to jest kwestia **pozycjonowania postaci**. Gdy prowadzę Agencję, oni są pozycjonowanie jako efektywnie mający nieskończoną ilość pieniędzy i mający usunąć problemy z magią. Ale inni traktują ich jak zbawicieli. Inni patrzą na nich jak na tych potężnych, którzy mogą im pomóc.

Czyli pieniądze nie mają dla nich znaczenia.

Dlatego oni skupiają się na tym, by pomóc. Lub przynajmniej nie robić krzywdy. Bo mogą grać tymi **do bycia kim aspirują**.

Jeżeli kapitan jest osobą kompetentną i kapitan chce pomóc i oni polubili kapitana - mało prawdopodobne, by chcieli zniewolić wbrew woli kapitana. Nie robisz takich rzeczy komuś kogo lubisz. Jeśli oni nie martwią się o pieniądze, to gdy powiedzą ‘bo więcej kasy i zysku’ możesz ich zawsze spytać ‘no i? Nie macie problemu z kasą.’ Nowa Intencja.

Nic co nie ma INTENCJI ("co dzięki temu się stanie w fikcji") nie wejdzie do fikcji. Intencja zniewolić załogę vs intencja pomóc załodze -> CZEMU zniewolić CZEMU pomóc?  Musimy iść w głąb.

Jeśli 'zniewolić dla zysku i władzy vs pomóc humanitarnie dla swojego sumienia' to pojawia się nowe pytanie 'czyli jeśli można pomóc dla zysku i władzy to też Wam pasuje?' Pomóc graczom znaleźć **w systemie negocjacyjnym tego co im razem pasuje**. Tak jak ja to zrobiłem przy Klaudii i Ariannie gdy mówiły o przyszłości Bladawira.

Istotne jest to, że postacie mogą robić akcje wobec siebie sprzeczne jak długo graczom to pasuje. Można postawić to jako obszar nieoznaczoności. Np.: Klaudia próbuje wkopać Bladawira, Arianna próbuje Bladawira przed Klaudią ocalić. Jest to wizja przyszłości, którą całkowicie potrafię sobie wyobrazić i wiem, że zarówno Ty jak i Kić będziecie się przy tym świetnie bawić. Ale **Wy obie akceptujecie tą nieoznaczoność i to, że postacie walczą przeciwko sobie a Wy jesteście na tej samej historii i razem odkrywacie co się stanie.**

Tu mówimy o bardziej zaawansowanych technikach, które niekoniecznie warto od razu nałożyć na randomy. Wolę mocniej pozycjonować randomy by takich sytuacji po prostu nie było. Ale mogę w sumie sprawdzić na jednej sesji z randomami co się stanie - czy przyjmą to, że ich postacie robią rzeczy częściowo sprzeczne wypełniając różne tory i wynik sesji będzie wynikową tego wszystkiego.

Inną rzeczą, którą warto uwzględnić jest Mroczna Agenda. Na Twojej sesji proponowałbym, żeby to był The Devastator - coś co niszczy Łódź Podwodną graczy i jeżeli nie zrobią czegoś to będą uwięzieni na stacji wraz z resztą nieumarłych. To by spowodowało, że gracze zadadzą pytanie inaczej. Nie ‘co możemy zrobić z tymi ludźmi, którzy są na naszej łasce’ a ‘co możemy zrobić z tymi ludźmi wiedząc, że jeżeli czegoś nie zrobimy to będziemy tu uwięzieni z nimi’. Pojawia się Dark Future oraz presja.

Jak słusznie zauważyłaś, EZ prowadzi się zupełnie inaczej i jakkolwiek jest prostsze do prowadzenia, wymaga konkretnych klocków w odpowiednich miejscach. Inaczej po prostu nie zadziała.

---
## 6. Jak zbudować buy-in graczy w świat i sesję?

I tu dochodzimy do agregacji wszystkiego co było do tej pory. Gdy miałem przyjemność prowadzić grę o nazwie ‘Paranoia’, nie będzie żadnej formy buy-in. Graczom nie będzie zależało na próbie uratowania tych ludzi, którzy tam się znajdują. Gra będzie bardzo zabawna, będzie bardzo emocjonująca, jednocześnie gracze nie budują wspólnej historii a robią fajne akcje przeciwko sobie.

Czemu?

Bo każda postać jest przejaskrawiona i przerysowana. Ich życie jest stracone przez sam fakt, że się urodzili w tym świecie. NPCe nawet nie aspirują do bycia kompetentnymi. Próbują przetrwać kolejny dzień, w dystopii pod kontrolą Mojego Przyjaciela Komputera.

EZ jest zupełnie innym systemem.

* Nakładam silną presję na graczy na każdej sesji - konkretna anomalia, konkretne osoby, które mogą ucierpieć, demonstracja okrutnych krzyżyków. To sprawia, że gracze mają poczucie, że naprawdę coś wygrali i coś osiągnęli.
* Każda kolejna sesja jest kontynuacją innej. To jest jedna stacja (lub jeden statek kosmiczny). Jeśli gracze chcą osiągnąć coś dużego to im pozwalam. Mogą zmienić kulturę stacji. Mogą zabić członka rady. Ich decyzje mają wagę i znaczenie. I są trwałe. To nie to, że ja im na coś pozwalam - oni faktycznie to decydują.
* Wszystkie postacie próbują być kompetentne w swoim zakresie kompetencji. Wyobraź sobie Anię. Ania dostała się do centrum dowodzenia za nepotyzm. Ania jest niekompetentna jak cholera. Ania o tym wie. Ania siedzi po nocach i rozpaczliwie się uczy i staje się troszeczkę, troszeczkę lepsza, mimo że inni patrzą na nią z pogardą. I nagle – pożar. Ania jest uwięziona w systemie dowódczym. Nie opanowała sposobu ewakuacji. Nie zdążyła. Nie chciałbyś jej pomóc? Nie uważasz, że jest **warta pomocy**? Nie chcielibyśmy dać jej szansy? Śmierć Ani po tym jednym paragrafie jest rzeczą smutną, bo Ania jest prawdziwą osobą, którą wszyscy znamy lub potrafimy sobie wyobrazić.
* Świat oraz sesja są nieprawdopodobnie logiczne. Jeżeli rozumiesz, jak działa magia, potrafisz przewidzieć efekty pojawienia się danej energii na danym terenie. Jeżeli rozumiesz, jakie są ograniczenia serwopancerzy, jesteś w stanie wymanewrować i pokonać serwopancerz w chowając się w bagnie. To sprawia że zwycięstwo naprawdę jest osiągnięte i czujesz dumę ze swoich pomysłów i swoich działań.

To są klucze do tego, by ludzie potrafili zaakceptować i zrobić buy-in w EZ. Tam są postacie, które znasz i jakkolwiek nie wszystkie lubisz, ale są prawdziwe. I Ty grasz postacią, którą aspirujesz by być w prawdziwym świecie.
