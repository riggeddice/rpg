# Analiza: Jakie subsystemy mechaniczne są nam potrzebne (240113)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Jakie podsystemy są nam potrzebne by móc działać prawidłowo?
* Bez jakich systemów i JAK system jest uboższy?
* Co działa a czego nie mamy?

---
## 2. Spis treści i omówienie

1. Jakie podsystemy mamy, potrzebujemy i istnieją - omówienie
    1. Intro - zidentyfikowane subsystemy
    2. Subsystem rozgrywki w oparciu o różnicę wizji
    3. Subsystem wysokoadrenalinowy: pościgi, walki itp.
    4. Subsystem projektów: konstrukcja długoterminowa jakiegoś bytu
    5. Subsystem endgame sesji: wynik sesji, jak się sesja potoczyła
2. Przykłady wykorzystania podsystemów z istniejących sesji (z komentarzami)
    1. (dla każdego podsystemu jest przykład z omówieniem, z sesji)

---
## 3. Jakie podsystemy mamy, potrzebujemy i istnieją - omówienie
### 3.1. Intro - zidentyfikowane subsystemy

EZ jako system RPG czułby się świetnie, gdyby wspierał cztery główne subsystemy. **Pierwszy system** jest wystarczającym do prowadzenia sesji, ale cztery byłyby idealne.

1. Subsystem rozgrywki: konflikt w oparciu o różnicę wizji
2. Subsystem wysokoadrenalinowy: pościgi, walki itp.
3. Subsystem projektów: konstrukcja długoterminowa jakiegoś bytu
4. Subsystem endgame sesji: wynik sesji, jak się sesja potoczyła

Omówmy poszczególne subsystemy.

---
### 3.2. Subsystem rozgrywki w oparciu o różnicę wizji

* Co to jest?
    * Jest to główna "mechanika EZ", jak opisana w [Notatce Stabilizacyjnej 230603](230603-mechanika-2305-stabilizacja)
    * Służy do głównej gry gdzie różne osoby mają różnice wizji co się dzieje przy stole
* Co by było gdyby go nie było?
    * EZ nie funkcjonuje. Szczęśliwie, ten subsystem jest 'perfekcyjny' (czytaj: działa)
    * Jeżeli tego podsystemu by nie było - MG prowadzi grę stuprocentowo uznaniowo. To się udaje co powinno się udać, nic innego się nie udaje. Właśnie ten podsystem odróżnia EZ od większości pozostałych gier.
* Czemu jest taki?
    * Rdzeniem EZ jest to, że każdy konflikt da się łatwo zasymulować. Niezależnie na jakie pytanie nie szukamy odpowiedzi istnieje możliwość zdobycia tej odpowiedzi.
    * Nie jest to najszybszy podsystem, ale daje pełnię możliwych rozwiązań

Podsystem oparty o różnice wizji na poziomie meta sprawia, w rzeczywistości grę kształtują zarówno gracze jak i MG. MG tworzy kanwę, gracze ją wypełniają i sprawiają, że linie, które nakreślił MG niekoniecznie się sprawdzą. Możliwość skonfliktowania prawie każdego elementu umożliwia wykroczenie poza standardową sesję jeżeli wszyscy się na to zgodzą.

Kluczowy i główny system EZ. Mam wrażenie, że jest dość unikalny dla EZ.

---
### 3.3. Subsystem wysokoadrenalinowy: pościgi, walki itp.

* Co to jest?
    * Podsystem odpowiedzialny za szybkie i dynamiczne działania. Walki czy pościgi.
    * Jednym z głównych celów tego podsystemu jest uczucie adrenaliny i napięcia.
    * Bardzo ważny jest szybkie przełożenie deklaracji i decyzji na fikcję. Ten podsystem najprawdopodobniej jest systemem akcyjnym a nie intencyjnym, gdzie intencja jest dość domyślna.
* Co by było gdyby go nie było?
    * Fox będzie smutna i musi prowadzić Warhammera
    * Sceny walki albo sceny w których detale mają duże znaczenie i adrenalina jest istotna będą musiały wykorzystywać główny subsystem rozgrywki w oparciu różnicę wizji.
    * To sprawia, że gra jest dużo wolniejsza i jakkolwiek sceny będą dobrze zasymulowane, ale nie będzie tej adrenaliny; nie będzie tej prędkości.
        * Nie ma "prawdziwych walk", są szachy.
        * I Fox musi prowadzić Warhammera. Co jest akceptowalne (jeśli chce), ale niepożądane (jeśli woli EZ a musi Warhammera bo nie ma adrenalinowego systemu).      
* Czemu jest taki?
    * Nie działa i nie istnieje w chwili obecnej.
    
Czekam na przykład dobrej walki przez Fox (co modeluję?).

---
### 3.4. Subsystem endgame sesji: wynik sesji, jak się sesja potoczyła

* Co to jest?
    * Podsystem odpowiedzialny za progresję celów w ramach pojedynczej sesji
    * Jednym z głównych celów tego podsystemu jest postępowanie celów Drugiej Strony w ramach pojedynczej sesji; zarządzanie Adwersariatem przez MG.
* Co by było gdyby go nie było?
    * To jest w pewien sposób takie Grand Finale sesji. Ile udało się postaciom osiągnąć. Ile udało się osiągnąć przeciwnikom. Jak zakończyła się tam opowieść i jak zmienił się świat w jej wyniku.
    * To sprawia, że wszystko jest 100% uznaniowe z perspektywy MG.
* Czemu jest taki?
    * Dzięki Torom jesteś w stanie patrzeć jak głęboko doszło do dojścia do celu przeciwnika albo sojusznika.
    * Dzięki Dark Agenda wiesz, jaki jest ostateczny cel przeciwnika i jakie kroki będzie robił.
    * Dzięki Dark Future wiesz o co grają jakie strony i co ważniejsze - jak będzie wyglądał świat jeśli gracze nic nie zrobią

W tej chwili system Adwersariatu, zarządzania nim i Endgame jest realizowany częściowo przez Tory a częściowo przez Dark Future i Dark Agenda. Nie jest to w pełni rozbudowany system i będę musiał nad nim jeszcze sporo popracować.

---
### 3.4. Subsystem projektów: konstrukcja długoterminowa jakiegoś bytu

* Co to jest?
    * Podsystem odpowiedzialny za wykonywanie wieloetapowej akcji, wieloetapowego projektu.
    * Różne osoby robią różne rzeczy, np. "projekt szkolny" albo "piszemy skomplikowany software razem".
* Co by było gdyby go nie było?
    * Na serio mechanizm dekompozycji torów / wizji i celów działa wystarczająco dobrze, ale tego nie widać.
* Czemu jest taki?
    * bo jest elementem większej całości w ramach szerszej mechaniki
    * używamy dekomponowanych Torów by określić co kupujemy i ptaszkami kupujemy określone sukcesy

---
## 4. Przykłady wykorzystania podsystemów z istniejących sesji (z komentarzami)
### 4.1. Subsystem rozgrywki w oparciu o różnicę wizji
#### 4.1.1. Przykład 

Najlepszy będzie przykład z Notatki Stabilizującej. Poniżej (pomiędzy wykrzyknikami).

Weźmy przykład dla zobrazowania sytuacji.

```
Planetoida Seikir. Atak piratów. Rodzina: Alicja, Bartek (Gracz) i dwójka dzieci. Są w swojej kwaterze.
Wpada pirat Szczepan (NPC), w ciężkim serwopancerzu szturmowym. Rodzina jest nieuzbrojona i niegroźna.
Szczepan dostał za zadanie znaleźć wartościowe dane i zabić wszystkich.
Bartek bardzo by chciał, by jego rodzina przeżyła.
```

* Jak wygląda świat idealny Gracza?
    * najważniejsze: Pirat nie zabija jego rodziny ani jego postaci
    * drugorzędnie: zapewnić bezpieczeństwo rodzinie
    * trzeciorzędnie: uzbroić się i pokonać piratów?
* Jak wygląda świat idealny MG?
    * Nie zależy **jemu** na zabiciu Bartka, nawet, jeśli Szczepan powinien go zabić. Wolałby by Bartek przeżył. Ale wiecie, gramy ostro ;-).
    * Zabicie rodziny Bartka jest rzeczą której nie chce, ale akceptuje.
    * Piraci wpadają, dostają cenne dane i wypadają
    * Nikt nie wie kim byli piraci lub że w ogóle byli tu piraci. Siły policyjne mają się pojawić i znaleźć opustoszałą planetoidę bez cienia śladów.

Czyli jak teraz będzie wyglądał konflikt?

* MG: "Co chcesz osiągnąć?"
* Gracz: **"Chcę postrzymać pirata przed zabiciem mojej rodziny"**
* MG: "Oki - jak chcesz to osiągnąć?"
    * MG wie już **co** Gracz chce. Teraz może przeanalizować w jaki sposób propozycja Gracza zadziała.
* Gracz: "Krzyczę do rodziny 'Uciekajcie!', po czym rzucam się na pirata. Nie uda mi się go pokonać, ale przynajmniej go spowolnię by byli w stanie uciec. Akceptuję, że zginę."
* MG: (procesuje)
    * serwopancerz sprawia, że pirat ma siłę ~50 kg na jednej ręce. Spowolnienie - nieefektywne. Czy Gracz tego nie wie? Źle rozumie sytuację?
    * NAWET jeśli rodzina ucieknie ze swojej komnaty, jest wielu piratów. Strategia nieskuteczna.
* MG -> Gracz: (wyjaśnia problem). "Czy podtrzymujesz deklarację? Najpewniej mówimy o czymś bliskim poziomowi heroicznemu i więcej niż jeden ptaszek."
* Gracz: "Nie, to może... dobra, piraci atakują z jakiegoś powodu. Może jeśli udowodnię, że jesteśmy bardziej przydatni żywi niż martwi to może uratuję rodzinę"
* MG: (procesuje)
    * Gracz nie wie czemu tu są, ale piraci mają konkretny cel
    * Jest niemała szansa, że się uda coś tu załatwić. Pirat Szczepan ma swoją rodzinę i nie lubi zabijać ludzi. Nie dlatego jest piratem bo jest sadystą a dlatego że ma długi i nie zna innego życia.
        * ((nawet jeśli wcześniej tego faktu nie było, w tym momencie MG może go dodać, bo czemu nie? Lepsza opowieść.))
        * ((jeśli nie dodamy, można wygenerować ten fakt mechaniką - Gracz może DODAĆ ten aspekt do pirata Szczepana na przestrzeni konfliktu))
* MG -> Gracz: "Dobrze, to może zadziałać, tu są pewne pola manewrów."
* Gracz: "Oki. To:"
    * **"Wpierw pokażę piratowi że jesteśmy niegroźni i że jesteśmy chętni pomóc"**
    * **"Potem dowiem się czemu piraci tu są i jak mogę im pomóc by ochronić rodzinę"**
    * **"I potem pomyślimy co dalej XD"**
* MG: (procesuje)
    * Jest konflikt, są określone cele i zakresy sukcesów
    * Konflikt będzie w Teatrze Społecznym i/lub Emocjonalnym, potencjalnie Umysłowym
        * Konflikt na pewno nie będzie w Teatrze Fizycznym; tam serwopancerz daje upiorną przewagę
    * Możemy przejść do deklaracji i konstrukcji puli.

---
#### 4.1.2. Omówienie

Tego nie trzeba już usprawniać ani nie trzeba tego zmieniać. Ten subsystem funkcjonuje perfekcyjnie z perspektywy tego co chcę osiągnąć.

Gracze mają kilka sposobów rozwiązania każdego problemu, łącznie ze sposobami. których sam Mistrz Gry nie zna. Na przestrzeni negocjacji oraz na przestrzeni modyfikacji czasoprzestrzeni (zarówno konflikty wsteczne jak i przednie, zarówno w obszarze bliskim konfliktowi jak i odległym) budujemy stabilną opowieść i modelujemy każdy możliwy konflikt.

Ten subsystem sam - tak jak jest - pozwala na zbudowanie każdej sensownej opowieści. Pozostałe subsystemy skupiają się na tym jak uatrakcyjnić rozgrywkę oraz jak wzbogacić wpływ graczy na rzeczywistość.

---
### 4.2. Subsystem wysokoadrenalinowy: pościgi, walki itp.
#### 4.2.1. Przykład

Przykład z sesji '240102-zaloga-vishaera-przezyje':

Załoga Talikazera wycofuje się zgodnie z poleceniami do śluzy, gdzie Helmut z poziomu Serbiniusa ma ich wziąć na pintkę. Klaudia i Martyn ich asekurują. W pewnym momencie Martyn odepchnął Klaudię na ścianę. Fragment 'pleśniomasy' przeciął miejsce gdzie Klaudia była i formuje coś na kształt humanoida skrzyżowanego z hydrą złożonego z pleśni. 

BAZA: +3Vz (servary są pancerniejsze) +3V (dwie osoby i Martyn umie walczyć) +5X (dudnienie magii i niestabilność terenu i terrorform na swoim terenie).

* Terrorform się rozdziela i każdą witką ostro atakuje wszystkich, pajęcza zmienna sieć ale z pleśni (2)
* Martyn bierze jakieś elementy sprzętu (coś co zabraliście jako tarczę) i zasłania. Klaudia używa _uszczelniacza_ servara by uszkadzać i redukować biomasę (2)
    * X: Sprzęt komunikacyjny z Serbiniusem został zniszczony. Martyn musiał zrobić manewr unikowy i po prostu nań padł.
* Terrorform skupia się na ataku w załogę Talikazera. Nieważne KTO zostanie. Biomasa musi być. (3Xb)
* Klaudia przesuwa się by wziąć to na pancerz servara. I krzyczy "nie potrzebujesz tego jesteśmy tu by ratować ludzi. Ta energia NIE BĘDZIE POTRZEBNA!" (2Vr)
    * V: Terrorform nie ustępuje, ale pancerz Klaudii wytrzymał pierwsze uderzenie, choć magia sprawiła że było niesamowicie mocne. Zanim mógł uderzyć ponownie, Martyn poraził go prądem. Część biomasy obumarła.
* Martyn przesuwa się by on i Klaudia byli po stronie ludzi.
* Terrorform skupia się na ataku w nogi. Uderzyć, zablokować, unieszkodliwić. Wkręcić się w serwo. (1X)
* Klaudia elektryfikuje swój servar. (3V)
    * Vg: Terrorform próbował się wkręcić i Klaudia wysunęła się jako pułapka a Martyn osłaniał uciekających. Gdy Terrorform był 'na Klaudii', ta odpaliła elektryfikację. Terrorform zasyczał, szok go odpędził.

Terroform, ciężko uszkodzony się ewakuuje. Martyn i Klaudia osłonili załogę, po czym idą dalej.

---
#### 4.2.2. Omówienie

Aktualna operacja wyglądała w taki sposób - budujemy bazę która zwykle jest oparta o przewagi, następnie opisujemy co się dzieje, w zależności od tego modyfikujemy pulę, ciągniemy, wynik - i odpowiednio opisujemy co się dzieje, opisujemy co kto robi, modyfikujemy pulę dodając odpowiednią liczbę żetonów, kontynuujemy.

* Co zadziałało - mieliśmy wpływ i zejście na poziom niższy, akcyjny. Było dużo szybciej.
* Co nie zadziałało – initial pool. Źle pododawane żetony to środka. Ciągle za wolno.

Co warto sprawdzić:

* Pula startowa rzędu 5V5X
* 3 wymiany akcji; to sprawi, że jedna strona dostanie 1 żeton a druga [1, 3] żetony
* Dwa kolory - POZYCJA oraz RANA.

Każda wymiana prowadzi do zmiany POZYCJI albo RANY, po czym idziemy i działamy dalej.

Jak to może wyglądać:

* Pula startowa: `3Vg 2Vr 3Xg 2Xr` (gdzie g oznacza Pozycję a r oznacza Ranę)
* Wymiany opisów, krótka dynamiczna scenka. Załóżmy, żę Graczowi dobrze poszło.
    * Pula dostaje: +[`1Xg, 1Vg 1Vr`] (czyli nowa pula: `4Vg 3Vr 4Xg 2Xr`)
    * Ciągniemy, jest `Xr` - doszło do trwałej utraty zasobu / Rany (dla `Xg` byłoby dramatyczne pogorszenie tymczasowej Pozycji / niedowaga)
* Wymiany opisów, krótka dynamiczna scenka... pętla.

Coś takiego.

---
### 4.3. Subsystem endgame sesji: wynik sesji, jak się sesja potoczyła
#### 4.3.1. Przykład

Kilka przykładów. 

Z sesji '210112-elainka-kontra-caly-swiat' (lekko zmodyfikowane) - **sprawdzenie wyniku na podstawie zgromadzonych punktów**:

```
* Pula startowa ENDGAME: 0V 5X
* Na przestrzeni sesji gracze generowali (V) i (X) jako działania opcjonalne lub przez upływ czasu. Na końcu, draw 5 z opcją eskalacji.

14 (V), 7 (X) -> VVXXXVVV:

* Reaktory nie wytrzymały. Eksplozje.
* 43 osoby zginęły w wyniku tej operacji. Za duże sprzężenie z nanitkami sentisieci. Plus, eksplozje.
* Andrea potrzebuje NATYCHMIASTOWEJ pomocy medycznej
```

Jeszcze wcześniej nie sprawdzaliśmy wyniku, kupowaliśmy go. '181021-powrot-minerwy-z-terrorforma', **Kupowanie zmiany w świecie**:

```
**SCENA:**: Adam dowiaduje się gdzie jest Erwin; Lilia zostaje zmuszona do współpracy z Adamem. (18:40)
...
(Oki, to sprawia, że Pięknotka skontaktowała się z Ateną Sowińską... powiedzenia tego, co wie (Konflikt Typowy))

* Ż: 3
* K: 1

I potem na końcu sesji lub w kluczowym momencie:

(Ż: -5: ciężko ranny Galilien)...
```

Używaliśmy też bardziej zaawansowanych torów, podpiętych do Dark Future. '181104-kotlina-duchow', '190422-pustogorski-konflikt':

```
W podobny sposób tory rosły.

* Player Devastation (1k3): 14
    * 4: Skażenie magiczne czarodziejek
    * 8: Skażenie i rany; coraz gorzej się walczy
    * 12: Ciężko ranne czarodziejki + poddające się glukszwajny
    * 16: Portal do innego Świata ("wróć do domu") + Pastmaster
* Długość toru kupowania czasu: 13
    * 5: wsparcie Olgi
    * 10: wsparcie Epirjona
    * 15: wsparcie Pustogoru

LUB:

2 Wpływu -> 50% +1 do toru. Tory:

* Król Grzymość: /5
* Wojna Pustogorska: /3
* Eksodus Miasteczka: 2/5
* Cierpienie Ludzi: 2/4
* Ranna Pięknotka: /4
```

Czasem nie używaliśmy Torów a konkretnych Zasobów i uderzaliśmy w nie bezpośrednio (podchodzi pod mechanikę projektów). **Zarządzanie Zasobami na sesji**. '221022-derelict-okarantis-wejscie':

```
Zasoby: FINANSE, SPECJALISTYCZNE, METAL, ENERGIA, WODA, ZYSK

* Przykład zmiany: "Czyli przed nimi najpewniej 3tygodniowa / miesięczna wyprawa. (FINANSE-10)"
* Przykład zmiany: "TrZ (dokumenty PLUS skany dron) +3 -> X: Pierwsza "śluza intruzywna" się nie udała i marnuje... (SPECJALISTYCZNE-1)"

Koniec sesji: 

* SCA Hiyori: strasznie wykosztowała się z zasobów: FINANSE: -10, SPECJALISTYCZNE: -7, PROTOMAT+1, ENERGIA-1, WODA-1, DANE+1, (ZYSK, CENNE, RARE_MAT):0
```

A czasem używaliśmy ekstra Entropicznych do sterowania tą sytuacją.

---
#### 4.3.2. Omówienie

System Endgame jest bardzo interesującym podsystemem, jednocześnie będąc dość… nietypowym. Ten podsystem pozwala na prawidłowe zarządzanie tym co się dzieje w świecie niezależnie czy postacie graczy coś robią czy niczego nie robią.

Ten system musi być podpięty zarówno do czasu rzeczywistego (by generować presję) jak i do mechaniki rozwiązywania konfliktów (by druga strona także wykonywała działania). Jeżeli jest zbyt ostro wyskalowany, gracze nie są w stanie w żaden sposób niczego zrobić, albo są w stanie rozwiązać jeden problem z 5 i będą nieusatysfakcjonowani.

Jeśli jest zbyt łagodnie wyskalowany, koszt zarządzania tym systemem dramatycznie przekracza ewentualne korzyści z niego.

Z perspektywy AW ten podsystem rozwiązany był przez zegary. Zegary jednak nie działają tak jakbyśmy się spodziewali - nigdy nie wiadomo kiedy jak je przesuwać i jak je interpretować. W tej chwili rozwiązuje ten problem używając frakcji, agend i torów, ale ten podsystem nie jest skończony i jeszcze nie działa tak dobrze jak bym sobie tego życzył.

To znaczy - prawidłowo mnie kieruje i mówi mi jak prowadzić sesję. Ale nie pokazuje mi aktualnego stanu sesji w takim stopniu jakbym sobie tego życzył. Próbowałem to rozwiązać wykorzystując Komponenty Sesji, np. Struktury. Niestety, po stronie mg nie mam tego narzędzia rozwiązanego tak jakbym sobie tego życzył.

Trudność tego podsystemu polega na tym, że z jednej strony chcesz penalizować głupie ruchy graczy i marnowanie przez nich czasu, ale z drugiej strony jeśli nie zostawisz graczom żadnej swobody i jeszcze jesteś na łasce generatora losowego to jest poczucie bardzo nieuczciwej sesji nie z winy graczy.

I rozwiązanie tego będzie bardziej trudne niż mi się wydawało.

---
### 4.4. Subsystem projektów: konstrukcja długoterminowa jakiegoś bytu
#### 4.4.1. Przykład

Brak z sesji. O dziwo.

---
#### 4.4.2. Omówienie

Ile nie pracowaliśmy nad mechanikami projektów, zawsze jakoś to działało i nigdy jeszcze tak nie było, by jakoś nie było. Zwykle finalizowaliśmy mechanikę projektów przez połączenie torów z konkretnymi rzeczami, które chcieliśmy osiągnąć. W obie strony. 

Przykładowo:

```
Chcemy zrobić królewski posiłek na imprezę.

* ZASOBY
    * Czas: OK, na szybko, katastrofa
    * Pieniądze: OK, spory koszt, katastrofa
* CELE
    * Istnieje posiłek.
        * Ma Smak: kiepski, dobry, świetny
        * Wizualnie: kiepsko, dobrze, świetnie

Ptaszki pozwolą prowadzić W PRAWO cele, Krzyżyki w PRAWO zasoby (zużycie) lub jakieś nieoczekiwane efekty uboczne. Czyli np. VVXVX można kupić: "istnieje - dobry - świetny - kiepski wizualnie" z kosztem: "wyrobiliśmy się na szybko - ze sporym kosztem".
```