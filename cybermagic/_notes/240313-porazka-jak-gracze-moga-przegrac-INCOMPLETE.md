# Porażka - jak gracze mogą przegrać?

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Jak gracze mogą przegrać?
* Jak wygląda przegrana w EZ?
* Czy sesję w EZ da się przegrać?

---
## 2. Spis treści i omówienie

1. Co to jest porażka?
    1. Kilka różnych soczewek
    2. Obserwacje z soczewek
    3. Konkluzja
2. Ogólny problem

---
## 3. Co to jest porażka
### 3.1. Kilka soczewek

1. Forbes: (https://www.forbes.com/sites/carleysime/2019/01/28/can-we-please-redefine-failure-and-success/ )
    * Failure is defined as an absence or lack of success.
        * The main problem here is that most of us aren’t great at defining success
    * Different framing
        * Success. The state of living and working according to our values
        * Failure. The state of living and working in a way that isn’t aligned with our values
2. Play Videogames To Fail: (https://bigthink.com/neuropsych/play-video-games-to-fail/ , "Art of Failure")
    * Failure feels awful, so people avoid it as often as they can.
    * When failing a game’s challenge, Juul notes, a player discovers a deficiency in their ability or approach.
    * Rather, failure is a key part of the experience.
        * players prefer games where they feel responsible for failure, not games in which success is guaranteed
    * Game that does not offer the player a chance (presenting an insurmountable obstacle, not informing the player of its internal rules) is considered broken
    * A player is first introduced to a goal, fails at the goal to reveal an inadequacy, and then searches for a solution
        * Dark Souls: You’ve fought this boss many times. You are now aware of his/her/its patterns and you know how to react to each one.
    * Well-designed video game provides us with clear goals, a fair chance at success, and rewards us for achieving.
        * The real world, in contrast, makes no such promises. Goals can be opaque, success perpetually out of reach, and rewards ill-defined.
    * Failure was not only a ‘problem’ but also a critical precondition for learning
3. Games that allow players to **feel responsible** for their failures...

### 3.2. Obserwacje z soczewek

* Czyli RNG to potencjał na porażkę, ale jest adaptacja. Porażka musi być konsekwencją graczy / ryzyka.

## 4. Ogólny problem

Mam pytanie, na które nie potrafię odpowiedzieć.

Jak wygląda **PORAŻKA** w sesji RPG, przy założeniu że osoby przy stole się dobrze bawią?

Dam przykład:

* Jeśli gram w Unreal Tournament, mogę się świetnie bawić, ale nie mam pierwszego miejsca. Czyli przegrałem, ale bawiłem się dobrze.
* Jeśli gram w Civilization / Master of Orion, mogę się świetnie bawić ale przeciwnicy "wygrali grę". Czyli przegrałem.
* Jeśli gram w piłkę nożną, mamy drugie miejsce. Czyli przegraliśmy. Wszystko stracone!
* W obu przypadkach się uśmiecham, adrenalinka wysoko, mogę grać jeszcze raz.

Zwykle (zgodnie z definicjami):

* Sukces: osiągnięcie celów (tu "wygranie gry / pierwsze miejsce")
* Porażka: 
    * różnica między wynikiem a osiągniętymi celami ("mam trzecie miejsce")
    * utracenie zasobów uniemożliwiających osiągnięcie celu ("jest 10 minut do końca, a wynik w piłce nożnej jest 7:1 na naszą niekorzyść")
    * utracenie okoliczności na których mi zależy ("wygrałem grę, ale musiałem militarnie bo nie dałem rady dyplomatycznie a chciałem!")

Ale jak wygląda PORAŻKA w RPG, przy założeniu zachowania zasady dobrej zabawy wszystkich obecnych przy stole?

Autentycznie zastanawiam się, czy da się w ogóle przegrać sesję w taki sposób jak przegrywamy gry komputerowe / w sporcie:

* Świat - oraz mechanika - jest pomiędzy uczestnikami. Czyli "zawsze ktoś czegoś nie wie" a w grze komputerowej wszystko jest jawne jak zaprojektował projektant.
* Zasady w grze komputerowej są silnie sterowane, w RPG musisz bardzo dużo improwizować i generować na bieżąco. Czyli - niezależnie od 'realizmu' - coś jest niejasne, niejawne i nie wszystko jest perfekcyjnie zrozumiałe dla wszystkich obecnych.
* Dużo zależy od pozycjonowania ze strony MG - jeśli otoczysz pierwszopoziomowego woja 4 czarnymi smokami, GG NOOBZ. Porażka, niezasłużona.
* W grze komputerowej możesz grać jeszcze raz. 
    * Przykładowo, strata postaci / utrata wątku sprawia, że już się nie da jeszcze raz.

Co mamy:

* Kić: dla mnie porażka postaci - próbowaliśmy uratować postać Anastazji, ale w wyniku kiepskiego planu / złych rzutów / ... i mimo naszego uporu się nie udało. Poświęciliśmy dużo i i tak nie osiągnęliśmy celu.
    * -> Kić: **akceptowalne lub nie, zależy od rodzaju sesji i tego, co chcemy**
* Żółw: "gracze podjęli OPCJONALNĄ grę o wysokiej stawce, mieli TPK wiedząc o szansie przed rzutem"
    * -> Żółw: **gracze KONTROLOWALI warunki i ROZUMIELI stawki, podjęli ryzyko i nie wyszło** (nie dość dużo danych, pechowe rzuty)