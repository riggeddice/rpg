# Shard

## Pytania

1. Jak to zrobić, by postacie nie były podobne do siebie, by Gracze grali postaciami? By postacie MG też się różniły?
2. Sposoby działania postaci

## Tag

rpg

## Kontekst

* The Positive Trait Thesaurus
* https://en.wikipedia.org/wiki/Enneagram_of_Personality
* http://znaki-zodiaku.com.pl/
* Panty Explosion

## Odpowiedź

### 1. Wykorzystanie

* Każda postać ma swoją kategorię "sposób działania" (#4).
* Gracz wybiera DWA traity mające siły i słabości.
* Z czego buduje / wybiera 3 pozytywne i 1 negatywny.

Wygląda to tak:

* Daredevil, Urok
* SIŁA: działanie osobiste, bezpośrednie
* SIŁA: bezproblemowo zdobywa informacje
* SIŁA: wszyscy kochają tą postać, tą personę
* SŁABOŚĆ: nie potrafi działać ostrożnie... ani w ukryciu

## 2. Lista traitów

### Daredevil

* SIŁA: Doskonale działa przy ogromnej dawce ryzyka i adrenaliny
* SIŁA: Nieustraszony, działa osobiście
* SŁABOŚĆ: Nie radzi sobie jeśli musi unikać ryzyka lub nie ma tej adrenaliny; wybiera ryzykowny ruch

### Entuzjasta

* SIŁA: Ogromna energia motywuje innych i podnosi im morale.
* SIŁA: Niezłomność; trudno entuzjastę zawrócić czy zdemotywować.
* SŁABOŚĆ: Entuzjasta jest męczący dla innych; dodatkowo ciężko jest Entuzjaście się choć na moment zatrzymać.

### Pająk

* SIŁA: Pająk świetnie działa z drugiej linii, przesuwając pionki i obserwując wszystko.
* SIŁA: Doskonale ukrywa swoje myśli oraz trudno Pająka wyczuć.
* SŁABOŚĆ: Pająk niechętnie ryzykuje na pierwszej linii; plus, trudno nawiązać mu przyjaźnie czy kontakt.

### Femme Fatale

* SIŁA: Mistrzyni nawiązywania relacji i przyjaźni, wszędzie zdobędzie kontakty. Jej najgroźniejszą bronią jest urok i charyzma.
* SIŁA: Uwielbia działania bezpośrednie. Plotki, uśmiech, kontakty osobiste. Zdobędzie informacje i uwagę.
* SŁABOŚĆ: Trudno jednak Femme Fatale powstrzymać się przed używaniem swoich umiejętności, jak i nie lubi działać z ukrycia.

### Badacz

* SIŁA: rozwiązuje zagadki i odkrywa prawdę, potrafi znaleźć rzeczy, jakich inni nie widzą
* SIŁA: najlepiej czuje się pomiędzy dokumentacją a badanym zjawiskiem
* SŁABOŚĆ: skupiony na badaniu, często nie zauważa innych ludzi i ich potrzeb emocjonalnych

### Tyran

* SIŁA: Zastraszanie, terror oraz autorytarne rządy charakteryzują Tyrana - wie jak zadziałać, by go słuchali.
* SIŁA: Jednocześnie Tyran lubi planować i kontrolować sytuację systemami i metodycznym podejściem.
* SŁABOŚĆ: Jakiekolwiek formy zaprzyjaźniania się lub pozytywnych relacji nie zadziałają wobec kogokolwiek kto zna Tyrana.

### Wesołek

_Świat jest teatrem a wesoły żart potrafi zapobiec niejednej wojnie_.

* SIŁA: Wesołek jest uzbrojony w potężny arsenał żartów potrafiących rozładować niejeden konflikt. Lubi żarty i dobrą zabawę.
* SIŁA: Mało kto potrafi tak kierować uwagą innych i zapewniać ten typ dobrego nastroju.
* SŁABOŚĆ: Niestety, podejście do żartów sprawia, że często nie jest brany pod uwagę poważnie - zwłaszcza przy przesłuchiwaniu czy dowodzeniu.

### Kameleon

* SIŁA: W zależności od tego z kim ma do czynienia adaptuje się do tej osoby zachowaniem i słowami. Zupełnie, jakby Kameleon nie miał własnej osobowości.
* SIŁA: Kameleon na krótką metę dla każdego wygląda wiarygodnie. Mistrz aktorstwa i masek, każdemu wszystko wmówi.
* SŁABOŚĆ: Wśród wielu różnych osób Kameleon nie jest w stanie używać swoich kluczowych sił - każdy bowiem zna Kameleona z innej strony.

### Pierwszy Oficer

* SIŁA: nieskończenie lojalny Kapitanowi, niezłomny Oficer wykona zadanie niezależnie od kosztu.
* SIŁA: ten typ lojalności wzbudza szacunek u innych i sprawia, że Oficer może działać siłami swymi lub Oddziału.
* SŁABOŚĆ: lojalność między Kapitanem i Sprawą potrafią Oficera rozsypać. Oficer jest też przewidywalny.

### Cichy Specjalista (czegoś), np. pilot myśliwca

* SIŁA: profesjonalista w swojej dziedzinie, oddał życie Sztuce - jak nazywa swoją pracę / działanie.
* SIŁA: w obszarze swojej kompetencji porusza się doskonale, bardzo się wyspecjalizował.
* SŁABOŚĆ: nie radzi sobie z kontaktami międzyludzkimi; uważa je za ogólnie mniej istotne niż swoją specjalizację.



### Rycerz

* SIŁA: Zawsze pewny siebie Rycerz idzie na pierwszy ogień w bój. Uderza i idzie za ciosem, brawurowo.
* SIŁA: Bez problemu porywa za sobą innych i przejmuje dowodzenie.
* SŁABOŚĆ: Rycerz niechętnie zmienia plany i łatwo wprowadzić Rycerza w pułapkę przez liniowość działania.

### Artysta

* SIŁA: Przez swoje dzieła i dokonania Artysta wzbudza ogromne emocje i wywiera wpływ pokazując innym czym jest Sztuka.
* SIŁA: Artysta jest trochę nie z tego świata, odporny na krytykę i wpływy mentalne, mając ukojenie w dziełach.
* SŁABOŚĆ: Nie radzi sobie jednak przy działaniu w pośpiechu lub w sytuacjach rutynowych i powtarzalnych.

### Istota (czegoś), np. "leśnik żyjący lasem"

_"Multivirt jest moim światem. Tu jesteś na MOIM terenie."_.

* SIŁA: zna swój ekosystem jak własny dom - nieważne jak jest niebezpieczny, porusza się po tym terenie płynnie i umie z niego korzystać.
* SIŁA: gdy jest na swoim terenie, ma przewagę której inni nie potrafią osiągnąć. To dom, azyl i źródło siły.
* SŁABOŚĆ: poza swoim terenem czuje się jak ryba wyjęta z wody. Odizolowana, cierpi i próbuje odzyskać możliwość powrotu.

### Samotny Drapieżnik

_Bez polityki, bez innych ludzi, bez komplikacji. Ja i świat, który chce mnie zniszczyć. Kocham to uczucie_.

* SIŁA: nie tylko potrafi samotnie przetrwać - do tego jeszcze najpewniej pokona to, na co poluje. Działa samotnie, na wrogim terenie.
* SIŁA: radzi sobie z kryzysami, potrafi przetrwać i bez problemu potrafi skupić się na celu.
* SŁABOŚĆ: zupełnie nie radzi sobie z zależnością od innych lub z nawiązywaniem relacji. Nie ufa innym.

### Gwiazda

* SIŁA: Gwiazda przyciąga uwagę wszystkich oraz kontroluje tłumy bez większego wysiłku.
* SIŁA: Gwiazda doskonale włada ludzkimi sercami i potrafi osiągnąć większość rzeczy na linii społecznej.
* SŁABOŚĆ: Gwiazda musi być na pierwszym planie. Działanie z drugiej linii lub bycie ignorowaną ją boli.

### Konstruktor

_Jesteśmy ludźmi. Wykorzystujemy narzędzia do rozwiązywania naszych problemów._.

* SIŁA: Na każdy problem znajduje odpowiednie rozwiązanie - zbudowanie czegoś, co ów problem rozwiąże. Gadżeciarz.
* SIŁA: Metodyczny, uporządkowany i dobry w cyklu: od problemu do rozwiązania (które jest konkretnym narzędziem).
* SŁABOŚĆ: Działania szybkie, spontaniczne i / lub bez swoich narzędzi. Tak jest najgorzej - jak zwierzę.

### Wizjoner

* SIŁA: Posiada swoją silną wizję przyszłości i potrafi argumentować z pasją, porywając innych za sobą i zarażając ich swą utopią.
* SIŁA: Niezłomny, momentami ekstrawagancki, bez problemu przyciąga fanów niezależnie od tego jaka ta wizja by nie była.
* SŁABOŚĆ: Niestety, część ludzi całkowicie odrzuca i wielu staje się mu wrogimi - zwłaszcza, gdy wizja jest dla nich sprzeczna.

### Opoka

* SIŁA: Niezależnie od sytuacji, potrafi wszystkich uspokoić i ustabilizować sytuację. Niezłomny i niemożliwy do wyprowadzenia z równowagi.
* SIŁA: Działa metodycznie i ostrożnie, nigdy się nie spieszy. Jest jak woda, która drąży skałę. Dobrze planuje.
* SŁABOŚĆ: To jednak sprawia, że nie przekonuje nikogo uczuciami. Nie działa też spontanicznie - woli planowanie.

### Słodziak

* SIŁA: Metoda na "młodszą siostrę" - zawsze znajduje kogoś, kto pomoże. Uważany za całkowicie niegroźnego.
* SIŁA: Doskonałe aktorstwo połączone ze świetnym budowaniem relacji.
* SŁABOŚĆ: W sytuacji, w której te techniki nie działają bo np. mamy do czynienia z maszynami.

### Paranoik

* SIŁA: Doskonały obserwator, zawsze czujny - praktycznie niemożliwy do zaskoczenia.
* SIŁA: Bardzo cierpliwy i metodyczny - nawet mysz się nie prześlizgnie. Praktycznie się nie nudzi.
* SŁABOŚĆ: W jego obliczu nie ma pojęcia "luźnej atmosfery". Jakiekolwiek przyjaźnie też wymagają dużej pomocy.


### Negocjator

* SIŁA: Negocjator jest wzywany do rozwiązywania konfliktów oraz zdobycia kluczowych zasobów od drugiej strony - w zamian za coś mniej bolesnego.
* SIŁA: Mało kto tak jak Negocjator potrafi ukrywać swoje myśli i uczucia oraz wydobyć kluczowe informacje od drugiej strony.
* SŁABOŚĆ: Negocjator jednak nie porwie nikogo za sobą - używa technik i logiki. Jest przezroczysty i nie przyciąga uwagi.




## 3. Proto-Lista

* **Adaptacja**: SUPER: sytuacje niespodziewane i dziwne, KIEPSKO: sytuacje powtarzalne i rutynowe
* **Przyjazność**: SUPER: budowanie atmosfery, integrowanie osób, empatia, KIEPSKO: zadawanie cierpienia, wykluczanie
* **Uważność**: SUPER: zauważanie rzeczy, wyciąganie informacji, percepcja, obserwacja, KIEPSKO: przejście obok zagadki
* **Zespołowość**: SUPER: optymizm i morale, działanie jako wsparcie, współlojalność, plany, KIEPSKO: działanie samotnie, nadmiar zaufania do swoich
* **Elegancja**: SUPER: kontrola atmosfery, budowanie wrażenia, wiedza o zachowaniu, KIEPSKO: działanie wbrew wizerunkowi
* **Ciekawość**: SUPER: używanie swojej wiedzy, eksploracja, KIEPSKO: działanie zgodne z planem, metodyczna precyzja
* **Dyscyplina**: SUPER: metodyczne działanie, plany, przejmowanie dowodenia, KIEPSKO: budowanie relacji, rozluźnianie atmosfery
* **Optymalizacja**: SUPER: odnajdowanie się, usprawnianie pracy, systematyczność, dowodzenie, KIEPSKO: relacje, działanie "bez niczego"
* **Ekstrawagancja**: SUPER: przyciąganie uwagi, wzbudzanie uczuć, przyciąganie fanów, KIEPSKO: dyskrecja, maskowanie się
* **Życzliwość**: SUPER: rozładowanie atmosfery, relacje, przyjaźnie, KIEPSKO: zastraszanie, przejmowanie dowodzenia, personalne rywalizowanie
* **Honor, szczerość**: SUPER: przejmowanie dowodzenia, niezłomność, negocjacje z wrogiem, KIEPSKO: jakiekolwiek kłamstwa
* **Paladyn**: SUPER: ochrona innych, przejmowanie dowodzenia, pierwsza linia, KIEPSKO: przejść obojętnie koło cierpienia
* **Precyzja**: SUPER: planowanie, skupienie na detalach, metodyczność, KIEPSKO: działanie szybkie i spontaniczne
* **Wytrwałość**: SUPER: upór, nieprzekonywalność, odporność, KIEPSKO: zmiana celu, ustąpienie
* **Duchowość**: SUPER: immovable object, stabilizacja sytuacji, KIEPSKO: relacje z "innymi"

* **Daredevil**: SUPER: wysokoadrenalinowe sprawy, działa osobiście, KIEPSKO: ostrożność, powstrzymywanie się
* **Entuzjazm**: SUPER: motywowanie, morale, niezłomność, KIEPSKO: działanie z drugiej linii, łatwo zmanipulować
* **Pająk**: SUPER: ukrywanie myśli, obserwacja, działanie innymi, KIEPSKO: przyjaźnie, działanie z pierwszej linii
* **Pewność siebie**: SUPER: porywanie za sobą innych, pójście za ciosem, pierwsza linia, KIEPSKO: zmienianie planów, cierpliwość i ostrożność
* **Artysta**: SUPER: wzbudzanie emocji, budowanie piękna, odporność na krytykę, KIEPSKO: działanie w pośpiechu, sytuacje powtarzalne, rutynowe, nieznaczące
* **Analityczność**: SUPER: odkrywanie prawdy, rozwiązywanie zagadek, planowanie, KIEPSKO: rozluźnianie atmosfery, działania w zaskoczeniu
* **Istota (natury**): SUPER: działanie zgodne z ekosystemem, wie wszystko o ekosystemie, KIEPSKO: ryba wyjęta z wody
* **Zaradność**: SUPER: działanie samotne, skupienie na celu, survival, planowanie kryzysowe, KIEPSKO: działanie z innymi, relacje, wykonywanie rozkazów
* **Lojalność**: SUPER: wzbudzanie szacunku, niezłomność, wykona zadanie KIEPSKO: działanie wbrew swojej sprawie
* **Gwiazda**: SUPER: przyciąganie uwagi, działanie grupą, władanie sercami, KIEPSKO: izolacja, działanie samotne, działanie z drugiej linii
* **Inżynier**: SUPER: budowa rzeczy, działanie narzędziami, metodyczność, KIEPSKO: działania bez narzędzi, działania spontaniczne
* **Rozbawienie**: SUPER: rozładowanie atmosfery, żarty, rozrywka, odwracanie uwagi, KIEPSKO: utrzymanie powagi, przejmowanie dowodzenia
* **Wizjoner**: SUPER: dążenie do swojej wizji, niezłomność, porywanie za sobą, KIEPSKO: relacje, przyjaźnie, odrzucenie pracy nad wizją
* **Opoka**: SUPER: stabilizacja sytuacji, znajdowanie wystarczająco dobrego rozwiązania, incorruptible, KIEPSKO: optymalizacja, działanie pierwszej linii
* **Spokój**: SUPER: działania metodyczne i ostrożne, stabilizacja innych, odpieranie emocji, KIEPSKO: działania spontaniczne, przekonywanie emocjami
* **Polyanna**: SUPER: incorruptible, znalezienie pomocy, relacje, KIEPSKO: łatwa manipulacja, zadawanie cierpienia
* **Czujność**: SUPER: niemożliwy do zaskoczenia, percepcja, świetna obserwacja, KIEPSKO: przyjazne relacje, rozluźnianie atmosfery
* **Urok**: SUPER: relacje, kontakty, przyjaźnie, zdobywanie informacji, KIEPSKO: działanie w ukryciu, powstrzymywanie się
* **Dyplomacja**: SUPER: rozwiązać konflikt, ukryć swoje myśli, wydobyć informacje, KIEPSKO: porwać za sobą innych

Nurturing, Resourceful, Studious, Supportive, Thrifty, Traditional, Whimsical, Wholesome, 

====
