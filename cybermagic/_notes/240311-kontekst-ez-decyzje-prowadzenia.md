# Kontekst EZ: decyzje prowadzenia

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić, Fox, Flamerog

---
## 1. Purpose

* Czy sesje są za łatwe?
* Do czego jest dobre EZ?
* Czy EZ degeneruje skillset graczy?

---
## 2. Spis treści i omówienie

1. Gra jako modelowanie rzeczywistości
2. Dokument wizji - po co jest EZ
3. Kontekst sesji RPG
    1. Na co nakierowujemy sesję - wymiary
        1. Story-first vs Challenge-first
        2. Plot-based vs Character-based
        3. Cele indywidualne vs Cele drużynowe
        4. Dynamiczna akcja vs Planowanie
        5. Stopień trudności i jak bardzo ma im się udać
            1. Czemu nie FULL POWER! MG?
            2. Stopnie trudności - jak podnieść / obniżyć (wymiary)? 
            3. Jak bardzo ma się udać?
        6. Co ja optymalizuję w EZ a co CHCĘ optymalizować w EZ?
    2. Co chcą robić gracze
        1. Wizja, Strategia, Taktyka
        2. Jak to wygląda w praktyce?
    3. Ograniczenia
        1. Gracze i typy graczy (pod kątem ograniczeń)
            1. Gracz Całkowicie Nowy
            2. Gracz Przestraszony, Wyłączający się
            3. Gracz Robiący YOLO ruchy
            4. Gracz Chcący Wyzwań i Cierpienia
            5. Gracz Chcący Ukojenia / Spokoju
            6. Gracz Który Chce Patrzeć I Nie Chce Grać
            7. Gracz Który Chce Mieć Swoją Ciekawą Postać
        2. Środowisko / otoczenie
            1. Nowy Skomplikowany System I Świat
            2. Discord, online
            3. Konwent, offline
            4. Ograniczenie czasowe
4. Wątpliwości, potencjalne problemy, uwagi
    1. Sesja przechodzi się sama
    2. EZ jako system jest niespójny z perspektywy gracza - co jest ważne a co nie, planowanie, akcja...
    3. Gracze się potencjalnie nie uczą
    4. Gracze się uczą robić ruchy bo "jakoś to będzie"

---
## 3. Gra jako modelowanie rzeczywistości

Wszystkie systemy RPG są modelami - uproszczeniami rzeczywistości pod kątem konkretnego celu i punktu widzenia. Nie ma systemów "perfekcyjnych" tak jak nie ma narzędzi perfekcyjnych. Są tylko narzędzia perfekcyjne do osiągnięcia konkretnego celu.

Wszystkie modele są błędne, ale niektóre są przydatne.

---
## 4. Dokument wizji - po co jest EZ

Odwołuję się do EVERGREEN celów systemu: [Evergreen EZ system](230808-EVERGREEN-rd-design-goals.md). Na szybko:

1. **Zasady wiodące designem**
    * (1) Każdy konflikt da się zamodelować
    * (2) Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.
    * (3) System intencyjny
    * (4) System negocjacyjny - coś wygrywasz, coś poświęcasz
    * (6) Nie ma możliwości, by osoba siedząca przy stole nie wpłynęła na fikcję
    * (7) Gracz ma kontrolę nad ryzykiem
    * (8) Catchup mechanism
    * (9) Chaos i nieprzewidywalność
    * (10) Nie da się przypadkiem spieprzyć fikcji
2. **Parametry niefunkcjonalne**
    *  (11) Mechanika jest szybka (gdy się wdrożysz)
    *  (12) Mechanika jest tania z perspektywy kosztu intelektualnego (gdy się wdrożysz)
3. **Inne ważne aspekty**
    * (13) Decyzje gracza poza fikcją mają znaczenie
    * (14) Manewry (deklaracje) gracza w fikcji mają znaczenie

Te tenety sprawiają, że EZ jest (aspiruje do bycia) systemem, który:

* Daje Ci nadzieję. Zawsze można coś zrobić, zawsze jeśli to jest 'wycofać się'.
* Nie pozwoli Ci spieprzyć przez przypadek. Może grać lepszy gracz ze słabszym graczem.
* Jest silnikiem budującym historię, inspirującym, gdzie Twoje decyzje i ruchy mają znaczenie.
* Jest silnikiem szybkim.
* Sprawi, że im masz lepsze pomysły i im lepiej wykorzystujesz wszystkie klocki, tym lepiej Ci idzie
* Jesteś w stanie podejść do rzeczy w których nie jesteś ekspertem i sobie poradzisz

Z tej perspektywy, EZ uczy Cię następujących rzeczy:

* Zawsze coś da się zrobić. Nigdy nie trać nadziei.
    * "każdego smoka da się rozłożyć na serię mniejszych kroków"
* Jesteś w stanie negocjować sukces i porażkę
    * "nie wygrasz wszystkiego, ale w każdej sytuacji w której COŚ masz czego chce Druga Strona możesz coś uzyskać"
* W sytuacji niespodziewanej, masz rzeczy 'dookoła' które możesz zaadaptować i użyć
    * "wszystko jest bronią jak jesteś dość zdesperowany"
* Nie każdą rzecz warto zrobić - czemu to robisz? Jakie są konsekwencje?
    * "czy warto poświęcić coś ważnego na czym Ci zależy za 45% szansy na coś więcej?"
    * "jeśli nie masz celu i nie wiesz po co coś robisz, poświęcasz / ryzykujesz zasoby i nic nie dostaniesz"
* Każda AKCJA jest pochodną CELU oraz jest rozbijalna na KROKI. Nie ma CELU -> nie ma AKCJI, CEL -> KROKI -> AKCJA.

---
## 5. Kontekst sesji RPG
### 5.1. Na co nakierowujemy sesję - wymiary
#### 5.1.1. Story-first vs Challenge-first

* Story-based
    * Każda sesja jest **o czymś**
    * Gracze podejmują **decyzje**, dokonują **wyboru fabularnego** - jak opowieść ma się potoczyć
    * Gracze grają o konkretny wariant historii z otoczeniem na którym im zależy
    * Większy zakres / scope, "ważniejsze" decyzje (z perspektywy świata)
    * **"Tworzymy jak najlepszy film używając frameworka gry"**
    * Przykład skrajnego systemu: `Microscope`, `Follow`
* Challenge-based
    * Każda sesja ma **coś do wygrania i do przegrania**
    * Gracze próbują **wygrać / pokonać przeciwnika**, próbują wymanewrować rzeczywistość
    * Gracze grają o konkretną serię sukcesów; historia jest mniej istotna
    * Więcej detali i silniej określone klocki i niezmienniki
    * **"Gramy w grę i próbujemy ją wygrać; opowieść powstaje przy okazji"**
    * Przykład skrajnego systemu: gra komputerowa `Ufo: Enemy Unknown (stara)` na ironman

To jest "budżet" oraz "oś". Im bardziej przesuwamy to w jedną stronę, tym bardziej druga strona jest dezoptymalizowana. ALE - jest ogromna ilość "miejsca" gdzie można mieć oba te wymiary w jakichś proporcjach.

* Jeśli sesja jest w wysokim stopniu Challenge-based, istnieje możliwość, że skończy się katastrofalnie i nie będzie ciekawej historii dla zainteresowanych tym graczy. 
    * WIEMY, że w `Fate:Stay Night` dojdzie do walki pomiędzy użytkownikami Graala. Wiemy, że nieważne kto wygra, świat się 'skończy'. Postacie mogą przegrać.
    * Gracze grają o ZWYCIĘSTWO. Ich porażka oznacza że rzeczy się porozsypywały, historia może nie zadziałać i wszystkie zasoby, wątki i fabuła - WSZYSTKO - jest silnie narażone na każdy błędny lub poprawny ruch Graczy.
        * porażka: postacie graczy giną. Wszystko na czym im zależało obraca się w proch.
* Jeśli jest w wysokim stopniu Story-based, gracze zdefiniują historię i "nie mają jak przegrać tego wariantu fabuły o który walczą".
    * WIEMY, że `Hercules Poirot` złapie mordercę. Dalej nie wiemy ILU ludzi zabije morderca oraz JAK DALEKO posunie swoje mroczne plany.
    * Gracze grają o HISTORIĘ. Ich celem jest podjęcie wyboru (np. "pozwolić mordercy uciec, bo to było sprawiedliwe morderstwo CZY prawo jest ważniejsze"). Ich porażka oznacza, że NIE POSZŁO TAK JAK CHCIELI, że musieli poświęcić więcej lub że decyzja o przyszłości i historii nie jest w ich rękach a w rękach MG.
        * porażka: Poirot złapał mordercę, ale ten osiągnął swój cel kompletnie i gdyby nie inni, Poirot mógłby nie dać rady mordercy złapać. Co więcej, gracze reprezentujący Poirot chcieli wolności dla mordercy a morderca poszedł na szafot i Poirot ma publiczne podziękowania.

EZ jest silnie zaprojektowana jako Story-first, ale przy użyciu komponentów Challenge-first. Ale - to ważne - jeśli chcecie robić Challenge-first bez komponentów Story-first, są DUŻO lepsze systemy niż EZ.

---
#### 5.1.2. Plot-based vs Character-based

Za warsztatem (220711-rpg-plot-character-based)

* PLOT-BASED
    * nieważne kim grasz, ważne jaka jest historia
    * ważniejsza jest FABUŁA i działania postaci ale same postaci mogą być płaskimi kartkami. 
        * Przykład: spaghetti western. Nie wiemy kim jest kowboj ale jego obecność zmienia stan miasteczka i nie skupiamy się na przeszłości kowboja a na reakcjach / działaniach miasteczka na działania kowboja 
        * kowboj reaguje a potem decyduje (metastruktura skuteczna na jednostrzały)
    * horrory często 'plot-based' (obligatoryjna blondynka, obligatoryjny nerd... nic o nich nie wiemy, stereotypy)
    * **finał zmienia świat lub nasze wyobrażenie o nim**
    * Negacja PLOT: czy gdyby gracze NIC NIE ZROBILI to potoczyłoby się to tak samo? Zmiana w świecie, co się zmieniło.
    * PLOT: theme: chain of events, external conflict (what happens), reach destination, escalation of stakes, DARK FUTURE
* CHARACTER-BASED
    * historia jest dookoła postaci graczy
    * ważne jest kim jest postać i jak się zmienia
    * historia dotyczy mojej postaci osobiście - **finał zmienia bohatera**
    * dużo silniejsze splątanie postaci z innymi postaciami
    * Negacja CHAR: czy dla INNEJ postaci mogłoby się to potoczyć tak samo? Zmiana w postaci, nauczyła się.
    * CHARACTER: theme: flaws/choices, internal conflict (why it matters), how changes, INTERNAL CHANGE
* Inna budowa i narzędzia (Pragnienie postaci vs Dark Future || Interakcje postaci vs Działania świata)
* Jeśli robimy historię, na jakie PYTANIE odpowiada sesja? Czy zmieniamy POSTACIE czy ŚWIAT?

Oba warianty gry zadziałają w kontekście EZ, ale to będą zupełnie inne sesje. Ja często skupiam się na plot-based z uwagi na ograniczenia.

---
#### 5.1.3. Cele indywidualne vs Cele drużynowe

Za warsztatem (230707-rpg-cele-ind-team-enemy)

Cele na sesji:

* Indywidualny
    * dezintegruje, konfliktuje
    * jeśli za silny, N zespołów a nie jeden zespół
    * jeśli są skonfliktowane, masz pvp
    * zwiększa atrakcyjność sesji przez różnorodność agend
    * Przykładowe systemy z dominantą: **Paranoia**, **Agon**
* Grupowy / drużynowy
    * spaja, łączy
    * zwykle jest najsliniejszym celem - wygrywamy lub przegrywamy
    * daje osadzenie: (czemu chcemy TAM iść? co robić dalej?)
* Przeciwnika
    * w relacji do celów graczy - jeśli nie jest w relacji, to nie jest przeciwnik
    * ostro skonfliktowany z graczami
    * daje 'walkę' i aktywną agendę przeciwnika

W kontekście EZ dla mniej doświadczonych graczy **ZAWSZE** daję im cel drużynowy przy niższej eksploracji celu indywidualnego. Dzięki systemowi intencyjnemu, gracze muszą wszystkie akcje uzasadnić pod kątem celu drużynowego.

Jeśli gracz ma cel indywidualny, nie może być on sprzeczny z celem drużynowym, bo nie zaakceptuję takiego celu dla postaci.

Czy tak musi być? Nie. Ale akurat ta populacja graczy jaką mam tego sobie życzy.

---
#### 5.1.4. Dynamiczna akcja vs Planowanie

Z perspektywy cynefin, EZ operuje na poziomie [Complex](https://en.wikipedia.org/wiki/Cynefin_framework). Trochę planowania, ale żaden plan nigdy nie wytrzymał spotkania z rzeczywistym problemem.

![Cynefin, mapa](Materials/240311/002-240311-cynefin.png)

To znaczy, że **zawsze chcesz wiedzieć dokąd idziesz i czym jest sukces** i jakie masz wysokopoziomowe cele (objectives) do osiągnięcia, jak podzielić je na kroki i jak te cele oraz kroki prowadzą do tego sukcesu, ale niekoniecznie będziesz wchodzić we wszystkie potrzebne detale. 

Jako przykład, weźmy książkę "Redemption" (Ryan Drake) Willa Jordana:

![Redemption Excerpt](Materials/240311/001-240311-redemption-excerpt.png)

Żeby odbić osobę z więzienia, trzeba wykonać taką serię kroków. I potem pojawiają się detale.

* Oczekuję, że gracz potrafi wykonać tego typu analizę, niezależnie od poziomu kompetencji gracza.
    * Jeśli nie, jako MG pomogę - zatrzymajcie się, rozrysujcie co macie, co chcecie dostać, co Wam przeszkadza - i zmuszę ich do tego by powiedzieli:
        * TEGO chcemy / to chcemy dostać
        * DLATEGO nie możemy
        * TAKIE kroki mogą nam to dać
        * TAKIE materiały będziemy potrzebować ("jakiś samolot")
        * TAK możemy je pozyskać jak ich nie mamy
* Absolutnie nie oczekuję, że gracz wpadnie na "Elmendorf Air Force Base in Alaska", czy dokładnie jak przeprowadzić insercję z powietrza.
    * Tu ja im pomogę, 'backportując rzeczywistość pod plan' jeśli to jest możliwe i te postacie mogłyby to mieć.
    * Ale tego powyżej - CO chcę osiągnąć i JAK (ogólnie) nie zamierzam robić za nich.

Czy EZ może działać na większym poziomie detali? Oczywiście, że tak. Z Dzióbkiem zawsze to robię. Ale z uwagi na constrainty (o tym później) tego zwykle nie robię.

---
#### 5.1.5. Stopień trudności i jak bardzo ma im się udać
##### 5.1.5.1. Czemu nie 'tak jak się da'

Z góry zapomnijmy o jednym wariancie - że "MG ma walić tak mocno jak może". Jeśli tak by było, daję graczom 'pierwszopoziomowe postacie' i otaczam ich 'czarnymi smokami'. GG NOOBS! YOU SUCK!

Czyli są pewne ograniczenia. I one mocno wynikają z poprzednich wymiarów tego o czym mówimy.

Gdy buduję sesję, to **moją** odpowiedzialnością jest ustawienie stopni trudności, pozycjonowania postaci itp. To moja odpowiedzialność, czy to jest mem z Piper Perri ("4 guys 1 girl" dla niepoinformowanych) (NIE SZUKAJCIE TEGO NA UNCENSORED!!!), czy sensowna sesja.

---
##### 5.1.5.2. Stopnie trudności

Teraz - stopnie trudności można podnieść następująco (z pewnością jest tego więcej, ale to dobry start):

1. **Osłabić postacie**
    * "dzieciak" nie dostanie dobrej interpretacji wydarzeń a "doświadczony detektyw" tak
    * "dzieciak" nie wykona takich operacji jak "master hacker" (trudniejsze testy)
    * "dzieciak" nie ma domyślnej wiedzy "advancera orbitalnego" (jak połączyć orbity statków by dało się przejść); tu gracz musi wiedzieć bo postać nie pomoże
    * "komandos" w przedszkolu poradzi sobie gorzej niż "przeszkolanka". Typowe fish-out-of-water.
2. **Osłabić sojuszników i środki postaci**
    * "dzieciak" nie ma wsparcia sojuszników jakie ma "dyrektor CIA"
    * brak dostępności do zasobów, limitowane zasoby w formie toru
3. **Zwiększyć siłę przeciwników i niezmienników**
    * komodor Bladawir jest **NIEMOŻLIWY** do pokonania przez postacie graczy politycznie, bo się zemści i zniszczy Twoich bliskich
4. **Zwiększyć ilość niezmienników i detali i znajomość graczy**
    * im więcej jest "ustalone", tym bardziej gracze muszą się adaptować do tego co jest a nie powoływać nowe rzeczy
    * tu pojawia się konieczność dokładniejszego, lepszego planowania
5. **Zwiększyć złożoność fabularną**
    * dzieje się więcej niż tylko to co na sesji - NPCe mają agendy, więc nie wszystko jest "na temat"
    * jest dużo klocków, agend, celów... i trzeba w tym wszystkim nawigować i się nie pogubić. A "świat idzie do przodu"
6. **Zwiększyć presję czasową** - IRL i na sesji
7. **Podnieść siłę przeciwników**
    * walka z lewiatanem to kilka torów na poziomie heroicznym / ekstremalnym. Walka ze szczurem to jeden test typowy (if any).
8. **Postawić niekorzystne otoczenie**
    * "chcę być najlepsza na balu" jeśli jesteś nielubiana i z zewnątrz to trudniejsze niż jak jesteś w środku.
9. **Intensyfikacja celów indywidualnych**
    * na tym się opiera system AGON iirc.
10. **Obniżenie wiedzy postaci i graczy**
    * jest różnica czy gramy i ROZUMIEMY czy gramy i NIE ROZUMIEMY natury przeciwnika / zjawiska / tego co używamy
11. **Konsekwencje błędów i jak łagodny jest MG**
    * jest różnica "jeśli to zrobisz to..." a "ok, nie żyjesz". Jak bardzo MG jest łagodny i pomaga?
    * (nie dotyczy fizyki świata, tu gracze mają prawo nie rozumieć)

Różne wymiary mogą być wzmacniane / obniżane w różny sposób. Większość sesji / filmów optymalizuje różne wymiary w różny sposób.

**Predator (ze Schwarzeneggerem)**:

* silne postacie, ALE przeciwnik dużo silniejszy
* możliwość powoływania 'sprzętu i otoczenia', postacie na niezmiennikach
* mała złożoność fabularna
* silne cele indywidualne i niekorzystne otoczenie

---
##### 5.1.5.3. Co ma graczom się udać?

Bardzo istotne jest to, że nie na każdej sesji MG chce dostać wszystko. Już przed sesją MG musi odpowiedzieć sobie na pytanie jak daleko chce się posunąć i co jest gwarantowane, że gracze dostaną a co może całkowicie się spieprzyć.

W typowej kampanii D&D postacie graczy nie zostają permanentnie usunięte. MG nie odbierze graczom poziomu doświadczenia (wraith, wight), nie zabije postaci, która ma jeszcze ważny wątek itp.

Jest tu też coś więcej – jeśli MG umieścił postać graczy w jakimś miejscu i nie ze swojej winy te postacie ucierpiały (bo MG się pomylił), większość MG powie, że postaci gracze za to nie należy karać.

To prowadzi nas do pytania **jak daleko MG ma się posunąć** i **co gracze mogą realnie wygrać lub stracić**. I każda sesja / każdy MG odpowie na te pytania trochę inaczej.

Załóżmy taką sytuację - postacie graczy są na statku kosmicznym. Celem sesji jest to, by dowiedzieć się, że dowódca postaci graczy doprowadził do śmierci wszystkich na tym statku by tylko iść w karierze do przodu. To sprawi, że gracze stają przed ciekawą decyzją - czy należy dowódcę wesprzeć (i coś dzięki temu dostać, lub może ten dowódca teraz jest dobrym człowiekiem i robi dużo dobrego) czy należy oddać sprawiedliwość ofiarom dowódcy.

* W niektórych sesjach chodzi o to, by gracze faktycznie dowiedzieli się, że dowódca stał za tym wszystkim. Gra nie toczy się o to czy się dowiedzą, gra toczy się o to jaką decyzję podejmą.
* W niektórych sesjach chodzi o to, że gracze na przestrzeni ciężkiej pracy i planowania znaleźli ślad nie wykrytego przestępcy. I gracze próbują dowiedzieć się, że to był ich dowódca. W praktyce, mogą nigdy się tego nie dowiedzieć.

Oba typy sesji są poprawne. To są sesje na inny temat o innym stylu prowadzenia.

Ważne, że **MG musi przed sesją wiedzieć co graczom się MUSI udać (np. postacie nie zginą, dowiedzą się X) a co podlega testom i analizie**.

---
#### 5.1.6. Co ja optymalizuję w EZ a co CHCĘ optymalizować w EZ?
#### 5.1.6.1. Przypominam wymiary

1. Story-first vs Challenge-first
2. Plot-based vs Character-based
3. Cele indywidualne vs Cele drużynowe
4. Dynamiczna akcja vs Planowanie
5. Stopień trudności i jak bardzo ma im się udać
        
#### 5.1.6.2. Przykładowe 'settingi'

* **Agencja Lux Umbrarum**
    * Trudność
        * BARDZO silne postacie o KORZYSTNYM ekosystemie (wszyscy chcą im pomóc) (ułatwienie)
        * DUŻA złożoność fabularna (utrudnienie)
        * NISKA wiedza postaci i graczy o wszystkim (utrudnienie)
        * DUŻA ilość niezmienników, konieczność kombinowania (utrudnienie), ale z uwagi na siłę postaci i ekosystemu MNÓSTWO dróg dojścia (ogromne ułatwienie)
    * **oneliner**: story-first, plot-based, team-first (brak indywidualnych), akcja >> planowanie + trochę badania, proste sesje budujące opowieść i zmuszające do decyzji
    * -> idealne dla nowych graczy w kontekście konwentowym
* **Furia Mataris w Aurum (Kić)**
    * Trudność
        * SILNA postać bez magii w obcym środowisku, z pomniejszymi kontaktami a dookoła sporo magów; główna siła to zaskoczenie (TRUDNE)
        * Przeciwnicy to często niemądrzy i nie skupiający się na rzeczy magowie (ułatwienie), ale trzeba działać dyskretnie i rozgrywać politycznie bo konieczność stealth (trudniejsze)
        * Silnie zarysowany cel i duża złożoność fabularna, sporo klocków i niezmienników. Sporo planowania i "jak do tego podejść". 
        * Ogromna znajomość świata i systemu przez graczy; silne założenie że gracz to wie, rozumie i używa
    * **oneliner**: story-challenge-balance, character-first, team-first, akcja > planowanie, jak się nie uda to zupełnie kierunek stracony; trzeba mafię INACZEJ wprowadzić
    * -> dobry balans challenge ze story-based, z koniecznością selekcji co robimy kiedy i jak
* **Ziarno Kuratorów na Karnaxianie (Kić, Dzióbek) lub Ekstaflos na Tezifeng (Kić, Fox)**
    * Trudność
        * RELATYWNIE SŁABE postacie w dobrym ekosystemie (statek Serbinius) kontra Przeciwnik atakujący z zaskoczenia i robiący pułapkę. Przeciwnik ma fabrykatory / nietypowe działania.
        * Agenci nie jest w stanie sensownie operować na jednostce przeciwnika, bardzo dużo niebezpiecznych niezmienników (np. miny w kosmosie, gazy...)
        * Konieczność zauważania detali (debris w kosmosie, koleś jest TROSZKĘ NIE TAKI...) i bezwzględnie silne konflikty jak postacie się wpakują przez niezauważenie
        * Presja czasowa w grze: jak gracze nic nie robią, więcej cywili ginie. W połączeniu z timeboxowaniem czasu IRL, więc ograniczony czas na planowanie.
        * Bardzo dokładne detale i silne niezmienniki - MG dokładnie wie co gdzie i jak i gracze muszą tego użyć
    * **oneliner**: challenge-first, plot-based, team-first z elementami indywidualnymi, planowanie + detale > akcja, straszna siła przeciwników ALE możliwość ewakuacji
    * -> challenge-first, bardzo trudna sesja którą Dzióbek zaatakował i poddał (nie mieli szans) a Fox wezwała wsparcie i asekurowała

Jak widać, EZ daje nam różne możliwości używając powyższego frameworka. Czyli się da.

#### 5.1.6.3. Co kiedy

Dla nowych graczy i osób nie rozumiejących dobrze EZ optimum to coś w stylu Agencji. Są w stanie wygenerować fajną opowieść, ich znaczenie jest pod kątem tego co faktycznie stworzą i jakie decyzje podejmą i nie są w stanie sobie wszystkiego rozpieprzyć. Idealne na konwent.

Dla osób, które już kilka rzeczy w EZ zrobiły, ale nie mają persystencji (nie są powiązane z żadnymi wątkami i raczej grają okazjonalnie) skupiam się nadal na story-first a nie challenge-first. Tu mogę podnieść stopnie trudności, bo gracz już rozumie niektóre wymiary trudności; nie przeładuję go nowościami i problemami. Alternatywnie - mogę zrobić sesję typu challenge-first, z dużo mniejszą ilością magii i konieczności rozumienia rzeczywistości. Też zadziała (przykład sesji: „dziki czołg na wolności niszczy i dewastuje zróbmy coś, bo nas wykończy”.

Dla osób, które mają persystencję i które dłużej grają w EZ, kalibruje konkretne wymiary stopnia trudności pod te osoby. To, co chcą robić i co ich interesuje. Lub na co mają ochotę w danym momencie.

Oczywiście, to jest jeszcze trudniejszy problem, bo gracze są różni...

---
### 5.2. Co chcą robić gracze - Wizja, Strategia, Taktyka
#### 5.2.1. O co chodzi z Wizją, Strategią, Taktyką?

* Typ 'Wizja, Strategia, Taktyka w rękach Graczy'
    * Typowy OSRowy Sandbox (nie masz celu, nie masz presji, masz lokację i heksy)
    * definiuje co się ma dziać OGÓLNIE (mamy wytyczne, sami decydujemy co i jak)
    * Przykład: Arianna jako komodor Orbitera mająca flotę gdzieś "daleko" z celem 'obecności Orbitera i ogólnie stabilizacji'
* Typ 'Wizja nadana, Strategia, Taktyka w rękach Graczy'
    * Arianna jako komodor Orbitera z flotą gdzieś "daleko", ma wytyczne jak ma wyglądać teren jak z nim skończy (np. pozbyć się Saitaerowców, zabezpieczyć pokój między drakolitami i savaranami...)
    * Większość gier komputerowych typu "Civilization 4" (MUSISZ wygrać, od Ciebie zależy jak)
    * definiuje co się ma dziać OGÓLNIE oraz CO MA SIĘ STAĆ, gracz decyduje JAK TO ZROBIĆ na jeden z wielu możliwych sposobów.
    * Przykład: Arianna jako kapitan Orbitera mająca przejąć kontrolę nad planetoidą Tekel.
* Typ 'Wizja i Strategia nadane, Taktyka w rękach Graczy'
    * Arianna jako kapitan Orbitera mająca przejąć kontrolę MILITARNIE nad planetoidą Tekel mając zasoby jakie ma i pozostając niezauważoną.
    * definiuje co się ma dziać OGÓLNIE oraz CO MA SIĘ STAĆ i W JAKI SPOSÓB TO ZROBIMY, gracz ma to przeprowadzić jak najlepiej
    * Arianna próbująca uratować ludzi z Serenit, robiąc intruzję na pokład Serenit - i teraz dowodzi by się udało jak najlepiej
    * Typowy loch D&D.

Innymi słowy, na których teatrach działa gracz: co definiuje, co jest w jego sferze kontroli?

#### 5.2.2. Jak to wygląda w praktyce - przykłady

**Rekiny.**

Popatrzmy na sesje wśrod Rekinów. Marysia Sowińska ma do czynienia z grupą rozwydrzonych młodych arystokratów-magów z Aurum traktujących okolicę i lokalsów jak prowincję.

Marysia - dowodząca oficjalnie terenem - decyduje się na rozkochanie w sobie Ernesta z Eterni (Wizja) oraz na to, by docelowo Rekiny zaczęły być przydatne na tym terenie, skupić się na redukcji Skażenia magicznego na tym terenie (Wizja). A, a przy okazji chce uciemiężyć swoją kuzynkę, Amelię (nadal Wizja). Rozkochanie w sobie Ernesta sprawi, że Amelia - której chłopakiem jest Ernest - będzie bardzo smutna (Rozkochanie Ernesta to Strategia wspieraąca Wizję cierpienia Amelii) a odpowiednie budowanie sojuszy przy pomocy Ernesta i splątanie ze sobą kluczowych Rekinów (Strategia) sprawi, że inne Rekiny też się musza do Marysi dopasować.

To są wysokopoziomowe cele graczki prowadzącej Marysię. I teraz zostaje Taktyka - kto robi co, kiedy i jak.

**Arianna na Orbiterze.**

Arianna jest pod komodorem Bladawirem. Lub pod admirałem Kramerem. Lub pod kimś. Arianna nie definiuje **Wizji** - ona ma zdefiniowaną Wizję. Inni mówią jej co ma osiągnąć, ale to Arianna określa w jaki sposób to zrobi, co może / chce osiągnąć i co dzięki temu dostanie (Strategia / Taktyka).

#### 5.2.2. Jak to wygląda w praktyce - implikacje

Sesja ma 3,5 godziny; czasami 4. W tym czasie musi się "zamknąć odcinek". Zamknięty raport. Skończona sesja.

**Marysia**

To, że Marysia definiuje poziom wizji i na jej sesjach jednocześnie walczymy na poziomie Wizji, Strategii i Taktyki bardzo spowalnia te sesje. Z uwagi na to jakiego typu gracze uczestniczą w tamtych sesjach (co lubią, w czym są dobrzy, czego chcą od RPG) i jakiego typu postacie występują na tych sesjach (arystokraci ze służbą i środkami na nieswoim terenie), poziom taktyki często jest wykonywany przez podwładnych. Nie zawsze - ale jeżeli gracz nie ma kompetencji oraz postać nie ma kompetencji, ale ma podwładnych, to podwładni zrobią to dla postaci gracza. Styl sesji „intrygi + drama + zarządzanie”.

**Arianna**

To, że Arianna dowodzi statkiem kosmicznym wcale nie oznacza, że sama niczego nie robi. Większość jej konfliktów to konflikty natury taktycznej - jak kogoś przekonać do czegoś, co kiedyś należy robić. Poziom strategiczny zapewniany jest zarówno przez Ariannę jak i Klaudię i Eustachego - razem rozpracowują, jak rozwiązać dany problem.

Ten konkretny zespół graczy nie skupia się na poziomie Wizji. Raczej wykonują polecenia, chcąc zobaczyć, gdzie fabuła ich poniesie. Jedynie, jeżeli Wizja okaże się być nieakceptowalna to zaczną działać przeciwko (przykład: Bladawir). Ale ogólnie te postacie zakładają, że „Orbiter wie co robi”. Co ma sens, patrząc na hierarchiczną strukturę militarną i na kulturę, z której te postacie pochodzą.

**Ogólnie**

Gdybym prowadził całkowicie emergentne (sandboxowe, samowzbudne) sesje, jedynie definiowałbym jakie problemy są w „bazie” / „kompanii najemników” i zostawiłbym graczom zarówno poziom wizji, strategii jak i taktyki. Zrobiłem coś takiego i zadziałało bardzo dobrze.

Problem: nie wszyscy gracze to lubią i nie wszyscy gracze są w tym dobrzy. Dodatkowo, niezależnie od poziomu wyzwania, gracze często lubią mieć „sesję o czymś”. Konkretny morał. Coś wartościowego - coś więcej niż tylko epizod. Albo wychodzące z postaci (character-based) albo wychodzące z otoczenia i specyfiki (plot-based). 

Po naprawdę dużej ilości testów okazało się, że graczom bardziej podoba się sytuacja, gdzie jest jakaś wysokopoziomowa fabuła. Jakkolwiek można odpowiednio podnieść stopień trudności, by gracze musieli wywalczyć sobie zrozumienie tej fabuły, ale w wypadku porażki gracze są zwykle dosyć nieszczęśliwi. Gracze częściej akceptują przegraną rozumianą jako „ nie dostaliśmy tej fabuły co chcieliśmy” niż „nie mamy pojęcia co tam się stało i się odbiliśmy.”

Seria testów sprawiła, że zwykle nadaję poziom Wizji, zostawiając graczom Strategię i Taktykę. Dopiero postacie, które są „wolne” i „powiązane ze światem” sterowane przez graczy, którzy są doświadczeni w systemie i dobrze rozumieją świat i kontekst tej sytuacji - dopiero oni są w stanie podjąć decyzję i objąć poziom Wizji.

---
### 5.3. Jakie mamy ograniczenia (constraints)
#### 5.3.1. Gracze i typy graczy (pod kątem ograniczeń)
##### 5.3.1.1. Preambuła

EZ nie jest najlepszym systemem RPG na świecie, ale aspiruje do bycia najlepszym systemem typu XXX. Wiemy już, że XXX nie jest „ szybką i dynamiczną walką” - do tego są lepsi MG i lepsze systemy. EZ nie jest też najlepszym systemem do realistycznego planowania z detalami. Rozdzielczość mechaniki i komponenty typu negocjowanie + powoływanie zwyczajnie sprawiają, że przy za dużej ilości niezmienników i istotnych detali użycie innego systemu po prostu może być lepsze.

Jednak istnieje duża populacja graczy, którzy jakkolwiek nakładają pewne ograniczenia na prowadzenie, ale mogą dobrze bawić się z perspektywy EZ. Poniżej wyjaśnienie jak radzić sobie z poszczególnymi kategoriami graczy.

---
##### 5.3.1.2. Gracz Całkowicie Nowy

Typowe teksty:

* To jest science fiction a nie fantasy???
* (próbuje przeczytać wszystkie materiały które otrzymał, myśląc, że to bardzo ważne)

Co z tym robimy:

Potencjalnie perfekcyjna kombinacja - nowy gracz, nowy w RPG, nie zna świata, nie ma powiązania konsekwencji z czynami, nie chce się zbłaźnić i ogólnie chce zagrać, ale jest niepewny.

Temu trzeba przede wszystkim pokazać, że jest w stanie sobie poradzić, że coś zrobił na sesji, pozwolić się graczowi zanurzyć w świecie, wydarzeniach i zrobić coś sensownego. Zdecydowanie temu graczowi nie dajemy kluczowych ruchów ani kluczowych decyzji. Jeszcze.

Ten gracz nie zna swoich ograniczeń, my też ich nie znamy. Trzeba dać mu ruchy na wszystkich 3 poziomach – wizja, strategia, taktyka. Trzeba dać mu zadania challenge-first i story-first i monitorować. Trzeba zobaczyć z czym czuje się dobrze, z czym czuje się źle i przesunąć go do odpowiedniej kategorii po kilku sesjach.


---
##### 5.3.1.3. Gracz Przestraszony, Wyłączający się

Typowe teksty:

* „nigdy nie grałam negocjatorką, nie wiem, czy sobie poradzę”
* „a co jeżeli tam będzie czekała na nas zasadzka?”
* „nie wiem, jak to zrobić i w sumie się boję”

Co z tym robimy:

Ten gracz musi przede wszystkim zobaczyć, że jego obecność zdecydowanie pomaga zespołowi i gdyby tego gracza nie było to sesja potoczyłaby się gorzej.

Wniosek: ten typ graczy przede wszystkim potrzebuje inspiracji. Potrzebuje czuć, że będzie dobrze i nie zepsuje gry sobie a przede wszystkim innym. Zaczynam od niższego stopnia trudności i pozwalamy graczowi przejść przez fajną opowieść i podjęcie znaczących decyzji. 

To znaczy, że graczowi muszę pokazać, że (X) nie oznacza absolutnej katastrofy. Muszą pokazać, że da się wynegocjować coś nawet w kiepskiej sytuacji. Na dalszym etapie sesji muszę pokazać, że nawet jak nie da się czegoś zrobić jedną stroną, da się wymyślić coś innego. Nawet jeśli wprowadzę go w pułapkę, może z niej wyjść używając swojego intelektu i powolnego spełniania celów pośrednich.

Po 2-3 sesjach tego typu graczowi mogę podnosić stopień trudności. I to zaczyna działać. Mogę dowalić pewne konsekwencje. Mogę postawić bardzo skomplikowany dylemat moralny i mocno cisnąć w stronę którego gracz nie chce.

Jeśli gracz chce też radzić sobie z wyższym stopniem trudności docelowo, nie będzie tu problemu. 

Koszt posiadania tego typu graczy na sesji to to, że muszę iść dużo łagodniej. Muszę pozwolić im na „po prostu gadanie”. Bo tak jak zawsze uciszam graczy którzy bezsensownie planują (wchodzą w pętle, gubią się, prowadzą dialogi bez celu) to ta konkretna populacja potrzebuje się wpierw oswoić i poczuć że mogą i mają jakąś sprawczość.

---
##### 5.3.1.4. Gracz Robiący YOLO ruchy

Typowe teksty: 

* "To ja prześlizguję się na tyły przeciwnika i zatruwam tą studnię"
* "gdy oni szukają potwora, ja spróbuję pozyskać wsparcie w najbliższej świątyni"
* "wyzywam strażnika na pojedynek. Odwrócę uwagę od reszty (gdy reszta nie potrzebuje odwrócenie uwagi)"

Co z tym robimy: 

Jedna z głównych różnic pomiędzy systemami rpg opartymi o akcje i tymi opartymi o intencje to właśnie ta kwestia. W systemie akcyjnym możesz powiedzieć jedno z tych zdań powyżej - gracz definiuje co postać robi. Ale w systemie intencyjnym jest inaczej - gracz nie może definiować co postać robi. Gracz może jedynie powiedzieć co chce osiągnąć i jak to robi (w domyśle - jak uważa, że to dostanie).

Przykładowy dialog:

* Gracz: prześlizguję się i zatruwam studnię
* MG: co chcesz za pomocą tego dostać?
* Gracz, wariant 1: nie wiem, to jest to co postać by zrobiła
* MG, wariant 1: akcja jest niemożliwa, dopóki nie określimy, jak wygląda sukces tej operacji

Wariant 1 pokazuje, że nie ma możliwości zrobienia ruchu, jeżeli wszyscy przy stole nie rozumieją konsekwencji tego ruchu w ten sam sposób. To jest tak jak zrobić zły ruch w szachach (np. przesunąć laufra jak hetmana). To jest gra niezgodna z zasadami. Chodźmy dalej.

* Gracz, wariant 2: Dzięki temu będziemy w stanie zabić tego smoka
* MG, wariant 2: Chwila, nie widzę tego. Pokaż mi krok po kroku jak zatrucie studni doprowadzi do zabicia smoka.

Wariant 2 jest bardzo podobny do wariantu 1. To jest jedna z silniejszych stron EZ - jeżeli gracz operuje przy błędnych założeniach (z perspektywy MG), to znaczy, że rozjechał im się SIS (Shared Imaginary Space). Dzięki przejściu krok po kroku - zrobienie pobieżnego planu na poziomie strategii – MG (lub inni gracze) są w stanie skorygować gracza z perspektywy danej akcji.

Jeżeli gracz robi to z jeszcze innego powodu – przykładowo, chce iść za celem indywidualnym a nie celem grupowym, to wychodzimy od kontraktu sesji. Czy gracze mogą przedkładać cele indywidualne nad grupowe. W wypadku niektórych sesji Fox + Kić? Tak. Zwykle Fox + Kić? Niezbyt. Zwykle Kić + Anadia? Tak. Randomy? Absolutnie nie – to stworzyłoby akcję PVP: Gracz kontra Reszta Zespołu.

Jak rozwiązać ten typ problemu? Silnie osadzić akcję w celach. Nie da się robić YOLO ruchów w EZ. Każdy ruch jest podparty o jakiś cel, a cel można rozbić na serię kroków. Proste. 

Jak pomóc graczowi, który to robi a nie chce (np. jest zagubiony czy się nudzi i robi WHATEVER)? Też proste - trzeba zbudować odpowiednio trudną sytuację, wyrzucić w to gracza i gdy gracz próbuje zrobić akcję YOLO wykorzystać dialog z jednego z wariantów. Pokazać graczowi jak przekształcić STRATEGIĘ w serię TAKTYCZNYCH AKCJI. Za którymś razem będzie działać. Detale: przeczytaj sekcji o planowaniu.

---
##### 5.3.1.5. Gracz Chcący Wyzwań i Cierpienia

Typowe teksty: 

* "To nie było takie trudne; dało się to przejść stosunkowo łatwo"
* "Wydawałoby się, że Kuratorzy będą jakimś wyzwaniem."

Co z tym robimy: 

Istnieje pewna populacja Graczy, dla których bardzo ważne jest to by można przegrać do zera. By nie tylko ich ruchy miały znaczenie, ale też by ich błędny ruchy zniszczyły ważne rzeczy. By móc faktycznie wygrać to na czym nam zależy. To są ludzie grający na stopniu trudności „ULTRA BRUTAL!”.

Jeżeli mamy zespół ludzi tego typu, jest super. Patrzymy na sekcję tego dokumentu „ stopnie trudności” i podkręcamy na maksimum. Sesja ma dużo mniejszy zakres niż zwykle, ale będzie dużo trudniejsza i będzie miała bardzo dużo niezmienników. Co więcej, MG daje pełne prawo planowania - ale po 4 godzinach dochodzi do tej katastrofy (czytaj: MG wygrywa). Czyli gracze mają możliwość w pełni zarządzać swoim czasem.

Jeśli jednak mamy zespół mieszany, mamy problem. 

Co można zrobić i co warto zrobić: Zdefiniować o co gramy. 

* Jeśli gracz wie, że np. celem sesji jest decyzja, to nie będzie czuć się oszukany, jeśli zrobią błąd taktyczny a i tak mogą dojść do tej decyzji.
* Jeśli gracz wie, że uratują dany statek kosmiczny ALE w zależności od dobrego planu zginie mniej lub więcej osób, gracz ma jawne High Score – punktację „jak dobrze poszło”. Wtedy warto zrobić tak, że 25% jest łatwa do uratowania, 25% jest trudna, 25% wymaga cholernie dobrego planu a ostatnie 25% to chyba się nie da. Odpowiednio dobrzy gracze i tak udowodnią, że się da.

Co więcej, nie będzie też problemów z przesterowaniem stopnia trudności. Jeśli coś jest za trudne i testy są za trudne - gracz po prostu podda walkę i podejdzie z innej strony lub zmieni cel („tych już się nie uda uratować”). 

W tego typu sesjach warto zrobić tor apokalipsy oraz ograniczone zasoby. 

**To jest problem którego jeszcze nie rozwiązałem, acz mam pewne przemyślenia jak widać.**

---
##### 5.3.1.6. Gracz Chcący Ukojenia / Spokoju

Typowe teksty: 

* "To jest za trudne, wszystko poszło w cholerę!"
* "Słuchaj, mam gorszy tydzień - zrobimy prostszą sesję? Lub mniej mroczną?"

Co z tym robimy: 

Gdy byłem młodszy, bardzo dużo grałem w GW2. Byłem jednym z najlepszych mesmerów na serwerze, wygrywałem stabilnie gry 1v3. 12 lat później jestem casualem. Nadal jestem dobry - ale Jestem tylko dobry. 90% percentyl, nie 99% (a bądźmy szczerzy, 90% wobec generalnej populacji to niewielkie osiągnięcie).

Czasami ktoś nie chce mieć bardzo ciężkich wyzwań czy morderczych sesji. Czasami ktoś chce przeżyć fajną opowieść i podjąć jakieś ciekawe decyzje. Czasami ktoś chce zobaczyć pozytywny świat i nie musieć o wszystko walczyć, zwłaszcza jak w życiu w danym momencie jest ciężko.

I ok.

W tych momentach należy dramatycznie przesunąć wajchę z challenge-first na story-first. Skupić się na ciekawej opowieści o Theme&Vision który interesuje graczy i MG nie będzie walczył przy użyciu (X) by wszystko poszło w cholerę a będzie walczył o pewne wydarzenia okolicznościowe czy rzeczy mniej bolesne dla tych graczy.

EZ nadaje się na łagodniejszą grę. Nie jest to wszystko do czego się nadaje, ale da się to zrobić. Jako przykład - na sesjach konwentowych (lub jak gram z graczami 15-) zakładam, że to jest strategia którą powinienem przyjąć.

---
##### 5.3.1.7. Gracz Który Chce Patrzeć I Nie Chce Grać

Typowe teksty: 

* "To ja w sumie nie wchodzę do tego konfliktu; popatrzę co się stanie."
* "Poczekam, na razie nic nie robię."

Co z tym robimy: 

Istnieją bardzo różne interesujące gry RPG. Przykładowo, D&D pozwala każdemu graczowi na to, by grał na minimalnym poziomie energii. Ma jakąś tam postać, wykonuje ruchy zgodne z kartą, mało się odzywa, dobrze bawi się siedząc w grupie innych osób. „Gra imprezowa, RPG edition”.

EZ nie jest tego typu grą.

EZ jest grą szybką z dużą ilością decyzji, która potrafi być bardzo męcząca przy odpowiedniej sesji. Zarządzanie „kto mówi, kto miał ruch” itp. jest po prostu nudne i uciążliwe. Inni gracze widząc, że ktoś tylko siedzi będą próbowali dać temu graczowi akcję. 

Z perspektywy EZ ktoś, kto po prostu siedzi i nic nie robi jest ogromnym kosztem dla wszystkich obecnych.

Wniosek: EXTERMINATUS. Ten typ osób nie ma miejsca w tym systemie. Jeśli ktoś dłuższy czas nic nie robi, MG - przekaż mu dowolny ruch w dowolnym momencie. To co ja zwykle robię, tworzę rzecz dosyć oczywistą i każę temu graczowi wykonać tą akcję. No nie ma jak na to nie wpaść.

Jeśli to go nie uruchomi, to do końca sesji będzie dostawał tego typu akcje oczywiste. Ale jeżeli go to uruchomi chociaż troszeczkę, dam mu też więcej ruchu na poziomie strategii. Może po prostu jest to osoba nieśmiała.

Jednak, jeśli nie chce grać – niszczy melodię przy stole. Jest kosztem.

---
##### 5.3.1.8. Gracz Który Chce Mieć Swoją Ciekawą Postać

Typowe teksty: 

* "Tak naprawdę gram nieślubnym synem admirała floty"
* "Jeśli mieszkam na terenie Aurum, najpewniej potwór jakiej energii zniszczył mi dzieciństwo?"
* "Chciałbym odbudować potęgę swojej rodziny i zemścić się na swoim bracie"

Co z tym robimy: 

Cele indywidualne! Zwłaszcza na poziomie wizji. Dodawanie komponentów do sesji, w których ta postać może osiągać częściowo swoje cele zbliżając się do tego co chce osiągnąć. Nawet na sesjach opartych o plot uwzględniać komponenty oparte o character. Jest to bardzo cenny gracz z perspektywy EZ, bo obecność tego typu graczy pomaga MG zrobić coś serio fajnego z kolizji wizji wielu graczy i swojej.

EZ jest jednym z lepszych systemów który może pomóc w prowadzeniu takich postaci - przy odpowiednim pozycjonowaniu pojawiają się nowe obszary (V) i (X) które pozwolą na prawidłowe dodawanie i odejmowanie rzeczy z historii i fabuły danej postaci. Tego typu graczy należy wspierać w ten sposób, we mają możliwość zobaczenia we historia ich postaci też porusza się do przodu, ale też idzie w złą stronę w zależności od ich decyzji.

To czego nie możesz robić - dopóki zespół nie będzie odpowiednio zgranej nie konfliktuj celów indywidualnych z celami grupowymi. Zamiast zbudować dramaturgię, zbudujesz konflikt w zespole. A EZ nie jest dobrym systemem do gier pvp.

Ważne - ta postać nie może być całkowicie supportowa. Przykładowo, Klaudia, która znajduje się na Inferni nie do końca ma jak robić progresję swoich celów i swojej Wizji. Czyli - skup się POWAŻNIE na pozycjonowaniu takiej postaci.

---
#### 5.3.2. Środowisko / otoczenie
##### 5.3.2.1. Preambuła

Nie tylko Gracze stanowią ograniczenie. Mamy też ograniczenia wynikające z fizyki i świata rzeczywistego ;-).

---
##### 5.3.2.2. Nowy Skomplikowany System I Świat

Nie przeskoczymy tego – EZ to tak twarde science fiction jak potrafiłem zaprojektować z dodatkiem magii opierającej się na regułach fizyki. Z logicznymi frakcjami, importem i eksportem i wszystko ma sens i stanowi jedną całość.

Z perspektywy nowej osoby wchodzącej w system - lub po prostu osoby, która próbuje coś zrobić - ten zbiór ograniczeń jest absolutnie katastrofalny. 

Jeśli powiem zdanie "Orbiter ma do czynienia z korwetą dowodzoną przez tien Diakon, sprzężoną sentisiecią z korwetą przy energii Ixion/Sempitus", to od razu widać kilkanaście rzeczy, które całkowicie poszły nie tak - jest to bardzo dziwne.

Myślałem, że rozwiązaniem jest działanie w Agencji światach Noctis. Myliłem się. Magda mi udowodniła, że światy astoriańskie są dużo bardziej atrakcyjne, co potwierdziły Fox i Kić. Jednocześnie Magda mi udowodniła, że da się to zrobić - da się wprowadzić nowych graczy w światy astoriańskie przy całej ich złożoności.

Nie zmienia to faktu, że przy całkowicie nowej, odmiennej mechanice i tak skomplikowanym świecie pierwsze sesje są tutorialami na wielu poziomach jednocześnie.

---
##### 5.3.2.3. Discord, online

3 osoby siedzą przy stole podczas sesji. Dobrze się bawią. Mają mimikę, mają różne zmysły. Gdy ktoś planuje i nic nie mówi to patrzysz i widzisz zmarszczenie na pyszczku.

3 osoby są na Discordzie, grając zdalnie. Statystycznie jedna gra w Minecrafta. Jeżeli nie ma ciągłej intensyfikacji działań, ktoś zaczyna się wyłączać. MUSZĘ redukować fazę planowania - choć mogę robić planowanie bardziej w formie online, gdzie ludzie wymieniają się notatkami rysują rzeczy i je pokazują.

Widać wyraźnie różnice pomiędzy tymi trybami prowadzenia.

---
##### 5.3.2.4. Konwent, offline

4-6 randomów gra. Każda osoba jest inna i przyszła dla czegoś innego. Mamy mało czasu. Każda osoba ma 10% czasu. Ludzie się zapętlają i dookoła jest głośno. Jedna osoba jest świeża i w sumie chciała zapisać się na Warhammera ale nie było dla niej miejsca. 

To muszą być bardzo proste sesje – SZEROKIE w zakresie, z ciekawymi postaciami i decyzjami, ale wartkie i szerokie. Graczom powinno się udać. A limit czasowy jest święty.

---
##### 5.3.2.5. Ograniczenie czasowe

3.5h / 4h. Ani minuty dłużej. Jedna sesja - jeden wątek.

Czy tak musi być? Niekoniecznie. Ale to musiałbym przedyskutować z Szanownymi Dziewczynami ^^. Mogę iść po trybie Stary Doctor Who - jeden "odcinek" to cztery "sesje" i w ten sposób sesja może mieć 10h+, co powiększa zakres i możliwości jakie mamy na pojedynczej sesji.

## 6. Wątpliwości, potencjalne problemy, uwagi
### 6.1. Sesja przechodzi się sama

Problem: 

* Częściowo sesja przechodzi się sama bez możliwości porażki
* Brak możliwości porażki zabiera sukcesowi smaku
* Koszty kosztami, ale jak się uprzecie to zrobicie
* -> WIĘC sesja przechodzi się sama

Odpowiedź: 

* Tak jest, jeśli sesja jest w strukturze: story-first, difficulty-low
    * Nawet wtedy można zrobić odpowiedni high-score, by ruchy graczy miały znaczenia (jeśli są różni gracze przy stole)
    * Można zdefiniować o co toczy się sesja z perspektywy wyzwań, jeśli sesja jest story-first i chodzi np. o decyzję
    * patrz (5.3.1.5. Gracz Chcący Wyzwań i Cierpienia)
* Tak nie musi być, jeśli sesja jest w strukturze: challenge-first (ale to wymaga konkretnej populacji graczy)
    * Przykład: "Ziarno Kuratorów na Karnaxianie". Tam gracze przez: 
        * bardzo trudne pozycjonowanie
        * serię pechowych rzutów 
        * złą selekcję konsekwencji (X) 
        * -> poddali cel sesji, zamiast tego skupiając się na ratowaniu okolicy. Nie uratują załogi Karnaxiana, ALE zapewnią by Karnaxian był jedyną problematyczną jednostką.
    * Przykłady tego typu sesji to też 'Whispraith na Neikatis'; większość "horrorów" jakie prowadzę mają wysoką letalność postaci na których graczom jednak zależy (nawet jak nie swoich)

### 6.2. EZ jako system jest niespójny z perspektywy gracza - co jest ważne a co nie, planowanie, akcja...

Problem: 

* Zwykle sesje to akcja - akcja - problem - akcja
* A potem nagle jest sesja typu 'statek z ogrodem i LOL WYSKAKUJE APOKALIPTYCZNY TERRORFORM' wtf
* Paraliż i nie wiadomo co robić. Czyli jak w sumie grać w EZ? 
    * Planować i patrzeć na detale? Źle, bo "marnujemy czas". 
    * Nie planować i ignorować detale? Źle, bo "lol terrorform były ślady".
* -> WIĘC nie wiadomo jak uważnie patrzeć na różne rzeczy na sesji i jak patrzeć na sesję

Odpowiedź: 

* EZ optymalizuje adaptację i radzenie sobie w DOWOLNYCH warunkach używając TEGO CO MASZ. Cynefin 3 (Complex).
    * Jeśli masz na sesji "NAGLE WYSKAKUJE ALIEN" to gracz ma: 
        * mechanikę, za pomocą której może powołać rzeczy
        * sprzęt, PRZYGOTOWANIE postaci. Postać domyślnie jest przygotowana, nawet jak gracz nie zrobił tego aktywnie. Takie rzeczy masz na KARCIE POSTACI lub w głowie.
    * -> adaptacja dynamiczna do nowej sytuacji
        * tak, poniesiesz straty, ale nie zginiesz. Możesz musieć ZMIENIĆ CEL, ale nie stracisz wszystkiego.
            * MG może poddać konflikt. Często to robię jak "nie chcę więcej od konfliktu teraz".
    * Jeśli gracz nie ma pomysłu jak i w jaki sposób sobie poradzić, STOP, zatrzymujemy, dekompozycja (planowanie) w środku akcji
        * TAK można pójść w dynamiczną szybką walkę. Ale nie zrobię tego graczom z zaskoczenia.
            * plus, do tego są lepsze systemy.
* -> EZ jest systemem **ze slow-motion**. Gracz może nie wiedzieć, postać ZAWSZE jest kompetentna. Postać ma odruchy, gracz może pomyśleć.
    * dzięki temu nie muszę Was ostrzegać "wiecie, tu może być jumpscare"
    * dzięki temu możecie planować gdy to potrzebne lub obudzić się w czarnym tyłeczku jak to potrzebne
    * i ZAWSZE macie mechanikę która jakoś Was uratuje - bo nawet jak nic nie wymyślicie, ja mogę poddać konflikt po odebraniu Wam celów i 
        * "oki, jedyny cel jaki Wam zostawiam - uciekacie z tego statku, nic innego nie osiągniecie"

Główna zaleta: **NIE MUSZĘ WAS OSTRZEGAĆ JAKA TO SESJA!** Nawet jak Was źle pozycjonuję, zawsze mogę poddać konflikt (jak to z mojej winy; else, raczej nie poddam).

### 6.3. Gracze się potencjalnie nie uczą

Problem: 

* Jeśli mechanika nas chroni przed tym, co dla nas byłoby trudne, to nie urośniemy w tą stronę
* A w sumie chroni nawet przed rozmową z npcami jeśli tego użyjemy.
* Ten styl może produkować słabszych graczy nie będących dość bystrymi na większe plany
* -> WIĘC jest ryzyko, że gracze się nie uczą i nie stają się lepsi

Odpowiedź: 

* EZ uczy, ale innych rzeczy. Kopiuję z (4 - Dokument Wizji)
    * Zawsze coś da się zrobić. Nigdy nie trać nadziei.
        * "każdego smoka da się rozłożyć na serię mniejszych kroków"
    * Jesteś w stanie negocjować sukces i porażkę
        * "nie wygrasz wszystkiego, ale w każdej sytuacji w której COŚ masz czego chce Druga Strona możesz coś uzyskać"
    * W sytuacji niespodziewanej, masz rzeczy 'dookoła' które możesz zaadaptować i użyć
        * "wszystko jest bronią jak jesteś dość zdesperowany"
    * Nie każdą rzecz warto zrobić - czemu to robisz? Jakie są konsekwencje?
        * "czy warto poświęcić coś ważnego na czym Ci zależy za 45% szansy na coś więcej?"
        * "jeśli nie masz celu i nie wiesz po co coś robisz, poświęcasz / ryzykujesz zasoby i nic nie dostaniesz"
* Nie jestem "prawdziwą mamą". Nie każda sesja ma ten sam stopień trudności, cel itp. **Ten dokument pokazuje Wielką Wizję Sesji RPG By Żółw**.
    * jeśli ktoś ma gorszy dzień, zrobię prostszą / optymistyczniejszą sesję
    * jeśli mam zespół żądający ciężkich akcji, zrobię sesję cięższą
    * ALE: jeśli mam Przestraszonego Nowego Gracza, zrobię sesję łagodną z sukcesami tej osoby (patrz: 5.3.1.3. Gracz Przestraszony, Wyłączający się)
* -> Jeśli mam gracza który na razie się uczy nowych rzeczy w obszarze np. ryzyka, to nie zmuszam do uczenia się w obszarze komunikacji.
    * jak chce, zacznie robić i dostanie bonusy
    * ale to wynika z tenetów - nie musisz być wygadany by grać bardem. Pomoże, tak, ale to nie terapia a gra.
        * JEŚLI gracze chcą się uczyć XXX, niech mi powiedzą XXX.

TAK, prawda, ten styl może zatrzymać gracza w rozwoju w którymś z wymiarów. Ale to GRACZ musi tego chcieć. Tak być nie musi. 

Ale nie jestem jego prawdziwą mamą by go zmuszać do ewolucji w innych wymiarach niż tych wynikających z tenetów (Dokument wizji - po co jest EZ).

### 6.4. Gracze się uczą robić ruchy bo "jakoś to będzie"

Problem: 

* Po grze w EZ gracz zaczął pushowac w robienie akcji bo "jakoś to wyjdzie" zamiast planowania czy zbierania danych.
* Gracz zaczyna potencjalnie patrzeć "jak postać zginie zrobię nową" i "nie zależy mi by frakcja z którą współpracuję przeżyła"
* EZ promuje wykonywanie ruchów, bo się udadzą przez dynamiczną adaptację
* -> WIĘC EZ może generować ten typ podejścia czy ruchów

Odpowiedź: 

* Patrz: gracze robią ruchy YOLO (5.3.1.4. Gracz Robiący YOLO ruchy)
    * Musisz podpiąć pod intencję. Zwłaszcza intencję grupy.
    * Musisz pokazać logiczny łańcuch prowadzący do sukcesu.
    * Cele indywidualne ZWYKLE (poza nielicznymi sesjami) są wtórne do celu grupowego.
        * Nawet jeśli graczowi nie zależy i robi ruchy to I TAK musi je uzasadnić pod kątem celów i sukcesów bo nie wejdą w fikcję.
* Priorytetyzuję dynamikę i adaptację (co wynika z Cynefin, patrz: 5.1.4. Dynamiczna akcja vs Planowanie)
    * ALE nie promuję ruchów "bo tak". Cały system jest zaprojektowany by to było niemożliwe

Dlatego nie sądzę, by to akurat było spowodowane przez EZ. Nie tym razem. EZ blokuje to bardzo skutecznie, więc powinno trenować WŁAŚNIE TO.

