---
layout: mechanika
title: "Notatka mechaniki, 230507 - Mechanika dynamicznych żetonów"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

## 1. Purpose

Odkąd mamy pule modyfikowane dynamicznie, stary mechanizm 'TrZ+2' przestaje działać.

## 2. High-level overview

* Maksymalna ilość przewag / niedowag: 3. Potężne mają moc '3'. Zwykłe mają moc '1'.

## 3. Mikro-mechaniki które wykorzystuję

* aspekty (defensywne i ofensywne, kontrowanie aspektów itp)
* dynamiczna pula (dodawanie i odejmowanie per strategia itp)
* kolorowanie żetonów (V -> Vg)
* typy starć: ciało (siła, prędkość, wytrzymałość), społeczne (grupa, percepcja, przegadanie), duchowo-magiczne (morale, magia, zmobilizowanie się), intelektualne (taktyka, psychotroniczne)

## 4. Wyjaśnienie na przykładach (wszystko ze spoilerami)

### 4.1. Jim Raynor vs Tychus, w barze (Starcraft 2)

https://youtu.be/agnbcNY7K0U?t=65

To jest dobra bitwa pokazująca szerokość mechaniki i narzędzi mechanicznych

* Cel Raynora: 
    * mieć nieuszkodzony statek i załogę, która mu ufa
    * mieć kohezję zespołu
    * nie zostać rannym
    * móc dalej korzystać z Tychusa (czyli np. rift Tychus - załoga nie może paść)
    * odzyskać kontrolę nad Tychusem. Nie musi kochać, ale musi być możliwość by mu ufać.
* Cel MG: 
    * uszkodzić / zniszczyć zasoby Raynora (m.in. Tychusa)
    * uszkodzić / zniszczyć reputację Raynora
    * zmusić Raynora do decyzji co robią dalej

Procedura walki (V zawsze dla gracza):

* pula startowa:
    * 8X (zwykłe, Raynor pokazał, że traci swoją drogę po wizjach Zeratula i wpada w alkoholizm po drodze)
    * 6V (zwykłe, Raynor wielokrotnie pokazał co i jak, ludzie mu zawdzięczają i jest kompetentny w walce)
    * = 8X 6V
    * wchodzą w grę np. aspekty społeczne ('Tychus to outsider', 'wszyscy ufają Raynorowi' o mocy 1 ea)
    * wchodzą w grę np. aspekty bojowe ('Tychus ma power suit' o mocy 3)
* Tychus: "you got them all fooled, Jimmy, you run at sign of troubles..."
    * atak **społeczny** w grupę sojuszników, by wyizolować Raynora i uszkodzić kohezję. I/lub **emocjonalny** by tauntować.
    * jeśli Raynor próbuje zdeeskalować, ma pulę 8X6V i konieczność osiągnięcia głębszego sukcesu (3)
    * Raynor musi odeprzeć 'taunt' by nie zaatakować oraz dobrze wypaść przed innymi.
    * Zauważcie - Tychus jest tu 'in control'.
        * Taunt idzie po czymś co Raynora naprawdę boli ("Kerrigan to bolesna rana") więc przekolorowuję 4X -> 4Xr (potężny cios emocjonalny)
    * +3Vz -> gracz chce wykonać manewr ESKALACJI. Dobrze zna Tychusa oraz swoją załogę.
        * Vz to 'zasób', ale tym razem czytaj jako PRZEWAGA.
        * jeśli gracz opisuje że go tauntuje by Tychus zaczął, dam mu +2Vr
    * Pula: (4X 4Xr)(6V 3Vz 2Vr) na odparcie taunta | 19 żetonów
        * Raynor wyciąga któryś V
        * Raynor odzyskuje pęd
        * Raynor eskaluje by się zrecentrować
            * Raynor wyciąga któryś V. Pula się regeneruje do podstawowej (8X 6V 2Vr)
        * komentarz: 
            * Xr spowodowałby odpalenie aspektu Raynora 'nie mogę sobie wybaczyć co się stało Kerrigan' albo 'faktycznie zawodzę swoich ludzi', co daje mi jakiś Compel.
* Raynor rzuca w Tychusa niedopałkiem
    * atak **emocjonalny** mający tauntować Tychusa. 
    * pula aktualna: (8X 6V 2Vr)
    * Dodaję +3Vz za potężną przewagę - Raynor dokładnie wie jak Tychus walczy i wie, że Tychus ma przewagę sprzętu i wie, że Tychus nim chwilowo gardzi / jest nań zły.
    * Dodaję +3Vg za potężną przewagę - Tychus jest nietrzeźwy, jest łatwy do tauntowania.
    * Utrzymuję +2Vr za pomysł.
    * pula: (8X 6V 3Vz 3Vg 2Vr) czyli (8X - 14V) | 22 żetony
        * Raynor wyciąga 'X': bar zostanie zdewastowany w walce (nie oddaje pędu, poświęca okoliczność)
        * Raynor wyciąga 'V': Tychus zostanie sprowokowany i zaatakuje
* Tychus atakuje - rzuca rzeczami i szarżuje. (atak **fizyczny**)
    * jako, że wszystkie poprzednie przewagi działają bez zmian, mogę kontynuować pulę.
    * aktualna pula: (7X 5V 3Vz 3Vg 2Vr) czyli (7X - 13V) | 20 żetonów z 1V1X poza pulą
    * Tychus ma niesamowite defensywy; Raynor nie ma nic co może zrobić. Mimo, że Raynor ma pęd, musi odeprzeć podstawowe ciosy i ataki zanim w ogóle może coś zrobić z tym dalej.
        * Koloruję 4X -> 4Xr jako HEAVY DAMAGE.
    * Intencja Raynora: "wyjść z bezpośredniej walki i znaleźć otwarcie". Otwarcie będzie wymagało odparcia serii ataków i uzyskanie inicjatywy raz jeszcze, czyli V+V.
    * pula: (3X 4Xr 5V 3Vz 3Vg 2Vr) czyli (7X - 13V) | 20 żetonów z 1V1X poza pulą 
        * (power suit NIE DAJE przewagi w prędkości a tylko w sile!!!)
        * V: uniknięcie serii podstawowych ataków (szarża, cios w ziemię, cios w powietrze)
        * V: dodatkowy taunt (i trzeci ptaszek) rzuconą butelką. Ten taunt ma zrobić 'opening'.
            * gdyby w konflikcie emocjonalnym pula była inna, dodalibyśmy V lub X do puli jak należy - tym razem były podobne.
* Opening realizuje się przez stworzenie aspektu środowiskowego 'danger! electric current'. Więc Tychus rzuca obiektem (tworząc hazard) i Raynor musi go porazić prądem
    * Raynor ma pęd / inicjatywę, więc nie ma konieczności robienia testu uników, to ciąg dalszy operacji (X zmieni inicjatywę a nie zrani Raynora)
    * pula bez zmian: (3X 4Xr 3V 3Vz 3Vg 2Vr) czyli (7X - 11V) | 18 żetonów z 3V1X poza pulą 
        * V: electrocute; defensywy Tychusa są w power suicie który ma anty-aspekt na elektryczność, więc Magnitude jest kategorię wyżej, więc czwarty V działa jak szósty.
            * Tychus down
* Wykorzystując sytuację Raynor interaktuje z resztą zespołu by ich zagrzać do walki po swojej stronie. Atak społeczny.
    * usuwam przewagi wobec Tychusa, bo, no, nie interaktujemy z Tychusem (-3Vg -3Vz)
    * dodaję przewagę: właśnie ludzie widzieli że Raynor rozwalił power suit (+3Vz)
    * zmieniam znaczenie 4Xr; to już nie 'mauler damage' a 'oki, Tychus rozwalił nam bar to jego wina' (zasada mięsa armatniego - ten krzyżyk uderza w reputację Tychusa a nie coś Raynora)
    * gracz walnął mówkę. +1Vr.
    * pula: (3X 4Xr 3V 3Vz 3Vr) czyli (7X - 9V) | 16 żetonów z 4V1X poza pulą 
        * V: sukces, Raynor kupił swoich ludzi. Oraz Tychusa (piąty ptaszek, i przy takim sukcesie akceptuję współpracę bo Tychus ma aspekt 'poważa siłę')

Jak widzicie, użyliśmy:

* przekolorowanie żetonów na znaczące
* dynamiczne przewagi na podstawie (wizualnej) różnicy aspektów
* rotacja po typach konfliktów
* inicjatywa / pęd
* regeneracja puli 'recentrowaniem'
* zmiana environmentals (electric hazard)
* compel (tauntowanie)
* magnitude ptaszka per aspekt

### 4.2. Kerrigan vs Narud (Starcraft 2)

https://www.youtube.com/watch?v=v-eJezQV1cY

* Cel Kerrigan: 
    * zniszczyć Naruda
* Cel MG: 
    * zmusić Kerrigan do ucieczki (i zapewnić, że ta strategia nie zadziała)
        * Narud zostaje tam gdzie jest i może robić machinacje
    * zadać trwałe obrażenia Kerrigan

Ta bitwa skupia się na przeskakiwaniu pomiędzy typami konfliktów (emocjonalny / fizyczny) oraz na ciągłej zmianie inicjatywy.

Tak Kerrigan jak i Narud mają pancerz kinetyczny. (Kerrigan: regeneracja / carapace, Narud: pole magiczne / 'shield')

Bitwa zaczyna się tym, że Kerrigan wchodzi na teren kontrolowany przez Naruda (shapeshifter). On ma inicjatywę z definicji. Narud próbuje zdecentrować Kerrigan. Walka zaczyna się **nie** od walki fizycznej a od walki **emocjonalnej**. 

Narud ma silne przewagi: 

* przewaga zaskoczenia i znajomości terenu 
* przewagę tego, że Kerrigan ma ciężką ranę emocjonalną a on jest shapeshifterem
* jego pancerz jest liczony jako superciężki, więc trzeba umożliwić jego zdjęcie jako aspektu.

.

* Pula podstawowa: Tr, czyli 8X 6V
* W walkach emocjonalnych dotyczących Raynora lub Kerrigan kiedyś, konflikt jest klasy Ex, czyli 11X 3V.

Procedura walki (V zawsze dla gracza):

* Kerrigan próbuje zlokalizować Naruda i przejąć inicjatywę.
* "Is that what you think" i forma Raynora
    * Narud nie konfliktuje bycia zlokalizowanym, ale konfliktuje inicjatywę i używa formy Raynora - stąd atak **emocjonalny**.
    * Kerrigan próbuje przejąć inicjatywę. Dzięki dobremu opisowi gracza ma +2Vr, ale... 
    * pula 11X 3V 2Vr (11X 6V) to pula po prostu FATALNA. Acz nie widzę nic co gracz może tu usprawnić. | 17 żetonów
        * X: nadanie Kerrigan tymczasowego aspektu 'the past conquers the present'. Kerrigan póki go nie zdejmie to Narud ma Przewagę wobec niej w temacie emocjonalnym.
            * z uwagi na SŁABOŚĆ Kerrigan, to działa jak trzeci krzyżyk.
        * X: zadanie Kerrigan RANY. Jej pancerz jest uszkodzony. (aspekt pancerza i / lub regeneracji przestaje działać)
        * V: Kerrigan przejmuje inicjatywę ("No, you're not him" + odrzucenie przeciwnika)
* Kerrigan ma inicjatywę, atakuje używając Nydus Worm do zwiększenia pędu. Musi uszkodzić pancerz. Atak **fizyczny**
    * zmiana puli: Ex -> Tr (bo nie emocjonalne), Kerrigan ma gigantyczną przewagę pędu (Nydus worm), więc +3Vz.
    * pula: 6X 5V 3Vz 2Vr   (10V 6X)    | wyjęte: XXV | 16 żetonów
        * V: Kerrigan skontrowała atak magiczny Naruda używając worma i Narud sam rozwalił swój pancerz XD
        * X: Kerrigan zadaje serię ciosów pod wpływem emocji, oddając inicjatywę Narudowi. ("you're not him!" zamiast użyć power attack).
            * Narud morphuje w Kerrigan "I'm everything you lost."
* Narud przechodzi na atak **emocjonalny**, ponownie, by otworzyć atak fizyczny.
    * zmiana puli: Tr -> Ex (bo emocjonalny). Gorzej, mamy aspekt 'past conquers the present' (+1Xr). I Kerrigan nie ma dostępu do worma. Czyli usuwamy 3Vz.
    * pula 8X 1Xr 2V 2Vr (4V 10X)    | wyjęte: XXVVX
        * A Kerrigan **musi** odzyskać inicjatywę
    * X: potężny atak z zaskoczenia (przez aspekt 'past conquers present') zadając Kerrigan Ranę.
    * gracz Kerrigan sugeruje użycie bólu oraz podniesienia skali Rany jako zasobu by odzyskać kontrolę. MG się zgadza. 
        * Aspekt 'past conquers present' zostaje wyłączony (przez ból), 1Xr znika.
        * +3Or -> reprezentując sukces przy podniesieniu poziomu Rany
        * +3Vz -> ból jako zasób w ataku emocjonalnym (w którym jesteśmy).
        * +2Vr za 'gracz ma jaja' ciągnąc przy piątym krzyżyku i mając dobry pomysł.
    * zmiana puli: 7X 1V 4Vr 3Vz 3Or (8V +3Or 7X)    | wyjęte: XXVVXX
        * przy 'X' (piąty krzyżyk) silnie rekomendowałbym graczowi zaakceptowanie mojego rozwiązania - Kerrigan jest bardzo ciężko ranna, ale da radę uciec. MG dostaje wszystko co chciał.
            * piąty krzyżyk nie znaczy że MUSZĘ zabić postać. Ale daję Kerrigan negatywny aspekt 'boi się Naruda', 'nie wygra z Narudem', 'ciężko ranna' i ją wycofam.
            * jeśli gracz nie zaakceptuje... zabiję Kerrigan. Lepiej niech nie eskaluje.
            * ale tu Kerrigan eskaluje XD.
    * Or: Kerrigan przejmuje inicjatywę kosztem podniesienia Rany do Poważnej Rany gdy Narud mówi "I am everything you have never had".
* Kerrigan chce zadać MAULER DAMAGE. Chce do swojego aspektu dodać ciężkie obrażenia. Zrobić opening Narudowi. Czyli musi mieć DWA ptaszki.
    * zmiana puli Ex -> Tr (bo fizyczny). 
        * Niestety, muszę do puli dodać +3Xr za Poważną Ranę (ciężkie utrudnienie w testach fizycznych).
        * usuwam przewagę 3Vz (ból nie ma tu znaczenia a raczej przeszkadza)
        * dodaję przewagę 3Vz (Kerrigan ma Naruda tam gdzie chce i ma przewagę zaskoczenia; Narud ma aspekt 'arogancki' i do tej pory wygrywa z Kerrigan łatwo)
    * czyli pula: 4X 3Xr 4V 4Vr 3Vz 2Or (11V +2Or 7X)    | wyjęte: XXVVXXOr
    * V: 'trzeci ptaszek', następna rana jest MAULER DAMAGE. Objawia się jako "Kerrigan unieruchamia Naruda"
    * Or: death strike. Kerrigan też traci przytomność (ma szczęście że ma regenerację, która się włączy po pewnym czasie, bo to by ją zabiło).

Jak widzicie, użyliśmy:

* Ciągłej zmiany inicjatywy; Kto ma inicjatywę, ten decyduje o następnym ruchu
* Ciągłej zmiany form walki i aspektów tymczasowych
    * gdyby Narud odzyskał inicjatywę, aspekt MAULER DAMAGE by 'się zmarnował'. To jest jak 'mam go... a nie, udało mu się O WŁOS.'
* Odpalenie aspektów słabości postaci (Narud - arogancja, Kerrigan - przeszłość)
* Rotacji pul: Ex -> T -> Ex...

### 4.3. Gundam Barbatos vs Carta Issue + Honor Guard (Gundam Iron Blooded Orphans)

https://youtu.be/MB_gVIqvnWM?t=52

* Cel Barbatosa:
    * Szybko zniszczyć przeszkody na drodze pociągu
    * Usunąć elitarnego oficera przeciwnika (Carta)
    * Uniknąć ran
* Cel MG:
    * Unieruchomić pociąg, zmuszając graczy do jakichś kiepskich układów z lokalsami i do porzucenia cennego lootu
    * Uszkodzić jednostki graczy, by nie nadawały się do dobrej walki dalej

.

Tu bardzo mocno wchodzą aspekty. Carta gada dobrą minutę przed bitwą a jej honorowa gwardia nie ma pojęcia, że mają do czynienia z psychopatą (Mikazuki w Barbatosie). Więc - Carta daje im 30 minut na przygotowanie honorowej walki 3v3. Co na to Mikazuki? "oki, uruchamiam Barbatosa i atakuję zanim się zorientują korzystając z okazji że nikt się nie spodziewa".

Czyli Mika ma 2 poziomy Potężnej Przewagi: "Carta jest honorowa i spodziewa się honorowej walki", "Barbatos atakuje ze śniegu z zaskoczenia" ORAZ "Przeciwnik nie jest gotowy i jest poza mechami". 2 poziomy oznaczają, że obniżam stopień z Tr do Tp.

* Pula podstawowa: Tp+2, czyli 5X 9V 2Vr
* Normalnie byłaby to bitwa 1v3. W praktyce - jest to bitwa 1v0. Nikt nie jest gotowy do walki poza Barbatosem.
* Deklaracja gracza: "eksterminacja". Gracz ma pęd / inicjatywę.
* Modyfikacja puli: +3Vz (Barbatos jest jednostką przełamania; nieporęczny, ale tu to nie ma znaczenia). To jest dosłownie strzelanie do kaczek, więc +1Vr.
* Pula: 5X, 9V, 3Vz, 3Vr.
    * V: Zaczynamy od konfliktu **społecznego i taktycznego** by zrobić zaskoczenie. Sukces, Barbatos ma ZARÓWNO inicjatywę jak i zaskoczenie.          | 5X, 8V, 3Vz, 3Vr      | wyjęte: V
    * V: Barbatos uderza 'od dołu' Graze, by zabić pilota. Sukces.                                                                                      | 5X, 7V, 3Vz, 3Vr      | wyjęte: 2V
    * V: Mika DALEJ ma pęd. Rzuca broń by unieszkodliwić i przewrócić drugiego przeciwnika. Mika może zrobić MAULER DAMAGE.                             | 5X, 6V, 3Vz, 3Vr      | wyjęte: 3V
    * V: MAULER DAMAGE. Przeciwnik nie żyje. Rozdeptany.                                                                                                | 5X, 5V, 3Vz, 3Vr      | wyjęte: 4V
        * Carta dostaje aspekt 'rozbita', ale jej Graze jest już aktywny. Barbatos odzyskał broń.
* Teraz:
    * Graze jest jednostką bardzo szybką i zwrotną, Barbatos jest ciężką jednostką przełamania.
    * Mika **dalej** ma inicjatywę.
* Carta uniknęła jednego uderzenia, ale dostała drugim (w tors). To jest kontynuacja poprzedniej puli.
    * V: Graze Carty ma niesprawny pancerz ORAZ traci rękę. Jest pokonany. Następne ruchy są tylko i wyłącznie fabularne (żaden atak nic nie zrobił Barbatosowi) | 5X, 4V, 3Vz, 3Vr      | wyjęte: 5V
* Carta NADAL nie ma inicjatywy, ale próbuje się wycofać. Jest to jedna z nielicznych akcji dostępnych postaci która nie ma inicjatywy.
    * ZMIANA PULI: 
        * +3Vg (reprezentuje kiepski stan Graze'a Carty; bardzo poważne zniszczenia)
        * Przekolorowanie krzyżyków na 'krzyżyk oznacza że ucieknie'
    * Pula: 5X, 4V, 3Vg, 3Vz, 3Vr  (5X 13V)    | wyjęte: 5V
        * V: Graze Carty nieaktywny, Carta na łasce Mikazuki / Barbatosa.

Jak widzicie, użyliśmy:

* Pęd / inicjatywa
* Taktyka totalnie zmieniłą stopień trudności konfliktu do trywialnego
* Ucieczka jest akcją możliwą nawet jeśli nie masz inicjatywy

### 4.4 Kunato + Totarou vs Gauna-Gard Type (Knights of Sidonia)

https://www.youtube.com/watch?v=vWagKj6po8Y

* Cel Zespołu:
    * Ochronić kluczowe struktury w statku-matce
    * Zniszczyć atakujące Gauny (potwory)
* Cel MG:
    * Zniszczyć kluczowe struktury w statku-matce
    * Uszkodzić Gardy (mechy) i je unieczynnić

Podczas walki mamy sytuację DWÓCH graczy JEDNA Gauna. To jest dość trudne do zasymulowania, bo żaden z nich nie jest 'główny' a żaden nie jest 'dodatkowy'. Na potrzeby tej operacji potraktowałbym ich jako jeden 'zespół', odpowiednio kolorując krzyżyki (porażki poszczególnych postaci). Aczkolwiek Kić słusznie mówi, że możemy rozwiązać to DWOMA pulami (jedna pula na "herosa", as in: "tylko herosi w HoMM3 mogą dowodzić swoimi armiami").

Na potrzeby analizy przykładu dalej skupiam się na założeniu, że pula jest jedna (bo jeśli są dwie to wszytkimi powyższymi przykładami to już rozwiązaliśmy; tyle, że Gauna)

Wychodzimy od celów gracza. Gracze chcą poczyścić Gauny i żeby niczego poważnie nie uszkodzić.

* Pula podstawowa: Ex -> 11X 3V
    * aspekty: dużo Gaun, konieczność osłony bazy, (hidden: Gauna-kishin): +3X
* Na początku inicjatywę mają GAUNY. Postacie graczy wchodzą na zastany teren.
* Gracze współpracują, mają wsparcie centrali i mają to przećwiczone. +3V, +2Vr. Pula -> 11X 6V 2Vr
    * V: Inicjatywa przechodzi do postaci graczy. To też znaczy, że Kunato omija wiązkę lasera Gauny-Kishin.        -> 11X 5V 2Vr
    * wyraźnie gracz ma inicjatywę, bo Kunato leci za Gauną w pościgu.
    * X: Inicjatywa przechodzi do Gauny (uderzenie, zderzenie się).                                                 -> 10X 5V 2Vr
* Gauna próbuje zadać ranę laserem. Kunato robi rozpaczliwy manewr - nie próbuje odzyskać inicjatywy tylko się broni. Kupuje czas Toutarou. (+3Vg) -> 10X 5V 2Vr 3Vg
    * Vg: Udało się, Toutarou dołącza i Kunato zestrzeliwuje 'nieważną' Gaunę, ale DALEJ inicjatywa jest w rękach Gauny. Czas przejąć inicjatywę. (-3Vg) -> 10X 5V 2Vr
    * X: Tourarou zestrzelony, ale to tylko jedna rana. Gard jest uszkodzony, ale da się coś z nim zrobić.  -> 9X 5V 2Vr
        * Kunato przechodzi do akcji TAUNT. (+3Vg) -> 9X 5V 2Vr 3Vg
    * Vg: TAUNT udany. Gauna nie może skupić się na Toutarou, musi walczyć z Kunato.
    * X: Gauna NIGDY nie miała niedowagi w tej walce. Leci od pleców i uderza w Kunato. Kunato jest wystawiony na MAULER DAMAGE i Gard jest uszkodzony (fabularnie, funkcjonalnie nic się nie stało)
        * Zespół MUSI wygrać inicjatywę. Kunato decyduje się na próbę walki o inicjatywę. (-3Vg) -> 8X 5V 2Vr
    * V: sukces. inicjatywa po stronie zespołu. To było to strzelanie z autocannona.
* Ruch Toutarou. Przeciwnik jest unieruchomiony i zaskoczony, więc Ex -> Tr (neutralizacja głównych sił gauna-kishin)
* pula: 8X 5V 2Vr -> 5X 8V 2Vr, ale sytuacja jest kiepska.
    * V: otwarcie następnego ataku na MAULER DAMAGE
    * V: egzekucja gauna-kishin

Co nowego się pojawiło:

* specjalna akcja jak nie masz inicjatywy: STALL. +3Vg, nie daje inicjatywy, pozwoli pojawić się sojusznikowi / sojusznik może zrobić ruch taktyczny.
* specjalna akcja jak nie masz inicjatywy: TAUNT. +3Vg (jak STALL), nie daje inicjatywy, zmusza przeciwnika na skupieniu się na Tobie a nie na preferowanym taktycznie celu.



