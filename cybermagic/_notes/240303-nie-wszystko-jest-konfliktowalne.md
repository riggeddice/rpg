# Nie wszystko jest konfliktowalne (240303)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox

---
## 1. Purpose

* Co podlega konfliktowi, co nie

---
## 2. Spis treści i omówienie

1. Konfliktowalność

---
## 2. Przykład
### 2.1. Rzeczy niemożliwe

Rzeczy poniżej nie są Konfliktem Heroicznym. Rzeczy poniżej są po prostu NIEMOŻLIWE:

* Normalny koleś próbujący przebiec przez normalną ścianę, ŻEBY dostać się na drugą stronę
    * Wynika z fizyki (miękkość ciała vs twardość ściany)
* Żołnierz szarżujący z bagnetem na czołg, ŻEBY go zniszczyć
    * Wynika z fizyki (odporność pancerza)
* Komandos łagodnie uspakajający płaczącego chłopca, ŻEBY się uspokoił i nie cierpiał
    * Wynika z założeń; chłopak stracił całą rodzinę, wszyscy zginęli na jego oczach
    * Nie ma nic co można powiedzieć co go uspokoi
* Mafioso próbujący rozkochać w sobie dziewczynę której długi spłacił, ŻEBY go naprawdę kochała
    * Wynika z założeń i... no, to tak nie działa.
* Mafioso próbujący rozstrzelać z AK-47 Supermana ŻEBY zranić Supermana i zadać mu ból
    * W założeniach świata Superman jest niewrażliwy na tego typu broń

### 2.1. Rzeczy możliwe

Rzeczy poniżej są możliwe:

* Normalny koleś próbujący rozwalić normalną ścianę młotem pneumatycznym, ŻEBY dostać się na drugą stronę
    * Wynika z fizyki - obecność odpowiedniego narzędzia umożliwia osiągnięcie sukcesu
    * -> Zmiana Okoliczności Konfliktu
* Żołnierz szarżujący z bagnetem na czołg, ŻEBY odwrócić uwagę czołgu od skradającego się kolegi z wyrzutnią rakiet
    * Zupełnie inny cel konfliktu - to jest możliwe
    * -> Zmiana Celu Konfliktu
* Komandos przepytujący płaczącego chłopca, ŻEBY dowiedzieć się co wie; nieważne, że cierpi
    * Zupełnie inny cel konfliktu - to jest możliwe
    * -> Zmiana Celu Konfliktu, poddanie niemożliwej części konfliktu
* Mafioso informujący dziewczynę, że jest jego żoną, ŻEBY udawała, że go kocha
    * Ma nad nią przewagę, ma środki i jest groźny.
    * -> Zmiana Celu Konfliktu
* Mafioso próbujący zabijć Lois Lane przy użyciu AK-47, ŻEBY zranić Supermana i zadać mu ból
    * W założeniach świata Lois Lane jest człowiekiem, podatnym na AK-47 jak my wszyscy
    * Stopień trudności konfliktu jest związany z prędkością reakcji Supermana
    * -> Zmiana Sposobu Osiągnięcia Celu (zranienia jakoś Supermana)

---
## 3. Niezmienniki

Istnieje koncept Niezmienników – rzeczy, których nie da się zmienić i które nie ulegają konfliktowi. Zwykle powiązane są z fizyką świata, rzeczami już zdefiniowanymi wcześniej, rzeczami potwierdzonymi konfliktem lub rzeczami silnie uzasadnionymi jako niezmienniki.

* Ludzie nie potrafią latać (chyba że mają odpowiedni sprzęt). Ludzie nie oddychają w próżni.
* Esurient nie jest w stanie się wyzwolić z Energii, która go pożera; nie da się go uratować.
* Ogólnie, ludzka psychika działa tak jak działa - nie ma magicznego słówka, za pomocą którego osoba cierpiąca przestanie cierpieć.
* Rodzice kochający swoje dzieci ich nie zdradzą bez bardzo silnej presji.
* Handlarz nie da Ci swojego towaru za darmo; musisz mu dać coś o odpowiedniej wartości, zastraszyć lub udowodnić, że to mu się bardziej opłaca.
* ...itp, itp

Niezmienniki są silnie osadzone w logice działania rzeczywistości i w konwencji, w której prowadzicie EZ. Ja prowadzę EZ tak realistycznie jak potrafię, ale nawet ja usuwam nieistotne elementy symulacji rzeczywistości.

Ważne jest to, że im więcej masz Niezmienników na sesji, tym bardziej rośnie stopień trudności tej sesji. Pewne Niezmienniki są potrzebne, bo inaczej Gracze mogą wymyślać cokolwiek i nie będą się dobrze bawić; Niezmienniki tworzą ramy sesji. Ale bardzo duża ilość Niezmienników redukuje kreatywność sesji.

---
## 4. Rola magii przy Niezmiennikach

Zgodnie z naszym dzisiejszym rozumieniem fizyki, FTL (podróże szybsze niż światło) są raczej niemożliwe. Rzeczy, które dzieją się w EZ nie zawsze mają pokrycie w fizyce. Tu wchodzi magia – jako wytrych. Dzięki magii rzeczy niemożliwe stają się możliwe.

Jako przykład - najbardziej efektywnym źródłem energii są baterie słoneczne przy Merkurym. Jednak zgodnie z dzisiejszą fizyką nie mamy umiejętności dobrego przesyłania energii. Dlatego wprowadziliśmy Irianium, specjalny metal stworzony przy użyciu magii (jak baterie litowo-jonowe, ale DUŻO lepsze). Dzięki temu tworzenie energii przy „Merkurym” i transport jej na „Ziemię” jest możliwy i jest bardzo opłacalny.

Do tego służy magia - ona umożliwi nam złamanie niektórych Niezmienników. Dodajmy magię do przykładów poniżej:

* Normalny koleś próbujący przebiec przez normalną ścianę, ŻEBY dostać się na drugą stronę
    * Energia Interis pozwoli na rozpad ściany, by koleś mógł przez nią przebiec
    * Energia Alteris pozwoli na zmianę geometrii rzeczywistości, by koleś mógł przez nią "przejść"
* Żołnierz szarżujący z bagnetem na czołg, ŻEBY go zniszczyć
    * Praktycznie dowolna kombinacja energii tworzących magię bojową pozwoli to osiągnąć.
* Komandos łagodnie uspakajający płaczącego chłopca, ŻEBY się uspokoił i nie cierpiał
    * Magia mentalna.
* Mafioso próbujący rozkochać w sobie dziewczynę której długi spłacił, ŻEBY go naprawdę kochała
    * Nadal magia mentalna.
* Mafioso próbujący rozstrzelać z AK-47 Supermana ŻEBY zranić Supermana i zadać mu ból
    * Energia Interis (do rozpadu mocy Supermana) + Exemplis (do wzmocnienia AK-47).

Czyli - "słaba magia" + "dużo Niezmienników" daje trudniejszą sesję z koniecznością poruszania się w okolicznościach sterowanych przez MG, a nie z szeroką kreacją przez Graczy. To prawidłowy styl grania, nie miejcie złudzeń. Ale wybierzcie to świadomie.

---
## 5. Konfliktowalność

Konfliktowi podlegają wszystkie rzeczy, które mają sens zgodnie z logiką świata, uwzględniając stan emocjonalny wszystkich obecnych, okoliczności tego konfliktu oraz cele do osiągnięcia.

Konfliktowi nie podlegają żadne rzeczy, które zmieniają jakiekolwiek Niezmienniki - niezależnie, czy Niezmienniki pochodzą z mechaniki, z definicji sceny czy z fizyki świata. Niezmienniki nie muszą mieć foreshadowingu (acz to dobra praktyka; nie chcecie, by Gracze czuli się "głupio" czy "oszukani"). Niezmienniki na sesji po prostu **są**.

Popatrzcie na taką sytuację:

* Gracze mają płaczącego chłopca. Gracze naturalnie chcą go uspokoić i dowiedzieć się czemu płacze.
* Gracze naturalnie chcą podejść łagodnie [sposób] i zależy im na tym, by chłopiec się uspokoił [cel]
* Jako MG wiecie, że chłopiec nie uspokoi się przy łagodnym podejściu - to jest w jego charakterze.
* Teraz gracze na mocy mechaniki mówią konflikt.
* Co w tej sytuacji?

Odpowiedź jest prosta:

* Gracze mogą się od niego DOWIEDZIEĆ czegoś, ZMUSIĆ do bycia cicho (zwykle siłą), DOPROWADZIĆ do bycia nieprzytomnym.
* Gracze nie są w stanie go USPOKOIĆ i sprawić, by nie płakał. To nie w jego charakterze. To jest niezmiennik.
* ALE jeśli techniki zastosowane przez Graczy **mogą** zadziałać, pójdę po konflikcie Ekstremalnym lub Heroicznym.

---
## 6. Pytanie: "Nie jest to strasznie uznaniowe i nie daje MG strasznie dużo władzy?"
### 6.1. Poziom symulacji (zrozumienia rzeczywistości, sceny, otoczenia konfliktu)

Tak, jest to bardzo uznaniowe. Tak, daje to MG bardzo dużo władzy.

* Ale czy da się przejść przez ścianę? Nie.
* Czy da się przebić przez ścianę mając odpowiednie materiały wybuchowe? Tak.
* Przez każdą ścianę? Nie. Są takie, których nie zniszczysz w ten sposób nie niszcząc całego budynku.

Odpowiedzialnością MG jest prawidłowe postawienie Niezmienników i stopni trudności. Jeśli to jest jakiś losowy budynek w normalnym kraju, nie da się przejść przez ścianę. Jeśli to jeden z tych „papierowych” budynków, gdzie mocniej walnie w ścianę to zrobisz dziurę – tak, da się przejść przez ścianę. To jedynie wymaga konfliktu o stopniu trudności odpowiadającym „ nie zostaniesz ranny”.

Wszystko zależy od konwencji i kontekstu świata. Stąd mój silny nacisk na logikę świata, fizykę świata i zrozumienia sceny.

### 6.2. Poziom narzędzi

Macie kluczowy mechanizm – **Intencyjność i Negocjacje - dzięki którym przypadkowo nic się nie spieprzy**.

* MG opisuje budynek, który ma być dobrze ufortyfikowany. Gracz zauważa duży słaby punkt – okna. MG przeprasza i rzeczywistość zostaje zmieniona by budynek – prawidłowo -był dobrze ufortyfikowany.
* Gracz opisuje, jak próbuje przebiec przez nasłonecznioną część stacji kosmicznej przy Merkurym. MG zaznacza, że tam jest ponad 400 Kelvinów temperatury. Ruch zostaje anulowany, Gracz osiągnie sukces inaczej.
* Ani MG ani Gracz nie musi wiedzieć wszystkiego. Wyprowadzicie to, co jest ważne podczas sesji, dokalibrujecie się i sami zbudujecie "swoje" zrozumienie świata EZ.
* Gracz pokazujący niespójność jest wrogiem w wielu "klasycznych RPG". W EZ - przez wbudowaną płynność - gracz jest sojusznikiem ratującym MG od błędu i pomagającym zbudować wspólny SIS (Shared Imaginary Space).

Macie **magię, która może nagiąć zasady rzeczywistości**.

* Nie da się teleportować w kosmosie, ALE jest energia Alteris.
* Nie da się oddychać w próżni, ALE jest adaptacja Ixiońska.

Macie **historię, która pokazuje ludzkie zachowania i geografię danego terenu**.

* Nikt normalny nie mieszkasz w bunkrze, ALE w okolicach Pustogoru mogą poruszać się Lewiatany, więc tam "żyjesz w pancernych budynkach".
* Noktianie i Astorianie ogólnie się zwalczają, ALE w obliczu potworów raczej staną po jednej stronie.

---
## 7. Pytanie: "EZ jest bardzo wymagające - znajomość świata, psychologii, ekonomii, fizyki..."

Tak wygląda, ale nie jest tak źle. Ratuje Was faza negocjacji i system intencyjny.

Przykład:

* Prowadziłem koleżance. Gadała z robotnikami i pytała, czy chcieliby, by im pomóc za darmo.
* Robotnicy nie, wszystko w porządku
* koleżanka na to, że coś jest z nimi nie tak. Normalnie robotnicy NIGDY nie odmówią w takiej sytuacji jaką zaprezentowałem.
* Po analizie sytuacji - miała rację. Umożliwiłem test i obniżyłem stopień trudności testu.

Czyli jeżeli jedna osoba przy stole coś wie, jest w stanie zmodyfikować fikcję uczciwie, bez "aha a więc to jest XXX!"

Jeżeli nikt przy stole o czymś nie wie, po prostu ten fragment rzeczywistości został uproszczony. Nikt z nas o tym nie wie, więc nikogo to nie boli.

Kontrprzykład:

* 25 lat temu grałem w Warhammera. MG opisał pentagram. 
* Jeden z graczy spytał jak dokładnie wygląda. MG wyjaśnił. 
* Gracz "AHA! Więc on odpycha demony zamiast przyzywać." 
* MG to ze smutkiem zaakceptował. 

W EZ zrobilibyśmy inaczej - gdybym ja był tym MG, spytałbym czemu Gracz o to pyta i poprosił go o wyjaśnienie, od czego to zależy i udzieliłbym takiej odpowiedzi, żeby było dobrze.

Tak więc EZ nie jest tak wymagającym systemem jak się wydaje. Nikt nie musi wiedzieć wszystkiego i nie da się tu łapać drugiej osoby za słówka.

Więc jeśli jedna osoba myśli że coś jest skonfliktowane a druga, że nie – porozmawiajcie, określcie, na czym polega różnica i zróbcie tak by "było dobrze".

---
## 8. SUBSTRAT ROZMOWY

Fox:

* żółw, scena bez kontekstu:
* Gracze mają płaczącego chłopca. Gracze naturalnie chcą go uspokoić i dowiedzieć się czemu płacze.
* Gracze naturalnie chcą podejść łagodnie [sposób] i zależy im na  tym, by chłopiec się uspokoił [cel]
* Jako MG wiesz, że chłopiec nie uspokoi się przy łagodnym podejściu - to jest w jego charakterze.
* Teraz gracze na mocy mechaniki mówią konflikt.
* Co w tej sytuacji?

Żółw:

* jeśli to nie jest niezmiennik i ich technika MOŻE zadziałać, idę po Ex lub Hr
* ale np. ma w środku pasożyta wie o tym i się NIE uspokoi - to nie podlega konfliktowi to by go uspokoić TAK SAMO jak nie podlega to że nie da się zabić supermana z pistoletu
* ale uspokajasz by coś osiągnąć. Zdobyć wiedzę, osiągnąć coś...
* realny przykład:
    * 2 wojna światowa
    * matki się chowają przed żołnierzami
    * niemowlę płacze
    * CEL: uciszyć dziecko
    * nie da się uspokoić. NIE DA SIĘ.
    * --> więc, zabijasz (dusisz). Osiągasz sukces -> nie zostajesz wykryta i dziecko jest cicho.
* Easy.

Dodatkowe informacje:

* definicja niezmiennika: coś niezmienialnego
    * konfliktem nie zmienisz niezmienników
* im wyższy poziom trudności tym mniej magii i mniej niezmienników i wyższa presja i wyższe stawki

FAQ (pojawiły się raz, ale wystarczy na Frequently):

* Q: Nie jest to strasznie silnie uznaniowe i nie daje mg strasznie dużo władzy?
    * A: czy da się przejść przez ścianę?
    * A: w konwencji i kontekście świata. Stąd mój silny nacisk na logikę świata i fizykę.
    * A: masz magię, która może ugiąć możliwości
        * nie da się przejść przez ścianę ALE jest interis
        * nie da się teleportować w kosmosie ALE jest alteris...
    * to dotyczy wszystkich systemów RPG out there
* Q: EZ wymaga od MG bardzo dużej znajomości świata, życia realnego, tematu wokół którego obraca się sesja...
    * A: nie, bo jest faza negocjacji
    * A: przykład: 
        * Sola gada z robotnikami i pyta czy potrzebują pomocy
        * odpowiadam "nie, nie chcą"
        * Sola "coś z nimi nie tak - nie ma możliwości by robotnicy NIE CHCIELI pomocy, to im ułatwi itp"
            * miała rację - modyfikacja poziomu testu, przekonywania, konsekwencji
        * mechanizmy negocjacji i ustalania kontekstu ratują MG i graczy przed zbudowaniem niespójności.
            * A jeśli nikt przy stole nie zna niespójności, to nie ma to przecież znaczenia
    * Cała EZ ma wbudowane mechanizmy by MG nie musiał być jedyną siłą uznaniową. Zauważ - jak tak na to spojrzeć to WSZYSTKIE RPG są ultra-uznaniowe, ale TYLKO EZ daje wbudowane w mechanikę mechanizmy pomocy MG przez graczy przy stole nie w formie "gracze vs MG"
    * i to nie tak że masz się jako MG źle z tym czuć. Przeciwnie. Nagle gracze nie są przeciwnikiem a zasobem
    * nie masz tak "omg ten pentagram jest na odwrót więc odpycha demony a nie przyzywa" - backportujesz rzeczywistość pod intencję i wszyscy przy stole się czegoś uczą
        * MG nie musi wiedzieć wszystkiego
        * a np. w Warhammerze, D&D, WoD - musi. A gracz pokazujący niespójność jest wrogiem. W EZ - przez wbudowaną płynność - gracz jest sojusznikiem ratującym MG od błędu.