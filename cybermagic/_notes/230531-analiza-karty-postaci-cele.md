---
layout: mechanika
title: "Notatka mechaniki, 230531 - Analiza kart postaci - cele"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox

## 1. Purpose

Wiemy już przez co przeszliśmy w przeszłości. Wiemy już jakie typy kart i narzędzi mamy do dyspozycji. Teraz pozostaje jedynie przejść przez to na co trzeba zwrócić uwagę, jakie failmodes ominąć i docelowo - zaprojektować sprawne karty postaci.

## 2. High-level overview

.

## 3. Substraty
### 3.1. Poprzedni dokument

W poprzednim dokumencie, [Analiza kart postaci pod kątem historycznym](230530-analiza-karty-postaci.md) wyprowadziliśmy serię podstawowych wymiarów które każda karta powinna mieć i spełniać.

Przykład tego, co jest w tamtym dokumencie:

![Arianna Verlen, 2004](Materials/230531/230531-01-2004-arianna-verlen.png)

### 3.2. Wymiary istotne dla karty postaci

Analizowaliśmy wymiary i wyprowadziliśmy serię wymiarów przydatnych (uzupełniłem po rozmowie z Fox i Darkenem, potem po poszerzeniu z ChatGPT):

1. **DOMINANTA: JAK UŻYĆ POSTACI NA SESJI**
    1. **Agenda**: Co interesuje postać i jak ją włączyć w sesję?
        * --> MG: co zrobić by postać zadziałała 
        * --> Gracz: co ma zrobić w sytuacji w której nie wie co robić
        * NEGACJA: nie ma połączenia postaci z sesją. Gracz nie ma motywacji do przesuwania postaci. Cele Gracza / postaci nie idą do przodu.
    2. **Obietnica**: Co postać obiecuje Graczowi - kiedy będzie świetna, czego Gracz może oczekiwać?
        * --> MG: jakiego typu wyzwania sprawią że ten i tylko ten Gracz będzie przydatny
        * --> Gracz: co będzie postać robiła epicko by się świetnie bawił
        * NEGACJA: Nie wiemy czym gramy i kiedy jest "nasz ruch by błyszczeć"
    3. **Zasoby**: Do czego postać ma dostęp, kogo zna itp.
        * --> MG: dodatkowe klocki świata którymi może wpleść postać w rzeczywistość
        * --> Gracz: co może powołać jeśli jest to potrzebne? Czego może użyć? Co wypracował?
        * NEGACJA: mniejsze umocowanie, Gracz nie czuje się "częścią" czegoś, mniej możliwości
        * **Interakcje z innymi postaciami**
            * specjalny podzbiór zasobów, linkujący do świata, historii i generujący dramę
            * potencjalnie też z lokacjami?
            * "Sorcerer" Edwardsa praktycznie "chodzi" na tym mechanizmie, też Apocalypse World.
    4. **Umocowanie w świecie**
        * --> MG, Gracz: dany zbiór klocków MG / świata który interesuje obie strony
        * NEGACJA: postać nie jest podmontowana do świata, nie zależy jej, nie ma wpływania, nie wie co i po co
    5. **Role w grupie**: w pewien sposób podzbiór Obietnicy; jej wkład w różne sytuacje
        * --> MG: w jakich okolicznościach ta postać będzie świetna
        * --> Gracz: kiedy ta postać ma wziąć na siebie ciężar
        * NEGACJA: może być taka sytuacja, że Gracz NIGDY nie ma ruchu "dla siebie" lub tego nie postrzega.
    6. **Kwestie mechaniczne**
        * --> MG: specjalne ruchy / manewry / możliwości o których warto wiedzieć?
        * --> Gracz: co może zrobić, co pokonać i do czego nie podskakiwać
        * NEGACJA: Gracz musi znać mechanikę płynnie, MG nie wie czego się spodziewać, Gracz może nie wiedzieć co zrobić
2. **DOMINANTA: JAK SPRAWIĆ BY POSTAĆ BYŁA FAJNA, SPÓJNA (JAK KSIĄŻKA) I JAK ZROZUMIEĆ SWOJĄ / CUDZĄ POSTAĆ TAK SAMO**
    1. **Słabości**:
        * --> MG: jak uatrakcyjnić sesję, gdzie nacisnąć by dać ruch innej postaci / _compelować_ tą?
        * --> Gracz: jak pokazać słabsze strony własnej postaci, budować konflikt wewnętrzny
        * NEGACJA: Nie mamy wbudowanych konfliktów w postać. Nie ma możliwości uderzenia w jej słaby punkt. Redukcja dramy i taktyki.
    2. **Historia**
        * --> MG: co postać zrobiła w przeszłości (czyli najpewniej zrobi w przyszłości)? Umocowanie postaci w 
        świecie, czyli klocki
        * --> Gracz: co postać zrobiła w przeszłości i jest w tym dobra? Z jakimi klockami wchodziła w interakcję?
        * NEGACJA: postać nie jest "prawdziwa"
    3. **Sterowanie**: Jak kierować postacią, jak się ma zachowywać, jak zachować jej spójność?
        * --> MG, Gracz: dla sytuacji X co zrobi postać? Jak się zachowa? Jakie ma quirki? Jak mówi? Na co reaguje?
        * NEGACJA: Nie mamy spójności, Batman raz jest sobą a raz kandyduje na burmistrza. Mniej ciekawych opowieści.
    4. **Core Wound -> Core Lie**: 
        * --> MG, Gracz: ?
        * NEGACJA: postać niekoniecznie ma prawdziwą "motywację" i nie ma błędnych strategii. Nie ma możliwości wygenerować prawdziwie głębokich konfliktów (zwłaszcza na linii WOUND - LIE - CEL SESJI).
    5. **Opis**: Wizualnie / quirki charakteru, ale potencjalnie też rzeczy których nie widać, silne strony itp?
        * --> MG, Gracz: ???
        * NEGACJA: ?
    6. **Lokalizacje przestrzenne**: pokazane mi przez Darkena; dzięki temu wiemy gdzie spodziewać się postaci i w jakich sytuacjach / kontekstach może działać
        * --> MG: gdzie użyć postaci, gdzie się jej spodziewać, najpewniej co będzie robić
3. **DOMINANTA: JAK UŻYWAĆ KARTY POSTACI, METAPARAMETRY**
    1. **UI Gracza**
        * --> MG: jak dostosować sesję pod tą konkretną postać, co Gracz widzi? Czy Gracz poradzi sobie z TYM 
        wyzwaniem?
        * --> Gracz: jakiego typu akcje może zrobić Gracz, czego oczekiwać od MG?
        * NEGACJA: rozpad komunikacji Gracz - MG, nie wiadomo co się dzieje i czemu Gracz nic nie robi / robi głupie ruchy, Gracz nie wie CO robić.
            * to jest ciekawe, że Apocalypse World ma wbudowane dobre ruchy jako checklista (niekoniecznie jako perfekcyjne narzędzie do gry)
    2. **Łatwość i szybkość użycia dla wszystkich usecasów**
        * --> MG: szybka budowa postaci na sesję
        * --> Gracz: budowa swojej postaci tak jak chce
        * NEGACJA: nie będziemy tego używać, nieważne jak dobre by nie było. Nie przez przypadek używamy fiszek generatora - to jest proste.

Metaparametry nie mają znaczenia "jako cecha karty", ale jest tam coś ważnego - usecasy. Więc przeanalizujmy usecasy karty postaci.

### 3.3. Usecases, "user stories"

Po rozmowie z Fox wyprowadziliśmy poważny failmode - karty postaci są strasznie trudne do stworzenia i wypełnienia przez graczy. To natomiast mnie skierowało do "oki, ale co w ogóle robi się z kartami postaci".

![ChatGPT 4 o usecases karty postaci](Materials/230531/230531-02-chatgpt-usecases-charsheet.png)

Jak zwykle, nie można tego użyć 1:1, ale są tu pewne istotne informacje które trzeba zauważyć z perspektywy karty postaci i celów karty postaci. By się upewnić, że dostanę dość wartościowych informacji, kazałem zrobić to w formacie bliższego do User Story.

![ChatGPT 4 o "user stories" karty postaci](Materials/230531/230531-03-chatgpt-userstories-charsheet.png)

Przydatne. Pomogło mi uporządkować główne akcje które wykonujemy na kartach postaci.

1. **Pomiędzy sesjami**
    1. **Tworzenie postaci**
        1. Oś: postać tymczasowa (tempka, konwentówka) - postać na stałe, kampanijna
        2. Oś: nie mam pomysłu na postać - mam pomysł na postać
        3. Oś: nie znam świata i systemu - znam świat i system
        4. Oś: nie znam mechaniki - znam mechanikę
        5. Oś: nie wiem co robimy tym zespołem na sesjach - wiem to
        6. Oś: chcę się delektować - nie mam czasu
    2. **Rozwój / aktualizacja postaci**
        1. Tu mam prosto; do tego jest achronologia + aplikacja, ale warto zapisać pro forma
    3. **Dopasowanie sesji / sceny / konfliktu do postaci**
        1. To robi MG; musi postać rozumieć zarówno na poziomie
            1. Umiejętności i zasobów (możliwości)
            2. Agendy i zachowania ("serca")
            3. --> zwykle to robię z przeszłych Dokonań jak nie mam karty postaci. Kogo oszukuję, moi gracze nie mają kart a jak mają to ich nie czytają XD.
    4. **Komunikowanie Gracz -> MG co chcę od dalszych sesji i scen**
        1. To znaczy że Gracz musi rozumieć postać i MG musi zrozumieć co Gracz mówi
    5. **Planowanie sesji pod kątem spotlightu**
        1. MG musi się upewnić, że każdy ma "coś dla siebie", zwłaszcza ten cichy gracz.
    6. **Zapamiętanie historii postaci**
        1. RdButler (aplikacja)
    7. **Zapamiętanie relacji postaci / lokalizacji postaci**
        1. RdButler (aplikacja)
    8. **Dodać luźne notatki "na kiedyś"**
        1. Karta postaci to plik notatnika. Zawsze się da. A aplikacja to "łyknie".
    9. **Zrozumienie świata / rzeczywistości**
        1. Jestem paladynem Arazille. Co to znaczy?
2. **Podczas sesji**
    1. **Co moja postać może zrobić w TEJ sytuacji?**
        1. Skillset, zasoby itp.
    2. **Planowanie taktyki**
        1. Użycie zbioru kart postaci by prawidłowo rozlokować co kto może i jak winien działać
    3. **Umocowanie postaci w świecie**
        1. Co to znaczy "paladyn Crissara i Arazille?" Co za tym się niesie?   
    4. **Wyciągnięcie supertajnej broni zdobytej 999 sesji temu**
        1. RdButler (aplikacja).
    5. **Liczenie expa / surowców / amunicji**
        1. Niektórzy to lubią. Ja to robię zwykle aspektami tymczasowymi i krzyżykami. I aplikacją (progresja).
3. **Animacja / sterowanie unikalną postacią**
    1. **Granie zachowując spójność postaci, nawet w stresie i gdy mi się to "nie opłaca"**
        1. "Nieważne kto gra Eleną Verlen, zachowuje się jak Elena Verlen"
            1. Zwykle robiłem to dodając Agendę i Sterowanie do mechaniki, więc premiowałem zachowanie, nie tylko skille. Jak Klucze z The Shadows of Yesterday.
    2. **Jak moja postać powinna się zachować w TEJ sytuacji?**
        1. Agenda, charakter itp.
    3. **Konkretny styl zachowania postaci, "narracja i roleplay"**
        1. "Sterowanie"
4. **Mechanika na sesji**
    1. **W czym moja postać jest NAJLEPSZA?**
        1. Jakaś forma wyróżnika - w tej akcji TA POSTAĆ jest najlepsza. Najlepiej imo to robi Apocalypse World przez absolutną dywersyfikację ról (pochodne AW już niekoniecznie).
        2. Subset: Jakie unikalne rzeczy może robić moja postać i żadna inna.
        3. Subset: kiedy jeśli moja postać się zaangażuje w akcję to osiągnie dewastujący sukces?
    2. **Jak działa mechanika gry? (rozumienie: UI) - przewidywanie**
        1. "co najpewniej się stanie jak kopnę minotaura w pośladek?"
        2. "czy dam radę przekonać barmankę by mnie schowała zanim mnie zauważą strażnicy?"
    3. **Interpretacja wyniku testu**
        1. "wyciągnąłem ptaszek, co teraz, jeśli jestem arcymagiem i to był konflikt magiczny? A co jak byłem tylko magiem?"

To chyba wszystko. Jak nie, mam nadzieję, że ktoś kto to przeczyta mi powie o czym nie pomyślałem :-)

## 3.4. Bariery i blokady

Popatrzmy jeszcze raz na jakąś referencyjną sprawną postać, choćby na nieszczęsną Ariannę.

![Arianna Verlen, 2004](Materials/230531/230531-01-2004-arianna-verlen.png)

Czy ja zrobię taką kartę postaci w 30 minut? Na 100%. Czy Kić, Fox czy Dzióbek? Na pewno. Czy całkowicie nowa osoba wchodząca do systemu? Nie.

Pojawią się bariery i blokady. Przy karcie która ma parametry:

`Aleksander Barbarzyńca: Siła 10, Zręczność 6, Inteligencja 8, Charyzma 5` 

nikt nie wypełni tego "źle" i nie będzie czuł się głupio. W wypadku karty Arianny bardzo łatwo wypełnić coś co się nie będzie podobało twórcy postaci. Pojawia się problem z samooceną lub oceną innych.

Jakie więc widzimy potencjalne bariery i blokady które designer karty postaci musi zniwelować by łatwo było ją uzupełnić dla każdej osoby, nawet pierwszorazowego gracza?

![Bariery i blokady](Materials/230531/230531-04-chatgpt-failmodes-charsheet.png)

O dziwo, teraz dostaliśmy dobre rzeczy na których możemy pracować. A jak chodzi o typowe failmody / nieudane karty postaci?

![Bariery i blokady](Materials/230531/230531-05-chatgpt-failmodes-charsheet.png)

Z czego sumarycznie możemy wyprowadzić blokady i failmodes karty postaci:

1. **Blokady** - to jest powód czemu ludzie nie chcą używać kart postaci
    1. **Nie znam reguł**
        1. Nie wiem w ogóle jak wypełnić kartę
        2. Nie wiem jak wypełnić kartę by było dobrze i ta postać była przydatna
    2. **Strach przed oceną i błędami**
        1. To gra społeczna. Nikt nie chce być małżem.
        2. Tu też jest porównywanie się z innymi. "Oni robią super a ja tak :-("
    3. **Za dużo możliwości - nie wiem co wybrać**
        1. Lub co gorsza, problem "nie mam z czego wybrać mam wymyśleć AAA NIE WIEM CO ROBIĆ"
            1. Rozwiązywałem fiszkami. Działało.
    4. **Nie znam świata lub kontekstu**
        1. Jak ja napiszę "ekspert od fabrykatorów ze specjalizacją w psychotronice", wiem DOKŁADNIE co ta postać potrafi. Podejrzewam, że nawet Wy nie wiecie, czemu taka postać nie mając źródła astinianu niedaleko jest mało użyteczna. A co mówić o nowym graczu?
    5. **Niegodna inwestycja czasu**
        1. Jeśli wypełnianie postaci to 60 minut, NOPE!!! Nie mam czasu na bullshit.
2. **Failmodes** - to się może zdarzyć jeśli system nie pomoże twórcy postaci
    1. **Problem niszy**
        1. **Nieprzydatna postać - nieefektywna alokacja wg. mechaniki**
            1. Gracz stworzył wojownika nie mającego siły i nic nie trafiającego. Ta postać jest bezużyteczna podczas walki. Gracz się bawi źle.
        2. **Duplikacja niszy bez cienia unikalności**
            1. Bard może robić wszystko, więc nikt inny nie jest potrzebny.
            2. Bard nie może robić nic, bo zespołowi: wojownik, kleryk, mag i złodziej zrobią wszystko lepiej.
    2. **Postać niekompatybilna z sesją**
        1. gram Inkwizytorem Światła Bez Cienia Zawahania by infiltrować mroczne miasto w klimacie Noir. Lub gram Casanovą w zakonie paladyńskim. To czym chcę grać nie ma jak zachować spójności w kontekście sesji.
    3. **Opis / background postaci niekompatybilny z power level kampanii, tzn "to nie ma sensu"**
        1. "Byłem admirałem floty kosmicznej. Zabiłem smoka, solo. Boi się mnie król wszelkich liczy. Co mówisz? Przynieś 10 szczurów bym dostał 10 miedzi? Jasne, czasy są ciężkie."
    4. **Brak Agendy i Motywacji**
        1. "jestem w karczmie. Jestem mroczny. Dobrze, pójdę z Wami... ale nie mam po co i niczego nie chcę, na niczym mi nie zależy"
            1. Dlaczego większość co się fascynuje Drizztem Do'Urdenem nie rozumieją co zrobił Salvatore? Drizzt **MIAŁ** motywację. A oni nie. 
            2. Jeśli nie wiem czego chce postać i gdzie chce prowadzić nią Gracz jako MG... jak mam zrobić udaną i angażującą sesję? Ok, mogę zrobić Monster in the House, ale ile można?
    5. **Hiperspecjalizacja / hipergeneralizacja**
        1. Ula Blakenbauer może być ekspertem od Płaszczek i tylko nich. Ona jest NPCem. Ale postać gracza co np. jest tylko pilotem statku i nic nie może innego zrobić (bo jest sparaliżowany) - co mogę zrobić za sesje takiej postaci? Jak się przyda?
        2. Poprzednio pokazany problem barda, co nic nie może bo inni robią to lepiej.

## 3.5. Wymagania niefunkcjonalne wobec karty postaci

Czyli nie "co da się zrobić" a "okoliczności tego robienia". Rozpatrywane dla każdej akcji z osobna, czyli same wymagania niefunkcjonalne są ważne i rozpatrywane dla "Tworzenia Postaci", "W czym moja postać jest dobra" itp.

1. **Prędkość użycia akcji**
    1. Oś: jestem początkujący - używam tego już dość długo
2. **Łatwość użycia akcji**
    1. Oś: nic nie wiem - znam świat i kontekst i teorię
        1. Przykład 1: 
            * Maja Samszar
                * ENCAO:  ++-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Rite of passage
                * styl: UR z elementami WB; lekko emocjonalnie niestabilna arystokratka która UWIELBIA wszystkich pouczać`
            * Ja perfekcyjnie wiem jak sterować Mają. Dla mnie jest **bardzo łatwe**, ale wymaga wiedzy.
        2. Przykład 2:

![Inkwizytor, z fiszki](Materials/230531/230531-06-inkwizytor.png)

                * Wszyscy z wielką łatwością wykorzystywali ten typ fiszki, nieważne jak dobrze znali system, mechanikę itp.

3. **Mam dużą kontrolę i precyzję nad akcją**
    1. "Jestem w stanie zasymulować dowolny konflikt w egzystencji" (mechanika konfliktów)
    2. "Jestem w stanie zrobić bardzo odmienne postacie i ich zapis sprawia, że inny gracz poprowadzi postać dokładnie tak samo jak ja"
4. **Fajnie się tej akcji używa**
    1. Po prostu. Bardzo uznaniowe. Grunt, by targetowi (grupie docelowej) pasowało.
    2. Czy chcę to kontynuować? Warto? Używanie tego sprawia mi przyjemność?
    3. Z perspektywy żetonów - **tactile** tu się znajduje.
5. **Przejrzystość instrukcji / akcji**
    1. Jak szybko jestem w stanie zrozumieć jak to działa bez demonstracji?
    2. W wypadkach gdzie dwie osoby się nie zgadzają - jak szybko dojdziemy do jednoznacznej odpowiedzi?
6. **Znaczenie inwestycji pracy i energii w akcję; "wpływ gracza na akcję"**
    1. Jak bardzo jest istotne co tam się zrobi / wypełni?
    2. Czy jeśli powiem "wojownik" a ktoś rozpisze coś ciekawego - czy będzie nagrodzony za ekstra pracę?
    3. antyprzykład: "to ja go tnę" == wartościowy i klimatyczny opis
7. **Jedność interpretacji akcji / instrukcji**
    1. Czy dwie osoby zrozumieją to samo tak samo?
8. **Prostota akcji**
    1. Czy nie trzeba zapamiętać za dużo?
    2. Czy niewyspana i lekko zdekoncentrowana osoba może łatwo to zrobić?
9. **Zwięzłość**
    1. Czy da się to zmieścić na 1 stronie? 1 karcie? 1 linijce?
10. **Show**
    1. Czy powoduje efekt "wow"

## 4. Co dalej

1. Najbliższy krok
    1. Uzupełnić wymagania niefunkcjonalne per akcja
    2. Uzupełnić Blokady i Failmodes
    3. Uzupełnić i uporządkować Usecases
    4. Uzupełnić i uporządkować Wymiary karty postaci
2. Następny krok
    1. Priorytetyzacja NFun per akcja
    2. Gdy mamy priorytety, projektować i testować pod kątem priorytetów
    3. Zrobić szkieletowe karty i sprawdzić jak działają
    4. ...
    5. Profit.

