# Analiza

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

* Jak przydzielać krzyżyki by nie zniechęcić

---
## 2. Spis treści i omówienie

1. Krótka myśl

---
## 3. Krótka myśl

Jest to w pewien sposób powiązane z [240304 - O początku sesji](240304-o-poczatku-sesji.md).

Jeśli gracz wyjmie pierwszy (X) i zakończy się to tragedią dla postaci gracza, gracz jest silnie zmotywowany by nie używać mechaniki. Wykorzystanie mechaniki jest kluczem do prawidłowego działania w kontekście EZ. Tylko tak możesz zapewnić ufortyfikowanie swojej pozycji przed MG i innymi graczami.

Co robię w tej sytuacji? 

Po pierwsze, wykorzystuję **łańcuchy** - „ uszkadzam pancerz” a dopiero potem go przebijam. Demonstruję co złego się stanie.

Ciekawe jest to co robię na sesjach z randomami - mówię im coś w stylu "rura eksploduje wszyscy umieracie" i pokazuje im, że to nie ma sensu. Tłumaczę im negocjowalność (X). Zarówno (V) jak i (X) są negocjowalne i jakkolwiek jedna osoba ma zdanie ostateczne, ale nie da się przez przypadek i bez pewnej formy ostrzeżenia zrobić czegoś katastrofalnego.

Jeśli jednak gracze już wiedzą, że są potencjalne rzeczy katastrofalne w ramach tego konfliktu, pierwszy (X) może być katastrofalny. Ale to trzeba jawnie powiedzieć przed testem.
