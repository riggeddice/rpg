## 1. Obraz postaci

### Faceless NPC:

**Name:** Iga

#### Wartości
##### Moje wartości (triada / diada)

* Self-direction: autonomy, ability to control your own way and direction
* Face: prestige; having a specific reputation and upholding it
* Stimulation: feel alive, feel more, new things, experiment, explore

##### Moje antywartości (diada)

* Hedonism: pleasure above anything else
* Conformity: being one of the group, homogenity of a group

#### Osobowość
##### Ocean

ENCAO:  -0+0-

##### Jak się objawia osobowość

* Nie znosi być w centrum uwagi
* Prostolinijny i otwarty; podstępy nie są tu siłą
* Pracowity, skupiający się na pracy jako na wartości

#### Potencjalna praca:

* Dekorator sklepów
* Bagażowy

#### Potencjalne silniki motywacyjne:
##### Moje silniki:

* Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
* Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy.

##### Silniki WROGA lub anty-silnik:

* Soccer Mom: zapewnić bezpieczeństwo, harmonię i pozory idealności w swojej okolicy. A przynajmniej wobec swojej domeny.

## 2. Analiza

CZYLI:

_Zacznijmy od czegoś. Czegokolwiek. Zwykle zaczynam od wartości._

* TAK: Chce kierować swoim życiem, ważny jest prestiż i życie pełnią życia. 
    * --> widzę taką niezależną istotkę która pasuje do gangu.
        * "NIE POWIESZ MI CO MAM ROBIĆ!"
        * "YA TALKIN' TO ME?!"
        * "Jak Cię nie szanują, to Cię podgryzają."
        * "Dziewczyna w tym świecie to musi sama sobie radzić, nikt jej nie pomoże"
    * Teraz na czym jej nie zależy?
        * przyjemność własna i dopasowanie do innych
            * "Nie robię tego bo lubię a bo <drive>"
            * "Nieważne czy inni mnie zaakceptują, ważne, że <drive>"
            * "Inni to debile"

_Dobra, mam obraz Igi. Widzę mniej więcej jak nią sterować, mam serię cytatów jakie mogłaby powiedzieć. Teraz dodajmy jej osobowość_

ENCAO: -0+0-

* nisko reaguje na nagrody
* wysoka kontrola impulsów, uważność i sumienność
* przyziemna, praktyczna, mało wyobraźni

_Dodajmy traity z osobowości_

* Nie znosi być w centrum uwagi
* Prostolinijny i otwarty; podstępy nie są tu siłą
* Pracowity, skupiający się na pracy jako na wartości

_Modyfikujemy cytaty_

* "Nieważne, co powiedzą mi co mam robić i tak zrobię to co chcę" (niskie E powoduje że nie krzyczy)
* "Jak Cię nie szanują, to Cię podgryzają. Więc wypracuj cierpliwie pozycję." (wysokie C daje jej sumienność)
* "Dziewczyna w tym świecie to musi sama sobie radzić, nikt jej nie pomoże"
* "Nie robię tego bo lubię a bo <drive>."
* "Nieważne czy inni mnie zaakceptują, ważne, że <drive>"
* "Inni to debile, mogą tylko wysysać Twoją energię" (niskie E oznacza że się męczy)
* "Krok po kroku, niewdzięcznie, ale dojdziesz do celu idąc według planu - w ten sposób osiągniesz wolność" (E-C+O-)
* "Podstępami i drogą na skróty zniszczysz sobie reputację. I będziesz płakać jak Kim Kardashian jak jej syn znalazł jej sex tape" (Prestiż + Conscientiousness)

_Mamy zatem cichą dziewczynę, która jest dość uparta, ma mało wyobraźni ale osiągnie cel i trudno ją zniechęcić bo nie prowadzą jej ani marzenia (O-) ani potencjalna nagroda (E-). Za to sumiennie, jak wół dojdzie do celu (C+) - bo chce samodzielności, reputacji i poczucia, że nie osiągnęła jeszcze szczytu swoich możliwości (tak zmutowałem stymulację, która jej po prostu już przestała pasować z takim OCEAN)_

_Czas wprowadzić silnik_

* TAK:
    * Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
    * Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy.
* NIE:
    * Soccer Mom: zapewnić bezpieczeństwo, harmonię i pozory idealności w swojej okolicy. A przynajmniej wobec swojej domeny.

_Tu jest miejsce gdzie normalnie przelosuję silnik, bo ten mi nie pasuje. Więc przelosowujemy:_

* Potencjalne silniki motywacyjne:
    * Moje silniki:
        * Rite of passage: by dziecko stało się dorosłym, musi dokonać Wielkiego Czynu. Zdobyć czyjeś ucho, zabić potwora...
            * _moja interpretacja: ewolucja. Zostać kimś w tym świecie. Przejść na wyższy poziom._
        * Ważny sojusznik: uzyskać szacunek i wsparcie kogoś ważnego, mocno usytuowanego.
    * Silniki WROGA lub anty-silnik:
        * Ukojenie cierpienia Bashira: inni cierpią. Jeśli jestem w stanie ich ukoić i im pomóc, to mój moralny obowiązek.
            * _moja interpretacja: niech inni cierpią, mnie nic do tego. Ani nie pomogę, ani nie zaszkodzę._
            * _moja alternatywna interpretacja: wszyscy pomagają TAMTYM a ja muszę iść sama do góry. Niech ONI mają gorzej._

_Dajmy jej jako rite of passage uwolnienie spod wpływów Aurum. Po prostu jako przykład. Normalnie miałbym to z sesji / kontekstu sesji._

_Modyfikujemy cytaty_

* "Nieważne, co powiedzą mi co mam robić i tak zrobię to co chcę" (niskie E powoduje że nie krzyczy)
* "Jak Cię nie szanują, to Cię podgryzają. Więc wypracuj cierpliwie pozycję." (wysokie C daje jej sumienność)
* "Dziewczyna w tym świecie to musi sama sobie radzić, nikt jej nie pomoże. A tym z Aurum to zawsze wszystko dadzą" (resentyment z antysilnika)
* "Nie robię tego bo lubię a bo dzięki temu zdobędę sojuszników i wreszcie osiągnę niezależność od tych z Aurum." (dodanie celu z silnika)
* "Nieważne czy inni mnie zaakceptują, ważne, że będę wolna od wpływów z Aurum" (dodanie celu z silnika)
* "Inni to debile, mogą tylko wysysać Twoją energię. Nie będę udawać że lubię Aurum." (niskie E oznacza że się męczy + kieruję na Aurum)
* "Krok po kroku, niewdzięcznie, ale dojdziesz do celu idąc według planu - w ten sposób osiągniesz wolność" (E-C+O-)
* "Podstępami i drogą na skróty zniszczysz sobie reputację. I będziesz płakać jak Kim Kardashian jak jej syn znalazł jej sex tape" (Prestiż + Conscientiousness)
* "Niech się bawią, niech się śmieją. Dojdę wyżej niż oni uczciwą pracą. Jestem jak Kopciuszek - i jak Kopciuszek wygram" (pod silnik)

_I spójrzmy czy zawody jakkolwiek pasują_

* Bagażowy ->
    * Dekorator sklepów

_Oki, czyli młoda jest na dole i obsesyjnie próbuje ciężką pracą dostać się wyżej. Najpewniej dekoruje sklepy gdzie przychodzą ludzie z Aurum co jeszcze bardziej ją drażni. Ale spuszcza głowę - lepsze to i bycie niezależną niż podporządkować się innym i dopasować. Jej stymulacja polega na tym, że nie musi się dopasowywać i ma swoje małe przyjemności, które są potencjalnie zakazane. Takie "drobnomieszczańskie sekrety" które dają jej radość w tym świetle_.

_Jak wykorzystać ją w opowieści? Gdzie się przyda?_

* Nie złamie prawa i nie zadziała dyskretnie, ale pomoże informując o czymś co zaszkodzi Aurum
* Może ktoś z Aurum ją napastuje i ona mogłaby być księżniczką, ale woli być niezależna na dnie
* Może można ją wkręcić, by podłożyła narkotyki komuś z Aurum. I ona tego NIE zrobi. I ją trzeba złamać. A jest "przecież oczywistym wyborem bo ich nie lubi"
