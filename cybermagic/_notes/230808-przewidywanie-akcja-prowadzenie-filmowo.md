---
layout: mechanika
title: "Oczekiwania, akcje / intencje, działania filmowe"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Darken

---
## 1. Purpose

* Zapis i strukturyzacja rozmowy z Darkenem
* "Pierwszy krzyżyk", "jak stawiać konflikt złożony", "jak trzymać SIS"

---
## 2. High-level overview

1. Przykład walki ze smokiem
    1. Scena walki ze smokiem
    2. Przeciwnik to zbiór Problemów demonstrowanych Oczekiwaniami
    3. Optymalizacja pod kątem filmu
    4. Akcje i Intencje
2. Planszą MG są oczekiwania Graczy
    1. Budowanie Oczekiwań Graczy
    2. Znane komponenty
    3. Foreshadowing (Setup - Payoff)
    4. Aktywacja Klocka zamiast Announce Badness
    

---
## 3. Przykład walki ze smokiem
### 3.1. Scena walki ze smokiem z przemyśleniami MG

Kontekst - scena:

* Stan aktualny
    * Postać Gracza: Sariel, włócznik. Ma dwóch sojuszników (NPC): Elara i Maldor.
    * Przeciwnik: gruby niewielki smok, ziejący ogniem i morderczy w walce. Smok się bawi jak kot.
    * Lokalizacja: wioska sojusznicza naszych trzech postaci
    * Problem: Smok zniszczy wioskę lub zrobi krzywdę dużej ilości ludzi
* Stan oczekiwany
    * Wioska musi być uratowana przed Smokiem
        * redukcja zniszczeń, nie ucierpi za dużo osób, wioska nadal pełni funkcję miejsca przydatnego dla Sariela
        * Smok nie zagraża wiosce, a przynajmniej przez pewien czas
    * Sariel, Elara i Maldor nie powinni zginąć

Przeprowadzenie:

* **Sariel**: Zachodzimy smoka od strony budynków, po czym atakujemy włóczniami z trzech różnych stron. Ściągam na siebie ogień.
* **MG**: Smok jest od Was dość oddalony; jest na placu i strzela ognistymi strumieniami w różne budynki. Jeśli na niego zaszarżujesz, musisz uważać na ataki.
* **Sariel**: Dobra, czyli plan jest taki: szarżuję na gada, unikam pocisków, znajduję słaby punkt i mam zamiar go brutalnie i okrutnie dźgnąć.

![Szarża na smoka](Materials/230808/01-dragon-charge.png)
(Midjourney, `spearman charging at red dragon in a small village, dragon breathes fire --v 5.2`, `pan right, pan left`)

Wyobraźcie sobie taką scenę: 

* Smok na placu
* Sariel na niego szarżuje
* Smok zbiera się i zieje w Sariela ogniem
* Sariel skacze w bok, unika zionięcia jak w anime, zachodzi smoka od boku a w tym czasie sojusznicy zachodzą gada bezpiecznie
* Sariel korzysta z okazji że smok przygotowuje się do nowego zionięcia i wbija weń włócznię
* Smok wyje z bólu
* Sojusznicy Sariela dopadają smoka i wszyscy go dźgają
* Smok rozpościera skrzydła i odlatuje, by nigdy nie wrócić

To dobra scena filmowa. By to osiągnąć, zobaczmy, co jest potrzebne:

* Sariel musi skrócić dystans i skupić uwagę smoka na sobie
* Sariel musi znaleźć słaby punkt smoka (podbicie)
* Sariel musi zranić smoka i zniechęcić go do walki (manifestacja)
* (opcjonalne) Sariel i sojusznicy nie są ranni

Czas na powrót do sytuacji:

* **MG**: Smok jest daleko.
* **Sariel**: Szarżuję! (epicki opis godny anime z wykorzystaniem przeszkód terenowych by zbliżyć się do smoka)
* **MG**: Ekstremalny +4 (`11X 7V`)
* **RNG**: (sukces) (nowa pula: `11X 6V`)
* **MG**: Interpretuję: Sariel skutecznie uniknął płomieni i dotarł do smoka. Elara i Maldor są z drugiej strony gada. Smok ma pancerne łuski, nie masz jak go sensownie zranić bez słabego punktu.
* **Sariel**: Elara atakuje smoka włócznią, by gada rozproszyć, by się odsłonił. Będę miał słaby punkt przed oczami!
* **RNG**: (porażka) (nowa pula: `10X 6V`)
* **MG**: Interpretuję: smok rozpostarł skrzydła i machnął ogonem. Maldor dał radę uskoczyć, ale Elara została trafiona i odleciała kilka metrów. Najpewniej ma coś połamane. Ty uniknąłeś ataku.
* **Sariel**: akceptuję. Korzystam z okazji, zapieram się i eskaluję.
* **RNG**: (porażka) (nowa pula: `9X 6V`)
* **MG**: **Podbijam**. Smok Cię ignoruje i odwraca się w stronę Maldora, atakując go łapą. Maldor traci broń i odskakuje. Smok skupia się na Elarze, niezdolnej do obrony.
* **Sariel**: Podbiłeś, czyli smok chce zabić Elarę?
* **MG**: Na to wygląda.
* **Sariel**: Dobra, NADAL eskaluję. Musi mi się w końcu udać.
* **RNG**: (sukces) (nowa pula: `9X 5V`)
* **MG**: masz słaby punkt wroga. Jesteś w stanie go ciężko zranić i odepchnąć. Potrzebny Ci tylko jeden sukces więcej, musisz uniknąć uderzenia smoka i wbić mu włócznię pod łopatkę.
* **Sariel**: Eskaluję.
* **RNG**: (porażka) (nowa pula: `8X 5V`)
* **MG**: **Manifestuję**. Smok zabija Elarę.
* **Sariel**: Alternatywna propozycja. Krzyczę do Maldora, by mi zaufał i zaszarżował bez broni na smoka. Wyjdę na bezwzględnego dupka przed Elarą i Maldorem, Maldor będzie ranny, ale Elara przeżyje.
* **MG**: Akceptuję. Maldor rzuca się by chronić Elarę w czasie, gdy Sariel pozornie nic nie robi.
* **RNG**: (sukces) (nowa pula: `8X 4V`)
* **MG**: Uniknąłeś ataku skrzydeł gada. Wbiłeś smokowi włócznię prosto w pachwinę. Smok jest ranny. Ryknął z bólu.
* **Sariel**: Wystarczająco, by stąd odleciał?
* **MG**: Tak. Ale wiesz, jeszcze jeden (V) i go bardzo ciężko zranisz, odleci i zginie.
* **Sariel**: (śmiech) nie z tą pulą. Nie chcę śmierci Elary i Maldora. I chcę, by się do mnie jeszcze odzywali...

Popatrzmy na chwilę na tą scenę.

By móc **odeprzeć** smoka Sariel musiał mieć **trzy** (V). A każdy (X) był katastrofalny - pierwszy (X) prawie zabił jego sojuszniczkę.

Ale "filmowo" ta scena działała i trzymała się kupy. Dlaczego? Bo wiemy, co potrafi smok i mamy pewne oczekiwania co do smoczych umiejętności. Gracz **oczekiwał** bardzo trudnej i ryzykownej walki. Nie oczekiwał, że da radę smoka zabić (mimo, że MG to zaproponował by kusić).

Wyobraźcie sobie, że zamiast smoka w tym miejscu jest: 

* Typowy Goblin.
    * -> Najpewniej Sariel oczekuje, że zabije go pierwszym (V).
* Typowy Gobliński Łucznik
    * -> Najpewniej Sariel oczekuje, że podbiegnie do niego i zabije pierwszym (V).
* Dwóch Goblińskich Wojowników chroniących Dwóch Goblińskich Łuczników
    * -> Najpewniej Sariel oczekuje, że: 
        * skróci dystans nie będąc trafionym (V)
        * zabije gobliny (V)
        * (opcjonalnie) nie zostanie ranny (V)
    * -> Alternatywnie: zajdzie gobliny z zaskoczenia
        * zaskoczy gobliny i atakuje z zaskoczenia (V)
        * zabije gobliny nie będąc rannym (V)

### 3.2. Przeciwnik to zbiór Problemów demonstrowanych Oczekiwaniami
#### 3.2.1. Przeciwnik to zbiór Problemów

Parę przykładów o co mi chodzi, że przeciwnik to zbiór Problemów:

* **Goblin Wojownik**:
    * problem: "pokonać w walce", trudność: Typowy
* **Goblin Łucznik na otwartej przestrzeni**:
    * problem: "zmniejszyć dystans i zabić bezbronnego", trudność: Trudny
* **Goblin Łucznik, mam przeszkody terenowe**:
    * problem: "zmniejszyć dystans i zabić bezbronnego", trudność: Typowy
* **Gobliński Oddział (2 wojowników, 2 łuczników), frontalnie**:
    * problem: "zmniejszyć dystans i wejść w walkę w zwarciu", trudność: Trudny
    * następny problem: "pokonać dwa gobliny i nie dać się trafić łucznikom", trudność: Ekstremalny
    * okoliczność: "nie zostać rannym podczas niekorzystnej walki", trudność: Trudny
* **Gobliński Oddział (2 wojowników, 2 łuczników), podpełznięcie z zaskoczenia**:
    * problem: "zaskoczyć gobliny i zaatakować je od tyłu usuwając łuczników": Trudny
    * następny problem: "wygrać 1v2 z goblińskimi wojownikami, bez presji i bez ran": Trudny
* **Pancerny Smok, mam dwóch sojuszników, Smok jest na otwartym terenie**
    * problem: zbliżyć się do smoka i nie dać się usmażyć, trudność: Ekstremalny
    * problem: w zwarciu ze smokiem zlokalizować słaby punkt, trudność: Ekstremalny
    * problem: zranić smoka po zlokalizowaniu słabego punktu, trudność: Ekstremalny
    * okoliczność: nikt nie jest ranny w tak przeważającej walce, trudność: Trudny
* **Sariel jest zaatakowany przez kota domowego, który chce się bawić.**
    * problem: ???
    * to nie jest konflikt, kot nie chce zrobić krzywdy, MG nic nie ma do wygrania

A teraz popatrzcie na to:

* **Sariel atakuje Vespiravita z zaskoczenia; chce mu uciąć głowę**
    * deklaracja Sariela: skradam się do Vespiravita i jak tylko jestem w zasięgu i ów się odwróci, atakuję zza jego pleców i tnę prosto w szyję.
    * problem: ???

Jaki jest stopień trudności tego konfliktu? Jakie są kroki by ten konflikt zamapować? Czy to jeden test czy test złożony?

Nie wiecie co to Vespiravit. 

* Jeśli to coś w stylu nieopancerzonego humanoida, główny konflikt pójdzie w stronę "czy się przekradłem i zaskoczyłem" i "czy będę dość szybki by mu uciąć łeb", czyli 1-2 testy.
* Jeśli to coś w stylu opancerzonego żółwioludzia chowającego łeb w skorupie, główny konflikt jest dużo trudniejszy. "Czy się przekradłem i zaskoczyłem", "czy byłem szybki ZANIM schował łeb", "czy znalazłem słaby punkt" itp.

A jeśli **wiecie** co to vespiravit - niezbyt materialny upiór wyprodukowany przez Samszarów, to widzicie, że ten konflikt jest zupełnie innego typu i innej klasy.

To nas elegancko prowadzi do następnej sekcji.

#### 3.2.2. Przeciwnik jest demonstrowany / prezentowany Oczekiwaniami

Smok:

* **MG**: Smok jest groźny, musisz uniknąć zionięcia i znaleźć słaby punkt a każda porażka to problem
* **Sariel**: Okay :-(

Goblin:

* **MG**: Goblin to morderca, musisz mieć trzy ptaszki by go pokonać
* **Sariel**: Że niby jak?!

Skąd się bierze różnica?

**Oczekujemy** że smok będzie przepakowany, groźny i w ogóle podły. Oczekujemy, że goblin to mało groźne stworzenie zaprojektowane jako pomniejszy spowalniacz dla postaci Graczy. (incydentalnie, dlatego tak trudno jest niektórym MG prowadzi Sci-Fi. Gracze i MG mają mniej **Oczekiwań** odnośnie stanu systemu, sesji, sytuacji).

Wyobraźcie sobie taką scenę:

![Stacja kosmiczna z CZYMŚ](Materials/230808/02-space-station.png)
(midjourney, `moldy space station interior with something with tentacles hiding behind the door, horror, creepy, terrorwave, gothic horror --v 5.2` plus ekspansje)

Gracze przemierzają korytarze stacji przerośniętej czymś dziwnym. Docierają do grzybopodobnej istoty otoczonej przez na wpół przekształconych załogantów stacji.

Jeden z graczy panikuje. Celuje w grzybopodobną strukturę i strzela w nią działem laserowym, mimo, że inni nie chcą zwracać na siebie uwagi.

Grzybopodobna struktura umiera. Ludzie są uratowani.

WTF.

**Skąd efekt WTF**? Bo:

* Opis wskazywał na coś groźnego, trudnego i podłego
* Otoczenie jest poważnie splugawione i gracze ogólnie mają już powoli ciarki
* Oczekiwalibyśmy, że jeśli to takie proste to ktoś na tej stacji by naprawił sytuację przed nami
* Zwykle główny przeciwnik jest dość silny; oczekiwaliśmy ostrej walki

Jedną z najważniejszych ról Mistrza Gry jest budowanie odpowiednich oczekiwań Graczy wobec komponentów sesji jakich MG używa.

### 3.3. Optymalizacja pod kątem filmu

Mówiąc brutalnie - "jeśli to ma sens w filmie i pasuje fabularnie, ma sens na sesji". Mówię tu o dobrych filmach i książkach. Zwykle patrzę na anime z przyczyn dramatycznych.

**Jeśli nie wiesz jaki następny krok zrobić - gdyby to był film / książka, co powinno się stać?** Najprawdopodobniej jest to ruch prawidłowy.

Jeśli MG i Gracze "widzą" ten sam film / konwencję, sesja uda się dobrze. To, że smok może jednym (V) poważnie zranić Elarę jest oczywiste. To, że goblin pierwszym (V) poważnie rani Elarę byłoby bardzo dziwne. Od razu pokazuje, że goblin jest czymś innym niż się wydaje.

Nie możesz zrobić czegoś dziwnego i morderczego nie pokazując Graczom, że to jest możliwe. Nie łam konwencji. Chyba, że świadomie chcesz złamać konwencję i dokładnie wiesz co robisz.

* **MG**: Widzisz wysokiego szermierza w zbroi, przebijającego się przez obrońców wioski jak przez masło. Wygląda na orka weterana.
* **Gracz**: Zmierzam w jego stronę ostrożnie. Chcę go pokonać jako championa wroga.

Oczekiwania Gracza? Będzie to groźny, dobry przeciwnik. Gracz zaakceptuje kilka (V) by go móc pokonać. Bo to **pasuje do filmu w którym się toczy gra**, pasuje do konwencji. Mimo, że przed chwilą nic nie było wiadome o tym przeciwniku.

### 3.4. Akcje i Intencje

EZ wspiera bez problemu grę hybrydową. Sam prowadzę hybrydowo, nie tylko czysto intencyjnie.

* Akcje działają. Intencję inferujesz z akcji.
* Jeśli MG / Gracz nie rozumie konsekwencji Akcji, pyta "czy Twoim celem jest X? / co jest celem?"

Im bardziej (horror) / (sesja akcji), tym więcej akcji i mniej intencji (intencja domyślna).

Silnie akcyjny jest przez to cykl z Amandą Kajrat ("Furia Mataris agentką mafii").

---
## 4. Planszą MG są oczekiwania Graczy
### 4.1. Budowanie Oczekiwań Graczy

Niezbędne do budowania "melodii sesji". Techniki filmowe zdecydowanie tu pomagają. Poniżej zeksplorujemy takie, których jeszcze nie poruszyłem wcześniej w tej notatce.

Enumeracja:

1. Opis wizualny ("creepy stacja kosmiczna", "serio duży smok palący wioskę")
2. Demonstracja kompetencji ("szermierz przecinający obrońców jak masło")
3. Używanie znanych komponentów (goblin, smok)
4. Foreshadowing: setup/payoff
5. Aktywacja zasobu w jawny sposób

### 4.2. Znane komponenty

Mamy klocki:

* "Goblin" oznacza coś dość niegroźnego.
* "Smok" oznacza coś absolutnie fatalnego do pokonania
* Po wprowadzeniu nowego Komponentu, Gracze mają wobec niego Oczekiwania.

Wiemy, że ostatni przeciwnik w Opowieści jest zwykle "Bossem". Wiemy, że (maszyny i cyborgi) są raczej wrażliwe na wodę / prąd / hackowanie a (golemy) nie.

Jako MG używaj tych znanych klocków. Pomogą.

### 4.3. Foreshadowing (Setup - Payoff)

Mam cały warsztat na ten temat w formie "Setup - Payoff"

Wklejam subsekcję:

* logiczny graf prowadzący do finału : theme & vision -> samotna w programie Advancer.
* Jak to zrobić?
    * Wpierw zaprojektuj payoff, do niego zrób setupy łańcuchami logicznymi
        * Wpierw draft sesji, o co w niej chodzi, temat i misja a z tego stwórz payoff
        * Potrzebny Ci artefakt? Wprowadź go i plotki o nim wcześniej
        * Zasada setup – setup/reminder – payoff. Trójki.
    * Nie wprowadzaj rzeczy niepotrzebnych i szumu; graczom i tak jest ciężko
    * Rób setup -> payoff zamiast ekspozycji. Nie używaj tu flashbacków CHYBA że potrzebujesz setup do czegoś.

Wklejam grafikę:

![Graf "Samotna w programie Advancer"](Materials/230808/03-230808-setup-payoff-samotna-advancer.png)

### 4.4. Aktywacja Klocka zamiast Announce Badness

Główne przemyślenia odnośnie pierwszego krzyżyka.

* Stan aktualny
    * Postać Gracza: Sariel, włócznik. Ma dwóch sojuszników (NPC): Elara i Maldor.
    * Cel: Sariel i sojusznicy idą przez dziwne bagna, szukają zaginionego zwiadowcy
    * Lokalizacja: dziwne bagno
    * Przeciwnik: 
        * Gracze nie wiedzą, ale są na bagnach pełnych topielców
        * Oprócz tego samo bagno jest trudnym terenem
    * Problem: Zwiadowca umrze, jeśli Sariel i ekipa go nie uratują
* Stan oczekiwany
    * Zwiadowca będzie uratowany
    * Sariel i ekipa wrócą bezpieczni
    * Nikt nie będzie Dotknięty Energią Bagna

![Plugawe bagno](Materials/230808/04-230808-creepy-haunted-swamp.png)
(midjourney: `creepy swamp in the mist, fog, eerie and full of shadowy figures, dangerous path, creepy horror --ar 16:9 --v 5.2`)

Sariel i ekipa się częściowo rozdzielają, ale zostają w zasięgu widzenia. Szukają śladów Zwiadowcy. Znalezienie śladów Zwiadowcy w tych okolicznościach to konflikt `Tr Z`, bezpieczne poruszanie się po bagnie to Eskalacja.

Czyli Tr Z. Sariel próbuje się przedostać. **Porażka**.

Mistrz Gry **aktywuje komponent "Nawiedzone Bagno"**. 

_Jedno ze starych na wpół zatopionych ciał nagle otwiera oczy._

Co się właśnie stało?

1. Gracze dostają sygnał, że to miejsce jest niebezpieczne. **Setup**.
2. MG może uruchamiać ruchy Komponentu "Nawiedzone Bagno".
3. Gracze właśnie są pod presją.

Przy następnej porażce zrobiłbym coś jeszcze piękniejszego.

* **Sariel**: dobrze, manewrujemy pomiędzy istotami na bagnie. Szukamy zwiadowcy unikając wszelkich rzeczy jakie się ruszają.
* **RNG**: porażka
* **MG**: (kolejny **Setup**). Stopa Maldora ugrzęzła w bagnie. Maldor nie może się ruszyć a zbliża się do niego niezgrabnie jeden z nieumarłych. Teren jest bardzo trudny a Maldor zaczyna panikować.

Mistrz Gry **aktywował komponent "Unieruchamiający Teren"**

Nagle Sariel i ekipa widzą nowy stan:

* Są w głębi Nawiedzonego Bagna, które chce zabrać ich ze sobą 
    * i prezentuje nowych nieumarłych
    * i jest w pełni sprawnym aktywnym Komponentem
* Teren Unieruchamiający ich spowalnia i sprawia, że trudno się poruszać
    * nie tylko pasywnie ich unieruchamia, ale też aktywnie próbuje ich rozdzielić
* Maldor ma niskie morale; może zrobić coś głupiego i może się mu stać krzywda
* I do tego jest gdzieś jeszcze Zwiadowca którego chcieli znaleźć i którego MG może zabić jeśli Gracze się wycofają nawet nie próbując go ratować

Nawet, jeśli Gracze bez większego problemu przejdą przez to i odzyskają Zwiadowcę, jest to ciekawe miejsce i na pewno efektowny _encounter_.

## 5. Notatka ze spotkania irl

* pierwszy X: foreshadowing, pociągnięcie nowego motywu / klocka
* announce badness
* announce change
* prawo nie zgodzenia się na X / V 
    * "czy to ma sens" "czy chcemy w to iść"
    * "możesz wymienić to na coś GORSZEGO ale w innym wymiarze"
    * gesty otwierają negocjację, gracze niekoniecznie muszą się zgadzać - pomóż im
* nokaut wymaga drzewa dojścia do nokautu - seria rzeczy sprawiających że się DA zrobić nokaut, bo nie da się jednym ruchem tego zrobić
    * "poziom przekonania osób przy stole - czy to już?"
        * kluczowy zasób? Pewność grających? "kupione w historii?"
* karty jako przedmioty, które pokazują kontekst i montują Dark Heart (impulsy itp).
