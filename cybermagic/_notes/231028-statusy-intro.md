# Analiza: Czego potrzebują wszystkie elementy sesji (230925)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* First cut statusów

---
## 2. Spis treści i omówienie

1. Tenety
2. Istniejące statusy jakie znamy
    1. Skażony
    2. Rozproszony
    3. Ranny

---
## 3. Tenety

* Może działać więcej niż jeden Status na postać
* Statusy stackują

---
## 4. Istniejące statusy jakie znamy
### 4.1. Status: Skażony

* bardziej wrażliwy na energię magiczną i silniejsza magia, ale mniej kontrolowana
* ZAWSZE: +1X, +1Ob
* DODATKOWO JEŚLI MAGIA: 1V -> 1Vb, 1X -> 1Xb, +1Ob

### 4.2. Status: Rozproszony

* pod wpływem silnych emocji / myśli o czymś innym
* -1V, 3X -> 3Xg, +3Og
* Og reprezentuje "skupiony na swoich myślach i swoim działaniu" lub "działanie automatyczne"
* można wymienić Og na Or - sukces ALE zadajesz sobie ranę by się skupić

### 4.3. Status: Ranny

* osłabiony i mniej zdolny do działania
* -1V, 2V -> 2Or (sukces ale dalsza rana); możesz usunąć Or

