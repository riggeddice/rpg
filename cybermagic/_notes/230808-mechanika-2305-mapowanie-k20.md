---
layout: mechanika
title: "Notatka mechaniki, 230608, mapowanie 1k20"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

* Jak zrobić generator losowy oparty na 1k20?
    * Każdy ma 1k20, ludzie lubią 1k20
    * Nie każdy ma żetony

---
## 2. High-level overview

Mechanika jest kodowana jako **2305**.

1. Cel - uproszczenie. Każdy ma 1k20. Nie każdy ma żetony.
    1. Przypomnienie Design Goals, w kontekście RNG
2. Mechanika sigilowa, nie zakresowa
    1. Co to jest 'sigil vs zakres'?
    2. Porównanie sigile - zakresy
    3. Czemu sigile są lepsze dla EZ
3. Prawdopodobieństwa i mapowanie
    1. Cele mapowania - co ważne, co odrzucamy
    2. Najbardziej typowe konflikty
    3. Problem entropii
    4. Problem kolorowania żetonów
    5. Finalnie mapowanie pierwotnej kości
4. Wykorzystanie 1k20 zamiast puli
    1. Pojedynczy konflikt bez eskalacji
    2. Eskalacja i mechanizm kompensacji
    3. Dynamiczna zmiana puli
5. Podsumowanie całości
    1. Minimalny RNG w EZ na 1k20
    2. Maksymalny RNG w EZ na 1k20


---
## 3. Cel - uproszczenie. Każdy ma 1k20. Nie każdy ma żetony.
### 3.1. Przypomnienie Design Goals, w kontekście RNG

Pełen opis Design Goals został wydzielony do [osobnej notatki (230808-EVERGREEN-rd-design-goals)](230808-EVERGREEN-rd-design-goals) od teraz. Ten link może być niestabilny, acz będę pilnował.

Zostawiam te, które musimy rozpatrzyć pracując dalej:

1. **Zasady wiodące designem**
    * (1) **Każdy konflikt da się zamodelować**
        * kolorowanie + entropia mają znaczenie. Kolorowanie to podpowiedź, entropia to klucz.
    * (2) **Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.**
        * RNG -> kontekst -> decyzja -> negocjacja -> wynik
        * wygranie wyniku jest emocjonujące
    * (6) **Nie ma możliwości, by osoba siedząca przy stole nie wpłynęła na fikcję**
        * każdy ma kiedyś (V) lub (X). Nawet jeśli to dość pasywny gracz, musi potwierdzić.
    * (7) **Gracz ma kontrolę nad ryzykiem**
        * to Ty decydujesz o eskalacji lub nie
    * (8) **Catchup mechanism**
        * im bardziej naciskasz, tym więcej tracisz.
    * (9) **Chaos i nieprzewidywalność**
        * obvious, ale to znaczy, że nie może być kości z "tylko sukcesami"
2. **Parametry niefunkcjonalne**
    *  (11) **Mechanika jest szybka (gdy się wdrożysz)**
    *  (12) **Mechanika jest tania z perspektywy kosztu intelektualnego (gdy się wdrożysz)**
        * MECHANIKA, nie ROZGRYWKA. To jest ważne. Rzut kością ma być nieproblematyczny. To gra potem jest smakowita.
3. **Inne ważne aspekty**
    * (14) **Manewry (deklaracje) gracza w fikcji mają znaczenie**
        * konieczność premiowania dobrych decyzji mechanicznie (np. modyfikatory)

---
## 4. Mechanika sigilowa, nie zakresowa
### 4.1. Co to są mechaniki sigilowe i zakresowe
### 4.1.1. Co to są mechaniki zakresowe

![Mechaniki zakresowe](Materials/230808/01-230808-mechaniki-zakresowe.png)

* Typowe implementacje:
    * pięć przedziałów: TAK ORAZ, TAK ALE, TO SKOMPLIKOWANE, NIE ALE, NIE ORAZ
    * "więcej niż 11, trafiłeś. Więcej niż 20, trafienie krytyczne"
* Myślcie o nich trochę jak o prędkościomierzach: "jadę za wolno", "jadę dobrze", "jadę za szybko".
    * Są ciągłe (1-5, 6-10...)
    * Są uszeregowane (nie ma przedziału dobrego - złego - lepszego; zwykle jest (zły - dobry - lepszy - jeszcze lepszy niż lepszy)
    * Poszczególne pole nie ma znaczenia, ma znaczenie w jakim zakresie pole się znajduje
    * Mapowanie jest na jeden z **N przedziałów**, gdzie **N** to zwykle 5 lub mniej
* Modyfikatory dodają lub odejmują jakąś wartość do wyniku rzutu, bo chodzi o przesunięcie prawdopodobieństwa do którego zakresu trafimy

### 4.1.2. Co to są mechaniki sigilowe

![Mechaniki sigilowe](Materials/230808/02-230808-mechaniki-sigilowe.png)

* Typowe implementacje:
    * `dictionary` / `map` w programowaniu (_key-value pair_)
    * tabelki spotkań losowych / w "sandboxach"
* Działają jak mapy (klucz-wartość). 
    * Mogą mieć bardzo różnorodne "sigile" (symbole mające konkretne znaczenie)
        * dla 1k6 mogą mieć: {(1, kot), (2, pies), (3, jabłko), (4, naleśnik), (5, serduszko), (6, reroll)}
    * Niekoniecznie są ciągłe
        * dla 1k6 mogą mieć: {(1, P), (2, S), (3, P), (4, P), (5, S), (6, kotek)}
            * 1, 3, 4: daje Porażkę
            * 2, 5: daje Sukces
            * 6: daje kotka. Bo oczywiście że coś da kotka.
* Modyfikatory zwykle wymieniają mapowanie dla jednego z symboli na coś innego.
    * Np. pula: {(1, P), (2, P), (3, P), (4, S), (5, S), (6, S)}
    * Modyfikator uskrajniający: "niech jeden sukces będzie krytyczny i jedna porażka krytyczna" 
        * -> {(1, **Pk**), (2, P), (3, P), (4, S), (5, S), (6, **Sk**)}
    * Modyfikator pozytywny: "niech jedna porażka stanie się sukcesem
        * -> {(1, P), (2, P), (3, **S**), (4, S), (5, S), (6, S)}
    * Modyfikator dodający magię: "jeden sukces jest magiczny, jedna porażka też, daj 2 entropie symetrycznie."
        * -> {(1, **Pm**), (2, **Om**), (3, P), (4, S), (5, **Om**), (6, **Sm**)}

### 4.2. Porównanie sigile - zakresy

* Oczekiwania
    * Zakresy mają proste oczekiwanie - po rzucie wiesz mniej więcej "sukces / porażka", choć niekoniecznie jesteś pewny przy krawędziach
    * Sigile wymagają konsultacji z jakąś formą tabelki
* Precyzja
    * Sigile dają możliwość zrobienia **WSZYSTKIEGO** co daje mechanika żetonowa
    * Zakresy mają kilka 'zakresów' i to jeszcze powinny być odpowiednio uszeregowane
* Łatwość użycia
    * Zakresy są **DUŻO** łatwiejsze do użycia niż sigile. Rzucasz, jedno mapowanie i masz.
    * W sigilach dla 1k20 masz 20 nieciągłych zakresów. W zakresach masz 4-5 zakresów ciągłych.
* Wartościowa dynamiczna zmiana "puli"
    * W wypadku Kompensacji
        * można dodać +1 do wyniku 1k20 w Zakresowym lub flipnąć jeden sigil w Sigilowym
        * -> Praktycznie nic to nie zmienia w Zakresowym
        * -> Praktycznie nic to nie zmienia w Sigilowym
    * W wypadku modyfikatora
        * można dodać +2 do wyniku 1k20 w Zakresowym lub flipnąć 1-2 sigile w Sigilowym
        * -> Praktycznie nic to nie zmienia w Zakresowym
        * -> Praktycznie nic to nie zmienia w Sigilowym
    * W wypadku Entropii / trzeciej strony
        * w Zakresowym robimy nowe przedziały; bardzo trudno. W Sigilowym zmieniamy jakieś sigile.
        * -> Katastrofa w Zakresowym. Serio nie mam pojęcia jak do tego podejść.
        * -> Praktycznie nic to nie zmienia w Sigilowym jeśli masz prostą regułę (np. skrajne zewnętrzne wartości)
    * W wypadku Specjalnych Wartości (np. pojedyncze trafienie krytyczne)
        * w Zakresowym będzie ciężko bez zmiany wszystkich prawdopodobieństw. W Sigilowym - jeden sigil
        * j.w.

### 4.3. Czemu sigile są lepsze dla EZ

Sigile > zakresy

1. **Catchup**. Nie ma catchup -> feel się dramatycznie zmienia. Zaimplementuję przez flipnięcie (V -> X -> V)
2. **Modelowanie dowolnego konfliktu**. Sigilowa kość działa jak wyciąganie żetonów z woreczka.
    * catchup
    * modyfikatory
    * magia, entropiczne, kolorowanie...

Zakresy > sigile

1. **Prostsze i szybsze w użyciu**. "Prostsze" to kwestia wdrożenia, "szybsze" nie przeskoczymy.

Wniosek: z mojego punktu widzenia sigile są lepsze. Jeśli się okaże, że się pomyliłem, zawsze mogę przeprojektować jeszcze raz.

## 5. Prawdopodobieństwa i mapowanie
### 5.1. Cele mapowania - co ważne, co odrzucamy



### 5.2. Najbardziej typowe konflikty

### 5.3. Problem entropii

### 5.4. Problem kolorowania żetonów

### 5.5. Finalnie mapowanie pierwotnej kości

## 6. Wykorzystanie 1k20 zamiast puli
### 6.1. Pojedynczy konflikt bez eskalacji

### 6.2. Eskalacja i mechanizm kompensacji

### 6.3. Dynamiczna zmiana puli

## 7. Podsumowanie całości
### 7.1. Minimalny RNG w EZ na 1k20

### 7.2. Maksymalny RNG w EZ na 1k20
