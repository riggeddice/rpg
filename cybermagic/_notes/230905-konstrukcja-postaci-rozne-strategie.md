# Wyjaśnienie: Różne sposoby konstrukcji postaci pod sesje (230905)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Jak budować NPC? Jak budować postacie? Jak budować grupy postaci?
    * Różne strategie w zależności od celu, roli i tego "co mamy"

---
## 2. High-level overview

1. ITERACJA PIERWSZA: (Sesja <-> "Wzór") -> Rola
    1. Pierwsza jest zawiązka sesji / koncept sesji
    2. Drugie są role potrzebne na sesji
    3. Przykład pierwszej iteracji na przykładzie prawdziwej sesji
    4. Co tu jest ważnego
        * Wychodzisz od konceptu sesji lub wzoru (filmu/książki)
        * Iterujesz koncept sesji aż masz "coś"
        * Patrzysz na Role potrzebne na sesji
            * Gdzie "Role" oznaczają "zbiór czynności i zachowań danych postaci" a nie "gotowe postacie"
2. ITERACJA DRUGA: Rola -> Postać
    1. Różnica między Rolą i Postacią
    2. Metoda 1: modelujemy film
    3. Metoda 2: wychodzimy z metakultury / frakcji
    4. Metoda 3: wychodzimy z (relacji / interakcji) z innymi postaciami
        1. Kto pasuje do tego zespołu i jest odpowiednio unikalny?
        2. Kto jest naturalnym przyjacielem lub rywalem?
        3. Kto generuje ciekawe i wartościowe konflikty?
    5. Metoda 4: wychodzimy z jakiegoś komponentu Serca postaci
        1. Core Wound - Core Lie
        2. Głód i Wizja
        3. Osobowość i Wartości
3. Specjalne metody konstrukcji postaci (też wychodzą od celu MG, ale niekoniecznie od roli)
    1. Dark Mirror
    2. Extremist Mirror

---
## 3. ITERACJA PIERWSZA: (Sesja <-> "Wzór") -> Rola
### 3.1. Pierwsza jest zawiązka sesji / koncept sesji

Główna strategia - zaczynamy od roli na sesji

Żeby móc stworzyć sesję, często wystarczy **film** albo **pomysł** który upraszczam i modyfikuję. To jest prapoczątek.

Np. "Batman Beyond", pierwszy odcinek zawiera następujące sceny (polecam serial gorąco; jest z 1999-2001). Potencjalne elementy / zawiązki sesji z samego początka pierwszego odcinka:

* "Batman, który jest za stary; ma atak serca ratując porwaną dziewczynę, więc musi wziąć pistolet do ręki by zatrzymać przestępców. To kończy jego karierę, mówi 'nigdy więcej'"
    * scena bohatera, który jest za stary i ma atak serca podczas akcji; złamie ideały by ratować ludzi
    * motyw "wiek pokonuje nawet największych bohaterów"
    * motyw "umrę w miejscu pracy bo praca jest dla mnie tak ważna", człowiek, który nie pogodził się z wiekiem i chce dalej działać

Jeśli cokolwiek wzbudziło Twoje emocje, nadaje się na sesję po zmutowaniu.

### 3.2. Drugie są role potrzebne na sesji

**Role** oznaczają _zbiór czynności i oczekiwań od danej postaci_. Rolą managera (upraszczając) jest zarządzać zespołem, rolą programisty (upraszczając) jest pisać kod. Jednocześnie osoba zatrudniona na stanowisku programisty będzie robić dużo więcej niż wynika z samej roli - elementy roli kontroli jakości, roli analizy biznesowej i roli negocjacji.

W powyższym przykładzie z Batmanem Role są proste:

* Ktoś, kogo trzeba uratować (Porwany)
* Ktoś, kto porwał Porwanego (Napadający)
* (Stary Bohater), którego pokonuje wiek, ale dalej walczy

Złożenie z tego sesji jest proste:

* Napadający chcą (coś), więc porwali Porwanego który im to (jakoś) umożliwia
    * coś: dostęp do starego autonomicznego czołgu, który zna (jakieś sekrety)
    * jakoś: bo Porwany jest synem (eks-mistrzowskiej złodziejki) i ma kody kontrolne czołgu z uwagi na $(dziwne wydarzenie z przeszłości powiązane z matką)
* Stary Bohater kiedyś walczył przeciwko czołgowi; nie chce powtórki z rozrywki. Wraca z emerytury, by uratować miasto po raz ostatni
* Postacie Graczy niezależnie walczą przeciwko Napadającym, nie wiedzą (jeszcze) o czołgu, Bohaterze ani Porwanym

Zauważcie - na razie nie wiemy **niczego** o postaciach, motywacjach i elementach sesji. Składamy jedynie strukturę, i to iteracyjnie. Ale widzimy jakie są **role** postaci na sesjach i łączymi filmy / książki przez serie dziwnych wydarzeń.

---
### 3.3 Przykład pierwszej iteracji na przykładzie prawdziwej sesji

Sesja "druga tienka przybywa na pomoc". W ramach tej sesji mamy postać gracza z Aurum (arystokracji) - Elenę Samszar - która jest kochana w domu (mimo, że powoduje sporo kłopotów) i która jest wysłana jako oficer łącznikowy do Orbitera (wojsko kosmiczne) by "dorośleć i się zrehabilitować i zresocjalizować". Czyli stara praktyka wysyłania niesfornej piątej córki arystokracji na prowincję by się wyszalała i nauczyła życia.

To sprawia, że potrzebuję kilka postaci (w kolejności wymyślania; wykorzystuję wszelkie filmy i książki typu "droga bohatera"):

ROLE:

* przełożony z ramienia Orbitera, który będzie wyżej niż Elena i który będzie skupiał się na sesji a niekoniecznie na Elenie 
    * (odpowiednik dyrektora szkoły, ze Wzoru; żyje w innym świecie i nie obchodzi go postać gracza, chce, by była niewidoczna)
    * -> ma pokazać graczowi, że tu "zasady są inne" i nic nie będzie jak było. W domu postać mogła wszystko, tu jest traktowana jak "NPC w opowieści Orbitera". Mają zadanie i muszą je wykonać.
    * Dalej nazywam go **Mikołaj Larnecjat**
* stereotypowy "sierżant" z ramienia Orbitera, który jednak próbuje pomóc młodej panience się wdrożyć w nowe życie, choć jest surowy z natury. Ale da jej szansę.
    * (odpowiednik starego mentora który dał szansę młodzikowi, ze Wzoru)
    * -> ma testować postać gracza, ma dać okazję pokazać tą postać i jak zareaguje
        * np. "obierz ziemniaki", "idź tam zrób to"
        * ale też pomoże w trudnych sytuacjach i nakieruje delikatnie postać gracza
    * Dalej nazywam go **Antoni Paklinos**
* ktoś z 'nizin' chętny do zaprzyjaźnienia się z młodą panienką, z kim da się gadać i zbudować link z 'typowymi żołnierzami'
    * (odpowiednik tego kumpla ze szkoły co pierwszy podejdzie, nie boi się odmowy i jakkolwiek jest nieco dziwny ale wprowadzi do grupy)
    * -> ma umożliwość link postaci gracza z innymi NPCami i zbudować poczucie, że może nie jest tak źle
    * Dalej nazywam go **Tomasz Afagrel**
* ktoś ze 'starego życia' (z Aurum), kto będzie przekonywał panienkę o jej wyższości i wybitności. Szkodzi jej, nie wiedząc o tym, bo wyrośnie z tego.
    * (odpowiednik rodziców, którzy za bardzo trzymają dziecko pod kloszem. Pomogli, ale muszą pozwolić jej odejść)
    * -> ma dać bezpieczeństwo ale też ostateczny konflikt wyrwania się z tego bezpieczeństwa.
        * POTENCJALNIE Elena przestanie być 'rozpieszczoną tienką z Aurum' i zostanie 'sobą', cokolwiek to znaczy; nie wiem gdzie gracz ją poprowadzi.
    * Dalej nazywam go **Cyprian Mirisztalnik**

Widzicie wyraźnie w tym miejscu interakcje pomiędzy postaciami i ich wpływy na Elenę:

* Antoni (sierżant Orbitera)
    * testuje Elenę, daje jej okazję do pokazania kim jest i jak się zmienia (jeśli)
    * mediuje między Eleną i resztą oddziału Orbitera, daje jej szansę i buforuje ewentualne błędy gracza i postaci
    * można do niego przyjść i popytać, poprosić, dowiedzieć się o sprzęcie - umożliwia mi zrobienie obcego i dziwnego terenu a Antoni to jest ten "pozytywny" punkt co wyjaśni i pomoże
* Tomasz (kapral Orbitera)
    * wkręca Elenę w różne dziwne historie i problemy, ma ją odciągać od sforności i ma ożywić to miejsce
    * źródło ogromnej ilości konfliktów zwłaszcza, jeśli zachowuje się niegodnie i oczekiwalibyśmy czegoś lepszego od żołnierza
    * jednocześnie osoba dość pozytywna i sympatyczna
    * ociepla Elenę wśród żołnierzy Orbitera, sprawia, że Elena może być "wśród swoich", nawet, jeśli to tylko zwykły plebs a nie arystokracja Aurum
* Cyprian (chorąży Aurum)
    * pociesza Elenę, daje jej dobre pomysły, chce się nią opiekować i ją chronić, wierzy w nią i mówi jej co i jak
    * będzie bronił Elenę przed "tym co złe" w okolicy; w tym złym miejscu jest lojalny jej i tylko jej, jako jedyny
    * stanowi źródło konfliktów - będzie zwalczał (swoim zdaniem) złe ruchy Orbitera wobec Eleny. Jak Elena ma obierać ziemniaki, on obierze za nią
        * może się w Elenie będzie podkochiwał? Może zgłosił się na ochotnika? Nie wiem jeszcze, ale musi być niezwykle lojalny
    * docelowo, reprezentuje "starą Elenę". Jego zdaniem jej wszystko wolno, "bo jest arystokratką Aurum" a nie "bo na to zasłużyła swymi czynami i poświęceniem".
* Mikołaj (dowódca Orbitera)
    * ostateczny rozstrzygacz konfliktów, kij i ktoś kto OCZEKUJE wyników od Eleny
    * manager, generator sesji, generator oczekiwań (czyli źródło presji i źródło 'rób to lepiej')
    * najlepiej dla wszystkich jeśli tam JEST i nikt z nim nigdy nie gada; mówię - "dyrektor szkoły"

Lub graficznie:

![Relacje Eleny, to powyżej graficznie](Materials/230905/01-230905-relacje-eleny.png)

---
## 3.4 Co jest tu ważnego

* Wychodzisz od konceptu sesji lub wzoru (filmu/książki)
* Iterujesz koncept sesji aż masz "coś"
* Patrzysz na Role potrzebne na sesji
    * Gdzie "Role" oznaczają "zbiór czynności i zachowań danych postaci" a nie "gotowe postacie"
        * Programistycznie "Role" to "interfejsy"

---
## 4. ITERACJA DRUGA: Rola -> Postać
### 4.1. Różnica między Rolą i Postacią

Weźmy sobie żołnierza Orbitera z powyższej sesji. Nazwijmy go "Marcin Aczebniak". Jego Rola:

* Żołnierz Orbitera
* Koleś, który ($coś) dotknął w przeszłości (($powód dotknięcia?)), przez co jest przez ($ducha) opętany
    * $coś, $duch, ($powód dotknięcia): na razie nie wiem
* Koleś, który stanowi ($zagrożenie) dla reszty oddziału przez opętanie
    * $zagrożenie: on sam jest tylko "nośnikiem" i "kotwicą", ale ($duch) chce ($zguby) innych żołnierzy
        * $duch: ktoś, kto tu zginął z ręki Orbitera dawno temu
        * $zguba: szaleństwo i halucynacje kończące się w wystrzelaniu sojuszników

Czyli Rola spłaszcza się do "nośnik, pacjent zero, źródło zagrożenia".

A jak wyglądałby ten żołnierz jako Postać?

* Marcin Aczebniak
    * specjalizacja: zwiad i działania w górzystym i trudnym terenie
    * normalnie wesoły, nic nie robi sobie z trudnych warunków, ma kiepskie żarty na każdą okazję
    * reprezentowany przez 'potrzymaj mi piwo', pierwszy do trudnych i dzikich wyczynów, wieczny chłopiec
    * niesłychanie lojalny i pracowity, święcie wierzy w swój zespół i dowódcę. Nie jest tu 'za karę', żadnej pracy się nie boi
    * w życiu nie złamał żadnego rozkazu, jest raczej taki... prymusik przed przełożonymi
    * Jedyne, czego za wszelką cenę unika to imion bogów. Oraz koloru różowego. Jest przesądny.

Skąd to się wzięło?

* **Marcin Aczebniak**
    * "coś dotknął i dał się opętać, jest nośnikiem"
        * -> **normalnie wesoły, nic nie robi sobie z trudnych warunków, ma kiepskie żarty na każdą okazję**
        * -> **reprezentowany przez 'potrzymaj mi piwo', pierwszy do trudnych i dzikich wyczynów, wieczny chłopiec**
    * "on sam jest tylko "nośnikiem" i "kotwicą"" plus "$duch: ktoś, kto tu zginął z ręki Orbitera dawno temu"
        * -> **niesłychanie lojalny i pracowity, święcie wierzy w swój zespół i dowódcę. Nie jest tu 'za karę', żadnej pracy się nie boi**.
            * (Mirror Cypriana): 'lojalność i duszenie w sobie prowadzi do potencjalnej katastrofy'
            * (fabularnie): 'duch widząc że on wierzy w Orbiter chce go torturować, nie tylko zabić'
            * (cechy metakultury: faeril)
    * **w życiu nie złamał żadnego rozkazu, jest raczej taki... prymusik przed przełożonymi**
        * (cechy metakultury: faeril)
    * **jedyne, czego za wszelką cenę unika to imion bogów. Oraz koloru różowego. Jest przesądny.**
        * (cechy metakultury: faeril - silna wiara + przesądy)
        * okazja pokazania że coś jest nie tak, gdy przestaje tego unikać; on i duch się mieszają w jeden byt
        * coś co pokazuje postać, natywna cecha - koledzy mu dokuczają
    * -> **specjalizacja: zwiad i działania w górzystym i trudnym terenie**
        * bo inaczej nie miałby jak być pierwszym w którym zaczepi się duch. Musiał być na "wrogim" terenie.

Tak jakby... oblekłem szkielet (rolę) w cechy charakteru, które umożliwiły pewnym rzeczom się wydarzenie. A potem zmodyfikowałem to pod kątem dodania jakichś naturalnych cech charakteru dla postaci, zgodnie z różnymi metodami poniżej.

---
### 4.2. Metoda 1: modelujemy film

Starcraft: Brood War (rok 1998). Admirał DuGalle. Jeśli nie znacie postaci, tu jest intro: https://www.youtube.com/watch?v=-00uQzXyujI

DuGalle ma kilka charakterystycznych cech:

* Zachowuje się dość wyniośle i dumnie. Patrzy z wyższością. Mówi używając zaawansowanego słownictwa.
* Nie dba o "nie swoich ludzi". Nie uratował kolonistów. Nieprawdopodobnie zimny, nie dba o ludzkie życie.
* Jest praktykiem - nie chce "patrzeć na stół operacyjny gdzie tnie się zergi". On chce zobaczyć to w praktyce.
* Ma bliskiego przyjaciela (Alexieja Stukova) i wobec niego pozwala sobie na więcej

Niech DuGalle przekształci się w **komendanta Mikołaja Larnecjata**. Ale niech Larnecjat straci wszystkich bliskich. Jest "ostatnim ze swojego statku", osobą złamaną.:

* Mikołaj Larnecjat: dowódca grupy wydzielonej; stracił bliskich podczas wojny
    * OCEAN: (E-O-): zamknięty w sobie, rzetelny, spokojny i dobry w powtarzalnych operacjach. "Nie powinienem był przetrwać wojny. Jeszcze Orbiterowi jednak się przydam."
    * VALS: (Conformism(Duty), Humility): jak coś ma zrobić, zrobi i dostarczy. "Nieważne czy mi się podoba, czy nie. Jestem elementem maszyny Orbitera."
    * Core Wound - Lie: "Wszystko, co kochałem zginęło i zostało zniszczone. Jestem sam." - "Nie przywiązuj się. Jest tylko misja."
    * Styl: Spokojny, absolutnie nie inspirujący i cichy miłośnik muzyki klasycznej pochłonięty swoimi myślami, ale wykonujący każde polecenie rzetelnie i sensownie.
    * metakultura: Atarien: "dowódca każe, ja wykonuję. Na tym polega hierarchia. Tak się buduje coś większego niż my sami."

---
### 4.3. Metoda 2: wychodzimy z metakultury / frakcji

Teraz operujemy na **stereotypach**.

* Rola: _młody, niedoświadczony chłopak, który natrafił na coś bardzo niebezpiecznego_

Jak wyglądałoby to, gdybym dał tu **stereotyp::wiking**?

* Młody, ukrywający strach, wytatuowany i silny chłopak
    * Ubiera się w rzeczy przyciągające uwagę i pokazujące jego przeszłe osiągnięcia
* Skonfrontowawszy się z czymś strasznym, zaatakował to, maskując krzykiem swoje przerażenie
* Pierwszy impuls: atak, zniszczenie

Jak wyglądałoby to, gdybym dał tu **stereotyp::ulicznik**?

* Młody, wychudzony i żylasty chłopak, zawsze noszący przy sobie nóż
    * Ubiera się w rzeczy, by nie zwracać na siebie uwagi
* Skonfrontowawszy się z czymś strasznym, wycofał się, zostając w cieniu
* Pierwszy impuls: wycofanie się, obserwacja, szukanie okazji

**Metakultura** oznacza "etnoreligijną przynależność, która niesie ze sobą pewne wartości, zachowania i działania". Jest to termin fikcyjny, mający objąć rzeczy takie jak "stereotypowy wiking". Acz możecie pod to podstawić też frakcję, np. "mafia włoska" (od razu widzę elegancko ubranych, okrutnych ludzi, jak w Ojcu Chrzestnym).

Kluczem konstrukcji na podstawie metakultury czy frakcji jest stworzenie typowego członka tej grupy, potem nadanie jakichś cech wynikających z roli lub unikalizujących delikwenta.

Przykład: 

* **boronita Morten Baas**, przywódca niewielkiej grupki boronitów na terenach gdzie operuje Elena i Orbiter.
* Boronici należą do metakultury wenapnitów ( swiat/metakultury-i-ludy/lud-wenapnici--proetniczni-poszukiwacze-smierci-230905 ). Główne cechy wenapnitów:
    * wierzą, że obecność magii w świecie jest dowodem na to, że wszyscy żyją w symulacji. I da się opuścić Symulację przez Wielki Test.
    * wierzą w reinkarnację; dopóki dusza nie zda Wielkiego Testu, odrodzi się w innym ludzkim ciele
    * wierzą, że niezależnie w jakim ciele się nie znajdują, istnieje test możliwy do zdania ale zawsze bardzo trudny
    * wierzą, że im więcej "dobra" zrobią w tym świecie, tym lepszy będzie ich stan w przyszłym życiu
    * wierzą, że im więcej ludzi uwierzy w Symulację tym szybciej Symulacja zostanie zniszczona. 
    * nie wszyscy ludzie są prawdziwi. Niektórzy to tylko NPC. Nie-wenapnici MOGĄ być NPC. Wenapnici, z definicji, NPCami nigdy nie są. Więc zabijanie nie-wenapnitów niekoniecznie jest złe.

Nie będę wpisywał wszystkich cech wenapnitów, bo to niepotrzebne dla przykładu. **Boronici** to odłam wenapnitów mieszkający w tych górach, klanowi, ścisłe społeczeństwo, bardzo odizolowane, JESZCZE bardziej fanatyczni niż zwykle.

Mając to, spójrzmy na **Mortena Baasa**. Od boronitów odziedziczył:

* (cel) obecność Orbitera w okolicy stanowi dobry Test. I okazję na pozyskanie środków.
* (charakter) żarliwy, głęboko wierzący w wenapnictwo i Symulację. Nie boi się śmierci ani śmierci swoich ludzi, jeśli ma sens.
* (wobec Orbitera) można powiedzieć WSZYSTKO Orbiterowi, nic prawdziwego nie warto; oni nie są ludźmi, to tylko NPCe z Symulacji.
* (umiejętności) "te góry są moim domem", "znam tu każdą jaskinię", "potrafię żyć w warunkach nieakceptowalnych dla innych, bo to Test"
* strategia skrajnie ekspansywna: nawracanie, więcej dzieci, więcej terenu, porwania ludzi. "Duchy Gór". "Symulacja dostarczy wszystkiego co trzeba do Testu".
* blady, żylasty, zawsze otulony ciepłymi ubraniami.
* (rola) Żarliwy, quasi-kapłan. Mówi bardzo przekonywująco, wręcz hipnotycznie.

Potraficie sobie go wyobrazić, prawda? A teraz użyjmy generatora na podstawie fiszek (całkowicie losowo wygenerowana postać):

![Prototyp Baasa](Materials/230905/02-230905-przykladowy-prototyp-baasa.png)

I teraz je połączmy:

* Morten Baas: przywódca boronitów, wiecznie dąży do jak najlepszego życia swoich ludzi w niegościnnych terenach gór przelotyckich
    * OCEAN (O+ N-): nigdy nie jest zadowolony stanem aktualnym, zawsze dąży do poszerzenia posiadania. "Nieważne, czego Symulacja na nas nie rzuci, wszystko jest potencjalną okazją"
    * VALS (Achievement, Hedonism): dąży do jak najlepszego poprawienia lokalnych boronitów, ale też chce korzystać z życia. Nie jest ascetą. "Wielki Test nie oczekuje, że będziemy się umartwiać"
    * Core Wound - Lie: "Podczas burzy śnieżnej zaufaliśmy technologii i mój brat umarł" - "Nie możesz liczyć na magię czy technologię, Symulacja ukarze Cię za głupotę. Tylko wenapnici są godni zaufania."
    * Styl: patrzy na wszystko strategicznie, wszystko trzyma w garści i jest nieufny do poziomu paranoi. Zawsze ma drogę ucieczki. Ale żąda najlepszych kąsków, 'ma gadane' i potrafi ukryć co myśli.
    * Cel: ekspansja; wszystko nie-boronickie to 'fair game' jak długo nikt nie może nic mu przypisać. Tylko boronici są uważani za 'prawdziwych ludzi'.

---
### 4.4. Metoda 3: wychodzimy z (relacji / interakcji) z innymi postaciami
#### 4.4.1. Kto pasuje do tego zespołu i jest odpowiednio unikalny?

Popatrzcie na typowe anime:

* Mamy nieco nieśmiałego kolesia, który nie uczy się najlepiej i ma dobre serce

Kto pasuje do tego zespołu?

* Ten koleś, który się świetnie uczy i wszystko mu wychodzi w nauce. Najpewniej jest specem od komputerów i master hackerem.
* Przyjaciółka z dzieciństwa, która zawsze wpakuje głównego bohatera w kłopoty.

Innymi słowy, jeśli mamy jakieś postacie, możemy dodać kolejną postać przez dodanie kogoś o:

* unikalnej, wyostrzonej cesze charakteru
* unikalnej, wyostrzonej roli

Na przykład:

![Relacje wewnątrzzespołowe](Materials/230905/02-230905-kto-pasuje-do-zespolu.png)

Zauważcie - od razu widać, że: 

* Przyjaciel i Mikołaj konfliktują się o "rozkazy czy dobro ludzi", gdzie Mikołaj uważa że mają rozkazy i musi dbać o swoich, a Przyjaciel uważa, że misją Orbitera jest ratowanie ludzi i Mikołaj może wykonuje rozkazy ale zdradza funkcję Orbitera.
* Morton Baas i Mikołaj są podobni do siebie. Mają cechy wspólne. Ale też mają inne ekspresje, inaczej działają. Ale podobieństwa - mimo kultur - są uderzające.

---
#### 4.4.2. Kto jest naturalnym przyjacielem lub rywalem?

Wariant tego co powyżej. 

Popatrz na postać. Zastanów się, KTO by został przyjacielem - jakie ma cechy podobne, jakie odmienne. Co unikalnego wnosi, gdzie się zgadzają, o co się kłócą.

---
#### 4.4.3. Kto generuje ciekawe i wartościowe konflikty?

Spójrzmy na naszą sesję. Mamy Elenę, która trafia do nie-swojego terenu, niebezpiecznego. Jest sama.

* Dodajmy jej sojusznika ze "starego świata", kogoś, kto przypomina jej "poprzednie życie"
* Dodajmy jej sojusznika z "nowego świata", kogoś, kto pomaga jej nawigować "nowe życie"

W ten sposób powstaje para:

* Cyprian, chorąży Aurum, strażnik przeszłości i sojusznik Eleny
* Tomasz, kapral Orbitera, przyszłościowy prospekt chorych pomysłów dla Eleny

![Elena, Tomasz, Cyprian](Materials/230905/04-230905-elena-tomasz-cyprian.png)

Jak widzicie, jest to w pełni zaprojektowane pod kątem JAKIE KONFLIKTY MOGĘ ZROBIĆ. O co mogą się kłócić. Jakie pokusy lub komplikacje stawiają te postacie.

---
### 4.5. Metoda 4: wychodzimy z jakiegoś komponentu Serca postaci
#### 4.5.1. Core Wound - Core Lie
##### 4.5.1.1. Co to jest?

W sercu każdej aktywnej postaci która wstaje z łóżka robić nietypowe rzeczy jest _Core Wound_ - rana, która sprawia, że postać jest skrajnie zmotywowana do czegoś. Wydarzenia, które ukształtowały postać. _Core Wound_ prowadzi do _Core Lie_ - błędnej percepcji rzeczywistości, generując niespodziewane zachowania.

* Nie byłoby **Batmana**, gdyby **Bruce Wayne** jako dziecko nie stracił rodziców. To jego _Core Wound_, coś, co go pożera całe życie.
    * Jego _Core Lie_ to "jeśli będę bił przestępców, żadne inne dziecko nie ucierpi tak jak ja - tylko ja mogę uratować Gotham, działając poza systemem, bo systemy nie pomogą"
* Częste _Core Wound_ antagonisty w shonen anime to "miałaś MNIE kochać! Dlaczego wybrałaś JEGO!"
    * Co prowadzi do _Core Lie_ "jeśli zamknę Cię w złotej klatce i dam Ci wszystko, będziesz kochać mnie i będziesz ze mną na zawsze"

Z perspektywy linków:

* [Core Wound - Lie, wyjaśnione](https://writershelpingwriters.net/2014/02/help-character-wounds-list-common-themes/)
    * "Every Wound Contains a Lie" -> "Is this somehow my fault? Am I to blame?"
    * "Wounds Cause Flaws To Form"
* [Core Wound - Lie, jak używać do budowy zahaczek / zaczepek](https://www.novelwritingonedge.com/2020/10/hook-lines-with-core-wounds.html)
    * "Every core wound is based on a basic knowledge that we are unacceptable as we are, so we have to adjust and change to be perceived as good."
    * kilka fajnych przykładów

##### 4.5.1.2. Zastosujmy to w praktyce.

Podczas sesji z Eleną, graczka wygenerowała jako dodatkową sojuszniczkę "pokojówkę" imieniem Adelajda. To ma być kompetentna, ładna, cicha agentka, która będzie pomagać Elenie i robić dla niej rzeczy jakich panienka Elena sama zrobić nie może.

Jako MG, zadaję sobie pytania:

* Czemu Adelajda miałaby jechać z Eleną na "wygnanie"?
* Czemu Adelajda **chce** być cichą, niezauważalną dziewczyną, służącą Elenie (której nawet nie zna)?
* Jakie konflikty może dla mnie Adelajda generować w maksymalnie elegancki sposób?

Co prowadzi mnie do następującej struktury (dwa warianty Core Lie, oznaczone v1 i v2):

* Konstrukcja _Core Wound_
    * _Historia / Kontekst_: ród Eleny uratował mnie od innego rodu magów (Blakenbauerów), który okrutnie traktował mnie i moją rodzinę. Tam nauczono mnie ciszy i posłuszeństwa. 
    * _Rana właściwa_: Tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować. Oni wszyscy są tam i cierpią nadal. A ja nic nie mogę zrobić.
* Konstrukcja _Core Lie_
    * _Core Lie v1_: jeśli się będę wychylać, jeśli pokażę coś więcej niż posłuszeństwo i kompetencję, to ktoś inny mnie porwie lub ród Eleny mnie odrzuci
    * _Core Lie v2_: ród Eleny jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło

**Popatrzmy na _pierwszy wariant_ Adelajdy**:

* _Core Wound_: tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować. Oni wszyscy są tam i cierpią nadal. A ja nic nie mogę zrobić.
* _Core Lie_: jeśli się będę wychylać, jeśli pokażę coś więcej niż posłuszeństwo i kompetencję, to ktoś inny mnie porwie lub ród Eleny mnie odrzuci

Mamy do czynienia z kompetentną i cichą osobą, która nadal jest kierowana przez swoje demony. Czuje wieczne poczucie winy i strach, że wróci tam do Blakenbauerów. Adelajda jest wierna ze strachu, nieskończenie lojalna i uśmiecha się nawet gdy jest maltretowana przez inne postacie z rodu Eleny.

Adelajda w tej wersji jest przerażającą i smutną postacią przez to, co jej zrobiono i co sama sobie zrobiła. Daje możliwość Elenie pokazania Adelajdzie, że będzie dobrze, że ona jest bezpieczna tutaj i nic złego jej nie spotka.

**A teraz _drugi wariant_ Adelajdy**:

* _Core Wound_: tylko mnie udało się uciec i tylko mnie udało się rodowi Eleny uratować. Oni wszyscy są tam i cierpią nadal. A ja nic nie mogę zrobić.
* _Core Lie_: ród Eleny jest inny - to jedyny szlachetny, dobry ród; oni nie pozwalają na zło

Tu Adelajda uwierzyła (błędnie), że ród Eleny jest dobry i szlachetny; że oni wszyscy są tacy jak mag, który uratował Adelajdę. Adelajda w to wierzy, bo alternatywa jest dla niej zbyt straszna. Więc Adelajda nadal jest cicha i niezwykle lojalna, ale tym razem Adelajda będzie stawać w cichej obronie "swojego" rodu i będzie protestować przeciwko niektórym akcjom Eleny które - zdaniem Adelajdy - naruszają "rdzenną moralność" rodu Eleny.

**Jako, że w tej chwili szukam bardziej generatora konfliktów niż demonstracji Mrocznego Serca, wybieram wariant drugi**. Będzie dużo bardziej kompatybilny z sesjami jakie planuję, zwłaszcza, że Elena ma podejście "wiedza i duchy PONAD ludzi" a Adelajda dzięki _Core Lie_ wierzy, że Elena "ceni ludzi i ich dobrobyt nade wszystko". Więc pokojówka i doświadczona agentka i komandoska będzie skonfliktowana z Eleną, którą ma chronić.

Innymi słowy -> lepszy stan konfliktowo-dramatyczny.

---
#### 4.5.2. Głód i Wizja

Góry, w których znajduje się Elena to mroczne i niebezpieczne miejsce. Wyobraźmy sobie następującą historię:

* Młody i obiecujący zwiadowca noktiański, ma narzeczoną
* Rozbił się w tych górach podczas wojny
* POWINIEN BYŁ ZGINĄĆ. Ale nie zginął. Zmienił się. Stał się cieniem, potworem. 
* Nadal ma zdjęcie swojej ukochanej. Ale nie ma jak do niej wrócić. I nie wie czemu, ale żyje dalej.
* Trzy lata później, ląduje Orbiter. Jego wrogowie. Ci, przez których stracił wszystko.
    * A on, w swojej potwornej formie, jest w stanie zemścić się na nich...

Czego **naprawdę pragnie** ten zwiadowca? Jaki jest jego **Głód**?

* Zemścić się na Orbiterze
* Wrócić do bycia człowiekiem i wrócić do ukochanej

Jaka jest jego **Wizja**, czyli różnica między (światem który jest) a (światem, który powinien być)?

* Jest potworem -> powinien być człowiekiem
* Orbiter jest tutaj, nie ucierpiał -> Orbiter powinien zapłacić. Zgliszcza, ruiny. Niech cierpią tak jak on.
* Ukochana jest gdzieś daleko -> Ukochana jest szczęśliwa i bezpieczna. Z nim. On jest z nią.
* On marznie i jest wiecznie głodny -> Szczęśliwy, z rodziną, jako człowiek, uczciwie pracujący. Nie jest głodny ani nie jest mu zimno.

Czy widzicie **w jaki sposób** będzie się zachowywał nasz zwiadowca noktiański?

* Jeśli może skrzywdzić orbiterowców bez szkody dla siebie, zrobi to
* Jeśli może się najeść czy ogrzać, będzie do tego dążył

Ale to daje nam też ciekawe konflikty dla tej postaci:

* Niech Elena daje mu możliwość powrotu do ludzkiej formy. Czy wybierze to, czy zemstę na Orbiterze?
* Niech zobaczy śpiącą Adelajdę, łudząco podobną (w jego chorej głowie) do jego narzeczonej. Czy ją zabije? Porwie? Spróbuje chronić z oddali?

Ważne jest to, że **Głód i Wizja** determinują:

* Czego chce postać
* Jak wygląda idealny świat postaci

Co sprawia, że da się bardzo łatwo sterować postacią i "wejść w jej głowę" w nieoczekiwanych momentach fabularnych. Łatwo zrozumieć postać, jeśli znamy jej Głód i Wizję.

---
#### 4.5.3. Dokonania i Zawód

Jesteśmy tym, co zrobiliśmy w przeszłości i tym, co w przeszłości osiągnęliśmy. Nie słowa mają znaczenie a czyny. Dlatego wyjście od Dokonań i ew. Zawodu są dobrą strategią konstrukcji postaci.

W tych górach jest stosunkowo niebezpiecznie a Orbiter będzie wysyłał patrole mające badać zniszczone wraki i dziwne tereny. To sprawia, że na pewno grupa wydzielona Orbitera będzie posiadała **głównego medyka** - osobę odpowiedzialną za _triage_ i udzielanie pomocy tym, którzy ucierpieli na akcji.

Co sprawia, że właśnie ten _główny medyk_ (nazwijmy go Ambroży) został wysłany do tej bazy, na planetę? Co sprawia, że właśnie on się do tego najlepiej nadaje?

![Medyk, ChatGpt](Materials/230905/05-230905-medyk.png)

Wybierzmy z tego dwa i zróbmy je ciekawymi, a potem dodajmy trzeci. Nasz medyk nazywa się teraz **Ambroży Dalrik**

* gdy był na poprzedniej jednostce (OO Castigator), zmuszał ludzi do ostrych ćwiczeń fizycznych twierdząc, że jeśli kiedyś trafią na miejsce o wyższej grawitacji to będą mu wdzięczni. Nazywany "Spoconym Ambrożym" za jego plecami.
* zawsze interesował się dziwną roślinnością; ma wiele atlasów roślin egzotycznych i regularnie kolekcjonuje suszki. Na Castigatorze przekonał kapitana, by wydzielono dla niego małą eksperymentalną szklarnię.

Te dwa powyżej pokazują nam postać, która:

* uważa, że ruch to zdrowie i bardzo mocno naciska na to, by wszyscy ruszali się jak najwięcej. Taki [kaowiec sportowy](https://pl.wikipedia.org/wiki/Kaowiec).
* niespecjalnie zależy mu, by go kochano; przydomek "Spocony Ambroży", zmuszanie ludzi do ćwiczeń i przekonanie kapitana do jego własnej szklarni pokazuje, że raczej twardo naciska na to, by dostać czego chce.
* ma podejście eksploratora, szukającego różnych rzeczy. Kolekcjoner suszek, łatwo się wkraść w jego łaski nietypową rośliną.

Dodajmy do tego coś na linii społecznej, niech komuś podpadnie. Wzmocnijmy i wyostrzmy jego charakter. Na przykład:

* gdy spotkał się z narkotykami na Castigatorze, nikomu się nie kłaniał - tylko zgłosił i zaeskalował tak wysoko jak mógł, mimo, że dotyczyło to "dzieciaków z dobrego domu". Mimo, że przez to poważnie ucierpiała jego kariera.

Mamy teraz medyka, który jest dość uparty, który uważa, że "wie lepiej", jest fanem sportu i kultury zdrowia, nie do końca przejmuje się co o nim myślą. A wszystko to z trzech Dokonań i jednego Zawodu:

* **Ambroży Dalrik**: główny medyk ekspedycji Lohalian
    * Dokonania
        * gdy był na poprzedniej jednostce (OO Castigator), zmuszał ludzi do ostrych ćwiczeń fizycznych twierdząc, że jeśli kiedyś trafią na miejsce o wyższej grawitacji to będą mu wdzięczni. Nazywany "Spoconym Ambrożym" za jego plecami.
        * zawsze interesował się dziwną roślinnością; ma wiele atlasów roślin egzotycznych i regularnie kolekcjonuje suszki. Na Castigatorze przekonał kapitana, by wydzielono dla niego małą eksperymentalną szklarnię.
        * gdy spotkał się z narkotykami na Castigatorze, nikomu się nie kłaniał - tylko zgłosił i zaeskalował tak wysoko jak mógł, mimo, że dotyczyło to "dzieciaków z dobrego domu". Mimo, że przez to poważnie ucierpiała jego kariera.

Z tego już można wyprowadzić resztę karty postaci jakbyśmy chcieli.

A jakie konflikty są powiązane z CMO Dalrikiem?

* dziwna roślina, którą Ambroży chce pozyskać
* Ambroży namawia do ruchu fizycznego. Żołnierze mają niskie morale. Oni nie chcą ruchu fizycznego.
* żołnierze zachowują się niegodnie. Ambroży żąda, by zachowywali się godnie **albo** zaeskaluje wyżej.
* ktoś robi Ambrożemu na złość, bo Ambroży zalazł mu za odcisk

---
#### 4.5.4. Osobowość i Wartości

W tych górach - nie wiadomo dlaczego (_tzn. ja wiem, ale nie napiszę w tej notatce bo ją Kić może przeczyta_) - nie do końca dobrze działa sprzęt elektroniczny. Fabrykatory produkują rzeczy nie do końca dobrze, syntezatory jedzenia robią coś... o ograniczonej jadalności i są ciągłe problemy z agregatami prądotwórczymi.

Fabularnie - **jaką osobą** powinien być główny inżynier, by wygenerować najlepsze wątki fabularne i konflikty fabularne? Nasz inżynier jest bardzo kompetentną osobą, a - pozornie z jego winy - wszystko nie działa jak powinno.

![Inżynier, ChatGpt](Materials/230905/06-230905-inzynier.png)

Wariant trzeci, "Marek Wojda" ma największy potencjał. Przerobię go po mojemu i wyjaśnię:

* Marek Korniczew: główny inżynier z ramienia grupy wydzielonej Lohalian, Orbiter
    * OCEAN: (A+ O-): teamplayer, przyjazny i ogólnie lubiany, działa zgodnie z rutyną i wzorami. "Na wszystko są właściwe procedury i wspólnie rozwiążemy każdy problem"
    * VALS: (Achievement, Tradition): przywykł, że zna rozwiązanie każdego problemu i wykorzystuje przetestowane w boju metody. Problemy traktuje bardzo osobiście. "To od inżyniera zależy, w jakim stanie jest jednostka."
    * Styl: Planuje działania uwzględniając innych członków zespołu; nieodłącznie ze swoim kajecikiem. Uśmiechnięty i sympatyczny, "dobry duch" zespołu. Bardzo osobiście traktuje niepowodzenia techniczne.

Oki. WIEMY, że sprzęt nie działa z różnych powodów. Więc dodanie inżyniera, który:

* bardzo chce, by wszyscy byli zadowoleni i jest ogólnie lubiany
* jest niezwykle dumny ze swoich kompetencji
* wierzy w procedury i w podejście typowe

i skonfrontowanie go z sytuacją bardzo nietypową, na którą nie ma procedur powoduje u niego duży dysonans i buduje odpowiednio dobry potencjał konfliktowy.

Marek będzie się upierał, że on **już prawie to rozwiązał** (duma, przywiązanie do swojego mistrzostwa). Jednocześnie jest coraz bardziej zestresowany. Nikt nie chce, by on cierpiał, ale jak mu pomóc nie depcząc jego poczucia własnej wartości ;-)?

Jak zwykle, optymalizuję pod kątem maksymalizacji fabularnej.

---
## 5. Specjalne metody konstrukcji postaci (też wychodzą od celu MG, ale niekoniecznie od roli)

Tu nie będę się dokładnie rozpisywał, bo notatka i tak jest długa.

---
### 5.1. Dark Mirror

Budowa postaci by pokazać alternatywną drogę postaci. Świetna technika do konstrukcji "głównego przeciwnika".

* [Heart of Ice, analiza, Literary Devil (na YT)](https://www.youtube.com/watch?v=2p0CcH6dnjQ)
    * Core Wound:
        * Batman (**Traumatic Loss** (of parents)) -> walka z przestępcami kosztem "normalnego" życia
        * Mr Freeze (**Traumatic Loss** (of wife)) -> przetrwać za wszelką cenę, pomścić swoją żonę
    * Corrupted Virtue:
        * Mr Freeze: 
            * Miłość i Lojalność -> Okrutna Zemsta
            * Ratować i podtrzymywać życie -> niszczyć życia innych
    * Human Vulnerability

Jak widać, oni są podobni. Bardzo podobni. Ale w chwili Core Wound - tego, co zdefiniowało drogę postaci - poszli innymi drogami. Batman nie poszedł drogą zemsty a pomocy innym. Victor Fries, cóż...

---
### 5.2. Extremist Mirror

Budowa postaci na podstawie innych postaci, pokazanie, że są "tacy sami", ale jeden posunął się dalej od drugiego. Pokazane by zaszokować gracza - czy na pewno chce iść swoją postacią dalej tą drogą?
