---
layout: mechanika
title: "Notatka mechaniki, 230530 - Analiza kart postaci"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw

## 1. Purpose

Wszystkie postacie są potencjalnie podobne do siebie, a niekoniecznie tego szukamy.
Szukamy nisz i specjalizacji, ale szukamy też postaci które są po prostu silne i przydatne.

## 2. High-level overview

.

## 3. Analiza
### 3.1. Czy da się zrobić sesję bez kart postaci?

Tak, ale to jest trudniejsze. ZAŁOŻENIE: wszyscy tak samo rozumiemy świat (else: to dużo bardziej trudniejsze).

* "Kompetentna postać" - nie wiemy co robi, ale się przyda na sesji. Nie wiemy jakie ma zasoby, kogo zna, jaką ma agendę itp.
    * --> Za mało.
* "Nekromanta" 0 reanimuje nieumarłych. Grasz nekromantą - wiesz, że reanimujesz nieumarłych. **To jest coś co Ty i tylko Ty możesz zrobić w zespole**.
    * --> **Pierwszy poziom**: rola. Jeśli nie znasz roli i nie wiesz co znaczy, masz przechlapane, bo nie wiesz co się da i czego się nie da
* "Miles O'Brien, inżynier Deep Space Nine" ( https://memory-alpha.fandom.com/wiki/Miles_O%27Brien ) niesie zbiór informacji:
    * Irlandczyk, wychowany na Ziemi i jedzący prawdziwe jedzenie; nie dogaduje się z ojcem (wiolonczela vs starfleet)
    * Służył na ponad 6 statkach kosmicznych, ekspert od walki kosmicznej, główny inżynier
    * Nie cierpi Cardassian; jego skille z transporterem uratowały wielu ludzi uciekających przed nimi, ewakuował ludzi z masakry Sheik III
    * ...
    * --> **Drugi poziom**: zbiór PRZESZŁYCH DOKONAŃ / historia. Nie zawsze wiemy co dokładnie O'Brien robił czy co umie (czy co Batman robił czy co umie), ale przeszłe czyny które widzieliśmy determinują poszczególne możliwości co zrobi w przyszłości.
* "Mroczna postać w karczmie" - jestem mroczny. Siedzę. Cierpię. Jestem złośliwy i mroczny. Nie będę współpracował z zespołem, bo jestem mroczny.
    * --> **Trzeci poziom**: agenda. Każdy czegoś chce, inaczej czemu nie ma siedzieć i oglądać telewizora cały dzień?

### 3.2. Po co nam karty postaci?

Podstawowo:

1. Agenda: Co interesuje postać i jak ją włączyć w sesję?
    * --> MG: co zrobić by postać zadziałała 
    * --> Gracz: co ma zrobić w sytuacji w której nie wie co robić
2. Obietnica: Co postać obiecuje Graczowi - kiedy będzie świetna, czego Gracz może oczekiwać?
    * --> MG: jakiego typu wyzwania sprawią że ten i tylko ten Gracz będzie przydatny
    * --> Gracz: co będzie postać robiła epicko by się świetnie bawił
3. Słabości:
    * --> MG: jak uatrakcyjnić sesję, gdzie nacisnąć by dać ruch innej postaci / _compelować_ tą?
    * --> Gracz: jak pokazać słabsze strony własnej postaci, budować konflikt wewnętrzny
4. Sterowanie: Jak kierować postacią, jak się ma zachowywać, jak zachować jej spójność?
    * --> MG, Gracz: dla sytuacji X co zrobi postać? Jak się zachowa? Jakie ma quirki? Jak mówi? Na co reaguje?
5. Zasoby: Do czego postać ma dostęp, kogo zna itp.
    * --> MG: dodatkowe klocki świata którymi może wpleść postać w rzeczywistość
    * --> Gracz: co może powołać jeśli jest to potrzebne? Czego może użyć? Co wypracował?
6. UI Gracza
    * --> MG: jak dostosować sesję pod tą konkretną postać, co Gracz widzi? Czy Gracz poradzi sobie z TYM wyzwaniem?
    * --> Gracz: jakiego typu akcje może zrobić Gracz, czego oczekiwać od MG?
7. Historia
    * --> MG: co postać zrobiła w przeszłości (czyli najpewniej zrobi w przyszłości)? Umocowanie postaci w świecie, czyli klocki
    * --> Gracz: co postać zrobiła w przeszłości i jest w tym dobra? Z jakimi klockami wchodziła w interakcję?
8. Umocowanie w świecie
    * --> MG, Gracz: dany zbiór klocków MG / świata który interesuje obie strony
9. Opis: Wizualnie / quirki charakteru, ale potencjalnie też rzeczy których nie widać, silne strony itp?
    * --> MG, Gracz: ???

### 3.3. Czy "karta postaci" to to samo dla NPC / gracza?

* TAK - oczekujemy tego samego **zachowania** i **stylu** od tej samej postaci
    * nieważne, kto gra Batmanem, powinien zachowywać się jak Batman
    * Batman ma zawsze _zbliżony_ zestaw umiejętności i sposobów działania
    * "Batman, który nie poświęci chwili umierającej nastolatce by ją uspokoić i pocieszyć to jest Punisher w przebraniu"
        * --> charakter, zachowanie, podejście są dość niezmienne
        * --> dobra postać jest **w pewnym stopniu przewidywalna / ekstrapolowalna** w tym jak się zachowa
* NIE: Z perspektywy UI (interfejsu użytkownika) 
    * od postaci Gracza oczekujemy "jakie czynności zapewnia graczowi"
    * od NPCa oczekujemy "jakie przeszkody stanowi na sesji i jaki ma styl"
    * TAK: w obu wypadkach mamy "jaką agendę ma ten Aktor w tej Opowieści"
        * --> jak nim kierować

## 4. Historia kart postaci i mechanizmów kontrolnych postaci

### 4.1. REFERENCJA: 081212?, Mltv

Dominanta: karta postaci, jedna karta == jedna postać, najlepiej wyewoluowane z typowego fantasy co mam. Rok 2008.

![Multiversum, Ariel Gryphonthorn](Materials/230530/230530-mltv.png)

* Karta na pierwszy rzut oka jest mało czytelna
* Jeśli nie wiesz kim jest "kapłan Crissara" lub "kapłan Arazille" i czemu ten zrost jest dziwny, nie wiesz jak nim sterować
* Na pierwszy rzut oka jest to "konny sokolnik co dobrze gada" - widać jakieś siły
* Jego sprzęt nie ma znaczenia, dużo jest ukryte pod aspektami
* Historia pojawia się w aspektach społecznych

To nasz baseline. Niżej nie powinniśmy zejść. Nadal - na podstawie tej karty postaci nie poprowadzisz jej tak jak ja.

Od teraz dodaję nowy wymiar - **szybkość i łatwość użycia**.

### 4.2. REFERENCJA: 1308, Magic:Inwazja

Dominanta: karta postaci, mechanizm aspektowy (tagi), obecność ElMetu (dynamiczne składanie zaklęć po macierzy), magia w systemie pełni najważniejszą rolę. Rok 2013.

![Inwazja, Andrea](Materials/230530/230530-130908-andrea.png)

* Po raz pierwszy pojawia się AGENDA - Andrea czegoś chce (przywrócenie SŚ). Pojawiają się elemeny STEROWANIA - po prawej stronie Andrea ma informacje jak to robić.
    * Nagle MG wie jakie sesje robić dla Andrei i jakie ruchy Andrea chce zrobić. Tak samo graczka Andrei wie jak podejść do tematu i kiedy się "odpalić". Pojawia się drama między postaciami przez konflikt Agend.
* Najważniejsza część systemu - magia - jest łatwa i czytelna w użyciu. Andrea jest omnidetektorem ("Intelligo EVERYTHING"), umie tworzyć rzeczy związane z energią i przekształcać materię, życie i energię magiczną. Typowy czar? `Intelligo Vim Muto Vim Creo Igni` - "rzucam klątwę która gdy druga strona próbuje czarować, podpala się"
* Mamy dynamiczne postacie, aspekty mówią co da się zrobić i jakie to silne i skuteczne.
* Mamy po raz pierwszy historię; to jeszcze przed RdButlerem.
* Jest to pierwsza "karta jaka działa" z perspektywy celów jakie mamy. Wszystko potem to krążenie dookoła.

### 4.3. Pierwsze karty EZ (1807)

Dominanta: EZ nie jest jeszcze stabilnym systemem, postacie silnie pokazują czym ten system może być. Pierwsze sesje w EZ.

![EZ, Artur, 1807](Materials/230530/230530-1807-artur.png)

* Pojawia się KONCEPT postaci - krótki opis czym jest i czemu istnieje, takie TL;DR
* OTOCZENIE to environment, czyli gdzie spotkamy i do czego służy. Potem to zaniknęło, aż Darken udowodnił mi miesiąc temu, że to mega przydatne
    * -> spotkasz go TAM i robi TO. Element obietnicy i przewidywalności. 
        * Batmana nie spotkasz w kutrze rybackim łowiącym ryby (przewidywalność), a Conan Barbarzyńca był nawet królem (nieprzewidywalność)
* WADA to pierwsze wystąpienie Słabości.
* MISJA i MOTYWACJE to Agenda i elementy Sterowania
* ZACHOWANIA to Sterowanie, po raz pierwszy widać jak się postać zachowuje a nie tylko co robi
* UMIEJĘTNOŚCI to niekompletna Obietnica. Obietnicę zatraciliśmy, bo system się zdynamizował. Tu jest regres
* Zasoby też ulegają regresowi, acz Umocowanie przez konieczność kwiecistego opisu - wzrosło
* Historia jest realizowana przez Dokonania (RdButler); nie ma opcji pokazania czym postać jest bez tego. Więc - baseline 3.
* UI gracza się bardzo utrudniło, jak i łatwość użycia karty. Nie ma nic przyspieszającego. Masz tylko możliwość nauczyć się postaci. Fox to zgłaszała kiedyś iirc.

### 4.4. Apex EZ - Alan Bartozol (1904)

Dominanta: najlepsza karta EZ jaka bardzo długo działała jako najlepsza. A potem pojawiły się spotkania RPG środowe ;-).

![EZ, Alan, 1904](Materials/230530/230530-1904-alan.png)

* Bardzo silny nacisk na zachowanie (Agenda, Sterowanie) przez podziału Agendy na "co chcę dla mnie" i "co chcę dla innych"
* Ogólny Pomysł zastąpił Koncept - tylko to trzeba przeczytać by zrozumieć postać pobieżnie
* Zanika Obietnica; zamiast tego masz Wyróżnik który daje Ci przewagi i siły.
* Osłabione Sterowanie; sporo tego trafia do Wyróżnika
* Co ważne, zaniknęły Słabości!
* Wzmocnione Umocowanie przez skupienie się jak postać działa w świecie i nacisk na Zasoby
* --> ogólnie? Jeśli umiesz ją dobrze wypełnić i znasz świat, ta karta da Ci wszystko czego potrzebujesz
    * --> WIĘC UI spada oraz prędkość użycia trochę rośnie.

* Agenda, Obietnica, Słabości, Sterowanie, Zasoby, UI Gracza, Historia, Umocowanie, Opis

### 4.5. Fiszki nandeckowe (1904)

Kontekst: Nadeszła tragedia. Sesje z ludźmi nie znającymi systemu płynnie, na spotkaniach RPG ;-). Niemożliwe jest, by mogli zacząć czytać wszystkie karty postaci czy rozumieć czym jest destabilizacja _vitium_. Poprzednia strategia okazała się być suboptymalna. Rozwiązanie - ROLE a nie POSTACIE. Fiszki które możesz wydrukować i przynieść.

Każda z tych fiszek jest w pdfie, wielkości karty do gry, którą bierzesz i masz. Tu jest substrat.

![EZ, Fiszka, 1904](Materials/230530/230530-1904-fiszka-1.png)

* BARDZO duży nacisk na minimalną ilość tekstu, by się zmieściło na jednej karcie do gry
* BARDZO duży nacisk na "obietnicę" / "wyróżnik". Niestety, nie zadziałało jak chciałem.
* BARDZO duży nacisk na Motywację. Niestety, nie zadziałało jak chciałem.
* Czemu? Inni nie wizualizowali konceptów tak jak ja je widziałem, co powodowało kolizję zrozumienia.

### 4.6. Fiszki nandeckowe, APEX (190414)

Kontekst: dopracowane fiszki doszły do poziomu APEX. To działało najlepiej i najskuteczniej. Zmiana opisu i zmiana nacisku sprawiło, że gracze POTRAFILI grać tymi fiszkami, acz nieidealnie.

![EZ, Fiszka, 190414](Materials/230530/230530-1904-fiszka-2.png)

* "Ogólny pomysł" daje wizję postaci - o co chodzi
* "Czego pragnie a nie ma" daje jednoznaczny sygnał - tu chcesz iść
* "Sposób działania" - specjalne ruchy, obietnica mocy, elementy sterowania
* "Zasoby" łączyły postać ze światem i integrowały elementy historii
* "Przykłady" powinien pisać ktoś o tym samym kodzie kulturowym co target ;-).
* KATASTROFALNĄ wadą jest to, że bardzo trudno ZBUDOWAĆ taką rolę. Bo to nie jest postać. To rola.

### 4.7. Fiszki nandeckowe, rozdzielenie (1907)

Kontekst: Nie każdy naukowiec jest badaczem otchłani. Nie każdy inżynier pragnie rozwoju nowych technologii. Okazało się, że ta sama ROLA może potrzebować innej MOTYWACJI per sesja. Np. inżynier na statku pirackim może chcieć "wyjść z długów" czy "zdobyć niewolnika" a inżynier na statku Orbitera może chcieć "rozwalić piratów". Innymi słowy - jeśli chcemy sprawne postacie, ROLA musi być rozdzielona z MOTYWACJĄ.

![EZ, Split Fiszek](Materials/230530/230530-1904-split-fiszek.png)

* Zdecydowanie podniosła się dowolność i łatwość konstrukcji postaci
* Spadła prawidłowa używalność, za dużo o czym muszą myśleć gracze i się gubią
    * To jednak musi być jedna "kartka"

### 4.8. Fiszki nandeckowe, ostatni eksperyment (1911, 2004)

Kontekst: Po tym jak się okazało, że rozdzielenie nie jest optymalnym rozwiązaniem, kombinowałem z innymi wariantami. Poniżej - ostatnie rzeczy które z perspektywy fiszek nandeckowych działały. Jest tu kilka wartościowych innowacji, które wyjaśnię.

![EZ, Nandeck final](Materials/230530/230530-2004-nandeck-final.png)

* Pojawiły się znowu SŁABOŚCI i negacje charakteru
* Z uwagi na "wszechstronność" sesji spada dopasowanie do świata (umocowanie)
* Pojawiła się historia i Dokonania
* Zasoby PRÓBUJĄ umocować, ale wychodzi nie najlepiej.
* Pojawiają się "przykładowe działania", czyli Ruchy (-> Darken).

Czy to działało? Tak, zdecydowanie. Wyprodukowanie puli sensownych fiszek i dostarczenie ich do systemu / sesji byłoby czymś bardzo przydatnym. Uważam, że to jako "podstawowa karta postaci" jest wystarczająco dobre.

Oczywiście, historia się na tym nie zatrzymała. Brak nam powtarzalności zachowań postaci i siły umocowania. Więc...

### 4.8. Karta postaci wyhodowana z fiszki (2004)

Kontekst: szukam powtarzalności postaci NAWET kosztem prędkości.

![EZ, Sabina Kazitan, 2004](Materials/230530/230530-2004-sabina.png)

* Zapłaciłem prędkością i łatwością użycia, ale uzyskałem lepszy opis i kontekst postaci.
* Wszystkie lekcje z fiszek zostały niezmienione
* Zauważcie - KAŻDY potrafi sterować Sabiną mniej więcej tak jak ja.
* Spadł nacisk na jej wyróżniki i atuty - są 2 siły i 2 słabości, ale charakter wszedł do mechaniki.

### 4.9. Dwa lata później, powrót do karty postaci (2206)

Kontekst: gdy zacząłem integrować OCEAN w kartę postaci, sformowała się nietrywialna ale perfekcyjnie zrozumiała struktura. Jeśli znasz teorię OCEAN ofc.

![EZ, Martyn Hiwasser, 2206](Materials/230530/230530-2206-martyn.png)

* Po raz pierwszy NAPRAWDĘ wiem jak sterować postacią.
    * Ale: zatraciłem część Agendy, zatraciłem prędkość i łatwość użycia i UI
* To by było GENIALNE do tworzenia opowiadań, ale zawodzi przy operacjach "szybka postać na sesję"

### 4.10. Autogenerator i fiszki postaci dynamicznych do sesji (2301)

Kontekst: jak odróżnić postacie PODCZAS sesji? Jak sprawić, by dwóch żołnierzy było różnych? Jak dać Kić postać wygenerowaną w 10 minut jako tempkę?

Dodanie OCEAN (Openness, Conscientousness, Extraversion, Agreeableness, Neuroticism) + koła wartości Schwartza + kilku innych rzeczy do generatora losowego otworzyło mi błyskawiczne narzędzie zróżnorodniania NPC. I budowy na szybko kart postaci. Przy założeniu, że KTOKOLWIEK rozumie koncepty których tu używam.

![EZ, Fiszka intersesyjna, 2301](Materials/230530/230530-2301-fiszka-generatora.png)

* Jedyny priorytet - różnorodność i opis w minimalnej ilości znaków (jedna linijka)
* Założenie - wszystko wiesz i jak czegoś nie wiesz, odniesiesz się do tego z dokumentacji
* Narzędzie dla ultra-weteranów (ja, Kić) a nie dla graczy przelotnich
* Działa **REWELACYJNIE** jeśli się w to wdrożysz. Apex.

### 4.11. Eksperymenty z integracją aspektów do kart postaci (2305)

Innymi słowy, dalsze działania i eksperymenty. Poniżej nie jest to coś co BĘDZIE, ale pokazuje wektory myślenia.

![EZ, Helmut Szczypacz, 2305](Materials/230530/230530-2305-helmut.png)

## 5. Co nam jest potrzebne

Upraszczając, to co napisałem wcześniej. Dodaję NEGACJĘ - co jeśli tego nie mamy:

1. Agenda: Co interesuje postać i jak ją włączyć w sesję?
    * --> MG: co zrobić by postać zadziałała 
    * --> Gracz: co ma zrobić w sytuacji w której nie wie co robić
    * NEGACJA: nie ma połączenia postaci z sesją. Gracz nie ma motywacji do przesuwania postaci. Cele Gracza / postaci nie idą do przodu.
2. Obietnica: Co postać obiecuje Graczowi - kiedy będzie świetna, czego Gracz może oczekiwać?
    * --> MG: jakiego typu wyzwania sprawią że ten i tylko ten Gracz będzie przydatny
    * --> Gracz: co będzie postać robiła epicko by się świetnie bawił
    * NEGACJA: Nie wiemy czym gramy i kiedy jest "nasz ruch by błyszczeć"
3. Zasoby: Do czego postać ma dostęp, kogo zna itp.
    * --> MG: dodatkowe klocki świata którymi może wpleść postać w rzeczywistość
    * --> Gracz: co może powołać jeśli jest to potrzebne? Czego może użyć? Co wypracował?
    * NEGACJA: mniejsze umocowanie, Gracz nie czuje się "częścią" czegoś, mniej możliwości
4. Słabości:
    * --> MG: jak uatrakcyjnić sesję, gdzie nacisnąć by dać ruch innej postaci / _compelować_ tą?
    * --> Gracz: jak pokazać słabsze strony własnej postaci, budować konflikt wewnętrzny
    * NEGACJA: Nie mamy wbudowanych konfliktów w postać. Nie ma możliwości uderzenia w jej słaby punkt. Redukcja dramy i taktyki.
5. Sterowanie: Jak kierować postacią, jak się ma zachowywać, jak zachować jej spójność?
    * --> MG, Gracz: dla sytuacji X co zrobi postać? Jak się zachowa? Jakie ma quirki? Jak mówi? Na co reaguje?
    * NEGACJA: Nie mamy spójności, Batman raz jest sobą a raz kandyduje na burmistrza. Mniej ciekawych opowieści.
6. UI Gracza
    * --> MG: jak dostosować sesję pod tą konkretną postać, co Gracz widzi? Czy Gracz poradzi sobie z TYM 
    wyzwaniem?
    * --> Gracz: jakiego typu akcje może zrobić Gracz, czego oczekiwać od MG?
    * NEGACJA: rozpad komunikacji Gracz - MG, nie wiadomo co się dzieje i czemu Gracz nic nie robi / robi głupie ruchy, Gracz nie wie CO robić.
        * to jest ciekawe, że Apocalypse World ma wbudowane dobre ruchy jako checklista (niekoniecznie jako perfekcyjne narzędzie do gry)
7. Historia
    * --> MG: co postać zrobiła w przeszłości (czyli najpewniej zrobi w przyszłości)? Umocowanie postaci w 
    świecie, czyli klocki
    * --> Gracz: co postać zrobiła w przeszłości i jest w tym dobra? Z jakimi klockami wchodziła w interakcję?
    * NEGACJA: postać nie jest "prawdziwa"
8. Umocowanie w świecie
    * --> MG, Gracz: dany zbiór klocków MG / świata który interesuje obie strony
    * NEGACJA: postać nie jest podmontowana do świata, nie zależy jej, nie ma wpływania, nie wie co i po co
9. Opis: Wizualnie / quirki charakteru, ale potencjalnie też rzeczy których nie widać, silne strony itp?
    * --> MG, Gracz: ???
    * NEGACJA: ?
10. Łatwość użycia
    * --> MG: szybka budowa postaci na sesję
    * --> Gracz: budowa swojej postaci tak jak chce
    * NEGACJA: nie będziemy tego używać, nieważne jak dobre by nie było. Nie przez przypadek używamy fiszek generatora - to jest proste.

## 6. Co dalej?

Konieczność zrobienia sprawnej karty postaci, naturalnie. Potencjalnie mamy CZTERY formaty:

1. karta postaci dla Gracza
    * ?
2. karta postaci NPC
    * powtarzalna, zrozumiała
    * bardzo dobrze pokazująca jak ma działać
    * "Żeby Martyn był Martynem a Elena Eleną"
3. karta fiszkowca NPC
    * zrozumiała, minimalna
    * daje agendę, rolę na sesji, zróżnodnienie itp.
    * generator ma je produkować jak szalony ;-)
4. karta fiszki postaci Gracza
    * zrozumiała, minimalna
    * daje agendę, rolę na sesji, konkretne ruchy, konkretne UI

