# Analiza

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

* Jak zacząć sesję?

---
## 2. Spis treści i omówienie

1. Krótka myśl

---
## 3. Krótka myśl o początku sesji

Początek sesji Rpg działa zupełnie inaczej niż każda inna część tej sesji. Każda sesja idzie według pewnego cyklu.

* Na początku gracze niczego nie wiedzą, nie znają kontekstu, Nie wiedzą, co się dzieje i co mogą zrobić. 
* Pod koniec sesji gracze nie tylko wiedzą już co można zrobić, ale tak samo podjęli pewne decyzje, by rozwiązać problem, który napotkali.

To sprawia, że początek każdej sesji zawiera elementy railroadingu. Ja to zwykle robię przez scenę tutorialową - tak zwaną scenę Zero. Gracze nie tylko muszą być w stanie poznać mechanikę, ale tak samo muszą zrobić to w warunkach bezpiecznych, w których naprawdę nic nie mogą zepsuć. Oprócz tego gracze nie wiedzą, z jakiego typu zagrożeniem mają do czynienia, czy co mogą zrobić.

Więc każda sesja idzie według pewnego cyklu - wpierw zaczyna się railroadingiem, do którego należy też otwierająca scena „serialu”, potem kończy się konkretnym wyborem.

Stąd, jeżeli na przykład próbujemy zrobić coś takiego - postać gracza gra wyrzutkiem w zaginionej krainie i MG próbuje dodać jakiś ludzki element do tego wyrzutka to zakończy się katastrofalnie na tym etapie. MG może po prostu powiedzieć „opowiedz mi o tym wyrzutku”. Ale na tym etapie gracz nie ma pojęcia ani o świecie ani o swojej postaci. Nie powie niczego o swoim wyrzutku, bo za mało wie.

Co innego, jeżeli dodamy to później- jeżeli na przestrzeni sesji MG w którymś momencie zapyta „powiedz mi coś o wyrzutku”, to gracz może już być w stanie coś dodać.

To sprawia, że z mojej perspektywy najlepiej dać graczowi istniejącą kartę postaci i powiedzieć coś w stylu „ zmień ją pod siebie i zmień rzeczy, które ci nie pasują”. Bo na początku gracz nie jest w stanie niczego dodać, więc obowiązek dostarczenia dobrej i sprawnej postaci na sesję graczowi leży na mistrzu gry.
 
