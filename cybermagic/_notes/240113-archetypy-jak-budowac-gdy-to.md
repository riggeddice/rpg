# Analiza: Czego potrzebują wszystkie elementy sesji (230925)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Jak budować GDY-TO?
* Jak budować archetypy kart postaci dla Graczy na jednostrzały?

---
## 2. Spis treści i omówienie

1. Co przeczytać wcześniej?
2. Struktura GDY - TO?
    1. Po co to jest?
    2. Przykłady kiedy to nie działa
        1. Niszowa / trudna do użycia sytuacja GDY
        2. TO nie naprawia sytuacji / jest bardzo niszowe
    3. Czyli jak to robić by to działało?
3. Strategia uzupełniająca - affinity energii do magii
    1. Jak rozwiązałem magię - przykład
    2. Czemu magia jest zaprojektowana inaczej?

---
## 3. Co przeczytać wcześniej?

* Oki - tu są cele karty postaci - po co to jest: [230531-analiza-karty-postaci-cele](230531-analiza-karty-postaci-cele) . Zwłaszcza pkt 3.2.
* A tu jest aktualna 'wersja' karty postaci z wyjaśnieniami: [230601-karta-postaci-koncept](230601-karta-postaci-koncept) .
* A tu jest przykład KARTY POSTACI. Nie archetypu a karty postaci: [Vanessa Lemurczak](../aktorzy/2306-vanessa-lemurczak.md) .

Wyjaśniając wszystko dalej zakładam, że to co jest powyżej jest Ci znane.

---
## 4. Struktura GDY - TO?
### 4.1. Po co to jest?

Zacznijmy od samego początku - czemu w ogóle robimy kartę postaci? Czemu w ogóle robimy archetyp dla postaci? Żeby pomóc graczowi.

Każda linijka tekstu na karcie postaci, która nie pomaga graczowi jest szkodliwa. Nie jest ‘nieprzydatna’. Jest ‘szkodliwa’.

Długo pracując nad rzeczami, które zrozumie random grający pierwszy raz w życiu w EZ - system twardego scifi z magią w kosmosie - ustabilizowałem strukturę na gdy-to.

Czemu tak?

Jeżeli masz w grze komputerowej złoty klucz, to gdzieś są złote drzwi które są otwierane. Jeżeli znajdujesz złote drzwi gdzieś jest złoty klucz, który otworzy (dzięki, Izaro <3). Jeżeli ja jako twórca systemu dostarczam jednoznaczne „masz X które możesz wykorzystać, gdy Y”, to daje graczowi odpowiednik tego złotego klucza. I gracz patrzący na złote drzwi i mający na karcie postaci złoty klucz wie, że to jego chwila na działanie.

Co też prowadzi do bardzo, bardzo ważnej obserwacji - każda karta postaci musi mieć co najmniej jedno GDY-TO które jest tak bezdennie oczywiste do wykorzystania, że trzynastolatek biegający z laserami w głowie potrafi wykryć tą sytuację podczas gry. Jeżeli archetyp nie posiada jednej takiej sytuacji, jest archetypem błędnym.

### 4.2. Przykłady kiedy to nie działa
#### 4.2.1. Niszowa / trudna do użycia sytuacja GDY

Przykłady:

1. "GDY jest pora płacenia podatków, TO przejmujesz kontrolę nad firmą"
2. "GDY odbywa się koncert, TO przyciągasz uwagę do siebie"
3. "GDY jest cicho, TO potrafisz się przemieścić w dowolne miejsce"

O co chodzi:

* W pierwszym przypadku zawęziliśmy GDY tylko do płacenia podatków. Co z tego, że delikwent jest w stanie przejąć kontrolę nad firmą, jeżeli do tego celu musi sprawić, by w tym momencie były prowadzone podatki. Czyli musi stworzyć sytuację, w której natychmiast należy zapłacić podatki. Żaden normalny gracz nie wpadnie, jak to zrobić. Szukasz osoby doświadczonej.
* To samo jest w drugiej sytuacji – „gdy odbywa się koncert”. Sytuacja z koncertem ma sens, gdy na sesji jest koncert. Jeżeli nie ma miejsca na koncert na sesji, ta linijka tekstu jest martwa.
* Trzecia jest dużo bardziej podstępna, ale tak samo fatalna. „Gdy jest cicho”. Co to znaczy? Ta sytuacja jest za szeroka z perspektywy gracza i albo jest zbyt częsta (czyli przepakowana) albo zbyt rzadka. 

Odpowiedzialnością designera który tworzy sesję jest to by dostarczyć takie GDY-TO archetypom, żeby **niezależnie od ścieżki jakiej gracze nie wybrali każda postać mogła zrobić co najmniej jedną wartościową rzecz**. Mówimy o graczu typu ‘nieśmiały trzynastolatek’. 

To sprawia, że komponent ‘GDY’ musi być oczywisty dla 13-latka. Nie wszystkie - ale co najmniej jeden per archetyp.

#### 4.2.2. TO nie naprawia sytuacji / jest bardzo niszowe

Przykłady:

1. "GDY trzeba działać szybko, TO najszybciej konstruuje miksery"
2. "GDY trzeba działać szybko, TO zagrzewa wszystkich do działania"
3. "GDY trzeba działać szybko, TO zawsze znajdzie potrzebne narzędzie"

O co chodzi:

GDY działa tutaj wystarczająco prawidłowo; postać prawidłowo funkcjonuje, gdy należy się spieszyć. Spójrzmy jednak na komponent TO:

* W pierwszym przypadku mamy sytuację skrajnie niszową. Jak często postać będzie skonstruowała miksery na sesji? To jest bezużyteczne. Celem jest to, by gracz od razu wiedział, jak wykorzystać umiejętności swojej postaci do rozwiązania konkretnie tego problemu, z którym ma do czynienia. Bardzo rzadko naprawa mikserów będzie tym rozwiązaniem.
* W drugim wypadku da się tego już użyć – ‘zagrzewa wszystkich do działania’. Ale nie widać wyraźnie jak to przekłada się na sukces postaci na sesji. Samo zagrzewanie nic nie robi; co innego, jeżeli byłoby coś w stylu ‘sprawia że wszyscy inni pracują szybko’ albo ‘usuwa wszelkie konflikty międzyludzkie’. To drugie nadal jest niszowe, ale jest wykorzystywalne.
* W trzecim wypadku mamy wieloznaczność interpretacji co to jest ‘potrzebne narzędzie’. To jest borderline. Gdybym widział taki element na karcie postaci, uznałbym go za przepakowany, ale potrafiłbym go użyć bez problemu. Trzynastolatek? Wątpię. Czyli to nie może być ten ‘najprostszy przypadek’.

Znowu - odpowiedzialnością designera jest to, by gracz potrafił użyć archetypu z taką łatwością, jakby ‘widzę złote drzwi - użyję złotego klucza’. Jeśli komponent TO jest nieprecyzyjny lub trudny w użyciu, to nie gracz zawinił a designer.

### 4.3. Czyli jak to robić by to działało?

Algorytm budowy archetypu lub selekcji archetypu pod sesję jest wyjątkowo prosty:

1. Zaprojektuj sesję
2. Określ mniej więcej jakiego typu konflikty są na sesji zgodnie ze stosowaną przez ciebie strategią ustawienia konfliktów (u mnie to będą Dark Agenda, Dark Future, Fronty oraz Tory)
3. Na podstawie tego łatwo możesz określić serię różnych GDY
4. Na podstawie twoich przypuszczeń jak da się rozwiązać tą sesję ustal prawidłowe TO
5. Podzieli na pakiety
6. Nazwij pakiety, zrób z nich coś archetypicznego, coś co ludzie zrozumieją
7. Przetestuj sesję - czy może być rozwiązana przez dowolne 2 lub 3 archetypy. Jak nie, dodaj więcej GDY-TO do archetypów
8. Done

I tu wchodzi drugi komponent archetypu, który stosuję - czym jest ten archetyp? Sławny oneliner. 

‘Sabotażysta jest świetny w bieganiu, strzelaniu, wysadzaniu, skradaniu się, kradzieży kieszonkowej.’ 

Nawet tak kiepski opis jak ten powoduje, w jak gracz nie widzi swojego GDY-TO, to może wpaść na pomysł co z tą postacią da się zrobić i co ta postać mu umożliwia. Bo fundamentalnie GDY-TO są tylko wycinkami, przydatnymi przejawami działania postaci. Postać jest szersza niż GDY-TO.

## 5. Strategia alternatywna - affinity energii do magii
### 5.1. Jak rozwiązałem magię - przykład

Władawiec Diakon, bo on jest najlepszym przykładem:

```
* JEDNYM ZDANIEM: 
    * _manipulation of souls_; perfekcyjna mimikra i wejście w umysł drugiej osoby by stopniowo ją zmieniać. Czytanie myśli, obecność w snach i feromony.
* AFFINITY
    * Esuriit, Praecis, elementy Ixion (z Rodu)
        * w jego wypadku Alucis -> Praecis, Esuriit. 
            * Nie chce koić, chce rządzić i mieć świat pod kontrolą
            * Skupia się na pożądaniu i głodzie, na uczuciach ROZPACZLIWEJ POTRZEBY
            * Praecis, Ixion ORAZ Esuriit działają tylko w domenie mentalnej i 'ultrafizycznej' (np. _ars amandi_)
        * Ixion dotyczy jedynie jego własnej bioformy
            * Zmiana i dostosowanie siebie do _ars amandi_, konstrukcja feromonów
            * Zmiana swojego zachowania pod kątem tego co jest potrzebne, perfekcyjny mimik-corruptor
```

Chcę zwrócić tutaj uwagę na dwie rzeczy - nie ma tutaj prostej struktury GDY-TO, za to bardzo wyciągam wiedzę o Energiach i jak te Energie wchodzą w interakcję z charakterem tej konkretnej osoby. Nawet jeśli nie znasz postaci Władawca Diakona, czytając tylko element jego magii potrafisz bardzo dużo o nim powiedzieć.

To jest powiązane z jednym z głównych tenetów magii EZ - magia w EZ jest przedłużeniem woli i umiejętności maga a nie czymś dodatkowym, zewnętrznym.

### 5.2. Czemu magia jest zaprojektowana inaczej?

Magia jest zaprojektowana inaczej, ponieważ nie jest systemem dla początkujących. Nie miejcie złudzeń - początkujący gracze dobrze bawią się z magią, ku mojemu wielkiemu zdziwieniu. Z uwagi na konieczność trzymania się fizyki świata oraz fizyki magii dużo lepiej jest wyjść z istniejących Energii i zasymulować co tu się stanie w wypadku tej postaci niż projektować GDY-TO w izolacji.

Jednocześnie nie wyobrażam sobie stworzenie archetypu magicznego który po prostu dajemy graczowi. To raczej byłaby sytuacja, w której mamy archetyp ("Inżynier") który korzysta z magii i ta postać - ten archetyp - zupełnie nie zobaczy tego komponentu który tu występuje. 
