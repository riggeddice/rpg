# Analiza: Player Divergent Mastery: Core Mechanical Loop, starter (240122)

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

* Pierwsza analiza Skill Expression i Personality Expression
* Gdzie w core mechanical loop jest Skill/Personality Expression i czemu to jest potrzebne?

---
## 2. Spis treści i omówienie

1. O stopniach trudności
    1. Stopnie trudności - dane
    2. Stopnie trudności - analiza
2. Co to jest Player Divergent Mastery
    1. Dane
    2. Analiza
3. Mapowanie na Core Mechanical Loop


---
## 3. O stopniach trudności
### 3.1. Stopnie trudności - dane

Za [Analyzing Skill Expression in Games](https://github.com/chiamp/skill-expression-game-analysis):

**Cytat o poziomie skilli**:

Games that incorporate random chance elements can allow less-skilled players to occasionally beat better-skilled players. There exists an optimal balance between the win rates of the less-skilled players and the better-skilled players, that make a game popular.

As a consequence, games that never allow less-skilled players to beat better-skilled players, would make casual players less inclined to play those games. On the other hand, games that allow less-skilled players to beat better-skilled players too often would give the impression to the players that their choice of actions have little to no effect on the outcome of the game; i.e. the game gives players very little skill expression.

**Single-Player Blackjack example**:

The theoretical optimal win rate is 42.381% and is denoted by the green dotted line. This represents an upper bound as the highest possible win rate a player can achieve given the rules of this game.

The theoretical random win rate is 18.899% and is denoted by the red dotted line. Under the assumption that a no-skill player uses a random strategy, this represents a lower bound as the lowest possible win rate a player can achieve given the rules of this game, if the player has a non-negative amount of skill.


Za [Skill Expression from Crit the Books](https://critthebooks.com/2018/11/22/lesson-9-skill-expression/):

**Skill floor**:

An example of a character with a low skill floor is Lucio in a support role. In a support role, a player is expected to provide healing or other buffs for their team to make them more effective. Lucio does this very easily, as his healing and speed buff occurs in an aura around him without the need to have any complicated inputs or positioning from the player. In contrast, a character with a high skill floor in the support role would be Ana. In order to provide healing for her team, Ana needs to hit her teammates with her sniper rifle. 

**Jak tego używać**:

If you are new at a game and want results quickly, it is likely going to be a good use of your time to seek out strategies that have a low skill floor, so that you can gain a footing in the game quickly. 

If you are a type of player who appreciates the awards that learning a strategy can bring you, and you enjoy exploring that depth, then you would probably be interested in finding strategies with a high skill ceiling. You’ll be rewarded with enjoyment for seeking out the types of strategies that are rewarding for you.

**Kluczowe, agregat**:

* Games that never allow less-skilled players to beat better-skilled players, would make casual players less inclined to play those games.
* The theoretical optimal win rate is 42.381%, theoretical random win rate is 18.899%.
* If you are new at a game and want results quickly, it is likely going to be a good use of your time to seek out strategies that have a low skill floor, so that you can gain a footing in the game quickly.
* (zakładam że skill floor, skill ceiling, skill delta są już znane)

---
### 3.2. Stopnie trudności - analiza w kontekście EZ

W EZ mamy dwie role:

* Gracz
* MG / Procesor

Jeżeli gracz niedoświadczony jest w stanie zrobić wszystko identycznie jak gracz doświadczony, jest bardzo niski skill ceiling. Szczęśliwie wiemy, że w EZ tak się nie dzieje. Doświadczony gracz jest w stanie powołać bardzo dużo dodatkowych klocków i elementów, prawidłowo wykorzystać wszystkie elementy świata nawet te które nie są zaproponowane i jest w stanie przewidzieć dużo więcej niż Mistrz Gry powiedział.

Jednocześnie EZ jest systemem, w którym skill floor jest bardzo niski dla gracza. Nawet deklaracja typu ‘to jak go tnę’ jest to deklaracją wystarczającą do zrobienia czegoś sensownego – MG spyta co gracz chce osiągnąć, z intencji wyprowadzimy konkretne działania.

Ale spójrzmy teraz na rolę MG. W tej chwili MG ma bardzo wysoki skill floor. Wszystkie narzędzia, które próbuję zaprojektować i wprowadzić mają za zadanie obniżyć skill floor. Nadal – EZ pozwala ci zbudować sensowną sesję stosunkowo prosto. Z uwagi na to w jaką stronę kieruje się EZ, skill IMPACT (nowy koncept) wielu innych systemów jest niżej niż **startowe sesje** w EZ oraz skill floor jest dużo wyżej - w większości innych systemów MG może łatwo wszystko spieprzyć. W EZ praktycznie nie da się wszystkiego spieprzyć, jeśli trzymasz się serii niewielkich twardych zasad, które dotyczą kreowania sesji.

Still - jednym z głównych celów które mam do osiągnięcia jest dramatyczne obniżenia skill floor dla MG i eleganckie wprowadzenie wszystkich grających do systemu i świata.

---
## 4. Co to jest Player Divergent Mastery?
### 4.1. Divergent Mastery - dane

Nie mam źródła na tą nazwę. Korzystam z ChatGPT, nie umiem tego dobrze nazwać.

Popatrzmy na Unreal Tournament:

![UT Instagib](Materials/240116/240122-001-instagib.png)

Instagib jest specyficznym trybem, gdzie jedno delikatne trafienie automatycznie zabija przeciwnika. Jeżeli nie jesteś świetnym strzelcem, nie masz żadnych szans w tym trybie gry. Osoba niedoświadczona walcząca z ekspertem nigdy nie jest w stanie niczego zrobić.

Skupmy się jednak na tym gdzie jest _mastery_ w tej grze. Chodzi o prędkość akcji i reakcji, manewrowanie w powietrzu, wyjątkowa precyzja celowania i wysokoadrenalinowe ruchy.

Popatrzmy na Incredible Machine / Factorio / Contraption Maker:

![ContraptionMaker Puzzle](Materials/240116/240122-002-contraption.png)

Podstawowa gra polega na tym, że masz zaawansowane puzzle i używając odpowiednich komponentów próbujesz je rozwiązać. Spotkałem się z 2 typami _mastery_ w wypadku tej gry:

* użycie minimalnych ilości komponentów, mniejszych niż przewidział projektant
* zrobienie najdzikszej, najbardziej niemożliwej maszyny, która robi efekt ‘wow’

To co istotne - obie strategie wymagają dużej ilości ćwiczenia i kombinowania. Ale ci ludzie nie grają w tą samą grę, mimo że używają tych samych klocków.

Podstawowa gra polega na tym, że masz zaawansowane puzzle i używając odpowiednich komponentów próbujesz je rozwiązać. Spotkałem się z 2 typami _mastery_ w wypadku tej gry:

* użycie minimalnych ilości komponentów, mniejszych niż przewidział projektant
* zrobienie najdzikszej, najbardziej niemożliwej maszyny, która robi efekt ‘wow’

To co istotne - obie strategie wymagają dużej ilości ćwiczenia i kombinowania. Ale ci ludzie nie grają w tą samą grę, mimo że używają tych samych klocków.

Co jest najważniejsze z tego o co mi chodzi - ludzie grający w UT Instagib są nieprawdopodobnymi ekspertami w swoim trybie gry. Oni to kochają. Tak samo ludzie optymalizujący Contraption Maker. Tak samo ludzie budujący najbardziej złożoną maszynę Rube Goldberga.

Ale oni realizują swoje _mastery_ zupełnie w innych miejscach i w inny sposób. Mogą szanować umiejętności innej grupy, ale niekoniecznie interesowałoby ich granie w wariant innej grupy.

To jest _Player Divergent Mastery_ - różni ludzie próbują osiągnąć mistrzostwa w czymś zupełnie innym.

---
### 4.2. Divergent Mastery - analiza w kontekście EZ

Różni gracze w różny sposób chcą mieć formy ekspresji swojego mistrzostwa. Pierwsze próby mapowania tego były właśnie z poziomu GNS (Gamizm, Narratywizm, Symulacjonizm), ale to nie skupiało się na obszarach mistrzostwa graczy a na celach, dla których gracze grają. Co ma się dziać na sesji.

Popatrzmy na grę w D&D. Co tam tak naprawdę jest premiowane z perspektywy mistrzostwa? **Twarde trzymanie się zasad i znajomość systemu**:

[Buildy w D&D 5e ](https://old.reddit.com/r/dndnext/comments/w61ixr/flagship_build_series_the_seven_most_powerful/)

* ktoś kto zna udane buildy dla postaci jest w stanie być dużo skuteczniejszym na sesji
* ktoś kto zna wszystkie zaklęcia i gra magiem/warlockiem/whatever jest w stanie dużo lepiej rozwiązać problem i jest dużo skuteczniejszy

Popatrzmy na grę w EZ. Tam mistrzostwo jest w zupełnie innym miejscu:

* ktoś kto potrafi wykorzystywać szerokość świata jest w stanie przewidzieć następne ruchy
* ktoś kto potrafi dobrze powoływać elementy w otoczeniu bazując na relacjach i przedmiotach może dużo więcej niż ktoś inny
* kto się kto potrafi świetnie opisywać akcje ma bonusy do każdej puli

innymi słowy, w D&D premiowane jest coś zupełnie innego niż w wypadku EZ. To znaczy, że jeżeli ktoś chce grać w D&D, na pewno nie będzie chcieć grać w EZ – i vice versa.

Ale to też oznacza, że należy skupić się na Divergent Player Mastery i prawidłowo wbudować je w Core Mechanical Loop - gdzie jest mistrzostwo? Na czym ono polega? Jak wyglądają przykłady graczy manifestujących swoje mistrzostwo i randomów którzy tego jeszcze nie mogą robić?

Food for thought.

---
## 5. Mapowanie na Core Mechanical Loop
### 5.1. Czemu to ma być w CML?

Odpowiedź jest w tym, dlaczego Flame wybiera mechanikę uproszczonego Warhammera i dlaczego do Rogue Trader zainteresował się mechaniką EZ.

Jeżeli interesują cię bardzo ambitne walki z ogromnymi konsekwencjami i pędem, jednym z najważniejszych wymiarów jest szybkość mechaniki. Potencjalnie nawet mechanika, w której musisz rzucić 1k6 i przeczytać wynik może być za wolna. _Divergent Mastery_ którą tutaj gracz optymalizuje to trzymanie dziesiątków rzeczy jednocześnie w głowie, szybkie reagowanie i analiza ryzyka praktycznie w czasie rzeczywistym.

EZ zawodzi z uwagi na tempo. Nie musi zawodzić, ale nie może działać w formie intencyjnej - a na żywo składanie puli byłoby katastrofą w tym kontekście.

D&D zawodzi z uwagi na to, że masz za dużo obliczeń i musisz wybierać konkretne ataki w konkretny sposób. Wszelki nadmiar buchalterii jest katastrofalny w wypadku takiego celu.

Jeżeli system skupia się na konkretnych akcjach i ma wpisane konkretne _Divergent Mastery_ w CML, to gracze grając lepiej osiągają lepsze wyniki. Są nagradzani za lepszą grę i widzą, że stają się coraz lepsi. Masterują to, co ich interesuje najbardziej. I masz potem ludzi, którzy nieważne w jakim świecie nie grają na mechanice Warhammera - bo oni nie chcą grać w inny system, chcą „Unreal Tournament Instagib”. I to jest zdrowe i ok. Interesuje ich skórka innego systemu, ale chcą CML systemu, w który już grają.

To jest ważne - system nie może służyć wszystkim graczom a musi targetować konkretną niszę. 

---
### 5.2. Jak można wbudować Divergent Mastery w CML?

Załóżmy - jak w wypadku EZ - że chcemy nagradzać „fajne akcje”. Jeżeli gracz ma fajny pomysł - czy to przez użycie otoczenia, czy to przez użycie zasobu, czy przez powołanie czegoś, w ramach EZ możemy:

* zmienić skalę trudności (siłowanie się z trollem, żeby go zrzucić w przepaść? Ex/Hr. Wymanewrowanie trolla, żeby sam tam wpadł? Tr.)
* dodać punkty uznaniowe (świetna deklaracja akcji? Tr+2 -> Tr+3)
* użyć mechaniki do zmodyfikowania rzeczywistości („a mogłam tam być cały czas? -> TAK, więc akcja możliwa”)

I to sprawia, że gracze, którzy chcą masterować umiejętności _rozwiązywania problemów wielowymiarowych_ mają ogromną przestrzeń, w której mogą się bawić.

Tu jest tego więcej, ale nie o tym ta notatka.

---
### 5.3. W kontekście gracza, ok, ale MG?

To jest najciekawszy obszar, którego jeszcze nie przeanalizowałem. MG jest formą osoby, która gra. Jest to konkretna rola. Istnieje _Divergent Mastery_ też pod kątem MG w różnych systemach. I moją odpowiedzialnością - jako designera - jest zlokalizowanie tych rzeczy, ujawnienie ich i zbudowanie przestrzeni dla MG jak może się tego uczyć, obniżając skill floor do najniższego poziomu jak to możliwe.

---
### 5.4. Co dalej?

To jest primer. Starter. Początek rozważań. To jest coś by nie zapomnieć i wrócić do tego później, jak to mogę doprowadzić do prawidłowego działania.
