---
layout: mechanika
title: "230608 - Problem konsekwencji - inspiracja"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw

---
## 1. Purpose

Jednym z trudniejszych problemów, jaki jest z perspektywy MG w mechanice jest problem krzyżyków i ptaszków. Innymi słowy - konsekwencji.

Celem tej notatki jest zaproponowanie rozwiązania, które jest proste i sprawne. Tak bym nie tylko ja potrafił łatwo przydzielać krzyżyki i ptaszki.

---
## 2. High-level overview



---
## 3. Problem -> Rozwiązanie

## 3.1. Demonstracja problemu

Weźmy przykład konfliktu z sesji "230501-rozszczepiona-persefona-na-itorwienie".

```
KONTEKST:

Itorwien to jest statek kosmiczny szukający odrzutów / śmieci, zbiera je i recykluje.
Pojawił się sygnał SOS z Itorwiena "TAI Persefona nas zabija! SOS! SOS!"
Klaudia (Gracz) jest na pokładzie jednostki ratunkowej. Powinna uratować Itorwien.

SYTUACJA:

Dla Klaudii ta sytuacja jest dziwna: 
    Persefona powinna być w stanie zablokować każdy sygnał SOS. A wyszedł.
    Persefona powinna taktycznie wyłączyć sygnał SOS. A nie wyłączyła.
    Persefona nie dodała własnego pakietu danych do sygnału SOS. A powinna.

CEL:

Klaudia chce dowiedzieć się co się dzieje na Itorwienie i jak pomóc.
```

Żeby dowiedzieć się, co się stało, Klaudia może wykonać akcje:

* Nawiązania połączenia z Persefoną
* Przeskanowania Statku kosmicznego.
* Wejścia na pokład.

I tu już zaczynają się interesujące problemy.

Załóżmy akcję „wejście na pokład”. 

* Implementacja w fikcji:
    * Martyn, jej sojusznik zakłada serwopancerz i wchodzi na pokład Itorwiena.
* I wypada pierwszy (X).
    * Co się dzieje?
        * Sytuacja w stylu „_wejście na pokład jest niemożliwe_” jest nieakceptowalna - nie ma sensu oraz psuje sesję.
        * Sytuacja w stylu „_serwopancerz Martyna zostaje uszkodzony przez działanie Persefony i musi wrócić na pokład, żeby go naprawić_” nie jest prawidłowa: 
            * nie przesuwa fikcji do przodu. To jest odpowiednik „ otwieram drzwi - nie udaje mi się - próbuję jeszcze raz”
            * Wyjątek: jeżeli czas jest istotny to wtedy ma to sens (komentarz do Gracza w stylu „przez to Druga Strona ma więcej czasu”)
        * Nadanie negatywnego aspektu w stylu „_Persefona się boi i nie będzie współpracować_” może zadziałać 
            * ale mimo przesunięcia fikcji do przodu i zmiany stanu fikcji nie jest ciekawą odpowiedzią. "My to już wiemy", po prostu dodałem kolejne utrudnienie do współpracy z Persefoną przez ekstra aspekt.

Bądźmy szczerzy - nie jest łatwo wymyślić pierwszego krzyżyka w takiej sytuacji, zwłaszcza jeżeli się go nie spodziewamy i zwłaszcza, że obie strony jeszcze nie do końca wiedzą co się tu będzie działo.

Jedną ze strategii stawiania krzyżyków może być **Podbicie** - wtedy pierwszy krzyżyk służy do podbicia następnego krzyżyka. 

Pytanie brzmi w którą stronę MG prowadzi tą sytuację? Co chce osiągnąć? Co _może_ osiągnąć?

## 3.2. Potencjalne rozwiązanie - Aktorzy mają agendy. Mistrz Gry też.
### 3.2.1. Substrat

W pewien sposób rozwiązanie tego problemu pokazała mi Kić oraz mechanika Wicked Ones – a dokładniej, spojrzenie sandboxowe Wicked Ones.

**_Kić_**:

Gdy Kić buduje sesje, bardzo często wychodzi z kilku stron, które mają swoje agendy i coś chcą osiągnąć i ogólnie to nie ma pojęcia co się stanie. 

Zakłada, że gracz będzie stroną bardzo aktywną i przez prawidłowe pozycjonowanie postaci gracza, sama postać będzie miała motywację by jakoś to rozwiązać. 

A krzyżyki sprawiają, że inne strony będą dążyły do realizacji swoich celów, jeżeli Kić nie ma innego pomysłu. 

Przykład:

```
KONTEKST:

Na stacji kosmicznej dochodzi do handlu niewolnikami.
Olgierd (Gracz) szuka zaginionego agenta Orbitera.
Olgierd słyszał, że może z tym być powiązany groźny Tristan.

STRONY I AGENDY:

Olgierd: "znajdź zaginionego agenta Orbitera"
Siły porządkowe: "utrzymać status quo, chronić stację"
Tristan: "zarobić pieniądze" -> "władza w stacji"
```

W tym momencie, gdy Olgierdowi coś nie wychodzi w otoczeniu, krzyżyki mogą aktywować: 

* siły policyjne (które będą niechętne olgierdowi tu węszącemu)
* Tristana (który będzie próbował wykorzystać bądź pozbyć się aktualnego zagrożenia ze strony Olgierda)
* agendę / tor „zniknięty agent Orbitera”, gdzie dodajemy aspekty i okoliczności utrudniające Olgierdowi odzyskanie delikwenta i konfliktującego go z różnymi stronami.

W ten sposób nie musisz mieć dużo przygotowań sesji, by móc prawidłowo przeprowadzić taką sesję. 

* Krzyżyki dodają utrudnienia dla graczy i przesuwają agendę Drugiej Strony 
* Ptaszki prowadzą Graczy tam gdzie ich cel jest deklarowany.

Prawidłowość pozycjonowania Olgierda polega na tym, że jemu aktywnie zależy na odnalezieniu kogoś a pozostałym stronom aktywnie zależy na tym by ukryć coś co jest potencjalnie niezwiązane. 

Więc Olgierd - chce nie chce - ale będzie wchodził im w szkodę.

Jako, że Olgierd charakterologicznie nie pozwoli na to, żeby na terenie Orbitera działy się takie numery z handlem niewolnikami (co wynika z karty postaci i agendy postaci), Kić ma gwarancję, że Olgierd zrobi tym więcej niż tylko odnajdzie zaginionego agenta.

Dowcip polega na tym, że agent mógł po prostu zapić i siedzi w izbie wytrzeźwień. Olgierd i tak spróbuje coś zrobić z tą stacją.

A jak Kić chce utrudnić sesję, krzyżykami pójdzie w stronę "czy Olgierd zajmie się STACJĄ czy AGENTEM ORBITERA", efektywnie zmuszając Gracza do decyzji _kim jest Olgierd_ - czy ważniejsze dla niego są rozkazy i agent pijaczyna, czy ważniejsze jest przyskrzynienie Tristana.

Czy, oczywiście, Gracz i tak nie rozwiąże obu problemów używając wybitnego intelektu i świetnej mechaniki ;-).

**_Wicked Ones jako przykład dobrego sandboxowego podejścia_**:

Dobry sandbox zawiera kilka skonfliktowanych frakcji, które: 

* można rozgrywać między sobą i które mają jasno określone cele 
* jak i cechy, które można wykorzystać

Dzięki temu nawet jeśli postacie graczy są stosunkowo słabe, to frakcje są bardziej zajęte rozgrywką pomiędzy innymi frakcjami niż zmiażdżeniem postaci graczy.

Przykład dobrego sandboxa z _The Wicked Ones_:

![The Wicked Ones, sandbox example](Materials/230608/230608-wicked-ones-sandbox-example.png)

Popatrzcie na powyższą sytuację. Weźmy frakcję „College Arcanum”:

* są wyraźnie skonfliktowani ze szczurami, czyli można rozgrywać ich przeciwko szczurom
* będą walczyli przeciwko akcjom świątyni i wspierali akcję rodziny królewskiej
* ruchy które będą robić będą atakować królewską straż lub jedną frakcję szczurów
    * Czyli krzyżyk pozwoli nam np. wykonać ruch przeciw tamtym frakcjom który przy okazji szkodzi postaciom graczy
        * Przykład takiego krzyżyka:
            * kontekst: 
                * postacie graczy próbują dogadać się z merfolkami
                * postacie graczy przynoszą dla merfolków jakiś dar
                * robimy całkowicie randomowy konflikt
                    * Darken -> to co mi opowiadałeś o tym jak Ari wprowadziła konflikt mi się tu teraz skojarzyło. Nieważne JAKI konflikt jeśli mam dostęp do "konceptu" (V) i (X).
            * pierwszy (X) -> (podbicie) w okolicy pojawia się kilku magów, którzy akurat szukają dowodów na działanie szczurów w okolicy. I węszą pechowo blisko merfolków.
                * Darken, Ty nazwałbyś to `announce badness`. Ja `wprowadzeniem zasobu` / `cause random chaos`.
            * drugi (X) -> magowie wpadli na ślady merfolków i się nimi zainteresowali
            * trzeci (x) -> magowie przechwycili wysłannika merfolków

### 3.2.2. A gdyby "Rozszczepiona Persefona na Itorwienie" (to z Problemu) przyniosła zbiór agend?

Zamodelujmy dwie frakcje z tej sesji w stylu The Wicked Ones. Tak nie robiłem, ale tak _mógłbym_ zrobić. Zachowuję spójność i kontekst sesji, czyli gdybym używał tej techniki to dokładnie tak bym modelował poniższe frakcje.

* **Dowódca Klaudii, Fabian**
    * **Cechy**
        * Wszyscy ludzie muszą być uratowani
        * Niecierpliwy, chce mieć wszystko z głowy
        * Chce świetnie wypaść przed dowództwem (liczy na awans)
    * **Zasoby**
        * dowódca Orbitera w kontekście tej sesji
        * uprawnienia wydzielonej grupy Orbitera (czyt. "jestem baronem póki nie eskalujesz")
        * Serbinius, korweta ratunkowa z fabrykatorami
    * **Ruchy**
        * (V): jako dowódca statku z ramienia Orbitera, wymusza współpracę z Drugiej Strony.
            * Np. "Fabian opieprza dowódcę wrogiego statku i żąda współpracy albo wejdzie na pokład z bronią. Ich wybór."
        * (X): robi coś co utrudnia operację ale lepiej wypadniecie przed dowództwem
            * Np. "Fabian mówi, że nie możecie niczego zbyt mocno uszkodzić by było mniej kosztów i by Wasza jednostka nie była widziana jako droga w użyciu"
        * (X): robi coś racjonalnego ale sprzecznego z agendą postaci Gracza
            * Np. "Fabian każe zniszczyć uszkodzoną Persefonę. Klaudia jest bardzo pro-TAI i się z tym BARDZO nie zgadza. Otwiera się nowy front konfliktów."
* **Uszkodzona TAI Persefona, statek Itorwien**
    * **Cechy**
        * Rozbita na dziesiątki sub-osobowości walczących ze sobą i obejmujących różne podsystemy statku kosmicznego
        * Chce chronić swoją załogę, ale niszczyć piratów i osoby zagrażające statkowi
        * Zniszczone ograniczniki. Nie ma zahamowań.
    * **Zasoby**
        * każde urządzenie i komponent Itorwiena, statku-śmieciarki
        * władza nad życiem lub śmiercią załogi (możliwość wyłączania / włączania komponentów)
        * możliwość samozniszczenia statku przez przesterowanie silników lub reaktora
    * **Ruchy**
        * (X): działanie dziwne z uwagi na uszkodzoną psychotronikę
            * np. klasyfikuje niektórych ludzi jako wrogów LUB widzi piratów w pustym hangarze
        * (X): używa podsystemów statku by skrzywdzić "piratów"
            * np. miażdży ludzi śluzą. Lub zatrzaskuje śluzę tam gdzie widzi ludzi których nie ma. Lub mechanizmami naprawczymi uszkadza podłogę, by ktoś się zapadł.
        * (X): walczy o kontrolę nad statkiem ze sobą samą (przez rozszczepienie)
            * np. wysyła drony śmieciarkowe do zniszczenia nawigacji (bo uważa, że tam "jest wróg")

Wróćmy do sytuacji: 

Załóżmy akcję „wejście na pokład”. -> Martyn, jej sojusznik zakłada serwopancerz i wchodzi na pokład Itorwiena.

* (X): jakie mamy możliwości?
    * LINIA FABULARNA - PERSEFONA. 
        * Persefona nie posiada ograniczników. Niech więc sklasyfikuje Martyna jako pirata i spróbuje zabić go śluzą. Czyli zadziała prawidłowo, otworzy śluzę i jak Martyn będzie wchodził na pokład to spróbuje go zatrzasnąć i zmiażdżyć.
        * Więc pierwszy (X): (podbicie) zastawia pułapkę i otwiera śluzę.
            * Dla drugiego (X), Martyn nie dał się zatrzasnąć acz jest zablokowany w śluzie dwustronnie zamkniętej
                * Dla trzeciego (X), Martyna nie da się sensownie wydostać nie raniąc go, bo jest za blisko kadłuba.
    * LINIA FABULARNA - FABIAN CHCE DOBRZE WYPAŚĆ.
        * To ogólnie wygląda na prostą operację, więc czemu nie spróbować zrobić z tego materiału szkoleniowego czy demonstracji prawidłowego wejścia na pokład statku? Fabian każe to dobrze nagrywać Co sprawia że będzie do tego w przyszłości dostęp.
        * Więc pierwszy (X): (podbicie) wszystko jest dokładnie nagrywane w celach szkoleniowych.
            * Drugi (X): będą poważne rozmowy z Klaudią, Martynem, Fabianem itp. z uwagi na ich "nieprofesjonalne" zachowanie przeprowadzane przez Poważnych Ludzi Siedzącymi Przy Biurkach. Fabian cierpi przez opiernicz; załoga Serbiniusa będzie Obserwowana.
                * Trzeci (X): na pokładzie statku pojawi się konflikt (np. Klaudia chce pomóc TAI a zgodnie z procedurami trzeba ją zniszczyć). Co wybierze Klaudia? Jest nagrywana.

Zauważcie, że wprowadzenie jawnych agend wszystkich stron sprawia, że dużo prostszym staje się zaproponowanie właściwego krzyżyka. Przeniosłem problem w inne miejsce - jak zbudować sensowne agendy na sesję.

### 3.2.3. Budowa sensownych agend na sesję - koncept "fiszki / karty postaci".

Popatrzmy na postać Jokera (wariant: z filmu Nolana). 

* Batman gdy miał najgorszy dzień w swoim życiu skupił się na tym by nikt inny nie cierpiał tak jak on. 
* Gdy Joker miał najgorszy dzień w swoim życiu, skupił się na tym, by pokazać światu we wszyscy zrobiliby to co on. "_To co różni zwykłego człowieka ode mnie to jeden zły dzień._"

Joker nie jest jedyną postacią w fikcji która działa w ten sposób. Zupełnie inne metody, ale podobne cele ma Imperator Palpatine z Gwiezdnych Wojen.

Oboje implementują archetyp o nazwie [**The Corrupter**](https://tvtropes.org/pmwiki/pmwiki.php/Main/TheCorrupter).

W taki właśnie sposób powstały (archetypy / templatki / prototypy) kart postaci:

![Inspirator](Materials/230608/230608-02-inspirator.png)

I teraz:

**JEŻELI** mogę zrobić prototyp dla karty postaci na tymczasową sesję, to **TYM BARDZIEJ** mogę zrobić prototyp na "powtarzające się klocki i agendy".

Np. Fabian mógłby zostać frakcją typu "_Over-eager commander_" (nadgorliwy dowódca) a Persefona typu "_Murderous Spaceship Artificial Intelligence_" (mordercza AI statku kosmicznego).

Wtedy złożenie sesji to "weź klocki, zmień je trochę, popatrz na główne agendy i będzie dobrze". Wybrane klocki niosą "sugerowane krzyżyki" które stanowią inspirację i "sugerowane ptaszki" pokazujące co można z tymi klockami zrobić.

W pewien sposób częściową odpowiedzią na dylemat ptaszków i krzyżyków jest Apocalypse World i Threats stamtąd. Przykład:

![Warlord i Landscape z AW](Materials/230608/230608-03-warlord-aw.png)

Jak widać:

* "impulse" oznacza "czego pragnie" lub "commander's intent". Czyli "czemu to robi". W jaką stronę (agendowo) powinniśmy to robić.
* "MC Moves" to jest dokładnie seria akcji i ruchów które mogą być użyte, czyli strategie którymi dany byt prowadzi do "impulse"

Czyli np: 

* mamy na sesji Warlord (typ: Alpha Wolf)
    * postacie Graczy coś robią
        * pierwszy (X) -> jesteście na terenie Warlorda / jego ludzie są na tym terenie (podbicie)
        * drugi (X) -> któraś forma ataku ("hunt and dominate") albo pojawiają się i żądają ukorzenia się ("demand obedience"), np. "czemu tu jesteście, przeproście".
            * Mechanicznie: albo wchodzą w konflikt albo pokazują drugiej stronie że jest silniejsza - przyszłe relacje uszkodzone.
* mamy na sesji Landscape (typ: Mirage)
    * postacie Graczy działają na terenie gdzie jest Mirage
        * pierwszy (X) -> hide something (podbicie). Np. Gracze nie mogą tego znaleźć normalnymi metodami, muszą kombinować.
        * drugi (X) -> take something away (np. muszą zapłacić by dostać lokalnego przewodnika który ich zdradzi) lub present a guardian ("AHA! Wy też tego szukacie!!! Oddaj!" - crazed people)

### 3.2.4. A gdyby to SESJA niosła agendę a nie klocki?

Misspent Youth ma na to pewną odpowiedź - to jest system, w którym walczysz przeciwko The Authority, przeciw dystopijnej rzeczywistości. Prawdziwie punkowy system :-).

![Misspent Youth](Materials/230608/230608-04-misspent-youth.png)

* Authority stanowi głównego przeciwnika całej sesji - jest jeden przeciwnik, nie N agend. Pytanie sesji to "czy Authority zmiażdży ducha postaci czy postacie dadzą radę złamać system".
    * Vice: czego pragnie a nie ma
    * Victim: co ucierpiało przez to, że Authority dąży do Vice
    * Visage: jaka jest forma Authority (np. jedna osoba, system wierzeń, firma...)
    * Exploit: słaby punkt jakiego używają gracze by nie zniknąć

Czyli jako przykład:

* Postacie robią COŚ
    * Wariant krzyżyków: MONEY
        * Jakiś desperat ma nadzieję na pieniądze na następną dawkę narkotyków.
    * Wariant krzyżyków: SEX
        * Jakaś desperatka przyczepia się do postaci graczy z nadzieją, że sobie dorobi w wiadomy sposób i przeszkadza im w głównym celu. Lub ktoś próbuje porwać postacie graczy - są młodzi, więc da się ich sprzedać.
    * Wariant krzyżyków: DRUGS
        * Postacie graczy przypadkowo widzą coś czego nie powinny i trzeba wiać
    
Jak widzicie, to też ma potencjał.

## 4. Co dalej?
### 4.1. Highlevel

Jesteśmy za wcześnie na dalszą krystalizację.

Muszę poeksperymentować z różnymi formatami i wypracuję coś fajnego co działa. Ale mam wrażenie że tu gdzieś - w tej notatce i tych przykładach - jest odpowiedź na nasz problem.

Wicked Ones, Apocalypse World itp. dają kilka możliwych podejść.

W sumie... EZ mi się najlepiej prowadzi jako Sandbox dookoła filmowej wysokopoziomowej narracji. Kotu chyba też. A jednostrzały robimy epizodycznie ;-).

### 4.2. Co widzę już teraz?
### 4.2.1. Hazardy

Wyobraźcie sobie sesję, w której mamy **Hazard** typu: 

* "mgła astralna zaćmiewa ludzkie umysły"
* "kończy się tlen bo nie działa life support". 

Wiecie, typowe **Hazardy**. Normalnie daję je jako aspekty sytuacji, a co jeśli miałbym listę Hazardów wyglądających tak:

* Hazard: "mgła astralna zaćmiewa ludzkie umysły"
    * **impuls**: "ludzie zachowują się niewłaściwie"
    * **dominujące emocje**: frustracja > rozpacz
    * **ruchy**:
        * Ktoś robi coś głupiego i zagraża innym przez zaćmienie astralne
        * Ktoś ma halucynacje lub żyje w swojej przeszłości
        * Ktoś czegoś nie zrobił a myślał że zrobił

gdzie:

* impuls: jaki KIERUNEK ruchów wybrać. To jest _Commander's Intent_.
* dominujące emocje: w jakie emocje MG powinien iść używając tego hazardu. To "koloruje" sesję.
* ruchy: potencjalne krzyżyki

Mając taki hazard od razu widać co się może stać i jak to może zadziałać.

### 4.2.1. Theme & Vision & Dilemma
#### 4.2.1. Co?

Theme and Vision jest w pewien sposób mechanizmem wiodącym, pokazującym mi _czemu dana sesja istnieje_. Pokazuje mi co ja jako MG mam osiągnąć i kieruje mnie w daną stronę.

Czyli gdyby ta sesja była odcinkiem anime to dlaczego ta sesja nie jest „fillerem” tylko ma coś zrobić - co to jest to coś.

Dilemma jest podzbiorem Theme & Vision którego funkcją jest pokazanie głównej osi moralnej sesji - to oznacza że postaci graczy muszą podjąć nieoczywisty wybór i albo wybrać między 2 dobrymi opcjami albo między 2 złymi opcjami.

Uważam, że to też może przynieść pewne krzyżyki "overarching" (bardziej jak _Misspent Youth_ niż jak _Hazard_).

#### 4.2.2. Przykłady Theme & Vision z prawdziwych sesji dla zobrazowania

Sesja "230404-wszystkie-duchy-siewczyna":

```
CEL SESJI:

* pokazanie specyfiki rodu Samszar
    * KONTRAST z rodem Verlen: 
        * Verlenowie walczą z potworami
        * Samszarowie harmonizują z naturą
    * Harmonia z duchami
```

Standardowa metoda demonstracji czegoś przez kontrast. To mogę potraktować dokładnie tak jak hazard.

* **Demonstracja przez kontrast**
    * (X): wprowadź sytuację, która jest najprościej rozwiązywana przez to drugie podejście
        * np. "poczciwa świnia harcuje i straszy duchy. Verlenowie by to rozwalili, Samszarowie nie mogą - oni spróbują zrobić to pacyfistycznie"
    * (X): pokaż jak przez użycie drugiego podejścia coś się poważnie spieprzyło
        * np. "przez to, że rozwalili potwora rozsypał się ekosystem i musimy ściągać następnego by odpędzić pasożyty"
    * (X): wprowadź coś co jest łatwiejsze do rozwiązania przez to podejście niż przez alternatywne
        * np. "dzięki współpracy z duchami w ekosystemie łatwiej da się wykryć anomalie i zakłócenia"


Sesja "230329-zdrada-rozrywajaca-arkologie":

```
Theme, Vision, Dilemma: 

* Bardzo niefortunny efekt motyla
* Arkologia jest za mała i niezwykle niestabilna
    * malutkie kłótnie osobiste -> ogromna kaskada
```

To jest jednoliniowa sytuacja; trzymajmy się tego efektu motyla.

* **Bardzo niefortunny efekt motyla**
    * Pokaż małą sytuację, która nierozwiązana eksploduje na coś dużego
        * np. wyraźnie inżynier nie może się skupić przez to, że uważa, że dziewczyna go zdradza
            * (dalsza implikacja: jeśli będzie zdekoncentrowany to może nie zauważyć awarii która spowoduje złą różnicę ciśnień w rurach -> coś gdzieś wybuchnie)
    * Weź dowolną małą rzecz która już się wydarzyła i zrób implikację która jest mało prawdopodobna ale eksploduje
        * np. inżynier pokłócił się z dziewczyną -> inżynier głośno opowiada jaka z niej zołza -> jej przyjaciel pracujący z inżynierem to usłyszał i broni jej honoru -> pobili się -> oboje zawieszeni XD



Sesja "230325-ten-nawiedzany-i-ta-ukryta":

```
Melodia:

* CEL: nowy przeciwnik dla Marysi Sowińskiej i problemów inter-Rekinowych
* Chciała dobrze, wyszło jak zwykle
* "tien nie może zaprzyjaźnić się z człowiekiem, to głupie" 
* "tien nie może ufać innemu tienowi; skończy się na zdradzie"
* reputacja jest ważniejsza niż skuteczność i poproszenie o pomoc
```

To o tyle interesująca sesja, że główny przeciwnik znajduje się w tle i chce pomóc. Nie jest "przeciwny graczom". Autentycznie chce uratować innych, ale mimo ogromnej mocy całkowicie jej to nie wychodzi. 

A musi podtrzymywać swoją reputację w swoich oczach – najlepsza, bezwzględna - więc musi dyskretnie działać by jej sojusznicy nie wiedzieli, że ona próbuje pomóc. Bo jak to - mag i to arystokrata AURUM pomaga człowiekowi?

Z perspektywy postaci graczy ona jest przeciwnikiem, bo jest przyczyną połowy problemów. Ale gdy postaci graczy przyszły do niej by ona pomogła, to jak tylko ją poprosili powiedziała, że „wyjątkowo jest skłonna im pomóc” a w serduszku się cieszyła, że była w stanie naprawić sprawę jawnie.

Jak więc to zamodelować? Spróbujmy inaczej - wyjdę od modelu Misspent Youth. Widzę tu dwie dominujące... agendy? _„Authority”_?

1. **Chciała dobrze a wyszło jak zwykle**
    * **Głód**: Pomoc komuś kogo uważa za wartościowego (ALE wszystko obraca się w ruinę)
    * **Co traci**: "Jestem wartościową osobą i mam sprawczość".
    * **Ruchy**: 
        * (X): Coś co chciała zrobić, żeby pomóc robi straszne problemy tym którym miało pomóc
        * (X): Efekty uboczne jej pomocy są katastrofalne dla postronnych
        * (X): Stara się bardziej niż kiedykolwiek, nie wychodzi jej i robi desperackie ruchy
        * (V): Jej pomoc jest wartościowa i rozwiązuje duży problem
2. **Działa w ukryciu by zachować reputację**
    * **Głód**: Nikt nie może dowiedzieć się, że jest z tym powiązana
    * **Co traci**: "Jestem wierna sobie i swoim ideałom"
    * **Ruchy**: 
        * (V): Dyskretnie pomaga postaciom graczy rozwiązać problem, by nie wiedzieli, że to ona
        * (X): Bierze za dużo na siebie, przez co coś idzie nie tak.
        * (X): Robi komuś krzywdę by pozostać w ukryciu

Krzyżyki muszą działać niezgodnie z celami GRACZY - więc np. "efekty uboczne" są czymś z czym oni będą mieć do czynienia. Ptaszki to gdy MG mówi "słuchajcie, mam dla Was taką propozycję - KTOŚ wam pomoże ale nie wiecie kto ale to jest wartościowe", by namawiać do eskalacji.

Jak widać niezależnie od sytuacji i okoliczności, jeżeli pojawi się krzyżyk to taka sytuacja jak ta wyżej daje mi opcję „hidden movement” - dodanie okoliczności (przeszłych okoliczności) do planów Drugiej Strony. 

W ten sposób krzyżyki sprawiają, że Druga Strona jest „aktywna” - w teraźniejszości lub przeszłości.

### 4.3. Frakcje

Najpewniej jakaś forma agregatu pomiędzy Misspent Youth oraz Wicked Ones. Coś takiego jak pokazałem wcześniej z Fabianem, ale dla odmiany zrobione dobrze. 

Tu mam najwięcej do kombinowania.