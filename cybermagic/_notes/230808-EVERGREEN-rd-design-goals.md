## 1. Wysokopoziomowe cele (do doprecyzowania)

1. **Zasady wiodące designem**
    * (1) **Każdy konflikt da się zamodelować**
        * Każdy - absolutnie każdy - konflikt w fikcji jest możliwy do zamodelowania w mechanice
        * Testowane na: [notatce gdzie rozpartywane są testy z fikcji pod kątem tego jak działają](230507-mechanika-dynamicznych-zetonow.md)
        * AKTUALNA SŁABOŚĆ: grupa vs grupa. Da się zrobić kolorując żetony, ale _to nie to samo_.
            * inspiracja: Storming the Wizard's Tower, Lumpley
    * (2) **Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.**
        * To co najciekawsze dzieje się w środku testów, nie w wolnych rozmowach między graczami.
        * Gracze mają "pozytywny stres" - czy wyjdzie V, X, czy O.
            * RPG, co mi słusznie przypomnieliście (-> Dzióbek, Kić), to jest show, a nie tylko gra.
            * "czy odważę się pociągnąć następny żeton?"
        * Istnieje "pęd" w akcjach. Każda akcja **coś** zmienia. Nie ma akcji nullowych.
            * Każdy ptaszek i krzyżyk ZMIENIA stan gry w sposób NIEODWRACALNY
        * Mechanika jest punktem kulminacyjnym sytuacji, a nie czymś, co wykorzystujemy, bo musimy. 
    * (3) **System intencyjny**
        * Gdy 2 osoby siedzące przy stole mają inną interpretację, gdzie historia ma się potoczyć, wtedy używamy mechaniki.
            * Nie "gdy _dwie postacie_ są w konflikcie a gdy _dwie osoby przy stole_ 
    * (4) **System negocjacyjny - coś wygrywasz, coś poświęcasz**
        * Za każdym razem jak wykonywany jest test, Gracz coś stawia na szali i coś chce osiągnąć.
            * Tu wchodzi wielofazowość mechaniki - mogę zatrzymać się szybciej i nie eskalować wyżej.
        * Nagroda jest proporcjonalna do ryzyka 
            * Im wyżej idę tym większa nagroda, ale tym większa szansa, że nie wszystko pójdzie po mojemu.
                * acz to Gracz decyduje co jest skłonny poświęcić i na czym mu zależy
                    * --> Gracz zawsze osiągnie TO CO DLAŃ NAJWAŻNIEJSZE, pytanie czy zapłaci WSZYSTKIM INNYM.
    * (5) **Podejście taktyczno-dramatyczne. Strategie mają znaczenie. Decyzje i deklaracje Graczy mają OGROMNE znaczenie.**
        * Każde dociągnięcie żetonu ma znaczenie. Nie ma 'automatycznego' ciągnięcia.
        * Różne podejścia do tego samego problemu dają różne wyniki.
            * Na przykład, „Atak frontalny na trolla” vs „Przechytrzenie go, by poszedł na bagno”
        * Gracz poświęcając więcej czasu i energii na sprytniejsze, ciekawsze lub bardziej efektowne rozwiązanie jest wynagrodzony przez większy _Impact_ lub _Probability_ akcji.
            * To oznacza, że Gracze "awansują" jako ludzie. Doświadczony Gracz zrobi dużo więcej niż niedoświadczony Gracz.
    * (6) **Nie ma możliwości, by osoba siedząca przy stole nie wpłynęła na fikcję**
        * Nieważne jak niedoświadczona czy przestraszona jest osoba, musi być w stanie wpłynąć na fikcję. Jeśli JAKKOLWIEK się stara, coś zmieni.
            * Test: czy gdyby nie było tej osoby, to opowieść potoczyłaby się tak samo. Jeśli choć dla JEDNEJ osoby tak jest, mechanika / prowadzenie zawiodło
        * Nawet niedoświadczona osoba nie wdrożona w sytuacje może zmienić bieg historii. Chociaż lokalnie.
    * (7) **Gracz ma kontrolę nad ryzykiem**
        * Gracz decyduje "ciągnę lub coś poświęcam / poddaję konflikt"
        * Wynik konfliktu / stawki nie są zaskakujące. Gracz zawsze "czuje" co stawia i co może wygrać. 
            * Zarówno testy jak i potencjalne interpretacje są jawne
    * (8) **Catchup mechanism**
        * Jeśli ci nie wychodzi i ciągle dociągasz krzyżyki, rośnie szansa na ptaszek. Jeśli wypada ci dużo ptaszków, rośnie szansa na krzyżyk.
    * (9) **Chaos i nieprzewidywalność**
        * Nawet MG nie może sensownie przewidzieć co się stanie; V/X/O > wola MG
        * Pociągnięcie żetonów daje odpowiedź, nie interpretacja MG / Graczy
    * (10) **Nie da się przypadkiem spieprzyć fikcji**
        * Jeśli nie pasują Graczowi konsekwencje (X), może nie przyjąć i zaproponować coś innego, równego lub większego w skali
        * Zarówno Gracz jak i MG ewaluują konsekwencje pod kątem potencjalnie najlepszej opowieści
2. **Parametry niefunkcjonalne**
    * (11) **Mechanika jest szybka (gdy się wdrożysz)**
        * Nie musi być ŁATWA, ale musi być BARDZO SZYBKA
        * Złożenie puli - ciągnięcie - interpretacja muszą trwać poniżej 60 sekund CAŁOŚĆ.
    * (12) **Mechanika jest tania z perspektywy kosztu intelektualnego (gdy się wdrożysz)**
        * Intensywność rozgrywki powinna mieścić się w decyzjach, a nie w wykorzystywaniu mechaniki.
            * Mechanika jest mało zauważalnym silnikiem, który po prostu działa i którego wyniki powodują emocje.
        * Nawet zmęczona i zdekoncentrowana osoba powinna być w stanie dość bezproblemowo wykorzystać mechanikę
3. **Inne ważne aspekty**
    * (13) **Decyzje gracza poza fikcją mają znaczenie**
        * to są ruchy "na poziomie meta" - jakie konsekwencje akceptujemy, jakie rzeczy chcemy by się stały
        * Gracz jest w stanie "objąć" fragmenty rzeczywistości pod swoją kontrolę i "sprawić by coś się stało"
        * patrz (4) "System Negocjacyjny"
    * (14) **Manewry (deklaracje) gracza w fikcji mają znaczenie**
        * to są ruchy "na poziomie fikcji" - "chowam się za płonącym pojazdem by zamaskować sygnaturę termiczną i..."
        * Gracz jest premiowany za właściwe manewrowanie i deklarowanie w środku fikcji
        * patrz (5) "Podejście taktyczno-dramatyczne"

Warto tu przeczytać [Jakie są zasady / koncepty Apocalypse World słowami Bakera](https://lumpley.games/2019/12/30/powered-by-the-apocalypse-part-1/)

---
## 2. Obietnica mechaniki

1. **Wpłyniesz na historię jako Gracz. MG nie kontroluje fabuły, mechanika to robi.**
    * Gracz ma statystycznie większe szanse na sukces niż MG. 
    * Co więcej, Gracz wybiera co poświęca a co dzieje się jak Gracz chce.
        * Gracz jest tym co kształtuje w którą stronę pójdzie historia, MG kształtuje okoliczności tej historii.
2. **Będziesz czuć smak zasłużonego zwycięstwa. Twój _performance_ ma znaczenie.**
    * Twoje deklaracje i decyzje mają znaczenie. Lepsza deklaracja -> większa szansa na sukces.
    * Wybór właściwych konfliktów i decyzje co i jak robić pozwolą Ci poprowadzić historię w stronę, która Ciebie interesuje.
    * Wybór właściwych okoliczności konfliktów zdecydowanie zmienia fabułę oraz szanse na sukces.
    * To Gracz wybiera co poświęca. To sprawia, że przy odpowiednio dobrych pomysłach masz więcej szans na osiągnięcie interesującego Cię wyniku.
    * Do każdego problemu można podejść na wiele różnych sposobów. Jak nie wyjdzie siłowo, spróbuj społecznie.
3. **Będziesz czuć frustrację, m.in. z niesprawiedliwości generatora losowego**
    * Przewaga jest po Twojej stronie. Plan był perfekcyjny. Trzy krzyżyki. Przeciwnik był silniejszy. Poddajesz konflikt, bo już nie warto tracić więcej. Niezasłużona porażka.
        * Jak w życiu, nie wszystko możesz wygrać, ale wygrasz to na czym Ci najbardziej zależy - jeśli poświęcisz **wszystko** inne. Jeśli nie jesteś skłonny poświęcić wszystkiego innego, nie zależy Ci na zwycięstwie na tyle - i tak powinno być.
            * Nawet, jak masz sporo krzyżyków pod rząd, w końcu się kończą w puli. A jeśli nie masz już nic do poświęcenia - poświęć postać, sojuszników itp. i weź autosukces (o tym potem).
    * Wszystkie testy są jawne. Nawet MG nie ma jak zmienić wyniku. MG może tylko zinterpretować go łagodniej, ale ma związane ręce jak chodzi o sukces czy porażkę.
    * Nie wiesz co Ci się nie uda. Czasem przegrasz prawie wszystko. Czasem musisz poddać konflikt - może nawet sesję. Nie jest tak jak w filmie, że "bohaterowie zawsze wygrywają".
4. **Nigdy nie przegrasz wszystkiego ani nie wygrasz wszystkiego**
    * Im więcej sukcesów, tym większa szansa na porażkę i vice versa. (mechanizmy catch-up)
    * Im większy sukces chcesz osiągnąć, tym bardziej musisz zaeskalować. To sprawia, że większość konfliktów to "sporo wygrywasz, coś przegrywasz"
        * Nawet przy szansach 50%-50% Ty decydujesz by poświęcić mniej istotne dla Ciebie rzeczy by pozyskać rzeczy dla Ciebie najważniejsze.

---
## 3. Poziomy mechaniki i problem "Wizard i Excel"

Z uwagi na zasady rządzące mechaniką:

* Istnieje zbiór podstawowych zasad, które zawsze działają i których użycie daje ci minimalną EZ.
* Im bardziej zależy ci na modelowaniu złożonych konfliktów, tym więcej musisz użyć zasad bardziej zaawansowanych.
* Czyli zasady możemy podzielić na kręgi. Rdzeń, w którym są zasady podstawowe i potem dodatkowe warstwy Które mogą zmieniać zasady Rdzenia.

Najprawdopodobniej najlepszy zapis zasad jaki może istnieć w tej sytuacji to:

* Zapisać zasady rdzenia najpierw
* Potem zapisać zasady dodatkowe jako mutatory zasad rdzenia.
* Mamy też do czynienia z zasadami alternatywnymi np. „nie masz woreczka żetonów” -> „więc będziemy mapować wszystko na jakąś planszę czy k20”.

Chyba najważniejszy koncept, jaki leży za tym sposobem wykorzystywania mechaniki, w jaki my to robimy, to „Wizard kontra Excel”.

* Wizard mówi. Zrób ten krok potem ten potem ten zawsze w ten sam sposób. Np wpierw rzut na trafienie. Jeżeli się udało to rzut na obrażenia.
    * --> Wizard jest dużo ważniejszy, jeżeli grasz z początkującymi - zawsze znasz następny ruch.
* Excel daje ci zbiór narzędzi i mówi, jak można je wykorzystać, ale w zależności od kontekstu, ty dobierasz sobie odpowiednie elementy Excela, by osiągnąć swój cel.
    * Np masz konflikt Tr+3, ale występuje 2 graczy, więc koloruję połowę ptaszków i krzyżyków na zielono i połowę na różowo. Różowe żetony reprezentują (gracza 1). Zielone ptaszki reprezentują (gracza 2). 
    * Jako podstawowe narzędzia mieliśmy: [żetony, ciągnięcie żetonów, kolorowanie żetonów]. Jako agregację w stylu „Excela” ja zdecydowałem (poza sztywnymi zasadami), jak zamodelujemy to przy użyciu dostarczonych klocków.
    * --> Excel daje PowerUserom możliwość zbudowania perfekcyjnego narzędzia na "silniku" w zależności od kontekstu. Kontrola i precyzja przy zachowaniu uczciwości i zwięzłości instrukcji.

Gdy ja myślę o mechanice, myślę o niej jak o Excelu, ale gdy będę rozpisywał mechanikę, rozpisuję ją, jakby była Wizardem.

---
## 4. Design Goals, enumerowane

1. **Zasady wiodące designem**
    * (1) **Każdy konflikt da się zamodelować**
    * (2) **Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.**
    * (3) **System intencyjny**
    * (4) **System negocjacyjny - coś wygrywasz, coś poświęcasz**
    * (6) **Nie ma możliwości, by osoba siedząca przy stole nie wpłynęła na fikcję**
    * (7) **Gracz ma kontrolę nad ryzykiem**
    * (8) **Catchup mechanism**
    * (9) **Chaos i nieprzewidywalność**
    * (10) **Nie da się przypadkiem spieprzyć fikcji**
2. **Parametry niefunkcjonalne**
    *  (11) **Mechanika jest szybka (gdy się wdrożysz)**
    *  (12) **Mechanika jest tania z perspektywy kosztu intelektualnego (gdy się wdrożysz)**
3. **Inne ważne aspekty**
    * (13) **Decyzje gracza poza fikcją mają znaczenie**
    * (14) **Manewry (deklaracje) gracza w fikcji mają znaczenie**
