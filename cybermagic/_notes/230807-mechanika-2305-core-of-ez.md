---
layout: mechanika
title: "Notatka mechaniki, 230807 - mechanika, rdzeń EZ"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

Co stanowi absolutnie minimalną część EZ? Jak prowadzić EZ by zachować rdzeń EZ?

---
## 2. High-level overview

Mechanika jest kodowana jako **2305**.

1. Wysokopoziomowe cele - przypomnienie
2. Input / Output Randomness
    1. Co to jest
    2. W kontekście RPG
    3. Czemu przesuwam wszystko do Input jak mogę
3. Minimum Viable EZ
    1. Generator losowy (rdzeń)
    2. Mechanizm eskalacji (rdzeń)
    3. Mechanizm negocjacji (rdzeń)
    4. Mechanizm odrzucania Krzyżyków / wyboru konsekwencji / poświęcania (rdzeń)
    5. Mechanizm kompensacji / catchup (wsparcie)
    6. Mechanizm inspiracji (wsparcie)

---
## 3. Wysokopoziomowe cele

Są opisane w [230603 - stabilizacja mechaniki EZ 2305](230603-mechanika-2305-stabilizacja).

Tym razem skupiamy się na podzbiorze:

1. **Zasady wiodące designem**
    * (2) **Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.**
    * (3) **System intencyjny**
    * (4) **System negocjacyjny - coś wygrywasz, coś poświęcasz**
    * (7) **Gracz ma kontrolę nad ryzykiem**
    * (8) **Catchup mechanism**
    * (9) **Chaos i nieprzewidywalność**
    * (10) **Nie da się przypadkiem spieprzyć fikcji**

---
## 4. Input / Output Randomness - framework analizy czemu to działa
### 4.1. Co to jest

Za artykułem: http://www.niallcrabtree.com/blog/input-randomness-vs-output-randomness

```
Input randomness is when something random happens, and then a player can choose what to do with the information after the fact. An example of this is when you draw three cards from a deck, and you can choose which one to keep; the three cards you get to choose from were selected randomness, but now once you pick one of those three cards, the outcome of that choice is guaranteed. 

Output randomness is when the player does something, and the outcome of that is random. An example of this is when you choose to attack a monster, and you roll dice after making that decision to see whether or not you were successful. ​
```

Popatrzmy na dwie karty, lewą z Hearthstone i prawą z Magic: the Gathering.

![Arcane Missiles vs Magic Missiles](Materials/230807/01-230807-input-output-randomness.png)

* W obu grach Ty ciągniesz karty co turę i nie wiesz co dostaniesz (input randomness)
* Jeśli masz Arcane Missiles, nie wiesz co się stanie (output randomness)
* Jeśli masz Magic Missiles, dokładnie wiesz co się stanie - to co chcesz (brak output randomness)

Innymi słowy, kwestia jest **po której stronie decyzji jest element losowy**.

Jeśli na stole są trzy stwory:

* "Rzucam Magic Missiles i chcę zabić stwora 2/1 oraz 2/2" -> decyzja podjęta, wynik jest znany.
* "Rzucam Arcane Missiles i chcę zabić stwora 2/1 oraz 2/2" -> decyzja podjęta, ale nie wiemy czy się udało.

EZ celuje w przesunięcie większości elementów losowych w stronę **input randomness** a nie **output randomness**.

### 4.2. Input, Output i RPG

Weźmy popularny system RPG. Nie wiem, D&D / Warhammer / Oko Yrrhedesa.

* Gracz deklaruje "chcę, by mój elf pociął orka na plasterki"
    * DECYZJA jest podjęta
* Generator losowy powiedział "porażka, nie trafiasz"
    * BRAK MOŻLIWOŚCI PODJĘCIA DECYZJI
    * output randomness

Weźmy EZ.

* Gracz deklaruje "chcę, by mój elf pociął orka na plasterki"
    * DECYZJA jest podjęta
* Generator losowy powiedział "porażka"
    * I co dalej? Mimo porażki, dalej jest możliwość wejścia w interakcję z decyzją.
    * Gra się dopiero **zaczyna**, każdy pojedynczy konflikt ma znaczenie, można go eskalować itp.

Czemu to istotne? Popatrzcie na dokładnie tą samą sytuację, ale nieco inaczej.

### 4.3. EZ: przesuwa tak dużo Output Randomness w Input Randomness jak się da

Po ludzku "dostajemy wynik losowy i decyzje są POTEM. Decyzje nie ulegają losowości."

Pokażę skrajny przykład dla demonstracji zjawiska. Z konkluzją na dole.

KONTEKST:

* Mistrz Gry się pomylił. Wprowadził do gry _Wielkiego Inteligentnego Czerwonego Smoka_, ale Smok jest zamaskowany jako poczciwy Króliczek.
* Gracze mają niewyszkolone postacie.

Popularny system RPG:

* **Gracz** deklaruje "dobra, atakuję Króliczka"
* **MG**: "musisz osiągnąć 20/1k20 by trafić i zadać obrażenia; Króliczek ma 999 życia"
* **Gracz**: "wtf"
* **Gracz**: dobra, atakuję. (trafił, zadał jakieśtam obrażenia)
* **MG**: Króliczek oddaje. Strzela wiązką plazmy. Zadaje... (rzuca 10k20) 184 obrażeń
* **Gracz**: (moja postać nie żyje) (nic nie mogę zrobić) (trzeba było iść do wojska)

Zauważcie, że w powyższym przykładzie **nie było żadnych decyzji po stronie Gracza**:

1. Nie wie, że Króliczek to nie Króliczek
2. Nie ma jak obronić się przed atakiem Smoka
3. Jeden błąd MG zabija postać gracza. Chyba, że MG oszukuje na kościach albo nie aplikuje mechaniki.

A teraz ta sama sytuacja na EZ:

* **Gracz** deklaruje "dobra, atakuję Króliczka"
* **MG**: "co chcesz osiągnąć? Nie jesteś w stanie go zabić, nawet Heroicznym konfliktem"
    * _działanie intencyjne_
* **Gracz**: "dobra, zachodzę mu drogę; blokuję go, bo chcę uniemożliwić mu ucieczkę"
    * _specjalnie to wybieram, by wybrać najgłupszy wariant z perspektywy sytuacji_
* **MG**: Nie konfliktuję - udało Ci się to osiągnąć automatycznie.
* **MG**: Masz uwagę Króliczka. Króliczek strzela w Ciebie wiązką plazmy spopielając Cię na miejscu.
    * _deklaracja MG może być całkowicie nieuczciwa, jeśli Zespół pamięta zasady_
* **Gracz**: "czekaj, co?! Konfliktuję!!!"
    * _każda deklaracja MG może być skonfliktowana jeśli jest jakakolwiek możliwość_
* **MG**: Dobrze. Króliczek strzela wiązką plazmy. Jesteś zaskoczony. Chciałeś odciąć mu drogę ucieczki. Konflikt na pewno jest rzędu Ekstremalnego.
* **Gracz**: Nie akceptuję śmierci postaci.
    * _zakres stawek jest znany przed konfliktem; to jest stawka znana przed sesją_
* **Gracz**: Mam prawo zorientować się, że coś jest nie w porządku. Skoro byłem gotowy do odcinania drogi Króliczkowi, jestem też skupiony na nim i mogę coś zauważyć.
    * _sępienie bonusów, czyli zasada 'deklaracja w fikcji ma znaczenie_
* **MG**: Akceptuję. Ekstremalny, ale z jedną Przewagą i masz +3.
* **Gracz**: o co w sumie walczymy?
* **MG**: stan aktualny jest taki - Króliczek chce Cię spopielić oraz masz jego pełną uwagę.
* **Gracz**: ok. Więc chciałbym uniknąć płomienia ORAZ stracić uwagę Króliczka. Niech robi to co Króliczki robią. Daleko ode mnie.
    * _stawka jest znana przed konfliktem, jawnie lub domyślnie_
    * _w wypadku w którym jedna strona jest niepewna co się dzieje, wpierw odbudowujemy kontekst_
* **MG**: Więc - (Ex P +3)
* **Gracz**: Ciągnę... (porażka)
* **MG**: **Podbicie**. Proponuję "Plazma Cię dotknęła. Jesteś bardzo poparzony, potrzebujesz pomocy medycznej, ale nie natychmiastowo."
    * _MG nie deklaruje wyniku. MG proponuje wynik. Gracz ma decyzję i może go wymienić na coś innego._
* **Gracz**: Ok. Ciągnę dalej. (porażka)
* **MG**: **Manifestacja**. Proponuję takie zakończenie - "Udało Ci się uskoczyć, ale nie do końca. Plazma Cię poparzyła. Jesteś ciężko ranny, wszystkie łatwopalne rzeczy są zniszczone, jesteś wyłączony z akcji, ale ktoś przyjdzie Cię uratować zanim zginiesz w płomieniach. Króliczek pójdzie sobie dalej."
    * _MG optymalizuje opowieść; nie ma po co zabijać postaci Gracza_
    * _MG wie, że Gracz chce, by jego postać wyszła na przekozaka w okolicy; taka interpretacja uderza bezpośrednio w jego główny cel._
* **Gracz**: Mam inną propozycję. Może być tak, że Króliczek podpalił las? Zagraża to lokalnym elfom. Będę eskalował dalej by uniknąć.
    * _Gracz ma prawo odrzucić Krzyżyk. Gracz ma prawo wymienić Krzyżyk na inny o podobnej skali lub większej. Bardziej dotkliwy dla siebie._
    * _Z perspektywy MG, Gracz nie ma nic wspólnego z lokalnymi elfami, więc Krzyżyk nie jest bardziej dotkliwy._
* **MG**: W jaki sposób jest to dla Ciebie dotkliwe? Mam inną propozycję - niech Króliczek będzie rozjuszony Twoimi ruchami i zemści się na pobliskich osadach ludzkich - i oni wiedzą, że to wina Twojej postaci.
* **Gracz**: No dobra, akceptuję...

Popatrzcie, że w sytuacji powyżej Gracz **nigdy** nie jest w sytuacji "terminalnej". MG wie jak daleko się posunie, oboje wiedzą, że postać Gracza nie będzie stracona i nawet gdy MG się niezwykle pomylił (zamaskowany smok za króliczka) to są pewne granice. 

Innymi słowy, **mniej istotne jest to jak dostajemy wynik Sukces/Porażka/Entropia, dużo ważniejsze jest co robimy z tym wynikiem**. Gra się **zaczyna** od wyniku i decyzje podejmowane są właśnie na tym wyniku. Nie _output randomness_ a _input randomness_.

Mając to, przejdźmy do poszczególnych rdzennych komponentów EZ, czyli "minimum viable EZ".

---
## 5. Minimum Viable EZ
### 5.1. Generator losowy (rdzeń)

Na potrzeby przykładu, weźmy najprostszy możliwy generator losowy. 1k20.

* `1-10` to **Porażka (Krzyżyk)**
* `11-20` to **Sukces (Ptaszek)**
* **Entropia** jest reprezentowana przez skrajności, zaczynając od górnych
    * pierwszy Entropiczny żeton to `20`, drugi `1`, trzeci `19`, czwarty `2`...

Czyli odpowiednik konfliktu z magią by wyglądał tak:

* `1`: Paradoks (Porażka Magiczna)
* `2`: Efekt Skażenia (entropiczny magiczny)
* `3-10`: Porażka
* `11-18`: Sukces
* `19`: Efekt Skażenia (entropiczny magiczny)
* `20`: Soczewka (Sukces Magiczny)

Czy to probabilistycznie poprawne? Nie. Czy to da nam interesujące odpowiedzi? Tak. Dokładne mapowanie mechanizmu losowego zrobię w innej Notatce. Na razie to daje nam wszystkie potrzebne odpowiedzi.

Jakim kosztem? **Decyzje graczy w świecie gry i pozycjonowanie pionków nie mają znaczenia**. Nieważne czy postać jest "silna" czy "słaba", nieważne czy plan był "dobry" czy "kiepski".

Ale na potrzeby przykładu _good enough_.

Nie istnieje EZ bez Generatora Losowego. Całość EZ to zarządzanie ryzykiem i decyzje w obliczu (V), (X), (Ox). Żaden z następnych mechanizmów po prostu nie działa. EZ **wymaga** chaosu pochodzącego z generatora losowego:

![Rola RNG w EZ i innych systemach](Materials/230807/02-230807-rola-rng-rpg.png)

### 5.2. Mechanizm eskalacji (rdzeń)
#### 5.2.1. Wyjaśnienie

Jako, że każdy (V) lub (X) w rzeczywistości nie stanowią **rozwiązania akcji** a **poszerzenie kontekstu**, do rozwiązania akcji potrzebujemy określenia jak daleko postacie graczy się posuną czy co są skłonne postawić.

To sprawia, że ani (V) ani (X) nie są blokujące, dopóki wszystkim stronom zależy na tym, by coś móc osiągnąć w konflikcie.

Eskalacja jest rdzeniem EZ, bo właśnie Eskalacja i możliwość "posunięcia się dalej" jest tym, co zmienia _output randomness_ w _input randomness_. Dzięki Eskalacjom, Gracz może pójść dalej, osiągnąć więcej - lub stracić mniej. Jak w przykładzie ze Smokiem przebranym za Króliczka, Gracz może coś przyjąć lub powiedzieć "chcę zagrać o jeszcze wyższą stawkę".

#### 5.2.2. Przykład

Dobrze schowany Zespół monitorujący ruchy patroli przy użyciu noktowizorów i jednej smutnej drony czeka, aż dwuosobowy patrol będzie poza zasięgiem widzenia kogokolwiek. Leon i Amanda wysłani, by przechwycić tą dwójkę. Cel - wzbudzić terror, porwać kilka osób, najlepiej w sposób niezauważony.

* V (pierwszy sukces): udało się przechwycić patrol...
    * _sukces operacji_
* V (drugi sukces): ...i nikt niczego nie zauważył
    * _pierwsza eskalacja, dodanie okoliczności dodatkowej: nikt nic nie wie_
* V (trzeci sukces): ...a dodatkowo udało się złapać pojedynczego wartownika który poszedł się odlać
    * _druga eskalacja: celem było wzbudzeniem TERRORU. I to jest skuteczne, bo prawie ze środka obozu udało się porwać osobę która miała pilnować innych. Przekracza to oryginalny konflikt, ale dalej kontynuuje intencję._

### 5.3. Mechanizm negocjacji (rdzeń)
#### 5.3.1. Wyjaśnienie

To trudno złapać, ale jest bardzo istotne. W EZ kluczowe jest to, by to nie MG mówił "będzie TAK" a wynik "będzie TAK" było pochodną wspólnie negocjowanej fikcji.

Chodzi o to, by wszystko co nie jest potwierdzone konfliktem mogło ulec zmianie w fikcji. "Say yes or roll the dice", ale z tą różnicą że nawet wynik rzutu kością ulega dalszym negocjacjom, pokazuje tylko która strona osiągnęła konkretną przewagę.

Gracz nie jest jedyną osobą, która negocjuje. Mistrz Gry też negocjuje wynik fikcji.

#### 5.3.2. Przykład

Zademonstruję to na przykładzie poniższej sceny:

* Amanda, postać Gracza, próbuje się rozpaczliwie wycofać i zgubić ślad
* Amanda ma ograniczone surowce, ale jest wyszkoloną komandoską z ppanc działem snajperskim i w servarze klasy Lancer
* Przeciwnik nie ma pojęcia z czym dokładnie ma do czynienia, to raczej cywile polujący na rozbite postacie takie jak Amanda

Scena:

* Kontekst
    * Próbując wyciągnąć koleżankę, Amanda została tyci za długo i polują na nią psy
    * Amanda wytycza trasę na podobno bezpieczne miejsce, ale psy ją gonią
    * **Gracz** pyta czy jest coś czym może zrzucić z siebie psy. Coś potencjalnie niebezpiecznego.
    * **MG** mówi, że jest tu gniazdo zmutowanych szerszeni
    * Amanda deklaruje, że przechodzi przez gniazdo szerszeni.
* Sytuacja
    * Amanda wie, że psy nie zbliżą się do szerszeni, ale ona może rzucić w psy gniazdem szerszeni
    * Amanda ma pancerz plastalowy. Szerszenie go nie spenetrują.
* Akcja
    * Amanda rzuca w psy gniazdem szerszeni, próbując maksymalnie rozjuszyć szerszenie

Wynik konfliktu:

Tr Z +4:

* V (p): Amanda bezpiecznie aktywuje szerszenie przeciwko psom
* X (p): **Jeden samotny awian leci w tą stronę, by zobaczyć psy i co tu się dzieje**
* V (m): Amanda usuwa psy oraz awian liczy na glorię samemu

Podkreśliłem (X). Celem Gracza było:

* pozbyć się zagrożenia ze strony zmutowanych psów **teraz**
* nie stracić kluczowych zasobów
* zgubić przeciwnika

Moim celem było:

* nakierować przeważające siły na Amandę
* w wyniku -> pojmać Amandę

Więc **w wyniku** tego zestawu testów usunąłem psy "z planszy", ale wstawiłem pojedynczego awiana. Jeśli Amandzie uda się usunąć awiana **zanim** ten wezwie posiłki, jej cel będzie spełniony.

Amanda, w odpowiedzi, weszła w środek gniazda szerszeni ze swoją snajperką i wycelowała w stronę z której nadlatuje awian.

I tak potoczył się ciąg dalszy "negocjacji przy użyciu mechaniki":

Amanda - ukryta z ciężką snajperką - próbuje zestrzelić awiana, by wyglądało na wypadek. To powinno ich zniechęcić a przynajmniej kupić jej dość czasu.

* X: niestety, nie wygląda na wypadek
* V: awian został zestrzelony

Amanda spiernicza dalej. Amanda wie, że na otwartej przestrzeni na pewno ją znajdą; szuka czegoś, co da jej jakąś osłonę.

* Vz: Amanda znalazła dobry teren; jest coś na kształt wielkich drapieżnych krystalicznych jastrzębi? Dużo ich. Ale da się pełznąć _pod_ nimi - jest duża szansa, że awiany nie odważą się tam zbliżyć.
    * Więc Amanda poszła tamtą drogą

Amanda ZGUBIŁA awiany, a przynajmniej tymczasowo.

**Widzicie "przeciąganie liny" oraz krzyżowe decyzje Gracza i MG, gdzie mechanika pokazuje która strona tym razem dostaje to czego chce?** To w połączeniu z Eskalacjami i manewrami taktycznymi daje możliwości określenia co dokładnie się dzieje, które ryzyka brać na siebie i które lepiej odrzucić. Nigdy nie wygrasz wszystkiego, zawsze wygrasz coś.

### 5.4. Mechanizm odrzucania Krzyżyków / wyboru konsekwencji / poświęcania (rdzeń)
#### 5.4.1. Wyjaśnienie

Najsilniejszy mechanizm komplementarny do Mechanizmu Negocjacji i najlepiej widoczny aspekt negocjacji. 

Ogólnie, **każda strona ma prawo odrzucić konsekwencje Krzyżyka lub Ptaszka** jeśli zaproponuje inne konsekwencje, tak samo lub bardziej dotkliwe **dla postaci, celu Gracza lub wektoru historii Gracza**.

Ten mechanizm sprawia, że MG może naciskać bardzo mocno, bo Gracze mają możliwość odparcia ataku MG biorąc inne malusy na siebie. Ten mechanizm też sprawia, że MG może być lepszym kustoszem wspólnie budowanej opowieści, bo jeśli Gracze mają ogromny, katastroficzny sukces - MG może zaproponować im coś jeszcze lepszego by nie zniszczyć całkowicie czegoś kluczowego.

Zaznaczam, że to zakłada "sensownych" Graczy i "sensownego" MG, którzy wszyscy optymalizują jak najlepszą historię. Dlatego prowadzę w systemie intencyjnym. Tam dużo trudniej trollować i zawsze wracamy do podstawowego komponentu - "do czego dąży Twoja deklaracja / Twoje aktualne ruchy, powiedz mi czego pragniesz i mogę zaproponować Ci coś co Cię do tego zbliży."

Zauważcie też, że **dzięki temu mechanizmowi ruchy Graczy przesuwające taktycznie pionki w fikcji w rzeczywistości zwiększają statystycznie ilość rzeczy jakie dostają i redukują statystycznie rzeczy które poświęcają**. 

Innymi słowy: 

* jeśli dzięki manewrom i prawidłowemu używaniu fikcji Gracze przesuwają konflikty z Ekstremalnych na Trudne 
* to przy średnim prawdopodobieństwie 31% dla Ex+2 i 50% dla Tr+2, 
* dla 10 testów 
    * "poświęcają" nie 7 a 5 rzeczy 
    * "wygrywają" nie 3 a 5 rzeczy.

Dlatego ten mechanizm jest absolutnym kluczem do EZ. Selekcja takiego podzbioru historii, który da Ci większość tego co chcesz dostać i poświęca większość tego, na czym Ci mniej zależy.

#### 5.4.2. Przykład

* Kontekst
    * Amanda odzyskała dwie 'siostry' - Aynę i Xaverę
    * Serwopancerze (servary) są bytami krótkodystansowymi, wymagającymi ładowania. A są już długo poza ładowarkami
* Sytuacja
    * Amandzie udało się - kosztem jej servara - odzyskać _supply crate_ gdzie są baterie do servarów ich klasy
    * Ayna, najbliższa inżyniera z tego co mają, zabiera się za naprawdę servara Xavery - jedynego który jeszcze jakoś działa, kanibalizując resztki części servarów Ayny i Amandy.

Ayna nie ma fabrykatora, ale może coś zrobić. Niestety, te servary uległy pewnej degradacji - nie są zaprojektowane do działania długoterminowego w takich warunkach.

Tr Z +3 +3Og:

* X: Bardzo wolna operacja. 
    * _To plus ogień dają mi wprowadzenie innej frakcji. **Gracz się zgadza**._
* V: Xavera może opuścić servar bez jego niszczenia; mogą sprawiać wrażenie, że jest ich cztery a nie trzy. Mają prosty automat. 
    * _Tu Gracz eskaluje - niech Aynie się uda naładować servar Xavery. Jeden servar to coś._
* X: 
    * _Moja propozycja: Zespół zostanie zaskoczony przez wrogą frakcję._
    * _Gracz ma kontrpropozycję - to Furie (Amanda, Xavera, Ayna) zaskoczą przeciwnika, ale Xavera nie ma dostępu do servara. Zgadzam się, utrata servara jest katastrofalnym ciosem w zasoby._
    * WIĘC: Xavera jest poza servarem, jest przezbrojona, Furie nie marzną, mają servar ale jako prosty automat.

### 5.5. Mechanizm kompensacji / catchup (wsparcie)
#### 5.5.1. Wyjaśnienie

Im bardziej nie wychodzi, tym łatwiej będzie zaeskalować sukces. Im bardziej wychodzi, tym większa szansa że będzie porażka.

Jeśli mamy w puli żetonów `11X 5V` (Ex+2), to gdy wyjmiemy jeden żeton porażki (X), zostaje w puli `10X 5V`. Czyli szansa na sukces zmienia się z 5/16 (31%) na 5/15 (33%). Jeśli teraz wyjmiemy żeton sukcesu, w puli zostaje `10X 4V`, czyli szansa na sukces spada do (28%).

To sprawia, że eskalacje mają tendencję do _limes -> 50%_. Im wyżej eskalujesz, tym większa szansa, że coś pójdzie nie tak i wygrywasz część a część przegrywasz.

#### 5.5.2. Przykład

Jak w wyjaśnieniu, tu nie ma co pokazywać.

### 5.6. Mechanizm inspiracji (wsparcie)
#### 5.6.1. Wyjaśnienie

Kolory i Entropia. Możliwość zrzucenia jeszcze wyższego poziomu decyzji na RNG.

#### 5.6.2. Przykład

Mamy następującą sytuację:

* Olgierd próbuje: 
    * ratować załogantów z płonącego statku kosmicznego
    * odeprzeć piratów próbujących dostać się na pokład jego jednostki

Modeluję ten konflikt w następujący sposób:

* Tr +2: `8X 6V 2Vr`
    * `4X` koloruję na czerwono; one reprezentują rany i straty wśród załogantów
    * `4X` koloruję na zielono; one reprezentują piratów, którzy dostali się na pokład jego jednostki
    * `6V 2Vr` zostawiam graczowi Olgierda do decyzji co z tym robi

Dzięki temu mam prostszą interpretację co się dzieje.

Załóżmy, że potrzebuję `3V` do osiągnięcia wszystkich celów. Ciągnę do momentu 3V.

![Interpretacja sigili](Materials/230807/03-230807-interpretacja-sigili.png)

Widzę, że doszło do pewnych strat wśród załogantów i niewielka część piratów dostała się na pokład. Nic szczególnego w żadnej ze stron.

Więcej wyjaśniam o żetonach, kolorach itp w notatce [230603-mechanika-2305-stabilizacja](230603-mechanika-2305-stabilizacja.md)
