# Analiza

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić, Fox

---
## 1. Purpose

* O co gra MG?

---
## 2. Spis treści i omówienie

1. Co mamy z perspektywy zarządzania sesji
    1. Opowieść o (Theme & Vision)
    2. Dark Agenda i Motive Split
    3. Dark Future
2. Co dodajemy bo warto bo Kić ma rację nie wiedząc o tym
    1. "O co gra MG" (Processor's Profits?)
    2. To sprawia że Dark Future -> Default Future
    3. Ujawnienie Faz sesji

---
## 3. Krótka myśl
### 3.1. Co mamy

Sesje już teraz zawierają ważny komponent w którą stronę PRZESUWA SIĘ ŚWIAT. To są Motive-Split, Dark Agenda oraz Dark Future:

**"Opowieść o"** jest sekcją skupiającą się na pokazaniu WIZJI oraz KIERUNKU sesji. Jakiego typu uczucia należy wywołać? Jaki motyw podkreślać? Co mają gracze zauważyć?

```md
* Opowieść o (Theme and vision): 
    * "Dążenie do spełniania marzeń - obsesja na tym punkcie - skończy się katastrofą. Niezależnie od celu. Było warto?"
        * Ojciec, chroniący córkę przed magią - by tylko ją alienować od siebie za wszelką cenę
        * Arystokrata, pragnący integracji biosyntów - by patrzeć jak jego biosyntka umiera chroniąc go przed tłumem
```

**"Motive-Split"** a już w ogóle **"Dark Agendas"** pokazują w którą stronę naciskają FRAKCJE lub SIŁY znajdujące się na sesji. Pokazują jak się zachowają i kiedy wygrywają. Jednocześnie pokazuje motywy od których można się odbić jak nie ma się pomysłu na odpowiedni (X) lub na to co się stanie na sesji.

```md
* the-protector: Estril Cavalis, który pod wpływem Pierścienia chce zamknąć Aerinę w złotej klatce. Nic złego jej się nie stanie. Magia NIE wróci.
* dark-temptation: Pierścień oferuje spełnienie marzeń - ale zabiera Ci życie i energię.
* wrobieni-lecz-niewinni: Mawirowcy. Wycofali się w głąb Stacji, nie są już częścią większej grupy. Są poza prawem i Felina nie chce ich atakować.
```

**"Dark Future"** jest mechanizmem "nie mającym agend", jest to spojrzenie z lotu ptaka pokazujące przyszłość rzeczywistości jeśli gracze niczego nie zrobią. To jest dowód na to, że obecność graczy ma znaczenie.

```md
Dark Future

* Na Trzęsawisku powstanie śmiertelnie niebezpieczna broń artyleryjska, zagrażająca Zaczęstwu i awianom.
```

### 3.2. Co jest nowego

Kić zauważyła, że jest jeszcze jeden interesujący wymiar którego tu nie ma i który ma sens i który ma znaczenie. **O co gra MG**.

Wyobraźmy sobie następującą sesję – Infernia ma na pokładzie bombę. Bomba jest świetnie ukryta. Podłożyła ją niejaka Maja Nagrząbek.

* **Dark Agenda**: the-saboteur / the-devastator
* **Dark Future**: Infernia ginie w nuklearnym ogniu
* **Opowieść o**: jak zranione uczucia o miłości do Eustachego Mai niszczy Infernię.

Ale prawda jest taka, że MG **nie ma umocowania** do zniszczenia Inferni. Infernia na tej sesji nie zostanie zniszczona - to wiemy wszyscy. To nie jest sesja ze zdjętymi flagami.

Więc - o dziwo - żaden z mechanizmów powyżej nie pomaga MG w prawidłowym formułowaniu konfliktów. To, że **ta sesja** uniemożliwia zniszczenia Inferni nie zmienia faktu, że Maja będzie próbowała to zrobić. MG wie jak sterować sesją, ale nie ma pojęcia jak stawiać konflikty i o co gra.

Dodajmy więc nowy mechanizm. "O co gra MG". Czyli "Dark Future, ale z realistycznej perspektywy MG. Tak, WIEM, że zniszczysz Infernię na piątym (X) ale WIEMY też że to się nie stanie więc nie udawaj".

```md
* **O co gra MG**
    * Maja jest uznana za ofiarę Inferni, Eustachego i Arianny
    * Maja umiera i jej śmierć jest spowodowana (przynajmniej percepcyjnie) za działania Eustachego
    * Będzie kontrola na Inferni - jak doszło do wprowadzenia BOMBY na Infernię?!
    * Sporo osób na Inferni sympatyzuje z Mają, Eustachy jest osobą przed kim załoga raczej chroni dziewczyny, bo "jego magiczny magnetyzm".
```

I nagle mamy PIĘĆ mechanizmów jak można zarządzać sesją, (X), (V) itp.

* **Opowieść o** (theme & vision) 
    * pokazuje uczucia, motywy i co gracze mają zobaczyć. Temat. "What rhymes".
* **Dark Agenda** i **Motive Split** 
    * pokazują ruchy przeciwników, sposoby i co warto pokazać. Odblokowuje MG ruchy.
* **O co gra MG (Antagonistic Agenda?) (Processor's Profits?)**
    * to o czym mówimy teraz
    * to co MG faktycznie CHCE wywalczyć i CHCE wygrać
    * jeśli MG nie ma nic co chce wygrać, jest pasywnym procesorem i w sumie może jedynie symulować Default Future i Theme&Vision.
    * (przykład: sesja "Cykl Krwawej Prządki" -> Magda, dla Fox i Kić -> grałem o ZAPEWNIENIE CIĄGŁOŚCI CYKLU dla Pająka i kusiłem "zabij i nakarm PAJĄK" by móc dodać (Or) do puli.)
* **Default Future**
    * w tym momencie raczej Default niż Dark, pokazuje siłę frakcji i jeśli trzeba / można przesunąć czas - które siły poruszą się jak i jak to się skończyłoby bez graczy.
* **Fazy gry**
    * co konkretnie dzieje się w głównych fazach przy założeniu Default Future.
    * jeśli faza ma ~1h, to 4h sesja ma 3 fazy, gdzie ostatnia odpowiada DECYZJOM I ATAKU NA SUKCES - patrz notatka [240304: O początku sesji](240304-o-poczatku-sesji.md)
