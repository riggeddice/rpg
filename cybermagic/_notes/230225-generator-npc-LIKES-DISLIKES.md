## 1. Concerns

To jest żywy dokument identyfikujący problem i podający wizję potencjalnego rozwiązania. Będzie modyfikowany w trakcie dyskusji. Na końcu zostawimy "IMPLEMENT" lub "WONTFIX".

## 2. Aktualny problem z generatorem

Popatrzmy na typową postać z generatora NPC w wersji multiline

* **Name**: Alicja
    * Ocean: ENCAO:  0+-+0
        * GDZIE: Extraversion (siła reakcji na pozytywne bodźce), Neuroticism (siła reakcji na negatywne bodźce), Conscientousness (kontrola emocji, nałogów itp), Agreeableness (empatia i spolegliwość), Openness (szerokość skojarzeń i linków)
        * TRAITS:
            * Wiecznie zagubiony, nie robi tego co jest potrzebne
            * Poświęcił całą noc na hobby, więc jest niewyspany rano
            * Przymilny, taki trochę lizus
            * Pomaga innym którym potrzebna jest pomoc
            * Napięty i znerwicowany
    * Wartości:
        * TAK: Self-direction, TAK: Face, TAK: Power, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Opus Magnum: stworzyć wielkie dzieło, zrobić coś najdoskonalszego czego niekoniecznie zrozumieją inni.
        * TAK: Impress the superior / senpai: zrobić coś by osoba wyżej mnie doceniła i pogłaskała lub zobaczyła moją wartość!
        * NIE: Broken soul: zostawcie mnie wszyscy w spokoju. Nie chcę wiedzieć, nie chcę interaktować, nie chcę musieć. Za dużo. Dążę do samotności i ciszy.
        * NIE: Survival of the fittest: przetrwają najlepiej dostosowani. Naszą rolą jest ekspozycja i zwycięstwo.
    * Potencjalne prace:
        * Technik hydrolog
        * Garderobiana
        * Lekarz – specjalista pediatrii

lub oneline jak lubię (to ta sama postać ale po kilku skasowaniach mieści się w jednej linijce raportu)

* Alicja || ENCAO:  0+-+0 |Wiecznie zagubiony, nie robi tego co jest potrzebne;;Poświęcił całą noc na hobby, więc jest niewyspany rano;;Przymilny, taki trochę lizus;;Pomaga innym którym potrzebna jest pomoc;;Napięty i znerwicowany| VALS: Self-direction, Face, Power >> Tradition, Humility| DRIVE: Opus Magnum, Impress the superior / senpai

Dla **mnie** to oczywiste jak prowadzić taką Alicję.

* zagubiona, raczej nieśmiała, unika konfliktu, łatwo płacze i się stresuje
* wallflower, ale lubi ludzi, z nimi przebywać i im pomóc, choć boi się, że ją zranią / wyśmieją
* ma 'ja siama', dba o swoją pozycję i obsesyjnie się martwi że ową pozycję straci
* chce zrobić coś wielkiego lub komuś coś pokazać

Okazuje się, że **tylko ja** umiem to powyżej czytać i wykorzystywać. A nawet ja z uwagi na moje filtry w mózgu skupiam się na pewnych aspektach postaci i nie zawsze zagram nią prawidłowo.

WNIOSEK: potrzebne jest coś co przekształci OPIS / KONSTRUKCJĘ NPCa na UŻYWALNEGO NPCa. Najlepiej tak, bym nie był jedynym co tego umie użyć. Choćby do opowiadań, nie wiem.

_"Batman jakiego robisz nie jest Batmanem, jeśli nie pocieszy umierającej na raka dziewczynki. Jeśli zabierzesz ten aspekt Batmana, stanie się bliższy Punisherowi." - a random guy from the internet_

## 3. Inspiracja - jak to można rozwiązać

Shadow of the Forgotten Gods (ta gra o której Kić mówi że (żółw dużo w nią gra i niszczy świat), konkretnie gra bogiem imieniem Iastur, Władca Szaleństwa).

To jest bohater, czyli "postać przeciwnika". Mój oponent. To, co chce uratować świat:

![Bohater, który lubi rzeczy](Materials/230225/01-shadows-of-gods-mage-likes.png)

Zwróćcie uwagę, że czarodziejka Dicibem Asciur: 

* lubi: [jakąś_religię, jakiegoś_mediatora] 
* nie lubi [Gold, Combat]. 

Po prawej stronie widzicie, jakie akcje chce wykonywać metodą ruletkową. 

* Czyli rzuca 1k(91+19+19+19+15+5+5) i wybiera ten wariant, który "wypadnie". 
* Czyli szansa, że pójdzie trenować jest jak 91/suma(all), czyli 91/173, czyli 52%.
* Szansa, że pójdzie tłuc bandytów wynosi 19/173, czyli 11%
* BARDZO upraszczam faktyczną mechanikę gry, ale tak o tym można myśleć i jest to wystarczający model do dalszej analizy (dlatego liczby nie będą identyczne z tym co piszę)

Oki, w jaki sposób są nadawane te wagi akcji?

![Jak nadawane są wagi akcji](Materials/230225/02-shadows-of-gods-mage-actions.png)

* Jest to suma wynikająca z modyfikatorów
* poniżej jest to co ciekawe. 
    * Popatrzcie na lewą dolną stronę rysunku.
    * Positive Tags: "Combat, Cruelty, Danger"
        * czyli te tagi korelują z tą umiejętnością
        * jeśli postać LUBI Combat, ma +20. NIE LUBI, ma -20.
            * dokładnie to samo dotyczy Negative Tags tylko na odwrót.
        * jeśli postać BARDZO LUBI Combat, ma +60. BARDZO NIE LUBI, ma -60
    * Popatrzcie na Motivations ("środek rysunku")
        * "Menace" oznacza "postrzegane zagrożenie dla świata". Czytaj, smok ma 100 a szkielet ma 10. To jakaś baza, z naszego punktu widzenia (nasi agenci robiąc rzeczy akumulują menace, więc im więcej robią tym bardziej herosi chcą ich bić)
        * Nasza czarodziejka ma Dislike Combat
        * Gdyby miała zamiast tego Like Combat, dostałaby +20 do akcji zamiast -20, więc wynik nie byłby '32' a '72', co by się zamapowało na jakieś '31' czy coś a nie '19'.

Czyli by sprawić, że bohater coś zrobi, musimy zmienić jego charakter. Szczęśliwie, Iastur jest dokładnie bogiem zajmującym się takimi rzeczami.

![Jestem Iastur, zmieniam życia ludzi](Materials/230225/03-shadows-of-gods-iastur-influence.png)

Zrobiłem coś takiego - obniżyłem jej chęć do współpracy (Cooperation) i podniosłem jej chęć do okrucieństwa (Cruelty) za 2 punkty mocy:

![Nowa, lepsza czarodziejka](Materials/230225/04-shadows-of-gods-new-tags.png)

To sprawiło, że zmieniły jej się cele i to do jakich rzeczy chce dojść i co chce robić.

![Nowa, lepsza czarodziejka](Materials/230225/05-shadows-of-gods-new-actions.png)

Nowa, lepsza czarodziejka:

* Jej aktualna chęć Combat Banditry wynosi 52 przed modyfikatorami (a było 32)
    * co mapuje się na 32 na powyższym rysunku
* Pojawiła jej się nowa preferowana akcja - Sabotage Ruler (było 0, nagle jest 20)
    * positive: [cruelty], negative: [cooperation], a oba jej dodałem

Innymi słowy:

* Akcje mają bazowe MENACE oraz skorelowane TAGI
* Każda postać ma własne TAGI w formie LIKE/DISLIKE
* Manipulując TAGAMI zmieniasz zachowanie postaci

(tak, warto było grać w Shadows of the Forbidden Gods by zobaczyć tak eleganckie rozwiązanie co daje efekt godny X-Com jak chodzi o emergentne opowieści)

## 4. Wizja - jak do tego można podejść

Wracam do najważniejszego zdania.

_"Batman jakiego robisz nie jest Batmanem, jeśli nie pocieszy umierającej na raka dziewczynki. Jeśli zabierzesz ten aspekt Batmana, stanie się bliższy Punisherowi." - a random guy from the internet_

Czyli to co różni Batmana od Punishera to:

1. zbiór skilli i strategii (Batman lata na linie, Punisher używa broni)
2. osobowość, ekspresja osobowości i "charakter"
3. zasoby, pozycjonowanie i preferowane strategie

Załóżmy taką formę generacji:

1. Generujemy ENCAO niezależnie (Extraversion, Neuroticism...)
2. Generujemy Wartości zależnie od ENCAO, ale nie w pełni zależnie (Self-Direction, Humility...)
    * na tym etapie zakładam, że Wartości są tylko częściowo zależne od Osobowości. Tzn. może istnieć osoba, której Wartości są niekompatybilne z jej Osobowością (np. ktoś kto wysoko ceni SelfDirection ale potrzebuje opieki i chce być zaopiekowany i by ktoś powiedział jak żyć)
3. ENCAO i Wartości wygenerowały mi wagi, na podstawie których mogę zbudować LIKE / DISLIKE o pewnej mocy. 
    * Odcinam tagi do 7 likes i 7 dislikes max, albo do tych o intensywności przekraczającej X.
4. Traity i Drives są pochodną owych tagów.

Czyli dostałbym coś takiego (poniżej MOCK outputu na szybko):

* **Name**: Alicja
    * **Tags**
        * **Likes**: Reputation, Social Order, Helping
        * **Dislikes**: Danger, Conflict, Spotlight, Drugs, Stress, Hierarchies
    * Ocean: ENCAO:  0+-+0
        * GDZIE: Extraversion (siła reakcji na pozytywne bodźce), Neuroticism (siła reakcji na negatywne bodźce), Conscientousness (kontrola emocji, nałogów itp), Agreeableness (empatia i spolegliwość), Openness (szerokość skojarzeń i linków)
        * TRAITS:
            * Wiecznie zagubiony, nie robi tego co jest potrzebne
            * Poświęcił całą noc na hobby, więc jest niewyspany rano
            * Przymilny, taki trochę lizus
            * Pomaga innym którym potrzebna jest pomoc
            * Napięty i znerwicowany
    * Wartości:
        * TAK: Self-direction, TAK: Face, TAK: Power, NIE: Tradition, NIE: Humility
        * _komentarz_: przy takim ENCAO negacja zarówno Tradition i Humility mi nie pasuje, mniejsza szansa przez tagi.
    * Silnik:
        * TAK: Opus Magnum: stworzyć wielkie dzieło, zrobić coś najdoskonalszego czego niekoniecznie zrozumieją inni. 
            * _komentarz_: ma MARZENIE opus magnum, ale niskie C oznacza, że ciężko jej się zabrać
        * TAK: Impress the superior / senpai: zrobić coś by osoba wyżej mnie doceniła i pogłaskała lub zobaczyła moją wartość! 
            * _komentarz_: to jest prawidłowe tu, z tagów ma dużą szansę pojawienia się
        * NIE: Broken soul: zostawcie mnie wszyscy w spokoju. Nie chcę wiedzieć, nie chcę interaktować, nie chcę musieć. Za dużo. Dążę do samotności i ciszy.
            * _komentarz_: pasuje jako ANTYSILNIK. Nie możesz być na boku cywilizacji.
        * NIE: Survival of the fittest: przetrwają najlepiej dostosowani. Naszą rolą jest ekspozycja i zwycięstwo.
            * _komentarz_: social order temu zapobiega, więc to pasuje jako antysilnik. 
    * Potencjalne prace:
        * Technik hydrolog
        * Garderobiana
        * Lekarz – specjalista pediatrii

Zdaje mi się, że nie tylko buduje to spójniejsze postacie (plus numer 1) ale przede wszystkim łatwiej użyć tego na sesji / przy opowiadaniu (plus numer 2).

Jeśli robimy akcję potencjalnie niebezpieczną, Liliana mająca w LIKES [Danger, Impress] jest 100% YOLO! a Triana mająca w LIKES [Self-security] będzie "no nie wiem". Zamiast LIKES / DISLIKES można zrobić AFFINITY i... whatever jest odwrotnością, bo to nawet bardziej pasuje - nie wszystkie tagi to to co CHCEMY robić a niektóre to może być coś co robimy wbrew sobie, np. "prone to stress" czy "easily addicted".

Możemy też zrobić LIKES (co lubi i chce robić), DISLIKES (czego unika), AFFINITIES (co jej się zdarza nieważne czy chce czy nie).

## 5. Najtrudniejsze rzeczy do pokonania

1. "Zamknięta" zarządzalna lista tagów
2. mapowania: Wartość -> tag, Personality -> tag, tag -> other_stuff
3. dobre wyświetlanie / czytelność postaci, zwłaszcza w wariancie oneline (do raportu).
    * widok "faceless NPC" -> postać "nieważna", jednowymiarowa ("karczmarz")
    * widok "temporary NPC" -> postać wielowymiarowa ale znacząca na sesji ("randomowy student AMZ", "dyrektor szkoły")
    * widok "full NPC" -> postać wielowymiarowa i bogata w smaki ("Elena Verlen")

Programistycznie to prosty problem z tym co już mam. Ale konstrukcja tagów i mapperów to będzie smutna chwila.

## 6. Pytania

1. Czy to jest coś co jest prostsze do używania? 
    * (patrzę wartość dla innych - koszt implementacji)
2. Czy jakość NPCów ma potencjał wzrosnąć na tyle, by było warto to robić? 
    * (patrzę wartość dla siebie - koszt implementacji)
3. Czy na szybko widzicie coś prostszego lub lepszego?
    * w wymiarze implementacji
    * w wymiarze wyniku i używalności
4. O czym nie pomyślałem?
