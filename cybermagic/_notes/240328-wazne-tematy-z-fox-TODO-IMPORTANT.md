# Porażka - jak gracze mogą przegrać?

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Fox

---
## 0.1. Purpose

* Mamy wątki do analizy
* Trzeba je dokładnie przeanalizować
* Zrzut rozmów z Discorda

---
## 0.2. Spis treści i omówienie

1. Achronologia - kiedy podnieść konflikty
2. Zakres decyzji graczy - opowiadanie vs sesja
3. Jak skalować sesję w czasie
4. Złe czyny graczy - jak nagradzać i karać?

---
## 1. Achronologia - kiedy podnieść konflikty

achronologia -> wyjaśnić kiedy tak kiedy nie w wypadku amalgamacji

O co chodzi - są pewne problemy związane z wyjaśnieniem kiedy można achronologię a kiedy nie. 


## 2. Zakres decyzji graczy - opowiadanie vs sesja

* Fox: dobra, wydesytylowałam problem ostatniej sesji. Tak na prawdę jakby to była gra komputerowa to nic by nie straciła. Miałam dla graczy potężnego przeciwnika, którego normalnie nie da się pokonać. Przeciwnik jest mściwym duchem. Więc mieli dwie opcje:
    * pomóc mu odejść [wtedy musieli mu zapewnić 3 konkretne rzeczy - dla każdej z trzech osobowości co innego] 
    * zabić go [wtedy musieli zdobyc amulety które go więziły oraz broń i maga i zniszczyć ciało] Więc finalnie, mimo że miałam bardzo dużo kontentu sesja była bardzo... no jak gra komputerowa z otwartym światem.
* Fox: To nie była zła sesja, ale wolałabym, by gracze mieli szerszą manewrowość
* Irxallis: zabrakło Ci decyzji graczy?
* Fox: ...tak
* Fox: była... jedna defacto
* Fox: zabić lub pomóc
* Irxallis: tzn. byli "na sesji", ale "sesja nie zyskała na wartości bo tam byli"?
* Irxallis: tzn. podobało im się, sesja się udała, ale sesja nie SKORZYSTAŁA z ich obecności
* Fox: znaczy, mogli zadecydować o życiu npców, ale nie uważam, że to był duży wybór
* Fox: tak
* Fox: od początku wiedziałam które punkty mogą zostać zmienione i mogły być zmienione na a lub b
* Irxallis: wybór - "pomóc odejść" / "zabić" jest decyzją. Tu sesja zyskała - potencjalnie podjęli inną decyzję niż Ty byś podjęła. Czytam to w formie "może za mało zmiennych zmieniających wszystko", zbyt domknięta sesja (tzn. za mało emergentnych wydarzeń). Muszę pomyśleć bo WIEM o co Ci chodzi, ale to nowy koncept.
* Irxallis: kojarzysz sesję ja + Ty, tą ostatnią. Tamta sesja ostatecznie skończyła się w znaczący sposób (mogłaś uciec, zostałaś) i miała dużo subklocków (pomóc Marze, pomóc Jasperowi / zabić / nie słuchać...)
* Irxallis: tamta jest dobrym punktem odniesienia jako porównanie imo. Miałaś poczucie decyzyjności / gry komputerowej na tamtej sesji?
* Irxallis: (szukam czy dobrze czytam)
* Fox: Było dużo zmiennych. Gdybym na początku podjęła lepsze decyzje więcej mogłam osiągnąć.
* Irxallis: tak, tak by było
* Fox: Wiesz, gdybyśmy lepeiej zgrali to statek był do uratowania, gdyby było trochę gorzej, przynajmniej część załogi, to jak się skończyło było wariantem n końcowym
* Irxallis: oki - czyli dobrze szukam kierunku
* Fox: tak
* Irxallis: wiesz, że tamta sesja z Tobą trwała 4h PLUS miałem doświadczoną graczkę i zero granic po mojej stronie 🙂
* Irxallis: to jeden constraint który uwzględnj na sesjach z randomami
* Irxallis: ale tak, znajdźmy jak to naprawić
* Irxallis: bo 100% warto
* Fox: tak, bo ta sesja wyszła na prawdę dobrze. Gracze mieli dużo do robienia, trochę kminienia z wiadomości które zostali. Trochę podprowadzłam ich za rączę pod poszlaki. Mieli trochę fajnych pomysłów. Więc nie byłam w stanie na początku powiedzieć, co poszło nie tak
* Irxallis: słyszałaś o mojej uroczej ćmie 🙂
* Irxallis: gracze ogólnie byli zachwyceni. Ale fundamentalnie ta sesja była "sesją konwentową" - mają możliwość pomóc / spieprzyć, ale FUNDAMENTALNIE przy tamtym power levelu to pytanie ILE LUDZI ZGINIE i CZY WEZWIEMY AGENCJĘ i CZY POSTACIE ZGINĄ.
* Irxallis: więc to też nie jest full power
* Irxallis: btw, chcesz w środę młodszą Ariannę? Coś eksperymentalnego? Na coś masz ochotę? ^^
* Irxallis: wielkie dzięki za to. Muszę to przemielić. Ale nie jestem pewny czy jesteśmy w stanie z randomami to naprawić tbh. To jest "problem" jaki istnienie (nie tylko Ty masz, ja też) który jest na BARDZO wysokim poziomie. Rozwiązuję to silną emergentnością frakcji i innych klocków 
* Fox: Nie myśl o rozwiązywaniu dla randomów, pomyśl o rozwiązywaniu dla mistrza gry z mniejszą możliwością obliczeniową od ciebie
* Fox: Jeśli sesja ma więcej wyborów i więcej opiera się na graczach to i tak mogę wrzucić poszlaki i npców, by zobaczyli gdzie mogą pójść
* Fox: Ale jesli nie ma wyborów i są czasem 2 opcje no to cóż xd

## 3. Jak skalować sesję w czasie?

* Irxallis: daję im 3-4h. Jak są zmęczeni, kończę ^^
* Fox: ....?
* Irxallis: oki, jeśli zaczynamy o 19, kończymy o 23, powiedzmy
* Irxallis: jeśli BARDZO proszą by dać im +1h, dam
* Irxallis: else, koniec i przegrali z definicji
* Irxallis: zawsze masz max 4-4.5h
* Fox: No tak
* Fox: Ale jak to wyceniasz?
* Fox: Że sesja zajmie ci tyle
* Irxallis: skaluję sesję na tyle czasu
* Irxallis: hm, dzięki rozdzielczości konfliktu wiem
* Irxallis: tzn. mogę zmniejszyć rozdzielczość
* Irxallis: zamiast robić detale step-by-step mogę iść większymi ruchami
* Irxallis: czyt. Twoja sesja - detaliczna - była MNIEJSZA zakresowo
* Irxallis: nie wiem jak to robię, po prostu to robię. Doświadczenie. Kolejna rzecz do analizy ^^
* Fox: Czyli de facto dynamicznie modyfikujesz jak musisz
* Irxallis: tak, ale nie zawsze
* Irxallis: czasem wolę zostawić rozdzielczość i podzielić na 2

## 4. Złe czyny graczy - jak nagradzać i karać?

* Irxallis: widzę, że przebiłaś się przez mojego posta apokalipsy raniącego duszę Qnika? 😄
* Fox: Cudowny był
* Irxallis: mam to jako Notatkę (oczywiście), nie marnowałbym tego na discorda 😄
* Fox: Żałuję że na zakończenie zainteresowany nie skomantowal xd
* Fox: Dobrze xd tak warto barziej
* Irxallis: ten moment jak liczę megatony i teratony eksplozji wobec planetę XD
* Irxallis: czy prędkości
* Fox: Tak xd
* Irxallis: lub "Coriolis nie wiem jakie ma silniki i prędkości" <-- THAT DISS!
* Irxallis: pokazałem mu: "słuchaj, chcesz realizm, policz sobie ;p"
* Irxallis: bo nie każdy musi ^^
* Fox: Choć serio problem był "gracze robią złe rzeczy i to niszczy kampanię, co robić"
* Irxallis: serio sci-fi jest prostsze niż fantasy; w fantasy NIC nie zrobisz
* Irxallis: no tak, i powiedziałem "UŻYJ KONTRAKTU LOL"
* Fox: Yep
* Irxallis: ale skoro chce...
* Irxallis: kim ja jestem by nie pozwolić liczyć orbit transferowych czy nie bawić się w utrzymanie statku...
* Fox: Ale wiesz, mógłbyś to mieć w rozszerzonej notatce - jak radzić sobie z niecnymi czynami graczy
* Irxallis: to trafi na stronę jaką widziałaś
* Irxallis: nie myślałem o tym
* Fox: Wrzucić do tego i twój tracking punktów niegodziwości i działania frakcji przeciw graczom
* Irxallis: kurczę, muszę to jakoś zapisać
* Irxallis: dodać też to o lokalności postaci i czemu karty postaci działają jak działają
