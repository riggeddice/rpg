---
layout: mechanika
title: "Notatka mechaniki, 180325"
---

# {{ page.title }}

Author: Dzióbek

**Token conflict resolution system v0.7 (retro-fitted for current game mechanics)**

## 1. Purpose:

The purpose of this change is three-fold:

1. It adds a clear conflict resolution tool that allows for precise modelling of probabilities of outcomes.
2. It adds a direct mechanical tie-in between mechanics and in-game events, both as a means to tie mechanics to flavor and as a way to mechanicaly link scenes in new, often unexpected ways.
3. It creates a fast and clear tool for conflict resolution while at the same time creating a little ritual in conflict resolution that will prevent the emotions to fall down too fast after they have been built-up during aspect negotiation.

Examples:

1. In a zoomed-in scene a fight takes place on a staircase. In the first test one character tries to dash past their opponent and gain advantage by getting to higher ground. They succeed but in the process receive a light wound from their opponent. This results in two new gained aspects, a positive one 'high ground' and a negative one 'lightly wounded'. In the next part of this duel both may be used in the same test. The difficulty of the test will still be based directly on aspects used in the conflict. The result is clear from the sumbol chosen. However when a colored token is drawn it will change the in-game effect of this test by adding in a flavor tieing it to one of the gained aspects. For example if the token drawn is a 'conflicted success' depending on the color it will change meaning to:
- white 'you simply failed to achieve a full success'
- green 'you were close to failing, but thanks to the advantage the high ground provides you, you were able to turn it into a partial success'
- red 'you almost succeeded but unfortunately your injury made you falter and the enemy used that against you'

2. In a previous session an npc gained a new aspect 'stalked by an admirer'. Week later during the following session, during a conflict the GM proposes to replace one of the tokens with a consequence token for that aspect despite it not being directly involved in the conflict resolution. The type of token replaced is chosen at random to explore all possibilites. If this colored token is drawn from the bag it will affect the flavor of the result and link this aspect with the scene. For example if it is a 'failure' token maybe the character suddenly became aware of the stalker and it broke their concentration, or for a 'success' token maybe they saw the admiration in the eyes of the stalker and it motivated them to make the extra effort. Either way this stalker is now linked to this scene and may create a lead worth following after the conflict.


## 2. Requirements:

* set of tokens (see description below)
* implemented system for tracking aspects gained during play (see description below)
* bag for tokens (preferably cloth bag, non-transparent, big enough to fit a hand comfortably and leave room for vigorous rummaging)

Set of tokens:

1. All tokens in set have one of three symbols printed on them:
    1. success
    2. conflicted success
    3. failure
2. All tokens have a solid color background:
    1. white for regular tokens
    2. colored for consequence tokens (at least two colors, for example red and green :P, preferably four or more)
3. A full set of tokens consists of following tokens:
    1. 9 white tokens of each kind (success, conflicted success, failur)
    2. 6 tokens of each kind for each color
    
**Tracking aspects gained during play:**

1. During play if an entity gains an aspect during any conflict resolution it has to be marked visibly for all to see.
2. Example way would be to place a (preferably color coded) card with the aspect next to the player/character.
3. Aspects gained may be cleared after a session or they may linger between sessions as appropriate.


**important** this is the descripton of mechanics that can be placed on top of current conflict resolution to replace die throw, the main idea will be similar after conflict resolution refactoring but some of the work below will be eliminated making the process a bit more streamlined Conflict Resolution:

1. During play, between conflicts, all tokens reside on table, preferably in sorted piles.
2. When a conflict occurs, all conflict resolution remains unchanged up to the die throw. Take care to note all gained aspects used in this conflict resolution (meainng all aspects that were not cancelled out by another one)
3. **marked for streamlining** Instead of throwing the die iterate over each possible value on the die and determine if it would result in a success, a conflicted success or a failure. For each possible result set aside an appropriate token.
4. For each gained aspects used in this conflict resolution replace one of the set aside tokens according to following rules:
    1. for each gained aspect in character's favor replace two white tokens with two green ones starting with success tokens, then if no white success tokens are present replace conflicted success tokens, and lastly failure tokens
    2. for each gained aspect counted against the character replace two white tokens with two red ones starting with failure tokens, then if no white failure are present replace conflicted success tokens, and lastly success tokens
    3. in the unlikely situation when there are over 10 gained aspects used in an conflict the side with more gained aspects used gets all the appropriate tokens replaced with consequence tokens, the other side gets to replace the remaining tokens
5. If magic was used as part of conflict resolution 2 tokens should be replaced (preferably white, but if there are no white tokens available, consequence tokens should be replaced) by a separate color. The type of tokens replaced should be chosen at random.
6. If there are white tokens left any player (including GM) may propose to replace one (or more) tokens with a separate color due to an aspect not used in this conflict. This is counted as a consequence token. It may be connected to any gained aspect present in the game. The type of this token should be chosen according to the player's idea for what could happen or may be chosen at random to explore all possibilities. All players must agree to this. The purpose of this action is to try and link separate threads in the game in a situation where the player in question wants to leave this to chance.
7. After all token replacements have taken place all 20 of them should be palced in a bag. The bag should be shaken vigorously to mix them up and one should be picked at random.
8. The symbol on the token directly corresponds to the outcome of this conflict resolution.
9. The color, if other than white, carries additional meaning:
    1. if the token carries the color assigned to magic, a random magic effect takes place according to current rules
    2. if the color is one of the colors for consequences then the result of this conflict is linked to the aspect that caused this token to be placed in the bag

## 3. Future ideas to consider:

1. After switching to test difficulty, only 10 tokens should be used. Each test difficulty should have a strictly defined distribution of token types. Each gained aspect, as well as use of magic in conflict, should only replace a single token with colored one. To speed up the conflict resolution a pile of white tokens corresponding to each test difficulty should be prepared separately so that only token replacement has to be conducted when choosing which tokens to place in the bag.
2. If desired, aspects gained may have unique colors assigned so that if more than one token has been replaced one can tell exactly which gained aspect is responsible for the outcome of this test.
3. The number of tokens replaced when magic was used in conflict should vary. For a generic conflict 2 tokens may be replaced, while in a zoomed in scene only 1. Especially stressful or straining tests may raise number of tokens replaced (for example proportionaly to number of negative gained aspects used in test)
