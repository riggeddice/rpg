# Jak złożyć sesję
## 1. Checklista

1. Znajdź dominujący dylemat sesji 
2. Określ Dramat
    1. Ludzki dramat (między ludźmi)
    2. Co spotkało statek kosmiczny (czemu potrzebuje SOS)
3. Określ o co grasz jako MG 
    1. co jest dominującą katastrofą (jak się statek skończy jak gracze nic nie zrobią)
    2. w kontekście dylematu sesji
4. Złóż statek kosmiczny i określ co jest OCZYWISTYM problemem
5. Określ scenę startową
6. Określ postacie i daj im Karty Celów Indywidualnych

## 1.1. Checklista - przykład 1

"Wolność za wszelką cenę"

1. Znajdź dominujący dylemat sesji 
    1. A: "Wolność czy chwała?"
        1. Neikatiańska TAI jest po stronie noktian, ALE zagraża atarienom ze statku górniczego
        2. Noktianie S&R nie mają zaufania ze strony atarienów, ALE to oni rozwiążą problem
        3. TAI chce być wolna lub martwa, ALE da się ją wyłączyć. Zniewoli to ją, ale da zaufanie noktianom
2. Określ Dramat
    1. Ludzki dramat (między ludźmi)
        1. A: echo starej wojny
        2. A: nowa postać na statku S&R: "jestem świetną atarienką z Orbitera, nie pasuję do Was!"
    2. Co spotkało statek kosmiczny (czemu potrzebuje SOS)
        1. A: statek górniczy obraca się przeciw załodze
3. Określ o co grasz jako MG 
    1. co jest dominującą katastrofą (jak się statek skończy jak gracze nic nie zrobią)
        1. A: 
        1. A: 
    2. w kontekście dylematu sesji
        1. A: ludzie zaczną ginąć a TAI się rozprzestrzeni.
        1. A: Code Black "R.E.V.O.L.U.T.I.O.N."
4. Złóż statek kosmiczny i określ co jest OCZYWISTYM problemem
5. Określ jakie środki są potrzebne na sesji by pokazać co chcesz pokazać
    1. neikatiańska (wolna) TAI chcąca uwolnić noktian i dać lepsze życie; malfunctioning. 
        1. TAI do psychotronicznej walki.
        2. "You are free or coup de grace. Slavery is not an option."
    2. atarienka, która trafiła na korwetę S&R i rozpaczliwie nie jest "jedną z nich". Pro-Orbiter.
        1. "Nie pasuję tu, nie powinnam tu być, to jakaś pomyłka!"
    3. pokazanie, że nasi noktianie są kompetentni ale nie mają zaufania od przełożonych
        1. 
    4. zniszczenie TAI jest prostsze niż jej wyłączenie.
    5. 
5. Określ scenę startową
6. Określ postacie i daj im Karty Celów Indywidualnych

## 2. Jak o tym myśleć

Sesja działa jak oryginalny Star Trek. 

Technologia wprowadza kontekst i sytuację, ale trudne dylematy mają postacie graczy. To gracze decydują jak zostawić świat lepszym.
