# Co znajduje się na statku kosmicznym

## 1. Przykładowe statki kosmiczne

### 1.1. Holownik "Mroczny Wół"

* Co to jest
    * Niewielka jednostka holująca; do 5 osób
        * kluczowe role: kapitan, pilot + nawigator, advancer, inżynier
        * aspekty: potężna moc, ciasny, głośny, mnóstwo zakamarków, pancerny, niezgrabny
    * Typowe zadania: 
        * laweta, przetransportować coś dużego z punktu A do punktu B, przetransportowanie kontenera
        * zrobić przejście (w trybie pługa)
        * raczej porusza się po znanym terenie
* Rozkład
    * zewnętrze
        * frontowy hardpoint; pozwala na transportowanie czegoś lub zamontowanie np. pługa; zewnętrzny
        * wzmocniony pancerz przedni; lekkie uzbrojenie anty-śmieci kosmiczne
        * manipulator magnetyczny; tractor beam
        * dwie śluzy boczne + tylna śluza awaryjna
        * potężne silniki: tył, przód + silniki manewrowe boczne
        * brak elementów prześwitowych ("szyby"); taka pancerna bryła
    * wewnętrzne elementy
        * mostek + sterowanie manipulatorami i hardpointem + nawigacja
        * AI Core (klasa TAI: Semla)
        * kabiny załogi; współdzielone po 2 osoby
        * life support + apteczka
        * inżynieria + generatory memoriam
        * reaktor + baterie
        * magazyny wody + komponentów potrzebnych (paliwa, jedzenia itp)
        * mesa i rozrywka
* Przykładowe sesje unikalne
    * transportował coś, czego nie powinien
    * w zakamarkach Wołu szmuglowane jest coś, co musi być odnalezione
    
### 1.2. Statek górniczy "Koparka"

* Co to jest
    * Ogromna jednostka z małą załogą; do 10 osób
        * kluczowe role: kapitan, górnik (co najmniej szóstka), inżynier, kucharz, medyk, mały obszar dostępny dla ludzi (reszta to ładownie)
        * aspekty: potężna moc, ciasny, głośny, niezgrabny, ogromne ładownie, potężny laser górniczy
    * Typowe zadania: 
        * rozbijanie ciał kosmicznych, zbieranie rudy metali
        * dostarczyć urobek do punktu skupu
        * najczęściej porusza się obrębie systemu, pomiędzy pasami asteroidów (bardzo daleko na obrzeżach) a punktem skupu
* Rozkład
    * zewnętrze
        * laser górniczy, sterowany z mostka, służy do rozbijania skał
        * wzmocniony pancerz; lekkie uzbrojenie anty-śmieci kosmiczne
        * manipulator magnetyczny; tractor beam (dużo słabszy niż na holowniku, służy do łapania większych kawałków skał
        * dwie śluzy boczne + tylna śluza awaryjna
		* luki ładunkowe otwierane bezpośrednio na próżnię
        * potężne silniki umożliwiające przewóz ładunku, ale nie dające dużej prędkości. Ograniczone możliwości manewrowania (to kosztuje!)
        * opancerzone okno oraz kamery na kadłubie pozwalające obserwować otoczenie
    * wewnętrzne elementy
        * mostek + sterowanie laserem + komunikacja z górnikami na zewnątrz + nawigacja
        * AI Core (klasa TAI: Semla)
        * kabiny załogi; współdzielone po 4 osoby
        * life support 
		* stacja medyczna (wypadki chodzą po górnikach) 
        * inżynieria + generatory memoriam
        * reaktor + baterie
        * magazyny wody + komponentów potrzebnych (paliwa, jedzenia itp)
        * mesa i rozrywka
* Przykładowe sesje unikalne
    * przesadził z masą ładunku i stracił możliwość manewrowania. Załoga nie chce nic odrzucić bo znaleźli cenne surowce i chcą się w końcu odkuć
    * w złożach, które wydobyli było _coś_: może obcy organizm, może kosmiczna anomalia powodująca dziwne efekty..
	
	
### 1.3. Statek handlowy "Róg obfitości"

* Co to jest
    * Średnich rozmiarów jednostka zdolna do lądowania na planecie. Załog jest w większości rodziną. 
        * kluczowe role: kapitan, nawigator, pilot, inżynier, specjalista od załadunku, 
        * aspekty: mnóstwo zakamarków, zgrana załoga, sekrety rodzinne
    * Typowe zadania: 
        * przewożenie produktów
		* transport osób
		* lądowanie 
* Rozkład
    * zewnętrze
        * lekkie uzbrojenie anty-śmieci kosmiczne
        * cztery śluzy + śluza awaryjna + otwierana ładownia
        * silniki umożliwiające podróż pomiędzy układami
        * okno na mostku ułatwiające nawigację i manwery w porcie
    * wewnętrzne elementy
        * mostek + komunikacja + nawigacja
        * AI Core (klasa TAI: Semla lub Persefona)
        * kabiny załogi; dzielone według układów rodzinnych
		* kabiny pasażerskie (czasem pełniące rolę luków na delikatne materiały)
        * life support 
		* stacja medyczna (na długich trasach trzeba być samowystarczalnym) 
        * inżynieria + generatory memoriam
        * reaktor + baterie
        * magazyny wody + komponentów potrzebnych (paliwa, jedzenia itp)
        * mesa i rozrywka
* Przykładowe sesje unikalne
    * młodzik przeszmuglował coś na pokład wbrew starszyźnie. Nie wiedział, że to niebezpieczne...
    * anomalnia kosmiczna podczas skoku międzygwiezdnego
	
## 2. Typowe pomieszczenia

### 2.1. Lokalizacje ZEWNĘTRZNE

#### 2.1.1. Projektor ekranu ochronnego

W wypadku niektórych jednostek (bojowych lub operujących w warunkach podniesionego pyłu) możemy mieć do czynienia ze specjalnym generatorem pola siłowego - ekranem ochronnym. Bardzo kosztowny energetycznie, ale pozwala na wytrzymanie ogromnej ilości uderzeń.

#### 2.1.2. Śluza / Właz

Jakoś trzeba do statku wejść. Będą podstawowe i awaryjne.

#### 2.1.3. Manipulator (magnetyczny / kinetyczny)

Czasem statek będzie miał umiejętności przesuwania obiektów na zewnątrz. To będzie robić manipulatorem kinetycznym ("dźwig") lub magnetycznym.

#### 2.1.4. Wyrzutnie kapsuł ratunkowych

Zwłaszcza na jednostkach większych, rzadko na korwetach czy fregatach (za mało miejsca).

#### 2.2.5. Silniki

Napęd. Silniki mogą się zepsuć, mogą paść ofiarą sabotażu. Statek ma kilka TYPÓW silników (niekoniecznie wszystkie są obecne):

* krótkozasięgowe, rakiety
* dalekosiężne, warp
* manewrowe, rakiety
* hamujące, rakiety

#### 2.2.6. Sensory

Sposób wykrywania różnych rzeczy w przestrzeni.

#### 2.2.7. Anteny

Jeśli ktoś ma usłyszeć Twój krzyk to jakoś musisz go nadać...

### 2.2. Pomieszczenia

#### 2.2.1. Mostek

Główne centrum kontrolne statku. Tu zwykle znajdziesz kapitana i oficerów.
Tu również są wszelkie konsole.
W zależności od rodzaju statku, czasem mostek będzie malutki, z miejscem na jedną osobę, czasem będzie wymagał załogi kilkunastuosobowej.

#### 2.2.2. Reaktor

Źródło energii statku. Dość dobrze ekranowane na większości statków.
Może mieć wyciek, paść ofiarą sabotażu, albo dziwnie się zachowywać.
Tu często można znaleźć inżyniera.

#### 2.2.3. Baterie (technicznie, akumulatory)

Oprócz reaktora będziemy też mieć baterie do impulsowego uzupełnienia mocy lub do dostarczenia awaryjnego poziomu energii gdy reaktor nie działa.

#### 2.2.4. Sekcja mieszkalna

W zależności od rodzaju statku może być mała lub rozbudowana, może też nie być wydzielonej części mieszkalnej.
Tutaj najczęściej będzie sala treningowa, miejsce na personalne schowki, tu można pokazać osobowość członków załogi, a w wypadku horroru to tu gracze mogą znaleźć ślady lub ciała...

#### 2.2.5. AI Core 

Tuaj rezyduje sztuczna inteligencja sterująca statkiem i odpowiadająca za nawigację. 
Nawigacja w kosmosie jest trudna. Ludzie nie są w stanie sobie poradzić bez pomocy komputerów...
Poziom złożoności sztucznej inteligencji możecie ustalić w zespole, może być po prostu bardziej zaawansowanym komputerem lub kolejnym członkiem załogi.
Niezależnie od waszej decyzji, będzie to dość dobrze ekreanowane i chronione miejsce.

#### 2.2.6. Life support 

Oczyszczanie powietrza, utylizacja odpadów biologicznych... Bez tego nie da się żyć w kosmosie.

#### 2.2.7. Ambulatorium (med bay)

Miejsce pracy lekarza. Nie każdy statek będzie miał (to raczej specjalistyczne i drogie pomieszczenie)
Jeśli statek ratowany ma ambulatiorum, to gracze mogą tu znaleźć informację o tym, czy nie było problemów medycznych przed sygnałem SOS

#### 2.2.8. Magazyny

Każdy statek będzie miał jakiś luk magazynowy. Statki handlowe mogą mieć ich wiele i bardzo dużych. Małe jachty kosmiczne raczej będą mieć je ograniczone.
Tu może znaleźć się kontrabanda, niebezpieczny ładunek, o którym załoga nie wie, albo sprzęt, który może pomóc graczom... 
Miejsce pracy specjalisty od załadunku.

#### 2.2.9. Stacja naukowa

Raczej specjalistyczne pomieszczenie, podobnie jak ambulatiorum.
Jeśli gracze ratują statek badawczy to tu może być cała skarbnica śladów i ciekawostek.

#### 2.2.10. Sterowanie bronią

Głównie na statkach wojskowych.

#### 2.2.11. Fabrykator

Głównie na jednostkach wsparcia. Miejsce, w którym dochodzi do transformacji substratów w konkretne produkty. Np. "materiał + energia -> kombinezon."

#### 2.2.12. Generatory memoriam

Potrzebne do poruszania się w kosmosie i do stabilizowania amplitud magicznych.
Istotne tylko jeśli zdecydujecie się grać z opcją magii.

#### 2.2.13. Railway system

W wypadku dużych jednostek pojawia się konieczność poruszania się szybko po statku. Zawiera wewnętrzną "kolejkę" / "windę" / mechanizmy szybkiego poruszania się.

#### 2.2.14. Hibernacja

Sposób transportu dużej ilości ludzi w większym czasie ;-).

## 9. Sources

* https://yaletools.com/parts-of-submarine/
