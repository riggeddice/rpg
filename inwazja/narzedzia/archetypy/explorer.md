---
layout: default
title: The Explorer / Odkrywca
---

# {{ page.title }}

## Description
Explorer usually does not have financial issues. Has quite large knowledge and many social connections. Specializes in discoveries and mysteries. Quite social role, connected to expeditions, trophies and discoveries.

Odkrywca to osoba nie mająca ogromnych problemów finansowych. Jest to osoba posiadająca niemałą wiedzę i jakiś kapitał społeczny, specjalizująca się w odkryciach i zagadkach. Odkrywca to rola dość społeczna; powiązana z wyprawami i trofeami oraz odkryciami.


## Motivations / Behaviors

* to discover [something]
* to explore [something]
* to unravel a mystery of [something]

## Moves

1. make an amazing discovery
1. hide a discovery for a future use / add it to trophies
1. race a rival for being the first one
1. prepare / launch an expedition
1. show off the trophies
1. study trophies / artifacts
1. demand proper recognition
1. tell a story
1. present [someone] to [society]
1. research [something / someone] for future exploration