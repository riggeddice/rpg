---
layout: default
title: The Artisan / Rzemieślnik
---

# {{ page.title }}

## Description

Artisan wants to craft high quality things. 
He's motivated by hard work; daily works makes him feel comlete. It does not have to be art, but it must be crafty and it must be done.
Artisan will do the job the best he can and will strive to improve.

Rzemieślnik jest osobą, która chce przede wszystkim wykonywać dobrą robotę. Motywuje go ciężka, uczciwa praca i spełnienie znajduje w pracy dnia codziennego. Może nie jest to najbardziej efektowna czy najbardziej olśniewająca praca, ale musi być zrobiona. I on to zrobi tak dobrze, jak potrafi.

## Motivations / Behaviors

* to do their duty (being [something])

## Moves

1. improve the workshop
1. do something useful
1. create value for [someone]
1. present the work of craft
1. acquire specialized tools / build specialized tools
1. do their duty very well
1. get an apprentice
1. teach some of the knowledge
1. do something special for someone special
1. travel searching for a job
