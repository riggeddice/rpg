---
layout: default
title: The Dominator
---

# {{ page.title }}

## Description

Dominator does not necessarily want to rule the world. What they want is total control over something or someone; total and absolute control.
Control freak, using usually strong emotions. He's hated, loved and feared by his victims.

Dominator niekoniecznie chce rządzić światem. To, czego Dominator pragnie to absolutna kontrola nad kimś / czymś; całkowite zniewolenie i złamanie. Control freak, operujący przede wszystkim przy użyciu silnych emocji (najczęściej strachem), Dominator jest jednocześnie wielbiony, nienawidzony jak i [pasywna forma od przerażający :P] przez swoje ofiary.

## Motivations / Behaviors

* to enslave [someone / something]
* to break others, to be completely in control
* to completely dominate others

## Moves

1. select a new [victim]
1. expand his influence onto [someone]
1. make a show of his power (directly, with minions, with others' obedience)
1. exert influence on [captive] with strong emotions (terror, torment, lust)
1. force an [uninfuenced] to do something with (blackmail, strong emotions, corruption)
1. convert/corrupt [someone] with (gifts, trade, emotions)
1. hit those close to [someone] to (elicit emotions, gain obedience, break)
1. study [someone] to find weak spots
1. demand absolute obedience from [someone]
1. reward [someone] handsomely
1. announce future badness to (elicit [emotion], force [action)
1. acquire immoral minions for [mission]
1. make a deal with the devil to gain [something]
1. present a devil's choice to [someone] to (corrupt, surrender, …)
1. prepare a trap for [someone] using [bait]
1. capture / kidnap [someone]
1. operate / supply a dungeon in [location]
1. organize a cult
1. control [someone's] environment
1. defile [someone's] sanctuary
