---
layout: default
title: The Collector / Kolekcjoner
---

# {{ page.title }}

## Description

The Collector is quite simple: he aims to build as large as possible collection of items or people with certain characteristics.

Kolekcjoner jest archetypem osoby, która próbuje zebrać jak najwięcej obiektów / osób określonego typu.

http://tvtropes.org/pmwiki/pmwiki.php/Main/TheCollector
http://tvtropes.org/pmwiki/pmwiki.php/Main/LivingDollCollector

## Motivations / Behaviors

* Acquire [someone / something]

## Moves

1. try to acquire / kidnap [something] directly
1. imprison / hide [something]
1. locate an appropriate [something]
1. evaluate / test / assess [something]
1. trade [something] for [something else]
1. put [something] on display; unravel / present [something]
1. modify/adapt/transform/combine an object of collection
1. use an object of collection
1. elicit emotions in [someone] with [something]
1. recruit henchmen for [something]
1. prepare resources / arrange resources to get closer to [something]
1. manipulate [someone] to do [something] (unrelated to collection) which causes them to require Collector's help
1. manipulate [someone] to do [something] dangerous or reckless opening Collector an avenue to do [something]
