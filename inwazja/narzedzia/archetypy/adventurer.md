---
layout: default
title: The Adventurer / Poszukiwacz Przygód
---

# {{ page.title }}

## Description

Everything is something new and amazing for an Adventurer. A free soul, not willing to take root anywhere yet, one who wants to experience everything and see what this world has to offer. Adventurers tend to seek dangerous situations, but not too dangerous ones. They want to learn new things, sightsee, get some souvenirs, collect some amazing memories and play and have fun. They live their lives quite intense; they tend to go in carefree and hoping everything will turn out okay. In the same time, they usually have some experience which leads to them being able to adapt to the situation much faster than someone not prepared. So – puppy eyes, yes; stupidity, no.

Dla Poszukiwacza Przygód wszysto jest nowe i zachwycające. 
To wolna dusza, która nie chce jeszcze osiąść. Chce za to doświadczyć wszystkiego, co ten świat ma do zaoferowania.
Poszukiwacz Przygód będzoe pakował się w niebezpieczne sytuacje, ale nie zbyt niebezpieczne. Chce poznawać nowe rzeczy, uczyć się nowych umiejętności, zwiedzać, zdobywać pamiątki, budować wspaniałe wspomnienia i ogólnie dobrze się bawić.
Żyje bardzo intensywnie, czasem nieco lekkomyślny, zawsze wierzy, że wszystko dobrze się skończy.
Zwykle też ma trochę doświadczenia, które pozwala mu dostosować się do różnych sytuacji szybciej niż ktoś bez takiego przygotowania.
Nieco naiwny, ale w żadnym wypadku nie głupi.

## Motivations / Behaviors

* to experience something new, something awesome

## Moves

1. to seek adventure
1. to accept a quest