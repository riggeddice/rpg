---
layout: default
title: The Addicted / Uzależniony
---

# {{ page.title }}

## Description

Person with this archetype is a slave to their addiction.
He / she is driven to satiate their addiction. While they can realize that the addiction is not beneficial, it is stronger than they are.
Good example is David Gemmell's Morrigan.
Does not have to be 'typical' addiction either. For example "psycho-fan" might be addicted to their idol.

Uzależniony jest niewolnikiem swojego uzależnienia. Jego impulsem jest dążenie do zaspokojenia / znieczulenia swojego uzależnienia i jakkolwiek może zdawać sobie sprawę z tego, że nie jest to korzystne czy pożyteczne, jest to silniejsze od niego.
Bardzo dobrym przykładem jest Morrigan z "Rycerzy Mrocznej Chwały" Davida Gemmella jako przykład postaci wielowymiarowej mającej ten tag.

## Motivations / Behaviors

* to indulge in [vice]
* to avoid the withdrawal

## Moves

1. purify / improve the dose of the [vice]
1. increase / decrease the [vice]
1. have withdrawal effects in form of [effect]
1. have compulsionary effects in form of [effect]
1. indulge in addiction because of [reason]
1. fight an addiction because of [reason]
1. hide the nature of the addiction from [someone]
1. make a risky, self-destructive [action]
1. break the law to obtain [resource]
1. display the [properties] of the addiction
1. hurt [someone] (unwittingly / unwillingly) because of addiction
1. try to drown the addiction with [high emotional action] or different addiction
1. lure [someone] to join in addiction
1. defy the [compulsion] from the addiction
1. get lost in the bliss of [fulfilling the addiction] / pain of [suffering the withdrawal]
1. do something completely out of character for [a dose]
1. fail to do [something] important because of addiction
