---
layout: default
title: The Incompetent Expert / Niekompetentny ekspert
---

# {{ page.title }}

## Description
Zgodnie z zasadą Petera, każdy kiedyś jest awansowany poza swój poziom kompetencji. I tu mamy właśnie do czynienia z osobą, która mogła być swego czasu kompetentna, lecz już nie jest (w nowej roli?). Zwykle uważany za osobę kompetentną, za eksperta w swojej dziedzinie, Niekompetentny Ekspert rozpaczliwie próbuje zachować swój autorytet i udaje, że jest świetny a jego rady mają znaczenie.
Dotyczy: świetnego developera awansowanego do managera lub youtubera grającego w GW2 po tym, jak patch sprawił, że zardzewiał. Też: kiedyś kompetentny wojownik po wejściu nowego sprzętu i paradygmatu taktycznego.

According to Peter rule, everyone is promoted above their level of competence.
This person may have been competent once, but is not any more (most likely due to new position)
Usually seen as competent, an expert in their domain, Incompetent Expert desperately tries to keep his authority and pretends that he is great and his help matters.
Could be a great developer, youtuber playing a game after a long time, warrior after introduction of new equipment or tactics...

## Motivations / Behaviors

* to preserve the illusion
* to keep the importance, pay and privileges
* to catch up

## Moves

1. seek the knowledge about [expertise area> discreetly
1. give an advice about [something> (good, bad, dangerous)
1. make an obvious mistake showing incompetence
1. avoid making any decision whatsoever about [something>
1. delegate [something> which should be decided personally
1. perform [task> better suited to [old role>, not [new role>
1. tell a tale of the former exploits as anegdote to [something>
1. share the knowledge in the area with [someone> (is competent in, is not competent in)
1. be unavailable for questions and refuse to answer
1. appoint [someone> as an advisor and a go-to person
1. use authority and resources in a conflict to show who is important here
1. perform an [action> under duress (blackmail, threat…)
1. blindly follow the plan without adaptation to [opportunities>
1. allocate the resources to do [something> showing he is doing useful work (useful, useless)
1. make it impossible for [someone affiliated> to make a move because of [reasons> (no decision, constantly changing decisions…)
1. endanger someone by doing [something>
1. demand [compensation> for his help / work
1. indulge in privileges of the rank / role instead of doing [something>