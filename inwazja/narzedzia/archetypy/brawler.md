---
layout: default
title: The Brawler / Zabijaka
---

# {{ page.title }}

## Description

Brawler fights for the pure pleasure of fighting.

Zabijaka jest archetypem oznaczającym kogoś, kto walczy dla samej przyjemności walki.

http://tvtropes.org/pmwiki/pmwiki.php/Main/BloodKnight

## Motivations / Behaviors

* to challenge [someone / something]

## Moves

1. force an encounter with [someone]
1. enter the combat directly
1. seek a worthy opponent using [method]
1. incite / escalate a rivalry with [someone]
1. present / accept a challenge
1. prepare an arena
1. strengthen / weaken [an opponent]
1. boast / insult [someone]
1. make a show of strength
1. equalize the battlefield
1. create high stakes
1. make a finishing move against [opponent]