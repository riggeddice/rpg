---
layout: default
title: The Good, Moral Person / Dobry Człowiek
---

# {{ page.title }}

## Description
This archetype deals with a normal good person. This type of archetype usually tries to simply live well. This might not sound very exciting but this archetype is the “good neighbor” in the population. Usually helps other people, tries to live well and do not harm the others, this forms the positive part of the community.

Dobry Człowiek zwykle po prostu stara się żyć dobrze. Dobry sąsiad, zwykle pomaga innym, stara się nikogo nie krzywdzić. Dba o społeczność.


## Motivations / Behaviors

* to be a good person
* to live well
* to improve the community

## Moves

1. help [someone] in need
1. appreciate the hard work of [someone]; encourage them
1. protest or do [something] against the injustice
1. donate to a good [cause] (time, money, items)
1. integrate the society around [something]
1. state an opinion on [something]
1. improve the surroundings / neighborhood
1. do [something] what is right in the [situation]
1. volunteer to do [something]
1. do [the job] well, do not wait for recognition
1. expend [resources] for [greater cause] anonymously
1. help [someone] who would never appreciate it
1. shield [someone] weaker
1. work really hard, make [something] unlikely happen
1. make [someone] smile in the dark hour
1. establish contact with [someone]; get a friend
1. match [someone] with [someone] having [something] in common
