---
layout: inwazja-konspekt
title:  "Jaszczury rządzą miastem"
campaign: dusza-czapkowika
gm: żółw
players: raynor, arleta
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180418 - Czapkowicka Apatia Kulturalna](180418-czapkowicka-apatia-kulturalna.html)

### Chronologiczna

* [180418 - Czapkowicka Apatia Kulturalna](180418-czapkowicka-apatia-kulturalna.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* Wątek "Operiatrix bez Piotra Kita".

## Punkt zerowy

![Rysunek inicjujący sesję](Materials/180499/180419-init.png)

* Ż: Czemu tu w ogóle jesteście?
* R: Przypadek. Są przejazdem, jadą gdzie indziej. Ale - zatrzymali się po benzynę. I tam biedne przerażone dziecko na stacji benzynowej.
* Ż: Dlaczego chcecie to rozwiązać?
* A: Genowefa nie może obojętnie przejść. A Alojzy nie ma opcji.

## Potencjalne pytania historyczne

* null

## Misja właściwa

**Dzień 1**:

Genowefa i Alojzy nie jechali do Czapkowika. Byli tu tylko przejazdem, serio. Tak się jednak skończyło, że Alojzy trzymał rozhisteryzowanego szesnastolatka a Genowefa splotła zaklęcie uspokajające, bo "zęby... jaszczur..." nie brzmiało sensownie ani jak coś z czym da się gadać. Zadziałało, zaklęcie mentalne jest dobrą robotą. Chłopak się uspokoił i powiedział, że byli w operze czapkowickiej. On i Ola, jego przyjaciółka. I się obściskiwali. I usłyszeli straszny dźwięk. No to się trochę powspinali i zobaczyli jaszczura grającego na saksofonie. Takiego strasznego.

Jaszczur ich zobaczył i próbowali uciec. Ola spadła a on zwiał. Gdy Genowefa zauważyła, że to nie po dżentelmeńsku, chłopak zaznaczył, że się bał. No dobry argument. Magowie poszli więc poszukać tego jaszczura i ratować Olę. A paladyn, tzn. chłop, tzn. Maciek na przedzie. Na wypadek gdyby jaszczur był agresywny i niebezpieczny.

Alojzy się oderwał i poleciał pierwszy, w sam raz by usłyszeć wołanie o pomoc. Idzie biznesmen w garniaku i niesie Olę. Wyszedł do niego - pewny, że to jaszczur jest (bo tak samo brudna marynarka jak opowiadał Maciek) - i spytał, co biznesmen tu robi. Niestety, nie udało mu się niczego dowiedzieć. biznesmen się martwi o Olę; znalazł ją nieprzytomną pod drzewem.

Genia dotarła do Alojzego i powiedziała, że jest lekarzem co przebrany jaszczur przyjął z ogromną ulgą. Gdy Genia rzuciła czar leczniczy doszło do Paradoksu i pojawiły się płomienie. A w nich - pojawiły się sylwetki jaszczurów. Nasz nieszczęsny Alfred Werner, Jaszczur W Przebraniu i saksofonista, W DŁUGĄ! A Alojzy za nim! Czar ściany ziemi by złapać jaszczura; tyle, że gad się zwyczanie potknął o korzeń i nawet nie trafił w ścianę. Nieszczęśliwy Alojzy wyrżnął "biznesmenem" w ścianę (szkda by się zmarnowała) i spytał czemu on ucieka. No bo Alojzy goni. I czy biznesmen jest jaszczurem! A Alfred na to "NIE"! I Alojzy zgłupiał.

No, dobra, trzeba załagodzić sprawę jak Genia pomaga biednej Oli. Alojzy na zgodę dał piersiówkę biznesmenowi by się z nim napić. Alfred powąchał i się przyznał. JEST JASZCZUREM! SKĄD ALOJZY WIEDZIAŁ! Alojzy się ucieszył - znalazł jaszczura. Pogadali chwilę, dołączyła do tematu Genia. Alfred powiedział im o Operiatrix, że ćwiczy muzykę, że paintball... i że saksofonu się uczy. I poprosił o pomoc - będzie miała grać kapela postmetalowa co może Operiatrix przesunąć ku złemu. A on nie chce! Ona ma być piękna i kochana. No i paintball. Niech mu pomogą, no... ma 50 złotych, zapłaci im.

...

Dobra, coś trzeba zrobić bo niebezpieczna Operiatrix to niebezpieczna Operiatrix, może być mała masakra. Trzeba to rozwiązać. Ale jak? I tu pomysł Alojzego - trzeba Zdominować burmistrza tej miejscowości. On ma władzę, on pomoże im rozwiązać problem. Przepytany Maciek powiedział, że burmistrzem jest "laska, ale stara". Znaczy, kobieta. No, nieważne, dominacja jest skutecznym czarem niezależnie od płci.

Pani burmistrz jednak ma na sobie przedmioty ze srebra. To nieco utrudnia całe działanie. Cóż, podzielili się. Genowefa zajęła się sprawdzaniem jakichś sekretów Czapkowika by dobrze udawać wróżkę i lekarza; by dowiedzieć się jak najwięcej a Alojzy poszedł do baru Gwarek z asystentem Kamili, Mateuszem by go spić i wbić się w kalendarz pani burmistrz. Obu się udało - Genowefa dowiedziała się o tym, że pani burmistrz jest na coś chora a Alojzy wbił się w kalendarz.

**Dzień 2**:

Po wyczyszczeniu Maćka i Oli okazało się, że koncert post-black-metalowy będzie miał miejsce w tym samym czasie co spotkanie z panią burmistrz. Zespół się podzielił - Genowefa poszła do burmistrz, Alojzy zatrzyma koncert metalowy.

Genowefa poszła więc do pani burmistrz. Kamila Woreczek się zdziwiła, że Genia tyle wie na jej temat, ale zapytana, zdjęła srebro i rozebrała się do pasa. Niestety dla Geni, Kamila wyczuła w niej maga. Niestety dla Kamili, Genia wyczuła w niej jaszczura.

I co dalej? Genowefa wyjaśniła, że chce wprowadzić coś takiego, żeby nie było problemów; tam jest paintball w operze i to jest bardzo niebezpieczne. Dodatkowo, jest tu jeszcze jeden jaszczur. burmistrz się zdziwiła - jak to JESZCZE JEDEN? Nie może tu nikogo innego być, ona jest jedyną na powierzchni dbającą o interes małej podziemnej kolonii. Genia jest aż zdziwiona. No ok. Ale ważne jest to, że trzeba jakoś rozmontować niebezpieczny Pryzmat z jaszczurami. Ma! Trzeba zrobić polskie Roswell! Area 51! Wszędzie kiczowate, pluszowe i gumowe jaszczury.

Innymi słowy, jaszczur jest elementem folkloru. W ten sposób nie będzie nigdy już problemu z Pryzmatem "źli reptillianie chodzą pod ziemią i kontrolują umysły". Kamila się racjonalnie zgodziła - została przez ludzi wybrana na platformie postępu i zysków finansowych. Nie musi to być paintball; równie dobrze może być to "polskie Roswell".

Tymczasem w zupełnie innym miejscu, Estrellas Infernal przygotowują się do koncertu. Około 50 metalowców na widowni i cztery kobiety w postdeathmetalowej bandzie. Alojzy jest lekko zafrasowany jak to rozwiązać; nie chce, by doszło do skażenia Operiatrix. To byłoby bardzo, bardzo niefortunne. I ogólnie, nie byłoby źle, gdyby gdzieś tu działał jakiś mag. W końcu Alojzy wpadł na pomysł - wejść na Facebooka i robić pogardliwe komentarze o Estrellas Infernal ze strony fanów i o fanach ze strony Estrellas Infernal. Ogólnie, zrobić hardcorową kłótnię, by nie doszło do samego koncertu.

Prawie się udało. Prawie, bo Paradoks. Przeniósł na metalowców (acz nie Estrellas Infernal) echo podziemnego rapu jaszczurów spod Czapkowika. Metalowcy wyszli na scenę i zaczęli RAP BATTLE. Koncert się nie odbył. Operiatrix się najadła neutralną muzą (rap jaszczurów jest raczej wesoły i nieproblematyczny). I tak powstała nowa świecka tradycja...

## Wpływ na świat

* Żółw: 14
* Raynor: 5
* Arleta: 6

Czyli:

* Częścią obchodów Dni Jaszczura stała się tradycja Metalhead Rap Battle
* Marketing i fundusze unijne sprawiły, że Czapkowik zaczął być znany jako miasto od jaszczurów
* The Jaszczur of the Opera, z saksofonem. Nauczył się dzięki Paradoksowi z rapem.
* Mimo epickiej jaszczurowatości, jednak ludzie oglądają Netflixa.
* (claim wątku) Ludzie i jaszczury żyją w zgodzie w Czapkowiku.
* Mają silne wpływy w Czapkowiku - burmistrz im ufa i jaszczury rządzą miastem

# Streszczenie

Inna grupa magów przejeżdżała przez Czapkowik. Okazało się, że pojawia się ryzyko gry zespołu postdeathmetalowego co skazi Operiatrix; magowie zerwali więc koncert (by się nie odbył), lecz w efekcie powstał Metalhead Rap Battle. Dodatkowo, burmistrz Czapkowika, Kamila Woreczek też okazała się być jaszczurem. Magowie podeszli do sprawy racjonalnie - albo wybijemy wszystkich, albo zrobimy z tego atrakcję. I tak powstał plan (wspierany funduszami unijnymi) zrobić z Czapkowika polskie Roswell, ale z gumowymi kiczowatymi jaszczurami (na co burmistrz przystała)...

# Progresja

* Alfred Werner: The Jaszczur of the Opera, z saksofonem. Nauczył się dzięki Paradoksowi z rapem i naukom Pawła Szlezga.
* Operiatrix: nażywiona jaszczurzym rapem wykonanym przez ludzi. Na pewien czas powstrzymany process rozpadu.
* Genowefa Huppert: ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.
* Alojzy Bunnert: ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.

## Frakcji

* Miasto Czapkowik: Marketing i fundusze unijne sprawiły, że Czapkowik zaczął być znany jako miasto od jaszczurów
* Miasto Czapkowik: Częścią obchodów Dni Jaszczura stała się tradycja Metalhead Rap Battle
* Miasto Czapkowik: Mimo epickiej jaszczurowatości, jednak ludzie oglądają Netflixa; Pryzmat się przesuwa z niebezpiecznego na zwykły
* Estrellas Infernal: Mają pogorszone stosunki z ludnością Czapkowika.

# Zasługi

* mag: Genowefa Huppert, społeczno-polityczna jednostka lecznicza. Przekonała burmistrz (która była jaszczurem w przebraniu) do swojego planu integracji świata ludzi i jaszczurów. Ma dobre serce.
* mag: Alojzy Bunnert, łowca jaszczurów i łamacz koncertów. Spił asystenta burmistrz Czapkowika, potem zerwał koncert Estrellas Infernal. A przedtem jeszcze Alfreda złapał.
* vic: Alfred Werner, który grając i upiornie fałszując na saksofonie przeraził dwóch nastolatków. Potem nieprzytomną Olę niósł by udzielić jej pomocy. Lead dla Zespołu. Poczciwy jaszczur.
* czł: Maciek Drzeworóz, siedemnastolatek z Czapkowika; wezwał pomoc magów po tym jak przeraził go jaszczur i zostawił dziewczynę (Olę) samą z nim w operze.
* czł: Aleksandra Kurządek, szesnastolatka z Czapkowika; przez większość misji nieprzytomna, a potem bezużyteczna.
* vic: Kamila Woreczek, burmistrz Czapkowika i jaszczur w przebraniu; rozsądna i chcąca dobrze dla świata jaszczurów i ludzi. Przekształca Czapkowik za pomocą środków unijnych w "polskie Roswell"
* czł: Mateusz Podgardle, asystent pani burmistrz; spity przez Alojzego dał dostęp do Kamili Woreczek Genowefie. Ogólnie, dokładny i precyzyjny acz za dużo pije.
* czł: Kora Panik, aka "Diva Nera", wokalistka i liderka postdeathmetalowej grupy Estrellas Infernal. Może mieć aspekty viciniusa. Nie udało jej się przeprowadzić planu przebudzenia Operiatrix.

# Plany

* Kora Panik: nadal planuje przeprowadzić Awaken w Operze Czapkowickiej.

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik, pod którym FAKTYCZNIE mieszkają jaszczury. Niewielka kolonia. I który zacznie być znany z jaszczurzego kiczu.
                    1. Stara Opera, która czasem służy jako miejsce obściskiwania się nastolatków. I gdzie miał być koncert Estrellas Infernal.
                    1. Bar Gwarek, miejsce zapicia Mateusza Podgardle przez Alojzego (by wbić Genowefę w kalendarz burmistrz Czapkowika)
                    1. Klub Paintballa Snajper, który należy do burmistrz Kamili Woreczek (która jest jaszczurem w przebraniu)

# Czas

* Opóźnienie: 6
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Pokazanie ruiny po Wojnie Karradraela
* Pokazanie potęgi magów i magii
* Wdrożenie nowych graczy

## Po czym poznam sukces

* Udane konflikty
* Obecność sukcesów i porażek
* Radość na twarzach nowych graczy

## Wynik z perspektywy celu

* Sukces, Arleta sama stosowała mechanikę bez pomocy.
* Sukces, Arleta dobrze czuła się w postaci maga.
* Misja - jak widać - bardzo udana.

## Wykorzystana mechanika

Mechanika 1804

Wpływ:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
