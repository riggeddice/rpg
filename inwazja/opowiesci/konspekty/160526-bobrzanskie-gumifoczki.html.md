---
layout: inwazja-konspekt
title:  "Bobrzańskie Gumifoczki"
campaign: anulowane
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

### Kampanijna

* [160526 - Bobrzańskie Gumifoczki (LŁ, AB)](160526-bobrzanskie-gumifoczki.html)

## Punkt zerowy:

Ż: Dlaczego ta miłość - absolutnie bezsensowna, zakazana i... dziwna - zwróciła uwagę Luizy?
B: Recydywa na tylnym siedzeniu taksówki.
Ż: Kto był zaangażowany w ową miłość?
K: Miejscowa nauczycielka (Zofia) i piękny nieznajomy.
Ż: Gdzie oprowadzał Arek grupę turystów - i jaką, gdy zobaczył TO?
K: Grupa gimnazjalna. Miejscowy zamek z legendą o białej damie.
Ż: Co łączy pięknego nieznajomego z grupą gimnazjalistów?
B: Jest przedstawicielem rady rodziców. A imię mu Stefan.

## Misja właściwa:


Dwudziestu ośmiu gimnazjalistów. Jeden przedstawiciel rady rodziców. 
Arek wplótł szybko historię o tym, jak to biała dama słynęła z fok. I innych zwierzątek. Ale zwłaszcza fok. Szybko Arek - korzystając z tego, że wszyscy zapatrzeni są w MEGA FOTOGENICZNĄ FOKĘ - rzucił zaklęcie iluzyjne udowadniające, że to tylko inscenizacja.
I faktycznie, udało mu się (remis). Za bardzo. Przedstawiciel rady rodziców Stefan zaczął opowiadać, jak widać genialną inscenizację i on w ogóle powie o tym burmistrzowi i będzie grant unijny na fokscenizację i sławne foki z zamku...

WTF fotogeniczna foko. Zaiste, wtf?!

Anegdota o białej damie i foce... i wycieczka punkt następny...

Tymczasem Luiza ciągle jest zafascynowana tym, co widziała między Zofią a Stefanem. Konserwatywna, starsza nauczycielka a doszła do drugiej bazy...
Do taksówki biegnie dwóch młodych, zamaskowanych chłopaków. Mają kominiarki i siatki z TESCO. Wyraźnie przed czymś spierniczają. Luiza włącza bieg i odjeżdża - a oni chcą uciekać. Widząc rozpacz uciekających młodziaków, zatrzymuje taksówkę. Wpadli i krzyknęli "na Stolarską". Luiza odpaliła cichy alarm, po czym pojechała pod posterunek. Gdy jeden z nich wyjął nóż, dała po hamulcach - mocno - po czym wyskoczyła z samochodu z kluczykami. Nie udało jej się zwinąć kluczyków więc napastnicy zwinęli auto, ale - jest cichy alarm a samochód i tak jest korporacyjny...

WTF napastnicy?

Policja zwinęła napastników. Z nożem za żelki. Ukradli 4 torby TESCO żelek. Policja ich przesłucha - to uczniowie gimnazjum.

Siedziba gildii. Skrzydło zamku. W Bobrach.
Zdzisława Myszeczka ucieszyła się, że wszyscy aktualnie są. Zdecydowała się na ponowne przeprowadzenie głosowania w sprawie przywódcy gildii. I w sprawie nazwy gildii. Słysząc historie dnia dzisiejszego, Arek zaproponował "bobrzańskie gumifoczki". Zajcew dla jaj zagłosował za, Luiza tak samo i Arek. Zdzisia wyszła wściekła. Zostali Bobrzańskimi Gumifoczkami.
Grigorij podzielił się po raz kolejny podejrzeniami, że koty Zdzisi to coś dziwnego i paskudnego. Jak był pijany, to jeden kot zrzucił go z murów...

Romek wrócił ze Zdzisią (w innej sukni). Ta powiedziała, że nie zauważyła, żeby była jakaś aktywność riftu. Luiza potwierdziła.
Grigorij powiedział, że on się spróbuje z młodymi dogadać, tymi aresztowanymi. W końcu jest ich nauczycielem.

...a tak w ogóle to będzie niedługo dofinansowanie z Unii. Na fokę.

Ogólny plan:
- kot Mruczuś zostanie zmieniony w fokę i doprawiony iluzją. By podtrzymać, zasili się go riftem
- Jeśli z riftu coś wyjdzie, np. foka, zmieni się ją w kota Zdzisi

Arek został przez Zdzisię oddelegowany do tego, by porozmawiać z młodymi napastnikami. Grigorij - by odnaleźć o paskudnym tatuażu. Zdzisia wzięła na siebie znalezienie foki, która się wdzięczy...

Arek porozmawiał z młodymi na komisariacie. Usłyszał niesamowitą opowieść - jest TRZECIA foka...
"Zahipnotyzowała mnie foka z kosmosu więc ukradłem dla niej żelki z TESCO."

Była na piętrze w gimnazjum...

Arek stworzył wersję - młodzi wzięli żelki od nieznajomego (pedo/żelkofila) i im odbiło, zmanipulował ich i oni - ofiary - zatańczyli jak im zagrał pod wpływem narkotyku. A drugi czar - na policjanta. By im uwierzył... i na jego przełożonego też...

Arek wysłał SMSa do wszystkich - są 3 foki.
Luiza uzyskała swoje auto zastępcze. Poplotkowała w centrali taksówkarskiej - chciała dowiedzieć się czegoś o fokach...

I się dowiedziała - wielu ludziom śniła się foka czarna jak noc o czerwonych oczach. Plus, ludzie widzieli fokę z paskudnym tatuażem i fokę wdzięczącą się jak cholera.
W "Bobrze Codziennym" pojawiła się informacja, że jest teraz tydzień foki.

Arek kupił egzemplarz "Bobra Codziennego" i dał go Romanowi. Niech się dowie, kto i kiedy dodał to ogłoszenie. Też poszły SMSy do wszystkich członków Bobrzańskich Gumifoczek - poza Zdzisią. Czas na kolejne spotkanie.

Grigorij się miał dowiedzieć o foce z tatuażem. Znalazł ją, sklupał, przywiózł i wrzucił do kibla. Foka jest w kiblu w zamku. I faktycznie ma paskudny tatuaż. To jest foka z sealem. It's a sealed seal. Foka ma lekką aurę magiczną, ale nie pochodzącą z riftu.

Arek powiedział, że przeanalizował "Bobra Codziennego" - to ogłoszenie pochodzi od Świecy pośrednio.

Zdzisia przyszła, zawołana. Pochwaliła się, że złapała fokę (wdzięczącą się) i teleportującą - zneutralizowała to nagraniami na YT foki i sztuczkami z kotami.
Luiza i Zdzisia muszą współpracować by osiągnąć cel - zorientować się skąd magia na fokach. Luiza szybko przejęła inicjatywę i zaproponowała działanie. Zdzisia jest nieszczęśliwa - Luiza była szybsza. Rywalizowały w "która jest bardziej współpracująca", ale zremisowały...

Analiza aury na fokach wykazała, że fok jest PIĘĆ. Mają się żywić, robić to co foki robią, a docelowo połączą się w rytuale AND THEY WILL CREATE THE ULTIMATE SEAL... jak któraś nie dotrze, to nie dojdzie do połączenia. Nie będzie rytuału.

Jeśli którejś z fok zabraknie, zaklęcia się rozproszą. Rytuał się rozpadnie. 
Mimo obiekcji Zdzisi (ona chce mieć nowego kotka), zdecydowali się wywieźć jedną z fok by rozproszyć cały ekosystem rytuału.
I tak się stało...

# Zasługi

- mag: Arkadiusz Bankierz, TODO
- mag: Luiza Łapińska, TODO
- mag: Zdzisława Myszeczka, TODO
- mag: Roman Weiner, TODO
- mag: Grigorij Zajcew, TODO
- czł: Stefan Głamot, przedstawiciel rady rodziców, TODO 
- czł: Zofia Miczkewoł, starsza nauczycielka, TODO

# Lokalizacje

1. Świat
    1. TODO
        1. Bóbr
            1. Bobrzański zamek
                1. Rift
            1. TODO