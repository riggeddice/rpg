---
layout: inwazja-konspekt
title: "Biznes pośród niesnasek"
campaign: powrot-karradraela
players: kić, raynor, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170315 - Naszyjnik Wspomnień (HB, AB, DK)](170315-naszyjnik-wspomnien.html)

### Chronologiczna

* [170315 - Naszyjnik Wspomnień (HB, AB, DK)](170315-naszyjnik-wspomnien.html)

## Kontekst ogólny sytuacji

-

## Punkt zerowy:

**Stan początkowy**:

1. Essir Supernowej?
    1. Zbudowanie popytu na doskonały energy soczek smakowości i radości - Essir!: 0/10
    2. Zbudowanie Essiru zdolnego do porwania za sobą doskonałych magów: 0/10
    3. Przygotowanie linii produkcyjnej Essiru: 0/10
    4. Wejście do Zaćmionego Serca - kontakty i znajomości: 0/10
    5. Zbudowanie zabezpieczeń i systemów defensywnych: 1/10
    6. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 0/10
    7. Wadliwa partia kogoś skrzywdzi:      0/6
2. Emo-mrok Zaćmionego Serca
    1. Fortyfikacja Pryzmatu Zaćmionego Serca: 0/10
    2. Czarny PR przeciwko Essirowi: 1/10
    3. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 0/10
    4. Będzie jak było i jak jest (finalizacja): 0/10
    5. Brudne sztuczki przeciw przeciwnikom: 0/10
3. Wszystkie złe rzeczy
    1. Ciśnienie w KADEMie między frakcjami AD a KADEM: 0/10
    2. Rozpad Zaćmionego Serca: 0/10
    3. Judyta Maus is framed: 0/10
    4. Essir is poisoned: 1/10
    5. Internal infighting w Essirze: 0/10
    6. External destruction of Essir:   0/10

**Pytania i odpowiedzi**

* Ż: Co do smoka robią za projekt Judyta i Karolina by SUPERNOWA im ostro pomagała?
* K: Judyta to soczki. Supernowa to kucharka, Marcelin to alchemik. Coś w tym kierunku. Najpewniej same nie wiedzą.
* D: Specjalne potionki do generatorów dymu na imprezie. Żeby ludzie się lepiej bawili, łamały bariery. Żeby się mogli odnaleźć. Poncz radości i wyszalenia się. Aha, i dodajcie Karolinę.
* D: Najpewniej współpracują z klubem. Może chcą się dostać do prestiżowego klubu?
* Ż: W jaki sposób Marcelin został wciągnięty we WSPÓLNY PROJEKT z Supernową, Judytą i Karoliną?
* D: Potrzebowały alchemika. Marcelin nie odmówił. Kornelia go ściągnęła - jest ze Świecy.
* Ż: Gdzie Henryk widzi korzyści dla siebie z tego, że ich projekt osiągnie sukces?
* R: Henryk chce się wkupić w koneksje; to są świetne potencjalne macki.
* Ż: Wygeneruj miejsce, gdzie chce przebojem wejść Zespół. Nazwa i aspekt.
* D: Zaćmione Serce
* Ż: Co się stało, że Henryk skosztował Essiru?
* R: Siedział w swoim pokoju hotelowym z gotowym produktem i miał chwilę zwątpienia... a co mi tam. JUST DO IT!
* Ż: Jak Marcelin zdążył dotrzeć do Henryka przed jego śmiercią?
* D: Znalazła go sprzątaczka i zadzwoniła na pogotowie - przeszło do Edwina, odebrał Marcelin.
* Ż: Gdzie jest siedziba główna Essiru?
* K: W klimatycznym pustostanie. Takim z Historią. Kiedyś był tam klub. Niedaleko sadu Judyty.


## Misja właściwa:

**Dzień 1:**

_Rano: (+1 akcja O)_

W wyniku działań początkowych Essir został przesunięty do defensywy; były próby zatrucia sadu Judyty. Też mówi się o ich działaniu źle...

Marcelin podszedł do stawiania Henryka na nogi. Odruchy wymiotne, strzykawka, jak w pulp fiction. Nie adrenalina, ale POMOŻE. 4+2v4 i (-1 surowiec). R. Udało się - ale więcej partii tego cholerstwa już wypłynęło. Czyli cała PARTIA jest wadliwa... i +1 akcja. A Marcelin robił testy na płaszczkach...

Ze dwie skrzynki poszły. Wpadła szczęśliwa Supernowa i powiedziała, że Kornelia dała radę sprzedać skrzynkę rodzicom. Chyba z litości. Niestety, to TA partia. Po chwili paniki Supernowa dowiedziała się od nich, że partia jest zatruta i zadzwoniła do Silurii. A ta pierwsze słyszy...
Po krótkim kontakcie z Karoliną Siluria skontaktowała się z Joachimem Kartelem. 

* nowa ścieżka "wadliwa partia zabije"

Kartel schował Essir do piwnicy. Powiedział, że są złe plotki. Z radością odda. Martwi się, że Kornelia szarga jego dobre nazwisko.

_Południe: (+1 akcja 0)_

Joachim wyjaśnił Silurii problem - podobno Kornelia obnaża się w miejscach publicznych. Gdy się z nią skonfrontował, powiedziała, że zacznie rozbierać się by towar sprzedać. Więc spanikował i kupił wszystko... a podobno za Essirem stoi Żupan i w ogóle. Siluria się aż zdziwiła. 

* skasowana ścieżka "wadliwa partia zabije"

W ramach działań Kartel ściągnie maga, by ten pilnował i dbał o Kornelię. Płatnego maga.

Tymczasem Supernowa przekonała Marcelina, by przetestował partię na NIEJ. Łyknęła to. Zbladła strasznie i jej ciało zaczęło walczyć. Jednak Supernowa jest wspomagana i potężniejsza niż Henryk. Udało mu się zdispellować czar i dowiedzieć się co i jak. Partia jest skierowana na nie-Mausów, ciało Supernowej po prostu pokonało truciznę.

Dobra. Pojechali do szopy w sadzie Judyty gdzie najpewniej doszło do Skażenia. Tam Henryk zdecydował się poszukać echa emocji, z intencją wrogą. 4+3v5->S, ES.

* ES: Echo emocji nawiedza szopę.

Silna niechęć i pogarda wobec Judyty. Dwóch starszych magów walczących i manewrujących obok siebie. To jest element walki między nimi. Ten drugi mag opiekuje się Judytą.

_Wieczór: (+1 akcja 0)_

Marcelin w kawalerce Marcelina, z Henrykiem. Henryk próbował dowiedzieć się magicznie o co tu chodzi. Było tam DWÓCH magów. Jeden Maus chce zatruć i zniszczyć - reputację Judyty. Drugi Maus osłabia i chroni. OBOJE są Mausami.

Marcelin od razu rozpuścił poszukiwania wśród przyjaciółek i znajomych. Mausowie walczący między sobą. Henryk podpytał jeszcze swoje kontakty ze Szlachty. 7v4,7-> R,S (+1 akcja)

Oktawian Maus i Ferdynand Maus. Oktawian jest zwolennikiem współpracowania Mausów z innymi, Mausów różnorodnych i zarówno łagodnych i pomocnych jak i groźnych. Oktawian jest "dobrym wujkiem". Ferdynand jest zwolennikiem Mausów silnych i twardych. Mausów elitarnych; tacy jak Judyta powinni być twardzi lub odejść. Oktawian jest zwolennikiem Karradraela, Ferdynand - siły i potęgi.

Siluria pomagała Supernowej robić linię produkcyjną. Ze standardowym efektem...

I ekipa wróciła do bazy. I ku zdziwieniu zauważyli Silurię budującą linię produkcyjną. Gdy do Supernowej doszło imię Ferdynanda, ta chciała wezwać całą potęgę KADEMu na Ferdynanda. Siluria zmartwiała lekko - odwróciła uwagę Supernowej Marcelinem. Gdy Supernowa wróciła do pracy, Henryk powiedział o tym, że Ferdynand jest problemem - albo odsunąć Ferdynanda od Judyty albo się pozbyć Judyty i znaleźć inną linię. Alternatywą jest wojna domowa w świecie Mausów.

Siluria zdobywa ogląd sytuacji.

_Noc: (+1 akcja S)_

1. Essir Supernowej?:
    1. Zbudowanie popytu na doskonały energy soczek smakowości i radości - Essir!: 0/10
    2. Zbudowanie Essiru zdolnego do porwania za sobą doskonałych magów: 0/10
    3. Przygotowanie linii produkcyjnej Essiru: 5/10
    4. Wejście do Zaćmionego Serca - kontakty i znajomości: 0/10
    5. Zbudowanie zabezpieczeń i systemów defensywnych: 1/10
    6. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 1/10
2. Emo-mrok Zaćmionego Serca:
    1. Fortyfikacja Pryzmatu Zaćmionego Serca: 1/10
    2. Czarny PR przeciwko Essirowi: 2/10
    3. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 1/10
    4. Będzie jak było i jak jest (finalizacja): 1/10
    5. Brudne sztuczki przeciw przeciwnikom: 0/10
3. Wszystkie złe rzeczy:
    1. Ciśnienie w KADEMie między frakcjami AD a KADEM: 1/10
    2. Rozpad Zaćmionego Serca: 0/10
    3. Judyta Maus is framed: 0/10
    4. Essir is poisoned: 2/10
    5. Internal infighting w Essirze: 2/10
    6. External destruction of Essir: 0/10
    7. Problemy Joachima z Kornelią: 0/8

**Dzień 2:**

_Rano: (+1 akcja O)_

Siluria udała się do Franciszka Mausa. Przedstawiła mu sytuację - Essir, Judyta. Poprosiła, by objął to wszystko patronatem. 3+2v4->R. Franciszek się zgodził, ale poprosił, by Siluria poszła do Warmastera zapewnić jej jakąkolwiek defensywę. (+1 akcja)

Warmaster ucieszył się że widzi Silurię. Są JACYŚ magowie któzy robią Essir. I oni chcą wejść do Zaćmionego Serca. Warmaster nie pozwoli. Chcą ściągnąć TECHNOWIKINGA by splugawić Serce; więc Warmaster zbudował plotkę, że za tym stoi Żupan. Siluria z zainteresowaniem słuchała, jak to za działaniami przeciw Essirowi stoi jej kolega z gildii działając też przeciw... koleżance. Warmaster rozmawiał o tym z Karoliną, ta go zignorowała, więc zgodnie ze starym typowym stylem AD Warmaster zaeskalował.

Siluria poprosiła Warmastera o jakąś ochronę dla Judyty (nie mówiąc, że ta jest powiązana z Essirem). Warmaster się naturalnie zgodził.

Gdy Siluria wróciła do reszty zespołu, przedstawiła im problem. Ustalili, że przekonają razem Supernową. I faktycznie, udało się. Wynik:

* Essir skupi się na zbudowaniu klubu Ogniste Serce, konkurencyjnego do Zaćmionego Serca
* Ściągną technowikinga
* Ku wielkiej rozpaczy wszystkich, Supernowa wymyśliła budowanie klubu od wypalania własnych cegieł.
* Przekona się Karolinę.

**Podsumowanie torów**

1. Essir Supernowej?:
    1. Zbudowanie popytu na doskonały energy soczek smakowości i radości - Essir!: 0/10
    2. Zbudowanie Essiru zdolnego do porwania za sobą doskonałych magów: 1/10
    3. Przygotowanie linii produkcyjnej Essiru: 6/10
    4. Wejście do Zaćmionego Serca - kontakty i znajomości: 0/10
    5. Zbudowanie zabezpieczeń i systemów defensywnych: 2/10
    6. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 1/10
2. Emo-mrok Zaćmionego Serca:
    1. Fortyfikacja Pryzmatu Zaćmionego Serca: 2/10
    2. Czarny PR przeciwko Essirowi: 3/10
    3. Przedekorowanie Wielkiego Eventu Zaćmionego Serca: 2/10
    4. Będzie jak było i jak jest (finalizacja): 1/10
    5. Brudne sztuczki przeciw przeciwnikom: 0/10
3. Wszystkie złe rzeczy:
    1. Ciśnienie w KADEMie między frakcjami AD a KADEM: 1/10
    2. Rozpad Zaćmionego Serca: 0/10
    3. Judyta Maus is framed: 1/10
    4. Essir is poisoned: 2/10
    5. Internal infighting w Essirze: 3/10
    6. External destruction of Essir: 1/10
    7. Problemy Joachima z Kornelią: 0/8

**Interpretacja torów:**

Karolina zabezpieczyła podstawowo siedzibę główną Essiru przed atakiem. Supernowa z resztą zespołu skupili się na budowie linii produkcyjnej - i mają duże postępy. Tymczasem działania Warmastera doprowadziły do eskalacji czarnego PR przeciw Essirowi a Zaćmione Serce zmobilizowało się w obronie memetycznej. W samym Essirze pojawiają się lekkie konflikty wewnętrzne między Supernową a... resztą. Kornelia i Judyta mają inną wizję Essiru niż Supernowa.

**Przypomnienie Komplikacji i Skażenia**:

Skażeniu uległ sad Judyty - echem zwarcia Ferdynanda i Oktawiana. Jej własny sad nią gardzi.

# Progresja

* Judyta Maus: Sad Judyty uległ Skażeniu - echem zwarcia Ferdynanda i Oktawiana. Jej własny sad nią gardzi.
* Judyta Maus: ma patronat Franciszka Mausa i ochronę viciniusów Warmastera.

# Streszczenie

Na szczycie Mausów wojna o wizję rodu. Oktawian ma konserwatywne, kooperacyjne podejście a Ferdynand ma pro-drapieżne, izolacjonistyczne podejście. Zogniskowało się to wokół Judyty Maus, która AKURAT założyła biznes (Essir) z kilkoma innymi magami. Przez to, że Essir chciał wejść do Zaćmionego Serca, skonfliktowali się z Warmasterem... a Kornelia (córka Joachima) dokucza ojcu na każdym kroku. Siluria, Henryk i Marcelin musieli sobie z tym wszystkim poradzić.

# Zasługi

* mag: Siluria Diakon, załatwiła patronat Franciszka, dowiedziała się o problemach z Zaćmionym Sercem i rozwiązywała konflikty ludzkie.
* mag: Henryk Siwiecki, jedyny głos biznesowy w całej spółce (już żałuje). Poszerza krąg znajomych. Unika niepotrzebnych konfliktów. Poznał naturę przeciwnika - wojna Mausów.
* mag: Marcelin Blakenbauer, master lekarz i resuscytator; uratował Henryka i badał Supernową. Wybadał Skażony fragment eliksiru i sporo robi dla dziewczyn.
* mag: Judyta Maus, dostawca najlepszych soczków i owoców; okazuje się, że jest namierzana przez Ferdynanda Mausa i chroniona przez Oktawiana Mausa w wojnie o to czym mają być Mausowie.
* mag: Kornelia Kartel, która odnajduje się w roli sprzedawcy i 'reklamy przez strach ojca'. Przeraziła ojca tym, że będzie się rozbierać by sprzedawać essir.
* mag: Karolina Kupiec, dla której essir jest sposobem by wnieść ekstazę i przyjemność do Zaćmionego Serca. Główna alchemiczka zespołu. Też fortyfikuje bazę ;-).
* mag: Supernowa Diakon, wulkan energii, która wszystko hiperbolizuje i zmusza wszystkich do ogromnego wysiłku. Motywator i główny silnik biznesowego przedsięwzięcia.
* mag: Joachim Kartel, ojciec Kornelii, który kupił całą skażoną partię essiru, by tylko jego córka się nie zbłaźniła. Nie do końca radzi sobie z buntem młodej.
* mag: Oktawian Maus, z frakcji "Mausowie powinni żyć z innymi", konserwatywny i pro-Karradraelowy. Chroni Judytę jak może.
* mag: Ferdynand Maus, z frakcji "Mausowie powinni być drapieżny i bez Karradraela". Gardzi Judytą jako słabą i działa tak, by zdobyć popleczników.
* mag: Franciszek Maus, poproszony przez Silurię objął Judytę patronatem, chroniąc ją przed przyszłymi atakami. 
* mag: Warmaster, który przy całej swojej łagodności NIE odda Zaćmionego Serca. Rozpuścił plotki i zaczął mobilizować Zaćmione Serce do fortyfikacji pryzmatycznej i memetycznej.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Górny
                    1. Obrzeża
                        1. Osiedle Polne
                            1. Pustostan Żelazny, gdzie kiedyś był jakiś klub a teraz ma siedzibę główną Essir.
                        1. Ogródki działkowe
                            1. Sad Zielonej Myszy, Skażony przez czar Henryka, który przesunął tu echo emocjonalne czarów Ferdynanda. Czyli: sad gardzi Judytą.
                1. Kopalin
                    1. Nowy Kopalin
                        1. Emoklub Zaćmione Serce, fortyfikowany pryzmatycznie i memetycznie przez Warmastera.

# Czas

* Opóźnienie: 2
* Dni: 2

# Frakcje


## Essir Supernowej?

### Scena zwycięstwa:

* Zaćmione Serce będzie miało radość. Każdy znajdzie coś dla siebie. I taka mała piwnica dla emo ;-)
* JAK MOŻNA TAK SIEDZIEĆ, SMĘCIĆ I NIC NIE ROBIĆ. Energia do Zaćmionego Serca. Ludzie robią rzeczy!
* Ich grupa jest ważną frakcją działającą w Zaćmionym Sercu

### Motywacje:

* Nie można siedzieć i smęcić. Trzeba DZIAŁAĆ!
* Radość jest najważniejszą rzeczą na świecie!
* Mamy girlsband i nie zawahamy się go użyć!
* Wejdziemy przez najlepsze produkty, niezależnie od sposobu!

### Siły:

* Karolina, Kornelia, Judyta, Supernowa.
* Marcelin.

### Ścieżki:

1. Essir Supernowej?
    1. Zbudowanie popytu na doskonały energy soczek smakowości i radości - Essir!:  0/10
    1. Zbudowanie Essiru zdolnego do porwania za sobą doskonałych magów:            0/10
    1. Przygotowanie linii produkcyjnej Essiru:                                     0/10
    1. Wejście do Zaćmionego Serca - kontakty i znajomości:                         0/10
    1. Zbudowanie zabezpieczeń i systemów defensywnych:                             0/10
    1. Przedekorowanie Wielkiego Eventu Zaćmionego Serca:                           0/10
    1. "We fight together" - wielka przyjaźń                                        0/10


## Emo-mrok Zaćmionego Serca

### Scena zwycięstwa:

* Essir jest wyparty; będzie jak było i jak miało być
* Rozbicie frakcji przeciwnej

### Motywacje:

* To nasz mrok. Nasze życie. Nikt w to nie wlezie. Zbudujcie własny klub.
* To nasze wydarzenia i eventy. Niczyje inne. Nie dzielimy się.

### Siły:

* Warmaster
* Akolici Mroku

### Ścieżki:

1. Emo-mrok Zaćmionego Serca
    1. Fortyfikacja Pryzmatu Zaćmionego Serca:              0/10
    1. Czarny PR przeciwko Essirowi:                        0/10
    1. Przedekorowanie Wielkiego Eventu Zaćmionego Serca:   0/10
    1. Będzie jak było i jak jest (finalizacja):            0/10
    1. Brudne sztuczki przeciw przeciwnikom:                0/10


## Wszystkie złe rzeczy jakie mogą się stać

### Scena zwycięstwa:

* Wojna domowa KADEMu między frakcjami
* Wojna domowa w Świecy, wojna KADEM/Świeca
* Judyta Maus musi mieć Karradraela do ochrony

### Motywacje:

* brak

### Siły:

* Ferdynand vs Oktawian
* Wszystkie inne frakcje ;-)

### Ścieżki:

1. Wszystkie złe rzeczy
    1. Ciśnienie w KADEMie między frakcjami AD a KADEM:     0/10
    1. Rozpad Zaćmionego Serca:                             0/10
    1. Judyta Maus is framed:                               0/10
    1. Essir is poisoned:                                   0/10
    1. Internal infighting w Essirze:                       0/10




# Narzędzia MG

## Opis celu misji



## Cel misji

1. O-O-O-S-O-O-O-S, gdzie O to operacyjne (wzrost ścieżki) a S to komplikacje (nowa ścieżka).
    1. O: przeciwnik dał radę podnieść JAKĄŚ ścieżkę. Jeden lub wszyscy. Generowana jest nowy Postęp Przeciwnika.
    1. S: przeciwnik ma NOWĄ ścieżkę. Jeden lub wszyscy. +1 akcja dla wszystkich przeciwników. Generowana jest nowa Komplikacja Fabularna (wątek).
1. Dzień składa się z 4 faz, domyślnie. Normalne postacie w jednej fazie śpią ;-).

## Po czym poznam sukces

1. Nie ma tak szybkiego galopowania ścieżek; rozwadnia się sytuacja by gracz mógł wygrać jeśli bardzo zależy.
1. Nie ma tak dużego stresu wynikającego z pola widzenia graczy.
1. Mniej wymyślania wątków, więcej postępu przeciwnika
1. Lepszy upływ czasu - czas płynie

## Wynik z perspektywy celu

1. Żółw spał na misji, niewiele pamięta

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |
