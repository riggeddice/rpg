---
layout: inwazja-konspekt
title:  "Jasny sygnał Tamary"
campaign: wizja-dukata
players: kić, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171004 - Niestabilna magitrownia (PT, DA)](171004-niestabilna-magitrownia.html)

### Chronologiczna

* [171004 - Niestabilna magitrownia (PT, DA)](171004-niestabilna-magitrownia.html)

## Kontekst ogólny sytuacji

### Siły główne

**Tamara Muszkiet vs Robert Sądeczny**

* Tamara chce zniszczyć sensory Roberta i wysłać jasny sygnał - ona nie uznaje tutaj władzy mafii
* Tamara chce zakończyć dezinformację
* Tamara chce pokazać, że Świeca na tym terenie się liczy i ma znaczenie
* Robert jest lekko pogubiony; nie spodziewa się tak ciężkiego uderzenia. Chce dowiedzieć się o co chodzi.
* Robert chce utrzymać swoje czujniki i odbudować sieć informacyjną. Jakoś.

**Oktawia Aurinus**

* Oktawia chce sprawić, by Oliwia zaśpiewała na jednym koncercie dla mas


### Stan aktualny



### Istotne miejsca

* Mżawiór, gdzie znajduje się Portalisko Pustulskie
* Rogowiec, gdzie znajduje się hodowla Myszeczki
* Męczymordy, gdzie znajduje się magitrownia

## Punkt zerowy:

## Misja właściwa:

**Wcześniej**

* "Tien Bankierz, przybyłam jako wsparcie." - Tamara do Sylwestra
* "Jakkolwiek pani reputacja jest imponująca, jest pani tylko jedną terminuską" - Sylwester, zafrasowany
* "Powinnam wystarczyć, tien Bankierz" - Tamara, ze spokojem
* "Przeciw sobie mamy mafię, pełną dezinformację, nie mamy przyczółka w tym terenie i nie jesteśmy poważani" - Sylwester, próbując pokazać Tamarze problem
* "Zajmę się tym, tien Bankierz. Zacznę od dezinformacji." - Tamara, uprzejmie się kłaniając

**Dzień 1**:

Do Pauliny przyszedł Hektor zobaczyć w jakim stanie jest ogródek. Paulina się ucieszyła. Sama poszła zająć się pacjentami. Gdy to wszystko się jakoś rozładowało, zaczepiła go o Katię. Ale tak subtelnie. Hektor powiedział, że Eliksir Aerinus ma kilku magów pracowników i bardzo charyzmatycznego szefa. Paulina przesunęła na temat Katii (5v7->SS) by się dowiedzieć czegoś o niej czy coś się zmieniło te 2 miesiące temu, gdy sygnał był w efemerydzie. Skonfliktowanie sukcesu: Hektor czuje się winny, że zrobił coś ze zmianą materiałów eliksiru.

Hektor powiedział Paulinie, że te dwa miesiące temu przyszła do niego Stefania. Powiedziała, że spieszy się zrobienie pewnego eliksiru i trzeba zamienić pewne surowce na substytuty. On to zrobił, nawet jeszcze je dokalibrował odpowiednio. A potem był ten wypadek; nikt nie ucierpiał, ale od tej pory Katia mniej agituje (nie agituje), pracuje nad projektami których wcześniej odmawiała łącznie z eliksirami "dla wojska". Hektor podejrzewa, że są dla mafii. Trwa to za długo by to był efekt eliksiru zdaniem Hektora. Paulina się zasępiła. Widzi SPISEK. (-2 karty, +1 pkt Paulina)

Gdy Hektor sobie poszedł, Paulina dostała interesujące wezwanie - "terminus w gotowości bojowej". To Kajetan! Paulina zaprosiła go do siebie gorąco. I przyszedł. Dostał przytul. Opowiedział Paulinie, że Sylwester ściągnął wsparcie terminusów... pojawił się on i Tamara Muszkiet. Paulina się żachnęła - czemu Tamara? Kajetan też nie wie. Kajetan przyszedł do Pauliny prosić o intel - jej ufa a nikomu innemu nie może. Tamara zdaniem Kajetana nie chce się dzielić swoim planem - ale on z nią też się nie podzielił... Paulina scharakteryzowała Kajetanowi lokalnych magów i sytuację. Nie powiedziała o czujnikach w magitrowni. Powiedziała też, że ma ze sobą Diakonkę. Kajetan się nie ucieszył, ale Paulina powiedziała, że to fajna Diakonka ;-).

Kajetan się zdziwił, że tak silną akcję robi Dukat - to nietypowe. Gdy Paulina powiedziała, że Dukatowi udało się postawić chorego syna w ciele konstruminusa, Kajetan się zasępił - jest ryzyko, że Dukat nie panuje w pełni nad swoją organizacją bo się już w pełni nie interesuje. Paulina ostrzegła jeszcze Kajetana o Harvesterze; Kajetan powiedział, że będzie miał na to oko. Paulina przeszła w końcu do meritum - Katia. Powiedziała Kajetanowi wszystko co wiedziała; ten powiedział, że to poważna sprawa. Jakoś się musi dowiedzieć co się stało i zająć się sprawą. Problem w tym, że Kajetan nie miał jak się tego dowiedzieć. Cóż, musi się nie tłumaczyć i na tym się skończy. Kajetan powiedział, że powie, że dlatego przybył. (-1 karta)

A tymczasem Tamara odwiedziła magitrownię Histogram z nieszczęśliwym Marcinem Warinskym. Skontaktowała się z Karolem Marzycielem, który odwołał się do Daniela. Daniel się dowiedział od Marcina, że Tamara musi coś znaleźć i zapewnić, żeby na pewno nie doszło do Skażenia magitrowni w przyszłości. Daniel lekko zgłupiał - o co tu chodzi. Warinsky coś mówi o jakimś bycie technomantycznym szukanym przez Tamarę. Ok... to Daniel wysyła Dracenę. A Dracena? Poznała Tamarę i powiedziała Danielowi, że jest przeciwna robić cokolwiek z Tamarą - jest to niebezpieczna fanatyczka Świecy. Więc... Daniel wysłał Dracenę samą z misją nie robić kłopotów.

Oczywiście Dracena i Tamara się pokłóciły. A dokładniej, Dracena pokłóciła się z Tamarą, która Dracenę zignorowała. Warinsky znalazł inne punkty przyłącza czujników Sądecznego. Dostawszy co chciała, Tamara oddaliła się z magitrowni zostawiając kompensatory i bezpieczniki. Daniel nadał Dracenie temat przeanalizowanie tych bezpieczników - okazało się, że... są bezpieczne. Chronią przed konkretnym TYPEM energii, dość Skażonym. Oki, Daniel montuje - z failoverem, żeby dało się łatwo odłączyć. A Karol Marzyciel ma za zadanie przekalibrować czujniki - chce wykrywać ten typ energii tak samo.

**Dzień 3**:

Potężny energy surge, zatrzymany przez bezpieczniki Tamary. Coś się stało poza magitrownią, silna wiązka i silny impuls energii. Rozjarzyło dużą porcję sieci. Jednoczesne działanie w wiele miejsc. Daniel się zaniepokoił; zadzwonił do Sądecznego. A ten - wściekły jak fretka. Okazuje się, że jego czujniki nie działają. Sądeczny powiedział, że oczekiwał od Daniela że jak przyjdzie terminuska to on mu powie. Daniel zauważył, że to Sądeczny odpowiada za ochronę tego terenu i on nie czuje się szczególnie chroniony...

W wyniku rozmowy tej dwójki wyszło co następuje:

* Dracena dowiaduje się, że Daniel pracuje z mafią przeciw Tamarze
* Sądeczny szuka dookoła Tamary
* Daniel idzie sprawdzać i naprawiać czujniki

Dracena Daniela wyśmiała. Wpierw jest niewolnikiem mafii, teraz podlizał się Świecy i znowu mafii będzie czapkował? Jest przedsiębiorcą czy niewolnikiem? Daniel powiedział, że to nie tak - chroni się. Dracena wyśmiała ponownie i wróciła do pracy w magitrowni. A Daniel poszedł eksplorować najbliższy czujnik który nie działa. I znalazł tam przyczepkę - drona, który zagłusza. Gdy próbował rzucić zaklęcie, zza drzewa wyszedł przedstawiciel Legionu Tamary (pusty power suit) i zauważył, że Daniel nie wygląda jak Robert Sądeczny. Gdy Daniel zaczął się żołądkować, Legion odesłał go do Tamary.

Tamara przyjęła telefon uprzejmie. Daniel powiedział, że tak nie można, że ona szkodzi Świecy na tym terenie. Tamara powiedziała, że ona ratuje Daniela od mafii i wierzy, że Daniel nie chce współpracować z mafią. Więc dała mu plausible deniability. Powiedziała, że niebezpiecznie jest wybierać przegrywającą stronę. Na szczęście, powiedziała mu, że wierzy, że Daniel jest po stronie wygrywającej - po stronie Świecy. On się aż zatchnął lekko. Tamara ostrzegła przed odbudową czujników - użyła Skażenia szkodliwego dla magów. Nie chcemy rezonować efemerydy, prawda..?

...Daniel jest naprawdę wściekły i nic nie może zrobić. Może. Poprosił Myszeczkę o glukszwajna i wyjaśnił Sądecznemu sprawę. Sądeczny się zgodził znaleźć maga do pilnowania glukszwajna by spokojnie i konsekwentnie rozmontować tą energię i powoli odbudować system czujników. Niefart.

Tymczasem w innym miejscu, nad portaliskiem jest 8 potencjalnych klientów i Krzysztof z awianami. Krzysztof pilotuje master awiana. Nagle - dziwna energia z portaliska. Krzysztof zignorował, kontynuując akrobacje i pokaz. I... wszystkie awiany wyrwały się spod kontroli, lecąc na Senesgrad. Na efemerydę Senesgradzką...

Ok, to poważna sprawa. Krzysztof próbuje zrozumieć co w sumie się dzieje - lekko Paradoksalnym zaklęciem uwolnił swojego awiana i Skaził energię nad portaliskiem ku utrapieniu katalistów. Zaciemnił aurę. Wyczuł, że jakiś byt z efemerydy podobny wizualnie do jego szefowej, Oliwii, jest w awianach. Następnym ruchem Krzysztofa było zaciemnienie energii - awiany NIE lecą na efemerydę. Lecą nad montownię awianów. Udało się zmylić Oktawię (byt z efemerydy).

Chwilę później Oktawia zdecydowała się rzucić zaklęcie. Krzysztof je rozproszył. Zaklęcie to miało połączyć awiany w jeden byt. Oktawia skomunikowała się z Krzysztofem z poziomu jego awiana i powiedziała, że nie zgadza się na coś takiego. On jej przeszkadza. Krzysztof wykazał się wybitną dyplomacją i powiedział jej "idź sobie, przeszkadzasz". Oktawia się żachnęła i rozłączyła. Gdy już podlecieli nad bazę, Krzysztof nacisnął guzik wywalający klientów w guardian windach do montowni; ewakuacja katapultowa. Oktawia nie zdążyła zareagować... ale zdążyła katapultować też Krzysztofa. Ten, lecąc do ziemi, zobaczył jak ona macha mu złośliwie z awiana.

Bardzo szybko Krzysztof ostrzegł Oliwię o problemie i powiedział jej, że ma do czynienia z kimś przedstawiającym się jako "Oktawia". Oliwię zmroziło - przekazała klientów dalej i wpadła do innego awiana i leci za Krzysztofem (który wsiadł w swojego szybkiego awiana i poleciał za kluczem porwanych awianów). Gdy Krzysztof dogonił awiany, Oktawia stworzyła hologram swojej obecności. Powiedziała, że ma zakładników i oni umrą - chyba, że Oliwia zaśpiewa na stadionie jak kiedyś o tym marzyła.

Krzysztof odwrócił uwagę Oktawii epickim pokazem. Paradoksalne zaklęcie wysłało pokaz i muzykę do internetu - Backstreet Krzysztof Gwiazda Youtube. Faktycznie zafascynował Oktawię co pomogło Oliwii obronić się przed "zdradziłaś swoje marzenia". Oliwia obiecała Oktawii jej własnego awiana - takiego, jaki umie śpiewać. Oktawia dostała to, co chce i się rozproszyła. Oliwia i Krzysztof odetchnęli z ulgą. Ale czemu efemeryda się uaktywniła? W jakim jest stanie?

Do efemerydy pojechali samochodem Krzysztof i Oliwia. Co się dzieje. A Efemeryda Senesgradzka jest rozjarzona. Aktywna i ma dodatkowe kanały energii. Stare Weinerowe dampery i bezpieczniki są pouszkadzane i aktywne. Oliwia skupiła się na naprawianiu, Krzysztof na jej asekuracji. Jak tylko doprowadzili szkielet do działania, Oliwia bardzo ostrożnie weszła w efemerydę magicznie by ją uspokoić. Krzysztof zajął się dopływami energii, Oliwia uspokajaniem efemerydy.

Paradoksalna Naprawa Efemerydy. TECHNOMANCJA. Energia poszła w magitrownię... czas na powrót Oktawii. W magitrownię i w power suit Legionu Tamary też.

**Później**

* "To, co zrobiłaś niekoniecznie jest korzystne. Więcej, to złe. To praktycznie wypowiedzenie mafii wojny." - Sylwester do Tamary, z powagą
* "Jesteśmy Świecą. Wysłałam sygnał. Nie akceptujemy ich władzy." - Tamara, z nienaganną uprzejmością
* "Ryzykowałaś destabilizację tej efemerydy i ryzykowałaś, że lokalne siły odwrócą się przeciwko nam." - Sylwester, zirytowany
* "Powiedz, że działałam na własną rękę." - Tamara, wzruszając ramionami - "Czujniki już są niemożliwe do odbudowania. Wyrównałam trochę warunki."
* "Nie zrobię tego, Tamaro. Nie rzuca się swoich ludzi na pożarcie" - Sylwester, podwójnie zirytowany
* "Tylko Świeca ma znaczenie, Rezydencie Srebrnej Świecy. Zrobisz to, co jest Świecy potrzebne." - Tamara, ze spokojem
* "Ale o tym decyduję ja. Nie Ty." - Sylwester
* "Oczywiście, Rezydencie." - Tamara, z ukłonem

## Wpływ na świat:

JAK:

* 1: dodanie faktu lub okoliczności wobec 'neutral' lub 'claim'
* 1: counter działania MG z sensowną propozycją alternatywną (wobec CLAIM)
* 2: postawienie CLAIM.
* 3: przesunięcie zgodne z motywacją
* 5: przesunięcie niezgodne/sprzeczne z motywacją
* 5: przesunięcie wobec postaci z CLAIM wbrew graczowi

MG: 13 kart

* Żółw: 
    * CLAIM: Dracena i Oktawia będą miały duet.
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    * Bogata klientka chce polatać wyczynowo awianem niedaleko PoI (min. magitrowni). Bankierzówna. Potencjalny kontrakt.
    * CLAIM: Szybki, dyskretny awian. Trafi do Draceny.
* Kić:
    * fakt: Oktawia w power suit ma dostęp do tego co wie power suit. Wie to, co Tamara przekazała.
    * CLAIM: Oktawia dorośnie i zniknie.
    * fakt: Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    * fakt: Tamara robi się bardzo podejrzliwa wobec Myszeczki
* Raynor:
    * fakt: Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał.
    * CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela
    * Sądeczny nasyła Zofię na szpiegowanie Tamary
    * fakt: Sądeczny dostarczy maga, który zajmie się glukszwajnem
* Foczek:
    * CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów
    * CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii
    * Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    * fakt: to co poszło na YT nie zraża ludzi do oglądania. Bardziej jako reklamówka niż rzecz negatywna.

# Progresja



# Streszczenie

Tamara zdecydowała się uderzyć w sensory Sądecznego i Skaziła mu wszystkie sensory korzystając z rekonesansu w magitrowni Histogram. W wyniku tego doszło do zamanifestowania Oktawii Aurinus z efemerydy. Pilot i konstruktor awianów, Krzysztof wsparty przez Oliwię Aurinus wygasili Oktawię i samą efemerydę. Tymczasem Daniel przekonał Sądecznego, że działania Tamary to nie jego wina i pojawi się glukszwajn wyżerający Skażoną energię, by móc znowu montować czujniki. A Paulina spotkała się z Kajetanem i nadała mu temat Katii Grajek.

# Zasługi

* mag: Paulina Tarczyńska, zdobyła informacje od Hektora odnośnie Katii i przekazała wszystko co wie Kajetanowi. Potem wróciła do pacjentów - było ostro.
* mag: Krzysztof Grumrzyk, uratował klientów zakładników z okupowanych awianów, przekonał Oktawię, by go wspierała i został gwiazdą Youtuba dzięki Paradoksowi.
* mag: Daniel Akwitański, chciał uniknąć Tamary (wyjść dyplomatycznie z sytuacji), więc Skaziła wszystkie czujniki Sądecznego bez pytania. Wymyślił, że naprawi sprawę glukszwajnem. Jednym.
* mag: Oliwia Aurinus, zdiagnozowała problem z rozżarzoną efemerydą Senesgradzką i z pomocą Krzysztofa ją wygasiła. Też: doprowadziła do rozproszenia Oktawii. Szefowa montowni awianów i strażniczka efemerydy.
* mag: Hektor Reszniaczek, pracuje nadal dzielnie nad ogródkiem Pauliny i powiedział jej, że ma coś wspólnego z problemami Katii - nadał info o Stefanii Kołek. Ale chyba z Katią ok?
* mag: Kajetan Weiner, Paulina przekazała mu temat Katii Grajek skrzywdzonej w okolicy. Zsynchronizował wiedzę z Pauliną.
* mag: Stefania Kołek, w Eliksirze Aerinus, najpewniej współpracuje z Dukatem; wrobiła Hektora w coś, co może być powiązane z perypetiami Katii.
* vic: Oktawia Aurinus, efemeryda powołana przez Efemerydę Senesgradzką sprzężona z Oliwią Aurinus. Przejęła awiany Krzysztofa i chciała doprowadzić do tego, by Oliwia śpiewała przez... szantaż.
* mag: Dracena Diakon, skonfliktowana z Tamarą za przeszłość; prychnęła na Daniela za brak kręgosłupa i wysługiwanie się mafii. Dalej pracuje w magitrowni.
* mag: Tamara Muszkiet, pojawiła się w Powiecie Pustulskim, uderzyła w czujniki Sądecznego i wysłała prosty sygnał - Świeca nie akceptuje nie-gildiowych form kontroli. Spokojna i zimna jak nigdy przedtem.
* mag: Marcin Warinsky, katalista Świecy zdolny do Skażania energii w dość paskudny sposób; pomógł Tamarze uszkodzić sieć czujników Sądecznego.
* mag: Sylwester Bankierz, który ściągnął Tamarę, dał jej wolną rękę i nie spodziewał się tak brutalnego z jej strony ataku na Sądecznego. Zabronił jej dalszych działań tego typu.
* mag: Robert Sądeczny, nie docenił determinacji Tamary i stracił sieć czujników. Ściągnął glukszwajna i próbuje zregenerować to, co stracił, ale wszystko wypada mu z rąk.
* vic: Efemeryda Senesgradzka, rozżarzona przez konflikt Tamary i Sądecznego. Oliwia ją uspokoiła, ale Efemeryda jest już obudzona i widzi anomalie na tym terenie.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, miejsce spotkań Pauliny z Kajetanem i Hektorem i miejsce przyjmowania pacjentów.
                1. Mżawiór
                    1. Portalisko Pustulskie, nad którym krążyły awiany i które było punktem manifestacji Oktawii Aerinus.
                1. Męczymordy
                    1. Magitrownia Histogram, którą odwiedziła Tamara chcąc znaleźć sposób sabotowania czujników Roberta Sądecznego. Znalazła.
                    1. Las szeptów, gdzie Daniel sprawdził jeden z czujników Tamary i gdzie spotkał agenta Legionu.
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, rozżarzona przez konflikt Tamary i Sądecznego efemeryda, wygaszona i uspokojona przez Oliwię.
                1. Krolżysko
                    1. Montownia awianów Lotnik, gdzie była awaryjnie ewakuowana grupa klientów i gdzie montowane są awiany 

# Czas

* Opóźnienie: 2
* Dni: 3

# Wpływy przeszłe na kampanię

* Katia Grajek:
    * To Hektor pomaga w budowaniu eliksirów dla Katii (3)
* Nowa Melodia:
    * Robert dostaje haracz od "Nowej Melodii" (3)
    * Fakt: Dracenę wylano z "Nowej Melodii" za radykalizm (1)
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii (3)
* Dukat vs Myszeczka:
    * rezydent polityczny Dukata u Myszeczki (3)
    * Myszeczka staje po stronie rezydenta Świecy (3)
* Wejście Świecy na teren Dukata
    * Sylwester Bankierz sprowadza terminusów (3)
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i elektrownia nie miałoby tragedii (3)
    * Sylwester: prowadzi udaną akcję zdobycia transportu Dukata (3)
    * Kić: CLAIM "Trudna droga Sylwestra do zrozumienia terenu" (2)
    * "Sylwester został zesłany tu przez Newerję" (1)
    * Kić: COUNTER Dukat: "Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (1)
    * Kić: CLAIM: Dukat lubi Paulinę (2)
    * Fakt: Jeden z terminusów wezwanych przez Bankierza to Kajetan Weiner. (1)
* Wejście Zespołu Znikającej Świni
    * w okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą (1)
    * ciężarówka ze świniami jest półdostępna w okolicach Męczymordy (1)
    * goście od świń założyli grupę polującą na znikające świnie (1)
    * gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! : 1
    * gościu od znikających świń ma przydupasa którego misją jest to udowodnić! : 1
* Magitrownia Histogram
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej (1)
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana (3)
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by skażenie nie dotarło do elektrowni - to zbyt ważne. (3)
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej (3)
* Nie powiązane
    * Maria: dowie się o złych rzeczach jakie robiły siły Dukata (3)
    * pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów (3)
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia: 3
    * Kić: CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (2)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką" (1)
    * pielęgniarz od zwierząt (Henryk) z wiedzą o działaniach Myszeczki dla Natalii: 1
    * fakt: Harvester nie ma dość energii by się obudzić: 1
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera: 1
    * Natalia pozyskała bezpiecznie kolejną część artefaktu: 1
    * Kić: CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (2)
    * Kić: CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (2)


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
