---
layout: inwazja-konspekt
title:  "'Lord Jonatan'"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140505 - Musiał zginąć, bo Maus (AW)](140505-musial-zginac-bo-maus.html)

### Chronologiczna

* [140505 - Musiał zginąć, bo Maus (AW)](140505-musial-zginac-bo-maus.html)

## Wątki

- Powrót Mausów

## Punkt zerowy:

## Misja właściwa:

Wyjaśniwszy, co była w stanie i zrozumiawszy sytuację nieco lepiej, Andrea zapomniała o Kornelii i Vuko. Miała ważniejsze sprawy na głowie.

Udała się wreszcie ponownie na miejsce, gdzie wcześniej znajdował się rdzeń. Na tym terenie znajdowało się już złożone ad hoc skrzydło terminusów mające za zadanie oczyścić teren. 
Andrea chciała dopilnować, by to skrzydło nie domyśliło się za dużo.

Przed udaniem się w okolice rdzenia, Andrea ściągnęła dane wysłanego oddziału oraz wszystko, co mogła znaleźć odnośnie terenu, na którym cała sprawa miała miejsce. Okazało się, że był to teren uznawany za tak bezpieczny, że często miały tam miejsce akcje treningowe terminusów, "bo przecież nic tam się nie może stać".

Jeśli chodzi o oddział, jest to skrzydło "praktykanckie", szkoleniowe. Składa się on standardowo z pięciu magów:
- Tadeusz Baran, taktyk, dowódca. Raczej kompetentny, nic specjalnego, ale ma plecy, więc często dostaje dowództwo.
- Cezary Sito, katalista, puryfikator.
- Milena Diakon, katalistka, puryfikatorka, mająca już o dziwo doświadczenie bojowe
- Adam Pączek, terminus liniowy, lekarz, bez doświadczeń.
- Bolesław Derwisz, terminus liniowy, mentalista bez doświadczeń.

Andrea sprawdziła też, co sprawdzali członkowie skrzydła (jako ich koordynator miała dostęp do tego, czego żądali) Dowódca czytał raport i ogólne wiadomości o terenie, podobnie Milena Diakon, choć ona szukała nawet więcej.

Andrea martwiła się, jak to zrobić, żeby ten teren nie przyciągnął większego niż konieczne zainteresowania, ale zaczął jej się rysować pomysł.

Dostawszy się na miejsce akcji (brama w bezpiecznej odległości plus normalny transport), Andrea wysłała sygnał do skrzydła, dając im znać, że pojawił się ich koordynator.
Ku jej zdziwieniu, zamiast dowódcy, odezwała się Milena Diakon.
Powiedziała, że dowódca jest niedysponowany, i że ona chwilowo przejęła dowództwo.
Dziewczyna o przyczynach niedyspozycji nie chciała mówić, zapytana wprost powiedziała, że Baran zatruł się zupą grzybową... choć Andrea wyczuła, że nie powiedziała prawdy. W końcu Baran czytał raport, musiałby być idiotą, żeby się na to nabrać. Zresztą, Andrea wiedziała, że kucharza razem z zupą już nie ma w okolicy.
Przycisnęła więc Milenę i ta wyznała, że dowódca jest nieprzytomny na wskutek paradoksu własnego zaklęcia, ale niedługo powinien się obudzić.

Andrea zażądała od Mileny raportu jako od aktualnego dowódcy i uznała, że z zespołem może się spotkać wieczorem, jako że w chwili obecnej są w terenie.

Zgodnie z raportem skrzydło znalazło bezpieczną bazę w lokalnym hotelu (własność przybranego ojca Andrei), oczyszczanie terenu ze skażenia idzie dobrze i jedynym poważnym problemem jest fakt, że dowódca jest nieprzytomny na skutek paradoksu, który również skaził jej moc. Ale nie wezwali wsparcia uznając, że mimo wszystko są w stanie sobie poradzić. Teoretycznie powinni byli to zrobić w chwili, gdy padł dowódca, Andrei jednak bardzo to odpowiadało. 
Aha. Od czasu paradoksu dookoła utrzymuje się mgła. Całkowicie niemagiczna.

Dodatkowo, na terenie znajduje się "od wczoraj" niejaka Julia Weiner, ale choć skrzydło przyjęło do wiadomości jej obecność, to nie szukali z nią kontaktu.

Korzystając z faktu, że i tak nie mogła teraz rozmawiać z terminusami, Andrea skontaktowała się z Julią.
Po krótkiej rozmowie telefonicznej (przedstawienie się, umówienie się na punkt spotkania) Andrea ruszyła w kierunku Julii, kiedy nagle dostała od Jana Weinera przekaz hipernetem "To gorathaul! Błękitne ogniki!" razem z namiarem (Julia nie jest już podłączona, bo opuściła Świecę, ale wciąż ma kontakt z ojcem).

Andrea ruszyła biegiem w jej stronę, tworząc smakowite źródło energii (taka fałszywka, ale bardzo kusząca), które miało za zadanie zająć gorathaula podczas gdy Andrea z Julią się wycofają.
Szybka reakcja Andrei połączona z dobrze dobranym odwróceniem uwagi potwora zaimponowała Julii. 
Na sygnał alarmowy Andrei większość skrzydła (z wyjątkiem lekarza) pojawiła się poprzez portale. Portal Mileny był bardzo skażony, sama Milena również. Andrea natychmiast o to zapytała. Tadeusz Baran rzucił zaklęcie przez węzeł, który znaleźli (czyli wyciek z reaktora Lorda Jonatana) i miał paradoks. Milena próbowała zatrzymać ten czar i odpiąć go od węzła i udało jej się to, ale dużym kosztem. Cezary ją przebadał i poza głębokim skażeniem nic bardzo złego jej nie jest. Po prostu powinna ostrożniej czarować i nie powinno się jej zbytnio skanować, by nie doszło do infekcji. Bardzo unikała powiedzenia wszystkiego, bo boi się, że to będzie zaraportowane.

Gdy gorathaul nie stanowił już bezpośredniego zagrożenia i wszyscy byli w miarę bezpieczni, Andrea na osobności poprosiła Julię o przekazanie sugestii, która lepiej, żeby nie pojawiła się w żadnych systemach Świecy. 
Sugestią tą było, aby Jan Weiner zadbał, by uczynić z tego miejsca węzeł pod kontrolą rodu Weinerów. Powinno to być stosunkowo łatwe, gdyż Weinerowie już tutaj działali (rdzeń IR, stabilizacja mocy itd), wymaga tylko stworzenia pewnej ilości wstecznych wpisów w rejestrach. Kto jak kto, ale głowa rodu powinna być w stanie coś takiego osiągnąć...

Już w bazie (czyli hotelu) Andrea poprosiła Julię do prywatnej sekcji (ma klucze od papcia), gdzie mogła mieć w miarę rozsądną pewność, że pozostali nie będą jej szukać i przekonała Julię, żeby wystąpiła jako oficjalny konsultant (naukowiec i osoba, która już w tym terenie pracowała). Dopytała też o kilka faktów odnośnie Myślina i Lorda Jonatana - tak, w Myślinie jest ostatnio ciągła mgła. MOŻE pochodzićz Lorda Jonatana, ale LJ musiałby mieć jakieś możliwości działania na zewnątrz. A Renata przed utratą Karradraela nie zdążyła LJ uruchomić. Przed śmiercią Aurelii Maus LJ był całkowicie nieaktywny. Kto może wiedzieć o LJ? Nikt. Tylko Renata Maus, Julia i 4-5 osób nieistotnych dla sprawy. Seiras Jonatan Maus zadbał o to, by żaden Maus nie pamiętał o istnieniu tak zbudowanej fortecy.

Spytana o gorathaula, Julia stwierdziła że to wygląda na summon a nie efemerydę. Jeśli tak, to najpewniej pochodzi z LJ i BYĆ MOŻE faktycznie mgłę podtrzymuje LJ. Co gorsza, obecność różnych innych skażeńców sugeruje, że gorathaul może nie być jedynym potężnym skażeńcem w okolicy.

Następnie nadeszła pora na spotkanie ze skrzydłem. 
Andrea poprosiła Julię, by dała jej chwilę na osobności z terminusami, chwilę posłuchała pod drzwiami, próbując rozeznać się w dynamice zespołu, po czym cicho weszła. Kłócili się jeszcze przez krótką chwilę, ale szybko ją zauważyli.

Andrea przyjęła postawę starszego, bardziej doświadczonego dowódcy, który chce, aby jego podwładni nauczyli się jak najwięcej.

Od każdego z obecnych odebrała raport, poczynając od dowódcy - Tadeusz Baran już się obudził (kilka godzin wcześniej, zgodnie z przewidywaniami lekarza). Baran jednak jak się okazało, nie tylko nie był w stanie niczego powiedzieć, on nawet nie zebrał od pozostałych sprawozdania.

Cały zespół zjechała na równi za popełnione błędy, nawet u siedzącego cicho Sita coś znalazła. 
Następnie każdego za coś pochwaliła, choćby za drobiazg, ale tak samo uczciwie.

Od pierwszego kontaktu z tym skrzydłem Andrea miała poczucie, że coś jest nie tak.
Dowódca, który już na początku zostaje zdjęty z akcji. W dodatku po przebudzeniu zupełnie nie dba o to, co się z jego zespołem dzieje i nawet nie zebrał raportów.
Diakonka, która przejmuje dowodzenie niezgodnie z łańcuchem dowodzenia.
Sito, który siedzi cicho, ale potrafi uciszyć nadambitną Milenę jednym słowem.
Oraz Pączek i Derwisz, zapatrzeni w Milenę i potakujący jej we wszystkim.

Po rozmowie z zespołem Andrea oceniła, że Baran odda dowodzenie, jeśli uzna, że mu nic nie grozi. Milena pójdzie za Cezarym, ale niechętnie. Cezary posłucha Mileny a Adam i Bolesław pójdą z Mileną. Oddanie dowództwa jednemu z tej dwójki równa się oddaniu dowództwa Milenie...

Andrea skorzystała z możliwości, jakie dawała jej przynależność do wydziału wewnętrznego i sprawdziła komunikację oddziału i co kto sprawdzał.
Komunikacja w oddziale wydawała się dość normalna, jednak uwagę Andrei przykuły dwie rzeczy:
Po pierwsze, Milena Diakon sprawdzała informacje na temat gorathaula zanim jeszcze ten się ujawnił. Po drugie, Cezary Sito nie sprawdzał nic. 
Andrea przyjrzała się ponownie aktom Cezarego, tym razem dokładniej, i odkryła, że tak naprawdę nazywa się Maus. Od razu rozdzwoniły się w jej głowie dzwonki alarmowe. Jednak z zapisów wynikało, że Cezary zmienił nazwisko jak tylko Jonatan Maus stracił moc. Nie chciał mieć nic wspólnego z Mausami, unikał kontaktów, nawet zamiast rozwijać demonologię, poszedł w puryfikację...

Z kolei Milena Diakon najwyraźniej ma wrogów i to dość potężnych, choć nie wiadomo, jakich (z dostępnych w tym momencie akt to nie wynikało. Pomimo wystarczającego doświadczenia nigdy nie dostała dowództwa. Nawet na tą akcję zgłosiła się licząc, że w końcu się jej uda. Jednak jak tylko się pojawiła, natychmiast przydzielono do skrzydła Barana, dając mu dowodzenie...

Ciekawą informację znalazła również o Adamie Pączku. Jego bratu kiedyś bardzo pomógł wydział wewnętrzny.

Również i zespół dokonał własnego sprawdzenia na temat Andrei, niektórzy bardziej dokładnie niż inni.
Milena domyśliła się, że to Andrea jest autorką raportu z ostatniej akcji, który czytała (Andrea tego szczególnie nie kryła), zaś Bolesław dokopał się, że Andrea pracuje dla wydziału wewnętrznego. 
To nieco ochłodziło stosunki skrzydła z Andreą - w końcu w pobliżu policjanta każdy czuje się winny...

Andrea przepytała Milenę, dlaczego sprawdzała gorathaula i Cezarego, dlaczego nie sprawdzał nic.
Milena wybroniła się, że sprawdzała wszystko, co może być powiązane z taką mgłą i gorathaul był wyraźną możliwością. Natomiast Cezary stwierdził, że wziął na sobie mnóstwo rutynowych rzeczy, które musiały być zrobione i dlatego nie miał nic tak trudnego, by wymagało to dodatkowych sprawzdeń. 
Andrea przyjęła te wyjaśnienia, choć wciąż coś jej nie pasowało...

W normalnej sytuacji to Cezaremu Andrea oddałaby dowodzenie, głównie dlatego, że wyczuwała, że Milenie bardzo - aż do poziomu "za bardzo" - na tym zależy, jednak teraz, przy obecności Lorda Jonatana... nie miała wyboru. 

Gdy odebrała dowództwo od Barana i przekazała skrzydło Milenie, zaniepokoiła ją reakcja dziewczyny. Jej pierwsze dowodzenie, a nawet się nie ucieszyła... Zupełnie, jakby tego oczekiwała. Ale może nie powinna się dziwić - Milena sprawdziła, co mogła na temat wszystkich członków zespołu i wiedziała, że Sito to Maus.
Anrei nie pierwszy raz przeszła przez głowę myśl, że łatwo byłoby Milenie ukartować "wypadek" dowódcy, by to jej przypadło dowodzenie... Zwłaszcza z Mausem w zespole, nawet jeśli zakamuflowanym.

Jako, że Cezary i Milena w pewien sposób się krzyżowo kryli (jedyni kataliści, podobne zdanie itp) Andrea szybko sprawdziła - nie, Cezary i Milena po raz pierwszy spotkali się na tej akcji. Nie mogli współpracować wcześniej. Ale przeglądając ich przeszłe rozmowy hipernetowe wyszła jedna ciekawostka - to Cezary pierwszy odkrył Lorda Jonatana (wyciek interpretowany jako Węzeł). Dopiero potem poprosił Milenę, by na to spojrzała, bo on po podpięciu się do tego magicznie "nie zrozumiał". I dopiero potem doszło do tej akcji z Mileną i Baranem (co nieco potwierdzało paskudne podejrzenia Andrei odnośnie Mileny, skoro z Baranem Milena była sam na sam).

Tadeusz Baran stwierdził, że cokolwiek się nie dzieje, powinni wezwać wsparcie - nie ma kompetentnego dowódcy, jego trzeba ochraniać... w skrócie, on jako dowódca chce wezwać wsparcie. To się bardzo nie podoba Milenie, ale i Andrei. Po dłuższej dyskusji Andrea kontaktuje się z tien Weinerem i prosi go o dyskretne załatwienie wycofania Tadeusza Barana z obszaru narażonego. Powód prosty - im więcej magów, tym większa szansa na odkrycie Lorda Jonatana. Jak tylko Baran się dowiedział, że jemu nic nie grozi i nie pójdzie po jego karierze jeśli będzie jakiś problem, zgodził się na wycofanie i oddał dowództwo.

Późnym wieczorem Baran został wycofany przez Weinera. Zapytana o plan Milena stwierdziła, że gorathaul potrzebuje mgły do życia. Nie ma mgły -> nie ma gorathaula. Jej plan jest taki, by skupić się na odkażaniu okolicy i na zniszczeniu mgły, dzięki czemu gorathaul będzie bardzo osłabiony i będzie podatny na atak. Brzmi dobrze... jeśli FAKTYCZNIE gorathaul nie jest wezwany (jeśli mgła jest częściowo magiczna, gorathaul nie jest głodny. Jeśli nie jest głodny, nie atakuje ludzi...).

To co wzbudziło uwagę Andrei - dochodziło do komunikacji hipernetowej między Adamem i Mileną oraz Bolesławem i Mileną. Tak jakby coś się miało w nocy wydarzyć (a Milena miałaby nie spać). Nieco dziwne, Andrea zanotowała sobie by na to spojrzeć.

Milena rozstawiła warty i poszli spać. To znaczy, próbowali pójść spać - nie do końca im wszystkim to wychodziło. W końcu Julia powiedziała Andrei co się stało - wykryła bardzo potężną efemerydę, gdzieś nad miastem, obejmującą z pół Myślina... efemerydę z mgieł. Problem polegał na tym, że zgodnie z wiedzą Julii (potwierdzone przez Andreę) złożoność i moc tej efemerydy zdecydowanie przekracza to co tak niskie promieniowanie pola magicznego (Skażenie) może utrzymać.

Jakby było mało problemów, efemeryda uniemożliwia zaśnięcie wszystkich w mieście i ich trochę drenuje. SUPER. Szczęśliwie Andrea jest analitykiem magicznym a Julia jest wybitnym ekspertem i naukowcem (oraz katalistką). Julia zaproponowała rozmontowanie efemerydy od środka - pobieżne przebadanie jej z zewnątrz i wejście w efemerydę używając magicznego połączenia z Andreą. Po niemałym wahaniu, Andrea się zgodziła - ale pozostali mają pilnować Andreę i Julię w czasie w którym te będą rozmontowywały efemerydę.

NIE, nikt nie docenił "obudzenia" przez Andreę, no bo "już prawie zasnęli". Andrea w skrócie wyjaśniła naturę przeciwnika i plan - ona z Julią rozmontują efemerydę, Cezary zapewnia że nic się nie rozprzestrzeni poza efemerydę gdy ta będzie się rozpadać, Milena zajmie się perymetrem zewnętrznym (wszyscy boją się Mileny czarującej w takim stopniu Skażenia). 

Julia i Andrea weszły w formę energetyczną i dały się wchłonąć efemerydzie. Pierwsza myśl - to jest nieprawdopodobnie złożona efemeryda, bardziej złożona niż Julia kiedykolwiek widziała. Jednak Julia # Andrea podłączyły się do efemerydy i dały radę ją "shackować". Jako efekt uboczny znalazły się tak głęboko w efemerydzie, że nie są w stanie się same wydostać, chyba, że przez rozerwanie połączenia.

Zgodnie z analizami Julii, ta efemeryda pochodzi z Lorda Jonatana. To zła wiadomość. Gorsza jest taka, że efemeryda najpewniej jest systemem zasilającym Lorda Jonatana - zbiera energię z ludzi i zasila reaktory. To jednak co dzięki temu jest możliwe to jest wejście do samych systemów Lorda Jonatana. Wszystko wskazuje na to, że ekrany ochronne LJ jeszcze nie są czynne. Czyli forteca ma słaby punkt - da się wślizgnąć przez efemerydę żywieniową, ale to droga w jedną stronę i to dość niebezpieczna.

Oczywiście, zrobiły to.

Weszły do systemów LJ. Tam od razu zorientowały się, że jednak całość mgły jest kontrolowana przez LJ. Jednak nadal nie wyjaśnia to, czemu LJ jest w ogóle w stanie funkcjonować. Powinien być nieaktywny. Dziewczyny przeprowadziły dokładniejsze badania i wykryły ślad Karradraela.

Okazało się, że Aurelia faktycznie zniszczyła Karradraela swoim kosztem, lecz część woli Karradraela i pomniejszych jego mocy przetrwała i opętały Lorda Jonatana. LJ powrócił w stan uśpienia, lecz Kwiatuszek (też Maus) czerpiąc z niego energię umożliwiła Karradraelowi przebudzić podstawowe, awaryjne systemy LJ. Forteca jest w stanie funkcjonować na niewielkiej mocy i do tego z bardzo ograniczonymi systemami, lecz Karradrael jest doskonałym taktykiem i coś takiego w jego ręku to niemało.

Napotkawszy Karradraela, Julia dostała całkowitego załamania nerwowego. Szok połączony z panicznym strachem ją wprowadził w katatonię. Andrea się nie patyczkowała, instynkt wydziału wewnętrznego SŚ się włączył - nie dała rady jej obudzić mentalnym plaskaczem? Przekierowała jej moc na siebie (w świecie realnym objawiło się to Skażeniem jej kanałów) i wdarła się do pamięci i umiejętności Julii by zachować formę gestalta i móc dalej współpracować i zdobyć jak najwięcej od Lorda Jonatana. Nie było to dla Julii miłe, choć Andrea STARAŁA się być delikatna. Ale efektywność > delikatność.

Skanowanie systemów Karradraela z samym demonem opętującym systemy było nieco jak zabawa w kotka i myszkę, ale zaowocowało ciekawymi informacjami:
- gorathaul jest faktycznie elementem planu Karradraela. Chwilowo ma być dywersją i być w trybie pasywnym. Karradrael na coś czeka i wtedy uruchomi gorathaula jako dywersję.
- LJ generuje mgłę wraz z poszczególnymi obszarami Skażenia.
- w okolicy jest upoważniony Maus. Nie jest to Lord Maus, ale Karradrael jest w stanie częściowo oszukiwać systemy LJ. Jeśli Maus podłączony do LJ oddali się na 5 km od LJ, forteca przejdzie znowu w stan pasywny, jakby nie było tu Kwiatuszka.
- Karradrael skutecznie naprawił częściowo wyciek, efektywnie zmieniając węzeł w węzeł bezpieczny dla Weinerów (:>).
- LJ jest podłączony do 1.75 Mausa (Andrea: eeee... co?)
- Celem Karradraela jest zdobycie Renaty Maus. Jako, że plan jest niemożliwy, "plan B" - ale tego już nie odważyła się pogłębiać.
- LJ ma jeszcze jednego viciniusa na powierzchni - Otępiacza, który działa jedynie w mgłach, ale "mind fog" właściwie odbiera koncentrację i możliwości działania poddanych jemu ludzi i magów. Jest to istota półmaterialna, ale poza mgłami nie potrafi istnieć.

Zdobycie powyższych informacji było bardzo wyniszczające dla Julii, bo Andrea przekierowywała wszystkie negatywne efekty na Julię konsekwentnie uważając, że ona musi być aktywna do końca.

Dodatkowo Andrea dowiedziała się jeszcze dwóch bardzo istotnych rzeczy:
- Lustrzaki. LJ ma możliwość "konstruowania" demonów-zabójców poruszających się przez lustra. Chwilowo LJ ma do dyspozycji cztery Lustrzaki, które są w "standby" i czekają na rozkazy i namiar.
- W aktualnej chwili LJ konstruuje z pary młodych ludzi Skażeńca. Po co, tylko jeden Karradrael wie. Wygląda to jak akt bezsensownego okrucieństwa, lecz przy K. okrucieństwo jest zwykle celowe.

Andrea chciała jeszcze... ale nie zdążyła. Jej ciało straciło przytomność. Julii też.

Andrea otworzyła oczy w pokoju hotelowym. Efemeryda została rozproszona a ona miała przyjemność nasłuchać się wielkiej kłótni Mileny z Adamem (lekarzem). Milena skrzyczała Adama za podanie dziewczynom leków usypiających - takie rozerwanie mogło uszkodzić Julię i Andreę na dobre. Adam się odgryzł, że pomysł Mileny by z Cezarym odciąć dziewczyny od źródła jest jeszcze gorszy (i by był; skaziłbym i zniewoliłbym obie mocą LJ :P).

Aha, Milena jest w gorszym stanie niż wcześniej; najpewniej to kwestia braku snu i ciągłej potrzeby czarowania (w rzeczywistości: kolejna porcja krwi Mausa; Milena unika czarowania jak może, bo krew Mausa zakłóca jej równowagę magiczną).

Julia jest w bardzo złym stanie, Andrea w lepszym. Dowód: obudziła się :P. Aha, nie wspomniałem: przed nocą, przed manifestacją efemerydy Andrea wzięła środki (niemagiczne) by nie spać w nocy. To też dodaje do jej ogólnego zrujnowania organizmu. Nie, żeby ogólnie było idealnie - przez tą efemerydę całe skrzydło nie spało w nocy i wszyscy są zmęczeni, podirytowani i dużo wolniejsi.

Co Karradraelowi zupełnie nie przeszkadza.

Jako, że Andrea miała bardzo dobry pomysł "czas by wszyscy się wyspali", Karradrael uwolnił Skażeńca. Milena dała rozkaz - pojmać Skażeńca (tego sformowanego z ludzi), by można było go rozmontować i mu pomóc. Nie zabijać. Uzyskała uniesienie brwi w aprobacie ze strony Andrei, ale jednocześnie kazała Adamowi (lekarzowi) zostać z Julią i Andreą. Ta trójka powinna bez problemu poradzić sobie z pojmaniem Skażeńca.

Powód jest prosty - Karradrael poznał aurę Julii w systemach LJ i zobaczył nieprzytomną Julię tutaj. Nowy cel Karradraela - przechwycić i zdominować Julię Weiner. Skazić ją krwią Mausów by móc ukryć sekret.

Andrea powiedziała Adamowi, że ona jest na środkach i kazała mu pójść spać. Przy okazji sprawdziła swój stan - jest niezdolna do czarowania, kanały są nieaktywne, nie działa hipernet. W skrócie: super, jest bezużyteczna. Ale na warcie może stać. Więc Adam, jako jedyny mag skrzydła, mógł zażyć trochę snu.

Skrzydło wróciło, ze Skażeńcem. Nie była to bardzo trudna operacja, ale zajęła trochę czasu. Pozwoliło to Karradraelowi sformować plan ataku. Gdy tylko Andrea (wspierana przez Milenę dla niepoznaki) zaproponowała odpoczynek, Bolesław Derwisz zaraportował, że gorathaul się ruszył w kierunku miasta. Niestety, trzeba się nim zająć teraz.

Adam na ochotnika zgłosił się do odkażania Skażeńca. Milena stwierdziła, że pomoże mu puryfikacyjnie. Andrea ostro zaprotestowała - życie skażonych ludzi jest więcej warte a Milena nie powinna czarować. Wtedy na ochotnika zgłosił się Cezary - on nie będzie leczył Skażeńca, on zajmie się upewnieniem że nic złego się nie stanie i drobną puryfikacją. Plus, ktoś musi zostać z Julią. Wszyscy się na to zgodzili. Czyli Milena, Adam, Bolesław i Andrea to grupa uderzeniowa do zabicia gorathaula... co bardzo Karradraelowi pasuje (Cezary może podać krew Julii i Karradrael będzie w stanie się z nią połączyć, nawet jeśli nie permanentnie to przynajmniej po to, by ją uciszyć).

Andrea się bardzo zdziwiła - zgodnie z zebranymi danymi z LJ gorathaul powinien być pasywny. Coś się bardzo zmieniło, ale co? Adam Pączek zaproponował Andrei (bez konsultacji z Mileną, co ją rozsierdziło) dawkę Skażenia, które uruchomi jej kanały magiczne. Andrea się bardzo ucieszyła - będzie w stanie czarować, choć hipernet i tak jej nie zadziała i konsekwencje z tego będą bardzo bolesne. 

Andrea, po otrzymaniu Baterii Skażenia szybko zaproponowała relatywnie prosty plan - wymontowała z samochodu akumulator, dała go Derwiszowi (najsilniejszy z magów) i powiedziała jak porazić nim gorathaula. Sama sformowała smakowite źródło energii które dała Milenie do "trzymania" katalitycznie, by mogła odpalić to gdy pułapka będzie gotowa. No i przygotowała kilka zwykłych, niemagicznych rzeczy jak np. puszka z gazem pieprzowym do krzywdzenia żółwiaka mgieł.

Plan prawie się udał. Andrea miała wrażenie, że coś jest bardzo nie w porządku więc chciała się oddalić od skrzydła (by zrobić skan okolicy), ale doszło do sytuacji niespodziewanej. Cezary wstrzyknął swoją krew Julii i połączył ją z Karradraelem, ALE Karradrael nie jest nawet w jednej setnej tak silny jak przed swoim zniszczeniem przez Aurelię Maus. Łącząc się z Julią Karradrael utracił hold na Milenie. Skażona krew Mausów zareagowała w ciele czarodziejki i jej własna magia uderzyła w nią samą. Wyglądało to tak, jakby jej żyły rozbłysły oraz jej próg Skażenia przekroczył wszystkie możliwe poziomy. Doszliśmy do poziomu permanentnego Skażenia. Milena może sama formować efemerydy i Skażeńce.

Dla Andrei wyglądało to jak choroba autoimmunologiczna na poziomie magicznym. Czyli: dziwnie. Poczuła też delikatny smak magii krwi (nic dziwnego, to właśnie robił Karradrael). Niestety, Milena upuściła zaklęcie i sformował się smakowity kąsek dla gorathaula. Karradrael stanął przed trudną decyzją - co robić dalej. Niechętnie, ale wysłał tam gorathaula a sam skupił się na skażaniu Julii. Milena, po raz pierwszy od Dotknięcia Karradraela będąc choć trochę sobą, zebrała energię i uderzyła puryfikacją w siebie w żałosnej próbie pozbycia się krwi Mausów... oczywiście, nic to nie dało poza większym Skażeniem.

Gorathaul pojawił się na smakołyku przygotowanym przez Andreę. Karradrael, w pełni skupiony na Kontakcie z Julią Weiner nie zdołał połączyć się z żółwiakiem mgieł i ten wpadł w śliczną pułapkę. Porażony akumulatorem, dwoma czy trzema strzałami magicznymi został bardzo ciężko ranny. Gorathaul zadziałał na instynktach i się przeniósł gdzieś indziej. Karradrael wymusił na nim przeniesienie się do miasta i przesłał informację Milenie gdzie jest gorathaul. Jednocześnie odzyskał częściowe połączenie z Mileną, oddając hold z Julii i zostawiając Cezarego, by ten ją puryfikował, by nie było widać efektów działania Karradraela.

Milena poinformowała przez krwawy kaszel że poczuła gorathaula w mieście. Andrea kazała jej się wycofać. Chciała posłać z nią Derwisza lub Pączka, ale Milena zaprotestowała - Cezary ją odbierze. Andrea się zgodziła. Skrzydło w liczbie 3 osób (Derwisz, Pączek, Andrea) musiało działać BARDZO szybko, bo inaczej gorathaul zacznie zabijać... Niechętnie, ale Andrea pozwoliła Derwiszowi ich teleportować do miasta (do jednego z miejsc gdzie Derwisz wcześniej zrobił namiar). Karradrael próbował przechwycić portal i przesłać ich pod ziemię, w litą skałę, ale te systemy LJ nie działały. 

Podczas teleportacji Derwisz i Pączek powiedzieli Andrei o swoich erotycznych doświadczeniach z Mileną. A dokładniej: ona miała coś z tymi żyłami już wcześniej. Znowu się nabawiła czegoś podczas gdy Andrea i Julia były zajęte efemerydą...

Cezary jednak powiedział, że wszystko w porządku po jego stronie; jest on, Milena, Julia i Skażeniec. Nic im nie grozi. A dokładniej: Karradrael przesłał Cezaremu dwa Lustrzaki do obrony. Milena w rękach Cezarego była całkowicie dla Karradraela bezużyteczna, więc skupił się na Julii Weiner...

Tymczasem Adam, Bolesław i Andrea mieli plan. Jakkolwiek gorathaul był ciężko ranny, oni też nie byli w najlepszej formie, więc postanowili zastawić pułapkę z której gorathaul już nie wyjdzie. Andrea kupowała czas formując małe, lecz smakowite źródła energii by odwracać uwagę żółwiaka mgieł od ludzi a Adam i Bolesław przygotowywali pułapkę na gorathaula. Pułapka była w miarę prosta - zwykła, chamska bomba (zrobiona przez kogoś kto współpracuje z "ojcem" Andrei z akumulatorów) i rzucone na nią zaklęcie by NIE wybuchła. Gorathaul to zje -> rozsadzi go od środka. No i walenie prądem, bo prąd jest skuteczny.

Uruchomili pułapkę. Gorathaul zmaterializował się bezpośrednio za Bolesławem Derwiszem i jednym ugryzieniem w okolice kręgosłupa prawie go zabił. Walka z 3 vs 1 zmieniła się w 2 vs 1. Szczęśliwie, pułapka Andrei była BARDZO smakowita i instynkt gorathaula przekroczył wolę Karradraela. Zwłaszcza, że Karradrael wciąż skupiał się w pełni na Julii.

Ataki elektryczne jeszcze bardziej osłabiły gorathaula, lecz ten dał jeszcze radę wyemitować stożek zimna. Prosto w lekarza (Adama Pączka). Andrea zasłoniła go, padając w bardzo ciężkim stanie na ziemię skostniała i ledwo przytomna. W tym momencie bomba wybuchła i gorathaul został zniszczony.

Adam podszedł by wyleczyć Bolesława. Zaczął rzucać zaklęcie, lecz zamarł, uśmiechnięty. Karradrael przesunął tu otępiającego viciniusa. Andrea, w absolutnej desperacji rzuciła czar Paradoksalny - rozwiała z tego miejsca mgłę, ale wyemitowała falę Skażenia i zrobiła "coś" ludziom w okolicy. Tym chwilowo jednak się nie martwiła - Otępiacz nie jest w stanie funkcjonować poza mgłami, więc się musiał wycofać.

Tymczasem agonię Julii wyczuł inny vicinius, pilnujący okolicy LJ...

Andrea straciła przytomność rzucając ten czar. Gdy się obudziła, Adam dał radę podleczyć Derwisza (do stanu Skażonego, lecz nie trwale). Andrea była już w takim stanie, że do niczego się nie nadawała. W najlepszej formie był lekarz. O dziwo, mgła zaczęła opadać.

Wrócili do hotelu. Bolesław zdominował jakiegoś człowieka, by ten ich zawiózł. A w hotelu - niespodzianka. Cezary nie żyje, zagryziony przez coś. Nieprzytomna Milena z ustami uwalonymi krwią. Zniszczone dwa Lustrzaki (wygląda jak dwa rozbite lustra bez ramek). I nieprzytomna Julia z brązowymi żyłami (krew Mausów, reakcja autoimmunologiczna).

Co się stało? Cierń tu był. Koci vicinius strzegący terenu. Wpadł, zabił Cezarego i wrobił w to Milenę. Jako, że LJ nie ma Mausa, systemy zaczęły się wyłączać i na to nic już Karradrael nie mógł poradzić...

Julia jest ciężko Skażona i Dotknął jej Karradrael, ale jej organizm to zwalczy.
Milena ma zmiany nieodwracalne, ale wróciła do bycia sobą. Gorzej z jej reputacją i samopoczuciem.

Udało się sprawę wyciszyć. I Weinerowie mają nowy Węzeł. Taki, gdzie NIGDY nie może zbliżyć się żaden Maus...

# Streszczenie

Małe ad-hoc skrzydło terminusów dowodzone przez (otrutego) Tadeusza Barana ma oczyścić teren. Na terenie pojawił się gorathaul i ci magowie próbują się go pozbyć. Niestety, okazało się, że "Lord Jonatan" jest sprawny i próbuje przejąć kontrolę nad terenem z woli Karradraela. Przechwycił katalistę Cezarego i Skaził ciężko Milenę Diakon. Julia i Andrea wniknęły w na pół nieaktywnego aderialitha i odkryły, że Karradrael dąży do odzyskania Renaty Maus i do "planu B" (nie wie jak odzyskać Renatę). "Hackowanie" Lorda Jonatana Julia okupiła komą; gdy Karradrael skupił ogień na niej, Cierń zniszczył jego agenta a Andrea powstrzymała gorathaula. Brak Mausów zmusił aderialitha do pełni uśpienia. Do Myślina nie może nigdy zbliżyć się żaden Maus (bo jest tu na wpół przebudzony Karradrael) - Weinerowie przejęli teren w opiekę (Jan Weiner). 

# Zasługi

* mag: Andrea Wilgacz jako ta, która zatrzymała Aderialitha sprzężonego z cieniem Karradraela.
* mag: Cezary Sito jako Maus-puryfikator, który uległ potędze Karradraela.
* mag: Milena Diakon jako katalistka, która prawie stała się Mausem pod kontrolą Karradraela. A i tak skończyła Skażona.
* mag: Tadeusz Baran jako przywodca skrzydła karnego terminusów który wykazał się lenistwem i padł do sił Karradraela.
* mag: Bolesław Derwisz jako ludzista i mentalista który od zawsze jest pełny niechęci do Diakonów.
* mag: Adam Pączek jako medyk podatny na podszepty innych magów. Zwłaszcza ładnych Milen Diakon.
* vic: Karradrael jako "Lord Jonatan", aderialith typu forteca sprzężona z Karradraelem.
* vic: Cierń jako kot zabijający to co stoi mu na drodze.
* mag: Jan Weiner jako narzędzie ratunkowej komunikacji Julii z Andreą.
* mag: Julia Weiner jako ta, która shackowała aderialitha. I zaraz wpadła w panikę.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Myśliński
                1. Myślin
                    1. Centrum
                        1. Hotel "Luksus"
                    1. Okolice
                        1. Bieda-szyb (nielegalne wysypisko śmieci)
                            1. Grota z Rdzeniem Interradiacyjnym
                            1. Lokalizacja "Lorda Jonatana"

# Notatki MG przed sesją:

Cezary Sito (Maus) - katalista i puryfikator
- był spokojnym Mausem puryfikatorem
- trzymał się z dala od polityki
- panicznie bał się Jonatana i Renaty
- teraz: przyszły lord Maus

Milena Diakon - katalistka, puryfikator
- nigdy nie dowodziła; nikt jej nigdy nie zaufał
- arogancka i bardzo ambitna
- ma zmysł taktyczny
- ma potężnych wrogów
- rozwiązła jak Diakon, ale nigdy na akcji

Adam Pączek - medyk > terminus
- wydział wewnętrzny uratował kiedyś jego brata
- lepszy lekarz niż terminus
- mało asertywny, chyba, że w kwestii leczenia
- nie ufa katalistom i energiom magicznym

Bolesław Derwisz - terminus > mentalista
- ludzista
- zobaczył człowieka w Milenie i jest skonfliktowany (normalnie nie lubi Diakonów za ludzi)
- widzi zmiany w Milenie. Widzi, że coś jest nie tak.
- podczas seksu widział żyły Mileny (krew Mausów)


"Lord Jonatan" skorumpował Cezarego mocą Karradraela.
Cezary poprosił Milenę o pomoc w dotarciu do węzła i w ten sposób ją zdominował.
Milena uczestniczyła w rytuale który wypalił Tadeusza Barana. Po wszystkim, Milena dostała Krew Mausa.

Cel "Lorda Jonatana": Aktualizacja wiedzy o świecie (done). Zdobycie wiedzy o Renacie Maus (done). Przekształcenie Diakonki w Mausa by sprawdzić, czy się da. Sformowanie nowego Lorda Mausa. 

Cel Cezarego: zostanie nowym Lordem Mausem, w miejsce Renaty. Nie ma Karradraela, jest Lord Jonatan.

Dark Future: Dojdzie do korupcji wszystkich obecnych tu terminusów. Cezary i Milena odłowią grupę magów SŚ (kilkunastu im trzeba) i z ich pomocą stworzą zaczątek Karradraela i wcielą kontrolera w Cezarego. Cezary staje się nowym Lordem Maus i ma stały link z Lordem Jonatanem.