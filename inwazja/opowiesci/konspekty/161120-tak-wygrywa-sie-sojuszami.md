---
layout: inwazja-konspekt
title:  "Tak wygrywa się sojuszami"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160707 - Mała, szara myszka... (KB, DK, HB)](160707-mala-szara-myszka.html)

### Chronologiczna

* [161113 - Świeca nie zostawia swoich (AW)](161113-swieca-nie-zostawia-swoich.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Andrea zdecydowała się na próbę skomunikowania się z tien Anetą Rainer. Udało jej się, choć połączenie hipernetowe jest słabe.
Aneta powiedziała spokojnie Andrei, że nie może dać jej informacji jak złożyć własne połączenia hipernetowe. To zbyt tajne i delikatne a Andrea może być Spustoszona. Plus, Andrea nie ma narzędzi. Ale - jest niedaleko stary, przedZaćmieniowy emiter hipernetowy; nie jest w pełni kompatybilny z nowymi standardami, ale Aneta może przepiąć swój. A hipernet działać będzie lepiej. 
Andrea się zgodziła. Dostała lokalizację starego emitera; znajduje się w Kotach, jest u mechanika samochodowego. Potrzebny jest dobry katalista ze Świecy by go uruchomić. Emiter znajduje się na pace ciężarówki (zepsutej). Ten emiter jest duży.

Zdaniem Andrei, jest to zadanie idealne dla Juliana Pszczelaka i jego skrzydła szkoleniowego (do osłony). Jako, że ten emiter wymaga paliwa, muszą to zdobyć i przyprowadzić.
Julian obiecał, że Andrea dostanie tą ciężarówkę. Andrea zostawiła jemu decyzję co komu powiedzieć.

Lidia i Rafael pracują nad Rudolfem Jankowskim. Biedak dostał tym samym co Andrea, ale nie miał defensywnych systemów, więc składają go do kupy. Zdaniem Lidii, regeneracja zajmie tydzień. Widząc zdziwioną Andreę, zauważyła, że musi odbudować większość połączeń w jego głowie. Rafael powiedział, że jest sposób; jak Andrea pewnie zauważyła, Rafael jest nie tyle lekarzem co 'aggressive augmenter'. Może wymusić na Rudolfie takie wzmocnienie, ale wtedy są potencjalne... problemy. Niekoniecznie permanentne.
Na razie, Andrea nie chce iść w eksperymenty Rafaela.

Mieszko poprosił do siebie dyskretnie Andreę. Pokazał zbierającego się Tadeusza Barana, który powiedział, że nic mu nie jest. Mieszko zaraportował Andrei, że Tadeusz zachowywał się, jakby dostał udaru, ale mu przeszło... w czym, utyka - a nie utykał. Andrea od razu skojarzyła to z tym, co Rafael zrobił Baranowi by zmienić go w kompetentnego maga...
Andrea powiedziała Mieszkowi, że weźmie Tadeusza do Rafaela. Poszła i poprosiła na stronę Rafaela i Barana.

Rafael usłyszał o symptomach i facepalmował. Dał Baranowi tabletki (które nieufnie wziął). Rafael zapewnił go, że będzie dobrze. Baran nie wierzy, ale poszedł sobie.
Rafael wyjaśnił Andrei o co chodzi. To efekty uboczne. On sprzęgł wzmocnienie i spalanie organizmu z uczuciami dla Andrei. Ale założył naiwnie, że pojawi się jakiś seks. Nie ma nic. Baran w celibacie. Efekty uboczne strzeliły dużo szybciej niż powinny.

Andrea powiedziała Diakonowi, że chce powiedzieć Mieszkowi. Rafael się zgodził - pod warunkiem, że JEŚLI cokolwiek się mu stanie (Rafaelowi), łącznie z uderzeniem, to on opuszcza tą grupę. Andrei się to NIE podoba, a w zupełności nie podoba jej się postawa Rafaela. Ale niewiele może na to poradzić.

Andrea jest przeciążona tematami organizacyjnymi. Trzeba rozbudować bazę. Trzeba wzmocnić hipernet. Trzeba przygotować się na potencjalny atak. Trzeba postawić Jankowskiego. A wszystko zaczyna się rozłazić. Jeszcze utrzymuje się efekt ucieczki i spięcia, jeszcze wszyscy Andrei słuchają, jeszcze są w tle ich zwycięstwa... ale już "zaraz" Andrea podejrzewa kłopoty. Nie atakując, przegrywają. Atakując, przegrywają.
Andrea ma nadzieję, że nowy emiter pomoże jej w kontakcie z Agrestem.

Andrea poszła do Andżeliki i Mariana. Andżelika powiedziała, że hydroponika nie działa. Nie udało się jej przeszczepić. Rafael może odbudować hydroponikę, ale potrzebuje źródła quarków. Jest opcja by zrobić to bez źródła quarków, ale wtedy Rafael musi użyć jakiegoś innego subsystemu, np. ludzi/zwierzęta jako źródło hodowania. Tak czy inaczej, Marian powiedział, że spróbuje spojrzeć na Opla Astra i znaleźć coś, co może pomóc w znalezieniu przydatnego źródła energii.

Kajetan, Mieszko i Tadeusz - Andrea kazała im zacząć rozbudowywać defensywy i przygotować się do obrony. Kajetana też odesłała do pracy z Rafaelem i Andżeliką by móc maksymalnie wykorzystać sensowną hydroponikę. Coś, co niekoniecznie będzie smaczne, ale na pewno będzie się do czegoś nadawać by nie pomarli z głodu.

Plan Andrei jest prosty - niech WSZYSCY są zajęci i nie ma wtedy czasu na myślenie i martwienie się. I udało jej się - póki są zajęci, póty słuchają poleceń i nic dziwnego się nie będzie działo. Andrea ma kohezję zespołu, choć po kosztach.

Czas spotkać się z Kręgiem Życia. Marian Andrei powiedział, że on rozmawiał z Mówcą - magiem, który mówi z innymi magami. A przynajmniej tak zrozumiał. Podejście izolacjonistyczne.

Andrea udała się do Domostwa Krawędzi. Marian przedstawił Andreę i Franciszka Myszeczkę.

"Lady terminus, chciałaś się widzieć z kimś istotnym w hierarchii, więc sprowadziłem Lilaka" - Franciszek
[brwi ostro w górę] - Andrea

Franciszek wyjaśnił Andrei, że on jest Mówcą komunikującym magów z viciniusami a Lilak jest Mówcą komunikującym viciniusów z magami. On jest w hierarchii magów, Lilak jest w hierarchii viciniusów.

"Czyli jeśli dobrze rozumiem, decyzje podejmujecie wspólnie?" - lekko zagubiona Andrea
"Też nie tak, lady terminus. Powstają zespoły zadaniowe rozwiązujące ten problem." - spokojnie tłumaczący Franciszek Myszeczka
"A jeśli coś dotyczy wszystkich? Na przykład, wspólne zagrożenie?" - Andrea, próbująca - Andrea, analizując sytuację aktualną
"Zespół może składać się ze wszystkich członków Kręgu; z uwagi na różne podejścia nie da się zagwarantować jednomyślności" - lekko zrezygnowany Franciszek

Czyli jeden sojusz z nimi wszystkimi jest tak naprawdę niemożliwy z uwagi na samą strukturę. Andrea przedstawiła się jako Lady Terminus by pokazać swoje możliwości. Gdy zapytała Franciszka o to, co ten wie o aktualnej sytuacji, Franciszek odpowiedział, że patrole krwawych magów krążą po okolicy a Świeca utraciła kontrolę nad sytuacją. Powiedział, że więcej jej nie powie bo nie ma po co.
Nie stracili nikogo do tych patroli. Franciszek dodał, że druga strona wykorzystuje viciniusy, ale w inny sposób. Np. istoty działające węchem, smakiem, taumatyczne. Ale są kontrolowane, więc nie mają naturalnej inteligencji jaką posiadają viciniusy wolne.

Andrea powiedziała, że Świeca nie ma pełnej kontroli nad otoczeniem i nie może zapewniać bezpieczeństwa. Franciszek zauważył, że Świeca zapewnia bezpieczeństwo - magom. A zwłaszcza magom Świecy.
Koniec końców, negocjacje doszły do poziomu, w którym Franciszek powiedział, że są skłonni Andrei pomóc - jeśli Wtorek Śląski będzie pod kontrolą Kręgu a nie Świecy. Jeśli prawa Kręgu będą tu dominujące. Bo wtedy Franciszek jest w stanie przekonać inne viciniusy, że warto wspierać Świecę nawet po tym wszystkim, co Świeca robi.

Andrea się zgadza pod dwoma warunkami: wspierają Andreę jak długo ona jest Lady Terminus (albo aż miną trzy miesiące i to wszystko się skończy) ORAZ ta placówka co Andrea ją teraz zakłada zostaje na ich terenie jako placówka ambasadorska. Franciszek się na to zgodził. Dodatkowo, Andrea powiedziała, że podzielą się źródłami energii magicznej na tym terenie - te potrzebne do istnienia bazy zostaną dla bazy, pozostałe mogą przejść pod kontrolę Kręgu (bo viciniusy potrzebują magii do życia).
Franciszek się na to zgodził. Powiedział, że to przedyskutują w Kręgu, ale najpewniej się uda.

Franciszek też obiecał, że następnego dnia Lilak przyniesie Andrei mapę ze źródłami magicznymi. Miejscami możliwymi przez Andreę do wykorzystania do jej celów.

Dzień 2:

Julian ze skrzydłem wrócili. Niestety, bez ciężarówki. Problem - mechanik samochodowy jest "zaprogramowany". On nie odda tej ciężarówki a jest ona zbyt uszkodzona, by móc ją ukraść. Z użyciem magii można - ale Julian nie ma technomantów oraz obawia się ściągnąć Spustoszenie. Nie ma środków, więc wrócił. Hipernet nie działa, woli być ostrożniejszy niż podjąć złą decyzję. Mechanik nie jest zainteresowany Julianem; Skrzydło nie wzbudziło niczyjego zainteresowania, udało im się też uniknąć wszystkich patroli.

Rafaelowi, Andżelice i Kajetanowi udało się sensownie sformować w miarę działającą hydroponikę. Nie umrą z głodu a nawet jest opcja (to się nazywa: wypadek -_-) generowania całkiem paskudnej żywności która - przypadkowo - smakuje niektórym viciniusom. Coś na potencjalny handel.

Mieszko powiedział Andrei, że już wie, czemu w tym zamku się nie zainstalowała Świeca. Jest fatalnie trudny do obrony. Instalowanie zabezpieczeń nie wychodziło więc Mieszko pogadał z Kajetanem a POTEM z Julianem i wyszło im, że ten zamek ma pryzmat "nigdy niczego nie obronił". Ma to zaletę. Nikt nie będzie spodziewał się, że Świeca się tu ukryje.
Andrea stwierdziła, że potrzebuje tu kogoś, by wciągnął PRZECIWNIKÓW w pułapkę. Tu nikt się nie obroni. Więc przeciwnik będzie atakowany jak będzie w tym zamku...
Z dobrych wieści - Kajetan powiedział, że Pryzmat można zmienić. Wystarczy budować legendy i opowieści. Od ręki nie zmienią, docelowo... czemu nie?

Lidia zaraportowała, że Rudolf jest w nie najlepszym stanie. Prędzej czy później przydałby się lekarz nie będący Rafaelem. Rafael usłużnie zaproponował użycie jego eksperymentów. Andrea spojrzała na niego z wyrzutem...

Przybiegł Świeży Lilak. Wpuszczony został przez Mieszka na zamek. Lilak dał obiecane mapy Andrei - bardzo fajne mapy. Wtem, przyleciał... Deiiw. Lilak nie pozwala mu lądować, ale Deiiw chce handlować. Chce negocjować. Ma coś, co chce dać. Lilak ostrzegł, że mają sojusz. Deiiw stwierdził, że jest w innym zespole roboczym.
Lilak rzucił się, złapał i przydeptał Deiiwa. Przychapnął. 
Deiiw się przyznał, że znalazł czarodziejkę w Skażonym Węźle (na mapie oznaczony jako 'paranoia, rage inducing'). Porwał ją i zamknął. Uratował. Teraz chce nagrodę. Owoców! Złota! Coś na wymianę!
Lilak kontynuuje negocjacje.

"Dagmara pomaluje twoje pióra" - Lilak
"Zgoda! Zgoda! Deiiw pokaże!" - szczęśliwy Deiiw

Andżelika pracując z Deiiwem i Lilakiem poradzili sobie z lokalizacją miejsca zamknięcia czarodziejki; Deiiw wrzucił ją do jakiejś dziury (stare szambo) i przykrył słomą by nie krzyczała głośno.
Andrea sformowała "oddział bojowy" - Deiiw, Liliak, Mieszko i Lidia. Ich zadaniem jest wyciągnąć i przyprowadzić z powrotem czarodziejkę.

Lilak jeszcze wyjaśnił Andrei, że Deiiw jest strażnikiem tego Skażonego Węzła (źródło żywienia). Jeśli ta czarodziejka do niego wpadła... to najpewniej Deiiw ją tam popchnął. Potem uratował i liczył na nagrodę. On się zajmie Deiiwem; to nie jest problem Andrei. Dla Andrei sam koncept tego że Deiiw dostanie nagrodę za wepchnięcie czarodziejki do Węzła... a niech tam. To ich problem.
Zespół Ratowania Niezgrabnych Czarodziejek Poszedł.

Marian, Andżelika i Andrea skupili się na mapie by znaleźć jak najwłaściwsze Węzły które można by przysposobić. Na mapie pomocna legenda które są do czego wykorzystywane (w domyśle: których nie rozmontowywać). Wspólnie udało im się zlokalizować Węzeł który można przysposobić. Dostarczy dwa tygodnie zasilania do hydroponiki i innych potrzebnych komponentów.
Dwa tygodnie to naprawdę dużo.

Julian przeprosił Andreę za porażkę. Powiedział, że w tym świetle niewiele byli w stanie zrobić bez ryzykowania. I pojechał oburzeniem - współpracują z Marianem Łajdakiem. Ten mag jest cholernym podłym szmuglerem KADEMu. Przez niego Julian kiedyś miał kłopoty bo wpadł w nieudany szmugiel. Więc niech chociaż przeszmugluje coś na korzyść ANDREI. W końcu to szmugler. A pośrednio pokaże to że Julian był niewinny i zawinił Zły Proces Świecy.
Andrea dała Pszczelakowi zadanie - postawić i uruchomić artefakty które Despustoszają. Szczęśliwie, Mieszko załatwił do tej malutkiej bazy kilka i zostały one ewakuowane.
Łajdak, Kozak, Przylaz i Kopiec mają opracować niemagiczny plan zajumania tej ciężarówki. W idealnej sytuacji tak, by nikt się nie zorientował i by ludzka policja uznała to za czyn o niewielkiej szkodliwości...

Andrea dostała wiadomość od Anety Rainer: "pojutrze dostajesz posiłki, wieczorem". Grupa Zajcewów. Millennium wie więcej. Aneta nie wie, jak Andrei pomóc z tym odbiorem. Hipernet ciągle agonalny.
Andrea potrzebuje kontakt z Millennium i to asap.

Andrea poszła porozmawiać z Rafaelem odnośnie kontaktu z Millennium. Mag Millennium wyraził zdziwienie - on nie wie o odbijaniu Zajcewów; powiedziałby. Ale to dobry pomysł. Mogą pojechać, bądź... zmienić odrobinę biomózg.

"Możemy wykorzystać ten biomózg. Przemodulujemy go, by móc wykorzystać fale biokomunikacji kralotycznej" - zainspirowany Rafael
"Nie." - gasząca go Andrea
"Możemy pojechać do Piroga; tam była kiedyś wasza baza. Ale to nieco groźne." - zafrasowany Rafael
"Zbyt ryzykowne; nie ma prostszej opcji?" - lekko zirytowana Andrea
"Mogę... zadzwonić na komórkę." - wzdychający Rafael uważający się za geniusza

Stanęło na komórce i umówieniu punktu który NIE jest aktualną bazą Świecy. Rafael ma zadzwonić spoza tego miejsca i umówić się poza nim. 
Rafael umówił się na spotkanie w Czubrawce; wsi stosunkowo niedaleko Pirogów gdzie rzadko dzieje się coś istotnego. Andrea się ucieszyła.

Jeszcze bardziej ucieszyła się, gdy wróciła ekspedycja. 
Mieszko zaraportował, że ją mają. Dalia Weiner. Lekarz i puryfikatorka. Nie Spustoszona. Lidia na nią spojrzała; jutro będzie aktywna.

Tego dnia Andrea jeszcze pojechała z Kajetanem i Andżeliką Oplem Astra. Po pierwsze, Opel jest najlepszą formą dyskretnego transportu. Po drugie, Andrea pokazuje, że pracuje z różnymi siłami - dobra rzecz do zademonstrowania. I spotkali się w Gospodzie Ze Smalcem w Czubrawce z Draconisem Diakonem.

Andrei jej szczątkowy hipernet wypluł informacje o Draconisie. Kralothborn, niebezpieczny. Jeden z lepszych terminusów Millennium. WCIĄŻ ludzki, acz MNIEJ. 
Zsynchronizowali swoją wiedzę. Draconis powiedział o tym, że współpracują ze Świecą i próbują wyciągnąć bandę Zajcewkommando; niestety, portal Millennium ma rozrzut. Draconis zasugerował portal do Pirogów, Andrea powiedziała, że to wykluczone. Draconis powiedział, że może zatem spróbować puścić amplifikator portalu na... łodzi a Andrea może jakoś 'przyciągnąć' używając hipernetowego emitera.
Andrea powiedziała, że jeszcze tego nie ma. Draconis rozumie.

Andrea ostrzegła Draconisa przed Czeliminem - jest to istotna kwatera wroga. Draconis dla odmiany ostrzegł Andreę przed... Myślinem. To miejsce się fortyfikuje przed zewnętrzem. Dodatkowo Draconisowi nie pasuje to, że grupa Weinerów siedzi i nie robi... nic poza defensywą. A normalny Weiner coś by już wysadził. Dodajmy do tego to, że do Myślina docierają ogromne fale surowców - ciężarówkami i pociągami - i nagle sytuacja robi się skomplikowana.

Andrea powiedziała Draconisowi, że potrzebuje wsparcia niemagicznej 'wiły'. Dodatkowo, potrzebna jest Draconisowi dziewczyna o nieco luźnym podejściu do seksu. Draconis się uśmiechnął. Melodia Diakon. Wyśle ją do Andrei. Draconis też zaproponował Andrei kralotha do pomocy - i to nie zwykłego kralotha a takiego, który przedtem był w Kompleksie Centralnym i współpracował min. z Wiktorem Sowińskim. Ten kraloth dużo widział i w dużej ilości rzeczy uczestniczył - a Melodia i Rafael potrafią z nim rozmawiać. Tyle, że morale u Andrei spadnie jak ta sprowadzi sobie kralotha... bo kraloth.

A z drugiej strony może wzrosnąć w Kręgu Życia - bo kraloth jest viciniusem i to udowadnia, że Andrea "mówi szczerze".

Andrea przyznała się Draconisowi do tego, że jest Lady Terminus. Draconis pogratulował. 

"Czeka cię proces." - Draconis, spokojnie
"Wiem..." - Andrea, ciężko

Andrea martwi się ciosem morale po wprowadzeniu kralotha. Bardzo. Więc z Draconisem się umówili, że kraloth wpadnie na 2-3 dni.

# Progresja



# Streszczenie

Andrea nawiązała połączenie z Anetą Rainer. Dowiedziała się o starym przekaźniku hipernetowym, poprzedniej generacji (przed Zaćmieniem); niestety, Julian i jego skrzydło nie dali rady go zdobyć (właściciel jest 'zaprogramowany'). Rafael powiedział Andrei, widząc stan Barana, że efekty uboczne już strzeliły. Nie spodziewał się celibatu. Andrea w odpowiedzi nawiązała kontakt z Draconisem i z nim weszła w sojusz. Najważniejsze wydarzenie to wejście przez Andreę w sojusz z Kręgiem Życia i oddaniem im miasta (Wtorku Śląskiego) w zamian za silną współpracę i przyjęcie na stałe placówki Świecy na tym obszarze. Andrea już nie może doczekać się swojego procesu...

# Zasługi

* mag: Andrea Wilgacz, negocjująca liczne sojusze (po minimalnych kosztach), budująca swoje siły do walki ze Spustoszeniem i rozpaczliwie dbająca o morale i możliwość działania.
* mag: Aneta Rainer, dowodząca spokojnie tym co może w Kopalinie i komunikująca się z Andreą dając wszystkie potrzebne jej informacje. Solidna i można na niej polegać.
* mag: Julian Pszczelak, który nie dał rady zabrać człowiekowi uszkodzonej ciężarówki (ze skrzydłem szkoleniowym) więc skończył instalując sprzęt Anty-Spustoszeniowy
* mag: Lidia Weiner, niewiele może pomóc Rudolfowi, ale Dalię Weiner postawi błyskawicznie.
* mag: Rafael Diakon, faktyczny geniusz; zbyt chętny do własnych rozwiązań jak na gust Andrei. Pomocny, acz trzeba uważać - zapytaj trzy razy o alternatywę.
* mag: Andżelika Leszczyńska, po* magała w formowaniu ugody z Kręgiem na linii prawnej i formalnej; też: analiza map. Ogólnie, trzecioliniowa. Też: hydroponika.
* mag: Marian Łajdak, p.o. ambasadora w imieniu Andrei; też jej osobisty kierowca. Po* maga w określeniu najlepszego Węzła do zajumania. Trzecioliniowy.
* mag: Mieszko Bankierz, solidny pierwszy oficer. Odbił Dalię, instalował zabezpieczenia, ostrzegł Andreę przed problemami Barana... idealny zastępca.
* mag: Kajetan Weiner, rozbudowywał defensywy, badał Pryzmat zamku (niekorzystny wynik) i pojechał osłaniać Andreę na spotkanie z Draconisem.
* mag: Tadeusz Baran, czarodziej mający problemy zdrowotne przez brak seksu powiązany z zabiegami Rafaela.
* mag: Franciszek Myszeczka, Mówca Kręgu Życia dla * magów i * viciniusów. Niechętny Świecy (złe doświadczenia); wynegocjował Wtorek Śląski jako teren Kręgu. 
* vic: Świeży Lilak, Mówca Kręgu Życia dla * viciniusów i * magów. Duży czarny pies. Poważnie traktuje sojusz ze Świecą; dzięki elektrodzie na podgardlu umie mówić.
* vic: Deiiw Podniebny Grom, próżny, srokopodobny, wielki dzienny puchacz (nie pytajcie) władający kinezą. Kocha targowanie się i być w centrum uwagi. Uratował Dalię przed Skażonym Węzłem.
* mag: Dalia Weiner, wpadła do Skażonego Węzła z pomocą Deiiwa, po czym odbita i uratowana przez siły Świecy pod kontrolą Mieszka. Nieprzytomna.
* mag: Draconis Diakon, z ramienia Millennium wymienił informacje z Andreą. Chce współpracować. Sprowadza Andrei na ranek siły jakich ta potrzebuje.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Czubrawka, wieś niedaleko Pirogów (rozsądne dojazdowo) gdzie rzadko dzieje się coś istotnego
                    1. Gospoda Ze Smalcem, gdzie spotykają się Andrea i Draconis
                1. Wtorek Śląski, który w ramach porozumień Lady Terminus Andrei i Franciszka Myszeczki przechodzi pod jurysdykcję Kręgu Życia
                    1. Ośrodek historyczny
                        1. Ruina zamku, który - jak się okazuje - ma Pryzmat "nikt się tu nigdy nie obronił". Super.
                    1. Ośrodek rolniczy, dumnie nazwany fragment Wtorku gdzie dominuje rolnictwo
                        1. Sześciokrąg plantacji, 6 plantacji pod kontrolą Kręgu Życia
                            1. Plantacja Krawędzi, reprezentacyjna plantacja (z dominacją magów)
                                1. Domostwo Krawędzi, gdzie rezyduje Mówca Franciszek Myszeczka
                    1. Obrzeża
                        1. Skałki Bajeczne, gdzie znajduje się bardzo brudny Węzeł (Paranoia, Rage) do którego wpadła Dalia Weiner
                1. Myślin, do którego przybywa mnóstwo surowców pociągami i ciężarówkami i który jest spokojny... ZA spokojny.
                1. Czelimin, jako jedna z istotnych kwater Drugiej Strony
       
# Skrypt

- Katalina w opałach
- Głód i logistyka

# Czas

* Dni: 2