---
layout: inwazja-konspekt
title:  "Sekrety Rezydencji Szczypiorkow"
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [130503 - Renowacja obrazu Andromedy (An)](130503-renowacja-obrazu-andromedy.html)

### Chronologiczna

* [130503 - Renowacja obrazu Andromedy (An)](130503-renowacja-obrazu-andromedy.html)

## Misja właściwa:

- Po tym, jak terminus dowiedział się tego, czego się dowiedział o Kasi (pamięć itp), ma doła. Nie jest w stanie zająć się niczym konstruktywnym. Kasia daje mu do rana spokój.
Kasia dowiaduje się, że symbol tego medalionu (mnich z dłonią uniesioną w górę) jest symbolem z okolic wschodu Europy; jest to symbol pochodzący z prawosławia i przedstawia to, że każdy typ "cudów" jest podrzędny wobec Boga i kapłanów (jeśli pochodzi od Boga, oczywiste. Jeśli od Szatana, Bóg jest silniejszy). Ale - w połączeniu z rewersem, który widziała na medalionie Augustyna, ma trochę inne znaczenie - przedstawia nadrzędność władzy Kościoła nad wszelkimi formami cudów.
Nie jest pewna, czy mówić to "swojemu" terminusowi - nie spodobałaby mu się myśl, że magowie powinni być podrzędni wobec jakichś kapłanów.
Kasia podejrzewa, że szukanie tematów pod tym kątem może być monitorowane przez różnego rodzaju magów. Zakłada alter-ego sieciowe, skrzynka jednorazowa... mail do studenta informatyki 5 roku (losowego) który chce się dorobić - jak najwięcej informacji na ten temat. Ma nie zmyślać, ale WSZYSTKO jest ważne.
Kasia sugeruje terminusowi, że to Żanna jest czarodziejką, ale sugeruje, że niekoniecznie ona jest mastermindem za wszystkim. Kasia nie rozumie, jaka w tym wszystkim jest rola Żanny - starsza pani nie ma żadnego celu w czymś takim... August (terminus) nie może uwierzyć. Musi być więc inny mag - jaki mag da sobie dowodzić człowiekowi? Mag zawsze jest mastermindem...

- W nocy Kasia miała sen. Patrzyła w lustro. W lustrze widziała swoje własne odbicie, ale nie było nią. Odbicie nie pamięta kim jest - ale Kasia chce ją - chce je zabić. Pokazała rankę na ręce (i Kasia, jako jej odbicie uniosła swoją rękę - taka sama ranka). Odbicie zastanawia się, czy to Kasia czy odbicie - która jest prawdziwa. Kasia na to zadała proste pytanie odbiciu - "co jadłaś na śniadanie?". MEGA kill konfliktu (brak konfliktu), odbicie się rozpadło. Kasia się obudziła.
Stoi przed lustrem. Na lustrze wypisane jej krwią (tzn. w kurzu; wiele krwi tam nie ma, to było parę kropel) - "Wszystko można zapieczętować używając luster".
Kasia wylizała rankę, po czym poszła spać.

- Ranek. Kasia weszła do pokoju terminusa, odsłoniła zasłony i podała mu śniadanie. Terminus, zgnębiony, w nocy podjął decyzję - nic Kasi nie zrobi. Jest inteligentną kobietą, choć nie jest magiem. Może i dla jego ojca jest szansa? Plus, uratowała mu życie... Terminus najwyżej zaryzykuje karierę (nie, żeby miał perspektywy chwilowo)...
Kasia zauważyła pod łóżkiem terminusa małe lusterko z kroplą krwi. Czuje, że byłoby niekorzystne, gdyby temu lusterku coś się stało. Schowała je gdzieś w szafie.
Porozmawiała z teminusem - co zamierza? August ma plan - on chce iść z Kasią na spotkanie z Augustynem. Do tej rezydencji. On chce wyczuć pasywną aurę rezydencji - tak potężna efemeryda nie mogła powstać "tak po prostu". Musi jej towarzyszyć silne pole magiczne.
Weszła do pokoju Samira, w szlafroku, z zakrwawioną chusteczką. W nocy krew z nosa jej leciała, na tle ciśnieniowym. Podroczyła się z terminusem, udając, że jej szlafrok się rozpina. Terminus spłonił raka i się odwrócił błyskawicznie.
Terminus musi być czysty auralnie, więc nie wolno mu do wieczora czarować.
Obserwująca to wszystko Sandra gotuje się ze złości.

- Samira poszła "na miasto", Kasia sprawdzić pocztę w kafejce internetowej. Od studenta nic nie przyszło, niestety. Tymczasem do mieszkania wynajmowanego przez Kasię i Samirę wparowała Sandra. Skrzyczała terminusa za łamanie Maskarady, za przedmiotowe traktowanie ludzi, za bycie jurnym capem... ogólnie, dobrze jej szło. Na to weszła Kasia. Terminus przedstawił ją jako koleżankę (czarodziejkę) nie z SŚ, od czasów Zaćmienia która wezwała go na pomoc z efemerydą. Sandra straciła rezon. Spytała głupio czemu został na noc - bo nie miał gdzie iść, oczywiste.
Dyskusja szybko przerodziła się w tematy ludzistyczne - Sandra uważa, że ludzie są osobami godnymi pochwały, nie godnymi pogardy. Dała wizytówkę Kasi ("jakby jej potrzebowała"). Druga, rzucona w kąt, była dla Augusta. Sandra wyszła.
Kasia zauważyła, że Sandra "has a crush" wobec Augusta, ale zachowała to dla siebie.

- Gdy Kasia i August zostali sami, August zwymyślał Sandrę od najgorszych. Czemu ona tu jest? Czemu chce go kontrolować? Chce się napawać zniszczeniem jeszcze jednego maga??? I jak to wpłynie na ich akcję? Wyjaśnił Kasi pojęcie ludzizmu i podał jej wizytówkę którą dała mu Sandra - jest tam telefon i email, ale nie ma nic do skontaktowania magicznie. Kasia od razu zauważyła różnice w grubości wizytówki tej i swojej. Natychmiast powiedziała coś w stylu "Jako mag uważam, że" i wskazała na wizytówkę. August się błyskawicznie połapał. Porozmawiali o czymś niezobowiązującym, po czym Kasia zgubiła wizytówkę w centrum handlowym, ale sobie zachowała tą "bezpieczną".

- Jak już byli sami - naprawdę sami - Kasia spytała Augusta o co chodzi z tą Sandrą. August jej wyjaśnił, że Sandrze nie można ufać i że jest paskudną postacią, bo jest oportunistyczną zdrajczynią. Jej ojciec utracił magię podczas Zaćmienia, ale nie było z tym żadnych problemów. Był cenionym członkiem społeczności i nie musiał czarować, jego wiedza wystarczała. Matka tworzyła artefakty, dzięki którym mógł udawać że ma moc. Sandra to publicznie wyznała i zgłosiła to do odpowiednich sił. Nie dało się tego zatuszować. Nie było wyjścia - prawo jest jedno. Ojciec został wymazany a matka dostała pouczenie. Wyparła się Sandry.
Kasia zaznaczyła, że nie wszystko jest wiadome. Nie wiemy, jak było naprawdę, więc nie można oceniać. Terminus jej jednak nie ufa, nie lubi i nie chce mieć z Sandrą nic wspólnego. A fakt, że dziewczyna przyczepiła się do jego mistrza nie pomaga...
[Od Żółwia i Kić: W rzeczywistości matka zaczęła badać czarne rytuały by on odzyskał moc i Sandra to zgłosiła, by chronić matkę i powstrzymać ją przed czymś naprawdę złym. Matka dostała geas, by nie mogła się interesować takimi rzeczami, ojciec został wymazany. Reszta się zgadza. Sandra dostała nawet za to nagrodę, która jednak była "zatrutą pigułką".]

- Kasia i August nie jadą vanem. Na pewno skubana Sandra coś tam podrzuciła. PKP for the win. Bilety kupiła dzień wcześniej, wydrukowała, jest więc trudna do wykrycia. Przy podejściu na stację kupiła bilety do jakiegoś miejsca, nie do miejsca docelowego do Opola (i zmyliła Sandrę).
Terminus spojrzał na Kasię z szacunkiem.

- Gdy Kasia i August zbliżyli się do rezydencji Szczypiorków, zauważyli Ingę stojącą na gałęzi drzewa. Inga ubrana jest w czarną suknię i ma welon, wygląda na osobę w żałobie. Nie pokazali po sobie, że zauważyli jej obecność. Jednocześnie zauważyli, że jakąś zapłakaną kobietę odprawił lokaj.
Co mogą zrobić... idą dalej.

- Kobieta widząc, gdzie idzie Kasia i August ich zaczepiła. Przedstawiła się jako Izabela Kruczek. Poprosiła, by Kasia (jeśli może) przekazała dalej problem - Inga może być świadkiem pewnego wydarzenia, przez który jej syn został aresztowany. Chodzi o pobicie. Ktoś został pobity a jej syn z uwagi na historię z prawem został aresztowany. Inga była w pobliżu i była prowodyrką.
Kasia poprosiła, by kobieta poszła do okolicznej knajpki. Jak Kasia wyjdzie, porozmawia z nią. Zdesperowana matka się zgodziła. Kasia nic nie obiecuje, ale spróbuje coś zrobić.

- Lokaj zaprowadził Kasię do gabinetu Augustyna. Po drodze Kasia (i tylko Kasia, o czym jeszcze nie wie!) usłyszała kłótnię Radosława Szczypiorka z siostrą, Jolantą Wójt. Kłócili się o Ingę. Jolanta broniła Ingę przed jakimkolwiek podejrzeniem, Radosław żądał, by odpowiedziała za to cokolwiek zrobiła - przzecież można zawsze ją wykupić za pieniądze. Co mogła zrobić 19letnia dziewczyna, w końcu? Jolanta na to "nawet nie masz pojęcia".
W końcu Jolanta nie wytrzymała i kazała Radosławowi odczepić się od jej córki. Ten ją natychmiast uciszył zimnym, cichym głosem. Nie po to ojciec (Augustyn) tak się starał by wszystko dookoła podnieść i dopracować, by teraz jakaś rozwydrzona smarkula była ponad prawem.
Kasi się to spodobało, choć uznała to też za bardzo niepokojące. Jaka siła każe matce się zamknąć, jak atakują jej dziecko?

- W gabinecie Augustyna jest spokój, ale Kasia chciała dać szansę terminusowi na przejście się po domu by jak najwięcej zobaczył. Spytała, czy można przejść się do ogrodu, by mniej osób słyszało. Augustyn się zgodził. Po drodze Kasia usłyszała kilka kolejnych szczegółów. Usłyszała, że Inga zrobiła coś bardzo złego i Jolanta marudziła o ciężkiej karze. Radosław stwierdził, że prawo i sprawiedliwość są jedne dla wszystkich. Nie po to ojciec ciężko pracował, by teaz ona wszystko zniszczyła. Inna ciekawa kwestia - Radosław zarzuca, że Inga przebiera się w strój żałobny bez powodu, Jolanta na to, że taka moda.
Radosław zagroził siostrze tonem. A ona się wycofała i przeprosiła.

- W ogrodzie Kasia wyjaśniła Augustynowi, jak wygląda sytuacja. Dotarła do oryginalnego twórcy obrazu i dowiedziała się od niego, że zamówiono obraz bez podpisu. Kasia podała adres, podała też kto ten obraz zamówił - Żanna.
W odpowiedzi Augustyn powiedział, że Żanna się przyznała do tego, co zrobiła. Obraz się uszkodził w transporcie i nie chciała Augustyna drażnić. Dodatkowo, ten krucyfiks był problemem - kiedyś taki krucyfiks "zniknął" z jednego z kościołów i on, Augustyn go kupił. Potem dopiero się dowiedział co kupił.
Ok...

- Kasia zdecydowała się pokazać Augustynowi jeszcze jedną rzecz. To odpowiednio spreparowane płótno, by pokazać, że Inga raczej nie mogła przebić go nożem w taki sposób. Augustyn wezwał Radosława, by ten spróbował zniszczyć płótno tak, jak to podobno zrobiła Inga. Nie udało się to Radkowi. Augustyn powiedział, by Radosław znalazł osobę która NAPRAWDĘ to zniszczyła. Radosław i Augustyn wymienili spojrzenia - oni wiedzieli, kto naprawdę to zrobił, że zrobiła to NAPRAWDĘ Inga...

- Lokaj nie wyprowadził ich do wyjścia jak chciał Augustyn - wyprowadził ich do innego miejsca w ogrodzie. Tam czekała na nich (a dokładniej na Kasię) Jolanta Wójt, matka Ingi. Powiedziała Kasi, że sama jest aspirującą artystką (malarzem). Czy Andromeda mógłby zobaczyć jej dzieła do recenzji?
Jasne, Kasia się zainteresowała. Jak długo nie są oprawione a w rulonach, może zabrać je ze sobą.

- Wreszcie lokaj wyprowadził ich na zewnątrz. Tam zdecydowali się pójść sobie, lecz z drzewa dobiegł ich głos Ingi "wykorzystana, opłacona, odrzucona, zapomniana. Jak wszyscy". Mówiła oczywiście o Kasi i jej roli w całej tej sprawie. Ta się odgryzła Indze - "Ale wolna, swobodna i z prawem wyboru". Wyraźnie dotknęła senda. Inga zeskoczyła z gałęzi. Kasia spytała, czy Inga nie przeszłaby się z nią do knajpki (licząc na konfrontację), ta jednak wymówiła się tym, że jest w żałobie. 
Za to pociągnęła za sobą Kasię i Augusta do swojego pokoju, zupełnie ignorując błagania lokaja.

- Pokój Ingi jest eleganckim i bogatym pokojem panienki z dobrego domu. To, co się Kasi rzuciło od razu w oczy to kompletny brak srebra, sporo książek okultystycznych, nihilistycznych i o homo superior (oraz "kinder-satanistycznych"). Inga triumfalnie zdemaskowała Augusta jako prawdziwego Andromedę (malarza-geja) i zniknęła w pokoju obok. W czasie, w którym Kasia żarliwie prosiła Augusta, by ten NIE brał pędzla do ręki (a on żarliwie się zgadzał), Inga się przygotowywała.
Wyszła do nich nago, w płaszczu a'la Dracula, z kwiatem i czerwonym winem w kieliszku.
Terminus spiekł raka i uciekł.
Kasia wyjaśniła, że August nie jest Andromedą i nic nie będzie z sesji - Andromeda nie maluje inaczej niż ze zdjęć. Nawet dla niej. Za żadne pieniądze. Szkoda...
Z ciekawostek - na ciele Ingi są lekkie blizny, choć liczne. W szafce ma hantle. Takie, jakich nie powinna móc unieść.

- Lokaj już czekał. Zaprowadził ich do wyjścia. Po drodze mieli nietypowe spotkanie z kotem - kot podłożył sprytnie skórkę z banana, terminus (wciąż myślący o Indze i jej ciele) się prawie przewrócił a kot na niego zeskoczył i nadstawił się do głaskania. Jest piekielnie sprytny jak na kota. ZA piekielnie sprytny.

- W końcu opuścili tą rezydencję. Spotkali się w knajpce z Izabelą Kruczek i jej mężem. Tym razem ona milczała, on więcej mówił. Ich syn miał problemy z prawem, zauroczony był Ingą. Podobno doszło do pobicia ekspedienta w Żabce, było ich troje - dwóch facetów i Inga. Oczywiście, oskarżono jedyną osobę która miała zatarg z prawem...
Pobicie jest niestety bardzo ciężkie.
Kasia poprosiła o odbitki materiałów. Dostała je. Ma plan wysłać je Radosławowi i zobaczyć co z tego będzie :>. Przy odrobinie szczęścia, niemało :D.

- Wrócili w końcu do domu po tym wszystkim. Terminusa bardzo martwi aura magiczna w tamtym domu i to, że wykrył liczną obecność lapisu, co zamaskowało aurę magiczną. Kot jest dowodem na Skażenie magiczne, drugim (pośrednim) dowodem jest Inga...

- To był długi dzień.

"Najgorsze jest to, że przy takiej aurze magicznej życzenia stają się prawdą" - August
"Wiesz, że ona miała tam sporo książek o _homo superior_?" - Kasia


----
**Rozmowa Kasi z Augustem po powrocie z rezydencji**

- Chodzi mi o Ingę.
- Ach, tak... <rumieniec>
- Nie, nie to! Weź, wsadź głowę pod kran czy coś.
- Łatwo Ci mówić...
- Kojarzysz tą rozmowę między Radosławem a Jolantą? Chcę mu wysłać komplet materiałów od tych ludzi.
- Jaką rozmowę?
- Nie mów, że nic nie słyszałeś...
- Ale jaką rozmowę?
- Tą, w której się kłócili o Ingę.
- Nie było takiej rozmowy. Nie kojarzę żadnej. 
- <cicho> Ty nie słyszałeś... lokaj nie słyszał... Augustyn nie słyszał... to czemu ja słyszałam..?
- Ale co?!
- Przeczytaj sam.
- Co? 
- No to moje wspomnienie.
- Dobrze.

----
**Z rozmowy Kasi z Augustynem**

- Gdy powiedziałam "przeczytaj sobie wspomnienia z tego pobytu" nie miałam na myśli "obejrzyj sobie Ingę z innego kąta"! 
- Ja... przepraszam.
- No jasne, obejrzyj wroga z innej strony! A nuż z tyłu coś ukrywa!
- ... - August chciał zapaść się pod ziemię.
- Przynajmniej widziałeś też to, co miałeś oglądać! Twoja opinia? - Kasia była zniecierpliwiona.
- Ja... To ta efemeryda!
- Jasne, każdemu psu Burek! Postarałbyś się choć zabrzmieć przekonująco!
- Nie, naprawdę! Ta efemeryda, którą zniszczyłem, miała coś wspólnego z lustrami! Sama widziałaś ją w lustrach! I... te dźwięki dochodziły Cię właśnie z lustra! To znaczy... efemeryda Cię uwrażliwiła. Tymczasowo. Znaczy... przy takim polu to nic dziwnego... - August próbował rozpaczliwie się ratować.
- Taaak... - Kasia się zadumała. - Efemeryda... Zaraz, moment! I tak bzdura! Co efemerydę miałby obchodzić jakiś chłopak!
- Ten, któego skażą przez Ingę? - Terminus się zarumienił.
- Wiesz co, przez nią nie da się z Tobą gadać. Idź do jakiegoś przybytku i się rozładuj, co?
August uciekł z pokoju. Kasia patrzyła za nim oniemiała.
- W y n o c h a  z  m o j e j  s y p i a l n i!!!

----
**Z dyskusji Augusta z Kasią**

- Mam zgryz co robić dalej.
- Czemu?
- Powinienem zrobić dwie rzeczy jednocześnie. Po pierwsze, powinienem poszukać czegoś na temat tego medalionu w archiwach magicznych i tym podobne. Po drugie, trzeba umieścić Ingę pod stałą obserwacją, ale nie patrz tak na mnie! - Krzyknął terminus - Chcę ocenić jej próg skażenia! Jaka jest. Niebezpieczna!
- No już dobrze, nie musisz się tłumaczyć.
- Wiesz... z Ingą będziesz miał problem. To dwie godziny drogi.
- Nie, jeśli użyję magii.
- I wejdziesz na teren posiadłości świecący magią? Bałabym się nawet zbliżyć.
- Ale przed niemagicznymi metodami na pewno mają zabezpieczenia. To są bogaci ludzie. Tacy zawsze mają coś takiego.
- Wiesz... chyba wiem, kto tam jest magiem.
- Ta starsza pani.
- Ano...
- Ja się zastanawiam, czy jest jedynym magiem...
- Popatrz, myślimy podobnie. A ja mam... dziwny pomysł.
- Słucham.
- Jak wejść do pilnie strzeżonej fortecy?
- Zapukać?
- Dokładnie. - Kasia się uśmiechnęła. - Jeśli ona jest magiem, a jestem tego praktycznie pewna, to dlaczego z nią nie porozmawiać?
- Na zasadzie 'czarodziej z czarodziejem'?
- Aha.
- W sumie dobry pomysł. Też Cię jej przedstawię jako czarodziejkę.
- Erm... Wiesz... Ja trochę... Nie tego...
- W innym wypadku muszę iść sam.
- Wiesz, możesz mnie przedstawić. Tylko... co, jeśli ona na przykład zażąda dowodu?
- Nie zażąda. Nie ma podstaw! - W głosie terminusa brzmiało niedowierzanie. - Jaki mag przedstawi Ci człowieka jako maga?
- Wiesz co... chcę Ci coś jeszcze pokazać. Myślałam, że to był dziwny sen, ale... Może wpływ efemerydy...

----
**Z myśli Kasi**

Kocham tego chłopaka! Sam mi daje wymówki!
Wszystko z lustrami mogę podpiąć pod efemerydę. Oczywiście, tymczasowo i w ramach rozsądku, ale zawsze.
...
Ciekawe, kiedy się zorientuje, że czytał moją pamięć...

----
**Uwaga Kasi do terminusa**

Pewnie nie zauważyłeś - bo miałeś co innego do oberwacji - ale w pokoju Ingi były hantle. Takie do ćwiczeń jakbyś nie wiedział. Ktoś, kto przerzuca takie żelastwo, mógłby rozpruć płótno obrazu bez większego kłopotu.
Co ciekawe, nie widać tego po niej.

----
**Rozmowa Kasi z Augustem**

- Zwróciłeś uwagę, że ona była cała w bliznach?
- Dokładnie to w siateczce blizn. Jak po zaklęciach leczących. Jak terminus po zaklęciach leczących.
- Po zaklęciach zostają blizny?
- Zależy od ilości quarków i mocy. - August się skrzywił. - Liniowemu terminusowi często się nie opłaca usunąć wszystkiego. To za drogie. Co innego ja. Tak młodemu facetowi nie wypada mieć blizn. Plus, mistrz kupuje droższe języczniki. Nie oszczędza na nas.
- No dobrze... Ale ona ma kasę. Więc... to nie ma sensu.
- Może niekompetentny mag leczniczy...
- Może....

# Zasługi

* czł: Kasia Nowak jako mastermind grupy szturmującej Rezydencję Szczypiorków. Aha, dla Sandry: terminuska i koleżanka Augusta.
* czł: Samira Diakon jako osoba w coraz gorszym stanie fizycznym. Najpewniej przez Kasię. Nadal jednak ma złośliwy humor.
* mag: August Bankierz jako jedyny terminus w okolicy i próbujący się połapać co tam się dzieje.
* mag: Sandra Stryjek jako podkochująca się w Auguście ludzistka i technomantka strasznie o Augusta zazdrosna i robiąca błędne założenia.
* czł: Augustyn Szczypiorek, (83 l) najsilniejszy w rodzie Szczypiorków i twardą ręką dążący do zaprowadzenia prawa i porządku niezależnie od kosztu.
* mag: Żanna Szczypiorek, (68 l) żona Augustyna która wywołała trochę chaosu chcąc wymienić obraz Andromedy na inny.
* czł: Radosław Szczypiorek, (44 l) syn Augustyna i Żanny i brat Jolanty. Wyraźnie silniejszy pozycją niż siostra, drugi po ojcu w rodzie.
* czł: Jolanta Wójt, (39 l) córka Augustyna i Żanny i aspirująca artystka broniąca córki i dobrego imienia córki przed bratem i innymi.
* czł: Inga Wójt, (19 l) buntowniczy chuligan nie przejmujący się tym, że przed gośćmi paraduje nago i cieszący się z reakcji szokowych.
* czł: Izabela Kruczek, matka chłopaka jej zdaniem niesłusznie oskarżonego o ciężkie pobicie z winy Ingi Wójt.