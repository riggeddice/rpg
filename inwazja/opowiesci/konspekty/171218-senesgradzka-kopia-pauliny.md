---
layout: inwazja-konspekt
title:  "Senesgradzka kopia Pauliny"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171001 - Powrót do domu (PT)](171001-powrot-do-domu.html)

### Chronologiczna

* [171001 - Powrót do domu (PT)](171001-powrot-do-domu.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* "Paulina" (efemeryda) zagraża BEZPIECZEŃSTWU Pauliny. Jeśli się utrzyma i będzie lepsza... Paulina będzie sławna na świecie.
* Efemeryda zagraża PRZYNALEŻNOŚCI osób pracujących w przychodni. Rozrywa ich więzi społeczne i zmusza do wiecznej ewolucji.
* Efemeryda zapewnia BEZPIECZEŃSTWO ludzi na terenie przychodni.

### Wątki konsumowane:

-

## Punkt zerowy:

-

## Potencjalne pytania historyczne:

* Czy lekarze zmienią się w agentów Efemerydy z biegiem czasu?
    * DOMYŚLNIE: Tak, jeśli Paulina ich nie zatrzyma.
* Czy Paulina zostanie sławną lekarką przez Efemerydę?
    * DOMYŚLNIE: Tak.

## Misja właściwa:

**Dzień 1**:

_EKSPERYMENT: sesja nie zawiera przeciwnika. Nie ma celów przeciwnika. MG jest reaktywny; sesja eksploracyjna._

Maria skomunikowała się z Pauliną. Powiedziała, że w Senesgradzie przyjmuje "Paulina Tarczyńska". Więcej, Maria poszła ją odwiedzić - faktycznie, lekarka, Paulina, zachowuje się jak Paulina i w ogóle. Różnica taka, że Maria ma soullink z Pauliną a z "Pauliną" nie. Kiedy "Paulina" zaczęła przyjmować? Maria powiedziała, że niecały miesiąc temu. Paulina skorelowała to z wołaniem SOS... ale jak to się ma do rzeczy?

Dracena siadła do komputera. Odnajdzie informacje o "Paulinie". Maria skupi się na wizji lokalnej.

**Dzień 2**:

Maria wróciła z informacjami z wizji lokalnej:

* cała Przychodnia Kolejowa działa bardzo skutecznie; mają nadnormalne wyniki.
* coraz szerzej idzie informacja, że Paulina jest wybitną lekarką; Dukat/Sądeczny musieli słyszeć. Acz Sądeczny mógł nie skojarzyć że są dwie.
* lekarze w Przychodni Kolejowej pracują niezwykle ciężko i są silnie zmotywowani do ratowania ludzi. Kosztem ich rodzin i życia społecznego.
* wśród osób, które odeszły z przychodni jest opinia Pauliny jako "kij w dupie" i "praca ponad życie". Jedna wola - ratowania ludzi.
* nawet średni lekarze i pięlegniarki po pewnym czasie zarazili się tą misją ratowania ludzi.
* przychodnia jest tam od dawna. O dziwo, w papierach Paulina nie została przyjęta. Ona po prostu tam JEST.
* "Paulina" wychodzi z przychodni. Mieszka niedaleko, w bloku. Sama. Nie prowadzi życia towarzystkiego. Jej sąsiedzi znają Paulinę "od dawna", acz nie wiedzą co to znaczy (od kiedy, jak się poznali). Była tu od zawsze.

Kić: "No nie powiem, efekty są zgodne z motywacją Pauliny, ale... ja nie chcę płacić tego PITa!"

Dracena natomiast dowiedziała się, co następuje:

* nic nie wskazuje na desperackie działania żadnego maga ani człowieka co prowadziłoby do tego zjawiska.
* są magowie, którzy leczyli się u "Pauliny". Zwłaszcza z Rodziny Dukata. Jak Dracena zrobiła followup, tamci magowie nie "wykryli" nic dziwnego. Myśleli, że to czarodziejka.
* do "Pauliny" wystąpił czarodziej który chciał pisać whitepaper odnośnie leczenia ludzi. Odmówiła, bo jest zbyt zajęta faktycznym leczeniem.
* Paulina nabiera opinii w środowisku bardzo kompetentnej i bardzo dostępnej. Czyt "wszyscy do Pauliny". Wektor wyraźny - za parę miesięcy Paulina będzie celebrytką. Taki popdoktor.

Miesiąc temu Paulina nie wiedziała, że tu wróci. Więc to nie może być coś celowane w Paulinę. To nie ma sensu. Ale jest ktoś, kto może coś wiedzieć - Kaja Maślaczek.

Paulina skomunikowała się z Kają po hipernecie. Poprosiła o spotkanie w sprawie Senesgradu. Kaja przyjechała godzinę później z nadzieją na dane z efemerydy. Nie, to nie o to chodzi. Paulina spytała Kaję, czy wie o kopii Pauliny w Senesgradzie. Kaja powiedziała, że wie - pierwotnie myślała, że to PAULINA (bo się schowała w Senesgradzie, leczy dalej...) a potem jak Paulina się ujawniła, to ok. Tak sama Kaja by zrobiła. Ale teraz jak Paulina mówi - to musi być efemeryda senesgradzka.

Zdaniem Kai, to nie jest pierwszy raz, gdy efemeryda senesgradzka robi coś takiego. Kaja ma pomysł, jak sprawdzić, czy to efemeryda senesgradzka (sprawdzić inhibitory założone przez Oliwię Aurinus) - sprawdzi to. Zdaniem Pauliny, dwie Pauliny to o jedną za dużo... no i Kaja pojechała sprawdzić co jest grane.

Parę godzin później, Kaja się do Pauliny odezwała. To jest efemeryda senesgradzka. Nie ma wątpliwości, to jej kolejna manifestacja w formie Pauliny. Co gorsza... efemeryda senesgradzka jest na wielu fazach jednocześnie. Mimo inhibitorów, efemeryda wspiera energetycznie swoją manifestację. Nie ma opcji zagłodzenia manifestacji - Kaja nie potrafi znaleźć fazy czy miejsca, gdzie można odciąć źródło energii. Zdaniem Kai, "Paulina" przyprowadziła "efemerydę senesgradzką" do przychodni.

Super. Cała Pauliny nadzieja w tym, że jeśli efemeryda senesgradzka dobrze skopiowała Paulinę, to z "tamtą Pauliną" da się rozmawiać.

I bomba od Marii - "Paulina" w rzeczywistości mieszka w domu niejakiej Janiny Muczarok. Lekarki. Ale Janiny większość osób nie pamięta; ci, którzy ją pamiętają kojarzą, że "gdzieś wyjechała". Samotna starsza lekarka została "przejęta" przez efemerydę senesgradzką. Nikt tego nie zauważył, bo... no, efemeryda senesgradzka. Aura. Paulina poprosiła Dracenę, by ta zebrała dla niej informacje odnośnie Janiny Muczarok - co mogło zwrócić na nią uwagę efemerydy, kim była, jakie miała zwyczaje... by móc pomóc ją "wyrwać".

Paulina szuka informacji która może "Paulinę" zdecentrować i zrozumieć skąd Janina się wzięła w tym kontekście. Ta efemeryda nie jest "drapieżna", więc Janina musiała CHCIEĆ. Zarówno Dracena jak i Maria się mają tym zająć. Dracena weszła w tryb komputrona i użyła mocy magicznej, Maria zaczęła szukać i analizować, zwłaszcza dokumenty i rozmowy z ludźmi. 

* Janina (dyskretna i samotna, aura senesgradzka, niespójne źródła)
* Dracena i Maria (detektyw i technomantka, atak wielokanałowy)

Po zniwelowaniu, Dracena vs Janina (9v5->7): nie ma Paradoksu. Odpowiedzi:

* Paulina poznała zbiór własności Janiny których Paulina nie ma. Tiki, upodobania... - np. zamiłowanie do wytrawnego czerwonego wina.
* Paulina poznała zbiór drobnych faktów z przeszłości bliskich Janinie (aspekt "Presja na Janinę")
* Janina była idealistką. Zawsze chciała pomagać. Cyniczną idealistką, bo skopało ją za dużo razy. A niedawno wykryto u niej raka. Była przekonana, że umrze. To mogło otworzyć wrota efemerydzie - była zmęczona, nie wierzyła, że ktoś jej pomoże. Była zła. Chciała pomocy, chciała dalej pomagać... i efemeryda mogła odpowiedzieć.

Co poprowadziło Paulinę do jeszcze jednej prośby dla Marii - niech ona zbierze od tych pacjentów historie jak Janina im pomogła. Jak bardzo. Jak bardzo była potrzebna. Opowieści o Janinie - nieco szorstkiej, ale pomagającej doktor Janinie. Maria przyjęła to do wiadomości - zajmie się tym asap. Wynik pojawi się dnia 3, na początku.

* Historie (spójna opowieść, mało czasu, wpływ aury efemerydy) -> 11
* Maria (reporter tworzący historie, poza zakresem aury, dogłębna znajomość historii Janiny) -> +9

Wynik: {14} (8v5). Maria zdobyła zbiór opowieści o pani doktor które skutecznie mogą zadziałać, by ją zdecentrować.

* Janina, która sama dawała pieniądze na schronisko dla trudnych nastolatków. Dać im szansę na przyszłość.
* Janina, która brała trudne przypadki, żeby pomóc. I jakkolwiek oschła, znalazła czasu na bycie z przerażonymi młodymi.
* Janina, która ogólnie skupiała się na młodych ludziach by im pomagać. Zwłaszcza tych, których uznano za straconych.

Co dodatkowo dało Marii wskaźnik - Schronisko "Nadzieja" w Senesgradzie, dla trudnych dzieci i młodzieży.

Tymczasem Kaja też nie próżnowała. Skupiła się na poznaniu natury samej "Pauliny" i na jej poziomach energetycznych. Chce dowiedzieć się jak może zdestabilizować magicznie wpływ efemerydy senesgradzkiej na hosta. Jak rozerwać połączenie.

* Rozerwanie Janiny (połączenie poza wykryciem, host tego chce, ogromna energia (6)) -> 14
* Kaja (doskonałe czujniki i znajomość efemerydy, mistrzyni manipulacji, subtelne działania) -> 9

Wynik: skonfliktowany sukces:

* Kaja znalazła sposób by wysłać odpowiednio modulowany sygnał w Janinę by odłączyć od niej efemerydę (plus dla Pauliny)
* Kaja dostaje do Progresji metodykę rozłączania opętańców efemerydy senesgradzkiej od efemerydy; toczy nad tym badania. (minus dla Kić)

Kaja skontaktowała się z Pauliną i powiedziała jej jak to zrobić. Przeprosiła, ale musi iść zająć się tematami na dworze Eweliny.

**Dzień 3**:

Paulina zdecydowała się spotkać z "Pauliną" - swoją efemeryczną kopią. Dracenę udało jej się przekonać, by została w odwodzie. Dracena spytała, czy jej obecność by nie mogła pomóc przez aspekt wiły - Paulina odbiła, że to nie pomoże. Dracena westchnęła ze smutkiem.

Paulina pojechała do Senesgradu. Poczeka aż "Paulina" skończy pracę. Paulina skonfrontuje się z efemerydą na osiedlu Szeptackim, gdzie mieszka "Paulina". Poszła po godzinach pracy, by móc spotkać się z efemerydą sam na sam. Ale jak dostać się do mieszkania zajmowanego przez groźną manifestację? Zapukać.

Manifestacja była zaskoczona obecnością Pauliny. Zachowuje się tak jak Paulina zachowywała się kiedyś. Ale ostrożnie wpuściła Paulinę i spytała, czemu ta wygląda jak ONA. Paulina odpowiedziała, że manifestacja dokonuje założenia - to MANIFESTACJA wygląda jak Paulina. Manifestacja przyjęła tłumaczenie Pauliny za "ok, we have a case". Nadal jest "Pauliną" i jak Paulina kiedyś próbuje to rozwiązać i pomóc Paulinie.

Paulina jest ciekawa jak dokładna jest kopia efemerydy. Eksperyment - dwie kartki papieru. Niech każda z nich zapisze pytania na swojej stronie kartki i potem się nią wymienią. Kiedy doszło do kopii. Do kiedy jest zgodność? Obie odpowiadają na pytania obu. INTERESUJĄCY EKSTRA KONFLIKT:

* Ukryte szczegóły: (dziwne spojrzenie efemerydy, praca w szczątkowej wiedzy) -> 8
* Maria przez Paulinę szuka szczegółów: (praca z niemożliwym, coś co nie ma sensu, łowca paranormalnych szczegółów) -> 9 +8 z karty

16v8-> sukces +2. Przez MOMENT Paulina miała dostęp do efemerydy w obszarach gdzie efemeryda chce jej pomóc. Dwa pytania które mogą Paulinie pomóc odnośnie rzeczywistości.

* Q: W jaki sposób manifestacja jest zasilana?
* A: Efemeryda senesgradzka jest szersza i bardziej rozległa niż ktokolwiek ma pojęcie. Efemeryda jest połączona z innymi bytami na różnych fazach. W różnych leylinach. Dlatego pozornie nieistotne rzeczy mają wpływ na efemerydę - bo naprawdę dotykają jej zasilania. Po prostu magowie nie widzą, że są powiązane... i zasięg efemerydy przekracza Senesgrad.
* Q: Co sprawiło, że efemeryda senesgradzka wstrzyknęła "Paulinę" w Janinę? Dlaczego zadziałała ta efemeryda?
* A: Efemeryda senesgradzka odpowiedziała na kombinację kilku czynników. Brak władzy centralnej (odbitej w braku rezydentki Pauliny) - odbiła się w pryzmacie i efemeryda przeczytała pryzmat. Uratowała kiedyś magów którzy zawołali z rozpaczą do niej. I to też się odbiło w pryzmacie - przede wszystkim Grzybb i Rukolas. Trzecia część to Janina - lekarka. Umierająca, pragnąca wiecznie pomagać. Efemeryda użyła Pauliny jako "wzoru". Najbliższe do jej pryzmatycznych ech.

Paulina jest lekko wstrząśnięta. Nie wie czemu Grzybb czy Rukolas się tam pojawili. Ale widzi diwergencję - od momentu zintegrowania z Marią efemeryda nie ma jej wzoru - albo nie użyła TAMTEGO wzoru a jakiś wcześniejszy. To nie jest zwykła efemeryda... jest jeszcze dziwniejsza niż ktokolwiek myślał.

Manifestacja jest bardzo zatroskana tym, co widzi. Paulina która opuściła teren? Walczyła z Karradraelem? Robiła dziwne rzeczy z Draceną? Brzmi... niePaulinowo. Manifestacja jest jeszcze bardziej przekonana, że to ONA jest prawdziwą Pauliną. I trudno jej się dziwić... acz niektóre z tych pytań pokazują, że manifestacja jest PERFEKCYJNĄ KOPIĄ Pauliny. Co dla Pauliny jest mrożące krew w żyłach - nawet pytania jakie manifestacja zadała są pytaniami, jakie dawna Paulina by zadała.

* Manifestacja Efemerydy: ("z woli hosta", "głęboka kopia Pauliny", aura motywacji, manifestacja efemerydy, połączona z Efemerydą Senesgradzką (6), świat spójnych kłamstw (6), RESURGENCE:5)

Paulina zahaczyła manifestację - czemu nie wróciła do domu? Jak czaruje przy efemerydzie? Manifestacja wyjaśniła, że odeszła z roli rezydentki - magowie przeszkadzali jej w robieniu tego co jest ważne. I nauczyła się czarować przy efemerydzie, czerpiąc energię INACZEJ. Po czym manifestacja sama zapytała Paulinę, co się z nią stało. Czemu odeszła. Jak Dukat ją puścił. Paulina tu kombinuje jak jej to powiedzieć i nie zdradzić informacji o Marii. 

* "W kosmicznym porządku to ONA jest Pauliną": (deepcopy, aura motywacji, spójne kłamstwa (6)) -> 15
* "Taka jaka jestem jestem wystarczająca": (Maria i Dracena, silny research i Pryzmat i Wzór, przywykła do katalizy i mentalki) -> 9+7

Wynik: 20v15, efekt+2. Paulina nie tylko odparła atak efemerydy; dodatkowo przeszła do kontrataku i zaczęła zwracać manifestacji uwagę na Janinę Muczarok. Taka lekarka, taka opinia. Czy manifestacji nie brakuje tej lekarki? "Paulina" zaczęła się zastanawiać nad tą sytuacją. Faktycznie, nie ma jej. "Tak, zaprosiła mnie, bym jej pomogła..." - manifestacja przebiła się na sekundę. RESURGENCE -1.

Paulina powiedziała manifestacji, że Janiny nie ma. Manifestacja chce znaleźć Janinę; zaczerpnęła energię "znikąd", podpinając się do leylina bezpośrednio. Paulina zrobiła się biała. Podwójnie, gdy manifestacja zaproponowała wspólny czar - wspólnie mogą znaleźć Janinę. Paulina z wahaniem zdecydowała się złapać tą okazję. Ostrzegła Dracenę. I zdecydowała się... znaleźć Janinę. Wraz z manifestacją.

* "Znaleźć Janinę w efemerydzie": (połączona z efemerydą Senesgradzką (6), "z woli hosta", bezpośredni kontakt, energia leyline) -> 17
* "Znaleźć Janinę w manifestacji": (przez Quarka (6), czym Janina była (opowieści), modulacja Kai (6)) -> 12+9

Efekt Paradoksalny. RESURGENCE -1. Paulina zobaczyła Efemerydę Senesgradzką w jej całej multiwymiarowej okazałości. Nie zrozumiała NIC. Ku swej radości - to lovecraftiański widok. I zobaczyła Janinę; znalazła ją. Znalazła ją w manifestacji. Jej ciało tam nadal jest, przesączone magią Zmysłów i zmienione przez efemerydę. To nie jest "aura motywacji", to element jej natury potrzebny Efemerydzie by zapewnić Janinie to, czego ona oczekiwała. Motywowanie lekarzy do właściwej pracy. Ale za to nie ma już raka ;-).

Subtelna rekonstrukcja efemerydy - to jest "kierowany" vicinius, nie losowo złożony. Innymi słowy, to nie Skażeniec, to vicinius. Zdrowy, stabilny vicinius oparty o magię Zmysłów.

Manifestacja zbladła. W jej oczach PAULINA jest Janiną. Więc dlatego wróciła. Wróciła do domu. A manifestacja ma jej pomóc wrócić do bycia Janiną. Paulina oklapła - jak walczyć z czymś, co w żaden sposób nie może być po prostu przekonane? Ma plan. Niech Manifestacja spojrzy jeszcze raz. I tym razem uderzyła nieco innym zaklęciem: 

* "Dewastacja Pauliny przez nadmiar energii": (energia E.S. (6), energia leyline, Skażenie otoczenia) -> 14
* "Wymusić zrozumienie Janiny na manifestacji": (przez Quarka (6), modulacja Kai (3)) -> 9+9

Wynik: 19v14-> sukces+2. RESURGENCE-1 oraz manifestacja widzi Prawdę.

Manifestacja musiała zmierzyć się z prawdą. To nie Paulina jest Janiną. To ona... manifestacja wpadła w rozpacz. To niemożliwe - nie tylko dlatego, że ona chce żyć. A chce. Też dlatego, bo to znaczy, że Paulina ma popieprzone życie i wszystko jest z dupy. Paulina powiedziała, że Janina była zdesperowana. Lekarze tracą rodziny. Aura... jest niebezpieczna. Janina, taka jaka jest... jest celem dla terminusów. Czy może Janina może po prostu stać się jednością z efemerydą? Może lepiej jeśli Janina ze swoim podejściem po prostu zintegruje się z efemerydą i będzie tyci przesuwać myślenie efemerydy w kierunku na pomoc ludziom?

Manifestacja się zastanowiła. Gdzie Paulinowa chęć pomocy wszystkim? Paulina zauważyła, że pomaga, by nikomu nie zaszkodzić. Manifestacja zauważyła, że Paulina chce ją zabić. Ją i Janinę. Nie, nie chce - MUSI. Paulina potwierdziła, że nie chce. I że musi.

* "Nie chcę umierać...": (wola życia Janiny, inercja efemerydy, aura motywacji) -> 11
* "You must go...": (ona jest Pauliną, opowieści o Janinie, najlepsza opcja, research Marii) -> 12+4

Wynik: 17v11-> sukces+2. RESURGENCE-2.

* "Tyle... jeszcze mogłam zrobić... rozwiązywałam problemy mieszkania w polu magicznym, w Senesgradzie... moi pacjenci..." - manifestacja Pauliny
* "Zrobiłaś dużo. Zadbam o nich." - Paulina

Manifestacja powiedziała, że fizycznie wejdzie do efemerydy i otworzy swoje kanały magiczne. Roztopi się w efemerydzie. Wyraziła ogromny żal, że nie może żyć swoim życiem i że Janina zginie - ale rozumie. To jest najlepsza opcja, nikomu nie zagrażająca. Paulina wyraziła żal, że to nie mogło być prawdą. Manifestacja potwierdziła. Poprosiła Paulinę, by ta żyła swoim życiem tak, by ono było tego warte.


## Wpływ na świat:

JAK:

1. Co na tej sesji dookoła jakiegoś konfliktu czy zdarzenia było fajne? Co chcesz by w ramach tego się poszerzyło?
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

* Paulina dostała przednią reputację naprawdę skutecznej lekarki z czasów Senesgradu.
* Czy efemeryda Senesgradzka jest naprawdę efemerydą? Czy to nie jest coś innego? Może większego?

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (15)
    1. Janina Muczarok: w jakiejś formie istnieje jako byt wydzielony z Efemerydy; nadal chroni i pomaga schronisku dla nastolatków.
    1. Kaja Maślaczek: pracuje nad tym (z powodzeniem) by móc oddzielać byty sprzężone z Efemerydą Senesgradzką.
    1. Kaja Maślaczek: zostawiła i złożyła w Senesgradzie czujniki, by pomóc Paulinie pomagać randomowym ludziom Skażonym przez Efemerydę
    1. Efemeryda Senesgradzka: jest ogromna i łączy się z bardzo szeroką ilości leylinów i energii przez różne fazy
    1. Paulina Tarczyńska: zobowiązała się przed Efemerydą, że będzie chronić i pomagać ludziom Skażonym przez magię w Senesgradzie
1. Kić (5)
    1. Efemeryda Senesgradzka: gdziekolwiek sięga zasięg efemerydy, tam jest zwiększona motywacja do pomagania ludziom przez wpływ Janiny.
    1. Maria Newa: "jest w domu". W Senesgradzie znalazła zatrudnienie w gazecie "Dobro Ludzi" i znalazła tam miejsce dla siebie w mieszkaniu Janiny.

# Streszczenie

W Senesgradzie pojawiła się "druga Paulina". Okazało się, że to manifestacja Efemerydy Senesgradzkiej powstała na skutek rozpaczy starszej lekarki umierającej na raka. Paulina dowiedziała się mniej więcej jak funkcjonuje Efemeryda Senesgradzka i udało jej się zażegnać problem. Przy okazji Maria znalazła "dom" w Senesgradzie a Kaja zaczęła pracę nad destabilizacją manifestacji Efemerydy Senesgradzkiej.

# Progresja

* Efemeryda Senesgradzka: gdziekolwiek sięga zasięg efemerydy, tam jest zwiększona motywacja do pomagania ludziom przez wpływ Janiny.
* Efemeryda Senesgradzka: jest ogromna i łączy się z bardzo szeroką ilości leylinów i energii przez różne fazy
* Janina Muczarok: w jakiejś formie istnieje jako byt wydzielony z Efemerydy; nadal chroni i pomaga schronisku dla nastolatków
* Kaja Maślaczek: pracuje nad tym (z powodzeniem) by móc oddzielać byty sprzężone z Efemerydą Senesgradzką.
* Kaja Maślaczek: zostawiła i złożyła w Senesgradzie czujniki, by pomóc Paulinie pomagać randomowym ludziom Skażonym przez Efemerydę
* Paulina Tarczyńska: otrzymała od Kai Maślaczek w Senesgradzie czujniki, by widzieć randomowych ludzi Skażonych przez Efemerydę
* Paulina Tarczyńska: zobowiązała się przed Efemerydą, że będzie chronić i pomagać ludziom Skażonym przez magię w Senesgradzie
* Paulina Tarczyńska: świetna opinia doskonałej profesjonalistki, zwłaszcza w Senesgradzie. W świecie ludzi. Pomocna ludziom w sytuacjach beznadziejnych.
* Maria Newa: "jest w domu". W Senesgradzie znalazła zatrudnienie w gazecie "Dobro Ludzi" i znalazła tam miejsce dla siebie w mieszkaniu Janiny.

# Zasługi

* mag: Paulina Tarczyńska, skonfrontowała się z manifestacją senesgradzką siebie samej. Z bólem, przekonała swą manifestację do rozpłynięcia się w Efemerydzie. Poznała prawdziwą rozległość Efemerydy Senesgradzkiej.
* czł: Maria Newa, błyskawicznie zauważyła "Paulinę" w Senesgradzie, pomagała w negocjacjach, zbierała informacje i wplotła się w tkaninę Senesgradu. Dla Pauliny i dla siebie. Nieoceniona.
* mag: Dracena Diakon, komputron i ochrona Pauliny; doskonale wyszukuje informacje współpracując z Marią. Jak już się nie kłócą to świetnie współpracują.
* mag: Kaja Maślaczek, wezwana przez Paulinę w sprawie Senesgradu zdobyła sekwencję modulacyjną pomagając Paulinie. Bardzo była skupiona na pomocy Paulinie i społeczności senesgradzkiej.
* vic: Janina Muczarok, ciężko chora lekarka, która zintegrowała się z efemerydą senesgradzką tworząc manifestację Pauliny. Przekształcona w viciniusa, jej fizyczna forma umarła na końcu sesji.
* vic: Efemeryda Senesgradzka, dała Janinie to, o co ona prosiła. Benevolent lovecraftian horror. Zbudowała imprint Pauliny sprzed sprzężenia z Marią. Na tej sesji - nieaktywna.

# Plany

* Paulina Tarczyńska: Paulina od czasu do czasu poświęca czas by pomagać ludziom w Senesgradzie. Nie tylko dla Janiny, też, bo tak trzeba.
* Maria Newa: Zupełnie nie ufa Kai; chce zbudować własną sieć informatorów w Senesgradzie i upewnić się, że magia będzie pod kontrolą.
* Dracena Diakon: Ma w planach zdecydowanie częściej współpracować z Marią. Choćby po to, by Paulina przestała robić głupoty.
* Kaja Maślaczek: Zafascynowana Efemerydą Senesgradzką. Chce ją zrozumieć lepiej. Bardzo zmartwiona negatywnymi konsekwencjami Efemerydy na Senesgrad. Będzie monitorować.
* Janina Muczarok: W ramach swojej możliwości, w zakresie dostępu Efemerydy Senesgradzkiej, pomagać ludziom - leczyć, chronić. Zwłaszcza nastolatkom w kłopotach.
* Efemeryda Senesgradzka: Zdobywać nowe imprinty, nowe istoty by móc wstawiać odpowiednie byty w odpowiednie scenariusze.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Senesgrad
                    1. Szeptak
                        1. Przychodnia Kolejowa, gdzie przyjmuje manifestacja Pauliny.
                        1. Schronisko Nadzieja, dla nastolatków; bardzo udzielała się w nim swego czasu Janina Muczarok (lekarka).
                        1. Osiedle Szeptackie, osiedle sypialniane dla Senesgradu, gdzie mieszkała Janina, potem mieszkała "Paulina" i nie mieszka już nikt.
                    1. Dworcowo
                        1. Kolejowa Wieża Ciśnień, gdzie z efemerydą stopiła się Janina Muczarok kontrolowana przez manifestację Pauliny Tarczyńskiej.

# Czas

* Opóźnienie: 0
* Dni: 3

# Wątki kampanii

-

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
