---
layout: inwazja-konspekt
title:  "Zniknięcie Sophistii"
campaign: druga-inwazja
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna 

* [140114 - Zaginiony członek(AW, HB)](140114-zaginiony-czlonek.html)

### Chronologiczna

* [140114 - Zaginiony członek(AW, HB)](140114-zaginiony-czlonek.html)

## Misja właściwa:

Do Hektora Blakenbauera przyszedł brat, Marcelin. Powiedział, cały zmartwiony, że Sophistia zniknęła - od czasu włamania Sophistii do rezydencji Blakenbauerów nikt jej nie widział. Hektor niespecjalnie się tym przejął, lecz Marcelin się bardzo zmartwił. Do tego poziomu, że zdecydował się zadzwonić do najlepszego prywatnego detektywa na rynku. Do... Waldemara Zupaczki!
Hektor wybił mu to z głowy. Skąd w ogóle pomysł, żeby do Zupaczki dzwonić? Odpowiedź Marcelina była prosta - Zupaczka dotarł do Margaret pierwszy, zanim pojawiła się na miejscu Andrea. No, ale Hektor zaznaczył, że Zupaczka pracował dla przeciwników, dla "drugiej strony" - strony, która porwała Margaret. To najgorszy detektyw, którego można kiedykolwiek zatrudnić w tej sprawie!
Marcelin niechętnie przyznał mu rację. Ale on się trochę boi Andrei po tym incydencie w barze... czy Hektor mógłby jakoś to rozwiązać? Hektor westchnął. Widać, znowu na niego spada ściągnięcie do rezydencji Andrei. Ostatnio jak Marcelin sam zabrał się za poszukiwania prawie wyzwał Bolesława Bankierza na pojedynek... lepiej niech jego brat to zostawi...
Hektor chciał ściągnąć Andreę na południe, na obiad. Marcelin jednak przekonał go, że to musi być rano. Im wcześniej tym lepiej - w końcu Sophistia zaginęła!

Andrea naprawdę, NAPRAWDĘ nie doceniła tego, że ktoś ją wyciąga w sobotę z łóżka o 7 rano. No ale skoro to Hektor Blakenbauer, i to z nowym zleceniem... niech będzie. Zwłaszcza, że nadal nie rozumie, czemu o tego psa było tyle hałasu.
Po usłyszeniu o co chodzi, Andrei oklapły łapki. Poprzednio szukała psa, a teraz jakaś dziewczyna (mająca reputację osoby pojawiającej się i znikającej) zniknęła na ponad 24 godziny. No tak bardzo poważny detektyw! No ale dobrze... fakt faktem, w sprawę zamieszany był jakiś tajemniczy mag.

Najważniejsze, to dowiedzieć się od Marcelina jak najwięcej odnośnie Sophistii - kim jest, jak się nazywa, jaką jest osobą. Odpowiedzi Marcelina doprowadziły jednak wszystkich do głębokiego facepalma.
- Sophistia jest bezdomna. Tzn. mieszka zawsze u kogoś, ktoś ją przygarnie na noc, ale nie ma jednego "miejsca".
- Marcelin nie wie jak się nazywa. Wie jednak, z kim się bliżej przyjaźni - Diana DeVille (przydomek), która jest performerką w "Attice" (ludzki klub tematyczny o klimatach burleski).
- z nastawienia Sophistia jest hipiską i (nie wojującą) anarchistką. Jest do tego artystką wysokiej klasy. A zwłaszcza poetką.

Czyli mają dwa główne kierunki działania: Diana DeVille i klub "Attica" oraz Zły Mag, który próbował porwać Margaret i wykorzystał Sophistię. Hektor stwierdził, że ten "zły mag" jest w sumie trochę większym problemem niż zniknięcie bezdomnej anarchistki (mimo obiekcji Marcelina), więc zdecydował się dać Andrei jeszcze jedno zlecenie - zlokalizować i doprowadzić jego, Hektora do "złego maga" by móc go osądzić i postawić przed sądem.
...wait, what?
Andrea wytłumaczyła delikatnie Hektorowi, że ludzki sąd może nie być zbyt skutecznie w stanie poradzić sobie z magiem, zwłaszcza takim. Ale zlecenie zostaje! Na zlokalizowanie i złapanie. Tu cena poszła do góry... szukanie i złapanie niebezpiecznego przestępcy to dość ciężka sprawa.

No dobra. Dwa zadania naraz. Na szczęście da się zająć oboma! Andrea wykorzystała swoje kontakty na czarnym rynku, wynajęła 3-4 zbirów (Tanich Drani) i nasłała ich na Waldemara Zupaczkę (what... XD). Idea była taka, że Zupaczka został przez kogoś wynajęty, więc powinien wiedzieć przez kogo i w jakich okolicznościach. Zbiry mają wszystkie rzeczy wyciągnąć od Zupaczki, a bonus, jeśli jeszcze nakarmią go pluskwą (znaczy jeśli ją zje i będzie można zobaczyć czy coś potem powie czy z kimś się skontaktuje). Jak zdecydowała się zrobić, tak zrobiła - Zupaczka został napadnięty we własnym gabinecie, ostro pobity, nakarmiony pluskwą i wszystko powiedział - wynajęła go czarodziejka, nazywała się Amelia Eter. Miał odzyskać dla niej psa, który jej uciekł. Nie, Zupaczka nie pytał o szczegóły. Nic nie chciał wiedzieć. Przyjął takie zlecenie, bo płaciła. Potem zadzwoniła do niego, że już nie jest zainteresowana tym zleceniem... to zlecenie zostało odwołane, pies sam wrócił (po tym jak zwinęła go policja). On bał się, że to coś większego skoro policja go aresztowała, więc wolał się nie wychylać i nie narażać.

Czyli ogólnie Zupaczka nic nie wie. Niesamowite, kto by się tego spodziewał...
(Kić: "Ale należało mu się. Za przyjmowanie głupich zleceń.")

Jednocześnie, Andrea poszła spotkać się z ową Dianą DeVille. Wyciągnęła z niej kolejną porcję informacji odnośnie Sophistii - Sophistia jest osobą która ma grupę miejsc i osób, które ją z przyjemnością przygarną. Jest dość znana w środowisku, ale nie jest osobą "wpływową". Ogólnie jest osobą lubianą. Nie ma nigdy problemów z pieniędzmi, jednak nie jest bogata. Bardzo często zarabia w dziwne sposoby (np. tańczy na stole i deklamuje wiersze), a potem rozdaje zarobione pieniądze ludziom. Wydaje więcej niż zarabia. 
Diana powiedziała Andrei, jak nazywa się Sophistia - Jagoda Jeżyk. Ta natychmiast przekazała informację Hektorowi, który zrobił cross-check teo w bazach policyjnych i potwierdziło się coś dobijającego - Jagoda Jeżyk, Kamila Kirys, Aleksandra Arrat... to jest wszystko jedna i ta sama osoba. Ale wszystkie te persony tak naprawdę nie istnieją. Innymi słowy, nie wiadomo kim Sophistia jest naprawdę a wcześniej policji naprawdę nie zależało na szukaniu jej prawdziwej tożsamości - Sophistia po prostu nie wpada w TAKIE kłopoty z policją by było warto.
Andrea wróciła do Diany mówiąc o wielu tożsamościach i ta się jedynie uśmiechnęła. Jednak, gdy Andrea dała radę Dianę przekonać, że Sophistii naprawdę może coś grozić, Diana powiedziała, że faktycznie coś zwróciło jej uwagę. 

Otóż, dzień przed zaginięciem Margaret, do Sophistii podeszła nieznana jej kobieta. Sophistia nie wiedziała kto zacz i przyjęła ją z sympatią, jednak tamta się przedstawiła jako "Amelia Eter". Sophistia wyraźnie zbladła. Poszły na stronę. Potem Sophistia wróciła. Nie chciała o tym rozmawiać z Dianą, jednak poprosiła ją by pamiętała o jednej rzeczy jakby coś się złego stało - ona, Sophistia, idzie do miejsca dyskretnego spotkania, w jednym z budynków przeznaczonych do rozbiórki, na ulicy Koralowej. Andrea wzięła to pod uwagę i podziękowała Dianie - jeśli jest w stanie pomóc Sophistii, zrobi to.

Oki. Mamy więc "Amelię Eter", która pojawia się już dwa razy. Hektor puścił Amelię Eter po policyjnej bazie danych a Andrea poszła obadać to tajemnicze miejsce spotkania - może znajdzie jakieś wskazówki odnośnie lokalizacji Sophistii. Amelia Eter - okazało się, że jest to córka przedsiębiorcy branży metalurgicznej, silnie powiązanego z polityką (ludzką). Ten człowiek, Rufus Eter, ma córkę. Jednak córka nie pokazuje się ludziom, żyje w odosobnieniu... no nie pojawia się nigdzie. A bogaty ojciec potrafi skutecznie wyłączyć nadmiar niewłaściwych pytań.
Ciekawe.

Dobrze. Teraz Hektor ma pewne podstawy do działania. Sophistia jest osobą, która zbyt często zmienia tożsamość. A to może być dość poważne przestępstwo (zwłaszcza, że robi to bardzo nielegalnie). To wystarczy, by zażądać informacji gdzie po raz ostatni zarejestrował się jej telefon komórkowy. Dostał odpowiedź szybko - na dworcu kolejowym, całkiem niedawno (poniżej godziny). Poprosił policję o wysłanie tam kogoś - a nuż ją złapią. Jest 16:21.

Andrea dostała się do miejsca, gdzie miała spotkać się Sophistia z Amelią. Po uważnym przejrzeniu okolicy, weszła ostrożnie i stanęła jak wryta. To pułapka. Grupa sieci, w których zaplątany jest... odbezpieczony granat. Delikatnie zaczęła rozbrajać pułapkę tylko po to, by otrzymać silny cios od tyłu. Zwinęło ją, aktywowała pułapkę... ale granat był rozbrojony.
Andrea stanęła przed... no, potworem. Jest to zdecydowanie vicinius, albo bardzo skażony i przekształcony mag. Ma kilka cech pokrewnych z _dromopod iserat- i z potworem, który był Strażnikiem bazy Moriatha. Potwór (mający cechy kobiece) się zdecydowanie zdziwił - Andrea nie jest Blakenbauerem, nie ma ochoty jej zabijać. Potwór szybko wyjaśnił Andrei, że ród Blakenbauerów musi zostać zniszczony i ona, Andrea, nie powinna się w to wszystko mieszać. Inaczej oberwie z rykoszetu. A to jej jedyne ostrzeżenie. Na pytanie Andrei o Sophistię, potwór odpowiedział wzruszając ramiona, że jest już za późno i nie da się jej uratować. Gdy na zegarku Andrei wybiła pełna godzina (17:00), potwór dodał - Sophistia właśnie zginęła.

Andrea zaatakowała. Z zaskoczenia przewróciła potwora i go kopnęła tylko po to, by zostać ciężko poturbowaną przez szpony i potężne mięśnie viciniusa. Bijąc Andreę, vicinius jedynie dodał ze wściekłością "Moriath już nigdy nie powróci!" oraz insynuował, że to Blakenbauerowie stoją za Moriathem. Andrea nie jest w stanie walczyć, pada na ziemię nieprzytomna. Potwór na szczęście dla Andrei nie bił żeby zabić; próbował jedynie poturbować ciężko i pozbawić przytomności. I pokazać, z jaką łatwością może to zrobić.

O 17:24 Hektor jest zirytowany tym, że nie ma żadnej informacji odnośnie Sophistii. Pyta jak wygląda sytuacja i dowiaduje się, że przyjechała policja, znalazła dziewczynę zaćpaną w trupa i z uwagi na to, że zdecydowanie przedawkowała wezwali karetkę. Aktualnie Sophistia przebywa w szpitalu pod opieką lekarzy.
...wait, what? Przecież to nie pasuje.
Nnnno dobra. Hektor wraz z Marcelinem jadą do szpitala, zobaczyć jaki jest stan Sophistii. Marcelin jest w końcu alchemikiem, powinien być w stanie pomóc bardziej niż "zwykli" lekarze. No i Hektor z bardzo ciężkim sercem przygotował się do poproszenia o pomoc swego drugiego brata, Edwina. Który jest magicznym lekarzem. Który jest tak bezwzględny i skłonny do eksperymentowania jak dziadek. Super.

Na oddziale - klęska. Lekarze wszędzie, Sophistia umiera, niewiele mogą jej pomóc. Po prostu dawka śmiertelna. Co gorsza, Sophistia ma sine wargi i bardzo jasne tęczówki, czyli coś jeszcze jej jest (hint: jad dromopod iserat). NA TO WSZYSTKO ma objawy skażenia. W sumie, nędza. Hektor używa magii mentalnej do umożliwienia pracy Marcelinowi, ten jakoś stabilizuje Sophistię by mógł się nią zająć Edwin i by mogła w miarę sensownie funkcjonować (czyt. by mogła odzyskać świadomość). Potem Hektor jeszcze wykrył w niej efekt mentalny zmuszający ją do samobójstwa o 17:00 skacząc pod pociąg - to też naprawił. Wow...
Zabawnym efektem ubocznym tego koktajlu magii, narkotyków, jadu i innych takich było to, że Sophistia staje się niewrażliwa na zaklęcia wymazujące jej wiedzę o magii. Ot, taki psikus. To jest kwestia "jeśli to się zrobi, ona umrze". Suuuuper.

Po ustabilizowaniu Sophistii Hektor stwierdził, że odwiozą ją z Marcelinem do kliniki Edwina - specjalizującej się w takich sprawach. Lekarze, pod wpływem mentalki, niewiele protestowali :P. ALE NIESPODZIANKA! Sophistia pojechała do rezydencji Blakenbauerów!

Tymczasem Andrea się ocknęła i zdecydowała się na żałosny powrót do rezydencji Blakenbauerów. Tam doszło do spotkania Andrei, Blakenbauerów i ledwo świadomej Sophistii (na której widok Andrea się rozpłakała). Przesłuchana, Sophistia potwierdziła że wie jak wygląda mężczyzna, który wstrzyknął jej narkotyki. Hektor zrobił obraz tej postaci i Andrea go od razu poznała. To jest terminus.
...???

Czyli, podsumowując (chronologicznie):
- Zły Mag Vicinius w miejscu spotkania programuje Sophistię przy użyciu jadu dromopod iserat do kradzieży psa (TE ŚRODKI DLA PSA!!!)
- Sophistia ma popełnić samobójstwo o 17:00 na dworcu
- ALE po drodze spotyka terminusa, który jej wstrzykuje narkotyki i odbiera jej sprawność ruchów
- Nie jest w stanie popełnić samobójstwa, umiera od narkotyków
- Znajduje ją policja i ratują Blakenbauerowie.

...nieźle.

Andrea wie, kim jest ten terminus. To młody mag, Dariusz Kopyto. Ze swojej strony Hektor przypomniał sobie, że pamięta podobną sytuację - młody chłopak który nie brał narkotyków, a jednak zaćpał się na śmierć. To był syn jego kolegi, adwokata. Mając już dwie sprawy (dwie osoby) puścił zapytanie o wszystkie podobieństwa i wszystkie powiązania i dostał bardzo ciekawą odpowiedź:

Było ich pięcioro. Taka młoda banda. Syn adwokata, Sophistia, kucharz, pracownik fizyczny, syn policjanta. Z tej grupy żyją aktualnie tylko: Sophistia i kucharz (który w pijackim szale pobił na śmierć swojego kolegę; nota bene, jednego z tych pięciu i siedzi aktualnie w więzieniu). Wcześniej nie było tego widać (bo jak), ale wszystko wskazuje na to, że nasz terminus upodobał sobie całą ekipę i konsekwentnie eliminował jednego za drugim.

OMG TERMINUS SERYJNY MORDERCA! I DIDN'T KNOW!

Ale... o co tu chodzi? Dlaczego? Tego nie wiedzą. Andrea zdecydowała się dowiedzieć; nie może tak to zostać.

Sophistia zapytana o "Amelię Eter" odpowiedziała ze smutkiem "więc już wiecie". Okazuje się, że to ona, Sophistia, jest Amelią Eter. Jej poglądy, działania i styl życia mogłyby przynieść katastrofalne szkody jej ojcu, w związku z tym zerwali kontakty. Owszem, rodzice nadal ją wspierają finansowo i nadal się czasem kontaktują, ale nie mają ze sobą wiele wspólnego. W jej oczach, jej rodzice żyją w "obcym, złym" świecie. W ich oczach, ona przegrywa swoje życie i traci szansę za jaką wielu ludzi sprzedałoby nerkę.

Sophistia idzie na "naprawę" do Edwina Blakenbauera.


Sophistia jest uratowana.
Nie wiadomo nic więcej o magu/viciniusie, polującym na Blakenbauerów.
Dlaczego terminus zabija ludzi?
...tego dowiecie się w kolejnych odcinkach.


![](141023_ZnikniecieSophistii.jpg)

# Zasługi

* mag: Hektor Blakenbauer jako główny silnik odnalezienia Sophistii który wykorzystuje policyjne bazy danych do znalezienia faktów na jej temat by przekazać je Andrei.
* mag: Andrea Wilgacz jako bezwzględna detektyw szukająca Sophistii i ciężko poraniona ofiara Estery Piryt w formie pośredniej z Dromopod Iserat.
* mag: Waldemar Zupaczka jako biedna ofiara Andrei; pobity przez oprychów przyznał się do tego, że nic nie wie. A wszystko bo wziął pewne zlecenie.
* czł: Amelia Eter jako Sophistia, hipiska i (nie wojująca) anarchistka, niezła poetka która nabawiła się odporności na magię mentalną przez Skażenie, narkotyki i jad Dromopod Iserat.
* mag: Marcelin Blakenbauer jako osoba bardzo zmartwiona zniknięciem Sophistii do tego stopnia, że zmusił całą rodzinę by Coś Zrobili by ją uratować. I ją uratował.
* czł: Diana Larent jako Diana DeVille, performerka w "Attice" i przyjaciółka Sophistii która dała się przekonać Andrei, że jeśli nic o Sophistii nie powie, to Sophistia może zginąć.
* czł: Rufus Eter jako ojciec Sophistii i bogaty przedsiębiorca branży metalurgicznej, silnie powiązanego z polityką (ludzką) nie utrzymujący kontaktów z córką.
* vic: Estera Piryt jako vicinius na bazie Dromopod Iserat zastawiający pułapkę na Blakenbauera (chcący ich zniszczyć) o nastawieniu anty*Moriathowym. Użyła Sophistii jako przynęty.
* mag: Dariusz Kopyto jako terminus seryjny morderca który chciał zabić Sophistię (i przypadkiem uratował jej życie).