---
layout: inwazja-konspekt
title:  "Portal do EAM"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160723 - Czyj jest Kompleks Centralny? (SD)](160723-czyj-jest-kompleks-centralny.html)

### Chronologiczna

* [160723 - Czyj jest Kompleks Centralny? (SD)](160723-czyj-jest-kompleks-centralny.html)

## Kontekst ogólny sytuacji

- W kompleksie Centralnym jest około 500 magów i 100 viciniusów. Szlachta ma poniżej 100 magów. Koło 200-300 magów jest chorych na Irytkę.
- Lojaliści Ozydiusza ufortyfikowali się w Pionie Ogólnodostępnym
- Chorzy na Irytkę są w Technoparku Max Weber
- Tworzą się frakcje:
.... Szlachta, która ma wreszcie okazję przejąć władzę
.... Kurtyna, w opozycji do Szlachty. "Każdy ale nie oni".
.... Bankierze, którzy stanowią alternatywny (prawy) ośrodek władzy
.... Zajcewowie, którym dość pasuje chaos i wolność
.... Neutralni, którzy chcą kontynuacji swojej pracy i mają wszystko gdzieś
.... Stabilizatorzy, któzy chcą doprowadzić gildię do jak najlepszego działania niezależnie od konsekwencji
.... Lojaliści Ozydiusza, chcący zapewnić, by cele Ozydiusza były spełnione i wszystko działało zgodnie z planem
.... Wolni Magowie, którzy mają dość potęgi wielkich rodów

## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Wiktor praktycznie walił głową w ścianę. 500 magów. 100 viciniusów. 8 frakcji i atomizacja postępuje. Alicja Weiner, sekretarka Wiktora, spokojnie mu to wszystko zreferowała. Wiktor przesunął ją do Kompleksu Pałacowego, bo tu jest bezpieczniej. Dodatkowo, przez chaos - zgodnie z Alicją - podstawowe czynności Kompleksu przestają działać. Zasilanie. Farmy. Podstawowe rzeczy. Połączenie Irytki i ogólnej paniki daje taki wynik. Ktoś musi asap podpiąć się do zarządzania Kopalinem; bez osób na "tronie" Kompleks po prostu nie działa. A politycy byli POZA Kopalinem, gdy portal się rozsypał. Podpiąć się może każdy wysoki mag rodu rozpoznawany przez Świecę.

Dagmara zauważyła, że dlatego chciała zniszczyć Bankierzy. Wiktor powiedział, że podjął właściwą decyzję, ale Siluria widziała jego zawahanie. Szok mija i Wiktor wraca do normy. Wiktor rozkazał Dagmarze pójść do Diany i użyć "klucza" w Bramach; niech przejdzie przez portal i zdobędzie ekspertów. Kto pierwszy zdobędzie ekspertów i zabezpieczy Kompleks, temu zaufają magowie. Więc Dagmara odeszła. Wiktor odesłał Alicję, by ta zajęła się rozmową z Lenartem. Dopóki działa Irytka, Kompleks nie działa poprawnie. Po delikatnej sugestii Silurii, sam zdecydował się pójść. Nie ma dyshonoru. On nie potrzebuje wsparcia. Po namyśle, wyśle Dianę. Ona jest lepszą negocjatorką.

Alicja "Lordzie, Diana zachowuje się troszeczkę inaczej ostatnio."
Wiktor "Stres."

Tomasz "Lordzie, Bankierze odbijają Magazyn! Z bronią!"
Wiktor "Jak oni śmią!"
Tomasz "Możemy odpowiedzieć ogniem?"
Wiktor "Nie! Oddajcie pozycję. Niech oni walczą z bratami i siostrami ze Świecy. Ten magazyn jest niepotrzebny."

Wiktor powiedział Silurii, że tamte artefakty, że Skarbiec są zbyt niebezpieczne. Mogą zniszczyć Świecę. Po co mu to?
Na sugestię Silurii, że powinien skontaktować się z Bankierzami teraz, powiedział, że wpierw ma zamiar zbudować swoją pozycję jako "ten, który dba i chce pomóc Świecy". Ma zamiar pokazać, że jest też świetnym administratorem.

Siluria zapytała, jakie są największe problemy aktualnie w Świecy. Alicja powiedziała: hipernet, zasilanie i niedługo głód - wśród viciniusów. Wiktor pchnął gońców do Hydroponiki i do Magazynów; niech dowiedzą się co się dzieje i jaki jest stan żywności. I jak długo starczy dla kluczowych viciniusów. Może być konieczność przekierowania głównych operacji pod kątem viciniusów kluczowych a inne wprowadzić w stazę? Lub importować z zewnątrz - ale import wymaga przejścia przez teren kontrolowany przez Lojalistów Ozydiusza...

Wiktor o lojalistkach Ozydiusza "Przecież oczywiste, że to wina Blakenbauerów, czemu te dziwki stoją mi na drodze?!"

Wiktor oddalił się zająć swoimi sprawami. Siluria zaczęła wypytywać Alicję o jaki klucz chodzi; Alicja powiedziała, że ryzykują uszkodzenie Zasilania w Energetyce, ale w konsekwencji mogą dostać dostęp do eskpertów którzy doprowadzą Kompleks do działania. A najpewniej inni przywódcy nie wiedzą, że Diana ma ten klucz. Alicja spytała Silurię, czy nigdy nie zdziwiło jej, że KADEM nie napotkał EAM? Ten klucz który ma Diana to właśnie klucz do EAM - i Dagmara z Dianą przejdą przez portal, by móc ściągnąć ekspertów tutaj. Ekspertów zdolnych do tego, by postawić Kompleks Centralny.
Moment, jeśli Dagmara jest Spustoszona, istnieje ryzyko, że Spustoszy... EAM. Nie wiadomo, jak ostrzec ani zadziałać, a do tej pory Overmind Spustoszenia miał wszystko przewidziane. Nie wiadomo co robi zbroja Dagmary, nie wiadomo, czy nie ma jakiegoś na to planu.

Siluria zaczyna powoli urabiać Wiktora, żeby faktycznie Gerwazy mógł dowodzić obroną więzienia. Ale nie tak, żeby było, że ona CHCE. Nie, po prostu nie chce widzieć tej zakazanej mordy; do więzienia jego miejsce ;-). Sama poszła do Gerwazego, który sprawdza defensywy w skrzydle Sowińskich. Gerwazy nie do końca chciał współpracować; nie widział powodu, czemu miałby cokolwiek robić w tym temacie. Siluria powiedziała mu, że chcą przejść na EAM a Dagmara ma jakieś własne plany z tym powiązane. Nadal nic. Siluria zaproponowała, że Gerwazy może ściągnąć coś od Millennium co będzie mu potrzebne; przyjdzie z żarciem dla viciniusów. Gerwazy zaproponował jej kontakt z innym magiem. Siluria się nie zgodziła. Z ciężkim sercem, Gerwazy powiedział, że się tym zajmie. On też nie chce, by EAM było zagrożone. A nawet on nie słyszał o kluczu do EAM.
Przekazał temat tien Judycie Karnisz po szczątkowym hipernecie jaki mu został.

Siluria wróciła pracować nad danymi od Alicji odnośnie tego jaka żywność będzie potrzebna, w jakim stopniu itp. z Alicją. Przy okazji dowiedziała się, że Alicja pracuje z Wiktorem parę lat; jest wciągnięta przez Dianę ze swojego rodu Weinerów i Alicja też jest gorliwą popleczniczką Szlachty. W czym - Siluria zauważyła - Alicja nie wie o mroczniejszych aspektach Szlachty. Nigdy Alicja się z nimi nie spotkała i nie musiała. To urzędniczka, nie rebeliantka. Zdaniem Alicji doszło do katastrofy w Kompleksie Centralnym i ogólnie dookoła gildii; ona wierzy, że Szlachta będzie w stanie poradzić sobie z utrzymaniem porządku aż właściwe siły wrócą. W końcu Świeca jest wieczna.

Siluria podeszła Alicję, że to niesamowite, że zbroja Dagmary jest tak nieprawdopodobna; to bije magitechy Tamary na głowę. Skąd Wiktor to ściągnął, to nie wygląda jak EAM. Alicja potwierdziła - power suit Dagmary nie ma nic wspólnego z EAM. To coś innego; artefakt, potencjalnie jedyny lub jeden z nielicznych. Wiktor dostał ten power suit od Oktawiana Mausa. ZNOWU Oktawian Maus. 
Światła zgasły. Generatory padły. Po chwili uruchomiły się generatory awaryjne; Siluria czuje zapach Skażenia w powietrzu.
W radiowęźle Kompleksu rozległ się głos. Wojciech Szudek, puryfikator, ostrzegł, że w okolicach Bram doszło do silnego Skażenia. Puryfikatorzy się tym zajmą.
Czyżby Gerwazy?

Alicja "To nie wpłynie korzystnie na nasze negocjacje z Bankierzami, tego jestem dziwnie pewna..."

Siluria podkradła się pod drzwi Wiktora i zaczęła podsłuchiwać. A tam - Laura Bankierz. I opiernicza Wiktora z góry na dół. Wiktor się skutecznie kontroluje, mimo, że Laura wyraźnie próbuje go sprowokować. Chce, by rozgorzała bitwa między Bankierzami i Wiktorem; uważa Wiktora za winnego śmierci Ozydiusza. Wiktor wytrzymał (nie zabił) i ją wywalił. Ogólnie sygnał poszedł taki, że Wiktor Sowiński chciał się pobawić portalami i uszkodził generator... i Bramę...

Wiktor zażądał opisu sytuacji przez Dagmarę. Ta powiedziała, że Bianka Stein wypaczyła klucz a Judyta Maus starła się z Dagmarą. Były tam jeszcze inne lojalistki, ale Judytę udało się Dagmarze pojmać. Wiktor powiedział, żeby Elea zajęła się Bianką i Dianą (ranne i nieprzytomne). Ale Judytę chce, by Elea obudziła.
Wiktor zażądał, by przyszła Siluria. Dagmara przyprowadziła Judytę; Wiktor zdzielił ją w twarz i rozerwał ubrania, po czym zmusił do klęknięcia.

Judyta "I ty chcesz dowodzić Świecą..."

Wiktor się wściekł. Kazał Dagmarze ją zniewolić. Ta odmówiła - nie dość czasu (Spustoszenie jest wykrywalne). Dagmara jest odesłana by zająć się i przejąć pion dowodzenia Kopalinem. Ma nie zawieść, albo "skończy jak Judyta". A Siluria ma za zadanie zrobić Judycie to, co zrobiła wcześniej Dianie (złamać).

Judyta "Nie tacy próbowali mnie złamać."
Wiktor "Ale mnie się uda."

Wiktor ucieszył się, że mają Biankę Stein. Ona się im może bardzo przydać...

# Progresja

* Gerwazy Myszeczka: przysługa u Silurii, może ściągnąć coś od Millennium przy okazji jedzenia dla viciniusów

# Streszczenie

Kompleks Centralny powoli przestaje działać przez Irytkę i ogólną panikę. Pierwsze padnie jedzenie dla viciniusów, często kluczowych dla funkcjonowania Świecy (Wiktor i Siluria się tym zajęli). Dagmara chciała wejść na EAM dzięki Kluczowi i Bramie (by Spustoszyć); Judyta i Bianka ją powstrzymały, lecz wpadły w ręce Dagmary/Wiktora. Bankierze odbili magazyn artefaktów niebezpiecznych. Power Suit Dagmary pochodzi od Oktawiana Mausa. Generatory Kompleksu są w coraz gorszym stanie.

# Zasługi

* mag: Siluria Diakon, która rozpaczliwie unikając zwrócenia uwagi ze strony Dagmary zatrzymała jej plan Spustoszenia EAM
* mag: Alicja Weiner, sympatyczna sekretarka Wiktora i krewna Diany, która nie ma pojęcia o prawdziwej naturze swojego szefa.
* mag: Wiktor Sowiński, który próbował być dobrym administratorem a nie okrutnym tyranem. Wyszło jak zwykle. Ale próbował.
* mag: Dagmara Wyjątek, która prawie dostała się na EAM przez specjalną Bramę; złapała Biankę i Judytę. Nadal wykonuje rozkazy Wiktora. Jeszcze.
* mag: Diana Weiner, miała Klucz do EAM i kiedyś zwerbowała Alicję jako sekretarkę dla Wiktora. Skończyła ranna przy Bramie po ataku Judyty.
* mag: Tomasz Kuracz, terminus Szlachty i dowódca operacji gdy Dagmary nie ma w pobliżu. Ściśle lojalny wobec Wiktora.
* mag: Bianka Stein, która wypaczyła Klucz otwierający połączenia z EAM, odcinając Świecę od EAM. Wpadła w ręce Wiktora.
* mag: Judyta Karnisz, która związała walką Dagmarę i Dianę w czasie, gdy Bianka wypaczała Klucz łączący z EAM. Wpadła w ręce Wiktora.
* mag: Wojciech Szudek, puryfikator Świecy, który rozwiązywał problemy po Skażeniu Bramy i uszkodzeniu generatorów
* mag: Oktawian Maus, co prawda nieobecny, ale okazało się, że to on stoi za dostarczeniem power suita Dagmarze...

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum 
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Sala reprezentacyjna, sala tronowa Wiktora gdzie odbywa się całość planowania i zastraszania
                            1. Pion logistyczny
                                1. Bramy, gdzie doszło do walki Judyty i Dagmary i uszkodzeniu uległa jedna z Bram.
                                1. Hydroponika, przez Irytkę i panikę zaczyna szwankować zwłaszcza w obszarze jedzenia dla viciniusów.
                                1. Energetyka, nie tylko przez Irytkę i panikę szwankuje, ale i sprzężenie zwrotne czaru Bianki (Klucz do EAM) uszkodziło generatory.
                            1. Skarbiec Kopalina
                            1. Magazyn artefaktów niebezpiecznych, odbity i odzyskany przez Bankierzy od Szlachty

# Wątki

 

# Skrypt

Dzień 1:

- Próba wejścia do EAM przez Bramę i klucz dostarczony przez Dianę
- Bankierze odbijają Magazyn Artefaktów Niebezpiecznych
- Gerwazy proponuje pojedynki terminusów o to, kto będzie kontrolował poszczególne sale
- Wiktor negocjuje skupienie energii nad badaniami Irytki
- Dagmara znika przechwycić Pion Dowodzenia

# Czas

* Dni: 1