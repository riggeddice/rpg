---
layout: inwazja-konspekt
title:  "Jak ukraść ze Świecy Zajcewów"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

# Konspekt pisany

## Kontynuacja

### Kampanijna

* [161120 - Tak wygrywa się sojuszami! (AW)](161120-tak-wygrywa-sie-sojuszami.html)

### Chronologiczna

* [160707 - Mała, szara myszka... (KB, DK, HB)](160707-mala-szara-myszka.html)

## Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.
- The Governess w masce Wandy Ketran się pojawiła. Zmusiła Blakenbauerów do wycofania się z działań w Kopalinie mordując wszystko co oni kochają.

## Punkt zerowy:

Dawno temu, Wydział Wewnętrzny założył, że kiedyś może pojawić się problem utraty kontroli nad Kompleksem Centralnym. Przygotowali portal i zawarli go w bicyklu, dobrze ukrywając i przygotowali sensowny aktywator. Bicykl trafił do dwójki starszych magów kolekcjonujących starocie. Portal w bicyklu może zostać uruchomiony przy użyciu min. syberionu.

Nie tak dawno temu, Irina Zajcew współpracowała z Ozydiuszem Bankierzem. Ozydiusza martwiło to, że dzieją się rzeczy których nie rozumiał. Irina doprowadziła do tego, że Ozydiusz aresztował 8 magów Zajcewów i ich zamknął. On współpracował z tymi magami; byli tam na wypadek problemów. Niestety, Ozydiusz zginął i następcy Ozydiusza nie wiedzieli o tym, że Zajcewowie są tam jako wsparcie. Zajcewowie FAKTYCZNIE zostali uwięzieni.

Dodatkowo, Agresta bardzo martwi sprawa ze Świecą Daemonica i znikającym KADEMem. Potrzebna mu tien Bianka Stein - katalistka zdolna do rozwikłania tej zagadki. Terminus lojalny Świecy w Kompleksie Centralnym zdecydował się na ewakuowanie Zajcewów, Bianki i Elei Maus.

No to trzeba ich wyciągnąć. Tatiana Zajcew ich pozna a oni poznają Tatianę. W związku z tym Tatiana została desygnowana jako głównodowodzącą operacją i za osobę opuszczającą z Zajcewami Kopalin.

Dodatkowo:

- Elea Maus odkryła ograniczone jeszcze, ale sprawne lekarstwo na Irytkę Sprzężoną.
- Edwin robi "Skubny stuff" by się dowiedzieć WTF IS HAPPENING między Skubny - Governess.

- Marian Agrest wykonuje ruch: wyciągnięcie kluczowych jednostek z Kompleksu Centralnego.
... Agent wewnątrz Kompleksu uwalnia wszystkich więźniów i odpala wszystkie zagrożenia. Zaczyna się piekło
... Agent wewnątrz Kompleksu ściąga Biankę Stein wraz ze wsparciem grupy Zajcewów uwięzionych w Kompleksie Centralnym
... Tatiana musi otworzyć tymczasowy portal do Kompleksu - rewire'ować ukryty zapadły potral technomantyczny Agresta
... Zajcewowie muszą wzmocnić ten portal przez zgromadzenie wycieczki szkolnej (łamiąc Maskaradę wobec dzieci)
... Potem uciekną przez portal Millennium postawiony przez Amandę (Zajcewowie i uratowane jednostki muszą być niezauważone)
... Kurtyna pod kontrolą Emilii osłania odwrót i powoduje chaos

- Regentka Spustoszenia:
... ma bloodcurse na uciekających magach
... nie-ewakuowani magowie zginą (bo przestępcy)

## Misja właściwa:

Dzień 1:

"Co kojarzy się zwykle z Blakenbauerami?" - Mojra
"Płaszczki" - Hektor
"A grupa psychopatycznych Zajcewowych przestępców zamkniętych za straszne czyny?" - Mojra
"ABSOLUTNIE NIE!" - Hektor

"Lepiej już odejdź, zanim dojdziesz do uwalniania paskudnych Zajcewskich przestępców" - Mojra do załamanego Hektora.

"Dionizy, Alina... zejdźcie do podziemi. Klara wam wszystko wyjaśni. Albo Mojra. Nieważne. Jeden pieron." - pijany Hektor do sił specjalnych.
"I żadnych raportów do mnie..." - całkowicie zrezygnowany Hektor do Aliny.

"Profesorze... szykuje się coś, o czym wolałbym nie wiedzieć..." - Hektor, chwilę przed wymazaniem pamięci.

"Kto dowodzi operacją?" - Dionizy
"Ty. Wykazałeś proaktywność" - Mojra

"Ło, ale bohaterowie... nawaleni Zajcewowie zabijaki..." - Dionizy do Mojry, o portalach

Plan Dionizego: iść zneutralizować magów, potem się rozdzielić, potem odpalić portal. 
Plan Aliny: zamiast wyłączać system, spróbować go przechwycić. Może to tańszy model Krówków, ale pomoże.

Biorą ciuchy dla Zajcewów. Dresy. Dla całej spodziewanej grupy. 
Dystans z adhocowego portalu Zajcewów do portalu Millennium to jakieś 30 minut szybkim marszu.
Klara przygotowała 2 różne samochody osobowe mające skutecznie służyć jako dywersja - że niby tak ewakuują Zajcewów. Z autopilotem.
Alina i Dionizy zrobili rekonesans kanałów i okolicy przy domku Mecharośl.
Wrażenie: niskomagiczny teren o niskiej ważności. Tak, Alina zauważyła przelatującą dronę (typu zgodnego ze Spustoszeniem).
Kino jest znacznie bliżej kanałów niż domek. Czyli trzeba Zajcewów zbierać w kinie.
Dionizy i Alina zostawili w kanałach kilka potykaczy, rac... rzeczy, które mogą im się przydać na następny dzień.

Dzień 2:

Akcja przeznaczona jest odpowiednio rano. Przeszli koło "chłopca rozdającego Metro" i zagadali do Tatiany. Po czym weszli do garażu i spotkali się z systemem Krówków.
Pierwsza rzecz do pokonania: trzeba się przekraść do poziomu, gdy Dionizy ma możliwość wpłynąć na działanie systemu. (17,18 v 15 = F, S). Dionizy niezauważenie dociera na miejsce, jednak Alina musiała odpowiedzieć na za dużo pytań. 

Dionizy musi shackować system (18). Alina musi odpowiedzieć na wszystkie pytania (15).

Udało im się przehackować system, ale system w ostatnim strzale zdążył rzucić im repel spell. Alina się opanowała, ale Dionizy wpadł pod wpływ czaru strachu. Alina go złapała i spoliczkowała mówiąc, że przecież zawsze tak mówił i tak mu zależało... dała radę go opanować. Dionizy się zawstydził, z zaczerwienioną mordą, ale opanował i wziął się w garść.

Weszli przez garaż. Państwo Drożdżowscy oglądają telewizję w salonie. A za telewizorem właśnie znajduje się konsoleta systemu głównego; trzeba odciągnąć ich uwagę. Niespecjalnie obserwują sytuację. Dionizy się przekradł i wlał im środka przeczyszczającego; zadziałało. I potem Dionizy włamał się do systemu i go przechwycił zanim ktokolwiek dał radę się zorientować.

Dionizy zaszedł maga, Alina czarodziejkę. Sznur, knebel, pałka teleskopowa... i magowie są niegroźni i nieprzytomni. Pobrali próbki krwi państwa Drożdżowskich i przygotowali się do tego, by wprowadzić Tatianę i przepiąć system na nią. Poczekali aż żadna drona nie lata w okolicy, po czym wprowadzili Tatianę.
Przekonali ją, by dostarczyła trochę swojej krwi by przepiąć kontrolę na nią. Ale Dionizy nie jest chętny do hackowania ponownie... no trudno. Musiał spróbować. Nie wyszło - Tatiana rozkaszlała się krwią, ale udało się to osiągnąć Dionizemu w czas. System jest Tatiany. Choć Tania jest osłabiona i w złej formie.

Tatiana powiedziała, że musi się zsynchronizować z Syberią w kinie. Ma wprowadzać wszystkich do kina. 
Rozdzielili się - Dionizy poszedł do kina a Alina została osłaniać Tatianę. Tania ostrzegła Dionizego - tylko niech nie zwróci uwagi dzieci, bo Syberion.

Syberia wkroczyła do kina i do domku Mecharośl. Tatiana otworzyła portal. 

Przeszło 8 Zajcewów przez portal i przeniosło 2 kobiety. Ale żadną z nich nie jest Elea Maus. Zapytani, krzyknęli, że Elea kolaboruje a agent nie żyje. Tatiana zareagowała strachem, zwłaszcza, jak w oknie pojawił się mechaniczny terminus (i wystrzelił weń system Krówków). Alina kazała Tani zamknąć portal (zanim zmaterializował się Koszmar) i przeszły przez portal do kina. 

"Dobra, ekipa, rozbierać się!" - Zajcew do swoich
"Ale tu są dzieci!" - Dionizy
"One nie muszą." - Zajcew

"Kto tu dowodzi?" - Zajcew w eter
"Ja" - Dionizy
"Masz plan?" - Zajcew
"Tak" - Dionizy
"Idziemy" - Zajcew.

Ósemka golutkich i poranionych Zajcewów biegnąca do kanałów, z jedną nieprzytomną Bianką na rękach i ciągnących Wiolettę to niepowtarzalny widok.

Niestety, dzieci zostały Skażone syberionem (potrzebne do portalu). A Elei nie ma. Nie ma lekarza; ludzie tu najpewniej umrą, ku rozpaczy Tatiany. Niestety, misja przede wszystkim.
W kanałach Dionizy udowodnił, że jest najlepszym szczurem kanałowym w historii Inwazji i przeprowadził Zajcewów w dresach w sposób niewykrywalny gdy siły Wiktora Sowińskiego (czy Dagmary Wyjątek?) skupiały się na polowaniu na samochody-dywersje i na potyczkach z Kurtyną i Millennium.

Dionizy dał radę wycofać wszystkich zanim Druga Strona zorientowała się co w ogóle się stało.

I nikt niewłaściwy nie podejrzewa działania Blakenbauerów.

# Progresja

* Andrea Wilgacz: dostała dostęp do komando 8 Zajcewów dowodzonego przez Tatianę Zajcew
* Andrea Wilgacz: dostała Wiolettę Bankierz pod komendę; wraz z wiedzą o tym co dzieje się w Kompleksie Centralnym
* Andrea Wilgacz: dostała dostęp do Bianki Stein, kompetentnej katalistki i eksperta od portali

## Frakcji

* Blakenbauerowie: ocieplenie relacji z lojalistami Świecy, z Millennium, z Zajcewami Iriny.

# Streszczenie

Elea Maus odkryła wstępne lekarstwo przeciwko Irytce Sprzężonej. Marian Agrest skoordynował akcję - ośmiu Zajcewów (Zajcewkommando) zostało uwolnionych przez agenta w Świecy i on wywołał chaos. Niestety, Elea metaforycznie wbiła mu nóż w plecy i Dagmara Wyjątek go zabiła. Zajcewowie uciekli przez specjalny, ad-hocowy portal Świecy dzięki Tatianie, po czym Blakenbauerowie ich wycofali kanałami i przez portal Millenium przerzucili poza Kopalin. W Kompleksie Centralnym jest chaos a Andrea dostała dostęp do 8 wykwalifikowanych Zajcewów, Wioletty Bankierz (która wie co się dzieje w Kompleksie) oraz katalistki Bianki Stein - eksperta od portali i energii magicznych. Blackenbauerowie dostali obietnicę odbicia Patrycji i odepchnięcia The Governess.

# Zasługi

* mag: Hektor Blakenbauer, który zdecydował się pomóc, ale wolał 'nie wiedzieć' i który przekonał Emilię aby wysłała pomoc Skażonym syberionem dzieciom w kinie
* mag: Klara Blakenbauer, która skonstruowała samochody - wabiki - dywersję by kupić czas Dionizemu i Alinie.
* vic: Dionizy Kret, który zhakował system Krówków, oddał kontrolę nad nim Tatianie, po czym szybko i sprawnie przeprowadził Ewakuację. Uparcie walczył z przeznaczeniem.
* vic: Alina Bednarz, która otrząsnęła Dionizego i Tatianę, gdy było trzeba i - dla odmiany - miała tylko jedną fałszywą tożsamość. Poinformowała Hektora o potrzebie pomocy w kinie.
* mag: Marian Agrest, którego nie było na misji, lecz skoordynował działania Blakenbauerów, Millennium, lojalistów Świecy, Kurtyny i Zajcewów.
* mag: Mojra, która dowodziła operacją z ramienia Blakenbauerów i bawiła się w ciemiężenie Hektora "stracić Patrycję czy uwolnić przestępców".
* mag: Bianka Stein, katalistka i specjalistka od portali i energii * magicznych, ewakuowana przez Zajcewów a potem Blakenbauerów poza Kopalin.
* mag: Wioletta Bankierz, faktycznie porwana przez Zajcewów. Świadek tego, co dzieje się w Kompleksie Centralnym; zaseedowana przez Silurię ważnymi wiadomościami. Ewakuowana poza Kopalin.
* mag: Tatiana Zajcew, ochotniczka wyciągająca Zajcewkommando przez ad-hocowy portal. Oddała dowodzenie Dionizemu, zgodnie z planem do momentu ewakuacji. Poza Kopalinem.
* mag: Elea Maus, która odkryła lekarstwo na Irytkę. Gdy agenci Świecy chcieli ją ewakuować przez portal, zdradziła i została z Wiktorem Sowińskim.
* mag: Gerwazy Myszeczka, terminus, który zginął próbując wycofać Zajcewów i Eleę ("chronił" więzienie w tym czasie). KIA.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji, gdzie mieści się centrum dowodzenia Mojry
                    1. Dzielnica Trzech Myszy
                        1. Ulica Czarnobrodego, miejsce gdzie mieszka bardzo niewielu magów i które jest bardzo mało istotne
                            1. Kino sceniczne Pasja, gdzie doszło do Skażenia syberionem grupy dzieci by odpalić ad-hocowy portal do Kompleksu  Centralnego
                            1. Domek Mecharośl, gdzie znajdował się ukryty w bicyklu portal ad-hoc do Kompleksu Centralnego Świecy
                        1. Kanały, gdzie Blackenauerowie zbudowali sieć ewakuacji
                    1. Dzielnica Owadów
                        1. Szpital Pasteura, ufortyfikowany, gdzie mieści się portal Millennium prowadzący poza Kopalin
                        1. Kanały, gdzie Blackenauerowie zbudowali sieć ewakuacji
   
# Wątki

- Spustoszenie

# Skrypt

- Elea Maus odkryła ograniczone jeszcze, ale sprawne lekarstwo na Irytkę Sprzężoną.
- Edwin robi "Skubny stuff" by się dowiedzieć WTF IS HAPPENING między Skubny - Governess.

- Marian Agrest wykonuje ruch: wyciągnięcie kluczowych jednostek z Kompleksu Centralnego.
... Agent wewnątrz Kompleksu uwalnia wszystkich więźniów i odpala wszystkie zagrożenia. Zaczyna się piekło
... Agent wewnątrz Kompleksu ściąga Biankę Stein wraz ze wsparciem grupy Zajcewów uwięzionych w Kompleksie Centralnym
... Tatiana musi otworzyć tymczasowy portal do Kompleksu - rewire'ować ukryty zapadły potral technomantyczny Agresta
... Zajcewowie muszą wzmocnić ten portal przez zgromadzenie wycieczki szkolnej (łamiąc Maskaradę wobec dzieci)
... Potem uciekną przez portal Millennium postawiony przez Amandę (Zajcewowie i uratowane jednostki muszą być niezauważone)
... Kurtyna pod kontrolą Emilii osłania odwrót i powoduje chaos

- Regentka Spustoszenia:
... ma bloodcurse na uciekających magach
... nie-ewakuowani magowie zginą (bo przestępcy)


Drzewo decyzyjne (S/P):

1S) Dostanie się do domku dwóch magów z kolekcją technomantycznych rupieci (silne zabezpieczenia magiczne: stealth # hack # act).
1P) Wejście w walkę z magami; potencjalne ściągnięcie mechanicznego terminusa LUB śmierci magów.
1SS) Wyłączenie zabezpieczeń (hack # diversion # illusion)
1SP) Spowolnienie; -1 Zajcew
1SSS) Wciągnięcie dyskretne Tatiany
1SSP) Starcie z Drożdżowskimi
1SSSS) Wyłączenie Drożdżowskich 
1SSSP) -1 Zajcew
1SSSSS) Ochrona Tatiany przed mechanicznym terminusem
1SSSSP) Tatiana jest ranna; przerwane połączenie, -1 Zajcew
1SSSSSS) Ochrona dzieci przed koszmarem
1SSSSSP) Kilka dzieci nie żyje, więcej mechanicznych terminusów ściąga
for(P) reduce Zajcew (init: 8).
PP = śmierć kolejnego agenta w Świecy

2) Dyskretna ewakuacja agentów (w czym nieprzytomnej Wioletty i rannej Bianki) do rendezvous point
- 5 konfliktów.
- dla 2 porażek: portal Amandy zostaje zniszczony

# Czas

* Dni: 2