---
layout: inwazja-konspekt
title:  "Kaczuszka w servarze"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170501 - Streamerka w "Na Świeczniku" (Kić (temp), Til (temp), Viv (temp))](170501-streamerka-w-na-swieczniku.html)

### Chronologiczna

* [140508 - "Lord Jonatan" (AW)](140508-lord-jonatan.html)

## Wątki

- Powrót Mausów
- Aegis czy EIS?

## Punkt zerowy:

Karolina Maus szuka śmierci. Sformatowała Bogumiła Rojowca tak, by ten ją zabił.

Ż: W jaki sposób młody mag KADEMu (Rojowiec) znalazł przedsiębiorcę Roberta Kolca?
B: Z ogłoszenia w Pakcie.
Ż: Dlaczego Pakt opisywał życie zwykłego człowieka?
K: Ponieważ artykuł został napisany przez czarodziejkę, która była ludzistką.
Ż: Dlaczego Karolina Maus wybrała właśnie Bogumiła Rojowca (syna lorda terminusa SŚ)?
B: Nie wybrała. Ten, którego wybrała okazał się być niedostępny, więc wybrała tego konkretnego.
Ż: Dlaczego Mariusz Trzosik którego wybrała okazał się być niedostępny?
K: Bo potrzebowała czegoś co zrobi duże pierdut, a był niedostępny bo zaszył się w Instytucie i współpracował z Aleksandrą.
Ż: Co bardzo dziwnego powiedział Janek co naprowadziło Silurię i Pawła na dziwną sprawę?
B: "Hmm... ciekawe."
Ż: Dlaczego na pozornie niegroźną akcję Siluria dostała autoryzację GS Aegis 0002?
K: Z uwagi na to, że Bogumił Rojowiec jest synem lorda terminusa i może mieć do dyspozycji ogromną siłę ognia.
Ż: Jedna osoba, której naprawdę zależy na czarodziejce Srebrnej Świecy (Karolinie Maus)?
B: Mariusz Trzosik, szef Instytutu Zniszczenia KADEMu.
Ż: Nad jaką chorą, eksperymentalną bronią Janka ostatnio Rojowiec pracował?
K: Emiter przekierowujący energię magiczną ofiary w transformację w małą, nielotną kaczuszkę.

## Misja właściwa:

### Faza 1: Zniknięcie Bogumiła

"Ciekawe" - powiedział Janek. Zniknął jego Kaczkoryzator. Działo zmieniające maga (i tylko maga) w małą, niegroźną kaczuszkę. W małe kaczątka. To działo zostało stworzone na bazie tego, co wymyśliła Weronika Seton, gdy dawno temu zmieniła Norberta w słodkiego małego kotka.
Paweł westchnął. Poszedł szukać Bogumiła Rojowca. Na poligonie.
Znalazł ślady na poligonie. Włączył skan perymetru i zauważył, że poligon został opuszczony przez servar.
...co?
I jest tam jeszcze jedna kaczka więcej niż powinna być. I jeden gorathaul mniej.
Oki, Paweł spróbował odnieść kaczuszkę do containment chamber (pilnując działa plazmowego) i, niestety, gorathaul zaczął się retransformować.
Paweł usunął problem działem plazmowym.

Paweł poszedł porozmawiać z Jankiem. Faktycznie, servar pochodzi z Instytutu Technomancji. Ten sam Bogumił go wyniósł. Niestety, servar nie ma radia (nie zmieściło się) a transponder został wyłączony. Co gorsza, servar ma reaktor typu GS Aegis, czyli wysadzenie go zrobi Duże Bum.

Paweł z Silurią poszli porozmawiać z Martą. Ta ich autoryzowała do użycia GS Aegis 0002. Powiedziała, że trzeba znaleźć Bogumiła i użyć działa strumieniowego do zniszczenia tego servara.

Siluria zdecydowała się zsynchronizować z GS Aegis 0002. Systemy Aegis się obudziły. Siluria poczuła potężny intelekt, by po chwili przejąć kontrolę nad GS Aegis 0002. Po krótkim briefingu, Aegis zażądała działa typu swarm. Uzasadniła tym, że potrzebuje działa swarm by móc jakąś eksplozją uszkodzić Kaczkoryzator; przy odrobinie szczęścia tamten wybuchnie i promień porazi Bogumiła. Efektywnie wyłączając servar.
Siluria zażądała paczki danych o działaniu Aegis; jednak synchronizacja się rozpadła i Siluria zemdlała.

Aegis wykorzystała pretekst o konieczności znalezienia servara i poszła do konsoli. Paweł szybko obudził Silurię używając elektrowstrząsów i Siluria w ostatnim momencie zatrzymała głowicę. Jak dla Zespołu, Aegis jest nadzwyczaj autonomiczna i wykazuje bardzo dużą inicjatywę. I chyba sobie kpi z Silurii.

Ok. Siluria ma dość. Poszła do Norberta poprosić o coś co odróżnia "sytuację krytyczną" w jakiej Aegis może wykazywać inicjatywę od sytuacji niekrytycznej gdzie Aegis ma nie wykazać inicjatywy i słuchać. Dostała kryształ Quark. Siluria wprowadziła go do Aegis. Głowica przyjęła do wiadomości nowe instrukcje.

Zanim jeszcze wyjechali, Siluria pochodziła po KADEMie. Dowiedzieć się czegoś o Bogumile. I tak natrafiła na Jadwigę, jedną z praktykantek. Cichą adoratorkę Bogumiła. Ta powiedziała Silurii dość dużo ciekawych informacji.
- Bogumił jest synem lorda terminusa Rojowca, prawej ręki Andrzeja Sowińskiego. Nie chciał mówić czemu nie jest na Świecy.
- Ostatnimi czasy Bogumił zamykał się w pokoju i najpewniej medytował. To co ciekawe, jego moc magiczna zdecydowanie ostatnimi czasy wzrosła.
- Jeśli się z kimś nowym spotykał, to nie na KADEMie. Ale zainteresowania... tak, było coś. Jak "trwale zabić technomantę". Nie po prostu "zabić". Jak unicestwić ostatecznie technomantę. I "jak skontrować technomantę".
- (Siluria sprawdziła; KADEM nie pracuje nad niczym takim)

Ignat zawołał Silurię. Poprosił ją szybko o przyjście; w pokoju Bogumiła jest grupa artefaktów które jednocześnie są artefaktem zbiorowym. Każdy z nich indywidualnie zwiększa moc, ale grupowo robią coś jeszcze; pracuje nad tym właśnie Quasar.

Quasar pracuje nad artefaktem kombinowanym. Coś nie działa. Brakuje jednego elementu artefaktu; najprawdopodobniej czegoś na głowę. Quasar powiedziała, że konstrukcja tego złożonego artefaktu jest zbliżona do konstrukcji EIS, co oznacza, że twórca najprawdopodobniej studiował na EAM. A jeśli nie studiował na EAM, studiował u osoby, która stworzyła EIS. Innymi słowy, Quasar zakłada, że ktokolwiek stworzył te kombinowane artefakty jest osobą która pochodzi z EAM lub studiowała pod Aurelią Maus.

Przy tak zdefiniowanych informacjach, Zespół wszedł do samochodu i pojechali z Aegis która spróbowała zlokalizować servar swoim pasywnym radarem. Głowica sygnał łapie i gubi, ale zdecydowanie kieruje wszystkich do Kotów...

### Faza 2: Bunkier Karoliny w Kotach

Dotarli do Kotów. GS Aegis 0002 szuka, ale nie potrafi znaleźć servara. Po obejściu Kotów głowica stwierdziła, że nie jest w stanie znaleźć servara. Gdzieś w Kotach jest servar i reaktor jest wyłączony. Jest też jeden mag najpewniej pod wpływem artefaktu kombinowanego. Zdaniem Aegis, servar jest schowany za lapisem.

Siluria poszła do Kotów kupić mapę. Znaleźć miejsca, gdzie może się ukryć servar. Znalazła kilka miejsc:
1. kościół w Kotach 
2. mechanik samochodowy (z garażem)
3. remiza strażacka
4. bunkier
5. stodoły różnego typu

Bogumił Rojowiec nie wie o istnieniu GS Aegis. Nie jest powszechną wiedzą to czym głowica jest i co ona robi. Siluria kazała Aegis się zamaskować jako losowa dziewczyna, po czym wrócili do Kotów.
Siluria i Paweł włączyli auto i przejechali się po okolicy z włączonym VISORem. Pawłowi udało się zlokalizować emanację pola magicznego z okolic bunka w Kotach. Linia silnie technomantyczna (spójna z potencjalnym servarem).

No dobrze. Być może jest tam Rojowiec. Może założył pułapki. Pytanie tylko... czy tam faktycznie jest, na czym oni stoją. W związku z tym - plan. Paweł decyduje się uciekać przed VISORem dookoła bunkra a VISOR jest podłużnym czymś ze szmatą na wierzchu. Mają nadzieję, że dzięki temu odkryją, czy ktoś jest w bunkrze.

Sukces Pawła i sukces Karoliny. Paweł zorientował się z analizy wzoru VISORa, że ma do czynienia z magiem INNYM niż Bogumił Rojowiec. Owego maga nie ma w tej chwili w bunkrze, jednak ustawił repulsory (ludzie nie chcą się tu za bardzo zbliżać). Nie ma tam servara i wyraźnie ktoś tam już się znajdował zanim Bogumił opuścił KADEM. Karolina natomiast wykryła Pawła, Silurię, GS Aegis i VISOR (wykryła Aegis jako osobę) i założyła, że ma do czynienia z trzema magami KADEMu.

Siluria zostawiła swoją wizytówkę koło bunkra z nadzieją, że ktoś się z nią skontaktuje. Następnie idzie szukać w dalszych PoI gdzie może znajdować się servar...

Siluria skupiła się więc na samym Rojowcu. Hipoteza jest taka, że Rojowiec poluje na tego technomantę w Kotach (bo czemu szukał info jak zabić technomantę i czemu zwinął servar). Fakt jest taki, że Rojowiec ma kompleksy na punkcie ojca i miał wychowanie wojskowe. 
Artefakt wpływający na Rojowca, ten złożony, jest artefaktem stworzonym przy użyciu magii i technologii powiązanej z EAM. Jest to coś technomantycznego...

Niestety, nie są w stanie znaleźć niczego powiązanego z Rojowcem.

Magowie KADEMu siedzą niedaleko bunkra. Siluria z Aegis w miarę schowana, Paweł daje się znaleźć przy bunkrze. Czekają na wypadek jakby się pojawił mag (technomanta).

I technomantka się pojawiła. Słania się lekko na nogach, niedawno płakała, jest nieco "out of this world". Siluria do niej podeszła i zdecydowała się zagadać. Technomantka prawie zaatakowała, ale się zatrzymała. Gdy Siluria się przedstawiła jako "Siluria Diakon, KADEM" rozpoznała "Er-lord Urbanek". Siluria zdecydowała się na to, by sobie zaskarbić sympatię wyraźnie zmasakrowanej życiem technomantki (która przedstawiła się jako Karolina Maus). Udało jej się; #2 lasting bonus.

Siluria uderzyła do Karoliny; czy ta widziała jej przyjaciela, bo szukają. Karolina spytała, czy jeśli go znajdą to sobie pójdą? Tak. Karolina otworzyła bunker i poprosiła Silurię do środka ("chodź"). Siluria, po chwili wahania weszła do środka. Karolina odpaliła wszystkie elementy swoich konsol i dron, po czym zabrała się do przeszukiwań terenu i archiwalnych zdjęć. W tym czasie Siluria skupiła się na oglądaniu wnętrza bunkra; wyszło jej na to, że ten bunkier zamieszkiwany jest od co najmniej dwóch tygodni a osoba mieszkająca tu jest w głębokiej depresji. I co najmniej raz już próbowała popełnić samobójstwo.

Siluria znalazła też rzeczy wskazujące na to, że Karolina MOGŁA stać za całą tą sprawą; są tam różnego rodzaju plany artefaktów kombinowanych... 
Oczywiście, bez przegrzebania dokumentów nie ma na to żadnych dowodów.

Karolina jej przerwała i pokazała jej, że niedaleko Kotów, w lesie gdzie jest "kult kralotha" z pewnością chował się servar. Zgodnie z jej danymi, servar tam został i powinien się dalej znajdować.

### Faza 3: Kaczkoryzator

Siluria opuściła Karolinę i poszła wraz z Pawłem (i Aegis) do miejsca śmierci Lugardhaira. Niestety, nie dało się tego sensownie przeskanować, więc puścili Aegis przodem. Głowica wróciła i powiedziała, że servar tam jest; właśnie się uruchomił.

Świetnie.

Paweł i Siluria mają element zaskoczenia, ale nie chcą zabić Rojowca (zniszczyć servara).

Paweł skontaktował się z Quasar prosząc o update w sprawie artefaktu który ona bada. Quasar powiedziała, że artefakt o którym mówią jest artefaktem posiadającym centralny system sterowania, który nie może być zbyt daleko. 
Siluria podejrzewa, że wie, gdzie jest centrum dowodzenia - w bunkrze. Quasar autoryzowała Pawła do użycia broni ciężkiej.

...ciężkiej?

Korzystając z okazji, Paweł zaproponował... kaczkoryzację.
Servar nie jest typu self-activator i jest naładowany. Pancerz nie jest dość ciężki, by chronić przed kaczkoryzatorem. Bogumił nie wie, że na dole jest GS Aegis 0002. Głowica jest w trybie kameleona i może wyrwać kaczkoryzator servarowi i strzelić.

Innymi słowy, można skaczkoryzować Bogumiła. To doprowadzi do otwarcia się servara.

I dokładnie to zrobił KADEM. Bogumił skończył jako mała, paranoiczna kaczuszka do naprawienia przez magów KADEMu potem (biedny Norbert). To spowodowało, że Siluria i Paweł odzyskali:
- jednego sprawnego servara
- jeden sprawny element artefaktu złożonego
- jednego skaczkoryzowanego Bogumiła Rojowca
- jeden sprawny, przetestowany kaczkoryzator

I tu się pojawia ciekawy problem. Siluria ma silne podejrzenia, że za tym wszystkim stała Karolina Maus, ale nie ma dowodów. Dodatkowo, jakkolwiek Siluria chciałaby wrócić do Karoliny Maus (czemu ona im powiedziała prawdę jeśli za tym stoi?!), Paweł naciskał by jednak wrócić na KADEM. Niechętnie, Siluria się zgodziła. Wzięli wszystko ze sobą, wsiedli do samochodu i ruszyli na KADEM. Omijając bunkier, gdzie schroniła się Karolina.

Już w drodze powrotnej Siluria dostała SMSa od Karoliny "Er-lord Urbanek tego tak by nie zostawił. Wrzuciłby granat do bunkra". Lekko zagotowała się w niej krew i odpowiedziała mniej więcej tak:

"KADEM to nie tylko Urbanek tak jak Świeca to nie tylko Mausowie i Karradrael. Mając do wyboru dobro kolegi i głupią zemstę wybór może być tylko jeden. Nie rozumiem tylko czemu pomogłaś. Tak bardzo boisz się Urbanka?"

W odpowiedzi na to Karolina, już i tak bardzo niestabilna, spróbowała ponownie podciąć sobie żyły. Oczywiście, bez powodzenia - ona sama nie umie się zabić.

Magowie KADEMu dotarli na KADEM i zostawili tam jedną nieszczęśliwą kaczuszkę i resztę fantów. Następnie po namowach Silurii zdecydowali się wrócić do Kotów, do bunkra. A tam zastali nieznanego sobie maga (Oktawian Maus) próbujący dostać się do bunkra; Karolina nie pozwala mu wejść. Paweł otworzył bunkier i Siluria została świadkiem Karoliny, która nie umie się zabić, choć próbuje. Zrozumiała wtedy, że cała ta intryga była tylko po to, by ktoś Karolinę zabił.

Oktawian tuli mocno Karolinę i ją uspokaja (relacja: bardziej ojciec-córka, nie kochankowie). Zapytany przez Silurię dlaczego Karolina pozwoliła Rojowcowi odejść i im pomogła powiedział ostro, że nie bała się Urbanka. Po prostu zrobiło jej się Rojowca żal...

Siluria i Paweł nie mieli wyjścia. Opuścili bunkier i wrócili na KADEM...

# Streszczenie

Pierwsze użycie GS Aegis 0002; zniknął Bogumił Rojowiec (mag KADEMu w cieniu ojca) który ostatnio zaczął interesować się sposobami trwałego kontrowania i zabicia technomanty. Idąc jego śladem, w Kotach, Siluria i Paweł znaleźli Karolinę Maus - czarodziejkę, która chce umrzeć ale ma blokadę i która zmusiła Rojowca zdolnościami klasy EAM by ten ją zabił. Plan Karoliny został powstrzymany przez kaczkoryzator Janka; małe kaczuszki kiepsko obsługują groźne servary. Siluria i Paweł byli świadkami, jak Oktawian Maus tuli Karolinę i próbuje ją uspokoić. Czarodziejka jest niestabilna i nie chce żyć, ale nie może się zniszczyć. Cierpi na syndrom ghoula - Aurelia/Karolina.

# Zasługi

* mag: Siluria Diakon, dzięki której Karolinie zrobiło się żal Bogumiła Rojowca.
* mag: Paweł Sępiak, który zaproponował użycie kaczkoryzatora do bezstratniego odzyskania i uratowania Bogumiła Rojowca.
* mag: Jan Wątły, który zaalarmował Pawła, że Bogumił Rojowiec zwinął kaczkoryzator i zniknął. Też: współtwórca kaczkoryzatora. DLACZEGO!
* mag: Quasar, która błyskawicznie szybko złamała złożony artefakt wykorzystany przez Karolinę; wykryła podobieństwo do wiedzy EAM.
* mag: Bogumił Rojowiec, praktykant KADEMu dążący do maksymalizacji mocy, w cieniu ojca, wpadł w pułapkę Karoliny Maus i zagroził swemu pobytowi na KADEMie.
* vic: GS Aegis 0002, bardzo przydatna głowica, która zachowuje się, jakby była odrobinę złośliwa i szukała granic.
* mag: Karolina Maus, bardzo uzdolniona czarodziejka z objawami depresji, która próbuje popełnić samobójstwo cudzymi rękami.
* mag: Oktawian Maus, który wyraźnie martwi się o Karolinę i odciął KADEM od możliwości przesłuchania Karoliny bez eskalacji konfliktu.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                            1. Instytut Technomancji
                    1. Las Stu Kotów 
                        1. Poligon KADEMu
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Kościół Romański
                        1. Dom Kultury
                        1. Mechanik Samochodowy, z garażem
                        1. Remiza Strażacka
                    1. Las Stu Kotów
                        1. Bunkier
                        1. Grobowiec Lugardhaira

# Czas: 

* Opóźnienie: 10
* Dni: 2
