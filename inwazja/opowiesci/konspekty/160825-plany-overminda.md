---
layout: inwazja-konspekt
title:  "Plany Overminda"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170101 - Patrol? Kralotyczne maskowanie (AW)](170101-patrol-kralotyczne-maskowanie.html)

### Chronologiczna

* [160819 - Oblicze Guwernantki (HB, KB, AB, DK)](160819-oblicze-guwernantki.html)

## Kontekst ogólny sytuacji

The computer found in The Governess' lair had some vital information. From the computer, Clara was able to deduce several interesting data points. 

One of them is the fact, that there exists a line between the Central Compound of the Silver Candle and some other spots. Some constructions are being made on that line. It’s going to be quite nefarious – so it is time to stop it.

A second data point – there exists a special place in located deep in the Blutwurst territory. The only reference to that is “Alexandria”. Like the library of old. This is a main point of control – of adaptation – for the governess. Disabling or destroying the Alexandria could really harm our opponent and their abilities to adapt so quickly.

A further scary point would be “patient zero”. The lousy information located in the computer insinuate, that there is a new weapon – a new biological weapon – created by the governess or the forces a buffer and destined to rain plague upon pull magi which stand in the governess way. Finding the patient zero might allowing our team to protect or disable this particular disease before it starts.

And the last one – logistics. Those computers contain a lot of information; partial information – on basic movements of the truck fleets and other movements. It would be possible for our magi to create a model and try to disrupt the logistics and communication operation of the enemy. This might lead to the headquarters of enemy force. However, a greater gain might be to actually damage the incredible logistics our opponent has access to.

But in five days The Governess is going to modify all those data points of fire all those data points. Will our heroes manage? Will they stop the destiny? Will they prevail? We shall see.

## Punkt zerowy:

Decydujemy się zaatakować Aleksandrię oraz Pacjenta Zero.

## Misja właściwa:

Dzień 1:

**Początek**

Komputery Karoliny zawierają poniszczone, acz wciąż dostępne informacje na następujące tematy (w 3 tierach; 3-3-5):
- Pacjent Zero
- Aleksandria
- Budowa Linii
- Logistyka
- Anna Myszeczka
- Poszukiwania Karoliny
- Karolina
- Bazy Karoliny
- Aktualne operacje

Klara, Dionizy i Hektor, Alina naprawdę ostro pracowali by jak najwięcej dowiedzieć się o powyższych. Klara poprosiła Edwina i Marcelina o biologiczne wzmocnienie organizmu; chce mieć szybsze myślenie i przetwarzanie informacji, ale i uzupełnianie luk. Potem Klara ma zamiar się zintegrować z tymi komputerami - niech one staną się jej częścią (+9). Klara też zdecydowała się użyć swojej dawnej wiedzy, z czasów Weinerów. Projekt "Aleksandria". Organiczny komputer, składający się z grupy magów i ludzi - powiązanych siecią. Klara postawiła prostą "Aleksandrię"; niebezpieczną, acz potrzebną. Będzie próbowała odbudować komputery cały dzień.
Wraz z działaniem "Klareksandrii", Hektor dostawał dodatkowe informacje o które mógł dopytywać i dzwonić po swoich kontaktach - w bardziej lub mniej jawny sposób.

Od razu Klarze rzuciły się w oczy podobieństwa - "Aleksandria" złożona przez Klarę wygląda jak osłabiona forma "Aleksandrii" złożonej przez Karolinę Maus. Wygląda to tak, jakby Karolina wzięła plany i projekty Weinerów (jak kiedyś Klary) i usprawniła je używając Spustoszenia oraz Magii Krwi. Pytanie - skąd wiedziała? Skąd miała dostęp? Jakim cudem nawet genialna Karolina potrafiła to tak szybko zmienić? Czyżby przeżył ktoś poza Klarą ze starej grupy i starego rodu...? I czy współpracuje z Karoliną...?
"Aleksandria" zlokalizowana jest w Okólniczu, w pałacyku Blutwurstów. Jest całkowicie nieruchoma, zwłaszcza, jeśli jest naprawdę duża.

Hektor skontaktował się z Emilią. Wydobył od niej co tylko mógł. A potem skontaktował się z Szymonem Skubnym; niestety ten jest poza Kopalinem. Potem z Amandą Diakon z Millennium... by znaleźć jak najwięcej informacji.

I znalazł.

**Pacjent Zero:**

The patient zero is something which seems to be from before “Radom Accord”. The Governess had infected a mage with some kind of the blood frenzy: the blood thirst. She combined the natural blood frenzy from the dark magic with the special disease transmission which comes from the Desolation. In short, if a mage casts a spell next to the patient zero - he will probably get infected with the very same disease. The Governess cannot control this one. She simply locked the victim and put several guards; they are supposed to make sure the patient will not get out. Think: zombie apocalypse. The patient zero is located somewhere in Koty city.

**Aleksandria:**

In the past, Klara used to be a Weiner mage. They have created the Alexandria – a biological supercomputer; a link between humans and Magi which improves their own abilities and merges them into one network. However, something went very wrong and their circle got broken.
The Governess seems to know that idea; she has managed to take it and strengthen it. Somewhere in the Blutwurst Palace lies her version of the Alexandria project. It is about 100 people and magi, linked using Desolation and powered by the Blood magic. A much superior version in terms of efficiency, this one gives the governess and her overmind unprecedented adaptation speed and scientific knowledge.
This actually explains why one of their goals was to get into the EAM. If the overmind managed to get into the EAM and link those gifted magi with the Alexandria, by the sheer knowledge it would be almost unstoppable.
One of the key researchers from the computers – the Blutwurst Palace is almost impossible to break in and it actually is impossible to break in silently. However, The Governess had created several entities which has to go into the palace and out of the palace. This means that Margaret is able to transpose those masks – those detection masks – onto people entering the Palace.

**Konstrukcja:**

The only thing we know is the fact, that there exists a construction of some kind. This going to be a line to amplify the teleport spells; it has some kind of energy sources which are at this point unknown and the direction of teleportation is also unknown. All we know is that it is the teleport line between the Central Complex of Silver Candle and an unknown point which can be extrapolated from the line.

**Anna Myszeczka:**

This sorceress proved to be an innocent victim of The Governess. The Governess has managed to take hold on the sorceress in such a way, that she was able to actually take her children and manipulate her into doing whatever The Governess wanted. Then, as Karolina is a defiler, she was able to force the sorceress into murdering her own children to exemplify the pain. What happened after Anna was rescued by our heroes – she hates The Governess. She hates every force that The Governess represents. She can become a valuable asset for Blakenbauers.

**Logistyka:**

Our heroes were able to identify main nodes of operation of the logistics of enemy forces. Enemies are also using so-called “black trains” to transport stuff more efficiently. A scary information – enemy forces are, more or less, everywhere. In most of the “forsaken areas” the enemy forces using Desolation to be able to overtake the region. And those trains and trucks are the vector of the Desolation.

We have also managed to identify, that the headquarters of enemy forces is in Silesia. All this chaos and destruction has started somewhat here.

**Czego szuka Overmind**

All that chaos and destruction, all the magi who are suffering, all the Desolation…
What is happening is that the magi under the control of the overmind are trying to infiltrate the bases of the Silver Candle and try to find an answer to one question: where the hell is Renata Maus. Not finding the answer to this question they are leaving the taken over bases and they are hiding the presence. In the meantime, every single base of Silver Candle is heavily pressured by the forces of the overmind; that way the Candle is unable to retaliate or do anything sensible. 

**Karolina Maus**

Caroline. Analysis of all the data has shed new light on her. She was the one who has killed magi on the excavation; she was the one to bring Desolation there. She’s not a single person. In a single body and in a single mind two souls are barking constantly – Caroline and Aurelia. Long time ago Karradrael has spliced them and she has a full force of a ghoul syndrome. Right now she is being controlled and stabilized using the same link which Karradrael uses; the Maus connection.

Caroline is a very powerful mage. She’s also a potent defiler and on top of that she is Desolated. She is not an overmind, but she definitely is a controller of the Desolation. The Desolation does not have a hold on her however; the Maus connection does.

A powerful, desolated defiler. An ingenious sorceress with two souls. Definitely a dangerous one. Fortunately, because of those souls battling her own perception and her own willpower is not as strong. She is disoriented most of the time and she doesn’t know what is real and where does she start and where does she end. This means that if she is kept from the Maus connection she should be quite an easy foe. That is, for a Desolated defiler.

**Milena Diakon**

She has been turned into a weapon. During the corruption of her pattern by the Karradrael’s matrix she got the same connection a usual Maus does with the Maus network. Dagmara was supposed to use Milena – or rather, her corrupted pattern – to infect magi in the Central Complex with the Karradrael’s matrix. This would make them susceptible to whatever stands on the other side. For that the pattern inside Milena had to mature however; about a week more was needed.

**Szczepionka na Irytkę**

The vaccine against Irytka is exactly that – Irytka variant which has been spread in the Kopalin comes from the past; it is a very specialized variant. However, it does have a cure, one which has been improved and iterated on using the Alexandria. The cure which is being administered in the Technopark does a cure the illness. But the temporary cost is a strong reduction of magical power for a month or two.
In the meantime, some magi are unusually receptive. Those magi are selected and they are administered a set of different medicines. They become loyal to the cause of the overmind. That way about 10% of magi which are being vaccinated become indoctrinated. The overmind is building an army. And an illness like this can hit in every major city; the resources are really vast.

**Konkluzje**

In conclusion, enemies plan has proved to be quite brittle. But it is so many different plans in so many different areas that it doesn’t really matter. They may lose a plan and they will create another one. Currently, Blakenbauers have decided to focus on two areas – Alexandria (to destroy enemy adaptation and to reduce the transformation of aim change forbidden magitech) and Patient Zero (to buy time and make it impossible for enemy to use something so desperate that it may harm too much to the point of no return).
The loyalist forces of Silver Candle shall be informed as soon as possible, however because of spice they can’t really be warned in advance. This might really endanger the overall operation.


# Progresja

* Klara Blakenbauer: dostaje czar hermetyczny "Stworzenie Aleksandrii"

# Streszczenie

Z danych w komputerach i przepytania stronników Blakenbauerom udało się dowiedzieć jakie są plany Overminda. Dowiedzieli się też, że zakres przestrzeni Spustoszenia jest gigantyczny; praktycznie cała Polska - acz baza jest na Śląsku. Dowiedzieli się też, że Anna Myszeczka jest pewnym sojusznikiem a Karolina Maus jest Spustoszoną Defilerką kontrolowaną przez kanał Mausów; to sprawia że potencjalnie KAŻDY Maus jest pod kontrolą Overminda.

# Zasługi

* mag: Hektor Blakenbauer, który dogadał się z odpowiednimi osobami w odpowiednim czasie by poszerzać dane od Klary
* mag: Klara Blakenbauer, która postawiła własną (słabą) Aleksandrię i przypomniała sobie swoją przeszłość z tym powiązaną
* mag: Karolina Maus, nieobecna, acz dzięki jej zapisom udało się dowiedzieć wszystkich ważnych rzeczy na jej temat - Aurelia, sieć Mausów, Spustoszona Defilerka.
* mag: Milena Diakon, zmieniana w broń przez Overminda Spustoszenia; teraz jest w dobrych rękach (chyba), więc sobie powinna poradzić
* mag: Anna Myszeczka, która straciła rodzinę przez Overminda i która z radością pomoże Blakenbauerom. Z danych od Karoliny wynika, że nie jest tajną bronią.


# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
    
# Wątki

 

# Skrypt

- brak

# Czas

* Dni: 1