---
layout: inwazja-konspekt
title:  "Przyczajona Andromeda, ukryty Maus "
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [141220 - Bogini Marzeń w Żonkiborze (An)](141220-bogini-marzen-w-zonkiborze.html)

### Chronologiczna

* [141220 - Bogini Marzeń w Żonkiborze (An)](141220-bogini-marzen-w-zonkiborze.html)

## Misja właściwa:

Ranek. Samira i Sandra obudziły się bez żadnych powikłań, ku wielkiej radości Andromedy. Samira jakaś trochę bledsza, ale to normalne.
Po śniadaniu się rozdzieliły. Samira poszła spotkać się ze swoim kolegą, który też jest na tym festiwalu (Filip Sztukar) a Andromeda poszła spotkać się z malarzem (Artur Szmelc), zobaczyć co on może powiedzieć o majestacie bogini. Sandra nie wie co ze sobą zrobić, więc zaoferowała się, że rozejrzy się po okolicy i spróbuje zobaczyć jak wygląda sytuacja na festiwalu.

Andromeda umówiła się z malarzem przy budce z kebabem. Niechętnie, ale się zgodziła na tą lokalizację - nie zna na tyle miasteczka by zaproponować inne miejsce. 
Podczas rozmowy z Arturem dowiedziała się, że Artur widział boginię. Jest piękna i fascynująca, ale nie potrafi jej opisać lub sobie przypomnieć. "Młoda" proponowała, żeby zrobić jej zdjęcie, ale Artur się nie zgodził, nie oddałoby to pełni tego czym bogini jest. Młoda? Luiza, fotografka... ASPIRUJĄCA fotografka chcąca przekazywać prawdę. A tak właściwie, policja jej szuka - zrobiła zdjęcie czegoś co nie miało być widziane i wtopiła się w festiwal. A policjantów jest czterech, więc ciężka sprawa, nie radzą sobie zupełnie. Zwłaszcza z tym morderstwem. 
To, co zauważyła Andromeda (ale Artur nie) to fakt, że podczas Festiwalu nie było specjalnie burd. To znaczy: w ogóle. To więcej niż po prostu dziwne. 
Zdaniem Andromedy Artur jest częściowo pod wpływem, ale nie do tego stopnia by stać się fanatycznym wyznawcą. Nadal nie potrafi dojść do siebie po tym, że widział boginię; to sprzeczne z tym w co wierzył. Ale nie ma w nim naturalnej człowiekowi chęci pójścia i rozgłaszania. Co także jest nietypowe.

Sandra wróciła do Andromedy. Samira nie. Sandra zgłosiła, że sprawdziła stan aury i skanowała okolicę - wszystko wskazuje na to, że większość ludzi NIE jest pod wpływem tego wszystkiego. Jest ogólna aura (ambient) która powoduje, że ludzie nie chcą mówić niektórych rzeczy i się dobrze bawią i są drenowani z ciekawości, za to wzmacniane są ich marzenia oraz są osoby silniej wpłynięte przez tajemniczą boginię. A przynajmniej, tak uważa Sandra. Niestety, zaczęła zwracać za dużą uwagę, więc przerwała.
Jako, że Samira się nie pojawiała, Andromeda do niej zadzwoniła. Samira jest na festiwalu; ktoś ukradł jej lustro. Pryzmat Myśli. Andromeda się bardzo zmartwiła. Samira powiedziała, że rozmawiała z policjantem, ale policjant w ogóle nie słuchał, był jakby w innym świecie. Był zaszokowany, że ktoś jej coś ukradł, a w ogóle że lustro a nie portfel. Samira prychnęła, to jest FESTIWAL, tu się kradnie. Podsłuchała też rozmowę dwóch innych policjantów, że przecież morderstwo, Festiwal, a ich jest tylko czwórka. Co centrala sobie myśli, czemu nie wysyła nikogo więcej. Szef kwestionującej centralę policjantki powiedział, że ona ma się tym nie zajmować, bo centrala jest centralą i wie lepiej. Ale SKĄD centrala wie?

Andromeda i Sandra poszły na festiwal znaleźć Samirę. Samira była zajęta rozmową z piękną nieznajomą. Ta (Netheria Diakon) przedstawiła się jako Nikola Dorsz. Powiedziała, że zaginęła jej podopieczna i nie wróciła do domu. Zapytana przez Andromedę czemu jest to dla niej takie denerwujące zauważyła, że mówimy o festiwalu z 4 policjantami i 3 ochroniarzami. Nie jest trudno wkraść się na ten festiwal i porwać piękną szesnastolatkę. Netheria powiedziała, że na co dzień pracuje przy organizacji takich imprez jak ta, ale dziś jest prywatnie.
Podszedł do niej policjant (Wojciech Kajak) i spytał, czemu Netheria uważa, że "Lidia" (jej podopieczna) jest logicznym kolejnym celem mordercy. Netheria odpowiedziała (myśląc że jest poza zasięgiem słuchu Andromedy) że morderca poluje na osoby mające silne przekonania. Jej podopieczna ma takowe. Więc jeśli znajdą mordercę, znajdą jej podopieczną - i vice versa. Jak policjant nie był przekonany, dała mu kopertę z kartką papieru w środku (upoważnienie od centrali policyjnej, że Netheria jest tajniakiem itp). Policjant zrozumiał, że ma to potraktować jako priorytet.
Zespół oraz Netheria zawarły sojusz - Zespół poza lustrem szuka też "Lidii" a Netheria poza podopieczną szuka też lustra.
Netheria dostarczyła zdjęcie podopiecznej.

Oki. Andromeda, Samira i Sandra poszły swoją drogą. Andromeda szybko zorientowała się, że jeżeli będzie próbowała skupić się na wszystkich możliwych wątkach, nie osiągnie sukcesu w żadnym. Tymczasem przez amulet dotarła do niej wiadomość od terminusa odnośnie tej okolicy z punktu widzenia politycznego. 
W tej okolicy działa grupa magów o nazwie Triumwirat. Nazwa nieprzypadkowa, bo magów rządzących jest trzech. Mają wpływ na ~20 innych. Triumwirat nie działa jako łagodna, przyjacielska gildia - oni działają trotylem i napalmem oraz zastraszaniem, terrorem, szantażem - zarówno wobec magów jak i innych. Są naprawdę potężni bo zmasowali moc magiczną, mają fortecę nie do zdarcia (nikt nie ma pojęcia skąd) i pływają wprost w artefaktach i konstruktach. Zdaniem Herberta, Triumwirat to zło w czystej postaci. Sami nie muszą działać - do tego mają innych. Oni nigdy nie są zagrożeni i nie brudzą rąk.
Herbert zaznaczył, że byłby bardzo zobowiązany gdyby Andromeda nie połączyła z nimi sił. To byłoby z jego punktu widzenia potworne i podłe. Andromeda powiedziała, że ona nie ma zamiaru nikogo więcej kontrolować tym Ołtarzem i gdyby mogła, uwolniłaby tych magów których Ołtarz już kontroluje.
Herbert powiedział, że to się zmieni. Andromeda się żachnęła i wtedy terminus powiedział jej, że kiedyś badał Ołtarz. To "żywa istota", to nie tylko artefakt. Ma własną okrutną wolę. Gdy Andromeda wyraziła zdziwienie, że nic mu się nie stało Herbert powiedział z kamienną miną że kiedyś miał rodzinę. Ołtarz dał mu jednoznacznie do zrozumienia, że jest przydatny ale ma znać swoje miejsce w szeregu. Andromeda i Herbert umówili się, że jak ta sprawa się skończy to usiądą razem i spróbują rozwiązać problem Ołtarza, a dokładniej - wymienić informacje. Bo zdaniem Herberta Andromeda już ulega korupcji, ale bardzo powoli. I docelowo stanie się kapłanką taką, jak "Anioł".
Niezbyt pocieszająca wizja.

Andromeda zrobiła coś nieoczekiwanego - zapytała terminusa jak on podszedłby do tematu Festiwalu. Herbert nie zrozumiał - a gdzie jest problem. No, efemeryda. Herbert po krótkim namyśle zaproponował bardzo smakowitą przynętę ukrytą w JESZCZE JEDNEJ słabej efemerydzie (by ludzie nie mogli bogini tego przynieść) po czym potraktować efemerydę mieszanką srebrno-lapisową. Andromeda zamrugała. Następnie Herbert powiedział, że pojawienie się bogini w tym samym momencie jak Ołtarz wpadł w ręce Andromedy może nie być zbiegiem okoliczności. Może to drugi Ołtarz, albo coś podobnego - Herbert widzi pewne podobieństwa między obiektem kultu, szerokim oddziaływaniem oraz nietypową efemerydą, nietypowym bytem. 
Zapytany o radę odnośnie mordercy, Herbert powiedział, że Andromeda musi zobaczyć ciało. W końcu policjant miał głupie pytanie o transport krwi. To implikuje, że może coś paranormalnego mieć tu miejsce. A jeśli tak, cóż, on (Herbert) jest terminusem.
Herbert jeszcze strofował Andromedę za to, co zrobiła w nocy. Nie wie CO zrobiła, ale wyczuł bardzo, bardzo brudny rytuał. Jeśli na tym festiwalu jest jakikolwiek mag, też to wyczuł. Tak więc Herbert będzie troszeczkę dokładniej pilnował by Andromedzie nie stała się krzywda. Choć wolałby nie narażać się Triumwiratowi.
Andromeda powiedziała, że po prostu nie jest zdolna mu powiedzieć co zrobiła.
Herbert jeszcze powiedział Andromedzie coś bardzo ważnego - wszyscy niewolnicy Ołtarza są pod kontrolą Andromedy. To powoduje, że jakakolwiek kontrola mentalna czy innego typu jest wtórna do kontroli jaką nad nimi ma Andromeda. Czyli, w skrócie, na tą sytuację, magowie połączeni z Andromedą są NAJPEWNIEJ odporni na wpływ Bogini Marzeń.

Andromeda poszła na Festiwal, rozejrzeć się i dowiedzieć co i jak. Gdy zobaczyła znękanego Wojciecha Kajaka (policjanta) zdecydowała się podejść i zapytać jak mu pomóc. Wojciech pokazał dwa zdjęcia - "Lidii" oraz nieznanej dziewczyny i spytał, czy może Andromeda je widziała. Nie. Spytał też o lusterko (widać "Nikola" faktycznie dotrzymuje swojej części umowy, co zaskoczyło Andromedę - czemu policja szuka tego lusterka?). Andromeda wykorzystała ogłupienie i zmęczenie policjanta i dopytała o co chodzi w tym morderstwie. Wojciech jej powiedział, lecz usłyszała to Anna. Otóż wszystko wskazuje na to, że zabił wampir i wyssał krew ofiary. Ale wampiry nie istnieją. Anna zjechała Wojciecha jak psa, Andromeda wykazała się wiedzą w dziedzinie okultyzmu by ratować policjantowi skórę w odpowiedzi na co Anna wciągnęła Andromedę do ochotniczej pomocy policyjnej. Wojciech ma takie oczy. "Ale nie możesz, nie dowodzisz tą akcją". "Teraz jak jest tu tajniak NIKT nie wie kto dowodzi. Równie dobrze mogę ja dowodzić". Andromedzie znowu dało to do myślenia.
Do wesołej trójki podszedł mężczyzna, przedstawił się jako Feliks Hanson i powiedział, że jest jednym z opiekunów festiwalu. Wojciecha prawie trafił szlag. Ten festiwal NIE MA opiekunów. Ktoś powinien siedzieć. Wtedy Feliks powiedział mu gdzie znajduje się poszukiwana przez niego Amelia i policjant chciał iść, ale Anna go zatrzymała. Przy okazji ich kłótni Feliks powiedział Andromedzie, że bogini chce się z nią widzieć. Odeszli spotkać się z Panią Marzeń.

Spotkanie odbyło się na Wielkiej Karuzeli. Andromeda zobaczyła wysoką, piękną kobietę, którą już kiedyś widziała. A przynajmniej tak myśli. Kobieta odwróciła się do Andromedy i ta poczuła bijącą od niej energię. To nie jest efemeryda. To coś zupełnie innego.
Arazille (czyli piękna kobieta - demoniczna władczyni) zapytała Andromedę "Czemu tu jesteś, mój bracie". Andromeda rozsądnie milczała. Wtedy Arazille zwróciła się do samej Andromedy, adresując ją jako czarodziejkę luster. Po chwili zreflektowała się. Jakim cudem mag (czarodziejka luster) jest arcykapłanką jej brata, demoniczego władcy dominacji ludzi nad magami? Andromeda przyznała się, że jest człowiekiem. Arazille uznała ją za absolutnie fascynującą - arcykapłanka, czarodziejka luster oraz człowiek jednocześnie. Coś, co nie ma prawa teoretycznie istnieć. 
Arazille powiedziała, że są tu trzy byty - ona - Pani Marzeń, Karradrael - Pan Mausów ("król myszy") oraz Iliusitius - Pan Dominacji Ludzi nad Magami. Arazille ucieszyła się, że Iliusitius nie wkroczył by ją powstrzymać lub jej przeszkadzać; zupełnie nie przeszkadza jej obecność Andromedy. Dopytała się też, o czym Andromeda marzy. W sumie... o niczym. Arazille powiedziała, że między ich trójką - Iliusitius, Arazille, Karradrael nie ma miłości. Jeśli Andromeda chce odciąć się od Iliusitiusa, Arazille jej w tym pomoże.
Arazille powiedziała Andromedzie jak ma na imię dopiero wtedy. Wyjaśniła też, że ta trójka to idee. Kiedyś nazywani "bogami". Dziś, "demonicznymi władcami".
Pierwsze zadanie jakie Arazille zleciła Andromedzie to było znaleźć Karradraela (lub emisariusza Karradraela) na Festiwalu i dogadać się z nim. Ta sama sprawa - Karradrael # Arazille odetną bez problemu Andromedę od Iliusitiusa. Potem będą dalej się zwalczać.
Andromeda zaryzykowała pytanie - gdzie jest "Lidia". Arazille powiedziała, że "tej czarodziejki" nie ma na Festiwalu. Była, ale już nie ma.
Andromeda podziękowała i się oddaliła, całkowicie nieświadoma tego, z jaką siłą rozmawiała.

Andromeda skomunikowała się amuletem z Herbertem. Poprosiła, by wyjaśnił jej imiona "Arazille", "Karradrael" oraz "Iliusitius". Terminus westchnął ciężko. Z tej trójki zna jedynie Karradraela, a to dlatego, że KAŻDY słyszał o szaleństwach serasa Jonatana Mausa, zanim Renata Maus objęła patriarchat. Herbert wyjaśnił Andromedzie, że Karradrael to potężny demoniczny władca który wspierany jest energią całego potężnego rodu magicznego i który wspiera swoją osobą magiczny ród. Wyjaśnił, że Mausów nie da się przejąć, ale Karradrael ma opcję nadpisania dowolnego z Mausów. Zauważyli (Herbert z Andromedą), że ów Ołtarz działa przecież podobnie.
Andromeda ma już pojęcie z jakimi siłami ma do czynienia i zrobiło jej się przykro.
W związku z tym terminus dostał nowe, bohaterskie zadanie. Znaleźć Mausa. Herbert się zatchnął. Maus to nie Diakon (pierwsza nimfomanka w namiocie to najpewniej Diakonka). Maus specjalizuje się w Magii Krwi i w magii demonicznej; z tego rodu pochodzą najpotężniejsi kataliści. To arystokracja magów; mała szansa, że na Festiwalu znajdzie się Maus, bo to plebejskie. Andromeda stwierdziła, że może być miasto, jej to wisi. Ma być Maus. A Arazille wyczuła Karradraela, więc gdzieś tu musi być Maus.

Wydawszy rozkazy, Andromeda zdecydowała się odwiedzić policję. Udała się do Anny Makont (policjantki) kontynuować rozmowę przerwaną przedtem przez pojawienie się Feliksa Hansona. Andromeda zastała Annę w namiocie na festiwalu, który Anna przejęła na kwaterę tymczasową. Anna była zajęta pisaniem odwołania od decyzji osób wyżej o nie wysłaniu wsparcia; Andromeda dostrzegła, że to nie jest pierwsze odwołanie które Anna napisała. Anna przepytała Andromedę szybko z różnych sekt działających w taki sposób, że wykorzystują krew i to koniecznie krew osób wyznających inną wiarę. Andromeda podała kilka przykładów ale żaden z nich tu nie aplikuje. Nie potrafi też wskazać narzędzia działającego w taki sposób jak "wampir", ale nie jest medykiem. Anna podała kilka szczegółów Andromedzie - ofiara leżała martwa na łóżku, były ślady walki, krew wyssano z tętnicy szyjnej a w rękach położonych na piersiach ofiara miała Biblię.
Tak, Anna wysłała policjanta do księdza.
Andromeda poprosiła Annę o znalezienie kogoś o nazwisku "Maus", "Mysz" i inne myszoidalne. Nie dlatego że to podejrzany, ale ta osoba może bardzo pomóc w sprawie. Policjantka zgodziła się pomóc. 

Pół godziny później, jak Andromeda chciała się na chwilę położyć w swoim łóżku, dostała telefon od Samiry. Znalazła Luizę Wantę, młodą adeptkę fotografii. Więc może Andromeda będzie chciała z nią porozmawiać. Andromeda stwierdziła, że się nie wyśpi. Like, ever.
Luiza ukrywała się w Namiocie Świateł Filipa Sztukara. W tym momencie Andromeda wiedziała już jak Samira ją znalazła. 

Samira przedstawiła Andromedę jako osobę NIE z policji, która jednak jest żywotnie zainteresowana rozwiązaniem tych morderstw. Jednak w odróżnieniu od policji Andromeda nie ma nic przeciwko zapomnieniu skąd coś wie. Widać było wyraźnie, że Samira już skutecznie przekonała Luizę do współpracy, więc ta nie oponowała. Andromeda wykazała duże zainteresowanie dobrze zrobionymi zdjęciami osób na festiwalu (szczególnie szukając Amelii i "Lidii"). Znalazła je na zdjęciach. Amelia była w grupce młodych ludzi i zdjęcie Luizy złapało tam jeszcze przypatrującego się tej grupce z ponurym wyrazem twarzy Michała Czuka.
Oki, to trochę wyjaśnia. Przynajmniej Amelia istnieje naprawdę.
Zdjęcie z "Lidią" było bardziej interesujące. Luiza złapała "Lidię" idącą z dwoma mężczyznami odchodzącą od Namiotu Świateł. Nowy trop.
Luiza nie chce powiedzieć Andromedzie dlaczego się ukrywa przed policją a Andromeda nie chce jej płoszyć. W końcu może się jeszcze przydać.
Odnośnie morderstwa: Luiza zrobiła zdjęcie ciała. Wślizgnęła się na miejsce zbrodni i została przyuważona. Zrobiła tam kilka zdjęć.
Andromeda bez większego problemu wysępiła od Luizy kopie wszystkich zdjęć z Festiwalu - a Luiza robiła mnóstwo fotek. Ale negatywy zostają u Luizy.
Samira zauważyła, że właśnie tym różni się podejście Andromedy od podejścia policji. Gdyby policja nie weszła twardo, mogliby mieć te zdjęcia, łącznie ze zdjęciami z Festiwalu. Andromeda się uśmiechnęła.

Telefon od Sandry. Był atak, kolejna osoba nie żyje. Tym razem to była aktualna szycha Festiwalu, Małgorzata Poran. Na miejscu zbrodni jest świadek - policjantka Anna Makont. Sandra zobaczyła znane sobie ślady Magii Krwi - mają do czynienia z viciniusem Magii Krwi, z cholernym wampirem, z PRAWDZIWYM wampirem. Takim od ssania krwi.
Andromeda przytomnie spytała kiedy doszło do ataku - 5 minut temu. Andromeda pobiegła na miejsce zbrodni dając terminusowi informacje przez Amulet. Terminus ma zlokalizować i zidentyfikować wampira. Andromeda nie dała terminusowi żadnych wytycznych - może atakować, może obserwować, może zignorować.

Miejsce zbrodni, czyli dom (z biurem) Małgorzaty Poran zawiera chwilowo wszystkich policjantów w okolicy. Znaczy, czterech, w czym trzech aktywnych a jedna policjantka w anty-szokowym kocyku. Andromeda korzystając z całkowitego zamieszania i ze swoich zdolności aktorskich weszła tam na bezczela. Jak już weszła do domu, skorzystała z tego że ściany wewnętrzne są typu papierowego i wślizgnęła się do pokoju obok by podsłuchać rozmowy w pokoju ze zszokowaną Anną.
Stara dobra technika szklanka do ściany i Andromeda mogła usłyszeć, jak zszokowana Anna tłumaczy, że uratował ją tylko krzyżyk na szyi. Napastniczka powiedziała, że nie zabije wyznawczyni Prawdziwego Boga. Wyraźnie napastniczka i ofiara się znały; ofiara ani przez moment nie zmieniła wyrazu twrzy, zupełnie, jakby jej tu nie było.
Anna zaśmiała się histerycznie gdy brat podał jej zdjęcia osób z miasteczka - powiedziała, że żadna z tych osób nie ma czerwonych oczu, cholernych kłów i żadna nie żywi się krwią. Anna strzeliła i "zabiła" wampira. Sęk w tym, że wampir się nie przejął.
Policja naturalnie nie uwierzyła Annie i podali jej jakieś środki uspokajające.
Andromedzie skojarzyła się ta dziwna kobieta z kościoła... ale skąd / gdzie tu miejsce dla Mausa?

Andromeda stwierdziła, że nie czas na żarty. Ma zamiar ściągnąć Augusta. Nie może tego zrobić, bo jest na akcji, więc połączyła się amuletem z innym podległym sobie magiem (Patrykiem Romczakiem). Poprosiła go o wsparcie Augusta, by go odblokować. Celem Andromedy jest odblokowanie Augusta tak, by ten mógł ją wesprzeć gdyby coś bardzo złego się zaczęło dziać. Andromeda wzięła Patryka, bo nie chce wpływać na życie codzienne magów a on może wziąć urlop i nie ucierpi nikt ani nic. Więc (5#1v5->7) się udało; Augustowi uda się wykręcić i nikt się nie zorientuje że August wycofał się przed czasem (z sukcesem).
Niestety, August i Patryk chwilę będą się tym zajmowali.

Samira poszła zająć się swoimi sprawami. Andromeda poprosiła Sandrę, by ta podłożyła księdzu pluskwę. Żeby było wiadomo, czy ksiądz jest powodem czy skutkiem Arazille. 
Andromedzie zostało czekać.
Herbert przesłał informacje odnośnie tego kim jest wampir. Nazywa się Aneta Hanson, żonata z Feliksem i matka Pawła. Andromedę podniosło - Feliks Hanson to przecież ten człowiek który zaprowadził Andromedę do Arazille! A gdzie jest teraz wampir? W domu, z rodziną.
Andromeda ma dla Herberta jedno zadanie - ma znaleźć rytuały. Ma też sprawdzić ile w Anecie Hanson zostało człowieka. Herbert zaczął oponować - chwilowo jest sam, mają przeciwko sobie Arazille (demon lorda), on jest bez wsparcia i bez pomysłu... Andromeda powiedziała, że będzie miał wsparcie niedługo, ale na razie muszą działać z tym co mają. Herbert, niechętnie, zaakceptował zadanie.

Do Andromedy wróciła Sandra. Dała jej małe radyjko i powiedziała, że ma tam ustawienia na księdza i na policjantów (każdego z osobna). Jest to tak niemagiczne jak tylko Sandra potrafiła zrobić (użyła trików z lapisowaniem dla osłabienia i ukryciem aury). Sandra powiedziała, że spróbuje przelecieć się po Festiwalu i zbudować mapę czujnikami. Może coś znajdzie, może coś uda się znaleźć; Sandra nauczyła sie już, że warto mieć czujniki na terenie na którym się działa.

Andromeda zdecydowała się znaleźć Wojciecha Kajaka i porozmawiać z nim o "Lidii" (a raczej o tych dwóch facetach, którzy szli z "Lidią"). Znalazła trójkę policjantów rozmawiających ponuro przy namiocie policyjnym. Andromeda się wycofała i włączyła radyjko. Usłyszała jak się martwią o stan psychiczny Anny i o to czy jest w stanie działać, bo jest w ciężkim szoku. Użalanie się nad Anną przerwało im wyjście z namiotu zainteresowanej która powiedziała, że wraca do akcji bo nie mają ludzi. Widziała mordercę; to nie jest kobieta, to Szymon Skubny. Policjanci WTF. Dlatego Anna chce znaleźć narkotyki, by potwierdzić lub zaprzeczyć. 
Dowódca grupy, Rafał Szczęślik powiedział, że jeśli to faktycznie skubny, potrzebują solidnego wsparcia.
Policjanci zaczęli się rozchodzić (Rafał kazał Wojciechowi zostać z Anną - od tej pory mają się nie rozdzielać). Anna powiedziała Wojciechowi, że ta jego podopieczna, Amelia... i narkotyki. Wojciech dodał dwa do dwóch. Anna powiedziała, żeby ją wycofał. Wojciech się zgodził... ale powiedział, że Anna idzie z nim. Ta się żachnęła. Wojciech powiedział, że nie zostawi jej teraz samej. W końcu, po niemałej dyskusji doszli do porozumienia - Wojciech pójdzie z Anną do kościoła porozmawiać z tutejszym księdzem a potem oboje pójdą do Amelii.

Andromeda podeszła do policjantów i spytała się czy udało im się już zidentyfikować aktualny kult mający tu miejsce. Anna zbladła jak ściana, Wojciech zauważył i naskoczył na Andromedę - czemu się interesuje? Co ona ma z tym wspólnego? W czym może pomóc? Wojciech zauważył, że chwilowo Andromeda jedynie przeszkadza w pracy policji. Andromeda powiedziała, że zobaczy jeszcze i spróbuje zidentyfikować kult na co Anna szybko powiedziała, że to nie jest sprawa okultystyczna; tu działa groźny przestępca i ona ma się nie interesować.
Andromeda zauważyła, że Anna próbuje ją chronić. Wycofała się, niczego nie obiecując.

Mając wszystko na uwadze, świadoma swojej niewiedzy, Andromeda wróciła do chaty. Zaczęła robić szkic Arazille. W końcu, zapłacą jej za obraz bogini.

Andromeda usłyszała rozmowę w kościele. Anna i ksiądz. Ksiądz jest tak szalony jak wcześniej; nawet bardziej. Anna wyjechała od razu z grubej rury - czy ksiądz wie co ma na swoim terenie. Wojciech spojrzał na Annę a jak ksiądz powiedział, że fałszywę boginię Anna zaśmiała się mu w twarz. To żadna bogini. To tylko wampir. I on, ksiądz, chroni się bezpiecznie za murami świątyni i nawet nie ostrzegł wiernych. Gdzie był Bóg? Ksiądz na to, że Bóg działa przez wyznawców a Anna zarzuciła mu, że on nic nie robi. Nie zgłosił do Watykanu. A wampir WYRAŹNIE się go boi (Annę uratował krzyżyk). Więc ksiądz zdaniem Anny jest marionetką, nie zasłużył na to, by tu być.
Wojciech w końcu dał radę wyprowadzić siostrę z kościoła. Zażądał od niej wyjaśnień a policjantka się rozpłakała.
Przyznała, że próbowała chronić brata. Zaczęła prosić by wrócili do kościoła i zostali trochę, by byli bezpieczni - on się nie zgodził. NIE zostaną w kościele z TYM księdzem. Idą na spacer do lasu. Muszą porozmawiać. I ona ma mu powiedzieć WSZYSTKO.

Andromeda była pod wrażeniem. ONA by tego NIE zrobiła. 

Zaraz potem usłyszała od komunikatora księdza perlisty śmiech. Ksiądz krzyknął "Maryja" i usłyszał "tak mnie widzisz?" z bezbrzeżnym zdumieniem. Andromeda poznała głos - Arazille, we własnej osobie. Andromeda była świadkiem bezbrzeżnie żenującej scenki jak Arazille udawała, że nie może wejść do świątyni, jak kusiła księdza który (trzeba przyznać) dzielnie się opierał nawet gdy parzył go krzyż. I w końcu ksiądz połączył krzyż ze światłem słonecznym i "oparzył" Arazille. Usłyszał, jak Arazille wyśmiała księdza - jej lojalna opętana przez demona służka wykończy swoją rodzinę. Aneta Hanson. I Arazille zniknęła. A ksiądz wziął organistę i poszli na krucjatę. Polować na wampira. By uratować duszę przed demonem.

Andromeda skontaktowała się z terminusem i powiedziała, że musi ochronić wampira przed księdzem. Terminus był jeszcze bardziej załamany niż przedtem. Andromeda chciała zatem, by porwał wampira. Terminus w rozpaczy. On nie jest w stanie tego zrobić - złe ścieżki. Plus, nie wie czym jest wampir, jak bardzo już nie jest człowiekiem, co tu w ogóle się dzieje... nie wie co ten wampir może mu zrobić, jest to sprzeczne z działaniami i treningiem terminusów... no, tysiąc powodów. Andromeda nie chce odpuścić. W końcu dochodzą do jakiegoś kompromisowego planu. Terminus ma pójść i ostrzec wampira i próbować go wyciągnąć, NIE ujawniając się, NIE ujawniając się przed Arazille jeśli to możliwe.
Porozmawiali też o lokalnym terminusie. Tym, co (jeśli tu jest) nic nie robi.
Dupa a nie terminus.
W sumie terminus od Andromedy też jakiś kiepski... był kompetentny a nagle jakby się zepsuł.

Samira poszła do "Nikoli Dorsz" powiedzieć jej o tym, że jej podopieczna poszła z takimi dwoma osobami. "Nikola" podziękowała i kontynuowała śledztwo...

Faktycznie, Herbert zapukał do drzwi i poprosił o rozmowę z Anetą Hanson. Powiedział jej, że jest w ogromnym niebezpieczeństwie, że siły na które ona poluje chcą ją zniszczyć. Wzruszyła ramionami udając że nie wie o co chodzi. Powiedział jej że jest wysłannikiem Watykanu i ma jej pomóc. Straszliwa choroba wiary opanowała Festiwal i dotknęła także lokalnego księdza. Aneta zgodziła się pójść z Herbertem, który znalazł dla niej melinę.
Doszło też do poważnego starcia Herbert - Andromeda. Zdaniem Herberta nie ma wsparcia, nie ma sensu robienia tej operacji i jest to niepotrzebne narażanie terminusa. Andromeda mogłaby to zrobić; on by ją osłaniał. A tak, jest bez osłony. Więc niech Andromeda nie mówi, że jest inna niż poprzedni właściciel Ołtarza. Z jego punktu widzenia Andromeda jest bardziej uprzejma i milej to kamufluje, ale niewiele się różnią. Andromeda odbiła - czy kiedykolwiek pomyślał co ci ludzie czują? Ludzie też zasłużyli na spokojne życie a to co się dzieje tutaj dotyka ich najmocniej. Herbert na to "jedyni ludzie z którymi rozmawiałem to ci, co wydają mi rozkazy. Nie krzywdziłem żadnych, a krzywdzą mnie. Co mam myśleć". Andromedę zmartwiła ukryta w Herbercie gorycz i wściekłość na ludzi. Jeszcze bardziej zatęskniła za Augustem.

Terminus spróbował wypytać Anetę, na życzenie Andromedy (jakby mógł, utłukłby po prostu). Niestety, socjalnie ten konkretny terminus jest bardzo kiepski. Nie do końca mu wyszło, ale wydobył jedną informację, kluczową dla Andromedy. Arazille Dotknęła Anetę, jej męża i jej dziecko. Jednak po kilku godzinach efekt Arazille zaczął znikać z Anety, za to połączyła się z Bogiem. Bóg zdradził jej imię fałszywej bogini - Arazille. Dopytana dokładniej, powiedziała, że Bóg zsyła jej wiedzę; ona modli się, pytając a on zsyła jej informacje. Potem modliła się jak to naprawić, co ma zrobić i Bóg wysłał jej grupę czynności. Zmienił ją w formę Kaina i Hioba. Zrobiła to, czego Bóg od niej oczekiwał.
Andromeda miała chory pomysł. Może Arazille się przeliczyła i jakoś podpięła Anetę do... Karradraela. To by trochę tłumaczyło. Poza tym, że nikomu nic nie wiadomo by Karradrael kiedykolwiek łączył się z nie-magami. Ale w sumie nikt nic o Karradraelu nie wie.

Herbert zaproponował Andromedzie, by ta połączyła się z tien Andrzejem Szopem przy użyciu amuletu i zażądała od niego informacji przez hipernet o: Arazille, Karradraelu i wszystkim co może im się przydać w tym kontekście (łączenia Karradraela z człowiekiem). I niech sprawdzi czy Aneta Hanson jest Mausem. Andromeda zrobiła to, co zaproponował terminus. Zauważyła, że ów Andrzej Szop jest bardzo pokorny i służalczy. Westchnęła ciężko.

Ksiądz dotarł do domku państwa Hanson. Pomodlił się, widząc, że Anety nie ma. Poprosił rodzinę, by do niego zadzwoniła na wypadek jakby się pojawiła. Wraz z organistą obeszli jeszcze kilka domów prosząc o to samo. Po czym ksiądz podreptał z powrotem na parafię. Nie znajdzie Anety Hanson sam.

Do Andromedy dotarła wiadomość od Andrzeja Szopa: Aneta Hanson była kiedyś magiem rodu Maus, ale straciła moc. Więc odebrano jej pamięć, zmieniono trochę osobowość i stworzono jej historię. Po czym ożeniono z Feliksem Hansonem który miał syna i był samotnym ojcem. W związku z tym pojawiła się nowa teoria - Arazille chciała stworzyć korytarz Anety do siebie, ale reaktywowała korytarz do Karradraela. To jednak oznacza, że najpewniej NIE MA tu Karradraela. Jest tylko Arazille... i Andromeda. Co sprawia, że sytuacja Andromedy staje się dużo, dużo trudniejsza...


"Może ściągnijmy Barbarę? Nie chcę umierać samotnie" - zrozpaczony Herbert, który dowiedział się, że ma ratować wampira przed księdzem.
"Och! Światło! Krzyż! Moja jedyna słabość!" - Arazille, lekko fałszywym głosem, do księdza.

# Lokalizacje:

1. Żonkibór
    1. domek Ewy Czuk, w którym nadal znajduje się Zespół.
    1. Festiwal Marzeń
        1. Wielka Karuzela.
        1. Namiot Świateł Sztukara
    1. Utopia Marzeń
    1. Dmuchany Zamek
    1. pobliski kościół i parafia.


# Zasługi

* czł: Kasia Nowak, która kradnie wampira sprzed nosa bogini
* czł: Samira Diakon, społeczna iluzjonistka której niewiedza o magii coraz bardziej przeszkadza.
* mag: Sandra Stryjek, technomantka i astralistka która przy okazji jest ekspertem od mrocznych rytuałów. Ale się nie przyzna.
* czł: Artur Szmelc, malarz który niepokojąco dużo wie o młodych studentkach fotografii.
* czł: Luiza Wanta, młoda studentka fotografii która bardzo chce być artystką i reporterką jednocześnie.
* mag: Netheria Diakon, tu: Nikola Dorsz; czarodziejka i tymczasowa opiekunka Luksji. Która zniknęła w tajemniczych okolicznościach na Festiwalu.
* czł: Wojciech Kajak, policjant (partner i brat Anny) który jednocześnie dorabia sobie jako ten, który ma ściągnąć Amelię do domu.
* mag: Herbert Zioło, terminus Andromedy który, jakkolwiek kompetentny, ma w sobie dużo urazy do ludzi.
* czł: Anna Kajak, jeszcze: Makont, policjantka, której świat się po prostu zawalił przez okultyzm. Ale dzielnie walczy.
* czł: Feliks Hanson, weterynarz i mąż Anety, członek kultu Arazille. Choć nie zna imienia bogini.
* vic: Arazille, demoniczna władczyni Marzeń. Z jakiegoś powodu przejmuje kontrolę nad Festiwalem bardzo powoli i ostrożnie.
* vic: Karradrael, demoniczny władca Mausów. Najpewniej jedynie pasywne źródło wiedzy dla Anety Hanson (nee: Maus).
* vic: Iliusitius, demoniczny władca Dominacji Ludzi nad Magami. Nikt nie wie czemu jest tak pasywny, że Arazille musiała o nim powiedzieć Andromedzie...
* mag: Luksja Diakon, 16, młoda czarodziejka która kocha imprezy i gry światłem. Zniknęła z Festiwalu. Występuje tu jako "Lidia".
* czł: Filip Sztukar, znajomy Samiry ze świata rozrywki. Kiedyś iluzjonista po fachu i o mały włos narzeczony.
* czł: Michał Czuk, 16, pracowity chłopak który nie chce być "cool kid" i podkochuje się w dziewczynie która stała się "cool kid".
* mag: Patryk Romczak, katalista i lifeshaper wysłany przez Andromedę do wsparcia Augusta.
* vic: Aneta Hanson, wampir i matka. "Hiob i Kain". Skażona Krwią. Z domu Maus, człowiek, przekształciła się linkując do Karradraela przy linku z Arazille.
* czł: Szymon Skubny, osoba wskazana przez Annę Makont jako morderca.
* czł: Rafał Szczęślik, policjant. Dowodzi grupą. Choć jest wielce nieszczęśliwy że tu jest.
* czł: Małgorzata Poran, 47 lat, dołączyła do zacnego grona osób, które zginęły na Festiwalu.
* czł: Józef Pimczak, ksiądz z Żonkiborskiej parafii mający Misję.
* mag: Andrzej Szop, źródło informacji dla Herberta i Andromedy przy użyciu Srebrnej Świecy; bibliotekarz pasjonujący się historią wojen magów.

# Pytania:

Ż: Jaka jest ważna lokacja w Żonkiborze?
B: Dmuchany zamek.
Ż: Dlaczego dowództwo oddziału policyjnego spada na osobę w szoku, która jednak jakoś się trzyma?
B: Przejęła kontrolę, bo chce uratować policjantów i swojego brata (którego podejrzewa o bycie z tym silnie powiązanym)
Ż: Jak czarodziejka chce bez magii przekonać policjanta by skupił się na szukaniu jej podopiecznej?
B: Twierdzi, że jej podopieczna jest następnym celem. I daje dowód pośredni.
Ż: Co przeszkadza wampirowi w skutecznej eksterminacji kapłanów Arazille?
B: Sygnały od Karradraela - jest tu Andromeda, arcykapłanka Iliusitiusa.
K->Ż: Kto w mieście oparł się wpływowi efemerydy?
Ż: Okultysta, który udaje, że nic o okultyzmie nie wie.
Ż: Znajomy Samiry ze świata rozrywki.
K: Kiedyś okultysta po fachu i o mały włos narzeczony. Od dawna nie miała z nim kontaktu.