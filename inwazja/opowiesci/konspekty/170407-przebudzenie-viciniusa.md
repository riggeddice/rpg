---
layout: inwazja-konspekt
title:  "Przebudzenie viciniusa"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170319 - Camgirl na dragach (AB)](170319-camgirl-na-dragach.html)

### Chronologiczna

* [170319 - Camgirl na dragach (AB)](170319-camgirl-na-dragach.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

* Szymon Skubny rzucił kasę na przedstawienie teatralne w teatrze w Czeliminie i wyśle tam przedstawiciela
* Młoda camgirl, Paulina Widoczek, miała szansę zagrać tam niezbyt ważną rolę
* Ktoś podłożył Paulinie jakieś dziwne narkotyki; włączył jej się agresor. Wylądowała w szpitalu w Kopalinie
* Roman Brunowicz, jej chłopak i szef Ognistych Niedźwiedzi jest podejrzany
* Artur Bryś zna Romana. Nie chce, by Niedźwiedzie wpadły w ręce kogoś innego
* Roch Knut aspiruje do rządzenia Niedźwiedziami
* Luiza Wanta też jest w czelimińskim szpitalu; chce móc zrobić reportaż z przedstawicielem Skubnego
* Tajemniczego narkotyku nie ma na rynku
* Zdaniem Edwina, narkotyk wygląda na próbę stworzenia czegoś, dzięki czemu magowie wykorzystują ludzi do walki z magami.

Z zeznań Pauliny Alina mogła wydobyć co następuje:

* Jej pamięć NIE jest zakłócona. Jeśli jakiś mag miałby to czyścić, to do Pauliny jeszcze nie dotarł.
* Narkotyk udało Alinie się zlokalizować; był podany jej przez skórę, w rekwizycie. W rękawiczkach czy coś.
* Rękawiczki _nie_ występowały w komplecie. Paulina miała nosić strój. Dostała je do założenia od koleżanki.
* Paulinie po prostu włączył się agresor po kilkunastu minutach; pobiła kilka osób. Dlatego się bała, że będzie oskarżona.

A jak chodzi o skrypt misji:

![Rysunek pokazujący kto kogo jak i gdzie](Materials/170407/mission_diagram.png)

## Misja właściwa:

Dzień 1: 

Bryś pilnuje Pauliny. Edwin poszedł do Hektora ostrzec go, że sytuacja zrobiła się naprawdę dziwna. Powiedział, że na terenie Hektora są berserkerskie narkotyki bojowe. Silniejsze, szybsze, uodparniające trochę na magię. Efekty uboczne - berserker. Edwin zauważył, że chodzi o nasyłanie ludzi na innych magów. I to jest naprawdę niebezpieczne. Na bazie wiedzy Edwina, bardzo low-key. Ktoś chciał wrobić Skubnego. Jacyś magowie testują narkotyki (wie o jednym przypadku).

Edwin powiedział, że ma próbkę. Ma próbkę krwi. Nie może wyizolować narkotyku; najpewniej to kontaktówka. Wie, gdzie to się stało - w Czeliminie, w Teatrze Wiosennym; Skubny dofinansował przedstawienie. Na próbach to się stało; najpewniej dostała przez rękawiczki. Było w jej rękawiczkach. Dziewczyna nazywa się Paulina Widoczek. Jest w Szpitalu Wojskowym. Edwin wiele więcej nie może; zrobił dość pobieżny skan. Wraca do analizy narkotyku; nie powinna być za silnie Skażona.

Hektor kazał Bórowi przysłać do siebie Alinę i Dionizego. Niech Alina i Dionizy jadą do szpitala; tam się spotkają z Hektorem. Bo Bryś pilnujący dziewczyny to takie... nieBrysiowe, że się sam zainteresował. Hektor kazał wszystkim spotkać się u Brysia, w szpitalu.

Szpital Wojskowy. Alina i Dionizy pojechali; a przy szpitalu... policja. Alina skontaktowała się z Paczołem; miał pilnować Brysia. Paczoł powiedział, że zabezpieczył raporty z monitoringu; chyba Brysia pobiła... Paulina. WAT. Paczoł spytał, czy ona NA PEWNO jest człowiekiem a nie viciniusem. Powiedział, że chyba Edwin Blakenbauer tym razem lekko zawalił, jeśli takich efektów ubocznych nie zauważył. Gdzie ona uciekła? Paczoł nie wie, pomagał Brysiowi. Bryś jest lekko ranny, nie ciężko; ale chyba jest zatruty. Czymś.

Minął moment i Hektor przyjechał. W samą porę, by poczuć jak mag (czuje po aurze) wchodzi do szpitala, gdy poczuł mrowienie aury. Do budynku wchodzi mag. Hektor poprosił maga (Kornela) na bok; wylegitymował go. Ten przedstawił się jako Ireneusz Nugat. Gdy Hektor poprosił go o wylegitymowanie się, Kornel poprosił Hektora o to, by ten udowodnił, że ma prawo legitymować; gdy Kornel zobaczył "prokurator", wyjął artefakt Dominujący ("zostaw mnie") i go Dotknął. (5+1v5-> F(aura Hektora), S); Hektor odparł atak mentalny, ale Kornel dowiedział się, że ma do czynienia z magiem.

* Chyba mamy problem, proszę pana... - Hektor, lekko zamroczony
* Mogłeś powiedzieć... - Kornel, naprawdę zdziwiony
* Brat Marcelina Blakenbauera ze Świecy. - Hektor, z niesmakiem
* Faktycznie, słyszałem wiele dobrego o tym czarodzieju - Kornel, sprawdzając w hipernecie

Kornel przedstawił się prawdziwym imieniem i nazwiskiem. Gdy Hektor powiedział, że są po tej samej stronie, Kornel potwierdził. Jest terminusem i przyszedł wyczyścić problem. Dlatego tu jest. Stało się tu coś dziwnego; niewiele wiem. Hektor potwierdził tożsamość z Marcelinem; jest ktoś taki jak Kornel i Marcelin potwierdził, że Kornel jest w tamtej lokalizacji (Kornel odblokował swoją lokalizację Marcelinowi na moment).

* Czarodzieju; czy wiesz cokolwiek o tym co tu się dzieje? Dowiedziałem się z moich źródeł niedawno - Kornel
* Wie pan niewiele więcej niż ja. Paulina Widoczek. Wykryto u niej groźne narkotyki; widać nie opuściły jej ciała... - Hektor, wyjaśniając sprawę.

Kornel się zasępił. Poprosił Hektora, że chciałby ją żywcem. Hektor zaprotestował, że ma DUŻO przeciwko, jakby Kornel chciał ją zabić. Kornel zauważył, że ma podejrzanego; kogoś, kto MOŻE stać za takimi narkotykami. Kogoś ze Świecy. Dlaczego? Ambicja. Dlatego Kornel chce mieć Paulinę żywą; chce móc ją przesłuchać. Kornel nie ma nic przeciw obecności Hektora przy przesłuchaniu.

Kornel zdziwił się, że te narkotyki zadziałały z takim opóźnieniem. "To by było coś, czego nie spodziewałbym się po jej umiejętnościach". Hektor zanotował "jej". Kornel chciałby porozmawiać z Edwinem jak chodzi o narkotyk; może jest tam coś co pozwoli poznać prawdę.

* Niestety, Świeca nie jest szczególnie zainteresowana tematem takim jak ten; dla nich to nieistotne - Kornel, ze smutkiem
* Rozwiążmy tą sprawę, zanim stanie się na tyle poważna, by być istotna - Hektor, z determinacją

Hektor może robić obławę policją i grupą śledczą ORAZ używając viciniusów tropiących. Hektor natychmiast zażądał wydanie Brysia; tego trzeba przesłać natychmiast do placówki specjalistycznej na testy toksykologiczne. I Bryś szybko wyszedł. Odstawiany ze środkami bezpieczeństwa - przypięty itp.

Edwin dostał Brysia. Szybko stwierdził, co to jest za trucizna. To neurotoksyna obniżająca czas reakcji i paraliżująca.

* Ta dziołcha jest jadowita?! - Hektor
* Tak... - zaskoczony Edwin
* My mamy do czynienia z człowiekiem? - Hektor
* Nie wiem. Może uśpiony vicinius? - Edwin
* Musimy ją wytropić... - Hektor - Viciniusy tropiące?

Patrycja skontaktowała się z Aliną. Powiedziała, że coś znalazła... ale nie wie jak to interpretować. Powiedziała, że albo kamera była uszkodzona albo ona ogląda XFiles. Jest zdziwiona. Zgodnie z prośbą Aliny, przesłała na jej telefon. Dziewczyna wylądowała w uliczce (miała SKRZYDŁA). Złapała się za głowę i wyraźnie krzyczała. Po czym wbiegła do uliczki. Jej skóra wciąż się zmienia; ma stan niestabilny.

Na prośbę Aliny Patrycja przesłała to Hektorowi. Hektor powiedział, że to fotomontaż; Patrycja przeprosiła. Powiedziała że wyczyści oryginalny obraz. Hektor powiedział, że tak bywa z działaniami grupy hakerskiej.

Margaret wpadła do Hektora i powiedziała mu o uśpionych viciniusach (bo Edwin prosił). Ludzie z odpowiednią ilością tkanki magicznej ale bez "the push". I wtedy impuls potrafi ich transformować; zmienić ich w viciniusy aktywne. I najpewniej ta dziewczyna ma dokładnie taki los. Margaret powiedziała, że jeśli Hektor chce ją uratować, musi unikać czarowania w jej kierunku. By jej mocniej nie Skazić.

Gdy Hektor zapytał jak ją zatrzymać, Margaret zaproponowała naszyjnik Hektora (mimika). Hektor powiedział, że chciałby wstrzyknąć tam inne wspomnienia; Margaret zaproponowała by Hektor stworzył sztuczne wspomnienia i je wstrzyknął używając mocy Rezydencji. W ten sposób dadzą radę opanować viciniusa.

Hektorowi spodobał się ten pomysł.

Hektor poszedł do Rezydencji gdzie wziął swój naszyjnik. Użył laboratorium (-1 surowiec) i skupił się na wypaczeniu Mimika. (11v8->S). Mimik stał się tym, czego Hektor potrzebował. Neutralizatorem. Całkowicie wyłącza agresję i sprawia, że nosiciel jest uległy.
...idealny pacyfikator.

Edwin debatuje z Kornelem. Z oczywistych powodów Aliny i Dionizego nie ma; dołączył do nich naprawiony Bryś po lekkiej amnezji. Za dużo świadków wie, że pokonała go dziewczyna, więc Bryś nie stracił o tym pamięci. Powiedział, że musi pogadać ze swoim kumplem... żeby nie szukał dalej tej laski. Alina i Dionizy zauważyli, że MOŻE to ona będzie szukać swojego chłopaka.

* Z całej naszej trójki tylko Ty masz fejsika - Dionizy do Brysia
* Stalkuję małolaty. NIC NIE MÓW! - Bryś, orientując się za późno
* Uważaj by Cię nie pobiły! - Dionizy złośliwie
* MÓWIŁEM! - wkurzony Bryś
* Niebezpieczne masz hobby... - Dionizy ze śmiechem

Dionizy i Alina się przygotowali. Ciężki sprzęt anty-viciniusowy. A Bryś przygotował auto. Kornel się oddalił w bliżej nieznanym kierunku. Dionizy poprosił Edwina o coś na viciniusa. Edwin powiedział, że większość środków zapobiegających działa w taki sposób, że jest śmiertelnymi truciznami a on chciałby móc uratować.

Edwin skomunikował się z Hektorem.

* Hektorze, pożegnał się ze mną ten Twój terminus. - Edwin
* Tak? O co pytał? - Hektor
* O narkotyk. I to, co stało się z dziewczyną i czy da się ją uratować - Edwin
* Co mu powiedziałeś? - Hektor
* Że się da. Nie wiem, ale niech nie ma wątpliwości - Edwin - Gość jest ze Świecy.
* No, to już wiemy.
* Czyli życie dziewczyny było zagrożone gdyby nie dało się jej uratować... - Edwin
* Myślisz, że terminusi są takimi potworami? - Hektor
* Właśnie, jak na terminusa Świecy ów Kornel zadawał bardzo dobre pytania jeżeli chodzi o narkotyki. - Edwin
* Doprowadził Cię na coś? - zdziwiony Hektor
* Tak. I to jest dziwne. Musi naprawdę długo badać tą sprawę. - Edwin
* Wspominał, że ma jakieś podejrzenia odnośnie potencjalnego sprawcy... jakaś czarodziejka? - Hektor
* To coś nowego. Coś, czego nie znam. - Edwin - On naprawdę zna się na tym. Interesuje się tym osobiście.
* Chciałem Cię po prostu ostrzec. Wiesz, czego nie rozumiem? - Edwin
* Czego? - Hektor
* Mag Świecy, terminus, świetna karta... współpracuje z innymi magami, mało arogancji, chce pomóc ludziom... - Edwin - Może jestem cyniczny...
* Może to po prostu dobry człowiek? - Hektor, z nadzieją
* Wierzmy w to. - Edwin
* Albo ma coś na sumieniu - Hektor, zrezygnowany
* Właśnie... - Edwin, ciężko wzdychając

Dionizy i Alina dotarli do Piroga Dolnego. Tam jest bar cieszący się niekoniecznie dobrą sławą; mordownia. Mordownia Arena Wojny. I tam właśnie Bryś zabrał Alinę i Dionizego. Wykidajło i barman to Franciszek Knur.

* Bryś. - Knur
* Knur. - Bryś
* To, co zawsze? - Knur
* Nie, mąkę na zapleczu - Bryś
* Nie znam tej dwójki - Knur
* Knur. Mordo moja. Czy on wygląda na glinę? - Bryś o Dionizym - Z takim pyskiem?
* A ta cizia? - Knur
* A to moja dupa - Dionizy. Knur z uznaniem się uśmiechnął.

Alina próbuje przekonać Franciszka Knura, by pozwolił im wejść. (5+2v3->S). Knur pozwolił.

* Bryś, może to i gliny, ale możecie wejść. Mam to gdzieś. - Knur, z uśmiechem

Do Aliny od razu dotarło, że to bardzo dziwne stwierdzenie. Spróbowała to wyciągnąć od Knura. (5v4->F(lekkie kłopoty Brysia), S). Knur powiedział jej z uśmiechem o co chodzi: nawet, jeśli to gliny, to jednak pomoże to w SPEKTAKLU. Knur wie, że teraz rozwiązuje się problem przywództwa Ognistych Niedźwiedzi; i on wyraźnie nie stoi po stronie aktualnego szefa Niedźwiedzi...

I Spektakl zaczął się godzinę temu.

Do pomieszczenia wszedł Kornel. Spojrzał zdziwiony na Alinę i Dionizego. Nie spodziewał się ich tu. 

* Co Wy tu robicie? - zdziwiony Kornel
* Możemy zapytać o to samo... - Alina
* Jestem autoryzowany przez Waszego prokuratora, możecie spytać... - Kornel, zwyczajnie nie orientując się w sytuacji
* BRYŚ! TY ZDRADZIECKI CHUJU! - Dionizy na Brysia

Knur rechocze jak Dionizy i Alina najeżdżają na zaskoczonego Brysia który próbuje się tłumaczyć a w tym wszystkim zdziwiony Kornel. 

* Mogę wejść? - Kornel, uprzejmie do Knura
* No chyba pan żartuje, inspektorze - rozbawiony prośbą Knur
* Prosiłem grzecznie - Kornel, zaduszając Knura jednym szybkim ruchem
* Oczywiście, proszę... - Knur, przerażony
* Dziękuję - uprzejmy Kornel

Alina, Dionizy i Bryś wymienili spojrzenia. Artur został z Knurem, sprawdzając czy nic mu nie jest. To jednak kumpel...

Widownia na 30-40 osób ma kilkanaście zajętych. Na samej arenie stoi dwóch mężczyzn - Brunowicz i Parszywiak. Dissują się. Ogniste Niedźwiedzie walczą przez... obrazę?

* Podsumowując: Brunowicz to chuj. Dziołcha od niego uciekła i sprzedał nas glinom - Parszywiak
* Łżesz jak chuj - Brunowicz, elokwentnie

Sokole oko Dionizego wypatrzyło na sali Obligatoryjną Diakonkę. Jakaś cizia przyszła oglądać sobie jak biją się faceci... Alina zaczęła pracować nad plotą. Parszywiak dobrze gra; a naprawdę sprzedał się policji (nie Bryś, on!) i rozpuszczał ploty o Brysiu by odwrócić od siebie uwagę. (4v4->S).

GŁOSOWANIE! Wygrał Brunowicz! Zostanie dalej szefem Niedźwiedzi! Parszywiak zażądał walki. Brunowicz zauważył, że Parszywiak nigdy nie wygrał. Parszywiak powiedział, że do dzisiaj. Diakonka się ucieszyła.

Brunowicz i Parszywiak przygotowali się do walki; Parszywiak upadł na tyłek aplikując sobie narkotyk... czopkiem. Kornel wyraźnie się skrzywił. Podczas walki Parszywiak praktycznie zmasakrował Brunowicza. Gdy Kornel został zachęcony przez Dionizego do akcji, terminus zadziałał. Zszedł na arenę i włączył własne stymulatory. Podczas walki z Parszywiakiem zaczął zbierać dane o narkotyku; też nieźle dostał. W pewnym momencie Dorota zastrzeliła Parszywiaka anihilatorem; tak by zakłócić katalizę, nekromancję, biomancję etc. Zastrzeliła tzn. gość EKSPLODOWAŁ.

Kornel odegrał scenkę "znajdźmy zabójcę", pola siłowe dookoła budynku itp. Jednak Dorota spokojnie się mogła wycofać; i tak robiła to zdalnie. Dionizy szybko o sytuacji zawiadomił Hektora.

Hektor próbuje dowiedzieć się od Kornela o co chodzi (5v4->R(Hektor czyści)). Hektor dowiedział się od Kornela jego podejrzeń - Urszula Murczyk, ta od narkotyków co ją KADEM przyskrzynił. Sprzedała coś komuś. I Kornel szuka tamtych magów...

Hektor czyści wszystkich. Diakonka już się ulotniła; Kornel ją wypuścił. Hektor po kolei czyta ludzi i ich skanuje. Chce zrozumieć, o co chodzi. Co tu się działo. (7v5->S). Analizując pamięć Ognistych Niedźwiedzi Hektor dostał pewne odpowiedzi. Od dawna już Parszywiak chciał przejąć władzę. On był bardziej zwolennikiem hardcore - dragi, sława, dziewczyny. A Brunowicz jakkolwiek lubi sławę i dziewczyny to nie chce iść ZA daleko. I nie akceptuje dragów. Plus, zrobił się miękki - zakochał się.

Parszywiak faktycznie załatwił, że jeden Niedźwiedź poszedł i kazał załatwić odpowiednie rękawiczki. A potem kazał mu zanieść w worku foliowym te rękawiczki do gościa w Czeliminie. Ten gość się bał; on by zrobił wszystko. Najpewniej to był wektor by przekazać tej dziewczynie narkotyki bojowe.

Brunowicz był przerażony i się bał o swoją dziewczynę. Parszywiak podkopywał go konsekwentnie. Plus, miał coraz większe powodzenie na arenie. Wyraźnie miał kogoś, kto mu pomagał, kogoś z cienia. Ale tylko on wiedział. Raz się chełpił, że mógłby wszystkich pokonać jakby chciał; i że pod JEGO rządami Niedźwiedzie nie będą Ciepłymi Misiami. Tylko pokażą pazury i zabiorą co chcą od kogo chcą. Tajemniczy mag pracował wyraźnie z nim i tylko z nim.

Kornel spróbuje przebadać technomantyczną kupę w laboratoriach Świecy a Hektor przejął tkankę Parszywiaka. Podzielił się nią z Kornelem; tamten bierze próbki, ale się dzieli.

Alina i Dionizy zwalili wszystko równo na Edwina. Jak byli w klinice to podsłuchali jak Edwin gadał z Kornelem. Brzmi wiarygodnie a Hektor ma okazję opierniczyć Edwina za nieostrożność...

Alina i Dionizy zabrali pobitego ciężko Brunowicza do kliniki Słonecznik, ostrzegając Edwina o sytuacji. Uchylił okna, by zapach poszedł...

Blakenbauerowie wymyślili PLAN. Wzmocnią zapach Brunowicza i przeniosą go na ZETĘ, po czym Oddział Zeta złapie viciniusa, jak ten się wślizgnie do kliniki Słonecznik... a inny członek Zety wsadzi naszyjnik na viciniuskę.

Viciniuska przyszła. Zeta ją złapała. Chciała jeść... i się przytulać. Różne natury w niej walczyły. Ale trafiła na Zetę.

* Da się jej pomóc. Ale wtedy nic się nie dowiemy. - Edwin
* Ratujemy - Hektor
* Dobrze. Jestem lekarzem, ratujemy. - Edwin, nie zmrużywszy oka - Musimy powiedzieć Twojemu terminusowi, że stracimy ślady...
* Że straciliśmy ślady - Hektor, z uśmiechem
* Pomyliłem się. Zbyt chciałem jej pomóc. Nie zauważyłem, że stracimy ślady - Edwin, z uśmiechem

Kornel nie był szczęśliwy, ale rozumie. Takie rzeczy zdarzają się czasami...

# Progresja

* Patrycja Krowiowska: wierzy w dziwną szajkę hakerów, którzy bawią się w "x-files" na kamerach przemysłowych
* Hektor Blakenbauer: przekształcił swojego Mimika Symbiotycznego w myśli o wysokim poziomie spolegliwości, nieagresji i uległości
* Paulina Widoczek: przekształciła się w viciniusa pod opieką i czujnym nadzorem Edwina Blakenbauera; będzie funkcjonować w świecie ludzi
* Artur Bryś: dostał opinię gościa, którego skroiła słaba dziewczyna w szpitalu; najpewniej się do niej dobierał czy coś?
* Urszula Murczyk: Kornel skutecznie zrzucił na nią odpowiedzialność za narkotyki bojowe nad którymi eksperymentował

# Streszczenie

Baletnica pod wpływem narkotyków bojowych uruchomiła uśpione geny viciniusa; stłukła Brysia i uciekła. Hektor napotkał Kornela (terminusa szukającego tych narkotyków) i weszli we współpracę. Alina i Dionizy zapewnili, że Ogniste Niedźwiedzie nadal mają tego samego bossa (kumpla od Brysia). Kornel dokonał zdalnej egzekucji aspirującego do roli bossa Parszywiaka, który podłożył narkotyk baletnicy. Baletnicę złapał Hektor z Edwinem i doprowadzili ją do w miarę ludzkiej formy...

# Zasługi

* mag: Hektor Blakenbauer, namierzający terminusa i współpracujący z nim; wykorzystał naszyjnik (mimika), by ratować viciniuskę i zbajerował Patrycję że hakerzy
* vic: Alina Bednarz, aktorka wyciągająca informacje oraz rozpuszczająca podłe plotki ratujące Brunowicza przed utratą władzy
* vic: Dionizy Kret, szydzący, że Brysia pobiła dziewczyna; autor kilku pomysłów o lokalizowaniu viciniuski i udający chłopaka Aliny
* mag: Edwin Blakenbauer, zgłosił Hektorowi problem z narkotykami, ostrzegł, że Kornel jest jakiś... zbyt przyjacielski oraz doprowadził Paulinę Widoczek do formy funkcjonującej wśród ludzi.
* vic: Paulina Widoczek, uśpiona viciniuska uaktywniona przez narkotyki Kornela. Między "przytulić" a "pożreć"; pobiła Brysia. Skończyła u Edwina; ten ją naprawi.
* vic: Szczepan Paczoł, monitorujący sytuację (i Brysia) w szpitalu z ramienia Sił Specjalnych Hektora; zabezpieczył nagrania z monitoringu by nikt nic nie zobaczył
* czł: Artur Bryś, który wpierw został pobity przez transformującego viciniusa (Paulinę), potem doprowadził Alinę i Dionizego do Ognistych Niedźwiedzi jak Edwin go naprawił
* mag: Kornel Wadera, śmiertelnie niebezpieczny i uprzejmy terminus, któremu eksperymenty z narkotykami bojowymi wyrwały się spod kontroli. Dokonał egzekucji ludzkiego zdrajcy. Współpracował z Zespołem udając zdziwionego.
* czł: Patrycja Krowiowska, która monitorowała viciniusa (Paulinę) i w końcu uwierzyła w dziwnych hakerów fantomowo zmieniających zapisy kamer "x-files lulz".
* czł: Franciszek Knur, wykidajło i barman w Mordowni Arenie Wojny; członek Ognistych Niedźwiedzi stojący po stronie Parszywiaka przeciw Brunowiczowi.
* czł: Roman Brunowicz, przywódca Ognistych Niedźwiedzi, którego ciężko pobił Parszywiak na narkotykach bojowych; utrzymał władzę.
* czł: Dominik Parszywiak, który chciał przejąć władzę nad Ognistymi Niedźwiedziami i zlekceważył wolę Kornela. KIA. Skończył zestrzelony przez Dorotę Gacek.
* mag: Urszula Murczyk, nieobecna na misji, ale Kornel zrzucił na nią winę za to, że przygotowała narkotyki bojowe.
* mag: Dorota Gacek, fizyczna egzekutorka Kornela; przygotowała działko technomantyczne i zniszczyła Dominika Parszywiaka na oczach innych ludzi.
* vic: Oddział Zeta, siły ciężkie sprowadzone przez Edwina do kliniki Słonecznik, by złapać Paulinę Widoczek (viciniusa) nie robiąc jej krzywdy.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Trzech Myszy
                        1. Szpital Wojskowy, gdzie Widoczek przekształciła się w viciniusa i skąd zwiała Brysiowi i Paczołowi (raniąc i zatruwając Brysia)
                    1. Obrzeża
                        1. Klinika Słonecznik, kontrolowane przez Edwina miejsce do zadań specjalnych; tym razem posłużyło do pułapki na viciniusa i odtrucia Brysia
                1. Piróg Dolny
                    1. Pylisko
                        1. Mordownia Arena Wojny, miejsce walki o szefostwo Ognistych Niedźwiedzi i miejsce egzekucji ludzkiego zdrajcy przez Dorotę Gacek

# Czas

* Dni: 1

# Narzędzia MG

## Cel misji

* Viciniusy są wśród nas; różne rzeczy mogą je uruchomić.
* Czy wygra "płaszczka" i nauka / wiedza, czy uratowanie życia? A zwłaszcza: xxx CZY ratunek?
* Pokazać pierwsze eksperymenty Kornela z narkotykami bojowymi
* Czy Kornelowi uda się zrzucić winę na kogoś innego?
* Roman utrzyma władzę u Ognistych Niedźwiedzi, czy nie?
* Hektor jako prokurator między światem magów a ludzi - da radę? Pokazać siłę magów wobec ludzi.

## Po czym poznam sukces

* Wyjaśnię i pokażę koncept uśpionych viciniusów; będzie to zrozumiane i w kanonie systemu.
* Decyzja odnośnie losu Pauliny Widoczek.
* Pojawią się nietypowe narkotyki bojowe, które feedują kiedyś do przyszłych misji.
* Czy wyjdzie temat Urszuli Murczyk czy nie? ;-) -> progresja
* Kto dowodzi Ognistymi Niedźwiedziami?
* Na misji są ludzie i magowie; nie wszystko to sami magowie

Stan misji?

* Sukces: mamy uśpione viciniusy i stały się kanonem
* Sukces: Paulina staje się viciniusem ludzkim
* Sukces: narkotyki się pojawiły i Kornel zarzuca eksperymenty
* Sukces: Urszula ma wpis w progresji
* Sukces: Mamy władcę Ognistych Niedźwiedzi
* Porażka: Mimo obecności ludzi, to magowie/viciniusy robili wszystkie ważne ruchy: Hektor, Alina, Kornel
