---
layout: inwazja-konspekt
title:  "Wiktor kontra Kadem i Świeca"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151216 - Między prawdą i fikcją Arazille... (AB, DK)](151216-miedzy-prawda-i-fikcja-arazille.html)

### Chronologiczna

* [151216 - Między prawdą i fikcją Arazille... (AB, DK)](151216-miedzy-prawda-i-fikcja-arazille.html)

### Logiczna

* [160327 - Pięćdziesiąt oblicz Szlachty (SD)](160327-piecdziesiat-oblicz-szlachty.html)

## Kontekst misji

- Wiktor próbuje podporządkować sobie Silurię. Siluria próbuje podporządkować sobie Wiktora. Dalej.
- Gerwazy prawie zniszczył wzór Silurii; uratował ją Ozydiusz. Wioletta uczy się od Silurii jak być skuteczniejsza.
- Arazille się ujawniła, pożarła Kariatydę, ale Blakenbauerowie ją odparli. Zaraz uderzy plaga Irytki Sprzężonej.
- Weszło Millennium na teren Kopalina za prośbą Wiktora; działają w kompletnym ukryciu.

## Cel misji:

Cel misji: 
- Eskalacja. Wiktor dostaje czego chce od Silurii i ona niewiele na to może poradzić.
- Rozpoczęcie ostatniej fazy kampanii.
- Podmontowanie Spustoszenia Draceny do tego wszystkiego.

## Punkt zerowy:

- Ozydiuszowi nie podoba się to, co dzieje się z Wiolettą... ale ma to gdzieś. Próbuje odzyskać pancerz (potencjalnie Spustoszony) stworzony przez Tamarę od Wiktora.
- Wiktor ma powoli dość powolnej zabawy z Silurią. Zdecydował się przyspieszyć, nie słucha ostrzegającego go Gerwazego. To ON jest panem. 
- Wiktor ma dość być "tym trzecim lub czwartym" w Szlachcie. On chce rządzić Szlachtą. Ma dość oporu ze strony wszystkich, łącznie z własnym rodem.
- W skrócie, Wiktor chce iść w kierunku na Overlord Supreme i przejąć Kopalin. A jeśli ma do tego oddać jego część Millennium, niech tak będzie.
- Mirabelka Diakon (Millennium) jest opłacana przez Wiktora i ma za zadanie pomóc Wiktorowi zdobyć kontrolę nad magami poza Kopalinem.
- Amanda Diakon (Millennium) powoli wchodzi na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Marian Łajdak i Andżelika Leszczyńska dostali dostęp do nieaktywnego Spustoszenia; unieczynnili je. Muszą je schować ASAP.
- Wiktor poluje na to nieaktywne Spustoszenie. Wysłał licznych agentów. Marian i Andżelika nie radzą sobie z ukrywaniem się i nie mogą wejść do KADEMu.
- Gerwazy nie ma pomysłu jak się pozbyć Silurii tak, by Ozydiusz nie wiedział. Chwilowo jest niezdolny do działania.
- Dagmara próbuje wpłynąć na Silurię tak, by ta zrezygnowała póki jeszcze może. Wioletta próbuje Silurię przekonać, by nie odchodziła.

## Misja właściwa:

Dzień 1:

Dzień po regeneracji po Festiwalu Siluria obudziła się w świetnym humorze. Stwierdziła, że trzeba się pozbyć Gerwazego (bo niebezpieczny) i Dagmary (bo mniej niebezpieczna, ale przeszkadza). Wpadła na chytry plan i zdecydowała się go przeprowadzić. Z Wiolettą jako ochroniarzem poszła spotkać się z Diakonem; dokładniej, z Netherią. Wioletta nalegała, by być świadkiem rozmowy, ale Siluria ją przekonała. Wioletta będzie musiała to zgłosić. Siluria nie oponuje - chce zbudować z Wiktorem pewne zachowanie, power play.

Netheria z przyjemnością powitała kuzynkę. Silura poprosiła Netherię o to, by ta znalazła kogoś ze Świecy skłonnego Silurię zaatakować tak, by ta mogła być obroniona (lub poturbowana) - przez terminusa. Siluria chce wywrzeć odpowiednie wrażenie na pewnym mężczyźnie. Netheria się uśmiechnęła. Ona rozumie. Zajmie się tym za odpowiednią cenę. Dodatkowo Netheria powiedziała Silurii o tym, że w Kopalinie działają magowie KADEMu; kilku, próbuje się dostać do samego KADEMu, ale nie wiedzą o lockdownie. O dziwo, poluje na nich Świeca i jakoś jest w stanie ich mniej więcej namierzać. Netheria nie jest w stanie ich namierzyć; poruszają się w niewykrywalnym pojeździe (dla Netherii).

Siluria poprosiła Netherię, by ta wzięła od Silurii komunikator KADEMu, który dostała. Siluria zaszyfrowała wiadomość "lockdown". Poprosiła Netherię, by ta wysłała ten sygnał. Netheria się zgodziła; z radością wyśle go z klubu "Działo Plazmowe". W wiadomości jest informacja o lockdownie i o początku lockdownu. I o tym, że lockdown wciąż trwa. Siluria puściła to szyfrem - po co Świecy ułatwiać. Taki broadcast.

Z Wiolettą wróciły do pokoju (po drodze kupiła kilka drobiazgów). Tam, ku jej zdziwieniu, leży holokryształ Mausów. Na biurku Silurii. I Siluria nie wie, jak się tam dostał i czym jest. Wioletta nie wie o tym krysztale; sama nie rozpozna, że to nie kryształ Silurii. 
Po dyskusji i rozpatrzeniu opcji, Wioletta przyznała, że kryształ jest jednorazowy i dokona kolapsu. Siluria zdecydowała się go zobaczyć.

Fade in; kryształ miał dampening. Był rekonstrukcją; Siluria czuje, że osoba "rekonstruująca" nie czuje się z tym dobrze co jej pokazuje. Kryształ pokazał scenę jak Wiktor obrócił się przeciwko Antygonie po tym, jak ta go "zdradziła" z Draceną (patrz misja: "Antygona kontra Dracena") i nie panując nad sobą pobił ją dotkliwie a potem sukcesywnie torturował by ją w końcu złamać. Powiedział wyraźnie, że przez Antygonę dostanie się do Draceny "i dalej".
Kryształ zawierał jeszcze 3 informacje: 1) "jego wspierasz", 2) "ostrzeżenie", 3) "uciekaj póki jeszcze możesz!"...

Wioletta spojrzała z niepokojem na Silurię, ale nie było po niej nic widać. Kryształ był zaprojektowany, by Siluria mogła powiedzieć cokolwiek. Siluria się uśmiechnęła "cichy wielbiciel". Wioletta się rozluźniła. Ale cichy wielbiciel, który ma takie uprawnienia? Siluria - wzmacniając to, że chce wyciąć Gerwazego - powiedziała, że się domyśla kto to, ale nie powie. Wioletta się zaśmiała.

Siluria zdecydowała się uruchomić drugi element swojego planu. Porozmawiała z Wiolettą o Dagmarze; Siluria chce, by Wioletcie zrobiło się Dagmary żal i żeby ta z własnej inicjatywy zaproponowała Dagmarze jakąś formę pomocy czy terapii u Silurii. A jednocześnie chce, by Dagmara odebrała to jako manipulowanie Wiolettą - ale by Wioletta tak tego nie odebrała. Siluria nie chce zniszczyć ich przyjaźni. Silura chce jednak, by Dagmara Silurię zaatakowała, najlepiej na warcie Gerwazego.

Siluria: czy zamaskuje przed Wiolettą iluzję w iluzji: (Siluria: 20, Wioletta: 17) -> sukces.
Siluria: czy sprowokuje Dagmarę do działania: (Siluria: 15, Dagmara: 16) -> failure

Silurii udało się ukryć to przed Wiolettą i udało jej się doprowadzić do poróżnienia dziewczyn; nadal się lubią, ale Dagmara wygarnęła Wioletcie, że ta zmieniła się w pieska na pasku Silurii a Wioletta powiedziała Dagmarze, że ta nie jest w stanie zrozumieć przyjaźni nawet, gdyby ta ją kopnęła. Bo Wioletta chce tylko dobra Dagmary. I Siluria mogłaby pomóc. Wpływ Silurii na Wiolettę tylko się trochę zwiększył; wpływ na Dagmarę zmienił się na jeszcze silniejszą złość i brak zaufania ze strony terminuski. Dagmara jedynie powiedziała Wiktorowi, że Siluria nastawia Wiolettę przeciwko niemu. Wiktor ACKnął. Tak bardzo jej wierzy...

Wieczorem, na miłym spotkaniu Wiktor - Siluria, Wiktor poruszył pewną sprawę. Nacierając związaną, nagą Silurię silnym afrodyzjakiem powiedział jej, że potrzebuje lojalnej Wioletty która spełni wszystkie jego rozkazy za niecały tydzień. I Siluria dostanie czego tylko potrzebuje, by ona i Wiktor złamali i zindoktrynowali do tego poziomu Wiolettę. Wiktor powiedział, że "sytuacja się bardzo zmieniła" i że potrzebuje wyciągnąć pewną osobę ze Świecy tutaj; to umożliwi mu być czymś więcej. Powiedział Silurii, że ona jest "tylko wyrzutkiem KADEMu", a on "tylko książątkiem Szlachty i Sowińskich". A może być kimś innym. I ona też może być czymś więcej. Wiktor powiedział Silurii, że lockdown KADEMu to nie przypadek i że Silura może potem wrócić na KADEM z sekretami EAM. Czarodziejkę miło to połechtało... ale Wioletta... Siluria chciała jej pomóc, nie ją zniszczyć. A to co mówi Wiktor świadczy, że Wioletta może stracić karierę. Tu też Silurii pojawił się flashback Antygony...

Chwilowo Siluria zatraca się w ekstazie. Fakt, jak on jest niebezpieczny i jak potraktował Antygonę i mógłby ją... też ją podnieca. Bo Siluria złapie go dla siebie.

Siluria, choć half-crazed z ekstazy, dała kontrpropozycję. Skoro chodzi o "kogoś kto nie pasuje", czemu nie Diana? Lowly terminus (Wioletta) a Diana Weiner, jedna z przywódczyń Szlachty? Wiktorowi zaświeciły się oczy. Diana. Nigdy, NIGDY nie pomyślał o Dianie jako o kobiecie. Ona zawsze miała taki... high-brow, ethereal... dla Wiktora sama myśl złamania Diany sprawiła jeszcze większą przyjemność. Czy Siluria mu pomoże? Siluria szybko pomyślała o biednej Wioletcie i się zgodziła. I tak miała to w planach (nie do końca tak, ale zawsze), a Wiolettę warto uratować, po prostu...

Przy okazji, Siluria poznała plany Wiktora. Wiktor chce Szlachtę. Chce przejąć CAŁĄ Szlachtę. Dla siebie.

Dzień 2:

Klub "Działo plazmowe". Ku wielkiej rozpaczy Gerwazego, który ma Silurię pilnować, Siluria poszła naładować baterie - wpierw potańczyć, poszaleć, potem ostry seks na zapleczu. I właśnie wtedy została zaatakowana. Wtedy, gdy była najbardziej bezbronna. Oczywiście, Gerwazy stał na straży; olał temat (nie powinien podglądać) i zadziałał przeciwnie do najemnika załatwionego przez Netherię. Udało się Gerwazemu ciężko poranić atakującego, ale go nie schwytał; Netheria i napastnik zdecydowanie niedoszacowali siły Gerwazego. Silurii bardzo, bardzo pasuje to, że napastnik dał radę uciec.

Gerwazy przerwał "ładowanie baterii" i bezceremonialnie wytargał Silurię do jej pokoju w Świecy. Siluria jednak widziała całą akcję... jak chciała.
Po napaści Siluria jest bardzo grzeczna i bardzo posłuszna Gerwazemu i w ogóle zachowuje się jak cywil. Bardzo grzeczny cywil. Zaprosiła nawet Gerwazego na herbatkę. Terminus oczywiście odmówił, ale sprawdził pokój Silurii czy nikogo tam nie było i w ogóle. Więc byli sami w pokoju ;-).

Wieczorem, nosząc ulubioną obróżkę Wiktora, klęcząc przed nim nago Siluria wspomniała o cudownym Gerwazym. Siluria wielokrotnie powiedziała coś korzystnego o Gerwazym, pozwalając Wiktorowi na zauważenie, jak on działa na Silurię. Wiktor sponiewierał Silurię dość solidnie; był ból i siniaki, ale Siluria osiągnęła dwie rzeczy:
1) Wiktor poczuł się zazdrosny. Zazdrość Wiktora daje Silurii nad nim jakąś władzę.
2) Co prawda to Siluria jest na kolanach i robi co Wiktor chce, ale Wiktor podejmuje decyzje pasujące Silurii. Więc... kto jest tu "tym u góry"?

Dzień 3:

Ku "wielkiemu smutkowi" Silurii, pilnuje jej Wioletta. Wioletta, nieświadoma przed jakim losem uratowała ją Siluria (i się nigdy nie dowie). Wioletta powiedziała, że Gerwazy został wysłany na tajną misję przez Wiktora. Spytała, czy to on był tym "cichym wielbicielem". Siluria się uśmiechnęła i nie powiedziała ani słowa...

Wioletta też powiedziała, że Wiktor dostał nową strażniczkę. Nie chciał jej. Ktoś "wyżej" mu ją narzucił. Tien Sabina Sowińska. Skuteczna, chłodna, dama. Gwarantka dobrego zachowywania się Wiktora. Siluria się może nie ucieszyła, ale się nie zmartwiła. Lepsza Sabina niż Gerwazy. Sabina przybędzie następnego dnia.
Wioletta przekazała też wiadomość od Kermita Diakona; podobno "ulubione czerwone kwiaty Netherii zwiędły". To znaczy krytyczne niebezpieczeństwo, ale nie zagrażające Silurii. Ani nikomu z rodu. Patrząc, że Netheria to przekazała, chodzi o magów KADEMu...

Wioletta oczywiście to łyknęła. Nie miała pojęcia, że to był kod. Nieważne, czy jest tak naiwna czy woli się nie domyślać; Siluria wie jednak, że w tej kwestii lojalność Wioletty jest we właściwym miejscu.
Netheria powiedziała, że magowie KADEMu są ścigani. Mają mało energii, niewiele mocy, mają COŚ co chce uzyskać Świeca i Świeca umie ich namierzyć. Netheria zidentyfikowała magów Świecy polujących na KADEM - to siły Szlachty dowodzone przez... Dagmarę i Wiolettę.

Siluria zna tych magów KADEMu. To Marian Łajdak i Andżelika Leszczyńska. To sprawia, że Siluria może im przekazać, że to od niej pochodzi wiadomość. Siluria poprosiła Netherię o przysługę rodową. Niech Netheria doprowadzi do wycofania magów KADEMu. Siluria nie chce wiedzieć jak, gdzie itp. Chce wiedzieć, że ci magowie są bezpieczni. To wszystko o co prosi. Sama zostawi komunikat jaki ma być wysłany do Mariana i Andżeliki by oni byli w stanie przyjąć pomoc od Millennium. Niech to będzie "pomoc od Silurii", żeby wiedzieli, ale nie można tego tak przetransmitować.

Netheria się zgodziła. Siluria może na to liczyć...

## Dark Future:

Luźne:

V - Dagmara zostawia Silurii informację o traktowaniu Antygony przez Wiktora na krysztale rejestrującym.
X - Wioletta próbuje przekonać Silurię, że to nic takiego; że trzeba było to zrobić.
V - Siluria dostaje zadanie ulojalnienia Wioletty (Wiktorowi to potrzebne; musi mieć pewność)
V - Siluria dostaje wiadomość o rozpaczliwym położeniu Andżeliki i Mariana od magów KADEMu. Proszą o wsparcie Głowicy. Proszą o schowanie nieaktywnego Spustoszenia
X - Gerwazy skutecznie pokonuje i porywa Andżelikę i Mariana.
X - Siluria dowiaduje się, że najpewniej magowie KADEMu są przetrzymywani przez Wiktora.
X - Dagmara ma przesłuchać magów KADEMu używając "specjalnych technik".

# Zasługi

* mag: Siluria Diakon, która nago na kolanach doprowadziła do usunięcia Gerwazego i uratowała Wiolettę przed złamaniem kariery i jej psychiki.
* mag: Wiktor Sowiński, który planuje przejąć kontrolę nad całą Szlachtą i nie zawaha się złamać nikogo i niczego by to uzyskać. Poluje na KADEM.
* mag: Wioletta Bankierz, która stoi bardziej po stronie Silurii niż Wiktora. Uratowana przed złamaniem przez Wiktora (o czym nie wie).
* mag: Dagmara Wyjątek, nadal zwalcza Silurię. Nadal Wiktor ją ignoruje. Jej pozycja słabnie z dnia na dzień.
* mag: Gerwazy Myszeczka, który obronił Silurię przed zaaranżowanym przez Silurię atakiem, za co został przez Wiktora odesłany na "tajną misję".
* mag: Netheria Diakon, koordynująca dla Silurii rzeczy z ramienia Diakonów. Uratowała * magów KADEMu i służy jako kontakt Silurii.
* mag: Marian Łajdak, który ma nieaktywne Spustoszenie i rozpaczliwie chce zniknąć nim * magowie Szlachty go z nim złapią. Ma też Andżelikę Leszczyńską.
* mag: Sabina Sowińska, osoba wprowadzona "przez kogoś wyżej" na miejsce Gerwazego. Ma "chronić" Wiktora i zapewnić, by ten się zachowywał.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk 
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Kompleks Pałacowy
                                1. Skrzydło Sowińskich
                                     1. Pokoje Gościnne
                        1. Cyberklub Działo Plazmowe
                        1. Prywatne Osiedle Mirra
                            1. Apartament Netherii Diakon