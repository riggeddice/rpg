---
layout: inwazja-konspekt
title:  "Zaszczepić Adriana Murarza!"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161229 - Presja ze strony Czelimina... (AW)](161229-presja-ze-strony-czelimina.html)

### Chronologiczna

* [160803 - Sleeper agent Oktawiana (HB)](160803-sleeper-agent-oktawiana.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Edwin powiedział Dionizemu, że ktoś musi zdobyć szczepionkę z Technoparku. Dionizy nie chce - może ktoś już wyszedł z Technoparku? W gazetkach są informacje, że przecież różni magowie już są wyleczeni i wyszli z Technoparku i że zachęcanym jest by magowie przyszli się zaszczepić.
No ok, ale mimo, że wszystko ma miejsce w Technoparku, to szansa powrotu jest... niewielka. Dionizy i Alina nie są zbyt chętni by to robić. 
Po krótkiej rozmowie, wpadli na plan: Adrian Murarz. On ma się dać zaszczepić. A Dionizy i Alina są po to, by uratować Adriana / wydobyć go po zaszczepieniu.

Hektor i Klara są na rozmowie z Mojrą. Mojra powiedziała, że zadekretowano, że Baltazar Maus jest niewinny i że rozpoczęto działania przeciwko Malii Bankierz. Mojra powiedziała, że Świeca w całej Polsce jest atakowana; to nie tylko tutaj. Mojra przyznała, że hipernet bardzo szwankuje. Nawet do tego stopnia, że nie działa friend-or-foe czy rozpoznanie kto jest kim. Albo komunikacja. Ogólnie, hipernet działa... ale bardzo słabo. Systemy hipernetowe potrzebują zasilania i stabilnej aury magicznej a żaden z tych warunków nie jest spełniony.

Hektor skonktaktował się z Emilią - poprosił o udostępnienie Vladleny; ona może coś wiedzieć o Spustoszeniu. I faktycznie, Emilia dostarczyła Vladlenę pod osłoną. Zapytana o Spustoszenie, Vladlena powiedziała, że istnieje sposób wyłączenia Spustoszenia - użycie EMP. Vladlena zaproponowała zbudowanie EMP o mocy do wyłączenia Technoparku. Tylko potrzebne jej kilka Zajcewów do pomocy i porządny gabinet w Rezydencji. Hektor wyraził zgodę.

Margaret zwróciła się do Hektora - nie da się kontynuować projektu Trzygław. Brakuje kluczowego komponentu - kilku MAGÓW. Hektor powiedział, że porozmawia o tym z Dianą - ale nie teraz. Trzeba kupić trochę czasu...

Dzień 2:

Adrian Murarz dostał jasne instrukcje i wytyczne, by się słuchać Aliny i Dionizego i ma się udać zdobyć szczepionkę z Technoparku. Jego pamięć została dostosowana przez Rezydencję; Murarz nic nie wie, idzie się zaszczepić na Irytkę. A i Dionizy i Alina mają szybki 'extraction button' z pająka fazowego. A Klara już pilnuje pająka fazowego, by poradzić sobie z ekstrakcją zanim do czegoś dojdzie.

Przychodzi Adrian Murarz ze swoją maskotką do Technoparku. Adrian Murarz dostaje telefon komórkowy z lokalizacyjną apką. Terminus stojący przy drzwiach wysłał Alinę do kuchni a Murarza na szczepienie. Niestety, Murarz miał kiepską reakcję...
Terminus wywala Alinę za drzwi. Murarz wyjdzie za tydzień. Alina stawia opór - jest maskotką i w ogóle. Marek Kromlan informuje Alinę, że trzeba się skomunikować z Eleą Maus; ona jest lekarzem prowadzącym. Alina nadal narzeka i błaga, bo jej pani ją skrzywdzi. Terminus się zlitował - zaklęcie mentalne udające, że Alina faktycznie poszła i sprawdziła.
Klara facepalmowała. Na bioskanerach wszystko wskazuje na to, że Alina się nie przesunęła z miejsca. Nie mają go.

No cóż. Po tygodniu wyjdzie. Nie mają możliwości sensownego wydobycia szczepionki, nie w taki sposób. Porwanie maga - jeśli to tyle trwa - też nic nie da, bo szczepionka zdąży być zamaskowana. Edwin upomniał się u Elei o Murarza (zapewniając, że nic złego się mu nie stanie) i tyle.

Czyli akcje przeciwko temu miejscu nie są udane.

Vladlena dała Hektorowi telefon. Dzwoniła Patrycja, przerażona. Hektor się zdziwił - przecież jest porwana. A nie, Zajcewowie odbili Patrycję; The Governess się musiała wycofać. Niestety, nie dorwali jej. Siły Tatiany przekazały Patrycję Dionizemu i Alinie, w skrzyni. Patrycja trafiła do Rezydencji; okazało się, że The Governess zmieniła jej strukturę biologiczną. Gdyby Patrycja zginęła, Edwin nie dałby rady jej odbudować.

...czyli The Governess włada magią krwi...

Klara zobaczyła alarmujące sygnały skanerów magicznych. Kompleks Centralny rozjarzył się energią. Po chwili potężna wiązka energii wystrzeliła z Kompleksu Centralnego i uderzyła w Szpital Pasteura; Portal Millennium został zniszczony i potężna fala Skażenia uderzyła Szpital. Mojra, zapytana co się dzieje, odpowiedziała - użyty został Intensyfikator. Potężna broń zamontowana w Kompleksie Centralnym, jedna z broni pochodzących z przeszłości (i nieużywanych), który był używany zanim Porozumienia Radomskie doprowadziły do ogólnego pokoju między magami. Intensyfikator działa w ten sposób, że im większe stężenie magii tym większa szkoda; zupełnie jest niegroźny wobec ludzi, jednak im bardziej Skażony mag / obszar tym groźniejsze wyładowanie...

Z Hektorem skontaktował się Wiktor Sowiński. Powiedział, że zapomniał dostarczyć Hektorowi materiałów kluczowych do zbudowania oddziału Trzygław. Magowie są w drodze. Hektor powiedział, że nie chce tego robić - Wiktor powiedział, że ci magowie zostali skazani na śmierć przez niego oraz Bogdana Bankierza; innymi słowy, Świeca skazała tych magów na śmierć, więc niech posłużą do konstrukcji oddziału Trzygław. Hektor zaoponował i Wiktor dał mu do zrozumienia, że Intensyfikator będzie użyty. Wola Świecy musi zostać zrealizowana.

Otton powiedział, że mają zrobić czego Wiktor sobie życzy. 

Vladlena powiedziała, że Intensyfikator trzeba skazić syberionem ;p.

# Progresja

* Karolina Maus: traci Patrycję Krowiowską
* Hektor Blakenbauer: odzyskuje Patrycję Krowiowską
* Adrian Murarz: zaszczepiony na Irytkę Sprzężoną

## Frakcji

* Blakenbauerowie: tracą Adriana Murarza (tymczasowo)
* Blakenbauerowie: dostają Vladlenę i grupkę Zajcewów, którzy budują broń masowego zniszczenia Spustoszenia (i nie tylko)
* Blakenbauerowie: #5 magów "skazańców" do zbudowania oddziału Trzygław

# Streszczenie

Blakenbauerowie spróbowali uzyskać szczepionkę; jedynie stracili Adriana Murarza (zaszczepiony w Technoparku, na kwarantannie). Nie ma sensownej opcji jak to zrobić, ale dowiedzieli się, że programem kieruje Elea Maus. W międzyczasie, Projekt Trzygław dla Wiktora wymaga magów; Margaret tego nie zrobi (bo nie ma magów). Zajcewowie odbili Patrycję z rąk The Governess, Vladlena z kilkoma Zajcewami przybyła do Rezydencji Blakenbauerów. Wiktor odpalił Intensyfikator Energii Magicznej i wypalił siły Millennium ze Szpitala Pasteura. Dał Blakenbauerom 5 magów. Niech zrobią mu Trzygław.

# Zasługi

* mag: Hektor Blakenbauer, który łączył tyle interesów ile się dało, ściągnął Vladlenę i koordynował działania
* mag: Klara Blakenbauer, monitorująca akcję w szpitalu, informująca Mojrę o sprawie i ogólnie - oficer łącznikowy
* vic: Alina Bednarz, udająca maskotkę Adriana Murarza, która przez procedury nie osiągnęła wiele
* vic: Dionizy Kret, który przeżył (a plan Edwina by go doprowadził do zagłady)
* mag: Adrian Murarz, który po prostu się nie przydał... został zaszczepiony i stracony przez Blakenbauerów; atm w Technoparku.
* mag: Marek Kromlan, terminus porządkowy o dobrym sercu, który pilnuje by nic się nie stało złego w Technoparku, acz nie dopuszcza Aliny do Adriana Murarza.
* mag: Karolina Maus jako The Governess, odepchnięta i odparta przez Zajcewkomando; straciła Patrycję Krowiowską i bazę, acz niewiele więcej
* mag: Vladlena Zajcew, koordynująca działania Iriny Zajcew i Emilii Kołatki z Blakenbauerami; fizycznie w Rezydencji. Też: z kuzynami buduje broń Blakenbauerom ;-).
* mag: Mojra, wiedząca mnóstwo interesujących rzeczy o przeszłości Świecy i starająca się dodać brakujących informacji Blakenbauerom
* mag: Edwin Blakenbauer, mający fatalne plany i nieco miotający się bez wiedzy w jaki sposób poradzić sobie z całą tą sytuacją. Pała szczególną nienawiścią do The Governess.
* mag: Baltazar Maus, uwolniony i oddany pod pieczę Elei Maus; uznany za niewinnego przez przedstawicieli Świecy w Kopalinie
* mag: Malia Bankierz, osadzona w więzieniu Kompleksu Centralnego; aresztowana jako osoba próbująca skrzywdzić Baltazara Mausa
* mag: Wiktor Sowiński, który się... zmienił. Chce ratować Świecę? Na pewno podąża zgodnie z planem Spustoszenia i jest skłonny uderzyć żelazną pięścią
* czł: Patrycja Krowiowska, odzyskana przez Zajcewkomando; ma zmienioną strukturę biologiczną i jest w stazie w Rezydencji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Heksagon
                                1. Intensyfikator Energii Magicznej, odpalony przez siły Świecy przeciwko Millennium w Kopalinie
                        1. Technopark "Max Weber", przejęty przez magów do zajmowania się chorymi
                            1. Parter, gdzie mieści się dowodzenie Technoparku
                            1. Pierwsze piętro, z salami konferencyjnymi i punktami spotkań
                                1. Wielka sala konferencyjna, na 300 osób, gdzie znajduje się 120 magów chorych na Irytkę
                                1. Żółta sala, do 30 osób, gdzie mieszczą się osoby o reakcjach 'epsilon' na szczepionkę
                                1. Czerwona sala, do 40 osób, gdzie mieszczą się osoby o reakcjach 'delta' na szczepionkę
                                1. Zielona sala, do 80 osób, gdzie mieszczą się osoby o reakcjach 'gamma' na szczepionkę
                            1. Drugie piętro, gdzie mieszczą się siedziby firm biotechnologicznych i laboratoria
                                1. Firma kosmetyczna Piżmowar, biuro zarekwirowane na miejsce szczepienia magów chorych na Irytkę
                                1. Laboratorium biotechnologii przemysłowej, gdzie produkowana jest szczepionka
                    1. Dzielnica Owadów, w której znajdują się siły Millennium
                        1. Szpital Pasteura, trafiony przez Intensyfikator Kompleksu Centralnego; poważnie uszkodzone obszary "magiczne"

# Wątki

 

# Skrypt

- brak

# Czas

* Dni: 2