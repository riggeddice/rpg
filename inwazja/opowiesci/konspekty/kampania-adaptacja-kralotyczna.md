---
layout: default
categories: inwazja, campaign
title: "Adaptacja kralotyczna"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Prawdziwa natura Draceny](kampania-prawdziwa-natura-draceny.html)

## Opis

**Stan frakcji (początkowy)**:

![Kontekst frakcji na 180301](Materials/180399/campaign_adaptacja_kralotyczna_180304.jpg)

**Ogólna sytuacja**:

* Jest po wojnie z Karradraelem. Kopalin się odbudowuje. Magowie nie chcą konfliktów.
* SŚ osłabiona. Inne gildie dookoła rosną w siłę. Hipernet i kontrola magów mniej szczelne.
* Silne tendencje do Burz Magicznych i innych anomalii magicznych - za duże napromieniowanie wymagające czyszczenia.

**Potencjalne problemy**:

* Zbyt luźna struktura Millennium w powiązaniu z nietypowymi jednostkami; bardziej pro-siebie.
* Coś zostało po wojnie magów z Karradraelem. Plus, napromieniowania. Plus, trupy w szafie.
* Odbudowa i przetasowania polityczne na bazie nowego układu sił.

**Cel kampanii**:

* Eksploracja kralothów i zdolności tych cudownych istot.
* Eksploracja Millennium i problemów Millennium teraz jak nie ma Pax Świeca.
* Ożywienie Silurii i KADEMu w nowej, "lepszej" rzeczywistości.

# Historia

1. [Umarł z miłości](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html): 10/10/26 - 10/10/28
(170718-umarl-z-milosci)

Pracownicy MaksymLaw uzyskali 'koralowce' które zgubiła Krystalia. Koralowce dały im ograniczone moce magiczne. Dzięki temu pracownicy dali radę rozwiązać problem magicznego pióra (generatora energii) i dziwnych wydarzeń docelowo prowadzących do przyszłej śmierci Jolanty - jednej z prawniczek MaksymLaw. Po tym jak owym pracownikom udało się unieczynnić efemerydę i rozładować emocjonalne pole magiczne, Krystalia znalazła swoje koralowce, zdobyła pióro i odzyskała koralowce. Niesprawiedliwość.

1. [Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html): 10/11/28 - 10/12/01
(180310-kraloth-w-piwnicy)

Siluria zajęła się sprawą złowrogiego kralotha, który ma na koncie kilka martwych ludzi w Lesie Stu Kotów. Tematem zajmuje się wrogi jej terminus Świecy - Marek Kromlan. Kromlan jest kompetentny i bardziej zależy mu na ratowaniu niż tępieniu Silurii. Siluria odkryła, że w Mordowni bawi się Diakonka i ktoś prowokuje Niedźwiedzie do ataku na jubilera. Tym kimś okazała się żona jubilera, aliantka kralotha (acz nie zniewolona). Siluria pokonała tego kralotha a Karolina ewakuowała go nim pojawiła się Świeca.

1. [Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html): 10/12/03 - 10/12/04
(180311-cienie-procesu-izy)

Ku smutkowi Silurii, kraloth padł. Ku większemu smutkowi, Karolina jest zdeterminowana, by go "ożywić"... szczęśliwie, Siluria ściąga ekspertów z Millennium, Melodię i Laragnarhaga. Ścięła się też z Kromlanem, od którego wydobyła, że ów jest wrogi Silurii za to co spotkało "jego" Izę Łaniewską. Świeca poluje na Mirandę Maus - a Karradrael podobno nie może jej ściągnąć. Siluria poprosiła Whisper o pomoc Kopidołowi, który widzi dziwne efekty pryzmatyczne w Mare Felix powiązane z Lasem Stu Kotów.

1. [Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html): 10/12/05 - 10/12/07
(180318-czyzby-drugi-kraloth)

Siluria najpewniej nie była morderczynią kralotha. Ona była narzędziem zabójstwa; kraloth miał szok związany z INNYM kralothem. Plasterek Bójki? Dodatkowo, coś niepokojącego dzieje się na Mare Felix - odbija nie tylko Millennium i Arazille, odbija coś jeszcze. Siluria dodatkowo zapewniła dla Marii miejsce w Millennium i zdewastowała Żupana, który chciał się zabawić z Bójką. A tu - klops, Maria uciekła do Czelimina a Kromlan nie chce od Silurii podarunków. Aha, no i niefortunny Paradoks tworzący dziwny wibrator w Mordowni...

1. [Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html): 10/12/08 - 10/12/09
(180321-porwanie-pod-nosem-hipisow)

Edwin ściągnął Alinę, by ta wyrwała z Czelimina Marię Przysiadek. Alina ściągnęła Siergieja - alkoholika odporniejszego na Arazille niż przeciętny człowiek. Czelimin jest domeną Arazille jak Żonkibor - z faboklami i Pryzmatem. Siergiej nawiązał kontakt z Amelią Eter i ta, po rozmowie z nim i Aliną, pomogła wyciągnąć Marię by Edwin mógł ją uratować od losu kralothborn-bez-kralotha lub elementu Domeny Arazille. Siergiej spojrzał w Srebrne Lustro i został w Czeliminie jakiś czas.

1. [Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html): 10/12/08 - 10/12/10
(180327-szept-mare-felix)

Whisperwind zbudowała symulację na Mare Felix by odnaleźć co stało się z Hralglanathem. Udało jej dotrzeć sie do agentów Drugiego Kralotha i odnaleźć, jak padła do kralotha Bójka Diakon (o czym nikt nie wiedział). Niestety, przy okazji, nieświadomie, dała przyczółek Kralotycznym Kwiatom na Mare Felix...

1. [Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html): 10/12/13 - 10/12/15
(180402-petla-dookola-niekralotha)

Okazało się, że Bójka jest pod kontrolą Drugiego Kralotha. Zniknęła. War Room KADEMu i Melodia / Laragnarhag wykazali, że Bójka jest bezpieczna... najpewniej posłuży jako matka dla kralotha. Sam Drugi Kraloth nie jest już kralothem, jest Czymś Innym, za daleko zaadaptował. Idąc dalej, Siluria doszła do tego, że Miranda Maus gra na zniszczenie Mausów / uwolnienia się od Karradraela. Maria ma problemy przez policję ORAZ prokuraturę magów; Łajdak obiecał się tym zająć. Ze wszystkim chyba powiązany jest lokalny Klasztor - Klasztor Zrównania w Lesie Stu Kotów.

1. [Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html): 10/12/13 - 10/12/15
(180411-zlodzieje-niewaznych-artefaktow)

Złodziejami nieważnych artefaktów okazały się dwa larghartysy, kralothspawny należące do Drugiego Kralotha. Połączone siły Maurycego Mausa, Darii Rudas oraz Estrelli Diakon dały radę złapać jednego z nich, co stanowi niezły postęp do znalezienia Drugiego Kralotha.

1. [Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html): 10/12/17 - 10/12/19
(180526-krystalia-poluje-na-niekralotha)

Krystalia weszła do Jodłowca, by przekształcić ludzi w żywe przynęty dla kralotha. Silurii udało się odwrócić jej uwagę, ale nie dość. Krystalia zlokalizowała niekralotha i powiedziała kilka nowych rzeczy; jednak Elea pomagając ludziom szkodziła planom Krystalii. Krystalia poszła ją wyłączyć z akcji (z powodzeniem), po czym wpadli terminusi Kromlana i Strąka oraz unieszkodliwili Krystalię. A Bruniewicz poszedł do Klasztoru i ślad po nim zaginął.

1. [Śledzie na Fazie](/rpg/inwazja/opowiesci/konspekty/180619-sledzie-na-fazie.html): 10/12/02 - 10/12/04
(180619-sledzie-na-fazie)



## Progresja

|Misja|Progresja|
|------|------|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) sławna w odpowiednich obszarach jako osoba, która zdominowała kralotha przez łóżko.|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html) potrzebuje dostępu do leków kralotycznych, albo umrze.|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html) obwiniony o wszystkie kralotopodobne wydarzenia w okolicy od pewnego czasu.|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) ma dostęp do biosyntezatora kralotycznego (kiedyś: Hralglanatha).|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|KADEM dostęp do wszystkich ofiar kralotha (Hralglanatha).|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|KADEM dostęp do biosyntezatora kralotycznego (kiedyś: Hralglanatha), normalnie w rękach Silurii Diakon|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html) KIA. Jego ciało to biosyntezator dla Silurii.|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html) Dotknięta Wolnością Arazille; wie, że w Czeliminie jest bezpieczna. Nie wie czemu to wie.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html) zdobyła informacje odnośnie Mare Felix, które pomogą zrozumieć co dzieje się w Lesie Stu Kotów.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html) zdobył informacje odnośnie Mare Felix, które pomogą zrozumieć co dzieje się w Lesie Stu Kotów.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html) pozyskał informacje od martwego Hralglanatha odnośnie tego, czemu Arazille i czemu kraloth nie skrzywdził Marii.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) pozyska (przez przypadek) broń osoby, która dużo znaczy dla Kromlana od Mariana Łajdaka.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html) pogorszył sobie reputację w Rzecznej Chacie, bo wydarł się na Silurię o Infensę.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html) wysunęła się na czoło "Wolnych Mausów", daje im inspirację i nadzieję w niewoli Karradraela (Abelarda).|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|Ogniste Niedźwiedzie walka pomiędzy Filipem Czółno a Romanem Bruniewiczem o władzę.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|Wolni Mausowie uznali Mirandę za swoją nieformalną przywódczynię a Karradraela za nie dość silną istotę.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|Ogniste Niedźwiedzie sprzęgło się z nimi dziwne różowe dildo, artefakt stworzony przez Silurię Paradoksem. Łączy Niedźwiedzie i Bójkę.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html) publicznie zdominowany przez Silurię. Boi się Silurii. Nienawidzi Silurii. Jego reputacja w Mordowni Arenie Wojny uległa poważnemu uszkodzeniu.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|Domena Arazille kontroluje Czelimin; obecność Arazille jest tam wszechobecna. Czelimin przebija się na Mare Somnium, jak Żonkibór.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html) beznadziejnie uzależniona od plasterków kralotycznych, mimo, że jeszcze o tym nie wie.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|Ogniste Niedźwiedzie better or worse, ale Bójka zaakceptowała tą frakcję jako swoją przez te przeklęte plasterki.|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) Artur Żupan się jej boi|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Lucjan Kowalkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kowalkiewicz.html) z uwagi na obcość biologiczną i mentalną, potrafi poruszać się po Fazie Daemonica nie generując Pryzmatycznego Skażenia|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html) wycofała się do Drugiego Kralotha jako jego oddana agentka; gotowa do spawnowania nowego kralotha|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) uzyskał Bójkę w stanie nadającym się na zbudowanie nowego kralotha.|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) rozprzestrzenił swoje Kwiaty na Fazie Daemonica.|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Melinda Słonko](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-slonko.html) po zakończeniu tej kampanii i odzyskaniu jej przez magów skończy nie jako kralothborn a jako mag|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|Zielony Kwiatek po tym, jak Lucjan wpierniczył Ognistym Niedźwiedziom ta organizacja wzrosła w siłę i w szacunek w okolicy.|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Lucjan Kowalkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kowalkiewicz.html) doszło do fuzji jego umysłu i ciała konstruminusa Świecy. Jest już konstruminusem|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Janek Malczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-malczorek.html) Skażony przez Glarnohlagha mocą Melindy i Bójki; cichy agent Klasztoru Zrównania.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html) zsynchronizowała się ze Srebrnym Lustrem Arazille; może go swobodnie używać a lustro nie robi jej krzywdy.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html) trafiła na KADEM, pod opiekę Edwina i Norberta.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html) przebudziły się w niej geny kralothborn. Dotknęła ją moc Arazille. W tej chwili tylko forma i umysł jest u niej ludzka; wszystkie instynkty są obce.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html) okazuje się, że jest niewrażliwa na pieśni fabokli. Z jakiegoś powodu Arazille jej nie Dotyka.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|Domena Arazille odbicia Damy w Czerwonym pojawiają się też w Mare Somnium.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|Domena Arazille ma około 30 fabokli w Czeliminie, kapłana (Zachradnika) i ogólnie potężny hold w Czeliminie.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html) jest kluczem do zrozumienia odbić Damy w Czerwonym, choć nie wie dlaczego i o co chodzi.|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html) za dwa dni zostanie złapany przez Drugiego Kralotha (gwarantowana przyszłość)|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) rozprzestrzenił się na Fazę Daemonica|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html) poszerzenie opinii niszczycielki. To ona pomogła w złapaniu larghartysa. Jej niszczycielstwo jest epickie i bardzo przydatne|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html) jakkolwiek złapała pierwszego larghartysa, nie złapała drugiego i przez to ucierpiał Maurycy Maus. Bittersweet; bardziej bitter.|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|[Alegretta Tractus](/rpg/inwazja/opowiesci/karty-postaci/9999-alegretta-tractus.html) ZAWSZE wie, gdzie iść by dostać się do Maurycego Mausa|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) wpakuje się jakoś w Dziwne Dildo Silurii w Mordowni Arenie Wojny (guaranteed future)|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html) spawnuje nowego kralotha (guaranteed future)|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Patrycja Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-patrycja-widoczek.html) nie stanie się permanentnym viciniusem; ma opcję powrotu (guaranteed future)|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) Bójka spawnuje mu nowego kralotha (guaranteed future)|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html) rozczarowana Silurią; Siluria może i wszystkich ostrzegała, ale jednak nie stanęła po właściwej stronie - przeciw Krystalii|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html) polubił Eleę Maus - jest słonna dużo poświęcić dla bezpieczeństwa ludzi, stanęła na linii ognia Krystalii|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|Millennium ma zdecydowanie pogorszoną opinię przez Krystalię. Co jest warta gildia nie pilnująca swoich członków?|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html) bardzo podpadła Prokuraturze Magów; jest uznawana za groźniejszą niż Karolina Maus|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html) Marek Kromlan w końcu ją polubił. Nie jest dla niego "Mauską" a "Eleą, która ryzykuje dla ratowania ludzi"|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) Marek Kromlan osłonił ją przed prokuraturą magów (sprawy z Krystalią).|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) Elea jest nią rozczarowana, bo nie stanęła przeciwko Krystalii (czyt. była rozważna politycznie)|

## Plany

|Misja|Plan|
|-----|-----|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) dowiedzieć się, o co chodzi Markowi Kromlanowi? Czemu tak nie lubi Silurii? A czemu Diakonów? I Mausów (dobra, to jasne)?|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) dowiedzieć się o historii Hralglanatha i Marii Przysiadek. O co w tym wszystkim chodziło?|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html) chce zrozumieć, co stało się nieszczęsnemu Hralglanathowi - i czy to może pójść szerzej na inne kralothy.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html) chce zrozumieć, co dzieje się na Mare Felix i jak może to zatrzymać (uważa za niekorzystne).|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) chce zdobyć ceremonialną broń kirasjera Rusznicy, by lekko wstrząsnąć Kromlanem.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) chce pomóc Marii Przysiadek i zrozumieć o co chodzi z nią i Arazille.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) chce doprowadzić do zetknięcia się Marka Kromlana i Infensy Diakon.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html) bardzo zdeterminowana, by ALBO wskrzesić ALBO pochować Hralglanatha - ale na pewno nie trzymać go w tym stanie.|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|Spero Candela Szlachta 2.0, chce pozyskać Marka Kromlana jako przedstawiciela Właściwych Myśli.|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|[Mariusz Kurzyciel](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-kurzyciel.html) ma zamiar poważnie siąść do tematu eksploatacji kobiet przez mężczyzn i nawrócić wszystkich w Kolcie. Jak trzeba, siłą.|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html) chce się spotkać z Amelią Eter ponownie. Jakoś. Obudziła się w nim anarchistyczno-poszukiwawcza dusza. Wanderlust.|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|Prokuratura Magów przyjrzeć się tematowi Marii Przysiadek i Hralglanatha. Nie dopuścić do złamania Maskarady. Odbudować życia zniszczone przez kralotha.|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|Ogniste Niedźwiedzie przeprowadzić działania przeciwko Klasztorowi Zrównania. Mały Terror Site.|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html) poprowadzić Ogniste Niedźwiedzie do Klasztoru Zrównania. Wyjaśnić im, że tak się nie godzi przez mały Zastrasz.|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html) ostrzec Silurię przed tym, że Krystalia próbuje znaleźć Bójkę. Krystalia może Silurii bardzo napsuć.|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Patrycja Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-patrycja-widoczek.html) nie chce odejść z Ognistych Niedźwiedzi. Chce je zmienić na "dobry gang".|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html) ktoś nadużył jej dobrego imienia. Osobiście zaangażuje się w znalezienie Bójki, bogowie i magowie be damned...|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Glarnohlagh](/rpg/inwazja/opowiesci/karty-postaci/9999-glarnohlagh.html) jest zmuszony do natychmiastowego działania przez Krystalię; pętla dookoła niego się zaciska.|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html) chce znaleźć dowody na zło Krystalii Diakon by móc ją porządnie oskarżyć i skazać; nie jakieś byle co.|

