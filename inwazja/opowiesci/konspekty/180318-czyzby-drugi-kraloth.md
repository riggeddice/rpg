---
layout: inwazja-konspekt
title:  "Czyżby drugi kraloth?"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180311 - Cienie procesu Izy](180311-cienie-procesu-izy.html)

### Chronologiczna

* [180311 - Cienie procesu Izy](180311-cienie-procesu-izy.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

**Wizja artysty**:

brak

**Strony konfliktu, atuty i słabości**:

![Strony, atuty, słabości](Materials/180399/180318-story.png)

### Wątki konsumowane:

brak

## Punkt zerowy:

## Mapa logiczna Opowieści:

brak

## Pytania Generatywne:

* Ż: 
* K: 

## Potencjalne pytania historyczne:

* Czy Maria odda się pod opiekę Arazille? (TAK: 5)
* Czy Maria zostanie napadnięta i potraktowana plasterkiem? (TAK: 5)
* Czy Melodii i Laragnarhagowi uda się zaprojektować JAKĄŚ formę regeneracji? (TAK: 10)
* Czy Miranda zakłóci jednowładztwo Abelarda swoim przykładem? (TAK: 10)

## Misja właściwa:

**Dzień 1**:

Siluria rankiem. Piękny poranek, poza tym, że Karolina nadal kombinuje z kralothem - tym razem siedzi z Andżeliką w bibliotece. Nic nie ROBI ale SZUKA. To niebezpieczne dla KADEMu. Szczęśliwie, Quasar sabotuje wszelkie próby DZIAŁANIA. Szczęśliwie, dzięki Quasar nic z tego nie będzie a następnego dnia powinni pojawić się Laragnarhag i Melodia. Więc... jakkolwiek uciążliwe, Siluria może z tym żyć.

Siluria poszła do Edwina powiedzieć mu o tym, że ma tych speców. Edwin powiedział Silurii, że zna przyczynę śmierci - szok anafilaktyczny związany z innym, morderczym kralothem. To może być ten plasterek - ale Siluria mówi, że NIE MA MOWY - więc jest gdzieś INNY kraloth. I ten inny kraloth doprowadził do psychozy tego biedaka. Nie do poddaństwa. Owszem, to jest możliwe z wiedzy Silurii, ale PO CO? Ten konkretny kraloth został zniszczony wcześniej. Plasterek - z energią kralotyczną - jedynie to wzmocnił co już było i dobiło resztki umysłu Hralglanatha.

Optymistyczne. Siluria jako narzędzie zbrodni na kralocie.

Jest jeszcze coś. Edwin doszedł do tego dlaczego Maria przeżyła i czemu jej nie zaatakował. Ona jest z krwi Hralglanatha. Ona nie jest człowiekiem, ona jest kralothborn. Siluria zauważyła, że to wymaga przekazania do Millennium. Maria jest SŁABYM kralothborn, z lekką tylko augmentacją, ale jest kralothborn. I to ciekawe - jest autonomicznym kralothborn. Nie chciał jej dominować, nigdy. Ten kraloth zachowywał się inaczej niż przeciętny. Inna socjalizacja? Laragnarhag się przeewoluował by pasować do Millennium, ale skąd się wziął Hralglanath w takim razie? Zwłaszcza, że najpewniej Hralglanath był tu przed Arazille - i się jej wyraźnie nie bał.

Siluria stwierdziła, że Hralglanath może nie wiedział o Arazille. Edwin skontrował - to ON kazał Marii iść z tą kobietą (Klaudią) do Czelimina.

Więc - czy miał coś do magów? Nie chciał dominować ludzi, do momentu psychozy. Ale do Silurii zadziałał agresją. Edwin powiedział, że tego nie wie. Nie jest w stanie określić. Siluria się uśmiechnęła - może warto powiedzieć Kromlanowi, że w okolicy jest gdzieś DRUGI kraloth i to nie tak sympatyczny.

Edwin westchnął jeszcze, bo Ogniste Niedźwiedzie mu się rozłażą. Nic nie wie (bo jest zamknięty na KADEMie Daemonica, bo prokuratura magów), ale wie, że tam Coś Się Dzieje. I martwi się, bo jest tam Bryś, któremu Edwin jest coś winny i jest protovicinius. Gdy Siluria powiedziała że tam jest Bójka Diakon i stamtąd ma plasterek, Edwin się zmartwił - może być kolizja na linii Paulina Widoczek vs plasterek; może coś się uruchomić. Edwin poprosił Silurię, by ta coś zrobiła z Millennium - niech te plasterki tam nie będą. On sam poprosi Marcelina - może uda się Paulinę Widoczek jakoś odepchnąć z tego miejsca na jakiś czas.

Siluria powiedziała, że Paulina ma tam faceta. Szefa Niedźwiedzi.

* Ona nie odejdzie bez niego. On nie odejdzie bez swoich Haribo. Noż to cholerna tragedia Sofoklesa! - Edwin, z niesmakiem

Czas odwiedzić Whisperwind. I Whisperwind siedzi w Układzie Nerwowym KADEMu, rozplanowując dalsze działania, w Salach Symulacji. Powiedziała Silurii, że to połączenie trzech głównych emocji w Mare Felix, których tam nie było - Nienawiść, Szlachetny Gniew i taka... Frustracja, Stłumienie. Siluria zauważyła, że jeśli to wpłynęło na Pryzmat, to musi być potężne. Kopidół potwierdził. W czym Whisper zauważyła, że w Mare Felix przebijają się też sygnały Arazille, co nie ułatwia. Niestety, Mare Felix jest przez to niebezpieczne do eksploracji.

Zdaniem Whisper, wygląda to na coś PRZEMYŚLANEGO acz niekoniecznie ZORGANIZOWANEGO. Kopidół dodał, że te uczucia są wyraźnie zaprojektowane. Jak Whisper zakwestionowała, wycofał się. No tak to dla niego wygląda.

Mare Felix zwykle "odbija" Millennium, bo w Lesie Stu Kotów większość baz ma Millennium. Ale jakiś konkretny prąd musiał złapać Coś Innego i przez pewien czas Mare Felix będzie odbijać to Coś Innego. Acz zgodnie zarówno z Kopidołem jak i Whisperwind - to tu było od dawna, tylko się nie odbiło. Coś się stało, że jakiś prąd to porwał i odbił, bo normalnie Mare Felix odwierciedla walkę między Arazille a Millennium. Kopidół zaproponował, by Whisper spojrzała na Mare Somnium. Whisper ABSOLUTNIE zaprotestowała. To domena Arazille. 

Siluria poprosiła Whisper o sprawdzenie jednego miejsca na Fazie Daemonica. Czerwoną Bibliotekę. Wiemy KIEDY i GDZIE ale nie CZEMU. Czy Whisper może sprawdzić, czemu zaczął zabijać? Whisper powiedziała, że skoro kraloth jest na KADEMie, może złapać kawałek kralotha i iść za śladem. Kopidół powiedział, że ważniejsze jest sprzężenie Mare Felix i Lasu Stu Kotów i obserwacja przez czujniki. Ale JAK? Siluria i Whisper zauważyły, że nie uda się tego tak zrobić - za dużo pracy. Kopidół powiedział, że pomyśli. Porozmawia z Jankiem jak to ruszyć. I Warmastera poprosił o pomoc. Whisper się żachnęła, ona woli sprawdzić Czerwoną Bibliotekę Daemonica. Musi tylko określić w którym Mare ta się znajduje.

Ok...

Jeśli jakiś kraloth doprowadził Hralglanatha do takiego stanu, Millennium może być mocno zainteresowane. Bo czemu... to okrutne, nieludzkie, niekralocie i niepotrzebne. A Maria jako kralothborn potrzebuje JAKIEGOŚ kralotha albo naprawy. Ona biologicznie potrzebuje "leków" jak to nazwała. Nie jest w stanie defilersko sama tego syntezować... chyba. Nie to stadium.

Primus i kontakt do Netherii... nie ma wyjścia. Neti będzie gotowa spotkać się w Rzecznej Chacie za 2-3 godziny. Siluria poprosiła ją o dwie dawki obezwładniaczy kralothów, na wypadek jakby chciała Silurii je dać. Netheria się zgodziła. Niech tak będzie.

Cóż, Siluria zdecydowała się pojechać do Mordowni Areny Wojny, spotkać się ze swoją Bójką. Mordownia, gdzie stoi Kret przy barze. Poznał Silurię od razu i zaprosił, szczerząc się. Siluria jest ubrana adekwatnie jak na Mordownię, jak ostatnio. Zeszła na dół, spotkać się z Bójką a tam... Żupan. Podwójne zdziwienie. Żupan od razu przeszedł do konkretów. Niech Siluria pozbędzie się bluzeczki i najlepiej idzie na kolana.

ESCALATION ZOOM IN (2, 2, 2):

Siluria zaczyna po trochu zdejmować bluzeczkę, ale przy okazji łapie linkę. Idzie w pozorny bondage, by go złapać... Żupan złapał ją za ramiona i zmusił do uklęknięcia, Siluria poszła w "I accept" ale związać mu ręce.

* Żupan: +2 (dirty fighting) + 2 (motywacja) -> 4
* Siluria: +2 (zaskoczenie) +2 (narzędzie) +4 (tyranka, świta) + 4 (bondage, łóżko) -> 12

Żupan jest zaskoczony; szybkim ciosem kopnął Silurię prosto w brzuch, by się oderwać. Siluria przyjmie w biust. Ale nie puszcza linki. Chce go przewrócić. 

* Żupan: +2 (taktyczna pozycja) +4 (dirty, zaskoczenie) +4 (siła, bezwzględne posłuszeństwo) -> 10
* Siluria: +2 (linka) +2 (presja czasu na Żupanie) +2 (silna strona) +2 (motywacja) +2 (umiejętność walki z progresji) -> 10

Siluria uniknęła uderzenia i pociągnęła Żupana na ziemię. Ciornęła go o grunt; błyskawicznie przetoczył się na plecy, po czym wstał - ale on jest związany a Siluria nie.

* Czego chcesz... - Żupan z nienawiścią
* Ja? Na kolana i ją wyliż, za ostro wszedłeś - Siluria, walcząca o dominację
* ...chłopcy! Chodźcie tu, nauczyć ją moresu! - Żupan, do swoich ludzi
* ...serio... - Siluria
* Przytrzymajcie szefowi lalunię, bo się rzuca - Siluria parskając śmiechem do thugów Żupana.

* Żupan: +2 (wataha) +2 (chciwość) +2 (zabawa) +2 (wódz) +2 (teraz) -> 10
* Siluria: +2 (ośmieszenie) +2 (albo pomogą jemu albo zabawa życia) +4 (umiejki) +2 (tyrania) +2 (na szczycie) -> 12

Skonfliktowany Sukces. Działania Silurii skanalizowały ruchy Filipa Czółno. Filip, który tam też jest, spytał szefa, czy sobie ten serio nie poradzi z Silurią? Siluria skorzystała z okazji i podeszła doń z nożem i kazała mu przyklęknąć i zadowolić językiem Bójkę. Żupan to zrobił, niechętnie. Siluria kazała ludziom zawiązać linkę, po czym dołożyć sobie strapona. I Żupan zadowala Bójkę, Siluria mu jedzie straponem a jego thugi mają Bójkę z przodu.

Gdy Żupan bardzo piszczy i cierpi bo boli, dostaje plasterek z łaski Silurii. I tak się kończy opowieść o bardzo dominującym Żupanie w Mordowni Arenie Wojny. I historia jego jako poważanego "człowieka" w tym miejscu.

Siluria miło spędziła czas.

Bójka jest mało używalna i są tu ludzie (i rozwalony Żupan). Więc Siluria złapała Brunowicza. Ma przekazać Bójce liścik jak ta wyjdzie z mgły rozkoszy. Brunowicz zaznaczył, że to będzie wieczór. Siluria machnęła ręką, poszła do niej z powrotem. Ta jest zajęta z randomowym Niedźwiedziem. Siluria machnęła ręką, nie ma czasu. Walnęła mu porządne zaklęcie orgazmiczne. Udało się... prawie. Z zaklęcia Silurii skrystalizowało się (materia + mentalna + Coś Dziwnego) Paradoksalne Spore Dildo. Różowe. I klapło na ziemię. Gościu leży, koło niego dildo. Siluria odtruła Bójkę; ten proces niestety jest bolesny.

* Kuzynko... dlaczego...  - Bójka we łzach
* Bo muszę z tobą pogadać jak kumasz. Potem znowu weźmiesz plasterek - Siluria, z uśmiechem
* Oj... to jest na granicy wytrzymałości fizycznej mojego ciała - zmęczona Bójka

Bójka spytała Silurię, co w związku z tym (Pauliną Widoczek) ma zrobić. Siluria nie wie. Bójka powiedziała, że nie chce robić krzywdy tym ludziom, ale ich polubiła. Nie chce odejść... ale nie chce ich skrzywdzić. Zapytana przez Silurię odnośnie tego co tu się dzieje, wyjaśniła - jest tu power struggle. Widoczek chce odejść i wziąć Brunowicza, jest nuworysz (Czółno) który chce to wszystko przejąć. Może ona zrobi sobie krótką przerwę od tych plasterków...

* Użyjesz tego paradoksalnego dildo na mnie? - Bójka z nadzieją
* Serio..? - Siluria, zaskoczona
* W tym stanie..? - Bójka
* ... - Siluria
* Może zaszkodzić koralowcom... Rany, mam najgorszą robotę na świecie! - Bójka, podłamana, zanim plasterek ją zabrał ze sobą

Siluria wypchnęła dildo jak najdalej do szafy by nikt go nie znalazł. Sama... pojechała do Netherii. Lekko podłamana. Z Netherią spotkała się już późnym wieczorem, w Rzecznej Chacie.

Siluria powiedziała Netherii, że Hralglanatha załatwił inny kraloth - przez psychozę. Netheria jest niepocieszona. To wymaga dużej dysproporcji sił między kralothami...  gdy Siluria powiedziała o nieostrożnym terminusie Świecy (Kromlanie), Netheria się podłamała. Gdy Siluria powiedziała, że mają na terenie początkującego kralothborn, Neti powiedziała, że Millennium ją przyjmie. Alternatywą jest śmierć lub - jak się da - regenesis.

Niestety, kralothborn jest w rękach Świecy.

Usłyszawszy o dziwnych decyzjach Hralglanatha (wysłanie ludzi do Arazille, nie pętanie kralothborn Marii), Netheria się zainteresowała tym viciniusem. Ciekawe co z nim było i skąd się wziął. Siluria się zgodziła, że sama chciałaby wiedzieć...

Siluria wysłała jeszcze tylko sygnał do Kromlana "uważaj na groźnego kralotha w Lesie Stu Kotów, nie wiem gdzie jest i w ogóle". A potem, zmęczona, poszła spać.

**Dzień 2**:

Przy okazji, od rana, Infensa została poproszona by być w okolicy na wypadek, gdyby Coś Się Działo. Bo ona zna Kromlana. Infensa spytała, czego oczekuje Siluria - ano, oczekuje tego, by Infensa mogła... tam być jakby co. Infensa zauważyła, że Kromlan będzie jej nienawidził, bo Infensa jest NIE Izą. Siluria zapytała, czy INFENSA jest przyjaciółką Kromlana. Infensa nie wie. Może być. Ale czy jest?

Siluria pojechała potem do Piroga odwiedzić Eleę. Elea odpowiedziała sygnałem, że ma trudny przypadek. Siluria stwierdziła, że ma to gdzieś. I tak do niej pojedzie. Przyjechała do Elei. Ta chłodno zauważyła, że ma trudny przypadek.

* Mój trudny przypadek to właśnie Maria - Elea
* ...dlatego muszę porozmawiać z Kromlanem. Wezwiesz go tu? - Siluria
* ...ale SKĄD wiedziałaś o tym, że Maria ma trudny przypadek? - Elea, odnośnie Wstrząsu Kralotycznego
* Just KADEM things - Siluria, odnośnie tego że Maria jest kralothborn... o czym nie wie Elea

Elea po skontaktowaniu się z Kromlanem powiedziała, że Kromlan przyjedzie za kilka godzin. Jest czymś zajęty. Siluria facepalmowała - on wpadnie w łapy kralotha czy coś. I na to weszła Judyta. Siluria była świadkiem ostrej kłótni między czarodziejkami. Judyta jest zwolenniczką wolnej woli, Elea twierdzi, że Miranda jest największym zagrożeniem dla rodu Maus w historii:

* Jeśli faktycznie Karradrael jej nie rozkazuje (bo nie ma siły), Abelard nie jest prawdziwym seirasem. Miranda powinna nim być... ale to nie ma sensu.
* Jeśli Abelard nie chce jej wydać, Mausowie są zagrożeni ze strategią uległości.
* Nie wolno odmawiać woli Karradraela.

Tak czy inaczej, Elea uważa, że Miranda jest niebezpieczna. A Judyta jest wyraźnie w pro-Mirandowym obozie. Siluria zauważyła, że Judyta jest takim cichym kurierem na którego nikt nie zwraca uwagi. Po nie takiej krótkiej dyskusji na tematy filozoficzne, etyczne itp. Judyta pojechała, niezadowolona. Elei zdania nie zmieniła.

Elea chce iść do pacjentki. Siluria ją zatrzymała - Maria to kralothborn. Elea stwierdziła, że plasterki uderzyły wszystkich członków jej rodziny, ale Maria jest kralothborn - miała Wstrząs Kralotyczny. Więc inni wydobrzeją łatwiej. Gdy Elea poszła ją zobaczyć - okazało się, że Maria uciekła. Kilkanaście minut temu...

Siluria poprosiła o krew Marii i ją dostała od Elei. Siluria chce stały tracker - chce wiedzieć gdzie jest dokładnie i jak daleko. Z krwią to nie takie trudne. Test Trudny (10). Siluria ma wartość 8 + 2(krew). Skonfliktowany Sukces. Udało się znaleźć Marię - ale ona już zdążyła dotrzeć do Czelimina. I Siluria też dostała tą informację. Ma też możliwość komunikacji z Marią - przez starą dobrą komórkę, bo z niej zrobiła ten tracker...

Elea powiedziała, że może pomóc wszystkim obecnym... a Kromlan będzie niedługo. Dobra, Siluria wysłała wiadomość do Edwina... niech on wyśle Ogniste Niedźwiedzie, by porwały dla niego Marię. Edwin facepalmował, gdy Siluria wyjaśniła mu całą sytuację. Edwin obiecał, że spróbuje ją porwać swoimi siłami...

A Paulina zadzwoniła do Marii. Maria powiedziała parę ciekawych rzeczy:

* Hralglanath kazał jej zaprowadzić Klaudię do Czelimina, nawet pod nożem. Bo to pomoże jej i Grzegorzowi.
* To faktycznie pomogło, z tego co mówi Maria.
* Dwa dni później Hralglanath był psychotyczny i był w bibliotece.
* Zdaniem Marii, Hralglanath kazał jej uciec właśnie do Czelimina bo tam "oni" nie przyjdą.
* Zdaniem Marii, Hralglanath nie skrzywdził ani Klaudii ani Grzegorza.
* Zdaniem Edwina, Klaudia zginęła zabita przez Grzegorza pod wpływem kralotycznym. Ten drugi kraloth?

I kolejna prośba do Edwina - zidentyfikować wpływ. KTÓRY kraloth wpłynął na Grzegorza, by ten zabił Klaudię.

W końcu pojawił się Marek Kromlan, z dwoma konstruminusami. Wszedł do Elei. Wyjaśnił, że był zajęty poszukiwaniami Mirandy - miał sygnał z lasu. Marek się zdziwił, że Maria uciekła i że w ogóle w jej domu doszło do takiej sytuacji, ale zrozumiał kontekst - Baltazar odwołał konstruminusa, którego Kromlan zostawił do pilnowania Marii. Marek postara się wyciągnąć Marię z Czelimina... jakoś.

Na to weszła Siluria. Marek się wyraźnie spiął; nie chciał rozmawiać. Ale nie chodziło o niego - chodziło o Marię, więc się przemógł. Siluria przejęła od niego Marię i odpowiedzialność przyszłą za nią; wydobędzie ją z Czelimina i KADEM spróbuje jej pomóc. Kromlan się zgodził, acz z ciężkim sercem. Siluria ostrzegła go jeszcze, że jest tam gdzieś jeszcze jeden kraloth; to najpewniej on zabił Hralglanatha (a Siluria była po prostu narzędziem zbrodni).

Siluria zaprezentowała Kromlanowi broń, którą chciała dać mu w prezencie na przeprosiny. Kromlan spytał, czy to KADEM czy ona chce dać mu broń. Przeprosiny czy dar. Siluria powiedziała, że chciałaby - ona - zacząć od nowa. Kromlan powiedział, że nie może przyjąć, póki Silurii nie pozna lepiej. Siluria zaakceptowała takie coś, w przyszłości może się to zmieni.


## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (?)
    1. Bójka Diakon jest beznadziejnie uzależniona od plasterków kralotycznych.
    1. 
    1. 
1. Kić (?)
    1. 
    1. 
    1. 

# Streszczenie

Siluria najpewniej nie była morderczynią kralotha. Ona była narzędziem zabójstwa; kraloth miał szok związany z INNYM kralothem. Plasterek Bójki? Dodatkowo, coś niepokojącego dzieje się na Mare Felix - odbija nie tylko Millennium i Arazille, odbija coś jeszcze. Siluria dodatkowo zapewniła dla Marii miejsce w Millennium i zdewastowała Żupana, który chciał się zabawić z Bójką. A tu - klops, Maria uciekła do Czelimina a Kromlan nie chce od Silurii podarunków. Aha, no i niefortunny Paradoks tworzący dziwny wibrator w Mordowni...

# Progresja

* Siluria Diakon: Artur Żupan się jej boi
* Artur Żupan: publicznie zdominowany przez Silurię. Boi się Silurii. Nienawidzi Silurii. Jego reputacja w Mordowni Arenie Wojny uległa poważnemu uszkodzeniu.
* Bójka Diakon: beznadziejnie uzależniona od plasterków kralotycznych, mimo, że jeszcze o tym nie wie.
* Miranda Maus: wysunęła się na czoło "Wolnych Mausów", daje im inspirację i nadzieję w niewoli Karradraela (Abelarda).

## Frakcji

* Domena Arazille: kontroluje Czelimin; obecność Arazille jest tam wszechobecna. Czelimin przebija się na Mare Somnium, jak Żonkibór.
* Ogniste Niedźwiedzie: sprzęgło się z nimi dziwne różowe dildo, artefakt stworzony przez Silurię Paradoksem. Łączy Niedźwiedzie i Bójkę.
* Ogniste Niedźwiedzie: better or worse, ale Bójka zaakceptowała tą frakcję jako swoją przez te przeklęte plasterki.
* Ogniste Niedźwiedzie: walka pomiędzy Filipem Czółno a Romanem Bruniewiczem o władzę.
* Wolni Mausowie: uznali Mirandę za swoją nieformalną przywódczynię a Karradraela za nie dość silną istotę.

# Zasługi

* mag: Siluria Diakon, zdominowała Żupana, znalazła dla Marii potencjalną przyszłość w Millennium i powoli rozwiązuje tajemnicę drugiego kralotha - też, martwi się Mare Somnium i Mare Felix.
* mag: Quasar, sabotuje wszelkie próby wskrzeszania kralotha mogące zagrozić KADEMowi (czyli wszystkie!).
* vic: Hralglanath, nadal nie żyje; okazuje się, że miał bardzo nietypowy psychotyp jak na kralotha. Chciał być pomocny a nie dominować? Nie jest to zrozumiały kraloth.
* vic: Maria Przysiadek, okazuje się że jest kralothborn ze krwi Hralglanatha. Po ataku Ognistych Niedźwiedzi na jej dom miała atak psychotyczny od "plasterka"; uciekła do Czelimina.
* mag: Edwin Blakenbauer, który wyciągnął przyczynę śmierci Hralglanatha i martwi się o Paulinę Widoczek oraz Ogniste Niedźwiedzie. Obiecał Silurii, że odzyska Marię z Czelimina.
* mag: Whisperwind, zmartwiona stanem Mare Felix. Chce znaleźć odbicie pryzmatyczne Czerwonej Biblioteki, by zobaczyć przeszłość - jak zginął Hralglanath. Co tam się stało.
* mag: Lucjan Kopidół, który ma mnóstwo nieracjonalnych pomysłów jak zbadać powiązanie między Lasem Stu Kotów a Mare Felix. Cenny analityk odnajdujący dziwność na Fazie Daemonica.
* mag: Netheria Diakon, przyjmie z przyjemnością Marię do Millennium, by jej pomóc zregenerować życie w społeczeństwie.
* mag: Bójka Diakon, uzależniła się od plasterków kralotycznych. Punkt centralny walki między Silurią a Arturem Żupanem.
* mag: Artur Żupan, chciał zabawić się z Bójką a skończył pokonany przez Silurię, na czworakach.
* mag: Elea Maus, próbuje wszystkich uratować i twierdzi, że Miranda Maus jest strasznym zagrożeniem dla rodu przez to, że pokazuje słabość Abelarda / Karradraela.
* mag: Judyta Maus, młoda bojowniczka o "Mausowie powinni być wolni!" i wielka miłośniczka Mirandy. Elea ją odesłała, by nie zajmowała się sprawami dla dorosłych. Judyta szuka poparcia u Silurii.
* mag: Marek Kromlan, zalatany jak cholera, konstruminusy mu zabierają... oddaje KADEMowi co się da by to wszystko działało. Poluje na Mirandę Maus. Ma urazę do Silurii.
* czł: Filip Czółno, zdobył szacun bo stanął po stronie Silurii a nie Żupana. Wzmocnił to atakując z kilkoma Niedźwiedziami dom Marii.

# Plany



## Frakcji



# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Somnium, domena Arazille na Fazie Daemonica, odbicie Czelimina i Żonkibora
        1. Mare Felix, odbicie Lasu Stu Kotów; poza zwalczającymi się domenami Arazille i Millennium
        1. Mare Vortex
            1. Zamek As’Caen
                1. Kadem Daemonica
                    1. Układ Nerwowy
                        1. Sale Symulacji, gdzie Whisper rozplanowuje mapę Mare Felix i gdzie z Warmasterem i Kopidołem planują dalsze ruchy.
                    1. Skrzydło Medyczne, gdzie Siluria i Edwin debatują o przeszłych i dalszych losach kralotha i Niedźwiedzi.
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Klub Magów Rzeczna Chata, ostatnio główne miejsce spotkań Silurii i Netherii ;-).
                1. Piróg Dolny
                    1. Pylisko
                        1. Przychodnia Larent, Elea zajmowała się tam Marią. Niestety, Maria jej uciekła do Czelimina.
                        1. Mordownia Arena Wojny, gdzie Siluria pokazała Arturowi Żupanowi gdzie jego miejsce - i zniszczyła mu reputację u Ognistych Niedźwiedzi
            1. Powiat Czelimiński
                1. Miłejka
                    1. Warsztat Jubilerski, zaatakowany przez Ogniste Niedźwiedzie by podnieść władzę Filipa Czółno.

# Czas

* Opóźnienie: 0
* Dni: 2

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* 

## Po czym poznam sukces

* 

## Wynik z perspektywy celu

* 

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

* Eskalacja 2-2-2

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 punkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
