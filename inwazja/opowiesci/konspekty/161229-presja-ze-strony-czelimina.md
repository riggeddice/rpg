---
layout: inwazja-konspekt
title:  "Presja ze strony Czelimina"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160803 - Sleeper agent Oktawiana (HB)](160803-sleeper-agent-oktawiana.html)

### Chronologiczna

* [161204 - Zajcewowie po drugiej stronie (AW)](161204-zajcewowie-po-drugiej-stronie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Wezwanie Stalowego Śledzika Żarłacza jest czymś nietypowym. Czymś, czego Karradrael by się nie spodziewał po Mieszku Bankierzu. Jednak Mieszko jest wyraźnie potencjalnym Lordem Terminusem i wszystkie ślady wskazują na to, że to on dowodzi grupą. Karradrael wydał rozkaz uzyskania Mieszka Bankierza żywcem...

## Misja właściwa:

Dzień 1:

Andrea złapała kontakt z Agrestem - po hipernecie (tym starszej generacji). Agrest wysłał Andrei kody anulujące rozkaz samozniszczenia; ta fizycznie poczuła się inaczej. Transfer kodów trwał kilka godzin.

Andrea obudziła się ponownie. Znowu sygnał od Agresta. Znalazł Renatę Maus - ale sprawa jest nieco mało optymistyczna. Renata jest na Primusie, ale nie jest dostępna inaczej niż przez Fazę Daemonica. Andrea wie o pająku fazowym; wie też, że Agrest nie poda jej koordynatów przez hipernet. Innymi słowy, przynajmniej coś... dodatkowo, Renata jest w miejscu z wieloma scramblerami i jammerami. Tylko Basia Sowińska jest w stanie to otworzyć - a ona TEŻ jest na Fazie Daemonica. I Agrest nie może z nią negocjować sensownie bez argumentów.

Andrea i Agrest wspólnie złożyli dane. Świeca Daemonica dryfuje. Użyto technologii 'godtech', z czasu Wojen Bogów czy z podobnych okresów. Najpewniej mają do czynienia z bogiem. Andrea i Agrest stawiają, że z Karradraelem - ale ten Karradrael jest "jakiś inny". W czym Agrest sprawdził i wyszło mu, że to możliwe. Brak Lady Maus może doprowadzić do destabilizacji Karradraela... plus, Aurelia zrobiła Renacie/Karradraelowi coś STRASZNEGO. Andrea przekazała pobieżne informacje Agrestowi. Ten powiedział, że zajmie się polityką dla niej. Andrea doceniła. Powiedziała też o stanie Czelimina. Agrest jej rzucił - Myślin. Jeśli tam jest Karradrael, to tam coś się dzieje.

* "Jeżeli tam jest cicho, ze wszystkich miejsc..." - Agrest
* "Wiem. Dlatego tam się nie zbliżam." - Andrea, zniecierpliwiona
* "Nie wiesz, Andreo. Załóż, że WSZYSTKO co tam się dzieje jest pod jego kontrolą" - Agrest
* "Dlatego się tam nie zbliżam" - Andrea
* "Karradrael wykorzystuje ludzi, zwierzęta, magów, viciniusów... wszystko. Nie rób niczego, co może mieć JAKIKOLWIEK ślad pośredni do Ciebie" - Agrest, bardzo poważnie

Pukanie do drzwi przerwało im tą sielską rozmowę. To Melodia Diakon. 

* "Lady terminus! Mamy problem! Jest atak magii krwi w Twojego Mieszka! Nikt tego nie widzi! Tylko kraloth!" - Melodia, zadyszana
* "Laragnarhag mówi, że to bardzo poważne i może to zablokować. Ale to wymaga zmiany struktury... albo blokada wywoła XYZ." - Melodia, nadal lekko przestraszona

Czyli: Melodia twierdzi, że jest atak na Mieszka. Tylko kraloth to umie wyczuć. Mieszko nie uwierzy...

Andrea ściągnęła Laragnarhaga, Melodię, Rafaela i Mieszka. Szybkie spotkanie. 

* "Andreo, przerwałaś coś ważnego. Wioletta chciała zabić Barana za tą Irytkę..." - głęboko zirytowany Mieszko.
* "Przeżyję konflikt w zespole. Dużo trudniej z utratą kompetentnego terminusa." - Andrea, chłodno.
* "Kogo?" - Mieszko
* "Ciebie. Rafael - wyjaśnij, o co chodzi." - Andrea
* "To jest... skomplikowany problem... chodzi o..." - Rafael
* "Szybko, krótko i na temat" - Andrea, zirytowana
* "Pryzmatyczna wiązka magii krwi." - Rafael, wiedząc, że nikt go nie rozumie.

Rafael nie do końca rozumie kralotha; Laragnarhag nie 'nadaje' konceptami możliwymi przez Rafaela do interpretacji. Ogólnie rzecz biorąc, chodzi o coś takiego - jest ofiara, osoba powiązana z Mieszkiem. I ta osoba powiązana z Mieszkiem ma swój... umysł. Sposób myślenia. Swoją percepcję pryzmatyczną tamtej osoby. I to jest wykorzystywanie, by móc sympatią się połączyć z drugą osobą (wzmocnienie pryzmatyczne). I tym wzmocnieniem pryzmatycznym dochodzi do wysłania sygnału magii krwi którego funkcją jest Korupcja. Kraloth twierdził, że te mechanizmy były używane podczas Wojny - w wyniku czego Diakonowie połączyli siły z kralothami a Myszeczkowie stworzyli Mausów jako defensywę. To też jest 'godtech'.

Kraloth może to zablokować używając krwi ofiary. Można to też zatrzymać zmieniając wzór ofiary, lub zmieniając jej 'wyobrażenie'. Mieszko się nie zgodził - nie będzie dotykał go kraloth. Rafael szepnął do Melodii i kazał jej 'coś' zrobić. Ona spojrzała na niego z lekką obawą, po czym spoliczkowała Mieszka. Ten oddał mocno, prosto w twarz, pięścią. Żeby unieszkodliwić (skutecznie). Chciał dobić, ale się powstrzymał. Rafael zauważył do Andrei, że to jest wzmocnienie pryzmatyczne - konkretne cechy terminusa. Mieszko powiedział, że Rafael może sobie używać Melodii do ataku, ale on wie, kto naprawdę jest winny. Rafael po prostu wzruszył ramionami; wyraźnie jego interesuje tylko hipoteza.

Melodia go teraz BARDZO nie lubi. Spojrzała na niego ze szczerą wściekłością. 

Andrea dała rozkaz Jankowskiemu; ten obezwładnił Mieszka. Wysłała Jankowskiego i Mieszka do Skażonego Węzła. Powinno wystarczyć - przynajmniej na jakiś czas. A przynajmniej dać Andrei chwilę... Rafael docenił jej pomysł, spodobał mu się mu. Kraloth też. Powiedział Andrei (kraloth), że widział to w działaniu w Kompleksie Centralnym - użyte przeciwko Aleksandrowi Sowińskiemu na bazie Sabiny Sowińskiej. I powiedział, że widział 'pierwszą fazę pryzmatu' użytą też wobec maga Bankierzy. I ten mag tu jest. Wioletta. Ona była 'wzorem', 'źródłem' a nie celem.

Andrea wysłała jeden prosty sygnał do Agresta "załóż, że Kompleks Centralny jest stracony".

Andrea wezwała do siebie Wiolettę Bankierz i Tadeusza Barana. Powiedziała im, że mają ze sobą współpracować - przeszłość to przeszłość a teraźniejszość wymaga współpracy. Wioletta przywykła do nieracjonalnych rozkazów - blame Wiktor - więc przyłasiła się do Andrei i będzie posłuszna. Baran jest zakochany w Andrei, więc będzie posłuszny. Oboje będą. 

Andrea odesłała Barana i odpytała Wiolettę o Kompleks Centralny. Ta powiedziała wszystko, co wie. Opowiedziała o:

* Sabinie Sowińskiej (a Andrea powiązała to z 'tańczącą terminuską' i Aleksandrem Sowińskim - jej szefem)
* Działaniach Silurii w formie pozytywnej łagodząco moderującej Wiktora
* To, że Wiktor prawie oddał dowodzenie Dagmarze - ale Siluria przejęła kontrolę na MOMENT.
* To, że Dagmara ma BloodSuit (pochodzący z Czelimina) i że jest Spustoszony.
* Że viciniusy w Kompleksie są głodne i ruch oporu to grupa Bankierzy (od razu Andrea skojarzyła z tym co kraloth powiedział o Wioletcie).
* Że Wiktor dostał pomoc od Blakenbauerów
* Że Elea odkryła lek na Irytkę i że to jakaś pułapka.
* Co spotkało Dianę Weiner. De facto, że Szlachta jest zniszczona.

Update informacji od Wioletty leci do Agresta.

Po hipernecie, Andrea dostała jeszcze sygnał od Lidii - jest wyraźnie wstrząśnięta. Lidia kłóci się ostro z Melodią. 

* "Twoja kuzynka jest potworem. Spójrz co zrobiła Biance! Siluria jest gorsza niż kraloth!" - Lidia, wzburzona, do Melodii.
* "Nie ma nic złego w kralocie." - Melodia, spokojnie - "Plus, Siluria nie jest taką osobą."

Melodia zachowuje spokój, ale wyraźnie to nie jest jej dzień. A Lidia oskarża ją o przewiny Silurii - skoro ją broni, musi być podobna. Andrea zaczęła uspokajać Lidię - więzy Diakonów są silniejsze niż u Weinerów. Plus, nie znają całej sytuacji. Niestety, Siluria nie jest w stanie przekonać Lidii. Więc skierowała się ku Melodii - niech odpuści obronę kuzynki. Nie ma znaczenia co Lidia myśli o Silurii. To nieistotne. Znaczenie ma kohezja zespołu. Zresztą, po to Melodia tu jest. Udało się Andrei, Melodia odpuściła. Acz jest zraniona, choć nie przez Andreę. Na razie będzie z tym żyć, po prostu.

Andrea odprowadziła Melodię na parę kroków i powiedziała jej, że docenia. Melodia podziękowała i poszła do swojego pokoju. Po czym Andrea wróciła do Lidii. Lidia powiedziała, że naprawią Biankę. Po prostu... skala zniszczenia i przemocy... za dużo. Andrea powiedziała Lidii, że potrzebuje jej pomocy w zachowaniu kohezji zespołu. To starcie nie powinno się zdarzyć. Lidia odpowiedziała, że może. Ale jeśli widzi, że coś jest złe - to jest to złe i ona to powie. Melodia zdaniem Lidii zaczęła sama tą dyskusję. Andrea powiedziała, że trzeba było ją uciąć. Lidia wzruszyła ramionami. Powiedziała, że musi chronić Dalię.

I jeszcze z Andreą się spotyka Tatiana. Zajcewowie są poharatani, ale się pozbierani. Sama Tania ma dobry humor i wysokie morale. Powiedziała Andrei, że ma dostęp do artylerii, kontaktów Zajcewów i mniej legalnych działań Zajcewów. Tania ma dostęp i prawo. I chce pomóc Andrei. Andrea przekazała prośbę - niech Tania znajdzie jej informacje o Kiryle Sjeldzie. Tania powiedziała, że zrobi.

Andrea założyła sobie monitory na wszystko co może być wysłane do Mieszka.

Dzień 2:

Andrea musi porozmawiać z Draconisem. Jak iść na dno w procesie, to z przytupem...

Wiadomość do Andrei od Tatiany z prośbą o interwencję. Kłótnia, która się eskaluje. W jadalni - "Weinerowie przeciw Diakonom w temacie jakichś Diakonek". Tatiana niespecjalnie wie o co chodzi bo jest zajęta Zajcewowymi tematami - ale widząc poziom eskalacji zdecydowała się poprosić Andreę o działanie. Co zresztą Andrei bardzo pasuje. Andrea weszła dyskretnie.

* "Siluria zrobiła to, co uważała za słuszne. Nie rozumiecie, że ona ratuje magów!" - Wioletta, cała sycząca jak kotka
* "Wyprała Ci mózg, zupełnie jak tien Łaniewskiej." - Lidia, biała ze wściekłości
* "Siluria nie jest tego typu osobą by dominować osoby takie jak Wioletta" - bardzo sfrustrowana Melodia
* "Nie zaprzeczasz, że zrobiła coś takiego terminusce?!" - Kajetan, wstrząśnięty
* "Jak nie miała wyboru, to zrobiła." - Melodia, z determinacją
* "Zmiana umysłu, wypaczenie duszy... to jest czyn godny defilera" - Lidia, z pełnym przekonaniem
* "Defiler?! Nie byłaś w Kompleksie... Siluria NIE jest defilerką i NIE masz pojęcia o czym mówisz" - Wioletta, parskając
* "Widziałam jej dzieło w formie Bianki" - Lidia, z pozycji wyższości
* "WYDAWAŁO MI SIĘ..." - Andrea, zimno - "że wyraziłam się jasno" - patrząc na każdego po kolei

W kącie siedzi JEDEN Zajcew, wpieprza mięso i bardzo dobrze się bawi.

* "Niezależnie od tego co zrobiła - lub nie - czarodziejka Siluria Diakon... nic nie jesteśmy w stanie zrobić." - Andrea - "I potwierdziliście, czemu nie życzę sobie kłótni na ten temat."
* "Proszę wybaczyć, Lady Terminus. Po prostu nie chciałam, by zastrzelili ją na wejściu. Żeby dali jej szansę się wytłumaczyć" - Wioletta, stojąca na baczność
* "Zastrzelili? Przecież... przecież jesteśmy w Świecy. Tu jest... prawo. Sądy. O czym ty mówisz?" - zaszokowany Julian Pszczelak
* "Prawo prawem, ale... z doświadczenia, Świeca robi co chce. Są konsekwencje, nie prawo. Tylko siła." - Wioletta, nadal w pozycji na baczność
* "To inna Świeca niż ta, którą znam..." - Pszczelak, robiący za wyjątkowego idealistę.

Andrea zauważyła, że tej grupie Melodia próbowała de-eskalować a Wioletta broniła Silurii. Fakt, z nią o tym nie rozmawiała. Ale nic dziwnego - w końcu skąd Andrea miała domyśleć się tego, że Wioletta będzie bronić Silurii? Andrea powtórzyła rozkaz ponownie - nie są w stanie kłócić się i walczyć między sobą. Mają trudne sytuacje. Mają się moderować. Mogą mieć swoje opinie, ale mają działać jako zespół. Nie życzy sobie sporów z tego powodu.

* Julian Pszczelak skinął głową i będzie posłuszny.
* Melodia stanęła bokiem do wszystkich i poprawiła fryzurę. Very upset, ale będzie współpracować.
* Wioletta, nadal stojąca na baczność, wykona każdy rozkaz zgodnie z DUCHEM a nie literą. Wiktor dobrze ją wyszkolił.
* Lidia przedkłada swoją etykę nad pragmatyzm. Może "nie wsadzać innym swoich poglądów", ale je ma i będzie zgodnie z nimi działać.
* Kajetan... on nadal będzie współpracować z Andreą, ale coraz bardziej nie lubi swoich sojuszników (zwłaszcza z Millennium).

Andrea jest trochę zła i zirytowana, ale niewiele może poradzić przy tak kruchej strukturze sojuszy i sytuacji. Andrea sięgnęła do autorytetu Lady Terminus i do tego, że ich przeprowadziła przez piekło do tego co mają teraz. Zmusiła ich wszystkich, by jednak zaczęli współpracować. Dodała, by złagodzić cios i wzmocnić sygnał, że Laragnarhag i Rafael odchodzą z zespołu; ich czas się tu skończył. Kajetan i Lidia odebrali to jako redukcję wpływów Millennium; Melodia też, więc jeszcze silniej ją to uderzyło.

Andrea poczuła na sygnałach idących do Mieszka komunikaty. Przerywane, porozrywane sygnały dochodzące od torturowanej Kataliny z błaganiem o pomoc i ratunek. Podała nawet lokalizację - Czubrawka. Jest transportowana w kierunku na Czelimin. Część z tych sygnałów zaczyna być nadawana na ogólnym hipernecie. Andrea - nie chcąc by ktokolwiek się dowiedział - zaczęła blokować to po hipernecie. Niestety, nie wyszło; jest tylko opóźniony sygnał. Andrea kazała się wszystkim rozejść - a sama poprosiła Lidię.

* "Lady terminus, chciałam prosić o moment" - Melodia do Andrei
* "Czy to może poczekać?" - skupiona na nowym kryzysie Andrea
* "Tak, może..." - rozczarowana Melodia

Andrea zaprowadziła Lidię do biomózgu i wyjaśniła o co chodzi - chce odfiltrować te sygnały od Kataliny.

* Ale... możemy ją uratować. Mamy większą siłę niż ktokolwiek myśli. - Lidia, wstrząśnięta.
* Nie mówię, że nie. Ale nie chcę ciosu w morale. Mają docierać do mnie i tylko do mnie - Andrea, chłodno.
* Rozumiem, tak zrobię - Lidia, zdeterminowana.

Lidii udało się zatrzymać większość sygnałów, ale z uwagi na ród nie da rady zatrzymać sygnałów do Wioletty. Też Bankierz. Ostrzegła o tym Andreę. Ta natychmiast wezwała do siebie Wiolettę.

* Lady Terminus? - Wioletta, wyraźnie przestraszona
* Co panią niepokoi? - Andrea, chcąc wiedzieć na czym stoi
* Cała sytuacja - Wioletta, bezpiecznie unikając odpowiedzi
* Konkretnie? - Andrea, dociskając
* Jestem terminusem. Wykonuję polecenia. Nie mam obrazu sytuacji, więc jestem zaniepokojona - Wioletta, unikając odpowiedzi na wszystkie pytania
* Ten sygnał, jaki pani dostaje - nic z tym proszę nie robić a tym bardziej nikomu nie mówić. - Andrea, chłodno
* Oczywiście, Lady Terminus. - Wioletta, wahając się lekko.
* Słucham - Andrea, dociskając
* Czy mogę zasugerować moją percepcję tej sytuacji? - Wioletta, pamiętając Wiktora
* Słucham - Andrea, dalej chłodno
* To pułapka. Nie jesteśmy w stanie jej uratować. - Wioletta, lekko drżąc
* Też tak uważam - Andrea, poważnie - dlatego proszę niczego nie mówić
* Oczywiście, Lady terminus. Niczego nie powiem. Nie... nie dostałam żadnego sygnału - Wioletta, z lekką desperacją
* Doskonale. - Andrea.

Tak, Wioletta przeszła przez swoje w Kompleksie Centralnym oraz była przeszkolona przez Wiktora Sowińskiego - i to widać ;-). Andrea wie, że może być jej pewna.

* To jest już druga próba ataku na pani kuzyna. Na chwilę obecną znajduje się w miejscu, gdzie jest dużo trudniejszy do namierzenia. - Andrea, do Wioletty
* Rozumiem, Lady Terminus. Dobra decyzja, Lady Terminus. - Wioletta, z flashbackami od Wiktora
* Nie wiem, kiedy pozwolę mu opuścić kryjówkę. Niemniej jednak - jemu zwłaszcza nie wolno powiedzieć - Andrea, poważnie
* Oczywiście, Lady Terminus. Ja nie wiem niczego, czego nie mam wiedzieć. - bardzo gorąco zapewniająca Wioletta
* Doskonale. - Andrea, całkowicie nie mając czasu na zajmowanie się desperackim stanem Wioletty

Andrea poszła do Rafaela. Konferuje z Laragnarhagiem.

* Czarodzieju Diakon? - Andrea
* Moment... - Rafael, zajęty jakimiś toksynami

Andrea dała mu chwilę, po czym się doń odezwała.

* Chciałam bardzo panu podziękować za pomoc. - Andrea wiedząc, że przy wszystkich swych wadach Rafael zasłużył na podziękowania
* Nie ma problemu, tien Wilgacz. To jest przyjemność - uśmiechnięty Rafael; ile zdążył przetestować...
* Uważam jednak, że nadszedł czas byśmy się rozstali - Andrea prosto, wiedząc, że Rafael jest bezpośredni
* Jest taka możliwość. Jeśli nie jestem potrzebny, wrócę. - Rafael, spokojnie.
* Myślę, że w obecnej sytuacji bardziej przyda się pan gdzie indziej - Andrea
* Cała trójka ma odejść? - Rafael
* Melodia zostanie. Laragnarhag przybył tylko na krótki okres i cel jego pobytu został zrealizowany. - Andrea
* Kto zapewni Melodii bezpieczeństwo? Jest jedynym magiem Millennium w okolicy i nie umie się bronić - Rafael
* Jest pod moją ochroną tak samo jak wszyscy inni w tej bazie - Andrea, nie rozumiejąc
* Andreo, ale... stabilność emocjonalna. Ona będzie SAMA. - Rafael, naciskając na aspekt psychiki Diakonów
* ...być może będę musiała poprosić o innego członka waszego rodu - Andrea, orientując się w problemie
* Rozumiem. - Rafael, lekko wzruszając ramionami - To jak mamy wrócić?
* Kwestia do ustalenia. Dlatego muszę porozmawiać z Draconisem - Andrea
* Z przyjemnością; Laragnarhag jest świetnym medium - szeroko uśmiechnięty Rafael

Rafael się zespolił z Laragnarhagiem i zaczął mówić jako Draconis do Andrei. Andrea powiedziała, że ma do niego dwie sprawy, dwa tematy. Prostszy temat - obecność Laragnarhaga powoduje silne wstrząsy w grupie Andrei, więc Andrea musi go odesłać. Też podziękowała za obecność Rafaela. Powiedziała, że to może niekorzystnie wpłynąć na Melodię.

Draconis się uśmiechnął i powiedział, że nie ma problemu. Melodia jest specyficzna - ona może funkcjonować poza Millennium, byle miała koło siebie dużo ciepła i docenienia. Andrea powiedziała ze smutkiem, że w tej chwili ma z tym pewien problem - pojawiły się dyskusje na temat jej kuzynki. Niemniej jednak, czy Draconis ma jakiegoś Diakona najlepiej ze Świecy, najlepiej stabilnego emocjonalnie? Draconis powiedział, że spróbuje kogoś załatwić, przy odrobinie szczęścia jutro lub pojutrze. W czym - spoważniał - jest inny problem. Cały teren jest patrolowany przez Spustoszone drony; bardzo dużo się dzieje. Nie da się 'prześlizgnąć', nie jeśli baza Andrei ma zostać w ukryciu.

Z tym powiązana jest druga prośba Andrei. Chce sprowadzić Arazille na teren Czelimina.

Draconisa zamurowało.

* Nie jest to typowa prośba. - Draconis, naprawdę zaskoczony
* Nie. I nie robię tego z lekkim sercem. - Andrea
* Zainfekowanie jakiegoś terenu Arazille sprawia, że trudno się jej potem pozbyć.
* Zainfekowanie terenu Magią Krwi, Esuriit i tym podobnych... jest gorsze. A daje nam to okazję wymiany załóg.
* Wojny bogów... - szept Draconisa, który sobie coś przypomniał
* W tej chwili jak tak rozmawiamy jeden z magów Świecy jest torturowany, by ściągnąć jednego z moich magów w pułapkę. - Andrea, śmiertelnie poważnie
* On nic o tym nie wie. - Andrea, nadal poważnie - Ale przeciwnik zrobi to samo komukolwiek innemu.
* Wiesz już, z czym walczymy? - Draconis, wyraźnie zaskoczony - To przekracza nie tylko Porozumienia Radomskie. To przekracza... człowieczeństwo
* Nie wiem. Tylko myślę. Bardzo silnie podejrzewam Karradraela. Nie mam stuprocentowego potwierdzenia... ale dużo rzeczy na to wskazuje - Andrea, ze smutkiem - Ale jest jakiś inny
* Czyli jednak wojny bogów - Draconis posmutniał - Dobrze, powiem Ci jak wezwać Arazille. Ona jest w stanie kontrować Karradraela. Zwłaszcza teraz.
* W sensie? - zdziwiona Andrea
* Jest silniejsza niż kiedykolwiek. Powinna być w stanie ukoić Czelimin, nawet jeśli jest tam Karradrael. Wyczerpie to większość jej energii. - Draconis
* Same zalety, z mojego punktu widzenia - uśmiechnięta Andrea - Bo jego to potężnie uderzy. Przy okazji, da nam to możliwość wymiany magów.
* W sumie... jest sposób na odesłanie Rafaela i kralotha wcześniej - szeroko uśmiechnięta Andrea - Nie wdając się w szczegóły: KADEM
* Nie mam nic przeciwko - Draconis - Poczekaj tylko, aż podam Ci dane o Arazille.
* Poczekam - Andrea
* Twój proces wygląda coraz ciekawiej. - rozbawiony Draconis
* Tak, wiem... - niepocieszona Andrea

Połączenie rozerwane.

Andrea poszła do Mariana i Andżeliki. Oni pracują nad Oplem Astra, ale co tam robią? Sami dobrze nie wiedzą... na pytanie Andrei, Andżelika powiedziała, że to problem Entropii Pryzmatycznej - Opel za dawno temu nie był na Fazie i... potrzebuje trochę pracy. A oni (Andżelika i Marian) się nie znają. Więc... robią dobrą minę do złej gry.

Andrea dała im zadanie, które nie jest może przyjemne, ale jest bardzo ważne. Odwiezienie dwóch istot w konkretne miejsce. Marian się ucieszył. Andrea powiedziała, że Rafael i kraloth. Marian i Andżelika się wyraźnie wzdrygnęli. Andrea zaznaczyła, że kraloth się będzie zachowywał. Dostała powątpiewające spojrzenia magów KADEMu. Ale - przewiozą. To magowie KADEMu. Lubią wyzwania...

* Żeby zespół mi się nie rozleciał, muszę usunąć kralotha z tego terenu - Andrea, brutalnie.
* Nie ma sprawy. Lubię przewozić... kralothy... - Marian, niepewnie
* Istotna sprawa - MUSICIE być ukryci. Nawet, jeśli jedziecie przez Fazę. - Andrea, z naciskiem
* Jestem w stanie to zrobić - Andżelika, lekko niepewnie - Jest tam taki subkomponent w Oplu...
* Jak długo bezpiecznie dostarczy was i waszych pasażerów na miejsce... - Andrea
* Tak tak, nie ma problemu! - Marian, z fałszywym entuzjazmem
* Nie będzie problemu. Opel... nie zawodzi. Musisz w to wierzyć. - Andżelika, z pewnością, do Andrei.

Andrea od razu poznała Pryzmat.

Dobrze, czas spotkać się z cholerną Melodią... wysłała do niej ucznia, niech się przyda ;-). Melodia przyszła porozmawiać z Andreą.

* Lady Terminus, chciałam prosić o pozostawienie mi Laragnarhaga - Melodia, z proszącymi oczkami
* Obawiam się, że to jedno nie jest możliwe - Andrea, spokojnie
* On nikomu nie robi krzywdy. To mój przyjaciel. - Melodia, bardzo prosząco
* Wiem... ale sama wiesz, jaka jest reputacja kralothów. Wiem, że Laragnarhag nie robi nikomu krzywdy i wiem, że nie zrobi... ale miał spędzić tu krótki czas i jego obecność źle wpływa na morale. Aczkolwiek zgadzam się, że jest bardzo przydatny. - Andrea, zachowując spokój
* To jest okazja, by pokazać, że WSZYSCY walczymy z tym złem - Melodia, nieustępliwie
* Melodio, zgadzam się z Tobą, że jest to pewna okazja, ale mnie po prostu nie stać w tej chwili na przyjęcie konsekwencji z tego wynikających - Andrea - Połowa obecnych tu magów jest przerażona samym faktem jego obecności.
* ... - Melodia
* Postaram się, żebyś nie została sama - Andrea, uspokająco
* Spodziewałam się czegoś innego - Melodia, spokojnie
* Konkretnie?
* Spodziewałam się, że będę w środowisku w którym mogę pomóc, że będę... nie wiem, choć trochę szanowana? - zasmucona Diakonka
* Wbrew temu co wydajesz się uważać, ja cię szanuję - Andrea - Nie jestem w stanie nakazać szacunku. Sama świetnie wiesz, że to nie tak działa
* Uważam, że naprawdę możesz przyczynić się do spojenia tej grupy - Andrea, do Melodii - ale jako dowódca muszę dbać o to, by ta grupa miała szanse stać się drużyną. I naprawdę potrzebuję pomocy.
* Na razie potrzebowałaś mnie po to, by Baran nie zbaraniał i żebym zdobyła ciężarówkę. Nie czuję się traktowana jak OSOBA przez nikogo tutaj. - Melodia, z krzywym spojrzeniem
* Jedyne co mogę powiedzieć, to naprawdę 'przepraszam'. Nie było to moją intencją. - Andrea, szczerze
* Ustępuję na razie dla dobra zespołu, ale ja też jestem osobą. Mam ideały i poglądy. Nie jestem zabawką erotyczną dla Barana. Dla nikogo. - Melodia, wyjątkowo chłodno
* Czego oczekujesz? - Andrea, spokojnie
* Chcę być traktowana jak OSOBA. Nie chcę musieć się uśmiechać, ładnie wyglądać i nic nie robić. I nie móc bronić MOJEJ rodziny. - Melodia, zirytowana - I znowu: odsyłasz mojego przyjaciela. Rozumiem, dla dobra większości. Ale ja się muszę posunąć. Inni nie. Jestem Ci w ogóle potrzebna?
* Potrzebuję Twojej pomocy. Nie wiem jak możesz mi pomóc, ale jeśli możesz - pomóż. Nie musisz konsultować ze mną każdego posunięcia... - Andrea, chcąc złapać Melodię na ambicję
* Dobrze. Nawet, jeśli zostanę sama, nie będę sama. - Melodia, z nową determinacją.
* Poprosiłam Draconisa o kogoś z Twojego rodu, byś nie była sama. Tyle mogłam zrobić. - Andrea
* Tylko jedna prośba - jakbyś chciała organizować coś w formie większej imprezy... jak ma to wpływ taktyczny... to po prostu mi to powiedz - Andrea
* Nie ma sprawy. Mam parę pomysłów na pracę u podstaw. - Melodia

Andrea zobaczyła w Melodii poważne ostrzeżenie. Magowie po prostu nie biorą Melodii poważnie. A magowie Świecy nie potraktowali sojuszników z Millennium uczciwie.

Andrea skomunikowała się ze wszystkimi lokalnymi magami Świecy - do jadalni. Zrobiła proste wystąpienie. Mniej więcej w stronę: "Tien, nie wystawiacie najlepszego świadectwa Świecy. Ostatnie konflikty z magami spoza Świecy są coraz częstsze. Zupełnie niepotrzebne konflikty. Jak to o nas świadczy? Ci magowie zgodzili się nam pomóc, w ciężkiej sytuacji. A my co? Obrażamy ich za to? Deprecjonujemy? Gdyby sytuacja się odwróciła, to jak byście się czuli? To nie jest akceptowalne."

Andrea uzmysłowiła im, że nikt nie musi Świecy pomagać. Że sytuacja jest po prostu zła. Że może im się wydawać, że jest dobrze, ale jest po prostu źle. Że wrogowie są silni i są wszędzie dookoła. Że Kompleks padł. Że oni są największą siłą Świecy w okolicy. Poprzedni Lord Terminus nie żyje. Przywódca Wydziału Wewnętrznego w okolicy nie żyje. A ci inni magowie mimo wszystko im pomagają.

Andrei się udało. Magowie Świecy zaczną się sami kontrować... zaczną się zachowywać.

# Progresja

* Tatiana Zajcew: ma dostęp do 'artylerii' swoich rodziców i stowarzyszonych, rozmieszczonych w techbunkrach na Śląsku
* Tatiana Zajcew: ma dostęp do kontaktów swoich rodziców i niektórzy nawet będą jej słuchać ;-)
* Tatiana Zajcew: ma dostęp do informacji o różnych strukturach i obiektach swoich rodziców i stowarzyszonych
* Lidia Weiner: ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę
* Kajetan Weiner: ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę
* Andrea Wilgacz: nie jest już narażona na samozniszczenie przez Wydział Wewnętrzny
* Andrea Wilgacz: ma uprawnienia Lady Terminus "starego hipernetu"

# Streszczenie

Po znacznym powiększeniu sił Andrei we Wtorku, kohezja zaczęła się sypać. Pojawiły się linie i podziały, które z trudem Andrea odbudowała, min. decydując się na odesłanie Rafaela i Laragnarhaga. Jednak zanim do tego doszło - Laragnarhag ostrzegł Andreę, że Mieszko jest atakowany Pryzmatycznie po Krwi. Andrea kazała go wsadzić do Brudnego Węzła; kraloth dodał, że to się stało też w Kompleksie Centralnym. Melodia jest nieszczęśliwa z tego jak jest traktowana, Druga Strona zrobiła pułapkę na Mieszka torturując Katalinę w Czeliminie a Wioletta ostrzegła, by Andrea nie próbowała jej ratować. O dziwo, najbardziej zdyscyplinowaną siłą Andrei są Zajcewowie. Co ją dobija. Aha, Andrea poprosiła Draconisa o pomoc w przyzwaniu Arazille do Czelimina...

# Zasługi

* mag: Andrea Wilgacz, łagodząca konflikty w zespole, opracowała z Agrestem, że najpewniej Karradrael, chce wezwać Arazille i sparowała atak Czelimina na Mieszka.
* mag: Marian Agrest, który uratował Andreę od 'sygnału samozniszczenia' i ostrzegł ją poważnie przed Myślinem. Wypracowali Karradraela.
* mag: Melodia Diakon, posłaniec Rafaela, uderzona przez Mieszka, chroni dobre imię Silurii i ogólnie niezbyt poważana przez Świecę jako 'Diakonka do łóżka' i nic więcej. 
* mag: Mieszko Bankierz, cel ataku Czelimina. Wpierw Pryzmatyczny Atak Krwi, potem Katalina... ale skończył nieprzytomny w Skażonym Węźle.
* vic: Laragnarhag, który wyczuł Pryzmatyczny Atak Krwi w Mieszka i ostrzegł Andreę, że to samo działo się w Kompleksie Centralnym.
* mag: Rafael Diakon, próbuje tłumaczyć z kralociego na Andreowy i niezbyt mu wychodzi. Zirytowany, że nie rozumie Laragnarhaga.
* mag: Rudolf Jankowski, ogłuszył Mieszka i zaniósł go do Skażonego Węzła na polecenie Andrei. Dla jego dobra.
* mag: Wioletta Bankierz, bardzo posłuszna Andrei, broni Silurii przed - jej zdaniem - niewłaściwymi oskarżeniami. Przerażona tym co widzi. Przez problemy hipernetowe ma dostęp do niewłaściwych sekretów; obiecała milczenie.
* mag: Lidia Weiner, trochę prowodyrka konfliktów - ZBYT idealistyczna. Atakuje Silurię ("gorzej niż defilerka") i Melodię + Wiolettę ("bronisz ją?").
* mag: Tatiana Zajcew, dobry humor, wysokie morale, chce współpracować z Andreą i oddaje jej swoje siły i umiejętności do dyspozycji. Jasny punkt.
* mag: Katalina Bankierz, torturowana przez Czelimin. Pułapka na Mieszka, w którą nikt nie wpadł ;-).
* mag: Julian Pszczelak, nie wierzący w to, że Świeca może iść według zasady "might makes me right" ku zdziwieniu Wioletty
* mag: Draconis Diakon, który wymieni Andrei Rafaela i Laragnarhaga na innego * maga Millennium; też, pomoże jej z przywołaniem Arazille

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Czelimin, dokąd transportowana jest Katalina i gdzie odbywają się plany Drugiej Strony
            1. Powiat Kopaliński
                1. Czubrawka, gdzie podobno znajduje się Katalina, transportowana do Czelimina
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, całe dowodzenie Andrei ma tam miejsce. W sumie, cała misja.
                    1. Obrzeża
                        1. Skałki Bajeczne, gdzie znajduje się bardzo brudny Węzeł do którego wsadzili Mieszka by go odciąć od Skażenia Krwi

# Czas

* Dni: 2