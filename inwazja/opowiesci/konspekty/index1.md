---
layout: default
title: Konspekty Inwazji
---

# {{ page.title }}
 
## Inwazja 

## Historyczne

1. [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Światło w Zależu Leśnym

1. [150427 - Kult zaleskiego anioła (An, Mo)](150427-kult-zaleskiego-aniola.html)
1. [150429 - Terminusi w Zależu (JG, MG)](150429-terminusi-w-zalezu.html)
1. [150501 - Szalona "czarodziejka" Zależa (PT)](150501-szalona-czarodziejka-zaleza.html)

## Rezydentka Krukowa

1. [170523 - Opętany konstruminus (PT)](170523-opetany-konstruminus.html)
1. [170716 - Eteryczny Chłopiec i jego Pies (PT)](170716-eteryczny-chlopiec-i-jego-pies.html)
1. [170525 - New, better Senesgrad (Dru,Jojo,T(temp*3))](170525-new-better-senesgrad.html), English
1. [170510 - Najgorsze love story (PT)](170510-najgorsze-love-story.html)
1. [170515 - Niewolnica w leasingu (PT)](170515-niewolnica-w-leasingu.html)
1. [170518 - Machinacje maga rolniczego (PT)](170518-machinacje-maga-rolniczego.html)
1. [170614 - Kryzys przez eliksir (PT)](170614-kryzys-przez-eliksir.html)
1. [170702 - Miłość przez desperację (PT)](170702-milosc-przez-desperacje.html)
1. [170723 - Wywalczone życie Diany (PT)](170723-wywalczone-zycie-diany.html)
1. [170331 - Kiepsko porwana Paulina (PT)](170331-kiepsko-porwana-paulina.html)
1. [170404 - Wąż jako vicinius Pauliny (PT)](170404-waz-jako-vicinius-pauliny.html)
1. [170408 - Nie zabijajmy tych magów... (PT)](170409-nie-zabijajmy-tych-magow.html)
1. [170323 - Oszczędzili na rurach (PT)](170323-oszczedzili-na-rurach.html)
1. [170325 - Klątwożyt z lustra (PT)](170325-klatwozyt-z-lustra.html)
1. [170326 - Cała przeszłość spłonęła...](170326-cala-przeszlosc-splonela.html)
1. [160124 - Trzy opętane duszyczki... (PT)](160124-trzy-opetane-duszyczki.html)
1. [160131 - Dziwny Transmiter Weinerów (PT)](160131-dziwny-transmiter-weinerow.html)
1. [160227 - Zakazany Harvester (PT)](160227-zakazany-harvester.html)
1. [170312 - Przebudzony... Harvester? (PT)](170312-przebudzony-harvester.html) 

## Czarodziejka luster

1. [170207 - Błękitny Zaskroniec (An, temp)](170207-blekitny-zaskroniec.html)
1. [120918 - Można doprowadzić maga do problemu... (An)](120918-mozna-doprowadzic-maga-do-problemu.html)
1. [120920 - Ale nie można zmusić go do jego rozwiązania... (An)](120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html)
1. [121013 - Zwłaszcza, gdy coś jest ciągle nie tak. (An)](121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html)
1. [121104 - Banshee, córka mandragory. (An)](121104-banshee-corka-mandragory.html)
1. [160908 - Zabójczy spadek (An, temp)](160908-zabojczy-spadek.html)
1. [170815 - Bliźniaczka Andromedy (An)](170815-blizniaczka-andromedy.html)
1. [170816 - Na wezwanie Iliusitiusa (An, MB)](170816-na-wezwanie-iliusitiusa.html)
1. [130503 - Renowacja obrazu Andromedy (An)](130503-renowacja-obrazu-andromedy.html)
1. [130506 - Sekrety rezydencji Szczypiorków (An)](130506-sekrety-rezydencji-szczypiorkow.html)
1. [130511 - Ołtarz Podniesionej Dłoni (An)](130511-oltarz-podniesionej-dloni.html)
1. [131008 - "Mój Anioł" (An)](131008-moj-aniol.html)
1. [141218 - Portret Boga (An)](141218-portret-boga.html)
1. [141220 - Bogini Marzeń w Żonkiborze (An)](141220-bogini-marzen-w-zonkiborze.html)
1. [141227 - Przyczajona Andromeda, ukryty Maus (An)](141227-przyczajona-andromeda-ukryty-maus.html)
1. [141230 - Ofiara z wampira dla Arazille (An)](141230-ofiara-z-wampira-dla-arazille.html)
1. [150103 - Pryzmat Myśli pęka (An)](150103-pryzmat-mysli-peka.html)
1. [150104 - Terminus-defiler, kapłan Arazille (An)](150104-terminus-defiler-kaplan-arazille.html)
1. [150105 - Dar Iliusitiusa dla Andromedy (An)](150105-dar-iliusitiusa-dla-andromedy.html)
1. [150602 - Esme, najemniczka Netherii (EM)](150602-esme-najemniczka-netherii.html)

## Nie przydzielone

1. [140408 - Czarny Kamaz (AW, WZ)](140408-czarny-kamaz.html)
1. [140409 - Czwarta Frakcja Zajcewów (AW, WZ)](140409-czwarta-frakcja-zajcewow.html)
1. [170712 - Ucieczka Małży (Kić, Dzióbek, Żółw (temp))](170712-ucieczka-malzy.html)
1. [170718 - Umarł z miłości... (Kić, Raynor, Foczek (temp))](170718-umarl-z-milosci.html)

## Powrót Karradraela

1. [140503 - Wołanie o pomoc (AW, Til (temp), Viv (temp))](140503-wolanie-o-pomoc.html)
1. [140505 - Musiał zginąć, bo Maus (AW)](140505-musial-zginac-bo-maus.html)
1. [140508 - "Lord Jonatan" (AW)](140508-lord-jonatan.html)
1. [170501 - Streamerka w "Na Świeczniku" (Kić (temp), Til (temp), Viv (temp))](170501-streamerka-w-na-swieczniku.html)
1. [150729 - Kaczuszka w servarze (SD, PS)](150729-kaczuszka-w-servarze.html)
1. [161216 - Szept z Academii Daemonica (SD)](161216-szept-z-academii-daemonica.html)
1. [161217 - Niezbyt legalna 'Academia' Whisperwind (SD)](161217-niezbyt-legalna-academia-whisperwind.html)
1. [161218 - Zazdrość Warmastera (SD)](161218-zazdrosc-warmastera.html)
1. [161222 - Kto wpisał Błażeja do konkursu?! (SD)](161222-kto-wpisal-blazeja-do-konkursu.html)
1. [170221 - Przecież nie chodzi o koncert... (temp)](170221-przeciez-nie-chodzi-o-koncert.html)
1. [170108 - Samotna, w świecie magów (SD)](170108-samotna-w-swiecie-magow.html)
1. [170808 - Duch Opery (MB, PB)](170808-duch-opery.html)
1. [170228 - Polowanie na Mausównę (temp)](170228-polowanie-na-mausowne.html)
1. [170111 - EIS na kozetce (SD, PS)](170111-eis-na-kozetce.html)
1. [170115 - Klub "Dare Shiver" (SD)](170115-klub-dare-shiver.html)
1. [170118 - Ludzka prokuratura a magowie... (HB, AB, DK)](170118-ludzka-prokuratura-a-magowie.html)
1. [170217 - Skradziona pozytywka Mausów (SD, PS)](170217-skradziona-pozytywka-mausow.html)
1. [170417 - Ratując syrenopnącze (SD)](170417-ratujac-syrenopnacze.html)
1. [170315 - Naszyjnik Wspomnień (HB, AB, DK)](170315-naszyjnik-wspomnien.html)
1. [170707 - Biznes pośród niesnasek (MB, SD, HS)](170707-biznes-posrod-niesnasek.html)
1. [170319 - Camgirl na dragach (AB)](170319-camgirl-na-dragach.html)
1. [170407 - Przebudzenie viciniusa (HB, AB, DK)](170407-przebudzenie-viciniusa.html)
1. [170412 - Przed teatrem absurdu (HB, AB, DK)](170412-przed-teatrem-absurdu.html)
1. [141119 - Antygona kontra Dracena (SD, PS)](141119-antygona-kontra-dracena.html)
1. [141115 - GS "Aegis" 0002 (SD, PS)](141115-gs-aegis-0002.html)
1. [141123 - Druga kradzież Wyzwalacza (SD, EP)](141123-druga-kradziez-wyzwalacza.html)
1. [150110 - Bezpieczna baza w Kotach (SD, PS)](150110-bezpieczna-baza-w-kotach.html)
1. [150224 - Wojna domowa Spustoszenia (SD, PS)](150224-wojna-domowa-spustoszenia.html)
1. [150313 - To ile tam było szczepów Spustoszenia?!(SD, PS)](150313-ile-tam-bylo-szczepow-spustoszenia.html)
1. [150329 - Knowania Izy(SD, PS)](150329-knowania-izy.html)
1. [150325 - Morderstwo jak w książce (PT, HB)](150325-morderstwo-jak-w-ksiazce.html)
1. [150330 - Napaść na Annalizę (SD, PS)](150330-napasc-na-annalize.html)
1. [150411 - Dzień z życia Klemensa (Kx, AB)](150411-dzien-z-zycia-klemensa.html)
1. [150406 - Aurelia za aptoforma (PT)](150406-aurelia-za-aptoforma.html)
1. [150408 - Rykoszet zimnej wojny (HB, DK, AB)](150408-rykoszet-zimnej-wojny.html)
1. [161005 - Szmuglowanie artefaktów... (temp)](161005-szmuglowanie-artefaktow.html)
1. [150422 - Śladami aptoforma... (DK, AB)](150422-sladami-aptoforma.html)
1. [150604 - Proces bez szans wygrania (HB, SD, PS)](150604-proces-bez-szans-wygrania.html)
1. [160809 - Awokado dla wampira (temp)](160809-awokado-dla-wampira.html)
1. [150607 - Brat przeciw bratu (HB, SD, PS)](150607-brat-przeciw-bratu.html)
1. [160202 - Wolność pająka fazowego (KB, LB)](160202-wolnosc-pajaka-fazowego.html)
1. [150625 - Poligon kluczem do Marcelina? (HB, SD)](150625-poligon-kluczem-do-marcelina.html)
1. [150716 - Chora terminuska i żabolód (PT)](150716-chora-terminuska-i-zabolod.html)
1. [150704 - Najskrytszy sekret Tamary (HB, SD)](150704-najskrytszy-sekret-tamary.html)
1. [150718 - Splątane tropy: Spustoszenie? (AW)](150718-splatane-tropy-spustoszenie.html)
1. [150722 - Reverse kidnaping z Krupnioka (DK, HB, Kx)](150722-reverse-kidnapping-krupniok.html)
1. [150728 - Sojusz przeciwko Szlachcie (HB, SD)](150728-sojusz-przeciwko-szlachcie.html)
1. [150819 - Krótki antyporadnik o sojuszach (HB, SD, Kx)](150819-krotki-antyporadnik-o-sojuszach.html)
1. [150913 - Andrea węszy koło Szlachty (AW)](150913-andrea-weszy-kolo-szlachty.html)
1. [150820 - Klemens w roli swatki (Kx, AB)](150820-klemens-w-roli-swatki.html)
1. [151001 - Plan ujawnienia z Hipernetu (HB, DK)](151001-plan-ujawnienia-z-hipernetu.html)
1. [150823 - Atak na sanktuarium Estrelli (PT)](150823-atak-na-sanktuarium-estrelli.html)
1. [150826 - Pętla dookoła Pauliny (PT)](150826-petla-dookola-pauliny.html)
1. [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)
1. [151007 - Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)](151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)
1. [150829 - Hybryda w bazie Skorpiona (PT)](150829-hybryda-w-bazie-skorpiona.html)
1. [150930 - O Wandzie co Zajcewa nie chciała (AB, Kx)](150930-o-wandzie-co-zajcewa-nie-chciala.html)
1. [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)
1. [151014 - Jedno słowo prawdy za dużo... (HB, SD)](151014-jedno-slowo-prawdy-za-duzo.html)
1. [151021 - Przebudzenie Mojry (HB, SD)](151021-przebudzenie-mojry.html)
1. [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)
1. [151110 - Romeo i... Hektor (HB, AB, DK)](151110-romeo-i-hektor.html)
1. [160128 - Byli sobie przestępcy (HB, AB, LB)](160128-byli-sobie-przestepcy.html)
1. [160210 - Batmag uderza! (HB, KB, LB)](160210-batmag-uderza.html)
1. [160217 - Sojusz według Leonidasa (LB, AB, DK)](160217-sojusz-wedlug-leonidasa.html)
1. [160326 - Siluria na salonach Szlachty (SD)](160326-siluria-na-salonach-szlachty.html)
1. [151119 - Patrycja węszy szpiega (AB, DK)](151119-patrycja-weszy-szpiega.html)
1. [151124 - Odbudowa relacji konfliktem (HB, SD)](151124-odbudowa-relacji-konfliktem.html)
1. [151202 - Zdobycie węzła Vladleny (HB, AB)](151202-zdobycie-wezla-vladleny.html)
1. [160229 - Siedmiu magów - nie Mausów (AW)](160229-siedmiu-magow-nie-mausow.html)
1. [160327 - Pięćdziesiąt oblicz Szlachty (SD)](160327-piecdziesiat-oblicz-szlachty.html)
1. [151212 - Antyporadnik wędkarza Arazille (HB, AB, DK)](151212-antyporadnik-wedkarza-arazille.html)
1. [151216 - Między prawdą i fikcją Arazille... (AB, DK)](151216-miedzy-prawda-i-fikcja-arazille.html)
1. [160403 - Wiktor kontra KADEM i Świeca (SD)](160403-wiktor-kontra-kadem-i-swieca.html)
1. [160224 - Aniołki Marcelina (LB, KB)](160224-aniolki-marcelina.html)
1. [160303 - Otton zabija Zetę (AB, DK)](160303-otton-zabija-zete.html)
1. [160411 - Sekret śmierci na wykopaliskach (AW)](160411-sekret-smierci-na-wykopaliskach.html)
1. [160927 - Desperacka bateria dla Aegis (temp)](160927-desperacka-bateria-dla-aegis.html)
1. [160404 - The power of cute pet (SD)](160404-the-power-of-cute-pet.html)
1. [170321 - Tajemnica podłożonej świni (temp)](170321-tajemnica-podlozonej-swini.html)
1. [160309 - Irytka Sprzężona (HB, KB, LB)](160309-irytka-sprzezona.html)
1. [160316 - Frontalne wejście Millennium (HB, KB, LB)](160316-frontalne-wejscie-millennium.html)
1. [160406 - Najprawdziwszy sojusz Blakenbauerów (HB, KB, AB, DK)](160406-najprawdziwszy-sojusz-blakenbauerow.html)
1. [160417 - Symptomy kryzysu Świecy (AW)](160417-symptomy-kryzysu-swiecy.html)
1. [160412 - Spleśniała dusza terminuski (SD)](160412-splesniala-dusza-terminuski.html)
1. [160420 - Kolizja dwóch sojuszy (PT, HB, LB, KB)](160420-kolizja-dwoch-sojuszy.html)
1. [160424 - Uważaj, o czym marzysz... (AW, SD)](160424-uwazaj-o-czym-marzysz.html)
1. [160506 - Wyścig pająka z terminuską (HB, KB)](160506-wyscig-pajaka-z-terminuska.html)
1. [160723 - Czyj jest Kompleks Centralny? (SD)](160723-czyj-jest-kompleks-centralny.html)
1. [160724 - Portal do EAM (SD)](160724-portal-do-eam.html)
1. [161030 - Odbudowa dowodzenia Świecy (AW)](161030-odbudowa-dowodzenia-swiecy.html)
1. [160615 - Morderczyni w masce Wandy (HB, KB)](160615-morderczyni-w-masce-wandy.html)
1. [161101 - Bezwzględna Lady Terminus (AW)](161101-bezwzgledna-lady-terminus.html)
1. [160629 - Rezydencja? E, polujemy na dronę! (KB, DK, AB)](160629-rezydencja-e-polujemy-na-drone.html)
1. [161113 - Świeca nie zostawia swoich (AW)](161113-swieca-nie-zostawia-swoich.html)
1. [160821 - Wycofanie Mileny z piekła (SD)](160821-wycofanie-mileny-z-piekla.html)
1. [160707 - Mała, szara myszka... (KB, DK, HB)](160707-mala-szara-myszka.html)
1. [161120 - Tak wygrywa się sojuszami! (AW)](161120-tak-wygrywa-sie-sojuszami.html)
1. [160713 - Jak ukraść ze Świecy Zajcewów (AB, DK, HB)](160713-ukrasc-ze-swiecy-zajcewow.html)
1. [161204 - Zajcewowie po drugiej stronie (AW)](161204-zajcewowie-po-drugiej-stronie.html)
1. [160803 - Sleeper agent Oktawiana (HB)](160803-sleeper-agent-oktawiana.html)
1. [161229 - Presja ze strony Czelimina... (AW)](161229-presja-ze-strony-czelimina.html)
1. [160810 - Zaszczepić Adriana Murarza! (HB, KB, AB, DK)](160810-zaszczepic-adriana-murarza.html)
1. [161231 - Eskalacja Czelimina, eskalacja Andrei... (AW)](161231-eskalacja-czelimina-andrei.html)
1. [160819 - Oblicze Guwernantki (HB, KB, AB, DK)](160819-oblicze-guwernantki.html)
1. [170101 - Patrol? Kralotyczne maskowanie (AW)](170101-patrol-kralotyczne-maskowanie.html)
1. [160825 - Plany Overminda (HB, KB, AB, DK)](160825-plany-overminda.html)
1. [170103 - Wojna bogów w Czeliminie (AW)](170103-wojna-bogow-w-czeliminie.html)
1. [160914 - Aleksandria, krwawa Aleksandria... (HB, KB)](160914-aleksandria-krwawa-aleksandria.html)
1. [161012 - Kontratak Karradraela (HB, KB, SD)](161012-kontratak-karradraela.html)
1. [170122 - Gambit Anety Rainer (AW)](170122-gambit-anety-rainer.html)
1. [161026 - Zagłodzona ekspedycja Świecy (HB, SD)](161026-zaglodzona-ekspedycja-swiecy.html)
1. [161102 - Magowie Esuriit w domu... (HB, SD)](161102-magowie-esuriit-w-domu.html)
1. [161109 - Jak prawidłowo wpaść w pułapkę (HB, SD)](161109-jak-prawidlowo-wpasc-w-pulapke.html)
1. [161124 - Ponura historia ekspedycji Esuriit (HB, SD)](161124-ponura-historia-ekspedycji-esuriit.html)
1. [161130 - Sprowadzenie Mare Vortex (HB, SD)](161130-sprowadzenie-mare-vortex.html)
1. [161207 - Lizanie ran na KADEMie (HB, SD)](161207-lizanie-ran-na-kademie.html)
1. [170104 - Spalone generatory pryzmatyczne (HB, SD)](170104-spalone-generatory-pryzmatyczne.html)
1. [170125 - Przeprawa do Świecy Daemonica (HB, SD)](170125-przeprawa-do-swiecy-daemonica.html)
1. [170208 - Koniec wojny z Karradraelem (HB, SD)](170208-koniec-wojny-z-karradraelem.html)
1. [170215 - To się nazywa "łupy wojenne"? (HB, SD)](170215-to-sie-nazywa-lupy-wojenne.html)
1. [170226 - Wygraliśmy wojnę... prawda? (AW)](170226-wygralismy-wojne-prawda.html)
1. [170222 - Renata Souris i echo Urbanka... (HB, SD)](170222-renata-souris-i-echo-urbanka.html)
1. [170517 - Zegarmistrz i Alegretta (HB, KB)](170517-zegarmistrz-i-alegretta.html)
1. [170405 - Chyba wolelibyśmy kartony... (SD, PS)](170405-chyba-wolelibysmy-kartony.html)
1. [170531 - Autowar: pierwsze starcie (HB, KB)](170531-autowar-pierwsze-starcie.html)
1. [170607 - Oślepienie autowara (HB, KB)](170607-oslepienie-autowara.html)
1. [170519 - Odzyskać Aegis 0003 (SD, PS)](170519-odzyskac-aegis-0003.html)
1. [170823 - Suma niedokończonych spraw... (HB, KB, DK, AB, PK, HS)](170823-suma-niedokonczonych-spraw.html)
1. [170830 - "Wdzięczność" Iliusitiusa (HB, KB, DK, AB, PK, HS)](170830-wdziecznosc-iliusitiusa.html)
1. [170914 - Kolejna porażka Kinglorda](170914-kolejna-porazka-kinglorda.html)
1. [170920 - Początki prokuratury (HB, HS, SD)](170920-poczatki-prokuratury.html)

## Córka Lucyfera

1. [170530 - Córka Lucyfera (HS, Kić(temp))](170530-corka-lucyfera.html)
1. [170603 - Córka jest narzędziem? (HS, Kić(temp))](170603-corka-jest-narzedziem.html)
1. [170620 - Pułapka na Luksję (HS, Kić(temp))](170620-pulapka-na-luksje.html)
1. [170628 - Ukradziona apokalipsa (HS, Kić(temp))](170628-ukradziona-apokalipsa.html)

## Taniec liści

1. [160922 - Czarnoskalski konwent RPG (Esme, temp)](160922-czarnoskalski-konwent-rpg.html)
1. [160915 - Rekrutacja mimo woli (temp)](160915-rekrutacja-mimo-woli.html)
1. [160911 - Reedukacja Infensy (SD)](160911-reedukacja-infensy.html)
1. [160921 - Wandy wolność i wróżda (temp)](160921-wandy-wolnosc-i-wrozda.html)

## Ucieczka do Przodka

1. [150928 - Zamtuz z jedną wiłą (PT)](150928-zamtuz-z-jedna-wila.html)
1. [151003 - Zamtuz przestaje działać (PT)](151003-zamtuz-przestaje-dzialac.html)
1. [151013 - Kontrolowany odwrót z zamtuza (PT)](151013-kontrolowany-odwrot-zamtuza.html)
1. [151101 - Mafia Gali w szpitalu (PT)](151101-mafia-gali-w-szpitalu.html)
1. [151220 - Z Null Fieldem w garażu... (PT)](151220-z-nullfieldem-w-garazu.html)
1. [151223 - ..można uwolnić czarodziejkę... (PT)](151223-mozna-uwolnic-czarodziejke.html)
1. [151229 - ..choć to na sektę nie pomoże (PT)](151229-choc-to-na-sekte-nie-pomoze.html)
1. [151230 - Zajcew ze śmietnika partnerem... (PT)](151230-zajcew-ze-smietnika-partnerem.html)
1. [160106 - ...i kult zostaje rozgromiony (PT)](160106-i-kult-zostaje-rozgromiony.html)
1. [160117 - Muchy w sieci Korzunia (PT)](160117-muchy-w-sieci-korzunia.html)
1. [170423 - Rozpad magii w Leere (PT)](170423-rozpad-magii-w-leere.html)

## Prawdziwa natura Draceny

1. [170725 - Krzywdzę, bo kocham (4*temp)](170725-krzywdze-bo-kocham.html)
1. [140928 - Policjant widział anioła (PT)](140928-policjant-widzial-aniola.html)
1. [141012 - Aplikanci widzieli gorathaula (PT)](141012-aplikanci-widzieli-gorathaula.html)
1. [141019 - Lekarka widziała cybergothkę (PT)](141019-lekarka-widziala-cybergothke.html)
1. [141025 - Gildie widziały protomaga (PT)](141025-gildie-widzialy-protomaga.html)
1. [141026 - Dracena widziała swój koszmar (PT)](141026-dracena-widziala-swoj-koszmar.html)
1. [141102 - Paulina widziała sępy nad Nikolą (PT)](141102-paulina-widziala-sepy-nad-nikola.html)
1. [141110 - Prawdziwa natura Draceny (PT)](141110-prawdziwa-natura-draceny.html)
1. [160901 - Uwięziony w komputerze! (PT, temp)](160901-uwieziony-w-komputerze.html)
1. [160904 - Rozpaczliwie sterowany wzór (PT)](160904-rozpaczliwie-sterowany-wzor.html)
1. [160907 - Dracena... Blakenbauer? (PT)](160907-dracena-blakenbauer.html)
1. [160910 - Na żywym organiźmie Draceny... (PT)](160910-na-zywym-organizmie-draceny.html)
1. [160918 - Walka o duszę Draceny (PT)](160918-walka-o-dusze-draceny.html)
1. [161018 - Ballada o duszy ognistej (HS)](161018-ballada-o-duszy-ognistej.html)
1. [161002 - Wyciek syberionu (PT)](161002-wyciek-syberionu.html)
1. [161009 - "Paulino, zmieniłaś się..." (PT)](161009-paulino-zmienilas-sie.html)
1. [170212 - Nieufność w małym mieście (PT)](170212-nieufnosc-w-malym-miescie.html)

## Nicaretta

1. [161103 - Egzorcyzmy w Żonkiborze (HS, temp)](161103-egzorcyzmy-w-zonkiborze.html)
1. [161110 - Succubus myśli, że uciekł (HS, temp)](161110-succubus-mysli-ze-uciekl.html)
1. [161115 - Uciekła do femisatanistek (HS, temp)](161115-uciekla-do-femisatanistek.html)
1. [161129 - Ewakuacja Natalii, wejście maga (HS, temp)](161129-ewakuacja-natalii-wejscie-maga.html)
1. [161206 - Ucieczka Sióstr Światła (HS, temp)](161206-ucieczka-siostr-swiatla.html)
1. [161220 - Zniszczenie posągu Arazille (HS, KŁ)](161220-zniszczenie-posagu-arazille.html)
1. [170113 - "Dzisiaj złapiemy Nicarettę!" (HS, KŁ)](170113-dzisiaj-zlapiemy-nicarette.html)
1. [170120 - Wielki sojusz powszechny (HS, KŁ)](170120-wielki-sojusz-powszechny.html)
1. [170214 - Nekroborg dla Laetitii Gai (HS, KŁ)](170214-nekroborg-dla-laetitii-gai.html)

## Blakenbauerowie x "Skorpion"

1. [150304 - Ani słowa prawdy... (HB, AB)](150304-ani-slowa-prawdy.html)
1. [141006 - Klinika "Słonecznik" (WD, EM)](141006-klinika-slonecznik.html)
1. [141009 - Jad w prokuraturze (HB, Kx, AB)](141009-jad-w-prokuraturze.html)
1. [141022 - Po wymianie strzałów... (HB, Kx, AB)](141022-po-wymianie-strzalow.html)
1. [150210 - "Komandosi", czyli upadek bohaterki (HB)](150210-komandosi-upadek-bohaterki.html)
1. [141216 - Zabili mu syna (HB, Kx, AB)](141216-zabili-mu-syna.html)
1. [150115 - Negocjacje w LegioQuant (HB, Kx, AB)](150115-negocjacje-w-legioquant.html)
1. [150121 - Nowe życie Aliny (AB)](150121-nowe-zycie-aliny.html)

## Druga Inwazja

1. [140103 - Tak bardzo nie artefakt(AW, WZ)](140103-tak-bardzo-nie-artefakt.html)
1. [140109 - Uczniowie Moriatha(AW, WZ)](140109-uczniowie-moriatha.html)
1. [140114 - Zaginiony członek(AW, HB)](140114-zaginiony-czlonek.html)
1. [140121 - Zniknięcie Sophistii(AW, HB)](140121-znikniecie-sophistii.html)
1. [140201 - Ona zdradza, on zdradza(AW)](140201-ona-zdradza-on-zdradza.html)
1. [140208 - Na ratunek terminusce(AW, WZ)](140208-na-ratunek-terminusce.html)
1. [140213 - Pułapka na Edwina(AW, WZ, HB)](140213-pulapka-na-edwina.html)
1. [140219 - Niespodziewane wsparcie(AW, WZ)](140219-niespodziewane-wsparcie.html)
1. [140227 - Sophistia x Marcelin(AW, WZ, HB)](140227-sophistia-x-marcelin.html)
1. [140320 - Sprawa magicznych samochodów(AW, HB)](140320-sprawa-magicznych-samochodow.html)
1. [140312 - Atak na rezydencję Blakenbauerów (AW, WZ, HB)](140312-atak-na-rezydencje.html)
1. [140401 - Mojra, Moriath (AW, WZ, HB)](140401-mojra-moriath.html)
1. [140604 - Patriarcha Blakenbauer (AW, WZ, HB)](140604-patriarcha-blakenbauer.html)
1. [140611 - Rezydencja Blakenbauerów (AW, WZ, HB)](140611-rezydencja-blakenbauerow.html)
1. [140618 - Upadek Agresta (AW, WZ, HB)](140618-upadek-agresta.html)
1. [140625 - Ostatnia Saith (AW, WZ, HB)](140625-ostatnia-saith.html)
1. [140708 - Druga Inwazja (AW, WZ, HB)](140708-druga-inwazja.html)

## Wizja Dukata

1. [171001 - Powrót do domu (PT)](171001-powrot-do-domu.html)
1. [171003 - Świnia na portalisku (KD, temp)](171003-swinia-na-portalisku.html)
1. [171004 - Niestabilna magitrownia (PT, DA)](171004-niestabilna-magitrownia.html)
1. [171010 - Jasny sygnał Tamary (PT, DA, KG)](171010-jasny-sygnal-tamary.html)
1. [171015 - Powstrzymana wojna domowa (PT)](171015-powstrzymana-wojna-domowa.html)
1. [171022 - Tylko nieświadomy elemental (PT)](171022-tylko-nieswiadomy-elemental.html)
1. [171024 - Detektyw, lecz nie Sądecznego (PT, KD, KG)](171024-detektyw-lecz-nie-sadecznego.html)
1. [171029 - W co gra Sądeczny? (PT)](171029-w-co-gra-sadeczny.html)
1. [171031 - Utracona kontrola (PT, DA)](171031-utracona-kontrola.html)
1. [171101 - Magimedy przed epidemią (PT)](171101-magimedy-przed-epidemia.html)
1. [171105 - Epidemia dezinhibicji (PT, DA)](171105-epidemia-dezinhibicji.html)
1. [171115 -  (PT, DA)](171115-xxx.html)

## Koniec znanej historii Inwazji

## Poza czasem (alternatywne? Nie umieszczone? Anulowane?)

1. [160526 - Bobrzańskie Gumifoczki (LŁ, AB)](160526-bobrzanskie-gumifoczki.html), kiedyś: Gildia uprzednio zwana jako...
1. [160608 - Czy on wyszedł z Rifta...? (LŁ, AB, ZD)](160608-czy-on-wyszedl-z-rifta.html), kiedyś: Gildia uprzednio zwana jako...
1. [141210 - Złodzieje kielicha w akcji (APł, APu)](141210-zlodzieje-kielicha-w-akcji.html) (kiedyś: Ptaczewo blues #1)
1. [150120 - Pierścień też zniknął (APł, MDe)](150120-pierscien-tez-zniknal.html) (kiedyś: Ptaczewo blues #1)
1. [140702 - Miślęg zniknąć! (temp 1. 3)](140702-misleg-zniknac.html)
1. [130514 - (SKR) Chłopak, który chciał być bohaterem (Do, Jan, Ca)](130514-chlopak-ktory-chcial-byc-bohaterem.html)
1. [130529 - Zguba w muzeum (temp?)](130529-zguba-w-muzeum.html)
1. [131117 - (RD) Cel uświęca środki - początek (Pa)](131117-cel-uswieca-srodki.html)
1. [140808 - Ucieczka Trzmieli (HB, Kx)](140808-ucieczka-trzmieli.html) (kiedyś: upadek Blakenbauerów)
1. [140819 - Marcelin w klasztorze! (PT, HB)](140819-marcelin-w-klasztorze.html) (kiedyś: upadek Blakenbauerów)
1. [140910 - Reporter kontra Blakenbauerzy (PT, HB, Kx)](140910-reporter-kontra-blakenbauerzy.html) (kiedyś: upadek Blakenbauerów)


## Kod postaci graczy:

- An - __"Andromeda"__ - (Kić) - [człowiek, który nie zapomina]
- AW - __Andrea Wilgacz__ - (Kić) - [SŚ, pogardzany eks-terminus]
- WZ - __Wacław Zajcew__ - (Bartek) - [SŚ/Zajcew, ]
- PT - __Paulina Tarczyńska__ - (Kić) - [niezrzeszona, magiczny lekarz świata ludzi]
- WD - __Wiktor Diakon__ - (Bartek) - [SŚ/najemnik, ]
- EM - __Esme Myszeczka__ - (Kić) - [SŚ/najemnik, ] 
- HB - __Hektor Blakenbauer__ - (Dust) - [Blakenbauer/niezrzeszony, prokurator z magicznego rodu]
- Kx - __Klemens xxx__ - (Bartek) - [Blakenbauer/człowiek, cień i strażnik Marcelina]
- AB - __Alina Bednarz__ - (Kić) - [Blakenbauer/vicinius, cień i dobrobyt Hektora]
- SD - __Sabina (Siluria Tyrania) Diakon__ - (Kić) - [KADEM/mag, agentka tysiąca gildii]
- PS - __Paweł Sępiak__ - (Bartek) - [KADEM/mag, technomanta o dużej sile ognia]
- EP - __Eryk Płomień__ - (Pit) - [KADEM/mag, cyborg analityczno-militarny]

## Wątki:

- Mała prywatna wojenka
- Szlachta vs Kurtyna
- Rodzina Świecy
- Powrót Mausów
- Aegis czy EIS?
- Trzeci kontroler Spustoszenia
- Kościół w Kotach
- Wojny wielkich gildii Śląska
- Projekt "Moriath"
- Tymotheus Blakenbauer
- Wszystkie dziewczyny Marcelina
- Arazille, królowa niewolników
- Za chwałę Blakenbauerów
