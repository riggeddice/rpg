---
layout: inwazja-konspekt
title:  "Morderczyni w masce Wandy"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161030 - Odbudowa dowodzenia Świecy (AW)](161030-odbudowa-dowodzenia-swiecy.html)

### Chronologiczna

* [160506 - Wyścig pająka z terminuską (HB, KB)](160506-wyscig-pajaka-z-terminuska.html)

## Kontekst misji
- Wanda Ketran wróciła. Ale na pewno to pułapka, bo nie mogła wrócić.
- Wanda Ketran grozi złamaniem Maskarady i łamie reputację Hektora.

## Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.

## Punkt zerowy:

- Cel "Wandy Ketran": 
... wyłączyć Hektora i Blakenbauerów z akcji. Permanentnie.
... rozbić Kopalin

- Hektor ma swoją prokuraturę. Edwin ma swoich pacjentów. Margaret ma swoją restaurację (w Kopalinie). Klara ma swój warsztat. Marcelin ma swój klub nocny o którym nikt nie wie (obok Działa Plazmowego).


## Misja właściwa:

Dzień 1: 

Klara obejrzała filmik na YT. Wanda Ketran. Powiedziała jak Hektor ją porwał i robił jej krzywdę; dodatkowo rzuciła informacje odnośnie tego jakie rzeczy czasami robiła grupa specjalna Hektora. Wanda ma bardzo szeroką wiedzę odnośnie operacji Hektora...
Trzeba znaleźć, gdzie to zostało uploadowane. Klara wezwała do siebie Dionizego, który wyszedł od "Frau Blakenbauer". Dionizy po przeanalizowaniu filmiku stwierdził, że nie ma tam nic czego człowiek by nie był w stanie tego zrobić, ale musi mieć siatkę. I to solidną.

Do nagrania filmiku (Dionizy badał) służyła kamerka z laptopa. Nie jakiegoś idealnego. A Wanda nagrywa ze swojego pokoju. Pytanie: ile jest Wandy w Wandzie? Dionizy dowiedział się - to nie jest Wanda. Ktoś kto to zrobił wiedział o Wandzie, ale nie znał Wandy. 
Klara zauważyła, że jeśli to fake, to ktoś może chcieć pułapkę. Wykorzystała swój magiczny komputer - jeden z wielu jakie zbudowała od czasu jak trafiła do Rezydencji.

Z badań Dionizego wynika, że plik YT jest normalny. Został zrobiony i uploadowany na oko bez użycia magii. Metadane do filmiku nie zostały skasowane, filmik został zrobiony dnia poprzedniego w Kotach o 21:11. Autor to IDE jakiegoś urządzenia. Konto założono niedawno. Parę dni temu. Mają też lokalizację - współrzędne ze zrobionego filmiku. Więc wzięli jeden z komputerów Klary, wykorzystują go jako bramę i firewall, odpinają od sieci innych niż internet... 

Dionizy - lokalny haker i włamywacz kont YT - używając maszyny Klary ruszył do działania. I się włamał. Konto stoi przed Dionizym i Klarą otworem; oboje zostawili loggery i - kto się loguje i skąd się loguje. Szperając po koncie znaleźli kilka innych filmików - są private i gotowe do publikacji. Filmiki podają więcej dowodów na poszczególne działania Hektora. A jeden z tych filmików pokazuje Hektora w niekorzystnym świetle w świecie magów...

Najlepszy filmik i totalny fake. Hektor Blakenbauer molestuje swoją asystentkę - Kingę Melit.

Chwilowo nic w tych filmikach więcej nie występuje. 
Alina grała Wandę i przygotowali grupę filmików szkalujących różnych magów. Dodatkowo grupę filmików, gdzie oskarża różnych prokuratorów. WSZYSCY ją molestowali. Wszyscy prokuratorzy...
A wisienką na torcie będzie zlot prokuratorów satanistów. Z Hektorem na czele...
Nie uploadują tych filmików. Chcą nie spłoszyć; chcą się przygotować.

Oki. Kryzys potencjalnie zażegnany. Ale watch tam jest.

A Klara idzie do Edwina wraz z Dionizym i chce wstępną autoryzację o zasoby. Edwin potwierdził - to NIE jest Wanda. Prawdziwa Wanda jest w Rezydencji Tymotheusa.

Kilka godzin później watchdog się odpalił. Ktoś zalogował się na konto Wandy. Dostali wskaźnik - gospodarstwo agroturystyczne w Kotach. Klara przebija się, wyłącza diodę kamerki na laptopie uploadera i chce go zobaczyć. A tam... Wanda. Wbili jej się do telefonu; sprawdzili co i jak - a Wanda wygląda na sztucznego człowieka Diakonów. Ma fałszywe kontakty, fałszywe konwersacje, fałszywe zdjęcia.

Ale w liście kontaktów Wanda miała kilka osób które Dionizy kojarzy. Są to osoby, które kiedyś zostały przez Dionizego porwane dla Edwina. Dionizy je porywał dla Edwina, który je leczył i oddawał; chodziło o magiczne choroby. I te osoby mają nieprawdziwe numery telefonów. Dokładniej: sześć osób ma numery telefonów nie pasujących do imienia i nazwiska.
I "Wanda" nagrywa filmik - o popularnym i znanym lekarzu (Edwinie), który w rzeczywistości jest psychopatycznym mordercą i ona ma na to dowody. Jak zgłosiła Hektorowi - wiadomo, jak się to skończyło.

Jako, że to gospodarstwo agroturystyczne, jest tam sporo gości. I sporo telefonów.
Klara przygotuje zestaw robocików, cichą dronę...

Kolejna rozmowa z Edwinem...

Na pytanie o co chodzi z tymi sześcioma ludźmi - Edwin powiedział, że nad tymi przypadkami pracował z różnymi lekarzami, ale za każdym razem te przypadki były uznawane za dowód marnowania jego czasu i jego umiejętności przez magów. Innymi słowy, burza w świecie magów.
Dionizy wysłał patrole policyjne by sprawdzić co się dzieje. Okazało się, że WSZYSCY sześcioro umarło poprzedniego dnia...

Edwin jest w szoku. Klara poprosiła go o sprawdzenie, czy ktoś ubił tych ludzi? Jak oni zginęli? Grupa lokalnej policji ma przynieść do Rezydencji zwłoki tej piątki (zwykle chodził osobiście).

Alina, Dionizy i Adrian Murarz mają się dostać do Kotów; dowiedzieć się co się stało. Dowodzi Dionizy.
Zanim przeszli przez portal - Edwin ich ostrzegł z ponurą miną. Ci, którzy poszli po zwłoki zginęli - srebrne szrapnele. To była pułapka... na Edwina. Magiczna.

Mówimy o kimś, kto zabił kilka osób tylko po to, by w ich ciałach zastawić pułapkę... która MOŻE trafić Edwina. Gdyby Edwin zadziałał zgodnie ze standardowym działaniem, zginąłby lub skończyłby bardzo ciężko ranny.

Tymczasem Hektor dostał telefon od Edwina. Edwin powiedział "wracaj, był na mnie zamach". Hektor nie gadał wiele tylko wycofał się... dostał telefon od swojej asystentki, Kingi, która powiedziała, że czeka na niego ważny świadek na Skubnego. Hektor powiedział jej, żeby ktoś inny go przesłuchał. Kinga trzęsącym głosem powiedziała, że czyta z kartki i że jeśli Hektor nie wejdzie do budynku to już jej żywej nie zobaczy. W pobliżu nie ma maga bo Irytka. Hektor z bólem zdecydował się wrócić ratować swoją asystentkę - Kleofas Bór powiedział, że zawsze wiedział, że Hektor podejmie właściwą decyzję i gratuluje prokuratorowi odwagi. Potraktował go paralizatorem i odesłał do Rezydencji...

Po czym odważnie weszli do Prokuratury.

Hektor się obudził. Edwin powiedział Hektorowi, że ów dzielnie walczył (na miarę swoich możliwości), ale został pokonany; Kleofas i wspomagani już jadą z odzyskaną Kingą Melit. Kinga była torturowana artefaktem; została połamana i ma niewiele czasu życia. Cierpi. Przywiozą ją do Rezydencji, Edwin jej pomoże.

Rezydencja jednak nie wpuściła Kingi. Sygnał, że Kinga stanowi śmiertelne zagrożenie dla Blakenbauerów; jest zapułapkowana i jakikolwiek wpływ energii magicznej na Kingę spowoduje infekcję Blakenbauera. Edwin ściął się z Ottonem - chciał ją ratować - Otton kazał Kingę zabić (coup de grace)...

...czyli to była pułapka na Hektora. Ale ta pułapka nie zadziałała, bo Edwin zadziałał przedtem. Ten sam wzór - przeciwnik ma świetne pomysły, ale nie adaptuje. Nagrania z kamer to potwierdziły - stróż poszedł w obręczy (artefakt) do Kingi, założył jej obrącz, użył artefaktu itp. Wszystko było przewidziane, z wariantami - było świetnie przewidziane, ale nie było tam nic co pozwalało na działanie autonomiczne. 

Badania Edwina pokazały, że to była iluzja katalityczna. Kinga zarażałaby tylko Hektora. Nie musiała zginąć. Kolejne starcie Edwina z Ottonem, który powiedział, że Ród jest ważniejszy niż jakaśtam Kinga. Otton zakazał Blakenbauerom opuszczać Rezydencję i wyplątać się ze wszystkich spraw. Muszą się bronić.

Tymczasem "Wanda" uploadowała nowy filmik jako unlisted.  Tam wyjaśniła, że tak jak ją skrzywdzono, tak ona odpłaci. I "pokaże, jakie bestie kryją się pod maską Blakenbauerów - łącznie z kobietami"...

## Wynik misji:

- Blakenbauerowie wycofują się z działań; skupiają się na obronie siebie
- Kinga Melit nie żyje

# Streszczenie

Pojawiła się "The Governess" która chcąc wyłączyć Blakenbauerów z akcji zabijała metodycznie eks-pacjentów Edwina i asystentkę Hektora, aż Blakenbauerowie dostali zakaz od Ottona opuszczania Rezydencji (fakt, że Edwin i Hektor mogliby zginąć jakby nie zmienili nawyków bardzo pomógł). Governess wyraźnie celuje w Blakenbauerów. Wygląda jak Wanda Ketran (ale to NIE ona) i planuje zniszczyć Hektora. Kinga Melit zginęła jako zasadzka na prokuratora.

# Zasługi

* mag: Hektor Blakenbauer, który bohatersko chciał uratować swoją asystentkę, ale Bór potraktował go paralizatorem.
* mag: Klara Blakenbauer, dostarczająca wszystkich potrzebnych gadżetów i artefaktów i źródło pomysłów dzięki którym nie stała się krzywda Edwinowi.
* vic: Alina Bednarz, aktorka w twarzy Wandy, nagrywająca filmiki kontrujące kanał "Wandy".
* vic: Dionizy Kret, naczelny haker oglądający więcej filmików "Wandy" niż by kiedykolwiek chciał.
* czł: Kinga Melit, ofiara zamachu na Hektora. Umarła w męczarniach, gdyż Otton nie pozwolił Edwinowi próbować jej pomóc. KIA.
* mag: Karolina Maus, "The Governess" jako "Wanda Ketran", metodycznie niszcząca wszystko co Blakenbauerowie kochają by wprowadzić ich w paranoję i lockdown w Rezydencji.
* czł: Wanda Ketran, faktycznie nieprzytomna i nieaktywna w Rezydencji Tymotheusa. 
* czł: Kleofas Bór, który uratował Hektora przed katastrofą rażąc go paralizatorem, po czym wszedł do Prokuratury.
* mag: Edwin Blakenbauer, na którego zabójczyni polowała bazując na jego przeszłości; nie pozwolono mu uratować Kingi.
* mag: Otton Blakenbauer, wprowadził areszt domowy Blakenbauerów i bezwzględnie chroni swoją rodzinę.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
            1. Powiat Czelimiński
                1. Koty
                    1. Dwie hałdy
                        1. Agroturystyczny domek, gdzie zabunkrowała się The Governess (Karolina Maus) by zlikwidować siły Blakenbauerów

# Czas

* Dni: 1