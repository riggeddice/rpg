---
layout: inwazja-konspekt
title:  "Magimedy przed epidemią"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171031 - Utracona kontrola (PT, DA)](171031-utracona-kontrola.html)

### Chronologiczna

* [171031 - Utracona kontrola (PT, DA)](171031-utracona-kontrola.html)

## Kontekst ogólny sytuacji

### Wątki konsumowane:

* Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
* Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
* Jak wbić klin między Fornalisa a Sądecznego? (Kić)
* Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
* Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów
* Zofia: zostanie zniewolona i spętana, “zbyt duże organizacje są niebezpieczne”, impulsywna ryzykantka
* Sądeczny: dążenie do przewagi techno/magi/info, dążenie do władzy absolutnej: ekipa Sądecznego "Młot Sądu"

## Punkt zerowy:

* Ż: Kto może zdradzić Paulinie jakieś plany Dukata na ten region? Następne ruchy.
* K: Dobrocień.
* Ż: Skąd Maria wie o następnych ruchach oportunistycznych magów z okolicy?
* K: Zrobili coś głupiego i jej ludzcy sojusznicy to powiedzieli Marii.

## Potencjalne pytania historyczne:

* Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?
    * Domyślnie: TAK
* Czy będzie tu ściągnieta ekipa by zrobić porządek ze "świniami" i Świecą?
    * Domyślnie: TAK
* Czy Dukat wprowadzi tu nowego agenta - Czykomara - na czas braku Sądecznego?
    * Domyślnie: TAK

## Misja właściwa:

**Dzień 1**:

Paulina siedzi przed wyłączoną i ciężko ranną Anetą Rukolas. Aneta nie ma blizny, ale jej wzór jest rozszarpany i skrystalizowany przez portalisko. To jest bardzo skomplikowany problem...

Odwiedził ją Dobrocień. Powiedział Paulinie, że Sądeczny wprowadzi nową tymczasową regentkę - Felicję Szampierz. Sądeczny mówił, że nie trzeba, ale jest w pewnych niełaskach u Dukata. A Felicja ma zero skilli negocjacyjnych. Zero. Ona zrobi wojnę ze Świecą, zdaniem Dobrocienia. Stąd Dobrocień poprosił Paulinę, by ta wystawiła OPINIĘ MEDYCZNĄ Dukatowi. By powiedziała, że nie jest potrzebna Felicja jako regentka.

Paulina próbuje wydobyć od Dobrocienia jak najwięcej rzeczy co powiedzą czemu Felicja nie jest dobrym wyborem. Jej pasuje status quo. Dobrocień: Chce Pomóc (-2), Jest Nieważny (3), Nie Czai Polityki (3). Ale Paulina wie jak łączyć plotki i mu zniwelowała to że nie czai polityki. 6v5->S. Paulina złapała dwa aspekty które sprawiają, że Felicja się zupełnie nie nadaje: Tends To Go Overkill, Najpierw Walcz Potem Rozmawiaj. Ma na to dowody od Dobrocienia i z ogólnej okolicy. Super.

Teraz Paulina zdecydowała się odwiedzić Sądecznego. Sądeczny mieszka w Upiorzcu; trudno, po to jest awian. Paulina poleciała go odwiedzić.
Tam czeka na nią służąca Sądecznego, Elwira Czlikan. Piękna - prawie do poziomu Diakonki.
Elwira wybudziła Sądecznego i wpuściła Paulinę. Pokój silnie lapisowany, potężny magimed. Ale Sądeczny jest w bardzo złym stanie - rany Skażone Eterem, ciężkie rany fizyczne, zatrucie stymulantami bojowymi, pogryziony przez świnie, paraliż w lewej ręce... ogólnie, kiepsko.
Porozmawiali chwilę o Efraimie Weinerze, jak to Paulina go wyrufusiła już kilka razy. Potem o Anecie, którą Paulina zabrała Efraimowi. Potem Paulina zaoferowała że skalibruje lepiej magimed dla Sądecznego - on się zgodził.
Magimed jest (2), Dobrze Skalibrowany (3), Bardzo Zaawansowany (3). Paulina się zna na zaawansowanych magimedach i wie jak naprawiać wzór. 6v5->S. Zdecydowanie lepiej niż Efraim. Sądeczny aż uniósł brwi ze zdziwieniem.

* Dziękuję za uratowanie tych ludzi - Paulina, szczerze
* To mój teren. To moi magowie i ludzie. Wszyscy. - Sądeczny, z uśmiechem

Sądeczny zapytany o sprawę z Felicją powiedział wyraźnie - nie martwi się. Będzie musiał czyścić bo ona sobie nie poradzi. Dukat nie ma kadr. Po prostu NIE MA kadr na jego miejsce. Zdaniem Sądecznego powinien wysłać Czykomara, bo po Felicji będzie musiał serio sprzątać... to jego teren i jak tylko Sądeczny wyjdzie i wyzdrowieje to WRÓCI porządek. Tak jak wcześniej. I to będzie twardy powrót porządku.

Paulinę aż zastanowiło czemu Sądeczny jest tak pewny siebie że wróci i Felicja nic nie spsuje. Sądeczny jest Mistrzem Dezinformacji - Paulina zna ten teren i wie w jakim stopniu to co mówi jest prawdą. No i Paulina ma Marię. A Sądeczny jest w złym stanie. Sukces. Paulina dowiedziała się:

* Nikt poza Sądecznym nie umie operować propagandą i dezinformacją. Nie tak.
* Sądeczny ma ludzi. Ma gangi. Ma ekipy. Maszyna będzie się toczyć. Zanim nowa osoba się wdroży, mnóstwo czasu.
* Dukat nie ma kadr.

Paulina zrozumiała - jak Felicja tu przyjdzie i zacznie się rozbijać jak pijany zając, to nic nie zdziała. I tylko wszystko się popieprzy. Sądeczny zrobił Paulinie toast szklanką z herbatą widząc jej realizację. Paulina go zostawiła i Sądeczny wrócił do swojego magimedu. Elwira podeszła do Pauliny i spytała czy coś jeszcze. Paulina podziękowała i się wycofała.

Czas na spotkanie z Tamarą Muszkiet. Przy drzwiach stoi jeden konstruminus; wpuścił Paulinę do "kompleksu". Bez większych kłopotów Paulina dostała się do Tamary, z którą siedzi Dalia. Tamara powiedziała, że ma kompetentną lekarkę a Paulina potwierdziła. Tamara leży na łóżku w piwnicy, gotowa do przywiązania i ogrzewana. Dalia powiedziała, że jest to nawrót Lodowego Pocałunku i Tamara ma echo eteru w sobie. Niewiele może zrobić, regeneracja Tamary będzie długa. Dalia robi co może, ale... 

Paulina sama nie wie jak pomóc Dalii, nie tym co ma. Widać, że Tamara wzięła na siebie za dużo.

Paulina umieściła u Sądecznego fizjoterapeutę, który jest nieświadomym agentem. Ma fizjoterapeucić, ale jednocześnie Maria będzie wydobywać z niego informacje tak by on sam nie wiedział że jest agentem. Bardzo Paulina to doceniła. Paulina wypytała Dalię o stan magimeda. Powiedziała Dalii wyraźnie - ona pomoże jej z opanowaniem Tamary a Dalia pomoże Paulinie z Anetą Rukolas. Dalia się zgodziła z radością, praca z eksperymentalnym poziomem jest czymś dla niej idealnym. Paulina dała Dalii jeszcze silny usypiacz na Tamarę, jakby do czegoś doszło...

Sylwester Bankierz powiedział Paulinie, że może korzystać z zasobów Świecy. Pokazał jej zasoby Dalii i zasoby które on jako Rezydent ściągnął.

**Dzień 2**:

Z rana Paulina spotkała się z Dukatem. Dukat zaznaczył, że potrzebuje wiedzy Pauliny w ramach soullinkowania - w obszarze diagnostyki i wzmocnienia. Syn Dukata tak naprawdę nie żyje - jest tylko echo, sygnał. I on go przesyła jak może, splątuje z czym może... a "sygnał" się "uodparnia" na coraz to nowe kuracje.

Tak czy inaczej, nie po to Paulina tu przyszła i rozmowa szybko przeszła na inne tematy. Gdy Paulina powiedziała, że Świeca potrzebuje pomocy bo Tamara. I Tamara uratowała wielu. A Świeca nie daje dużego wsparcia. Dukat się uśmiechnął, gdy Paulina powiedziała, że Świeca potrzebuje pomocy. Z uwagi na stan Tamary Dukat się zgodził - przekaże jeden z magimedów (nie najlepszych, acz sprawnych i trochę więcej niż wystarczający) Paulinie, dla Tamary. Dla Świecy. Zgodził się, by na razie nie mówić, że to od niego, by Tamara przyjęła.

Dukat zapytany przez Paulinę o wprowadzenie Felicji się zdziwił. Nigdy nie rozważał Felicji. Zawsze myślał o Czykomarze. Czykomar co prawda nie dba o teren i jest zbyt delikatny, ale on zapewni informacje Dukatowi czy wszystko wie na tym terenie - czy nie dzieje się tam coś bardzo szkodliwego. Paulina zaproponowała, by Sądeczny został tam nadal rezydentem a Czykomar wszedł jako agent Wydziału Wewnętrznego Dukata. Dukat zauważył, że ma mało agentów i może nie zadziałać. Paulina zauważyła, że gangi się na Czykomara wypną jak spróbuje przejąć władzę.

Dukat jest (3) i Zirytowany Zaniepokojony Sądecznym (3). Paulina ma dobre argumenty +1 i powiedziała mu o Allitras +1. Przypomniała, że Sądeczny regularnie pomaga mu czymś dziwnym z synem za kolejne +1. Dodatkowo powiedziała mu o KADEMie. Dukat facepalmował... cóż. Zgodził się z Pauliną - Sądeczny zostaje a Czykomar jako obserwator. Nawet nie - w świetle Allitras, Czykomara nie ma co tam wprowadzać, pogubi się. Chwilowo zostaje Sądeczny i nie ma innych tematów.

Paulina poprosiła też Dukata o magimed dla siebie - i tak leczy jego ludzi a ma teraz jeden ciężki case. Dukat się zgodził.

Paulina opuściła Dukata z szerokim uśmiechem. Oba magimedy przyjadą następnego dnia do Pauliny, w vanie.

Paulina wróciła do siebie i ściągnęła Dalię po tym jak Dalia się upewniła, że Tamara nie będzie rozrabiać. Czas skupić się na Anecie Rukolas. Podczas prezentacji Anety Dalii odezwała się jeszcze do Pauliny Maria. Maria ostrzegła Paulinę, że pojawiły się ruchy w okolicy pod tytułem "ten teren jest niechroniony i można grabić do woli" - szuka się mięśni. Dla Pauliny jest to ważna wiadomość. Na potem.

Aneta Rukolas jest w złym stanie. Jest stabilna, ale Eliksir wżarł jej się we Wzór. Ma niestabilną strukturę, ma Echo Eteru, jest Przepalona Magicznie i jest Fizycznie Słaba. Wniknęła w nią Efemeryda (wyszła już, ale). Ogólnie - cała historia dewastacji nieszczęsnej czarodziejki. Dalia poskrobała się po głowie. Ciężka sprawa, nawet diagnostycznie.

Na początku trzeba obniżyć jej pole magiczne i doprowadzić ją do stabilnego działania. Fizycznie Słaba, Echo Eteru, Przepalona Magicznie, Nośnik Świńskiej Efemerydy. Koszmarnie trudna sprawa.

Wpierw, uznały, muszą wyciągnąć z niej Eter, bo to za mocno zaburza. Wpierw potrzebna jest dokładna diagnostyka pierwszego poziomu tego co się jej stało. Paulina przygotowała sobie odpowiedni sprzęt i Quarki przez które rzuca, po czym zabrała się za diagnostykę z pomocą Dalii: Paulina potrzebuje też lapisu (zajumała Świecy) i pijawek magicznych (zajumała Dalii). Z Dalią wszystkie zasoby dają Paulinie silne +6. 13v11->SS. Paulinie i Dalii udało się to zdiagnozować, acz Dalia złapała niezbyt ciężki klątwożyt.

Aneta jest zarojona klątwożytami, pochodnymi tego "czegoś" w Eterze. Badanie samych klątwożytów (kosztem Anety) pozwoliłoby zrozumieć naturę tego co jest w Eterze. Tu jest moment kiedy Paulina stwierdziła, że to TEN moment. Czas na kontakt z Tomaszem Myszeczką.

Myszeczka jest słaby i deliryczny. Potrafi myśleć, ale... nie jest silny. Nie może czarować. Nie zawsze kontaktuje. Ściągnął członka swojego rodu - Radosława - jako swojego lekarza. Ten coś tam robi żeby Myszeczce pomóc i Myszeczka przekierował rozmowę na niego.

Radosław powiedział, że jest praktykantem. Co jest Tomaszowi? Klątwożyty, słaby, Skażony. Czas i nie może czarować. Ma pod ręką farmę viciniusów na szczęście. Paulina zaczęła go przekonywać, że potrzebuje viciniusów też TUTAJ do pomocy jej pacjentom (Anecie). Paulina potrzebuje viciniusów dla Sądecznego, Tamary i Anety... Radosław zauważył, że viciniusami trzeba się opiekować i sterować, przygotowywać różne rzeczy, karmić... a Paulina nie jest hodowcą.

Dalia zauważyła, że ona się zna na dostosowywaniu różnego rodzaju środków i półproduktów w leki - specjalizuje się w tego typu działaniach. Zna się na "prostych" i "małych" viciniusach jak pijawki czy języczniki, ale nie na nipustakach czy glukszwajnach.

Paulina wpadła na rewelacyjny pomysł. Przeniesie tymczasowo bazę i centrum dowodzenia kryzysowego do Myszeczki. Tam w końcu jest wszystko ;-). Radosław jest nieszczęśliwy - nie ma uprawnień, jest nieprzygotowany na to... boi się trochę wujka. Paulina zauważyła, że będzie miał szanse się czegoś nauczyć i ona pomoże jego wujkowi (+2 za oba). No i Paulina obiecała, że zapozna go z Draceną (+1). SS. Młody się wciąż waha, więc Paulina obiecała zapłatę od zarówno mafii jak i Świecy. Radosław się zgodził - wie, że wujek jest Ferengi na kasę...

Dalia przez 2-3 dni będzie nieaktywna. Poza tym, nic jej nie będzie.

Paulina powiedziała Sądecznemu o "mięśniach" się zbierających do grabieży. Sądeczny się tym zajmie - a dokładniej, przekaże odpowiednie dyspozycje.

## Wpływ na świat:

JAK:

1. Co na tej sesji dookoła jakiegoś konfliktu czy zdarzenia było fajne? Co chcesz by w ramach tego się poszerzyło?
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

* Kić: Sądeczny uznał Paulinę za gracza a nie pionek - docenił to czym jest.
* Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?: NIE WIEMY. (Żółw). 5/10.
* Czy będzie tu ściągnieta ekipa by zrobić porządek ze "świniami" i Świecą?: TAK. (Żółw). Siła: 5/10.
* Czy Dukat wprowadzi tu nowego agenta - Czykomara - na czas braku Sądecznego?: NIE.

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw: (16)
    1. Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
    2. Wzór Anety jest trwale uszkodzony - wymaga czegoś nowego (Żółw)
    3. Większość magów dotykających portaliska podczas katastrofy - nie PC - jest zaklątwożytowana (Żółw)
    4. Klątwożyty i efemerydy prowadzą do zbliżającej się epidemii (Żółw)
    5. Jak krystality mogą pomóc w zatrzymaniu epidemii? (Żółw)
1. Kić: (8)
    1. Tomasz Myszeczka pomaga w tematach leczniczych. Poniesie koszty, ale zrekompensuje sobie wiele innych rzeczy. (Kić)
    2. Magimed zostaje u Pauliny jako trwały surowiec. (Kić)
    3. Kajetan odnajdzie niebezpieczne elementy Harvestera przed Dukatem (Kić)

# Progresja



# Streszczenie

Paulina dokalibrowała magimed Sądecznego i wsparła go w tym, by pozostał rezydentem mimo gniewu Dukata że to się wszystko sypie. Potem poprosiła Dukata o magimed dla Tamary. Następnie z Dalią skupiła się nad Anetą Rukolas - Dalia się zaraziła klątwożytem. Wiele wskazuje, że lokalny Eter jest bardzo niebezpiecznym i Skażonym miejscem... więc by zatrzymać potencjalną nadchodzącą epidemię Paulina przesunęła bazę do hodowli Myszeczki. Tam są viciniusy pomagające w leczeniu.

# Zasługi

* mag: Paulina Tarczyńska, załatwia każdemu magimeda od Dukata - Świeca, mafia, sobie... zajmuje się Anetą Rukolas z Dalią i zyskała niechętny szacunek Sądecznego.
* mag: Aneta Rukolas, jej Wzór jest zbyt uszkodzony na naprawę; trafiła do Pauliny na leczenie. Sądeczny nic z tym nie zrobi.
* mag: Jakub Dobrocień, ostrzegł Paulinę przed tym że Dukat chce zastąpić Sądecznego Felicją Szampierz. Przyszedł do Pauliny by coś z tym zrobiła.
* vic: Elwira Czlikan, piękna strażniczka i służka Sądecznego. Silnie zmodyfikowana magicznie i oddana swemu panu.
* mag: Robert Sądeczny, bardzo ciężko chory i ranny po sprawie z portaliskiem. Paulina obroniła go przed Dukatem. Chwilowo wyłączony z akcji w swoim pałacyku.
* mag: Dalia Weiner, lekarka Tamary która pomogła Paulinie zdiagnozować Anetę Rukolas; złapała krótkiego klątwożyta. Lepsza w eksperymentalnych niż kłótniach z Tamarą.
* mag: Tamara Muszkiet, bardzo ciężko chora, nawrót Lodowego Pocałunku i wyniszczona po portalisku. Trudna pacjentka. Paulina załatwiła dla niej od Dukata magimed.
* mag: Sylwester Bankierz, oferujący Paulinie zasoby Świecy i współpracujący z kim się da.
* mag: Tomasz Myszeczka, złapał klątwożyty, jest deliryczny i ściągnął Radka by się nim zajął. Sęk w tym że Radek jest praktykantem.
* mag: Radosław Myszeczka, nieszczęśliwy praktykant który potrafi zajmować się viciniusami. Zgodził się, by Paulina przeniosła centrum bazy do Myszeczków.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Głuszców
                1. Upiorzec
                    1. Pałacyk Gryfa, elegancki ze świetnym magimedem Roberta Sądecznego
            1. Powiat Pustulski
                1. Byczokrowie
                    1. Niezniszczalna Kazamata, gdzie w lochach ma "pokój" Tamara by mogła spokojnie regenerować
                1. Krukowo Czarne
                    1. Gabinet Pauliny, poprzednie centrum dowodzenia medyczno-epidemiologicznego i miejsce gdzie trafi Paulinowy magimed.
                1. Rogowiec
                    1. Hodowla Myszeczki, nowe centrum dowodzenia medyczno-epidemiologicznego Pauliny

# Czas

* Opóźnienie: 0
* Dni: 2

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się Kociebora z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak dla Kociebora. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    4. Konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem. (Raynor)
    5. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo. (Raynor)
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
    4. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    4. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    5. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    4. Lewiatan się zbliża. Jest już bardzo blisko.
1. Tamara x Sądeczny?
    1. Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra. (Kić)
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.
    4. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    5. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    6. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    5. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni. (Raynor)
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
    2. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    3. Portalisko jest silnie połączone z Allitras.
    4. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
