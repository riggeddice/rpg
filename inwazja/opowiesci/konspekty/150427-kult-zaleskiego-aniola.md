---
layout: inwazja-konspekt
title:  "Kult zaleskiego Anioła"
campaign: swiatlo-w-zalezu-lesnym
gm: żółw
players: kić, til, viv
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150427 - Kult zaleskiego anioła (An, Mo)](150427-kult-zaleskiego-aniola.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Punkt zerowy:

W miasteczku o pięknej nazwie Zależe Leśne grasuje potwór.

Ż: Jak na obecności potwora zyskuje lokalny mag?
K: Quarki, ekstrakcja i akumulacja.
Ż: Jak na obecności potwora zyskują niektórzy ludzie?
T: Pozornie są dużo zdrowsi, silniejsi, fizycznie lepsi i do przodu. I są długowieczni.
Ż: Jak przez obecność potwora cierpią wszyscy inni?
K: Ta energia jest drenowana z wszystkich innych; to redystrybucja energii.
Ż: Jak przez obecność potwora cierpi postać Tila?
T: Wplątał się w sieć zależności energetycznych.
Ż: Czemu potwór jest na dłuższą metę bardzo szkodliwy? Co się stanie jak nikt nic nie zrobi?
K: Osłabieni wymrą, wzmocnieni zmutują (tkanka magiczna).
Ż: Czego potrzebuje bądź pragnie potwór?
T: Wyznawców.
Ż: Gdzie najczęściej atakuje potwór?
K: W kościele.
Ż: Jakie miejsce potwór omija?
T: Siedziba lokalnego maga.
Ż: Czym objawia się obecność potwora w oczach osób niewtajemniczonych?
K: Pojawiają się znaki manifestacji bożej; halo, chóry anielskie w obecności potwora.
Ż: I czym ludzie to racjonalizują?
T: Religią. Jedni wierzą że tak musi być bo to religia, inni twierdzą, że to nawiedzenie.
Ż: Dlaczego Andromeda przybyła do tego miasteczka?
K: Dotarły do niej słuchy o tym i liczy, że jeśli dzieje się tam coś magicznego to zwlecze się za nią jakiś mag.
Ż: Skąd X wie o magii?
T: Wielokrotnie się na nią natkął w trakcie swoich podróży.
Ż: Skąd Andromeda wie, że może zaufać X?
K: Już wcześniej razem podróżowali i rozwiązali jakąś sprawę.
Ż: Skąd X wie, że może zaufać Andromedzie?
T: Ponieważ ufa ludziom.
Ż: Jakimi przesłankami kierował się cywil, który chciał przed wami zabić potwora?
K: Jedną z osób drenowanych był ktoś bliski. Zauważył, że ta bardzo bliska osoba zmieniła preferencje bardzo drastycznie; znalazł odstraszacz / osłabiacz.
Ż: Dlaczego potwór go dorwał; gdzie cywil popełnił błąd?
T: Zaczął pertraktować.
Ż: Podaj jedno istotne miejsce w miasteczku.
T: Pański dwór
K: Dawna chata lokalnej wiedźmy
Ż: Gdzie mieszka X?
T: W lokalnej pensji. W pokoju u kogoś wynajmującego pokoje.
Ż: Gdzie mieszka Andromeda?
K: W tej samej pensji.
K->Ż: Co cennego lub przydatnego można znaleźć w dawnej chatce wiedźmy?
Ż: Potencjalną broń, której niekoniecznie chcecie użyć.
T->Ż: Czy czarodziejka która jest w tym miasteczku jest świadoma tego co się dzieje?
Ż: Nie tylko jest świadoma ale i go wspiera w jego działaniach, choć pilnuje (naiwnie wierząc), by nie wyrwał się spod kontroli.

## Misja właściwa:

Pogrzeb nauczyciela historii. Słoneczny, piękny ranek. Andromeda i Mateusz. Zależe Leśne to niewielkie miasteczko, jedno gimnazjum. Zmarły (Paweł Franna) nie miał tu rodziny. Andromeda i Mateusz nie widzieli ciała, ale większość ważnych osób tu jest. Ludzie nie opłakują specjalnie nauczyciela, poza dyrektorem szkoły (wyraźnie wstrząśnięty) i jedną zapłakaną uczennicą. Widząc tą uczennicę ludzie lekko kręcą głową bez szczególnego.

Ksiądz - bez autorytetu. (Rafał Czapiek)
Aneta - wyraźny lider, patrzy z dezaprobatą na Karolinę
dwie starsze panie - wyraźna niechęć do zmarłego
konflikt wzbudził zainteresowanie 2 pijaczków; potem ruszą na Mateusza. Zidentyfikowali go jako łatwy cel.

Andromeda spróbowała zobaczyć obraz sytuacji. Zobaczyła, że tylko dyrektor i Karolina są smutni przez śmierć tego nauczyciela.
Pracownika domu pogrzebowego który zajmował się zmarłym nie ma na pogrzebie. Jest to dziwne.
Potwór robi ruch (przeciw pracownikowi domu pogrzebowego) - hidden movement.
Trumna jest wzmocniona deskami. Tak, by nikt nie mógł jej otworzyć bez spojrzenia do środka.

Koniec pogrzebu. Aneta nie pozwala Karolinie odejść (a rodzice nastolatki się na to zgadzają!), ale dwie starsze panie najechały na Anetę za zachowanie na pogrzebie. Korzystając z sytuacji Karolina uciekła a Aneta i staruszki zaczęły się kłócić. Podczas tej rozmowy Aneta zasugerowała, że zmarły był pedofilem.

Marek podając się za dziennikarza poszedł porozmawiać z Anetą i staruszkami. Powiedział, że chce z nimi zrobić wywiad - znaczące osoby w społeczności. Zgodzili się; Aneta zdecydowała się spotkać z Markiem za 30 min w kawiarence.

Marek poszedł pooglądać groby by zobaczyć, jak wygląda umieralność. Jakieś 2-3 miesiące temu pojawił się spike śmierci, starsi ludzie umierają wcześniej (np. dożywają mniej lat).

Andromeda poszła porozmawiać ze starszymi paniami by wydobyć plotki o społeczeństwie. Dała im do zrozumienia, że nie do końca (wcale) zgadza się z tą Anetą i zaskarbiła sobie ich zaufanie.
- 2 miesiące temu Aneta miała wypadek; rozwaliła się na motorze. Wydobrzała i... już nie jeździ leczyć króliczków. Wnuczce staruszki umarł piesek.
- Aneta podobno widzi anioły i Bóg jest z nią.
- Błazoń przywiózł z miasta to, że nauczyciel był pedofilem; dlatego właśnie nauczyciel się zabił.
- Jeżeli się sam nie zabił, Bóg go ukarał.
- Adam Bartniok (człowiek z zakładu pogrzebowego) się zapił i nie wytrzeźwiał od czasu zajmowania się zwłokami pedofila.

Marek miał spotkanie z dwoma żulami jeszcze na cmentarzu; ci posądzili go o NERKOFILIĘ by się bić. On ich zamanipulował (klęka się tylko przed Bogiem) i poszedł sobie.

Spotkanie i rozmowa z Anetą. Marek zaabsorbował jej uwagę pytając o pedofila. Zirytowała się, i wtedy Andromeda użyła subtelnego ruchu, dopytując o manifestację anioła. 
Dowiedziała się:
- manifestacja anioła: kształt anioła, dwa kolory, światło.
Potem Marek ją przycisnął by dopytać o jej rolę.
- nałożył na Anetę presję, by ta robiła głupie ruchy i się odkryła.
- Aneta przyznała się, że działała jako uzdrowicielka wykorzystująca wiarę i moc, leczyła też ludzi.

Przy zwiększonej presji ze strony Marka, Aneta wyszła i zadzwoniła w nieznane miejsce.

Marek zamówił przez internet 4 granaty hukowe. Szczególnie skuteczne do niszczenia szkła.

Marek poszukał danych o tym dworku. Dworek pana x anioł.
- hrabia Onufry Zaleski był czarną owcą rodu. To miejsce (dworek) zwało się Anielski Dwór i było "strzeżone przez anioła". Do czasu aż pojawił się hrabia, który "był wampirem". Ludzie umierali a on się nie starzał i dobrze się bawił.
- hrabia jest pochowany na lokalnym cmentarzu. W krypcie.
- chłopi chronili się przed hrabią w kościele. W dzień. Nocą nic im nie groziło.
- Onufry rozpieprzył wszystkie witraże. Coś się stało, że anioł zwrócił się kiedyś przeciwko Onufremu. Wtedy Onufry zniszczył wszystkie witraże, przestał dostawać energię i umarł niedługo potem (1-2 miesiące, szok z gwałtownego starzenia i braku energii magicznej).
- anioł obrócił się przeciwko hrabiemu gdy hrabia ruszył przeciwko księdzu. Ksiądz miał "nawróconego" członka świty hrabiego który prosił o azyl. Hrabia zażądał go z powrotem, ksiądz powiedział hrabiemu "nie". Połączenie księdza # osoby która chciała opuścić świtę wystarczyły by anioł zobaczył, że działa w niewłaściwej służbie.

Czas udać się na plebanię. Andromeda próbuje przekonać księdza, że Andromeda chce przekazać obraz kościołowi, ale do tego celu chce zobaczyć stare kroniki by jak najlepiej odwzorować.
- jakiś rok temu anonimowy dobroczyńca załatwił witraż. Witraż bez niczego niebieskiego (lapis)! Ten witraż jest specjalny; przedstawia krwistoczerwonego anioła w majestacie.
- witraż ten był zamontowany na "miejscu przeklętym"; nigdy nie było tam witrażu (czyt. od czasu Onufrego Zaleskiego).
- ksiądz nigdy nie widział anioła. Nigdy nie widział żadnych anielskich śladów. Nic nie wie.

Marek napisał maila do znajomego w policji lokalnej (Zależa).
- Lekarz sądowy i TEN policjant (ten znajomy) to podpisali. Ale to zupełnie nie ma sensu... policjant przeprosił i chce to sprawdzić.

Marek odciągnął księdza by dać Andromedzie możliwość porozmawiania z zastraszoną nastolatką w szafie. 

W kronikach parafii jest osoba młoda, wyglądająca na starzejącą się rzadziej; ksiądz powiedział że jest osobą zamiejscową i trzyma się na odludziu. Potencjalna czarodziejka.

Dobrze... wszystko wskazuje na ten witraż i na działania nieznanej czarodziejki. Pojawia się tu też pewien kult.
Jako rozwiązanie, Marek rozbija witraż granatem hukowym w nocy i w długą. Ucieka.

Andromeda też opuszcza teren, ale maluje obraz. Obraz, który przedstawia kościół oplatany przez viciniusa. Kralotha z anielskimi skrzydłami. Chce tu ściągnąć jakiegoś terminusa...

# Lokalizacje:

1. Świat
    1. Primus
        1. Podlasie
            1. Powiat Orłacki
                1. Zależe Leśne
                    1. Serce
                        1. pensja Błazonia
                        1. kawiarenka Ukrop
                        1. kościół
                    1. Wał Kartezia
                        1. dworek pana
                        1. cmentarz
                            1. krypta Onufrego Zaleskiego
                    1. Koniowie
                        1. siedziba czarodziejki
                        1. dawna chata lokalnej wiedźmy

# Zasługi

* czł: Kasia Nowak jako niewidzialna i subtelna siła wydobywająca kluczowe informacje od pani weterynarz. Też: magnes na terminusów.
* czł: Marek Ossoliński jako podróżnik szukający potworów do pokonania. Udaje dziennikarza, który interesuje się lokalnym aniołem i kultem.
* czł: Paweł Franna jako nauczyciel historii, który próbował zabić potwora przed Zespołem (i zginął próbując). Plotki zarzucają mu pedofilię. KIA.
* mag: Olga Miodownik jako czarodziejka która zyskuje z obecności potwora na tym terenie; sprzęgła się z nim dla wzmocnienia swoich sił.
* czł: Aneta Kosicz jako weterynarz, która najwięcej zyskuje z działań potwora; wygląda świetnie i czuje się świetnie. Prawie umarła gdy rozbiła się na motorze; ozdrowiała cudownie.
* czł: Franciszek Błazoń jako choleryk, który nie radzi sobie z nałogami; wynajmuje mieszkania w Zależu Leśnym. Kultysta Anioła.
* czł: Karolina Błazoń jako 14-letnia ulubienica nauczyciela historii, która została przez niego uratowana; chce wycofać się z kultu Anioła.
* czł: Rafał Czapiek jako ksiądz, który całkowicie nie ma autorytetu w Zależu Leśnym. Sympatyczny i pomocny, chce pomóc swojej parafii.
* czł: Mateusz Kozociej jako dyrektor gimnazjum, któremu naprawdę bardzo szkoda zmarłego nauczyciela historii.
* czł: Kamil Gurnat jako policjant (znajomy Marka), który podpisał papiery świadczące o tym, że nauczyciel umarł w nie tym miejscu. Gdy się zorientował, zaczął własne śledztwo.