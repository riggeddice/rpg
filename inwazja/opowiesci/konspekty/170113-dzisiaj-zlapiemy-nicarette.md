---
layout: inwazja-konspekt
title:  "'Dzisiaj złapiemy Nicarettę!'"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161220 - Zniszczenie posągu Arazille (HS, KŁ)](161220-zniszczenie-posagu-arazille.html)

### Chronologiczna

* [161220 - Zniszczenie posągu Arazille (HS, KŁ)](161220-zniszczenie-posagu-arazille.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Kulturno. Małe miasteczko. Wokół ryneczku: sklep spożywczy, kebab, kamieniczki turkusowe, wieża ratuszowa. Tuż obok rynku stalinowski architekt walnął szare blokowisko. Niedaleko są też kościół i... opera.

## Misja właściwa:

Dzień 1:

Henryk się obudził w hotelu Jantar. Piękny dzień. Trzeba polować na Nicarettę. Informacje od Anny Osiaczek wskazują na miejscowość Kulturno. W powiacie Lubelskim. Henryk zdecydował, że trzeba by to sprawdzić... chłopaki Kaldwora już przekazali informacje, że tam są takie rzeczy. Więcej, zlokalizowali tą, Grażynę (hosta Nicaretty). Nicaretta nie wie, że Henryk wie. Więc... Henryk zdecydował się zdobyć kontakt do lokalnego proboszcza. Ów nazywa się Witold Małek.

Lenartomin. Klepiczek się ucieszył widząc Henryka. Zaprosił go na pierogi. 

* Jak tam sytuacja w Lenartominie? - Henryk Siwiecki, chichocząc w duchu
* Jest wrzenie... ogólnie, dziewczyny są... polują na nie. Uspokajam ich, ale... zrobiły się nam nastroje na Salem. Nawet mamy azylantkę... - podłamany Klepiczek - Schroniła się w kościele, by nikt na nią nie polował.

Henryk i Andrzej poszli na pierogi na zaplecze. Tam Henryk zdecydował się zdominować Klepiczka (sukces: 4v4->5) i squarkował zaklęcie (-1 surowiec, do końca misji jest minionem) - użyto "egzorcysta" - Klepiczek jest przekonany, że był opętany przez demona i to ON podburzył tłumy przeciwko dziewczynom... a Henryk go uratuje.

* Henryku... ta dziewczyna nie ma nic wspólnego z tymi femisatanistkami. Ludzie chcą kogoś zlinczować, padło na nią... - Andrzej Klepiczek, wyjaśniając się Henrykowi
* Masz zadanie. Zadzwoń do proboszcza z Kulturna. - Siwiecki, z zadowoleniem
* Czego byś sobie życzył? Co mam się dowiedzieć czy załatwić? - Klepiczek, oddany Siwieckiemu

"Ksiądz jej pod jej wpływem, więc pewnie jest zdemoralizowany jak skurczybyk... mógłby pełnić rolę alfonsa i umówić mnie w celach seksualnych..." - Raynor, odtwarzający myślenie Siwieckiego

Henryk wymyślił plan - niech Andrzej zrobi pielgrzymkę do Kulturna. Niech Witold uwierzy, że to... jeden ksiądz. Chodzi o to, by wzbudzić taką reakcję, że to jest coś, że wszyscy wiedzą. Na tym rynku jest święte miejsce do pielgrzymkowania. W Kulturnie jest coś do czego warto pielgrzymkować... niech będzie, wieża ratuszowa. Tam jest lokalny święty. Mało święty, ALE LOKALNY. I to lokalny święty; Witold W ŻYCIU się nie przyzna, że nie zna lokalnego świętego...

Henryk przejmuje kontrolę nad Klepiczkiem i dzwoni do Witolda.

* Andrzej Klepiczek z tej strony, z parafii z Lenartomina - Henryk, ustami Andrzeja
* Zamieniam się w słuch - Witold, nic nie podejrzewając
* Jedna sprawa jest trochę personalna, druga oficjalna. Zacznę od tej oficjalnej. Zdaje sobie ksiądz proboszcz zdanie, że z Waszej parafii pochodzi patron (...) - Henryk, zaczynając bullshit

Sukces (5v4). Witold Małek pomoże. Zbierze ludzi, pomoże... to zmieni Pryzmat, wzmocni siły Henryka przeciwko siłom Nicaretty. Bardzo, bardzo korzystne. A Nicaretta jeszcze nic nie wie.

* Druga sprawa, bardziej personalna; słyszałem, że można w pańskim miasteczku ulżyć swym nieczystym żądzom... - Henryk ustami Andrzeja
* To tak jak w pańskim. To jest wszędzie. - lekko oburzony Witold.
* Niedawno powstał dom publiczny w pańskim miasteczku... - Henryk, kusząc Witolda by ten umówił księdza Klepiczka (znaczy, jego).

Henryk użył surowca (-1) Oczy Kościoła, by wyciągnąć jakieś brudy na Witolda. Coś, co go przekona. Witold spanikował, lekko.

* Spróbuję ci... załatwić... spotkanie w tym przybytku nieprawości. Byś... nawrócił tam... Powodzenia Twojej duszy! - lekko skonfundowany ksiądz Małek
* Dziękuję! Przyda się! - uradowany Henryk

Następnie Henryk przekształcił pamięć Klepiczka (-1 surowiec, perma, z NASTĘPNEGO dnia) by ten myślał, że podczas rozmowy przypomnieli sobie o lokalnym błogosławionym i Klepiczek zadzwonił po wpadnięciu na pomysł.

Henryk poszedł zobaczyć azylantkę; jest to dziewczę pod 30, acz przerażone. Klepiczek powiedział, że ona trudni się dość starym zawodem i DLATEGO chcieli ją zlinczować... Henryk powiedział, żeby Andrzej się nią opiekował, bo przychodzą złe czasy.

Po skończeniu roboty Henryk pojechał do Wyjorza. Do swojego pokoju w hotelu ;-).

W nocy obudziło Henryka szarpnięcie magiczne. Coś rozwala jego zaklęcie na Klepiczku... Henryk walczy! 5v4 -> FAIL. Marzena wycofała azylantkę. 5v4 -> SUCCESS. Henryk dowiedział się, że to Marzena. 5v5 -> FAIL. Marzena dowiedziała się, że to Henryk stoi za wszystkim. To on napuścił Klepiczka na femisatanistki, to on skrzywdził (z jej punktu widzenia) Klepiczka. I... 5v6 -> FAIL. Marzena zniszczyła to zaklęcie i sama przejęła nad Klepiczkiem kontrolę. 

Ma czas - całą noc - na zabawę z księdzem... przygotowanie czegoś, co krzywdzi Henryka.

Dzień 2:

Karina WIE gdzie ksiądz śpi... idzie i puka do jego drzwi. O szóstej.

* Oj... przyszłam nie w porę? - zdziwiona Karina
* Zepsuła mi zabawkę. Zabawka jest mi teraz potrzebna... - Henryk, z grobową miną.
* Marzena, femisatanistka. Ta, która została wygoniona z miasta i jest czarodziejką i teraz zepsuła mojego księdza który miał robić dla mnie rzeczy... - Henryk, cały wściekły
* To się jej pozbądź - Karina, wzruszając ramionami

Henryk w skrócie wyjaśnił Karinie, że Klepiczka chce odebrać Marzenie. Bo musi być jego. Zaproponował Karinie, czy Karina nie pojechałaby z nim. Karina się zgodziła. Więcej, zapewni, że Marzena zejdzie Henrykowi z drogi i nie będzie mu więcej bruździć. Henryk się ucieszył. Zwłaszcza, że Karina chce zaprosić Henryka na śniadanie. A potem na kawę, a potem pojadą sobie dalej. 

Po śniadaniu, pojechali do Lenartomina. Osobno. Spotkać się z księdzem Klepiczkiem.

Kościół w Lenartominie. Tam w środku jest Klepiczek. Zbliżając się do kościoła, Karina wyczuła aurę magiczną. W środku były rzucane zaklęcia. Starsza rzędu godzin. Brudne zaklęcia i jeszcze rezonowały o strukturę wewnętrzną kościoła... Karina się nieco zmieszała; jako ekspert od syberyjskiego przetrwania zdecydowała się na zabezpieczenie przed ewentualnymi magicznymi pułapkami. Ale... w sumie, nie chce tam iść. Poczekała na Henryka. Niech to ON ma problem.

Henryk w kropce. Próbuje dojść do tego jakie plany może mieć Marzena... 5v3 -> sukces. Marzena na PEWNO będzie chciała zemścić się na Henryku. I na PEWNO nie będzie jej w pobliżu w chwili obecnej. Jak ją ściągnąć? Należy zagrozić jakiejś kobiecie. Konkretnie. A zwłaszcza z zemsty Henryka... jest szansa, że wróci.

* Czekaj, czy ja cię dobrze rozumiem? Boisz się wejść jako ksiądz... do kościoła? - zaskoczony Hubert
* Mam na pieńku z jednym księdzem. Opowiem, jeśli się sytuacja dalej rozwinie. - zirytowany Henryk
* A powiedz mi jak się nazywa? Bym nie poszedł do niego do konfesjonału... - Hubert, szyderczo
* Nie znasz go, to prowincjonalny... - Henryk.

W południe przyjechał drab do Henryka (-1 surowiec). 

* To co robić, szefie? Pobić księdza? - drab, dłubiąc w nosie

Henryk rzucił nań zaklęcia, dwa: pierwszym jest dominacja (Henryk ma kontrolę nad tym co widzi, dostał zadanie - wejdź, rozejrzyj się, wróć i powiedz co się dzieje). Drugie zaklęcie - opóźnione zaklęcie. Takie, które zawiera zaklęcie dominacji. Na widok księdza, ma rzucić weń łańcuszkiem by odpalić ewentualne zaklęcie.

Drab spróbował to zrobić; herpynea dostała zaklęciem mentalnym, które nie działa na demony. Herpynea spojrzała na draba i powiedziała "to nie po bożemu", wracając do modlitwy. Drab skonfundowany wyszedł. Teraz Henryk też jest zaskoczony...

Karina wezwała pachołów Mistrza do pomocy. Chce znaleźć i złapać Marzenę... obiecała też Henrykowi, że spróbuje go osłonić. Namawia go, by wszedł do kościoła ;-).

* Może zburzymy ten kościół? - nieco zdesperowany Henryk
* Dobra! Mój drabon wchodzi do środka, wyciąga Klepiczka siłą, Ty mnie uziemiasz i dominuję tu Klepiczka siłą. Przynajmniej JA nie będę w kościele... chyba, że Klepiczek jest zaczarowany superciężkim czarem i ja nie mam jak go złamać... albo zamiast dominować, mogę najpierw mentalnie wyciągnąć, co on pamięta... - Henryk i jego masterplan

Drab wszedł do kościoła po raz drugi wyciągać Klepiczka (herpyneę). "Klepiczek" dał się wyprowadzić. Widząc Henryka i wyczuwając jego "smak", herpynea zaatakowała. (Konflikt: 4v6 F, F, Remis). Wyrwała się drabowi, dopadła do Henryka i go szlachtnęła ostrymi szponami. Nie udało jej się wejść w połączenie mentalne (R) dzięki tarczom Kariny; niestety, grupka ludzi podeszła i to wszystko widzi...

Karina użyła zaklęcia wspomagającego by wzmocnić Henryka. Henryk użył mocy egzorcyzmu (F): Henryk bezceremonialnie zaczerpnął energię od ludzi znajdujących się przy kościele; doprowadził ich do choroby, ale overpowerował demona. Herpynea została Banishowana.

Karina wezwała swoich trzech pachołów na swoją pozycję.

Oni kazali Karinie i Henrykowi stanąć koło siebie. Po czym dostali puszką z gazem i Henryk stracił przytomność. Karina - zgodnie z zadaniem - dostała krew dla Kinglorda...

Dzień 3:

Plebania. Karina i Henryk są nadzy w wannie. Pobici. Związani. Stoi nad nimi dwóch zaskoczonych policjantów i pytają się nawzajem, czy to ten pedofil...

* Albo Klepiczek się mści, skurwysyn... jeśli ten człowiek się dowiedział co nawyrabiałem... - załamany Raynor, 2017
* Z drugiej strony Klepiczek cały czas atakował kościół... - nadal załamany Raynor

Zabrali ich na komendę. Tam Henryk użył swojego surowca (zaufanie społeczne) i korzystając z okazji 9v8 mimo wszystko udało mu się wydostać. (F) -> trzeba zająć się tymi staruszkami jeszcze dziś. (F) -> policjanci zainteresują się Hubertem co go będzie wkurzać. (F) -> rana mu dokucza po tym wszystkim. Niestety. Malus fizyczny -2...

Więc po południu Henryk wyszedł z komendy policji... z Kariną. Karina zostawiła w papierach ślad swojej bytności z imieniem i nazwiskiem...

* KLEPICZEK! - Henryk Siwiecki, wściekły.

W Lenartominie, kościół. Echo brudnej energii magicznej zaniknęło. Klepiczek w środku. Jest pusto w kościele, Klepiczek się modli. Aha, jest o kulach.

Przed wejściem do kościoła, Henryk przygotował zaklęcie i wszedł do środka. Walnął weń zaklęciem wspomaganym przez Karinę. Przejął nad Klepiczkiem kontrolę od ręki; zdominował go na dłuższy okres (-1 surowiec).

* Patrzcie, patrzcie... one, te głupie baby widziały niby jak ja o kulach rozwalam innego księdza jak w filmie? - Klepiczek, pod kontrolą Henryka

Klepiczek wyjaśnił Henrykowi co się stało w tą pamiętną noc:

* Pamiętam jak światło i ciemność walczyły o moją duszę... i światło wygrało a potem był... sen - Klepiczek, entuzjastycznie
* To był głupi sen. Nie mógł być prawdziwy - Henryk, zrezygnowany
* Czyżbyś zadenuncjował kolegę księdza jako pedofila? - Henryk, kwaśno
* Tak, tak było! - Klepiczek, ze zdziwieniem
* A tak tak. Znam takie sny, są popularne. Nie przejmuj się. - Henryk, JESZCZE kwaśniej
* Jeszcze. Byłem jak w lustrze... i zwichnąłem sobie w lustrze. Bolało. Lekarze nie pozwalają mi się ruszać, mam historię medyczną. Jeszcze... płakałem do słuchawki. Ty (Henryk Siwiecki) kazałeś mi to zrobić. Powiedziałeś, że mnie skrzywdzisz. Kazałeś mi zamówić sobie jakieś dzieci... - Klepiczek, niekoniecznie rozumiejąc co się działo
* Plan spalony. - Henryk, zimno.

Innymi słowy, Marzena się postarała...

* Nicaretta Nicarettą, ale mam tu GORSZEGO demona do upolowania... - Raynor, 2017, zimno.

# Progresja

* Andrzej Klepiczek: dostał reputację "specyficznego" (bardzo żarliwy jak chodzi o lokalnych błogosławionych i taki, który potrzebuje ciała niewieściego)
* Kinglord: dostał krew Henryka Siwieckiego dzięki działaniom Kariny
* Nicaretta: wie, że Henryk o niej wie i że "nadchodzi" i się może fortyfikować
* Hubert Kaldwor: na którego plecach są policjanci dzięki Henrykowi
* Henryk Siwiecki: na jego temat pojawiły się pewne plotki związane z dziećmi...

# Streszczenie

Henryk wpadł na genialny pomysł zaskoczenia Nicaretty. Do tego potrzebował pielgrzymki - załatwił to przez zdominowanego Klepiczka. Jednak w nocy pojawiła się Marzena (podła sucz), która złamała czar Henryka i stwierdziła, że się zemści za wyżywanie się na femisatanistkach. Nie tylko zastawiła pułapkę (herpyneę) ale i przez Klepiczka ostrzegła Nicarettę i nadała temat, że Henryk jest pedofilem. Gdy Henryk i Karina poszli do Klepiczka, tam zostali zaatakowani; herpynea zraniła Henryka zanim ten ją egzorcyzmował. Karina wezwała wsparcie, załatwili Henryka by Kinglord dostał krew Henryka (a nikt Kariny nie podejrzewa). Henryk odzyskał Klepiczka, policja poluje na Huberta i Henryk jest NAPRAWDĘ zły na Marzenę...

# Zasługi

* mag: Henryk Siwiecki, którego doskonały plan posypał się przez JEDNO zaklęcie cholernej Marzeny. Skończył jako "ksiądz pedofil", pobity i nagi. Absolutna, sromotna porażka na każdym polu... Ale odzyskał Klepiczka na osłodę.
* mag: Karina Łoszad, zdradziła Henryka, gdy ten był najsłabszy i zdobyła jego krew. Poza tym, lojalnie pomagała. Zostawiła ślad (imię i nazwisko) policji w rekordach.
* czł: Witold Małek, ksiądz w Kulturnie. Pod kontrolą Nicaretty, kontaktował się z nim Klepiczek w kilku kwestiach. Dość sympatyczny i niegroźny.
* czł: Andrzej Klepiczek, który jak zabawka jest dominowany raz przez Henryka, raz przez Marzenę. Dostaje opinię "dziwnego świra" i donosi na Henryka do Witolda (pośrednio, Nicaretty). Też: coś z dziećmi i Henrykiem.
* mag: Marzena Dorszaj, która miała wielki dzień. Uratowała dziewczynę z kościoła, po czym zmasakrowała plan Henryka i zszargała mu opinię (min. odnośnie zamiłowania do dzieci) i zraniła herpyneą Henryka.
* czł: Hubert Kaldwor, który uśmiał się kosztem Henryka. A potem dostarczył mu draba. A POTEM napuścili się na niego policjanci...

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Wolny
                1. Kulturno, małe miasteczko. Wokół ryneczku: sklep spożywczy, kebab, kamieniczki turkusowe, wieża ratuszowa. Tuż obok rynku stalinowski architekt walnął szare blokowisko. Niedaleko są też kościół i... opera. Aha, jest tam Nicaretta.
                1. Lenartomin, w którym anty-kobiece nastroje rosną...
                    1. Kościół, zabrudzony magicznie przez Marzenę i gdzie Marzena zastawiła pułapkę (herpyneę) na Henryka.
            1. Powiat Tonkij
                1. Wyjorze
                    1. Centrum
                        1. Hotel Jantar, gdzie sypia Henryk Siwiecki. Na swój koszt, naturalnie.
           
# Czas

* Opóźnienie: 1
* Dni: 3