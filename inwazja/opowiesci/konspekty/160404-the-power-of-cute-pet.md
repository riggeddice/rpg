---
layout: inwazja-konspekt
title:  "The power of cute pet"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160927 - Desperacka bateria dla Aegis (temp)](160927-desperacka-bateria-dla-aegis.html)

### Chronologiczna

* [160403 - Wiktor kontra KADEM i Świeca (SD)](160403-wiktor-kontra-kadem-i-swieca.html)

### Inne

Misja kończy się w okolicach misji:
* [160309 - Irytka Sprzężona (HB, KB, LB)](160309-irytka-sprzezona.html)

## Kontekst misji

- Wiktor próbuje podporządkować sobie Silurię. Siluria próbuje podporządkować sobie Wiktora. Dalej.
- Gerwazy usunięty z drogi Silurii; na to miejsce weszła Sabina Sowińska. Wioletta stoi przy Silurii, nie Wiktorze.
- Arazille się ujawniła, pożarła Kariatydę, ale Blakenbauerowie ją odparli. Równolegle zaczyna się plaga Irytki Sprzężonej.
- Weszło Millennium na teren Kopalina za prośbą Wiktora; działają w kompletnym ukryciu.
- Millennium wycofuje magów KADEMu przed siłami Wiktora za prośbą Silurii.
- Wiktor próbuje zdobyć kontrolę nad Szlachtą przy użyciu Diany w jakiś dziwny sposób...

## Cel misji:

Cel misji: 
- Rozpoczęcie ostatniej fazy kampanii.
- Sprawdzenie, czy Wiktor będzie rządził Szlachtą czy przegra wojnę o Szlachtę.

## Punkt zerowy:

- Ozydiuszowi nie podoba się to, co dzieje się z Wiolettą... ale ma to gdzieś. Próbuje odzyskać pancerz (potencjalnie Spustoszony) stworzony przez Tamarę od Wiktora.
- Wiktor zmienił zdanie co do Silurii. Ma zamiar wykorzystać czarodziejkę KADEMu jako partnerkę. Jest kobietą, więc niewolnicę - ale godną partnerkę. Może się podzielić.
- Wiktor ma dość być "tym trzecim lub czwartym" w Szlachcie. On chce rządzić Szlachtą. Ma dość oporu ze strony wszystkich, łącznie z własnym rodem.
- W skrócie, Wiktor chce iść w kierunku na Overlord Supreme i przejąć Kopalin. A jeśli ma do tego oddać jego część Millennium, niech tak będzie.
- Mirabelka Diakon (Millennium) jest opłacana przez Wiktora i ma za zadanie pomóc Wiktorowi zdobyć kontrolę nad magami poza Kopalinem.
- Amanda Diakon (Millennium) powoli wchodzi na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Marian Łajdak i Andżelika Leszczyńska są ukrywani i przenoszeni przez Millennium by ich sensownie wycofać z Kopalina zanim złapie ich Wiktor.

## Misja właściwa:

Dzień 1:

Dla Silurii pewnym, niezaprzeczalnym problemem jest wciąż Dagmara. Gerwazy wypadł z orbity; został wysłany na "misję specjalną". Ale Dagmara jest ostatnim problemem. Jednocześnie, jest najmniejszym problemem. Choć póki nie pojawi się Sabina, nie wiadomo jak źle będzie.

Siluria cały czas kontynuuje z Wiolettą treningi. Czasem jest grzeczna, czasem jest niegrzeczna... Wioletta nigdy nie może przewidzieć jak Siluria się będzie zachowywać. A Wioletta jest tymczasowo przeciążona i TYLKO pilnuje Silurii; nic innego robić nie może. Poza wieczorami, gdzie Silurię pilnuje Wiktor.
Wioletta dostała od Silurii zadanie domowe - ma pomyśleć, co jej się PODOBA. Gdy terminuska się zdziwiła, Siluria powiedziała, że to jak dobór broni - używasz tego, co pasuje. Wioletta zrozumiała. Plus, Siluria ma plan - niech Wioletta zacznie wyrażać swoje potrzeby.

Siluria umówiła się na spotkanie z Romeo Diakonem. Wioletta strażniczka, pilnuje apartamentu Netherii, więc wszyscy mogą ją sobie dobrze obejrzeć. Siluria powiedziała na czym problem - Wioletta ("cute pet", słowami Romea) zaczęła swoją przygodę od dominacji i nikt jej nie pomógł. Ona by chciała. Więc trzeba by ją poadorować i naprawić. Romeo. Mag spojrzał na Silurię z rozpaczą - nie radzi sobie z "petami". O to chodzi. Ale jeśli KTOKOLWIEK ją naprawi, to chyba Romeo da radę? Mag westchnął. Jak nie ma innego wyjścia, Silurii odmówić nie może. Romeo powiedział, że "ją obudzi". Obiecał być delikatnym.

Jak ich spiknąć? Netheria powiedziała, że jutro ma imieniny i zaprasza Silurię, która nie może odmówić. To będzie w tym apartamencie; będzie kilku Diakonów i przyjaciół. Siluria się ucieszyła. Wioletta musi przyjść, bo musi pilnować Silurię. I musi się odpowiednio przebrać, by wyglądać stosownie. Romeo też zaczyna mieć plan działania...

Aha, Siluria jeszcze powiedziała, że gra w silnym power play. Jeśli będzie prosić o pomoc - niech jej nie pomagają. ACKnęli.

Wioletta jest przerażona; spodziewa się po imprezach Diakonów wszystkiego. Siluria chichocze; Netheria nie jest taka (to znaczy jest, ale ta impreza ma konkretny cel). Jeszcze Siluria przeleciała przez kilka sklepów i znalazła dla Netherii kolczyki pasujące do jej imienia. Następnie w pokoju nad nimi popracuje magicznie.

Wrócili do Kompleksu Świecy. A tam - niespodzianka. Dagmara pilnuje pokoju niedaleko pokoju Silurii. Ewidentnie ktoś tam jest. Przy krótkiej rozmowie, gdzie Wioletta chciała dowiedzieć się kto jest gościem, Dagmara przedstawiła gościa jako Eleę Maus, lekarkę magiczną. Dagmara nie jest pozytywnie nastawiona do tego, by w pokojach gościnnych koło siebie mieszkała Diakonka i Mausówna. Wioletta chcąc ją rozładować powiedziała, że idzie następnego dnia na imieniny. Taaak, bardzo rozładowała Dagmarę. Z pokoju wysunęła głowę Elea i spytała radośnie o imieniny. Chcąc rozładować potencjalny problem z Diakonką ofiarowała jej holokryształ pozwalający się wcielić w nudne życie mrówczej robotnicy - jako żart. Siluria doceniła (i gest i żart). Nadała na właściwych falach. Uspokojona Elea wróciła do pakowania się.

Wioletta powiedziała Silurii, że Wiktor chciał się z nimi spotkać. Jest zaskoczona. Oczywiście, czarodziejki poszły do niego. 
Wiktor jest wyraźnie zdenerwowany. Niedługo będzie tu Sabina - jego "święta, moralna kuzynka". Wiktor wyraźnie Sabiny nienawidzi. I nie może jej nic zrobić, bo ma wspomagane łącze hipernetowe, zupełnie takie jak mag wydziału wewnętrznego; jakiekolwiek ingerencje w maga mającego wspomagane łącze wywoła alarm. Siluria po raz pierwszy lekko przejęła kontrolę - podeszła do Wiktora, pchnęła go na fotel i zaczęła masować barki. Mag zamrugał zdziwiony. Spojrzał groźnie na Wiolettę, która uklękła, ale kazał jej wstać. Zagubił się trochę. Powiedział, że by zepsuć humor Sabinie ma zamiar zrobić przedstawienie. Zamówił już od Millennium (i otrzymał) uprzęże erotyczne typu 'denial-8'. Siluria wie co to jest. To uprzęże podkreślające urodę posiadaczek, które są sterowane przez kontrolera, wydzielające odpowiednie środki i utrzymujące noszącego (noszącą) w stanie zdefiniowanym przez władcę. I Wiktor wymyślił sobie - on rozmawia z Eleą, poniżej niego są Siluria i Wioletta w uprzężach i na to ma wejść Sabina. Jeśli Sabina chce go chronić - też dostanie uprząż. I ma ją nosić.

Właśnie, Elea? Wiktor powiedział, że Oktawian Maus mu ją polecił a potrzebny mu Maus do pomocy Dianie. Wioletta nic nie rozumie, ale woli nie pytać. Czyli Elea jest zaufaną czarodziejką nie-Szlachty dla Wiktora. Ciekawe. Potencjalnie zagrożenie?

Siluria zaprosiła Eleę na herbatkę; ta zaakceptowała z radością. Dziewczyny nie są pilnowane; nie trzeba. Elea wyraziła zdziwienie, że Mausówna i Diakonka spotykają się w samym sercu Szlachty i piją herbatkę. Zabawnie ten świat się zmienił - nie na gorsze. Elea jest rozluźniona; nie ma w niej niczego anty-Siluriowego. Siluria przekazała Elei obraz Silurii, która boi się bo KADEM w lockdownie a Wiktor jest potężny a dodatkowo Siluria lubi dobrą zabawę a Wiktor bawi się z nią tak jak ona lubi. Siluria nie jest złamana, ona tak chce. Dodatkowo - bez większych komplikacji - wyciągnęła od Elei to, czemu ONA tu jest. I czy ona wie, co Wiktor chce zrobić z Silurią.

Nie, Elea nie wie, co Wiktor chce zrobić z Silurią, ale spodziewa się WSZYSTKIEGO po tym co powiedział jej Oktawian. Elei nic nie zdziwi - jest "dead man's hand" Oktawiana a przy okazji jedną z lepszych lekarzy i corruptorek. Wiktor potrzebuje z jednej strony kogoś, kto się świetnie zna na łamaniu umysłów i nietypowych umysłach a z drugiej strony kogoś, kto potrafi kontrolować proxy. Siluria też wyczuła, że Elea ma strasznie duży pociąg do dominacji, ale strasznie, STRASZNIE z tym walczy i się kontroluje. Siluria się uśmiechnęła w sercu. JESZCZE walczy. Wyczuła też w Elei mrok wskazujący na praktykę defilera. 

Wieczór. Siluria u Wiktora. Poprosiła Wiktora o pożyczenie uprzęży na imprezę - taki zwyczaj. Wiktor, bawiący się z Silurią, zgodził się. Od tej pory Wioletta będzie nosiła uprząż częściej. On chce, by ona była bardziej w jego stylu - a Siluria będzie dla niego narzędziem do właściwego dostosowania Wioletty. Silurii bardzo to pasuje. Ona też chce, by Wioletta była bardziej wyzwolona - dla jej dobra. 

Dzień 2:

Nieszczęśliwa Wioletta ubrana w uprząż i Siluria ubrana w uprząż (oczywiście, na to - odpowiednie ubranie) idą na imprezę do Netherii z rana. Wioletta się wstydzi i trochę boi. Siluria ją troszeczkę uspokoiła.
Czarodziejki są odrobinę wcześniej. Netheria już czeka. Siluria oddała kontrolę nad ich uprzężami Netherii i zasygnalizowała jej, by ta nie ruszała Wioletty. Netheria ACKnęła. Czas, by impreza się zaczęła.

Siluria obserwowała z oddali, na imprezie, jak Romeo zaczął flirtować z Wiolettą. Delikatnie, nie zwracając na nią uwagi, ściągnął w ogóle tu 2-3 osoby specjalnie by go podziwiały, a potem skupił uwagę na Wioletcie. Do końca imprezy Wioletta będzie zauroczona Romeo. Siluria się uśmiechnęła. O to jej chodziło. A uprząż daje jej pretekst, dlaczego jest tak zaczerwieniona, mimo, że nie działa w ogóle.

Impreza była przesiąknięta erotyzmem, ale nie seksem. Siluria czuła się po prostu rewelacyjnie. Netheria zakomunikowała obecność uprzęży, Siluria wiedziała, że odzyskała nad nią kontrolę, więc jedynie używała jej do podnoszenia wrażeń. Dla Wioletty to wszystko było nowe, to przytulanie, głaskanie (nie erotyczne), ta bliskość, ciepłe poduszeczki, wygoda... gdyby na imprezie zaczęli się rozbierać, poszłaby za nimi, ale specjalnie nikt tego nie robił. Wioletta. Taka rozczarowana. Ale i zadowolona. A Romeo, ach, Romeo...

Impreza się skończyła. Wioletta odprowadziła Silurię do pokojów. Przyznała się, że tak się jeszcze nigdy nie czuła, że nie zna bliskości. Nie takiej. 

Wiktor poprosił do siebie Silurię bez Wioletty. Powiedział jej, że wszystko jest zgodnie z planem - wszystko jest na swoim miejscu. Z buduaru wyszła Elea i powiedziała, że wszystko już gotowe. Wyraźnie nie uprawiała seksu; co tam robiła? Wiktor powiedział, że pacjentka jest gotowa a Siluria zobaczy w nocy.

Wczesny wieczór. Pięknie wystrojona Wioletta i Siluria przyszły do niego. Wiktor zażyczył sobie, by powoli zdjęły ubrania i włączył sygnał w uprzężach, okazując swoją władzę. Chce, by Siluria myślała ale Wioletta ma czuć. Tylko. Bardzo bawi go władza nad terminuską. Troszkę przesadził, więc Siluria dała mu znać. Obniżył sygnał i poszedł do sali audiencyjnej z dwoma dobrze bawiącymi się czarodziejkami. Zaprosił Eleę i Dagmarę na spotkanie. Dagmara zobaczyła Silurię i rozkoszującą się uprzężą Wiolettą. Siluria pracuje nad Dagmarą; chce, by ta uważała, że to wszystko, biedna Wioletta, to wszystko jest wina Silurii. Dagmara uwierzyła, że to wina Silurii. 

Gdy przyszła Elea, była pod wrażeniem tego, co widziała. Siluria się SPECJALNIE postarała, by to mocno zadziałało na Eleę; udało jej się. Siluria specjalnie doprowadziła do tego, że dla Elei "stała się celem". Obiektem seksualnym. Wiktor nic nie wie.
Hungry defiler is hungry.

Weszła Sabina do sali audiencyjnej, spotkać się z Wiktorem. Na wejściu - she is stunned.
Siluria zauważyła, że tym razem Dagmara jest w power suicie. Patrzy na Sabinę jak na potencjalnego adwersarza, jak na potencjalnego napastnika. 

Siluria -> Dagmara: atak "she is cute like that, why do I hate her, anyway?" | 18v18

Dagmara zobaczyła szczęśliwą Wiolettę. Zobaczyła wijącą się w ekstazie Silurię, która spełnia życzenia Wiktora. I... mimo wspomagania neuropancerza, mimo swojej nienawiści do Silurii... widzi, że Wioletta swego czasu płakała przez Wiktora, a teraz jest szczęśliwa. Widzi, że Siluria jest... we władzy Wiktora, ale jest... 'cute'. I Dagmara mogłaby tak na nią patrzeć. Siluria - taka - jej się podoba...
Elea zauważyła, co Siluria zrobiła. Bardzo jej się to spodobało. Power. Power is everything for a defiler.

Siluria -> Sabina: atak "omg biedna dziewczyna! Potraktowana w taki sposób!" | 18v12

Sabina zbladła. Biedna, nieszczęśliwa czarodziejka. Obie. I wszystko tylko dlatego żeby ją zdenerwować?

Sabina zrobiła kilka kroków w stronę Wiktora. Dagmara się otrząsnęła i stanęła jej na drodze. Powiedziała, że pan Sabiny - Wiktor - oczekuje na jej przywitanie. Sabina prychnęła, że ten spektakl był niepotrzebny. Odwróciła się i wyszła, żegnana śmiechem Wiktora, że teraz nikt nie pomoże z osób wyżej. Że ona nie ma tu żadnej władzy. A jego dziewczyny same chcą. Więc nawet na to nie może nic poradzić...

Krótkie podsumowanie:
- Dagmara już nie nienawidzi Silurii (widzi dobry wpływ na Wiolettę) i chce widzieć Silurię jako 'cute pet' częściej. Mimo, że ją to boli.
- Elea pragnie kontroli nad Silurią, chce tą kontrolę skonsumować fizycznie. Widzi co Siluria robi (w kwestii Dagmary), ale jej to nie przeszkadza.
- Sabina uważa Silurię za niewinną ofiarę Wiktora.
- Wiktor uważa Silurię za wiernego 'cute peta' i cenną sojuszniczkę - jego ulubioną Diakonkę. Tak się zawsze zaczyna.
- Wioletta czuje się dobrze w towarzystwie Silurii. Ufa jej.

Trzy godziny później. Siluria już w swoim pokoju, bez uprzęży. Doszła do siebie (Diakonki dochodzą błyskawicznie). Odwiedziła ją Sabina.

Siluria pokazała Sabinie inny obraz siebie. Obraz takiej Silurii, która nie jest złamana, ale jest zauroczona mocą Wiktora i jest nawet skłonna opuścić dla niego KADEM. Silurii, której zrobiono coś bardzo złego. I jak KADEM się dowie... 
Sabina jest przerażona. Próbowała wpłynąć na Silurię, ale KADEMka zmanipulowała ją, że jest do uratowania, ale nie tak. Nie jednorazową rozmową.

Wieczór. Tym razem nie Wioletta odprowadza Silurię. Tym razem - Dagmara i Sabina.
Sabina: "Nie musisz tam iść." 
Dagmara: "Lord kazał - musi."
Siluria: "Oczywiście, że muszę." -> Sabina widzi, że Siluria zrobi dla Wiktora bardzo wiele.

W buduarze Siluria przekręciła klucz. Wiktor się do niej uśmiechnął. Tym razem Siluria przejęła władzę by go zadowolić.
Gdy tylko Wiktor został zaspokojony (i Siluria też), Wiktor zaprowadził ją do buduaru. Tam już czekała Elea i... spętana Diana Weiner.

Wiktor wyjaśnił, że Gerwazy zdobył i porwał dla niego Dianę i na jej miejsce zostało podłożone Proxy. Próbował Dianę złamać, ale się nie udało. Ani jemu, ani jemu z Eleą. Bez Silurii się im po prostu nie uda. Wiktor wyjaśnił Silurii o co tu chodzi - główna baza Świecy nie znajduje się w Warszawie jak wszyscy myślą, ale jest pomiędzy miastami, jest na Fazie Daemonica. Jest to Świeca Daemonica. I tam znajduje się lord Szlachty - Aleksander Sowiński. On nie powienien wiedzieć kto dowodzi Szlachtą; nikt tego nie wie. Ale się dowiedział. I on chce, by Diana ściągneła go na tą stronę, na tą fazę. Wtedy będzie w stanie go przejąć. I nawet jeśli nie będzie w stanie utrzymać całej Szlachty, przynajmniej utrzyma część Kopalińską. I jest skłonny podzielić się z Diakonami, Millennium, KADEMem czy cokolwiek jest potrzebne.

Siluria jest pod wrażeniem. Nie są to małe plany.

Siluria ma następujący plan:
- Diana: uwieść ją Wiktorem. Sprawić, by naprawdę się w nim zabujała, by on stał się jej światem... a wszystko to kryształami Elei i historiami tworzonymi przez Silurię i Wiktora wspólnie w mózgu Diany (głównym twórcą jest oczywiście Siluria).
- Wiktor: zmiękkczyć. Pokazać mu, że bycie poddanym też jest fajne. Siluria mu w tym pomoże. Pokaże mu przyjemność z uległości. Przyzwyczajać go do tego, że Siluria mówi "stój". Przyzwyczajać, że to ONA jest dominująca. Chwilowo tylko w kontekście kryształu, ale ona chce, by on przekroczył tą linię.
- Elea: pokazać jej, gdzie jest prawdziwa moc. Jeszcze nie pokazywać jej władzy nad Wiktorem, ale pokazać prawdziwą moc wobec Diany i to, KTO jest tu tak naprawdę panem. "Maus idzie za Karradraelem", Siluria chce Elei pokazać, że to ona jest "tutejszym Karradraelem".

Całą noc przygotowywane były i kalibrowane były kryształy, które będą Dianie puszczane raz za razem, by ją przekształcić. Jeden kryształ (godzina) przekłada się na jeden miesiąc. Elea jest naprawdę dobra.

- Diana: po jakimś tygodniu padnie. She will crack.
- Wiktor: zobaczył to, co Siluria chciała mu pokazać.
- Elea: będzie cicho.

...

# Zasługi

* mag: Siluria Diakon, cute pet, która pozbyła się wszystkich swoich wrogów i praktycznie jest o krok od kontroli Szlachty.
* mag: Netheria Diakon, która zapewnia możliwość bezpiecznego spotkania Silurii z resztą świata. The spymistress.
* mag: Romeo Diakon, delikatnie adorujący Wiolettę na prośbę Silurii. Ma ją "naprawić" po tym, co zrobił Wiktor.
* mag: Wioletta Bankierz, która czerpie przyjemność z erotyki i seksu a nie tylko cierpi. 
* mag: Dagmara Wyjątek, która zaczyna mieć wątpliwości co do Silurii... she is... cute. I pomaga Wioletcie.
* mag: Elea Maus, amoralna lekarka i reżyser holokryształów, która zobaczyła prawdziwą potęgę w Silurii i jak Maus do Karradraela lgnie do niej.
* mag: Gerwazy Myszeczka, który porwał Dianę Weiner dla Wiktora i na to miejsce wprowadził Proxy Mausów.
* mag: Wiktor Sowiński, który powiedział Silurii swój plan i który po raz pierwszy ma wyłom pozwalający Silurii na zaatakowanie.
* mag: Sabina Sowińska, szlachetna i bohaterska terminuska, dla której przerażający jest stan degeneracji przez Wiktora Silurii.
* mag: Diana Weiner, porwana i w łańcuchach, którą corruptuje Wiktor, Siluria oraz Elea. Cel: zdobycie władzy nad Szlachtą.
* mag: Aleksander Sowiński, prawdziwy lord Szlachty, który nie wie, że Wiktor zamierza się na jego miejsce.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Pokoje gościnne
                                    1. Buduar Wiktora
                    1. Prywatne osiedle Mirra
                        1. Apartament Netherii Diakon