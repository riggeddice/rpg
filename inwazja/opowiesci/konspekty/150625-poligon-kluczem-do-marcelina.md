---
layout: inwazja-konspekt
title:  "Poligon kluczem do Marcelina?"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Szlachta vs Kurtyna
- Wojny wielkich gildii Śląska
- Wszystkie dziewczyny Marcelina

## Kontynuacja

### Kampanijna

* [160202 - Wolność pająka fazowego (KB, LB)](160202-wolnosc-pajaka-fazowego.html)

### Chronologiczna

* [150607 - Brat przeciw bratu (HB, SD, PS)](150607-brat-przeciw-bratu.html)

## Punkt zerowy:

Ż: O co KADEM może oskarżyć Marcela Bankierza?
B: Przy jego obecności zniknęły wrażliwe materiały - jak dostać się na poligon doświadczalny.
Ż: Czemu to, że Marcel wypuścił tą informację na światło dzienne tak zirytowało Infernię?
D: W publicznej opinii Infernia stała się "jak każdy Zajcew" a walczyła by opinia była "że Diakon".
Ż: Jaką korzyść z tej misji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Co specyficznego było z tymi KADEMowymi żabolodami (o czym absolutnie nikt, łącznie z Warmasterem, nie wiedział)?
K: Eksperymentalne żabolody, które potrafią wytrzymać wyższą temperaturę i chłodzą mocniej.
Ż: Co się stało że KADEM potrzebował pilnie Pawła w innym miejscu na priorytecie krytycznym?
D: VISOR, GS "Aegis", Eryk Płomień i Paweł robią obchód po poligonach szukać problemów i śledzików.

## Misja właściwa:

Problemy do rozpatrzenia dalej:
- kto stoi za atakiem na KADEM? Dlaczego?
- kto był na poligonie? Jaki był cel wejścia na ten poligon? ŻABOLODY SERIOUSLY?
- czemu Pakt w to wszedł? (Założenie: że to robota Emilii)
- kto skoordynował to wszystko?
- Marcel Bankierz i jego rola.
- jaka czarodziejka ma koncept munduru dla Inferni?

"Infernia chce dostać DWA autobusy śledzików syberyjskich od Marcelina. I przeprosiny. "Z agrafką lub bez". Do tego, Infernia chciałaby mundur. Na SŚ jest czarodziejka która nie lubi KADEMu i potrafi dopasować odpowiedni mundur do rodów Inferni. Zrobiłaby furorę na balu Emilii."

Mariusz poprosił do swojego gabinetu Silurię. Powiedział jej, że Tamara Muszkiet - terminuska - jest ekspertem w tworzeniu pancerzy syntetycznych z włókien organicznych (i ubrań). Okazuje się, że Zenon Weiner - mag, który był na KADEMie - współpracował z Tamarą nad tworzeniem różnych pancerzy dla jej gwardii i jej samej. Mariusz całkowicie nielegalnie zwinął część technologii i zdobył specyfikację znaną Zenonowi.
To co ciekawe, cross-referencował tą speckę z emanacjami na poligonie i wyszło mu, że najpewniej ktoś z gwardii Tamary ("Kurtyny") był tam. Tylko dzięki temu udało się tej osobie wydostać, te pancerze są niesamowite. Ona mnóstwa reinwestuje... więc KADEM jest zainteresowany.
Mariusz prosi więc Silurię, by ta dała rady przekonać Hektora - może on da radę oskarżyć Tamarę :>. O cokolwiek.

Żabolody które zniknęły nie były normalne. To było coś, co wykorzystywano w ramach pracy studentów, na zaliczenie; Norbert dał im do zajmowania się żabolodami w ramach eksperymentów; niestety, teraz część zniknęła i nie ma na jakiej bazie ich zaliczyć. Może Tamara stoi za tym, by Marcelin miał problemy? Ach, przecież Marcel Bankierz był przez Tamarę wysłany! Coś trzeba zrobić... a Mariusz chce wrobić Tamarę. Może udowodnić, że magitechniczny pancerz zrobiony przez Tamarę znajdował się podczas ataku na poligon, ale do tego Mariusz potrzebuje jakikolwiek magitech zrobiony przez Tamarę... 

Siluria zatarła palce. Część Kurtyny ma dostęp do magitechów, więc może uwieść i przekabacić ^_^.

Tymczasem Hektor zdecydował się umówić z Infernią na KADEMie. Zanim do tego doszło, załatwił ze swoim demonem (Demon_481) zbiór zasad i komplikacji, dzięki którym proces będzie opóźniony; Hektorowi nie zależy na tym, by jak najszybciej do procesu doszło; chce przekierować proces na coś innego. Dokładniej: chce odkryć prawdę, a nie skazać Marcelina ;p.

Spotkanie z Infernią odbyło się na KADEMie. Infernia próbowała delikatnie zasugerować Hektorowi że dowody da się zgubić. Hektor jednak szedł w "prawo i uczciwość" aż Infernię znudził. Gdy Hektor spróbował Inferni pokazać, że obowiązek i pozycja prokuratora implikuje, że on nie powinien takich rzeczy słuchać Infernia spytała go groźnie, czy on JEJ grozi. Powiedziała Hektorowi, że jego pozycja wynika tylko i wyłącznie z polityki Świecy i niech on zna swoje miejsce i gra rolę jaką otrzymał. Hektor powiedział, że ma pilnować swojego nosa. Proces się odbędzie POWOLI i DOKŁADNIE, ale DEFINITYWNIE. Infernia zaznaczyła, że opinia publiczna jest zainteresowana prędkością a Hektor został celebrytą dzięki publikacji w Pakcie. Hektor się "ucieszył".

Wychodząc z KADEMu, Hektor zderzył się z jakąś młodą dziewczyną. Ta zaczęła się jąkać i przepraszać; Hektor od razu założył, że ta mu coś podłożyła i wyjechał jej z paszczy jako inkwizytor; czemu mu coś podkłada i kim jest. Dziewczyna się popłakała i przerażona zaczyna uciekać. Hektor kazał pojawić się przed nią Demonowi_481, który pojawił się koło... Hektora. I krzyknął "stój w imieniu prawa, jesteś ARESZTOWANA". W tle stoi grupa KADEMowców i pokłada się ze śmiechu. Hektor zakasał rękawy i biegnie w garniaku łapać dziewczynę. Młódka zwiała a Hektor został całkowicie sam, zasapany, a w tle rżą z niego KADEMowcy...

"Zapamiętaj, chłopie. Mniej burgerów, więcej dni nóg" - Ignat do Hektora.

A co młódka podłożyła? Pluskwę. Pluskwę ze świata ludzi. Ani jednego zaklęcia, nic; Hektor trafił na przeciwnika godnego siebie (jeśli chodzi o sprint).

Siluria poszła na spotkanie z kimś z okolicy Bankierza, ze Świecy. Chce się dowiedzieć jak najwięcej odnośnie Marcela; co to za jeden, i co go wiąże z Tamarą.

Siluria ma zgryz - interesuje ją temat poligonu. Marta wzięła na tapetę wybuchający artefakt; jest to ważne, więc zrobiła to osobiście. Też Silurię martwi Marcel Bankierz, który jest częściowo niedostępny, bo na Świecy. Od ręki zdobyła informację - Marcel Bankierz NIE jest powiązany z "Kurtyną" Tamary Muszkiet, ale jest powiązany z samą Tamarą. Tamara dostała Marcela "w prezencie" od gildii, ale ona nie chce z nim pracować a on nie chce pracować z nią; nie zgadza się z "Kurtyną" i tym co Kurtyna reprezentuje.
Więc Tamara wysłała Marcela na KADEM, by się go pozbyć i by pełnił swoją rolę. I w świetle tej informacji, to co się stało nie ma sensu.

Siluria i Hektor się spotkali; znowu na KADEMie. Hektor zapytał Silurię o tą tajemniczą młódkę która na niego wpadła, ale Siluria jej nie rozpoznaje. Hektor zatem chce pokazać to zaklęciem mentalnym by pokazać ów obraz Silurii...
Siluria zobaczyła czarodziejkę i ją poznała. Alisa Wiraż, studentka KADEMu. Szczur. Praktykantka. Siluria przeprosiła na moment Hektora i poszła do Norberta.

Norbert jest zafrasowany. Znaleźli na Poligonie skażyciel - zmodyfikowany węzeł którego funkcją jest wypaczanie eksperymentów i sabotaż. Dobrze było cholerstwo schowane, ale nie DOŚĆ dobrze. Skażyciel nie jest szczególnie tani, więc podejrzani są KADEM, Świeca i Millennium.
Więc Norbert jest nieco nerwowy. Jego zdaniem, ktoś połakomił się na żabolody i wszystko wzięło w łeb... a nie wiadomo od kiedy ten skażyciel tam leży. Trzeba zmienić procedury i w ogóle...
Siluria poprosiła Norberta o Alisę; ta zrobiła coś głupiego i podłożyła pluskwę prokuratorowi. Norbert - facepalm. Jasne, nie ma sprawy; niech Siluria ją przesłucha.

Alisa przyszła do Silurii. Już na wejściu zaczęła się wymawiać i płonić że się nie nadaje. Siluria docisnęła. Alisa przyznała się, że słyszała o organizacji której oficerem jest Siluria. Erotyczno-militarny kompleks władzy, loża masońska, działająca między gildiami i rekrutująca tylko najlepszych i najpiękniejszych. Nic dziwnego, że Alisa, naiwne stworzenie stwierdziła, że się nie nadaje. To najlepszy kawał Ignata ever (zdaniem Ignata), że Alisa w to uwierzyła.

Siluria dostała od Ignata Vine'a. Uciekająca niezidentyfikowana dziewczyna w masce kurczaka i prokurator z kolą w jednym ręku i burgerem w drugiej próbującym dogonić uciekającą. Prokurator ma doklejoną odznakę policyjną zrobioną w paincie. Dopisek "McChickenz - gonna catch them all". Ignat, tak bardzo subtelny.

Siluria też wypytała Alisę skąd przyszedł jej pomysł z pluskwą; ta się przyznała, że nie nadąża. I nie jest dość śmiała by sama zapytać więc jak ładna czarodziejka obiecała jej pomóc. Po skanie mentalnym Siluria rozpoznała tamtą czarodziejkę. Jest to Vladlena Zajcew. Bardzo oddana Emilii sekretarka Emilii. Siluria miała kolejny facepalm - Alisa praktykuje w Instytucie Zniszczenia, pod Mariuszem Trzosikiem. Naiwna i nieśmiała ALISA. W Instytucie Zniszczenia... Alisa wszystko zapisuje, bo gubi i zapomina.

Tymczasem Hektor zabija nudę siedząc w ogólnym pokoju na KADEMie i przygotowując się do wywiadu z prasą. Przysiadła się do niego starsza czarodziejka; powiedziała, że zna odpowiedzi na wszystkie pytania Hektora i z radością mu udzieli. Hektor od razu facepalmował - wróżka. Zadał pytanie z marszu "kto zaatakował poligon". Czarodziejka się zaśmiała i powiedziała, że na to nie jest przygotowana i nie może odpowiedzieć dokładnie. Po części wina wszystkich. Śmiejąc się wyszła i zostawiła Hektora drapiącego się po głowie. Tak bezużyteczna odpowiedź...

Przychodzi Siluria a Hektor skonfundowany. Gdy opisał Silurii wróżbitkę, Siluria powiedziała, że taka tu na KADEMie się nie porusza. Za to Siluria odwracając uwagę od Alisy powiedziała Hektorowi o Vladlenie Zajcew, asystentce Emilii Kołatki. I są pewne dowody, ale ukryte za Alisą... która ma być anonimowa; Siluria jej nie odda.

Siluria poszła poprosić Janka o wizję z kamer, skąd ta "wróżka" się wzięła. Przyszła z zewnątrz, spoza KADEMu. Przede wszystkim - spotkać się z Ignatem i wymienić się telefonami komórkowymi (dała mu swój telefon fizyczny i dostała jego). Potem poszła do Hektora gdy go zauważyła i opuściła KADEM.
Ignat, gdy Siluria zapytała, nie wyparł się niczego. Przyszła do niego i za vine'a z "burger king prokurator" dała mu nowy telefon z kilkoma odcinkami "Śledzikowa" (serial Zajcewów) oraz kilka nowych plotek i żartów. Najpewniej była z prasy; Infernia ich skontaktowała.

Siluria poszła więc do Inferni i spytała o co chodzi. Infernia uśmiechnęła się do Silurii i powiedziała, że sprzedała "Paktowi" Hektora. Dlaczego? Bo chce opóźnić proces i nie zna swojego miejsca. W świecie Inferni życie jest proste:
- Marcelin nie przeprosił, więc 2 autobusy śledzików i przeprosiny # mundur.
- Hektor nie zna swojego miejsca, więc Pakt i upokorzenie
- Marcel Bankierz nadwyrężył jej reputację, więc spłonie.

Siluria poszła do Hektora i poklepała go współczująco po ramieniu. Infernia sprzedała go Paktowi (tego nie powiedziała, powiedziała tylko że to była dziennikarka Paktu). Super.
Dodatkowo - co gorsze - Hektor powiedział dziennikarce Paktu o "incydencie na poligonie". Na pewno to wykorzysta...

Wpłynęły informacje od Marty odnośnie wydarzeń na KADEMie:
- wybuch artefaktu był przypadkowy; Lucjan poprosił o pomoc Alisę całkowicie nieświadom tego, że nie działa dobrze bez nadzoru.
- Alisa się zestresowała i założyła, że Lucjan wie co robi. Przekierowała niewłaściwą ilość mocy (Lucjan powiedział "pełna moc" i walnęła pełną).
- Mariusz Trzosik zaniedbał edukacji Alisy. On się nie nadaje do edukowania osoby takiej jak Alisa, po prostu się nie da.

Odnośnie poligonu:
- Skażyciel został podłożony podczas włamu. Nie był podłożony wcześniej.
- Było dwóch magów SŚ. Jeden z "Kurtyny", drugi nie. Między nimi doszło do walki.
- Gdy KADEM odkrył magów Świecy, zwarli szyk. Jeden teleportował się terminus-grade teleportation tool, drugi używając magitech armour.
- Z uwagi na stopnie działania systemu KADEMu, magitech najpewniej został zmasakrowany a kryształ zasilający - zniszczony.
- Marta organizuje obławę. Natychmiast. Przekierowuje tam min. Eryka i Pawła. Łącznie z Aegis.

Hektor -> Edwin. Wytłumaczył mu swój plan:
- Marcelin i Infernia jadą na jednym wózku tak naprawdę. Więc jak on idzie na dno, ona też.
- Jeśli w wyniku działania tego wszystkiego faktycznie doszło do penetracji KADEMu i problemów Świecy, oboje ucierpią.
- Jeśli jednak to niepowiązane, oboje dostaną stracha i nic z tego strasznego nie będzie. 
- Cel Hektora to pokazać, że twardy i uczciwy prokurator opłaca się Świecy i KADEMowi, by możni nie przejęli się Paktem.
- Do tego celu Edwin musi zgłosić utratę żabolodów, że poniósł stratę. Przesunąć z "Infernia vs Marcelin" na "Świeca i KADEM vs Złe Siły".

Edwin zaszokowany. Hektor ma sensowny plan. Nieco wbrew sobie, ale wejdzie w to.

Niestety, wyszedł nowy odcinek Paktu. A tam "Hektor, który w najczarniejszej chwili martwi się przede wszystkim poligonem KADEMu", zimny karierowicz, którego bardziej interesuje KADEM niż sprawa, który chce opóźnić udając, że mu zależy na prawdzie a jednocześnie poświęci brata... ogólnie rzecz biorąc, artykuł skupia się na tym, że dramaty ludzkie i że Hektor. Silnie generuje presję na przyspieszenie procesu. Kimkolwiek jest "Jolka", jest dobra w swoim fachu.

A Hektor przygotowuje się do konferencji prasowej...

# Streszczenie

KADEM (bardziej chciwa część KADEMu) próbuje oskarżyć Hektorem o cokolwiek Tamarę, by zwinąć jej technologie magitechowych pancerzy. Siluria nie przepada za Tamarą czy Kurtyną. Hektor został ofiarą żartu Ignata i powstał filmik "amerykański kurczak szeryf Blakenpower", który został rozpowszechniony przez "Pakt". Ukradzione żabolody były z projektów zaliczeniowych studentów. Na zaatakowanym Poligonie KADEMu było 2 magów: jeden z Kurtyny, drugi nie i oni toczyli walkę. Jeden teleportował się terminus-grade teleportation tool, drugi używając magitech armour. I wiele wskazuje za tym, że Vladlena lub Emilia stoją za atakiem na KADEM lub na Hektora.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                            1. Instytut Zniszczenia
                            1. Restauracja
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Las Stu Kotów
                        1. Poligon KADEMu

# Zasługi

* mag: Siluria Diakon, która nie będąc seksualno-militarnym oficerem opanowała i zajęła się Alisą; dodatkowo agreguje i zarządza informacjami, by Hektor nie dowiedział się czegoś czego nie powinien.
* mag: Hektor Blakenbauer, który jakkolwiek "gonił za kurczakiem", sformował plan jak wyplątać Marcelina lub wplątać Infernię... i przekonał do planu Edwina.
* mag: Mariusz Trzosik, który wyciągnął (nielegalnie) z Zenona Weinera informację o kombinezonach * magitechowych Tamary. Też: przełożony Alisy.
* mag: Tamara Muszkiet, która (jakkolwiek nieobecna) okazuje się być przywódczynią "Kurtyny" w SŚ oraz ekspertem od kombinezonów * magitechowych. 
* vic: Demon_481, który opóźnia proces Marcelina jak tylko się da plus nieskutecznie próbuje aresztować Alisę.
* mag: Infernia Diakon, która jest bardzo niezadowolona z działań Hektora; nie tylko ściągnęła Jolę (dziennikarkę) ale i otwarcie zagroziła Hektorowi.
* mag: Ignat Zajcew, który wkręcił Alisę w "lożę erotyczno-militarną" której oficerem jest Siluria i ogólnie świetnie się bawił kosztem Hektora.
* mag: Alisa Wiraż, naiwna i mało przebojowa praktykantka KADEMu o ogromnej mocy działająca w Instytucie Zniszczenia; chciała podłożyć Hektorowi pluskwę. Szybko biega.
* mag: Marcel Bankierz, którego obecność tutaj nieco komplikuje sprawę; nie stoi murem za Tamarą i nawet się nie lubią, więc czemu ryzykowałby dla niej karierę?
* mag: Vladlena Zajcew, niesamowicie lojalna asystentka Emilii Kołatki która najpewniej stała za intrygą Alisy w sprawie Hektora.
* mag: Edwin Blakenbauer, który ku swojemu ogromnemu zdumieniu docenił plan Hektora i zdecydował się go poprzeć.
* mag: Jolanta Sowińska, reporterka Paktu, która zajmuje się tematem Hektora Blakenbauera. Bezczelna; zmiennokształtna i/lub iluzjonistka? Chyba SŚ.
* mag: Norbert Sonet, zbierający informacje i przejmujący tymczasowo dowodzenia nad sprawami wewnętrznymi KADEMu za zgodą Marty.