---
layout: inwazja-konspekt
title:  "Eskalacja Czelimina, eskalacja Andrei"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160810 - Zaszczepić Adriana Murarza! (HB, KB, AB, DK)](160810-zaszczepic-adriana-murarza.html)

### Chronologiczna

* [161229 - Presja ze strony Czelimina... (AW)](161229-presja-ze-strony-czelimina.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Rano, Andrea ma niespodziewanego gościa. Nieśmiało puka do drzwi. Wioletta, cała przerażona.

* Lady Terminus. Ja... ja chciałam bardzo, BARDZO, BARDZO przeprosić! - Wioletta, zdesperowana
* Przypadkiem... powiedziałam wszystko. Wszystko, czego miałam nie mówić. - Wioletta, załamana
* Komu? - Andrea, zimno
* Melodii Diakon... - Wioletta, biała jak ściana
* Jak ONA zareagowała? - Andrea, nadal zimno
* Er... przytuliła mnie... mocniej. - Wioletta, w bardzo... niefortunnym położeniu
* Gdzie jest teraz? - Andrea
* W swoim pokoju - Wioletta

Andrea dostała zaszyfrowaną wiadomość do Mieszka od Laureny. Użyła dekodowania Lady Terminus Świecy i wzmocniła naturalnymi parametrami.

"Tu Laurena. Mieszko, zgłoś się. Mam mały fire team. Wiem, gdzie jest Katalina. Chcemy uderzyć. Jeśli się nie odezwiesz, zakładam, że nie żyjesz, kochanie. Atakuję w nocy. I need a tactician! Skałki Markotne. (jakiś element hasła prywatnego)".

A Wioletta wciąż przestraszona. Ewidentnie boi się jakiejś strasznej kary. Andrea kazała jej pójść po Melodię. Ta przyszła, w pięknej i eleganckiej sukni. Wioletta ma czekać pod drzwiami i nie podsłuchiwać.

* Lady Terminus? - Melodia, z uśmiechem
* Wioletta powiedziała coś, czego nie powinna była mówić - Andrea, chłodno
* Domyślam się. W czym, nie to, że miała większy wybór - rozbawiona Melodia
* Domyśliłam się... - Andrea
* Co jest takie zabawne? - zdziwiona Andrea
* To dziecko (mówi o Wioletcie) było torturowane przez maga Świecy - Wiktora Sowińskiego. Torturowane. Używam tego słowa z pełną świadomością - Melodia, poważniejąc
* Ona została przebudowana zgodnie z jego wolą. - Melodia
* O jak głębokiej przebudowie mówimy? - Andrea, bardzo poważna
* Płytkiej. Dzięki Silurii. Ona ją w sobie trochę rozkochała. Możesz nie akceptować jej metod, ale Wioletta ma dzięki temu coś o co potrafi się zaczepić, coś, co nie jest Wiktorem - Melodia
* Ona strasznie... cierpi, boi się ORAZ jest w żałobie po swojej przyjaciółce. I myśli, że nie wolno jej tego robić. Czegokolwiek okazać. W ogóle, czuć. Bo nie było rozkazu. - Melodia
* Że wolno? - lekko zdziwiona Andrea
* Tak. - Melodia, zdecydowanie
* Więc chcę jej pomóc i jej pomogę. W pewien sposób wejdę na miejsce Silurii - Melodia, z uśmiechem
* Miałaś rację, że jeszcze mogę tu Ci się przydać. - Melodia, znowu poważniejąc
* Co powie, zostanie między nami. Po prostu tak się bała, że powiedziałam jej, by Ci powiedziała co zrobiła - Melodia
* Jeśli ci to ułatwi, przedstawię to tak, jakbyś ją wybroniła od kary. - Andrea, chcąc pomóc Melodii
* Nie, nie rób tego. Powiedz jej, że nie zamierzałaś jej karać, acz jesteś rozczarowana. Nie jesteś Wiktorem Sowińskim - Melodia, szybkim głosem
* Nie jestem. - Andrea, naprawdę zdziwiona tym porównaniem
* Ale ona tego nie widzi. Nie wie. Masz władzę. Ona zna tylko Wiktora - tak ją sobie zbudował. - Melodia, bardzo zimno
* Ona dużo widziała. Może się dużo domyśleć. Rozumie naturę naszego wroga. - Melodia, zastanawiając się.

Przerwano im tą rozmowę. Deiiw zaczął pukać w szybę. Dzióbie. Andrea spojrzała na Melodię z wyrzutem. Ona zrobiła gest "to nie ja", błędnie rozumiejąc. Andrea otworzyła okno.

* Oczy! Oczyoczyoczy! Dei-i-i-w widzi oczy! - Deiiw, próbując ostrzec Andreę
* Groźne oczy. Oczy wszędzie. Oczy patrzą, oczy szukają. - Deiiw
* Gdzie szukają? - Andrea, zdziwiona
* Jeszcze nie tu. Ale będą tu. Oczy. - Deiiw, sfrustrowany.

Andrea wysłała Deiiwa po Lilaka. Deiiw spytał, czy Andrea pomaluje mu pióra. Wybawiła ją Melodia - ona pomaluje Deiiwowi pióra. Gdy Deiiw wyleciał, Andrea spojrzała na Melodię z wdzięcznością.

"Pospiesz się, kochanie. Proszę." - Laurena, do Mieszka, zaszyfrowaną wiadomością, przechwyconą przez Andreę.

Andrea została z Melodią. Odesłała Melodię i poprosiła do siebie Wiolettę. Powiedziała Wioletcie, że jest rozczarowana, że sekret nie utrzymał się nawet doby. Tu Wioletta zwiesiła głowę i próbuje pokazać swój poziom skruszenia (gdyby była psem, położyłaby się na grzbiecie i pokazałaby brzuch). W czym, powiedziała jej, że nie zamierza jej za to karać. Jednak - nikt INNY nie może się dowiedzieć a w szczególności Mieszko Bankierz. Wioletta skwapliwie potwierdziła. Andrea zauważyła (w duchu), że ostatnio Wioletta powiedziała dokładnie to samo.

* Jeśli Mieszko Bankierz się o tym dowie, najprawdopodobniej zginie. - Andrea, ze smutkiem
* Lady Terminus... jeśli się dowie, będzie miał szczęście, jeśli zginie... - Wioletta, lekko drżąc
* Co masz na myśli? - Andrea, zaciekawiona co Wioletta się domyśla
* Sabina Sowińska... przepraszam - Wioletta ma problemy z kontynuowaniem tego co mówić
* Nie możesz mówić? - Andrea, podejrzewając geas
* Mogę. Przepraszam. Ona została sprowadzona do roli zabawki, potem skorumpowała Aleksandra Sowińskiego a potem zabiła Ozydiusza - Wioletta, już opanowana
* A to wszystko zrobił ten kraloth i Wiktor Sowiński - Wioletta, suchym głosem - Dagmara... to co zrobili mojej przyjaciółce było jeszcze gorsze.
* Zmienili ją, Lady Terminus. Teraz ona dowodzi. A raczej... to, co jest tam zamiast niej. - Wioletta, z ciężkimi flashbackami
* Czelimin stoi za wszystkim. Dlatego... nie idźmy tam. To będzie nasz koniec. - Wioletta, błagając

Andrea pozwoliła Wioletcie odczuwać żałobę (jakkolwiek głupio się przy tym nie czując). Po czym ją odesłała i została sama z czarnymi myślami.

Do pokoju Andrei zapukał Rafael. Andrea wpuściła.

* Słyszałem, że tu dziś dowodzisz, Andreo..? - lekko zdumiony Rafael
* Mam pewien pomysł, po wczorajszym - Rafael, przechodząc od razu do rzeczy
* Słucham... - Andrea, bardzo sceptyczna
* Laragnarhag nie jest zwykłym kralothem Millennium. To znaczy: nie ma zwykłych kralothów... - Rafael, lekko sfrustrowany niemożnością wyjaśnienia
* Na czym polega jego niezwykłość? - zdziwiona Andrea
* Pamięta _takie_ rzeczy jak godtech. Nie jego pamięć, ale pamięta. Interesował się tym - Rafael, zapalający się
* On chciał pójść do Wiktora. By zrozumieć, by zobaczyć... ale nie wszystko wie. - Rafael
* W sensie? - Andrea, nie rozumiejąc
* Jest kralothem używającym terminów nawet nam nieznanych - Rafael, próbujący wyjaśnić - Co gorsza, on myśli jak kraloth.
* Gdyby powiązać wiedzę Laragnarhaga z wiedzą Pszczelaka i zrobić unię z tą, Wiolettą, która tam była, to możemy naprawdę dużo się dowiedzieć co się dzieje - Rafael, przedstawiający pomysł
* Mogę gwarantować, że wyjdzie z tego bez szwanku. Dołączę się jako ochotnik, jeśli chcesz mieć pewność - Rafael, myślący, że to uspokoi Andreę...
* ... - zaszokowana i rozdarta Andrea

"Ja jako ja... nie puściłabym tego. Ale Andrea to pieprzony wydział wewnętrzny. Nie bez powodu Agrest z nią pracuje... she can be a bitch. A to jest przewaga... potencjalnie duża." - rozdarta Kić, 2016 - "To JEST dla mnie pewien wysiłek, by myśleć jak Andrea, szczególnie w takiej sytuacji..."

* Nie nakażę tego ani Wioletcie ani Julianowi. I nie będzie to moja inicjatywa - Andrea, ze smutkiem, do Rafaela.
* Nie chcę doprowadzić do wojny między Millennium a Świecą. - Rafael, spokojnie
* Chcesz nie być odpowiedzialna, nie ma problemu. Polityka. Ale muszę wiedzieć jednoznacznie, że wolno mi to zrobić. - Rafael, nadal spokojnie
* Taka wiedza byłaby bardzo użyteczna, ale ja nie mogę czegoś takiego nakazać i nie nakażę - Andrea, z nadzieją, że Rafael zrozumie
* Nieproste. Wykonalne. - Rafael, zamyślony - Nie przyszedłem do Ciebie. Dzięki, Andreo. Zapomnij o całym temacie.
* Pszczelak jest mi potrzebny do jutra. - Andrea, bardzo spokojnie acz stanowczo
* Rozumiem. - Rafael, szeroko uśmiechnięty

Rafael wyszedł. Andrea zadumała się nad swoim procesem. Po raz kolejny.

Deiiw przybył z Lilakiem do Andrei. Lilak się przywitał uprzejmie. Deiiw chciał pióra...

Lilak ostrzegł, że po Śląsku poruszają się oddziały viciniusów i szukają... kogoś lub czegoś. Szukają po kanale magicznym, szukają wizualnie, szukają po emocjach. Są dość dokładne, ale nie ma tych grup dużo. To się łączy Andrei z wiedzą o Spustoszonych dronach...

Lilak zaproponował Andrei, że może spróbować znaleźć kompozycję oddziałów Oczu. Andrea poprosiła, by określił ilość tych oddziałów i ich mniej więcej kompozycję. Lilak nie ma zamiaru używać do tego celu magów - poprosi samych viciniusów. Andrea przyklasnęła pomysłowi. Ale muszą ukryć magów. Lilak potwierdził. Ich magowie, na ich terenie, będą bezpieczni. Lilak poleciał zająć się sprawami kompozycji i detekcji a Deiiw został wysłany do Melodii - ktoś musi malować te pióra...

Wśród sił Zajcewów znajduje się oficer taktyczny. Rodion Zajcew. Wysoki oficer taktyczny Iriny Zajcew. Andrea poprosiła go na konsultacje. Po wyjaśnieniu Rodionowi swojego planu, ten przeszedł do konkretów.

* Tien Wilgacz. Czyli mamy problem, bo coś chce pożreć Twojego terminusa? - Rodion, ze standardowym taktem
* Tak, a przy okazji może wykryć nas. Wolałabym nie dać go pożreć. A faktycznie co się stanie - będzie dużo gorsze. - Zmartwiona Andrea
* My mamy kralotha, nie?
* Tak... przy czym, jego utrata jest nieakceptowalna - Andrea, wyciągając szybkie wnioski
* Wróg szuka po magii krwi, nie? Tak mówiłaś - Rodion, eksplorujący inne możliwości ;-)
* Wszystko na to wskazuje, tak. Przy czym: znowu, nie jest to tylko magia krwi.
* Ale jakby kraloth pożarł twojego terminusa i nic z niego nie zostało, to jego wróg by go nie szukał - Rodion, z lekkim półuśmiechem.
* ... - Andrea, niezbyt zadowolona z jego sugestii. Aż zesztywniała.
* Oi, mała - Rodion, lekko zaskoczony jej reakcją - Ty kontrolujesz tego kralotha, nie?
* No to... jeśli wróg myśli, że wszystkie sygnały dochodzą od kralotha co wpieprzył twojego terminusa... to nie będzie go szukał - uśmiechnięty Rodion
* Nie no, patrz na to - Rodion, z szerokim uśmiechem - terminus zrobił swoje, tak? Został ranny. Poszedł się schować. Znalazł go kraloth i zjadł
* Brzmi jak pechowy terminus - Andrea, rozumiejąc o co chodzi
* Czyli łącząc Twój poprzedni plan z tym: kraloth ma sygnaturę terminusa, oddział ginie do viciniusów czy artylerii, widzą kralotha, raczej nie szukają dalej. - Rodion.

Andrea dopytała jeszcze Rodiona jak wygląda relacja między nim a Tatianą. Rodion powiedział, że Tania ma serce po dobrej stronie. Nie nadaje się na oficera bojowego. Jest raczej zwiadowcą, elektronem, nie terminuską. Udają, że dowodzi. Tylko ona ma uprawnienia do niektórych rzeczy, ale nie jest głupia. Rodion dodał, że on wyjaśni jej sytuację i Tania przekaże Rodionowi dowodzenie. Andrea poprosiła, by Rodion - jak skończy - przesłał Tatianę do niej. Andrea potrzebuje wsparcia.

W tym czasie Andrea porozmawia z Laragnarhagiem. I Rafaelem. Są razem, jak zwykle.

* Piękna lady terminus - uśmiechnięty Rafael
* Tak... witaj, Rafaelu - standardowa Andrea
* Laragnarhag, czy potrafisz sprawić, by WYGLĄDAŁO, jakbyś pożarł maga? - Andrea, zaznaczając 'wyglądało'.
* W dużym skrócie, zbliżają się do nas oddziały poszukiwawcze, między innymi mortalisy. Szukają Mieszka Bankierza. - Andrea, dodając kontekst
* Jeśli okaże się, że padł do kralotha... jest nadzieja, że stracą zainteresowanie tym terenem. - Andrea, kończąc
* ...tak. - Rafael, w imieniu Laragnarhaga
* Będziemy potrzebowali jego krwi - Rafael, w imieniu Laragnarhaga
* Niedługo po niego poślę - Andrea
* Nie stanie mu się krzywda. To mogę zapewnić. - Rafael, z chłopięcym uśmiechem
* Z czym wiąże się taki proces? - Andrea, chcąc się dopytać
* Unia - Rafael, tłumacząc - Stanie się elementem kralotha na pewien czas. A następnie kraloth go 'wypluje'.
* Czy można zrobić tak, by pozostał mniej lub bardziej nieprzytomny? - Andrea, z nadzieją
* Kraloth jest biologicznie defilerem. Jeśli chcemy oszukać przeciwnika który wie, jak działa kraloth, Laragnarhag musi... działać jak kraloth - Rafael, poważniejąc
* Czy Skażenie coś zmienia? - Andrea
* Nie. To jak ketchup do pizzy. Nie do każdej smakuje tak samo, ale zjesz pizzę z ketchupem, nawet jak włoska - bardzo obrazowy Rafael
* Czy instynkty kralotha nie przeważą? - Andrea, bardzo, bardzo zmartwiona tą analogią
* Będę częścią unii dla Ciebie, piękna pani - Rafael, chcąc uspokoić Andreę

"Czemu ja rzucam terminusa na pożarcie kralothowi jako terminus?!" - roześmiana Kić, 2016...

Andrea ustaliła z Rafaelem, że Laragnarhag oznaczy teren magią krwi jako swój.

Przyszła do Andrei Tatiana. Andrea poprosiła ją o ściągnięcie Rudolfa Jankowskiego i Mieszka Bankierza. Tatiana zaakceptowała zadanie. Węzeł Skaża w gniew + paranoję, ale Tania jest bardem; jest odporniejsza. Przy okazji, powiedziała Andrei dwie rzeczy: jeden, Rodion teraz dowodzi Zajcewami. Dwa, Kirył. Tania wie coś o nim. Kirył jest przywódcą bardzo małej gildii nomadycznej; nie tak dawno stracił członka załogi i jej rozpaczliwie szuka. Ślady wskazują na Polskę. Czarodziejka, która zaginęła z ekipy Kiryła nazywa się Karina Łoszad. Tania dodała, że Kirył akumuluje surowce, środki, broń... po pijanemu podobno rozważał wypowiedzenie wojny Polsce by ją odzyskać. Raz już się od Świecy odbił. Pół roku temu.

Tatiana się oddaliła. Andrea zadzwoniła do Kiryła. Ten powiedział, że aby się do nich dostać, jest pewien problem. Andrea sparowała, że potrzebuje przerzucić Śledzika w pewne miejsce... Kirył się złapał za głowę. Powiedział Andrei, by poszła do Mariana i poprosiła o złoty ząb dziadka Kiryła. Andrea się... lekko zdziwiła. Ale zakłada, że to jakieś hasło kodowe. Kirył powiedział, że aby tego użyć... trzeba zakopać go w ziemi, zacząć śpiewać "Kalinkę", i w DŁUGĄ!. Andrea zdecydowała, że to zadanie dla Zajcewa.

* Czy to ściąga Śledzika w stanie agresywnym? - zaintrygowana Andrea
* Czy pitbull, który widzi swego pana i chce go wyślimtać jest agresywny? A jak zobaczy, że to nie jego pan a miał nadzieję? - Kirył, odpowiadając... obrazowo
* Oki... jak dużo tej Kalinki ma być? - podłamana Andrea
* Widzisz, i tu jest ten problem... nie mam pojęcia - Kirył, tak szczerze jak tylko umie. - Jeśli mogę doradzić, użyj dziewczyny. Osobnika płci żeńskiej.
* Okej... będzie mniej agresywny? - Andrea, znowu zaintrygowana
* Nie strzeli pierwszy. - Kirył, najzupełniej poważnie - Najwyżej przejedzie, ale nie strzeli. 
* Czego potrzebujesz, żeby zacząć go kontrolować? - Andrea
* Nie na ten telefon - Kirył
* Kiryle Sjeld, mam dla Ciebie jedną propozycję. Może Ci się spodobać. - Andrea, zastawiając pułapkę
* Zamieniam się w słuch. - Kirył, który nie ma pojęcia w co zaraz wpadnie
* Pomóż mi w rozwiązaniu moich problemów a Świeca pomoże Ci znaleźć Karinę. Nie musisz teraz odpowiadać. - Andrea, poważnie
* Cieszę się, że w końcu zauważyliście. Na razie pomagam jak mogę. - Kirył, lekko zaskoczony
* Wiem. - Andrea, świadoma, że to go silnie zwiąże z jej planami

Tak. Wezwanie Śledzika to zdecydowanie zadanie dla Zajcewa. Zajcewki. Rodion polecił Tatianę. Jest bardką, szybko biega, nie ma instynktu zaczynania od walki... a jeśli nie dadzą tej szansy Tani, to Tania im nigdy tego nie wybaczy.

Andrea poszła więc do Mariana Łajdaka i poprosiła go o złoty ząb dziadka Kiryła. Ten, z boleścią, oddał Andrei ów ząb... powiedział, że to jest, no, ząb. Kirył mu go dał i Łajdakowi trudno było odmówić. Miał to nosić przy sobie dwa miesiące, chyba, że ktoś poprosi.

* Dał ci coś jeszcze? - zaciekawiona Andrea
* Jeśli o to poprosisz, tak. Jeśli nie, nie. - Marian, jak zawsze wykrętnie

Wróciła Tatiana z Mieszkiem i Rudolfem. Mieszko ciągle nieprzytomny. Andrea kazała Rudolfowi zanieść Mieszka do Rafaela. A potem - niech Dalia przejrzy stan Rudolfa. Tatianie Andrea dała nowe zadanie - przyzwać Stalowego Śledzika Żarłacza w odpowiednie miejsce... Tania ma banan na twarzy. Dostała puzderko z zębem i się oddaliła przyzywać Śledzika...

* Zajcewowie przygotowują grupę uderzeniową do zbombardowania grupy viciniusów GDZIEŚ.
* Tatiana przyzywa Śledzika
* Kajetan podszkala skrzydło szkoleniowe
* Rafael kusi Pszczelaka Wiolettą (dostał odpowiednio sfabrykowany posiłek)
* Melodia zajmuje się piórami Deiiwa
* Lilak zbiera informacje z otoczenia
* Laurena jest całkowicie zignorowana
* Lidia i Dalia próbują uruchomić Jankowskiego
* Mieszko z ketchupem jest mierzony łakomym wzrokiem Laragnarhaga

Ogólnie, nieźle...

"Śledzik i kraloth... co za paskudny teren..." - Kić, ostatnie zdanie 2016 - "Przynajmniej dla innych :3"


# Progresja


# Streszczenie

Andrea zignorowała błagania Kataliny. Laurena się odezwała, że ma mały oddział i chce uratować Katalinę. Andrea to też zignorowała - pułapka. Wioletta przyznała się, że wszystko wygadała Melodii. Melodia powiedziała Andrei, że Wioletta jest bardzo uszkodzona przez Wiktora. Rafael zaproponował, by dowiedzieć się o naturze wroga przez unię: Laragnarhag x Wioletta x Pszczelak. Andrea NIE powiedziała nie. Gdy Deiiw i Lilak przynieśli wiadomość o niebezpiecznych Patrolach ("oczy"), Rodion Zajcew zaproponował 'nakarmić' Laragnarhaga Mieszkiem (by zmylić). Andrea się zgodziła - jeśli to odwracalne ;-). Andrea obiecała Kiryłowi pomoc w znalezieniu Kariny - za co on pomógł jej przesunąć Śledzika do polowania na Patrole...

# Zasługi

* mag: Andrea Wilgacz, która ze smutkiem dopisuje nowe dokonania do swego procesu. Rzucenie Mieszka kralothowi, danie Rafaelowi wolnej ręki w sprawie Wioletty i Pszczelaka (i kralotha) i inne takie.
* mag: Wioletta Bankierz, nie utrzymała sekretu przez 24h i wszystko powiedziała Melodii. Jest dobrze wytrenowana przez Wiktora Sowińskiego. Ciągnie ją do Diakonek.
* mag: Laurena Bankierz, która podobno chce uratować Katalinę i prosi o pomoc Mieszka. Zignorowana przez wszystkich - zdaniem Andrei, pułapka.
* mag: Melodia Diakon, próbuje naprawić Wiolettę kontynuując pracę Silurii. Chce pomóc Andrei. Kończy na... malowaniu piór Deiiwa.
* vic: Deiiw Podniebny Grom, znalazł oddziały detekcyjne Karradraela z Czelimina. W zamian - Melodia malowała mu pióra.
* mag: Rafael Diakon, z kolejnym chorym pomysłem (Wioletta x Pszczelak x kraloth). Komunikuje kralotha z Andreą. Z trudem, próbuje skusić Pszczelaka Wiolettą.
* vic: Świeży Lilak, wyjaśnił Andrei o co chodzi z oddziałami zwiadowczymi * viciniusów. Pozna kompozycję i lokalizację.
* vic: Laragnarhag, kraloth specjalizujący się w Starej * magii i Starych Technologiach (godtech). Centralny element planów Andrei i Rafaela, ku zmartwieniu tej pierwszej.
* mag: Rodion Zajcew, wysoki oficer Iriny; 4x-letni Zajcew, strateg i taktyk. Pomógł Andrei z planem wykorzystującym kralotha do ukrycia Mieszka.
* mag: Tatiana Zajcew, jednak nie ona dowodzi Zajcewami a Rodion ;-). Dziewczyna na posyłki. Ale ma swój moment - przyzywa Śledzika Żarłacza zębem dziadka Kiryła...
* mag: Karina Łoszad, członek załogi Kiryła Sjelda, która zniknęła w tajemniczych okolicznościach ~rok temu. Kirył jej szuka do dzisiaj.
* mag: Kirył Sjeld, który przyzywa Śledzika częściami ciała (?!) swojego dziadka i który pozostaje lojalny przyjaciółce - Karinie Łoszad - która zniknęła ~rok temu.
* mag: Marian Łajdak, przekazujący złoty ząb dziadka Kiryła Andrei...
* mag: Mieszko Bankierz, napromieniowany przez Węzeł paranoi i gniewu. Ma być (tymczasową) karmą dla kralotha by zmylić Karradraela ;-).
* mag: Rudolf Jankowski, napromieniowany przez Węzeł paranoi i gniewu. Oddany Dalii i Lidii do naprawy, znowu.
* vic: Stalowy Śledzik Żarłacz, który gdzieś grasuje w okolicy i utrudnia Karradraelowi (i w sumie wszystkim) życie.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, centrum dowodzenia Andrei i centrum dużej ilości ludzkich dramatów ;-)
                    1. Obrzeża
                        1. Skałki Bajeczne, skąd Tatiana wyciągnęła Mieszka i Rudolfa na polecenie Andrei

# Czas

* Dni: 1