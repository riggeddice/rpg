---
layout: inwazja-konspekt
title:  "Szept Mare Felix"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180318 - Czyżby drugi kraloth?](180318-czyzby-drugi-kraloth.html)

### Chronologiczna

* [180318 - Czyżby drugi kraloth?](180318-czyzby-drugi-kraloth.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

**Wizja artysty**:

* brak

**Strony konfliktu, atuty i słabości**:

* brak

### Wątki konsumowane:

brak

## Punkt zerowy:

## Mapa logiczna Opowieści:

brak

## Pytania Generatywne:

* Ż: 
* K: 
* Ż: 
* R: 

## Potencjalne pytania historyczne:

* null

## Misja właściwa:

**Dzień 1**:

Lucjan otworzył oczy. Leży pośród kwiatów. Jak okiem sięgnie - łąka. Step, ale ukwiecony...
To samo Melinda. Kwiaty... ona jest botanikiem. Nie kojarzy tego. Nigdy takich nie widziała. Zasugerowała Lucjanowi zjedzenie kwiata, dla ciekawości. Lucjan zeżarł - i Porażka! Kralotyczny Kwiat uzmysłowił mu, że jest żywy... i że jego ciało nie jest jego. Lucjan zwymiotował kwiat.

Melinda chciałaby zdobyć jakieś kwiaty, ale nie wie jakie i gdzie. Boi się jakichś efektów ubocznych... zerwała jednak jednego z nich.

Melinda przypomniała sobie Klub Kolt w Jodłowcu. I Kwiaty ich tam przesunęły. Są w kanciapie. Zanim cokolwiek się stało, Lucjan wywalił drzwi i wszedł. Nago. Zaczął od "dzień dobry" i "dajcie ubrań". Ludzie pouciekali, został jedynie barman Janek. Janek patrzy z przerażeniem na nagiego faceta (konstruminusa). Lucjan pyta Janka czy ten go kojarzy. Janek kłamie, że tak. Lucjan słyszy, że Janek zawezwał policję - nacisnął przycisk. Aż jest zaskoczony, że to usłyszał...

Melinda podeszła i pocałowała Janka. Mindwarpowała go, kwiat kralotyczny aż się zwinął z radości. Janek - jej niewolnik - dał jej ubranie.

Lucjan spojrzał na Whisperwind i ją zauważył (wychyliła się, by móc znaleźć co oni robią). Zaatakował ją i mocno uderzył. Whisper, nie chcąc złamać symulacji, zaczęła udawać "eteryczną dziennikarkę". Gdy Lucjan powiedział, by poszła z nimi - ucieszyła się i poszła. Gdy Melinda zakwestionowała, by Whisper poszła z nimi - udało się Whisper z trudem utrzymać symulację...

Pojechali samochodem do "bezpiecznego miejsca" z Jankiem prowadzącym. Melinda chciała do Glina Żółtego, do swojego domu. Glitch symulacji; Whisper z trudem utrzymała symulację, ale Lucjan i Melinda to zauważyli. Whisper próbowała się wyłgać, że nic nie wie na ten temat... oczywiście, nie wyszło jej. Symulacja się rwie. Powiedziała im WIĘKSZOŚĆ prawdy - są w symulacji, ona chce ich wyciągnąć i nie może sama dojść do tego co i jak tu się stało. Poprosiła, by zachowywali się naturalnie. Po tym, jak zaczęli chwilę rozważać "życie swoim życiem" czy "czemu mamy jej pomagać", zauważyła, że nie ma większego wyjścia - symulacja już jest niestabilna.

Weszli do mieszkania Melindy. Janek dostał zadanie zrobić poncho dla Lucjana, Melinda poszła się wykąpać i przebrać.

Wizja przeszłości. Kraloth (Hralglanath) ich wyrwał z ich ciał i zrobił im to. To JEGO wina. On zrobił z Lucjana konstruminusa. On zrobił z Melindy kralothborn... nasi bohaterowie zdecydowali się zapolować na tego kralotha i go zniszczyć. Whisper, zapytana, powiedziała im - nie wiedzą o tym, czy wiedzieli o magii wcześniej. Sama Whisper zapisała sobie, że ten Drugi Kraloth wysyłał te wizje o Hralglanacie do tej dwójki. Czyli to była próba egzekucji kralotha.

Simulation shift. Są pod Mordownią Areną Wojny. Potrzebują wojowników, takich na rozwalenie kralotha. Więc - weszli do środka, Melinda pocałunkiem unieszkodliwiła barmana i ów pokazał im zejście na dół. Gdy Lucjan zszedł, poczuł mrowienie energii magicznej. Ktoś rzucił czar. Zainteresowało go to poważnie. Zobaczył Bójkę siedzącą na widowni, gdy siedem Niedźwiedzi się siłowało i ćwiczyło walkę. Lucjan się nie bawił - użył ciała konstruminusa i zaatakował niespodziewającą się niczego czarodziejkę Millennium.

Bójka z trudem obroniła atak i padła na arenę z Lucjanem. Zdążyła tylko pisnąć "Świeca... czemu? jak to?", zanim Melinda jej nie pocałowała, wprowadzając ją w mindwarp state. Niedźwiedzie niezadowolone z tego, że ich maskotka przestała oglądać, poszły pogadać z Lucjanem i doszło do bijatyki. Tymczasem Melinda wzięła Bójkę na bok i ją przesłuchała - czym jest kraloth, jak oni mogą go znaleźć itp.

Bójka powiedziała, że ona może znaleźć kralotha dla Melindy, jeśli będzie gdzieś blisko. Melinda wie, gdzie zabrać Bójkę - do Czerwonej Biblioteki. Bójka też powiedziała (wymuszone), że wyrywa się spod kontroli Melindy, więc Melinda zaaplikowała jej Kralotyczny Kwiat. Bójka odpłynęła, pod kontrolę Drugiego Kralotha.

Potem Bójka, zdominowana przez Melindę i Drugiego Kralotha, opanowała magią walczące Niedźwiedzie i oddała je Melindzie do całowania, po czym wyczyściła pamięć...

...symulacja się rozsypała. Whisperwind westchnęła ciężko. Tak czy inaczej, Whisper wie to co chciała - kraloth, DRUGI kraloth zabił Hralglanatha. Rękami Lucjana i Melindy. Whisper poprosiła Lucjana i Melindę, by oni spróbowali zlokalizować, gdzie znajduje się pole kralotycznych kwiatów - Melinda, opętana kralotycznym głodem chciała na Fazie wezwać kralotyczne kwiaty do pożarcia Whisperwind. Whisper się nie dała; odskoczyła i powiedziała, że ich uratuje.

Symulacja się skończyła, Whisper - potłuczona - wróciła do Zamku As'caen, poszukać Silurię i powiedzieć jej, że Bójka wpadła w zupełnie inny rodzaj kłopotów...

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

nowy wariant:

* 3: wygranie Typowego konfliktu
* 5: wygranie Trudnego konfliktu
* *2: wygranie Eskalacji

Z sesji:

1. Żółw (12)
    1. Bójka wycofała się do Drugiego Kralotha; gotowa do spawnowania nowego kralotha (5)
    1. Kralotyczne Kwiaty zaraziły Mare Felix przez działania Whisper i Melindy (5)
    1. W Jodłowcu pojawia się coraz silniejsza ideologia "samiec Twój wróg" reprezentowana przez Kurzyciela (2)
1. Kić (4)
    1. Melinda skończy jako mag, nie kralothborn - krew viciniusa (5)
1. Raynor (6)
    1. Lucjan skończy jako konstruminus-hipis (2)
    1. Zielony Kwiatek rośnie w siłę po tym, jak jego czampion wpierniczył Niedźwiedziom (2)
    1. Lucjan potrafi poruszać się po Fazie Daemonica nie generując Pryzmatycznego Skażenia (3)

# Streszczenie

Whisperwind zbudowała symulację na Mare Felix by odnaleźć co stało się z Hralglanathem. Udało jej dotrzeć sie do agentów Drugiego Kralotha i odnaleźć, jak padła do kralotha Bójka Diakon (o czym nikt nie wiedział). Niestety, przy okazji, nieświadomie, dała przyczółek Kralotycznym Kwiatom na Mare Felix...

# Progresja

* Bójka Diakon: wycofała się do Drugiego Kralotha jako jego oddana agentka; gotowa do spawnowania nowego kralotha
* Melinda Słonko: po zakończeniu tej kampanii i odzyskaniu jej przez magów skończy nie jako kralothborn a jako mag
* Lucjan Kowalkiewicz: doszło do fuzji jego umysłu i ciała konstruminusa Świecy. Jest już konstruminusem
* Lucjan Kowalkiewicz: z uwagi na obcość biologiczną i mentalną, potrafi poruszać się po Fazie Daemonica nie generując Pryzmatycznego Skażenia
* Glarnohlagh: rozprzestrzenił swoje Kwiaty na Fazie Daemonica.
* Glarnohlagh: uzyskał Bójkę w stanie nadającym się na zbudowanie nowego kralotha.
* Janek Malczorek: Skażony przez Glarnohlagha mocą Melindy i Bójki; cichy agent Klasztoru Zrównania.

## Frakcji

* Zielony Kwiatek: po tym, jak Lucjan wpierniczył Ognistym Niedźwiedziom ta organizacja wzrosła w siłę i w szacunek w okolicy.

# Zasługi

* vic: Melinda Słonko, kralothborn ściągnięta przez Whisper na Mare Felix. Okazało się, że Mindwarpowała barmana Kolta, Mindwarpowała Bójkę i ogólnie dała Drugiemu Kralothowi sukces. Aha, ściągnęła kralotyczne kwiaty na Mare Felix.
* vic: Lucjan Kowalkiewicz, konstruminus-kralothborn ściągnięty przez Whisper na Mare Felix. Wykrył obecność Whisper i doprowadził do zniewolenia Bójki.
* mag: Whisperwind, która przeprowadziła bardzo trudną symulację by odnaleźć przeszłość Hralglanatha... odkryta, udawała dziennikarkę i przypadkowo zaraziła Mare Felix kralotycznymi kwiatami.
* czł: Janek Malczorek, barman w "Kolcie" w Jodłowcu; spotkał się z Melindą i dostał mindwarp by stać się jej posłusznym niewolnikiem.
* mag: Bójka Diakon, co do której wreszcie wiadomo, czemu zmieniło się jej zachowanie z obserwatorki w lalkę. Została kralotycznie Skażona.

# Plany

* Mariusz Kurzyciel: ma zamiar poważnie siąść do tematu eksploatacji kobiet przez mężczyzn i nawrócić wszystkich w Kolcie. Jak trzeba, siłą.

## Frakcji



# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Felix, zarażone Kralotycznymi Kwiatami przez Whisperwind i Melindę Słonko
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Glin Żółty
                    1. Biblioteka Czerwona, gdzie Melinda i Lucjan zaatakowali i Skrzywdzili Hralglanatha z pomocą Ognistych Niedźwiedzi
                    1. Osiedle Misesa, gdzie mieszkała Melinda Słonko
                1. Jodłowiec
                    1. Klub Kolt, gdzie - jak się okazuje - znajdował się portal do Kralotycznych Kwiatów Glarnohlagha
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Pylisko
                        1. Mordownia Arena Wojny, miejsce zdominowania Bójki przez Glarnohlagha pośrednio, przez swoich agentów (Melindę i Lucjana)

# Czas

* Opóźnienie: 0
* Dni: 2

# Narzędzia MG

## Opis celu Opowieści

* 

## Cel Opowieści

* 

## Po czym poznam sukces

* 

## Wynik z perspektywy celu

* 

## Wykorzystana mechanika

Mechanika 1803, wariant z daty 1803

![Testowa mechanika Inwazji z żetonami, 180327](Materials/180399/mechanics-180327-test.png)

* obecność Eskalacji: 2-2-2.
* obecność żetonów

Wpływ:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
