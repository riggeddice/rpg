---
layout: inwazja-konspekt
title:  "Batmag Uderza!"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160128 - Byli sobie przestępcy (HB, AB, LB)](160128-byli-sobie-przestepcy.html)

### Chronologiczna

* [160128 - Byli sobie przestępcy (HB, AB, LB)](160128-byli-sobie-przestepcy.html)

### Punkt zerowy:
## Misja właściwa:

## Kontekst misji

Komendant straży pożarnej został aresztowany; podobno robił bombę i w ogóle.
Hektor był zagrożony, ale nieudolnie nań sfabrykowano dowody,
Kimaroj został przestraszony przez Alinę, że ktoś na niego poluje.

## Stawki:

K - Czy uda się Kimarojowi dowiedzieć się, kto go tępi?		NIE
D - Czy Pafnucy odpowie za grzechy przeszłości?				TAK
B - Czy Skubny będzie powiązany z tym wszystkich?			TAK

### Faza 1: Wina Skubnego

Dzień 1: 

Hektor się ucieszył, że znalazł katalistkę. I co lepsze, z własnego rodu. W piwnicach Blakenbauerów. Bombę zabezpieczyli saperzy i wywieźli do Rezydencji... jak Huberta Rębskiego nie było. I wesoła rodzinka Blakenbauerów w piwnicach, z zajumaną bombą zrobioną z achtungów.
Bomba wygląda BOMBOWO. Hektor pokazuje fiolkę Marcelina wykrywającą aurę magiczną ze słowami "przy tym mruga". Klara chciała wyjść z pokoju, ale Hektor ją zatrzymał; prosty katalistyczny skan Klary pokazał, że mają do czynienia z bombą zrobioną przez maga znającego świat ludzi; ktoś zastąpił narzędzia magią. Bomba została zrobiona by zostać znalezioną... i nie wybuchnąć.

Klara zdecydowała się zebrać odcisk magiczny; zajmie to trochę czasu, ale dostanie to. Dostanie coś co nadaje się jako dowód - "tak, to był ten mag".

Tymczasem podczas zdejmowania dowodu przez Klarę Hektor dostał wiadomość od Kleofasa Bora. Niejaki Daczyn przyjechał limuzyną do "Wyżerki" pogadać z Kimarojem. Na szczęście, Alina wcześniej zostawiła podsłuch jako Pafnucy w kwiatach... by wyglądało, jakby to był podsłuch Skubnego.

Oczywiście Daczyn znalazł podsłuch. Ale wpierw dał radę umówić się z Kimarojem który panikuje, że ktoś na niego poluje. Daczyn dostanie za to zmianę dostawcy w "Wyżerce" na takiego, jaki pasuje Daczynowi oraz dodatkowo dostanie "coś" w jednym czy dwóch pokojach. Daczyn się tym zajmie.

Leo skupił się na informacjach odnośnie tego, skąd Skubny mógł wiedzieć. Proste przeszukanie kupna i sprzedaży informacji przyniosły ciekawą odpowiedź - Skubny sprzedał informacje o konkretnych grzeszkach Pafnucego oraz Bogdana komuś. Stąd wiedział mniej więcej co się stanie. Zamieszanie Hektora w to wszystko było spowodowane tym, że Skubny chciał się rozerwać kosztem prokuratora.

Patrycja dostała za zadanie dowiedzieć się kto ma znaleźć bombę. 

### Faza 2: Aresztowanie niewinnych kryminalistów

Dzień 2:

Skubny jest niezadowolony. Spalili mu kebaba. Zaprosił Hektora do restauracji Królewskiej. Poskarżył się Hektorowi, że udzielił uczciwego interesu i uczciwej pożyczki a tu mu ludzie Daczyna kebaba spalili. Poprosił Hektora, by ten porozmawiał z Daczynem - przecież nie może tak być, że uczciwemu biznesmenowi kebaba palą. Hektor powiedział, że nie jest Hektorem na posyłki, więc Skubny dał mu adres magazynu, gdzie znajdują się nieoclone papierosy i alkohol z przemytu należące do Daczyna. Jeśli faktycznie to tam jest, Hektor porozmawia z Daczynem.

Patrycja znalazła na YT coś zaskakującego. Znalazła jak to Kimaroj chwali się jednej ze swoich Ukrainek, że napuścił Daczyna na Skubnego a Skubnego na Daczyna i chwali się, jak to on będzie na samej górze i będzie nie "Skubny oraz Daczyn" ale "Kimaroj, potem Skubny i Daczyn". I nawet się nie zorientują.
...taaaak. Hektor docenił.

W tym czasie Alina poprosiła Annę Kajak, by ta spojrzała na sprawę. Kto to wyciągnął i dlaczego teraz.

Niejaki Tomasz Jamnik skontaktował się z Leonidasem by sprzedać mu wiadomość. Nie tylko Daczyn znalazł podsłuch; znalazł notatkę Skubnego odręcznie napisaną w której była dyrektywa "zdobyć Wyżerkę". A do tej pory Wyżerka była miejscem całkowicie neutralnym. Za to Leonidas sprzedał Jamnikowi, że Skubny wykradł bombę i ona wybuchła. Stracił ludzi i teraz obwinia sabotaż. Jamnik to kupił. A czemu Skubny chciał bombę? Spalili mu kebaba.

Hektor wziął na akcję Leonidasa i Klarę. Tam też Bór i ekipa. Jak weszli do środka, Hektor i Leonidas poszli na bok; Leo tłumaczył czemu kebab został spalony a Klara wykradła i wysadziła bombę w magazynie. Nikomu nic się nie stało, ale sprawiło to interesujący związek przyczynowo-skutkowy: Hektorowi Skubny powiedział o magazynie (gdzie faktycznie był alkohol i papierosy), potem wybuchło z powodu TEJ SAMEJ BOMBY. Skubny chciał widać pozbyć się dwóch pieczeni przy jednym ogniu...

Bór zabezpieczył jakieś dowody przeciwko Daczynowi mimo wszystko. Hektor uwierzył, że to plan Skubnego.

Hektor wysłał do Skubnego i do Daczyna zaproszenie na posterunek. Obaj przyszli z świetnymi adwokatami.
Wpierw Hektor wziął Skubnego do pokoju zwierzeń. Oskarżył go o bombę, ale Skubny jest niewinny. Skubny powiedział o trzech ciemnych sprawkach Daczyna i wyjaśnił, czemu "Wyżerka" jest miejscem bezpiecznym - miejsce neutralne. Do momentu aż Kimaroj zaczął działać na własną rękę.
Skubny powiedział, że ktoś próbuje go wrobić i policja musi zacząć działać. Hektor wsadził go do aresztu.

Hektor wsadził do aresztu z nim Klemensa.

Daczyna też posadził. Niestety, nie ma nic twardego na Skubnego - a Skubny ma dziennikarkę ;-).

### Faza 3: Triumf Batmaga

Dzień 3:
PIP, służba celna, sanepid - częściowo od Hektora, częściowo od "Batmana" - poszły siły do "Wyżerki". Spłoszony Kimaroj został wzięty z zaskoczenia. Nic się nie spodziewał. 

...i Kimaroj został zawezwany przed oblicze Hektora. 

Kimaroj sypnął wszystko. To nie on nagrał tamten filmik. Przekazał Hektorowi co, gdzie, kiedy, po co, wszystko co może. Kto z kim i kiedy. Ale na Daczyna coś jest; Skubny nie. Kimaroj nie ma żadnych ambicji, on nigdy nie chciał być kimś. Dał Hektorowi mnóstwo materiału na tępienie drobnicy wszelakiej, na linki z innymi grupami, niekoniecznie lokalnymi... 
...co de facto jest wyrokiem śmierci dla Kimaroja. Więc dostanie program ochrony świadków.

CZYLI FILMIK JEST PODROBIONY! GDZIEŚ TU JEST MAG!

Hektor już wie!

"Wyżerka" została zamknięta. Nielegalnie pracujący ludzie zostali przesłuchani i przesłani do odpowiednich służb lub deportowani. Dokładnie przesłuchano zwłaszcza damy lekkich obyczajów sypiających z bossami.
Kebab nie został odbudowany.

Pafnucy zapłacił za grzechy przeszłości; został obciążony przez część panienek. Za bombę siedzieć nie poszedł, ale dostał za przeszłość i zrujnowano mu karierę.


## Dark Future:

### Faza 1: Wina Skubnego

- Kimaroj poprosił o pomoc Igora Daczyna o rozwiązanie tego problemu; ktoś chce go wrobić.
... 

- Batmag podłożył ślady wskazujące na Szymona Skubnego
... 

- Igor Daczyn znalazł wskazania na Skubnego. Wysłał tam swoich żołnierzy i spalili mu kebaba.
...  

### Faza 2: 

- Skubny zdziwiony; kebaba mu spalili. No ok... to on pójdzie do Hektora.
...

- Batmag puścił w internecie informację jak to się przechwalał Kimaroj że Daczyna załatwi przy użyciu Skubnego.
... Vigilante.damage <someone’s> reputation and social standing using <something inappropriate>

- Kimaroj zaczął błagać i przepraszać. Daczyn zażądał przeprosin i zadośćuczynienia
...

- Skubny elegancko kieruje prokuratora do magazynu z nieoclonym dobrem Daczyna
...


### Faza 3: 

- Daczyn i Skubny się spotykają w luksusowej restauracji (najlepiej jak jeszcze jest tam Hektor)
...

- Urzędnicy PIP i sanepid wchodzą do Kimaroja i znajdują masę problemów i powodów zamknięcia restauracji.
...

- Batmag podkłada Kimarojowi dowody świadczące o tym, że i Daczyn i Skubny byli przez Kimaroja skłóceni.
...

### Faza 4: 
 
- Batmag informuje Daczyna o tym co zrobił Kimaroj
...

- Batmag ostrzega Hektora.
...

- Daczyn zabija Kimaroja i jest aresztowany przez policję.
...

# Streszczenie

Kimaroj prowadzi "bezpieczne miejsce" dla przestępców w restauracji Wyżerka. Skubny poprosił Hektora o pomoc w rozwiązaniu konfliktu między nim a Daczynem; nie chce wojny grup przestępczych. Hektor wsadził Skubnego i Daczyna do aresztu. Przyciśnięty Kimaroj się złamał i dał Hektorowi dowody na Daczyna i inne siły przestępcze w okolicy. Trafił do programu ochrony świadków. Za wszystkim stoi mag - Anna Kozak (o czym nikt nie wie) - która chce być Batmagiem i wrobiła Hektora w działanie.

# Zasługi

* mag: Leonidas Blakenbauer, siła napędowa wojny gangów, zdobywał informacje oraz siał zamęt.
* mag: Hektor Blakenbauer, jako on sam - prokurator, który NIE WIE, a jednak posadził wszystkich.
* mag: Klara Blakenbauer, zdobyła sygnaturę "Bat-* maga" i zdetonowała bombę w * magazynie Daczyna uruchamiając spiralę Hektorozemsty.
* vic: Alina Bednarz, podłożyła podsłuch tak, że wszyscy myśleli, że to podsłuch Skubnego.
* vic: Klemens X, siedzi w areszcie ze Skubnym i go pilnuje.
* mag: Anna Kozak jako Bat* mag, tajemnicza postać, która osiągnęła co chciała - "Wyżerka" została zamknięta rękami Hektora Blakenbauera.
* czł: Bogdan Kimaroj, ofiara losu (i Bat* maga), przepuszczony przez Hektora trafił do programu ochrony świadków.
* czł: Szymon Skubny, NAPRAWDĘ niewinny obywatel który trafił do aresztu. Za niewinność. Nawet podatki zapłacił.
* czł: Igor Daczyn, przestępca większego kalibru choć bez rękawiczek Skubnego. Tak. Hektor też go posadził.
* czł: Kleofas Bór, dzielnie szarżujący na * magazyn i dzielnie z niego uciekający przed wybuchem bomby.
* czł: Patrycja Krowiowska, która dostała zadanie niemożliwe. A przynajmniej w tym czasie. Zwłaszcza, że TOR network.
* czł: Tomasz Jamnik, który sprzedał bezużyteczną informację Leonidasowi, za co dostał informację fałszywą nakręcającą wojnę gangów.
* czł: Pafnucy Zieczar, odpowiedział za mroczną przeszłość po tym, jak wydały go Ukrainki Kimaroja (i sam Kimaroj).

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
               1. Powiat Kopaliński
                    1. Kopalin
                        1. Centrum
                            1. Prokuratura
                            1. Restauracja Królewska
                        1. Obrzeża
                            1. Rezydencja Blakenbauerów
            1. Powiat Czelimiński
                1. Czelimin
                    1. restauracja / motel Wyżerka 