---
layout: inwazja-konspekt
title: "Suma niedokończonych spraw..."
campaign: powrot-karradraela
players: kić, dust, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170519 - Odzyskać Aegis 0003 (SD, PS)](170519-odzyskac-aegis-0003.html)

### Chronologiczna

* [170607 - Oślepienie autowara (HB, KB)](170607-oslepienie-autowara.html)

## Kontekst ogólny sytuacji

Temat Infensy i Renaty nadal budzą zainteresowanie magów, którzy się na dwie główne kategorie - jedni chcą o tym zapomnieć, wymazać z historii i nazwać anomalią a drudzy chcą doprowadzić do zwarcia szyków i wykluczenia magów, którzy byli z tym powiązani - głównie KADEMu i Hektora.

Pojawiły się działania w kierunku na prokuraturę magów. Silną lobbystką jest Andżelika. Niestety, jednocześnie lobbuje za karaniem Marianny, przez co jedynie rośnie ruch izolacji gildii. Znowu mamy dwie siły. Na to nakłada się Infensa... chwilowo izolacja jest dominującą siłą.

Działania Alegretty wzbudziły zainteresowanie magów Mausożernych - takich, którzy chcieliby po Karradraelu najlepiej karać różnych Mausów. Łatwo dojdą po śladach do Maurycego Mausa a tam - do Blakenbauerów. Na szczęście, Judyta Maus jest przeszczęśliwa z odzyskania Maurycego i z aktualnego stanu Alegretty - Hektor wyraźnie wybił blisko Silurii w kategorii Prawdziwych Bohaterów Judyty...

Eis zaczyna się zastanawiać, czy pomysł z sojuszem był najlepszy, czy nie pójść starym podejściem typu "silny ma rację". Nadal się waha - nie wszyscy magowie KADEMu traktowali ją źle, ale może Hektor nie jest tym magiem, którego szukała...

Autowar Bzizma dał radę połączyć się z leylinami i niestabilnymi Węzłami głębinowymi. Nie dał jednak rady w pełni opanować sytuacji co doprowadziło do lekkiego Skażenia i lekkiego naruszenia Maskarady. Musi skierować swoją uwagę w tą stronę; musi odrobinę spowolnić swoje główne infestacyjne działania.

Autowar stracił dostęp do sensorów zaawansowanych oraz do podstaw ekonomii, jakkolwiek infestacja ekonomii częściowo zneutralizowała działania Blakenbauerów. Mimo, że jednostka biowojenna nadal kontroluje cały ten teren, wie już, że jest podgryzana przez kogoś, kto jest dla autowara niewidzialny. Jednocześnie próg Skażenia przekroczył bezpieczny poziom jak i poziom złamania Maskarady stał się wyraźny; Bzizma musi się tym powoli zacząć zajmować.

* Sad, w którym znajduje się Bzizma uległ poważnej transformacji
* Estrella wpadła w problemy polityczne; znowu wyciągnięto jej działanie przeciw Świecy na rzecz ludzi
* Postrzeganie magiczne jest utrudnione na całym terenie Mszanki. Skażenie manifestuje się halucynacjami magicznymi
* W Skażeniu na terenie Mszanki wszędzie obecna jest energia Judyty Maus. Skażenie i dywersja...
* Siły przemytników zaczną odgrywać znaczącą rolę
* Bzizma jest uszkodzony bardziej niż się wydaje. Przez brak prądu potrzebuje dostępu do brzoskwiń asap.

## Punkt zerowy:

Koncept:

* Maus, który nie przestał kochać Renaty i pragnie ją pomścić
* Próżnia po Karradraelu szukająca swojego uzupełnienia
* Kinglord, pragnący zdobyć miejsce Karradraela

**Pytania i odpowiedzi**

* Ż: Jaka bardzo ważna sprawa przed sądem dotycząca Skubnego sprawiła, że Hektor musiał przełożyć atak na Bzizmę?
* D: Ruszył proces Skubnego z bardzo silnymi dowodami i MUSIAŁ zadziałać osobiście.
* Ż: Dlaczego Olga bardzo chce ochronić Hektora przed tragedią?
* K: Bo Bzizma.
* Ż: Co sprawia, że najlepszym przekaźnikiem potęgi boskiej jest właśnie Piotr?
* F: Tworzy sztukę podrasowaną magicznie. Łatwo może spełniać marzenia pomniejszych, którzy zobaczą to co potrzebują...
* Ż: Czemu Piotr jest niezbędny by odnaleźć i uratować Siwieckiego?
* R: Biorąc pod uwagę co on zażywa, jest jednym z nielicznych zdolnych do zsynchronizowania się mentalnego z Siwieckim.

## Misja właściwa:

**Dzień 1:**

Akcja_init:

* Bzizma: Produkcja Rojów: +1
* Mausożercy: Propagowanie Judyty Maus jako zła młodzież: +1
* Przemytnicy: Przybycie mściwych magów zinsząd: +1

Rano (1/5):             +1 akcja

Marcelin powiedział Hektorowi o tym, że Judyta płacze. Janusz Jawor, eks-Maus ją szykanuje i mówi "raz Karradrael zawsze Karradrael ona chce być jak Renata" i mówi o tym domu gdzie była Alegretta i o tamtej sygnaturze z Mszanki. Ogólnie, kiepsko. Estrella została oddelegowana do tego zadania; powiedziała, że spróbuje coś z tym zrobić. Co prawda jest w kłopotach, ale się tym jakoś zajmie.

Jewgenij Zajcew skontaktował się z Klarą. Powiedział, że tam jest taki mało legalny składzik artefaktów. No i Iwan Groźny już by chciał je odzyskać. I ma wpaść po nie ze swoimi siłami. No i Leonidas zawsze Klarę polecał... no a tam jest np. Portal Illuminati. Wyłączony, ale... pod wpływem energii magicznych to może dupnąć. I otworzyć portal Illuminati. No bo Iwan chce czarną polewkę wysłać, nikt mu siostry nie będzie bałamucił...

Klara próbuje Jewgenija przekonać, że tam przecież jest źle. Że oni wiele nie zrobią. Że potrzebują wsparcia Jewgenija.

3v5->R. Jewgenij się zgodził. Mają wsparcie jego przemytników. Od jutra grupka niezależnych magów-przemytników pomoże.

KF: 7: Łatwiejsze plany przeciwika. Mausożercy mają zdecydowany postęp.         +1 akcja, +2 akcja Mausożerców "kampania antyMausowa"

Nad Ladybugiem pracuje Margaret i Marcelin z pomocą Zajcewów Vladleny.          +3 do ścieżki.

Hektor przekonał Edwina o tym, że warto zrobić klątwożyta. Ten klątwożyt ma odciąć autowara od posiłku. 10v10->F,R

* KF: 9: Koszty działań rosną. Estrelli będzie coraz trudniej blokować działania antyJudytowe.          +1 akcja
* KF: 1: Pojawia się ktoś niepożądany. Tien Mirosław Sowiński, który chce przejąć autowara dla Świecy   +1 akcja

Wynik:

* Klątwożyt zakłócający energetycznie   +2

Judyta jest bezużyteczna. Płacze w kącie.

Przedpołudnie (2/5):          +1 akcja

* Margaret zajęła się pracą nad zatrutymi brzoskwiniami. Nie jest to jej silna strona, ale jest w stanie zacząć. (+1 ścieżka)
* Hektor z Edwinem dalej pracują nad klątwożytem. Kolejny sukces.       +2
* Klara pracuje nad kodami autoryzacyjnymi. Estrella zna te kody. Pomoże Klarze.

Konflikt:

* Autowar: Baza (2), Autowar (4), Adaptacja (1), Indoktrynacja (-2), Uszkodzony (+1), Rozkojarzony (-2), Autonomiczny (2) = 6
* Klara: Podstawa (5), Estrella (+2), Infodump + specimen (2), Rezydencja (1), Intensywna praca (-2) = 8

Wynik: Sukces. +3 ścieżka Kody autoryzacyjne.

Estrella ostrzegła, że za tym wszystkim stoi jakiś Sowiński. Dokładniej: Tadeusz Sowiński. On robi kampanię eksterminacji Mausów. W zemście za Sabinę. Cóż, Blakenbauerowie uznali, że autowar ważniejszy.

Marcelin poszedł pocieszać Judytę. 

Konflikt:

* Depresja Judyty: baza (2), opiekuńcza + radosna (-1), chce się wykazać (-1), bardzo przygnębiona (2), jest pretekstem do tępienia Mausów (1) = 3
* Marcelin: podstawa(5)

5v3->S. Judyta jest odpowiednio pocieszona. I nawet nie skończyli w łóżku.

Popołudnie (3/5):          +1 akcja

* Hektor i Edwin kończą klątwożyt: F,R. Mamy klątwożyta! :D.

* KF: 16: Wzrost znaczenia miejsca X. Domek, w którym mieszkał Maurycy ma śledczych którzy badają działania Mausów.     +1 akcja, +1 tor 'kampania antyMausowa'
* KF: Oczy na Estrelli. Jest jej jeszcze trudniej i politycznie problematyczne.                                         +1 akcja

Klara próbuje ukryć emanacje brzoskwiń. Autowar z uszkodzonymi sensorami tu jest akurat biedny (3). Klara z lekką pomocą: 5v3->S. Brzoskwinie jako wektor klątwożyta zostały przygotowane...

TYMCZASEM PLANY SIĘ ZMIENIĄ! BO WCHODZI KINGLORD!

**Dzień 2, 3:**

Peter, the artist, was asleep after taking some drugs as usual. He was found by Olga in his sorry state. To ensure his cooperation she has managed to smuggle a bomb into his room. And she managed to wake Peter up. As Peter was not really cooperating with her, Olga pointed towards the bomb and noticed she is able to arrest him over that object. Peter was stupefied a bit; it was definitely not his bomb. He went the way of "I have rights and permission because I'm drawing that bomb", which actually left Olga stupefied.

Anyway, she asked him to draw the picture he was drawing at this time; the one showing the massacre. He didn't really want to, but she was quite convincing with the bomb and everything. So he did draw the most important part for her – the part were Hector and Marcelin were dead.

Olga decided she had enough and asked Peter to cooperate with Marcelin. The artist phoned Marcelin and told him there is that woman here who interrupts him and want something from him and Marcelin should be the one to talk with Olga. The special agent left Peter with disgust. Peter imbibed some more and returned to narcotic sleep. When Marcelin came to see what Peter was blubbering about, he noticed the bomb and one completely inanimate Peter. Marcelin marveled at the bomb,  decided it might be kind of tricky and decided to leave without waking Peter.

In a completely different place Henry is being tasked by Kinglord to devise a way to kill the prosecutor Hector. Henry devised a way containing the water sprinklers, some gasoline, shards of silver combined with this gasoline and lots of people with machetes willing to kill people running out of the building. 

===

Several hours later Clemens met with Daniel and Alina explaining that Hector is really endangered. Directors orders – there's going to be a very successful plot especially now while Hector really wants to be in the courthouse because he can finally catch Skubny. Daniel and Alina plotted that Hector will not make it on time - Skubny is presumed to be sick and as Hector is very stubborn, Alina will morph into a driver and the limousine Hector is driving will finally stop working in the middle of nowhere without reception.

This did not stop Kinglord. Henry got a second order – kill Hector in transit. Otherwise it will be… Unpleasant. Henry tried to devise a clever way but after a moment he decided that clever ways will fail. A force of about 50 demons shall be summoned and sent through portal to kill Hector. Kinglord decided that this is quite a nice plan and killed a single kindergarten to fuel the demons. Henry was unhappy.

===

Olga woke Peter up again. She injected him with some kind of... Thing and asked him how is Hector going to die now. She explained that he has a connection with the deity and what he is drawing is the future which is going to happen. Peter, of course, did not believe. Olga decided to pay him to ensure his cooperation. As Peter could feel the blood aura around Olga and as she was paying well, he has decided to go with it. And so Peter has grown the limousine ripped apart by demons and everybody dead.

Olga phoned Alina instantly. She wants her, that an army of mercenaries is going to come and they have to evacuate Hector ASAP. Alena knows better than to ignore all those warnings even if she doesn't understand how she knows some things; the car got magically and instantly repaired and they drove. Then a set of portals opened and the demons came out screeching for Hector's blood. Daniel took over the driver's seat and made a miraculous feats to buy some time; however the car got destroyed in the process. But it was enough for Hector to cast a elemental spell which created a stone barrier between them and the demons. And as Clara was aware of the problems, she has used the phase spider and safely evacuated the team into the mansion.

So everyone was safe and the only thing remaining is what the hell happened and why.

===

Olga asked Peter to DRAW a person responsible for the assaults. Peter has their own a small little cute looking girl. New version of Henry. Olga looked at it with incredulity – this is a mastermind responsible for trying to kill Hector? Well, she knew better than to ignore and she made sure the information got passed to the magi in the mansion.

Seeing the painting, Clara was able to recognize a part of it. It was a place she had known, because she had been there earlier – it was the Blutwurst castle. She remembered the particular's of that place after the Alexandria episode. 

A strike team was assembled – Clara, Mordred. They went close to the Castle and were supposed to extract the mastermind and bring her to the mansion for questioning. Alive. Maybe unpleasant. During one of the spells cast by the magi to infiltrate the castle they have managed to port themselves directly into the cellar – a safe cellar – which was a very positive occurence. Little did they know that it happened with Iliusitius interference who brought there Peter.

So, we have Peter. He is completely intoxicated in some kind of torpor and bliss. And he wakes up in the cellar as a trap. A trap sprung by Clara and Mordred - to lure the mastermind or at least Karina (whom Clara knows is here). It almost worked – Karina went down, saw Peter and brought him up with her to talk about art and vampires.

In the meantime Henry knows something wrong is happening here. He got saved by Karina from the dangerous Kinglord. Karina simply wanted to have a toy, a small little girl she could play with, dress and infect with vampirism. So Henry did everything Karina ordered him to up to this very point...

Henry knows someone is coming. And he doesn't know the intentions of that particular someone. What he does in the girls body is – smash his face repeatedly on the ground and try to tie his body up. To make sure she is recognized as a victim not as the perpetrator.

It didn't work on those particular magi who came. But they were amused.

Having extracted the mastermind from the mansion into the cellar they have evacuated - and Iliusitius returned Peter to his rightful place leaving Karina stranded and bewildered.

**Podsumowanie torów**

-

**Interpretacja torów:**

-

# Progresja

* Henryk Siwiecki: można do niego mówić "Alicjo". Zmieniony przez Kinglorda w dziewczynkę.
* Judyta Maus: uważa Marcelina Blakenbauera za swojego prawdziwego przyjaciela.
* Marcelin Blakenbauer: ma oddaną przyjaciółkę w Judycie Maus.

# Streszczenie

The operation of taking over the autowar has managed to make sure Bzizma is weakened and there exists an access to the core of the autowar. However, the forces of Kinglord have decided to inexplicably kill Hector. As a reaction to that, Iliusitius has sent Olga to make sure Hector will survive. In the process a mage artist, Peter, has been used as a conductor between the plans of the Kinglord and intelligence of the Blakenbauers.

# Zasługi

* mag: Piotr Kit, wieczny pijak, dobrze się bawił, zarobił sporo kasy od Olgi i najlepiej na tym wyszedł. Olga mu kupiła alkohol... A nic nie chciał robić... no i kilka obrazów. Ma wizje które się sprawdzają.
* mag: Klara Blakenbauer, przekonała Mordreda żeby ruszył temat. Inwigilacja piwnicy.
* mag: Mordred Blakenbauer, inwigilator posiadłości Blutwurstów. Ma oczy w całej posiadłości. Uwierzył Siwieckiemu jako dziewczynce, że ten był kontrolowany.
* mag: Hektor Blakenbauer, nie dał się zabić, co jest sporym osiągnięciem. Nie dotarł na proces Skubnego. Stoczył walkę z demonami i heroicznie wycofał swoje wsparcie.
* mag: Henryk Siwiecki, z rozkazów Kinglorda miał zaplanować śmierć Hektora. Nie wyszło, skończył jako słodka dziewczynka w służbie Kariny von Blutwurst. Świetny w maskowaniu. Zdobył krew Kariny i wpadł w łapy Mordreda.
* vic: Dionizy Kret, broniła Hektora zacięcie przed demonami, opracowywał plan obrony i mistrzowsko ewakuował się autem.
* vic: Alina Bednarz, broniła Hektora zacięcie przed demonami, opracowywała plan obrony i wysadzała demony.
* czł: Olga Miodownik, z rozkazu Iliusitiusa chroniła tyłek Hektora przez wymuszanie na Piotrze malowania obrazów. Chce się powiesić przez interakcję z tym magiem.
* vic: Karina von Blutwurst, wzmocniona i po stronie Kinglorda; ubłagała Kinglorda, by oddał jej Henryka do zabawy.
* mag: Marcelin Blakenbauer, ukochany syn tatusia (Ottona), przekonał go, by Otton znalazł informacje o autowarze dla Blakenbauerów. Też: twórca toksyn przeciw autowarowi. Pocieszył Judytę Maus.
* mag: Jewgenij Zajcew, który obiecał wsparcie swoich przemytników, bo w okolicach Bzizmy jest bardzo cenny Skażający artefakt.
* mag: Margaret Blakenbauer, odwracająca uwagę Bzizmy i os Bzizmy "skażonymi" osami z krwią Judyty Maus, by Bzizma szukał Karradraela a nie ataku...
* mag: Otton Blakenbauer, który łącząc się z Rezydencją ściągnął informacje na temat Bzizmy na prośbę Marcelina. Okazuje się, że ma słabość do brzoskwiń.
* mag: Estrella Diakon, przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.
* vic: Bzizma Stlitlitlix, połączył się z większością leylinów pod Mszanką, ale stracił zaawansowane sensory. Wie już, że nie widzi swojego przeciwnika...
* mag: Judyta Maus, której sadownicze umiejętności i magia pogodowa okazały się kluczowe do zniszczenia sadu z żywnością dla Bzizmy. Jej aura jest wszędzie w Mszance.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
        1. Małopolska 
            1. Powiat Okólny
                1. Okólnicz
                    1. Pierwogród
                        1. Pałac Blutwurstów

# Czas

* Dni: 3


# Waśnie

-

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
