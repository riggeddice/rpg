---
layout: inwazja-konspekt
title:  "Śladami aptoforma"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161005 - Szmuglowanie artefaktów... (temp)](161005-szmuglowanie-artefaktow.html)

### Chronologiczna

* [150408 - Rykoszet zimnej wojny (HB, DK, AB)](150408-rykoszet-zimnej-wojny.html)

# Wątki

- Projekt "Moriath"
- Tymotheus Blakenbauer
- Arazille, królowa niewolników

## Punkt zerowy:

Ż: Skąd pojawiła się informacja, że jakiś ślad znajduje się w pokoju Mojry?
K: Źródłem wiedzy jest Borys Kumin (niespodzianka).
Ż: Dlaczego Dionizy i Alina mogą polegać tylko na grupie śledczej, nie na Blakenbauerach?
D: Marcelin to Marcelin, Edwin i Margaret odcięli się w biolabie, Otton jest poza dyskusjami, Hektor niekoniecznie podjąłby słuszną decyzję.
K->Ż: ?
Ż: ?
D->Ż: W jaki sposób Zespół są w stanie przyblokować zdolności końcówek aptoforma by nie dać się zdominować?
Ż: Neuronull dostarczony przez Edwina. Lub ewentualnie coś, by stracić słuch.

## Misja właściwa:

W Rezydencji Blakenbauerów. Dionizy i Alina. Wiadomo, że aptoform może mieć kilka końcówek a Blakenbauerowie mają już 3. Wiadomo, że końcówki służą do nauki aptoforma i wiadomo, że gdzieś w tle znajduje się mag, który ma coś wspólnego z aptoformem (i uważa, że to on ma kontrolę). Wiadomo też, że Margaret i Edwin COŚ wiedzą a Hektor nie wie nic... i najpewniej Arazille jest zainteresowana aptoformem.

Ż: Kto jest ulubionym Blakenbauerem Borysa?
D: Otton - twarda ręka rodu.
Ż: Co Borys chce od Dionizego?
K: Alinę.

Do Dionizego przyszedł Borys, powiedzieć o tym, że znalazł pewien dowód w sprawie aptoforma. Ale chce za to coś od Dionizego. Chce, by Dionizy zamontował kamerkę pod prysznicem Aliny (AcuityHeart_direct)(porażka). Dionizy poszedł na tą ugodę. W zamian za to, Borys powiedział co znalazł: Mojra w pokoju ma dziennik, w którym są różne dokumenty; min dotyczące aptoforma. Dionizy wyczuł, że Borys nie mówi wszystkiego, więc on wszedł twardo (AcuityHeart_direct)(wygrana). Borys się wycofał i powiedział wszystko; to nie jest dziennik MOJRY, to zbiór danych przygotowanych przez Mojrę by przekazać je jakimś ludziom; jakiemuś człowiekowi. Dane są zebrane przez Klemensa, by szpiegować Blakenbauerów dla Mojry w tej kwestii.
Borys poprosił, by nie wyszło, że to on powiedział. Boi się Klemensa. Dionizy się zgodził.

Dionizy zdecydował się wywiązać z umowy (AcuBod_AtkSubtle Dionizego vs AcuHrt_DefSubtle Aliny) (6#1v8->10): udało się. Dionizy dostarczył Borysowi to, co Borys chciał otrzymać: filmik, jak Alina kąpie się pod prysznicem.

Borys zgodnie z umową dostarczył Dionizemu dziennik.
D->Ż: Warunki w jakich będzie znajdował się rdzeń aptoforma.
Ż: Rdzeń jest około 1.5-metrowym insektem w kształcie chrząszczopodobnym, bardzo źle reagującym na światło i wymagającym odpowiedniej wilgoci oraz składu chemicznego. Rdzeń aptoforma zajeżdża ściekami i nie nadaje się do trzymania w obiekcie poruszającym się (na dłuższą metę). Spaliny samochodowe są dla niego bardzo trujące, co powoduje wykluczenie niektórych lokalizacji. Ogólnie miejsca powiązane z chemikaliami brzmią dobrze (jakieś zakłady chemiczne). Aptoform kontroluje końcówki do 10 km.

Czyli mamy zarówno lokalizację: Kopalin jak i Myszeczkowo. Na bazie grupy potencjalnych lokalizacji da się zawęzić wszystkie potencjalne miejsca do "sensownych" lokalizacji.

K->Ż: Bonusy do walki przeciwko aptoformowi.
Ż: #2.
Ż: Wymyślcie po jednej słabości aptoforma.
K: Odpowiednie granaty dymne ze "spalinami" (odpowiednie środki) by dusić cholerstwo i je osłabiać (nie zabić).
D: Odpowiednie feromony do wciśnięcia do granatów, by wymusić ucieczkę cholernych końcówek.

Alina na bazie posiadanych informacji indukuje: stara, opuszczona już oczyszczalna ścieków należąca do zakładów produkcji nawozów. Lokalizacja: Piróg Dolny. Ta lokalizacja pasuje szczególnie, ponieważ jest w zasięgu, spełnia warunki i w okolicy w ogóle nie działała żadna końcówka aptoforma. Alina dostała też plany kompleksu.
Właścielem terenu jest Dariusz Larent. On ma klucze do tego terenu. Nawiązał umowę z firmą ochroniarską "Maczeta".

Ż: Jaka jest linia łącząca Paulinę i aspiranta terminusa Kaspiana Bankierza.
D: Paulina na prośbę Powiewu pomagała Kaspianowi (wylizać go po akcji).

Na noc, Dionizy i Alina zdecydowali się podejść do akcji. Furgonetka opancerzona, odpowiednio przygotowane granaty, transporter na aptoforma, maski gazowe i noktowizory, paralizatory... wszystko gotowe. W nocy ochroniarze śpią. A Alina zwinęła jeden miotacz płomieni...
Przygotowali też coś, dzięki czemu można wysadzić oczyszczalnię - dzięki temu mag nie będzie wiedział, że aptoform przeżył. A potem wywieźć aptoforma, by przekroczyć 10 km by końcówki padły... same zalety.

Zanim zapadła noc, Alina spotkała się z szefem Maczety. "Jest zainteresowana wynajęciem usług" i wypytuje o ofertę, możliwości itp. Jako bogata damulka. Próbując go przekonać (HrtBod_sub 6v6)(remis) udało jej się zdobyć materiał genetyczny by się weń transformować, ale poniosła pewne koszty zaliczki (nie bardzo duże) w tym procesie. Biedne finanse Blakenbauerów. Nie zauważą (Hektor tak, bo on to widzi...).

Noc. Alina w formie szefa Maczety i Dionizy w formie Dionizego. Podeszli do obu ochroniarzy (którzy są nieco przerażeni tą niespodziewaną inspekcją), Alina ich opieprza i wykryła (AcuBod_sub_def) kamerkę ukrytą. To zmienia plan Aliny. Przepędza ich do obchodów po terenie... kamerki są w trybie streamingu i one NIE POWINNY tu być. To komplikuje akcję. Są liczne blind spoty, ale najpewniej wykryją, że ochroniarze zniknęli.
Niedobrze.
Dodatkowo podczas obchodu okazało się, że na szosie, niedaleko, częściowo ukryty znajduje się... karawan.
...co...

Dionizy krzyczy na biednych ochroniarzy, którzy nie zauważyli karawanu. Alina rozglądając się (AcuBod_sub_atk)(5v4->8) zauważyła ślady: 3 osoby, zmierzające na teren oczyszczalni. I ochroniarze ich nie wyłapali...
Dionizy postanowił zrobić im opiernicz generalny, nie mówić im o tym wszystkim. Alina zdecydowała się na pojechanie ich jak pies (WilHrt_dir_atk) i kazała im opuścić teren bo do niczego się nie nadają; przyjedzie inna ekipa. W ramach upokorzenia ich kazała im zgłosić się następnego dnia w bazie i zgłosić się do bezpośredniego przełożonego ze wszystkimi szczegółami co spieprzyli. OKRUCIEŃSTWO! (11v6->autowin).

Oki. Noc, opuszczona oczyszczalnia, trzy nieznane osoby w środku, jeden karawan, potencjalnie aptoform, dziwne kamerki... Dionizy i Alina.

Dionizy włamał się do kamerek (AcuWil) z pomocą Aliny. Zauważył: 4 mężczyzn; wyglądają jakby wytwarzali narkotyki i jeden z nich monitoruje kamerki. W oczyszczalni znajduje się też ksiądz (potencjalnie egzorcysta) i dwie osoby cywilne. Jest tam jeszcze jedna osoba; też widoczna przez kamerę. Stoi gdzieś powyżej, na jakiejś metalowej żerdzi. Obserwuje księdza.

Dionizy zdecydował się na rozłożenie kilku dywersyjnych ładunków wybuchowych i podłożenie trackera do karawanu. Dionizy chce zlokalizować gdzie ów karawan odjedzie. I udało się to bez problemu. W karawanie jest tracker i podsłuch.

Ok. W czasie, w którym u góry coś się dzieje Alina i Dionizy zdecydowali się na przejście po podziemiach w poszukiwaniu aptoforma. Faktycznie, klimat w podziemiach pasuje na aptoforma. Weszli innym wejściem bazując na posiadanym planie. Alina jest słabym punktem poruszania się w tych podziemiach (BodWil_dir_def)(7v6->sukces). I w ten sposób Dionizy i Alina wypadli na aptoforma tak, by chrząszcz ich nie zauważył.

Tymczasem u góry zaczęło się robić ciekawie. Ksiądz i jego dwóch agentów zostali otoczeni przez dilerów z nożami. Ksiądz zaczął się modlić i wzywać potęgę bożą. Arazille (która była prawdziwą siłą za księdzem Pimczakiem) wpłynęła na dilerów i zaczęła ich konwertować...
Widząc to i mając pod ręką aptoforma Alina zdecydowała się na test taktyki (AcuWil_dir_atk) i wygrała. Wyczekali, aż kształt z żerdzi zeskoczył na dół. To wampir, kiedyś Diakonka (Crystal Shard, agentka Tymotheusa Blakenbauera). Zaatakowała księdza, ale zasłonił go diler i zginął z rozszarpanym gardłem. Arazille Dotknęła Crystal i wampirzyca zaczęła się wić... walka woli i surowców. A Tymotheusa nie ma w pobliżu by przechylić szalę, gdyż przez działania Pauliny i aptoforma jest w zupełnie innym miejscu, wyprowadzając w pole magów Powiewu...

Korzystając z klinczu i zamieszania Dionizy rzuca aptoformowi granat przygotowany wcześniej, trujący dla aptoforma. Potem z trudem (10v9 BodWil_dir_def) wyprowadzają aptoforma do samochodu odpalając ładunki jako dywersję ale NIE niszcząc kompleksu (czyli nie zabijając ludzi w środku i nie uwalniając Shard). Po czym tak szybko jak to tylko możliwe Dionizy i Alina uciekają do Rezydencji Blakenbauerów z uprowadzonym aptoformem (z powodzeniem).

Tymczasem Arazille wygrała ze zbudowanymi nakazami i zakazami przez Tymotheusa i opętała Crystal Shard. Nie ma aptoforma (źle), ale ma doskonałe ciało, stworzone do walki a co najważniejsze, ciało maga (czyli Arazille może rozwinąć to w coś bliższego swoim celom). Co prawda Shard jest bardzo niekompatybilna z Arazille (Arazille żywi się tylko przez marzenia, Shard przez cierpienie), ale w perspektywie czasu bogini da radę przebudować nieszczęsną czarodziejkę, która stała się wampirem.

Jedynym całkowicie przegranym jest Tymotheus Blakenbauer. Stracił zarówno aptoforma jak i Shard.

Dionizy poznał lokalizację bazy Arazille (tracker na karawanie).

# Streszczenie

Borys odkrył lokalizację rdzenia aptoforma i posłano na to Siły Specjalne Hektora. W nieaktywnej oczyszczalni ścieków. Siły specjalne weszły podczas, gdy dwie siły tam zaczęły walczyć - Arazille (bogini marzeń) oraz ochroniarze aptoforma. W wyniku Siły Specjalne porwały końcówkę aptoforma a Arazille inkarnowała się w ciele "wampirzycy", bardzo zmienionej Diakonki i niewolnicy Tymotheusa. Jedynym całkowicie przegranym jest Tymotheus Blakenbauer. Stracił zarówno aptoforma jak i Shard.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                1. Piróg Dolny
                    1. Obrzeża
                        1. Zalew piroski
                        1. Zakład produkcji nawozów Chegolent
                        1. Nieaktywna oczyszczalnia ścieków

# Zasługi

* vic: Dionizy Kret jako kolaborant z Borysem na cnotę Aliny, noszący nieprzytomnego aptoforma w podziemiach dywersant o ciężkiej pięści hackujący strategicznie kamery.
* vic: Alina Bednarz jako śledcza jednostka terroru usuwająca zbędnych ochroniarzy; wytrzymała straszną aurę w podziemiach. Ukradła aptoforma z Dionizym od Tymotheusa.
* vic: Aptoform Mirasilaler, wyjątkowo - niewinna ofiara porwana przez Dionizego i Alinę od Tymotheusa. Chciał wolności a trafił do Czarnego Laboratorium w Rezydencji Blakenbauerów...
* czł: Borys Kumin, który dla oglądania nagiego kobiecego ciała jest skłonny sprzedać innych agentów Blakenbauerów. Szczęśliwie relatywnie niegroźny, choć obleśny.
* czł: Dariusz Larent, właściciel terenu na którym znajduje się opuszczona oczyszczalnia ścieków w Pirogu Dolnym. Na pewno bogaty.
* vic: Arazille, która chciała inkarnować się w aptoformie lecz przez machinacje Zespołu nie dała rady; musiała zadowolić się ciałem Crystal Shard.
* vic: Crystal Shard, wampirzyca (kiedyś Diakonka) i niewolnica Tymotheusa Blakenbauera; uwolniona przez Arazille stała się ciałem bogini marzeń.
* czł: Józef Pimczak, ksiądz w służbie Arazille, który miał doprowadzić do nawrócenia aptoforma. Zamiast tego doprowadził do inkarnacji Arazille w wampirzycy.
* czł: Emil Maczeta, szef agencji ochroniarskiej "Maczeta", który dał Alinie próbkę materiału genetycznego za niezbyt wielką sumę (nieświadomy tej sytuacji).
* mag: Mojra, nadal nieprzytomna i unieszkodliwiona; współpracowała z Klemensem by unieszkodliwić aptoforma zanim napotkała umysł Arazille.