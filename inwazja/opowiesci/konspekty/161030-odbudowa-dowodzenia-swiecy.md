---
layout: inwazja-konspekt
title:  "Odbudowa dowodzenia Świecy"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160724 - Portal do EAM (SD)](160724-portal-do-eam.html)

### Chronologiczna

* [160424 - Uważaj, o czym marzysz... (AW, SD)](160424-uwazaj-o-czym-marzysz.html)

## Punkt zerowy:
## Kontekst ogólny sytuacji

Andrea de facto została "wysłana" na zewnątrz i skończyła w dziurze. Współpracując z siłami Millennium, Andrea zdążyła ostrzec siły Świecy, że coś się stanie. Nie przewidziała jednak - NIKT nie przewidział - tego co nastąpiło.

- Aleksander Sowiński, przełożony Andrei, rozerwał połączenie Świecy Daemonica z Kopalinem i zginął
- Ozydiusz Bankierz, Sabina Sowińska i kilku innych magów zginęło; podejrzana (niesłusznie) jest Amanda Diakon (szczęśliwie, Andrea zna prawdę)
- Hipernet zaczyna się rozsypywać

Nagle, sytuacja Andrei jest interesująca - ma Mieszka Bankierza, Tadeusza Barana, skrzydło szkoleniowe (3 niedoświadczonych # doświadczony) oraz okoliczne siły i kontakty. Szczęśliwie, ma też wędrownego terminusa - Kajetana Weinera. Niestety, podobno w Czeliminie jest Krwawa Kuźnia...


## Misja właściwa:

Dzień 1:

Szok hipernetowy. Andrea odczuła go mocniej; ma zaawansowane połączenia hipernetowe, więc szok był tym większy. A zwłaszcza, że jej bezpośredni przełożony zginął - i poczuła to. Poczuła też death spell; zasada martwej ręki. Sowiński przed śmiercią włączył kaskadę samozniszczenia swoich agentów. Andrea WIEDZIAŁA, że to jest możliwe. Ale tego nikt NIGDY nie robił. Nigdy...

Dzień 2:

Andrea czuje się BARDZO źle. Jest przy niej Lidia Weiner, neuronautka. Wygląda na wyczerpaną. Jest tam też Rafael Diakon, również wykończony...
Andrea dostaje komunikat po hipernecie; to 'ghost' komunikatu Agresta.

"Jeżeli to czytasz, uruchomiłem procedurę wskrzeszenia. Podczas twojej ostatniej wizyty zaszczepiłem u ciebie autonomiczny system rejestrująco-kontrolny, mający przejąć dowodzenie w wypadku, w którym zostaniesz zabita. System będzie istniał 12 godzin. Tyle powinno wystarczyć, by przekazać mi informacje i dokończyć twoje sprawy. Jeśli to czytasz, system nie mógł się ze mną skontaktować, więc wydał w twoim imieniu różne polecenia i zginął."

Andrea ma uczucie, jakby nie wszystko pamiętała; zniszczenia dotknęły jej pamięci, ale nie tożsamości. Mechanizm Agresta w jakiś sposób ją uratował...

Lidia powiedziała Andrei, że odcięła ją od hipernetu. Zrobiła krótką diagnostykę; Andrea funkcjonuje prawidłowo. Rafael powiedział, że wpadł tu z Millennium, sprowadzony przez Andreę i że ona właśnie powiedziała co Rafael i Lidia mają zrobić. Lidia powiedziała, że niestety wzór Andrei jest uszkodzony i się rozpada; Andrea musi pić krew magów, a najlepiej magów Świecy, żeby przeżyć. Potrzebne jej Laboratorium Mgieł by ją naprawić.

Rafael powiedział, że Mieszko Bankierz wykrył w Andrei death spell Wydziału Wewnętrznego i chciał dokończyć dzieła; ale Kajetan go powstrzymał i zamknął w szafie. Dowodzi Tadeusz Baran. Andrea się podłamała. Rafael jej obiecał, że jak się obudzi, Tadeusz będzie sprawnym i kompetentnym dowódcą. Andrei się to bardzo nie spodobało, ale nie ma wyjścia - Lidia musi nad nią jeszcze trochę popracować...

Późny wieczór. Andrea otwiera oczy. Wszystko znowu pamięta. Widzi białą jak ściana Lidię. Lidia powiedziała, że ma w domku systemy zabezpieczeń. Gdy Lidia pracowała nad Andreą, jej dom (i nie tylko) został zaatakowany - wszyscy magowie zostali porwani. Napastnicy byli Spustoszeni.
Andrea ma ogromne oczy. Nie wie co się dzieje...

Komunikat od Agresta do Andrei. Andrea aż się wyprostowała.

"Żyję" - Andrea, cała obolała i wykończona
"Nie musisz mi dziękować" - Agrest, ze swoim standardowym chłodem
"..." - Andrea, orientująca się, że żyje TYLKO dlatego, że system nie mógł się połączyć z Agrestem
"Jest trzech magów wydziału wewnętrznego, którzy to przeżyli w całej okolicy Kopalina" - Agrest
"..." - Andrea, uzmysławiająca sobie konsekwencje zniszczeń

Andrea powiedziała Agrestowi co wie. Wszystko o Krwawej Kuźni, wszystko o danych okolicznych. Ten odpowiedział, że Millennium powinno być w stanie przywołać Arazille - niech zatem Arazille będzie przywołana w Krwawej Kuźni.
Agrest wysłał Andrei informacje odnośnie power suita Wiktora Sowińskiego.
Agrest też promował Andreę do stopnia Lady Terminus Kopalina. Dzięki temu Andrea jest w stanie podejmować wiążące decyzje i negocjować z Millennium.
Powiedział też, że Świeca Daemonica jest w ruinie. Dostali strasznie niszczycielską bombę magiczną; generatory są niesprawne. Agrest powiedział, jak stara to technologia...
Andrea zaproponowała Agrestowi, by - korzystając z Fazy Daemonica - fizycznie spotkać się z KADEMem (który chwilowo jest w Fazie Daemonica). Agrest powiedział, że spróbuje.

Lidia powiedziała Andrei, że ta powinna iść spać. Ta się nie zgodziła. Jeszcze nie. Wpierw - obchód. 
Andrea spotkała się wpierw z Kajetanem i - zgodnie z Porozumieniem Radomskim - zwerbowała go tymczasowo jako agenta Lady Terminus Kopalina.

"Brakuje tu tylko Zajcewa. Zajcew opowiedziałby historię, jak to zdrajca w Świecy próbował zabić Lady Terminus przejmującą spadek po Ozydiuszu, lecz nie dał rady. Potem szlachetny acz zagubiony terminus chciał dokończyć dzieła i wędrowny terminus uratował Lady Terminus. Mandoliną. Direct hit." - Kajetan, zaskoczony obrotem sytuacji.

Tadeusz Baran - ożywiony i z większą energią - powiedział Andrei, że wysłał magów Skrzydła Szkoleniowego by dofortyfikowali budynek gdzie oni się znajdują. On sam interfejsował z Biostanowiskiem i znalazł kilka ścieżek ewakuacyjnych. Znalazł też ścieżkę, która nie istnieje - zaczyna taką składać i budować, przy założeniu, że przeciwnik wie wszystko. Zgodnie z rozkazami Andrei (których Andrea nie wydała) pozwolił też Rafaelowi Diakonowi na wejście w interakcję z biostanowiskiem.

Baran zdał Andrei dokładne sprawozdanie z tego co robił; sprawozdanie JEST spójne i sensowne. Szokowa różnica. Nie chce powiedzieć Andrei co się zmieniło; lekko spiekł raka. Ale ściąga manuale tien Anety Rainer i robi co może.
Andrea wzięła Kajetana Weinera i poszła do szafy, gdzie znajduje się biedny Mieszko Bankierz. Związany i zakneblowany. Gdy on zobaczył Andreę - i jej aurę - to od razu otworzyły mu się oczy. Zobaczył, że Andrea jest Lady Terminus. Zaczął przepraszać i mówić, że w sytuacji jaką zastał to była jedyna sensowna opcja. Andrea wie, że Mieszko jest lojalny...
Poprosiła Kajetana, by ten rozwiązał Mieszka. Done. Ale załamał się, gdy dowiedział się, że Baran dowodzi bazą...

Rafael pracuje z biostanowiskiem; wzmocnił to trochę. Nie jest to tanie, ale w tej sytuacji każda przewaga może uratować życie. Andrea przedstawiła się biostanowisku jako Lady Terminus. Rafael wyjaśnił jej, że Tadeusz Baran się w niej zakochał na zabój; dodatkowo, to go niestety spala. Rafael estymuje, że w ciągu 2 lat Tadeusz umrze jeśli Rafael tego nie wyłączy.

Rafael też powiedział Andrei o tym, że niedaleko jest portal do EAM. Stary i niesprawny, jeszcze z czasów, jak EAM było zlokalizowane na Primusie a nie na Fazie Daemonica. Bianka potrafiłaby go przepiąć na Fazę; ale jej nie ma. Oni są w stanie (przy odrobinie szczęścia) przepiąć to tak, by Millennium mogło przesłać im jakieś siły, materiały, wsparcie. Pomysł jest ciekawy z perspektywy Andrei.

Rafael powiedział ze smutkiem, że stracili Fortitię. Stracił z nią kontakt. Nie wie co się stało - Andrea wie, to te 'roaming squads' o których mówiła Lidia...
Andrea powiedziała, że jej priorytetem jest ustabilizowanie całej tej sytuacji.
Rafael powiedział Andrei, że on też odda się pod dowodzenie Andrei.

# Progresja

* Andrea Wilgacz: została tymczasową Lady Terminus Kopalina
* Andrea Wilgacz: musi pić krew maga Świecy, by przeżyć
* Tadeusz Baran: stymulowany i zakochany w Andrei; kompetentny jak kiedyś

# Streszczenie

Wraz ze śmiercią Aleksandra Sowińskiego poszedł deathspell - uderzył w magów podległych Wydziałowi Wewnętrznemu w Kopalinie. Andrea jednak jest "kretem" Agresta - uruchomił się w niej defensywny system Agresta, który zmusił ją do przeżycia. "Andrea" ściągnęła Rafaela Diakona i Lidię Weiner, by ją "wskrzesili". Agrest wyjaśnił jej sytuację i promował Andreę do Lady Terminus Kopalina. Andrea odbudowuje swoje siły w Świecy i okolice Piroga. Siły Spustoszenia polują na magów w okolicy. Świeca jest w ruinie.

# Zasługi

* mag: Andrea Wilgacz, "wskrzeszona" przez program Agresta, tymczasowa Lady Terminus Kopalina. Odbudowuje łańcuch dowodzenia w Świecy.
* mag: Lidia Weiner, neuronautka ściągnięta by uratować Andreę przed deathspellem. Przy okazji uratowało ją to od porwania.
* mag: Rafael Diakon, zawadiacki lekarz i lifeshaper z EAM. Całkowicie amoralny, przedkłada wyniki nad wszystko inne. Uroczy.
* mag: Tadeusz Baran, dostał zastrzyk kompetencji i dowodzi placówką w Pirogu. A czemu? Bo Rafael zakochał go w Andrei (i stymulował).
* mag: Marian Agrest, zimny mastermind przedkładający dane nad życie Andrei. Ustanowił ją Lady Terminus i przekazał informacje.
* mag: Kajetan Weiner, uratował Andreę załatwiając Mieszka mandoliną. Oddał się pod dowództwo Andrei.
* mag: Mieszko Bankierz, chciał dobić zdrajcę... a to się okazało, że Andrea jest Lady Terminus Kopalina. Przysiągł wierność.
* mag: Fortitia Diakon, porwana przez Spustoszenie; Rafael się o nią bardzo martwi - i słusznie.

# Lokalizacje

1. Świat
   1. Primus
       1. Śląsk
           1. Powiat Kopaliński
               1. Piróg Górny
                   1. Obrzeża
                       1. Skład budowlany Żuczek, fortyfikowany przez młodych magów skrzydła szkoleniowego by nie panikowali
                           1. Ukryta kwatera Świecy, zdecydowanie za ciasna na ilość magów która się tu znajduje
                               1. Biostanowisko dowodzenia, podrasowane przez Rafaela Diakona
                   1. Las Stu Kotów
                       1. Martwy Portal, który kiedyś służył EAM a teraz nie służy absolutnie nikomu

# Skrypt


# Czas

* Dni: 2