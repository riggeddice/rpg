---
layout: inwazja-konspekt
title:  "Egzorcyzmy w Żonkiborze"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161103 - Egzorcyzmy w Żonkiborze (HS, temp)](161103-egzorcyzmy-w-zonkiborze.html)

### Chronologiczna

* [161018 - Ballada o duszy ognistej (HS)](161018-ballada-o-duszy-ognistej.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

Ż: Z czego słynie pobliska miejscowość?
R: Żonkibór, z Festiwalu Marzeń.
Ż: Dlaczego Henryk został poproszony o pojawienie się w Żonkiborze (objawy)?
R: Został wezwany jako ksiądz pomocniczy by poradzić sobie z egzorcyzmem.

## Misja właściwa:

Dzień 1:

Cztery dni później. Leje jak z cebra, gdy dotarłeś na parafię w Żonkiborze. Henryk wyczuwa energię podobną do syberionu... co jest niezbyt prawdopodobne.
Ksiądz go zaprosił do parafii i przywitał. BEZECNE ZACHOWANIA!

"To było spokojne miejsce! Nic bezecnego się tu nie działo... odkąd mój poprzednik uciekł z modelką"

Ksiądz Filip nie chce wszystkiego powiedzieć, ale bredzi coś o mafii erotycznej. Wikarego uwiodły... jego wikarego. Szczepana. Henryk zdecydował się dowiedzieć wszystkiego na ten temat; o co w ogóle tu chodzi? Filip ryknął, że jego też uwiodły.

"Wybiczuj mnie, ojcze!" - Filip
"Cierp synu, bo zgrzeszyłeś!" - Henryk, biczując

Niestety, Filip reaguje... nietypowo na biczowanie. Pragnie tego mocniej. Coś... coś jest nie tak.
Henryk powiedział, że Filip jest pod wpływem diabła. Będzie egzorcyzmował!
...i tak, bez użycia magii udało się go wyegzorcyzmować.
...to był trójkącik. Z wikarym i jeszcze jedną panną, której nie znał...

Dzień 2:

Ksiądz Filip jest już w dobrej formie. Załamany lekko. I bardzo skruszony, choć nie ma w nim niezdrowej żarliwości.
Ksiądz Filip powiedział, że Festiwal w Żonkiborze jest wieczny. Część z tych dziewczyn przybyła z zewnątrz... część jest lokalna. Na pewno rozpoznał Jessikę, swoją parafiankę. Jak to się stało? Przyszły na mszę, zostały po mszy i zaczęły się rozbierać i kusić, nie dało się ich wypchnąć...

Henryk zdecydował się poczekać na mszę (godzinę) na wypadek, gdyby któreś się pojawiły na mszy. Będzie koncelebrował mszę.
Podczas mszy ksiądz Filip pokazał Henrykowi dwie dziewczyny i jednego chłopaka. Należą do tej grupy, na pewno te dwie dziewczyny. A one zachowują się lekko... mniej przytomnie. Za to facet między nimi - wyszczerz.
W ogłoszeniach parafialnych - Henryk zbierać będzie skargi parafian i pomoc 1:1.

Nicaretta zdecydowała się wysłać dwie swoje służki, by te skontaktowały się z księdzem...
Tymczasem do Henryka przyszła pani Balbina. Z narzekaniem. Że ksiądz Filip ściągnął sobie młodą i ładną zakonnicę, że otworzyli dom publiczny - i burmistrz się zgodził otworzyć dom publiczny. To się nie godzi!
Ale Jessika, jej wnuczka, jest dobrym dzieckiem. Co podłamało Henryka - on wie, że Jessika była wśród tych które atakowały Filipa...

Gdy Balbina sobie poszła, do Henryka przyszła napalona dziewczyna. Powiedziała mu, że zgrzeszyła... zaczęła cicho mówić, po czym rzuciła się na niego. Henryk cudem uniknął dotknięcia jej ciała (wektor przekazania skażenia) i zamknął się w szafie. Jessika próbowała się do niego dostać, Henryk bardzo trzymał szafę i pokrzykiwał z rozkoszy chcąc ją zmylić. Udało mu się. Rozczarowana Jessika została przywołana do porządku przez inną dziewczynę; oddaliły się. Po godzinie.

Henryk wyszedł z szafy. Wie, że parafia nie jest znowu miejscem bezpiecznym. Aura poza parafią jest silniejsza; ale wpierw się musi przebrać po cywilnemu. Na parafii nikt go nie zobaczył.
Henryk poszedł na Festiwal. Szuka źródła Skażenia na Festiwalu; poświęca na to cały dzień.
Znalazł. Źródłem Skażenia jest fragment festiwalu nosząca nazwę Utopia Marzeń. Wydzielony fragment Festiwalu, gdzie ludzie nie wchodzą... choć Henryk widział, jak czasem ktoś tam wejdzie. Pilnuje tego młody chłopaczek flirtujący z ładną dziewczyną. Sęk w tym... że oni są nieprawdziwi. Efemerydy lub demony.

Henryk został aresztowany przez policję - przez Rafała Szczęślika bo był za nim portret pamięciowy. Za molestowanie i czyny niewłaściwe wobec nieletniej. Henryk NIE chce dać się zamknąć i skończyć w jakimś bondage... telefon do przyjaciela (water). Udało mu się. Aresztowali go i wywieźli do aresztu poza Żonkibór, by rano wypuścić. Uwolnił go telefon do przyjaciela.

Dzień 3:

Henryk wraca do Żonkiboru... nie jest wyspany.
Poszedł na plebanię, zobaczyć co u księdza Filipa. Zbliżając się do kościoła, Henryk wyczuwa potężne nasilenie energii pryzmatu.
Grają organy i w ogóle. Henryk podszedł do kościoła i zerka. Orgia na 33 osoby, gdzie ksiądz i zakonnica biorą udział. I mają konkretną księgę.
Henryk wyczuwa wiry energii magicznej. To jest rytuał...
Henryk poczuł - to rytuał przyzywania demona. Na tym etapie nie jest w stanie sensownie zatrzymać tego demona (nie chce spalić kościoła).

Henryk poleciał do miasta. Musi zebrać babcie i głęboko religijnych ludzi by walczyć z potęgą plugawiącą kościół. Nie potrafi ich zebrać... 
Użył magii. Poszerzył swoje wici.
W tym czasie Nicaretta dała radę się inkarnować i zamanifestować. Inkarnowała się w zakonnicy. Zabrała księgę i odleciała, nie chcąc walczyć przeciwko magom w tym miejscu póki jest słaba.

Henryk z armią wiernych poszedł do kościoła, gdzie odbywa się demoniczna orgia. Ma ogromną armię - 50-60 osób.
Henryk próbuje potężnym zaklęciem puryfikować kościół i osoby w środku.
Henrykowi udało się spuryfikować kościół, aczkolwiek Nicaretta poznała jego smak, jego energię. Umie wykryć jego energię magiczną, więc dodała dystansu i w długą!

I tak naprawdę resztę dnia Henryk poświęcił na czyszczenie reszty miasta. Poświęcił też Quarki które zdobył ostatnio na przeczyszczenie pamięci; udało mu się to, acz zajęło to dość sporo czasu.

I ostatnie co zostało to znaleźć i usunąć Nicarettę...

# Progresja

* Henryk Siwiecki: -1 void na czyszczenie ludzi
* Nicaretta: + księga rytuałów erotycznych

# Streszczenie

Henryk dotarł do Żonkiboru, zaproszony przez lokalnego księdza. Okazało się, że w Żonkiboru panuje aura pryzmatu i jest ona wyraźnie skierowana na erotyzm i orgie. Henryk prawie został 'usunięty' przez napalone nastolatki (nośniki magii), ale w końcu został aresztowany. Gdy wrócił, w kościele toczyła się orgia przyzywająca sukkuba - Nicarettę. Henryk sformował grupę wiernych stawiających czoło grupie 'kultystów', co zmusiło Nicarettę (inkarnowaną w zakonnicy) do wycofania się, po czym wyczyścił Żonkibór. Nicaretta uciekła z księgą rytuałów i jest na wolności...

# Zasługi

* mag: Henryk Siwiecki, który zwerbował armię ludzi w Żonkiborze by zatrzymać przyzwanie sukkuba... i prawie wyszło. Też: chował się w szafie przed napaloną nastolatką.
* czł: Filip Gładki, ksiądz w Żonkiborze. Miał trójkąt z wikarym. Był chłostany by cierpieć za własne grzechy. Paskudna klątwa Nicaretty.
* czł: Szczepan Sławski, wikary uwiedziony przez Nicarettę i jej świtę. Trójkącik z księdzem, dodatkowo: nie odegrał ŻADNEJ sensownej roli.
* czł: Jessika Gniewoń, gimnazjalistka. Polowała na księży w imię Nicaretty. Jednego upolowała, drugi zamknął się w szafie i ją zmylił wydając nieskromne dźwięki.
* czł: Balbina Gniewoń, starsza kobieta, która skarżyła się księdzu na rozpustę na Festiwalu i nie tylko. Ogólnie, plotkarka która chciała być gosposią księdza.
* czł: Marzena Gilek, zakonnica. Host Nicaretty i poprzednia gosposia księdza Filipa z Żonkiboru.
* czł: Rafał Szczęślik, policjant. Aresztował Henryka na jedną kluczową noc po podszeptach Jessiki. Przez niego Henryka nie było gdy Nicaretta się inkarnowała.
* vic: Nicaretta, succubus, taktyk i corruptor. Znajdowała się między Fazami, inkarnowała się w zakonnicy dzięki podszeptom z Drugiej Strony. Zmuszona do ucieczki przez Henryka.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Tonkij
                1. Żonkibór, gdzie pojawili się różowi kultyści i stały Pryzmat
                    1. Festiwal Marzeń, "wieczny" Festiwal będący pod wiecznym nasileniem Pryzmatu
                        1. Utopia Marzeń, dziwne miejsce chronione przez sytuacyjne efemerydy
                    1. Parafia
                        1. Kościół, który stał się miejscem przyzwania sukkuba Nicaretty.
                        1. Plebania, gdzie są izby sypialne i gdzie spał Henryk.

# Skrypt

brak

# Czas

* Opóźnienie: 4 dni
* Dni: 3

====

# Przeciwnik

Nicaretta:

corruptor 3
uwodzenie 3

taktyka 2