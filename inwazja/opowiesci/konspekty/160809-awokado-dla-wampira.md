---
layout: inwazja-konspekt
title:  "Awokado Dla Wampira"
campaign: powrot-karradraela
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150604 - Proces bez szans wygrania (HB, SD, PS)](150604-proces-bez-szans-wygrania.html)

### Chronologiczna

* [150604 - Proces bez szans wygrania (HB, SD, PS)](150604-proces-bez-szans-wygrania.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

W okolicach Trocina znajduje się potężna placówka Srebrnej Świecy która jednocześnie jest słabym punktem gildii. Niedaleko znajduje się "mały ród" "wampirów" rodu Blutwurst, dekadenckich i niespecjalnie niebezpiecznych defilerów. Lord uznał tych magów za potencjalny wektor ataku na Świecę, więc wysłał tam swoją The Governess.
Agentka Spustoszenia wysłała pojedynczych agentów na ten obszar i obserwowała okolicę, pozostając w oddali. W końcu udało jej się znaleźć słaby punkt magów rodu Blutwurst - porywają ludzi lub magów i podmieniają ich na homunkulusy odpowiednio symulujące znikniętych ludzi.
Jako, że pałac Blutwurst jest niemożliwy do sforsowania przez The Governess z uwagi na zabezpieczenia magiczne, ta zdecydowała się na podstęp. Usunęła dwa homunkulusy i sprawdziła jakie siły mają Blutwurstowie w okolicy. Kontrolują lokalną policję i opinię publiczną. Więc zdecydowała się na nieco inny ruch - zaczęła podkładać dowody i przygotowywać nową pułapkę przy użyciu prawdnika jadowitego (zioło szkodzące magom), artefaktu (lub dwóch) z kropiaktorium i niewinnych ludzi...

- w ostatnim czasie (miesiąc temu) rodzice Ireny się wzbogacili; magia Ireny pomogła ich rodzinnemu biznesowi
- około dwa tygodnie temu Irena została porwana przez Blutwursta i wprowadzono na jej miejsce homunkulusa
- The Governess zniszczyła homunkulusa dwa dni temu
- The Governess fabrykuje dowody na działania Blutwurstów

Ż: Jakiego typu outsider może pomóc Wam w rozwiązaniu zagadki - i czemu pozwolicie sobie pomóc?
K: Prezeska stowarzyszenia zajmującego się rękodziełem - udziela się na szerokim terenie i wydaje się wiedzieć więcej niż my.
Ż: Jakiego typu biznes rodzinny prowadzą rodzice Ireny?
R: Bar lokalny gdzie każdy może się najeść i napić - mieli intratny gastronomiczny kontrakt z urzędu miasta.


## Misja właściwa:

### Dzień 1:

Policjant w areszcie męczy Aleksandra. Musiał wypuścić - brak dowodów. Wyraźnie nie lubi Aleksandra. On ostatni widział Irenę zanim zniknęła.
Antonina u rodziców Ireny. Matka: powiedziała, że Irena nie była taka jak zawsze. Ojciec: kłótnia. To jego mała córeczka. Antonina: że to może dlatego bo Irena miała chłopaka. Neutralizacja kłótni. To na pewno jego wina.
Pukanie do drzwi. Aleksander Tomaszewski. Zaproszony, bo chce pomóc (inaczej policja go zgarnie, ale tego nie powiedział).
Zachowywała się jak nie ona. Osowiała, przestała się odchudzać, pożera awokado jak szalona.

Aleksander ze zgodą rodziców wchodzi na komputer Ireny.
... Rodzice Ireny się wzbogacili w wyniku zlecenia od urzędu miasta
... Przed tym jak się zmieniła, Irena dostała maila od "Jana Kowalskiego" odnośnie kociej karmy i wystawy kotów.
... Był lokalny konkurs miss piękności, sponsorowany przez hrabiego Adolphusa von Blutwursta
... Ireny nie było na konkursie; szukała potem w necie "zaginięcie dziewczyn" i "pałacyk Blutwurst"
... Na FB rozpytywała się o Adrianę Wężyk - która zaginęła jakieś pół roku temu. Policja powiązała zniknięcie Adriany z narkotykami.
... Adrianę szukała też Jolanta Lipińska, jej mistrzyni zajmująca się rękodziełem
... POTEM tydzień później Irena zaczęła zachowywać się inaczej - potwierdzenie od Aleksandra. Osowiała i zachowuje się dziwnie
... Irena zachowywała się poprawnie. Ale nie było w niej duszy od tej pory
... 2 tygodnie później Irena zniknęła.

Dzwonek do drzwi. Przyszedł chłopak - Filip - i chciał pomóc. Ojciec chciał go chyba zabić - Antonina się rzuciła go ratować, została znokautowana (tak go uratowała).
Filip powiedział, że znalazł pendrive'a należącego do Ireny.

Tam jest dokument Worda. Zaszyfrowany hasłem Ireny - odszyfrował go Aleksander; dwa zdjęcia - pięknej nieznanej dziewczyny (Adriany Wężyk) i photoshopowane zdjęcie Ireny na taką samą suknię (archaiczna, po renowacji) z tekstem gotyckim "uważaj na Blutwurstów - nie zbliżaj się do nich - M." Geolokacja z oryginalnego zdjęcia wskazuje na pałacyk Blutwurstów.

Von Blutwurstowie to tego typu ludzie, że kupili sobie pałacyk i się przemianowali na "von Blutwurst". Mają kasę, ale nie wiadomo za co i jak. Element napływowy, najpewniej Niemcy, bo Niemcy zawsze okradają polskie ziemie. Gardzą plebsem.
Antonina skontaktowała się z Izabelą Bąk, która uczy prywatnie Karinę von Blutwurst. Powiedziała, że zaginęła uczennica - Izabela zaprosiła do siebie Antoninę i Aleksandra. 
Po spokojnej rozmowie, Antonina nacisnęła - z Izabelą jest wyraźnie coś nie tak. 

Antonina zaczęła rozmawiać i naciskać, by przełamać zaklęcie mentalne. By pomóc Tosi, Aleksander podszedł i pocałował ją mocno w usta.
Szok złamał zaklęcie mentalne.

"Wampiry nie istnieją!"
... Karina ma "koleżanki". Ale większość jest "w piwnicy"
... Boi się fatalnie wampirów; uważa, że tam się dzieje coś strasznego
... Jolanta Lipińska zostawiła wizytówkę KIEDYŚ. Gdy poprzednim razem to się stało
... Nie idźcie do tego pałacyku, nigdy, NIGDY!
... Myślała że uczyła Karinę rok, ale uczyła 3 miesiące.

Po histerii Antonina zapisała ją do znajomego psychologa. Nie wiadomo co wywołało u niej taką reakcję, ale to może ją zabić. Gdy znajoma psycholog przyszła (zabrać Izabelę), Antonina i Aleksander wyszli z mieszkania. Jest wczesna noc.

Aleksander googluje; szuka rzeczy o Blutwurstach w korelacjach z zaginięciami.
... wszyscy przeciwnicy Blutwurstów albo rezygnują, albo się wyprowadzają, albo odchodzą w apatię
... odkąd Blutwurstowie się tu pojawili, zwiększył się zakres zaginięć, ale nieznacznie
... odkąd Blutwurstowie się pojawili, pojawiły się wśród lekarzy dyskusje o apatii i dziwnych zachowaniach młodych ludzi. Też: awokado.
... zniknęły 3 tygodnie temu 2 osoby (apatia#awokado)(powiązanie: Governess). Po pewnym czasie jednak się odnalazły; znaczącym, że ZANIM wróciły to śledztwo zostało spowalniane.
... Blutwurstowie nie mają nic wspólnego z internetem. Nic.

### Dzień 2:

Antonina skontaktowała się z okolicznymi psychologami i wypytuje o apatię wśród młodzieży. Tych ludzi z apatią będzie koło 150. Ale rozproszeni, więc tego nie widać. 
Antonina dzwoni do Jolanty Lipińskiej. Powiedziała, że zaginęła dziewczyna. Jolanta powiedziała, że jeśli przedwczoraj, da się ją uratować. Spotkajmy się w Chlewniczu, w restauracji - za godzinę.

Chlewnicz. Jolanta kupiła jedzenie dla całej trójki; The Governess się spóźni. Dostali jedzenie z prawdnikiem jadowitym; plan The Governess zadziała.
W rozmawie z Jolantą wyszło co następuje:
... Katrina nie żyje. Zginęła dawno temu.
... Najpewniej porywane są dziewczyny WYGLĄDAJĄCE jak Katrina.
... Blutwurstowie są nieco nienormalni; chcą odbudować swoją córkę?
... Blutwurstowie mają świetne narkotyki syntetyczne
... Kiedyś Adriana uciekła (pracowała jako pokojówka); miała pomieszane zmysły.

Uzupełniło się to też gdy przybyła The Governess. Potwierdziła, że Irena jest jeszcze sobą... przez niewielki czas. Potrzebują mało czasu.

Trzeba rozwiązać problem. W sposób dość niestandardowy - proxy tor, klient pocztowy, mail anonimowy: w którym wskazuje na to że podejrzana postać wprowadziła młodą dziewczynę przez bramę Blutwurstów. Ma to być przeczytane jako ewidentne wskazanie na Aleksandra, mimo, że tego tam nie ma napisanego. Opis dziewczyny pasuje do Ireny. A w mailu jeszcze przetrzymywanie porwanej # narkotyki. I fakty. 

EPICKI efekt. Wleciał szwadron napakowanych uzbrojonych antyterrorystów. Pół godziny później... wyszli. Papa, nic się nie stało, spoko... ale nie zgarnęli też Aleksandra...?
Przecież... przecież ich nie naćpali na miejscu?

"A jak oni spanikują i zjedzą wszystkie ciała...?" - Raynor

Wieczór. 

Aleksander siedzi spokojnie w pokoju i szuka danych. Nagle - trzaśnięcie drzwi do domu. Aleksander zamyka drzwi do pokoju i włącza system zabezpieczeń. Przez drzwi zaczyna przesączać się mgła. Ku zdumieniu Aleksandra, zmieniła się w... hrabiego Adolphusa Blutwursta (O_o). To wampir. Seriously.

Aleksander naciska klawisz "nagrywaj". Adolphus żąda od Aleksandra wyjaśnienia o co chodzi i czemu ten nań wysłał antyterrorystów (Governess wysłała wiadomość, że to on). Aleksander jest niepokorny, więc hrabia dobiera się do jego szyi wampirzym zwyczajem... i prawdnik jadowity dostaje się do krwi maga. Hrabia pada na ziemię w drgawkach. Aleksander ma wtf. Glanem skorygował głowę wampira.

The Governess czeka w oknie. Gratuluje Aleksandrowi. Wyjaśnia, że Aleksander i jego towarzyszka zaraz wszystko zapomną a hrabia odda jej kontrolę nad okolicą i ludzie będą bezpieczni. Aleksander w panice naciska "upload". The Governess spokojnie wyjaśnia, że jest techno- i infomantką. To nie zadziała. Ale gratuluje pomysłu.

I zapadła ciemność...

# Progresja

# Streszczenie

Von Blutwurstowie wraz ze swoimi "ofiarami" w obszarze utraconym (Małopolska/Okólnicz) wpadli w ręce Spustoszenia dzięki intrydze The Governess. Dzięki bohaterskim działaniom zwykłych ludzi, cywile nawet nie ucierpieli specjalnie (stan ludzi się nawet poprawił).

# Zasługi

* czł: Aleksander Tomaszewski, doprowadził policję do faktycznych zbrodniarzy, googlował po nocach i się nikomu nie kłaniał. O dziwo, przeżył.
* czł: Antonina Brzeszcz, naciskała gdzie miała naciskać, zdobywała informacje i łagodziła konflikty.
* mag: Karolina Maus jako The Governess, która manipulowała wydarzeniami zza sceny i doprowadziła przy użyciu ludzi do przejęcia przez Spustoszenie Blutwurstów
* mag: Adolphus von Blutwurst, potężny "wampir" i arystokrata-defiler, który łyknął nie tej krwi co powinien i wpadł w łapki Spustoszenia.
* mag: Karina von Blutwurst, wielka nieobecna, "wampir", na której temat The Governess sfabrykowała jeden milion mitów. 
* mag: Irena Paniszok, młoda czarodziejka i pupilka Aleksandra, której niewprawna * magia zaprowadziła ją do piwnicy Blutwurstów.
* czł: Izabela Bąk, nauczycielka muzyki (skrzypce), uczy Karinę i po złamaniu zaklęć przez Olka i Tosię wpadła w zasadniczą histerię. Wyjdzie z tego.
* czł: Jolanta Lipińska, prezeska stowarzyszenia zajmującego się rękodziełem, podstawiona i wprowadzona przez The Governess. W rzeczywistości: nie ma z tym wszystkim nic wspólnego
* czł: Leopold Teściak, policjant, były UBek, który z niewiadomych przyczyn nie lubi Olka. Z wzajemnością.
* czł: Damian Paniszok, ojciec Ireny, porywczy * człowiek pracy, poczciwy właściciel baru tęskniący za córką i robiący głupie rzeczy agresją.
* czł: Elżbieta Paniszok, matka Ireny, która potrafi powiedzieć "to nie moja córka" i mieć rację. 
* czł: Filip Czumko, chłopak Ireny, który oberwał za niewinność, chciał pomóc i znalazł podstawionego pendrive'a. Tyle wygrać.

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Okólny
                1. Okólnicz
                    1. Miasteczko
                        1. Policja
                            1. Areszt
                 1. Pierwogród
                     1. Pałac Blutwurstów, rządzony przez trzech defilerów i forteca nie do zdobycia
                1. Chlewnicz, miasteczko niedaleko Okólnicza poza obszarem wpływów kogokolwiek
                    1. Restauracja Drapak, gdzie The Governess doprowadziła do tego, by ludzie we krwi mieli zioło tępiące magów
  
# Wątki

 

# Skrypt

- brak

# Czas

* Dni: 2