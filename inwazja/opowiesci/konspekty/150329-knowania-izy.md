---
layout: inwazja-konspekt
title:  "Knowania Izy"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150313 - To ile tam było szczepów Spustoszenia?!(SD, PS)](150313-ile-tam-bylo-szczepow-spustoszenia.html)

### Chronologiczna

* [150313 - To ile tam było szczepów Spustoszenia?!(SD, PS)](150313-ile-tam-bylo-szczepow-spustoszenia.html)

### Logiczna

* [141119 - Antygona kontra Dracena (SD, PS)](141119-antygona-kontra-dracena.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

PODSUMOWANIE:
- Julia poszła porozmawiać z Izą odnośnie tego, że Iza wiedziała wcześniej o Spustoszeniu i nic nie powiedziała. Siluria i Paweł sprowokowali ją do starcia, by dowiedzieć się dlaczego to miało być sekretem.
- Zenon Weiner, Marian Welkrat znajdują się na KADEMie. Są bezpiecznie odizolowani od wszystkiego i współpracują z KADEMem w sprawie Spustoszenia.
- Overmind Spustoszenia Aurelii ma trafić na KADEM jak tylko Marta wydeleguje siły by tam bezpiecznie ów byt przenieść.
- Marcel Bankierz przysłany przez Srebrną Świecę do ochrony i izolacji Zenona Weinera został przechwycony i zneutralizowany; relaksuje się na KADEMie.
- Nadal nie wiadomo co uruchomiło "ten drugi" kontroler Spustoszenia, co go wyzwoliło.
- Czterech magów w Powiewie mogło doprowadzić do nauczenia Spustoszenia. Nie wiadomo kto.

## Misja właściwa:

W świetle wszystkiego co się działo Zespół stwierdził, że nie jest dość kompetentny by bawić się w detektywów kto ukradł ("nauczył") Spustoszenie. I po co. Zdecydowali się wykorzystać najlepszego, najbardziej dyskretnego i najbardziej pewnego pomocy detektywa w ramach budżetu jaki posiadają. Ignata Zajcewa. Detektywa z KADEMu, który naprawdę nie lubi Srebrnej Świecy i jest zwolennikiem podejścia do "wielkiego KADEMu" i "być jak Urbanek". Poza tym, sympatyczny.
Ignat prowadzi wewnętrzne biuro detektywistyczne ze środka KADEMu, tylko dla magów KADEMu. 

"Wynieśli surykatkę zagłady z Powiewu, że ma być groźne? Albo śledzia syberyjskiego może?" - Ignat do Silurii, nie wierząc że coś groźnego można wynieść z Powiewu.

Ignat, zachęcony przez Silurię obiecał zająć się tematem Spustoszenia (ktoś wyniósł (sklonował) Spustoszenie Powiewu). Obiecał, że dowie się czego może, ale nie obiecuje za dużo. Musi wpierw spotkać się z ekspertami od Spustoszenia (Marianem, Zenonem) i nie będzie problemu. (dowie się: 15/k20). Siluria zsynchronizowała się z nim odnośnie wiedzy.

Paweł się ucieszył, że mają detektywa.

Zenon i Marian, zapytani, powiedzieli że to Marian poprosił Izę Łaniewską o pomoc. Nie miał większej nadziei, bo Iza od czasu śmierci Kaliny niezbyt pomagała Powiewowi Świeżości (dokładniej: pomoże jeśli ma coś zniszczyć, nie pomoże jeśli nie ma czegoś natychmiast do zniszczenia) ale Iza się zgodziła. Marian powiedział, że Iza "i tak była w okolicy".

Parę godzin później, Julia poprosiła o spotkanie z Silurią i Pawłem. Wieczorem.

Julia porozmawiała z Izą. Powiedziała jej, że zachowała się nieetycznie i powinna była jej powiedzieć wcześniej że miała coś z tym wspólnego a nie udawać, że Srebrna Świeca niczego nie wie. Zażądała informacji (17/20). W wyniku konfliktu Julia nie powiedziała Izie kto ją o to pytał i się tym interesuje i przesunęła Izę na pozycję bardziej defensywną. Ale za to dowiedziała się, że Iza wie coś więcej:
- Iza nic nie powiedziała o Spustoszeniu, ale Tamara wiedziała. Świeca jako gildia nie wiedziała.
- Teren był zabezpieczony; gdyby nie magowie KADEMu to by zostało wszystko ukryte (ale najpewniej byłoby więcej ofiar)
- Powiew Świeżości nie może być w to zamieszany. Nie może być z tym powiązany, bo zostanie zniszczony.
- Oktawian Maus; był powiązany, ale zdaniem Izy nie ze Spustoszeniem. Był tam, ale nie był powiązany ze Spustoszeniem.
- Zdaniem Izy temat Spustoszenia jest zamknięty. Zdaniem Julii, niekoniecznie tak jak Julia by chciała.

Następnego wieczoru Siluria i Paweł spotkali się z Ignatem Zajcewem w jego "biurze".

Ignat powiedział, co odkrył:
- Kluczową dla niego informacją było to, że w tą sprawę zamieszana jest Izabela Łaniewska.
- Przed "pacjentem zero" (odpaleniem Spustoszenia w Kotach) Izabela Łaniewska kontaktowała się czasem z Draceną Diakon.
- Przed "pacjentem zero" Izabela Łaniewska była autoryzowana przez Tamarę Muszkiet do użycia specjalnego wariantu Spustoszenia które "odbiera pamięć". Ignat nie wie więcej na ten temat; nie wie kogo pytać.
- Dracena spotykała się z Salazarem Bankierzem i najpewniej wtedy przekazała mu Spustoszenie.
- Dracena na stan wiedzy Ignata NIE spotkała się z Izą by oddać jej Spustoszenie.

Wszystko wskazuje na współpracę między Tamarą, Izą i Draceną. Najdziwniejsza kombinacja ever.

Zdaniem Ignata (który NIE CIERPI Srebrnej Świecy) jeśli Tamara i Iza sprowadziły Spustoszenie na Koty... to się nie WYPŁACZĄ z tego. To będzie po prostu przechlapane. Srebrna Świeca zostanie uderzona tak bardzo mocno, że to będzie piękne.

Z uwagi na to, że nie ma tu NIC sympatycznego z czym da się współpracować (Dracena nie cierpi Silurii, Iza nie lubi nikogo, Tamara nie cierpi KADEMu) Zespół zdecydował że najlepiej uderzyć w Dracenę przez jej siostrę, Antygonę.
Drugim kanałem - Siluria spróbuje uderzyć w mistrza Srebrnej Świecy który jest w jej kontaktach (Diakon lub z nim spała). Spróbuje się dowiedzieć, jakie oficjalnie było ostatnie zadanie w jakim działały Iza oraz Tamara. Siluria chce znaleźć coś, dzięki czemu będzie w stanie potencjalnie szantażować czarodziejki gdyby nie chciały współpracować z KADEMem. Plus, czy SŚ w ogóle wie o pojawieniu się Spustoszenia.

Siluria poprosiła o pomoc maga SŚ (z frakcji "współpracy SŚ z innymi") - Joachim Kartel - i dostała odpowiedzi na swoje pytania:
- Srebrna Świeca nie ma pojęcia o tym, że doszło do wycieku Spustoszenia. Nie ma nic w rekordach.
- Tamara jest oddelegowana na operację ćwiczeniową z koordynacji 2-3 skrzydeł. Tak się składa, że wszystkie identyfikowane są jako "Stalowa Kurtyna".
- Iza nie ma oficjalnie żadnych zadań. Jest na patrolu i standby.
- Zgodnie z papierami Iza i Tamara nie współpracowały.

Z uwagi na to, w jakim froncie ten mag działa i z uwagi na to, że magowie "Stalowej Kurtyny" postawili triggery na wszystkich wydarzeniach powiązanych z członkami Kurtyny, o zainteresowaniu działaniami dowiedziała się Tamara Muszkiet.

Siluria i Paweł poszli spotkać się z Antygoną Diakon. Siluria zadzwoniła do Antygony, że chcą się z nią zobaczyć i że chodzi o Dracenę. Antygona bez większego entuzjazmu się zgodziła na spotkanie, w "Zaćmionym Sercu". Tam jest półmrok i spokój. Wieczorem, naturalnie. Siluria zaprotestowała - chce się spotkać wcześniej. Antygona zgodziła się spotkać za dwie godziny.

Antygona przyszła, nieszczęśliwa i przerażona. Siluria powiedziała jej, że widzi ryzyko, że Dracena została wykorzystana wbrew swojej woli a jeśli nie została wykorzystana tylko zrobiła to z własnej woli, chciałaby poznać jej motywy. Antygona prawie szeptem spytała, czego Siluria i Paweł oczekują. Chodzi o współpracę Draceny z Izą... tu Antygona zbladła jeszcze bardziej. Siluria zorientowała się, że być może Antygona wie więcej niż się wydaje. To samo Paweł.
Paweł zabrał się do przesłuchiwania Antygony.

Antygona pękła bardzo szybko.
- Powiedziała, że wie o Spustoszeniu.
- Powiedziała, że ona tam była a Spustoszenie wymknęło się spod kontroli.
- Powiedziała, że nadal jest kontrolerem Spustoszenia (Paweł szybko sprawdził na komórce; nie jest; jeśli to Spustoszenie Aurelii najpewniej samo się zniszczyło bo nie ma Spustoszenia z którym powinno walczyć).
- Powiedziała o istnieniu Szlachty - grupie potężnych magów silnie umocowanych politycznie, którzy bawią się wszystkimi jak chcą.
- Powiedziała, że infiltrowała Szlachtę zbierając dowody, ale przez działania Silurii i Pawła została zdekonspirowana przez Dracenę.
- Powiedziała, że została zabawką Szlachty... i nawet Iza nie mogła jej uratować. Jedyną opcją - Spustoszenie.
- Powiedziała, że miała Szlachcie wystawić Dracenę. Ale wystawiła im Spustoszenie... w swojej osobie <martwy uśmiech>
- Powiedziała, że Szlachta nadal jest Spustoszona a ona (Antygona) ich kontroluje.

Tak, jasne. Paweł i Siluria widzą, że coś jest z Antygoną bardzo nie tak. Jest sterroryzowana, spanikowana ale jednocześnie jest... dziwna. Siluria pytała więcej, Antygona przyznała, że plan był planem Draceny; rola Izy w tym wszystkim jest nieznana. Dracena chciała być tą która będzie kontrolerem Spustoszenia, ale Antygona zabrała jej to i wstrzyknęła sobie Spustoszenie...

Paweł skontaktował się telefonicznie z Warmasterem i poprosił go, by przybył z VISORem. Muszą Antygonę przebadać i przeskanować. Coś jest tu bardzo nie tak...

Siluria powiedziała Antygonie, żeby ta powiedziała jej o Kotach. Antygona zaczęła zaprzeczać - nie było jej tam. Nic nie widziała. Nic nie było. Nikt nie zginął. Nie zginęli ludzie. I zaczęła krzyczeć.
Paweł potraktował ją paralizatorem. By uciszyć i by nie zrobiła sobie i innym krzywdy.

I na to otworzyły się drzwi i weszła Dracena. Krzyknęła "już nie żyjesz" i rzuciła się z pięściami na Silurię.
Paweł potraktował ją paralizatorem. Druga osoba na ziemi.

[Slow clap]. Przy drzwiach stoi Iza. Uśmiecha się do nich. Jest uzbrojona i gotowa do walki i proponuje magom KADEMu, by sobie poszli.
Paweł i Siluria powiedzieli, że nie zostawią Izy samej z nieprzytomnymi. Ona zauważyła, że niedługo będzie tu Tamara z oddziałem szturmowym. Paweł i Siluria zauważyli, że są blisko KADEMu.
Iza powiedziała, że jej osobiście nie przeszkadza utopienie we krwi trzech gildii (Millennium, KADEMu i Świecy). Paweł i Siluria obarczyli Izę odpowiedzialnością za śmierć ludzi w Kotach; Iza powiedziała, że ma dużo więcej na sumieniu niż tylko oni.
Iza zachowywała się, jakby chciała bronić nieprzytomne Diakonki przed KADEMem natomiast Siluria i Paweł koniecznie chcieli chronić nieprzytomne Diakonki przed Izą, która wyraźnie tuszuje wszystkie ślady. W końcu zaczęli proponować rozwiązania inne.

Stanęło na tym, że Siluria zadzwoniła do Draconisa (ze zgodą Izy) i poprosiła, by ten zabrał ze sobą Antygonę i Dracenę. Iza zgodziła się i dodała, że prosi Draconisa o opiekę nad Antygoną zgodnie z jakąśtam konwencją. Poprosiła o azyl dla Antygony (co chroni ją też przed Srebrną Świecą). Ale Draconis nie ma prawa powiedzieć nikomu a ZWŁASZCZA KADEMowi niczego co dotyczy spraw wewnętrznych SŚ.

...Marta dostaje z tego pełen raport...


# Streszczenie

Iza pozycjonuje się na masterminda całego tego problemu; odciąga uwagę od Antygony i Draceny (Antygona mówi, że była kontrolerem a Dracena chyba doprowadziła wycieku Spustoszenia jakoś). Tamara - okazuje się - autoryzowała Izę do użycia Spustoszenia... odbierającego pamięć, broni Świecy. Z bełkotu Antygony dowiedzieli się też, że ona została zabawką Szlachty a Spustoszenie miało ją uratować. Ale coś poszło nie tak. Iza zatrzymała rozmowę i powiedziała, że ona utopi 3 gildie we krwi jeśli musi, ale Dracena i Antygona idą do Millennium pod opiekę Draconisa. Tak się stało.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                    1. Nowy Kopalin
                        1. Emoklub "Zaćmione serce"

# Pytania:

TO RESOLVE:

K - Dlaczego Judyta Karnisz, terminuska ze Świecy zwróci się o pomoc do Pawła i Silurii w sprawie po Spustoszeniu?
Ż->K - Bo w każdym innym wypadku pewni magowie zostaną niesprawiedliwie i nieproporcjonalnie mocno ukarani za Spustoszenie.
K - Dlaczego Oktawian Maus bardzo nie chce wpaść w ręce Tamary Muszkiet?
Ż->K - Bo nie chce zostać uznany winnym z automatu.
B - Dlaczego wybór miślęga jako kontrolera Spustoszenia nie był przypadkowy?
Ż->B - Kraloth był czynnikiem nieprzewidzianym; natomiast miślęg jest czymś czym da się w miarę łatwo sterować.
K - W którym aspekcie Iza nie do końca zgadza się z celami Tamary?
Ż->K - Izie zupełnie nie zależy na znalezieniu winnych Spustoszenia.
B - Dlaczego Tamara, mimo, że wie czym jej to grozi będzie próbowała ominąć zakaz?
Ż->B - Bo jest w jakimś stopniu powiązana z EAM i chce chronić wiedzę Zenona przed KADEMem.

NEW:

Ż - W co paskudnego kiedyś wpakował się Ignat Zajcew (mag-detektyw z KADEMu)?
B - Stanął w sytuacji pomiędzy Ewą i Tatianą.
Ż - Jakie rozwiązanie natury socjalnej rozwiązało ten problem?
K - Wymagało to uspokojenia Ewy i przemówienie do rozsądku Tatianie (negocjacje).
Ż - Co paskudnego zbroił kiedyś Mariusz Trzosik (Instytut Zniszczenia KADEMu)?
K - Połączył stary wynalazek Setona z nowym wynalazkiem Janka i nikomu nie powiedział o skutkach.
Ż - W wyniku czego jak ucierpiały osoby trzecie?
B - System namierzania Setona objął swoim zasięgiem około dwóch przecznic.
Ż - Dlaczego "akcję w której znajdował się nieznany mag" mogła uratować tylko Izabela Łaniewska?
B - Były potrzebne takt i wyczucie Izy, jej niezaangażowanie polityczne i fakt że nie ma nic do stracenia.
Ż - I w jaki sposób nieznany mag by ucierpiał gdyby Iza tego nie zrobiła (high stakes)?
K - Zostałby poddany jakiegoś typu przemianie; przekształciliby go na maga rodu Sępiak.
Ż - I czemu Judyta Karnisz nadal się interesuje sprawą Spustoszenia?
B - Czuje się wymanewrowana przez KADEM w tej sprawie.
Ż - Czemu Paweł nic nie wie o tym, że ktoś próbuje reanimować ród Sępiak?
K - Te próby reanimacji są przeprowadzane tak, by w żaden sposób nie wiązać się z istniejącym rodem.

# Zasługi

* mag: Siluria Diakon jako ta, która wsadza kij w mrowisko i trochę detektywem trochę kuzynką dochodzi do pierwszej wersji prawdy
* mag: Paweł Sępiak jako ekspert od twardych przesłuchań, który skutecznie włada paralizatorem
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, która skutecznie alienuje się od wszystkich. Oraz potencjalnie jako osoba która za wszystkim stojąc wpuściła Spustoszenie na Koty i na pewno najbardziej irytująca postać w systemie.
* mag: Antygona Diakon jako "kontroler Spustoszenia", osoba, która pękła na przesłuchaniu i osoba wyraźnie "nie do końca" jeśli chodzi o stan psychiczny
* mag: Dracena Diakon jako (podobno) twórca planu by użyć Spustoszenia do zniszczenia Szlachty (frakcji SŚ). Też, przykład jak NIE atakować (zwłaszcza mając przewagę zaskoczenia)
* mag: Draconis Diakon jako ten, który rozwiązał węzeł gordyjski Iza - Zespół i zabrał Antygonę i Dracenę do Millennium, na naprawę.
* mag: Julia Weiner jako przywódczyni Powiewu Świeżości która woli współpracować z Silurią i Pawłem niż z Izą (dawną przyjaciółką) z uwagi na powagę sytuacji.
* mag: Oktawian Maus jako Mało Ważny Maus, który jest powiązany z całą sprawą ale zdaniem Julii (od Izy) nie ze Spustoszeniem.
* mag: Tamara Muszkiet jako ta, która przekazała Izie Spustoszenie odbierające pamięć oraz tuszowała dla Izy tematy powiązane ze Spustoszeniem.
* mag: Joachim Kartel jako czarodziej wyższy rangą w Srebrnej Świecy, który dla Silurii dowiaduje się jakie są informacje o Tamarze i Izie w publicznych rekordach.
* mag: Salazar Bankierz jako jak się okazało "muł" dla Draceny przenoszący Spustoszenie o którym nie wiedział, by nauczyło się od Spustoszenia w Powiewie.
* mag: Ignat Zajcew jako kompetentny detektyw KADEMu nienawidzący SŚ, który odkrył dla Silurii powiązania między Izą, Tamarą i Draceną.

# Czas

* Dni: 3

# Wątki

- Szlachta vs Kurtyna
- Trzeci kontroler Spustoszenia
- Wojny wielkich gildii Śląska