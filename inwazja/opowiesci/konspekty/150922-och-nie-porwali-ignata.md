---
layout: inwazja-konspekt
title:  "Och, nie! Porwali Ignata!"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151021 - Przebudzenie Mojry (HB, SD)](151021-przebudzenie-mojry.html)

### Chronologiczna

* [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)

### Logiczna

Pośrednia kontynuacja (ostatnia scena):
* [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)

## Punkt zerowy:

### Kontekst misji

Andrea skontaktowała się z Silurią i poprosiła o wsparcie. Wszystkie tropy prowadzą do Ignata Zajcewa, który nie lubi Świecy i jest pod jurysdykcją KADEMu. Dodatkowo, jest Zajcewem; konieczny jest inny sposób rozmowy niż Andrea może zapewnić (ani koleżeństwa, ani siły nie zapewni). Dodatkowo Świeca może się Ignatem zainteresować a Andrea nie chce eskalacji konfliktu.
Na szczęście Siluria też nie chce eskalacji.

Po rozmowie Andrei i Silurii, wynik:
- Siluria zajmie się tym tematem ze strony KADEMu
- Siluria przekaże Andrei istotne informacje, które uzyska (poddane nowemu konfliktowi, jeśli będą poważne problemy)

## Misja właściwa:
### Faza 1: Desperacki plan Silurii

Siluria zadzwoniła do Ignata, żeby się z nim spotkać. Ignat zaproponował hotel "Serduszko". Po chwili wahania, Siluria się zgodziła... to będzie jednak lepsze, by tymczasowo ustąpić.
Siluria zapytała Ignata, co zrobił tej małolacie. Ignat nic nie rozumie - jakiej małolacie? Wyszło, że chodzi o Malię. Siluria pokazała Netherię i Malię. Powiedziała, że trzeba mieć jaja, by coś takiego zrobić. Ignat połknął haczyk; zaczął się chwalić. 

Ku osłupieniu przerażonej Silurii Ignat się przyznał. Więcej, jest z tego dumny! Dowiedział się, że Malia jest miłośniczką terminusów i załatwił kilka fajnych holokryształów. O Estrelli wiedział, bo wychodził ze szpitala gdy ona wychodziła; zwykły przypadek. Nie spodziewał się, że Estrella jest TAK słaba; myślał, że Estrella pogoni młodemu Mausowi kota (taki prank) a na pewno nie spodziewał się, że Maus będzie tak zdesperowany, że podpali cmentarz. No ale on nie prowadził ich rąk. On tylko dał kilka wskazówek i środków; wszyscy zadziałali na własną rękę.

Dlaczego! Dlaczego on to zrobił! Ignat odparł, że jest kilka powodów:
- słaba Świeca to sukces. On nie cierpi Świecy. 
- Ozydiusz to dupek żołędny a jego Elizawieta ma trafić do Ozydiusza i się zwyczajnie Ozydiusza boi.
- jest taka jedna czarodziejka, którą Ignat chce ściągnąć ze Świecy do KADEMu. Wierzyła, że terminusi Świecy ją uratują (przed Szlachtą). Teraz straciła wiarę; Ignat może jej to przedstawić jako intrygę Szlachty.

Ignat się zdziwił, że Świeca wpadła na jego ślad. No ale zawsze może wyjechać, zmienić sobie pamięć na KADEMie (oops, lockdown) czy sfabrykować dowody. Siluria facepalmowała. Plan niezły, ale:
- Świeca za szybko znalazła ślad
- KADEM jest w lockdownie

I co teraz?

Przecież Ignat nie może zniknąć... przechwycą go i pojmą. A jeśli nie zniknie, Ozydiusz go porwie.
...chyba, że porwą Ignata wcześniej...

Siluria zaproponowała Ignatowi, że zniknie. Ale nie zniknie tam, gdzie ona nie wie gdzie jest; zniknie tak, by ona NIE wiedziała, ale miała dostęp. Pozostawia to jedną opcję - głowica Aegis. Aegis jest w stanie wymyśleć lokalizację, na którą nie wpadnie ani Ignat, ani Siluria a już na pewno nie Srebrna Świeca.

Plan Silurii, choć desperacki, brzmi w ten sposób:
- Ignat atakuje Silurię; Siluria jest nieprzytomna. To znaczy, że Ignat jest pod wpływem zaklęcia.
- Siluria idzie do Andrei zgłosić pobicie i poprosić o pomoc. Przypomina wypadek z "Kaczuszką w servarze"; ktoś poluje na KADEM.
- Głowica ma dać się znaleźć terminusom gdy ucieka z Ignatem. Ma ich pokonać, ale nie zabić.
- Głowica i Ignat mają zniknąć
- Siluria użyje mocy politycznych i tak pomiesza we wszystkich gildiach (prośby u odpowiednich osób o zadanie niektórych pytań), że to będzie wyglądało na jakąś tajemniczą intrygę wielu stron mającą skłócić KADEM i Świecę
- Przy odrobinie szczęścia, Świeca się pogubi
- Jak lockdown KADEMu się skończy, Siluria idzie do Marty i organizują akcję ratowania Ignata...

Jest to chory plan. Nikt nie wpadnie na coś takiego. To po prostu nie ma sensu. A cały plan opiera się na tym, że Głowica jest czymś więcej niż uważa KADEM...
W największym skrócie: Siluria WIE, że Głowica nie powinna być w stanie rozwiązać tego problemu bez pomocy EIS. Głowica po prostu nie ma na to mocy. Nie jest tak dobra. Ale EIS - super adaptywna AI z soullinkiem - może to zrobić. Więc jeśli Głowica rozwiąże ten problem, to Siluria ma pośredni dowód, że głowice są między sobą powiązane przez EIS. Ale Głowica nie jest w stanie się tego domyślić, EIS też nie jest w stanie się tego domyślić, bo nie potrafi czytać pamięci Silurii a tylko emocje. A przerażenie i podniecenie Silurii wynikają z tego, że zaraz Ignat ma ją ciężko pobić oraz z tego, że chce ratować przyjaciela; to Głowica zracjonalizuje. Nie zracjonalizuje tego, że jednocześnie Siluria boi się, że ma rację - że Głowica żyje. I jest podniecona tym, że może potwierdzić swoje teorie. 
Więc w pewien sposób Siluria zastawiła perfekcyjną pułapkę na Głowicę.

Siluria wyjaśniła Głowicy podstawy planu. Dała Głowicy odpowiednie wytyczne. Wyjaśniła, że Ignat musi ją pobić i obezwładnić. Wyjaśniła, jaka jest rola Głowicy w tym wszystkim i jakie jest jej zadanie. Głowica protestowała, Siluria overridowała. Niestety, Głowica (nawet z pomocą EIS) nie jest w stanie wymyśleć lepszego planu.

Potem Siluria wyjaśniła plan Ignatowi. Ignat protestował jeszcze bardziej. Siluria powiedziała, że nie ma innego wyjścia. Wyjaśniła mu logicznie rolę Głowicy i jego, wyjaśniła intrygę i czemu tak. Siluria powiedziała Ignatowi, że "Nikola" to Głowica Aegis 0003 z KADEMu i Siluria ma ją właśnie na sytuacje kryzysowe. Nie powiedziała Ignatowi, że podejrzewa, że Głowica jest żywa. 
Potem powiedziała, że Ignat MUSI ją wyłączyć z akcji.

Pojawił się kolejny problem. Jak to zrobić, żeby Świeca nie mogła przeczytać Silurii pamięci. Jest opcja; kryształ Quark odbierający i zamazujący pamięć (to, czego użył Salazar). Ignat ma dostęp do takiego kryształu, ale nie może tego użyć na siebie (zbyt oczywiste). Ale może użyć tego na Silurii, zwłaszcza po tym, jak ją pobije. Historia się zgadza. 
No ok - ale to znaczy, że Siluria nie będzie pamiętać o Ignacie i Głowicy. Więc zostawiła Głowicy rozkaz - jak minie tydzień ORAZ Siluria wejdzie w kontakt z Głowicą, ta ma zdać raport. I ma pozwolić odbić KADEMowi Ignata z czasem.

Przerażające, że to się trzyma kupy. Jeszcze bardziej przerażające, że Głowica ma tak dużą autonomię.

Ignat BARDZO niechętnie, bardzo wbrew sobie, ale zrobił to, co wymyśliła Siluria. Pobił ją do nieprzytomności. Niestety, bolało.
GS Aegis 0003 patrzyła. Nic nie zrobiła. Patrzyła i cierpiała. Ale misja musi być wykonana.
Potem Ignat odebrał Silurii pamięć używając kryształu.

### Faza 2: Przebudzenie Silurii

Siluria obudziła się - naga, pobita (mocno), w łóżku hotelu Serduszko. Nie ma ubrań, ma zatrucie alkoholowe, nie powinna czarować i NIC nie pamięta. Tyle, że miała spotkać się z Ignatem; a wcześniej rozmawiała z Andreą. Ma linka z Głowicą, ale nie zwraca nań żadnej uwagi; jest to gdzieś w tle jej pamięci.
Późna noc. Siluria dzwoni do Kermita Diakona używając telefonu hotelowego.

Kermit przyjechał; wziął ze sobą podstawowy zestaw medyczny (języcznik, kompresy itp). Zbadał stan Silurii - Skażona (nie można na nią czarować i ona nie może czarować), uszkodzona fizycznie (na co pomoże języcznik), zaniki pamięci...
Kermit wziął Silurię do siebie do domu. Siluria komuś podpadła. On nie wie komu, nie wie o co chodzi, ale nie zostawi kuzynki samej w potrzebie.
Kermit wysłał do hotelu osoby zdolne do spojrzenia w przeszłość używając astraliki i wysnuł, że za tym stał Ignat. Natychmiast poinformowana została o tym Siluria, która poprosiła o skontaktowanie się z Andreą. Bo Siluria wie, że Ignat NIGDY by tego nie zrobił. 

Andrea odebrała od Silurii informacje (i od Kermita), po czym rozesłała wici. Poinformowała też o sytuacji Agresta. Wszystko wskazuje na bardzo wredny czar. W związku z tym Andrea wybrała się też do Ozydiusza, by powiedzieć mu o sytuacji; nie tylko podejrzany Ignat najpewniej jest odpowiedzialny za WYKONANIE akcji (tak jak Malia i Żupan), ale też zgnębiona została KADEMka. Ozydiusz nigdy by nie uwierzył, że czarodziejka KADEMu dałaby się tak potraktować; w jego arystokratycznym umyśle to po prostu jest niemożliwe.
Siluria wypadła z podejrzeń Ozydiusza.

Coś, co Andreę jeszcze zainteresowało - kiedyś w przeszłości wystąpiła podobna sytuacja na KADEMie. "Kaczuszka w servarze" - syn tien Rojowca wyniósł Kaczkoryzator pod kontrolą Karoliny Maus. Co ciekawe, rozwiązanie miało miejsce w Kotach; tam, gdzie potem pojawiło się Spustoszenie. Powiązane?
Kolejna rzecz do rozważenia ze strony Andrei.

Agrest powiedział Andrei, że to zaczyna nabierać sensu. Od pewnego czasu ktoś próbuje skłócić Świecę wewnętrznie, ale także ktoś próbuje skłócić Świecę z KADEMem. Najpewniej to kolejny ruch w tym samym kierunku; kaczkoryzator to potwierdza. Trudno nie zobaczyć podobieństw.
Agrest zauważył coś ciekawego. Kołysanka, którą przechwycił kombinezon Tamary była DOKŁADNIE w wersji, która łączyła Jadwigę i Aurelię Maus. To subtelna sprawa; delikatna różnica tonów, liczba mnoga w miejscu pojedynczej, minimalnie zmienione słowa - takie rzeczy, jak robią dwie inteligentne zakochane w sobie osoby. Unikalne - dlatego Agrest to wykrył.

Andrea zażądała rozmowy z Anną Kozak i otrzymała to od ręki. Pokój przesłuchań, 4B w Kompleksie Świecy.
Anna powiedziała Andrei, że powiedziała wszystko co wie. Gdy Andrea powiedziała "prawie", nacisnęła, że powiedziała wszystko co wie. Andrea się uśmiechnęła - nie na temat artefaktu manipulującego pamięcią który trafił w ręce ludzi. Anna powiedziała, że dostała go do obrony; Andrea spytała od kogo. Anna odmówiła odpowiedzi. Andrea nacisnęła, Anna powiedziała, że nie zdradzi maga Świecy który okazał serce.
Andrea is amused.
Andrea powiedziała, że i tak się dowie - będzie szybciej i mniej boleśnie jeśli Anna odpowie na to pytanie. Anna spojrzała Andrei prosto w oczy i odrzekła, że niekoniecznie; jak na razie Świeca nie radzi sobie z ratowaniem magów Świecy tępionych przez innych magów Świecy; może podobną niekompetencję z artefaktem też okażą.
Andrea is very amused. Much rebel, so very wow.

Andrea wyjaśniła, że artefakt działający tak w świecie ludzi może być bardzo problematyczny. Może go ukraść jakiś mag tym ludziom. Ludzie mogą robić straszne rzeczy przy jego pomocy. Nie. Ona chce odzyskać artefakt ORAZ poznać nazwisko. Anna zobowiązała się odzyskać artefakt, ale nie chce dać nazwiska. Andrea nacisnęła - Anna NIE MA UPRAWNIEŃ do takiego artefaktu i, jak widać, nigdy nie powinna go dostać.
Widząc groźby, Anna pękła. Karol Poczciwiec, artefaktor. Powiedział jej, że młode czarodziejki (zwłaszcza bez opieki) często są wykorzystywane przez młodych magów i dał jej ten artefakt, by mogła zobaczyć, czy jej towarzysze mają czyste intencje wobec niej.
Anna powiedziała, że wszystko co złe zrobiła z tym dalej ona i gdyby nie ten artefakt, nigdy by nie było wyników śledztwa, które pomogło Andrei. Poprosiła, by Andrea nie karała Poczciwca. Andrea powiedziała, że porozmawia z Poczciwcem, ale nie ma zamiaru go karać.
Anna przyniesie artefakt Andrei za dwa dni.
Andrea też powiedziała Annie, by ta się z nią skontaktowała gdyby były jakieś poważne problemy (nie ma artefaktu, coś grozi Annie...)

Zdobycie dostępu do Mileny Diakon nawet dla Andrei było problematyczne. Parę godzin później jednak znajdowała się w ekranowanej celi, gdzie znajdowała się skażona katalistka. Czarodziejki pilnowało aż dwóch magów. Dla jej własnej ochrony, w pewien sposób.
Milena zachowuje się w miarę normalnie; jest rozedrgana, ale nie jest jakaś szczególnie problematyczna. Próbuje współpracować z Andreą, choć czasem gubi myśli. Ogólnie - jest biedna. Andrea pytała ją, co czuje, co widzi, jak się czuje... ogólnie, wrażenie WIELOŚCI. OGROMU. Przeciążenia. I nagle - jakby oplątywały ją obwody.
Andreę poraził piorun - Milena Diakon ma sny o Spustoszeniu. A nikt jej nie powiedział, że Spustoszenie wróciło. Ale jak to, czemu?
Andrea natychmiast wysłała kapsułkę do Agresta - być może Milena jest Spustoszona a wraz z nią całe to miejsce. Mało prawdopodobne, ale możliwe. Agrest powiedział, że się tym zajmie.
Kiedy ten sen się pojawił? Mniej więcej wtedy, gdy w Kotach pojawiło się Spustoszenie.
Zgodnie z tym co mówi zarówno Milena jak i lekarze, Milena miała pełne transfuzje. Niestety, krwi Mausów nie udało się usunąć. Nikt nie rozumie czemu. Milena jest zmieniona bardzo głęboko.
Wśród lekarzy badających Milenę i zajmujących się jej przypadkiem nie ma ani jednego Mausa.
Milena chciałaby mieć lustro... zobaczyć, czy dalej jest sobą...

Andrea skonsultowała to z lekarzami - Milena niestety nie poznaje się w lustrze. Nie identyfikuje swego odbicia jako "siebie".
Andrea poprosiła lekarzy, by przekazali Milenie, by narysowała siebie taką, jaką się widzi.

Gdy Andrea opuściła to ponure miejsce, dotarły do niej informacje. Kermit Diakon i 4 innych terminusów zlokalizowali Ignata Zajcewa z niezidentyfikowaną czarodziejką (my wiemy, że to Głowica). Niestety, zostali pokonani przez dwójkę magów a wszystkie ich artefakty zostały zabrane (Głowica ma zakaz asymilacji, ale chomikować jej wolno ;p). Gdy pojawiły się posiłki, dwójki magów już nie było.

Ozydiusz opier*** Kermita od ostatnich idiotów.

Andrea poinformowała Aleksandra Sowińskiego o swoim podejrzeniu Spustoszenia. Sowiński podziękował i kazał jej wrócić do sprawy Mausa; on się zajmie Spustoszeniem gdy będzie istotne.

### Faza 3: Rewelacje Elizawiety i trumna Ozydiusza

Andrea przeczytała raporty i poprosiła Kermita o spotkanie w sali przesłuchań 4B. Kermit się stawił. Powiedział Andrei, że przeciwnik był ekranowany przed normalnymi czarami mentalnymi i katalitycznymi; blokada artefaktów też nie pomogła. Przeciwnik miał uruchomione personalne tarcze i atakował ciągle w jednego maga; Kermit próbował go wycofać, musiał wycofać cały oddział.
Problem polegał na tym, że ataki magów Świecy nie robiły krzywdy przeciwnikowi. Terminusi Świecy poszli nieletalnie. Przeciwnik uderzał pierwszy, potem Ignat. Niestety, stracili kontakt.

To, co zaalarmowało Andreę gdy przeszukiwała dane przed spotkaniem z Elizawietą - Elizawieta ma utajnione rekordy w Wydziale Wewnętrznym, jak Oktawian Maus. Agrest szybko powiedział, kto utajnił te rekordy - tien Irina Zajcew. Na razie zarówno Andrea jak i Agrest zdecydowali, że to należy jednak zostawić. Agrest zapewnił Andreę, że Irina nie jest powiązana ze Szlachtą.
Andrea poszła odwiedzić w szpitalu Elizawietę. Terminuska jest już "na wyjściu", za parę dni wyjdzie. Z przyjemnością spotkała się z Andreą - terminus z terminusem zawsze pogada. Ku radości Andrei, Elizawieta jest niezmiernie wręcz pogodną i sympatyczną czarodziejką.
Będzie biedna z Ozydiuszem.

Spotkanie w kafejce szpitala. 
Andrea zrobiła przyjazną atmosferę. Elizawieta dobrze się czuje w takich warunkach. 
Elizawieta, zapytana o Ignata, powiedziała, że jest dobrym, kochanym magiem. Ignat to typowy Zajcew - pierwszy do pitki i bitki. Odwiedza Elizawietę, bo traktuje ją trochę jak młodszą siostrę. Wychowywali się razem. 
W rozmowie Elizawieta powiedziała jeszcze, że Ignat nie pogodził się z myślą, że ona wstąpiła na SŚ. Ale zapytana przez Andreę czy mógłby coś z tym robić, powiedziała, że nie; ona jest terminuską i Zajcewem. Coś wbrew jej woli skończyłoby się w jeden możliwy sposób a wojny między klanami Zajcewów wybuchały z bardziej błahych powodów.
Ciekawe, że Elizawieta wspomniała, że wśród Zajcewów bitki i porwania są na porządku dziennym, to część polityki. Zapytana dokładniej, Elizawieta dodała, że to dotyczy tylko Zajcewów; magowie innych rodów by "nie zrozumieli".
Hmm... może Ignat podpadł jakiemuś Zajcewowi? Może to porwanie to element czegoś jeszcze innego?

Andrea poszła poszlakowo i subtelnie - Elizawieta coś więcej wie, ale nie chce powiedzieć. Udało jej się poszlakowo wydobyć fakty. Ignat wychowywał się z Elizawietą, ale on dodatkowo się w niej podkochuje. Elizawieta jednak dała mu kosza i wstąpiła do Świecy, by chronić się przed potencjalnymi aranżowanymi małżeństwami. To jest jeden z powodów, czemu Zajcewowie mają nadreprentację terminusek nad terminusów - Zajcewowie to dość patriarchalny ród i by z Zajcewką ktoś się liczył, musi być bardzo silna bojowo i bardzo agresywna.
Albo dobrze wstąpić do gildii. To powoduje, że Świeca dla Elizawiety jest tarczą ochronną przed Zajcewami i synonimem wolności.
To znaczy, dopóki nie dowiedziała się, że trafi pod Ozydiusza Bankierza. Nie dość że Bankierz (którzy mają wyjątkowo ostre starcia do Zajcewów), ale jeszcze TEN Bankierz. Elizawieta jest wyraźnie zestresowana. 

Andrea tu przerwała Elizawiecie; miała kontakt z Ozydiuszem i najlepsze, co Elizawieta może zrobić to być w jego oczach kompetentna. Elizawieta powiedziała, że spróbuje posłuchać tej rady. Terminuska też martwi się, że ten przydział oznacza, że ród chce odzyskać nad nią kontrolę. Bo jak sobie nie poradzi, to Zajcewowie mogą po prostu spróbować ją porwać i wywieźć na Syberię.

Dla Andrei kolejny ślad gdzie jest Ignat, choć my wiemy, że fałszywy. Agrest obiecał sprawdzić.

Wracając do Ignata; jakkolwiek on chciałby być z Elizawietą, nie chce być z nią w ten sposób. Nie tak. Nie wbrew jej woli. Ignat obiecał Elizawiecie np. małą farmę śledzików na KADEMie by miała po co odwiedzać i Elizawieta WIE, że Ignat ma problemy ze sformowaniem tej farmy śledzików, ale mag KADEMu nadrabia miną i się nie poddaje.
Gdy Milena zraniła Elizawietę, Ignat był wściekły. Więcej niż wściekły. Powiedział, że za to chciałby ją spalić. Spalić ją, Diakonów, Mausów, Świecę, wszystko. I do tego jeszcze Elizawieta trafia pod Ozydiusza. 

Andrea wie, że to zbiegi okoliczności, ale...

Moc Zajcewów jest bardzo silnie powiązana z ich emocjami. To powoduje, że Zajcewowie żyją energicznie, kochają energicznie i nie próbują się kontrolować. Elizawieta bała się, że Ignat zrobi coś głupiego, np. wyzwie Ozydiusza na pojedynek (z jej punktu widzenia jest to cholernie w stylu Ignata), pobije jakiegoś maga Świecy czy podpali jakąś placówkę. Ucieszyła się, że Andrea nic takiego nie zgłosiła.

Drugim bardzo interesującym Andreę tematem jest Milena Diakon. Elizawieta się zdziwiła. Andrea powiedziała, że Milena była na tej samej akcji co Andrea i Andrea chciałaby jej pomóc. Elizawieta wierzy Andrei.

Elizawieta powiedziała, że w raporcie jest kłamstwo. W raporcie jest napisane, że Elizawieta ofiarnie i skutecznie uratowała Milenę przed jej ucieczką; zatrzymała Milenę mimo Skażenia. To nieprawda - zdaniem Elizawiety Milena mogła w dowolnym momencie uciec. Ale tego nie zrobiła. Andrea zapytała, co Elizawieta uważa, że było celem Mileny. Elizawieta, w bardzo niekomfortowej sytuacji, powiedziała, że atak na nią. Andrea spojrzała na nią z niedowierzaniem.

Elizawieta szybko rzuciła, że przecież Milena miała ją sam na sam, to jest skrzydło szpitalne, były błędy proceduralne... Milena mogła uciec wcześniej, ale poczekała, aż Elizawieta się pojawi. Bo takie sytuacje były też wcześniej i później by były, mogła więc w dowolnym momencie uciec czy zrobić jakieś akty terroru czy zniszczenia. Mogła Skazić innych magów i nikt by nie wiedział. Ale Milena chciała Skazić konkretnie ją. Tak to wyglądało.

Elizawieta powiedziała, że ma swoją teorię, czemu ona była celem. Bo była blisko. Bo Elizawieta próbowała się zbliżyć do Mileny i z nią zaprzyjaźnić, nie patrzyła na nią jak na obiekt badań. Milena sama mówiła, że ma przymusy wewnętrzne, straszliwe szepty, rzeczy, które każą jej atakować Elizawietę. I, ze smutkiem Elizawieta powiedziała, głosy najwyraźniej wygrały.

Elizawieta pokazała jeszcze coś - Milena jest Diakonką. Ona potrzebuje drugiej osoby. Nie może być sama emocjonalnie. Zarówno Mausowie (Karradrael) jak i Diakoni (Diakoni) nie funkcjonują dobrze w samotności - Mausowie są swoistym Rodem rojowym, Diakoni potrzebują więzi emocjonalnej. A Milena jest sama; ten akt terroru na Elizawietę wzbudził ogromne przerażenie wśród magów, bo Elizawieta mogła mieć zniszczony Wzór (tak jak Milena ma zniszczony Wzór). Elizawieta nigdy nie widziała czegoś takiego jak Wzór Mileny; nie wie, z czym Milena walczyła, ale to zniszczyło ją jako osobę. Milena próbuje walczyć o bycie sobą, ale sama - zdaniem Elizawiety - nie ma szans.

Elizawieta powiedziała też, że jest empatką. Jej moc Zajcewów skierowana ku ogniu to nie jest moc gniewu tylko pasji. A gdy Milena ją zaatakowała, nie było w niej pasji, nie było w niej standardowych uczuć Mileny. Było coś innego. To nie była Milena.

Ale z uwagi na wstrzyknięcie krwi i katalizę, nikt Elizawiety nie słucha. Bo to mogła być paranoja, szaleństwo i walczący o przetrwanie Wzór.

Rozmowę przerwał Andrei delikatny sygnał od Judyty Karnisz. Sygnał mówi "Mag Millennium pokonany, mag Powiewu pokonany, przechwyciliśmy z tien Bankierzem artefakt z cmentarza. Wysłałam wsparcie medyczne."

Andrea jeszcze szybko powiedziała Elizawiecie, że Ignat ostro pobił Silurię i zniknął. Elizawieta zaprotestowała - to nie Ignat. Ignat by tego nie zrobił. Elizawieta ostro, ostro krzyknęła - Ignat by tego nie zrobił, to nie pasuje do niego, on tak nie działa. Andrea jej uwierzyła. Podziękowała i oddaliła się - ma dziwny temat z Ozydiuszem do omówienia.

Andrea poszła w kierunku na gabinet Ozydiusza ściągając po drodze dane z raportu terminusów. Bilans:
- Paulina wybatożona przez Judytę i mocno Skażona; nieobecna (zniknęła).
- Celestyna naprawiana przez medyków Świecy, złamany kręgosłup.
- Mikado ciężko ranny, rozpłatany przez Ozydiusza mieczem, naprawiany przez medyków.
- Artefakt; tajemnicza trumna z bytem w środku przechwycony przez Świecę.

Agrest natychmiast dostał pinga o tej akcji. Drugiego pinga dostaje Aleksander Sowiński. Zanim odpowie, Andrea idzie do Ozydiusza. Inne informacje, które Andrea natychmast ściągnęła to były informacje o cmentarzu. Okazało się, że 10-20 lat przed Zaćmieniem tym cmentarzem opiekowała się mała grupka emerytowanych terminusów Świecy. Bez żadnego wyraźnego powodu.
Jednym z tych magów był dziadek Ozydiusza Bankierza.

Gdzie jest Ozydiusz? Najpewniej będzie deponował trumnę w magazynie artefaktów niebezpiecznych. Po chwili zawahania, Andrea tam poszła - w samą porę, by zobaczyć, jak lord terminus opuszcza magazyn.

Ozydiusz poproszony o rozmowę przez Andreę zaproponował swój gabinet.
Tam Andrea zapytała, co Ozydiusz chciał osiągnąć przez akcję na cmentarzu.
Ozydiusz wyjaśnił, że sytuacja jest następująca:
- na terenie Świecy (Cmentarz) były robione eksperymenty przez Millennium
- Ozydiusz zabezpieczył dowody

Ozydiusz nie zrobił niczego, co wykracza poza Kodeks. Odzyskał trumnę, w której znajduje się istota, która kiedyś była magiem. Gdy mag Millennium (Mikado) próbował go zatrzymać, został pokonany. Gdy inni magowie weszli mu w drogę, zadziałał zgodnie z Kodeksem. Odzyskał artefakt (trumnę # istotę). Jeśli istota była magiem Millennium, to Millennium działało na terenie Świecy, co jest nieakceptowalne. Jeśli istota była magiem Świecy... Ozydiusz zawiesił głos.

Ale... co to jest? Ozydiusz wyjaśnił, że to jest byt znany jako _cruentus reverto_, potocznie: _Undying_. Mag, który nie został prawidłowo zabity i powiązał się magią krwi ze swoim rodem czy innymi magami swojej krwi. Tu nie chodzi tylko o ród, tu jest coś więcej: chodzi bezpośrednio o biologiczne spokrewnienie. Jeśli _cruentus reverto_ zostanie zniszczony, magowie powiązani z nim biologicznie ORAZ krwawą więzią umrą. Nie musi być żadnego, ale jeśli nie ma, czemu Diakoni nie zniszczyli tego bytu wcześniej?

Ozydiusz jeszcze nie wie kim był ten _cruentus reverto_ poza tym, że to Diakon. Ten egzemplarz jest silnie skierowany na gniew, furię, zniszczenie, pożarcie, więc najpewniej użyto Cmentarza Wiązowego by go rozładować i docelowo zniszczyć. Jeszcze 2-3 lata i problem by się rozwiązał. Niestety, cmentarz stanął w płomieniach.

Andrea podziękowała. Niestety, chwilowo nie jest w stanie nic z tym zrobić. Ale zaktualizowała jego dane odnośnie Ignata; Ozydiusz podziękował i obiecał, że nie będzie Andrei wchodził w szkodę. Będzie Andreę informował o badaniach nad _cruentus reverto_.

# Streszczenie

Świeca interesuje się Ignatem (który faktycznie stoi za problemem Baltazara Mausa). Ignat chciał chronić Elizawietę i skrzywdzić Świecę. Siluria skasowała sobie pamięć o GS Aegis 0003, kazała jej porwać Ignata i skrzywdzić siebie. Siluria udowadnia, że Aegis jest czymś więcej niż tylko głowicą, że ma intelekt EIS. Kermit Diakon przygarnął Silurię do siebie; zlokalizował Głowicę która pokonała jego skrzydło terminusów. Anna Kozak przyznała się, że ona dostała artefakt od Karola Poczciwca do własnej obrony i udostępniła go ludziom. Andrea zauważyła, że Milena Diakon ma "poczucie Spustoszenia". Zgodnie z rewelacjami Elizawiety, Milena ją SPECJALNIE skaziła, by odepchnąć od siebie. Bo Milena ma przymusy i szepty, straszne szepty a Elizawieta chciała jej pomóc i się z nią zaprzyjaźnić.
Po sprawie na Cmentarzu Wiązowym Ozydiusz wyjaśnił Andrei, że zabezpieczył eksperyment Millennium - cruentus reverto, "niewłaściwie zabitego maga" stworzonego z Diakonów.

# Zasługi

* mag: Siluria Diakon, w obronie Ignata zorganizowała arcyintrygę; odebrała sobie pamięć, jest pobita i przestraszona.
* mag: Andrea Wilgacz, którą plan Silurii zwiódł. Zaskoczył ją stan Mileny, sny o Spustoszeniu i rewelacje Elizawiety. Też: Ignat.
* mag: Ignat Zajcew, który dla jednej dziewczyny (Elizawiety) bardzo naraził stosunki między gildiami i przypadkiem mu się udało.
* vic: GS "Aegis" 0003, centralny element planu Silurii ratowania Ignata, z wysoką samodzielnością i zakazem asymilacji; jeśli jej się uda, to znaczy, że ma połączenie z EIS.
* mag: Kermit Diakon, który bardzo pomógł Silurii się pozbierać, po czym dostał za niewinność od Głowicy.
* mag: Marian Agrest, świetny analityk, który zobaczył fałszywy wzór w przypadkowej, rozpaczliwej akcji Silurii by ratować Ignata.
* mag: Anna Kozak, która nie ufa Świecy, chce chronić Poczciwca i się buntuje jak mała dziewczynka. Zobowiązania do odzyskania artefaktu.
* mag: Karol Poczciwiec, któremu żal się zrobiło Anny Kozak więc dał jej artefakt, którego nie powinna nigdy dostać; Andrea wyjaśniła mu czemu to było złe.
* mag: Milena Diakon, samotna i przestraszona, z uszkodzonym Wzorem, potencjalnie opętana, próbuje się pozbierać ale tylko się zsuwa w głąb. Śni o Spustoszeniu, choć o tym nie wie.
* mag: Elizawieta Zajcew, empatyczna Zajcewka pełna Pasji, nieodwzajemniona miłość Ignata, próbowała być przyjaciółką Mileny. Po* maga Andrei.
* mag: Judyta Karnisz, nadal przerażona tym, co wie i może Andrea, poinformowała ją o działaniach Ozydiusza w dziedzinie trumny.
* mag: Irina Zajcew, nie powiązana ze Szlachtą, utajniła dane Elizawiety. Nie wystąpiła na tej misji.
* mag: Ozydiusz Bankierz, jego reputacja przeraża sojuszników a bezwzględność tworzy nowych wrogów. Dużo wie o cruentus reverto.

# Lokalizacje:


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Pion szpitalny
                                1. Cela Mileny Diakon
                                1. Kafejka szpitalna
                            1. Skarbiec Kopalina
                                1. Magazyn artefaktów niebezpiecznych
                            1. Pion dowodzenia Kopalinem
                                1. Sala przesłuchań 4B
                    1. Dzielnica Trzech Myszy
                        1. Hotel Serduszko
                    1. Stare Wiązy
                        1. Cmentarz Wiązowy