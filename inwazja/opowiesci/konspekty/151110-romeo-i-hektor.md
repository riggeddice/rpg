---
layout: inwazja-konspekt
title:  "Romeo i... Hektor"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)

### Chronologiczna

* [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)

### Logiczna

* [151021 - Przebudzenie Mojry (HB, SD)](151021-przebudzenie-mojry.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
Ż: Co jest szczególnie cennego / ciekawego w węźle Vladleny?
D: Jest sterylny; jest to przenośny Węzeł, którego bardzo trudno Skazić emocjami i już ma poziom stabilny (autokumuluje).
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".


## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Siluria została pobita przez Ignata i wyłączona z akcji.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę, która potem została przez Hektora upokorzona.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.

### Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Misja właściwa:
### Plan graczy

- N/A

### Faza 1: Ale czemu Ty jesteś Romeo...

Szymon Skubny próbował zdobyć koncesję na telewizję "SkubnyTV". Na szczęście, siły Hektora przejrzały papiery i "witamy w Polsce" - zablokowały to. Nie da się dobrać do jego tematów na YouTube, ale nie ma problemu. Na razie nie ma powodu eskalować.

...z korytarza dobiegają dźwięki gitarą. Sekretarz Hektora, Patryk Kloszard informuje Hektora z nieszczęśliwą miną, że na korytarzu jest bardzo przystojny młody mężczyzna ubrany jak matador z GITARĄ. I śpiewa o "pięknym Hektorro". Gdy Hektor wyszedł, ów przedstawił się jako Romeo Diakon. Rzucił się Hektorowi na szyję, ale ten Romeo zwiał. Romeo krzyknął, czy Hektor nie pamięta ostatniej nocy! Od razu mnóstwo zainteresowania; gdy Hektor spytał, czy Romeo go z kimś nie pomylił, ten położył na stole liścik. Dionizy ów liścik podniósł (bo Hektor mu kazał; sam nie dotknie) i czar miłosny go załatwił... Romeo się zaśmiał widząc przerażenie Hektora. Pokazał Hektorowi, że ma ich więcej... Hektor zwiał do pokoju i wezwał ochronę.

Romeo został wyprowadzony, krzycząc, że powie całemu światu prawdę o Hektorze. A Dionizy potrzebuje pomocy Edwina...
Hektor przejrzał pendrive'a. Jest tam Romeo i Hektor w BARDZO jednoznacznej relacji z kilkoma nieletnimi... tak koło 12-15 lat...
Cholera.

Silurii oczywiście nie ma. Nie ma opcji kontaktu z Silurią.

Więc kolejnymi osobami do kontaktu jest Szlachta - Diana Weiner...
Diana powiedziała, że Romeo jest zabójcą. Takim, co bawi się swoją ofiarą; albo poluje na Hektora by go zabić, albo by zniszczyć jego wizerunek. Nie jest bardzo drogi, musi się zainteresować.

"Użycie kogoś takiego jak Romeo jest po prostu... wulgarne" - Diana do Hektora
"Doskonałe słowo, tien Weiner, doskonałe" - przerażony Hektor

Hektor poprosił Dianę, by ta znalazła zleceniodawców Romea. Ona spróbowała zrenegocjować, gdzie znajdzie się Węzeł ukradziony Kurtynie; Hektor powiedział, że go to nie interesuje. Diana powiedziała, że czasem - rzadko bo rzadko - ale Romeo działa na własną rękę bo ktoś mu się spodoba...

Świetnie.

Edwin i reszta rodziny została wtajemniczona w sytuację. Marcelin się ucieszył. Hektor ma chłopaka! Edwin Marcelina zaraz usadził i powiedział Hektorowi, że ma zamiar z Marcelinem coś przygotować dla Hektora. Jak najszybciej. I przetestuje na Dionizym.

Operacja "Zdobyć Węzeł" zostaje przełożona na nieco później. Hektor musi wpierw spotkać się z Emilią a nieco boi się wyjść... więc Alina i Dionizy (po korekcie) będą wysłani by odnaleźć Romeo. Hektor poprosił Ottona o wiedzę o Diakonach; dostał upload z Rezydencji. Dopiero teraz Hektor zrozumiał i docenił jak bardzo Siluria go oszczędza i jest zachowawcza...
Hektor przygotował strategię dyskusji z Romeo i interakcji z nim. Poprosił Margaret, by ta zbudowała mu coś do znalezienia Romea... ale Edwin to zablokował. Od czego jest Alina i Dionizy?

...no i Alina chce znaleźć Diakona. JAK?
Zdecydowała się pójść do Rzecznej Chaty. Tam rozpytuje o Romeo Diakona i spędza tam trochę czasu.
Do Aliny podeszła Netheria i spytała, czemu pyta o Romea. Alina powiedziała, że chciałaby się z nim spotkać; Netheria zadzwoniła do Romeo i powiedziała, że Alina chce się z nim spotkać z ramienia "Herr Blakenbauera". Romeo odparł, że jak tylko skończy udzielać wywiadu, natychmiast się pojawi. Alina przyjęła do wiadomości - poczeka.

Netheria chciała zaprosić Alinę do teatru, ale Dionizy dał jej dyskretne sygnały "uciekaj". Alina głupia nie jest; nie pójdzie nigdzie z Diakonką. Niech i tak będzie. Dionizy i Alina dzielnie czekają na Romeo.

"Ach kochany Hektor! Jakie on ma słodziusie policzki!" - Romeo na całą Rzeczną Chatę do Aliny.
Bez problemu Romeo pojechał do Rezydencji, tyle, że zabezpieczył się telefonem do Netherii.

Podczas jazdy Alina dopytała Romeo o co mu chodzi; Romeo powiedział, że nie jest fanem hipokryzji i z przyjemnością pokaże Hektorowi, że prokuratura albo będzie bezstronna, albo będzie miała wolne miejsce. Hektor skrzywdził jego przyjaciółkę.
Nagle Romeo powiedział, że muszą jechać TAM! Znalazł jakąś ładną dziewczynę na ulicy i powiedział Alinie, że ona jedzie z nimi. Alina się postawiła, więc odwieźli dziewczynę do domu (pomysł Dionizego). Romeo powiedział, że szkoda, że w końcu nie zabrali młodej ze sobą, Hektor by docenił...

Opóźnienie dało Hektorowi szansę na ubranie się i przygotwanie po tym, jak Edwin poinformował go, że Romeo jedzie do Rezydencji. Przed zaprowadzeniem Romea, Alina zdała Hektorowi raport z całej wyprawy. Rozmowa zaczęła się całkiem nieźle, ale dość szybko eskalowała. Romeo zażądał, aby Hektor wybrał stronę - Szlachta lub Kurtyna - i jednoznacznie określił swoje preferencje w tej sprawie. Dodatkowo, Romeo powiedział, że Hektor BĘDZIE jego, że Szymon Skubny ma wywiad z Romeo a dodatkowo że Hektor musi ustąpić z prokuratury magicznej.

Romeo powiedział, że "wisi" coś Zajcewce, czy to Vladlenie czy to Tatianie - nie wiadomo. Wspomniał obie. Uważa, że Hektor jest problemem i że jest na pasku Szlachty co nie podoba się jego przyjaciółce. On postanowił rozwiązać ten problem. Jeśli Hektor nie zrezygnuje i nie "zostanie jego", to Romeo ma zamiar opublikować taśmy a dodatkowo może pozabijać ludzi, którzy uczestniczą w nagraniu. To stwierdzenie wściekło Hektora; zwłaszcza, że potem Romeo powiedział, że Silurię ciężko pobito i jest w fatalnym stanie. Potraktował to bardzo lekko co oburzyło Hektora. Zarzucił Romeo, że ów nie interesuje się losem swojej rodziny. Diakon wstał i wyszedł.

Poinformowany o całej sprawie Edwin poszedł z tym do Mojry. Ta skontaktowała się z Triumwiratem Diakonów i powiedziała, że jeśli COKOLWIEK Diakoni zrobią wobec Hektora lub Blakenbauerów, mogą ucierpieć Diakoni w strukturach Świecy. To bardzo ochłodziło stosunki, ale jednocześnie dała Blakenbauerom immunitet ze strony Diakonów.

Hektor wie, że ma wroga u Zajcewów. Na pewno jednym z nich jest Tatiana, i to jest wróg który się nie ukrywa.

Do Hektora zadzwonił Szymon Skubny. Powiedział, że w jego ręce trafił pendrive z obrzydliwymi i oczywiście nieprawdziwymi materiałami; on jednak musi je opublikować w necie; chyba, że dostanie tą koncesję na SkubnyTV. Wtedy będzie zbyt zajęty. Jak to Skubny zauważył, nie ma znaczenia czy to prawda czy nie; znaczenie ma co ludzie uważają i w co wierzą.

Hektor uznał, że mniejszą stratą będzie przestać blokować Skubnego. I tak prędzej czy później by to dostał, bo faktycznie działa legalnie. Lekko irytujące, ale niech będzie...
Zaskakujące dla Hektora było jak bardzo groźna i skuteczna jest Mojra.

Jeden plus z tego - Infernia Diakon (czy też Zajcew) MUSI wycofać oskarżenie wobec Marcelina. Musi. Inaczej złamie zasady nadane przez Mojrę. I w ten sposób Marcelin jest nagle w całkowicie bezpieczny :D.

# Streszczenie

Ktoś napuścił Romeo Diakona na Hektora Blakenbauera; chciał go uwieść jako 'dominator'. Power play. Romeo "wisi" coś Zajcewce. Romeo obraził Hektora, ten śmiertelnie obraził Romeo, Mojra skontaktowała się z Triumwiratem Diakonów i jakkolwiek zabezpieczyła Blakenbauerów przed problemami ze strony Diakonów, to jednak stosunki Diakoni - Blakenbauerowie są teraz zimno wrogie (plus: Infernia musi wycofać zarzuty wobec Marcelina, więc ten jest bezpieczny). Hektor zgodził się, by Skubny dostał licencję na SkubnyTV.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                        1. Klub magów "Rzeczna Chata"
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów

# Zasługi

* mag: Hektor Blakenbauer, który nie radził sobie z Romeem, ale z pomocą Mojry się pozbył z nim problemu. Wie, że walczy z Tatianą.
* vic: Alina Bednarz, która poszła do świata * magów ("Rzeczna Chata") dostarczyć Hektorowi Romeo Diakona.
* vic: Dionizy Kret, który chroniąc Hektora został trafiony zaklęciem Romea i się zakochał w Romeo na zabój.
* czł: Szymon Skubny, który zdobył telewizję lokalną "SkubnyTV". Nie chciał publikować materiałów na Hektora "bo dobry z niego przeciwnik".
* czł: Patryk Kloszard, sekretarz Hektora, który nie radzi sobie z wyrzuceniem "matadora z gitarą".
* mag: Romeo Diakon, bardzo niebezpieczny "zabójca wizerunku", przyjaciel Vladleny i znajomy Tatiany; chciał zmusić Hektora do ustąpienia z prokuratury.
* mag: Diana Weiner, która opowiedziała Hektorowi o Romeo Diakonie.
* mag: Edwin Blakenbauer, robiący antyzaklęcia i neuronull na gwałt dla Hektora.
* mag: Netheria Diakon, która skontaktowała Alinę z Romeem; Alina nie poszła z nią do teatru.
* mag: Mojra, która jednym telefonem do Triumwiratu ochłodziła stosunki Blakenbauer-Diakon, ale zabezpieczyła Blakenbauerów przed Diakonami.
* mag: Tatiana Zajcew, wróg Hektora, która nie chce odpuścić; stoi za pojawieniem się Romea Diakona.
* mag: Marcelin Blakenbauer, który jest całkowicie bezpieczny od oskarżeń Inferni i Diakonów dzięki Mojrze i Romeo.