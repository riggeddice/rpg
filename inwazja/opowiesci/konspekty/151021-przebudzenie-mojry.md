---
layout: inwazja-konspekt
title:  "Przebudzenie Mojry"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151014 - Jedno słowo prawdy za dużo... (HB, SD)](151014-jedno-slowo-prawdy-za-duzo.html)

### Chronologiczna

* [151014 - Jedno słowo prawdy za dużo... (HB, SD)](151014-jedno-slowo-prawdy-za-duzo.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.

Ż: Jaki świetny produkt chce syntetyzować Szlachta w Blakenbauerlab?
K: Symbionty wzmacniające magów; endosymbionty wspomagające.
Ż: Co jest szczególnie cennego / ciekawego w węźle Vladleny?
D: Jest sterylny; jest to przenośny Węzeł, którego bardzo trudno Skazić emocjami i już ma poziom stabilny (autokumuluje).
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".

## Misja właściwa:
### Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Część magów Kurtyny jest skupionych dookoła Hektora.
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie.
- Arazille działa gdzieś w okolicy.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- tien Sasanka jest w rękach Silurii i powiedział jej o Aurelu Czarko.
- Oktawian Maus żąda sprawiedliwości dla Mileny Diakon, by dowiedzieć się gdzie jest Karradrael.
- Hektor dostał dowody przeciwko Vladlenie od Szlachty z prośbą wniesienia oskarżenia o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów.
- Wszystko co spotka Vladlenę spotka też Marcelina.

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- Mojra się budzi
- Mojra zgłasza, że Arazille jest na wolności
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Plan graczy

- N/A

### Faza 1: Przebudzenie Mojry

Magowie Kurtyny znajdujący się w Rezydencji zdecydowali się na rozmowę z Hektorem. Ich mówca, niejaki Rufus Czubek, powiedział Hektorowi, że on ich zdradził! Hektor nieco skonfundowany; ale o co chodzi. Rufus powiedział, że przecież Hektor oskarżył Vladlenę - a samo oskarżenie wystawiło ją na pośmiewisko publiczne. A Rufus myślał, że Hektor jest po stronie Kurtyny. Hektor zauważył, że przecież trzeba to zrobić. Rufus wyciągnął Izę, potem Tamarę...
Potem Rufus pokazał Hektorowi wiadomość od Emilii "come to me". Dodatkowo Szlachta - mag Szlachty przybył do Rezydencji i dostarczył jakieś plany by BlakenLab je zbudował dla Szlachty. Jak tak może być - Hektor nie jest po żadnej ze stron! Hektor na to, że nie może powiedzieć, ale przecież współpracuje z Emilią... i ze Szlachtą też. Hektor nie chce wypuszczać tych magów z ręki Emilii. Lekko wywarł wpływem Rezydencji na nich (bardzo lekko).

Emilia, ostatnia nadzieja Kurtyny przeciwko Hektorowi, pomocnemu i zacnemu. Emilia dała radę przechwycić magów Kurtyny. Przeszli na jej stronę, opuścili obszar wpływu Hektora i wrócili do domu, jedynie wzmacniając siłę Emilii. Zdaniem Hektora - Szlachta może nimi teraz manipulować. Zdaniem Emilii - Hektor nie może ;p.

Z Hektorem skontaktowała się też Diana Weiner. Powiedziała, że to wielka szkoda, że Kurtyna opuściła Hektora. Ma dobre źródło informacji - wśród nich jest zdrajca. Tego Hektor się domyślał; tu Diana też powiedziała, że ma grupę praktykantów. Nie ma co z nimi zrobić. Jako, że Hektor jest sojusznikiem Szlachty, Diana jest skłonna wysłać praktykantów na poszukiwaniowcowanie magów czyniących krzywdę ludziom i ogólnie używających mocy magicznej przeciwko ludziom i prawu ludzkiemu. Diana nie udaje - robi to dlatego, bo uważa Hektora za sojusznika. Hektor docenił i podziękował.

Zaraz potem Diana uderzyła jak żmija - wyciągnęła z Hektora subtelnie informację o tym, że Vladlena wiedziała o Węźle i znowu subtelnie przekonała go, że przecież osoba taka jak 'lena nie może mieć dostępu do takiego Węzła! To nie było szczególnie trudne; Hektor też tak uważa. Zaraz potem Diana spróbowała przekonać Hektora, by ten pozwolił Szlachcie zabrał Węzeł; ooooo nie. Hektor się postawił. Ten Węzeł przyda się do kalibracji i budowy symbiontów! Pod takimi argumentami Diana się wycofała; podzielą się Węzłem, ale Blakenbauerowie go od Kurtyny zdobędą.

Hektor jest nawet zadowolony. A i Margaret czy Edwin się ucieszą.

Siluria dostała zaproszenie od Wiktora Sowińskiego; pierwsza faza spotkania w Ogrodach Srebrnej Świecy. Druga faza spotkania - Siluria ma wykupione honorowe miejsce w loży Areny Srebrnej Świecy. Jako, że Wiktor wysłał bardzo eleganckie i formalne pismo. Siluria uniosła brwi. Wpierw eleganckie, ładne pismo, potem spotkanie w ogrodach (bo tam pokazuje, że docenia piękno i umie się zachować), potem załatwia jej lożę (gdzie pokazuje, że ma pieniądze i wpływy) a najpewniej będzie chciał pokazać jej jak walczy na arenie (pokazując swoje mistrzostwo i waleczność). Wiktor zaproponował Silurii, że jak chce, ta może wezwać przyjaciela (myśli o Hektorze). Siluria - nie poprosiła Hektora. Ona poprosiła Dianę (rozerwać Szlachtę, pokazać Wiktorowi, że umie przewidzieć jego ruchy i nie jest łatwa; plus okazja to oplątania Diany - lub wplątanie Diany i Wiktora w siebie (jeśli Wiktor pójdzie w Dianę, też skończy się na BDSM a Dianie się to nie spodoba)). Siluria napisała pismo na pełni mocy by pokazać Wiktorowi, że jest mu równa. 

Odpowiedź Silurii zdecydowanie zaimponowała Wiktorowi. Nie spodziewał się; there is more to her than meets his eye.

Spotkanie w ogrodach. 
Siluria jest ubrana adekwatnie, ubiór zawsze zabiera jakąś dozę wyzwania. Coś z etykietą, sygnał, coś, co każe mu myśleć. Ona pokazuje, że jest jego równą. To jest wyzwanie, które mu rzuca. Wiktor - bardzo elegancko ubrany, szarmancki, przystojny jak cholera. Wiktor ma przy sobie nienagannego, starszego kamerdynera - maga. Odesłał go, jednak mag przysyła mu drinki i inne rzeczy na gest Wiktora. 

Wiktor w ogrodach pokazał Silurii pojedynczy kwiat i opowiedział jego historię - historię Diakonki, która go stworzyła (Heraldinia Diakon) by istniał wiecznie. Utraciła moc podczas Zaćmienia, ale kwiat przetrwał. Pokazał jej kilka takich kwiatów - Siluria wyrobiła sobie opinię o Wiktorze jako o magu, który interesuje się egzotycznymi okazami, po czym zamyka je w kolekcji. Ale o nich pamięta. Podczas dyskusji różnego typu Wiktor próbował sondować Silurię - jakie ma słabe strony, jak Wiktor ma do niej dotrzeć. Siluria spróbowała zrobić kontr-konflikt. Ona chce, by Wiktor zobaczył to, co ona chce by zobaczył - z jednej strony ona jest elegancka, uznaje równość, uważa się za różną - a z drugiej strony ona szuka króla. Nie chama, arystokratę z prawdziwego zdarzenia, który będzie w stanie jej dogodzić i się do niej zaadaptować. I ma czas a nie jest pochopny.

Wiktor uważa, że znalazł słaby punkt Silurii. Siluria uważa, że Wiktor straci magitecha i nie zauważy.

Czas na część drugą - loża. Odebrali Dianę z umówionego miejsca - Wiktor spojrzał zdziwiony na Dianę, Siluria zasugerowała mu, że ten dobór jest dla JEGO przyjemności, ale też ze złośliwym lekko uśmiechem. Wiktor uniósł brew. Docenił.
Siluria nie próbuje zaćmić Diany; próbuje stworzyć historię w której pojawia się sugestia potencjalnego trójkąta. Na razie atakuje Wiktora, ośrodkowo celując jedynie z Dianą. Jej celem jest to, by Wiktor Sowiński 1) docenił działania Silurii, 2) popatrzył na Dianę jako na KOBIETĘ. Udało jej się. Wiktor zupełnie inaczej spojrzał na Dianę... ona tego nie zauważyła. Jeszcze.

Dwie bitwy później Wiktor zszedł na dół, na arenę. Zdjął magitech armour - zostawił go kamerdynerowi - i wyszedł - ubrany w sam mundur i z rapierem w ręku. Podczas walk Wiktora Siluria próbuje wpłynąć na Dianę tak, by ona zobaczyła w Wiktorze faceta. Mężczyznę. Potencjalny obiekt seksualny. Nie jest to łatwe, ale Siluria jest Diakonką a Wiktor nieświadomie jej w tym bardzo pomaga. A Diana identyfikuje się trochę z Silurią.

Działania Silurii były nadspodziewanie skuteczne; Diana zupełnie się tego nie spodziewała a feromony Silurii działają i na nią. Nie tylko Diana jest zainteresowana potencjalnie Wiktorem. Teraz dodatkowo Diana jest zainteresowana i Wiktorem i Silurią - pojawiły się w jej głowie myśli jakich nigdy się by nie spodziewała, bo zawsze miała za dużo pracy.

Wiktor rządzi na arenie. Pokonuje magów. Też terminusów. W pewnym momencie na arenę wyszła osoba, której by się nikt nie spodziewał. Tatiana Zajcew.

"Nie dziwię się, że terminus może bać się zranić Sowińskiego... ale ja nie mam tego problemu". - Tatiana, by wszyscy słuchali.
"...czy mogę prosić, by do mojej rywalki ktoś dołączył?" - Wiktor.

Siluria głośno zaproponowała, by magowie na widowni przygotowali jak najwięcej pięcio-dziesięcio sekundowych zaklęć wspomagających. Niech je rozrzucają na arenie. To da większy poziom chaotyczności i zabawy. Wiktor się zgodził i zaśmiał, że nie zamierza tego używać. Tatiana też się zgodziła. Ona ma zamiar tego użyć.

Walka była interesująca. Tatiana wykorzystywała te mikrozaklęcia i artefakty; dała radę nawet zranić Wiktora raz czy dwa. Wiktor nie używał broni; dał sobie maksymalny możliwy handicap (bez magii, bez broni, ale Tatiana nie ma możliwości sformowania prawidłowo zaklęcia; nie ma pięciu sekund). W końcu doszło do momentu kulminacyjnego, Tatiana strzeliła bardzo silnym zaklęciem myśląc, że Wiktor uniknie. Nie uniknął. Przyjął "na klatę". Strasznie bolało, ale przeforsował i nie zemdlał; złapał Tanię, szarpnął za włosy, jak ona próbowała się wyrwać, rozerwał jej bluzkę (przedtem rozerwał tarcze) i spojrzał na Silurię. Odstąpić czy zniszczyć. Siluria się zastanowiła, ale "odstąpić". Wiktor zmusił Tatianę do uklęknięcia i się oddalił, odwrócił plecami. Tatiana zaatakowała, zaślepiona wściekłością, Wiktor się odsunął tylko i pchnął Tanię w gołe plecy; upadła na twarz. Wiktor odstąpił od walki i dostał huczne oklaski.

Diana nie wie co o tym wszystkim myśleć. Nie widziała nigdy takiej walki...

Emilia skontaktowała się z Hektorem. Spytała go, czy nie widział może tien Sasanki. Hektor powiedział, że nie - sam go szuka. Nigdy go nie widział, ale Sasanka go napadł gdy Hektor rozmawiał ze Szlachtą. Emilia powiedziała, że jest świadoma autonomicznych ruchów swoich agentów, ale teraz jak tu jest to wszystko się zmieni (faktycznie, na razie się zmieniło). Emilia jeszcze nacisnęła Hektora - Sasanka jest młodym magiem, który otrzymał moc świeżo po Zaćmieniu. Nadal kontaktuje się ze światem ludzi; na razie Emilia na jego miejsce podłożyła homunkulusa, by rodzice nic nie wiedzieli. Ale ona się martwi. Jeśli Hektor o nim nie wie, najpewniej Szlachta go złapała. 

Hektor tymczasem przekazał dane Silurii - gdzie jest Sasanka? Czy nie może dopytać Szlachty? Siluria wie gdzie jest Sasanka (sama go porwała); ale stwierdziła, że bezpieczniej jest udawać, że nie i spytała o to Wiktora. Powiedziała, że Sasanka zwinął coś, co należało do niej i ona chciałaby to odzyskać. Czy byłby tak dobry i go odnalazł (lub powiedział gdzie jest, jeśli wie)? Wiktor obiecał, że znajdzie dla niej Sasankę. I faktycznie - wysłał siły. 

Nic to nie da.

Godzinę później Edwin poprosił do laboratorium Hektora.
Czekała tam na niego Mojra. Edwin i Hektor wprowadzili szybko Mojrę w sytuację i wyjaśnili jej co się dzieje. Chłodna i opanowana Mojra praktycznie facepalmowała jak dowiedziała się, że Hektor został prokuratorem świata magów. Natychmiast powiedziała mu, że jest wykorzystywany do usuwania przeciwników politycznych. Hektor powiedział, że wie. Mojra spytała go, czy zmienił zdanie co do etyki, prawdy, sprawiedliwości i innych bzdur? Niezupełnie - ale świat magów jest specyficzny. Mojra potwierdziła - Hektor może wyciągnąć z tego coś dobrego, pod warunkiem, że ma silnych sponsorów.

I Mojra wysłała sygnał w hipernecie o powrocie Arazille, załączając informacje o tym, że ona sama miała kłopoty. Nie powiedziała nic o Tymotheusie Blakenbauerze. Tylko o Arazille. 

W Srebrnej Świecy - ogromne poruszenie. Swoimi kanałami o Arazille dowiedziała się też Siluria...

### Faza 2: Pierwszy symbiont

Dwa dni później.

Siluria czasem spotyka się z Wiktorem, czasem spotyka się z Dianą. Jej plan jest bardzo skuteczny; krok po kroku wszystko się przesuwa. Zarówno Szlachta jak i Kurtyna szukają Sasanki z zerowym powodzeniem. Emilia się martwi, że być może Arazille go dorwała. Świeca szuka Arazille - i nie może znaleźć. Bogini zapadła się jak kamień w wodę.

Siluria dała radę dowiedzieć się od Wiktora rzeczy, która ją interesuje - dlaczego atak na poligon? Co to komu daje? Jaki był plan, jaki spisek? Wiktor nie przyznał, że to oni, ale przyznał, że wie o co chodziło. Atak na poligon KADEMu był sformowany po to, by zmusić Tamarę do działania. Tam nie było nic więcej. Nic. Tylko - wymusić na Tamarze działanie by ją unieczynnić. A że Tamara była chora... cóż. "Przypadek". Wszystko było zaplanowane.
Dla Silurii to ważna wiadomość - w tle znajduje się ktoś bardzo dobrze orientujący się i bardzo skuteczny. Nie było to akcja przeciw KADEMowi, KADEM był tu tylko narzędziem... a przy całej szkodzie dla dumy KADEMu to jest opcja preferowana.

Hektora uwagę ściągnął raport jego sił specjalnych. Szymon Skubny organizuje wydarzenie wspierające czytelnictwo u młodzieży. Nagrody książkowe i możliwość spotkania się z celebrytami. Konkursy na opowiadania w tematyce książek. Dla młodszych - konkurs rysunkowy. Ogólnie, wydał trochę pieniędzy i znowu jest o nim głośno. 
Hektor co prawda nie wie, czemu Skubny to robi (co Skubny knuje), ale to na pewno jest niebezpieczne. A przynajmniej podejrzane.

Hektor zdecydował się tam wybrać. Niestety, bez Silurii; Głowica ją odwołała w innej sprawie i Siluria uznała to za krytyczne.
Hektor rozmawia ze Skubnym. Przekomarzają się nieco; są dla siebie złośliwi. To wygląda na normalny mecenat; nie ma tu nic szczególnie podejrzanego. Zdaniem Hektora - to jest odwrócenie uwagi. Jeśli Skubny jest tutaj i rozmawia z Hektorem, ma perfekcyjne alibi. Skubny musi być w innym miejscu i robić coś innego, potencjalnie ZŁEGO. Hektor kazał grupie śledczej zignorować Skubnego i go szukać.
Patrycja stwierdziła, że to nie na jej nerwy. Ale wykona polecenie... uwierzy w "sobowtóra Skubnego" jak go zobaczy.

A czemu Silurii nie było? Bo Głowica jej powiedziała, że co najmniej dwóch magów zbliża się w kierunku bunkra z Sasanką. Kurtyna.
Czy Głowica jest w stanie ich podsłuchiwać? Tak. Oni dostają wiadomości od Emilii; szukają Sasanki a ona ma detektor.

Siluria doszła do tego, jak Emilia może szukać Sasanki. Jest tylko jeden sposób - ma jego krew. Żaden problem! Siluria przywabiła mnóstwo małych zwierzątek, Głowica cięła Sasankę tak, by ów zaczął krwawić, Siluria skopiowała wzór krwi i pokrwawiła wszystkie zwierzaki, po czym je rozesłała z powrotem do lasu, kanałów etc.
Nagle - detektor Sasanki zaczął świrować... a Siluria każe głowicy przenieść Sasankę do innego safehouse'a. Innego techbunkra KADEMu. A Siluria skaża kolejne zwierzątka Sasanką. Przygotowała czar, który to zapewni, by zwierzęta zapadły na chorobę "mam wzór jak Sasanka".

KAŻDE ZWIERZĘ W LESIE BĘDZIE MIEĆ COŚ Z SASANKI! - Siluria. W głowie.

Kurtyna NIGDY nie znajdzie Sasanki. Nie w ten sposób. Emilia została pokonana.

Jan Szczupak anonsuje gościa Hektorowi - przybyła tien Tatiana Zajcew. Poprosiła o oprowadzenie jej po Rezydencji, po czym rozejrzała się z ciekawością. Hektor spytał Tatianę, po co ona tu jest, co chce zobaczyć, czemu tu się porusza. Tania powiedziała - zgodnie z prawdą - że zawarła zakład z kuzynem. Poprosiła o cokolwiek - COKOLWIEK. Może być łyżka. Bo kuzyn się najpewniej będzie próbował włamać do Rezydencji... i skończy się tak, że ona wygra zakład.

Hektor is not impressed.

Hektor pojechał po niej jako po dzieciakach robiących sobie głupie zakłady. Tania poprosiła o prezent. Hektor ją od dzieci. Tatiana ledwo się kontroluje by nie dać w pysk i pyta, czy Hektor ją wyzywa na pojedynek. On na nią spojrzał z politowaniem. Ona chce go wyzwać na pojedynek. On wzgardził infantylnym zachowaniem i kazał jej się wynosić z Rezydencji.

Tatiana się wyniosła. Powiedziała Hektorowi "do widzenia". Nie zostawi tego w taki sposób.

Hektor poszedł do szefa ochrony - Borysa. Powiedział Borysowi, że w nocy najpewniej włamie się tu jakiś Zajcew. Borys ma go przechwycić, nic mu nie robić, zamknąć w pokoju i zawołać Hektora. Borys przytaknął.

W nocy próbował się włamać Zajcew. Borys się z nim dogadał i sprzedał mu coś - kapeć (używany), który znajdował się w środku Rezydencji. Kapeć był droższy niż jego waga w złocie - ale Zajcew wygrał zakład z Tatianą. Zapłacił. Są rzeczy ważniejsze niż pieniądze - tu chodzi o zakład!

"Borys, ty kanalio! Pazerna kanalio! Co za kanalia!" - Dust, o szefie ochrony...

Hektor usłyszał krzyki w Rezydencji, ze środka. To Adrian... który ma w sobie Symbiont. Obserwuje to zakłopotana Margaret i załamana Mojra. Mojra powiedziała Hektorowi, że to autoryzowała a Adrian zgłosił się na ochotnika. 
Adrian podczas całego bełkotania powiedział Hektorowi, że widział piękne kobiety w łańcuchach... i one mogą być Hektora. Wiktor da je Hektorowi jak on do nich dołączy.
Margaret to... usprawni. Ale bełkot Adriana udowadnia, że ten symbiont robi coś... więcej. Adrian wcześniej tego nie wiedział.
Ciekawe.

### Spekulacje:

- Siluria w końcu wie, że poligon to była kwestia rozgrywek wewnątrz Świecy i że nie było tam nic więcej.
- Siluria również zajumała Sasankę, tym razem skutecznie. Obecnie plan Silurii co do Sasanki: potrzymać go w stazie w ciszy i spokoju, aż cały shitstorm minie, po czym wypuścić na wpół nago w środku lasu, nie mającego pojęcia, co robił, gdzie był i co się z nim działo.

Szlachta
- Diana po sponiewieraniu przez Wiktora będzie chciala stworzyć Szlachtę na nowo, wyłącznie z prawymi magami, będąc wybredna w doborze członków
- Siluria postara się odwrócić uwagę Wiktora tak, by można było zajumać magitecha. Siluria poważnie rozważa podłożenie śladów Sasanki
- W obecnej fazie symbionty okazały się czymś w rodzaju lepszego serum prawdy, pozwalającego odzyskać nawet utraconą pamięć. Może wszczepić to Szlachcie..?
!!!! - Informację o Arazielle Siluria przekazuje Triumwiratowi.
- Siluria jest na chwilę obecną zainteresowana "zabawą" z Wiktorem i Dianą. O ile nie stanie się coś dużego, to pozostanie jej głównym obszarem działania. Stopniowo, krok po kroku dąży do zaplanowanego przez siebie rozwiązania sprawy.
- Hektor zainteresuje się węzłem, zlecając Dionizemu jego zajumanie.
- Siluria zainteresuje się tematem Aurela Czarko, który napuścił Sasankę na atak. Najchętniej jakimś sposobem, bez zwracania na siebie uwagi, Siluria zasugerowałaby jego istnienie Emilii.
- Siluria będzie się dopytywać o stan Mileny i jeśli tylko znajdzie sposób, postara się jej pomóc.

Zakład:
- Kić: może chodziło o to, która ścieżka (który sposób) zadziala
- Kić: Tania coraz bardziej nie lubi Hektora. Pomimo jej prób rozmowy i dogadania się (z jej punktu widzenia), Hektor ciągle ją obraża i poniża (w jej oczach). Ciekawe, kiedy Tania nie wytrzyma... I co zrobi
- Gdyby Hektor wiedzial, co się przydarzyło Tani, inaczej podszedłby do sprawy.
- Dust: Boję się, że przgrany zakład zaważy na decyzji o tym, w jaki sposób Zajcewowie podejdą do Blackenbauerów i skieruje ich na 'evil, sneaky, mafia way'

Arazille:
- Emilia potraktuje sprawę poważnie, ale na chwilę obecną nie bardzo ma szerokie możliwości. Może próbować uzyskać jakieś wsparcie czy to ze strony Hektora czy Silurii
- Arazielle częściowo zajmuje Crystal. Wampirzyca żywi się magią. Kiedy się najada, wraca jej część pamięci i przypomina sobie kim była. Czy Arazielle będzie chciała ją uwolnić?
- Crystal byla Diakonką. Kiedy Siluria się o tym dowie, nie wiadomo, co zrobi.
- Gdyby dało się użyć mocy Diakonów i zespolić Crystal z kralothem... może by to pomogło. Ale czy Diakonowie zaryzykują coś takiego?
- Crystal prawdopodobnie obecnie poluje na magów. Nie zabija, ale wyżera moc.

## Dark Future:
### Actors:

Emilia Kołatka (Silk Hiding Steel)
Edwin Blakenbauer (Sentinel)
Wiktor Sowiński (Dominator)
Diana Weiner (Cult Speaker)
Tatiana Zajcew (Brawler//Explorer)
Tymotheus Blakenbauer (Engineer)
Mojra (Cleaner)

Magowie Kurtyny (Społeczność dookoła celebryty)
Vladlena Zajcew (Warmonger)


### Faza 1: Przebudzenie Mojry

- Magowie Kurtyny są oburzeni działaniami Hektora; jak może atakować Lenę \| \| MK.(rally against <someone> threatening the <celebrity>)
- Emilia odzyskuje Kurtynę przez działania Hektora i Szlachty \| Emilia.(outsmart <someone> and maneuever to have an upper hand)
- Diana próbuje wkręcić Hektora w to, by ten dał jej dostęp do Węzła Vladleny. \| Diana.(strengthen the cult)
- Wiktor chce się umówić z Silurią i ją zrozumieć lepiej i się z nią zapoznać. \| Wiktor.(study <someone> to find weak spots)
- Emilia szuka tien Sasanki, którego nie może się doliczyć \| Emilia.()
- Mojra ogłasza, że Arazille powróciła i żąda pokoju pomiędzy wszystkimi siłami \| Mojra.(create an event to distract from <something>)
- Margaret próbuje konstruuje pierwszego symbionta dla Szlachty \| Margaret.(create a functional prototype)

### Faza 2: Pierwszy symbiont

- Szymon Skubny przygotowuje event promujący czytelnictwa \| Szymon.(improve their surroundings by <doing something>)
- Emilia skupia się na szukaniu Arazille siłami Kurtyny \| 
- Tatiana przychodzi odwiedzić Rezydencję Blakenbauerów bez pozornie żadnego powodu \| Tatiana.(race a rival)
- Margaret chce przetestować symbiont używając tien Adriana Murarza \| 
- Adrian się popsuł; Symbiont się nie zintegrował dobrze i Adrian wpadł w szał; Margaret go wyłączyła \|
- Wiktor idzie na ekspedycję zdobycia Węzła z agentami; zostawi "prezent" \| Wiktor.(defile <someone's> sanctuary)
- Diana odwraca uwagę przez swój call to action - help each other \| Diana.(speak publicly)
- Mojra to wyjaśni ze Szlachtą... \| Mojra.(mediate with <someone> to solve a problem of <something>)

# Streszczenie

Emilia odzyskała magów Kurtyny z Rezydencji Blakenbauerów. Diana Weiner oddała Hektorowi grupę praktykantów Szlachty do rozwiązywania problemów ze światem ludzi. W zamian za to - Hektor przechwyci Węzeł Vladleny. Wiktor zaleca się do Silurii ze wzajemnością. Wiktor na arenie (macho man) pokonuje terminusów i Tatianę. Mojra się obudziła i powiedziała Hektorowi, że jest wykorzystywany do niszczenia przeciwników politycznych Szlachty. Świeca wie o Arazille i jej szuka. Siluria dowiedziała się, że atak na poligon KADEMu to sprawa polityczna Świecy; jak Hektor prokurator. Siluria skutecznie zamaskowała Sasankę przed Emilią. Hektor odmówił Tatianie czegoś z Rezydencji a jej kolega - Zajcew - przekupił Borysa i kupił stary kapeć. Tania przegrała zakład. Adrian Murarz został sprzężony symbiontem (autoryzacja Mojry); symbiont Szlachty robi więcej niż mówił Wiktor.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Ogrody Świecy
                                1. Arena Maximus
                        1. Plac Wszystkich Kultur, na którym odbywa się festiwal czytelnictwa młodzieży sponsorowany przez Skubnego

# Zasługi

* mag: Hektor Blakenbauer, oddał * magów Kurtyny Emilii, oddał wiedzę o węźle Vladleny Dianie, nie znalazł nic na Skubnego, patronizował Tatianę i zaufał uczciwości Borysa.
* mag: Siluria Diakon, sprytna uwodzicielka i manipulatorka, która skutecznie odwróciła uwagę na Sasankę i go przenosi. Celuje w trójkącik z Wiktorem i Dianą.
* mag: Edwin Blakenbauer, który nie potrafić robić dramatycznych wejść, ale obudził Mojrę. Ogólnie - bezużyteczny na tej misji.
* mag: Margaret Blakenbauer, która wstrzyknęła pierwszy endosymbiont Murarzowi. Nie wyszło. 
* mag: Mojra, która wreszcie się obudziła po bardzo długim okresie nieaktywności; ostrzegła Świecę przed powrotem Arazille.
* mag: Wiktor Sowiński, uroczy amant Silurii, który upokarza publicznie Tatianę i pokazuje swoje mroczne oblicze. Walczy jak szatan.
* mag: Diana Weiner, która podeszła Hektora i po raz pierwszy zobaczyła w Wiktorze faceta.
* mag: Emilia Kołatka, która rekonsoliduje siły Kurtyny i (nieudanie) szuka Sasanki. Plan niezły, ale Siluria.
* mag: Tatiana Zajcew, która ma zły dzień. Wpierw Wiktor upokarza ją na Arenie Maximus, potem Hektor uniemożliwia jej wygranie zakładu. Sponiewierana i poniżona. 
* mag: Rufus Czubek, * mag Kurtyny i prowodyr ścieżki powrotu do Emilii; postawił się Hektorowi i porwał * magów Kurtyny za sobą.
* mag: Edward Sasanka, który będąc nieprzytomnym całą misję wywołał więcej zamieszania niż kiedykolwiek. Wszyscy (poza Silurią) go szukają.
* czł: Szymon Skubny, który zorganizował festiwal czytelnictwa młodzieży i podobno NIC za tym nie stoi. Jjjjasne.
* czł: Borys Kumin, kanalia a nie szef ochrony. Sprzedał Zajcewowi kapeć zamiast zamknąć go w lochu. Dorobił się a Tatiana przegrała zakład.
* vic: GS "Aegis" 0003, jednostka szybkiego ostrzegania przed Emilią polującą na Sasankę. Przenosi Sasankę i gubi Emilię z planem Silurii.
* mag: Adrian Murarz, któremu wprowadzono endosymbionta Blakenbauerów i Szlachty; przypomniał sobie rzeczy których "nigdy nie pamiętał".