---
layout: inwazja-konspekt
title:  "Spalone generatory pryzmatyczne"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161207 - Lizanie ran na KADEMie (HB, SD)](161207-lizanie-ran-na-kademie.html)

### Chronologiczna

* [161207 - Lizanie ran na KADEMie (HB, SD)](161207-lizanie-ran-na-kademie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Hektor obudził się pierwszy. Jest w pokoju z Anną (?). Ta wyjaśniła, że nie ufa KADEMowi. Trzymają ich w więzieniu - a jeszcze dodatkowo organy Blakenbauerów są przydatne do różnych rytuałów. Anna nie do końca wie, na CZYM polega fenomen Blakenbauerów, ale uważa go za zagrożonego... więc zdecydowała się spać z nim. Anna, ogólnie, zaczęła snuć paranoiczne i konspiracyjne teorie na temat KADEMu. Anna (sentinel, zastraszanie 2, SCA0 = 3). Hektor czuje się niepewnie... i raczej uważa, że jest zagrożony. Hektor wchodzi do łóżka z Anną i się przytula. Ona uspokaja Hektora jak dziecko... lub kota... w drugiej ręce trzymając Hektora.

Siluria śpi słodko z Infernią. Jest jej dobrze, ciepło i rozkosznie. Nagle - elektrowstrząs. Siluria i Infernia, rozkojarzone. Bezcielesny głos Quasar powiedział Silurii, że ta ma się ubierać - przy przejściu przez Esuriit padły generatory pryzmatyczne. To znaczy, że pojawia się możliwość efemeryd... więc Siluria ma to rozwiązać. I siluria poszła do Mikada... który poleruje swój miecz. Dosłownie. Nie to, co można myśleć.

Mikado wygląda na bardzo zmęczonego. Okazuje się, że on cały czas się skupia; wie, że są na Fazie Daemonica i nie wie o generatorach pryzmatycznych - więc pilnował, by nie "myśleć" w niewłaściwy sposób. Mikado jest bardzo zmęczony, ale pójdzie z Silurią. Siluria przeszła przez Norberta i dostała stymulanty dla Mikada; acz Norbert jest niezadowolony; uważa, że powinni wszyscy spać.

Anna powstrzymała Hektora od otworzenia drzwi Silurii; nie wiadomo, czy Siluria nie została przekształcona. Siluria poszła w kierunku na jedzenie i głód Hektora. Siluria jednak dała radę przekonać Hektora i złamać głodem jego paranoję. Mikado się uśmiechnął do Hektora i spytał Annę, czy ta też chce przyjść jeść. Anna spojrzała z wyrzutem na Hektora i potwierdziła.

Kryzys śniadaniowy rozwiązany.

O czekaj. Nie. Infernia jest kucharką...  A Infernia i Hektor mają... _historię_.

Infernia załatwiła dla Hektora na poligonie bioforma bojowego. Siluria upewniła się, że Infernia wszystko wyłączyła na poligonie. Oczywiście, tego NIE zrobiła. Infernia nie pała miłością do Hektora ;-). Bioform bojowy to drapieżnik, dużej wielkości ainshker. To cholerstwo umie walczyć i jest częściowo transorganizowane na poziomie głowy (kombinowana praca Kariny i Ilony; Karina jako biomag a Ilona jako ekspert od umysłu). 

Hektor jest w lesie (na poligonie). Jest ciemno. Wdrapuje się na drzewo, zaskakuje go jednak ainshker. Wystrzelił z innego drzewa, zrzucił Hektora i rozorał mu klatę. Poszła krew. Hektor leży na ziemi, dał się poranić by złapać ainshkera i odgryźć mu łapę. Ainshker dał radę się odbić, wyszedł gorzej z tej walki. Ainshker został skutecznie uderzony widmami Hektora; wpadł w tryb berserk używając obwodów Ilony. Ainshker zaatakował na pełnej mocy nie dbając o przetrwanie. Komputer celowniczy pomaga. Ainshker zaatakował, Hektor go złapał i przegryzł mu kręgosłup. Przy szyi. Czas pobawić się jedzeniem, potem wziął w pysk i przyszedł się pochwalić Silurii.

"Smacznego. Od razu mam ochotę na dziki seks, jak na to patrzę." - Infernia, do Silurii, facepalmując.

Siluria swoim zachowaniem dała radę zmniejszyć paranoję Anny. Uff. Anna nie będzie stanowiła zagrożenia dla KADEMu (w zakresie tej misji). A Hektor jak pojadł, trochę się zmniejszył i poczuł się lepiej. Trzeba by wziąć prysznic... Hektor zorientował się, że nie ma ciuchów na zmianę. 

* Chodź. Wiem co tam masz. Chodź - Siluria
* Skąd wiesz... - Hektor, skonfundowany
* Marcelin pokazał zdjęcia - Infernia, bardzo pomocna, jak to ona
* Zabiję...... - Hektor, załamany

Mikado spytał Silurię, czy istnieje opcja wykorzystania hal symulacji? On i Anna się założyli, czy miecz jest szybszy od pistoletu. Siluria się ucieszyła. Niech Mikado i Anna trochę wyluzują. Anna skonfliktowana, ale też chce. Świetnie - poszli do hal symulacji męczyć się ze swoimi zakładami...

Siluria chce oprowadzić Hektora. Wtem - Ilona. Ta jest ciekawa jak myśli prawnik (ewentualnie chce zasilić drapieżnymi heurystykami ainshkery czy marcadoriany czy inne takie). Siluria ich opóźniła; by Hektora zmotywować, dała wyzwanie.

* "Kto pierwszy w moim gabinecie, wygra" - Ilona, z uśmiechem
* "Ale... gdzie jest twój gabinet?" - Hektor, skonfundowany
* "No właśnie ;-)" - Ilona, z szerokim uśmiechem

Hektor znalazł jej gabinet. Lekko roztargniona, zajmuje się AI i transorganikami. Gdyby miała kota, umarłby z głodu... Hektor się na nią zaczaił... i chciał ją zaskoczyć. Sęk w tym, że GS Aegis 0004 się rzuciła na Hektora i go odepchnęła. Ten był zaskoczony; Aegis nie uderzyła by zabić, tylko by odepchnąć. Ilona zatrzymała Aegis; próbowała odwrócić uwagę Hektora od Aegis 0004 w wyniku czego powiedziała mu całkiem sporo o Aegis 0003 i o tym, że Siluria miała do niej duży dostęp. I że Siluria zgubiła jej głowicę. Więc Ilona się martwi. Udało jej się całkowicie odwrócić uwagę od Aegis 0004; ZWŁASZCZA żeby Hektor nie powiedział Silurii że Ilona ma nową głowicę. Ilona dodała epic rant, że nikt nie patrzy na Diakonki inaczej jak "z łóżka do łóżka"; nikt nie widzi, co Siluria tak naprawdę tam zrobiła.

Hektor wypytał Ilonę odnośnie głowicy - ta powiedziała, że głowicę może kontrolować mag KADEMu i bardzo trudno ją znaleźć. Hektor zaproponował Edwina; Ilona odpowiedziała, że jakkolwiek Edwin jest jej byłym (ku WIELKIEMU zdziwieniu Hektora) to jednak nie zadziała - on nie ma uprawnień :-(...

A tymczasem, Siluria. 

Siluria poszła do magazynu, w którym znajduje się EIS. Nikogo tam nie ma, na pewno nie ma Alisy. Ale jest EIS. Leży w kontenerach, wiecznie obudzona i jednocześnie śpiąca... i Siluria zaczyna monologować.

"Cześć. To ja, Siluria. Jestem bezpiecznie na KADEMie. Wierzę, że też bezpiecznie gdzieś jesteś i czekasz na nasz powrót. Martwię się, że nie wiesz co robić. Znajdź sobie jakąś bezpieczną kryjówkę. Naprawdę warto to zrobić..." - Siluria ma cichą nadzieję, że coś z jej monologu dojdzie do głowicy. - "Aegis 0003, Nihilus. Znajdź bezpieczne miejsce... poczekaj na nas. Wrócimy." 

Siluria próbuje PRZEKAZAĆ, że się trochę martwi i troszczy...

* "Silurio. Generatory pryzmatyczne przestały działać. Mamy skażenie efemeryd" - Quasar, do Silurii, przerywając jej. Siluria uśmiechnęła się srogo - Anna i Mikado tego nie zauważą...
* "Musimy rozdzielić magów Świecy i ludzi. Wszystkich. Poziom Pryzmatu przekroczył nasze uszkodzone generatory i poszła kaskada" - Quasar, doprecyzowując

Hektor widząc, że Ilona powiedziała, że muszą zostać w tym samym miejscu w którym są bo coś złego się dzieje - powiedział Ilonie, że musi znaleźć Silurię. Ona go nie trzymała, wie jak działa Pryzmat. Hektor podjął trop i poleciał szybko. Dużymi susami. Po drodze złapali Bestię Rojowiec i Kotała - wystrzelili z broni KADEMowych do czegoś co uważają za efemerydę. Hektor, nie zmieniając lotu, walnął w nich Widmami. Udało się mu ich przerazić. Jednak został ranny, chlapie krwią na lewo i prawo i leci dalej, licząc na swoją regenerację. 

Hektor znalazł Silurię. Ta idzie szybkim krokiem korytarzem i nagle zza rogu wypada ranna Bestia. Siluria i Hektor się spotkali ;-). Po drodze spotkali się z Karoliną, która przechodziła i pytała o to, czy są jakieś problemy... i czy nie chcą jakichś rekreacyjnych hedośrodków. Hektorowi włączył się tryb prokuratora, ale Karolina potraktowała to z dużą sympatią i dowolnością. Powiedziała, że to nie uzależnia i nie jest sprzedawane. I nie działa na ludzi; tylko dla magów.

Gdy się zbliżają do skrzydła medycznego, poziom Pryzmatu jest bardzo, bardzo silny. Magowie Świecy tak się boją Esuriit, że... ściągają tu Esuriit. Przekroczyli pewną granicę. Siluria zaczęła gadać o... Skubnym. By zmniejszyć nacisk Pryzmatu, zdobyć emocje z Primusa... i się udało. Siluria używa żywiołowego Hektora i innego Pryzmatu by móc dostać się do skrzydła szpitalnego. Skubny ratuje dzień... udało się.

Jest Norbert z Kariną. Ciężko pracują, by utrzymać integralność KADEMu w obliczu pseudo-Esuriit. Magowie KADEMu nie są zestresowani - to KADEM. Norbert ostrzegł, że simulacra mu się spaliły; magowie Świecy siedzą i się martwią wspólnie. Sami się nakręcają. Złe myśli, złe uczucia. Trzeba ich jakoś rozweselić, coś zrobić... co zadziała. Bogdan próbował ich jakoś pocieszać, ale nie wyszło przez radio. Morale Świecy jest zerowe a w tej chwili Norbert boi się ich usypiać...

Największym problemem jest właśnie Marianna. Jest w pokoju obok, ale nie da się do niej wejść (bo dowódcy nie można przeszkadzać). Siluria ma więc rozwiązanie - trzeba porozmawiać z Marianną. Ona uspokoi resztę swoich ludzi. Siluria i Hektor poszli więc odwiedzić Mariannę, wśród innych magów Świecy... Siluria próbuje samym swoim przejściem zasiać odrobinę nadziei.

Marianna. Sama w pokoju. Twierdzi, że ona już nie wróci do domu. Twierdzi, że jej misja się już skończyła. Twierdzi, że jest już martwa. Marianna Sowińska umarła na Fazie Esuriit...

Siluria powiedziała Mariannie, że ona żyje i ma się całkiem dobrze. Rzuciła się na Mariannę z silnym całusem. Ta ją odepchnęła - w rękawiczkach. Siluria się zarazi. Gdy ta rzuciła się raz jeszcze, dostała nożem; Hektor odciągnął Silurię przed ostrzejszą raną (lekkie draśnięcie). Marianna jest serio. Siluria poprosiła Hektora, by ten przytrzymał Mariannę. Złapał za rękę Mariannę i ją podniósł. Ból. Siluria tym razem się nie patyczkowała - dragi od Karoliny, striptiz i ostry całus. Marianna została całkowicie zdominowana i zaspokojona. I jest szczęśliwa. 

"Wypraszam sobie. To nie są dragi. To hedonistyczne środki psychoaktywne. Nie uzależniają..." - Karolina, zdecydowanie oburzona, offscreen.

Marianna została zaspokojona i jest w lekkim przyjemnym transie. A potem kolej na Hektora...

Przesunęli pryzmat. Jeszcze Norbert... widząc, że to działa, wpuścił magom Świecy gaz... erotycznie pobudzający by im wybić z głowy Esuriit. Ewentualnymi następstwami można martwić się potem...

# Progresja



# Streszczenie

Hektor wdraża się w swoją nową rolę Bestii, adaptuje coraz lepiej. Niestety, po problemach na Esuriit na KADEMie padły generatory pryzmatyczne, co wymaga skupienia uwagi i usuwania efemeryd. Jako, że uratowani magowie Świecy przyciągają Pryzmat Esuriit, Siluria spacyfikowała Mariannę po swojemu a Norbert wpuścił magom Świecy różową mgiełkę ;-). Eis ma GS Aegis 0004 oraz korzysta z uszkodzenia generatorów pryzmatycznych.

# Zasługi

* mag: Hektor Blakenbauer, wpadł w paranoję, z której wyciągnął go głód. Potem zeżarł ainshkera którego pokonał, wyciągnął informacje o Aegis z Ilony, nastraszył młodzików i... skończył z Silurią w różowej mgiełce ;-).
* mag: Siluria Diakon, wyciąga Hektora z paranoi posiłkiem, monologuje do Eis, po czym wyciąga Mariannę z depresji swoimi metodami... i poprawia humor Hektorowi i sobie inspirując Norberta. Zmienia Pryzmat.
* mag: Anna Myszeczka, wpadająca w ciężką paranoję i zarażająca nią Hektora; na szczęście, Mikado wziął ją na sparing. Miecz czy Pistolet - oto jest pytanie. Przegapiła wszystkie prawdziwe problemy.
* mag: Mikado Diakon, tak bardzo próbował się koncentrować wiedząc, że jest na Fazie... że w końcu się dowiedział o generatorach. Wziął Annę na sparing by ją odstresować. Przegapił wszystkie problemy.
* mag: Norbert Sonet, lekarz skwapliwie korzystający z poczynań Silurii i wpuszczający różową mgiełkę zdepresjonowanym * magom Świecy. Mało co go rusza - jest na KADEMie od dawna.
* mag: Ilona Amant, chyba jedyny * mag KADEMu, który puścił sekret do Bestii... Uruchomiła w tajemnicy GS Aegis 0004. Eks Edwina. Dość nieżyciowa, acz kochana. Papla.
* mag: Quasar, ucho wiecznie podsłuchujące cokolwiek się nie dzieje. Gumowe ucho KADEMu. Koordynuje i informuje * magów KADEMu o wszystkich potencjalnych problemach.
* mag: Infernia Diakon, pełniąca szlachetną funkcję kucharki, nadal nie lubi Hektora i złośliwie ironizuje. Jednak przezwyciężyła niechęć... choć zastawiła na Hektora pułapkę czy dwie (rozbrojone przez Silurię)
* vic: Eis, steruje GS Aegis 0004 (i chroni Ilonę), po czym Siluria do niej mówi by poinformować GS Aegis 0003 o tym, co Siluria od niej oczekuje. Korzysta z uszkodzenia generatorów pryzmatycznych...
* mag: Bogumił Rojowiec, strzelił z lekkiego działka strumieniowego w Hektora (myśli, że to efemeryda) i skończył skulony ze strachu przez Hektorowy atak mentalny. Musi uprać spodnie.
* mag: Karolina Kupiec, wesoła terminuska dostarczająca narko... er, 'hedonistyczne środki psychoaktywne' które nie uzależniają. Nie handluje, rozdaje.
* mag: Marianna Sowińska, w głębokiej depresji, wierząca, że są na Esuriit... co zmusiło KADEM do środków specjalnych. Skończyła w ekstazie po środkach Karoliny i działaniach Silurii ;-).

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Poligon wspomagany, gdzie Hektor miał okazję zapolować i zjeść wspomaganego Ainshkera (wynik pracy Kariny i Ilony)
                    1. Skrzydło naukowe
                        1. Instytut Technomancji, miejsce, w którym Ilona Amant niepodzielnie rządzi i wita Hektora z przyjemnością
                    1. Skrzydło medyczne
                        1. Sale regeneracji, gdzie Norbert zainspirowany przez Silurię wpuścił różową mgiełkę. Tam też Marianna spotkała się z (miłym) przeznaczeniem.
                    1. Skrzydło wypoczynkowe
                        1. Pokoje gościnne, gdzie śpi Hektor... okazuje się, że z Anną
                    1. Skrzydło wzmocnione
                        1. Pancerne magazyny
                            1. Magazyn TECH_KN_15, gdzie Siluria monologowała do Eis.
                    1. Układ nerwowy
                        1. Sale symulacji, gdzie Anna i Mikado sparingowali "Miecz czy Pistolet", przez co przeoczyli wszystkie problemy

# Czas

* Dni: 1