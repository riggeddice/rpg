---
layout: inwazja-konspekt
title:  "Atak na sanktuarium Estrelli"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151001 - Plan ujawnienia z Hipernetu (HB, DK)](151001-plan-ujawnienia-z-hipernetu.html)

### Chronologiczna

* [151001 - Plan ujawnienia z Hipernetu (HB, DK)](151001-plan-ujawnienia-z-hipernetu.html)

## Punkt zerowy:

Ż: Co Paulina potrzebowała do projektu 'Aurelia 2.0' z cmentarza, ze wszystkich miejsc na Ziemi?
K: Nie tyle z cmentarza a z parafii koło niego; chodzi o informację o drzewie genealogicznym.
Ż: Gdzie Paulina zauważyła Olgę?
K: W supermarkecie, gdy ta kupowała coś.
Ż: Dlaczego trumna i ciało musiały wpierw zostać emocjonalnie "wygaszone" zanim można je zniszczyć?
K: Ponieważ fala uderzeniowa mogłaby zranić inne osoby z tej samej krwi z uwagi na specyfikę śmierci czarodziejki.
K: Dlaczego Iliusitius nie chce, by Olga dopadła Paulinę?
Ż: Iliusitius nie atakuje magów losowo. On atakuje tych, którzy jego zdaniem powinni być zniszczeni a Paulinę wykorzystał (kiedyś) jako przydatne narzędzie.

## Misja właściwa:

### Faza 1: Katarzyna. Nie Aurelia, nie EIS.

W Powiewie Świeżości, a dokładniej w głębi zamku As'caen wyhodowana została w końcu Katarzyna Kotek. Jeszcze nie w pełni kompletna, zmorfowana ze krwi starszej pani (uzgodnione z nią wcześniej, że dostanie drugie życie) oraz wyhodowanego człowieka Diakonów. Katarzyna ma unikalną umiejętność - doskonała pamięć oraz zbiór wiedzy która była dostępna Aurelii; nie całej, w formie przebić. The librarian. Biblioteka Powiewu Świeżości. Vicinius, świadoma tego, że jest viciniusem.
A nazywa się Kasia Kotek, bo koty to emblemat Kamili Maliniak; na jej cześć. Oczywiście, do dziś Julia nic nie wie.

Paulina po raz kolejny miała dyskusję z Marią; dla Marii nieakceptowalne było to, że Paulina kupiła od Diakonów człowieka (wyhodowanego) i tego człowieka poświęciła. Zdaniem Marii, zabiła osobę. Zdaniem Pauliny, to nie była osoba, tylko ciało. Ale ta dyskusja pozostała nierozegrana; żadna z nich nie jest w stanie przekonać drugiej.

Z biovatu w zamku As'caen (który pochodzi z przeszłości; iteracja "Zaćmienie") wyłoniła sie Katarzyna. 3x-letnia, nieciekawa bibliotekarka. A jej pierwsze słowa to "czy jesteś moją mamą" zadane do Pauliny. Ona odpowiedziała, że nie; Roman powiedział, że on jest. Paulina zaprzeczyła. Katarzyna nie imprintowała się na żadnego z tej dwójki. Gdy jej wyjaśniono, że jest naprawdę autonomiczna, zrozumiała.
Paulina zrobiła jej parę testów; Katarzyna faktycznie posiada część pamięci Aurelii Maus i część pamięci EIS. Czyli eksperyment udany. Katarzyna jest też autonomiczna, niezależna i jest bibliotekarką. Chce nią być. I chce opuścić zamek i pójść do Powiewu, o którym wie niebezpiecznie dużo. Roman chce ją zostawić w zamku na razie, ale Katarzyna chce zobaczyć swój dom.

Zdaniem Pauliny Katarzyna jest lojalna wobec Powiewu. Wyjaśniła też Kasi, że nie o wszystkim powinna mówić. Powiedziała, że Kasia powinna jeszcze zostać w zamku, na co ta odpowiedziała ostro "rozumiem". Katarzyna zaczyna dopiero radzić sobie z emocjami; to jej nowe życie. Nie jest Aurelią, nie jest EIS, jest czymś zupełnie innym, jest Katarzyną... ale część pamięci posiada i to ją formatuje tak a nie inaczej.
Co pamięta? Pamięta, że chciała polaryzować kryształ Quark i to się mogło źle skończyć...

Paulina nie pozwoliła Romanowi dać Katarzynie fantastyki. Roman chce dać Katarzynie jakieś romansidła ludzkie. Nie "Twilight", jakieś takie zwykłe, ludzkie, ze świata ludzi by nadkompensować brak emocji Aurelii. Paulina lekko facepalmowała, ale niech będzie...
Przy okazji, Katarzyna zdradziła, że Roman ma stare playboye które czasami animuje. Trzyma je w cegle, za łóżkiem. 
Paulina dorzuciła trochę Grocholi i Musierowicz. Ludzkich książek, normal stuff. Też kilka takich ze świata magów, autorstwa magów. 
Też dorzuciła kilka bajek, ale dorzucając koncept czym jest bajka.
Z pomocą Marii dobrała książki tak, by (współpracując z Katarzyną) kierować Katarzynę na stabilizację osobowości ale tak, by nie było w Katarzynie tego, co było w EIS czy Aurelii jeśli chodzi o ludzi (10v10->11). Paulinie udało się wywrzeć silny wpływ na Katarzynę w etapie wczesnoformatywnym. 

Kasia wie, że jest viciniusem i jest stworzona. Przyjęła to niepokojąco normalnie i chce zabrać się do roboty. Na razie jej pamięć to okruchy rzeczywistości, nie powiązane nijak ze sobą, nad którymi naprawdę nie ma żadnej kontroli...
Na razie Katarzynę łatwo opanować; nie stawia żadnego oporu. Jeszcze. Paulina jest przekonana, że ten stan się zmieni (Kasia jest zbyt zagubiona by protestować). Roman wierzy, że tak będzie zawsze, ale nie ma zupełnie cierpliwości do Kasi...

Maria zarzuciła Paulinie, że ta się bawi w boga. Paulina zauważyła, od czego odwiodła Romana (użycie osoby martwej). Ale powiedziała też, że gdyby Paulina nie pomogła, Roman poprosiłby kogoś innego - a tak, Katarzynę da się stworzyć jako inteligentną i myślącą istotę; nie jakiegoś bezmyślnego czy okrutnego viciniusa a osobę.
Maria doradziła, by Paulina powiedziała o wszystkim Julii. Julia będzie postawiona w fatalnej sytuacji przez to wszystko. Paulina powiedziała, że najpewniej to zrobi dość szybko, choć niekoniecznie teraz.

### Faza 2: Kapłanka Iliusitiusa w supermarkecie

Paulina, opuszczając Powiew dostała mentalny sygnał od Marii. Maria wysłała jej obraz Olgi Miodownik kupującej znicze w supermarkecie. Maria powiedziała, że może to Paulinę zainteresować; jest tu agentka Iliusitiusa (kiedyś - pomiędzy stworzeniem Olgi jako agentki Iliusitiusa a aktualną chwilą - Iliusitius znowu połączył się z Pauliną wykorzystując ją do zniszczenia czegoś gdzie ich interesy były zbieżne; mimo, że Iliusitius usunął pamięć Paulinie, Maria przywróciła wiedzę).

Paulina poprosiła Marię, by ta obserwowała Olgę a sama poszła w tamtym kierunku jak najszybciej. Jeśli Olga jest w pobliżu, Paulina chce móc jej "pomóc" by nie zrobiła czegoś drastycznego. Lepiej, by Iliusitius miał większe środki, bo z natury nie działa drastycznie jak nie musi.

Maria śledzi Olgę. Olga weszła w bramę; Maria bardzo ostrożnie podążyła z nią i skonfrontowała się z nią Olga. "Czemu mnie śledzisz?". Paulina skróciła dystans i powiedziała, że dlatego, bo ONA poprosiła o to Marię. Olga jest zdziwiona - kim jest Paulina? Paulina rzuciła cichego hinta "nie wiem czemu, ale czuję, że jesteś ważna" i się przedstawiły z Marią. Olga poprosiła, by jej nie śledziły. Jest to niebezpieczne i dla Olgi i dla Marii/Pauliny.

Maria się oddaliła; Oldze to nie przeszkadzało, ale Paulina została na miejscu. Olga uniosła brwi - Paulina jeszcze tu jest? Paulina się uśmiechnęła i powiedziała, że jej pan nie uratuje Olgi, jeśli ona operuje w Kopalinie; Paulina nie chce wymawiać w tym mieście imienia Iliusitiusa (i powiedziała to Oldze). Jednocześnie Paulina dodała, że Olga i ona mają często wysokie podobieństwo celów, więc może porozmawiają.
Olga zaprosiła Paulinę do auta. Mogą wyjechać na jakiś czas z miasta, porozmawiać.

W samochodzie doszło do wymiany informacji. Paulina "nie wie" czemu pamięta, ale pamięta i chce pomóc Iliusitiusowi warunkowo; jak na razie, żaden niewinny nie ucierpiał dzięki jego akcjom. Paulina zauważyła, że Olga pamięta o magii i o tym, że sama była czarodziejką. Pamięta też, że Paulina jest magicznym lekarzem, ale nie wie, że Paulina miała udział w jej stworzeniu. Olga powiedziała Paulinie, że jej problemem jest to, że jest pewna czarodziejka która śledzi starszego pana. Ogląda go. Nie wchodzi w kontakt, ale go obserwuje; jego i jego rodzinę. Paulina nacisnęła - to nie dość, by Olga się tym zajęła. Olga zdradziła, że jest coś jeszcze; weszła w kontakt z pewną siatką ludzi i od nich dostała ten temat. Ma nadzieję go rozwiązać i dowiedzieć się, czy nic temu panu nie grozi. Kto wie, może dzięki temu zdobędzie nowych agentów dla Iliusitiusa (czyt. źródło danych)? A i z perspektywy Hektora będzie wiedziała więcej.

"Tak więc włączając się do tej siatki, powiększę ilość informacji jakie ma On by chronić ludzi. Kto wie, może dodatkowo dostanę medal od mojego przełożonego?" - Olga do Pauliny, w odpowiedzi na pytanie czemu zajmuje się jednym starszym panem.

Olga przyznała jeszcze jedną rzecz - ona wie kto rzuca czar. Co prawda jest tylko człowiekiem, ale potrafi używać srebra a dzięki wiedzy maga wie jak zlokalizować i dojść do tego kto i co. To tylko kwestia czasu i umiejętności detektywistycznych. A dla Iliusitiusa nie ma znaczenia, czy mag krzywdzi jedną osobę, czy wielu; wróżda jest jedna. Sprawiedliwość będzie wymierzona. Olga dodała, że fabrykują dowody w świecie magów i rzucają ich na zniszczenie.

Olga zadała jeszcze pytanie, czy da się przeczytać pamięć Pauliny; czy istnieje taka ścieżka, że ktoś się dowie. Paulina zaprzeczyła. Olga poprosiła o możliwość sprawdzenia a Paulina się zgodziła. Maria wycofała wiedzę Pauliny a Olga weszła w jej pamięć... a dokładniej, Iliusitius to zrobił (zmieszanie krwi kapłanki i czarodziejki). Iliusitius nie znalazł tej pamięci. Spytał też Paulinę o jej intencje - kiedy zdradzi jego istnienie. Tylko wtedy, jeśli Iliusitius będzie chciał skrzywdzić niewinnego albo wykorzystać swoje sposoby działania i metody mając alternatywę nieletalną. Normalnie - nie, ponieważ jakkolwiek Paulina nie zgadza się z metodami Iliusitiusa, zgadza się z tym co on chce zrobić.

Gdy Iliusitius opuścił umysł Pauliny, Olga zaproponowała jej elektrolit.

"Elektrolit jeszcze nikogo nie zabił... w przeciwieństwie do Olgi, ale co tam..." - anonimowa graczka (Kić)

Olga powiedziała o co chodzi - jest pewna czarodziejka śledząca starszego pana i rzucająca czary na... kwiaty na cmentarzu. Olga nie widzi korelacji ani problemów, ale jeszcze obserwuje i jest naturalnie nieufna. Ta czarodziejka nie jest na radarze Olgi, ale organizacja ;-). Olga nie wie, co te czary na kwiaty oznaczają, ale cieszy się, że Paulina może jej to sprawdzić (katalistka / lekarz magiczny / nekromantka). Paulina też się cieszy... ha ha...

Dobrze, Paulina bierze to na siebie. Sprawdzi to.

Olga zauważyła jeszcze, że Paulina jest bardzo odporna na Iliusitiusa. Normalnie takie połączenie krwi i inwazja Jego Światła wypala magowi umysł. Paulina jednak była wzmocniona używając ołtarza Iliusitiusa; jest uodporniona na Światło. Iliusitius może jej przypominać i zapominać, może z nią się komunikować, może dać jej swe dary. Lub wykończyć. W wypadku innych może tylko wykończyć.

### Faza 3: Terminuska na cmentarzu

Olga i Paulina dotarły na Cmentarz Wiązowy. 

Faktycznie, siedzi tam czarodziejka rodu Diakon. Wygląda bardzo niegroźnie; lekko blada, porusza się bardzo ostrożnie. Siedzi i obserwuje. W pewnym momencie wstała i cofnęła się parę kroków, schowała się blisko kapliczki. Niedaleko przeszedł starszy pan z rodziną - to właśnie o tym człowieku mówiła Olga.
Olga zaraportowała kontakt. Zidentyfikowała dwóch nie powiązanych młodych ludzi idących w kierunku czarodziejki. Paulina z ukrycia obserwowała; jeden podszedł i zapytał Estrellę o godzinę. Jak terminuska spojrzała na zegarek, złapał ją za usta i wciągnął z drugim do kaplicy. Paulina spytała Olgę czy to jeden z jej "przyjaciół". Ta powiedziała, że nic o tym nie wie.

Paulina podeszła podsłuchiwać kaplicę. Tam usłyszała dźwięk uderzenia (pięść osiłka w brzuch Estrelli z całej siły, dokładniej), po czym dźwięk darcia materiału. Gwałcić im się zachciało...
Paulina przygotowała zaklęcie Dominacji i weszła do środka, rzucając je w ludzi. Promień czaru objął też Estrellę; terminuska ze spalonymi kanałami pisnęła z bólu i popłynęła jej krew z nosa. Paulina rozpoznała, że to kwestia kanałów. Poprosiła terminuskę, by ta nie krzyczała i odsunęła Zdominowanych osiłków (podczas rzucania czaru poczuła, że przełamała jakiś inny czar). Estrella zwinęła się w kłębek; uderzenie otworzyło niektóre rany. Paulina pobieżnie ją zbadała i wysłała osiłków na przesłuchanie do Olgi. Badanie Estrelli przyniosło dość... straszne rezultaty - czarodziejka niedawno wyszła z ciężkiego stanu i spalonych kanałów (do poziomu poważnie uszkodzonych), a teraz revert. Normalnie nie powinno się w takim stanie wypuścić maga od lekarza...

Terminuska przyznała, że nazywa się Estrella Diakon i jest terminuską Świecy. Przyznała, że hipernet nie działa, ale powiedziała, że lada chwila wróci. Paulina bez problemu przejrzała blef, ale udała, że wierzy. Chce, by Estrella z nią poszła by mogła się nią zaopiekować. Terminuska jest za słaba, by móc iść sama. Plus ma podarte ubranie ;p.

A Olga przesłuchuje osiłków w sposób, w jaki tylko Olga lubi najbardziej.

"Ja jestem lekarzem prawdy, ty jesteś lekarzem ciała i duszy" - Olga filozoficznie do Pauliny o tym, że przy użyciu tortur dowie się więcej.

Pod argumentem Pauliny (nie chce czarować) Olga zdecydowała się zrezygnować z przypominania osiłkom co ich spotkało (torturami potrafi spowodować przypomnienie sobie rzeczy które magia płytko ukryje). Ale Paulina chce się dowiedzieć więcej od samej Estrelli; plus, bardzo nie lubi takich rzeczy. Olga więc przeszła tylko na tortury które nie wymagają ani śladów ani leczenia. Po prostu silny ból.

Estrella powiedziała Paulinie, że musi zadzwonić. Paulina chce się Estrellą zająć i wziąć ją do siebie, ale terminuska odmówiła; wpierw wezwie przyjaciółkę. Zadzwoniła do Netherii i poprosiła, by ta rzuciła wszystko i przyjechała.
Tymczasem jeden (aktualnie nietorturowany) chłopak zaraportował Paulinie, że zbliża się strażnik (a Paulina i Estrella są w kanciapie). Paulina powiedziała to Estrelli z nadzieją że ta zdecyduje się pójść z nią. Estrella poprosiła Paulinę, by ta porozmawiała ze strażnikiem i pozwolił jej zostać - ale bez czarów. Paulina się zgodziła.

Paulina wyszła porozmawiać ze strażnikiem (Grzegorzem Śliwą) i poprosiła go, żeby Estrella mogła poleżeć w kanciapie bo "koleżanka źle się poczuła". Powiedziała, że jest lekarzem. W wyniku konfliktu strażnik się zgodził; dał im spokój.

Potem Paulina zapytała Estrellę czemu tak ciężko ranna terminuska jest na cmentarzu. 

"Czasami po bardzo ciężkiej walce... lub po czymś naprawdę złym, warto przyjść sobie tutaj i zobaczyć po co tak naprawdę walczysz" - Estrella do Pauliny, zapytana czemu jest na tym cmentarzu.

Diakonka dodała, że ten cmentarz jest formą strefy sanktuarium. Jakiś mag wcześniej, przed nią próbował zmienić to miejsce w Sanktuarium (Estrella podejrzewa, że ktoś bliski jemu umarł) a potem czary zaczęły ulegać erozji z czasem. Estrella po prostu przychodzi i ładuje te zaklęcia oraz dokłada własne. Chce takiego spokojnego, przyjemnego miejsca. Nie ma w tym nic więcej.

Maria oczywiście Estrelli nie wierzy. Paulina nie zgadza się z Marią - magowie mają prawo do takich rzeczy. Olga słysząc to co mówi Estrella jest dość zadowolona z działań Diakonki, choć BARDZO sceptyczna co do Sanktuarium - Olga nie przywykła, by magowie robili takie rzeczy i płacili za to z dobrej woli; zgodnie z wiedzą Iliusitiusa jest to raczej anomalia.
Zdaniem Pauliny nawet jeśli Olga ma rację i ma to coś maskować, to jest to dobra rzecz wynikowo i warto to dalej maskować. Olga zgadza się z tym poglądem, to samo Maria.

Olga przekazała Paulinie wyniki swojego "śledztwa" - osobą odpowiedzialną za rzucenie na nich zaklęcia jest najpewniej ktoś podstawiony. Wie, jak wygląda. Olga powiedziała, żeby Paulina zajęła się terminuską, ona znajdzie odpowiedzialnego człowieka. Powiedziała też, że przydałby się jej mentalista. Paulina powiedziała, że pomoże - świetnie. W takim razie jak tylko Olga znajdzie odpowiedzialną osobę, da znać Paulinie natychmiast.

Paulinie jedna rzecz się nie spodobała. W zaklęciu sugestii które przełamała zobaczyła cień aury reklamowej (reaktywne acuity will direct). "Kropiaktorium". Tyle zostało. W końcu jej czar wypalił skutecznie tamto zaklęcie.
Ale reklamowy komponent był zaprojektowany by być maksymalnie niezmazywalnym. Więc przetrwał. Jest też wręcz stworzony do "zrobienia magicznego zdjęcia" i ma unikalny, skomplikowany wzór bardzo trudny do podrobienia.

I na to weszła Netheria Diakon, wezwana przez Estrellę. Słysząc "Kropiaktorium" się rozpromieniła. Tanie, nietrwałe artefakty Adriana Kropiaka i jego artefaktorium. Artefakty imprezowe. Każdy z nich ma unikalny GUID, żeby nie było możliwości zrobić przy ich pomocy krzywdy innym. Tien Kropiak jest bardzo tani i działa masowo; bardzo chętnie współpracuje z ośrodkami władzy. Zgodnie ze słowami Netherii, nie każdy wie, że artefakty mają unikalny guid...

Estrella powiedziała, że Kropiak z radością odpowie na kilka pytań w takim razie. Netheria się zgodziła. A Paulina zrobiła kopię wzoru artefaktu by mieć co pokazać Kropiakowi i zdobyć możliwość współpracy z nim. A Olga kontynuuje szukanie człowieka (podejrzewają przypadkową ofiarę, ale trzeba to sprawdzić).

Paulina i Netheria wzięły Estrellę do samochodu Pauliny i pojechały. Zdominowanych ludzi Paulina zostawiła; chce pod koniec dnia ich podczyścić by nie cierpieli bez potrzeby. Rzuciła na nich czar suppresujący to co zrobili (próba gwałtu) i zostawiła ich by robili co chcą; zostawiła sobie tylko linka do nich by móc ich znaleźć w dowolnym momencie.
Bo po co mają cierpieć? W normalnych okolicznościach NIGDY by tego nie zrobili - to normalni młodzi ludzie z rodzinami.

Paulina nie wie jaki mag to zrobił, ale Paulina już go nie lubi.

Paulina wzięła Estrellę do gabinetu. Tam zdecydowała się - mimo magii i kanałów - pomóc terminusce wyjść z tego. Zgodnie z badaniami to co się stało na cmentarzu cofnęło terminuskę o 3 tygodnie rekonwalescencji. Paulina wykorzystuje przede wszystkim ludzkie techniki, ale dodatkowo ma narzędzie dodatkowe - Netherię. Zapytana, Netheria powiedziała że ona i Estrella są parą. Więcej, są głęboką parą (termin Diakoński; to skomplikowane), więc synchronizują się na poziomie biologicznym i magicznym zachowując swoją odrębność. Dzięki temu Paulina jest w stanie wykorzystać Netherię do pomocy Estrelli.
Netheria nie pozwoliłaby magowi Świecy tego zrobić, by nie przechwycił wzoru Netherii, ale Paulina jest niezrzeszona (dzięki temu Netheria zdąży przekształcić trochę swój wzór by Paulina nie mogła zdobyć nad nią kontroli).

Niestety (porażka) zajęło to do późnego wieczora. Po drodze Olga znalazła już nieszczęsnego bezdomnego który został użyty do tego niecnego celu, ale Paulina powiedziała, że może tym zająć się rano. Ciężka praca pozwoliła skrócić okres rekonwalescencji terminuski do 1 tygodnia.

### Faza 4: Spalony cmentarz

W środku nocy (4 rano) Maria wysłała silny impuls mentalny do Pauliny. Cmentarz płonie. Paulina błyskawicznie wyciągnęła Netherię z łóżka (nie ruszając Estrelli) i powiedziała jej o sytuacji. Netherią to bardzo wstrząsnęło - ten cmentarz jest bardzo bliski Estrelli, to jest "jej" sanktuarium, "jej" miejsce odpoczynku. Paulina też zadzwoniła do Olgi i ją obudziła; trzeba się tym zająć. Maria wysłała Paulinie obraz podpalacza - to ten nieszczęsny, bogu ducha winny strażnik. Teraz jest z nim policja i gość płacze, nie wie czemu to zrobił. On sam zadzwonił po straż i policję (ale przedtem podpalał z benzyną). Paulina spytała Olgę, czy ta jest w stanie coś z tym zrobić żeby mu pomóc, jest w końcu niewinny. Olga powiedziała, by zostawić to jej.

Już na miejscu Paulina i Netheria obserwują jak to wygląda. Ogień był celowany by zniszczyć wszystko co piękne. Nie zamazywanie śladów a zniszczenie. Czarodziejki są wściekłe.
Zdaniem Marii to jest atak na Estrellę. Wszystko, co Estrella kocha zostaje zniszczone (łącznie z samą Estrellą).

Paulina i Netheria chcą wejść na teren cmentarza. Chcą uratować co się da. Na drodze stanął im policjant, ale Olga machnęła identyfikatorem (siły specjalne) i powiedziała, że te dwie są z nią. Policjant pozwolił im wejść na cmentarz. Olga nie weszła z nimi - rozpłynęła się pomagać rannym i strażakom a czarodziejki zostały zająć się zaklęciami.

ESCALATION MODE:
- uratować wszystko z zaklęć co się da; uniemożliwić dalsze zniszczenia
- uratować wszystko z cmentarza co się da; uniemożliwić dalsze zniszczenia
- żeby nikomu nic się nie stało
- żeby magia nie wypełzła na zewnątrz; żeby nie złamać Maskarady

ESCALATION_1: Netheria i Paulina próbują odciąć ogień, ograniczyć go maksymalnie, by nie eskalować zniszczeń. Żywioł próuje poparzyć i skrzywdzić jak najmocniej, zwłaszcza z potężnymi wyładowaniami magicznymi wynikającymi z zniszczonych zaklęć (kwiatów i roślin). Nierozegrane. Sprawdzenie, czy Paulina zdąży zatrzymać plan swój i Netherii by się zorientować w szalejącej magii (nie ogień, magiczny ogień). (zawodowy reactive direct vs 9 na katalizie bez magii: 10v9 -> remis). Wynik:
Paulina i Netheria zaczęły rozwijać zaklęcie odcięcia. Jednak Paulina, katalistka, zorientowała się, że dzika magia pełznie w kierunku Netherii (bo ona rzuciła czar pierwsza). Paulina szybko zmieniła czar, destabilizując go (destabilizacja wspólnego zaklęcia spowodowała rezonans) i pieprznęło w Netherię, ale ta, jakkolwiek lekko ranna po kanałach zorientowała się co się dzieje (lekka rana Netherii).

ESCALATION_1: Żywioł próbuje Skazić strażaków i Podnieść nieumarłych. Uncontain. Paulina próbuje uziemić magię i ją okiełznać do dalszych działań. Netheria używa astraliki do opanowania nastroju. Niestety, Netherii się nie udało. Doszło do erupcji Skażeń i paniki u strażaków; jedyne co Netherii udało się zrobić to przekierować na siebie Skażenie (jej tkanka magiczna jest odpowiednio silna, ale nawet ona nie wytrzyma długo i jest zdjęta z akcji). Paulina w desperacji użyła bardzo silnego zaklęcia. Zsynchronizowała się z lokalną energią, pozwoliła Żywiołowi na skażanie strażaków i budzenie nieumarłych (bo zwyczajnie Skażenie uderzy Netherię) i poświęciła wytrzymałość magiczną Netherii (ciężka rana magicza, za to #2), za to puściła potężną energię negatywną w ziemię i obudziła armię minionów używając tej energii i odgrodziła oraz containowała magiczny komponent. Netheria się rozkaszlała i przewróciła; jest ciężko Skażona, ale może działać.

ESCALATION_1 fallout: Żywioł opanowany (#3 do testów Pauliny), Netheria ciężko Skażona, nieumarli pod kontrolą Pauliny, Skażenie obszaru.

ESCALATION_2: Netheria próbuje znaleźć i przekonać jakiegoś terminusa, by lokalnie działający właśnie terminusi pomogli jej i Paulinie zamiast robić swoje. W tym czasie Paulina próbuje opanować energię i jej "nie wypuścić na wolność". Netheria pobiegła i bardzo szybko przekonała terminusów, że sytuacja jest tymczasowo opanowywana przez Paulinę i nią (Millennium # Skażona Diakonka robią cuda). Paulina tymczasem nie jest w stanie opanować całości tej energii, ale trzyma, powoli się Skażając. Skażenie było jeszcze daleko od granicy Wzoru Pauliny gdy poczuła wsparcie terminusów. Mogła puścić.

ESCALATION_2 fallout: Żywioł pod kontrolą, Netheria ciężko Skażona (3), Paulina Skażona (2), terminusi współpracują, nieumarli pod kontrolą Pauliny, Skażenie obszaru, żywioł (ogień) kontrolowany.

ESCALATION_3: Netheria i Paulina próbują przesunąć kwiaty astralnie (żeby uratować tyle z zaklęć ile się da). Terminusi tego nie muszą rozumieć, żywioł to ich problem. Skażenie próbuje zniszczyć jak najwięcej wszystkiego (v17).
Bonusy: 2 za terminusów, 2 za Skażenia, 1 za Netherię. Paulina: dodatkowy test, reactive acu#ht sbt. Wyczuła coś pod ziemią, jakąś energię, coś bardzo nie w porządku. Przyjrzała się jej dokładnie - kosztem rozproszenia uwagy (-2). To jakiś potężny, energetyczny byt; mag, który kiedyś żył a potem umarł, ale nie został prawidłowo zabity ani zniszczony (a teraz się nie da). Byt ten musi zostać wpierw zneutralizowany energiami spokoju i 'tranquility' a dopiero potem można będzie rozwiązać problem i zniszczyć ów byt do końca.

Tym ważniejsze stało się uratowanie jak największej ilości kwiatów. Szczęśliwie wspomagana przez terminusów SŚ i przez Netherię udało się to osiągnąć naprawdę szybko. Niewiele kwiatów zostało zniszczonych (sukces). Sytuacja opanowana, tajemniczy byt pod ziemią, rozbudzony przez energię negatywną może powrócić do snu...

ESCALATION_3_fallout: Netheria ciężko Skażona (3), Paulina Skażona (2), Paulina musi porozmawiać z terminusami, ludziom nic się nie stało, cmentarz jest całkowicie zneutralizowany.

END_OF_ESCALATION_MODE.

Paulina, Skażona i czująca się fatalnie została poproszona o rozmowę z dowódcą akcji. Zastali ją, gdy pomagała jednemu z rannych strażaków. Z ciężkim sercem, Paulina zostawiła strażaków i poszła do dowodzącego akcją. A tam powitał ją... Kermit Diakon.

- "Co za spotkanie, pani doktor" - wyraźnie zdziwiony Kermit widząc, że ZNOWU Paulina.

Kermit zadał kilka prostych pytań, ale jako, że Paulina w sumie nie wie nic dla niego ciekawego to spojrzał na nią ze współczuciem i odesłał ją do domu. Ma następnego dnia się stawić u niego w biurze jak już będzie gotowa i w lepszej formie. 
Paulina poprosiła Kermita, by ten dowiedział się od Grzegorza Śliwy zaklęcia w taki sposób, by to nie skrzywdziło samego Śliwy. Żeby nie robić dodatkowych problemów nieszczęsnemu strażnikowi który wyraźnie był pod kontrolą.
Kermit się zgodził.

Paulina i Netheria wróciły do Estrelli i resztę nocy spędziły w łóżkach. I Paulina w najbliższym czasie unika srebra jak ognia.

# Streszczenie

Katarzyna Kotek została stworzona jako homunkulus Aurelii/EIS przez Paulinę i Romana. Spięcie między Pauliną i Marią o Katarzynę. W supermarkecie Maria wykryła Olgę Miodownik - agentkę Iliusitiusa; Paulina zaryzykowała i przyznała Oldze, że pamięta Iliusitiusa. Połączyły siły - Olga śledzi działania Estrelli na cmentarzu, która "może krzywdzić ludzi". Pojechały obie - tam Estrellę (wciąż fatalny stan po przesłuchaniu Andrei) napadło dwóch osiłków i Paulina ją uratowała. Osiłki były zaczarowane przez tanie zabawne artefakty z Kropiaktorium. Paulina pomogła terminusce; cmentarz to jej "sanktuarium" jakie ona kocha. W nocy - cmentarz spłonął; Paulina z trudem z Netherią dały radę to powstrzymać. Niestety, większość "dobrej magii" na cmentarzu została zniszczona, działania pokoleń. Coś jest pod spodem i to się obudziło.

# Zasługi

* mag: Paulina Tarczyńska, bohaterka trzymająca Węzeł, heroiczny lekarz i twórczyni Kasi Kotek. Sojusznik Iliusitiusa.
* vic: Katarzyna Kotek, stworzona jako bibliotekarka Powiewu. Ma przebicia wiedzy Aurelii i EIS. Nie wie, czym jest, ale chce pełnić swą rolę. 
* mag: Roman Gieroj, który nadal kontynuuje projekt 'wiedza Aurelii bez wad jej charakteru'. Zupełnie nie umie podejść do "Aurelii 2.0".
* czł: Maria Newa, dobra pamięć, kompetentna, ale nie dość by śledzić Olgę. Ma lekko przytarty nosek.
* czł: Olga Miodownik, kapłanka Iliusitiusa w siłach specjalnych planująca poszerzenie swej siatki o jakąś nieznaną grupę ludzi (czyt. Skorpion).
* vic: Iliusitius, który konsekwentnie poszerza swoje wpływy i zdecydował się wejść w sojusz z Pauliną. Nie celuje w Estrellę. Kontroluje Olgę.
* mag: Estrella Diakon, wciąż ranna terminuska z silnie ograniczonymi kanałami * magicznymi (leczy się); pielęgnuje stary Cmentarz Wiązowy. Ukochana Netherii.
* mag: Netheria Diakon, która udowodniła, że czarodziejka imprezowa też może być bohaterką. Ukochana Estrelli.
* czł: Maciej Tykwa, kucharz i silny młody * człowiek (33), który pod wpływem * magii dominacji spróbował zgwałcić Estrellę.
* czł: Mateusz Tykwa, mechanik i złota rączka; brat Macieja. 31 lat, pod wpływem * magii dominacji spróbował zgwałcić Estrellę.
* czł: Grzegorz Śliwa, strażnik na cmentarzu i cel zaklęcia. Niewinny, ma problemy... a ma dobre serce (pomógł Paulinie i Estrelli). Olga mu pomoże.
* mag: Kermit Diakon, terminus dowodzący akcją opanowania pożaru na cmentarzu, który udowodnił, że terminus też ma serce. 
* mag: Adrian Kropiak, właściciel "Kropiaktorium" (artefaktorium), miejsca gdzie sprzedawane są tanie artefakty zabawowe z unikalnymi GUIDami.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Trzech Myszy
                        1. Supermarket "Smakołyk"
                    1. Dzielnica Owadów
                        1. Gabinet lekarski Pauliny
                    1. Stare Wiązy
                        1. Cmentarz Wiązowy
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen