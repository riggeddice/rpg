---
layout: inwazja-konspekt
title:  "Byli sobie przestępcy"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151110 - Romeo i... Hektor (HB, AB, DK)](151110-romeo-i-hektor.html)

### Chronologiczna

* [151110 - Romeo i... Hektor (HB, AB, DK)](151110-romeo-i-hektor.html)

## Punkt zerowy:

Ż: Zawód zaufania publicznego - osoba, która została podejrzana o mroczne sprawy?
D: Komendant straży pożarnej.
Ż: W jaki sposób ta osoba bardzo pomagała Hektorowi podczas rozwiązywania problemów prawnych?
K: Kompetentnie i w konsekwentny sposób robi swoje; pomógł Hektorowi wyśledzić piromantę w szeregach straży.
Ż: Jakim niegodnym czynem ta osoba się teraz kompromituje?
B: Bierze w łapę przy odbiorach budynków; zdarza mu się przymknąć oko. I w ogóle.

## Misja właściwa:
### Faza 1: Prokuratura aresztowana.

Dzień 1: 

Komendant straży pożarnej dał cynk Alinie; coś się dzieje. A Alina bezpośrednio współpracowała z nim przy poszukiwaniu piromanty; miała być jego osłoną na wypadek zemsty.

Miejsce zbrodni. Siąpi deszcz. Policja wbija do domu komendanta straży pożarnej. Dowodzi Hubert Rębski - antyterrorysta. Wyprowadzili ogłupiałego komendanta a potem resztę rodziny. Weszło forensic, potem weszła grupa saperów. Alina podbiła do Huberta; on jednak nie chce z nią rozmawiać. Powiedział tylko, że prokuratury to jeszcze nie dotyczy, jest to poza zakresem choć na pewno Hektora by zainteresowało. Ale on nie będzie współpracował.

Komendant mieszka w domu; ma własną posesję, dom, psa (odpowiednio spacyfikowanego). Alina obudziła Patrycję telefonicznie; niech Patrycja obudzi bandę forensic i przyśle ich tutaj. Aha, i niech dowie się o co chodzi.
A Hubert ma wyraźne anse do Hektora. Alina ma zamiar się tu wbić.

Kleofas Bór zadzwonił do Hektora, obudził i powiedział o sytuacji. Hektor powiedział, że ubiera się i jedzie. Kleofas zadzwonił do Aliny i powiedział jej, że załatwił dywersję. Tymczasem Alina weszła na teren domu. Poszła do forensików, pokazała dokumenty, poprosiła o użyczenie sprzętu i weszła. Dowiedziała się, że szukają dokumentów i znaleźli bombę. 

Przyjechali tu po papiery. I żeby nie dało się ukryć korupcji. Jednym z podejrzeń jest to, że Hektor i Pafnucy są w zmowie korupcyjnej. Jednak zaczęli się interesować tym, czemu Alina zadaje te pytania... Alina szybko zadzwoniła do Hektora zanim ją capnęli. Powiedziała, że Hektor jest współpodejrzany o korupcję. I twierdzą, że mają dowody. Hektor usłyszawszy to, wrócił. Nie jedzie spotkać się z Hubertem. To byłoby niebezpieczne.

Tymczasem Hubert zgarnął Alinę; oskarżył ją o potencjalne manipulacje dowodami i ma zamiar przekazać informacje prokuratorowi generalnemu. Zgarnął Alinę, żeby ją przesłuchać.

"Leonidasie. Czy zamawiałeś pizzę?" - Edwin, po tym jak pierwsza grupa antyterrorystów wpadła na zabezpieczenia...

Szczęśliwie Leonidas rozpoznał w nich antyterrorystów. Edwin nie wiedział o co chodzi W OGÓLE. Kolejna grupa już zamiast na zabezpieczenia wpadła na mindcontrol i zgłosili, że wszystko w porządku. Tymczasem Edwin i Leo muszą poskładać antyterrorystów... ale jako, że to potrwa, potrzebują sztucznych ludzi (lub tanich Zajcewów). Hektor skontaktował się z Dianą Weiner; ona dostarczy szybko odpowiednie osoby - w zamian za polityczno-prawną przysługę podobnego kalibru w przyszłości.

"O, kuzyn Leo. Co ty tu robisz?" - Hektor.
"Przyspieszony kurs anatomii..." - Leo.

Hektor zadzwonił do Arkadiusza Klusińskiego, prokuratora generalnego. Poprosił o sprawdzenie - Klusiński jest magiem rodu Sowińskich. Klusiński stwierdził, że spojrzy; wysłanie sił do Rezydencji było czymś czego by nie autoryzował. 
Antyterroryści zwinęli Hektora i papiery.

Tymczasem w kazamatach nienawiści (areszt śledczy) Hubert przesłuchuje Alinę. Hubert idzie ostro i twardo, jak Hektor. Alina próbuje wychytrzyć się i powiedzieć mu nieprawdę. Rozpłakała się (na zawołanie), i powiedziała, że weszła z własnej inicjatywy i ostrzegła Kleofasa. Pracowała wcześniej z nieszczęsnym komendantem; on ją wezwał. Z jej punktu widzenia Hektor był z tym nie powiązany.

Niestety, ku wielkiemu nieszczęściu Huberta, okazało się, że dokumenty korupcyjne Hektora są podrobione. Wyraźnie podrobione. Wystarczająco wyraźnie. Hubert wypuścił Alinę i Hektora i dał im wolną rękę...

Hektor przejął sprawę. Sprawa zmienia się w dochodzenie.

### Faza 2: Prokoratura w natarciu

Dzień 2:

O 7:30 rano Hektor budzi nieszczęśliwego Leonidasa. Zaprosił go na śniadanie. Poprosił Leonidasa o pomoc w śledztwie. Hektor ma Plan - niech ktoś przejrzy WSZYSTKIE dokumenty. Szczęśliwie w biurze już czeka Kleofas Bór * człowiek, który jest zawsze kompetentny i gotowy na wszystko dla swojego prokuratora. Domyślił się potrzeb Hektora i ściągnął wszystkie papiery. Jak dostał polecenie zdobycia bilingów i maili, ucieszył się. Zagoni ludzi do pracy.
Kinga, asystentka Hektora, została zagoniona do załatwienia Hektorowi wizyty w areszcie śledczym. Założenie proste: Pafnucy jest niewinny.

Leonidas z ponurym spojrzeniem ogarnął dokumenty i rzucił czar skanujący; próbuje dowiedzieć się co i jak zostało zrobione (pod kątem oszustw). Podczas pracy zaklęcia weszła Kinga; Hektor szybko zamknął za nią drzwi i ona się rozkrzyczała. Zanim zdążyła coś zrobić, Hektor ją Zdominował. Ma przynosić kawę, ignorować wszystko i nikogo nie wpuszczać. Hektor "nienawidzę tego robić" Blakenbauer.

Faktycznie, dokumenty okazały się ciekawe. Prawdziwe były w większości rachunki i "normalne" dokumenty. Sfałszowane były dokumenty o bombie (choć część jest z internetu). Dokumenty odnośnie bomby i odnośnie tego gościa (Kimaroja) są w stanie się same obronić. Ale dokumenty z Hektorem są wyraźnie sfałszowane. Niewprawnie.
Kinga weszła i dała Hektorowi wielki bukiet kwiatów. Na liściku Szymon Skubny. Hektor podziękował bukietowi kwiatów (zakładając mylnie że jest tam podsłuch) i wyrzucił go rękami Kingi. Liścik brzmiał "z wyrazami szacunku dla chytrego przestępcy uczciwy obywatel Szymon".

Grupa specjalna dostała nowe zadanie - dowiedzieć się wszystkiego o panu Kimaroju co da się dowiedzieć. Alina została wezwana przed oblicze Edwina. Niezbyt szczęśliwa, dowiedziała się, że ma udawać zmiennokształtną płaszczkę jako Kleofas Bór. Bo Hektor potrzebuje. Alina przeszła przez Kingę i dotarła do Hektora, gdzie udowodniła, że jest zmiennokształtna; Hektor wpuścił ją do gabinetu. Hektor zlecił Alinie ("Milusiowi") zadanie - zaaranżować spotkanie Kimaroja z komendantem straży Pafnucym Zieczarem. 

Podczas rozmowy z Hektorem Alina zaproponowała, że zmieni się w jedną z jego agentek - Aliną Bednarz. Czyli Alina zmieniła się w Kleofasa by udawać zmiennokształtnego który zaraz zmieni się w Alinę. Leonidas oraz Hektor z Aliną przebraną za Alinę ruszyli do aresztu śledczego. Tam - bardzo nieszczęśliwy Pafnucy. Alina pobrała jego materiał. Potem porozmawiała z Pafnucym - nic nie wie, nic nie rozumie, nic nie wie o bombie. Potem Alina skończyła, wyszła i wrócił Hektor i Leo.

Leo poszedł jako adwokat. Pafnucy się złamał - powiedział, że nie wie o co chodzi, nie kojarzy, nic nie zrobił, ale tego Kimaroja kojarzy z przeszłością. Kiedyś, bardzo dawno temu, uczestniczył w nielegalnych adopcjach i Kimaroj się dowiedział. Szantażował go i zmusił do pomocy w szmuglu ludzi przez granicę i do Polski. Bomba mu się skojarzyła, bo się z Kimarojem rozstali po tym, jak niewielka bomba uszkodziła ciężarówkę mająca być ostrzeżeniem - wszedł na czyjś teren. Potem już nie utrzymywali kontaktu. Pafnucy naprawdę nie wie czemu teraz...
Alina za lustrem weneckim (przy niepisanej zgodzie Hektora) studiuje wzorce zachowania Pafnucego.

Hektor potrzebuje kogoś od materiałów wybuchowych. Padło na Marcelina - który powiedział, że ściągnie mu przyjaciółkę. Hektor wzgardził przyjaciółką; Marcelinowi się wypsnęło, że ona (Riksza Zajcew) jest ekpertem od materiałów wybuchowych i wszystkiego co wybucha; to ją zawsze fascynowało. To ona rzuciła Marcelina i przekazała go Inferni Diakon (Zajcew). Mogłaby zeznawać na korzyść Marcelina, ale nie chce jej się. Za mało ciekawe. Jednak Hektor przeważył - Marcelin przyjedzie i przeanalizuje bombę i nie włączy Rikszy; za to Wanda będzie zwiedzać gabinet prokuratora.

Marcelin dotarł do bomby i doszedł do tego, że bomba jest przetransportowana. A na pewno ma na sobie aurę magiczną. I w ogóle jest zrobiona niewprawnie i używając magii. Z achtungów. Kupionych przez paragony i podłożonych prokuratorowi. Alina wzięła to na siebie; grupa śledcza zwinie to i usunie ślady zostawiając "zwykłą bombę". Marcelin przeszedł się jeszcze przez dom, szukając śladów aury magicznej; znalazł aurę magiczną w miejscu gdzie były dokumenty. Czyli najpewniej podłożono je magicznie.

W czasie, jak Zespół tym się zajmował, grupa specjalna szukała danych o Kimaroju. Mięli trochę czasu, pogrzebali...
Bogdan Kimaroj jest byłym alfonsem; handlował ludźmi, przemyt, wszystkie różne fajne rzeczy. Z biegiem czasu wyprowadził sobie "Wyżerkę", motel / restaurację przy drodze. Mieszka sobie jak król i jest szczęśliwy. Pełni usługi drapieżniejszym od siebie.
Oczywiście, Hektor radośnie wysłał Alinę i Leonidasa. 

Alina (jako Pafnucy) i Leonidas weszli do "Wyżerki". Leo jako prawnik (adwokat) "Pafnucego", "Pafnucy" jako zrozpaczony strażak jakiemu zrobili najazd na chatę. Spotkanie z Bogdanem Kimarojem; ten się zdziwił. Powiedział "Pafnucemu", że zgodnie z umową zostawił go w spokoju. Alina zaczęła jęczeć, że jej weszli na chatę i chcieli wrobić Pafnucego oraz Bogdana. Kimaroj się zdrażnił - nie chce tego słuchać. Nie chce mieć z tym nic wspólnego. "Ale już i tak cię wrabiają". Kimaroj powiedział, że obroni SIEBIE; Pafnucego ma gdzieś.
Przy okazji Alina zauważyła że Kimaroj dostał wielki bukiet róż. Od Szymona Skubnego. Z podpisem "Od szanownego obywatela do przestępcy; prokuratura dobierze się do złych ludzi."

Ma Skubny tupet.

# Streszczenie

Znajomy komendant straży pożarnej został oskarżony o konstrukcję maszyny piekielnej; Hubert Rębski chce aresztować Hektora. Diana Weiner uratowała sprawę za sporą polityczno-prawną przysługę. Ktoś podrobił dokumenty (wulgarnie trywialnie) oskarżające Hektora o korupcję. Część dokumentów jednak jest prawdziwa - powiązanych z Kimarojem. Bardzo dawno temu ów komendant i Kimaroj przemycali ludzi z Ukrainy; potem przestali. Hektor skupił uwagę na tej sprawie.

# Zasługi

* mag: Leonidas Blakenbauer, skutecznie udający prawnika i rozpoznający dokumenty prawdziwe od fałszywych. Też: podaje kończyny Edwinowi.
* mag: Hektor Blakenbauer, aresztowany po tym, jak ktoś próbował nieudolnie go wrobić w aferę korupcyjną. Niewinny! Zaczyna kontrśledztwo.
* vic: Alina Bednarz, która ostrzegła Hektora, dała się aresztować a potem puściła maszynę w ruch rozmawiając z "gangsterem" Bogdanem Kimarojem.
* mag: Arkadiusz Klusiński, prokurator generalny. Mag rodu Sowińskich, który powiedział Hektorowi, że on nie stoi za tym wszystkim co go spotkało.
* mag: Diana Weiner, która z radością za przyszłą przysługę dostarczyła Hektorowi sztucznych ludzi by kupić czas Edwinowi i Leo na poskładanie antyterrorystów...
* mag: Edwin Blakenbauer, składający antyterrorystów wbijających do Rezydencji do kupy...
* czł: Pafnucy Zieczar, komendant straży pożarnej, który twierdzi, że nic nie wie; ktoś chce wrobić go chyba za dawne czasy gdy pomagał Kimarojowi z przemytem ludzi.
* czł: Bogdan Kimaroj, kiedyś alfons i gangster, teraz "na emeryturce" i właściciel "Wyżerki". Przeciwnik Skubnego. Zaczyna działania defensywne; ktoś chce go wrobić.
* czł: Szymon Skubny, który wysłał kwiaty zarówno Hektorowi jak i Bogdanowi Kimarojowi jako "szanowny obywatel". Czerpie mnóstwo radości z tej sprawy.
* czł: Kinga Melit, asystentka Hektora, którą zdominował zaklęciem mentalnym i zmusił do ignorowania całej sytuacji. Nawet nie rozlała kawy.
* czł: Hubert Rębski, prowadził akcję wejścia do komendanta Pafnucego Zieczara i podejrzewał go o współpracę z Hektorem. A, i aresztował Alinę.
* czł: Kleofas Bór, zawsze dziarski, rześki i gotowy - zwłaszcza do budzenia innych ludzi na życzenie Hektora.
 
# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Dzielnica Kwiatowa
                        1. Dom komendanta straży pożarnej
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
            1. Powiat Czelimiński
                1. Czelimin
                    1. restauracja / motel Wyżerka  