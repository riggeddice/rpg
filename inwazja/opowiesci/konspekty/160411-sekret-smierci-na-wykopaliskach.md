---
layout: inwazja-konspekt
title:  "Sekret śmierci na wykopaliskach"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160303 - Otton zabija Zetę (AB, DK)](160303-otton-zabija-zete.html)

### Chronologiczna

* [160229 - Siedmiu magów - nie Mausów (AW)](160229-siedmiu-magow-nie-mausow.html)

### Logiczna

* [160202 - Wolność pająka fazowego (KB, LB)](160202-wolnosc-pajaka-fazowego.html)

### Inne

Kończy się przed misją:
* [160309 - Irytka Sprzężona (HB, KB, LB)](160309-irytka-sprzezona.html)

## Kontekst misji

- Ktoś napuszcza na siebie Świecę i KADEM.
- Ktoś napuszcza na siebie Kurtynę i Szlachtę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Działania archeologiczne Dominika Bankierza pod uważnym okiem Inkwizytor Adeli Maus.
- Na wykopaliskach ucierpiał Bankierz i w komie jest Oktawian.

## Kontekst ogólny sytuacji

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin "szuka" Tymotheusa.
- Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).

## Notatki Kić (jako Andrea)
Ludzie/magowie:
- Salazar Bankierz - nic nie pamięta, wymazał sam siebie, on powiedział Annie o poligonie
- Oktawian Maus - ! szlachciur, potencjalnie zmanipulowany, fanatyk, wszystko dla Mausów, według własnych słów wysoko w hierarchii
- Ozydiusz Bankierz - może nie całkiem zły, ale wciąż dupek
- Aurel Czarko - opracaował włam na KADEM, kto za nim stoi? Szlachta?
- Anna Kozak - omg. świadek jak rzadko. była na poligonie
- Dominik Bankierz - magiczny lekarz, mag szlachty, któremu zaufał Salazar. Razem z Wiktorem i Oktawianem byli w Kotach podczas Spustoszenia.

Inne:
- Pancerz - zakodowany przekaz zawierał kołysankę, lekko zmodyfikowaną wobec "normalnej" wersji śpiewanej przez magów. Najpewniej kod kontekstowy. Wyjaśnienie: kołysanka w wersji łączącej Jadwigę i Aurelię Maus. Ale czemyu ta piosenka w tym kontekście?

Problemy/pytanie:

- Dlaczego Weiner był celem "trzeciego" Spustoszenia? (?)
- Dlaczego wydział wewn nic nie robi w sprawie ataku na KADEM? (?)  Potencjalnie szef jest w Szlachcie. Ale czy to jest wytłumaczenie? Była mowa o ukrytej sile - co nią jest? Może im służy?
- Czemu Tamara jest celem? bo stała na drodze?
- Tajemnice Oktawiana Mausa.
- Jakim cudem wszyscy eksperci od Spustoszenia zniknęli tak szybko i tak skutecznie?
- Milena Diakon - na pół Maus - co zrobić?
- Wykopaliska - prowadzone przez Bankierza ze Szlachty, mogą doprowadzić do odkrycia istotnych dla Mausów faktów.

## Punkt zerowy:



## Misja właściwa:

Dzień 1:

Andrea ciągle w szoku. Oktawian Maus w komie. Adela Maus zniknęła. Dominik Bankierz - nie żyje. 
Aleksander Sowiński - którego Andrea podejrzewa o to, że należy do Szlachty - dał jej za zadanie dowiedzieć się co do cholery się wydarzyło. On też jest w szoku. Andrea operuje jako pełnoprawny członek Wydziału Wewnętrznego - jest to coś bardzo interesującego Świecę.
Na miejscu są Wojmił Bankierz, z ramienia Rodu (uwaga: nie Świecy, nie jest magiem Świecy) i Aneta Rainer z ramienia Świecy. Mausowie nikogo nie wysłali. Sowiński nie wie czemu.

Wykopaliska miały miejsce w Lesie Stu Kotów, miejscu, które kiedyś było bardzo niebezpieczne; dlatego właśnie Kopalin znajduje się tu gdzie się znajduje. Nic jednak nie wskazuje na to, że miało dojść do aż tak strasznego odkrycia. Miejscem badania było opuszczone podziemne domostwo magów - Mausów. Ziemianka sprzed czasów powstania Karradraela (wiekowa). Miejsce to było zakopane i "utracone" - Dominik Bankierz przeprowadzał ekspedycję archeologiczną a Oktawian go zniechęcał. Andrea nawet pamiętała, że seiras Maus mówił o istnieniu tych wykopalisk i wysłał osobiście Adelę by się temu przyglądała...

Rozkazy Anety Rainer (dane z rozkazów Świecy): dowiedzieć się co się stało. Contain the problem. Dowodzi operacją. Teren Wykopalisk oficjalnie należy do Świecy; tak właściwie powinien należeć do Rodu Maus (ich ziemianka). W praktyce, nikogo nie interesował. Do czasu Wykopalisk i zniszczenia. 
Bankierz, który padł to Bankierz ze Świecy.

Andrea skontaktowała się z tien Rainer. Aneta - lojalistka - jest chętna do współpracy. Wyjaśniła, że Wojmił Bankierz ma bardzo wrogi stosunek do Mausów i ma on podejście "komornika" - Bankierz umarł, trzeba zapłacić. A najlepiej jak zapłacą Mausowie. I dodadzą odszkodowanie. Bankierza nie interesuje nic, co nie jest powiązane z odszkodowaniami (najlepiej płaconymi przez Mausów; jak się nie da, przez SŚ).
Andrea natychmiast skupiła się na tym, by znaleźć coś prawniczego - czy Dominik Bankierz miał wszystkie pozwolenia itp. Andrea poświęciła (1 water low) i załatwiła prawników. Powiedziała też Anecie, że jest wsparciem dla niej (podwładną) oficjalnie, by nie antagonizować Bankierza. Bankierz nie musi wiedzieć o działaniu i zainteresowaniu Wydziału Wewnętrznego.

I Andrea poszła na teren Wykopalisk, chcąc zobaczyć jak to wygląda i o co tu chodzi... Andrea wyjęła też z Wydziału Wewnętrznego dodatkowe artefakty wspierające badania (1 void_low).

Andrea dostała telefon od Krzysztofa Wieczorka. Swojego "przybranego ojca", lokalnego bandziorka z Myślina. Usłyszała lament i narzekanie. Nawet stara Czajka sprzedała swoją ziemię - ziemię, jaką Wieczorek bardzo chciał kupić - niejakiemu Arturowi Janczeckiemu. I Janczecki skupuje grunt w Myślinie na potęgę, buduje się też. Dodatkowo, przyjeżdżają pociągi których nie ma w rozkładach a jak Wieczorek wysyła swoich ludzi by dowiedzieli się "co jest", to oni wracają że "no, wszystko w porządku". Magia. A że Andrea chciała być zawsze poinformowana o takich sprawach i że to zaczęło się stosunkowo niedawno...
Andrea powiedziała mu, że zrobi co może - chwilowo ma sprawę, ale żeby wiedział, że się tym jakoś zajmie.

Andrea szybko zrobiła strzał do archiwów SŚ używając Wydziału. Czy dzieje się coś w tamtym obszarze? Odpowiedź brzmi "tak, Jan Weiner zgłosił rozbudowę kolektorów interradiacyjnych z ramienia Rodu Weiner". Andrea połączyła się szybko z magiem Weiner w Wydziale Wewnętrznym, swoim kolegą, i spytała o co chodzi w tej operacji którą zgłosił Jan Weiner. Ona nie chce go kłopotać, a przecież pracowała z nim (a nawet mu córkę - Julię - uratowała). Kolega wyjaśnił Andrei, że... w sumie to nie ma sensu. 
Wszystko wskazuje na to, że chodzi o inhibicję Rdzenia Interradiacyjnego; niespecjalnie ma to sens, bo sam Rdzeń nie będzie działać przy tak niskim stężeniu ludzi jak jest w Myślinie. Ale Jan Weiner - według papierów - planuje też eksperymenty do których będzie dokładał by młodych magów uczyć korzystania z techno-pryzmatu. Andrea - wiedząc, CO tam jest - docenia plausible deniability. Ale też ciekawi ją, czemu teraz i czemu tak. Czyżby coś się zmieniło? A może wykrył kolejny wyciek?

Tak czy inaczej, Andrea się pożegnała z kolegą i doszła na Wykopaliska. A tam - ostra sytuacja. Aneta i Wojmił się kłócą o użycie narzędzi; jeśli Wojmił użyje swoich to dowie się jak zginął Dominik. Ale ogólna prawda co tu się stało zostanie utracona. Jeśli jednak Aneta użyje swoich, dowie się co się STAŁO ale Wojmił nie dowie się niczego. 
Andrea nie rozumie o co chodzi. Krótki strzał do jednego z magów; okazuje się, że aura jest tu Skażona i niestabilna; wszyscy boją się w jakikolwiek sposób ingerować. Dane są poutracane i niestety skutecznie. 

W tej sytuacji Andrea nie ma wyjścia. Nie może wejść cicho. Weszła twardo; Wydział Wewnętrzny przejmuje sprawę. Zażądała odpowiedzi od Anety, potem powołując się na Ozydiusza zażądała ich od Wojmiła. Uciszyła go i zdominowała kompletnie. Kazała przynieść SWÓJ sprzęt, dużo lepszy.
Andrea przejrzała aurę przy użyciu swojego sprzętu. Katastrofa. Całkowite echo w echu. Faktyczna magiczna katastrofa, odpowiednik granatu przy zwłokach. Strasznie trudno będzie to rozplątać a i nici tego co się stało są nieczytelne. Mimo, że Świeca już stabilizowała aurę by zatrzymać odpływ. Niestety, ratownicy też uszkodzili aurę bo próbowali ratować (nikt poza Wojmiłem nigdy nie ma do ratowników o to pretensji).

Andrea kazała Biance przeprowadzić niedestruktywne badania wymieniając kilka z nazwy. Kazała też najlepszemu kataliście Bankierza pomóc Biance. Celem Andrei jest:
- ślady tego co się stało z Oktawianem
- co tu się stało?
- gdzie jest umysł Mausa?
- czy są jakiekolwiek ślady Adeli?

Stopień STARTOWY przeciwnika: 8#6#6#3#6#2 = 31
Stopień Wydziału Wewnętrznego: 6#6#2#9#3#2 = 28

Bianka wyizolowała 'strand' który jest nietypowy. Andrea kazała jej kontynuować analizę (#czas). Bianka powiedziała, że tu aktywnie jakiś mag zafałszowywał aurę; to było więcej niż tylko wypadek. Zainteresował ją nietypowy strand, nie umie go jeszcze określić, ale umie go wyizolować by go zbadać...
Wojmił się ucieszył. To potwierdza, że ktoś będzie płacił. Wstępnie zgodził się współpracować ze Świecą (zwłaszcza gdy usłyszał magiczne słowo 'Ozydiusz').

Andrea wykonała krótki telefon do Krzysztofa mówiąc, że miał rację - to magowie, na razie niech Krzysztof trzyma się z daleka. Tak daleko jak tylko może. Andrea wie, że jego przede wszystkim martwi ziemia i utrata wpływów... a to, że magowie tam są jest dlań drugorzędne.

Dzień 2:

W okolicach 7 rano Andrea się obudziła. Poczuła... impuls aury. Coś niepokojącego. Wychodzi gotowa do reakcji. Bianka powiedziała, że ma ten strand i udało jej się go zidentyfikować. Zamarła. Andrea zesztywniała. Natychmiast kazała wszystko przerwać. Przypomniała sobie czym jest ta subtelna emanacja aury. Rozpoznała strand Spustoszenia.
Andrea krzyknęła, że nie wolno nic więcej robić. Natychmiast przesłała Anecie (rozespanej) sygnał Spustoszenia. Ta zamarła i odesłała wyostrzony obraz hipernetowy - Bianka jest pożerana przez Spustoszenie; łączy się z narzędziami. To samo wspierający ją katalista Bankierzy. Ale to Spustoszenie jest bardziej żarłoczne...
Aneta kazała Andrei uciekać i wezwać wsparcie. Sama przejęła dowodzenie nad walką.
Andrea się nie patyczkowała. Wezwała Ozydiusza.

Ozydiusz przybył na rumaku ze swoimi terminuskami, sensorami i nullifikatorami Spustoszenia. Bardzo, bardzo szybko. Andrea szybko powiedziała mu jak wygląda sytuacja, po czym powiedziała, że aura jest dla niej bardzo ważna. Ozydiusz upewnił się (dokładniej to ją poinformował), że życie magów > czystość aury (Andrea potwierdziła) i Ozydiusz kazał jej się wycofać. Dowiedziawszy się, że jest katalistką, kazał jej jednak zostać. Sam zaczął wzywać posiłki i opanowywać teren.

Zgodnie z opisem Anety, to Spustoszenie zachowuje się "klasycznie". Nie jest zbyt inteligentne. Aneta zrobiła rów i ta dwójka w ów rów wpadła. Ozydiusz na miejscu użył nullifikatora i mimo ogromnego progu Skażenia Spustoszeniem, Bianka i katalista Bankierzy zostali oczyszczeni.
Ozydiusz powiedział, że teraz to już jego sprawa. Spytał Andrei, czy aura jest dość czysta. Andrea powiedziała, że sprawdzi... i się Spustoszyła (bo właśnie w tej aurze ukryte jest Spustoszenie). Ozydiusz widząc co dzieje się z Andreą nacisnął spust nullifikatora. Andrea straciła przytomność.

Dzień 3:

Andrea się obudziła. Hipernet restored. Siedzi nad nią Elizawieta Zajcew. Andrea jest w Skrzydle Szpitalnym Świecy. 
Elizawieta powiedziała Andrei, że Ozydiusz rozkazał pełną puryfikację aury i zniszczenie jej najdrobniejszych śladów; nie dopuści do potencjalnego wektoru Spustoszenia. Ale zachował aurę i wszystko co może być potrzebne. Nie mając dostępu do Zenona Weinera (głównego eksperta od Spustoszenia) ani do drugiego eksperta od Spustoszenia (Marian Welkrat jest zamknięty na KADEMie) wysłał to do tych osób które mogą się na tym znać. Z nadzieją, że porówna wyniki. Chodzi o informację o samych wynikach.

Pierwszą informację już dostał. Tam musiało być "jakieś" Spustoszenie. I ekspedycja się na nie natknęła. A potem jak się aura rozmyła i Bianka ją odbudowała, Spustoszenie wróciło - ale jako jakaś skażona, nietrwała, niestabilna i żarłoczna forma.

Wojmił Bankierz zajmuje się dojściem do tego, kto ufundował ekspedycję i kogo może pozwać. Bo ktoś kto ufundował ekspedycję z pewnością stoi za Spustoszeniem. Poszedł śladami pieniędzy.

A Andrea opuściła szpital.

Andrea zażądała analizy sygnału aury (ostrzegła o Spustoszeniu) przez Laboratorium Mgieł. Chce pełną analizę i odcięcia wyników tych badań tak bardzo jak tylko się da. Ktoś tu sobie pogrywa w bardzo niebezpieczny sposób.
- Co zabiło Bankierza?
- Ile tropów magów są w stanie zidentyfikować i wyizolować?
- Czy to był wypadek, przypadek, czy pułapka? Jeśli pułapka to jaka?

Dodatkowo Andrea zażądała od sił Wydziału Wewnętrznego by dowiedzieli się, skąd Dominikowi Bankierzowi przyszło do głowy kopanie w tym punkcie.
A sama poszła spotkać się z seirasem Mausem. Wieczorem.

Seiras Ernest Maus przyjął ją w biurowcu "Biała Mysz". Ucieszył się na jej widok. Andrea zdecydowała, że go zmartwi. Seiras Maus powiedział Andrei, że Rodzina Świecy zdecydowała się zrezygnować z programu wobec Mausów; odbiorą Elei i Rufusowi Baltazara. Oczywiście, Elea się odwołuje, ale zakulisowo Ernest już wie, że nie mają szans. To tylko kwestia czasu. Ernest nawet przez to nie chronił interesu Mausów w sprawie Wykopalisk. 
Andrea ma pomysł. Powiedziała Ernestowi, by ten poprosił Eleę by ta załatwiła sprawę z Hektorem i by Hektor Blakenbauer pozwał Świecę.

Ernest się przyznał Andrei do czegoś. Bez Karradraela, on nie ma dostępu do wiedzy Mausów. Jego ród umiera a on nie może na to poradzić. Mausowie nie robili notatek, ksiąg, artefaktów... wszystko przechodziło przez Karradraela. To co zrobiła Aurelia uszkodziło samą sieć łączącą Mausów; a to co zrobiła Renata sprawiło, że nie mogą już liczyć na Karradraela. Gdy Andrea pyta o tamten teren wykopalisk, Ernest Maus - seiras - nie wie co tam było. Oktawian wiedział. On był badaczem. Ale teraz Oktawiana nie ma i Ernest nawet nie wie co się stało.

Skąd tam wzięła się Adela Maus? Ernest sam ją tam wysłał. Po zastanowieniu, Ernest powiedział, że to Oktawian go do tego namówił. Bał się, strasznie się czegoś bał. A z drugiej strony Andrea zorientowała się, że przecież Adela Maus (inkwizytor) była zawsze chroniona przez Karradraela. Ale teraz go nie ma - Adela mogła faktycznie wpaść w pułapkę.
Andrea zażądała od Ernesta dostępu do informacji pochodzących z biblioteki i zapisków z którymi pracował Oktawian. To się robi coraz bardziej istotne a bez tego nie da się rozwikłać co tak naprawdę się tam stało. Seiras Maus się wahał... ale w końcu wyraził zgodę. Wysłał Andrei do pomocy "najlepszą archiwistkę" Mausów - Zuzannę.

Zuza była pomocna. Była tak pomocna jak tylko się dało - pokazała, z jakimi danymi Oktawian pracował. Andrea przegrzebuje te dane dogłębnie a Zuza niczego nie ukrywa. Andrea szuka czegokolwiek o Wykopaliskach, dlaczego czuł się zagrożony, skąd wiedział. (22).

Odpowiedź dla Andrei była szokująca. Oktawian nie WIEDZIAŁ co tam jest. Ale Oktawian stał za tym wszystkim. On zastawił pułapkę na Adelę Maus - pozbawił seirasa Inkwizytora. Szukał miejsca, gdzie można schować "byt energetyczny". Wszystko wskazuje na to, że wiedział o Spustoszeniu. Ale sam Spustoszony nie był. Mógł ze Spustoszeniem współpracować... Oktawian był dość naiwny. Ale przynajmniej w wypadku Adeli widać złą wolę. Chęć usunięcia Inkwizytora.

Seiras - gdy o tym usłyszał - prawie nie mógł się pozbierać. Oktawian Maus całe życie oddał Rodowi. Bez Inkwizytora nie ma Mausów. Andrea powiedziała, że co najmniej o części tych faktów trzeba powiedzieć przełożonym... seiras nie mógł na to nic poradzić, musiał się zgodzić...

Ernest oraz Andrea doszli do jeszcze jednej rzeczy - Adela mogła zranić mieczem Oktawiana. Ostrze Karradraela jest bronią sprzężoną z Karradraelem; jeśli Maus był nim trafiony (z intencją), to Maus zostaje wysłany prosto do Karradraela. Albo jak to niemożliwe, zostaje uwięziony w mieczu. Póki nie znajdą miecza, póty nie da się uruchomić Oktawiana Mausa. Bo go tam zwyczajnie nie ma...

Wyniki z Laboratorium Mgieł:
- Dominik Bankierz został zniszczony i poświęcony w rytuale Magii Krwi, zasilającym inne rzeczy (min. zakłócenia aury)
- Był tam co najmniej jeden inny mag. I władał on Magią Krwi.
- Na bazie powyższego wszystko wskazuje na to, że to była pułapka.

Wyniki z podążania za Wojmiłem i ogólnie tym wątkiem:
- Pomysł pojawił się w klubie miłośników historii Kopalina i okolic; zasugerował go Nikodem Sowiński.
- Nikodem opłacił ekspedycję, chcąc zaimponować Laurenie Bankierz. Bo spokrewnienie z Dominikiem; Nikodem chciał go wdrożyć w proste wykopaliska.
- Wojmił rozpoczyna wielką akcję "Nikodem płać za śmierć Dominika".

## Wynik misji:
- Andrea wie, że Oktawian nie jest "czystą dziewicą"; stał za usunięciem Adeli.
- Wojmił Bankierz poluje na majątek Sowińskich
- Aleksander Sowiński o niczym nie wiedział; jest w szoku
- seiras Maus jest taki trochę biedny, ale współpracuje jak może

## Dark Future:

To buy:



# Zasługi

* mag: Andrea Wilgacz, wykorzystująca pełnię mocy Wydziału Wewnętrznego, tymczasowo Spustoszona, odkryła mroczny plan Oktawiana...
* mag: Aleksander Sowiński, przełożony Andrei zmartwiony tym co stało się z Oktawianem; chce, by Andrea rozwiązała i wyjaśniła tą kwestię.
* mag: Dominik Bankierz, jednak nie żyje; zginął na Wykopaliskach. KIA.
* mag: Wojmił Bankierz, terminus Rodu, próbuje odkryć jak zginął Dominik Bankierz by ktoś za to zapłacił (finansowo).
* mag: Aneta Rainer, 37-letnia terminuska Świecy, lojalistka, przejęła tymczasowo dowodzenie przeciw Spustoszeniu i wykazała się niezłomną, cichą lojalnością.
* czł: Krzysztof Wieczorek, który ubolewa z całego serca, że * magowie wykupują "mu" ziemię w Myślinie. Donosi Andrei z nadzieją, że ona coś z tym zrobi.
* czł: Artur Janczecki, człowiek wykupujący ziemię w Myślinie. Podstawiony z perspektywy * magów.
* mag: Jan Weiner, robiący coś w Myślinie; zgłosił "poszerzanie paneli interradiacyjnych". 
* mag: Bianka Stein, katalistka Świecy, bardzo kompetentnta i pracowita. Jedna z najlepszych w Kopalinie. Wyizolowała nić Spustoszenia i się nim zaraziła.
* mag: Ozydiusz Bankierz, którego boi się Wojmił i którego w sumie boją się wszyscy. Dodatkowo, jedzie na koniu i usuwa Spustoszenie.
* mag: Elizawieta Zajcew, z ramienia Ozydiusza wyjaśniająca Andrei jak wygląda sytuacja po Spustoszeniu.
* mag: Felicjan Weiner, "ostatni" ekspert Spustoszenia któremu ufa Ozydiusz.
* mag: Ernest Maus, bardzo zdesperowany seiras rozpaczliwie współpracujący z Andreą. Zorientował się, że bez Karradraela Mausowie umierają.
* mag: Zuzanna Maus, pomogła Andrei poznać prawdę o Oktawianie. "Najlepsza archiwistka Mausów". Pracowała z Oktawianem.
* mag: Oktawian Maus, który okazał się być mastermindem akcji na Wykopaliskach i usunął Inkwizytor Adelę Maus z obszaru wpływów seirasa Mausa...

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Pion szpitalny 
                                1. Sala regeneracyjna
                    1. Dzielnica Mausów
                        1. Biurowiec "Biała Mysz"
                    1. Las Stu Kotów
                        1. Zabytkowa ziemianka Mausów, sprzed Karradraela; gdzie są wykopaliska
                    1. Dzielnica Owadów
                        1. Klub miłośników historii Kopalina i okolic
            1. Powiat Myśliński
                1. Myślin, gdzie Jan Weiner skupuje ziemię na potęgę
        1. Mazowsze
            1. Powiat Kupalski
                1. Trzeci Gród
                    1. Centralny pierścień murów
                        1. Biblioteka zrębowa