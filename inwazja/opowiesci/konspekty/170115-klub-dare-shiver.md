---
layout: inwazja-konspekt
title:  "Klub Dare Shiver"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170111 - EIS na kozetce (SD, PS)](170111-eis-na-kozetce.html)

### Chronologiczna

* [170111 - EIS na kozetce (SD, PS)](170111-eis-na-kozetce.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

### Dzień 1:

Temat z Eis zakończył się powodzeniem. Siluria następnego dnia jest w wyjątkowo dobrym humorze. Wyrufusiła Quasar. Tego nie robi się na co dzień. Jest to okazja, którą należy adekwatnie uczcić...

Siluria poszła spotkać się z Ignatem. A Ignat jest w Sali Wojny, ku zdziwieniu Silurii...

...oczywiście, że Siluria idzie pooglądać. Ignat jest tam a w roli koordynatora jest Janek.

* To wszystko na co cię stać? - Ignat, już ranny
* Ignat... jesteś ranny... Twoje simulacrum się rozpada... - Janek, zmartwiony
* WAL WSZYSTKIM CO MASZ! - ryczący Ignat

Siluria zdecydowała się dowiedzieć od Janka o co tu do cholery chodzi... Janek szczerze nie wie, o co chodzi. Ignat niby chciał poćwiczyć, ale w ten sposób niczego się nie nauczy. Janek nawet poosłabiał część rzeczy, by Ignat MÓGŁ cokolwiek poćwiczyć... nie jest głupi i nie jest sadystą. Janek wyjaśnił, że to nie nauczy go do żadnego pojedynku; teraz Ignat walczy z rojem latających uzbrojonych pajęczaków modelowanych po Amandzie Diakon... Ignat po prostu chciał z czymś walczyć. A Janek zawsze ma jakieś pomysły.

* DOBRA! Daj mi coś, w co mogę przypierdolić! Nie jakieś debilne pajączki! - Ignat, wkurzony na maksa
* Oki... Ruination, awaken... to nie ma sensu... - Janek, zrezygnowany
* Nie, czekaj! Daj mu coś upokarzającego! Niech przegra w upokarzający sposób! - Siluria, uśmiechnięta
* Oki... Różowy lizokróliczek galaretniasty - Janek, zdziwiony
* CO KURDE?! - Ignat, widząc, co Sala Wojny wyrenderowała

Króliczek wielkości 10-letniego dziecka. Różowy i słitaśny. Rzucił się lizać Ignata długim językiem; ten mu z półobrotu i... ugrzązł w króliku. Po chwili mocowania (i grząźnięcia) język królika zajął całą twarz Ignata. Simulacrum straciło przytomność z powodu braku tlenu...

Ignat wszedł do pomieszczenia naklupać Jankowi i zdziwił się widząc Silurię. Janek spokojnie wyjaśnił, że króliczek jest najlepszą bronią na Ignata, więc Janek zrobił dokładnie to, o co Ignat go prosił. Siluria (konflikt 5#2 v 3#2: uliczny wojownik # porywczy # sytuacyjne) zaatakowała Ignata słownie; o co mu chodzi, to nie jego normalne działanie. Gdy Ignat zaczął się rzucać, Siluria złapała go za ramiona i solidnie pocałowała w usta, chcąc odwrócić jego uwagę. Udało się Silurii; Ignat ogłosił Jankowi, że idzie się przespać z Silurią. I poszedł z Silurią. Janek tylko poprawił okulary...

Siluria powiedziała Ignatowi, że idą do Hali Symulacji. On powiedział, że chce się przespać z Silurią. Ta powiedziała, że w takim razie musi z nią iść. W łóżku ona się dziś tylko przytula. Ignat facepalmował - co za dzień... poszedł z Silurią.

Siluria zabawiła się z Ignatem w "jak mnie złapiesz, będę Twoja". Ustawia halę tak, by mieć przewagę. Jest prawie rozebrana, ale dobrze naoliwiona; i powołuje obiekty w Hali. (4#2v4 -> sukces). Ignat dostał swoją nagrodę... w końcu. Solidnie się zmęczył. Trochę mu przeszło.

* To może powiesz o co chodziło? - Siluria, po wszystkim
* Powiedziałbym 'co Ty wiesz o dziewczynach', ale Ty akurat możesz coś wiedzieć o dziewczynach... - Ignat, zastanawiając się
* Problemy z dziewczynami? - Siluria, współczująca
* Mam przyjaciółkę. Wśród tienowatych. - zrezygnowany Ignat - Poprosiła o spotkanie i powiedziała, że jeśli się będę tak dalej zachowywał, nici z przyjaźni...
* Oliwier? - Siluria
* Mhm... - Ignat - A to dobra przyjaciółka. Z rodu. Mojego.
* Czyli zależy Ci na niej - Siluria, bezpośrednio
* Mhm... - Ignat
* I co w związku z tym chcesz zrobić? Jakie widzisz opcje? - Siluria
* Nie wiem. Rozpierdolić tienowatego Bankierza w drobny mak? - Ignat, bez przekonania
* Nie zmieni to jej nastawienia - Siluria
* No, nie. Może jej przejdzie. - Ignat, z nadzieją
* Zajcewka? Przejdzie? - Siluria
* Ech... - Ignat, znowu zrezygnowany - I stąd mój humor...

Siluria zwróciła uwagę Ignatowi, że to znaczy, że on musi udobruchać swoją przyjaciółkę. I żadna ilość śledzi nie pomoże. Siluria i Ignat zaczęli dla odmiany poważną rozmowę, podczas której Siluria kierowała Ignata powoli na to, że jeśli jego przyjaciółka jest na niego zła, to MOŻE pewne klasy rzeczy które zrobił wymagają INNYCH klas rozwiązań. Może warto przeprosić Oliwiera? Elizawieta by doceniła i na pewno czuła się lepiej zachowaniem Ignata. (konflikt). (5#1 v 2#3 -> F, S). Siluria zaproponowała, by Ignat w ramach przeprosin zaproponował Oliwierowi granie w Rzecznej Chacie. Ignat niechętnie, ale się zgodził.

* Wiesz, ten tienowaty Bankierz bubek będzie się pysznił, że to tylko dzięki niemu... - zrezygnowany Ignat - a to dzięki niej.
* Być może i z tym da się coś załatwić - uśmiechająca się Siluria - Pomyślę o tym, dobrze?
* No... nie cierpię przepraszać... - Ignat.
* Nikt nie lubi. Ale czasem... - Siluria, przytulając Ignata - To odróżnia magów od viciniusów, wiesz?

Siluria zadzwoniła do Netherii, dowiedzieć się, czy da się załatwić wystąpienie w Rzecznej Chacie. Mistrzyni Szpiegów i Imprez się tylko uśmiechnęła i wyciągnęła ręce po pieniądze KADEMu ;-). Siluria z ciężkim sercem zapłaciła (-1 surowiec). Siluria powiedziała Ignatowi, że jest załatwione i że Oliwier może wystąpić w Rzecznej Chacie - na koszt KADEMu. Ignat ma powiedzieć to Elizawiecie. Powinno zadziałać ;-). (hint: zadziała. Od MG.)

Wieczorem Siluria dostała liścik od swojego znajomego, tien Joachima Kartela z prośbą o spotkanie. Siluria z przyjemnością poszła do Kompleksu Centralnego (do Restauracji Dyskretnej) spotkać się ze znajomym. Kartel czeka na Silurię; postawił całkiem niezłe danie. To znaczy, że się martwi. I faktycznie - Joachim martwi się o córkę, Kornelię. Joachim pracuje w Wydziale Obsługi Hipernetu, w Kompleksie Centralnym. I jego asystent powiedział mu, że Kornelia robiła zapytania typu "jak przekraść się nago przez rynek, w biały dzień". I... Joachim się martwi. Kornelia była zawsze cichą dziewczyną; nie do końca dobrze przetrwała fakt Ascendowania podczas Zaćmienia. Nie ma przyjaciół, myśli jak człowiek, a nagle coś takiego. Joachim Kartel dał Silurii nazwisko jej koleżanki o której wie - Judyta Maus. Jego zdaniem to ona bałamuci mu Kornelię...

* Mam się czym martwić, prawda? Nie jestem przewrażliwionym ojcem? - Kartel, zmartwiony
* O ile wiem, nie jesteś. Za mało jednak wiem. - Siluria
* W domu Kornelia nic nie pokazała... z jednej strony jest skryta, z drugiej jest weselsza - Kartel - Ona się nie może niczego dowiedzieć...

No cóż, Siluria zadzwoniła do Netherii. Znowu. (-1 surowiec).

* Przyjaciel ze Świecy martwi się o swoją córkę. Nie jest do końca pewny, ale dziewczyna zachowuje się ostatnio trochę inaczej - Siluria, wyjaśniając problem
* W czym Ci pomóc? - Netheria, do rzeczy
* Z kim przestaje i co to za towarzystwo? I czy to towarzystwo namawia ją do dziwnych zakładów? Próbować szantażować? - Siluria
* Niepokojące. Wszystkiego się dowiem. - Netheria, z uśmiechem - A ona się nie dowie, że pytałam.

Netheria pochwaliła Silurię za robienie świetnej roboty. Siluria stabilizuje pokój między gildiami, jest tym klejem, którego cywilizacja potrzebuje.

### Dzień 2:

Rano, Siluria dostała notkę od Netherii. W notce była jedna fraza "Dare Shiver". Siluria od razu poznała - klub Adonisa Sowińskiego. Klub poszukiwaczy adrenaliny i stawiania się w niekomfortowych sytuacjach. Na plus, na PEWNO nie chodzi o szantaż. Na minus... to jest Dare Shiver... Nic groźnego, ALE...

Oki. Siluria poszła znowu spotkać się z Kartelem. Teraz wszystko jest dla niej jasne.

* Silurio, widzę, że teraz stawiam Ci zarówno kolacje jak i śniadania - uśmiechnięty Joachim Kartel
* Wiem, co robi. Nie jest aż TAK źle. Mogło być DUŻO gorzej... - Siluria, ważąc słowa
* ... - pytające spojrzenie Kartela
* Twoja córka po prostu zapisała się do klubu magów szukających wyzwań i wrażeń - Siluria.
* ...co? - zaskoczony Kartel

Siluria wyjaśniła Kartelowi Dare Shiver, profil autora, założenia klubu. Wyjaśniła, że nie jest ZMUSZANA. To zawsze jej decycja - zawsze ma możliwość opuszczenia klubu tu i teraz i nikt jej złego słowa nie powie. I do tej pory nikomu nic złego się nie stało. Najwyżej najedli się wstydu.

* ...nie mam pojęcia, co mam z tym zrobić. Czy powinienem... - załamany Kartel
* Nie wiem. - Siluria, spokojnie - Słuchaj... tak naprawdę Ty i Twoja żona znacie córkę najlepiej.
* Właśnie widzę, że nie. - Kartel
* To nie do końca tak. - Siluria z lekkim uśmiechem - Ona zawsze była cicha i spokojna, tak?
* Tak. - Kartel
* To całkowicie normalny etap dorastania, ZWŁASZCZA u Diakonów. Zaczyna eksperymentować. - Siluria, współczująca - Też szalone dziecko często się ucisza i poważnieje. A magię ma od niedawna. Może zrobić więcej niż kiedykolwiek mogła. Być może ona potrzebuje sobie coś udowodnić... oczywiście, istnieje pewne ryzyko. Ryzyko, że SAMA pójdzie za daleko. Że przekroczy granice których nie chce, bo przekraczanie granic okaże się łatwe...
* Ma tylko 23 lata... - Kartel
* Jest dorosła. Z punktu widzenia WSZYSTKICH praw - Siluria, z naciskiem.
* Masz... dla mnie jakąkolwiek radę? - Kartel
* Do dzisiaj ma mnie... nam za złe, że ukrywaliśmy przed nią informacje o magii, nasze życia... że ją - de facto - zostawiliśmy jak osiągnęła pełnoletniość. Bo musieliśmy. - Kartel, mając to poczucie winy.
* Czy wy ze sobą rozmawiacie tak... ogólnie? - Siluria, w problem-solving mode
* Tak. Tak, rozmawiamy. O codziennych sprawach. O magii. Mamy porozumienie, ale jest... ona... powiedziała, że gdy nas potrzebowała emocjonalnie, nas nie było. Przyzwyczaiła się. I nie chce, byśmy teraz jak nagle ma moc i jest wartościowa; byśmy teraz udawali zainteresowanie nią jako osobą. - Kartel, z desperacją - Ona nie chce przyjąć, że nie było nam wolno. Że nie mogliśmy. Mamy dobre stosunki. Mamy ciepło. Szanujemy się. Ale to jest... delikatne.
* Będąc brutalną, uważam, że ten akurat klub nie miałby prawa działać gdyby był groźny. - Siluria - Pierwsze poważne obrażenia i nawet nazwisko Adonisa nie uratuje. Więc... fizycznie nic jej nie grozi. Może się najeść dużo wstydu, może potencjalnie przekroczyć granice, ale... nie będzie to nic nieodwracalnego.
* Jeśli będzie się upierała PRZECIW doradcy rodzinnemu... you can always dare her ;-) - Siluria, z uśmiechem - Zwróćcie się do Rodziny Świecy. W końcu właśnie tym się zajmują. A na pewno nie jesteście jedynym przypadkiem.

Siluria postanowiła, że będzie obserwować działania Kornelii. Ustawić monitoring na działania potencjalnie niebezpieczne. Kto wie, może w klubie mają wielką tablicę wielkich planów as a part of the 'dare'? To chyba najlepsza opcja... zwłaszcza, że Joachim Kartel NIE JEST na tyle ważną osobą, by ktoś się interesował jego córką by przez nią dojść do niego.

Siluria zapisała sobie na kartce "nigdy nie powiedzieć Ignatowi o klubie".

Ale warto dowiedzieć się więcej odnośnie samego klubu. Więc Siluria przejechała się do Dare Shiver - klubu znajdującego się w Pirogu Dolnym (w klubie Włóczykij, na uboczu). Uzbroiła się w urodę, acz nie na polowanie ;-). W klubie Włóczykij tego dnia strażnikiem jest Joachim Kopiec. Siluria weszła do klubu Włóczykij. Tam wszystko wygląda... jak taki klub kabinet dla podróżników (40-50 lat, opowieści...). Jest jeden parametr który Silurii od ręki nie pasuje. Młody chłopak w liberii (Kopiec).

Siluria opromieniła Kopca uśmiechem, tak, by mu kolana zmiękkły (sukces konflikt) i podeszła do niego.

* Dzień dobry. Szukam... wycieczki. - Siluria, zagadując
* Dzień dobry, pani. Moją rolą tu jest spełnianie pani zachcianek. - Kopiec, lekko zestresowany. Siluria się aż zdziwiła.
* Skoro tak... - Siluria zrobiła drapieżny uśmiech. Kopiec się lekko wystraszył i Siluria to zauważyła - To zaprowadź mnie do Adonisa Bankierza
* Tego nie mogę zrobić, bo nie mogę opuścić tego posterunku - Kopiec, zestresowany
* A więc wezwij kogoś, kto może. Taka właśnie moja zachcianka - uśmiechnięta szeroko Siluria
* Musi pani sama go znaleźć. Tego nie mogę zrobić - Kopiec, nadal zestresowany

Siluria poważnie rozważyła przymuszenie młodzieniaszka. Ale nie... nie chce tego zrobić. To mogłoby zostać uznane jako atak. Usiadła na krześle, nogę na nogę, sukienka trochę wyżej i lekko podręczyć... zapytała go, co może zatem zrobić. On jej powiedział, że jest WIELE dróg dotarcia do Adonisa, wszystkie ryzykowne. Siluria stwierdziła, że nie ma ochoty taplać się w błotku czy inne takie... są tu jakieś wazy (don't touch), są drzwi (don't enter)... Siluria więc pomyślała, by zrobić coś, co nie będzie w pełni zgodne z klubem, ale nie do KOŃCA. Siluria chce Adonisa wywabić.

Siluria NIE będzie grać w grę Adonisa. Zobaczyła scenę, na której zwykle toczą się opowieści o podróżach. Siluria kazała młodemu Kopcowi pójść i załatwić jej konkretne tytuły muzyczne, adekwatne do tego, co ona chce zrobić. A chce zrobić striptiz takiego poziomu, żeby Adonis MUSIAŁ wyjść z nory, bo inaczej opowieści o klubie staną się OPOWIEŚCIĄ samą w sobie... chłopak niczego nie podejrzewając to zrobił. Siluria przygotowała się, wyszła na scenę i zaczęła snuć opowieść. Opowieść snuta... "szanowni państwo, gdzieś daleko stąd w ciepłych krajach plemię które nie zna cywilizacji i żyją bardzo blisko natury, nie korzystają z rzeczy..." powoli się rozbierając. Na początek nic niezwykłego, ale idzie coraz dalej...

"Ciekawostką jest to, że aby porozmawiać z szefem tego plemienia, trzeba przed nim stanąć całkowicie nago..." - i tu Siluria zaczyna ostry striptiz. Adonis obserwował to z radością, ale... do tego momentu. Konflikt. Siluria: 6 vs 4 -> remis. Adonis wyszedł, jak Siluria została już w samych majteczkach. Widząc go, Siluria powiedziała "o, pojawił się wódz" i zdjęła majteczki. Adonis cały zaskoczony, zaczął klaskać i kurtyna zeszła w dół. Zaprosił Silurię do właściwego klubu. Spytał, czy ona się chce ubrać. Ona odpowiedziała, że dla niej nie ma to znaczenia, ale "she does not do dares". Więc się ubrała, gdy Adonis podziwiał jej piękno.

* Wyzwałaś mnie z powodzeniem, Silurio Diakon. Jestem naprawdę, naprawdę pod wrażeniem. - Adonis, pełen podziwu
* Dziękuję. Nie zawsze jest to możliwe - uśmiechnięta Siluria. To JEST zwycięstwo.

Gdy Silurię Adonis prowadził do swojego gabinetu, dostała spojrzenie czy dwa od różnych magów z Dare Shiver. Te spojrzenia były pełne PODZIWU. Że ona się ODWAŻYŁA. Siluria odwzajemniała uśmiechem, ale drapieżnym. Niech drżą. Ona buduje swój image.

* Wiem, kto mnie zastąpi jeśli kiedyś przejdę na emeryturę - Adonis, już w swoim gabinecie, z przyjacielskim uśmiechem
* Dziękuję za komplement - Siluria
* Od tej pory zawsze możesz wejść do klubu, nawet jako nie-członek. - Adonis, wyjaśniając zasady. Siluria doceniła.

Siluria powiedziała Adonisowi, że chciała go poznać i dowiedzieć się o klubie. Nic więcej. Dla Adonisa kolejne zaskoczenie - nie przywykł.

* Zwykle magowie, którzy tu trafiają chcą zmienić coś w swoim życiu. Jesteś nietypowa, naprawdę nietypowa. - Adonis do Silurii
* Nie raz to słyszałam - Siluria, z uśmiechem
* Tylko jedna osoba poza Tobą weszła do tego klubu bez intencji dołączenia. Ona też ma kartę wstępu. - Adonis. - Jolanta (Sowińska).
* Mnie tak naprawdę sprowadza przysługa dla przyjaciela. I ciekawość. - Siluria, grając w otwarte karty.

Adonis wyjaśnił Silurii, jak działa Dare Shiver:

* Wieczór zaczyna się hazardem. Kto będzie wyzwany i przez kogo. On sam uczestniczy w tym hazardzie. Jeśli ktoś nie chce, nie musi.
* Dare jest spersonalizowany z gestaltu woli magów uczestniczących w nim. Każda grupa generuje inne 'dare'. - tu Adonis coś ukrywa.
* Grupy są dobierane charakterologicznie. Nie pojawi się grupa, gdzie ktoś by ucierpiał. Jest bezpieczeństwo (np. nie ma grupy Diakonka - dziewczyna ze świata ludzi)
* Grupy dobiera magiczny system; grupa kryształów Mausowych i innych komponentów. One generują 'dare' i one dobierają grupy
* Czasem dare jest niezrealizowany. Porażka jest elementem gry. To element dreszczyku. Zadania są generowane tak, by być TRUDNE dla tej osoby.

Adonis nabiera wody w usta, jak mówi o tych magicznych kryształach i systemach. Te wszystkie szczegóły chce zachować dla siebie. Siluria powiedziała, że to jest uspokajające; to całkiem bezpieczne rozwiązanie. Adonis zauważył, że część zadań to np. kradzieże rzeczy małych (potem oddanie), wystąpienia publiczne itp. Nie wszystko jest "łatwe". Nie wszystko jest "bezpieczne". Ale zawsze jest asekuracja. Nie ma grupy bez asekuracji, nie ma też grupy bez silnych ubezpieczeń - i on płaci za te ubezpieczenia. Członkostwo jest płatne. Po kilku pierwszych darmowych próbach, członek sam decyduje, czy chce kontynuować i płacić, czy chce przejść do "Dare Shiver Emeritus". Klub nie wiąże się z ŻADNYMI korzyściami.

* Skąd pomysł? - Siluria, zaskoczona
* Jutro może przyjść kolejne Zaćmienie. Żyjmy pełnią mocy, żyjmy pełnią życia. Wszystkie 'dare' są rejestrowane i przechodzą do legendy Dare Shiver, jak legendy Zajcewów. - Adonis, zapalając się
* A nie wpływa to na Twoje systemy magiczne? - Siluria, KONFLIKTUJĄC (5#3 sneak, wrażenie, jego impuls vs 4#2->6, S) (założenie: Siluria wie to tylko dla siebie)
* Tak... i nie. Pryzmat może zachęcać do rywalizacji i przekraczania granic. Ale mortalis żywi się tym wszystkim. - Adonis, wyjaśniając
* Ich własne emocje generują energię magiczną; to sprawia, że nie trzeba aż tyle dopłacać - Adonis, kontynuując wyjaśnienia
* To genialne narzędzia moich poprzedników, prezydentów Dare Shiver. Ja jestem tylko kolejnym z nich - Adonis
* Zaćmienie zniszczyło kilka filii Dare Shiver; ja odbudowuję śląską. - Adonis - To cel mojego istnienia
* Brzmi jak duże wyzwanie - Siluria, z ciekawością
* Największym sekretem są kryształy generujące wyzwania. One nigdy nie wygenerują czegoś, czego mag NIE CHCE. Ale na pewno się nie PRZYZNA - dokładne wyjaśnienia Adonisa
* Dzięki temu mag może powiedzieć 'o boże, czemu mi to zrobiliście!', ale naprawdę, sam tego chciał - Adonis, z szerokim uśmiechem
* Rozumiem teraz... - Siluria
* Poprzednim prezydentem Dare Shiver był mag Twojego rodu. Zaćmienie go zabrało. - Adonis ze smutkiem - On mnie wdrożył. Ja sam się namaściłem na następcę.
* I słusznie. - Siluria, rozumiejąc coraz więcej

Siluria doceniła plausible deniability. Z jednej strony... Kartel dostałby zawał serca, gdyby wiedział, że ten ekshibicjonizm pochodzi z jego córki... ale nie musi wiedzieć. To tylko faza, nie wie, czego ona może w ogóle chcieć. Siluria jeszcze chwilę z nim porozmawiała, pozostawiła mu też malutki dare: "Jeśli chcesz, odwiedź mnie kiedyś na KADEMie. Pokażę Ci hale symulacji". Adonis się uśmiechnął. Lubi hazard.

Siluria, przyciśnięta lekko przez Adonisa powiedziała, że pewna młoda osoba zmieniła swoje zachowanie i to zmartwiło jej bliskich. Nie zdradziła, o kogo chodzi i Adonis nie ma dość informacji, by to stwierdzić. Uspokoiło to prezydenta Dare Shiver. Nie jest to nic groźnego. Siluria powiedziała, że ta sama intencja - czy to nie coś groźnego - jest też z drugiej strony. Siluria powiedziała, że najgorsze co tej osobie grozi to trochę strachu i trochę wstydu. Adonis potwierdził. 

Siluria pojechała do domu, uspokojona całą sytuacją. W klubie jeszcze przez parę godzin Siluria była głównym tematem. Siluria domyśla się, że kilka 'dare' w tym tygodniu się przesunie. A może coś będzie na nią...

### Dzień 3: 

Rano Ignat przyniósł Silurii bilecik. Na papeterii (ręczna, acz nieco niewprawna robota) zaproszenie dla Silurii na sok owocowy do sadu w okolicy Piroga Górnego. Podany adres. Pisząca (Judyta Maus) się przedstawiła i napisała, że będzie sama; jeśli Siluria chce, może kogoś przyprowadzić ze sobą. Wyraźnie czytał. Gdy Siluria mu to powiedziała, powiedział, że dołączona była najpewniej bomba. Dał ją Karolinie Kupiec. Siluria wskoczyła w szlafrok i pobiegła do Instytutu Biomancji...

Karolina siedzi w Instytucie. Siluria widzi, jak ta bada tajemniczego, pomarańczowego płynu. Jest biohazard i wszystko. Ignat za Silurią. Siluria zapytała Karolinę, czy to soczek. Ta potwierdziła entuzjastycznie. Powiedziała, że to trochę pomarańczowy, trochę gruszkowy... ogólnie, fajnie zrobiony soczek przez maga. Zero rzeczy groźnych, zero psychotropów. Soczek. Siluria dała Karolinie i Ignatowi do skosztowania; dobre to. Karolina powiedziała jeszcze, że najdziwniejszy jest demoniczny posmak aury magicznej dookoła generacji tego soczku, ale to nie ma negatywnych efektów ubocznych. Jest po prostu... dziwne.

* No chyba nie pójdziesz - Ignat, zaskoczony
* Oczywiście że pójdę. I pójdę SAMA. - Siluria, ostro
* Pójdę z Tobą. - Ignat, ostro (KONFLIKT: 3#1 (martwi się o Silurię) vs 4 -> F, S)

Siluria zauważyła, że dostaje sporo zaproszeń. Nie ma się o co martwić. Ignat powiedział dwa słowa. Tienowata Mauska. Siluria powiedziała, że Ignat będzie się wykrzywiał i obrażał i krzyczał na Judytę, a ona (Siluria) NIE chce. Siluria przekonała Ignata, że jest nieustępliwa. Ignat zdecydował, że będzie Silurię ŚLEDZIŁ ale nic jej nie powie. Jakby coś się działo, niech Siluria odpali tracer jaki dostanie od Ignata. Siluria się zgodziła.

...Ignat szybko zdobył self-activator eksperymentalnego servara od Janka...

* No chyba Cię pogięło. To jest jak plecak... - Siluria, zaskoczona
* No mam tylko taki. Wsadzisz do torebki. - Ignat
* ...
* Zamontuję Ci to na plecach, pod sukienką. - Ignat, chcąc być pomocny
* NIE! Nie zgadzam się. Absolutnie. - Siluria, oburzona

Siluria zorientowała się, że Ignat coś kręci. To nie MOŻE być transponder. Ignat, burcząc, odniósł i przyniósł Silurii bardziej transponderowaty transponder... Siluria ozdobiła tym nawet włosy. Te na głowie. Ignat chce też mieć wizję.

* Tienowata Mausówna. Nie ma tematów prywatnych. Jest tylko wojna - burczący Ignat
* Ignaś, pamiętasz ostatnie? Możesz nie lubić magów Świecy, ale wciąż są magami. - Siluria, niezadowolona
* Świecy? - zdziwiła się Karolina - Ale na liściku NIE jest podpisana jako tien.
* Noż... jeszcze gorzej. NIEtienowata Mausówna nie ma NIC, żadnych zalet. Po cholerę się z nią spotykać? - prychnął Ignat
* Przyznaj się po prostu, że jesteś zazdrosny - Siluria
* FHGTGHN... - Ignat, wychodząc z pomieszczenia

Siluria doszła do tego, że najpewniej coś się zmieniło na linii Ignat - Balrog. Poszła więc odwiedzić Bogumiła Rojowca; ten zwykle wie dość sporo odnośnie tego, co się dzieje w Świecy. Rojowiec z przyjemnością przedstawił Silurii sytuację (prenumeruje Pakt, co nie wzbudza zachwytu u Silurii):

Balrog Bankierz zdobył w Pakcie artykuł o tym, że dzięki jego interwencji mag KADEMu przeprosił. Jolanta, pisząca artykuł, napisała, że KADEM jako gildia nie jest zła, że Ignat ma moralność i tak naprawdę różne rzeczy zdarzają się w świecie magów, ale Balrog skutecznie próbował wyraźnie wybić się na fali Ignata i wspaniałomyślnie wycofał się z wyzwania. Był tam też wywiad z Elizawietą, która powiedziała, że takie zachowanie jest nieakceptowalne, ale cieszy się, że Ignat doszedł do zmysłów, bo "nie jest złym chłopcem" i ona go lubi, mimo, że KADEM.

Ogólnie, Jolanta wyraźnie starała się pojednać gildie i przejść nad tym wydarzeniem do porządku dziennego. Ignat nadal nie lubi Balroga. Bardzo. Ale do Joli w tej sprawie nic nie ma i jest szczęśliwy, że Elizawieta mu wybaczyła. Ignat jest wściekły na Balroga i na to, że został upubliczniony, ale Siluria sama widzi, że sprawa się zaczyna już powoli rozpraszać...

Siluria odpisała Judycie Maus, że będzie wieczorem. Przekazała (-1 surowiec) przez Kartela wiadomość do Kornelii Kartel do Judyty Maus, bo ta... nie zostawiła żadnej wiadomości jak się z nią skontaktować. FML.

Resztę dnia Silurii zajęły typowe sprawy KADEMu. Wieczorem przejechała się - nie wiedząc, że jest śledzona - do lokalizacji podanej przez Judytę. To w Pirogu Górnym; są tam takie ogródki działkowe, nawet tyci większe. I jeden z tych ogródków to sad. I o tym sadzie mówiła Judyta. Mały sad z małą szklarnią i małym domkiem. Siluria stanęła przy sadzie i poczuła demoniczną aurę. Nie jest zestresowana. Za to Judyta jest cała zestresowana ;-).

* Interesujące miejsce - Siluria
* Dziękuję! - zestresowana Judyta
* Skąd aura? - zdziwiona Siluria
* Er, augmentacja, narzędzia... parobek? - lekko zakłopotana Judyta - Tak czy inaczej, zrobiłam soczek :D
* Jasne - rozbawiona Siluria

Judyta zaprowadziła Silurię do domku. Tam Siluria poczuła silną GRANICĘ energii magicznej, przechodząc przez próg. Aż Judycie postawiła włosy zelektryzowane. Judyta przeprosiła; lekko się jej aury sprzęgły. Siluria weszła - ZUPEŁNIE inny mikroklimat. Ciepło, przyjemnie, ładny zapach, rośliny tropikalne... Judyta musiała zdjąć okrycie i została w samej sukni. Siluria też.

Siluria zorientowała się, że przy takim BZYT! to najpewniej transponder/kamerka się spalił. Ale ona może to naprawić, zanim Ignat coś zrobi...

* Pozwolisz, że rzucę jakieś zaklęcie? Mam trochę nadopiekuńczego kolegę... - Siluria, zrezygnowana, do Judyty
* Ależ oczywiście, oczywiście! - Judyta - Wiem... słyszałam o przerażających magach KADEMu. Czytałam nawet w Pakcie
* Powiedzmy, że Pakt potrafi zdecydowanie wyolbrzymiać rzeczy... czytujesz go może regularnie? - zdziwiona Siluria
* Tak - Judyta
* Pamiętasz to o straszliwych Mausach? - Siluria, z uśmiechem
* Ale to o Mausach to była prawda... - naiwna Judyta. Siluria facepalmowała...

Silurii udało się naprawić transponder (KONFLIKT 4v2->5). Ignat, zanim zdążył się wbić, dostał z powrotem sygnał. Uspokojony, wrócił do nasłuchiwania. A Judyta podała Silurii soczek. Powiedziała, że to NIE jest trucizna. Siluria spojrzała na nią z lekkim pobłażaniem. Wie o tym... a soczek był naprawdę niezły.

* Czym mogę Ci służyć? - do tego Siluriowy uśmiech, podobny do tego w klubie (Judyta była jedną z tych, którzy widzieli Dare Silurii)
* Ja... nie... ale... - Judyta strzeliła czerwonego pąsa - chciałam...
* Bardzo dobry sok - Siluria, z uśmiechem
* Właśnie o to chodzi. Chciałam... zrobić sok. Z Tobą. - Judyta, strasznie zawstydzona
* Znaczy... oczywiście, nie ma problemu. Jest to dość nietypowa prośba... - Siluria, zaskoczona (KONFLIKT na wykrycie intencji: S). Judyta się boi, bo... no, KADEM / Maus, bo Siluria jest jaka jest (ma o niej opinię). 

Dare polega na... zrobieniu magicznego soczku z Silurią. Z jej perspektywy: jest mało ważną Mausówną. KADEM nie do końca ma dobrą historię z Mausami; gildie nie przepadają za Mausami. Ona jest tutaj sama. Nie ma defensyw. Siluria mogła przyprowadzić kogokolwiek. Judyta musi przekonać Silurię, by ta zrobiła z nią magiczny soczek i Judyta ma go wypić, niezależnie co to nie jest. To jest dla niej dość silny dare. I tak, ta niepewność co Siluria zrobi itp. też jest silnym elementem tego wszystkiego. 

Siluria zdecydowała się skorzystać z tego w taki sposób - skoro Judyta i tak wyciągnęła do Silurii rękę pierwsza, Siluria chce nawiązać kontakt. Magiczny soczek... i Siluria ma wymyśleć co! Judyta smakowo pomoże to skomponować. Siluria wymyśliła odpowiednie komponenty magiczne: niech soczek podniesie jej Social Acuity. Niech Judyta potrafi się odgryźć, niech jest czarująca... i do tego, by też Judytę w pewien sposób zadowolić - niech będzie bardziej ponętna, bardziej flirciarska ;-). Nie chodzi o usunięcie barier. Chodzi o wzmocnienie pewności siebie i przyznanie się do swoich potrzeb przed sobą. Też: pokazanie jej troszkę jakim życiem żyje Siluria, jak inni na nią reagują i jak może to być miłe. Ale nie przekraczać granic. To nie być eliksir; tylko Judyta.

Silura mówi Judycie, co ona ma czarować i robić. Siluria dowodzi zaklęciem; Judyta wprowadza swoje komponenty na życzenie Silurii. Siluria łączy swój lifeshaping i swoją magię mentalną / Diakońską z lifeshapingiem Judyty. Siluria dokłada magię urody i magię mentalną. A Judyta, mimo pewnych zahamowań, w pełni współpracuje z Silurią - co się KADEMce bardzo podoba. Judyta współpracuje (#1) i dostarcza surowce i sympatię (#2). Siluria dodała do tego jeszcze kilka środków od Karoliny (#3, -1 surowiec). Czyli 12: Heroiczna Elita.

Judyta spojrzała na Silurię z lekką obawą, widząc skończony soczek. Siluria widzi, jak Judyta jest zestresowana. Siluria rozlała do dwóch szklanek i się napiła. Judyta też...

Siluria szybko się zorientowała, że jednej rzeczy nie wzięła pod uwagę. Zwiększenie poziomu pewności siebie Judyty, 'dare', sexual tension... powoduje to, że Judyta się... zainteresowała Silurią. W związku z tym, szeroko uśmiechnięta Siluria zdecydowała się na zebranie się. Judyta spróbowała ją zatrzymać. KONFLIKT: 3 (eliksir), 2 (podstawa) -> 5. Siluria chce ją wymanewrować; nie jest nią zainteresowana. Nie tak. Nie w ten sposób; chciała dać jej przyjemne wspomnienia, ale nie przelecieć ją pod wpływem eliksiru. Siluria: 5#1 -> 6. 6v5 -> F, F, S: Siluria zauważyła, że jednak ciężko z Judytą to poszło. Za mocno na nią zadziałało. Siluria musi zostać z nią do rana, uspokajać, unikać kontaktów erotycznych, głaskać po główce... bez sugestii... Silurii też było ciężko, ale dała radę. Nad ranem jak Judyta zasnęła, Siluria się wymknęła. Ostro napalona i niewyspana...

Siluria poszła na KADEM. Ignat się nie pokazał. Siluria poszła na KADEM, złapała Infernię i poszła się wyżyć ;-).

A następnego dnia Judyta pójdzie do klubu i będzie zupełnie inaczej, szczęśliwiej się zachowywać ;-). Do końca następnego dnia. Potem do niej dotrze co NAPRAWDĘ zrobiła i mogło się stać. Udany 'dare'.

# Progresja

* Siluria Diakon: dostała kartę wstępu do Dare Shiver, dożywotnią.
* Jolanta Sowińska: ma dożywotnią kartę wstępu do Dare Shiver.
* Oliwier Bonwant: zagra w Rzecznej Chacie; wybaczył Ignatowi pobicie
* Balrog Bankierz: wybił się kosztem Ignata Zajcewa jako paladyn w lśniącej zbroi chroniący Oliwiera
* Judyta Maus: awestruck Silurią. Fangirl Silurii ;-).

# Streszczenie

Ignat w wyjątkowo paskudnym humorze; Elizawieta go opierniczyła za to, że pobił Oliwiera. Netheria załatwiła Oliwierowi występ w Rzecznej Chacie by Ignat miał JAK go przeprosić. Udało się, acz Balrog przypisał te zasługi sobie. Artykuł w Pakcie poprawia stan KADEM/Świeca, kosztem Ignata, ale Elizawieta wybaczyła Ignatowi, więc... ok? Siluria poszła tropem Dare Shiver, klubu magów ryzykantów i wyciągnęła jego prezydenta, Adonisa, do rozmowy. Adonis wyjaśnił, że Dare Shiver jest niegroźny dla swoich członków; Siluria się martwiła, bo tien Kartel martwił się o swoją córkę (ascendowała na maga po Zaćmieniu a jej rodzice byli już magami). Po wyjaśnieniach Adonisa poczuła się lepiej. Potem Siluria pomogła Judycie Maus z jej wyzwaniem; w wyniku tego Judyta jest Siluria-struck i stała się Siluria-fangirl.

# Zasługi

* mag: Siluria Diakon, załagodziła szczelinę między Ignatem i Elizawietą, zeksplorowała Dare Shiver przez sprowokowanie Adonisa, pomogła Kartelowi w problemie 'rodzinnym' i pomogła Judycie w jej wyzwaniu, robiąc z niej fangirl Silurii.
* mag: Jan Wątły, gdy Ignat powiedział w Sali Wojny "uderz najsilniejszym co masz" skutecznie uderzał tym, co pokonywało Ignata i nie dawało mu szansy na wyżycie się ;-).
* mag: Ignat Zajcew, zirytowany jak osa; przeprosił Oliwiera za pobicie (bo Elizawieta), próbuje chronić Silurię i BARDZO mu się nie podoba, że tienowaci zrobili sobie z niego publiczne używanie...
* mag: Elizawieta Zajcew, która powiedziała Ignatowi, że za to co zrobił może się rozsypać ich przyjaźń. Jedyna siła w galaktyce zdolna zmusić Ignata do przeproszenia maga Świecy ;-).
* mag: Netheria Diakon, która załatwiła dla Silurii za KADEMowe pieniądze występ dla Oliwiera w Rzecznej Chacie by zamknąć wreszcie temat sporu KADEM / Świeca.
* mag: Joachim Kartel, zamartwiający się o dziwnie zachowującą się córkę ojciec; poprosił Silurię o pomoc w zrozumieniu tematu. Siluria delikatnie wyjaśniła mu jak działa Dare Shiver i poradziła Poradnię Rodziny Świecy.
* mag: Kornelia Kartel, młoda adeptka Dare Shiver, która zachowuje się nieco nienaturalnie (co widzi ojciec); min. googluje "jak bez ubrania poruszać się niezauważenie po mieście" i inne takie.
* mag: Judyta Maus, jedyna (?) przyjaciółka (?) Kornelii Kartel, w Dare Shiver, której 'dare' to było zaprosić Silurię na wspólne zrobienie soczku i wypicie go. Niestety, jej odporność była niższa niż Siluria zakładała. Została fangirl Silurii.
* mag: Joachim Kopiec, tego dnia strażnik w Dare Shiver (dare), który musiał robić wszystko o co go poproszą. Miał przyjemność zobaczyć striptiz Silurii.
* mag: Adonis Sowiński, prezydent Dare Shiver; wyjaśnił Silurii jak ten klub działa oraz musiał opanować sytuację po tym, jak Siluria zrobiła striptiz w ludzkiej części klubu ;-).
* mag: Jolanta Sowińska, która ma kartę wstępu do Dare Shiver i która napisała w Pakcie artykuł mający pojednać troszkę magów Świecy oraz KADEMu.
* mag: Karolina Kupiec, wykryła, że sok jabłkowy (z domieszkami) jest dokładnie tym i niczym więcej. Zresztą, jest to całkiem smaczny sok.
* mag: Bogumił Rojowiec, główne źródło informacji Silurii o Świecy; powiedział o najnowszej publikacji Paktu, lansowaniu się Balroga i normalizacji ze strony Jolanty.
* mag: Balrog Bankierz, lansuje się kosztem Ignata, ale czas powoli zejść z tego konia; wyekstraktował całość wartości jaką był w stanie na tym etapie. Łaskawie wycofał pojedynek.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Górny
                    1. Obrzeża
                        1. Osiedle Polne
                            1. Klub Włóczykij, klub podróżniczy (z opowieściami i artefaktami ludzkimi) gdzie znajduje się też zakamuflowany Dare Shiver
                                1. Klub Dare Shiver, centrala klubu z wszystkimi magitechowymi systemami i specjalistycznymi komponentami
                        1. Ogródki działkowe
                            1. Sad Zielonej Myszy, należący do Judyty Maus, która robi tam różnego rodzaju soczki i gdzie wykorzystywane są demoniczne narzędzia
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy 
                            1. Pion ogólnodostępny
                                1. Restauracja Dyskretna, gdzie Joachim Kartel i Siluria mieli rozmowy o córce Kartela.
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Układ nerwowy
                        1. Sale symulacji, gdzie Siluria dowiedziała się co Ignata boli i które wykorzystała w erotyczny sposób ku rozpaczy osób sprzątających ;-)
                            1. Sala Wojny, gdzie Ignat dał na sobie testować różne zabawki Janka. Który, niestety, nie dawał się Ignatowi wyżyć i go pokonywał.

# Czas

* Dni: 3
