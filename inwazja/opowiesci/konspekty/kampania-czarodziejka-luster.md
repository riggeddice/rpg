---
layout: default
categories: inwazja, campaign
title: "Czarodziejka Luster"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Światło w Zależu Leśnym](kampania-swiatlo-w-zalezu-lesnym.html)

## Opis

-

# Historia

1. [Błękitny zaskroniec](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html): 10/01/03 - 10/01/05
(170207-blekitny-zaskroniec)

Przez sprzężenie się lokalnych legend w Wężokuju - legendy o Żmiju Psotniku i niedaleko znajdującej się w Żonkiborze Arazille doszło do powstania Błękitnego Zaskrońca - odłamka Arazille. Żona heroinisty wezwała Zaskrońca krwią, by jej mąż nie cierpiał. Czarodziejka Matylda i Andromeda udająca maga dały radę rozwiązać problem i naprawić sprawę, głównie magią Matyldy ;-).

1. [Można doprowadzić maga do problemu...](/rpg/inwazja/opowiesci/konspekty/120918-mozna-doprowadzic-maga-do-problemu.html): 10/01/06 - 10/01/07
(120918-mozna-doprowadzic-maga-do-problemu)



1. [Ale nie można zmusić go do jego rozwiązania...](/rpg/inwazja/opowiesci/konspekty/120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html): 10/01/08 - 10/01/09
(120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania)



1. [Zwłaszcza, gdy coś jest ciągle nie tak](/rpg/inwazja/opowiesci/konspekty/121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html): 10/01/10 - 10/01/11
(121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak)



1. [Banshee, córka mandragory](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html): 10/01/12 - 10/01/13
(121104-banshee-corka-mandragory)



1. [Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html): 10/01/14 - 10/01/17
(160908-zabojczy-spadek)

Andromeda spotkała się z syberyjskim artefaktem - figurką śledzia syberyjskiego z zaklętym miecztopaukiem. Niestety, artefakt ten zdążył już skazić wrażliwą 22-latkę, Emilię Bogatkę. Viciniuska zbudowała dookoła siebie (nieświadomie) kult i ludzie zaczęli ginąć. Andromeda z lokalnym patologiem, Zygmuntem próbowali dojść do tego o co chodzi. Niestety, nie zdążyli zniszczyć figurki - wbiły siły Skorpiona i porwały Emilię oraz zdobyły figurkę dla siebie.

1. [Bliźniaczka Andromedy](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html): 10/01/23 - 10/01/25
(170815-blizniaczka-andromedy)

Podążając za plotką o tym spadku i znikającej dziewczynie, Andromeda napotkała na swojego "klona". Przycisnęła dziewczynę i pomogła jej uciec; okazało się, że znajomy jej Mecenasa stoi za tym wszystkim. Opowiedział Andromedzie historię o tajnej organizacji ratującej ludzi przed siłami paranormalnymi. Men in Black. Której eks-członkiem jest Andromeda... Co jeśli jest prawdą oznacza, że Andromeda była kapłanką Iliusitiusa kiedyś?

1. [Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html): 10/01/27 - 10/01/29
(170816-na-wezwanie-iliusitiusa)

Mordred i Klara odpowiedzieli na tajemniczą flarę kojarzącą się z Iliusitiusem. Na miejscu uratowali Augusta Bankierza, który wpadł prawie w sidła klątwożytowanej Marianny - czarodziejki, która straciła moc. Zastawili pułapkę i usunęli potwora ujawniając Iliusitiusa. Dodatkowo, ewakuowali wszystkich nieszczęśliwych magów tak, by nikomu nic się nie stało. A magowie muszą Coś Zrobić z Iliusitiusem w okolicy...

1. [Renowacja obrazu Andromedy](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html): 10/01/30 - 10/01/31
(130503-renowacja-obrazu-andromedy)



1. [Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html): 10/02/01 - 10/02/02
(130506-sekrety-rezydencji-szczypiorkow)



1. [Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html): 10/02/03 - 10/02/04
(130511-oltarz-podniesionej-dloni)



1. ['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html): 10/02/05 - 10/02/06
(131008-moj-aniol)



1. [Portret Boga](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html): 10/02/07 - 10/02/08
(141218-portret-boga)



1. [Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html): 10/02/09 - 10/02/10
(141220-bogini-marzen-w-zonkiborze)



1. [Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html): 10/02/11 - 10/02/12
(141227-przyczajona-andromeda-ukryty-maus)



1. [Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html): 10/02/13 - 10/02/14
(141230-ofiara-z-wampira-dla-arazille)



1. [Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html): 10/02/15 - 10/02/16
(150103-pryzmat-mysli-peka)



1. [Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html): 10/02/17 - 10/02/18
(150104-terminus-defiler-kaplan-arazille)



1. [Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html): 10/02/05 - 10/02/06
(150105-dar-iliusitiusa-dla-andromedy)



1. [Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html): 10/02/19 - 10/02/20
(150602-esme-najemniczka-netherii)



## Progresja

|Misja|Progresja|
|------|------|
|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|Skorpion zdobył figurkę śledzia syberyjskiego z wpisanym miecztapaukiem (generującą krystalizowany syberion kosztem snu i marzeń)|
|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|Skorpion porwał Emilię Bogatek jako potencjalną broń / narzędzie|

