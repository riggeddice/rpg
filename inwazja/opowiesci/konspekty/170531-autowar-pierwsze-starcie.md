---
layout: inwazja-konspekt
title:  "Autowar: pierwsze starcie"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170405 - Chyba wolelibyśmy kartony... (SD, PS)](170405-chyba-wolelibysmy-kartony.html)

### Chronologiczna

* [170517 - Zegarmistrz i Alegretta (HB, KB)](170517-zegarmistrz-i-alegretta.html)

## Koncept wysokopoziomowy misji

Autowar uruchomiony przez Andreę w trybie defensywnym. Nie wie co się dzieje i co ma ze sobą zrobić, więc próbuje zbudować sobie bazę w Mszance Polnej. Estrella dowiaduje się o tym i ściąga jako wsparcie Blakenbauerów - Świeca w tej chwili raczej autowara rozwali bombą atomową...

## Kontekst ogólny sytuacji

Temat Infensy i Renaty nadal budzą zainteresowanie magów, którzy się na dwie główne kategorie - jedni chcą o tym zapomnieć, wymazać z historii i nazwać anomalią a drudzy chcą doprowadzić do zwarcia szyków i wykluczenia magów, którzy byli z tym powiązani - głównie KADEMu i Hektora.

Pojawiły się działania w kierunku na prokuraturę magów. Silną lobbystką jest Andżelika. Niestety, jednocześnie lobbuje za karaniem Marianny, przez co jedynie rośnie ruch izolacji gildii. Znowu mamy dwie siły. Na to nakłada się Infensa... chwilowo izolacja jest dominującą siłą.

Działania Alegretty wzbudziły zainteresowanie magów Mausożernych - takich, którzy chcieliby po Karradraelu najlepiej karać różnych Mausów. Łatwo dojdą po śladach do Maurycego Mausa a tam - do Blakenbauerów. Na szczęście, Judyta Maus jest przeszczęśliwa z odzyskania Maurycego i z aktualnego stanu Alegretty - Hektor wyraźnie wybił blisko Silurii w kategorii Prawdziwych Bohaterów Judyty...

Eis zaczyna się zastanawiać, czy pomysł z sojuszem był najlepszy, czy nie pójść starym podejściem typu "silny ma rację". Nadal się waha - nie wszyscy magowie KADEMu traktowali ją źle, ale może Hektor nie jest tym magiem, którego szukała...

## Punkt zerowy:

Mszanka Polna, dość przednia wieś z basenem niedaleko Czelimina. Porozmawiajmy sobie o modelu biznesowym.

* Ż: Na czym polega podstawowy model ekonomiczny Mszanki? Tzn. z czego żyje większość ludzi i jaki to ma zakres?
* D: Większość ludzi w tej wsi żyje z hodowli kur.
* Ż: Co jest główną produkowaną żywnością w Mszance?
* K: Kotlety DeVolaille - ogólnie, kotlety DeVolaille z Mszanki są popularne. Eksport gotowych wyrobów garmażeryjnych z drobiu.
* Ż: Jaki jest najwyższy budynek w Mszance?
* D: Ogromny silos. Na wzbogaconą karmę itp. GMO w pełnym tego słowa znaczeniu.
* Ż: Powiedz mi, kto jest stosunkowo ważną osobą w Mszance?
* K: Miejscowy weterynarz (min. od kur). Stanisław Bazyliszek.

Żółw_cel: Zbudowanie kontekstu i postaci pierwotnych dla Mszanki.

* Ż: Podaj jedno zdanie o tymczasowej (we wsi nieważnej) postaci Dusta, którą spotka Coś Złego. Aha, nazwij swoją.
* K: Milena. A on: drobny, zwinny i ciekawski. Wszędzie się wciśnie i nie dał się przyłapać. Robi drobne prace we wsi, dorywczo.
* Ż: Podaj jedno zdanie o tymczasowej (we wsi nieważnej) postaci Kić, którą spotka Coś Złego. Aha, nazwij swoją.
* D: Gerwazy. Opiekunka osób starszych we wsi. Do rany przyłóż, ale straszna plotkara. Monitoring we wsi musi być ;-). Główny nexus informacji.
* Ż: Określ lokalizację spotkania tych postaci z Problemem.
* K: We wsi, ale nie w centrum; trochę z boku. Było im po drodze, szli w kierunku sadu brzoskwiniowego.
* Ż: Określ przyczyny, dla których tam się znaleźli, pozornie nie związane z Problemem.
* D: Gerwazy miał zbierać owoce sezonowe natomiast Milena zajmuje się ojcem właściciela sadu i przyszła po owoce, by upiec z nich ciasto.

Żółw_cel: To da nam pierwszą scenę startową do pkt 0.

**Scena startowa:**

Gerwazy zauważył, że ziemia jest ciepła. Mimo psiej pogody temperatura ziemi jest... wysoka. Za ciepło. Ziemia jest ciepła. Termy? I na to weszła Milena...

We wsi jest świetny potencjalny inwestor. Proboszcz - Rafał Łopnik. Umówili się, że po pracy mogą iść do proboszcza i omówią jakiś biznesplan na termy ;-).

Gerwazy kopie. Znalazł coś pasożytowego? winorośl? Coś z ząbkami przyczepionej do najlepszego drzewa... Milena prosi weterynarza o przyjechanie.

Weterynarz przyjechał. Skłamał (co Milena zauważyła), że pod ziemią są takie drogie rośliny. Milena mu uciekła; Gerwazego dziabnęła osa. Sparaliżowany, porwany.

Milena rozprzestrzeniła plotkę idąc do starszego pana którym się opiekowała. On jest na wózku i niezbyt przytomny. Ale patrzy na Milenę zupełnie inaczej. Bardzo świadomie.

Milena się wycofała i ściągnęła syna starszego pana. Ojca nie ma na wózku. Zniknął.

Pojawiają się osy. Milena zwiała do piwnicy. Syn schował się w szafie. Dorwała go osa.

Piwnica. Starszy pan, już NIE na wózku otworzył drzwi do piwnicy. Milena krzyknęła. Dorwała ją osa. Infestacja.

* Ż: Czemu Blakenbauerom opłaca się pomóc zdezorganizowanej Świecy w tej sprawie?
* D: Świeca im zapłaci za pomoc. Cena jest negocjowalna. A Hektor chce zdobyć przychylność Świecy.
* Ż: Skąd Estrella dowiedziała się o tych wydarzeniach, mniej więcej?
* K: Plotka dotarła do Estrelli. Włącznie z ciepłą ziemią i dziwną pasożytniczą rośliną.

Żółw: To nam daje przyczynę wykonania działań wspomagających

* Ż: Czemu nie do końca możecie liczyć na jakiekolwiek wsparcie ze strony gildii?
* K: Gildie próbują się pozbierać po sprawach z Karradraelem. A to nie do końca jest teren jakiejkolwiek gildii.
* Ż: Nazwij istotną osobę, która może Wam pomóc w rozwiązaniu tego problemu.
* D: Wirgiliusz Kartofel; może nam pomóc przez dane o rodzinach we wsi. Intelligence. Anomalie może wyłapać i przedstawić.
* Ż: Co może zapewnić Wam pewną niewykrywalność przez Drugą Stronę?
* K: Aura ochronna Rezydencji Blakenbauerów; bardzo skuteczna przeciwko bestiom różnego rodzaju.
* Ż: Co sprawia, że macie jakiekolwiek szanse mimo całej tej sytuacji?
* D: Autowar jest uszkodzony; da się go rozproszyć i odwrócić jego uwagę łatwiej niż powinno się dać.

Żółw_cel: Zbudowanie wsparcia kontekstowego.

**Ścieżki Fabularne**:

* **B_Infestacja Cywili**: 0/6
* **B_Infestacja Żywności i Ekonomii**: 0/6
* **B_Regeneracja i Adaptacja Bzizmy**: nieskończone
* **B_Produkcja Rojów**: 0/8
* **B_Podpięcie się do leylinów**: 0/8
* **Złamanie Maskarady**: 0/8
* **Skażenie Magiczne**: 0/8

## Misja właściwa:

**Dzień 1:**

Do Rezydencji przyszła Estrella i poprosiła o rozmowę z Hektorem. Powiedziała, że Świeca w "kryzysie klasy A" wysadzi taką Infestację.

Estrella powiedziała, że wie, że Blakenbauerowie chcą zdobyć taką istotę. Niech tak będzie. Ona pomoże.

Estrella opowiedziała o ciepłej ziemi, dziwnej roślinie i osie. Plotka. Wiarygodna.

Hektor chce pomóc ludziom. Estrella pomoże z prokuraturą magów za to.

Edwin zaznaczył, że Estrella jest kiepską terminuską. Niech lepiej Hektor nie oddaje jej kontroli. Dane od Ozydiusza.

Klara i Hektor i Estrella mają złapać jak najwięcej typów os dla Margaret i się wycofać asap.

Estrella dostała tarczę jak inni. Handlarze drobiem w ciężarówce. Jadą na miejsce - do koła rolniczego. Jako audytorzy (+1 akcja).

Wirgiliusz sceptyczny. Estrella skonfliktowała; ma gotowe dokumenty (fałszerstwo). 7v4->S. (+1 akcja). 

Hektor przepytał Wirgiliusza, po drodze maskując PRAWDZIWE pytania o teren i okoliczności. 7v4->S (+1 akcja). 

* Ludzie w silosie pracują jak w zegarku; jak nigdy.
* Nie ma nigdzie żadnych os. ŻADNYCH.
* W sadzie są super brzoskwinie, acz nikt ich niespecjalnie chce sprzedawać
* Kryty basen - wyjątkowo dużo ludzi go odwiedza. Kury są przednie.
* Fabryka DeVolaille - wyjątkowo dobre przeroby.

Hektor chce przekonać Wirgiliusza, żeby ten mu ściągnął te dziwne brzoskwinie. 5v3->S. Wirgiliusz wysłał "panią Kasię". Tak dla niego, Wirgiliusza. (+1 akcja)

Zakwaterowali się w Kurmszance. Marcelin NIE JEST z Estrellą w pokoju. Estrella zmontowała komunikację po hipernecie, prywatny pokój. Ma uprawnienia, bo Andrea awansowała kogokolwiek kto był przydatny - tymczasowo. Klara szuka zabezpieczeń. Magicznie. Bio, techno i info. 6v4->S. (+1 akcja)

* Są pluskwy Wirgiliusza; on chce słuchać co robią audytorzy, o czym mówią, jak zwiększyć swoje szanse...
* Są owady należące do Bzizmy. Ale nie widzą postaci - jest tarcza Rezydencji.
* Bzizma jest umysłem zaawansowanym - nie jest osą ani królową roju.

Klara zdecydowała się przygotować drony jeszcze w Rezydencji (+1) i przez warsztat w Rezydencji (+1, -1 surowiec). Zajmie jej to do końca dnia (+1). 8v?->10, ES (+2 akcje)

ES: Efemeryda lub smakołyk: ściąga osę, która Was nie widzi.

* Węzły użyteczne; przy Basenie - silny elementalny. Przy silosie - słabiutki elementalny. Przy sadzie - silny biomantyczny. Ten jest głęboko pod ziemią i najsilniejszy.
* Basen jest zmieniony. To jest wylęgarnia. Tam są ludzie, którzy są Hostami. Wylęgają osy - ale oni są naprawiani. Przeciwnik stara się zachować Maskaradę.
* Silos. Tam jest coś... dziwnego. Jakaś doczepka biologiczna. Najwyższy budynek - to jest strażnica; to oczy. I tam też znajdują się osy, acz jeden Rój os. Są tam 3 typy os.
* Sad. W sadzie nie widać nic dziwnego. Nic. Teren jest czyściutki.
* W fabryce ludzie po prostu pracują bardzo mechanicznie. Nic nie mówią, działają unisono. Jak jedna maszyna.

W międzyczasie - Hektor z Estrellą idą na brzoskwinie do Wirgiliusza. Dostali brzoskwinie, poszli do góry.

Do Klary przyleciała osa. Ledwo widzialna. Po chwili kolejne osy - z jabłkiem, z innymi owocami i zaczęła je ładować. Po naładowaniu, odleciała. Czyli osy żywią się jakąś formą energii magicznej Skażającej owoce... to jest osa wojowniczka.

RAN 7: 2,4,5,5,7,7,7 -> 0,1,2,1,1,1,1

* **B_Infestacja Cywili**:                  0/6
* **B_Infestacja Żywności i Ekonomii**:     0/6
* **B_Regeneracja i Adaptacja Bzizmy**:     0/nieskończone
* **B_Produkcja Rojów**:                    1/8
* **B_Podpięcie się do leylinów**:          3/8     (+1 tier)
* **Złamanie Maskarady**:                   0/8
* **Skażenie Magiczne**:                    3/8     (+1 tier)

Próg Skażenia magicznego w mieście się troszeczkę podniósł. Bzizma nie dał rady kontrolować podpięcia się do leylinów całkowicie. Jest rozkojarzony.

**Dzień 2:**

Mają plan. Potrzebują osy, człowieka i pluskwy.

Pluskwa to problem Estrelli. Ma użyć Zmysłów by ją przetransportować; by ją zmylić. Udało się (+1 akcja).

Zabierają też brzoskwinie ze sobą.

Ludzie pracują w fabryce DeVolaille. Poprzedniego dnia przyuważyli sobie dronę. Zaklęciem Zmysłów przekształcono ciężarówkę Zespołu w autobus. Gdy drona wychodzi z domu, Hektor rzuca zaklęcie - dominacja. Wchodzi do autobusu i wszystko w porządku. Hektor połączył się z nieludzkim już umysłem: ma do czynienia z pasożytem na człowieku. 5+3v5->Remis. (+1 akcja)

Tyle, że INNE drony nie widzą tej konkretnej drony. A zagubiona drona raportuje, że ta drona jest... no, na miejscu. Problem dotarł do autowara, który delegował problem do os... i osy zaczęły szukać.

Klara wzięła superpancerną butelkę. Wraz z Hektorem - architektem materii i miłośnikiem budownictwa - zbudowali butelkę z zabezpieczeniami na osy. I Klara wsadziła tam sygnaturę smakołyku oraz sygnaturę zaginiętej drony. No i Marcelin ze swoimi feromonami unieszkodliwiającymi osy. 4+5v7->S. Udało się złapać 2 różne osy w ilości większej. (+1 akcja)

Po czym - w nogi ciężarówką, odcinając osy.

Autowar puścił pościg. Estrella rzuciła iluzję - kilka ciężarówek w różnych miejscach. Klara wzmocniła ciężarówkę magicznie a Hektor rzucił się do roboty - mistrz kierownicy ucieka, łamiąc przepisy drogowe. Pieprzyć przepisy, tu są osy! 4+2v5->Remis. Komplikacje: (+1 akcja)

KF: Wzrost znaczenia grupy X: TODO.

I udało się im ewakuować. I znają zasięg dron... nie dociera do Kopalina... (+1 akcja). A epicentrum królowej jest w sadzie - wiedzą jak daleko osy leciały za decoyami.

Margaret ostrzegła, że to z czym walczą to nie vicinius. To autowar Świecy.

Estrella obiecała, że spróbuje coś się dowiedzieć...

**TO DO**:

* Określić, znaczenie jakiej grupy bardzo pójdzie do góry

**Ścieżki Fabularne:**

RAN 4: 2,5,6,6 -> 1,1,2,0

* **B_Infestacja Cywili**:                  0/6
* **B_Infestacja Żywności i Ekonomii**:     1/6
* **B_Regeneracja i Adaptacja Bzizmy**:     0/nieskończone
* **B_Produkcja Rojów**:                    1/8
* **B_Podpięcie się do leylinów**:          4/8     (+2 tier)
* **Złamanie Maskarady**:                   2/8     (+1 tier)
* **Skażenie Magiczne**:                    3/8     (+1 tier)

**Interpretacja Ścieżek:**

Autowar Bzizma dał radę połączyć się z leylinami i niestabilnymi Węzłami głębinowymi. Nie dał jednak rady w pełni opanować sytuacji co doprowadziło do lekkiego Skażenia i lekkiego naruszenia Maskarady. Musi skierować swoją uwagę w tą stronę; musi odrobinę spowolnić swoje główne infestacyjne działania.

Jednocześnie pewna grupa jest bardzo istotna...

# Progresja

* Estrella Diakon: ma tymczasowo podniesione uprawnienia jako terminus przez Andreę; jest użyteczna i można na niej polegać, więc... 

# Streszczenie

Estrella przyszła do Blakenbauerów pytając, czy to co się dzieje w Mszance to ich vicinius. Wyjaśniła, że tam dzieje się coś strasznego - infestacja os. Margaret i Hektor chcą to dla Blakenbauerów; Estrella nie oponuje. Na miejscu okazało się, że to bardzo silna infestacja a przeciwnik buduje małe Węzły taktyczne. Hektor, Estrella i Klara porwali kilka dron tajemniczego przeciwnika i wycofali się do Rezydencji. Na miejscu, Margaret zidentyfikowała ich przeciwnika jako autowara Świecy...

# Zasługi

* mag: Hektor Blakenbauer, mistrz kierownicy uciekający przed osami, mentalista dominujący ludzkie drony autowara i mag, który chce pomóc ludziom (a najlepiej, jak zwinie autowara Świecy)
* mag: Klara Blakenbauer, badała zarażony przez autowara teren dronami, skanowała i wykrywała podsłuchy ORAZ mistrzyni łapania os w butelki. Znalazła oso-miętkę.
* mag: Estrella Diakon, która wkręciła Blakenbauerów w próbę pokonania autowara. Nie miała nikogo innego a alternatywą jest zniszczenie wioski i śmierć wielu ludzi.
* vic: Bzizma Stlitlitlix, zagubiony autowar; uszkodzony; próbuje ufortyfikować teren i przygotować się do dalszej walki z... czymkolwiek trzeba zniszczyć.
* czł: Stanisław Bazyliszek, weterynarz min. od kur. Infested. Dość poważany we wsi, jeden z pierwszych zainfestowanych. 
* czł: Milena Pacan, Opiekunka osób starszych we wsi. Do rany przyłóż, ale straszna plotkara. Monitoring we wsi musi być ;-). Główny nexus informacji. Infested.
* czł: Gerwazy Śmiałek, drobny, zwinny i ciekawski. Wszędzie się wciśnie i nie dał się przyłapać. Robi drobne prace we wsi, dorywczo. Infested.
* czł: Rafał Łopnik, proboszcz. Największy inwestor we wsi i ogólnie osoba ze świetnym zmysłem biznesowym.
* czł: Wirgiliusz Kartofel, przewodniczący koła rolników; chce jak najwięcej zyskać dla "swoich" i utrzymać pozycję. Pomaga "audytorom". Ulega urokowi Estrelli.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, do której przyszła Estrella i w której Edwin rozpaczliwie ratuje umierającego Mausa.
                1. Mszanka Polna, dość przednia wieś niedaleko Czelimina; niestety, nawiedził ją uszkodzony autowar - Bzizma.
                    1. Centrum
                        1. Basen, Infested Lair; gorące miejsce z małym węzłem elementalnym służącym jako Spawning Pool.
                    1. Wieś
                        1. Sad Brzoskwiniowy, Infested Headquarters; pod nim znajduje się Bzizma i mały węzeł biomantyczny. Świetne brzoskwinie.
                        1. Silos, Infested, wykorzystywany jako najwyższy budynek we wsi; stacjonuje tam jeden Rój i systemy detekcyjne autowara.
                        1. Centrum Drobiarskie Kurmszanka, spory budynek, gdzie spotykają się miłośnicy drobiu. Coś między hotelem a peweksem.
                    1. Zamieście
                        1. Fabryka DeVolaille, gdzie produkowane są devolaile z których słynie Mszanka. Infested; produkcja zdecydowanie rośnie.

# Czas

* Opóźnienie: 1
* Dni: 2

# Frakcje:


## Zagubiony Autowar

### Koncept na tej misji:

Istota stworzona do toczenia wojny i zniszczeń. Autowar jest bardzo niebezpieczny i... wchodzi w tryb fortecy. Nie do końca wie co robić i z czym walczy.

### Scena zwycięstwa:

* Nikt się nie orientuje, ale Mszanka stała się bezpieczną bazą dla Bzizmy i jego sił
* Infested Humans zapewniają szersze jedzenie, logistykę, ekonomię i pilnują Maskarady; jednocześnie zapewniają Bzizmie jedzenie i działanie
* Bzizma powoli rozprzestrzenia swoje oczy i działania na zewnątrz, szukając szerszych działań
* Eternal Expansion. W końcu znajdzie Wroga.

### Motywacje:

* Maskarada musi być zachowana. Nic nie może wyjść na zewnątrz.
* Trzeba przygotować oddziały do walki z Wrogiem. Wróg jest skuteczny i niebezpieczny.
* Zapewnić logistykę i fortyfikację; musi przetrwać przede wszystkim.

### Siły:

* Bzizma, autowar owadzi; ufortyfikowany w Sadzie Brzoskwiniowym: Autowar (4), Duży (1), Adaptacja Biologiczna (1), Tarcza Fizyczna (2), Tarcza Mentalna (3), Tarcza Magiczna (3), Bardzo Pancerny (2), Rój Strażniczy (3), Zindoktrynowany (2), Autonomiczny (2), Regeneracja Osami (1), Regeneracja Infestowanymi (2), Ukryty (2), Najedzony (2). Ufortyfikowany (2), Uszkodzony (1), Łatwy do rozproszenia i odwrócenia uwagi (2)
* Pierwszy Rój Os (Wojowniczki, Samobójcze, Lęgowe): Taktycznie Zbilansowany Rój (5)
* Drugi Rój Os (Wojowniczki, Samobójcze, Lęgowe): Taktycznie Zbilansowany Rój (5)
* Infested Humans: Devoted (3)
* Osa Wojowniczka: Zwinna, Pancerna, Szybka, Toksyczna, Mordercze Żądło, Kontrolowana (2)
* Osa Lęgowa: Pancerna, Duża, Paraliżuje, Dominacja Pasożytnicza (2), Kontrolowana (2)
* Osa Samobójcza: Niestabilna, Zwinna, Bardzo Szybka (2), Bardzo Toksyczna (2), Kontrolowana (2)

### Wymagane kroki:

* Infestacja cywili
* Infestacja pożywienia i rozprzestrzenie Oczu Bzizmy
* Zapewnienie Maskarady przez wyłapywanie jednostek kluczowych
* Zapewnienie źródeł energii przez wysyłanie Infestantów i podpięcie się do leylinów
* Produkcja Rojów
* Rozprzestrzenianie się po terenie celem Infestacji i znalezieniem Wroga

### Ścieżki:

* **Infestacja Cywili**: 2,2,2
* **Infestacja Żywności i Ekonomii**: 2,2,2
* **Regeneracja i Adaptacja Bzizmy**: nieskończone
* **Produkcja Rojów**: 2,2,2,2
* **Podpięcie się do leylinów**: 2,2,2,2
* **Złamanie Maskarady**: 2,2,2,2
* **Skażenie Magiczne**: 2,2,2,2

# Narzędzia MG

## Cel fabularny misji

Pokazanie potęgi autowarów. Oraz jak fajna jest Estrella ;-).

## Cel techniczny misji

1. Weryfikacja nowego podejścia do magii - szkoły magiczne + specjalizacje
2. Weryfikacja frakcji v2
3. Weryfikacja systemu aspektowego
4. Weryfikacja ścieżek fabularnych
    1. Akcja gracza: reakcja każdej frakcji
    2. Akcja frakcji: +(0,1,2) na losowy tor
    3. Mechanizm eskalacji (brutalizacji) meta-ścieżek
    4. Patrz: [Notatka 170520](/rpg/mechanika/notatka_170520.html)

## Po czym poznam sukces

1. Szkoły magiczne ze specjalizacjami
    1. Magia staje się potężniejsza; szersza, feel jest bardziej jak ElMet.
    2. Postacie są dobrze odwzorowana przez sztywne szkoły magiczne.
    3. Widzimy GRANICE działania postaci. Wiemy, co może zrobić magią a co nie.
    4. Specjalizacje magiczne nie przeszkadzają
2. Frakcje
    1. Świat żyje; zawsze wiem, do czego dążą poszczególne strony aktywne
    2. Bookkeeping nie jest morderczy
    3. Gracze są w stanie wejść w interakcje z frakcjami i im przeszkadzać lub pomóc
    4. Zawsze wiem kto jest gdzie i co robi
3. System Aspektowy
    1. Istnieją bossfighty; walka z Bzizmą jest nietrywialna
    2. Mamy różne typy konfliktów: 'jednostrzałowe' i bardziej interesujące, wieloetapowe
    3. Istnieje możliwość większego wykorzystywania Okoliczności podczas konfliktów
    4. Mniejsze wychylenia wynikające z pojedynczego testu - przegranie testu nie uniemożliwia osiągnięcia celu.
4. Ścieżki Fabularne
    1. Frakcje funkcjonują prawidłowo i ich cele postępują do przodu, jeśli nieskontrowane
    2. Gracze są w stanie osłabić frakcje lub nawet je skontrować
    3. Gracze, jeśli odpowiednio poświęcą, są w stanie brutalizować i osłabić frakcje poważnie.

## Wynik z perspektywy celu

1. Szkoły magiczne ze specjalizacjami
    1. Dust: podoba mi się łączenie szkół. Otwiera ciekawe możliwości. 
2. Frakcje
    1. Żółw: działa. Wymaga jakiegoś autogeneratora na tory, ale działa.
3. System Aspektowy
    1. Dust: nie do końca widać. Nie na tej sesji.
4. Ścieżki Fabularne
    1. Dust: pasuje mi pokazywanie zmian, które umiemy zaobserwować jako postacie.
5. Scena Startowa
    1. Dust: Bardzo przyspieszyła pierwsze działania. Poszliśmy tam, gdzie to miało sens. Buduje ramkę.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Modyfikatory:

|  .Okoliczności. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .MODYFIKATORY. |
|                 |          |         |         |            |                 |

Wynik:

| .Postać. | .Opozycja. | .Modyfikatory. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |   SFR   |
