---
layout: inwazja-konspekt
title:  "Proces bez szans wygrania"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

# Wątki

- Szlachta vs Kurtyna
- Wojny wielkich gildii Śląska

## Kontynuacja

### Kampanijna

* [150422 - Śladami aptoforma... (DK, AB)](150422-sladami-aptoforma.html)

### Chronologiczna

* [150422 - Śladami aptoforma... (DK, AB)](150422-sladami-aptoforma.html)

### Logiczna

* [150330 - Napaść na Annalizę (SD, PS)](150330-napasc-na-annalize.html)

## Punkt zerowy:

Ż: Dlaczego, powszechnym zdaniem, Iza sprowadziła Spustoszenie?
K: Wymyśliła jakiś dziwny plan i nie ma zahamowań, by kogoś poświęcić. One step too far.
Ż: Czym ostatnio objawiła się iskrząca się wrogość KADEMu i SŚ?
D: Świeca jawnie zasabotowała jakiś program KADEMu.
Ż: Dlaczego właśnie Hektor Blakenbauer, ze wszystkich potencjalnych magów?
K: Jest niepowiązany, więc wszyscy mają nadzieję na maksymalną bezstronność.
Ż: Jaki to był program KADEMu?
K: Wykorzystano Marcela Bankierza; jego akcje doprowadziły do zniszczeń. "Między nami Zajcewami coś się rozwaliło" -> prasa SŚ, jaki to KADEM KADEMowaty.
Ż: Co ostatnio było poważnym problemem Tamary?
D: Przekroczyła kiedyś uprawnienia w ramieniach działań przeciwko Millennium i były na nią dowody.

## Misja właściwa:

W kontekście posiadania aptoforma i wyłączonej z akcji Mojry Otton musiał wykonać pewne działania. Jednak nie mógł tego zrobić jeżeli w Rezydencji znajdują się osoby, które nie są zgodne z tym co chce osiągnąć. I tu pojawia się problem - Hektor. Normalnie, Hektorowi można by potem wyczyścić pamięć, jednak tym razem operacje i badania wymagają pewnej delikatności; to powoduje, że nie będzie dało się wykorzystać mocy Rezydencji na Hektora. Czyli: Hektora trzeba się pozbyć fizycznie z Rezydencji. I to tak, by na pewno nie zdążył wrócić.

Rozwiązaniem okazał się problem Izabeli Łaniewskiej. Terminuska podpadła bardzo mocno KADEMowi (o czym wie Edwin) i niektórym siłon w Świecy (o czym wie Marcelin). W związku z tym Margaret wymyśliła szalony podstęp - oskarżyć tien Łaniewską o bycie niegodną zawodu terminusa. Rozumowanie Margaret było takie, że Izie przecież nic nie grozi (bo Hektor nie zna prawa świata magów). Dodatkowo, jest to coś co Hektora zdecydowanie zawsze interesowało, a los Izy jest dla Blakenbauerów obojętny. Bez Mojry, nikt nie był w stanie powiedzieć co na to frakcja SŚ współpracuąca z Blakenbauerami, więc plan został wprowadzony w życie.

Margaret skontaktowała się z SŚ i zdobyła tam demona prawniczego do przekazania Hektorowi. To, o czym nie wiedziała to to, że ów demon jest "shackowany" - jednak nie przejęłaby się tym. No i poszła porozmawiać z Hektorem, drugą linią Edwin poinformował KADEM, a KADEM przekazał informacje Silurii i Pawłowi.

"Nasz ród jest czymś więcej. My się nie boimy terroru terminuski Łaniewskiej!" - Margaret namawiająca Hektora.
Margaret spotkała się z Hektorem i zaczęła opowiadać o złej Izie i jak to nikt nie chce jej posadzić bo wszyscy się boją "terrorterminuski". Hektor się zgodził. Margaret dostarczyła Hektorowi demona prawniczego Srebrnej Świecy (taniego, z przeceny). Jak Hektor go wezwał (używając lakieru do paznokci), zobaczył dlaczego jest taki tani... demon jest w przebraniu chippendalesa. Sama przepaska. I nie da się nic na niego założyć, więc wybór jest prosty "z agrafką czy bez".

Cóż, Margaret dodatkowo dodała kontakt na KADEM (Siluria i Paweł) dodając, że oni są potencjalnymi oskarżycielami i że z radością owo oskarżenie wniosą. Brakuje im tylko kompetentnego prokuratora.

Tymczasem Paweł i Siluria spotkali się z Andżeliką. Ta powiedziała, że udało się znaleźć prokuratora, który jest całkowicie poza gildiami (więc Tamara nie będzie w stanie przesunąć tego w "KADEM kontra SŚ") i jest powiązany zarówno z KADEMem jak i ze Świecą - ma więc wszystkie znamiona bezstronności. 
Niestety, jest to Blakenbauer. Okrutny i czarny ród, ale na pewno kompetentny i profesjonalny. Dzieje się już proces przeciw Izie i Hektor Blakenbauer po prostu szuka pomocy - świadków, śledczych.... kogoś, kto pomoże mu zmontować jak najskuteczniejsze oskarżenie.

Siluria i Paweł się zgodzili. Wszystko co pozwoli ukarać Izę za jej zbrodnie - nieważne jak bardzo czarny jest Blakenbauer - jest opłacalne i godne uznania. Andżelika wzięła na siebie całość roboty papierkowej i ostrzegła Zespół, że Izę będzie broniła Emilia. Jej przyjaciółka. To bardzo zdziwiło Silurię - Emilia jest konserwatystką i jakkolwiek zna prawo magiczne, ale nie jest ekspertem. Jednak Andżelika wyjaśniła, że Emilia jest lepsza niż jakikolwiek adwokat "z urzędu" a z uwagi na pozycję Emilii Iza dostała bardzo silną tarczę polityczną. Dlaczego - Andżelika nie wie i zupełnie tego nie rozumie.

Dobrze więc, trzeba się spotkać. W KADEMowej restauracji. I wszedł do restauracji Hektor Blakenbauer. Ponury, poważny, kompetetny. Nie powiedział, że w limuzynie zostawił biednego Jana Szczupaka (kierowcę) z prawie nagim demonem i kazał Janowi pilnować demona. Bo tak, Hektor przyjechał (ku uciesze Silurii) na KADEM limuzyną kierowaną przez człowieka.

Podczas spotkania, na szybko, Paweł wyjaśnił Hektorowi sytuację odnośnie świadków:
- Oktawian Maus i Zenon Weiner ze Srebrnej Świecy są potencjalnymi magami których można zapytać - ale wyjdzie, że są jakoś powiązani. A Oktawian to w ogóle enigma.
- terminusi na akcji (Iza, Tamara, Judyta) najpewniej nie będą chcieli zeznawać. Judyta może tak... reszta na pewno nie. I są chronieni prawem SŚ przed zeznawaniem.
- większość magów boi się zeznawać przeciwko Izie. Bo Iza.

Ale Paweł znalazł jeszcze jedną osobę - Marysię Kiras, z Millennium. Ona może być asem w rękawie. Na to jednak zaprotestowała Siluria - Marysia jest chroniona przez Millennium jako agent ukryty. Wszelkie próby ją wyciągania są wyjątkowo niewskazane i Millennium potraktuje to jako działanie wrogie. Nadal - może być warto to zrobić, ale niekoniecznie jako pierwszy pomysł.

Tu też wyszło fajne nieporozumienie - okazało się, że Hektor myślał że to KADEM składa oskarżenie a KADEM, że Hektor. Nie mają więc oskarżyciela. Znowu temat wraca do Marysi... bo bez oskarżyciela po prostu nie będzie procesu.

Hektor dopytał - okazuje się, że na Izę jest mnóstwo poszlak. Nie ma jasnego POWODU dlaczego Iza by to zrobiła; ogólna opinia jest taka, że miała jakiś chory plan. I z tego wszystkiego wynika:
- wszyscy magowie co mogą zeznawać byli Spustoszeni oraz większość była leczona na KADEMie. Niewiarygodni świadkowie.
- nie ma ŻADNEGO dowodu wiarygodnego i wprost.
- nie wiadomo czemu ona to zrobiła.

Czyli: nie ma oskarżenia, nie ma dowodów, nie ma osób pokrzywdzonych a oskarżana jest terminuska na akcji. Po prostu świetnie.

Paweł przypomniał sobie Estrellę Diakon. Terminuska Srebrnej Świecy, która nadaje się jako początek oskarżenia. Co by dało potencjalnego oskarżyciela. Więc Siluria i Paweł nie są stroną oskarżającą. I wtedy Hektor im się przyznał, że nie jest ekspertem od prawa świata magów, ale ma dostęp do kompendium wiedzy prawnej Srebrnej Świecy. Demon_481. Więc Hektor zaprosił Silurię i Pawła do limuzyny by poznali demona...
Czyli nie tylko nie ma nic na Izę, ale jeszcze nie mają odpowiedniej znajomości prawa. Może i może być gorzej... ale jak?

Hektorowi przyszło do głowy, że w sumie nie wie czy może temu demonowi ufać - w końcu dostał go tanio. Szczęśliwie, są na KADEMie. Jest tu mnóstwo różnych urządzeń i narzędzi. Hektor, z pomocą Silurii i Pawła zdecydował się demona przebadać i przesłuchać (22v17) i demon pękł. Powiedział, że został wynajęty przez siły za którymi stoi tien Emilia Kołatka; ma zrobić subtelny acz znaczący błąd proceduralny gdyby Hektor wygrywał co pozwoli Izie odejść na wolność.
Ale teraz słucha Hektora. I będzie zeznawał że został przekupiony.
Emilia? Zupełnie nie w jej stylu. Ale... jest strategiem...

No dobrze, czyli Emilia się zabezpieczyła. Iza naprawdę jest trudna do unieszkodliwienia. Zwłaszcza, że Tamara Muszkiet stawia sprawę jako Srebrna Świeca kontra KADEM i na pewno spróbuje wykluczyć ze sprawy Hektora. I po to właśnie Estrella Diakon, oskarżycielka Izy ze strony SŚ... jeśli się zgodzi. Na szczęście, Siluria po ciężkich przebojach przekonała Estrellę, by podjęła sprawę ze strony Świecy. Estrella się zgodziła, ale tylko przez wzgląd na krew (Estrella DIAKON, Siluria DIAKON). Terminuska powiedziała, że jej zdaniem Spustoszenie nie ma wiele wspólnego z Izą. Nie ten styl. Ale pójdzie na to, z nadzieją, że coś się ruszy. Spróbuje w jakimś stopniu pomóc by ta sprawa nie umarła.

W czym nadal nie wiadomo dlaczego Iza to wszystko zrobiła. A nawet - CO dokładnie zrobiła.

Zespół zdecydował się więc pojechać do Powiewu Świeżości z nadzieją, że tam znajdą się jakieś dowody. W końcu: tam było jedno ze źródeł Spustoszenia. Hektor zaczął więc rozmowę z Julią, która  powiedziała Hektorowi wszystkie informacje na temat Spustoszenia. Siluria zamknęła mu komputer na palcach, bo Hektor nie może się dowiedzieć niczego o Dracenie. Innymi słowy, tą linią niczego się nie dowiedzą...

Siluria zauważyła, że "Szlachta" jest prawdopodobnym motywem z punktu widzenia Izy; niestety, jeśli taka organizacja istnieje, to zasabotują salę sądową. Więc, inna motywacja. Iza, widząc zło na świecie, stwierdziła że potrzebuje więcej mocy i skupiła się na zdobyciu informacji. A to, niestety, brzmi jak Iza.
Badania danych komputerów dostarczone przez Annalizę wykazały, że dużo wcześniej, w przeszłości (przed "Antygona kontra Dracena"), Iza szukała czegoś co pozwoli na stałą korektę zachowania, najlepiej w sposób niezauważalny. To wskazuje na to, że ten cel by pasował i jest bardzo słabą poszlaką pośrednią wskazującą na to, że potrzebne było Spustoszenie zmieniające pamięć...

Siluria i Paweł zauważyli, że nie da się przesłuchać Izy. Iza zawsze eskaluje gdy jest zaatakowana. I niestety, Iza nie ma cierpliwości czy szczególnych umiejętności społecznych. To powoduje, że niebezpiecznie jest ją przesłuchać...
...moment. Ale to oznacza, że jeśli będzie na sali sądowej, Hektor z bazą 15 ZMASAKRUJE Izę. Wyprowadzi ją z równowagi. Iza nie ma żadnych szans. Czyli jeśli dojdzie do procesu, Iza od samego początku ma przegraną bo Hektor sprowokuje ją do zaatakowania Hektora. I to właśnie jest sposób.

Innymi słowy:
- nie ma na Izę dowodów
- Hektor nie zna prawa magicznego
- Andżelika załatwi problemy proceduralne a Estrella będzie wnoszącą oskarżenie
- ... i z samej retoryki, nie mając ABSOLUTNIE NIC na Izę powinni być w stanie wygrać.

Nadal nie ma dowodów. Ale nie są potrzebne. Iza miała kilkanaście razy już przecież "prawie przed sądem". Jest możliwość zebrania wszystkich tych malutkich spraw i zrobienie pozwu zbiorowego. Andżelika przygotuje papiery, by wszystko prawidłowo trafiło. Bo jakkolwiek Emilia jest jej przyjaciółką, Izabela Łaniewska zasłużyła na karę.
A demon_481 pomoże ze wszystkimi aspektami prawnymi Srebrnej Świecy.

Pojawia się ciekawy problem - czy Hektor decyduje się ujawnić, że jego demon został zmieniony. Nie ma stu procent pewności, że to robota Emilii, ale z perspektywy prawnej to była robota Emilii. Jeśli Hektor to powie (praworządność), Iza dostanie ostrzejszy wyrok. Jeśli nie (taktyczność), nie będzie że to on.
Hektor decyduje się zachować dla siebie informację o tym, że "złamał uszkodzenie demona" (naprawił). Przyjrzy się Emilii, a jeśli ona faktycznie jest winna... to wtedy uderzy.

Hektor poszedł na salę sądową bez demona. Wygląda jak chłopek-roztropek. Ale w garniturze. Nie w szacie ceremonialnej.

I - faktycznie - udało się. Podczas rozprawy Hektor skupiał się na bombardowaniu Izy i używaniu mów i zaskarbianiu sobie publiczności. Emilia była w tej kwestii zdecydowanie słabsza, ale broniła Izy ze wszystkich sił (i potrafiła parę razy pokrzyżować plany Hektora). Hektor też bez żadnych skrupułów uderzył mocno w Emilię, min. wykazując shackowanie demona co zniszczyło nieskazitelność reputacji Emilii Kołatki, zwłaszcza że czarodziejka zamiast się bronić skupiała się na poprawie sytuacji Izy.
Jednak Hektor dał radę wyprowadzić Izę z równowagi. Powiedział, że wszystkie jej działania to tylko odbicie traumy psychologicznej polegającej na tym, że Kalina (przyjaciółka Izy) zginęła a Izy nie było w pobliżu; że Iza cierpi na patologiczną nieufność i że ma elementy choroby terminusów.

Siluria i Paweł z wielką satysfakcją zobaczyli jak Iza rzuca się na Hektora tylko po to, by Emilia ją poraziła paralizatorem; Iza przed procesem się na to zgodziła. Hektor odwrócił to w "jej własna obrona przewidywała, że terminuska stała się potworem". Przy takich umiejętnościach retorycznych, przy takich faktach Hektor bezapelacyjnie zwyciężył nie mając ANI JEDNEGO DOWODU.

Wynik całego procesu:
- Izabela Łaniewska opuszcza Świecę; trafia pod opiekę Millennium, którzy mają poprowadzić dla niej terapię by jej pomóc.
- Iza ma całkowity zakaz zbliżania się do Silurii, Hektora i Pawła.
- Emilia dostała bardzo silny cios w reputację; jej "stronnictwo" zostaje poważnie osłabione.
- Na fali, Hektor dostał silny szacunek od Srebrnej Świecy oraz dostał uprawnienia oskarżania magów; może pełnić rolę prokuratora świata magów.
- Odrobinę poprawiły się stosunki KADEM - SŚ; większość tego wszystkiego przypisano Izie.
- Siluria i Paweł się dowiedzieli, że motywacją Izy była osłona Draceny. To był plan Draceny, ze Spustoszeniem, Iza po prostu jako terminus próbowała maskować jej uczestnictwo i uniemożliwić gildiom ukarania czarodziejki Millennium.
- Siluria i Paweł dowiedzieli się, że "Szlachta" istnieje naprawdę i że Iza próbowała z nimi walczyć ale NAWET ona została spacyfikowana. Stąd pomysł Draceny ze Spustoszeniem który podchwyciła Iza Łaniewska.
- Iza wypada całkowicie z orbity zarówno Srebrnej Świecy jak i znaczenia.
- Demon_481 zostaje z Hektorem na stałe i za darmo, za zasługi.

# Streszczenie

Blakenbauerowie chcąc zająć się niegodnie Mojrą zaaranżowali proces przeciwko Izie, włączając w to wszystko KADEM. Proces nie miał prawa się udać - nie ma na Izę żadnych dowodów. Hektor jednak dokonał niemożliwego i Millennium przejmie nad Izą jurysdykcję, przekształcając ją i robiąc jej terapię. Mimo działań Tamary i podłożenia Hektorowi "shackowanego" demona prawniczego (niewiadomo przez kogo), Hektor wygrał - a Emilia jako adwokat Świecy dostała silny cios polityczny i Kurtyna ucierpiała boleśnie.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Pion ogólnodostępny
                                1. Sąd magów
                        1. KADEM Primus
                            1. Restauracja
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Nowy Kopalin
                        1. Powiew Świeżości

# Zasługi

* mag: Siluria Diakon, która załatwiła Estrellę jako główne siły oskarżenia przeciwko Izie Łaniewskiej.
* mag: Paweł Sępiak, pomysłodawca zniszczenia Izy bez dowodów i bez cienia prawdy przy użyciu umiejętności Hektora Blakenbauera.
* mag: Hektor Blakenbauer jako ekspert prokurator, który bez cienia dowodów i bez znajomości prawa * magów wygrał sprawę przeciw Emilii Kołatce i zniszczył Izę Łaniewską.
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, która przegrała wszystko mimo braku dowodów przez swój porywczy charakter. Przymusowo wysłana na terapię do Millennium, nie liczy się już w SŚ.
* mag: Emilia Kołatka, która broniła Izę nawet kosztem swojej reputacji i bardzo za to ucierpiała politycznie i po opinii; Hektor ją zmiażdżył.
* mag: Andżelika Leszczyńska, która pomogła z proceduralną częścią dokumentów by Srebrna Świeca nie mogła odrzucić pozwu przeciwko Izie.
* mag: Margaret Blakenbauer, która po prostu chciała się pozbyć Hektora z Rezydencji, więc wysłała go na oskarżenie przeciwko terminusce Izie Łaniewskiej.
* czł: Jan Szczupak, uczciwy i zrozpaczony kierowca Hektora Blakenbauera, który musiał pilnować demona_481... przez co nasłuchał się przerażających pornożartów.
* vic: Demon_481, demon prawniczy ubrany w samą przepaskę biodrową "z agrafką lub bez", z rozkazami zrobienia błędu proceduralnego. Został z Hektorem.
* mag: Estrella Diakon, terminuska Srebrnej Świecy, która zdecydowała się wystąpić z oskarżeniem wobec Izy by Spustoszenie nie zostało zamiecione pod dywan.
* mag: Julia Weiner, która nic a nic nie pomogła Hektorowi w zdobyciu dowodów przeciwko Izie. Bo nic nie wiedziała.