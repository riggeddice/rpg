---
layout: inwazja-konspekt
title:  "New, better Senesgrad"
campaign: rezydentka-krukowa
players: kić, druar, jojo
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170716 - Eteryczny Chłopiec i jego Pies (PT)](170716-eteryczny-chlopiec-i-jego-pies.html)

### Chronologiczna

* [170716 - Eteryczny Chłopiec i jego Pies (PT)](170716-eteryczny-chlopiec-i-jego-pies.html)

## High level context:

GM wants to explore Senesgrad and its context a bit more. Also, build the current situation. The purpose of the mission is currently unknown.

The Ephemeral is semi-active.

## Initial state:

**Exploratory questions:**

Ż: Why is the Steam Museum so important to the local population?
K: There are workplaces connected to it, there is a community and some occasional events.
Ż: What circumstances made it so the investor decided to renovate a Water Tower and change it into a restaurant?
K: A new train connection to Senesgrad; it may recover and bring new people in.
Ż: Why can this work from the business perspective?
D: Lots of tourists, potentially of richer kind. A fancy restaurant should work.
Ż: What good would the restaurant and the closure of Steam Museum do to the region?
J: The closure of old stuff would let the people move forward. It would bring people in, let them stay. Steam Museum is looking to the past, which too many people do with nostalgia. People want to return to the past. And those changes would make them think of the future.
Ż: Who is the main opposition to those changes?
K: The current maintainer and the curator of the Steam Museum, Rafał Adison.
Ż: What strange, nonlethal YET dangerous phenomenon did one of you see?
D: I was walking next to the tower and I saw that strange light from the inside. And then my cousin got sick (she did recover though).
Ż: Why the restaurant/museum plans may cause the phenomenon to intensify and return?
J: There is that legend that once an old steamworker died, he was proud. And the tower is haunted. And this would dismantle his work. 
Ż: Why do you all want this restaurant/museum thing to stop from happening?
K: Dru's cousin got sick. T is a historian and a librarian. Jo is a local town journalist ("People's Wellbeing", privatized, left-leaning) who tries to make sure no bad news about the city will be written.
Ż: Why is it still possible to stop the changes? Helpful circumstances.
D: Well, the construction is kind of accessible; there is very lax security. Also, there is a strong community loving the Steam Museum.
Ż: Who can help you with stopping it?
J: An editor-in-chief of the "People's Wellbeing"; he is an influential 64-y-old mayor of the city. Named Karol Szczur.
Ż: How did a local politician connected to those plans win on 'efficiency' platform?
K: Name is Stefan Brązień; he won on the platform of transport, basically. He is the one who was behind the opening of the new track.

**Story Paths**:

Does not apply in this mission

## Misja właściwa:

The actual session:

**Day 1:**

After Kurt contacted his friends - Sonia and Klara - they have discussed briefly the current situation. Between the Water Tower and the Steam Museum there was a high risk something will happen to the Water Tower. The workers were already in the barracks, waiting for the permits from the city - which was just a matter of time. Yet Kurt could not forget that strange green light he saw and the fact his cousin was sick...

Sonia and Klara focused on learning more about the strange phenomenon - in terms of finding strange information about the light (7v6->S). Although Klara found, that some records were edited - some false facts were planted - they did manage to find a single survivor of a contact with a green light. The guy was a vagabond accordeon player; he got really bad afterwards. Sonia went to interview the guy (Paweł Franka) and the guy said, that the light took a PART of him. And he hopes it will give it back, but it never gives back.

Klara noticed, that there exists a relation between the green light appearing (basically, stuff happening) and the fact, that 3 women who designed the Steam Museum left the area and stopped taking care of it. According to story, there were 3 women and 1 man, but there is no information about the man whatsoever. And Sonia noticed, that the strange phenomena happen mostly after some very powerful storms. As if storms were the energy sources for the phenomena - various phenomena. 

So basically there is a problem. The light may actually be a problem and the changes related to the Water Tower may actually make it happen even worse and more often. In a way, it is necessary to stop the construction, but how?

Actually, Kurt is a stock broker. He is able to plant some false evidence that Paweł Madler is actually bankrupt and he does this thing only to prolong the agony of his company. He did ask several loaded questions (7v6->S) and made it happen; there are some records which prove that variant. This should be enough to make people start asking questions...

**Day 2:**

...but people do not ask questions on their own, someone has to make them. So after a gossip and fake information was properly introduced to the system, Sonia decided to pass that information discreetly to the mayor. But how? Well, the best way would be to pass the information via another journalist. So she does that (4v3->F,F,S). As a result, yes, another journalist passed the information to the mayor and the mayor decided to stop the possibility of Water Tower changes. Especially by Madler's company.

F: Madler does not really understand what is going on, so he hired a private investigator who quickly managed to catch the scent of Sonia...

**The map**:

![The logical map of the Senesgrad](Materials/170525/senesgrad_map_raw.jpg)

**Following up:**

The plans of New Senesgrad party has been put on hold; Madler - a chance to do something to Water Tower - got into some gossip problems related to potential bankrupcy. He did hire a private investigator to learn who did this to him; that person will soon explore Senesgrad.

The ephemeral will eventually dampen and the supernatural phenomena will stop happening. For now, the past and the secrets of it has been preserved.

# Progresja

* Paweł Madler: gossip and false evidence say that he is close to bankrupcy

# Streszczenie

Kurt did see a strange light in the Water Tower which made his cousin sick. This led to Sonia and Klara find old stories about Water Tower and a witness - there is something paranormal here. To protect the region, the businessman - Madler - had rumors created about the bankrupcy of his company; Future Senesgrad faction will require a different approach... 

# Zasługi

* czł: Kurt Zieloniek, a stock broker who has seen strange light at Water Tower; he stopped the Water Tower reconstruction by planting false hints towards Madler being bankrupt.
* czł: Sonia Adamowiec, a journalist of "Dobro Ludzi"; she found out the Water Tower incident survivor and gave a lead about Madler being bankrupt to another journalist.
* czł: Klara Pieśniarz, a local historian, librarian and a medium; she found some old information on the Water Tower and similar incidents.
* czł: Rafał Adison, a current curator of Steam Museum who really does not want the museum to be closed.
* czł: Paweł Madler, businessman who wanted to change the Water Tower into a luxury restaurant and got framed into 'oh this guy is bankrupt' scheme.
* czł: Stefan Brązień, a local politician selected on 'efficiency' platform; he got Senesgrad to have a new track and had to stop Water Tower reconstruction because Madler is bankrupt
* czł: Karol Szczur, a powerful 64-y-old editor-in-chief in "Dobro Ludzi" and a mayor of Senesgrad. 
* czł: Paweł Franka, a vagabond accordeon player who 'lost a part of himself' in a Water Tower incident. 
* vic: Efemeryda Senesgradzka, gained a part of Paweł Franka at one point of time. Resides in a Water Tower. Tends to be activated and wakes up during the thunderstorms.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, "Water Tower", where some supernatural phenomenons seem to appear
                        1. Jezioro Czereśniowe, an artificial lake and water source.
                        1. Siedziba Dobra Ludzi, a left-wing privatized newspaper of Senesgrad
                    1. Zalesie
                        1. Muzeum Pary, "Steam Museum", which was supposed to be dismantled by the actions of a local politician, Stefan Brązień

# Czas

* Opóźnienie: 2
* Dni: 2

# Factions:

### Madler's business

**Concept on this session:**

A businessman who wants to become famous.

**Victory scene:**

* A Water Tower got rebuilt and becomes a fancy restaurant
* Lots of Important And Relevant People eat at the WaterTower restaurant
* Madler becomes famous for rejuvenating Senesgrad; those VIPs talk with him

**Motivations:**

* Be famous and recognizable in the good way
* Earn on the Water Tower business
* Be on good side of every important entity nearby

**Paths:**

* not applicable here


### Future Senesgrad

**Concept on this session:**

A platform by Stefan Brązień which wants to move Senesgrad from the past to the future.

**Victory scene:**

* Both the Water Tower and the Steam Museum get scrapped and made awesome again
* The people looking at the past grudgingly accept the future is more important
* Future Senesgrad actually will secure the future elections against other parties

**Motivations:**

* Get reelected
* Build a futuristic future for Senesgrad
* Help the local community according to vision of the future, not past

**Paths:**

* Not applicable here

