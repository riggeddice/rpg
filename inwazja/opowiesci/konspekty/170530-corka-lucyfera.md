---
layout: inwazja-konspekt
title:  "Córka Lucyfera"
campaign: corka-lucyfera
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170530 - Córka Lucyfera (HS, Kić(temp))](170530-corka-lucyfera.html)

### Chronologiczna

* [161222 - Kto wpisał Błażeja do konkursu?! (SD)](161222-kto-wpisal-blazeja-do-konkursu.html)

## Koncept wysokopoziomowy misji

Psinos 2.0. Tu się zaczyna.

## Kontekst ogólny sytuacji

Brak.

## Punkt zerowy:

* Ż: Określ, z czego słynie to piękne, nowe miasto, które jest na Terenie Utraconym. Aha, i jak się zwie.
* K: Nazywa się Storczyn Mniejszy; słynie z kur karmionych paszą opartą o czosnek, więc ich nie trzeba przyprawiać.
* Ż: Z czego przede wszystkim utrzymują się ludzie w tym mieście; jaki jest jego profil ekonomiczny
* R: Epicki czosnkowy przemysł. Fabryki, dystrybutorzy, uprawa, granulowany... premium czosnek ze Storczyna. Farmerzy, handlowcy, fabrykatorzy...
* Ż: Skąd Świeca dowiedziała się o tej sprawie i CZEMU Estrella została wysłana jako wsparcie Siwieckiego?
* K: Henryk wysłał prośbę o wsparcie. A czemu ona? Bo się na nią Ozydiusz Bankierz wkurzył. I jak pachoł prosi, dostanie bezużyteczną pachołkę.
* Ż: Skąd się tam wziął Siwiecki, który odnalazł tam faktyczne problemy natury magicznej?
* R: Jest księdzem na żołdzie księdza... dostał zlecenie na kolejny egzorcyzm. Został wezwany przez lokalnego księdza - Klepiczka
* Ż: Czemu jest w tym mieście nadreprezentacja ludzi ubogich i bezdomnych?
* K: Bańka z budżetu państwa; państwo dotowało czosnek, ludzie brali kredyty...
* Ż: Nazwij miejsce / strefę, gdzie często znajdują się bezdomni lub sieroty. Czemu tam?
* R: Powstawały farmy i fabryki czosnku; jest taka STARA megafabryka + farma czosnku. I ona padła a pola gdzieś indziej.

Żółw_cel: Mamy jakoś określone miasto i kontekst startowy postaci.

* Ż: Podaj jeden aspekt nieletniej "postaci" Raynora, którą spotka Coś Złego. Nazwij swoją.
* K: Kalina; aspekt to "ciekawski".
* Ż: Podaj jeden aspekt nieletniej "postaci" Kić, którą spotka Coś Złego. Nazwij swoją.
* R: Filip; aspekt to "nieśmiałość".
* Ż: Określ lokalizację spotkania.
* K: Obok dawnego składu czosnku. Nikt tam nie mieszka. Jest na uboczu i w ogóle.
* Ż: Określ przyczyny, dla których Kalina i Filip tam się znaleźli.
* R: Lokalna fundacja charytatywna wydaje w tamtym miejscu zupy.

Żółw_cel: To da nam pierwszą scenę startową do pkt 0.

**Scena startowa:**

Nasze tymczasowe postacie - Filip i Kalina - są w okolicach składu czosnku, bo tam rozdają zupę. Postacie biorą zupę. Dosiada się do nich Mateusz Misiokruk; ukradli mu trzy dni temu na prośbę księdza kiedyś jakieś manuskrypty satanistyczne.

Filip natychmiast wysyła do innych kolegów sygnał "wtopa". 5v4->S. Dzieciaki rozlewają zupę, robią ogólny chaos... Filip i Kalina uciekają. Uciekają do swojej bezpiecznej kryjówki - chatka, dom bogacza w tym mieście (czosnkowy mogul, któreś pokolenie) - Tomasz Kantosz; ma altankę a niedaleko ma budę dla psa. A pies ich lubi. Uniknęli poszukiwań ze strony satanistów.

Do momentu aż przyszła Luksja, osobiście. Torturowała psa, żywiąc się jego energią i asymilując Magią Krwi. Pies (już nieumarły) znalazł ich kryjówkę. W reakcji, Filip zaatakował nożem; pies go przewrócił. Kalina zaatakowała dyskretniej, ale zatrzymała się na tarczy Luksji. Luksja wzięła ich obu. Musi z nimi poważnie porozmawiać...

* Ż: Co złego się im stało, nie łamiącego Maskarady?
* R: Znaleziono ich nieprzytomnych, wyziębionych na skraju rzeki niedaleko miasta.
* Ż: Co paranormalnego ich zmieniło? W jaki sposób? Czemu to jest złe?
* K: Potrzebuje Pryzmatu. Część ludzi modyfikuje, by wierzyli w to, czym ma być Pryzmat. Oni stali się głosicielami Pryzmatu.

Żółw: To nam daje dowód na paranormalność i pokazuje z czym mamy do czynienia.

* Ż: Nazwij istotną osobę, która może Wam pomóc w rozwiązaniu tego problemu.
* K: Wśród satanistów jest ukryta opcja antysatanistyczna. Janina Jasionek.
* Ż: W jakim miejscu rezydualna aura magiczna jest szczególnie silna i niebezpieczna?
* R: Miejski dom kultury.
* Ż: Nazwij dominującą szkołę magiczną w okolicy sierocińca.
* K: Sfera Zmysłów.
* Ż: W jakim miejscu bazę mają Estrella i Henryk?
* R: Plebania Zenobiego Klepiczka.

Żółw_cel: Zbudowanie wsparcia kontekstowego.

**Ścieżki Fabularne**:

* **L_Konwersja satanistyczna**:        0
* **L_Transformacja w thralle**:        0
* **L_Przygotowanie artefaktu**:        0
* **L_Zdobycie władzy politycznej**:    0
* **L_Skażenie magią**:                 0
* **L_Anarchia w mieście**:             0
* **L_Uruchomienie Artefaktu**:         0

## Misja właściwa:

**Dzień 1:**

Henryk, znajdujący się na plebanii, wie, że ktoś grzebał przy nieszczęsnym Klepiczku. Mentalnie. I Henryk chce dowiedzieć się, co mu zrobiono. Pasywne badanie wykazało, że na nim znajdują się stare zaklęcia mentalne. Dzień, dwa. Nie ma pułapek, nie ma katalizy. Zaklęcia wskazują na brak finezji ale sporą moc magiczną. (F,S).

KF: Zmiana zdania innej strony: Klepiczek już NIE CHCE egzorcyzmów. 

Henryk próbuje przeskanować, co zrobili Klepiczkowi. R,S. Przyszła Luksja, użyła mentalki, zmusiła go do splugawienia hostii, zabrała dokument, przypomniała, że kazał zrobić to dzieciom. Powiedziała, że "taty tu nie ma" gdy Klepiczek powiedział "Apage Satanas". Aha, przekształciła go trochę w kierunku na satanizm i wymazała pamięć. AHA, i powiedziała "pierwsza zakonnica jaką zobaczysz - Twoja".

KF: Seks, przemoc lub krew: Klepiczek zakocha się w Estrelli NA ZABÓJ.

Henryk zatem próbuje wyczyścić Klepiczka. Niech ten się naprawi; Henryk nie potrzebuje kolejnego satanisty. Henryk potrzebuje Klepiczka jako wsparcia. Odwołuje się do "wspólnych" aurorytetów - wybitny kaznodzieja, papież... 6v5->S. Udało się.

**Dzień 2:**

No i w końcu Estrella dotarła na plebanię. Sprawdziła - NIE, nie pomyliła adresu. Skontaktowała się z Henrykiem. Tak, to jest właściwe miejsce. ON jest księdzem. ONA Diakonką. Spotkanie było... ciekawe. Ksiądz Klepiczek był zaznajomiony z tym, że przyjdzie do Henryka "zakonnica". Estrella się... ucieszyła w lekko złośliwy sposób. Będzie zakonnicą. Ale potrzebny jej jest odpowiedni strój. Siwiecki załatwił...

Siwiecki przedstawił Estrellę księdzu Klepiczkowi. Ta od razu spróbowała wywrzeć wrażenie pięknem. Udało jej się. Ksiądz Klepiczek w jej obecności wyraźnie skupił swoją uwagę na niej.

Klepiczek: Zakochany do nieprzytomności (5 aspekt)

Siwiecki facepalmował. Przypomniał sobie jak Luksja zrobiła TO Klepiczkowi. Zenobi zakomplementował Estrellę i oprowadził ją po plebanii. Opowiadał Estrelli odnośnie żywotów świętych i o tym, że kiedyś kościół katolicki nie miał celibatu... NAJGORSZA RANDKA EVER. Henryk po hipernecie zapytany przez Estrellę powiedział, że "córka Szatana" ma coś z tym wspólnego. Gdy Estrella wymówiła się zmęczeniem, ksiądz zaproponował jej swoje łóżko; sam pójdzie gdzieś indziej. Estrella wymówiła się ślubami. 4+2v5->S. Duży wpływ na Klepiczka; zdecydował się zgodzić z Estrellą.

RANDOM 6: 2,2,2,3,5,5 -> 1,1,1,0,1,1:

* **L_Konwersja satanistyczna**:        0/5
* **L_Transformacja w thralle**:        3/6 (tier 1)
* **L_Przygotowanie artefaktu**:        0/4
* **L_Zdobycie władzy politycznej**:    0/6
* **L_Skażenie magią**:                 2/12
* **L_Anarchia w mieście**:             0/12
* **L_Uruchomienie Artefaktu**:         0/10
* **Tor zauroczenia księdza**:         10/10 (tier 3)

Po hipernecie Estrella połączyła się z Siwieckim. Zimno, jak na nią, spytała, co tu się stało. Jest Diakonką, ale to nie jest normalne. Henryk powiedział, że to był kaprys pewnej czarodziejki... odzyskała sobie rytuał satanistyczny i walnęła księdzu urok. Aha, ZAKONNICA.

* Teraz tak na to patrzę, że sugerowanie przebierania się za zakonnicę mogło być niefortunne... - Henryk
* W rzeczy samej - Estrella, zimno
* Tak samo jak próba pokazania się z tej... ładnej strony... temu księdzu - Henryk, odgryzając się

Henryk zauważył, że tamta czarodziejka jest najpewniej ich celem. Mają do czynienia z rytuałami satanistycznymi. Myślał, że sobie poradzi... aż silna magia nie weszła w grę. Młode kobiety + silna magia == smutek. Powiedział o losie dzieci, które wykradły papiery; dzieci trafiły do szpitala. Luksja COŚ im zrobiła. A w opisie rytuału było "część musi wierzyć a część być przekaźnikami".

Trzeba zepsuć Pryzmat. Zdaniem Henryka. Część ludzi musi uwierzyć w Lucyfera, więc... trzeba coś z tym zrobić.

Estrella rzuciła się do ksiąg parafalnych. Co się tu dzieje. Kto z kim żyje, kto czym się zajmuje... perfekcyjne zbieranie danych. Na to jeszcze biblioteka lokalna. Skorelować dane z parafii z danymi z legend. Czy jest tu jakiś dziwny rytuał, coś związanego z Lucyferem? Estrella chce się dowiedzieć... zidentyfikować coś odnośnie tego rytuału. Chmura prawdopodobieństwa. Jakieś pojęcie, z czym się mierzy.

Wiedza o Rytuale: Bardzo rzadki, Niekompletne dane, Sekret. (2 baza, 2,4,2 -> 10).

Estrella wykorzystuje Bibliotekę (1), Dane ze Świecy (1) i Netherię (1, -1 surowiec). 3+3v6->S. Estrelli udało się znaleźć RODZINĘ rytuałów; nie wie, który to konkretnie, ale wie, jakiego typu są to rytuały. Ostatni aspekt jaki został to "Niekompletne dane". Typ rytuału to "wzmacniający maga kosztem populacji". Zwykle jest powiązany z Magią Krwi. Mało jest bezpiecznych rytuałów tego typu... Netherii przyszło do głowy wezwanie wsparcia, ale... nie musi. Jeszcze nie. Ma jakiegoś katalistę. Dodatkowo, ten typ rytuałów jest BARDZO głośny i wyczuwalny. Co najmniej dzień pracy.

A Henryk w tym czasie zdecydował się iść do Domu Kultury. Co tam jest, co tam się dzieje i z czym to jest związane. Henryk wyczuwa zaklęcia Brudne, Toporne, bardzo Naergetyzowane i z wysokim Skażeniem. Tam w środku, w Domu Kultury, coś jest. Henryk wszedł do Domu Kultury; tam spotkał się z panią z Recepcji. Skażenie aż buchnęło mu w oczy. A Skażona sala to sala komputerowa, ogólnodostępna. Henryk dowiedział się o "dzieciach tańczących ze Światłami", imprezie, która ma miejsce niedługo. Darmową.

Henryk uwiarygadnia swoją próbę wycofania się wieczorną mszą. 3+1v3->R; pojawiają się dzieciaki ALE Henryk opuścił Dom Kultury. I wysłał "mayday" na hipernecie do Estrelli. I ludzie są creepy i próbowali go zatrzymać.

Dzieciaki zaczęły gonić księdza. Ten podkasał sutannę i W NOGI! Szybko złapał jakiegoś postawnego mężczyznę, mentalką 1-sekundowy czar by dzieciaki opóźnić. Udało się 3v3->S. Zwiewa, i napotkał Estrellę. Ta ma 5 sekund. W scenie są Ludzie, Uwaga na Księdza. I jest Skażenie. Estrella nie ma czasu na sensowne działanie - kazała się przesunąć księdzu a sama użyła Zmysłów by ksiądz leciał dalej jako iluzja... 

Estrella musi poradzić sobie z tym, że Uwaga Na Księdza; ale dzieciaki się odbiły. Estrella chce wyprowadzić goniące dzieci z centrum. 5v4->S, z Efektem Skażenia.

ES: One step too far...: po mieście lata iluzja księdza Siwieckiego. Będzie biegać. SOLIDNIE. Cały czas (jakieś 48h). Aha, oprócz tego: Estrella i Henryk są niewidzialni.

Dzieciaki poleciały za iluzją. Estrella powiedziała Henrykowi, że MUSZĄ się nimi zająć; dzieciakom coś zrobił ten wrogi mag. Trzeba dowiedzieć się o co chodzi i im pomóc. Henryk powiedział, żeby Estrella zajęła się lepiej z nim tą salą komputerową...

Estrella niechętnie podzieliła się z Henrykiem na temat tego, co to za typ rytuału. 

RANDOM 4: 1,1,5,6 -> 1,1,0,1

* **L_Konwersja satanistyczna**:        2/5 (tier 1)
* **L_Transformacja w thralle**:        3/6 (tier 1)
* **L_Przygotowanie artefaktu**:        0/4
* **L_Zdobycie władzy politycznej**:    0/6
* **L_Skażenie magią**:                 2/12
* **L_Anarchia w mieście**:             1/12
* **L_Uruchomienie Artefaktu**:         0/10
* **Tor zauroczenia księdza**:         10/10 (tier 3)

Są niewidzialni, warto korzystać. Więc, jednak czas na zajęcie się nieszczęsną salą komputerową. Zwłaszcza, że tam powinien teraz ktoś być. W sali komputerowej są ludzie. Skażenie Duże (2), Sporo Ludzi (2). A komputery na oko coś tym ludziom robią...

Estrella zdecydowała się rozwalić różdżką (-1 surowiec) kable w ścianie. Nikt nie będzie miał prądu przez najbliższy czas. Sukces. Ludzie zaczęli się rozchodzić, bo nie ma prądu -> nie ma zabawy. I Estrella wraz z Henrykiem zostali sami ze Skażonymi komputerami.

Czar wspólny. Co się dzieje z tymi komputerami. Duże Skażenie (2), Brudny Czar (1) -> 5. 4+1v5->S; Efekty Skażenia.

ES: Interakcja z Pryzmatem: Dispell zaklęcia na komputerach +emisja Pryzmatu. +1 tor satanistów.

Co robił ten czar? Podprogowo programował ludzi w satanistów. Plus, "receptywnych" fotografowały kamerki i wysyłały na Facebooka do konkretnego konta. "Luksja Pandemoniae". Teraz tego czaru już zupełnie nie ma.

I to jest moment, gdy magowie poszli na plebanię się przespać...

**TO DO**:

* Dzieci biegające za wirtualnym księdzem
* Osłabienie Pryzmatu miejsca
* Osłabienie satanizmu w tej okolicy

**Ścieżki Fabularne:**

RANDOM 2: 1,5 -> 2,0; +1 za tor.

* **L_Konwersja satanistyczna**:        5/5 (tier 2 DONE)
* **L_Transformacja w thralle**:        3/6 (tier 1)
* **L_Przygotowanie artefaktu**:        0/4
* **L_Zdobycie władzy politycznej**:    0/6
* **L_Skażenie magią**:                 2/12
* **L_Anarchia w mieście**:             1/12
* **L_Uruchomienie Artefaktu**:         0/10
* **Tor zauroczenia księdza**:         10/10 (tier 3)

**Interpretacja Ścieżek:**

W okolicy pojawił się "biegający wirtualny ksiądz Siwiecki". Niestety, Luksji udało się sformować potężny kult satanistyczny, na odpowiednim poziomie jaki jest jej potrzebny. Już widoczna część potrzebnej Luksji populacji zmieniona została w thralle. Sama natura tych thralli jest jeszcze nieznana.

Ksiądz Zenobi Klepiczek jest zabójczo zakochany i zafascynowany Estrellą.

Będziemy kontynuować te Ścieżki.

# Progresja



# Streszczenie

Siwiecki poszedł pomóc z egzorcyzmami w Storczynie, stolicy czosnku - a tam prawdziwa "córka Lucyfera" jak na siebie mówi, próbująca wprowadzić jakiś mroczny rytuał wzmacniający maga kosztem populacji. Henrykowi i Estrelli udało się uniknąć ataków ze strony niebezpiecznych ludzi i thralli; rozmontowali kilka rzeczy przygotowanych przez Luksję. Niestety, ilość thralli ciągle rośnie i powstał potężny kult satanistyczny. A lokalny ksiądz - Zenobi Klepiczek - jest na zabój zakochany w Estrelli.

# Zasługi

* mag: Henryk Siwiecki, zaprzyjaźniający się z Zenobim Klepiczkiem, skutecznie uniknął ataku thralli, wyciągnął z głowy Klepiczka kluczowe dane o rytuale.
* mag: Estrella Diakon, twórczyni wirtualnego księdza-joggera i poszukiwaczka informacji o rytuałach... i komputerach.
* mag: Luksja Pandemoniae, tajemnicza czarodziejka mówiąca o sobie "córka Lucyfera"; czaruje krwawą magią i pragnie wzmocnić się kosztem poświęcania części ludzi.
* czł: Łukasz Tworzyw, arcykapłan satanistów; jeden z wysokich rangą sojuszników Luksji a kiedyś świecki człowiek w kościele. Złożył potężny kult satanistyczny.
* czł: Zenobi Klepiczek, ksiądz w Storczynie Mniejszym; głęboko zakochany w Estrelli przez machinacje Luksji. Pomocny, niewiele może zrobić stając przeciw takiemu złu.
* czł: Kalina Cząberek, nieśmiała; sierota i szczur kościelny. Ukradła satanistyczny manuskrypt dla Klepiczka. Luksja przekształciła ją w thralla. Siostra Filipa.
* czł: Filip Cząberek, ciekawski; sierota i szczur kościelny. Ukradł satanistyczny manuskrypt dla Klepiczka. Luksja przekształciła go w thralla. Brat Kaliny.
* czł: Janina Jasionek, satanistka, która... nie chce być satanistką. Nie spodziewała się czegoś TAKIEGO. Ale nie widzi możliwości ucieczki...

# Lokalizacje

1. Świat
    1. Primus
        1. Lubuskie
            1. Powiat Szarczawy
                1. Storczyn Mniejszy
                    1. Fabryczny
                        1. Stara Megafabryka Czosnku, wielki kompleks, teraz już używany przede wszystkim przez uboższych i bezdomnych jako "dom"
                        1. Nieużywany Składzik Czosnku, gdzie teraz organizacja charytatywna wydaje jedzenie ubogim
                    1. Luksusowy
                        1. Posesja Kantosza, gdzie kryjówkę zorganizowali sobie Filip i Kalina... co na Luksję nie pomogło
                        1. Miejski Dom Kultury, gdzie sataniści zorganizowali główny obszar indoktrynacyjny
                    1. Duchowy
                        1. Plebania, na której zamieszkał Henryk i Estrella jako zakonnica


# Czas

* Dni: 2

# Frakcje:


## Córka Lucyfera

### Koncept na tej misji:

Młoda dziewczyna z thrallami, próbująca doprowadzić do zwiększenia poziomu swojej potęgi z rozkazu "Lucyfera".

### Scena zwycięstwa:

* Miasto spowija krwawa poświata; dochodzi do rytuału transcendencji.
* Sama Luksja absorbuje energię miasta, stając się efektywnie najpotężniejszą czarodziejką w okolicy.
* Lucyfer będzie dumny ze swojej kochanej córeczki.

### Motywacje:

* "Mój ojciec będzie ze mnie bardzo zadowolony."
* "Stanę się najpotężniejszą czarodziejką na całym świecie."
* "Na pohybel kościoła i Boga. Mój ojciec zwycięży."

### Siły:

* Luksja Pandemoniae, czarodziejka: mentalistka, biomantka, infomantka, katalistka, Krew.
* Łukasz Tworzyw, najwyższy kapłan-satanista, w służbie Luksji.
* Sataniści Łukasza, duża grupa lojalnych satanistów.
* Thralle Luksji, Zmienieni w odpowiedniki Krwawców, ale przekaźniki energetyczne.

### Wymagane kroki:

* Skonwertowanie odpowiedniej ilości ludzi na satanizm: 5
* Transformacja indukcyjna odpowiedniej ilości ludzi w thralle: 6
* Zdobycie władzy politycznej: 6
* Przygotowanie Artefaktu Psinoskiego: 4
* Uruchomienie Artefaktu Psinoskiego: 5

### Ścieżki:

* **Konwersja satanistyczna**: 2,3: 
* **Transformacja w thralle**: 2,2,2: 
* **Przygotowanie artefaktu**: 2,2: 
* **Zdobycie władzy politycznej**: 2,2,2: 
* **Skażenie magią**: 3,3,3,3: 
* **Anarchia w mieście**: 3,3,3,3: nikt nie kontroluje, co się dzieje; 
* **Uruchomienie Artefaktu**: 5,10: 


# Narzędzia MG

## Cel fabularny misji

Otwarcie misji z "Córką Lucyfera", dla Henryka, by można było określić kim jest ta postać. To jest, Henryk. Nie Luksja.

## Cel techniczny misji

1. Weryfikacja nowego podejścia do magii - szkoły magiczne + specjalizacje
2. Gracz jest w stanie grać nie swoją postacią. Test: Estrella.
3. Losowe Efekty Skażenia
4. Losowe Komplikacje Fabularne
5. Weryfikacja frakcji v2
6. Weryfikacja INNEGO poziomu granulacji konfliktów: przez aspekty
7. Weryfikacja ścieżek fabularnych
    1. Akcja gracza: reakcja każdej frakcji
    2. Akcja frakcji: +(0,1,2) na losowy tor
    3. Mechanizm eskalacji (brutalizacji) meta-ścieżek
    4. Patrz: [Notatka 170520](/rpg/mechanika/notatka_170520.html)

## Po czym poznam sukces

1. Szkoły magiczne ze specjalizacjami
    1. Magia staje się potężniejsza; szersza, feel jest bardziej jak ElMet.
    2. Postacie są dobrze odwzorowana przez sztywne szkoły magiczne.
    3. Widzimy GRANICE działania postaci. Wiemy, co może zrobić magią a co nie.
    4. Specjalizacje magiczne nie przeszkadzają
2. Estrella
    1. Gracz będzie w stanie prawidłowo grać nie swoją postacią.
    2. Gracz będzie wiedzieć, jak grać taką postacią.
3. Efekty Skażenia
    1. MG zawsze ma coś, co może użyć; mniejszy koszt kognitywny
    2. Są interesujące i nie niszczą fabuły.
    3. Nie niszczą fabułę a ją wzbogacają - albo ciekawsze wyniki albo nowe wątki.
    4. Gracz jest zmuszony do WYBORU
4. Komplikacje Fabularne
    1. MG zawsze ma coś, co może użyć; mniejszy koszt kognitywny
    2. Są interesujące i nie niszczą fabuły.
    3. Nie niszczą fabułę a ją wzbogacają - albo ciekawsze wyniki albo nowe wątki.
    4. Gracz jest zmuszony do WYBORU
5. Frakcje
    1. Świat żyje; zawsze wiem, do czego dążą poszczególne strony aktywne
    2. Bookkeeping nie jest morderczy
    3. Gracze są w stanie wejść w interakcje z frakcjami i im przeszkadzać lub pomóc
    4. Zawsze wiem kto jest gdzie i co robi
6. Granulacja konfliktów
    1. Mamy różne typy konfliktów: 'jednostrzałowe' i bardziej interesujące, wieloetapowe
    2. Istnieje możliwość większego wykorzystywania Okoliczności podczas konfliktów
    3. Mniejsze wychylenia wynikające z pojedynczego testu - przegranie testu nie uniemożliwia osiągnięcia celu.
7. Ścieżki Fabularne
    1. Frakcje funkcjonują prawidłowo i ich cele postępują do przodu, jeśli nieskontrowane
    2. Gracze są w stanie osłabić frakcje lub nawet je skontrować
    3. Gracze, jeśli odpowiednio poświęcą, są w stanie brutalizować i osłabić frakcje poważnie.

## Wynik z perspektywy celu

1. Szkoły magiczne ze specjalizacjami
    1. Żółw: Chyba zostanie. Rozważam +1 za magię MIMO WSZYSTKO. Bo magia jest za słaba bez tego.
    2. Kić: Nie wiem...
2. Estrella
    1. Kić: Estrella jest niekompletnie zbudowana; brakowało Estrelli detekcji i de facto zmylania.
    2. Kić: Z powodu brakujących impulsów TROCHĘ mi brakowało co robić. Nie pamiętałam, że miała być profesjonalistką.
    3. Żółw: Mi się podobała Twoja gra Estrellą.
3. Efekty Skażenia
    1. Żółw: Duży sukces na tej misji.
    2. Kić: Zgadzam się. Naprawdę fajnie wyszło.
    3. Żółw: Ok, koniec testowania; wchodzi.
4. Komplikacje Fabularne
    1. Żółw: Duży sukces na tej misji.
    2. Kić: Zgadzam się. Naprawdę fajnie wyszło.
    3. Żółw: Ok, koniec testowania; wchodzi.
5. Frakcje i Ścieżki.
    1. Żółw: Świat żyje i jest postęp w ruchach przeciwnika. To jest super.
    2. Żółw: Aktualna implementacja działa chyba najlepiej.
    3. Kić: Frakcje, myślę, dobre jest ujawnianie torów gdy się o nim dowiesz.
6. Granulacja konfliktów
    1. Żółw: Świetne wykorzystanie aspektów przy badaniu rytuału. To wyszło jak chciałem.
    2. Kić: Nie skończyłam tego testu, zatrzymałam się w połowie...
    3. Żółw: Fabularnie musiałaś przerwać. Siwiecki potrzebował ratunku.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Modyfikatory:

|  .Okoliczności. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .MODYFIKATORY. |
|                 |          |         |         |            |                 |

Wynik:

| .Postać. | .Opozycja. | .Modyfikatory. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |   SFR   |
