---
layout: inwazja-konspekt
title:  "Oszczędzili na rurach"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170408 - Nie zabijajmy tych magów... (PT)](170409-nie-zabijajmy-tych-magow.html)

### Chronologiczna

* [170408 - Nie zabijajmy tych magów... (PT)](170409-nie-zabijajmy-tych-magow.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Dzień 1: 

Do gabinetu Pauliny przyszedł rano mężczyzna; Tadeusz Kuraszewicz. Jest chory - a przynajmniej, mówi, że jest chory. Wygląda... nędznie. Powiedział, że to zabrzmi głupio, ale... traci włosy. Wszystkie. Paulina robi badania, czy się przemęczał... odpytuje. I dwóch innych jego kolegów. Są pracownikami biurowymi; nie brzmi to Paulinie szczególnie niebezpiecznie. Zaczęło się to jakieś 3 tygodnie temu; włosy na głowie jeszcze ma, ale niektóre inne włosy zanikają... 

Paulina przepisała serię badań, część z nich droższych. Spojrzał na nią ze smutkiem. Ale wtedy Paulinę uderzyło - wyczuła napromieniowanie magiczne. Gość ma w sobie elementy (śladowe) tkanki magicznej. Jest w nim energia magiczna. A nie powinien - to człowiek, nie ma kanałów... Paulina poprowadziła rozmowę, po czym zaczyna wklepywać coś w kompa, po czym rzuciła nań czar diagnostyczny. Ale facet nie ma ze sobą nic srebrnego, więc może.

(6v5->F (pogorszenie),F (inny lekarz), S): Gościowi się niestety pogorszyło (będzie potrzebował L4), jest pod długotrwałym wpływem energii magicznej i najpewniej ta energia pochodzi gdzieś od niego z miejsca pracy. Paulina poznała naturę tego, w co się transformuje: w NIC. To Skażenie próbuje... usprawnić jego własności aerodynamiczne. Czy hydrodynamiczne. Zredukować tarcie. Też, próbuje... jest powiązane z jakimś... systemem eksperckim? Coś liczy. Znowu jakiś mag się bawi..? W odpowiednio długoterminowej perspektywie to go zabije. Ale raczej miesiące niż dni.

Tadeusz dostał serię badań i wyjaśnił, że jego biuro jest w mieści kilkanaście kilometrów stąd; w Kropwi Dzikiej. On pracuje w firmie Elgieskład, spedycyjnej firmie zajmującej się rozwożeniem produktów. Siedzi w obszarze biurowo-papierkowo-magazynowym. Tam, gdzie papiery.

Paulina nie jest mu w stanie w tej chwili specjalnie pomóc; wysłała go na badania, Paulina wygląda na trochę zaniepokojoną objawami i Tadeusz ma zrobić je asap i wrócić. Upewniła go, że to nie jest zaraźliwe; nic przynajmniej na to nie wskazuje. Gdy już wyszedł, trzeba się przejechać...

Paulina zabrała ze sobą podstawowe wyposażenie odnośnie magii i poszła. Pojechała. I dotarła do Kropwi Dzikiej, do siedziby Elgieskładu. Poza godzinami pracy. Elgieskład znajduje się w biurowcu; taki czteropiętrowiec z kilkoma firmami, dużo szkła i sympatyczny starszy strażnik w budynku. 

Paulina przygotowała się do rzucenia zaklęcia analizy energii magicznej z wyszukaniem źródła. Podjęła konieczne środki ostrożności, nie chcąc władować się na Węzeł czy efemerydę. Przez bezpieczniki i pośredniki. Przy okazji, akceptuje, że pójdzie flara. Wpierw wysłała więc sygnał "jestem tu i nikomu nie chcę szkodzić", po czym poszło zaklęcie badające.

(5v6->F(strażnik), R(konstruminusi)): Paulina rzuciła zaklęcie. Dała radę zlokalizować coś zaskakującego - pływy energii magicznej docierają do tego budynku... rurami. I nie tylko tego budynku. Ogólnie rury są wektorem bardzo niskiego poziomu energii magicznej. Z jakiegoś powodu, w tym budynku ta słabiutka energia magiczna rezonuje z... czymś i w tym budynku jest mocniejsza. Ale coś... przecieka. Nie na poziomie efemeryd ale na poziomie "bakterie będą mutować".

Do Pauliny podszedł strażnik. Spytał, czy wszystko w porządku; Paulina potwierdziła, lekko zagubiona. Powiedział jej, że rozebrali płot, ale to teren prywatny. Paulina przeprosiła i oddaliła się o kilka kroków. Gdy Paulina przeszła parę kroków, zaczepił ją lokalny konstruminus. Powiedział jej, że jest mechanicznym terminusem ( ;-) ) i jak wygląda sytuacja - z pięciu magów normalnie w okolicy czterech pojechało na wakacje. Jeden został. Konstruminusy pilnują, by nic złego się nie stało i się nie dzieje. Teren bezpieczny, nie ma zagrożeń, nie ma problemów. Jak co, Paulina może z nimi rozmawiać na publicznym hipernecie. I jak co są dostępni.

Paulina w sumie chciałaby się spotkać się z lokalnym magiem. Tym, co został, kimkolwiek ten mag nie jest... 

Mechaniczny terminus (Eneus Mucro), zapytany o to, jak spotkać się z magiem powiedział, że w centrum są kamienice. I w jednej z nich znajduje się mag, który nie wyjechał. Nazywa się Archibald Składak. Eneus wyjaśnił, że Archibald siedzi w swojej kamienicy; przywożą mu jedzenie i nie kontaktuje się specjalnie z ludźmi ani innymi magami. Trochę samotnik. Ma wszystko gdzieś.

Paulina i tak zdecydowała się złożyć Archibaldowi kurtuazyjną wizytę. Bo czemu nie? Poszła do sklepu i kupiła bezalkoholową bombonierkę, przyzwoitą. Cała kamienica jest jego, więc Paulina po prostu nacisnęła na guzik z lekką emisją - idzie mag. Drzwi się przed Pauliną otworzyły...

Archibald Składak jest magiem dość zadbanym, koło 50-tki. Cała kamienica jest wysprzątana, wszystko przez demony i autosystemy. Kamienica trochę Paulinie kojarzy się z grobowcem; czy mauzoleum? Archibald uśmiechnął się do Pauliny z lekką melancholią i przywitał się z nią. Ubrany jest w mundur, coś... w stylu eleganckiego uniformu. Stara szlachta czy coś.

Archibald spytał Paulinę, czy ta nie chce posłuchać o przeszłości Składaków. Paulina była tak zaskoczona jego pytaniem, że aż chciała posłuchać. Dostała opowieść o Strażnikach, konkurencyjnych do Świecy. Ale Świeca eksploatowała ludzi a Strażnicy ich totalnie ignorowali - więc z czasem Strażnicy przegrali. Potężne, stare artefakty, zgromadzone źródła energi...

Paulina ostrożnie sprawdziła jak wygląda sytuacja - Archibald raczej ma cały świat "gdzieś", żyjąc swoim życiem i swoją przeszłością. Paulina uznała, że jakby zaczęła się inwazja demoniszcz i innych takich, Archibald nie wysunie nosa z kamienicy. Chyba, że inny "Strażnik" go poprosi. A Strażnicy to gildia. Ale - są też zasady honoru, godności, prowadzenia się, zasad...

Paulinę zaskoczyło pozytywnie to, że nikt nie jest jej tu tak naprawdę wrogi. Ale niepokojące jest to, co tu się dzieje. Bardzo niepokojące. Bo skąd ten wyciek?

Paulina jeszcze dostała od Archibalda akceptację jej działań na tym terenie. Jak długo nie dotyczy to jego kamienicy, co Paulina nie zrobi, Archibaldowi pasuje. Niech tylko zachowuje się z GODNOŚCIĄ.

Archibald nie wie gdzie są mieszkania pozostałych magów. Paulina "musi zapytać syntetyków". Oni Paulinie powiedzą. Zwłaszcza ten "miedziany". Paulina aż się zdziwiła. Archibald powiedział, że ci konstruminusi działają tu sprzed Zaćmienia i jego ukochana żona dobrze wspominała "miedzianego".

Tak. Żona. Zaćmienie. Paulina nie wnika.

Dzień 2:

Następnego dnia Paulina znowu przyjechała do Kropwi i połączyła się z konstruminusem. Sygnał po hipernecie. Konstruminus się pojawił. Paulina poprosiła go o wsparcie i ochronę; ma zamiar sprawdzić coś, co ją zaniepokoiło. Konstruminus przeszedł w tryb chroniący Paulinę. Paulina idzie po śladzie... 

(5v5->F(pacjent), F(czas), R(koszt quarków)): Paulina nachodziła się cały dzień osłaniana przez konstruminusa, zużyła trochę kryształów Quark (-1 surowiec) i ogólnie... odbił się od niej pacjent (umówiła na jutro rano), ale znalazła gdzie jest źródło Skażenia. To konkretne miejsca w rurach zakopanych pod ziemią. Coś jest w rurach, coś blisko siebie. I to jest coś, co może wymagać wykopek...

Szczęśliwie, te wykopki to typowe "miejsce na rury" - nie ma tam czegoś super kluczowego. Ale pozwolenia, odcięcie wody... wszystkie takie rzeczy. Nawet Paulina nie do końca wie jak się za to zabrać. Ale to rury właśnie tu i tylko tu.

Czyli pierwszy problem jest tu, w rurach. A drugi problem jest w siedzibie Elgieskładu. Problem do rozwiązania jest na linii rury - Elgieskład. Jednego z tych dwóch punktów nie ma - jest znowu spokój do czasu. Nie ma obu - jest spokój. Ale póki są dwa, Paulinowi pacjenci będą cierpieć...

Dzień 3:

Po odprawieniu pacjenta, Paulina pojechała znowu do Kropwi. Z dużo mniejszym optymizmem. Ma pomysł jak to rozwiązać, ale... oki. Wezwała do pomocy Eneusa Mucro. Wprowadziła go w sytuację - jest energia magiczna wpływająca na ludzi. I ta energia krzywdzi tych ludzi. A w tym budynku jest jakieś wzmocnienie... i Paulina nie do końca wie jakie. Eneus spytał, czy ma to zlokalizować i usunąć. Paulina powiedziała, że ona by chciała tak zrobić.

Paulina powiedziała Eneusowi, że ludzie są dla niej istotni. Konstruminus powiedział, że oni nie mają znaczenia. Paulina powiedziała, że mają. Paulina poszła, że ludzie chronią, pomagają... w tony utylitarno-przyjazne. (6v3->S). Eneus zaakceptował rozumowanie Pauliny. Powiedział jej, że nie do końca ma jak dostać się do budynku "bezpiecznie i cicho". Jest jednostką bojową, nie infiltracyjną. Paulina powiedziała, że pójdzie z Eneusem. Eneus i Paulina pójdą, usuną strażnika (mentalka Pauliny) i spróbują zneutralizować Skażenie w Elgieskładzie.

Paulina zaproponowała pójść w dzień. W dzień wszystkie biura są otwarte i mniej osób zwróci na nich uwagę. Zwłaszcza z mentalką. Eneus zaakceptował swoją rolę - awaryjna siła ognia i eliminacja potencjalnego Skażeńca.

Poszli do biura. Paulina przygotowała czar mentalny by wszyscy ich zignorowali (4v4->S) i weszli do budynku, a potem do Elgieskładu. Kataliza Pauliny wystarczyła, by doprowadzić ją do papierów, ksiąg itp. W tamtym pomieszczeniu pracują ludzie, którzy na razie NIE zauważyli ekipy (bo zachowują się jakby mieli prawo tu być); na jednej z wyższych półek znajduje się źródło Skażenia. Źródło to jest słabe. Ludzie pracują nad formularzami; nieszczególnie patrzą do góry. Eneus wzbił się w powietrze, wziął teczkę i wylądował.

Zespół opuścił firmę. Bezpiecznie. Nikt im nie przeszkadzał. Największe zdziwienie Pauliny.

Oki, wyszli i poszli z folderem do siedziby Słojozen. Mała firemka zajmująca się słoikami. Paulina ostrożnie wyjęła zawartość folderu. Zobaczyła lustro. Odechciało jej się badać.

* Artefaktyczne lustro. - Eneus
* Tak. Najchętniej zniszczyłabym w bezpiecznej odległości... - Paulina
* Możesz sprawdzić jego własności? To niebezpieczne. - Eneus
* TECHNICZNIE rzecz biorąc, mogę... - Paulina, niechętnie

Konstruminus wziął lustro i zaczął wysyłać serię sygnałów. Paulina obserwując to stwierdziła, że to lustro działa jako... wzmacniacz. Części sygnałów. Nie złapało jeszcze "wrogiej" osobowości. Jest to... dziwny artefakt. Paulina jednak je przebada...

(5v4-> F(jest czyjeś), R(jest OGÓLNIE bezużyteczne)): Paulina bardzo ostrożnie bada lustro. Lustro jest powiązane z jakąś sygnaturą maga. Jakiś mag wykorzystywał to lustro do... czegoś. Lustro to wzmacniało energię magiczną powiązaną z jakąś sygnaturą. To lustro dostawało waveband magii i wzmacniało część wavebanda kosztem reszty. Najpewniej wzmacniało to moc magiczną maga w jakimś zakresie kosztem innego zakresu. A jako, że tam dochodziła szeroka energia magiczna, lustro robiło to co powinno - i wzmacniało... zakres. 

Przerabianie ludzi w "opływowość" Paulinie kojarzy się jednoznacznie z tym, że nie ma "woli" po drugiej stronie. To interakcja artefaktów czy efemeryd. To nie jest "wola", że ktoś próbuje coś kosztem ludzi zrobić. No i Paulina ma sygnaturę tajemniczego maga - właściciela lustra.

Paulina nie jest dość chętna do przestrajania lustra. To jest niebezpieczne. Eneus spytał Paulinę, czy chce, by on przejął odpowiedzialność za to lustro. Paulina poprosiła o przechowanie, acz nie chce go oddawać. Eneus przyjął; to lustro tu poczeka na nią.

Eneus, zapytany przez Paulinę, powiedział, że to nie sygnatura żadnego z magów, którzy wyjechali. Acz któryś z tamtych magów POWINIEN być powiązany z magiem "od lustra". Eneus kiedyś w przeszłości spotkał się z tą sygnaturą. Bardzo przelotnie.

Ku zdziwieniu Pauliny, Eneus powiedział, że lokalni magowie nie uruchomili konstruminusów. Te dwa konstruminusy są tu "stałymi bywalcami". Na tym terenie nie ma terminusów, więc Świeca przydzieliła tą dwójkę. Ten teren nie jest warty wysłania terminusa; oni są tu by pilnować i ostrzegać. 

Została jeszcze kwestia źródła energii w rurach.

Paulina powiedziała Eneusowi, że chce się dostać do rur. I nie do końca ma pomysł jak to zrobić niskokosztowo. "Rury czasem się psują". Paulina poprosiła Eneusa, by ten zrobił "coś" by zakład wodociągów musiał przyjechać i wykopać rurę. Eneus uruchomił działo wstrząsowe i uszkodził rurę (niedaleko Skażenia). Trzeba będzie naprawić ;-).

Ludziom zaczęła kończyć się woda w domach. Zakłady wodociągowe zaczęły lokalizować awarię... Paulina, ku swemu wielkiemu rozczarowaniu, zauważyła, że wodociągi szukają. Eneus wyśle sygnał do Pauliny jak coś się zmieni. Paulina wróciła do siebie, do Krukowa...

Dzień 4:

Eneus wysłał sygnał. Wykopki! Paulina radośnie (przesadziłem ;p) ruszyła do Kropwi. Paulina świadkiem jak pracują nad rurą, przecinają, wymieniają fragmenty... a Paulina trzyma się w okolicy szukając źródła Skażenia. (6v4->S). Paulina wykryła źródło Skażenia. To dwie rury, dokładniej ich odcinki, bardzo blisko studzienek. To coś... "przecieka" magicznie. Na tych rurach coś jest i to "przecieka" do środka.

Ludzie pracują blisko, acz nie bezpośrednio, więc niewiele im grozi. Poziom energii magicznej jest Skażający, acz bardzo słabo. Paulina zdecydowała się poczekać do końca dnia roboczego i... do roboty.

Eneus powiedział Paulinie coś ciekawego. Na rurze jest... coś. Zamocowane sztucznie. Sensory Eneusa informują go, że to bomba. Struktura techorganiczna oparta o magię? To musi być bomba.

* Nie potrafię zlokalizować zapalnika ani ładunku; bardzo zaawansowana struktura bomby - Eneus
* Albo to nie bomba. - Paulina
* Co innego można zamontować o takich parametrach? I po co? - Eneus
* Kto montowałby taką bombę na RURZE? - Paulina
* Jestem terminusem. Nie zadaję takich pytań. - Eneus

Paulina połączyła się po "niekoszernym hipernecie" ze znajomymi z półświatka magicznego. Ma sygnaturę tego co się dzieje. Paulina jest katalistką... ale nie wie co ma badać. Szuka ekspertów w mrocznym półświatku od... biotech, artefaktu, dziwne zastosowania tychże. (-1 surowiec).

Powiedziała, że jest w tej okolicy i ma taki zgryz. I nie wie co się dzieje. 

* Czekaj, młoda. Na rurze, koło studzienki, tak? - Maciej
* No... - Paulina
* Elementy biologiczne i czujniki, tak? - Maciej
* I przecieka do rury... - Paulina
* Czyli chujowo zrobione. - Maciej
* Nie poradzę... - Paulina
* Czy to po prostu nie jest pieprzony CZUJNIK przepływu wody? - Maciej
* Ja się nie znam... ale kto by robił taki? - Paulina, zaskoczona
* Jest okazja. Ludzie potrzebują czujnik. Wsadzasz czujnik. Dostajesz kasę. Tak to działa. - Maciej, objaśniając cierpliwie Paulinie jak działa ekonomia.
* Jak to zdjąć? - Paulina, skonfundowana
* No... rozmagicznij. Ludzie przyjdą, powiedzą "nie działa" i postawią nowy. Sami ci zdejmą. - Maciej, rozbawiony bezradnością Pauliny

...brzmi legalnie...

Paulina zrobiła badania magiczne. Yup. Czujnik. Czujnik ze "sprytnymi" rzeczami - ktoś wzorował się na nanobotach... które właśnie wymierają z braku wody XD. Paulina zrozumiała co się stało i facepalmowała. Czujnik oparty jest o biofilmy. ALE! Te biofilmy zaczęły umierać. Ktoś np. spuścił coś czego nie powinien, albo po prostu zmieniło się coś w wodzie. I to zaczęło powodować przeciekanie...

Ile i gdzie tego?! Maciej pomocny jak zawsze powiedział Paulinie, że musi iść do papierków i to znaleźć...

Paulina rozmagiczniła czujniki i wróciła do Krukowa. TAK BARDZO PROBLEM... Wszystko naprawione...

# Progresja

* Paulina Tarczyńska: ma dziwne lustro wzmacniające, należące do nieznanego maga

# Streszczenie

Gdy do gabinetu Pauliny przyszedł Skażony człowiek, Paulina poszła mu pomóc. Wchodząc w sojusz z rozgadanym konstruminusem zauważyła, że w Kropwi Dzikiej Skażenie pochodzi z rur. Ciężkie badania i starcia z biurokracją pozwoliły jej odkryć mroczną prawdę - jakiś mag na boku dorobił sobie wsadzając magiczne czujniki wody... wyszło taniej. I zaczęły przeciekać magicznie. Też, Paulina pozyskała tajemnicze lustro nieznanego maga powiązanego jakoś z tym miastem.

# Zasługi

* mag: Paulina Tarczyńska, dzielnie walcząca z randomowym lustrem i uszkodzonym czujnikiem wody. Nie jest to najbardziej ambitny moment jej życia.
* czł: Tadeusz Kuraszewicz, nieszczęśnik zmieniany w opływowego bloba przez Skażenie; pracuje w Elgieskładzie jako gość od papierków.
* czł: Piotr Pyszny, strażnik biurowca gdzie jest Elgieskład; odnotował obecność Pauliny gdy ta badała źródło Skażenia.
* vic: Eneus Mucro, konstruminus na Mazowszu; dość autonomiczny jak na syntetycznego terminusa. O dziwo, pomocny i proaktywny.
* mag: Archibald Składak, ostatni "Strażnik", który dobrze Paulinę przyjął... acz nie zamierzał niczego robić. Sporo opowiada.
* mag: Maciej Brzydal, mag z półświatka, który niejeden już przekręt zrobił z ludźmi; zna się z Pauliną i jej z radością wyjaśnił podstawy ekonomii.
* org: Elgieskład, spedycyjna firma zajmująca się rozwożeniem produktów w Kropwi Dzikiej (Mazowsze); pracownicy ulegają Skażeniu. Było tam dziwne lustro.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, do którego przybył chory Kuraszewicz (pracujący na co dzień w Elgieskładzie)
                1. Kropiew Dzika, małe, acz dość nowoczesne miasto
                    1. Centrum
                        1. siedziba Elgieskład, gdzie znajdowało się tajemnicze lustro stanowiące wzmocnienie Skażenia
                        1. eleganckie kamienice, gdzie mieszka Archibald Składak - "Ostatni Strażnik"
                    1. Wschód
                        1. siedziba Słojozen, firmy, gdzie mieści się kwatera konstruminusów; zajmują się słoikami...

# Czas

* Dni: 4

# Narzędzia MG

## Cel misji

* Test misji pod kątem maksymalnego open-end
* Test misji "bez przeciwnika"
* Sprawdzenie czy da się zbudować dużo konfliktów walcząc z Molochem

## Po czym poznam sukces


## Wynik z perspektywy celu

