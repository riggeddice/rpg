---
layout: inwazja-konspekt
title:  "Zajcew ze śmietnika partnerem..."
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151229 - ..choć to na sektę nie pomoże (PT)](151229-choc-to-na-sekte-nie-pomoze.html)

### Chronologiczna

* [151101 - Mafia Gali w szpitalu (PT)](151101-mafia-gali-w-szpitalu.html)

## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa (Orank) i sektę (Kły Kaina); ślady przekierowuje na nich.
Wiła nie żyje, zatruła się narkotykami gdzieś w lesie; Joachim Zajcew szuka Kłów w sprawie "zabitej przez nich wiły".
Policjanci na SERIO szukają śladów tajemniczej krwawej sekty. Coś w przeszłości się tu działo.
Podobno była tu krwawa relikwia nazistowsko-satanistyczna.
Maria dorabia pracując dla "Rytmu Codziennego"; informuje Paulinę, że baza sekty jest w lesie przy Gęsilocie.
Sekta konsoliduje siły i wyniszcza lokalnego gangstera.

## Stawki:

- Czy "Kły Kaina" ustawią skutecznie szkołę sztuk walki uczącej 'Faim de Loup'?
- Czy Łukasz Perkas da radę uchronić się przed nędznym losem (nieważne skąd przyjdzie ów los)? 
- Czy Korzunio skutecznie ukryje prawdziwą przyczynę śmierci wiły przed Joachimem? \| TAK, Paulina zdecydowała się uderzyć sektę, więc skierowała Joachima na nich.

## Misja właściwa:
### Faza 1: Destabilizacja: wszyscy tracą.

Dzień 1:

Paulina wraca do domu po skończonej pracy; po drodze usłyszała z pobliskiego kontenera na śmieci dźwięk jakby kot się zatrzasnął. Otworzyła, a tam nie kot. Joachim Zajcew leży w śmieciach, zalany głęboko i cuchnie zarówno śmieciami jak i tanim alkoholem. Nie jest zbyt przytomny.
Paulina zadzwoniła do Jerzego Karmelika. Poprosiła go o wzięcie rękawiczek i podjechanie do niej; ale KONIECZNIE samochód wyłożyć czymś. Nie powie czemu, pełna konspira. Karmelik przyjechał; jest w końcu winny Marzenę Paulinie. Paulina pokazała mu nieprzytomnego Zajcewa i powiedziała, że chce mu pomóc. Niech on nie pyta - normalnie, zawiadomiłaby policję, ale ostatnio... sam wie. Karmelik oczywiście nic nie wie, ale poczuł się częścią Czegoś Większego i nie będzie pytał. Wpakował nieprzytomnego maga do auta i zawiózł go do garażu Marzeny (teraz: Pauliny). Uniósł brwi widząc przygotowaną bazę, ale nic nie powiedział. Jest zarówno naiwny jak i lubi dreszczyk emocji.

Karmelik odjechał a Paulina uśmiechnęła się; KTOŚ ma tu NullField. Paulina wpierw potraktowała go NullFieldem (by wyniszczyć wszystkie pułapki), potem magiczne przesłuchanie póki jest nieprzytomny i nie do końca kontaktuje. Paulinie moment zajęło nawet magiczne odróżnianie prawdy od fikcji (nigdy nie wiadomo czy to co przytrafia się Zajcewowi jest prawdą czy fikcją)

K: Droga maga do śmietnika?
Ź: Dowiedział się o czyścicielu (Korzunio) i go znalazł. Korzunio go przekonał, że to sekta ukradła mu wiłę. Gdy Joachim poszedł kłócić się z Rekinem (który w ogóle nie wiedział o co chodzi) skończyło się na Zajcewie który stracił przytomność, został zalany w trupa i wyrzucono śmieci.
K: Gdzie Joachim Zajcew ma bazę i jak się do niej wbić?
Ź: Jeździ żukiem. Niemagicznym żukiem. Był bardziej magiczny ale coś się w nim spsuło; potem dopiero doń dotarło, że Przodek.
K: Co sprawi, że Zajcew sobie stąd pójdzie w cholerę?
Ź: Albo odnajdzie swoją wiłę, albo spróbuje odnaleźć jej ciało i ją pomścić. A zemsta jego będzie niczym rozpalone żelazo. Chwilowo wierzy, że to sekta.

Paulina jest w rozterce. Z jednej strony podoba jej się to, że jest tu mag który może pomóc skroić potencjalnie niebezpieczną sektę. Z drugiej strony... Gala i Tymek mogą uciec. Może im się upiec! Trudna decyzja... Paulina jednak zdecydowała, że wytępienie kultu jest dla niej ważniejsze niż zemsta. Kult jest większym problemem, cokolwiek nie mówić.

Paulina postanowiła zgasić światła, wygodnie usadowić Zajcewa, podać mu miskę wody z elektrolitem do chłeptania (stwierdzam chłeptanie bo kac) i zostanie. Nawiąże kontakt. Zajcew doszedł do siebie i faktycznie udało im się porozmawiać. Paulinie całkiem spodobało się to, że młody Zajcew nie poleciał na nią z mordą a całkiem realnie ocenił sytuację. Pogadali sobie - ona jako lokalna lekarka nie chcąca mieć sekty na tym terenie, on jako przyjezdny artysta-pirolita chcący odzyskać swoją wiłę lub rozwalić tych, którzy mu ją zabili. Zajcew domyśla się, że ona już nie żyje. Ale nie traci nadziei; z jakiegoś powodu zależy mu na tej wile. Doszli do porozumienia - Joachim ani Paulina nie pytają tego drugiego. Mają prosty, krótki sojusz celowy. Wymiana informacji, pomoc i wspólny plan. Żadne nie rzuca się do akcji bez konsultacji z drugim. Joachim wywarł niezłe wrażenie na Paulinie - nieco ciapowaty, fatalistyczny realista. Ale nie boi się zatańczyć z ogniem.

Rozstali się wymieniając kontaktem (dwa telefony prepaid).
A Paulina wróciła do domu. Odpocząć.

HIDDEN:
- Joachim spotkał się z Wiesławem Rekinem. Idzie ostro; Rekin nie wie o co chodzi; Joachim kończy na śmietniku znaleziony przez Paulinę.
- Policja doszła do dealera w "Czarnym Dworze". Połączyli informacje o wojnie między organizacjami. Zaczęli akcję masową.
END

Dzień 2:

Rano znowu w pracy. Tam, plotki i ploteczki. Bryluje oczywiście Karmelik! Bratanka mu zwinęła policja! W nocy weszli! Że niby był ważną narkoszychą!
...co...?
Po naciśnięciu, okazało się, że to "nie były jakieś antyterrorysty" tylko przyszła Alicja i zwinęła chłopaka w dzień. Ale na dworze było ciemno! Historia się Karmelikowi posypała... tyle, że faktycznie policja wyraźnie zintensyfikowała działania...
Jako, że Paulina PAMIĘTA rozmowę Alicji Gąszcz z młodym, poważnie podejrzewa, że Alicja zamknęła młodego prewencyjnie.

I wtedy Maria rzuciła jej bombę. Kolejny artykuł Łukasza Perkasa - w tym opisuje związki mafijne między policją (a w szczególności Alicją) i przestępczością skupioną dookoła Czarnego Dworu (a w szczególności Ireneuszem Przaśnikiem). Maria powiedziała, że artykuły te są pisane "na szybko", "na kolanie" - ona by je dużo lepiej rozciągnęła, lepszy research... gość ma talent, ale teraz go po prostu marnuje. Niszczy sobie reputację. Paulina spytała Marię, czy jej zdaniem Perkas wierzy w ten kult. Nie. Nie ma żarliwości, jest chałtura. Ma jednak młodą żonę. Paulina zdecydowała, że chyba odwiedzi dziennikarza i przeczyta jego pamięć - co oni na niego mają...

Po pracy, Paulina przeszła się do dziennikarza. Przedstawiciel wyższej klasy średniej; mieszka w apartamentowcu. Paulina czeka pod apartamentowcem w którym on mieszka, z nadzieją że go złapie. I złapała, choć trochę poczekała - dziennikarze mają nieregularne godziny pracy. Przygotowała i rzuciła nań zaklęcie skanujące; ku swojemu niezadowoleniu, nie było to jej najsilniejsze; wyszedł jej brudny czar i musiała włożyć więcej energii niż chciała.

K: Czemu on współpracuje z sektą; co oni na niego mają?
Ż: Zmusili go i nagrali jego seks z nieletnią. I poinformowali, że jego żonę spotka to samo jeśli nie będzie współpracował. Plus, opublikują nagranie. Wszędzie.

Ochroniarzom apartamentowca się Paulina nie spodobała. Poprosili, by sobie poszła. Oczywiście, poszła sobie.
Paulina wróciła do siebie i zadzwoniła do Joachima - dowiedział się czegoś? Nie. Ale nawiązał kontakt z Korzuniem (nie powiedział Paulinie nazwiska) i czyściciel pomoże w rozmontowaniu potęgi lokalnej sekty, dodatkowo dostarczając własne oczy i uszy w okolicy.

Innymi słowy:
- by ratować sytuację, Tymek zabija wiłę.
- by wycofać się z terenu, Gala Zajcew ściąga Korzunia.
- by chronić Tymka i Galę i ukryć wszystko, Korzunio ściągnął sektę.
- by wyczyścić sektę, Joachim Zajcew nawiązuje kontakt z Korzuniem.
- by Zajcew mógł się zemścić za zabitą wiłę, Korzunio ma rozmontować sektę.

Paulina poprosiła jeszcze Marię, by ta poszperała o Wiesławie Rekinie. Może jest coś o nim i powiązaniach z sektą. Może coś, co sprawi, że będzie dało się go skompromitować lub zdyskredytować. Coś, co odbierze odrobinę pozytywnej reputacji. Albo nawet pozwoli napuścić na niego policję.

W telewizji lokalnej wiadomości. Młoda dziennikarka, Agnieszka Mariacka oznajmia, że dostała dostęp do niejawnych danych policji. Z nich wynika, że atak na Szpital Gotycki (ten, który przeprowadziła Gala Zajcew) to była robota krwawego kultu, który jest podejrzewany o odrodzenie przez policję. Podobno, kult próbuje werbować młodych ludzi i podobno jest to bardzo niebezpieczna sekta. Zalecana jest ostrożność. Dziennikarka oznajmiła również, że lokalna policjantka - Alicja G. - zostaje zawieszona w obowiązkach. 

Jeszcze jedna siła. Świetnie. Ci dla odmiany atakują sektę...

### Faza 2: Walka o kościół

Dzień 3:

Paulina idzie rano do pracy. Dostaje telefon od Zajcewa - standoff na parafii. Grupa sekciarzy weszła na teren parafii, ksiądz się zabarykadował. Paulina bierze dzień na żądanie i tam jedzie; tymczasem Zajcew i Korzunio się zabarykadowali i bronią kościoła przed kultystami. Korzunio weteran nie takie rzeczy już dla Zajcewów robił...
Kultystów jest dziewięciu. A Zajcew dostał od Pauliny info, że jedzie do nich lekarz (Paulina); też, ksiądz wezwał policję.

Paulina przybyła, gdy na szczęście już było po wszystkim. Z dziewięciu kultystów trzech ma wyraźne objawy potraktowania gazem pieprzowym, kościoła bronią Joachim Zajcew i Korzunio (ksiądz też jest ale się schował), Korzunio ściągnął jeszcze jednego łebka, policjantów jest dwóch - Artur Kurczak i Bartosz Bławatek. I jest z nimi pies. Kultyści po dłuższej dyskusji sobie poszli; nie chcieli walczyć w tych warunkach i okolicznościach. 

Ksiądz powiedział, że weszli na teren kościoła, ale Korzunio ("ten dżentelmen") uratował sytuację. 

Policjanci przesłuchali wszystkich, dowiedzieli się co chcieli (niewiele), po czym wzięli pieska i wyprowadzili go na spacer. Szukając napastników. W końcu mają pieska ;-). Jednocześnie, policja wezwała wsparcie - niech ktoś chroni ten kościół. Przynajmniej tymczasowo, potrzebny jest policjant do ochrony kościoła.

Paulina zorientowała się w pewnym curiosum. Nie czuje aury magicznej, żadnej. Jeśli tu jest relikwia, to mimo wszystko MOŻE coś powinno być. Ale przeszła się po kościele - i nic. Owszem, może coś być w lapisie - ale równie dobrze może być gdziekolwiek indziej. A Grażyna Tuloz coś za łatwo Paulinie powiedziała, że ta relikwia jest w kościele... Paulina sprawdziła kilka typowych miejsc; ale nie, nie ma tu ani oryginalnej relikwii ani jej podróby.

Maria skontaktowała się z Pauliną. Ma informacje odnośnie Wiesława Rekina.
- Wiesław Rekin jest faktycznie "ulicznym wojownikiem". Niebezpieczny i brutalny, słynie z mrocznych proroctw i okrucieństwa.
- Rekin był kilka razy skazany. Za pobicia i wymuszenia.
- Jest adeptem 'Faim de loup', ale z uwagi na to, że deprawował młodzież (werbunek do sekty) ma zakaz pracy z młodzieżą.

Paulina się uśmiechnęła od ucha do ucha. Mniej więcej tego potrzebowała. Wygrzebała adres mailowy Agnieszki Mariackiej i poprosiła Marię, by ta wysłała jej dyskretnie tą informację. I żeby Maria wysłała też tą informację policji (tak zaczęła się współpraca Marii i Artura Kurczaka).

Przy odrobinie szczęścia to doprowadzi do poważnego uszkodzenia dojo. Rekin wpadnie w kłopoty. Na drugim froncie, policja oraz lokalny gang pchają i uderzają w sektę. A Zajcew jest spacyfikowany i nie zamierza robić żadnych głupich ruchów bez konsultacji z Pauliną...

Czas na kontratak.

## Dark Future:

### Faza 1: Destabilizacja: wszyscy tracą.

|---------------|-----------------|-------------|-------------|--------------|--------------------|
| Frakcja       | Earth/Location  |  Air/People |  Water/Mood |  Fire/Action |  Void/Supernatural |
|---------------|-----------------|-------------|-------------|--------------|--------------------|
| Gala_Tymek    |       05        |     02      |      05     |      03      |        00          |
| 'Kły Kaina'   |       07        |     11      |      07     |      04      |        00          |
| Policja       |       10        |     05      |      05     |      05      |        00          |
| Przaśnik      |       09        |     04      |      03     |      08      |        00          |
| Parafia       |       10        |     15      |      10     |      00      |        02          |
 
- Joachim spotkał się z Wiesławem Rekinem. Idzie ostro; Rekin nie wie o co chodzi; Joachim kończy na śmietniku znaleziony przez Paulinę.
..... (Gala_Tymek.Earth_uu)
- Policja doszła do dealera w "Czarnym Dworze". Połączyli informacje o wojnie między organizacjami. Zaczęli akcję masową.
..... CompetentPolice.interrogate <someone> successfully getting <information>
..... Policja.F.uu, Przaśnik.E.d, Przaśnik.F.d, Kły.E.d, Kły.F.d
- Perkas opisuje, jak to policja się podlizuje Przaśnikowi (Alicja). Że tak naprawdę policja chodzi na pasku! 
..... BlackCult.sow discord and anarchy among <location>, presenting themselves as the only force to keep peace.
..... Policja.F.dd, Policja.W.ddd, Policja.A.d, Kły.F.d, Kły.A.u, Kły.W.u
- Przaśnik "zdobywa" materiały ze śledztwa wprowadzając atak na Szpital Gotycki; wszystko wina kultu.
..... LocalSmallMob.frame <someone> for distraction / as an attack
..... Przaśnik.F.d, Kult.A.dd, Kult.F.d
- Alicja zostaje wysłana na urlop przez szefostwo.
..... Policja.E.d, Policja.A.d, Policja.W.d, Policja.F.d

### Faza 2: Fortyfikacja: linie na piasku

|---------------|-----------------|-------------|-------------|--------------|--------------------|
|  Frakcja      |  Earth/Location |  Air/People |  Water/Mood |  Fire/Action |  Void/Supernatural |
|---------------|-----------------|-------------|-------------|--------------|--------------------|
| Gala_Tymek    |       07        |     02      |      05     |      03      |        00          |
| 'Kły Kaina'   |       06        |     10      |      08     |      01      |        00          |
| Policja       |       09        |     03      |      01     |      04      |        00          |
| Przaśnik      |       08        |     04      |      03     |      06      |        00          |
| Parafia       |       10        |     15      |      10     |      00      |        02          |
| Joachim       |       03        |     02      |      03     |      00      |        05          |

- Kult wchodzi na Gęsilocką parafię i szuka 'relikwii'. By ściągnąć "innych" kultystów.
..... BlackCult.make a show of unity and of power to impress <someone>
..... Parafia.E.ddd, Kły.E.d, Kły.W.uu, Kły.F.uuu
- Policja wbija i wypędza Kultystów z parafii; Bławatek i Kurczak nie dali się zastraszyć.
..... CompetentPolice.diffuse a difficult situation <event> without bloodshed
..... Policja.W.u, Policja.F.u
- Joachim 'tworzy' obraz swojej wiły by straszyć kultystów. Oni nadal nie wiedzą o co chodzi.
..... Avenger.remind <someone> the reason why avenger is on their tail
..... Joachim.F.uu, Joachim.V.d

# Zasługi

* mag: Paulina Tarczyńska, ta, która pociąga za wszystkie sznurki: Zajcew, policja, dziennikarze, Maria... "nic nie robi" a dzięki niej wiele się dzieje. Aka "Ania Diakon".
* mag: Joachim Zajcew, nieco fajtłapowaty artysta-pirolita, realistyczny fatalista. Sekciarze go upili i wrzucili do śmieci. Wszedł w sojusz z Pauliną ("Anią Diakon").
* czł: Maria Newa, niezastąpione źródło informacji dla Pauliny odnośnie wydarzeń lokalnych oraz przeciwników Pauliny.
* czł: Łukasz Perkas, który - jak się okazuje - jest skutecznie szantażowany przez sektę.
* czł: Wiesław Rekin, mistrz sztuk walki, który nie ma prawa pracować z młodzieżą. Też: wywalił Zajcewa na śmietnik.
* czł: Krystian Korzunio, który wpierw ściągnął sektę a teraz pracując dla Joachima (i Pauliny) chce zniszczyć sektę. Ostrzegł przed atakiem na kościół i go bronił.
* czł: Alicja Gąszcz, która została zawieszona za ścisłą współpracę z przestępcami.
* czł: Artur Kurczak, chroniący kościół i przekonujący kultystów, że to dobry moment by sobie poszli.
* czł: Bartosz Bławatek, posiadacz psa, który z Arturem wpierw bronił kościoła a potem poszedł szukać z psem dowolnego odizolowanego kultysty.
* czł: Jerzy Karmelik, straszny plotkarz ze skłonnościami do przesady. Też: po* maga Paulinie by być częścią Czegoś Większego. 
* czł: Agnieszka Mariacka, dziennikarka telewizji lokalnej, która ogłosiła, że kult się ponownie pojawił, najpewniej na zlecenie Przaśnika.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                    1. Osiedle Metalowe
                        1. Garaże osiedlowe
                    1. Równia Słoneczna
                        1. Apartamentowiec "Słoneczko"
                1. Gęsilot
                    1. Plebania gęsilocka
                        1. Kościół nowoczesny