---
layout: inwazja-konspekt
title:  "Atak na rezydencję Blakenbauerów"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140320 - Sprawa magicznych samochodów(AW, HB)](140320-sprawa-magicznych-samochodow.html)

### Chronologiczna

* [140320 - Sprawa magicznych samochodów(AW, HB)](140320-sprawa-magicznych-samochodow.html)

## Misja właściwa:

![](140312_AtakNaBlakenbauerow1.jpg)

# Zasługi

* mag: Hektor Blakenbauer jako osoba próbująca poskładać wszystkie fakty do kupy po ataku na Rezydencję.
* mag: Andrea Wilgacz jako analityk magiczny i osoba kojarząca źródła informacji ze wszystkich możliwych stron by odsłonić o co chodziło z projektem "Moriath".
* mag: Wacław Zajcew jako czarodziej który wykorzystuje kontakty wśród Zajcewów do poznania faktów o ataku na Rezydencję. Też: pyta Ottona o Mojrę.
* mag: Otton Blakenbauer jako bardzo potężny czarodziej który zmiótł napastników w Rezydencji i który bardzo dużo wie, ale szaleństwo uniemożliwia mu komunikację. Obsesja na punkcie Inwazji.
* czł: Borys Kumin, który umiera w pidżamie podczas ataku na Rezydencję Blakenbauerów do niczego się nie przydając (KIA). Nie wiadomo czemu go zabito.
* mag: Remigiusz Zajcew, taki typowy słup którego się wynajmuje by ukryć kto to robi. Powiedział, że atak miał wyglądać na zwykły napad a mieli podłożyć pluskwy.
* mag: Juliusz Szaman, który nasłał dwóch młodych do zdobycia informacji od Andrei i Wacława. Zamiast zapytać, zaatakowali... więc zostali pojmani.
* mag: Mojra, której tożsamość (Moriath) została wreszcie ujawniona. Nadal * jak nie żyła tak nie żyje.
* czł: Jan Szczupak, który wpuścił napastników na teren Rezydencji nie spodziewając się jakie będą konsekwencje. Jego rodzina była zagrożona.
* vic: Estera Piryt, nee Myszeczka, która wróciła dzięki Edwinowi do formy maga i opowiedziała, że pracowała jako sekretarka Mojry.
* mag: Teresa Żyraf, która bardzo boi się teraz Krystalii i zamieszkała w Bazie Detektywów by być chroniona.
* mag: Krystalia Diakon, bardzo niestabilna; zaatakowała Teresę i została obezwładniona przez Wacława.
* mag: Grzegorz Czerwiec, który okazuje się czysty i nie zamindwormowany.
* mag: Bolesław Bankierz, o którego śmierci właśnie się dowiaduje Andrea; zginął od noża w plecy podczas pojedynku (nożownikiem była osoba trzecia).
* czł: Elżbieta Niemoc, ukochana Edwina która jest zagrożona przez całą tą sprawę.
* mag: Waldemar Zupaczka, który bał się wymazania pamięci ale przyznał, że za nie*zapłacenie za psa wygadał wszystko co wie o Blakenbauerach... komuś. Przesłuchał martwego Borysa.