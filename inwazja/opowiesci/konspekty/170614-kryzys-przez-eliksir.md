---
layout: inwazja-konspekt
title:  "Kryzys przez eliksir"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170518 - Machinacje maga rolniczego (PT)](170518-machinacje-maga-rolniczego.html)

### Chronologiczna

* [170518 - Machinacje maga rolniczego (PT)](170518-machinacje-maga-rolniczego.html)

## Kontekst ogólny sytuacji

W myślach Diany zaczynają pojawiać się pytania: czy tak jak ona żyje, czy tak życie musi wyglądać? Nadal jest przekonana, że Ewelina Bankierz ogólnie jej pomaga... ale czy naprawdę to jest uczciwa cena za tą pomoc?

Ewelina osiąga elementy tego, co chce. Grazoniusz zrobił coś głupiego i wypadł z dworu Eweliny, Diana jest zestresowana przez okolicznych facetów i coraz bardziej skupia się na tym, że może w towarzystwie dziewczyn jest po prostu lepiej... co prawda u Diany powoli pojawiają się elementy godności, ale nadal jest zdecydowana zostać u Eweliny bardziej niż cokolwiek innego.

Hektor i Diana są w sobie już naprawdę zakochani.

Hektor powoli orientuje się, że nie jest w stanie wygrać przeciw potędze Bankierzy samotnie. Coraz bardziej zbliża się do Rodziny Dukata, zwłaszcza, że pierwsza dawka była za darmo. Coraz bardziej Hektora kusi, by rosnąć w siłę, być silniejszym i potężniejszym, by musieli się z nim liczyć.

Grazoniusz zaczyna się stresować swoją pozycją. Musi jakoś odzyskać sławę i poklask; w innym wypadku skończy jako ofiara dowcipów. Musi jakoś się zemścić na Hektorze ale tak, by to było wyrafinowane i nie robiło nikomu krzywdy - a by inni arystokraci mogli to docenić... no i musi wrócić do łask Eweliny.

## Punkt zerowy:

**Ewelina:**

* Diana nie jest zainteresowana mężczyznami:    2/8    Tier 1
* Presja społeczna na Grazoniuszu:              0/6
* Grazoniusz wypada z łask Newerji:             0/6
* Muszę być autonomiczna, Newerjo!:             0/6

**Grazoniusz**:

* Odzyskanie twarzy w swej grupie:              0/8
* Ustawienie Hektora na właściwym miejscu:      0/6
* Usprawnienie okolicy i pomoc Dianie:          0/8
* Uderzenie w Ewelinę:                          0/6

**Neutralna**:

* Diana zostaje u Eweliny:                      2/6     Tier 1
* Diana buntuje się przeciw Ewelinie:           2/8     Tier 1
* Diana wybiera swoją rodzinę:                  0/6
* Diana odchodzi w świat, samotnie:             0/6
* Diana wybiera Hektora:                        4/8     Tier 2
* Zadomowiony w okolicy Hektor:                 2/8     Tier 1
* Hektor zdesperowany, integracja z Dukatem:    2/8     Tier 1

## Misja właściwa:

**Dzień 1:**

Poranek. Grazoniusz przyszedł na śniadanie w wyjątkowo dobrym humorze. Pozbierał się dość wcześnie; wcześniej, niż poprzednio. Diana mu, naturalnie, usługuje. Przynosi jedzenie itp. Hektor NIE je śniadania, gdy je Grazoniusz. Nie chce się złościć.

Grazoniusz spytał Paulinę, czy ma dla "swojego parobka" jakieś zadanie. Paulina odpowiedziała ostro, że Hektor jest gościem. Grazoniusz powiedział, że chce, by Diana Hektorowi pomogła... ale jeśli za bardzo się nie pobrudzi. Paulina powiedziała, że Hektor robi w ogródku. Grazoniusz poprosił Dianę, by Hektorowi pomogła. Diana się wzdrygnęła - ku zdziwieniu Pauliny. Grazoniusz jest rozpromieniony. Paulinie się to bardzo, bardzo nie podoba.

Diana się oddaliła - jak powinna. Grazoniusz powiedział Paulinie, że rozłoży detektory głębokiej magii. Paulina jest zaalarmowana - Dukat ma gdzieś tu teleportacyjny beacon. Paulina zaznaczyła, że nie chce by pojawiały się tu jakieś specjalne urządzenia Skażające teren. Grazoniusz powiedział, że zrobi skan ręczny.

Paulina zaczęła knuć. Powiedziała, że Ewelina przejęła się za mocno - normalnie rezydentce nic nie grozi. Powiedziała, że ma temat, w którym przyda jej się kompetentny katalista. Pewna Wieża Ciśnień w Senesgradzie... skrzyżowana z leylinami anomalia, efemeryda i w ogóle kapsuła czasu. Grazoniusz zainteresował się tematem. Paulina spytała go, jak magicznie podszedłby do tematu. Odpowiedź ją zmartwiła. "Jeśli to efemeryda połączona z leylinami, to jest to źródło energii".

Technicznie, Grazoniusz ma rację. Praktycznie, Paulina wie, co to znaczy... Świeca nie ma tu obecności a jedna gildia już tu zginęła przez tą Kapsułę.

Grazoniusz jest bardzo zainteresowany. Paulina widzi, że on się chce wykazać. Takie "nie wiem jeszcze CO, ale muszę COŚ ZROBIĆ". Paulina spróbowała dowiedzieć się, jaka jest agenda Grazoniusza - różne aspekty jego motywacji. Po co mu ten skan w domu Pauliny? Oraz co się stało Dianie (wzdrygnęła się na myśl o pracy z Hektorem).

3+1v3->R. KF: Zmiana Okoliczności sceny. Coś się zmieniło w ogródku.        +2 akcje.

Grazoniusz wyjaśnił Paulinie, o co chodzi. Jest w rodzie czarodziejka - starsza czarodziejka, Newerja Bankierz. I ona trzęsie okolicą. Oddała udziały wszystkim młodszym Bankierzom. Swego czasu Apoloniusz wyrufusił Ewelinę z jej udziałów (rozkochał, zmanipulował i porzucił) i na bazie tego stworzył swoje małe imperium, odcinając się od władzy Newerji. Newerja stwierdziła, że Ewelina jest niekompentna finansowo. Nadal dała jej nowe udziały, ale powiedziała, że jakiś facet musi to trzymać. Bo Ewelina traci głowę i w ogóle.

I tu pojawiają się Grazoniusz i inni Bankierze. Młodzi kuzyni. Każde z nich chce wejść w spółkę z Eweliną, ale ona nie chce. I wymyśla jakieś głupoty. Grazoniusz tu się pojawił, bo Ewelina miała problem z Dianą i on chce pomóc. I jemu zależałoby np. na wykazaniu się przed Newerją. I np. zdobyciu listu polecającego od Pauliny. Newerja to wredna czarodziejka - ruch Apoloniusza jej się spodobał...

Paulina stwierdziła, że delikatnie musi wywiedzieć się, o co chodzi na linii Grazoniusz, Diana i Hektor. Grazoniusz jest w tej sprawie dość tajemniczy. Paulina spytała Grazoniusza, czy on dostał jakieś wytyczne od Eweliny w sprawie Diany. Nie. Grazoniusz powiedział, że Ewelina lubi swoje zwierzątko (Dianę). Więc... nowy konflikt.

4+1v4->S        +1 akcja

Grazoniusz, rozluźniony pozytywnym nastawieniem Pauliny powiedział, że jego towarzystwo się z niego śmieje. Powstały memy jak ucieka przed kurczakiem. Więc musi ośmieszyć Hektora i zrobić o nim podłą historyjkę. Podał Dianie eliksir zniesmaczenia, odwracający uczucia jej do Hektora. Hektor prędzej czy później zrobi coś głupiego i Dianę odepchnie. Potem będzie o nią walczył. To sprawi, że powstanie śmieszna komedia a on się zemści nie robiąc nikomu krzywdy. I wróci do łask towarzystwa. I Newerji.

Paulina opieprzyła Grazoniusza. Ten opieprzył Paulinę - przez Hektora traci swoją grupę społeczną. MUSI się zemścić. Nie ma wyjścia. Wybrał sposób, który nie robi nikomu trwałej krzywdy a on odzyskuje twarz. Newerja powiedziała wyraźnie - Ewelina może wszystkich tępić, ale przeciw niej nie można podnieść nawet paluszka. Tu Paulina u Grazoniusza wyczuwa naprawdę sporą dawkę frustracji... więc Ewelina się bawi i wszystkich podgryza a oni nic jej nie mogą zrobić.

Grazoniusz jest otwarty na inne metody. Ale nie ma pomysłu jak może NIE stracić swojej okrutnej, bananowej młodzieżowej grupy.

Paulina zrobiła wyraźne żądanie. Nie życzy sobie, żeby Grazoniusz czarował w JEJ domu, na JEJ terenie bez poinformowania JEJ o tym. Ma natychmiast zdjąć zaklęcie z Diany. Zakaz czarowania na JEJ terenie. W innym wypadku Paulina jest skłonna ściągnąć całą potęgę Świecy przeciwko Grazoniuszowi - jako rezydentka. Grazoniusz był do tej pory przyjazny Paulinie, więc Przyjazny działa przeciwko. Jest też dumny. Paulina jest Rezydentką i za nią stoi potęga Świecy. Paulina stanęła agresywnie.

1+2v3+1-> F,S. KF: 2 - Niefart sojusznika: Hektorowi coś bardzo, bardzo nie wyszło z naprawą Okoliczności tych ziół...     +1 akcja

Grazoniusz powiedział Paulinie, że ona wykorzystała go. Skłoniła do powiedzenia relacji między Bankierzami na tym terenie, potem wślizgnęła się w jego łaski tylko po to by potem tak mu odpłacić. On jej tego nie zapomni. Zrobi co ona chce, ale nie zapomni. Przy okazji - Grazoniusz wyprowadza się z tego budynku w trybie natychmiastowym a Diana idzie z nim.

Paulina odpowiedziała, że nie. Diana została przysłana przez Ewelinę by pomóc jej. A Grazoniusz... cóż, nie znajdzie sobie żadnego miejsca lepszego niż chata Pauliny. Grazoniusz parsknął i powiedział, że to nie będzie problem. Zapytał, jak niby ma chronić Dianę, skoro Diana ma zostać z Pauliną a on nie może czarować bez zapytania Pauliny? Paulina zignorowała pytanie.

Dobrze. Paulina nie chce mieć niebezpiecznego wroga. Zaproponowała mu deal. Ona da mu list polecający mimo wszystko. A jeżeli Grazoniusz chce dostać laurkę, niech pomoże jej z efemerydą z Senesgradu. A list polecający jest już teraz, z marszu. Tylko musi przestrzegać jej zasad - ona nie chce mu patrzeć na ręce, bo nie chce, by Grazoniusz naruszał jej system zasad. Albo się stosuje do jej zasad, albo odejdzie i dostanie list polecający. Ale jest pod jej kontrolą.

3+2v3+1-> F, F, S       +2 akcje

* KF: 13 - Zmiana zdania innej strony: siły Dukata zainteresowały się... Dianą.
* KF: 05 - Utrata, uszkodzenie sprzętu: ogródek w ruinie.

Grazoniusz niechętnie i nieufnie, ale zgodził się - wstępnie - na współpracę z Pauliną na takich warunkach. I tak przyjedzie swoim nowym domem. Paulina zrobiła nowy tor:

* Udobruchany Grazoniusz:                  2/6      Tier 1

Przesunięcie torów:

* Gwarantowane akcje: +1 na Ew1, +2 na Ew3, +2 Gr1, +1 Gr2
* Akcji: 5. Ew: 1,2,3,3,4 (1,0,1,1,0). Gr: 2,2,3,4,4 (1,1,2,1,1). Ne: 2,4,7,7,7 (1,2,2,2,1)

**Ewelina:**

* Diana nie jest zainteresowana mężczyznami:    4/8     Tier 2
* Presja społeczna na Grazoniuszu:              0/6
* Grazoniusz wypada z łask Newerji:             4/6     Tier 2
* Muszę być autonomiczna, Newerjo!:             0/6

**Grazoniusz**:

* Odzyskanie twarzy w swej grupie:              2/8     Tier 1
* Ustawienie Hektora na właściwym miejscu:      3/6     Tier 2
* Usprawnienie okolicy i pomoc Dianie:          2/8     Tier 1
* Uderzenie w Ewelinę:                          1/6

**Neutralna**:

* Diana zostaje u Eweliny:                      2/6     Tier 1
* Diana buntuje się przeciw Ewelinie:           3/8     Tier 1
* Diana wybiera swoją rodzinę:                  0/6
* Diana odchodzi w świat, samotnie:             2/6     Tier 1
* Diana wybiera Hektora:                        4/8     Tier 2
* Zadomowiony w okolicy Hektor:                 2/8     Tier 1
* Hektor zdesperowany, integracja z Dukatem:    7/8     Tier 3

**Paulina**:

* Udobruchany Grazoniusz:                       2/6     Tier 1

Przypomnienie ogródka i zdarzeń w ogródku:

* Zmiana Okoliczności sceny. Coś się zmieniło w ogródku
* Niefart sojusznika: Hektorowi coś bardzo, bardzo nie wyszło z naprawą Okoliczności tych ziół
* Utrata, uszkodzenie sprzętu: ogródek w ruinie.

Powrót do misji:

Paulina poczuła sygnał SOS od Diany. Znowu. Z jej własnego ogródka po hipernecie. Sygnał zawierał "efemeryda" i ogólnie jest spanikowany. Grazoniusz też to poczuł. Chciał iść, ale machnął ręką. Paulina powiedziała mu "pomóż" i Grazoniusz poleciał za nią.

Cały ogródek nie jest taki, jak powinien być. Mnóstwo dziwnych kwiatów i ziół. Przestrzennie jest "za duży" i ma oszałamiający zapach... Oszałamiający. Paulina rzuciła szybko brudny czar - odcięła węch sobie i Grazoniuszowi. Ale wpierw musi przetrwać atak (Oszałamiający Zapach). 4v3->S. Udało się Paulinie wszystko zabezpieczyć - siebie i Grazoniusza. Rzuciła to nań przez dotyk. Paulina widzi połacie terenu - jej... łąka? (ogródek). Połacie terenu. I fae...

Paulina zaczyna badanie tego cholerstwa. Kazała Grazoniuszowi się wesprzeć, sama ruszyła do diagnostyki. Paulina walczy z Kompleksowym i Splątanym bytem Emocjonalnym. 5+1v4. S i ESkażenia. Soczewka, dokładniej. To powoduje, że dla Pauliny byt już nie jest Kompleksowy. Zdarła aspekt.

Komponenty Zmysłów się rozsypały. Paulina widzi byt stworzony z tych wszystkich ziół, czerpiących z Diany i Hektora, wysysający z nich energię. Kwiaty są różnymi formami ciała Diany. To wszystko razem - łącznie z magami - stanowi jedną istotę. Coś naprawdę, naprawdę się spieprzyło...

Paulina ma do czynienia z ogrodem będącym Splątanym i Emocjonalnym bytem stworzonym z połączenia samego ogrodu i tych dwóch magów. Paulina próbuje to rozplątać, wyjąć Dianę i Hektora ze Skażonego ogrodu. 4+1v4->S. Paulina, ciężko skupiona, rozplątała Dianę i Hektora z tego bytu. Byt zaczął błyskawicznie degenerować bez źródła energii... zabierając ze sobą cały ogródek. Wymarł. Pragmatyzm Pauliny...

Paulina ma niezbyt ubraną i nieprzytomną Dianę, nieprzytomnego Hektora i martwy ogródek gdzie wszystko po prostu umarło...

Paulina złapała Dianę i wciągnęła ją do domu. Grazoniusz pomógł z Hektorem. Nic im nie będzie. Paulina narzuciła na Dianę prześcieradło... widzi jednak, że Grazoniusz patrzy na nią trochę jak na mebel, nie osobę. Usunął z niej eliksir, na życzenie Pauliny.

Całość: +2 akcje.

Paulina poprosiła Grazoniusza, by ten pojechał sprawdzić co się dzieje z TAMTĄ efemerydą. Wezwie mu taksówkę. Paulina chce by go tu nie było... zwłaszcza, gdy Hektor się obudzi. Niech Grazoniusz się rozejrzy - w ostateczności DELIKATNE badanie magiczne Efemerydy Kapsuły Czasu w Senesgradzie. Grazoniusz się zgodził - nie miał większego wyjścia - i pojechał.

Do Pauliny zza płota zagadał sąsiad, Janusz Wosiciel. Zabiadolił, jak to Paulinie wszystko wymarło. I spytał ostrożnie, czy Paulina ma nie do końca stabilną intelektualnie pacjentkę. Paulina zaczęła węszyć: 3v2->S. Janusz przyznał się, że Zośka rozpowiadała o dziewczynie u Pauliny co to chce biegać goło. A Paulina jej zabrania. Ale... Paulina nikomu nic nie mówiła..? I Zosia nie powinna nic wiedzieć..?

Paulina przekierowała uwagę Wosiciela. Powiedziała, że ma gości, ale... normalnych. Nie wie co Zosia..? I zasugerowała Wosicielowi, że Paulina jest kiepskim ogrodnikiem - zamieniła przez przypadek pestycyd w nocy za nawóz... i zabiła wszystko :-(. 2+2v3->F,R. 

* KF: 09 - Koszty działań rosną: ten ogródek szybko nie będzie sprawny... jest Skażony i co gorsza tempo musi być normalne :-(.                 +1 akcja
* KF: 16 - Wzrost znaczenia Wieży Ciśnień w Senesgradzie - Efemeryda nadal jest sprawna a ludzie w Senesgradzie nadal coś chcą z wieżą robić    +1 akcja

Po rozwiązaniu problemu Zosi Wiecznie Wiedzącej i Janusza Przedstawiciela Komitetu Sąsiedzkiego, Paulina musiała wrócić zająć się swoimi gośćmi. Szczęśliwie, Hektor jest pierwszy na nogach. Paulina zastała go załamanego. Wyjaśniła mu, co spotkało Grazoniusza - nie wolno mu czarować - i powiedziała, że Hektor też nie może nic z tym zrobić. Powiedziała mu, że cała ich praca poszła na marne. Hektor... nie jest zadowolony.

Paulina poinformowała też Hektora, że Grazoniusz zdjął eliksir z Diany i jaki to był eliksir. Pokazała mu, że ona MA władzę i potęgę. Hektor się tego nie spodziewał.

Hektor jest Bardzo Rozgoryczony, Zdesperowany i Żądny Zemsty (4). Paulina jest Rezydentką i jest Godna Zaufania (2). Pokazała Hektorowi, że stoi po jego stronie (1). I Paulina obiecała Hektorowi zemstę - JEŚLI on niczego nie będzie robił z tym tematem bo jej będzie przeszkadzał (1).

3+4v2+4-> R, KF: 4 - Kontr-akcja przeciwnika: Ewelina zaatakowała Grazoniusza po linii Newerji/przyjaciół. +1 tor Eweliny wypychania Grazoniusza i +1 akcja. -2 tor Hektor i Dukat.

Paulina odesłała Hektora, by ten się zajął ogródkiem (czy coś da się uratować) a sama skupiła się na Dianie... po chwili pracy nad nią Diana otworzyła oczy. Zauważyła prześcieradło - czyli zaklęcie przestało działać. Paulina szybko zorientowała się, że Diana ma wielki uraz i do Grazoniusza i do Hektora... ogólnie, do wszystkich mężczyzn na świecie. Ich to bawi. Ewelina ma swoje powody, oni chcą ją kontrolować dla zabawy.

Paulina ostro powiedziała Dianie, że nie może jej pokazać alternatywy, bo alternatywa wymaga wyjścia do świata ludzi a Diana nie jest w stanie 100% kontrolować iluzji, co wielokrotnie już udowodniła. Diana potwierdziła ze smutkiem, nie idzie jej tak dobrze jak by chciała... Paulina powiedzała Dianie, że wyhoduje z DIANY coś co wygląda jak ubranie - Diana niczego nie założy, ale będzie miała coś co wygląda jak strój, mimo, że jest częścią ciała Diany. Niezbyt ukrwione czy z zakończeniami nerwowymi. Diana potwierdziła - tego nie ma w zakazach. Ewelina o tym nie pomyślała. Paulina poświęciła jakiś strój by to móc sformować na Dianie.

3+1v3->S, ES: 13 - Ta sama intencja, inny czar: Paulina STWORZYŁA na Dianie ubranie. Nie zmieniła biologicznie Diany, sprawiła, że Diana wyhodowała ubranie. Diana nie wie. +1 akcja.

Paulina zwróciła Dianie uwagę, że to (ewolucja Diany w istotę "ubraniową") jest przykład jak można inteligentnie rozwiązać problem rozkazów Eweliny. Czyli jest droga wyjścia. Ale - Diana musi zacząć myśleć. Z tych rozkazów jest opcja wyjścia. Czyli jakaś forma buntu Diany przeciw Ewelinie. Diana jest Bardzo Posłuszna, Myśli Długoterminowo, Wierzy Ewelinie (4). Jednocześnie jest Zakochana w Hektorze, Chce Się Uwolnić i Ufa Paulinie (3). 

3+4v2+4->R, KF: 16 - Zainteresowanie Nowej Strony - pojawi się NOWY Bankierz, który widzi, że Grazoniuszowi nie wychodzi i chce jego kosztem... +2 akcje, Diana kontra Ewelina +2.

I potem Paulina poszła znowu do Hektora. Powiedziała mu, że Diana ma ubrania. Hektor bardzo się ucieszył. Paulina potwierdziła -Diana ma ubrania. I zaczyna powoli kombinować jak obchodzić rozkazy Eweliny, jak jej nie słuchać ślepo. Hektor bardzo, bardzo docenił. -2 tor Dukata. +1 akcja.

Wieczorem Paulina dostała wiadomość od Czykomara. Wiadomość głosiła "nie są biologicznie spokrewnieni".

Innymi słowy, całe życie Diany to kłamstwo. Jej "rodzina" nie jest jej biologiczną rodziną...

Przesunięcie torów:

* Gwarantowane akcje: +1 na Ew3, -4 na Ne7, +2 Ne2
* Akcji: 8. 
    * Ew: 1,1,2,2,3,3,4,4 (1,2,1,0,1,2,2,1)
    * Gr: 1,2,2,3,3,3,4,4 (1,1,0,0,1,1,1,1)
    * Ne: 1,2,2,3,5,5,6,7 (1,1,1,1,0,0,1,1)

**Ewelina:**

* Diana nie jest zainteresowana mężczyznami:    7/8     Tier 3
* Presja społeczna na Grazoniuszu:              1/6
* Grazoniusz wypada z łask Newerji:             8/6     Tier FINAL
* Muszę być autonomiczna, Newerjo!:             1/6

**Grazoniusz**:

* Odzyskanie twarzy w swej grupie:              3/8     Tier 1
* Ustawienie Hektora na właściwym miejscu:      4/6     Tier 2
* Usprawnienie okolicy i pomoc Dianie:          4/8     Tier 2
* Uderzenie w Ewelinę:                          3/6     Tier 1

**Neutralna**:

* Diana zostaje u Eweliny:                      3/6     Tier 1
* Diana buntuje się przeciw Ewelinie:           6/8     Tier 3
* Diana wybiera swoją rodzinę:                  1/6
* Diana odchodzi w świat, samotnie:             2/6     Tier 1
* Diana wybiera Hektora:                        4/8     Tier 2
* Zadomowiony w okolicy Hektor:                 3/8     Tier 1
* Hektor zdesperowany, integracja z Dukatem:    4/8     Tier 2

**Paulina**:

* Udobruchany Grazoniusz:                       2/6     Tier 1

**Interpretacja torów:**

Grazoniusz DOSZCZĘTNIE wypadł z łask zarówno Newerji jak i Eweliny. Jest non-parameter. Hektor co prawda został częściowo "ustawiony na swoim miejscu", ale upadek Grazoniusza sprawił, że to też przestało być problemem. Tak jak działania Grazoniusza przeciw Ewelinie.

Diana coraz bardziej stwierdza, że mężczyźni są nic niewarci. Nie przeszkadza jej to być zakochaną w Hektorze, lecz nie skłania jej do pozytywnego dla ich związku kroku. Z drugiej strony, Diana zaczyna czuć smak wolności od Eweliny - może faktycznie powinno być inaczej? Zaczyna sama kombinować, a to jest coś nowego dla Diany.

Hektor nadal jest zainteresowany współpracą z Dukatem, ale nie widzi tego jako absolutną konieczność.

Grazoniusz dał radę pomóc trochę w czyszczeniu Skażenia w okolicy i drobnych pracach "rezydenckich". Dowiedział się też kilku rzeczy o aktualnej manifestacji Efemerydy Kapsuły.

**Przypomnienie Komplikacji i Skażenia**:

* Ogródek Pauliny jest zniszczony, Skażony szybko nie wróci do normy.
* Wzrost znaczenia Wieży Ciśnień w Senesgradzie - Efemeryda nadal jest sprawna i ma nową manifestację. A ludzie tam coś kombinują.
* Ubranie Diany jest wyhodowane ale jest ubraniem - wyraźnym złamaniem zakazu Eweliny. Diana nie wie.
* Widząc klęskę Grazoniusza pojawi się nowy Bankierz chcący wejść na miejsce upadłego Grazoniusza...

# Progresja

* Grazoniusz Bankierz: wyleciał doszczętnie z łask Newerji Bankierz i z orbity Eweliny Bankierz

# Streszczenie

Grazoniusz chciał odzyskać twarz; podał Dianie eliksir obrzydzenia do Hektora, chcąc zrobić nową komedię. W wyniku tego ogródek Pauliny został Skażony, zniszczony i zdewastowany (emocjonalna magia), Grazoniusz wyleciał z kręgu dookoła Eweliny i Newerja się go pozbyła. Całkowicie się nie opłacało. Czykomar poinformował Paulinę, że rodzina Diany nie jest jej biologiczną rodziną. Diana powoli, powoli zaczyna myśleć, że są alternatywy dla Eweliny a Hektor jest rozdarty między Dukatem i Pauliną.

# Zasługi

* mag: Paulina Tarczyńska, w trybie kryzysowym; straciła ogródek, ale uratowała Hektora i zablokowała podły plan Grazoniusza. Pokazała ząbki jako rezydentka.
* mag: Diana Łuczkiewicz, wypiła eliksir obrzydzenia z rozkazu Grazoniusza i była przyczyną zrujnowanego ogródka Pauliny. Powoli się zaczyna uwalniać od myśli, że Ewelina jest jej panią na zawsze.
* mag: Hektor Reszniaczek, prawie wpadł w ręce Dukata, ale Paulina go powstrzymała; załamany czynami Grazoniusza i własną bezradnością. Emocjonalnie czarując zniszczył Paulinie ogródek.
* mag: Grazoniusz Bankierz, chciał odzyskać twarz w podły sposób, dzięki czemu machinacje Eweliny zniszczyły jego pozycję na jej dworze. Chwilowo stracił wszystko co było dlań cenne.
* czł: Zofia Murczówik, która przejrzała przez iluzję Diany i zobaczyła, że ta chodzi nieubrana. Oczywiście, zaraz pół miasteczka wiedziało.
* czł: Janusz Wosiciel, sąsiad Pauliny i straszny plotkarz; Paulina przekazała mu, że pomyliła pestycyd z nawozem w nocy i dlatego zabiła swój ogródek, odwracając uwagę od Diany.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, miejsce wielkiej dramy między Hektorem, Dianą i Grazoniuszem
                        1. Ogródek, zniszczony doszczętnie przez katastrofę magiczną.
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, gdzie - zdaniem Grazoniusza - coś nowego i dziwnego zaczyna się dziać
# Czas

* Opóźnienie: 1
* Dni: 1

# Frakcje

## Lady Ewelina Bankierz

### Koncept na tej misji:

Królowa Lodu, chcąca odepchnąć od siebie Grazoniusza. Też: chce pokazać Dianie, że faceci to świnie. Manipulatorka dla swoich celów.

### Scena zwycięstwa:

* Grazoniusz został odepchnięty przez Newerję Bankierz i odchodzi w niesławie
* Diana nie jest już zainteresowana żadnym mężczyzną w okolicy
* Sama Ewelina ma czyste ręce i nikt nie może jej niczego udowodnić

### Motywacje:

* "Samodzielna, niezależna, nie potrzebująca nikogo - to ja"
* "Moi kuzyni to głupcy. Niestety, muszę ich przecierpieć... chyba, że..."

### Siły:

* Ewelina Bankierz, manipulatorka w tle
* koledzy i znajomi Grazoniusza, mający na niego wpływ
* Diana, na którą Ewelina ma wpływ

### Wymagane kroki:

* Popchnięcie Grazoniusza za daleko, min. przez presję społeczną
* Zarejestrowanie wszystkiego i pokazanie wyniku Newerji - czarodziejce starej daty
* Pokazanie Dianie, że na mężczyznach nie można polegać i zawsze zawiodą

### Ścieżki:

* **Diana nie jest zainteresowana mężczyznami**: 2/8(2,2,2,2): przesunięcie Diany na pozycję podobną do swojej
* **Presja społeczna na Grazoniuszu**: 0/6(2,2,2): wykorzystanie znajomych Grazoniusza i innych Bankierzy by Grazoniusz robił głupoty
* **Grazoniusz wypada z łask Newerji**: 0/6(2,2,2): starsza czarodziejka nie doceniłaby niewłaściwego zachowania
* **Muszę być autonomiczna, Newerjo!**: 0/6(2,2,2): starsza czarodziejka może rzuci jej jakiś ochłap autonomiczności bez kuzynów


## Grazoniusz Bankierz

### Koncept na tej misji:

Młody arystokrata, który chce dobrze... ale NIE MOŻE stracić twarzy! Nie może! To dlań najważniejsze.

### Scena zwycięstwa:

* Hektor rozumie gdzie jego miejsce i pokornie poddaje się Grazoniuszowej władzy
* Ewelina może zaciskać zęby, ale Grazoniusz decyduje, co i jak będzie się działo - zwłaszcza w sprawach majątkowych
* Grazoniusz jest znowu na topowej pozycji w swoim towarzystwie. Jest ważny, potrzebny, lubiany i popularny.

### Motywacje:

* "Za kogo oni wszyscy mnie mają! Jestem... ważny! To JA mam znaczenie!"
* "Rezydentka rezydentką. Warto jej pomóc, choć z plebsu. W końcu pełni ważną pracę, mimo, że dziwna..."
* "Arystokracja jest arystokracją i chamstwo powinno słuchać."

### Siły:

* Grazoniusz, sam z siebie
* Kilku jego druhów chętnych do pomocy Grazoniuszowi, zwłaszcza materialno-magicznej (niewielkiej)

### Wymagane kroki:

* Upokorzyć Hektora i doprowadzić do tego, by się złamał. Np. używając Diany.
* Spełnić wszystkie durne polecenia Eweliny tak, by dostać pochwały od rezydentki - i pokazać je Newerji.
* Zrobić śmieszną komedię, by odzyskać twarz u znajomych.
* Epicko się wykazać, najlepiej w sposób niemożliwy dla innych.

### Ścieżki:

* **Odzyskanie twarzy w swej grupie**: 0/8(2,2,2,2): Grazoniusz stracił twarz u znajomych. To jest niedopuszczalne i niebezpieczne...
* **Ustawienie Hektora na właściwym miejscu**: 0/6(2,2,2): Hektor go uraził - musi zapłacić. Ale Grazoniusz jest litościwy...
* **Usprawnienie okolicy i pomoc Dianie**: 0/8(2,2,2,2): Jeśli już i tak tu jest, może pomóc czy to rezydencce czy to Dianie.
* **Uderzenie w Ewelinę**: 0/6(2,2,2): może przez oderwanie Diany, może przez pokazanie przydatności Newerji. Ale nie odpadnie z gry.


## Frakcja neutralna

### Ścieżki:

* **Diana zostaje u Eweliny**: 0/6
* **Diana buntuje się przeciw Ewelinie**: 2/8
* **Diana wybiera swoją rodzinę**: 0/6
* **Diana odchodzi w świat, samotnie**: 0/6
* **Diana wybiera Hektora**: 0/6
* **Zadomowiony w okolicy Hektor**: 2/8
* **Hektor zdesperowany, integracja z Dukatem**: 2/8


# Narzędzia MG

## Cel misji

1. Pierwszy prototyp aspektów jako utrudnienia zamiast Okoliczności:
    1. Pomysł nadal jest 0-2
    1. Aspekty dzielą się na cztery kategorie
        1. Natura (wnętrze): inherentne cechy istoty, np. "pancerna" czy "szybka"
        1. Otoczenie (zewnętrze): rzeczy wspierające istotę, np. "jest ciemno" czy "świeci po oczach"
        1. Stan (status): stan istoty, np. "ranna" czy "wzmocniona bloodstone"
        1. Skala: NIE należy do aspektów
    1. Aspekty składają się w sposób 'unbound' - sumują się "w nieskończoność".
    1. Aspekty mają moc 'zwykły' +1, 'duży' +2, 'przytłaczający' +3.
1. Surowiec nie dodaje +1 od siebie. Surowiec powołuje nowy aspekt.

## Po czym poznam sukces

1. Prototyp aspektów
    1. Aspekty umożliwiają manipulowanie aspektami. Dodawanie ich, odejmowanie, redukowanie.
        1. Gracz jest w stanie wygrać konflikt manipulując aspektami
        1. To ma zastąpić tory (nie Ścieżki): wyższa granularność. Zamiast Wpływu.
        1. To ma sprawić, że przeciwnicy się RÓŻNIĄ. I warto używać taktyki.
    1. Kategorie aspektów umożliwiają lepszą kontrolę synergii - CHWILOWO są unbound
    1. Różnicowanie mocy aspektów umożliwia podjęcie decyzji 'to zdejmujemy, to przełamujemy'.

## Wynik z perspektywy celu

1. Prototyp aspektów
    1. K: Pasuje. To było przyjemne.
    1. Ż: Ale nie miałaś powodu tyle kombinować; testy były za proste na tej misji. Z perspektywy podejścia a nie wartości matematycznych.
    1. K: Jeśli masz nazwany aspekt, stosunkowo łatwo mi wykombinować kontrę; zupełnie nie próbowałam rozwiązać emocjonalności ogródka. Padł po odcięciu energii ;-). Więc po kolei rozbierałam aspekty w tamten sposób.
    1. Ż: Fakt. No i Soczewka.
1. Ścieżki i granulacja
    1. Ż: Wygląda na to, że to akurat zaczęło działać poprawnie. Prędkość mi odpowiada; nie jest opresyjna, ale wymaga reakcji.
    1. Ż: Granulacja... wraz z aspektami chyba niestety spadła. Może tymczasowo; nie było zbyt trudnych konfliktów.
    1. K: Ścieżki... pomijając anomalie losowe, których się sporo zrobiło (złe rzuty) - jeśli masz złe rzuty to nawet jeśli zrobisz mało akcji, ścieżki lecą jak szalone. Za szybko? Nie wiem. 
    1. Ż: Spróbujmy więc inaczej: stały wzrost o '1' a nie 0-1-2 na ścieżkach. To usunęłoby część anomalii i dałoby większą przewidywalność.
    1. K: Spróbujmy.
    1. K: Granulacja... wbrew pozorom, była porządna. Mam lekko mieszane uczucia bo nie pamiętam. A do tego poziomu mieliśmy konflikty wyższego poziomu; przywykłam to rzeczy poruszających się szybciej... co było za szybko. Więcej testów.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |
