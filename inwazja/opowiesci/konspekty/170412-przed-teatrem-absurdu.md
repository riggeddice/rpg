---
layout: inwazja-konspekt
title:  "Przed teatrem absurdu"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170407 - Przebudzenie viciniusa (HB, AB, DK)](170407-przebudzenie-viciniusa.html)

### Chronologiczna

* [170407 - Przebudzenie viciniusa (HB, AB, DK)](170407-przebudzenie-viciniusa.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

* Szymon Skubny rzucił kasę na przedstawienie teatralne w teatrze w Czeliminie i wyśle tam przedstawiciela
* Młoda camgirl, Paulina Widoczek, miała szansę zagrać tam niezbyt ważną rolę
* Luiza Wanta też jest w czelimińskim szpitalu; chce móc zrobić reportaż z przedstawicielem Skubnego
* Bianka Rubini chce zniszczyć reputację Skubnego; jak trzeba, spali teatr

A jak chodzi o skrypt misji:

![Rysunek pokazujący kto kogo jak i gdzie](Materials/170412/mission_diagram.png)

## Misja właściwa:

Dzień 1: 

Dionizego odwiedził Bryś. Nieszczęśliwy jak cholera. Podobno Bór smali cholewki do Pauliny Widoczek, załatwił jej pracę i kupił jej dom? Do tego jeszcze Brunowicz chce wpieprzyć, oczywiście, Borowi. No ale Bryś się nie zgadza, więc to jest "dowód, że ten jest konfidentem". Bryś wymyślił, że jakby Dionizy naklupał Brunowiczowi, to jest dowód, że nie jest z policji więc nie jest konfidentem...

Dionizy się załamał. Powiedział Brysiowi, że ON się tym zajmie. Bryś ma spowalniać Brunowicza... ale Bryś czegoś nie powiedział... (3v2->S). Bryś się przyznał. Alina go zobowiązała, by wyprowadził Paulinę na prostą, więc poprosił Bora... 

Dobra. Dionizy poprosił Alinę. To poważny problem.

* Alina, Twój kołcz społeczny, lajfstajlowy Bryś i wielki guru społeczeństwa któremu zleciłaś opiekę nad Pauliną... - Dionizy zaczynając mowę
* Czegoś się naćpał? Idź, idź po jakąś odtrutkę... - Alina, zaskoczona
* Ze mną wszystko w porządku. Pytanie, czy z Tobą... - Dionizy, rozbawiony - Tak się poczuł, że uświadomił sobie niekompetencję co nie jest typowe dla Antka. I zlecił to zadanie... Borowi.
* Słuchaj. Słuchaj. Przyszedł do mnie nasz genialny kolega Artek. Cudowne dziecko stwierdziło, że mam się pobić z jego kumplem, by wyszło, że nie jestem z policji, by go powstrzymać przed pobiciem naszego szefa Bora. TAK, WIEM. - Dionizy, załamany.
* Podobno nasz szef smali cholewy do tej dziewczyny co jej ostatnio pomogliśmy. Co... czy ma coś z nią wspólnego? W każdym razie chłopak tej dziewczyny chce wpuścić wpiernicz naszemu szefowi. Powiedziałem Artkowi, by przypilnował swojego kolegę a my się przyjrzyjmy Borowi. Bo... kupiłby jej dom? - Dionizy
* Mogę Ci w tym pomóc, za drobną przysługę... trzeba pozamiatać. Wkopałaś naszego szefa. - Dionizy
* Co, Hektor coś oberwał? - Alina, złośliwie
* Bór się ewidentnie przejął. DOM JEJ KUPIŁ. - Dionizy - A jego twarz chce kumpel Brysia obić... widzę zalety tego rozwiązania...

Dionizy powiedział Alinie, że trzeba dojść do tego, czemu to robi... bo to już jest pewna przesada i pewien absurd. Oki...

Czas pójść do Patrycji. Patrycji? Tak, chodzi o dowody przeciwko Borowi. Więc, Alina i Dionizy przyszli do Patrycji - która NADAL szuka fantomowych hakerów z XFiles. Jest cała zdeterminowana - ZNAJDZIE ICH. Upokorzyli ją. Dionizy przekierował jej uwagę; czy Patrycja może sprawdzić, czy coś Patrycję łączy z Borem? Patrycja w dużym szoku. Czemu? Dionizy powiedział, że Bór może wpadł w kłopoty. Patrycja się zmartwiła. (3+1v3->F(Bryś wisi PATRYCJI), F(Hektor się dowie), R(Bór TEŻ się dowie)) Oczywiście, Patrycja obiecała, że nie będzie problemu.

Patrycja cała blada. Nie wyszło jej. Odpaliła jakieś zabezpieczenia; ale może Hektor się zgodzi. Zawsze się zgadza. Zadzwoniła do niego.

...dwie godziny później. Hektor się relaksuje i nagle dostaje wiadomość od Patrycji, że... martwi się o Bora i zaczęła szukać śladów. Poprosiła Hektora o autoryzację. Hektor powiedział, że jej nie dostanie. Kazał jej czekać na dalsze instrukcje... 

* Panie prokuratorze? - Bór, zdziwiony
* Bór, co jest, do cholery - Hektor, dzwoniąc do Bora
* Panie... prokuratorze?
* Gdzie pan teraz jest? 
* W teatrze. Dokulturalniam się.
* Po spektaklu proszę do mnie przyjechać!

Patrycja powiedziała, że Bór nie ma domku. Nie kupił jej domku. Bór ma cały szeregowiec domków. Nie jeden. I jej jeden być może wynajął. I może załatwił pracę. ...czyli zrobił... wszystko co jest potrzebne by wyszła na nogi? Cała ta sprawa na marne?

Spoko, Bryś weźmie to na siebie. Patrycja powiedziała, że tą jedną rzecz musi wziąć na siebie... dla nich.

Telefon do Brysia. Ten powiedział, że jest w teatrze z Romkiem. Bo tam jest ta dziewczyna. No ale to wygląda na coś powiązanego ze Skubnym... i Bryś nie rozumie czemu Paulina działa właśnie w taki sposób w tym miejscu... czemu Bór kręci ze... Skubnym?

Dobra. Dionizy i Alina jadą. Tego się normalnie nie da zrobić...

Teatr w Czeliminie. Sztuka się skończyła. Zespół natknął się na Brysia powstrzymującego Romka przed pobiciem się z Kleofasem Borem (który nie chce walczyć). Gdy przyszła jeszcze Paulina Widoczek, sprawa się deeskalowała i zaeskalowała. Bór poprosił Romka by ten zniszczył mu auto i zadzwonił do Hektora, że ktoś od Skubnego rozwalił mu auto. Hektor ryknął, że jedzie. Bór potwierdził i się ucieszył po czym Dionizego, Alinę i innych wygonił. On bierze Hektora na siebie.

* Ale... to dobre auto za pieniądze podatników! - Dionizy, nie rozumiejąc Bora
* No właśnie - uśmiechnięty Bór - A teraz, idźcie na piwo. Nie było Was tu.

Bór powiedział, że nie ma NIC wspólnego z Pauliną. Nie jest nią zainteresowany. Był poproszony, by wyszła na prostą, więc... robi to. Ma tu miejsce takie, by móc to zrobić i on często pomaga różnym ofiarom różnych dziwnych historii po ich akcjach.

* Szczur, ale o dobrym sercu! - Dionizy o Kleofasie, do Aliny

Hektor dotarł do teatru Wiosennego w Czeliminie. Bór czeka. A Hektora idealny wieczór legł w gruzach...

* Doszły mnie słuchy, że bardzo się pan interesuje tą ostatnią ofiarą narkotyków, Bór
* Naprawdę, panie prokuratorze? - zdziwiony Bór
* Tak, Bór - Hektor
* Podobno tworzy się tu niezdrowa relacja. Czy mamy tu problem etyczny, panie Bór? - Hektor, ostro
* Nie, panie prokuratorze. - Bór, z zapałem
* To jak to się stało?
* Nie wiem, panie prokuratorze. Nie wiem jak to się stało
* No, podobno załatwił pan jej dom i pracę
* Tak, panie prokuratorze. 
* Wygląda to jak jakieś niezdrowe zainteresowanie które może podchwycić prasa. Proszę powiedzieć, dlaczego, Bór?
* Resocjalizacja, panie prokuratorze. Daję jej szansę, pracę, oraz mam możliwość badania tych narkotyków! - Bór, z entuzjazmem
* Dobra robota, Bór! - zadowolony Hektor

Borowi się upiekło i rozładował sprawę. Prawie. Pojawił się Kruczolis i zaczął nabijać się z Hektora i zniszczonego auta. Hektor myślał, że ludzie Skubnego go po prostu tauntują, a Kruczolis - nie wiedząc kto to zniszczył - bawi się kosztami Hektora i jego nieudolnością...

TYMCZASEM na piwie.

Paulina spytała o co poszło i co się stało. Rozmowa zeszła na temat Bora. Alina powiedziała, że ma tatuaż na rowku między pośladkami. Dionizy zauważył, że to znaczy, że on lubi chłopców. Udało im się skutecznie przekonać Brunowicza (4v2->S), że nie ma co być zazdrosny o Bora...

* Czyli... czyli Brysiu NIE jest z policji tylko z mafii? - zalany Roman
* Chłopie... mafia nie istnieje. Nie ma żadnej mafii. I wtedy kury zrozumiały. - Alina

Popili, pobawili się dobrze... wyszło też, że Paulina chciałaby żeby Brunowicz zrezygnował z Niedźwiedzi i skupił się na normalnej pracy... ale on nie chce.

Dzień 2:

* O co chodziło z Patrycją? Mieliście iść z nią na piwo. - Bór
* O cholera... przepraszamy panie poruczniku - Dionizy
* Zwłaszcza po tym jak szukała delikatnych danych o swoich przełożonych. - Bór

I Bór wygonił ich ćwiczyć, puszczając sprawę w niepamięć ;-).

# Progresja

Dionizy Kret: Roman Brunowicz go lubi; nie sądzi, by Dionizy był z policji
Alina Bednarz: Roman Brunowicz ją lubi; nie sądzi, by Alina była z policji

# Streszczenie

Alina zrzuciła na Brysia wyprowadzenie Pauliny Widoczek na prostą... więc ten poprosił o pomoc Kleofasa Bora. I Bór jej pomógł - mieszkanie załatwił, pracę... co doprowadziło do spięć między Brysiem i Brunowiczem. Zespół Dionizy/Alina zaczął podejrzewać wpływ magii na Bora więc zrobili pełną konspirę, tylko by odkryć, że... Bór po prostu ma dobre serce. Przy okazji udało im się NIE zauważyć, jak siły Dukata ścinające się z siłami Skubnego doprowadziły do pożaru w Teatrze Wiosennym w Czeliminie...

# Zasługi

* mag: Hektor Blakenbauer, mag ze zniszczonymi planami wieczornymi, którego Bór omotał wyjaśnieniami i który nie dał się zaprosić do teatru Skubnemu
* vic: Alina Bednarz, od jej niewinnej prośby zaczęło się pasmo problemów i nieszczęść; wymyśliła tatuaż z motylkiem odpędzając ponure myśli Romana o Borze
* vic: Dionizy Kret, wszystko, co mogło mu nie wyjść mu nie wyszło i Bór oraz Hektor się wszystkiego dowiedzieli... ale wywiedział się od Brysia o co chodzi
* vic: Paulina Widoczek, której Bór pomógł i załatwił jej pracę tancerki w teatrze. Chce wyciągnąć Brunowicza z drogi przestępczej, ale nie ma dużych szans.
* czł: Artur Bryś, który wpakował wszystkich w problem Bora i powstrzymywał kumpla przed bitwą z Borem; szczęśliwie, nie ma już opinii konfidenta u Brunowicza
* czł: Kleofas Bór, "oportunistyczny szczur, ale z dobrym sercem", który wszystkich rozegrał by pomóc dziewczynie wyjść na prostą i mieć szansę w życiu. Ma bogatego kuzyna.
* czł: Patrycja Krowiowska, która oderwała się od szukania "hakerów XFiles" by dowiedzieć się o Borze... potem wciągnęła Hektora by się wybielić i serio skomplikowała sprawę.
* czł: Roman Brunowicz, prostolinijny szef Ognistych Niedźwiedzi, który chciał by jego dziewczyna miała jak najlepiej... ale nie chce odchodzić z Niedźwiedzi. Pogodził się z Dionizym, Aliną i Brysiem.
* czł: Krzysztof Kruczolis, ręka Skubnego, który chciał jak najlepiej zrobić sztukę a przez działanie ludzi Dukata reputacja Skubnego ucierpiała
* czł: Luiza Wanta, praktykantka dziennikarstwa, która - jak chcieli ludzie Dukata - napisała artykuł ośmieszający niekompetencję Kruczolisa (że teatr się zapalił)

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Czelimin
                    1. Teatr Wiosenny, który został uszkodzony w pożarze w wyniku działań ludzi Dukata chcących obniżyć pozycję Skubnego
                    1. Piwiarnia Kufel, miejsce z różnymi ciekawymi alkoholami, gdzie zespół się troszkę pojednał z Brunowiczem
                1. Miłejka, wieś niedaleko Czelimina
                    1. Szeregowiec Powrotu, gdzie Bór znajduje mieszkania dla ofiar magii i osób, którym warto pomóc; należy do kuzyna Bora

# Czas

* Dni: 2

# Narzędzia MG

## Cel misji

* Pokazanie, jak w rozgrywce między Skubnym a Dukatem ucierpią normalni ludzie. Kruczolis dąży do "Skubny szef kultury", Bianka do "Skubny fails"
* Pokazanie, jak pojedynczy artefakt potrafi zmienić sytuację w drastyczny sposób - a siły Skubnego mają... er, Alinę/Dionizego?
* Hektor współpracuje z ludźmi Skubnego, czy pozwala ludziom przegrać ze wsparciem magicznym?
* Kleofas Bór - co to za jeden i czemu dowodzi Siłami Hektora
* Czy Bianka da radę skrzywdzić niewinnego przedstawiciela Skubnego? I czy Zespół na to pozwoli?
* Czy Bianka przeżyje?
* Czy Luiza obciąży Kruczolisa? I czy Zespół go osłoni?
* Pokazanie Kleofasa budującego ofiarom dom...
* Czy Bryś wyjdzie z "Ty konfidencie tępy"?

## Po czym poznam sukces

* Postacie MUSZĄ coś zrobić - teatr spłonie lub będzie uratowany
* Postacie viciniuskie radzą sobie z parowaniem magicznego wsparcia ze strony ludzi Dukata (a zwłaszcza Bianki)
* Poziom zniszczenia zależy od zaangażowania graczy ;-). Mają 2-3 konflikty na ratowanie teatru
* Zrozumiemy lepiej Kleofasa Bora. Kim jest i dlaczego robi to co robi - i tak jak robi
* Kruczolis przeżyje, lub nie. Ale będzie silnym punktem na misji.
* Bianka ucieka LUB umiera
* Luiza pojawi się na misji jako źródło wiedzy lub strzelba chekova
* Bryś będzie albo spalony u Ognistych Niedźwiedzi, albo pojednany z Romanem Brunowiczem

## Stan misji?

* FAIL: ominęliśmy misję
* FAIL: ominęliśmy misję
* FAIL: ominęliśmy misję
* SUKCES: szczur o dobrym sercu i dobrotliwy szefu
* FAIL: Kruczolis nie był istotny
* SUKCES: Bianka jest niezauważona
* FAIL: Luiza się nie pojawiła
* SUKCES: Bryś i Brunowicz się lubią ale dalej jest konfidentem ;-)
