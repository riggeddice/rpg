---
layout: inwazja-konspekt
title:  "Ofiara z wampira dla Arazille"
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141227 - Przyczajona Andromeda, ukryty Maus (An)](141227-przyczajona-andromeda-ukryty-maus.html)

### Chronologiczna

* [141227 - Przyczajona Andromeda, ukryty Maus (An)](141227-przyczajona-andromeda-ukryty-maus.html)

## Punkt zerowy:

## Misja właściwa:

Andromeda ma poważny problem. Z jednej strony chciałaby się pozbyć Amuletu i Ołtarza Podniesionej Dłoni, zwłaszcza wiedząc jaka siła stoi za amuletem (demoniczny władca Iliusitius), ale z drugiej strony nie chce wpaść w ręce Arazille, tej samej klasy istoty władającej marzeniami. No i nie chce by "jej" magom coś się stało. Aha, chce jeszcze pozbyć się Arazille z tego obszaru i to w taki sposób by nikt nie ucierpiał za bardzo. No i, oczywiście, usuwając Amulet Podniesionej Dłoni. W skrócie: bardzo, bardzo trudne zadanie.

Andromeda ma więc pewnego Mausa, który stał się wampirem - Anetę Hanson. Aneta utraciła moc i straciła link z Karradraelem, ale okazało się, że najpewniej Arazille próbując połączyć się z Anetą wykorzystała dokładnie te same kanały i odnowiła połączenie z Karradraelem. Ale najpewniej Karradraela tu nie ma. Człowiek nie jest magiem a nawet mag nie jest w stanie sam nawiązać kontaktu z Karradraelem; to Karradrael nawiązuje kontakt z Mausami. Jednak Anecie Hanson udało się połączyć z Karradraelem, a raczej dostać dostęp do jego "bazy danych". Nawet poznała imię demonicznej władczyni - Arazille. Niestety, skaziła się i stała się wampirem (to już na własne życzenie) pod wpływem min. aury emitowanej przez Arazille która i na nią wpłynęła zwiększając jej religijność do poziomu wręcz fanatycznego.
Czyli ona się raczej nie będzie znała na okultyzmie. Po prostu ślepo powtarzała to, co wyczytała w umyśle Karradraela. Nawet nie pamięta co zrobiła... Szkoda.
Zdaniem Andromedy biedna, biedna Aneta Hanson jest już martwa.
Terminus zabunkrował Anetę w jamie pod drzewem w lesie. Jako wysłannik Watykanu poprosił Anetę, by nie wychodziła z tej kryjówki.

Andromeda miała nadzieję na uzyskanie wsparcia od policjantów. Niestety, po zobaczeniu akcji z wampirem najprawdopodobniej Anna Makont na której Kasi zależało najbardziej się rozsypała. A jej brat który też jest słabym ogniwem tego wszystkiego zaczął chronić siostrę i policjanci zajęli się sobą zamiast pomagać Kasi. Dwaj pozostali policjanci zajmują się szukaniem narkotyków na które skierowała ich Anna... sęk w tym, że jak Andromeda wie, najpewniej nie ma tu żadnych narkotyków. Bo po co. Więc zamiast szukać czegoś co się może przydać policja zajmuje się gonieniem wiatru w polu.
Andromeda poprosiła Herberta, by ten przekazał bibliotekarzowi (Andrzejowi Szopowi) obraz fotki na której "Lidia" odchodzi z dwoma mężczyznami od Namiotu Świateł. Jest zainteresowana, czy którykolwiek z tej trójki jest znanym magiem (czyli wiadomo że jest magiem). Szop, lekko spanikowany powiedział, że robi co może i że znajdzie informacje - poprosił tylko Andromedę o trochę czasu. Ta, lekko zdziwiona jego reakcją, dała mu ten czas. On obiecał rekordy i informacje na temat Arazille.

Wieczór. Andromeda stwierdziła, że to jest TEN moment, gdy należy odwiedzić Arazille. Kręciła się po Festiwalu aż napotkała Feliksa Hansona i poprosiła go o audiencję u bogini marzeń. Feliks skupił się chwilę i powiedział, że Arazille czeka na Andromedę w najbliższym namiocie.
Czyli Arazille jest mobilna. Dobrze wiedzieć...
Arazille wyglądała jak oszałamiająco piękna kobieta. Andromeda znowu miała wrażenie, że zna tą twarz, ale nie wie skąd - najpewniej ze snów. Arazille zapytała w jakiej sprawie przychodzi do niej Andromeda. Kasia zaczęła od pytania jak Arazille odcięła Kasię od Iliusitiusa - w sensie, nie mogła komunikować się z nikim rozmawiając z Arazille. Bogini się uśmiechnęła i powiedziała, że to nie ona odcięła Kasię. Kasia jest w pełni skupiona na Arazille gdy z nią rozmawia; a nie było co odcinać, bo Andromeda nigdy nie była podpięta do Iliusitiusa...
Arazille wysnuła teorię, że podpięcie Kasi do Ołtarza Podniesionej Dłoni odbyło się nieprawidłowo. Kasia z wahaniem potwierdziła, że taka opcja istnieje (podpięła się jednocześnie z Ingą, która była magiem). To sprawiło, że zabezpieczenia przed magami Ołtarza nie zabiło Ingi, ale Ołtarz nie podpiął się do Kasi, bo wyczuł Ingę. I od tej pory Kasia chodzi na "nie dociśniętym kabelku". To powoduje, że Iliusitius nie ma mocy skorumpować Kasi szybko; ale będzie ją miał, to tylko kwestia czasu.
Andromeda się tego spodziewała, ale nie zrobiło jej się wcale weselej.
Arazille powiedziała Andromedzie, że jest w stanie zniszczyć Ołtarz i ją uwolnić. Kasia poprosiła, by nikt nie zginął na co Arazille, że ona musi zebrać dużo więcej energii; Arazille może nie wygląda, ale jest na bardzo niskim poziomie energetycznym. Kasia westchnęła - woli nie widzieć Arazille na pełnej mocy. Bogini Marzeń się cały czas ładuje. Powoli, ale akumuluje energię. Do tego służy ten Festiwal.
Zapytana czy może zmienić lokację np. na hospicjum czy na szpital Arazille powiedziała, że może - ale to będzie dużo wolniejsze a na razie się spieszy. Czemu? Nie odpowiedziała.
Arazille powiedziała Andromedzie, że ta powinna dostarczyć jej morderczynię. Po pierwsze, nie ma tu Karradraela - Arazille się tego domyśliła. Po drugie, taki wampir to smaczny kąsek energetyczny. Po trzecie, morderstwa i śmierci mocno osłabiają moc Arazille. Bolesna rzeczywistość jest najgorszym wrogiem pięknych marzeń. Andromeda spytała, czy Aneta Hanson może jeszcze być człowiekiem, lecz demoniczna władczyni odpowiedziała, że raczej nie. Ale skończy jako szczęśliwy, smaczny kąsek. Andromedy to NIE pocieszyło.

Rozstały się w uśmiechach i ukłonach.

Andromeda natychmiast poprosiła swojego znajomego studenta, eksperta od googla i okultyzmu, Feliksa Bozura o wyszukanie wszystkiego co się da na temat "Arazille" i "Iliusitiusa". Feliks się ucieszył nowym zleceniem; akurat trochę brakowało mu pieniędzy. A Kasia płaci nieźle.
Tymczasem Sandra podsłuchała fragment rozmowy Andromedy z Arazille swoimi czujnikami (19/k20). Przerażona, zdecydowała się działać.

W nocy Kasię obudziło dziwne uczucie. Gdy otworzyła oczy, zorientowała się, że jest związana kablami i zakneblowana. Przerażenie. I zdziwienie, bo nad sobą zobaczyła... Sandrę. Sandra zapowiedziała jej, żeby nie mówiła głośno, by nie obudzić Samiry. Kasia cicho potwierdziła. Sandra ją ostrożnie odkneblowała i Kasia ku swemu zdumieniu zobaczyła, że Sandra się jej boi. Sandra zarzuciła ją pytaniami. Iliusitius. Z kim rozmawiała w namiocie. Jaki wampir. Magia Krwi - o co tu chodzi. I co zrobiła Augustowi. I kim Kasia tak naprawdę jest.
Terminus powiedział Kasi że wchodzi, ale Andromeda go natychmiast powstrzymała. Nie chciała tu walki magów nad swoim ciałem w piżamie. Kasia zaczęła odpowiadać uspokajająco, lecz do pokoju weszła Samira. Przeprosiła Sandrę z uprzejmym uśmiechem, po czym fala magiczna z lustra trzymanego przez Samirę uderzyła Sandrę i czarodziejka osunęła się na podłogę. Chwilę potem Samira sama osunęła się na podłogę.
Hańba! Kasia nie umie się wydostać z kabli. Przyszedł terminus i ją rozplątał, po czym sobie poszedł, mamrocząc niezadowolony o Iliusitiusie - wreszcie dowiedział się, jakie cholerstwo się tu pałęta i jakie trzyma ekipę magów.
Kasia wyprowadziła oszołomioną Samirę do łóżka mówiąc jej, że lunatykowała i Samira zasnęła jak dziecko. Po czym Sandra zaczęła się budzić. Jest w złym stanie - krew z nosa, zimno jej, zawroty głowy... a Herbert potwierdził, że tu był rzucony czar i to brudny i jakby niewprawny.
Kasia zaopiekowała się Sandrą. Pogłaskała ją do snu i upewniła się, że śpi. Przy wejściu zostawiła zwykłe lusterko (bez powodu). Kasia zamyka drzwi w swoim pokoju i idzie spać.

Jak już Kasia prawie zasnęła, obudził ją Herbert. Powiedział, że ma złą wiadomość - Aneta Hanson nie jest głodna, nic nie piła i nie może spać. Andromeda spochmurniała - to implikuje, że w jakimś stopniu nie jest już człowiekiem. Herbert potwierdził jej obawy - najpewniej Aneta Hanson faktycznie stała się wampirem. W tym wypadku prędzej czy później będzie musiała wyjść z kryjówki i kogoś zabić...
Bardzo mało pocieszające.
Andromeda westchnęła. Najpewniej będzie musiała zabić wampira. Oddać Anetę Hanson Arazille. Jeśli to wzmocni Boginię Marzeń, być może pomoże to w rozbrojeniu Ołtarza. Andromeda porozmawiała też z terminusem na temat pośpiechu Arazille. Jedyne do czego doszli to to, że albo Arazille się spieszy bo jej filakterium jest nietrwałe albo oczekuje, że ktoś się pojawi w okolicy (np. lokalny terminus lub Triumwirat).
Andromeda z Herbertem zawarli tymczasowe porozumienie i tymczasową nieagresję. Nie zakładają złych intencji po stronie tej drugiej osoby. Herbert powiedział, że warto oddać Anetę Arazille, jeśli dzięki temu oni zostaną uwolnieni. Podkreślił, że Aneta Hanson nie jest już człowiekiem i nie ma dla niej odwrotu. Andromedzie przypomniała się mandragora i córka mandragory...
Plus, Herbert zrobił Andromedzie mini-wykład o magii krwi i o Illuminati.
Tak pokrzepiona, mogła wreszcie pójść spać.

Ranek. Jak Sandra się obudziła i spojrzała w lustro, wszystko sobie przypomniała. Jak Andromeda. I Sandra nie wie co z tym zrobić...
Andromeda się obudziła tyci wcześniej. Pierwszą osobą jaką chciała odwiedzić była właśnie śpiąca Sandra. Andromeda przygotowała śniadanie, ale Samira tym razem nie wyszła od rana na festiwal, zdecydowała, że zostanie z Andromedą i zje z nimi. Sandra zeszła na dół, ale wpierw rzuciła zaklęcie na telewizor.
W telewizorze leciał program dokumentalny "krwawe wierzenia Polinezyjczyków". WTF. A tak naprawdę to była Sandra, która pokazywała jak Magia Krwi, która krótkoterminowo może działać, długoterminowo nie tylko się nie opłaca, ale i korumpuje. Andromeda zaczęła coś podejrzewać i spojrzała na Sandrę. Ta zwróciła spojrzenie, choć ze strachem. Kasia dostrzegła, że Sandra ma jedną rękę pod stołem. 
Samira dostrzegła, że sytuacja jest dosyć napięta. Zapytała co się dzieje, żadna z dziewczyn nie odpowiedziała. Dopytała więc co i jak, nie otrzymując odpowiedzi stwierdziła że przyniesie deser. I wyszła. A Sandra i Kasia zaczęły rozmawiać. Sandra zarzuciła Kasi Augusta (to ją najbardziej bolało) i Magię Krwi (to było drugie). Rozpoznała po wzorze amuletu Iliusitiusa. Kasia potwierdziła. Sandra powiedziała, że musi Kasię zgłosić, na co Kasia zauważyła, że Sandra ją zabije.
Ogólnie, paskudna sytuacja. Sandra nie ufa Kasi i się jej boi a Kasia tyle razy była już na muszce maga (tak, Sandra ma pistolet XD) że przywykła i jej to tak nie rusza. 
Na to weszła Samira. Zrobiła awanturę, przede wszystkim Sandrze za broń i za celowanie w Kasię.
Wbrew protestom, wzięła Sandrę ze sobą do swojego pokoju porozmawiać.
I okazuje się, że Sandra nie zapomina. Jak sama Sandra zauważyła, Kasia jej "coś zrobiła". Sandra jest w panice.

Samira zeszła z góry. Powiedziała Kasi, że ta nie musi jej kłamać odnośnie tego co się stało, bo wyraźnie prawdy nie może powiedzieć. Widząc minę Kasi, Samira się uśmiechnęła - w branży rozrywkowej sekretów jest więcej niż w mafii, jej zdaniem. Powiedziała, że Sandra się wyprowadzi; nie może chwilowo mieszkać z nimi. Ale będzie w kontakcie i dalej będzie współpracować. Na gorzkie stwierdzenie Andromedy, że najpewniej już nie z nią, Samira powiedziała że nic nie wiadomo, ludziom się zmieniają punkty widzenia.

I na to ku radości (?) Andromedy dotarły informacje od Andrzeja Szopa. Po kolei:
- "Lidia" została zidentyfikowana jako Luksja Diakon. Czarodziejka. 16 lat, kocha światło.
- Z dwóch ludzi odprowadzających Luksję Artur znalazł jednego. To lokalny ochroniarz Festiwalu; nazywa się Robert Przerot.
- Gdzie Arazille, tam fabokle. Próżniacze byty, które powstają z ludzi; śpiewy i zawodzenie fabokli osłabia odporność i wzmacnia wpływ Arazille. Fabokle są odwracalne. Dotyka to ludzi młodych, praktycznie nie spotyka się fabokli poza zakresem [13, 28] lat. Jeśli Kasia chce walczyć z Arazille, musi zapewnić sobie nieobecność fabokli. Inaczej game over.
- Nie ma możliwości, by Arazille działała na tej fazie egzystencji przy tak niskim poziomie energetycznym. Ktoś (człowiek lub mag) musi być jej filakterium. Jeśli Arazille nie osiągnie poziomu samoakumulacyjnego, jej zniszczenie zabija tamtą osobę. Jeśli wejdzie w tryb samoakumulacyjny, mag / człowiek, choć ciężko Skażony, przeżyje. No, ma szanse. 
- Arazille nie byłaby w stanie zmusić kogoś by ją przyjął. Ktoś musiał zostać ochotnikiem. Co przy jej mocach nie jest bardzo trudne.
- Samo istnienie Arazille skaża jej filakterium.
- Jest więcej Ołtarzy Podniesionej Dłoni, wszystkie dedykowane Iliusitiusowi.
- Nie udało się znaleźć Andrzejowi ani jednego przypadku gdzie Iliusitius nie był w stanie połączyć się z kontrolerem ołtarza. Teoria o "złym połączeniu" jest błędna.
- Najbardziej popularny, skuteczny i tani sposób walki z Arazille polegał na tym, że brało się grupę ludzi i zabijało w okrutny sposób jednego po drugim. Nieważne, wyznawcy lub nie. Jej pole magiczne i jej aspekt są wyjątkowo niekompatybilne z zabijaniem i działaniami tego typu.
- Najbardziej popularny sposób walki z Iliusitiusem polegał na tym, że poświęcało się zdominowanych magów i eksterminowało ludzi kontrolujących amulety i innych powiązanych. Do momentu pojawienia się Mausów i Karradraela - wtedy po prostu na front szli Mausowie nad którymi Iliusitius nie ma władzy. Karradrael umożliwiał Mausowi bycie przejętym przez Ołtarz, po czym korzystał z jego kanału by zniszczyć Ołtarz. Niestety, pechowy Maus czasem (zwykle) ginął. Ale było to najbezpieczniejsze dla wszystkich innych. Szok z uderzenia Karradraela w Iliusitiusa był tak wielki, że kilka Ołtarzy traciło od razu moc.
- Karradrael nigdy nie robi tego samego błędu więcej niż jeden raz, chyba, że Patriarcha Maus sobie tego wyraźnie zażyczy.

Andromeda jest zadowolona z pracy Andrzeja Szopa. Bardzo mu podziękowała, czego ten zupełnie się nie spodziewał.

Herbert skontaktował się z Andromedą. Zapytał jej, skąd Sandra wiedziała, że Kasia ma coś wspólnego z Arazille i magią krwi. Andromeda powiedziała, że Sandra ma siatkę podsłuchów na całym terenie. Na to Herbert spytał, jaka będzie opinia Sandry na temat nakarmienia Arazille wampirem; na to Andromeda zauważyła, że bardzo negatywna. Herbert zapronował, całkiem zresztą rozsądnie, usnięcie pamięci Sandry i po problemie. Kasia wie, że to niemożliwe, więc bez podania powodów zaprotestowała.
Herbert miał dwa pomysły: albo pozbędą się Sandry z obszaru (by nie widziała co się tu stało) albo pokażą Sandrze w jakim stopniu wampir nie jest już człowiekiem; tak, by to po prostu była najlepsza opcja. Bez możliwości uratowania. Na pytanie jak Herbert proponuje to pokazać terminus powiedział, że wystarczy chwilę wampira przegłodzić i wypuścić go wolno wśród ludzi. I poczekać. No, oczywiście wkroczyć w takim momencie, by nikomu nic się nie stało.
Kasia nie doceniła.

Za to Kasia ma szczwany plan. Poprosić Samirę, by ta poprosiła Sandrę, by ta dla odmiany dowiedziała się, dlaczego mimo ponawianych prób zdobycia przez policję wsparcia owego wsparcia nie ma. Kasia miała do czynienia z policją wcześniej - jeśli zgłaszali potrzebę wsparcia, zwykle je dostawali. A ten Festiwal i tak nie powinien mieć tylko czterech policjantów. Czyli "jak i dlaczego". Jak Arazille to zrobiła. 
Kasia upewniła się też na zdjęciach Luizy Wanty, że Nikola Dorsz przybyła na Festiwal z Lidią. I że przybyły tydzień temu. To oznacza, że żadna z nich nie może być filakterium dla Arazille. Przynajmniej tyle dobrze.

Godzinę później Samira wróciła do Kasi. Z radością poinformowała ją, że Sandra się ucieszyła tym zadaniem i spróbuje się tego dowiedzieć. Tak więc - ku wielkiemu zadowoleniu Kasi - Sandra będzie z głowy przez okres pożerania wampira. A być może na dłużej. Kasia zdaje sobie sprawę z tego, że Sandra potrzebuje trochę czasu z tym wszystkim - z punktu widzenia Sandry Kasia dotknęła ją Magią Krwi i przekształciła jej wzór. A Sandra najbardziej na świecie boi się właśnie Magii Krwi. Co też powoduje, że Kasia trochę nie jest pewna Sandry (czy nie zgłosi), ale Kasia upewniła się, że Sandra zdaje sobie sprawę z tego, że jeśli to zgłosi, to: Kasia zginie, August ma poważne problemy (albo zginie) i bardzo marny los spotka innych niewolników Ołtarza.
Tak więc Kasia się nie boi. Sandra nie potrafiłaby skazać na śmierć tylu osób.
Andromeda mistrzowska szantażystka :D.

Kasia zdecydowała, że nie dość dokładnie sprawdza co robią policjanci. Włączyła swoje podsłuchowe radyjko. Usłyszała tam działania dwóch grup - Wojciecha i Anny oraz Rafała i Macieja.
Anna i Wojciech rozmawiają o tajemniczej Amelii. Nie umieją się dobrze skupić. Zastanawiają się, czy w ogóle z nią rozmawiali i gdzie w ogóle jest. Kasia facepalmowała. Wszystko wskazuje na to, że Arazille się ich pozbyła - jak tylko policjanci docierają do Amelii, zapominają o niej. Albo myślą, że dotarli do Amelii. I szukają, wiecznie jej szukają. Kasia wie już, że na nich nie ma co liczyć.
Tymczasem rozmowa Rafała i Macieja była bardziej interesująca. Rafał twierdzi, że znalazł twarde narkotyki. Kilkanaście ludzi na ziemi, nieprzytomnych. Mówi, że jeśli to za mało na wsparcie to on już nie wie co. Maciej wysłał kolejną prośbę o wsparcie. Rafał i Maciej zabezpieczają teren i Rafał wezwał karetkę.

Jako, że Andrzej Szop sobie nieźle poradził ostatnio, Kasia miała dla niego nowe zadanie. Nieważne jak (ale niech mu się nie stanie żadna krzywda) - ale niech Andrzej znajdzie informacje w rekordach Srebrnej Świecy odnośnie tego, kto powinien być terminusem w tej okolicy. Szop zasugerował też, że spróbuje znaleźć kilka nazwisk magów SŚ w okolicy; zanim uderzyła Inwazja. Jeśli uda się mu kogoś znaleźć, Andromeda będzie w stanie przynajmniej z kimś się skontaktować kto będzie miał dla niej jakiekolwiek informacje. A dokładniej: nie ona. Terminus INCOGNITO (wtedy mniej incognito, ale Andromeda przewiduje pojawienie się Romana i Augusta niedługo; trzech wspierających się magów to już niemało).

Andromeda poprosiła też Andrzeja o to, by ów przygotował "Dead Man's Hand". Jeśli cokolwiek się stanie Andromedzie (w domyśle: tym magom też), żeby informacja o Arazille i lokalizacji Arazille trafiła do Srebrnej Świecy. Żeby jakiś terminus, jakiś mag był w stanie to złapać i przekazać dalej. Nawet jeśli magowie nie zdecydują się zareagować, niech wiedzą co się dzieje.

Andromeda stwierdziła, że nie jest w stanie kontrolować wszystkiego. Poszła porozmawiać z Arazille. Ale tym razem zdecydowała się zrobić eksperyment - wybrała sobie taki pustawy namiot i będąc w tym namiocie powiedziała imię bogini marzeń. Ku swemu zadowoleniu, Arazille nie zareagowała. Albo zdecydowała się zignorować Kasię, albo Arazille nie ma nasłuchu na powiedzenie jej imienia. Sprawdziła to w kilku jeszcze namiotach i znowu Arazille nie zareagowała. Czyli nawet osoby pod wpływem Arazille najpewniej nie są z nią połączone. Cenna wiadomość.
Więc Andromeda wyłapała Feliksa Hansona i poprosiła go o ponowne spotkanie z Panią Marzeń. Hanson skierował ją do dość odległego namiotu, tak z 7 minut spaceru. Sandry powinno już w okolicy nie być, co jedynie bardzo Kasię cieszy w perspektywie tego, o czym będą rozmawiać.

Sama rozmowa Arazille z Andromedą odbyła się w bardzo kulturalnej i przyjemnej (jak zawsze) atmosferze. Andromeda powiedziała Arazille, że znalazła i może jej wydać Anetę Hanson. Ale chciałaby wpierw się upewnić, że jest wampirem i że nie da jej się już uratować. Arazille skwapliwie powiedziała, że już za późno, ale to nie było wystarczające dla Kasi. Arazille zgodziła się na układ - jeśli Aneta Hanson jeszcze jest człowiekiem, to jest Kasi. Kasia może ją ubierać, karmić, co chce. Jeśli nie jest - Arazille ją pożre i nażywi się jej energią. Tu Arazille zaproponowała Kasi, że dodatkowo spróbuje pociągnąć z Iliusitiusa w taki sposób, by nie mógł skrzywdzić żadnego z magów Ołtarza. Ale każde osłabienie Iliusitiusa to wzmocnienie szansy na uwolnienie Andromedy i magów z Ołtarza.
Kasia zapytała też Arazille o lustra. Bogini Marzeń powiedziała, że jest dość blisko luster - w końcu ludzie przeglądają się w lustrach z nadzieją że coś zobaczą. Ale nie bardzo blisko luster, bo kryje się tam też depresja. Arazille powiedziała, że jak zakończą temat Ołtarza to z przyjemnością pomoże Kasi w odkryciu swojej natury - ale nie teraz, bo potrzebuje całej mocy jaką może zgromadzić. Spieszy się. Nie powiedziała czemu i na co.
Powiedziała też Kasi, że są na jednym wózku. Ich aktualne losy są splątane. Gdy Arazille dotknęła amuletu Iliusitiusa na szyi Kasi (poprzednie spotkanie, oparzyło Kasię) to wyczuła to, co chciała zobaczyć - Kasia w ogóle nie ma połączenia z Iliusitiusem. Zero. Ale Władca Dominacji nadal będzie w stanie subtelnie wpływać i korumpować Andromedę przez Amulet... jeśli Arazille jej nie uratuje. Arazille powiedziała też, że pierwsza dawka i przysługa gratis. 
Zapytana przez Andromedę, Arazille dodała jeszcze, że Nikola Dorsz opuściła Festiwal. Wczoraj. Po rozmowie z Samirą. Przedtem tylko rozmawiała z jednym z ochroniarzy, Robertem Przerotem - i już nie wróciła.

Andromeda zapytała też Arazille o lokalnego terminusa. No musi jakiś być. Arazille stwierdziła, że nie jest to problem Kasi. Przyciśnięta, spytała "przeszkadza Ci?". Kasia zauważyła, że z jej historią każdy terminus jej przeszkadza a Arazille zaznaczyła, że ONA jest jakby wyraźniejszym i ciekawszym celem niż Kasia. Chwilę porozmawiały, ale nie dało się niczego z Arazille na ten temat wyciągnąć. Co jedynie obudziło w Kasi pewne podejrzenia odnośnie terminusów i filakteriów pewnych bogiń.
Andrzej Szop pracuje już nad terminusem i potencjalnymi kontaktami.

Arazille powiedziała Andromedzie na odchodnym, że Kasia jest jej naturalnym wyznawcą a Arazille jest naturalną boginią Andromedy. Ta oczywiście zrobiła dziwną minę - wtf. Arazille się uśmiechnęła i powiedziała, że jeśli Andromeda sprawdziłaby dokładniej historię Arazille, sama by do tego samego wniosku doszła. Kasia stwierdziła, że próbowała. Arazille powiedziała, że nie dziwi jej, że magowie próbują usunąć informacje na jej temat. Ale Arazille stwierdziła też, że jej słowa będą odebrane jako "kuszenie", więc ma zamiar na tym skończyć. Jak Kasia jest ciekawa, niech sama szuka.

A o co chodziło Arazille gdy mówiła o pierwszej dawce - Bogini Marzeń wpłynęła na Sandrę zanim ta wyjechała, by ją częściowo zneutralizować (tak, zmaterializowała się przy niej osobiście i ją pocałowała).
- Bardzo wzmocniła poczucie osamotnienia i pustki u Sandry.
- Wzmocniła uczucie Sandry do Augusta.
- Wprowadziła Sandrę w pewien chaos emocjonalny, wzmocniła połączenie z Andromedą.
- Wzmocniła w Sandrze nieufność i poczucie bezradności w kontakcie z innymi magami.
W ten sposób Arazille upewniła się, że Sandra nie będzie chciała zdradzić Andromedy i nikomu nie zgłosi, że coś złego dzieje się na Festiwalu. Bo w jej aktualnym stanie psychicznym utrata Augusta, Andromedy, Samiry i doprowadzenie do śmierci innych magów to byłby po prostu koszmar.

Zgodnie z szacunkami Herberta wampir strasznie zgłodnieje następnego dnia. To powoduje, że Andromeda chce ściągnąć Augusta i Romana na... wieczór następnego dnia (jak skończy się sprawa z wampirem; nie chce słuchać jakiejś głupiej opowiastki jak to ona jest zła karmiąc Arazille z ust Augusta, który nie miał trudnych wyborów w życiu). Co powoduje, że ma całe popołudnie i wieczór przed sobą. I wypadałoby jakoś to spędzić. Ma dwie opcje: trochę pomalować (bardzo kuszące) lub spróbować namierzyć Luksję Diakon i "Nikolę Dorsz" którą w tej chwili podejrzewa o bycie magiem, potencjalnie potężnego rodu Diakon. Co nie jest fajne. I niestety to może się okazać dość krytyczne.
A obecność "narkotyków" wykrytych przez policjantów implikuje, że FAKTYCZNIE Diakonowie mogą być jakoś w ów Festiwal zamieszani. Nie byłby to pierwszy raz gdy impreza się komuś wymknie spod kontroli... 

Właśnie, policjanci. Andromeda włączyła znowu podsłuch. Udało jej się usłyszeć coś bardzo interesującego (choć na to musiała chwilę poczekać) - rozmowę czterech policjantów. Rafał i Maciej byli wyraźnie pijani a Wojciech i Anna próbowali od nich wyciągnąć gdzie są narkotyki. Nie są w stanie powiedzieć nic poza tym, że to było w Namiocie z Lwami. Wojciech przypominał "wezwaliście karetkę". Rafał powiedział, że karetka odjechała, bo podobno są pijani. Ale on jest trzrzrzeźwy! Anna spytała go co widział, co się stało. Rafał powiedział, że tańczyli...
Wojciech spytał Annę czy to Skubny. Ona "tak, racja..." ironicznie. Policjanci stwierdzili, że muszą tylko znaleźć Amelię (tu słuchająca Andromeda facepalmowała) i jak ją wycofają to muszą poszukać tych narkotyków. Anna przyznała bratu, że zmyśliła narkotyki. Wojciech powiedział, że narkotyki rozumie, a w wampira nie uwierzy.

Tymczasem wampir głodnieje. Mimo, że policjant w nią nie wierzy.

Zgodnie z informacjami od Arazille, Luksja Diakon i Nikola Dorsz nie są obecne na Festiwalu. Mogło się zmienić, ale nic na to nie wskazuje. Andromeda niechętnie zostawia nieznanego maga na plecach, w związku z tym postanowiła do niej zadzwonić. Netheria nie odebrała telefonu. Jednak po pięciu minutach oddzwoniła. W rozmowie Andromeda "przypadkiem" spytała czy udało się znaleźć Luksję (Netheria łagodnie poprawiła "Lidię") i jej lusterko. Netheria powiedziała, że znalazła Lidię i nie ma żadnego problemu. Jednak lusterka "pamiątki po babci" nie udało się znaleźć, wyraźnie ktoś je ukradł i najpewniej wywiózł. Kasia chciała umówić się na spotkanie. Netheria chciała odmówić, lecz ktoś szepnął jej zza pleców (co każe Andromedzie podejrzewać, że to było na telefonie głośnomówiącym) i Netheria zmieniła zdanie. Spotkanie za 3 godziny, w knajpce w Żonkiborze, poza terenem Festiwalu.

Kasia nie jest głupia. Poprosiła, żeby Herbert upewnił się, że tam nie ma jakiegoś maga który zastawia właśnie pułapkę. Bo w końcu i to jest możliwe. Herbert jako ekspert od pułapek powinien być w stanie znaleźć pułapkę w trakcie zastawiania nadal pozostając nie wykrytym. Herbert się zgodził.
Terminus Andromedy obserwował teren cały czas i stwierdził, że nie została zastawiona żadna pułapka. Jednak pasywnie wykrył obecność maga, który pojawił się pół godziny przed ich spotkaniem i zamelinował się gdzieś w miasteczku. Poczuł maga, bo wykrył pojedynczy delikatny skan aktywny. Zdaniem Herberta, kompetentny terminus - gdyby Herbert miał jakiś czar, artefakt lub rzucałby jakiś poważny czar wcześniej, mógłby zostać wykryty. Oczywiście, Herbert zaraportował to Kasi. Tamten mag także przeszedł w tryb ukryty. Herbert zgodnie z procedurami powinien zrobić taki skan; ale stary wytrawny terminus wie, że procedury są po to, by łapać młodszych kolegów po fachu.

Spotkanie w "Przyjaźni". Siedzi sobie Andromeda, sączy bezalkoholowe i otwierają się drzwi. Wchodzi Netheria. Uśmiecha się do Andromedy i podaje jej rękę. Andromeda ściska rękę. Wiązka energii negatywnej przepływa przez Kasię i artystka malowniczo traci przytomność. Nic więcej nie wie.

Trzy godziny później Andromeda otwiera oczy. Pamięta, że przysnęła, czekając na Netherię. Diakonka wystawiła ją do wiatru. Jednak chwilę później Andromeda spojrzała w lustro i jej pamięć wróciła. Niestety, Andromeda nie ma pojęcia co się działo jak była nieprzytomna - ktoś zrobił to fachowo.

Szczęśliwie, w krzakach siedział Herbert. I od owego totalnie bezużytecznego militarnie terminusa dowiedziała się wszystkiego. Herbert nie reagował, bo Netheria użyła dawki obezwładniającej, więc chciała Kasię żywcem. Jako, że tajemniczy terminus nie wykonał ruchu, Herbert zdecydował się poczekać. Netheria wyszła z Kasią z "Przyjaźni" uprzednio modyfikując pamięć obecnym w kafejce i wtedy terminus zaatakował. Bez walki pokonał zaskoczoną Netherię; zupełnie, jakby ona się tego nie spodziewała. Zmusił ją, by zmieniła rekordy w pamięci ludzi - nigdy jej tam nie było, wystawiła Kasię do wiatru. Po czym Netherię uprowadził.
Problem w tym, że Herbert rozpoznał owego terminusa. To Gabriel Newa, bohater wojenny walk przeciwko Inwazji. Pośmiertnie odznaczony. Wszyscy myśleli, że Gabriel zginął. Herbert nie uważa, by Gabriel dołączył do Inwazji, jednak Gabriel bardzo męczył się z uzależnieniem od Magii Krwi. Innymi słowy, nie mówionym głośno sekretem było to, że bohater wojenny i bardzo potężny terminus militarny był defilerem. Miał być leczony po Inwazji, ale ta procedura jest bardzo trudna i rzadko dobrze się kończy.
Szybka hipoteza Herberta i Kasi - Gabriel jest cynglem Arazille a ona pomaga mu w ukojeniu pragnienia Krwi. W końcu Karradrael robi to dla Mausów. Ale jeśli tak, to Arazille ma dużo większą moc bojową niż ktokolwiek kiedykolwiek podejrzewał...
Hipoteza numer dwa: Gabriel był tam po to, by uratować Kasię przed Netherią z woli Arazille. Czyli Arazille ratuje Kasię!
Herbert obiecał zdobyć dla Kasi dossier Gabriela. Cokolwiek co pozwoli jej zrozumieć porywanie Diakonek.
Herbert powiedział też Kasi, że słyszał jak Gabriel powiedział do Nikoli Dorsz "Netherio". Czyli udało im się dojść do tego kim była owa Nikola Dorsz...

Żółw & Kić: "Wyobraźmy sobie taką sytuację. Perfekcyjnie zakonspirowana Netheria. Nagle dzwoni telefon. Kazali jej oddzwonić. I nagle - pytanie 'czy znalazłaś Luksję?'. I całą historyjkę Netherii, jakakolwiek by nie była, diabli wzięli. Netheria Diakon, nekromantka-mentalistka która po prostu ma cholernego pecha. I Andromeda master troll."

Kić: "Przykro mi, Netheria będzie musiała przeżyć jedną noc z defilerskim terminusem..."

Późny wieczór. Herbertowi udało się skompilować z danych Srebrnej Świecy informacje na temat Gabriela Newy. Terminus borykał się z Krwawym Uzależnieniem, ale dawał radę i "posilał się poza domem". Miał rodzinę, ale Zaćmienie sprawiło, że jedna córka i jego żona utraciły moc magiczną. Gabriel kombinował i pisał odwołania, udawało mu się odroczyć, ale już nie bardzo długo. Po "śmierci" Gabriela doszło do morderstwa - cała rodzina Gabriela zginęła. Podejrzewano, że agenci Inwazji się zemścili. W świetle tych faktów i tego, że Gabriel żyje, Herbert wysunął nową hipotezę - Gabriel porwał swoją rodzinę i opuścił z nimi Świecę, by się schować gdzieś tu, w okolicach Żonkiboru.

Jeszcze przed pójściem spać Andromedę odwiedziła Samira. Podzieliła się z Kasią swoimi obserwacjami.
Festiwal rośnie błyskawicznie. Nawet za szybko - farba nie nadąża wyschnąć. Jednocześnie wszystko jest robione bardzo tanio, praktycznie poniżej kosztów; cały Żonkibór i ekonomia Żonkiboru dotuje Festiwal. Nie załamuje się tylko dlatego, że ludzie nie biorą wynagrodzenia ponad absolutne minimum do przeżycia. Samira zaraportowała niezdrowy pośpiech i dodała, że jest reklama - ale nie jest to reklama niekierowana. Są wysyłane osoby z Festiwalu których zadaniem jest znaleźć innych i zachęcić ich do przybycia. Młodzi ludzie są wysyłani do modnych klubów i promują to, co lokalni chcą usłyszeć - łatwy seks, tanie atrakcje, nagrody, możliwość wygrania wielkich kwot, brak dużej ochrony... zależy kto jest grupą docelową. Celem jest ściągnąć jak najwięcej osób na Festiwal wszelkimi metodami tak, by nikt "ogólny" się tym nie zainteresował. A wszystko to wygląda jak działania oddolne, Samira jednak czuje, że jest tu coś więcej, to jest zbyt skoordynowane, jest to działanie genialnego masterminda.
Samira powiedziała, że Festiwal przygotowuje się na wojnę. Nie wie z kim, nie wie dlaczego, ale to dokładnie takie wrażenie, podzielane przez wiele osób na samym Festiwalu które "znają się na rzeczy".

Następnego dnia okazało się, że faktycznie, Aneta Hanson (wampir) nie panuje nad sobą. Cierpi. Dwa, trzy razy próbowała rzucić się na Herberta (ale terminus jest tego świadomy, więc zatrzymał ataki). Aneta płacze, nie wie co się z nią dzieje, ale nie potrafi się powstrzymać, głód jest silniejszy od niej. Herbert ją uśpił i unieczynnił.
Herbert był zmuszony się ujawnić magicznie, ale to było wliczone w koszta tej operacji. Wampir, po uśpieniu, został złożony w ofierze Arazille.

Ofiara wyglądała w taki sposób: Andromeda, trzyma za rękę Anetę i trzyma za rękę Arazille. Arazille trzyma drugą ręką Anetę. Taki trójkąt. I Arazille zaczęła ekstrakcję, kazała Kasi skupić się na osłabieniu Iliusitiusa, na pękającym Ołtarzu. Zdaniem Arazille osłabienie Iliusitiusa teraz przyniesie korzyść w przyszłości, gdy dojdzie do samego łamania Ołtarza. Arazille pociągnęła moc z Iliusitiusa, który czuł obecność zarówno Arazille jak i Karradraela. Chwilę potem zmieniła cel, pociągnęła energię z Karradraela, który poczuł Arazille i Iliusitiusa i który dopiero wtedy zorientował się, że jest tu jakiś Maus. Karradrael szybko przeanalizował co się stało i jak czerpiąc z Anety (uznał, że utrata energii do Arazille jest warta zobaczenia jak on przeoczył połączenie z Mausem), po czym zdecydował się krwawo zdetonować Anetę Hanson. Jednak Arazille zatrzymała detonację i pozwoliła Anecie umrzeć w mniej efektowny sposób. Po czym, jako, że ból Kasi (amulet parzy!) był zbyt wielki przerwała też drenaż Iliusitiusa.

Andromedę bardzo mocno to uderzyło. Zarówno fizycznie (jest mocno Skażona ilościami energii które przez nią przechodziły) jak i psychicznie (uczestniczyła w czymś co w jej odczuciu było torturowaniem Anety Hanson co zakończyło się jej śmiercią). A miała odejść maksymalnie bezboleśnie. Niestety, zmieniła się sytuacja (telefon Kasi do Netherii) i Arazille potrzebuje znacznie więcej energii niż planowała, znacznie szybciej. Ale tego naturalnie Arazille Kasi nie może powiedzieć.

Przez lustra tą scenę poczuła Samira. Ale także i Sandra, płacząc z całego serca nad Anetą, Andromedą, Augustem i sobą... i nienawidząc się za to, że nie zgłosiła tego nikomu i że nie zgłosi, bo za bardzo kocha i nie jest to tego warte po tym co spotkało ją po zgłoszeniu matki...


"Słabszy Iliusitius - mi łatwiej. Silniejsza Arazille - mi łatwiej. Moje cele - twoje cele. Im większa różnica Arazille - Iliusitius, tym większa szansa że ołtarz bum bum i nie ma bu bu." - Arazille do Andromedy próbując wyjaśnić co chce osiągnąć.
"Ja nie chciałam być ratowana!!!" - Andromeda do Herberta o działaniach Gabriela.
"Wojna nigdy się nie kończy, ale zmieniają się narzędzia wojny" - Samira, cytując kogoś kogo usłyszała na Festiwalu.

# Lokalizacje:

1. Żonkibór
    1. melina terminusa gdzie został ukryty wampir. Pod drzewem. W lesie.
    1. domek Ewy Czuk, w którym nadal znajduje się Zespół.
    1. Festiwal Marzeń
        1. Jakiś losowy namiot, w którym pojawiła się Arazille
        1. Namiot z Lwami (gdzie jest mnóstwo małych kotków i ani jednego lwa)
    1. knajpka "Przyjaźń"

# Zasługi

* czł: Kasia Nowak, która kombinuje jak mieć ciastko i zjeść ciastko (Iliusitius vs Arazille) i szantażuje/trolluje sojuszników jak pro.
* vic: Aneta Hanson, fanatycznie religijny wampir magii krwi. Oddana idei zniszczenia fałszywej bogini.
* mag: Herbert Zioło, terminus który jako jedyny niewolnik amuletu nie tylko się postawił Andromedzie ale nawet doszedł z nią do porozumienia.
* mag: Andrzej Szop, próbuje się zupełnie nie wychylać i być tak przydatny dla Andromedy jak tylko mag być może.
* czł: Feliks Hanson, aktualnie pełniący obowiązki szefa kultu Arazille. Nadal nie zna imienia bogini.
* vic: Arazille, demoniczna władczyni Marzeń. Dużo mówi, dużo się śmieje, działa przez agentów i spełnia życzenia niewiernych. Aha, ma mały poziom energii.
* czł: Feliks Bozur, student wyszukujący dla Andromedy niebezpieczne hasła o demon lordach w Googlu (i innych źródłach).
* mag: Sandra Stryjek, bondage expert-to-be, która też (jak się okazuje) nie zapomina. Ale musi się nauczyć zamykać drzwi na klucz.
* czł: Samira Diakon, która rozbraja mniej stabilnych psychicznie sojuszników i wszyscy są za to jej wdzięczni.
* czł: Robert Przerot, ochroniarz Festiwalu Marzeń odprowadzający gdzieś Luksję Diakon.
* vic: Karradrael, legendy i elementy historii jego powstania.
* vic: Iliusitius, legendy i opowieści na temat jego i jego słabych punktów.
* czł: Wojciech Kajak, policjant szukający Amelii w nieskończonej pętli marzeń.
* czł: Anna Kajak, jeszcze: Makont, policjantka zapętlona z bratem w "czy już rozmawialiśmy z Amelią".
* czł: Rafał Szczęślik, policjant. Dowodzi grupą. Znalazł twarde narkotyki... a przynajmniej tak myśli.
* czł: Maciej Dworek, policjant o którym nie można absolutnie nic powiedzieć (poza tym że się słucha Rafała Szczęślika).
* mag: Netheria Diakon, której jeden telefon Andromedy zniszczył subtelny plan i wpakował ją w OGROMNE kłopoty.
* mag: Gabriel Newa, lokalny terminus który okazuje się być weteranem wojny z Inwazją chorym na krwawe uzależnienie. Aha, podobno nie żyje.