---
layout: inwazja-konspekt
title:  "Sojusz przeciwko Szlachcie"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150722 - Reverse kidnaping z Krupnioka (DK, HB, Kx)](150722-reverse-kidnapping-krupniok.html)

### Chronologiczna

* [150718 - Splątane tropy: Spustoszenie? (AW)](150718-splatane-tropy-spustoszenie.html)
* [150722 - Reverse kidnaping z Krupnioka (DK, HB, Kx)](150722-reverse-kidnapping-krupniok.html)

### Punkt zerowy:

Ż: Jaką korzyść z tej misji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Co specyficznego było z tymi KADEMowymi żabolodami (o czym absolutnie nikt, łącznie z Warmasterem, nie wiedział)?
K: Eksperymentalne żabolody, które potrafią wytrzymać wyższą temperaturę i chłodzą mocniej.
Ż: Na kim najmocniej odbiją się obostrzenia Marty (lockdown KADEMu)?
K: Na Ignacie Zajcewie.
Ż: Co może wyciągnąć SŚ na KADEM by jeszcze bardziej eskalować spiralę nienawiści?
D: Dowody, że KADEM "twórczo czerpie" z ich technologii i pomysłów.
Ż: Najważniejszy eliksir stworzony przez Marcelina w ostatnim czasie?
K: Marcelin był kluczowym elementem eliksiru pozwalającego na zatrzymanie magicznej plagi przez Edwina.
Ż: Dlaczego terminuska zdecydowała się na odwet właśnie przeciwko tej osobie (Dianie)?
D: Bez skrupułów wykorzystała sytuację.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Powiedz mi coś o dziewczynie, przez którą Marcel wpadł w kłopoty.
D: Ma lepkie ręce. Cicha, zwinna, niepozorna. Absolutnie niebojowa.


Problemy do rozpatrzenia dalej:
- kto stoi za atakiem na KADEM? Dlaczego?
- jak ruszyć Tamarę, by dostać dostęp do magitechowych pancerzy

Inne:
- Full lockdown KADEMu, Siluria została wysłana by pomóc Hektorowi w temacie Marcelina.
- Marian Agrest montuje ofensywę przeciwko Szlachcie.

## Misja właściwa:

### Faza 0: Po tej samej stronie

Działania Andrei nie dały rady zneutralizować Hektora Blakenbauera. Okazało się jednak, że Andrea pokazała Agrestowi potencjalny słaby punkt Hektora - Hektor próbuje ratować ludzi. Innymi słowy, potencjalnie da się przesunąć Hektora do współpracy z Kurtyną przeciw Szlachcie.

Leokadia Myszeczka, alchemik i miłośniczka luster i antyków. Jednocześnie eks-czarodziejka rodu Maus, terminuska kontrolująca się (bierze leki), której "kotwicą" jest Tamara Muszkiet. Jednocześnie osoba niesamowicie wręcz pragmatyczna i bezwzględna, mająca jednocześnie myślenie dziecka w wielu kwestiach.
Na żądanie Agresta Saith Kameleon odwiedziła Leokadię w nocy pod postacią Tamary i powiedziała, że Kurtyna i Hektor są potencjalnie po tej samej stronie. Kameleon zaproponowała plan Leokadii i naciskała ją tak mocno, aż ją zdestabilizowała.

Leokadia ma jedną misję. Doprowadzić do połączenia sił Blakenbauerów, Emilii Kołatki i Kurtyny przeciwko Szlachcie. W zamian za co, "Tamara" pomoże jej dowiedzieć się, kto zaatakował KADEM z ramienia Srebrnej Świecy (bo, zdaniem Agresta, jest to temat dla Wydziału Wewnętrznego).

### Faza 1: Marcelin i Leokadia

W czasie gdy Hektor zajęty był pomaganiem Edwinowi i zespołowi sił specjalnych przeciwko Tymkowi Mausowi i Gali Zajcew, Siluria dowiedziała się z wiarygodnych źródeł (przyjaciel wyżej postawiony w Świecy), że faktycznie Tamara jest chora na Lodowy Pocałunek i znajduje się na terenie Kopalina. Ale jej dokładna lokalizacja jest nieznana. Wskazuje to na siły wyżej postawione w Srebrnej Świecy.

Ozydiusz Bankierz, zapytany przez Hektora o ostatnią komunikację Tamary powiedział, że to był Salazar Bankierz. Tyle, że jest wyczyszczony.
Ozydiusz też podał dodatkowe informacje; Hektor dostał zbiór magów z którymi także komunikowała się Tamara w ostatnim czasie.

Hektor poszedł do Margaret. Poprosić ją o pomoc ze zbudowaniem detektora osoby, która była na poligonie KADEMu.
Margaret się zgodziła; potrzebna jej próbka energii z poligonu KADEMu. Jeśli nie ma do tego dostępu, spróbuje użyć energii ratunkowych amuletów terminusów Świecy; nie jest to idealne, ale zawsze jakaś odpowiedź (choć żaden dowód). Powiedziała też Hektorowi, że jest problem - zniknął jej Marcelin. Ona zbudowała dla Marcelina merkuriasza (rtęciowca) bo prosił; w końcu alchemik. I Marcelin wyprowadził merkuriasza w butelce poza Rezydencję; Klemens zlokalizował Marcelina gdzieś w okolicy Centralnego Kompleksu Srebrnej Świecy.

...super.

Hektor poszedł do Silurii i powiedział, że ma opcję detektora ale potrzebny mu skan energetyczny z Poligonu KADEMu. Siluria po wahaniu zgodziła się, by "Nikola" posłużyła jako źródło (była na Poligonie gdy do tego doszło). Hektor zostawił Margaret, Silurię i głowicę bojową w Rzecznej Chacie a sam poszedł do kompleksu SŚ szukać Marcelina...

W samą porę. Marcelin rozmawia z małą, ładną Zajcewką. Okazuje się, że to Vladlena Zajcew. I Vladlena zaprosiła (kwiatami) Marcelina na bal do Emilii Kołatki. Hektor wszedł bez cienia elegancji i porwał Marcelina łypiąc na Vladlenę z ukosa. Marcelin wyjaśnił mu szybko poziom nietaktu jaki zrobił i powiedział, że zaproszono go na bal. Hektor się zdziwił; to podobno plan przyjaciółki Marcelina, niejakiej Leokadii Myszeczki. Hektor przypomniał sobie, że min. z Leokadią kontaktowała się Tamara. Leokadia jest członkiem Kurtyny. Brzmi to coraz gorzej...

"Zbiórka charytatywna na magów na których poluje Hektor... i idzie na nią Marcelin."

Za namową Marcelina, Hektor przeprosił Vladlenę i wprosił się (z osobą towarzyszącą) na bal do Emilii u Vladleny. Vladlena się zgodziła przez wzgląd na Marcelina i Leokadię.
Ale nie wie, że przyjdzie Siluria - mag KADEMu.

Marcelin powiedział jeszcze Hektorowi, że merkuriasz którego otrzymał od Margaret dał Leokadii. Przedtem go przekształcił za jej radami - stał się bardziej przezroczysty, bardziej szklany, bardziej zmiennokształtny i bardziej niebezpieczny. "Lustro, które nie jest lustrem".

Marcelin wrócił z Hektorem do Rezydencji i udzielił Hektorowi podstawy kodów etykiety. Zapytany przez Hektora, co trzeba dorzucić na bal charytatywny, Marcelin powiedział "załatwione - pomogliśmy tym dzieciom a Leokadia poświadczyła". Marcelin dostał opieprz za niedopilnowanie, żeby wszystko było po cichu.

Tymczasem Siluria wrobiła Margaret. GS Aegis dostał detektor aury KADEMu i go zasymilował.

Gdy Hektor i Siluria się spotkali, Siluria powiedziała Hektorowi, że mają detektor i Hektor dostał wydmuszkę. Nikola powiedziała, że jako mag KADEMu jest silnie zsynchronizowana z detektorem i ona powinna tego użyć. Hektor też zaprosił Silurię na bal charytatywny wieczorem. Gdy Siluria usłyszała, że to bal u Emilii, opadła jej szczęka. Nie da się tam dostać. Jest to bal dla wybranych przez Emilię osób gdzie Emilia ustala co i gdzie się dzieje oraz zawsze coś ważnego ogłasza - niekoniecznie z perspektywy Świecy, ale coś istotnego. Siluria pół się spodziewała, że Emilia wygłosi krucjatę przeciw Hektorowi, ale to oznacza, że nie o to chodzi (bo został dopuszczony).

Hektor dostał wiadomość od Dionizego; Leokadia została pobita i okradziona.
Dionizy powiedział, że 3 magów weszło i załatwiło Leokadię i wyniosło grupę przedmiotów. Dionizy nie wchodził; nie będzie wbijał do domu terminusa (zwłaszcza alchemika).

Tak się składa, że Siluria interesowała się wcześniej Leokadią (14v12) - Leokadia z domu jest z rodu Maus. Zmieniła ród za czasów Renaty. Leokadia jest najwierniejszą agentką Tamary; Tamara nie uznaje jej jako przyjaciółki, ale Leokadia zawsze patrzy na Tamarę jak na swoją idolkę. Leokadia jest absolutnie bezwzględna; łączy cechy Mausów i Myszeczków. Jest niezwykle lojalna. Katalistka (Maus), alchemik (Myszeczka) i fascynuje się lustrami, co nie wpływa dobrze na jej kondycję psychiczną i magiczną. Do tego jest terminusem i chroni słabszych; jest członkiem Kurtyny, bo Tamara dała jej szansę. Leokadia bardzo stara się nie zawieść i jej nie zmarnować.
- Leokadia raczej nie atakowałaby Hektora przez Marcelina. Raczej zrobiłaby to bezpośrednio.
- Leokadia jest jedną z nielicznych silnie widocznych postaci Kurtyny.

### Faza 2: Po tej samej stronie

Hektor i Siluria (z Aegis) pojechali do domu Leokadii; Aegis weszła pierwsza. Pułapek nie ma; jest martwy pies (pies Leokadii), zniszczone wszystko i część rzeczy ukradzionych. Leokadia jest ciężko ranna, ale się nie wykrwawi.
Siluria próbowała pomóc Leokadii; Hektor i Aegis znaleźli secret stash Leokadii. Tam są eliksiry leczące. Hektor jednak nie został przekonany przez Silurię; próbowała, ale nie dała rady. Hektor nie pozwolił Silurii podać tych leków; wezwał Edwina zamiast tego. Edwin będzie za 10 minut.

Tymczasem 3 minuty później weszło 2 magów. W panice, jeden wyciągnął różdżkę na Silurię a Aegis zasłoniła Silurię i prawie uderzyła - Siluria zatrzymała Aegis. Drugi mag zobaczył leżącą Leokadię i chciał wyminąć Hektora i do niej się rzucić. Doszło do szarpaniny, mag wyciągnął broń a Hektor go poraził paralizatorem. 
Drugi mag zaczął uciekać; na żądanie Silurii "Nikola" ściągnęła ją z powrotem.
Hektor chciał go przesłuchać, jednak Siluria mu przeszkodziła; nie podoba jej się brutalność tego wszystkiego. 

Szczęśliwie, szybko pojawił się Edwin. Spojrzał na Hektora, westchnął i podał Leokadii dokładnie ten sam eliksir, który Hektor i "Nikola" znaleźli ukryte w skrytce Leokadii. Gdy terminuska Kurtyny zaczęła wymiotować i wracać do siebie, Edwin zostawił opiekę nad nią Silurii a sam skupił się na porażonym przez Hektora magu Kurtyny.

Leokadia zaczęła dochodzić do siebie. 

Gdy minął pewien czas, Leokadia się uśmiechnęła radośnie. Podpytana przez Silurię powiedziała, że "zna twarz wroga" - osoba odpowiedzialna za atak na nią (Leokadię) to Diana Weiner, wysoko postawiony członek Szlachty. Leokadia nie wiedziała, że Diana jest członkiem Szlachty, ale przekształcony przy jej pomocy przez Marcelina merkuriasz w lustro podsłuchał rozmowy i bardzo ciężko poranił Dianę, przy odrobinie szczęścia ze skutkami śmiertelnymi. A aura lustra pochodzi od Marcelina; z Leokadią nie ma nic wspólnego. Więc Kurtyna będzie się mścić najpewniej na Marcelinie, nie Leokadii... więc Hektor jest na tym samym wózku co Kurtyna. Dodatkowo, Leokadia ma dowody, że siły Blakenbauerów atakowały Tymka Mausa i Galę Zajcew, nawet, jeśli dyskretnie (to ona pomogła Marcelinowi).

Czego chce Leokadia? Leokadia chce, by Blakenbauerowie współpracowali z Kurtyną przeciwko Szlachcie. Chce, by Tamara była bezpieczna. I jest skłonna poświęcić sporo by to zapewnić. Powiedziała zaskoczonej Silurii, że Iza Łaniewska zostanie przekształcona przez Millennium w Diakonkę co "ją zabije", bo "to nie będzie już ta sama Iza".

Leokadia powiedziała, że to nie był jej plan; "oni" przyszli do niej w nocy, opiekują się Tamarą i mają od niej upoważnienie. A Leokadia zrobi bardzo wiele, by chronić swoich przyjaciół - Marcelina (przed Hektorem), Tamarę (przed Hektorem) itp. A Emilia, Tamara, Marcelin (tu lekkie politowanie) nie mają jaj by zrobić to co NALEŻY by powstrzymać Szlachtę.

Więc zrobi to Leokadia.

Terminuska Kurtyny też powiedziała, że na balu mogą porozmawiać z Emilią; tam się wszystko powinno rozwiązać i wyjaśnić. Emilia jest taktykiem, Leokadia jest tylko "Ostrzem Tamary" tak jak Adela Maus jest "Ostrzem Karradraela". Siluria, znając przeszłość Leokadii dostała gęsiej skórki. Cholerna fanatyczka...

# Streszczenie

Leokadia Myszeczka, "ostrze Tamary", wrobiła Marcelina w dostarczenie jej merkuriasza (lustrzaka) i dała się napaść i poranić. Dzięki temu, że napastnicy zabrali jej magiczne przedmioty, merkuriasz zaatakował Dianę Weiner i ją ciężko poranił. Teraz Szlachta BĘDZIE winić Blakenbauerów i Blakenbauerowie WEJDĄ w sojusz z Kurtyną przeciw Szlachcie. Cholerna fanatyczka. Aha, plan pochodzi od Saith Kameleon, czyli od Agresta. Oczywiście.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Pion badawczo-produkcyjny
                                1. Laboratoria alchemiczne
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Dzielnica Trzech Myszy
                        1. Domek Leokadii Myszeczki
        
# Zasługi

* mag: Siluria Diakon, próbuje skierować Hektora do zrobienia tego co należy; wyciągnęła z Leokadii to, co chciała się dowiedzieć i wzmocniła GS Aegis 0003.
* mag: Hektor Blakenbauer, wchodzi twardo jak diament zarówno w sojuszników jak i wrogów; awatar Sprawiedliwości i woli prokuratury.
* mag: Leokadia Myszeczka, terminuska Kurtyny, której wolą było połączenie sił Hektora i Emilii przeciwko Szlachcie; niestabilna psychicznie; jej metody są tak okrutne jak jej nazwisko. 
* mag: Emilia Kołatka, która organizuje bardzo ważny bal; z rozkazu Agresta zgodziła się na plan Leokadii Myszeczki i na zaproszenie Marcelina, Hektora i "kogoś z KADEMu".
* mag: Vladlena Zajcew, urocza sekretarka Emilii zapraszająca Marcelina na bal; wobec Hektora formalna i chłodna.
* mag: Marcelin Blakenbauer, który zaufał Leokadii (swojej przyjaciółce a nie kochance), załatwił jej merkuriasza, wprosił się na bal Emilii... ogólnie, kłopoty.
* mag: Margaret Blakenbauer, która zrobiła detektor osoby będącej na poligonie KADEMu... który zarufusiła jej Siluria i dała Aegis do wchłonięcia.
* mag: Edwin Blakenbauer, który przyszedł wyleczyć ciężko skrzywdzoną przez Szlachtę Leokadię i skrzywdzonego przez Hektora * maga Kurtyny.
* vic: GS "Aegis" 0003, głowica wykazująca niepokojącą autonomię i "uczucia" chroniąc Silurię; poszerzona przez Silurię o detektor aury KADEMu.
* mag: Saith Kameleon, która na życzenie Agresta zdestabilizowała psychicznie Leokadię Myszeczkę by uderzyć w Szlachtę.
* mag: Marian Agrest, który jeśli nie może wyłączyć z akcji prokuratora, spróbuje go skaptować do swojej sprawy kijem i marchewką.
* mag: Ozydiusz Bankierz, chce się pozbyć Tamary i problemów z Tamarą i dlatego współpracuje z KADEMem przeciw Tamarze.
* mag: Diana Weiner, wysoko postawiona członkini Szlachty w Srebrnej Świecy ciężko poraniona przez merkuriasza Marcelina przez pułapkę Leokadii Myszeczki.