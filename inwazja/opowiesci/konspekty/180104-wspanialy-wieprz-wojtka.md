---
layout: inwazja-konspekt
title:  "Wspaniały Wieprz Wojtka"
campaign: wizja-dukata
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171229 - Esuriit w Półdarze](171229-esuriit-w-poldarze.html)

### Chronologiczna

* [171229 - Esuriit w Półdarze](171229-esuriit-w-poldarze.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

- Kajetan Weiner wyczyścił lokalizację intruzji Esuriit na Primusie.
- oczkodzik wraca; coś znalazł na tamtej polanie

### Wątki konsumowane:

brak

## Punkt zerowy:

## Potencjalne pytania historyczne:

* Czy Stefania zdobędzie czarnotrufle Esuriit? (TAK, długość: 3)
* Czy oczkodzik zdobędzie czarnotrufle Esuriit? (TAK, długość: 3)
* Świniołowcy mają dowody na to, że tu są "dziwne świnie" i grzebią w temacie (TAK, długość: 3)
* Harcerz wpadnie w kłopoty w Walecznisku (TAK, długość: 3)

## Misja właściwa:

**Dzień 1**:

Misja dla młodych prosta - Kajetan wyczyścił, oni muszą znaleźć oczkodzika i upewnić się, że nie ma problemów. Patrol, weryfikacja, wyjście.
Świeca dostarczyła pułapkę na oczkodzika Anatolowi i Kindze. [Coś w tym stylu.](https://www.youtube.com/watch?v=1lrPLUaZ61w) Muszą zmontować na miejscu.
Zespół zaczął od harcerzy. Zatrzymał ich druh. Powiedzieli mu że szukają local folklore and legends. Druh Bonifacy.
Bonifacy chce pomóc, ale jest zaplątany w podanie - chce by burmistrz zamknął nielegalną farmę norek. A skąd o tym wie? Jest jakiś efekt mentalny.
Bonifacy pokazał barak w lesie na mapie. Nie wie skąd, tam są ślady krwi.
Zaklęcie mentalne skanujące: jest w lesie oczkodzik, oczkodzik retransmitował wizję norek. Bonifacy nic nie "widział". Ale jest w okolicy inny mag (kobieta) polujący na oczkodzika i jest jakiś człowiek polujący na oczkodzika.
W parowie okazało się, że oczkodzik tu był. Wyżerał rośliny Skażone Esuriit. Jego naturalne miejsce żerowania. Może... zamknąć parów? Wtedy jak oczkodzik tu wlezie to nie wylezie.
Co teraz - wracać do harcerzy czy zastawić pułapkę na świnię?
Poszli więc do tego "baraku", czyli Waleczniska. Walecznisko ma kłódkę. Kinga sprawdziła artefaktem czy tam nie ma czegoś magicznego.
Tak. Retransmitery i lekki alert zabezpieczeń. Retransmitery kierują w dół. Zwierzęta są opętane wściekłością podczas walki.
Weszli zaklęciem przejścia przez ścianę (Anatola). Walecznisko jest siermiężne; kiepska magia krwi. Niskopoziomowa.
Siermiężny fight club przy użyciu norek. 10-20 osób na widowni.
Zespół zdecydował, że trzeba wysłać raport do centrali i skupić się na baraku a nie na oczkodziku.
Anatol przygotował się do zaklęcia skanującego krew norki. Soczewka - Bonifacy zapomniał WSZYSTKO związanego z norkami i przestał przyswajać.
Anatol dowiedział się za to, że te walki są cotygodniowe. Następna za 2 dni. Są regularne. A norki JUŻ są elementem efemerydy. Te walki są od roku ponad.
Zespół wrócił do obozu harcerskiego porozmawiać z Wojtkiem - lokalnym ekspertem od świń.
Wojtek ucieszył się z zainteresowania, ale szybko zamilkł - nie chce się wygadać. Wie coś. A dotknął oczkodzika i wygląda na to, że lubi tą świnię.
Wojtek opowiedział jak to karmi świnię czasem. Oczkodzik go lubi. Zbiera dla świni języczniki z drewnianych domków - tamto je.
Ogólnie, Wojtek i oczkodzik się polubili i całkiem nieźle się bawią - oczkodzik go szuka od czasu do czasu. A Wojtek ma fajne sny.
Zespół udał się do wąwozu z pułapką (unikając obozu harcerskiego) i zaczął zastawiać pułapkę. Kinga wykazała się wybitnym talentem w pułapkowaniu świń.
Noc. Zaczyna się. Przyszedł oczkodzik i został zapułapkowany. Stefania celowała z broni w niego by go unieruchomić; wbił się Anatol teleportem i zatrzymał ją, bo to akcja Świecy.
Stefania uśmiechnęła się, powiedziała że Myszeczce uciekł i nie ma nagrody. Tough luck. Odeszła w spokoju, przedstawiając się.
Przybiegł zrozpaczony Wojtek. ŚWINIĘ MU KRZYWDZĄ! Zobaczył świetlistego Anatola odchodzącego w dal. Szuka jak wypuścić oczkodzika.
Anatol zdecydował się rzucić czar usypiający Wojtka biologicznie. Oczkodzik używa transsprzężenia z Wojtkiem. TRANSGENICZNY ŚWINIOCZŁOWIEK.
Wieprz przetransportowany. Misja wykonana. Ludzie poczyszczeni.

**Dzień 3**:

Czas zająć się Waleczniskiem. Nasz Zespół kiepsko śledzi; nie radzą sobie z tym jak by mogli. Więc - niech ludzie wejdą do Walecznisko i robią swoje walczenie a Anatol fortyfikuje teren. Niech nikt nie może wyjść.
Kinga czeka aż ludzie wymasakrują norki i ich pojedynczo powyłapuje. Niech powiedzą, skąd są norki. Nie ma Paradoksów. Pamięć wyczyszczona.
Zespół dowiedział się, że to jest założone dawno przez burmistrza; on czerpie z tego kasę. Czasem to nie walka fretek. Czasem to wyuzdane imprezy. Chodzi o silny ładunek emocjonalny.
Zespół, nadal niezauważony (acz cbzyszczenie zajęło cały dzień) zdecydował się zostawić te walki i znaleźć maga za to odpowiedzialnego - bo krew.

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy
* 5: anulowanie wyniku konfliktu

Z sesji:

1. Żółw (19)
    1. Wojtek: zostaje Człowiekiem Od Świń. Świnie affinity.
    1. Stefania: pozyskuje czarnotrufle Esuriit.
    1. Łowcy Świń: pozyskują dowody i lead na inne świnie.
    1. Stefania: fortyfikuje działania Eliksiru Aerinus pod kątem tego że jej było wolno.
    1. Bonifacy: nie rejestruje niczego odnośnie norek, już nigdy.
    1. Łowcy Świń: silniejsza współpraca z harcerzami (pan samochodzik w wersji fail).
1. Kić (3)
    1. Sylwester Bankierz: po akcji dał pozytywną opinię Kindze (że samodzielnie zrobiła dobrze).
1. Dzióbek (3)
    1. Stefania: przekaże wieści o efektownej akcji Anatola. The word will spread.

# Streszczenie

Po usunięciu Wyssańca Esuriit Kajetan poprosił kogoś o monitorowanie terenu. Dwoje uczniów terminusów skupiło się na złapaniu i usunięciu oczkodzika z powodzeniem; dodatkowo, doszli do tego, że są tam dziwne walki norek. Nie zatrzymali ich, ale zdecydowali się znaleźć odpowiedzialnego maga.

# Progresja

* Anatol Sowiński: Stefania Kołek przekazuje szerzej wieści o efektownym wejściu Anatola. Anatol dostaje opinię lepszego niż jest.
* Kinga Bankierz: Sylwester Bankierz dał Kindze pozytywną opinię jako niezależnej agentce Świecy
* Bonifacy Jeż: Nie zauważa norek. Nie zapamiętuje norek. Norki dla niego nie istnieją. Nic związanego z norkami. Luka poznawcza wywołana Paradoksem.
* Wojciech Piekarz: uzyskuje 'świnie affinity'. Świnie do niego ciągną, on dobrze z nimi żyje, one go lubią, jemu łatwiej je zrozumieć.
* Stefania Kołek: uzyskuje niewielką próbkę czarnotrufli Esuriit. Nie do końca jeszcze wie co to.
* Stefania Kołek: wystarczająco zabezpieczyła siebie i Eliksir Aerinus u Sądecznego odnośnie czarnotrufli.

## Frakcji

* Łowcy Dziwnych Świń: mają bardzo silne przesłanki, że coś paranormalnego się dzieje i ze świniami też
* Łowcy Dziwnych Świń: uzyskali współpracę z kilkoma grupami mazowieckich harcerzy. Frakcja rośnie w ludzi.
* Senesgradzki Chaos: utracił lekko zmutowanego oczkodzika w okolicach Półdary
* Senesgradzki Chaos: pozyskał Golema Ogrodniczego Czarnotrufli, napędzanego energią Esuriit.

# Zasługi

* mag: Anatol Sowiński, od efektownych wejść i walki z oczkodzikiem o transgeniczność Wojtka. Główny negocjator zespołu (smutne).
* mag: Kinga Bankierz, wykorzystywała mnóstwo artefaktów Świecy i Dare Shiver by pozyskać oczkodzika. Główna tropicielka zespołu (smutne).
* mag: Stefania Kołek, szybko wymyśla alibi, szuka oczkodzika by się go pozbyć i nie wchodzi w konflikt ze Świecą. Coś kręci w okolicy. Zdaniem Kingi, główna podejrzana w sprawie walk norek.
* czł: Bonifacy Jeż, pomocny druh, który pisał petycję odnośnie nielegalnej hodowli norek. Paradoks pozbawił go umiejętności rejestrowania norek.
* czł: Wojciech Piekarz, pokonał swój strach i zaprzyjaźnił się z oczkodzikiem. Pomógł magom go znaleźć, ku swej wielkiej rozpaczy.
* czł: Paweł Kupiernik, chodził po okolicy, szukał oczkodzika i wzmacniał Łowców Dziwnych Świń o harcerzy.
* czł: Gabriel Purchasz, burmistrz Półdary zaangażowany w fight club Walecznisko w lesie. Ma nielegalną hodowlę norek.

# Plany

* Anatol Sowiński: Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).
* Kinga Bankierz: Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).
* Stefania Kołek: Zbadać czym są te dziwne czarnotrufle (Esuriit). Jakie mają własności. Czym się różnią.
* Stefania Kołek: Kontynuować biznes budowania czarnotrufli i uniknąć wykrycia; doukryć tą sprawę.

## Frakcji

* Senesgradzki Chaos: Używając Golema Ogrodniczego Czarnotrufli, Skazić jak najwięcej spod ziemi.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Półdara
                    1. Miasteczko turystyczne
                        1. Działki turystyczne, gdzie znajduje się nielegalna hodowla norek
                            1. Nielegalna hodowla norek, gdzie jest mała efemeryda zmieniająca norki w waleczne gryzonie.
                    1. Las turystyczny
                        1. Obóz harcerski, centralny sztab dowodzenia Zespołu.
                        1. Parów Wtargnięcia, w okolicy którego grasował oczkodzik. Tam został złapany. Pod nim jest hodowla czarnotrufli.
                        1. Walecznisko, metalowy zamaskowany barak w lesie. Tam toczą się walki norek i inne silne emocjonalnie rzeczy. Stamtąd energia pompowana jest pod ziemię.

# Czas

* Opóźnienie: 2
* Dni: 3

# Wątki kampanii

nieistotne

# Przeciwnicy

## Oczkodzik Lokalny
### Opis

Zmutowany Oczkodzik szukający czarnotrufli.

### Poziom

* Trwałość: 2
* Poziom: Trudny (5)
* Siły: Detektor, Szperacz
* Słabości: wrażliwy na katalizę, chce tych czarnotrufli, 

## Golem Esuriit
### Opis

Golem zajmujący się czarnotruflami, Skażony energią Esuriit i napędzany przez nią.

### Poziom

* Trwałość: 3
* Poziom: Trudny (5)
* Siły: napędzany energią Esuriit, bardzo pancerny, bardzo silny, relentless hatred, wzbudza strach
* Słabości: widzi tylko energię życiową, stosunkowo powolny, niezgrabny

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* Sprawdzenie mechaniki 1801 jako całości
* Sprawdzenie kalibracji liczb 1801
* Zrobienie czegoś co się Dzióbkowi spodoba ;-)

## Po czym poznam sukces

* Dzióbek się będzie dobrze bawił
* Liczby dadzą dobry rezultat

## Wynik z perspektywy celu

* Dzióbek się dobrze bawił: sukces
* Liczby dały ciekawy rezultat; było za prosto. 
    * Wszystkie konflikty wygrane przez postacie. 
    * Mitygacja: podnieść bazy konfliktów o 1-1-1-1 dla próby (wariant mechaniki 1806).

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

