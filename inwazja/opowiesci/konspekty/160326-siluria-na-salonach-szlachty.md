---
layout: inwazja-konspekt
title:  "Siluria na salonach Szlachty"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160217 - Sojusz według Leonidasa (LB, AB, DK)](160217-sojusz-wedlug-leonidasa.html)

### Chronologiczna

* [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)

### Logiczna

* [151007 - Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)](151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)
* [151021 - Przebudzenie Mojry (HB, SD)](151021-przebudzenie-mojry.html)

## Punkt zerowy:

## Misja właściwa:

### Kontekst misji

- Siluria pobita, osłabiona i ogólnie biedna; ale interesuje się nią Wiktor Sowiński (i trochę Diana Weiner).
- Wiktor próbuje podporządkować sobie Silurię. Siluria próbuje podporządkować sobie Wiktora.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów bo Arazille.

### Cel misji:

Cel misji: wyprowadzenie w jakim stanie skończy Wiktor i Siluria, oboje tańczący dookoła siebie i próbujący przechwycić drugą stronę. A przy okazji Diana jako "ofiara" między nimi.

### Faza 1: Siluria w "nowym domu"

Siluria jest pobita i regeneruje w domku Kermita. Jako, że Kermit dość szybko znalazł Tajemniczego Sprawcę, dowiedziała się też, że został opieprzony przez Ozydiusza; nie dał rady zatrzymać Głowicy (pardon, tajemniczego przestępcy). 

Pukanie do drzwi, wchodzi... Wiktor Sowiński. Idzie do Silurii cały pewien słusznego gniewu i mówi jej, że niestety nie jest tu bezpieczna. Pomaga jej wstać i mówi jej, że Siluria jest pod jego osobistą opieką i zaprasza ją do Centrum Wewnętrznego Świecy, skrzydła pałacowego. Siluria próbuje oponować, ale Wiktor jest nieugięty. W końcu skrzydło Kermita zostało pokonane a tam Siluria jest pod ochroną Wiktora.

(Wiktor 13v?): Siluria idzie do Wiktora, pod jego opiekę.
(Siluria 13v11->win): wywalcza sobie swobodę.

Z wdziękiem, Siluria poddaje tą bitwę; niewiele może poradzić na osobę tak zdeterminowaną. Zwłaszcza, że nie pamięta co się działo i jest ciekawa jak wygląda Kompleks Pałacowy Świecy. Jednak wywalcza sobie prawo do wolności i w miarę swobodnego poruszania się; nie chce się pchać w zakazane miejsca, ale chce móc opuścić kompleks. Nie jest delikatnym kwiatkiem. Wiktor akceptuje. Po swojemu docenia; Siluria jest w końcu silną dziewczyną.

Siluria zabiera ze sobą najistotniejsze rzeczy w ciągu 15 minut (by pokazać Wiktorowi, że umie) i zostawia Kermitowi notkę. Tam zaznacza, co ją interesuje by jej jeszcze podesłał (komunikat bardzo zrozumiały zwłaszcza dla Diakonów). Wiktor mówi Silurii pod czyją będzie opieką - jakie osoby będą ją pilnować. Siluria zauważyła, że wszystkie są ze znacznych rodów. Wiktor pokazuje jej pośrednio jaką ma władzę. Siluria się cieszy - to będzie jej władza ;p.

Kompleks Pałacowy Świecy. Siluria dostała własny pokój, duży. Pilnuje tego pokoju dwójka magów:
- tien Gerwazy Myszeczka, terminus-strażnik Wiktora, który pilnuje Wiktora i go chroni. Stara gwardia Świecy. "Przekazany w spadku".
- tien Wioletta Bankierz, terminuska, lojalna, kompetentna i godna zaufania czarodziejka Szlachty. Ambitna, kompetentna karierowiczka.

Siluria zwęszyła słaby punkt. Wioletta jest karierowiczką; jeśli zwęszy skąd teraz wieje wiatr, może stanąć po stronie Silurii... z drugiej strony, Gerwazy jest potencjalnie bardzo niebezpieczny ale i potencjalnie bardzo pomocny; Myszeczki o Diakonach wiedzą bardzo dużo (i vice versa) a Gerwazy jest STARSZYM terminusem (40+ lat). To powoduje, że jest potencjalnie bardzo niebezpieczny... ale on nie jest członkiem Szlachty. I tu jest jego potencjalny słaby punkt (a raczej: możliwość działania Silurii). Ale to znaczy, że Wiktor będąc w Szlachcie dąży do naruszenia porządku Sowińskich... czyli potencjalna intryga gdzie Siluria i Gerwazy mogą teoretycznie współpracować...

Siluria widzi tu pewien dodatkowy konflikt. Jeśli oni mają ją strzec, to im bardziej Siluria korzysta ze swojej wolności, tym im trudniej. Więc lepiej od razu ustawić sprawę jasno. Szczęśliwie, Wiktor dobrze zaczął - przedstawił Silurię Wioletcie i Gerwazemu i powiedział, że włos jej nie może NIGDY z głowy spaść ale ma mieć swobodę ruchów. I mają to zapewnić. Środki są dość mało ograniczone. Wioletta próbowała oponować, ale Wiktor ją uciszył swoim "środki są nieograniczone". Wioletta się uśmiechnęła i powiedziała, że "lady Diakon" jest bezpieczna.

Siluria chciała osiągnąć jeszcze jedną rzecz - w końcu w chwili obecnej wygląda na taką... no, przenoszoną. Bardziej własność niż osoba. Zdecydowała, że trzeba pokazać jakiś charakter. Zapytała, czy istnieje możliwość poćwiczyć z terminusem; nie chce następnym razem być tak pokonana (jakkolwiek "tak" wyglądało). Wiktor się uśmiechnął - może ćwiczyć z nim. Siluria powiedziała, że jest za dobry dla niej; nie jest w stanie wygrać ani nawet sensownie walczyć. Pochlebiła mu. Wybujałe ego Wiktora jedynie pomogło; od ręki go przekonała. Gerwazy się uśmiechnął, ale Wioletta nic nie zauważyła.

Wiktor zastanowił się i Wioletta zaproponowała się na ochotniczkę do szkolenia Silurii. Wiktor powiedział jej, żeby nawet nie ważyła się uszkodzić Silurii. Wioletta powiedziała, że nawet by nie śmiała. Opinia Wioletty o Silurii jest w miarę jasna - trophy girl dla Wiktora. Silurii to pasuje. Zwłaszcza, że sparingi zapewniają jej jeszcze możliwość wejścia w kontakt fizyczny z młodą terminuską Bankierzy...

Wiktor poszedł zająć się swoimi sprawami, Siluria dostała pokój do dyspozycji... musi się rozgościć, poprosić o prawo do podstawowych zaklęć, odebrać przesyłkę od Kermita... normalne rzeczy.

Dzień 2.

- Dagmara skłania Wiktora, by ten sprawdził Silurię; niech ta pomoże jej określić miejsce Blakenbauerów wobec Szlachty i Kurtyny. Plus, infiltracja Kurtyny.
- Wiktor żąda od Silurii skrzywdzenia maga Kurtyny, tien Antoniego Kurzamyśla i przekazanie informacji z jakim Millennium może współpracować.

Rano Siluria została poproszona na audiencję do Wiktora. Odprowadziła ją niezawodna Wioletta. Siluria przygotowuje się dobrze, ale nie idzie jakoś super wyzywająco. W drodze jeszcze Wioletta zasugerowała sparring po południu. Siluria się zgodziła - jeśli Wiktor się zgodzi.
Wiktor przyjął ją w dużej sali. Jest tam Wiktor, inny czarodziej Świecy wyglądający bardzo niekomfortowo (zbyt nieważny by Siluria mogła go poznać) i czarodziejka w mundurze, z blizną na twarzy. Tien Dagmara Wyjątek. Po minie Wioletty Siluria poznaje, że ta też jest zdziwiona.

Wiktor powiedział Silurii, że ten mag tutaj pochodzi z Kurtyny i powiedział, że Blakenbauerowie tak naprawdę współdziałają ze Szlachtą i Kurtyną. I czy ona pomoże jemu w ostatecznym pokonaniu Kurtyny. Zapytany, wywalił Wiolettę z pokoju (nie doceniła). Potem wyjaśnił: istnieją zaklęcia których on nie może rzucić; zaklęcia Diakonów. I on chciałby, by Siluria rzuciła takie zaklęcie na tien Kurzamyśla. On dostarczy zaklęcie, ona rzuci zaklęcie jakiego nie-Diakon będzie miał problemy rzucić. Siluria nie chce tego zrobić... i nie chce narażać KADEMu. Tien Kurzamyśl jest magiem w bazie Emilii Kołatki i Wiktor chciałby móc mieć sleeper agenta którego uruchomi i unieszkodliwi Emilię. Jak wiadomo, Emilia jest kiepska w walce.

Widząc sytuację - to pułapka Dagmary Wyjątek przeciwko Silurii - ta decyduje się na współpracę. Niech Dagmara się gniewa i złości. Z punktu widzenia Wiktora, Dagmara jest nieistotna. Siluria zauważyła ku swojemu zaskoczeniu, że Dagmara się podkochuje w Wiktorze...
Siluria zaznacza, że oferta dostarczenia tych zaklęć jest bardzo kusząca, ale ona nie chce w to mieszać KADEMu. Wiktor się zgodził; chce najlepszą Diakonkę jaką zna. Tu Siluria zauważyła, że Wiktor wie o uczuciach Dagmary i to wykorzystuje; oraz, że wierzy w dobrą wolę Silurii.

Mag Kurtyny zaczął błagać o litość Silurię i mówić, że jest przecież czarodziejką KADEMu. Wiktor mu przerwał. Zdrajca ma zachować ciszę. Siluria mu współczuje, ale nie okaże tego. Nie tu i nie teraz. Siluria jest skłonna dać Wiktorowi to czego chce; pokazać się jako power-hungry sorceress (pasuje do KADEMu) a dodatkowo dać Wiktorowi co ten chce, dzięki czemu tien Sowiński będzie bardziej jej pewny. 

ESCALATION:
Faza 1:
- Siluria próbuje przekonać Wiktora i Dagmarę że jest po ich stronie
- Dagmara stara się udowodnić Wiktorowi, że Siluria jest zła

Siluria przekonywanie: 08b # 06 (skill) # 06 (daje to co chce # KADEM) -> 20
Wiktor trudność:  04b # 06 (skill) # 03 (okoliczności, ostrzeżenie) -> 13
Dagmara trudność: 08b # 04 (skill) # 06 (Diakon, pułapka, skanowanie) # 05 (zaklęcie wspomagające) -> 23

Dagmara przekonuje Wiktora:
Dagmara trudność: 04b # 02 (skill) # 03 (lojalna służba) -> 09

Wynik:
- Siluria przekonała Wiktora, że jest po stronie Szlachty i pomoże
- Dagmara nie jest w stanie już nigdy przekonać Wiktora; ten też się nie zgodzi na pułapkę ze strony Dagmary by wykazać podłość Silurii.
- Wiktor wierzy w lojalność Silurii.

Faza 2:

Wiktor się szeroko uśmiechnął. Mag Kurtyny chciał kwilić dalej, ale Dagmara go uciszyła jednym ruchem palca; Silurię zdziwiło, że Dagmara nie usunęła sobie blizny z twarzy (bo ją strasznie oszpeca).
Dagmara zaatakowała. Ciche słowa. Tak jak Siluria jest czarodziejką KADEMu, nie powinna tego robić. Tak jak łamie przysięgi KADEMu, tak złamie przysięgę daną Wiktorowi...? Siluria stwierdziła, że mogłaby poczuć się obrażona takim postawieniem sprawy. A KADEM chce zwiększenia mocy; przymknąłby oczy na to, co mówi Siluria, bo to nie jest wbrew zasadom KADEMu.

Dagmara trudność: 06b # 02 (skill) # 03 (przygotowanie, znajomość Wiktora) -> 11

Siluria chce, by Wiktor zobaczył, że Siluria poczuła się obrażona. "Jeśli tak traktuje sojuszników, to jak wrogów?". I ogólnie. Jest biedna i ranna i była poproszona o pomoc...

Siluria: 17. 

Wynik:
- Wiktor się zdrażnił. Obsobaczył Dagmarę, że ta się wyżywa na niewinnej i zmęczonej Diakonce. Kazał jej wyjść z sali.
- Dagmara odmówiła. Siluria jeszcze nie rzuciła tego zaklęcia; ona zostanie. Wiktor zaeskalował. Dagmara powiedziała, że Diakonka może rzucić nie to zaklęcie; a ona to zobaczy. Wiktor pozwolił jej zostać.

END OF ESCALATION

Siluria się zgodziła; jako, że to rytuał, zajmie moment. Parę dni. Zaczęła przygotowywać destabilizator wzoru maga, gdy Wiktor ją zatrzymał. To nie pora na to. Pokazał Dagmarze, że Silurii można ufać. Poprosił też Silurię o to, by ta podała mu kontakty biznesowe. Magów i osoby z Millennium które może wynająć. Interesują go siły, które pozwolą na to, by tworzyć kryzysy na kopalińskiej prowincji i by je rozwiązywać. Wiktor chce przejąć Świecę dookoła Kopalina, by Ozydiusz się nie zorientował.

Siluria z przyjemnością przekazała kontakt. I tak to by się stało, a to jej nic nie kosztuje. A nawet daje małą przysługę u rodzinki. Bo Wiktor zapłaci.

Wiktor podziękował Silurii i pozwolił jej odejść. Kazał Dagmarze odprowadzić Silurię do pokoju.

Więc Silurię odprowadzały dwie terminuski. Wioletta dała dyskretnie do zrozumienia Silurii, że widzi, że ta jest cenionym gościem i jest skłonna jej posprzyjać. Pokazała też, że zna się na Diakonach pytając o "itd" (żeby Siluria nie była sama w łóżku). Wioletta zaznaczyła, że traktuje Silurię jak cenionego gościa, na co Dagmara zmierzyła ją groźnym spojrzeniem. Co ciekawe, Siluria zauważyła, że dwie terminuski trzymają się w miarę blisko; nie są przyjaciółkami, ale nie są rywalkami i się lubią na swój sposób. Wioletta uważa swoją pozycję na wyższą niż Dagmary, co już w ogóle jest niesamowite dla Silurii.

Dla Dagmary Wiktor jest Ukochanym Przywódcą. Jest mu fanatycznie lojalna oraz jest w nim zakochana. Ale nie słucha go; ma zbyt silny charakter. Ma własne, zdecydowane zdanie. Dla Wioletty Wiktor jest wielkim przywódcą, ale Wioletta patrzy na to dużo bardziej racjonalnie i pragmatycznie. Ona może nawet nie wierzyć w swojego przywódcę, ale daje jej korzyści więc będzie lojalna.

Jako, że Siluria chce się rozpakować, Wioletta i Dagmara poćwiczą pierwsze. Potem Siluria dołączy.

Siluria skończyła się rozpakowywać. Szybki przegląd - adekwatny strój do ćwiczeń - zadbanie, by był cholernie seksowny - i narzucenie normalnego ubioru (błyskawicznego do zdjęcia). I poszła na salę szkoleniową. A tam... 
Dagmara i Wioletta walczą. Wioletta jest świetną wojowniczką, nie tylko dobrą terminuską, ale świetnie walczy. Widać, czemu jest ochroniarzem Wiktora. Dagmara jest słabsza; jest między nimi zdecydowana różnica. Ale Dagmara walcząca ciężką bronią i odpowiednio pozycjonując jest w stanie zagrozić lepszej od siebie przeciwcznicze. Walczą na ostre. Dagmara nie używa painkillerów. Na jej ciele jest sporo blizn; Siluria jest już pewna, że to część wzoru Dagmary. Innymi słowy, Dagmara myśli o sobie jako o czarodziejce z bliznami. To jej tożsamość.

Przypadkowo, w tej walce wygrała Dagmara (w większości wygrywa Wioletta). Dagmara prawie zrobiła coup de grace, ale się zatrzymała. Siluria zobaczyła, że Dagmara NAPRAWDĘ rozważała coup de grace. Wioletta też to wie. Uśmiechnęła się tylko do Dagmary, ta odpowiedziała uśmiechem. Ich relacja jest... ciekawa.
Terminuski sobie ufają do tego stopnia, że jest krew w okolicy. Gdy tylko Siluria objawiła swoją obecność, krew została usunięta. Wioletta wypiła odpowiedni eliksir, Dagmara też. 

Dagmara poszła na bok, popatrzeć jak Wioletta i Siluria walczą. Jest ciekawa jak dobra w walce jest Siluria. Wioletta spytała Silurię, na jakiej broni się zna. Na żadnej. Wioletta użyła Projektora Świecy do dopasowania noża dla Silurii. To od tej pory broń Silurii. Spróbują tak walczyć; ona pójdzie bez broni.
Póki Dagmara obserwuje, Siluria się hamuje.

Jako, że Siluria fatalnie złapała nóż, Wioletta poprosiła Dagmarę o to, by ta stanęła przeciw niej. Wioletta wzięła nóż, poprosiła Dagmarę, by ta wzięła miecz dwuręczny. Starcie nie poszło po myśli Wioletty; gdyby nie ekran ochronny Dagmara rozcięłaby terminuskę na pół. Powiedziała, że "ona zawsze walczy na serio". Widząc jakie spojrzenie rzuciła jej Wioletta, Siluria ma wrażenie, że to była wiadomość dla Silurii... i ciekawe, co poszło po hipernecie. Bo Wioletta najpewniej chciała się po prostu popisać...

Po krótkim poćwiczeniu "na sucho", Siluria zdecydowała się stawić czoło Wioletcie. Wioletta jest bezbronna, Siluria ma nóż.
Wioletta jest jedną z najlepszych wojowniczek w systemie. Siluria ma nóż; przegrała bitwę, ale nie jest tragicznie dzięki przewadze broni.

Dagmara powiedziała, że już widziała wszystko i sobie poszła. Siluria oczywiście w to nie wierzy. 
Siluria próbuje walczyć w taki sposób, by całkowicie zanudzieć potencjalnie obserwującą Dagmarę. 17 v 13. Udało się. Nie zniechęciła Wioletty, ale na pewno ma spokój z Dagmarą.

ESCALATION:

Krok 1:

- Wioletta chce zmęczyć Silurię, żeby ta dała jej spokój. \| WIOL: 15, SIL: 6 -> Siluria wymęczona. Płaci licznikiem.
- Wioletta chce zachwycić Silurię treningiem, żeby ta doceniła ją w oczach lorda Wiktora Sowińskiego. \| WIOL: 10, SIL: 12 -> Siluria poddaje konflikt; wykorzysta.
- Siluria chce zaskoczyć Wiolettę; zrobić w Wioletcie "o!", żeby Wioletta uznała Silurię za osobę wartą poświęcenia czasu. \| SIL: 15, WIOL: 10 -> Wioletta przekonana.

W wyniku Siluria jest bardzo wymęczona (i spocona; organizm Diakonki wydziela odpowiednie feromony), ale dzielnie walczy. Wioletta się zainteresowała Silurią (czysto intelektualnie się zainteresowała). Dziewczyny zaczynają się dobrze bawić, choć nie w relacji równoprawnych wojowniczek. W relacji mistrzyni - uczennica.

Krok 2:

Siluria zmieniła lekko tryb walki. Poprosiła Wiktorię, by ta jej pomogła; czasami Siluria jest potencjalnie obezwładniona. Ktoś ją trzyma. Ktoś się z nią mocuje. I jak ona ma z tym sobie radzić? Wioletta profesjonalnie zaproponowała, by Siluria ją w taki sposób złapała a ona sama jej to pokaże. A Siluria umiejętnie łapie Wiolettę tak, by tamta czuła dotyk ciała ORAZ słowami i sytuacjami w odpowiednie miejsca kieruje jej umysł.
...biedna terminuska jest zupełnie nie w swojej lidze...

- Siluria chce zainteresować sobą Wiolettę. Naprężone ciało, zapach... sama nie chce sprawiać wrażenia że CHCE tak działać. To ma pochodzić od Wioletty. \| SIL: 17, WIOL: 10 -> Siluria ją ma.
- Wioletta chce dowiedzieć się od Silurii co zaszło na linii Dagmara - Siluria - Wiktor. \| Siluria poddaje konflikt za informację o relacji Wiktoria - Dagmara.

Wioletta powiedziała, że Dagmara jest lojalnym żołnierzem. "Obedient dog". Zrobi co się jej każe, ale przede wszystkim co uważa za słuszne. Wioletta często była wzywana do łóżka Wiktora; Dagmara nie. Powiedziała, że blizna na twarzy Dagmary została zrobiona przez Wiktora. Smagnął ogniobiczem myśląc, że się cofnie; wszyscy się cofają. Dagmara się nie cofnęła i nie pozwoliła wyleczyć tej blizny. Min. gdy Wiktor patrzy na Dagmarę, ma wyrzuty sumienia, że ją oszpecił. Ale nie da się Dagmarze pomóc. Nie, dopóki ona nie chce.

Krok 3:

Wioletta jest niepewna i chce się wycofać; nie przywykła do takich sytuacji. Siluria chce zostawić "lasting impression" i doprowadzić do tego, że Wioletta uzna, że jej reakcje są ok, że nie musi brać płynów, że to normalne przy Diakonach. Że nie musi się ani dziwić ani bać ani wstydzić. Więc Siluria zręcznie sprowadza rozmowę na Diakonów i próbuje to wszystko pokazać Wioletcie tak, by tamta myślała, że sama o to pytała.

- Wioletta chce się wycofać. \| WIOL: 13 SIL 15 -> Wioletta się nie wycofa.
- Siluria chce zarzucić wędkę na Wiolettę. \| SIL 18 WIOL 16 -> nie wychodzi więc ESKALACJA.

Siluria obiecała Wioletcie, że może ją czegoś nauczyć. Wioletta udaje, że nie rozumie. Siluria oferuje Wioletcie, że ją nauczy różnych rzeczy; może Wioletta nie jest Diakonką, ale nie musi nią być, by móc osiągać sukcesy. Wioletta uczy Silurię walczyć, to ona Wiolettę nauczy walczyć... inaczej.
Wioletta spytała kontrolnie, czy chodzi o Wiktora. Siluria się uśmiechnęła. Zależy, jak Wioletta ma zamiar to wykorzystać.
Jako próbkę pokazała Wioletcie jak potrafi się uśmiechać i patrzeć, podkręcając terminuskę i uzmysławiając jej, że Siluria od początku wie, jak działa na Wiolettę. A jednocześnie na nią nie nastaje. Mogła zrobić co chciała, ale nie zrobiła tego. Po czym Siluria poszła się wykąpać zostawiając terminuskę w lekkiej rozterce, między fascynacją, obietnicą mocy, lekką obawą i ekscytacją. 

Wioletcie zdecydowanie podoba się to uczucie.

END OF ESCALATION:

Oczywiście, zanudzona na śmierć Dagmara nic z tego nie widziała.

Trzy kolejne dni Siluria będzie z Dagmarą składać rytuał na biednego maga Kurtyny. Jednocześnie trzy kolejne dni Siluria będzie ćwiczyła z Wiolettą. Nie wyklucza się to.
Siluria dostała potwierdzenie, że nigdy jeszcze nie było samodzielnej Diakonki tak jak jest teraz ona. Gerwazy skutecznie uniemożliwiał Diakonkom innym niż Antygona kontakt z Wiktorem Sowińskim. Działał też przeciw Silurii, ale Wiktor mu powiedział "lol nope". Cenna wiadomość dla Silurii.

# Streszczenie

Wiktor Sowiński "zaprosił" Silurię do Kompleksu Centralnego. Gerwazy i Wioletta chronią jej, ale dają jej wolność. Siluria uczestniczy w rytuale zmieniającym tien Antoniego Kurzamyśla w sleeper agenta (w posiadłości Emilii); przekonała Wiktora, że jest mu lojalna. Przekazała Wiktorowi kontakty biznesowe Millennium, by ten mógł wynająć agentów. Potem - sparing z Wiolettą, w wyniku którego ta jest zafascynowana Silurią i używaniem erotyzmu w walce / sparingach. Wioletta nauczy Silurię walczyć a Siluria nauczy Wiolettę walczyć... inaczej.

# Zasługi

* mag: Siluria Diakon, infiltrująca Szlachtę, manipulująca Wiktorem, ścierająca się z Dagmarą i uwodząca Wiolettę. Ogólnie: Siluria w nowym, tymczasowym domu.
* mag: Wiktor Sowiński, udzielny książę Szlachty. Ten, któremu wszystko wolno. Wierzy w niewinność i uczciwość Silurii. Otacza się inteligentnymi sojusznikami, których nie słucha.
* mag: Gerwazy Myszeczka, 40-letni terminus chroniący Wiktora Sowińskiego. Nie jest ze Szlachty. Wygląda na to, że nie ma słabych punktów. Na razie.
* mag: Wioletta Bankierz, młoda, bardzo ładna, bardzo kompetentna osobista terminusa Wiktora. Prosty cel dla Silurii. Proaktywna i oportunistyczna karierowiczka. Groźna w walce.
* mag: Dagmara Wyjątek, terminuska z bliznami; bardzo nieugięta i podkochująca się (mocno) w Wiktorze. Rozpaczliwie chce się wykazać. Lojalna ponad wszystko. Robi co chce. Potrafi manipulować, choć udaje cegłę.
* mag: Antoni Kurzamyśl, mag Kurtyny, zwykle w bazie Emilii Kołatki. Teraz: porwany przez Dagmarę, by Siluria przekształciła go w sleeper agenta.
* mag: Kermit Diakon, który przygarnął Silurię tylko po to, by oddać Wiktorowi. Potem: nosił jej rzeczy i służył jako konsultant Wioletcie.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Sala reprezentacyjna
                                1. Elitarna sala treningowa
                    1. Dzielnica Kwiatowa
                        1. Domek Kermita Diakona