---
layout: inwazja-konspekt
title:  "Rekrutacja mimo woli"
campaign: taniec-lisci
gm: żółw
players: wojtuś, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160922 - Czarnoskalski konwent RPG (Esme, temp)](160922-czarnoskalski-konwent-rpg.html)

### Chronologiczna

* [160922 - Czarnoskalski konwent RPG (Esme, temp)](160922-czarnoskalski-konwent-rpg.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

"Idealna polska firma"

1. **Prowadzicie wspólny biznes w którym macie kontakt ze sporą ilością ludzi / firm (basen, agencja ochroniarska, przewóz ludzi...) - jaki to biznes?**
Prowadzimy sporą firmę kurierską zatrudniamy prawie setkę kierowców, większość zwykłej wielkości trucki, kilka tirów. Zdarzają się zarówno zwykłe przesyłki jak książka z allegro, komputer, ale są czasem przesyłki typu 20 tysięcy iphonów. W związku z tym prowadzimy stałe relacje z różnej maści firmami zajmującymi się ochroną.
2. **Jak się nazywa Wasza firma?**
Nazwa firmy TrustPort
3. **Jedno z Was ma kryminalną przeszłość (wykryty / nie wykryty / był w więzieniu...) - jaką? (pobicia, włamania, narkotyki, coś z komputerami, malwersacje...)**
Marcin Szybisty swojego czasu prowadził aktywną działalność przemytniczą. Głównie alkohol oraz wyroby tytoniowe przez wschodnią granicę, ale były również epizody związane z przemytem ludzi. Jego działalność nigdy nie wyszła na jaw, wycofał się z interesu po mocnym spięciu z ówczesnym wspólnikiem.
4. **Niedawno firma miała prawie katastrofę z powodu błędu. Trzeba było złamać prawo by ratować firmę. Jak złamaliście prawo i co to był za błąd?**
Firma otrzymala swietny kontrakt od b. duzego klienta na dowiezienie ogromnej ilosci elektroniki z centrum dsytrybucji tuz za granica Polsko-Niemiecka do ponad 50 centrow handlowych na terenie Polski. Pech chcial, ze podczas zalatwiania dokumentow przewozowych i celnych zaniedbano w firmie kilka formalnosci co doprowadzilo do zatrzymania kilku pojazdow z towarami na granicy i konfiskaty przez urzedy. Klient jesli by tylko sie o tym dowiedzial nakazalby zaplacic TrustPortowi odszkodowanie przewyzszajace roczne dochody spolki. Wspolnicy zdecydowali sie na bardzo ryzykowny ruch, zeby podrobic mase dokumentow zwiazanych z przekazaniem towaru po swojej stronie i przekupic paru przedstawicieli sieci handlowych, zeby towaru przyszlo wiecej niz w rzeczywistosci. Oprocz samego ogromnego ryzyka takiej malwersacji, jeden z nich wpadl w szal podczas rozmowy ze wspolnikami i zamierzal o wszystkim poinformowac wladze wlasnej firmy, klienta i sluzby. 
5. **Jak nazywa się firma należąca do osoby (jak się nazywa ta osoba), z którą jedno z Was się nienawidzi? Z jakiego powodu się nienawidzicie?**
Nazwa firmy - Cormex (Właściciel Konrad Matczak - były wspólnik Marcina Szybistego). Podczas największej akcji w trakcie swojej wspólnej działalności przestępczej, Konrad postanowił zatrzymać całą kontrabandę. Okazało się, że Konrad ma niezbite dowody na działalność przestępczą swojego wspólnika (tak naprawdę Konrad pociągał za sznurki, Marcin był wykonawcą większości roboty). Zagroził, że wyda go organom ścigania jeżeli będzie chociaż próbował odzyskać jego część.
6. **Jakie firma ma źródło finansowania?**
Zrodlo finansowania to oczywiscie dochody spolki, firma nie ma finansowania ani publicznego, ani specjalnie mafijnego, choc TrustPort nie wnika zbyt gleboko w biznesowe pochodzenie swoich klientow...

Ż: Co łączy malwersacje z przeszłości z policją w Okólniczu?
R: Zastępca komendanta pełnił służbę na granicy podczas tamtego transportu i wtedy nas spotkał... i kojarzy - służbista. Nie dostał awansu bo "się pomylił" (miał rację).
Ż: Przesłanka, kto stoi za tym co się stało?
W: Moja siostra pytająca o mojego wspólnika choć nigdy tego nie robi

## Misja właściwa:

Dzień 1:

Do Włodzimierza z nieznanego numeru zadzwoniła "Dorota". Która powiedziała, że ich ciężarówka - z TrustPortu - ma niewygodne dla TrustPortu informacje. I jedzie właśnie na policję. Zapytana czemu to mówi, powiedziała "Nie lubię jak politycy niszczą firmy". Ok... Włodek nie byłby sobą jakby nie potraktował tego poważnie. Zadzwonił do kierowcy, Jacka Molendy, i powiedział by ten wrócił do bazy. Jacek się nie zgodził. Włodzimierz nie dzwoni po ludziach. Od tego ma asystentkę...

Włodek kazał asystentce zadzwonić do kierowcy i powiedział, że ma nie dostać pensji. Kierowca nie był najszczęśliwszy, ale on zna Włodka; wie, że "on tak ma" i zaraz zmieni zdanie. Więc zostawił awizo i wrócił do bazy, gdzie pojechali Włodek i Marcin. Zmartwieni - nie spodziewali się takiej sytuacji.
Tymczasem Marcin dostał telefon od siostry mieszkającej w Rotbergu. Detektyw się jej pytał o Włodzimierza. Niefajnie... siostra poradziła by zrezygnoawł ze wspólnika. Marcin poprosił siostrę o ten numer telefonu i go dostał.

W końcu dotarła ciężarówka na miejsce i oni też dojechali. Sprawdzili - paczka na policję jest nadana (to najpewniej zapas papieru toaletowego czy ręczników), ale jest do niej przyczepiona teczka. Skąd ta teczka? No, był tu taki gość w ich kolorach firmowych i powiedział Jackowi, że musi to dokleić. A że firma ma procedury jakie ma... ;-).

Co w teczce? Papiery, tajne papiery (zniszczone kiedyś!!!) udowadniające że TrustPort działa jak taka... cwaniacka firma. Jakby to trafiło na komendę w Okólniczu, gg. Firma pada a oni do więzienia... Czyli ktoś chce ich zniszczyć, ale czemu? I ta Dorota... kim ona jest?

Dobra, wszystko jest monitorowane; to było na ichnim parkingu. Faktycznie, był gość; nieznana facjata, nieznany człowiek. Ale często zatrudniają tempworkerów - taka praca, czasem mocniej czasem słabiej a papiery... mniej istotne. Mają jego twarz i pani Helenka musi poszukać w dokumentach co to za jeden. Nie znalazła. Ma szukać BARDZIEJ.

Dzień 2:

Pani Helenka nadal nic nie ma. Nie ma szans, nie znajdzie się tego tajemniczego gościa. Cóż... 

Nasi niefortunni prezesi objeżdżają różne firmy z którymi fałszowali i malwersowali w tamtych czasach; ktoś MUSI coś wiedzieć. W końcu trafili na sieć handlową Rumcajs. Tam asystentka wpuściła Marcina i Włodzimierza do szefa, ale Włodek poznał głos - ta asystentka to Dorota która doń dzwoniła. Poprosił by zrobiła im herbatkę i spytał o imię - Dorota. AHA!

Andrzej (prezes Rumcajsa) nie chce nic powiedzieć; ale rezygnuje z usług TrustPortu. Namawiany przez Marcina przyznał, że będzie pracował teraz z konkurencją z Cormexu. Bardzo silnie przekonywany przez Marcina (i z Włodkiem który poszedł gdzieś indziej) powiedział, że za Cormexem stoi firma za którą stoi ktoś od Skubnego. A on się boi Skubnego. 
Andrzej nie ma pojęcia skąd się wzięły te dokumenty; mają na niego coś podobnego. Nie chce wiedzieć. Podejrzewa Skubnego.

No niefajnie, nikt nie chce podpaść Skubnemu...

Tymczasem Włodzimierz poszedł porozmawiać z Dorotą. Ona najpierw powiedziała, że nie wie o czym on mówi ale po chwili się zgodziła. Tak, ona pomogła. Czemu - bo nie lubi jak firmy są niszczone. A czemu nie pomoże bardziej? Pomoże... jeśli są silni. Na słabych szkoda jej czasu. Ona sama nie wie kto za tym stoi; to wie jej przełożony. Obiecała że się z nim skontaktuje i umówili się na wieczór w "Feniksie" w Rotbergu - daleko, daleko stąd. I Włodzimierz płaci.

Mając jeszcze trochę czasu, zadzwonili do Leszka Żółtego, detektywa gadającego z siostrą Szybistego. Okazuje się, że on zbiera materiały. Dorabia sobie sprzedając co lepsze historie do Paktu.

"Czy nie ma pan żadnej godności?" - Marcin, zirytowany, do Żółtego
"Nie. Kiedyś chciałem być lekarzem. Chce pan bym opowiedział panu resztę historii mojego życia?" - Żółty, spokojnie

Dobra. O 19:00 spotkali się w Feniksie z Dorotą. Faktycznie płaci Włodzimierz... ku swemu nieszczęściu. Dorota, w małej czarnej mini już czeka. Zapytana przez Marcina czemu mają jej zaufać odpowiedziała spokojnie - dała im teczkę na nich. Powiedziała, że w firmie mają kreta.
Prezesi posmutnieli. Kret :-(.

Próbowali przekonać Dorotę by powiedziała kto, ale się nie zgodziła. Nie jest pewna że są dość silni. Za to ma dla nich coś innego - dokumenty z Cormex, podobnej klasy jak były na nich. To wystarczyło, by olśnić Marcina - on uważa, że w tym świecie Dorota i jej organizacja są stronami z którymi warto pracować.

Włodek stwierdził, że jak nie dość silni... to niech ona spotka się z nim na ringu bokserskim. XD. Dorota się zgodziła. Podała kreta - to Jolanta - po czym umówili się na wieczór. Jeśli Dorota wygra, Włodek ufunduje salę gimnastyczną szkole sztuki walki (1 wieczór / tydzień przez 2 lata). Jeśli Włodek wygra, Dorota załatwi im spotkanie ze swoim przełożonym. Dobra, późny wieczór, sala gimnastyczna - FIGHT!

W pierwszym starciu z trzech Dorota wygrała; zaskoczyła Włodka i go załatwiła. Świetnie walczy.
W drugim Włodek podszedł defensywniej i Dorota poszła z brudną walką. Pokonała Włodka wijącego się na ziemi... acz on pokonał ból, złapał kij z aikido i jej przypier***. Dorota na krótko straciła przytomność z zaskoczenia.
W trzecim - decydującym - starciu stwierdzili, że trzymają się walki fair. Dorota pokonała Włodka, ale on zaeskalował, dał złamać sobie nos i znokautował terminuskę.

Cel Doroty osiągnięty - ma szacun i zaufanie u Włodka. Zaimponowała mu.

Spotkanie z jej przełożonym następnego dnia. W międzyczasie, Włodek zwolnił Jolantę.

"Kurwa! Zwolniłem asystentkę! Nie mam komu mówić co ma robić!" - Włodek
"Nie uwierzą że to ty bo nie dzwonisz osobiście do ludzi" - Marcin

Dzień 3:

Spotkanie w Feniksie. W Rotbergu.
Spotkanie z Kornelem.

Włodek i Marcin dowiedzieli się od Kornela, że ów jest politykiem. Niebezpiecznym, wpływowym i chętnym do rozwiązania problemów korupcyjnych w najwyższych kręgach władzy. Nie, nie takiej korupcji jak "ta ich". Takiej prawdziwej i groźnej. Zapytany jak proponuje rozwiązać problem, zaproponował wysłać materiały o Cormex do Hektora Blakenbauera. Ten człowiek to pies na Skubnego.

I tak też zrobili, przez podstawionego słupa.

Docelowo Dorota teraz może nadać ich komuś... by musieli schronić się pod opiekę Kornela.
Nikt nie powiedział, że to sprawiedliwe...

# Progresja

* Dorota Gacek: dostaje znajomości w firmie transportowej TrustPort
* Kornel Wadera: dostaje znajomości w firmie transportowej TrustPort

## Frakcji

* Taniec Liści: dostaje macki w firmie transportowej TrustPort

# Streszczenie

Dorota Gacek dostała za zadanie przejąć kontrolę nad firmą TrustPort. Wmanewrowała szefów tej firmy w stary konflikt z konkurencyjnym Cormexem powiązanym ze Skubnym, po czym oddała ich w ręce Kornela Wadery. Szefowie TrustPort nadali donos na Cormex, po czym Dorota miała możliwość tak ułożyć wydarzenia, by TrustPort wpadł w ręce Tańca Liści.

# Zasługi

* czł: Marcin Szybisty, upijał prezesa by wyciągać informacje, blokował niebezpieczne pomysły i bronił interesów firmy
* czł: Włodzimierz Tulewicz, sprał terminuskę Dorotę by dostać się do Kornela... jego ludzie z nim rozmawiają tylko, gdy z nimi pije
* czł: Jacek Molenda, kurier i kierowca pracujący dla TrustPort, który zna swoich szefów i wie na co może sobie z nimi pozwolić.
* czł: Konrad Matczak, prezes firmy transportowej Cormex (wielkiego rywala TrustPortu). Wpadł w strefę wpływów Szymona Skubnego.
* czł: Leopold Teściak, policjant Okólnicki, który zdobyłby reputację i szacunek gdyby udało mu się udowodnić, że TrustPort jest winny.
* czł: Magda Szybisty, dentystka pracująca w Rotbergu i siostra Marcina, której bardzo zależy na tym by w Pakcie nie pojawiło się ich nazwisko.
* czł: Jolanta Iwan, asystentka Włodzimierza i Marcina. Posądzona o bycie kretem i zwolniona; wrobiona przez Dorotę Gacek na potrzeby ukrycia infomancji.
* czł: Leszek Żółty, prywatny detektyw o cynicznym podejściu dorabiający sobie wysyłając leady do Paktu. Amoralny i stosunkowo niedrogi.
* czł: Andrzej Marciniak, skorumpowany prezes sieci Rumcajs który swego czasu prowadził malwersacje z szefami TrustPort, "w innych czasach".
* mag: Dorota Gacek, która doprowadziła do włączenia firmy TrustPort do strefy wpływów Tańca Liści. Intrygantka - infomantka bijąca się w brudny sposób z Włodkiem o szacun.
* mag: Kornel Wadera, główny koordynator i guru Tańca Liści pragnący poszerzyć wpływy organizacji by móc zacząć Sanację Świecy
* czł: Szymon Skubny, którego mafii wszyscy biznesmeni w okolicy się boją (i który sam nie wystąpił na tej sesji)

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Okólny
                1. Okólnicz
                    1. Miasteczko
                        1. Policja
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Dolny 
                    1. Obrzeża
                        1. Firma Cormex, konkurencja wobec TrustPort i pod pośrednią kontrolą Szymona Skubnego.
            1. Powiat Leere
                1. Dzierżniów, gdzie magia bardzo słabo działa
                    1. Ulica skarbowa
                        1. Firma przewozowa TrustPort, 100-osobowa, wpadła w obszar wpływów Tańca Liści
            1. Powiat Myśliński
                1. Bażantów
                    1. Strefa gospodarcza
                        1. Sieć handlowa Rumcajs, luźno powiązana z TrustPort i która prawie wpadła (przez Dorotę) w strefę Skubnego
        1. Dolnośląskie
            1. Powiat Rotberg
                1. Rotberg, jedno z największych i najważniejszych miast w województwie
                    1. Centrum
                        1. Luksusowa Restauracja Feniks, najdroższa i najbardziej luksusowa restauracja w Rotbergu; bez podsłuchów ;p
   
# Wątki



# Skrypt
znaczących konfliktów: [10 - 15]

fate points: 2/osobę

Siły drugiej strony:

- brak znaczących

# Czas

* Opóźnienie: 6 dni
* Dni: 3