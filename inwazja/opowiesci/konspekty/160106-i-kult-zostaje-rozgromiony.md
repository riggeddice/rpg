---
layout: inwazja-konspekt
title:  "...i kult zostaje rozgromiony"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151230 - Zajcew ze śmietnika partnerem... (PT)](151230-zajcew-ze-smietnika-partnerem.html)

### Chronologiczna

* [151230 - Zajcew ze śmietnika partnerem... (PT)](151230-zajcew-ze-smietnika-partnerem.html)

## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa (Orank) i sektę (Kły Kaina); ślady przekierowuje na nich.
Wiła nie żyje, zatruła się narkotykami gdzieś w lesie; Joachim Zajcew szuka Kłów w sprawie "zabitej przez nich wiły".
Policjanci na SERIO szukają śladów tajemniczej krwawej sekty. Coś w przeszłości się tu działo.
Podobno była tu krwawa relikwia nazistowsko-satanistyczna.
Maria dorabia pracując dla "Rytmu Codziennego"; informuje Paulinę, że baza sekty jest w lesie przy Gęsilocie.
Sekta konsoliduje siły i wyniszcza lokalnego gangstera, ten odpowiada ogniem.
Joachim Zajcew wszedł w sojusz z Pauliną przeciw sekcie i ściągnął do siebie czyściciela Korzunia.
Okazuje się, że Wiesław Rekin - członek Kłów - jest niebezpiecznym ulicznym wojownikiem.

## Stawki:

- Czy "Kły Kaina" ustawią skutecznie szkołę sztuk walki uczącej 'Faim de Loup'?			NIE
- Czy Łukasz Perkas da radę uchronić się przed nędznym losem (nieważne skąd przyjdzie ów los)? 
- Czy Alicja Gąszcz zostanie agentką Korzunia?

## Misja właściwa:
### Faza 1: Atak na kult

Dzień 1:

Paulina jest w szpitalu. Pracuje dzielnie. Z punktu widzenia Grażyny Tuloz zostawiła temat sekty... do momentu, aż dostała linka do unlisted YouTube od Marii.
Na filmiku siedzi mężczyzna z łysą głową, uduchowioną twarzą, bródką i wąsem i ogłasza, że mają do dyspozycji świętą relikwię. Trzyma artefakt - lub coś co wygląda jak ten artefakt w ręku. Naciska dłoń i po swastyce płynie krew. Lub coś co jak krew wygląda. Sekciarz wzywa wszystkich do dołączenia do niego i ogłasza, że będą pić krew niewiernych.
Paulina smuteczek. Przesłała Zajcewowi; dostała odpowiedź, że to na pewno niemagiczne. Wyczułby. A w ogóle to czyściciel mówi, że to na pewno niemagiczne. Zdaniem Pauliny, czyściciel w coś gra, Zajcew to łyka a sekta jest niefortunnie inteligentna...

Maria przesłała też policji to nagranie. Policja musi wiedzieć; to wygląda na eskalację. Umożliwiło to Kurczakowi wezwanie posiłków i udowodnienie, że źle się dzieje w Przodku...

Paulina się boi. Boi się, że Korzunio oszukuje Joachima Zajcewa, boi się, że Korzunio ma swoją agendę... boi się, że sekta za bardzo prosperuje. W związku z tym Paulina poprosiła go o spotkanie... z Korzuniem. Joachim, Krystian i Paulina. Korzunio zaproponował na spotkanie bibliotekę. Nieco zdziwiona, Paulina się zgodziła. Wieczorem.

Ku wielkiemu zadowoleniu Pauliny, w wiadomościach lokalnych pojawiła się Agnieszka Mariacka. Przedstawiła profil Wiesława Rekina - oprych, okrutny, bandzior, zakaz pracy z młodzieżą, z oskarżeniem: gdzie byli urzędnicy gdy ta gnida otwierała cokolwiek? Dodatkowo, "skruszony obywatel" (patrz: siły Przaśnika) pokazał nagranie z mowy Rekina do młodych ludzi, antysystemowe i przekraczające granice akceptowalności.

Spotkanie z Korzuniem wieczorem. Joachim, Krystian i Paulina mają spotkanie. Krystian Korzunio łyknął "tien Anię Diakon" i powiedział, że to on podłożył sekcie fałszywą relikwię. A przypadkiem ów filmik na YouTube przesłał też komuś, kto ma coś wspólnego ze "starymi satanistami" (czyt: Przaśnik). I jest szansa, że pojawi się też inny filmik na YT. A jeśli przypadkowo policja uważa, że cała ta sprawa jest zbyt niebezpieczna i zgarnie kilka osób...
Paulina i Krystian doszli do porozumienia - ona akceptuje to, że wszystko jest winą sekty (w szczególności problemy wiły), on zapewni bezpieczeństwo i zniszczenie sekty. Tak bardzo polityka.

Paulina też zapytała o to, jak idzie sprawa z wiłą. Korzunio zrzucił winę na sektę. Jak Paulina może pomóc? Trzeba ją znaleźć. Joachim ma kosmyk jej włosów, ale z uwagi na specyfikę tego miejsca może być to trudne. Ale to musi poczekać.

"Nie operuję na ludziach; operuję na strukturach, chyba, że szukam agentów" - Korzunio, 2016

Paulina jeszcze dała do zrozumienia Korzuniowi, że ona zna świat ludzi i że jest skłonna pomóc mu ukryć wszystko - jeśli tylko faktycznie w okolicy będzie spokój. Korzunio też dał jej do zrozumienia, że jego celem jest wyprowadzić stąd Zajcewa i zamknąć temat. Jest czyścicielem. Nie obchodzi go to miejsce, sekta, wiła. Chce minimalizować straty, bo to ludzie - jego gatunek, ale ważniejsze jest dla niego zadowolenie pracodawcy - tu: Gali i Joachima. Czyli "niech nikt się o nas nie dowie".

Paulina doskonale zrozumiała. Mają potencjalny sojusz.

Dzień 2:

Rano Paulina obudziła się do doskonałej wiadomości. Za Wiesławem Rekinem wysłano list gończy. Dojo zamknięte. No dawno jej tak nic humoru nie poprawiło; policja wbiła się do dojo, ale Rekin uciekł. A to wszystko w reakcji na działanie narwańca, który chciał skrzywdzić dziennikarkę; Agnieszkę Mariacką. Szczęśliwie, Agnieszka była chroniona przez prywatną agencję ochroniarską "Kobra". I "Kobra" ochroniła dziennikarkę przed kultystą. Rekin nie dał się aresztować; zniknął.

Przy okazji; Paulina dowiedziała się, że Korzunio załatwił ten atak na dziennikarkę. Wszystko było pod kontrolą; on po prostu chciał zapewnić, że policja wykona jakiś ruch; chciał dać im pretekst. Paulina się uśmiechnęła. Lepiej współpracować.

Najbardziej Paulinę martwi artefakt. Zdecydowała się dowiedzieć od Marii odnośnie przeszłości; tego, co ta sekta robiła kiedyś. Czy ta "relikwia" była magiczna? Czy to tylko symbol? Czy sekciarze zdradzali jakiekolwiek moce magiczne? Maria obiecała spojrzeć. Skupiła się na Arturze Kurczaku i Grażynie Tuloz. Poprosiła też Marię o to, by sprawdziła historię tej sekty i artefaktu. To się robi zbyt istotne; czy jednak Maria nie mogłaby przyjechać?

### Faza 2: Udaremniona apokalipsa

W szpitalu Pauliny włączyły się generatory awaryjne. Padł prąd.
Okazuje się, że prąd padł kaskadowo - nie tylko tutaj. Padła dzielnica. Grażyna Tuloz powiedziała, że to dlatego, że wysadzili transmiter... niedobrze.
Chwilę potem wybuchło dojo. Paulina orientuje się, że chyba nie o to chodziło Korzuniowi... zadzwoniła do Zajcewa. Ten powiedział, że no tak, sekta działa. Ale on ma wszystko pod kontrolą. Paulina zauważyła, że rzeczy wybuchają - na to Zajcew, że to sprawy między ludźmi. Paulina tupnęła nogą i powiedziała, że ona chce tu porządek. Nie ma być wybuchów. Żadnej apokaliptycznej sekty. Joachim się przestraszył i powiedział, że się tym zajmie.

Chwilę potem zadzwonił do Pauliny Korzunio. Sekciarze zbierają się do ataku na parafię Gęsilocką, policja jest zajęta w mieście i przygotowuje atak. Paulina chce tam się dostać by uratować księdza; Korzunio powiedział, że jej to załatwi. Nie tylko załatwił jej samochód, ale też zadzwonił do Grażyny Tuloz i ta zwolniła Paulinę. Idąc do samochodu Paulina zobaczyła, że Zajcew skutecznie rozbroił kolejną bombę, tym razem koło szpitala...
Wow.

Kierowca odwozi szybko Paulinę na parafię; to młody gość który wygląda na sekciarza. Powiedział zdziwionej Paulinie, że "kapitan Korzunio" jest zajęty innymi sprawami, więc on ją odwiezie. Paulina się po drodze przygotowuje - jest ich między 5 a 10, są już w parafii; policja nie jest na parafii tylko w kilku innych miejscach.

Parafia. Są już w środku i mają księdza... gorzej, mają księdza przygotowanego na rytualne morderstwo na ołtarzu. Są zajęci sobą. Większość jest lekko podminowana; ale dwóch jest takich zaciekłych satanistów starego stylu. Jest ich siedmiu.

ESCALATION 060106

Init_sect: 6 (val) + 3 (liczba/morale) + 3 (moc Przodka jeśli magia) = 12
Paulina: 11 + 2 = 13

Paulina chce, by sobie poszli i czuli, że zrobili to co chcą zrobić.
Kultyści chcą zabić księdza w rytualny sposób i spalić parafię.

Faza: Zabijanie księdza
1) Paulina pokazuje im w ich umysłach krwawe i okrutne sceny. Podpiera się znajomością anatomii, operacjami itp. Paulina używa fiolki leku by wzmocnić sympatią zaklęcie. SUKCES ich zatrzymuje, PORAŻKA lekko rani księdza. Porażka (11v12-> -1). Ksiądz jest lekko ranny.
2) Paulina musi pokonać (1) w Escalation Mode. Jako koszt (reroll) wzięła "ci, którzy już byli zatwardziali (ta dwójka) jest stracona". Nie do przekonania, zatwardziali. (14v1->11)
3) Kultyści eskalują. Grupka zaczyna się łamać (ksiądz to widzi i siedzi cicho, tylko się modli) - 3 kultystów ma efekt "what have I done". Próbują uciekać; dwóch zatwardziałych łapie jednego i przygotowują go do poświęcenia. Dwóch totalnie ucieka i nie chce mieć już z żadną sektą nigdy nic wspólnego. Nigdy. (9v11->1). Na miejscu zostaje 2 zatwardziałych, 1 ofiara i 2 niezdecydowanych.
4) Paulina eskaluje. Wchodzi policja. Nie zdążyli poświęcić kultysty. (13v1->17).

Bilans: 3 osoby z 7 kultystów nie będą kultystami (NIGDY) i będą zeznawać. Zwłaszcza ten co miał być ofiarą. Ksiądz przeżył, lekko ranny. Nikt nie zdążył podpalić parafii. Policja ma wszystkich w garści. 2 kultystów jest mega zatwardziałych i niebezpiecznych (tylko dwóch).

END_ESCALATION 060106

Bławatek podchodzi do Pauliny z marsową miną. Co ona tu robi, kultystka? Paulina: nie! Wezwano ją by udzieliła pomocy lekarskiej. Bławatek spojrzał, zmierzył wzrokiem, zrozumiał, że bała się wejść i to łyknął. Oki. Wysłał ją do "reszty lekarzy". A tam, lekarze policyjni. I ogólnie "mała ekipa". Antyterroryści i w ogóle.

Bławatek wie, gdzie iść; orientuje się, gdzie zlokalizowana jest sekta. Teraz idą ich rozgromić. Paulina stwierdza, że zdecydowanie chce być na tej akcji. Nikt lekarza nie wywali.

Akcją dowodzi antyterrorysta Hubert Rębski. Dowodzi akcjami na Śląsku. Teren bazy sekty został otoczony i przygotowany. Zgodnie z przesłuchaniami, sekta ma bunkier. Policja jest świetnie przygotowana i ma wszystkie możliwe przewagi. Rębski jest ostrożny.

I akcja policyjna odbyła się bez zarzutu. Paulina - jako lekarz, z drugiej linii - uderzała szerokimi falami mentalnymi; by wywołać jak najwięcej paniki i zamieszania wśród kultystów, by zniszczyć im morale. By ich zniechęcić. (13). Pomogła też uratować parę osób.

Kult został zniszczony.

- policja oraz Przaśnik wspólnie obronili Agnieszkę Mariacką; mimo, że stracili 'narwańca' to dali radę aresztować kilku innych
- Joachim Zajcew ograniczył zniszczenia
- Sołtys Gęsilotu wystawił Paulinie podziękowanie za akcję (Korzunio; dla Tuloz lub Hermana)
- Wiesław Rekin na wolności
- kult stworzył kilka drobnych komórek; w fazie utajonej
- Maria miała świetny artykuł z wydarzeń w Przodku
- W mieście zapanowała mała panika i histeria, ale policja szybko to opanowała
- Joachim Zajcew odzyskał szczątki wiły i zabrał je ze sobą w bliżej nieznanym kierunku

## Dark Future:

### Faza 1: Atak na kult

|---------------|-----------------|----------------|---------------|--------------|--------------------|
|  Frakcja      |  Earth/Physical |  Air/Knowledge |  Water/People |  Fire/Craft  |  Void/Supernatural |
|---------------|-----------------|----------------|---------------|--------------|--------------------|
| 'Kły Kaina'   |       05        |       05       |      13       |      04      |        00          |
| Policja       |       09        |       06       |      03       |      02      |        00          |
| Przaśnik      |       08        |       03       |      06       |      04      |        00          |
| Parafia       |       07        |       01       |      13       |      00      |        02          |
| Korzunio      |       07        |       10       |      02       |      01      |        00          |

- Kult pokazuje, że ma relikwię (fałszywą). Idzie Unlisted na YouTube. Rallying cry do lokalnych zwolenników.
..... BlackCult.create, use or show <something> which is a relic of the cult
..... Kły.A.uu, Kły.W.uu, Kły.F.uu
- Prasa (Agnieszka) atakuje Wiesława Rekina; daje powód policji by Rekina aresztować. Policja to robi. Rekin ucieka. Dojo zamknięte.
..... N/A
..... Kły.E.d, Kły.W.dd, Kły.F.dd
- Policja trasuje Rekina; mają powód do wezwania wsparcia - odnajdują go w lesie Gęsilockim.
..... CompetentPolice.find a trail leading to <someone> connected with <something>
..... Kły.E.dd, Policja.W.uu, Policja.F.uu
- Przaśnik chroni Agnieszkę przed napadem ze strony narwańca z 'Faim de Loup'. Przekazuje go policji.
..... LocalSmallMob.protect <someone> who belongs to them (or pays ;p)
..... Przaśnik.W.u, Kły.F.d, Policja.E.d, Policja.A.u
- Korzunio manipuluje dowodami; łączy wszystkie ostatnie wydarzenia z sektą i "znajduje świadków"
..... Cleaner.find <someone> to be a scapegoat in place of a real culprit
..... Kły.E.dd, Policja.F.u, Policja.A.uu

### Faza 2: Udaremniona apokalipsa

|---------------|-----------------|----------------|---------------|--------------|--------------------|
|  Frakcja      |  Earth/Physical |  Air/Knowledge |  Water/People |  Fire/Craft  |  Void/Supernatural |
|---------------|-----------------|----------------|---------------|--------------|--------------------|
| 'Kły Kaina'   |       00        |       07       |      13       |      03      |        00          |
| Policja       |       08        |       09       |      05       |      04      |        00          |
| Przaśnik      |       08        |       03       |      07       |      04      |        00          |
| Parafia       |       07        |       01       |      13       |      00      |        02          |
| Korzunio      |       07        |       10       |      02       |      01      |        00          |

- Kult buduje sieć baz mniejszych i większych w pobliskich miejscach
..... BlackCult.take root <somewhere>
..... Kły.W.ddddd
- Kult przygotowuje sieć ataków "koniec świata już blisko"; Korzunio ich zinfiltrował i redukuje ich działania z Zajcewem.
..... BlackCult.sow discord and anarchy among <location>, presenting themselves as the only force to keep peace.
..... Kły.W.dd, Kły.F.d, miasto.Panic.u
- Kult podpala Gęsilocką parafię.
..... BlackCult.sow discord and anarchy among <location>, presenting themselves as the only force to keep peace.
..... Kły.F.d, Kły.W.u, miasto.Panic.u
- Kult próbuje odbić narwańca od Agnieszki; powstrzymany przez policję oraz Przaśnika, ale poradził sobie; ciężkie straty po wszystkich stronach
..... BlackCult.send a group of cultists to overpower <someone> in order to get <something>
..... Kły.F.dd, Kły.W.dd, Kły.E.dd, Policja.E.dd, Policja.F.dd, Przaśnik.E.dd, Przaśnik.F.dd, miasto.Panic.uu
- Przaśnik 'udowadnia', że Kły mają fałszywą relikwię podstawiając 'starego satanistę'
..... LocalSmallMob.bribe / motivate <someone> into giving them <something> they want
..... Kły.A.dd, Kły.W.ddd
- Policja uderza w samo serce kultu w Gęsilocie
..... CompetentPolice.surround and move in to eliminate <someone> - a group / gang in their defensible position
..... Kły.E.ddd, Kły.W.ddddd, Kły.F.ddd; Wiesław Rekin na wolności

# Zasługi

* mag: Paulina Tarczyńska, która nie tylko uratowała księdza przed mordem rytualnym, ale też kilka osób przed * członkostwem w sekcie.
* czł: Maria Newa, która napisała dobry (i dla odmiany płatny!) artykuł z wydarzeń w Przodku.
* mag: Joachim Zajcew, który chciał zostawić sektę i Przodek w stanie wojny... aż Paulina tupnęła nogą. Współpracował dzielnie.
* czł: Krystian Korzunio, czyściciel który chciał ograniczyć ludzkie straty i który transportował Paulinę tam gdzie trzeba. Sojusznik Pauliny i 'agent' Joachima dowodzący akcją z wielu stron.
* czł: Wiesław Rekin, kultysta Kłów Kaina i wojownik Faim de Loup na wolności; śmiertelnie niebezpieczny acz ścigany.
* org: Ochrona Kobra Przodek, grupa chroniąca Agnieszkę Mariacką przed atakiem kultysty ze zlecenia Przaśnika.
* czł: Agnieszka Mariacka, demonstrująca przeszłość Wiesława Rekina i (ochroniona) ofiara narwańca (kultysty).
* czł: Grażyna Tuloz, dowodziła Szpitalem Gotyckim podczas wielkiego ataku kultu w Przodku. Powierniczka relikwii.
* czł: Tomasz Leżniak, ksiądz prawie złożony w ofierze przez kultystów; uratowany przez Paulinę.
* czł: Bartosz Bławatek, policjant po* magający w dowodzeniu akcją w Gęsilocie.
* czł: Hubert Rębski, antyterrorysta dowodzący takimi (militarno-antykultystycznymi) akcjami na Śląsku. Rozgromił siedzibę kultu.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. Główna biblioteka
                1. Gęsilot
                    1. Plebania gęsilocka
                        1. Kościół nowoczesny