---
layout: inwazja-konspekt
title:  "Jedno słowo prawdy za dużo"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)

### Chronologiczna

* [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)

### Logiczna

* [151007 - Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)](151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.

## Misja właściwa:
### Kontekst misji:

- Część magów Kurtyny chce współpracować z Hektorem; boją się tego, że Szlachta ich zmasakruje i w ich interesie jest Hektor.
- Wiktor Sowiński i Diana Weiner chcą współpracować z Hektorem; myślą, że Hektor stoi bardziej po ich stronie; Wiktor jest bardzo zainteresowany Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Edwin wie, że coś dziwnego się dzieje na linii Blakenbauerów; Vladlena czegoś szukała zbliżonego do aptoforma, ale im nic nie uciekło.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- tien Sasanka jest w rękach Silurii i powiedział jej o Aurelu Czarko.
- Oktawian Maus żąda sprawiedliwości dla Mileny Diakon, by dowiedzieć się gdzie jest Karradrael.
- Do Kopalina wraca Emilia, uzbrojona w nowe pomysły przeciwko Szlachcie.
- Hektor dostał dowody przeciwko Vladlenie od Szlachty z prośbą wniesienia oskarżenia o próbę Spustoszenia hipernetu.

### Plan graczy

- Siluria skupia się na wizycie u Triumwiratu Millennium w sprawie Mileny.
- Siluria chce się spotkać z Emilią.
- Hektor chce się spotkać z Emilią by zrozumieć, co chce Kurtyna osiągnąć.
- Hektor chce iść śladami wskazanymi przez Vladlenę.

### Faza 1: Martwa ręka Vladleny

Hektor zdecydował się dowiedzieć od Edwina wszystkiego na temat tego czego szuka Vladlena. Edwin siedzi sobie w pokoju i czyta książkę. Hektor zagadnął go o to, czego szukała Vladlena; odpowiedział, że się myliła. Szukała czegoś powiązanego z nim i Margaret, a to nie im coś uciekło. Hektor nie wchodzi w głąb tej dyskusji, bo zwyczajnie nie jest w stanie domyślić się działania rzeczy powiązanych z Tymotheusem...
Hektor poprosił jeszcze Edwina, by ten zapewnił bezpieczeństwo tien Murarzowi - ma nie być płaszczką i ma być "sojusznikiem". Edwin obiecał, że się tym zajmie.

Do Rezydencji przybył gość. Tatiana Zajcew. Chciała porozmawiać z Hektorem. Zostawiła wizytówkę; Hektor woli jej nie otwierać. Zaanonsował ją Jan Szczupak.
Tatiana - bardzo sympatyczna, choć typowa lekko agresywna Zajcewka. Zażądała wydania Vladleny i zdziwiła się, że ta jest aresztowana. Tatiana powiedziała, że jeśli 'lena jest aresztowana to prokuratura jest odpowiedzialna za wszystkie rzeczy od momentu aresztu. Ogólnie się posprzeczali i Tania zagroziła Hektorowi, że jeśli 'lenie coś się stało, ród wyciągnie konsekwencje. Hektor zauważył, że groźba wobec urzędu to poważna sprawa. Tania powiedziała, że urząd jej nie obchodzi - to groźba wobec osób którzy skrzywdzili przyjaciółkę. Vladlena miała dead man's switch i on odpalił.

Hektor poszedł do Edwina porozmawiać z nim od Vladlenie. Spytał, czy można ją wypuścić. No... nie. A czy zrobił jej coś niewłaściwego. Niech zdefiniuje "niewłaściwego". Hektor docisnął Edwina; ten się przyznał do następujących informacji:
- Tymotheus Blakenbauer jest w Kopalinie; to był jego aptoform który zwinęli magowie
- Vladlena znalazła aptoforma; gorzej, znalazła jakieś DALSZE eksperymenty Tymotheusa
- Edwin to wymazał i niestety Zajcewowie się tego dowiedzą i wyciągną to z Vladleny (Vladlena nie wie co znalazła)
- Jeśli w ciągu 48 godzin Vladlena nie opuści Rezydencji, info co badał / mazał Edwin zniknie
- Edwin dowiedział się o bardzo cennym Węźle.

Komunikat od Wiktora Sowińskiego; zaproszenie do rozmowy z Hektorem. Hektor podszedł do akwarium porozmawiać. Magowie Szlachty z radością pytają Hektora, czy dowody na Vladlenę są wystarczające. Tak, są... Hektor zacznie działania oskarżenia jeszcze dzisiaj.
Doskonale. Magowie Szlachty podziękowali i powiedzieli, że jakby Hektor ich potrzebował, są do dyspozycji.
Hektor westchnął...

Hektor poszedł porozmawiać z Ottonem. Powiedział mu, jak wygląda sytuacja. Otton powiedział Hektorowi co następuje:
- Blakenbauerowie nie zgłosili obecności Arazille.
- Tymotheus Blakenbauer używał aptoforma jako pułapkę na Arazille; chciał ją uwięzić (nie udałoby mu się zdaniem Ottona)
- Blakenbauerowie nie zgłosili obecności Tymotheusa.
- Arazille jest nieznana, w miejscu całkowicie nieznanym.
- Edwin dowiedział się od Mojry podczas jej regeneracji, że Arazille / Tymotheus działali jak działali.
- Otton uploadował Hektorowi wiedzę z Rezydencji na temat Arazille

Hektor powiedział, że chciałby rozwiązać sprawę w taki sposób:
- Zgłaszają Świecy obecność Tymotheusa oraz Arazille.
- Blakenbauerowie biorą na siebie walkę z Tymotheusem.
- Świeca jest poinformowana o Arazille.
- Blakenbauerowie biorą na siebie reperacje wobec Zajcewów.

Otton się zgodził na plan Hektora. Jedno słowo prawdy nie zabije Blakenbauerów. Koszt będzie ogromny i będzie to wykorzystane politycznie przeciwko nim, ale jest to coś, co warto zrobić w tej sytuacji...

Co by bardzo pomogło w tej sytuacji - Mojra na nogach. Czyli, muszą opóźnić politycznie wszystko po to, by Edwin zdążył postawić Mojrę; ona powinna być w stanie politycznie rozładować sytuację. Tylko ona może tak naprawdę coś z tym zrobić...

Siluria udała się do Triumwiratu Millennium. Z Silurią spotkała się Amanda Diakon. Siluria powiedziała jej o Milenie Diakon i o problemach Mileny. Amanda się bardzo zdziwiła - Diakoni są bardzo zżytym rodem i Diakoni nie zostawiają swoich członków w potrzebie. Amanda zaraz sprawdziła DBook i wyszło jej, ku jej wielkiemu zdziwieniu, że osoby bliskie Milenie nie zauważyły jej dziwnego zachowania. To znaczy, że albo są przekupione, albo ich percepcja jest naruszona, albo jest tam jakiś bardzo skuteczny homunkulus. Amanda zmarszczyła kształtną brew. To nie jest właściwe.

Amanda ustaliła co następuje - Siluria nie musi zatrzymywać Hektora. Niech Oktawian zażąda swojej sprawiedliwości. Niech ją dostanie; Diakoni upomną się o swoją siostrę. Amanda powiedziała Silurii, że Diakoni wiedzą bardzo wiele o Mausach i Karradraelu. Taka sytuacja ze zmieniającym się i odradzającym się Skażonym wzorem oznacza, że Milenie wstrzyknięto wzór Karradraela, a nie Mausa. Milena ma bezpośredni kontakt z Karradraelem, choć o tym nie wie. She is his vessel. Zdaniem Silurii: los gorszy od śmierci. Amanda powiedziała, że da się pomóc Milenie - trzeba ją sprzęc z kralothem. Karradrael nie potrafi zintegrować się z kralothem ani z kralotycznym magiem; powinno to pomóc Milenie...

Amanda podziękowała Silurii i vice versa. To był udany dzień.

### Faza 2: Dochodząc do Prawdy

Hektor siedzi na bujanym fotelu i myśli. Jedyna radosna rzecz w Rezydencji to Marcelin x Wanda i ogień na kominku...
Hektor i Siluria spotykają się w wyciszonym gabinecie w prokuraturze, jak zawsze. Hektor powiedział Silurii o Arazille - to co uploadował mu Otton ale bez danych specyficznych dla Blakenbauerów. Siluria DUUUUŻO się dowiedziała o Arazille. Hektor powiedział Silurii, że potrzebuje sojuszu przeciwko Arazille. Hektor potrzebuje czas. Siluria ostrzegła Hektora - pospolite ruszenie przeciwko Arazille po prostu nie zadziała. Zebranie ogromnych sił sprawi, że Arazille się dowie.
Hektor chce uruchomić wielkie rody - ale tak, by Arazille nic nie wiedziała. Precyzyjnie.

Sęk w tym, że nikt nie wie o obecności Arazille. Potrzebny jest "interpol". Ale... nie zadziała. Arazille jest stara i nieprawdopodobnie sprytna i mądra. I zawsze wraca. Sposób magów na pokonanie Arazille to zlokalizować Arazille i masakrować jej wyznawców jednego po drugim.

...Hektorowi to nie pasuje. Potrzebny jest inny sposób...
Inny sposób walki z Arazille - napuścić na nią Karradraela lub Iliusitiusa... też nie do końca Hektorowi nie pasuje.

### Interludium: Pytania

D: Dlaczego Kurtyna jest gotowa współpracować z Hektorem mimo, że utrzymuje kontakty ze Szlachtą?
K: Ponieważ to Emilia przejmuje stery a Emilia jest świetnym politykiem - jej zdaniem ma to sens.
K: Arazille to super ciężki politycznie temat. Czemu Siluria ma wejść w ten temat?
Ż: Arazille zawsze była najbardziej wroga Mausom i Diakonom - bo te dwa rody wykorzystują ludzi jako surowiec.
Ż: Dlaczego Szlachta z przyjemnością obróci się przeciwko Arazille? Co z tego mają?
K: W Szlachcie są też magowie którzy wykorzystują ludzi. Mentalnie są wrogami Arazille.

"Czyli Hektor jest najmniejszym wrogiem Arazille a to ON zbiera siły by ją zatrzymać? O_O" - Dust

K: Siluria jest bytem mocno politycznym. Czy Siluria powinna coś zrobić, komuś powiedzieć?
Ż: Amandzie tak; Diakoni boją się Arazille. Ale poza tym, nie ma obowiązku czy potrzeby.
D: Dlaczego Zajcewowie odroczą vendettę na Blakenbauerach?
Ż: Irina dostanie pomoc Ottona w projekcie "Żywy Aderialith" i seiras Benjamin zażąda od Zajcewów zaprzestania.

"Gdyby Diana i Vladlena dzieliły poglądy, byłyby najlepszymi przyjaciółkami" - Dust. - "Obie są gotowe do poświęceń, misja przede wszystkim"

### Faza 2: Dochodząc do Prawdy, cz.2

W prokuraturze czeka na Hektora i Silurię liścik od Emilii. Zaproszenie do siebie.
Siluria ubrała siebie i Hektora odpowiednio; chodzi o to, by pokazali się właściwie i od właściwej strony. Po to ma "Nieskończoną Garderobę Nicole Archer" jako czar.

Pojechali na spotkanie z Emilią, do jej domu. Pod Kopalinem. A kierowcą jest Dionizy Kret, znany zabójca magów. A na tylnym siedzeniu Głowica.
Spotkanie u Emilii. Tien Kołatka wygląda na delikatną i jest zdecydowanie nieuzbrojona, w eleganckich szatach.

Emilia powiedziała, jaki ma plan: była spotkać się z Tamarą i udało jej się dowiedzieć, że każdy magitech jaki Tamara buduje ma bardzo zaawansowane kody diagnostyczne. Jeśli Emilia dostanie w swoje ręce magitech noszony przez Wiktora Sowińskiego, będzie w stanie dowiedzieć się wszystkiego co Wiktor Sowiński przekazał, zrobił itp jak długo miał na sobie magitech. Włącznie z czasem, gdy był Spustoszony.

Siluria powiedziała, że Wiktor się nią zainteresował; spróbuje dostarczyć Emilii ten magitech na kilka godzin. Lub Emilię do magitecha. Jakoś to się rozwiąże ;-). Emilia zauważyła, że jest skłonna do poświęceń, ale tylko w ograniczonym stopniu. Hektor się ucieszył - dla odmiany ma do dyspozycji sensownego, zrównoważonego sojusznika. 

Emilia ostrzegła Hektora, że Zajcewowie to, co spotka w wyroku Vladlenę zastosują wobec Marcelina.

"Ty mówisz symetria. Hektor mówi mafia..."

W związku z tym Otton dał Hektorowi wytyczne - Vladlena może dostać po łapkach, ale ma nie stać jej się poważna krzywda. Hektor się "ucieszył", ale Ottona nie przeskoczy. Prokuratura sterowana odgórnie...

Hektor wprowadził akt oskarżenia przeciwko Vladlenie.
Tatiana się wycofała po rozkazach ojca.

### Spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- Mojra się budzi
- Mojra zgłasza, że Arazille jest na wolności
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Dark Future:
### Faza 1: Zagęszczenie temperatury

- Przychodzi Tatiana i żąda wydania Vladleny. Teraz. \| Tatiana.(present / accept a challenge)
- Szlachta żąda, by Hektor zadziałał przeciwko Vladlenie \| Wiktor.(present a devil's choice)

# Streszczenie

Tatiana zażądała wydania Vladleny. Hektor odmówił i przyjął odpowiedzialność za wszystko co się z Vladleną stało od momentu aresztowania. Edwin skasował Vladlenie pamięć o aptoformie i innych eksperymentach Tymotheusa. Plus, dowiedział się o Węźle Vladleny. Hektor rozpoczął proces Vladleny. Blakenbauerowie zgłosili obecność Arazille i Tymotheusa teraz i biorą Tymotheusa na siebie (Arazille na Świecę). A Siluria dowiedziała się od Amandy, że Milena Diakon ma wstrzykniętą nie matrycę Mausów a Karradraela; da się ją uratować przez sprzężenie z kralothem. Na spotkaniu z Emilią, czarodziejka rzuciła bombę - magitech Wiktora ma informacje z czasów jak Wiktor był Spustoszony i wszystko co kiedykolwiek zrobił. Czyli to może zniszczyć Szlachtę.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Osada Strażnicza
                        1. Dworek Emilii Kołatki

# Zasługi

* mag: Hektor Blakenbauer, który bawi się w politykę i próbuje zażegnać wojnę z Zajcewami... wyciągając Arazille na światło dzienne.
* mag: Siluria Diakon, która naświetliła sprawę Mileny Rodowi... oraz poszła na wojnę z Arazille (i Szlachtą).
* mag: Tatiana Zajcew, która stała się kamyczkiem, który pociągnął za sobą lawinę (współpracowała z Vladleną).
* mag: Vladlena Zajcew, która nawet w stazie robi problemy a jej plan awaryjny dał duże korzyści Zajcewom kosztem Blakenbauerów.
* mag: Amanda Diakon, jedna z Triumwiratu Rodu Diakonów; nie wiedziała o stanie Mileny Diakon i zajęła się tym, dlaczego nie wiedziała.
* mag: Milena Diakon, która - zgodnie z wiedzą Amandy - ma wstrzyknięty wzór Karradraela i której można pomóc przez integrację z kralothem.
* mag: Edwin Blakenbauer, który chciał dobrze... ale przez kaskadę działań Tatiany doprowadził prawie do wojny Blakenbauerów z Zajcewami.
* mag: Otton Blakenbauer, który z lekkim rozbawieniem ogląda, jak Hektor próbuje uratować ród z problemów w które wpędził ród Tymotheus.
* mag: Wiktor Sowiński, którego bardzo cieszy rozpoczęcie oskarżenia przeciwko Vladlenie i który pomoże Hektorowi jakby ten miał kłopoty.
* mag: Emilia Kołatka, wybitny polityk, która wraca z planem jak ujawnić czym jest Szlachta przez dostęp do kodów dostępu * magitecha Wiktora Sowińskiego.
vic: Arazille, której nie ma na misji, ale staje się idealnym chłopcem do bicia odwracającym uwagę od Blakenbauerów.
* mag: Tymotheus Blakenbauer, którego działania wykryła Vladlena, ukrywał przed nią Edwin i przez to Hektor (i cały ród) wpadł w kłopoty.
* mag: Benjamin Zajcew, który umówił się z Ottonem na koszty związane z atakiem na Vladlenę; Otton pomoże w projekcie "Żywy Aderialith" a 'lena tylko dostanie po łapkach.

# Actors:

- Emilia Kołatka (Silk Hiding Steel)
- Edwin Blakenbauer (Sentinel)
- Wiktor Sowiński (Dominator)
- Tatiana Zajcew (Brawler//Explorer)
- Tymotheus Blakenbauer (Dominator//Engineer)
- Magowie Kurtyny (Społeczność dookoła celebryty)