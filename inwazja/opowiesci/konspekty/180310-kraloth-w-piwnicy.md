---
layout: inwazja-konspekt
title:  "Kraloth w piwnicy"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170718 - Umarł z miłości (temp))](170718-umarl-z-milosci.html)

### Chronologiczna

* [170718 - Umarł z miłości (temp))](170718-umarl-z-milosci.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

**Wizja artysty**:

![Wizualny opis Opowieści](Materials/180399/story_kraloth_w_piwnicy_vision.jpg)

**Strony konfliktu, atuty i słabości**:

![Strony, atuty, słabości](Materials/180399/story_kraloth_w_piwnicy_2.png)

### Wątki konsumowane:

brak

## Punkt zerowy:

## Mapa logiczna Opowieści:

![Mapa Opowieści](Materials/180399/story_kraloth_w_piwnicy.png)

## Pytania Generatywne:

* Ż: Kto powie Silurii o problemie z potencjalnym ZŁYM KRALOTHEM i dlaczego właśnie jej?
* K: Judyta Maus. Bo dowiedziała się po kanale Mausowym a Siluria to jej bohaterka.

## Potencjalne pytania historyczne:

* Czy uda się zrzucić winę na Hralglanatha? (TAK: 3)
* Czy Hralglanathowi uda się wzmocnić kosztem ludzi? (TAK: 4)
* Czy w Paulinie Widoczek obudzi się krew viciniusa? (TAK: 5)
* Czy dojdzie do skandalu i eskalacji na linii Millennium - SŚ? (TAK: 5)

## Misja właściwa:

**Dzień 1**:

Judyta Maus skontaktowała się telefonicznie z Silurią, swoją bohaterką. Ostrzegła ją przed działaniem niedaleko Lasu Stu Kotów - jest tam gdzieś złowrogi kraloth. Elea Maus dostała w swoje ręce dwójkę ludzi - mężczyznę i kobietę. Jej nie udało się już Elei uratować, ale jego tak. Przyniósł ich terminus Świecy; Elea nie ma dla niego jeszcze informacji. Zapytana, czemu mówi o tym Silurii w ogóle, Judyta się zmieszała, ale odpowiedziała - wszyscy nienawidzą Mausów. Jak dalej tak pójdzie z tym kralothem, wszyscy znienawidzą też Diakonów, bo podejrzane jest Millennium.

Siluria zrozumiała ostrzeżenie. Judyta po prostu chce oszczędzić nienawiści. Naciśnięta, Judyta powiedziała jeszcze, że Millennium się zaczyna rozpychać w okolicy a Świeca wysłała tu nowego maga - Baltazara Sowińskiego, na którego mówią "Miecz na Mausów". Coś się dzieje w Lesie Stu Kotów...

Siluria szybko połączyła się z Netherią Diakon, by dopytać o kralotha działającego w tamtym terenie. Netheria zauważyła, że w Lesie Stu Kotów są bazy i ukryte bazy Millennium. Netheria zauważyła, że Millennium ma dość luźną strukturę więc coś się MOŻE dziać, ale ona o tym nic nie wie. A same kralothy nie zgładzały żadnych nowych kralothów czy działań - w czym nie szukały aktywnie.

Dobrze więc, czas porozmawiać z lekarką - z Eleą... ale tu przydałoby się jakieś wsparcie. Jakiś mag KADEMu. Najlepiej - niech to będzie Karolina Kupiec. Karolina porozmawiała z Silurią i spytała, skąd ta wie, że to kraloth? To może być Tajna Broń Millennium. Siluria się zdziwiła - o wielu rzeczach słyszała, ale nie słyszała o Tajnej Broni Millennium która emuluje kralothy. Karolina powiedziała, że to wie od swojej koleżanki w Millennium - jako, że Karolina jest terminuską, chce wpierw uwzględnić wszystkie możliwości. Jakkolwiek zdaniem Silurii może być to Acid Trip Karoliny, warto wziąć to pod uwagę - Karolina ma kontakty w "bardziej chemicznej części świata". Karolina poprosiła o godzinę na przygotowanie; Silurii to pasuje.

Czas na kontakt hipernetowy z Estrellą - lubianą terminuską Świecy. Siluria się zatem z nią połączyła. Estrella akurat była w środku małej imprezy; przeszła do ubikacji. Siluria spytała o to, który terminus operuje w tym terenie. Podobno podrzucił Elei rannych. Estrella powiedziała Silurii, że to nieco trudniejsze - Marek Kromlan podlega Baltazarowi Sowińskiemu a sam Baltazar nie podlega pod ten Kompleks Centralny, czyli nie jest pod rozkazami Anety Rainer. Aneta MOŻE wydać rozkaz i Baltazar MUSI posłuchać, ale Baltazar nie musi jej raportować. To specgrupa wysłana by pomóc. Świeca trochę rozprzestrzeniła siły by pomóc wszystkim Kompleksom.

Estrella spytała Silurię, czemu to ciekawe. Podobno polują na kralotha. Estrella się zdziwiła - kraloth, tu, teraz? Estrella jeszcze dodała Silurii - Kromlan jest negatywnie nastawiony ALBO do Millennium ALBO do Diakonów. Estrella nie wie czemu, ale tyle udało jej się zauważyć. Podobno Kromlan jako wsparcie ma dwóch konstruminusów, jest tam samodzielnie. Świeca zwyczajnie nie ma sił na pełne skrzydło. Był kiedyś magiem Szlachty.

Mając powyższe informacje i przygotowaną Karolinę, Siluria jest gotowa do działania. Czas wziąć Karolinę i jedziemy do Piroga Dolnego. Czas spotkać się z Eleą. Ta pracuje w prywatnej przychodni charytatywnej Larent, należącej do lokalnego biznesmena, Dariusza Larenta w ramach CSR. Elea Maus jest dokładnie tą samą osobą jaką Siluria pamięta - wesoła chichotka. Elea powiedziała Silurii, że kobieta zginęła zabita przez mężczyznę. Kobieta była ekstatyczna bólem (skażenie kralotyczne), mężczyzna ma deathwish i chce zadawać rany. Chwilowo potrzebuje do życia kralotycznych soków, ale Elea nie jest w stanie mu pomóc - zakłóci pamięć a terminus żąda by ona jak najszybciej dowiedziała się jak najwięcej o samym kralocie. Ale Elea nie jest neuronautką! Więc Elea znalazła jakieś urywki w jego pamięci; terminus nie do końca jej wierzy więc ściąga innego neuronautę. Ma być za dwa dni, ale człowiek - jakkolwiek przetrwa - będzie praktycznie nie do odratowania potem.

Elea poprosiła Silurię, czy nie można przekonać terminusa - bo pacjent (Grzegorz Nocniarz) umrze. Siluria WIE, że Kromlan ma coś do Diakonów czy Millennium, ale jest tu też Karolina - i ona może negocjować w imieniu Elei. A Siluria działa w tle. Pretekst dla Karoliny - jest terminuską, chce poczyścić teren i trafiła na taką sytuację.

* Marek: Trudny (10), antyDiakon (Z), antySiluria (Z), musi się wykazać, nieufny Elei (Z) -> 12
* Siluria: sprzęg Karolina-Siluria, Marek nie chce krzywdy, nasz neuronauta*2, kształtowanie otoczenia, manipulacja przez słabość -> +8+8->18 (plus surowiec)

Kromlan się zgodził, jak długo neuronauta KADEMu nie był czynnie zaangażowany w Wojnę Karradraela po stronie Millennium. Siluria się radośnie zgodziła - o Edwinie można wiele powiedzieć, ale nie to :D. Więc - ściągnęła jak najszybciej Edwina do Elei.

* Stare dobre czasy... znowu spotykamy się w tej grupie - Edwin, z wisielczym uśmiechem
* Przynajmniej nie przy Irytce - Elea, z uśmiechem
* Nic a nic się nie wstydzisz - Edwin, z niedowierzaniem
* Nie mam czego - Elea, nie przestając się uśmiechać - Chroniłam wszystkich których mogłam chronić tak dalece jak mogłam.
* To akurat prawda... - Siluria, cicho

Edwin zabrał się do roboty. Siluria z uśmiechem patrzyła, jak Edwin wspierany przez Eleę i Karolinę skupił się na ratowaniu człowieka i na ekstrakcji wspomnień by złożyć to do kupy. Po chwili Edwin przerwał i powiedział, że zajmie mu to kilka godzin. Najpewniej do jutra. Da się go uratować i da się wyciągnąć informacje z jego pamięci. Już coś ma - jakąś bibliotekę, zaplecze biblioteki. Jakąś erotyczną sektę? Kilka osób. I kraloth. Coś Edwinowi jednak nie pasuje w tym wszystkim - weźmie pacjenta na KADEM. Elea zaoponowała - ona nie pojedzie na KADEM. Edwin zauważył, że ona nie jest mu tam potrzebna. Ok. Edwin z Karoliną się oddalili na godzinkę by przenieść pacjenta na KADEM...

Siluria dostała od Edwina też informacje odnośnie miejsc o których Nocniarz myślał. Głównym z tych miejsc jest Klub Kolt w Jodłowcu. On ogólnie mieszkał w Jodłowcu. Kobieta nazywała się Klaudia Czymak, była jego kochanką. Zgodnie z jego pamięcią - tego co udało się Edwinowi wydobyć na szybko - często spotykali się w kościele neoromańskim w Czeliminie. I - co ciekawe - z jego wspomnień wynikało że to ON chciał ją tam (do kościoła) zaprowadzić a ona się opierała. W czym Edwin mówi, że tu dużo nie pasuje.

Silurii bardzo nie pasuje ta opowieść. Arazille? Arazille puściłaby istotę sprzężoną z kralothem? Jest bardzo osłabiona, ale nie tak. Czyli cała ta historia - zdaniem Silurii - na razie nie ma sensu. Albo to nie kraloth albo to nie Czelimin. Może ten kraloth podkłada ludziom wspomnienia, ale...

Siluria wsiadła w samochód i pojechała do klubu Kolt. Tam władza Arazille nie sięga (poza Czeliminem), a bez rozpoznania wiedzy o Grzegorzu Nocniarzu cała ta historia może nie być możliwa do wyjaśnienia.

Klub Kolt. Od razu spojrzenia wszystkich zwróciły się ku pięknej Silurii. Byłaby rozczarowana, gdyby było inaczej. Ogólnie, nikt nie jest nachalny, ale atmosfera jest pozytywna. Siluria przysiadła przy barze, wyszukała receptywnego stałego bywalca i zagadała. A dokładniej - pozwoliła sobie pomóc w grze arcade. I wypytała o Grzegorza.

* Grzegorz był zakochany w Klaudii a ona w nim - ale PÓŹNIEJ
* Tajemnicza "dama w czerwonym" pomogła Grzegorzowi ją przekonać. Nie tak tajemnicza, bo to żona jubilera - z Miłejki.
* Grzegorz przed wizytą "damy w czerwonym" łaził po kościołach z Klaudią. Sporo. Ale nie był religijny.
* Grzegorz miał fazy "rozwalić wszystkie kościoły" i "musimy zobaczyć kolejny kościół"
* Lokalny ruch feministyczny "Śmierć Mężczyznom" - do niego należała Klaudia.
* A jednak Grzegorz dał radę wyrwać ze Śmierci Mężczyznom Klaudię dla siebie, hehehehe.

Siluria się zmyła. I - późnym już wieczorem - dostała jeszcze wiadomość (niespodziewaną) od Edwina. Edwin potrzebuje 24-48h, bo siedzi z pacjentem. Ogólnie: problem jest taki, że Ogniste Niedźwiedzie zgodnie z tym co powiedział Edwinowi Bryś mają zamiar iść z kimś się bić. A Edwin czuje się za nich trochę odpowiedzialny. Potrzebuje chwili... Edwin dał na nich namiar - Mordownia Arena Wojny, w Pirogu Dolnym. Edwin ostrzegł Silurię też przed Pauliną Widoczek z krwią viciniusa.

Jeszcze tej nocy Siluria poszła do Mordowni Areny Wojny. Sama. Ubrana jest klubowo, mały push-up, w lekko ambitniejszym makijażu itp. Co chce, to dostanie.

Weszła do Mordowni. Tam łupie chamskie kombo techno-hiphop. Jest "nadziemie" gdzie Siluria ma dostęp, gdzie są damy do towarzystwa i silni faceci i podziemia, gdzie jest "akcja". Ale gatekeeperem jest Franciszek Knur, barman. Siluria podeszła do niego i poprosiła - ulubione dla Artura, dla Pauliny, piwo dla wszystkich i klucz na dół dla mnie. Knur się uśmiechnął. Jakie Siluria ma uprawnienia by zejść na dół? Siluria pokazała mu piersi. Knur zmacał, ocenił, że prawdziwe - po czym dał jej klucz mrucząc, że tyle jest "feminazistek" w okolicy, że nie ma szans. Siluria właśnie udowodniła, że nie jest jedną z "nich".

Siluria zeszła na dół i spotkała nietypowy widok - na arenie stoi Artur Bryś, niechętny, a po drugiej stronie Paulina Widoczek. Bryś chroni przed Widoczek jakąś dziewczynę (co dla kogolwiek znającego Brysia jest absurdem). Siluria nawiązała kontakt z załamanym szefem Niedźwiedzi, Bruniewiczem, i usłyszała co się dzieje - Bruniewicz i Niedźwiedzie mają "fajną dziołchę" na zapleczu. Paulina jest zazdrosna i zdecydowała się ją uciemiężyć. Bryś, niechętnie, zdecydował się ją bronić. Absurd.

Siluria przekonała Bruniewicza, by ów zaprowadził ją na to zaplecze. Tam jest ładnie spętana Diakonka, gotowa na wiele. Siluria dała do zrozumienia Bruniewiczowi, że podoba jej się ta sytuacja i uspokoiła szefa Niedźwiedzi. Gdy Diakonka pisnęła, dał jej "plasterek". Gdy ów wyszedł zobaczyć kto wygrał, Siluria zwinęła kilka plasterków i porozmawiała z kuzynką - ta jest tu, bo chce. Nie do końca kontaktuje, nazwała Silurię "Krystalią" i powiedziała, że koralowce mają się nieźle. Co dla Silurii brzmi bardzo Krystaliowato.

Okazało się, że wygrała Paulina. Bryś jednak dał jej wygrać po tym, jak upewnił się, że nie zrobi Diakonce trwałej krzywdy. Paulina zaczęła od policzkowania i bicia czarodziejki, która - zwłaszcza po plasterku - przyjęła to baaaaardzo radośnie. Co więcej, jej pot zaczął mieć odpowiednie własności, więc zaczęło działać to też na samą Paulinę. Siluria, chcąc rozkręcić towarzystwo, walnęła plasterkiem w Brunowicza i w Brysia i rozpoczęła porządną orgię...

Myślała, że jej się upiecze (szybko wstanie itp), ale w tym plasterku było coś kralotycznego. Także utraciła kontrolę do rana.

**Dzień 2**

Siluria się obudziła w jednej splątanej cielesnej masie. Wyspana i w świetnej formie. Oprócz niej zregenerowały się jeszcze: Bójka i Paulina. Bójka w super humorze; gdy Paulina ją odwiązała, ta wdzięcznie plasnęła na ziemię z szerokim bananem. Tymczasem Paulina... cała podłamana. Wyraźnie, jakkolwiek się jej podobało, żałuje. Bójka zaznaczyła, że Paulina wiele się od niej nie różni, co Paulinę silnie ubodło.

Jeszcze pod prysznicem Bójka i Siluria porozmawiały - plasterki pochodzą od Krystalii (zrozumiałe...), Bójka upewniała się, że Siluria nie dała ich człowiekowi. Gdy Siluria potwierdziła, że dostał Bryś i Bruniewicz, Bójka obiecała ich wyczyścić, bo te plasterki mogą wyżreć mózg człowiekowi. Coś silnie kralotycznego. Siluria poprosiła, by Bójka uniemożliwiła im działanie. Bójka obiecała, że to zrobi. Może i miewa wyżarty mózg plasterkami, ale pamięta, że Bruniewicz coś mówił o tym, że "laska w czerwonym" podrywa mu dziewczynę i trzeba coś z tym zrobić.

Oki. Znowu "czerwony". Siluria zastrzygła uszami. Wyszła więc z Mordowni wiedząc, że ma już zawsze do niej dostęp...

Edwin dostał od Silurii wiadomość, że zadanie zostało wykonane w Siluriowym stylu. Dostała też odpowiedź od Blakenbauera - wizyta w kościele Czelimina była prawdą. Owa "dama w czerwonym" skłoniła Grzegorza by tam zaprowadził Klaudię, pod groźbą noża. Bo tam wyleczy się z opętania. Nowa hipoteza Edwina - kapłanka Arazille? Edwin nadal grzebie w tej pamięci, ale wszystko wskazuje na to, że kraloth był w bibliotece w Glinie Żółtym. W kościele była trójka - Grzegorz, Klaudia, "dama w czerwieni".

Siluria ruszyła szukać danych o tej niewieście. Internet, okolice Czelimina, search images... kim ty jesteś...? (vs 5 -> SS). Siluria znalazła delikwentkę - to Maria Przysiadek, żona jubilera i faktyczna piękność. Też: skandalistka w stylu Silurii; piękna i chętna, jak to mówią. Mężowi to pasuje. W czym - nie zawsze tak było. Jeszcze półtora roku temu oni kwestowali na rzecz zwalczania raka Marii. Po tym jak jej się polepszyło, zmieniła też podejście wraz z mężem. Też była zaplątana z mężem w sprawy związane ze Skubnym, by zgromadzić pieniądze na leczenie.

Siluria pojechała do Miłejki, gdzie jest dom Marii. Po drodze jednak połączyła się z lokalnym terminusem - Markiem Kromlanem. Marek potraktował komunikat Silurii eleganckim lodem. Siluria powiedziała mu o Marii Przysiadek. On odbił, że ją znalazł innymi drogami - kraloth wykończył jej kabałę w Bibliotece Czerwonej. Tam został śluz kralotha, ludzie zniewoleni przez kralotha itp. Zostawił tam konstruminusa na straży i ich ustabilizował. Siluria zaproponowała, że KADEM ich wyleczy. Marek się zgodził - nie widzi, żeby ze wszystkich gildii za tym stał KADEM. Lekko złagodniał. Siluria też się ociepliła nieznacznie, on chce tym ludziom pomóc.

Siluria zaproponowała Markowi, że ona pojedzie i zda mu relację i powie co i jak (8vs7->S). Marek się zgodził. Wyraźnie niechętnie, ale jest zajęty też innymi rzeczami i źródłami informacji. Zwyczajnie magów Świecy w okolicy jest za mało. Jak długo Siluria zda mu relacje i powie co się dzieje, Markowi to pasuje. Siluria tak prowadzi rozmowę, by Marek był święcie przekonany, że to on dowodzi tą akcją.

Miłejka. Siluria odwiedziła Miłejkę. Dom jubilera wygląda na taki... chroniony. Domowarsztat. Siluria poobserwowała chwilę domostwo. Wyszło jej wyraźnie coś ciekawego - ten dom jest gorzej zabezpieczony niż mógłby być. On ma mniej zabezpieczeń niż okoliczne domy. A na pewno ma więcej wartości. Tak jakby zapraszał złodziei. Widać tam też zabawki dla nastolatka. Więc mamy dom z nastolatkiem, bogata rodzina, podatny na włamania. Nieco nietypowe.

Siluria zdecydowała się poobserwować dom by zobaczyć Marię i na nią się natknąć; nie chce rozmawiać w domu Marii. Gdy Maria pojechała samochodem, Siluria pojechała za nią. Maria pojechała do Jodłowca. Siluria zaczepiła ją niedaleko Kolta; podobno z Marią można się dobrze zabawić. Ta się uśmiechnęła i zaprosiła Silurię do hotelu. Siluria aż się zdziwiła, ale zrozumiała - Maria uważa to za jakiś żart czy "dare". Faktycznie, w hotelu doszło do dobrej zabawy - Siluria prowadziła a Marii się to bardzo podobało. Przy okazji Siluria się upewniła, że Maria ma podniesione parametry erotyczne, co wskazuje na Diakona czy kralotha. W czym kraloth niszczy umysł, więc nie pasuje...

Czas przesłuchać Marię. Dwie zrelaksowane, nagie dziewczyny. 

W tych warunkach Siluria nie miała wielu kłopotów, by wyciągnąć z Marii kluczowych informacji:

* Maria nie zawsze taka była. Odkąd dostała leki na raka, jej się poprawiło. Ma leki i je ciągle zażywa. (Siluria nie rozumie co to za leki).
* Jej mąż to akceptuje. Jej syn... cóż, nie jest mu łatwo. Ale nie mają innego wyjścia.
* Maria jest w stanie pomóc wielu ludziom, bo ma... właściwości lecznicze. Siluria aż zastrzygła uszami.
* Maria nie jest w stanie pomóc swojemu przyjacielowi, więc spytała, czy Siluria ma właściwości lecznicze.

Siluria? Lecznicze? Ok, Siluria złapała się tej okazji i wyszło na to, że może pojechać do Marii i pomóc jej przyjacielowi - a przynajmniej zobaczyć. Pojechały więc; Siluria przy okazji wysłała wiadomość do Karoliny i poprosiła o ostrożne wsparcie. Karolina będzie na pozycji.

Siluria weszła do domu. Mąż Marii, Szczepan, był nieco zaskoczony, że Maria chce zejść z Silurią do piwnicy ale się zgodził gdy Siluria powiedziała, że nie ma nic przeciw trójkącikowi. On też dołączy, choć wyraźnie się zestresował.

W piwnicy jest kraloth. Niewielki i uszkodzony. Wystrzelił macki w kierunku Silurii. Po krótkim szoku, Siluria zaakceptowała sytuację i stwierdziła, że spróbuje to wykorzystać. 

* **Hralglanath**
* Cel: asymilacja energii
* Trwałość: 3 (lekkie, trwałe, terminalne)
* Poziom: Trudny (10)
* Siły (atak): 
    * ekstadefiler (3)
    * biosynteza (3)
* Siły (obrona): 
    * ekstadefiler (3)
    * kralotyczny umysł (3)
* Słabości: 
    * corrupted mind
    * osłabiony
    * nie chce skrzywdzić Marii

Siluria nie chce się po prostu ODERWAĆ. Ona chce POKONAĆ kralotha. Wyłączyć go, a jak się da, zdominować. Kraloth nie chce po prostu nasycić się Silurią - on chce ją zaadaptować do siebie i doszczętnie zniewolić, użyć jej ciała do naprawy swojego umysłu.

Kraloth wystrzelił swoje macki w kierunku Silurii, ku wielkiemu zaskoczeniu Marii. Kraloth zaczął rozrywać ubranie Silurii na kawałki, tracąc przewagę zaskoczenia. Kiepski pancerz, ale pancerz. Siluria poczuła dobrze jej znaną emanację magii krwi dochodzącą ze strony viciniusa. Kraloth zaczął przygotowywać się do asymilacji; Siluria korzystając z okazji rzuciła szybko zaklęcie Preekstazy. Odmówienie kralothowi zasobu. If she cannot come, he cannot feed...

Zwykły test Trudny na koncentrację; w czym +2 za hermetyk. Skonfliktowany sukces. Udało jej się rzucić preekstazę, acz zaklęcie to zostało Skażone obecnością kralotha. Zaklęcie poszło szeroko, srebro rozpaliło dom, ale fala poooooszła. Maria i Szczepan też zostali trafieni. Kraloth spawnował dodatkowe macki i objął Marię i Szczepana, acz wyraźnie się zawahał. Siluria nadal jest głównym celem. Maria i Szczepan zaczęli błagać kralotha o wyzwolenie, ale kraloth... nie. Kraloth nie zrobił tego. Nie ruszył w ich kierunku, acz spija z nich energię.

Kraloth wniknął w Silurię z ogromną mocą, głodny jak nigdy. Ale nawiązuje też połączenie mentalne. Kraloth ma uszkodzony, ciężko uszkodzony umysł. Ale próbuje się odbudować układem nerwowym maga... czego bez orgazmu Silurii nie zrobi. Kraloth 13, Siluria 14. Skonfliktowany. Siluria zostaje Naruszona. Kraloth jednak też musi rotować biofluidy, co nie jest dla niego łatwe w tym stanie (-1, -1).

Siluria zdecydowała się błyskawicznie użyć surowca - bardzo ostra przyprawa od Inferni. Prosto w korpus, by cierpiał, by się bał. Siluria dominatrix z pieprzem kontra mentalnie uszkodzony kraloth na instynktahch. Kraloth się rzuca, Siluria przez pysk. 16v8 -> sukces. Kraloth zapiszczał; skupił się na defensywnej produkcji śluzu i płynów. Upuścił Silurię, Marię i Szczepana. Maria krzyknęła na Silurię, ale niewiele może zrobić. 

Kraloth został ciężko skrzywdzony za próbę ataku. Siluria zajęła się usunięciem jego bólu tak, by skojarzenie było "jestem miły, jest mi dobrze" oraz "pies do pana - pan mnie głaszcze, jest mi dobrze". Osoba z sygnałem. Kraloth jest nieufny i ucierpiał, plus ma uszkodzony umysł (15). Siluria zdecydowała się wzmocnić zaklęciem Amnestii Diakon; lekko przemodelować go, by był poddany Silurii i trochę ukoić jego ból. Niestety, Siluria jest Naruszona i jest w preekstazie. Zamiast zaklęcia użyła plasterka jakiego zwinęła Bójce (14). Skonfliktowany sukces. Kraloth zwinął się w ekstatycznej agonii dominacji "większego kralotha". Jednak to sprawiło, że stał się agresywniejszy - chce przetrwać i wygrać.

Siluria zdecydowała się na nie korzystanie z okazji. Ona nie chce po prostu "odejść". Ona chce ZDOMINOWAĆ kralotha, nawet takiego z uszkodzonym umysłem. Chwała! A Hralglanath zaczął się bronić przed większym kralothem, zaczął się transformować i przebudowywać. A Siluria się strasznie męczy przez preekstazę, nie mówiąc już o tych wszystkich ludziach! (Karolina is on it). Siluria nie jest w stanie nawet odebrać sygnałów od Karoliny i nie wie co nadaje.

Siluria wie, że kralothy to crucio-ekstadefilerzy. Crucio to pieprz, eksta to plasterek. Siluria zdecydowała się ponowić sygnał - plasterek ORAZ pieprz, razem. Zmusić kralotha do poddania się. Zgodnie z rytuałem kralothów. W to samo miejsce jedno i drugie. Hralglanath CHCE BYĆ WOLNY, nie chce się podporządkować żadnemu kralothowi - więc walczy ponad normalne działanie kralotha (14v12->S). Kraloth jest "wstrząśnięty" - nie dość, że ranny to jeszcze w obecności większej mocy niż on. Siluria wie, że w tym momencie kraloth się powinien poddać, zwłaszcza, że większy kraloth może pomóc mu naprawić umysł.

Ten się nie poddał. Ten zaatakował, mackami krwi i przemocy. Mającymi rozszarpać Silurię na kawałki. Atak z zaskoczenia, niespodziewany, bo konsekwencja w świecie kralothów to byłaby śmierć. 10 vs 8 -> SS. Kraloth posiekał Silurię i zasymilował jej energię jako cruciodefiler. Ale - ku zaskoczeniu Silurii - kraloth nie potrafi zasymilować jej energii. Wyraźnie się zakrztusił. Klasyczny WTF dla Silurii, ale przy tej ranie Silurii połączyła się preekstaza z cierpieniem. Siluria odpływa, krwawi i jest wniebowzięta a kraloth się krztusi i nie umie asymilować. Dodajmy do tego wrzaski ludzi... zwłaszcza, że kraloth nie był szczególnie precyzyjny.

Siluria zdecydowała się skorzystać z okazji póki kraloth jeszcze nie odbudował sytuacji, aczkolwiek jest slightly airheaded. Siluria podeszła do zamrażarki w piwnicy, wyrwała kabel i walnęła nim w kralotha, by dupnąć go prądem (konflikt Zwykły, z uwagi na stan Silurii). Skonfliktowany. Kralotha DUPNĘŁO. Siluria niestety po walce będzie musiała odpłynąć. Kraloth jest lekko ogłuszony, wystarczająco, by Siluria miała chwilę...

Siluria zdecydowała się na jeszcze jeden atak - plasterek plus przyprawy. I wzięła wór. Chce wsadzić kralotha do wora, zasymulować początek asymilacji z nadzieją, że kraloth nie zorientuje się, że nie ma enzymów - po to pieprz i plasterek, powinien być już dość ogłupiony. Udało jej się - kraloth zaczął zachowywać się podlegle. Co prawda NADAL nie rozumie komend Silurii, ale odpełzł do swojego kąta.

A Siluria odpłynęła... zwolniła preekstazę i dała Karolinie JAKIŚ sygnał, bo nie wie już co wysłała. A starała się wysłać "nie daj mu zabić pełza".

* Kromlan jest na miejscu, jest tu też Karolina go kontrująca
* Siluria jest nieprzytomna, ranna i ekstatyczna
* Pełz jest ciężko uszkodzony i podległy Silurii
* Ludzie są wyczerpani i naruszono Maskaradę lokalnie
* Nikt nie zginął, acz są ranni

![Rysunek konfliktu](Materials/180399/story_kraloth_w_piwnicy_3.png)

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (18)
    1. Czy uda się zrzucić winę na Hralglanatha?: TAK (3)
    1. Czy Hralglanathowi uda się wzmocnić kosztem ludzi?: TAK (4)
    1. Czy dojdzie do skandalu i eskalacji na linii Millennium - SŚ?: NIE, KADEM - SŚ (5)
    1. Marek Kromlan ma zdecydowanie złą opinię o Silurii teraz jak ją poznał (3)
    1. Maria dalej potrzebuje leków kralotycznych; bez nich umrze (3)
1. Kić (10)
    1. Karolina ewakuowała pełza na KADEM (2)
    1. Osoby, które powinny wiedzieć wiedzą, że Siluria zdominowała kralotha (3)
    1. Kraloth jest nie do odratowania, ale nadaje się na biosyntezator na KADEMie (3)
    1. Kraloth - biosyntezator trafia jako zasób Silurii (2)

# Streszczenie

Siluria zajęła się sprawą złowrogiego kralotha, który ma na koncie kilka martwych ludzi w Lesie Stu Kotów. Tematem zajmuje się wrogi jej terminus Świecy - Marek Kromlan. Kromlan jest kompetentny i bardziej zależy mu na ratowaniu niż tępieniu Silurii. Siluria odkryła, że w Mordowni bawi się Diakonka i ktoś prowokuje Niedźwiedzie do ataku na jubilera. Tym kimś okazała się żona jubilera, aliantka kralotha (acz nie zniewolona). Siluria pokonała tego kralotha a Karolina ewakuowała go nim pojawiła się Świeca.

# Progresja

* Siluria Diakon: sławna w odpowiednich obszarach jako osoba, która zdominowała kralotha przez łóżko.
* Siluria Diakon: ma dostęp do biosyntezatora kralotycznego (kiedyś: Hralglanatha).
* Hralglanath: KIA. Jego ciało to biosyntezator dla Silurii.
* Hralglanath: obwiniony o wszystkie kralotopodobne wydarzenia w okolicy od pewnego czasu.
* Maria Przysiadek: potrzebuje dostępu do leków kralotycznych, albo umrze.
* Maria Przysiadek: Dotknięta Wolnością Arazille; wie, że w Czeliminie jest bezpieczna. Nie wie czemu to wie.

## Frakcji

* KADEM: dostęp do biosyntezatora kralotycznego (kiedyś: Hralglanatha), normalnie w rękach Silurii Diakon
* KADEM: dostęp do wszystkich ofiar kralotha (Hralglanatha).

# Zasługi

* mag: Siluria Diakon, zdominowała kralotha z pozycji podrzędnej. Uratowała ofiarę tego kralotha. Znalazła żonę jubilera przed terminusem Świecy. Zrobiła orgię w Mordowni. Ogólnie, niegroźna.
* mag: Judyta Maus, poważnie zaniepokojona tym, że na terenie operuje groźny terminus Świecy. Ostrzegła Silurię, że gdzieś łazi kraloth, bo nie chce by Diakoni cierpieli jak Mausowie.
* mag: Elea Maus, teraz lekarka w przychodni w Kotach. Keeps low profile. Nic się nie zmieniła, niczego nie żałuje. Ratuje ludzi, z pomocą polityczną Silurii.
* mag: Baltazar Sowiński, zwany "Miecz na Mausów". Judyta Maus się go śmiertelnie boi. Wielki nieobecny, acz wspiera Marka Kromlana konstruminusami.
* mag: Netheria Diakon, która nic nie wie o dzikich kralothach w okolicach Kotów. Zainteresowana tematem nie mniej niż Siluria.
* mag: Karolina Kupiec, tym razem w roli dyplomaty i łagodzicielki konfliktów. Również frontman wobec Marka Kromlana. W ostatniej chwili ewakuowała kralotha na KADEM.
* mag: Estrella Diakon, nie mówiąca nic tajnego, ale pomocna by Siluria mogła zorientować się co się dzieje. Mała mapa terenu i stanu politycznego Świecy.
* mag: Marek Kromlan, kompetentny terminus który bardziej chce ratować magów i ludzi niż angażować się w walkę z Silurią. Szukał kralotha; Siluria znalazła go pierwsza.
* czł: Grzegorz Nocniarz, pacjent Elei; uratowany przez Edwina przed szaleństwem. Coś tam z Klaudią (nie żyje) i Arazille w Czeliminie. Skrzywdzony przez kralotha.
* mag: Edwin Blakenbauer, dalej czuje skutki Wojny Karradraela. Ma pewne problemy z Eleą, ale pacjent jest pierwszy. Ratuje Grzegorza Nocniarza od Świecy i kralotycznych wpływów. Tymczasowy neuronauta. Nadal troszczy się o eks-swoich ludzi (Bryś).
* czł: Franciszek Knur, nadal barman w Mordowni Arenie Wojny; Siluria pokazała mu biust, to ją wpuścił. Uczestniczył w Wielkiej Orgii.
* czł: Artur Bryś, wbrew sobie, bronił Diakonki na arenie Mordowni przed dziewczyną szefa. Dostał plasterek od Silurii, co poprawiło mu "humor".
* czł: Szczepan Przysiadek, mąż Marii, jubiler mający w domu sporo srebra. Akceptuje styl życia żony i kralotha w piwnicy. Nie jest kralothbound, acz jest sympatykiem.
* vic: Maria Przysiadek, "dama w czerwonym", żona jubilera i kralothbound. Sojuszniczka i przyjaciółka kralotha, nie jego niewolnica. Celuje w ratowanie kralotha, za cenę innych (?).
* czł: Roman Bruniewicz, szef Niedźwiedzi. Skonfliktowany między Diakonką w więzach a własną dziewczyną. Plasterek rozwiązał jego wątpliwości i obawy ;-).
* vic: Paulina Widoczek, piekielnie zazdrosna o Bójkę Diakon w więzach; niestety dla niej, efekty Diakonki wpływają też na nią. Skończyła w Wielkiej Orgii.
* mag: Krystalia Diakon, cień Diakonki (nieobecna), która robi eksperymenty z koralowcami i plasterkami. Wspiera Bójkę Diakon w jej eskapadach w Mordowni.
* mag: Bójka Diakon, "rezydentka" Mordowni i lokalna lalka. Praktycznie na ciągłym haju. Bondage fetish, przynajmniej na razie. Wspiera Krystalię w jej eksperymentach. Dzieli się z Silurią plasterkami. Too kinky to torture.
* vic: Hralglanath, nietypowy kraloth. Ciężko mentalnie uszkodzony, nie przetrwał spotkania z Silurią. KIA.

# Plany

* Siluria Diakon: dowiedzieć się o historii Hralglanatha i Marii Przysiadek. O co w tym wszystkim chodziło?
* Siluria Diakon: dowiedzieć się, o co chodzi Markowi Kromlanowi? Czemu tak nie lubi Silurii? A czemu Diakonów? I Mausów (dobra, to jasne)?

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Las Stu Kotów
                1. Okolice Piroga Dolnego, gdzie znaleziono Klaudię i Grzegorza. Klaudia już nie żyła, Grzegorza doprowadzono na czas do Elei Maus.
            1. Powiat Czelimiński
                1. Glin Żółty
                    1. Biblioteka Czerwona, miejsce, które służyło za małą orgietkownię. Zniszczone przez kralotha. Ofiary odratowane przez Kromlana (trafiły do Edwina).
                1. Miłejka
                    1. Warsztat Jubilerski, gdzie mieszka Szczepan, Maria i Czesław. Ma mnóstwo srebra a w piwnicy - kralotha.
                1. Jodłowiec
                    1. Klub Kolt, gdzie odbywają się ciekawe rozmowy o kobietach - zwłaszcza, gdy zacznie je Siluria.
                    1. Cityhotel Jodłowiec, miejsce, do którego Maria Przysiadek ma zawsze pełen dostęp, nawet na kilka godzin z Silurią.
                1. Czelimin
                    1. Kościół Neoromański, gdzie podobno Grzegorz zabrał Klaudię z pomocą Marii... by spotkała Arazille? Co nie ma sensu.
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Pylisko
                        1. Przychodnia Larent, aktualne miejsce pracy Elei Maus. Niewielka prywatnie dotowana przychodnia.
                        1. Mordownia Arena Wojny, gdzie w podziemiach rozgrywają się dantejskie sceny i jest arena wojny.

# Czas

* Opóźnienie: 30
* Dni: 3

# Wątki kampanii

nieistotne

# Przeciwnicy

## XXX
### Opis
### Poziom

* Trwałość: ?
* Poziom: Trudny (10)
* Siły: 
* Słabości: 

# Wątki kampanii

1. 

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* 

## Po czym poznam sukces

* 

## Wynik z perspektywy celu

* 

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

