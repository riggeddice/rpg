---
layout: inwazja-konspekt
title:  "Pułapka na Edwina"
campaign: druga-inwazja
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140208 - Na ratunek terminusce(AW, WZ)](140208-na-ratunek-terminusce.html

### Chronologiczna

* [140208 - Na ratunek terminusce(AW, WZ)](140208-na-ratunek-terminusce.html

## Misja właściwa:

1. Do Hektora dzwoni Sophistia z informacją że zostałą aresztowana a Marcelin jest w szpitalu, został dźgnięty nożem. Hektor na początku ignoruje Sophistię, ale że nie może się dodzwonić do Marcelina wysyła Jana żeby odebrał ją z aresztu. Sam dowiaduje się w którym szpitalu leży Marcelin i jedzie zobaczyć co się dzieje, informuje Edwina, magiczny lekarz może się przydać.

2. Na miejscu w pokoju Marcelina jest pielęgniarka, Hektor żeby spokojnie przebadać Marcelina próbuje rzucić zaklęcie magii mentalnej żeby móc spokojnie używać magii. Pielęniarka atakuje Hektora i przerywa swój shapeshift wracając do swojego naturalnego kształtu. To vicinius który poturbował Andreę niedawno. Seria wyjątkowo pechowych rzutów kośćmi owocuje tym co następuje:
- Hektor nie jest w stanie się obronić, ale zdołał wybrać numer do Edwina który słyszy w jakich tarapatach jest jego brat.
- Hektor próbuje grać na czas i przy okazji wyciągnąć z viciniusa trochę informacji, ale nie dość że nic się nie dowiaduje to jeszcze vivinius wyciąga od niego informację o tym że tak, Edwin przyjdzie bratu na pomoc.
- Hektor mimo usilnych prób nie jest w stanie zapobiec transformacji, vicinius rani go tak długo aż wymusza transformację.

3. W międzyczasie do Andrei dzwoni Sophistia i prosi o pomoc. Andrea budzi Wacława i jadą razem na ratunek prokuratora. Docierają do szpitala chwilę po terminusach przysłąnych przez Edwina. Terminusom dowodzi Marian Agrest. Wacłąw rozstawia pole siłowe pod szpitalem które łapie ludzi wypadających przez okna. Nie chcąc wchodzić terminusom w drogę Andrea bierze na siebie wyszukiwanie celów a Wacław je realizuje. Andrea namierza człowieka który próbuje wymusić transformację w nieprzytomnym Marcelinie (lub zabić go), Wacław rzuca obszarowe zaklęcie mentalne zmuszające ludzi do utraty przytomości. Terminusi obezwłądniają przemienionego Hektora i wdają się w dyskusję z Edwinem co dalej z nim zrobić. W międzyczasie Andrea zajmuje się poszukiwaniem śladów a Wacław w ramach odciągania od niej uwagi zaczyna rytuał mający ustabilizować wszystkich rannych w szpitalu. W wyniku akcji zginęło pięciu ludzi, a dwóch terminusów zostało rannych. Andrea znajduje w teczce Hektora podrzucone dokumenty opisujące eksperymenty Blackenbauerów na ludziach i magach. Wyraźnie zostały podłożone i miały być znalezione przez terminusów. Dodatkowo (natural 20) znajduje ślad magiczny który pozwoli jej zlokalizować viciniusa. Ze względu na to że dochodzeniu przewodzi Agrest Andrea nie dzieli się z terminusami swoimi spostrzeżeniami. Wacław w międzyczasie robi fotkę obezwłądnionego przez terminusów Hektora, w razie potrzeby fotkę prokuratora okręgowego powinno się dać wymienić na drobną przysługę u kogoś z rodziny. Dodatkowo zabezpieczają i kasują nagrania z monitoringu na których widać przebieg starcia między viciniusem a Hektorem.

4. W posiadłości Blackenbauerów Edwin podlecza Hektora po czym zabiera się za leczenie Marcelina, który jest w dużo gorszym stanie. Terminusi przesłuchują Hektora, w tym badzo interesuje ich wątek Sophistii i skąd ona wiedziała że coś się dzieje. Jako że w zespole Agresta jest terminus któy chciał ją zabić Hektor udaje że nic nie wie. Osiąga jedyny swój sukces na tej sesji i Agrest wychodzi z posiadłości przekonany że Hektor jest chętny do współpracy ale naprawdę nie potrafi pomóc, możliwe że przemiana w bestię miesza mu w pamięci.

5. W biurze Andrea i Wacław przeglądają zebrane materiały i robią ich kopię. Przy okazji wychodzi na jaw że ludzi i magów do eksperymentów dostarczała m.in. Bożena Zajcew, taki los mógł potencjalnie czekać Tatianę. Po wyjściu terminusów z posiadłości Blackenbauerów Andrea i Wacław udają się tam porozmawiać z Hektorem. Hektor nie kojarzy w ogóle że byli świadkami sceny w szpitalu. Wyjaśniają jaki był ich udział i przekazują co znaleźli. Wywiązuje się rozmowa w której planują wspólnie w trójkę w jaki sposób podejść viciniusa i pojmać zanim zrobi więcej szkód. W rozmowie w cztery oczy z Hektorem (Andrea i Wacław za zgodą Hektora podsłuchują rozmowę) Edwin wypiera się swojego udziału przy tworzeniu takiego viciniusa, twierzi że nie byłby w stanie, sugeruje że może Margaret miała z tym coś wspólnego. Przy okazji Edwin "prosi" Hektora żeby ten zamknął gdzies tą małą upierdliwą bo mu się plącze pod nogami, mowa oczywiście o Sophistii. Za radą Edwina Andrea i Wacław przed akcją wstrzykują sobie odrobinę przetworzonej krwi Hektora, gdyby się przeistoczył w trakcie akcji powinien rozpoznawać ich jako swoich i nie atakować. Dodatkowo na bazie opisu viciniusa Edwin przygotowuje środek który powinien ją obezwładnić "pod warunkiem że przebiją się jakoś przez pancerz". Na akcję idą przygotowani jak na ciężką bitwę, w pełnych tarczach, pancerzu, uzbrojeni w broń palną, środki wybuchowe oraz flashbangi. Dodatkowo w ramach metody dostarczenia środka obezwładniającego biorą również pistolety strzałkowe z tym środkiem, a w ramach zabezpieczenia się przed jadem antidotum na niego. Edwin jest przekonany że jeżeli dostarczą mu viciniusa będzie w stanie przywrócić jej oryginalną formę.

6. Vicinius zabunkrował się w piwnicy opuszczonego budynku. Na pierwszym piętrze urzęduje sobie komuna, ludzie, potencjalnie przejęci przez viciniusa. Aby uzyskać maksimum zaskoczenia i minimum strat w ludziach rozdzielają się, Andrea i Wacław idą do piwnicy zająć się viciniusem, Hektor ma za zadanie wyciągnąć ludzi poza teren walki a następnie wrócić jako wsparcie. Atakują dokładnie w tej samej chwili. Hektor rzuca zaklęcie sugestii na ludzi żeby wyszli na zewnątrz i zostaje wyśmiany. Andrea rzuca zaklęcie które ma oznaczyć viciniusa tak żeby nie był w stanie się ukryć w trakcie walki. Ułamek sekundy po zaklęciu Andrei Wacław odpala Wielką Gębę w piwnicy, cementując swoją opinię "ciut szalonego Zajcewa", Andrea chowa się za Wacławem. Vicinius jest całkowicie zaskoczony, zostaje trafiony i uszkadza mu to solidnie pancerz. Wacław traci tarcze ale jest nietknięty, wykorzystując ostatni moment zaskoczenia wrzuca do piwnicy dwa granaty między viciniusa a urządzenie na ścianie któe wygląda jak nieaktywny portal, Andrea wycofuje się w górę schodów, Wacław robi pad płaski na pysk w górę schodów. Granaty trafiają zanim vicinius zdążył dotrzeć do urządzenia, dodatkowo uszkadzają pancerz i niszczą urządzenie. Vicinius zaczyna rzucać zaklęcie przyzwania (prawdopodobnie zniewolonego maga), Andrea rzuca mu pod nogi flashbang co owocuje utratą koncentracji i paradoksem. Hektor podejmuje drugą próbę wyprowadzenia ludzi z budynku, próbuje ich przerazić wspomagany zamieszaniem które odbywa się w piwnicy. Osiąga drugi i ostatni sukces na tej sesji, tym razem częściowy. Ludzie uciekają, niestety Hektor też wpadł pod wpływ własnego zaklęcia i ucieka z nimi. W tym samym czasie w piwnicy paradoks viciniusa powoduje otwarcie jednokierunkowego portalu ze zbiornika wodnego (basenu?) do piwnicy. Piwnicę szybko zaczyna wypełniać woda. Wacław przygotowuje świetlisty piorun, vicinius zaczyna rzucać zaklęcie teleportacyjne do beacona który znajduje się gdzieś na poziomie poniżej piwnicy. Andrea reaguje i zapętla zaklęcie teleportacyjne viciniusa, vicinius teleportuje się w to samo miejsce, nadal jest w piwnicy pełnej wody, w tym momencie uderza piorun Wacława zdejmując jej resztki pancerza i raniąc ją. Budynek zaczyna się trząść, ale jeszcze stoi, przepięcie elektryczne z wody poszło na instalację elektryczną budynku, która zaczyna się palić. W ten sposób udowodniono żę wpuścić Zajcewa do domu to nawet jak będzie zalany i tak zacznie się palić. Vicinius nie ma wyjścia, szarżuje w kierunku klatki schodowej, chce wziąć Wacława (osmalony i bez tarcz) za zakładnika, Andrea składa się do strzału, Wacław przyjmuje atak i broni się wystawiając viciniusa na czysty strzał Andrei, vicinius zostaje obezwładniony. Niestety dawka obliczona przez Edwina nie do końca jest prawidłowa i vicinius wyraźnie cierpi i trzeba do jak najszybciej zabrać do Edwina celem ustabilizowania. Vicinius zostaje ogłuszony i obezwładniony.

7. Po przewiezieniu do posiadłości Blackenbauerów Edwin zabiera się za leczenie viciniusa i rozpoznanie czy jest w stanie odwrócić proces (jest, ale zajmie to parę tygodni). Nie wpuszcza nikogo do laboratorium, odrzuca ofetrę pomocy ze strony Andrei, sam zajmuje się wszystkim, asystuje mu tylko Hektor. Również Edwin przesłuchuje viciniuskę i w rozmowie z Hektorem (tym razem naprawdę bez świadków) przekazuje że viciniuska zaplanowała wszystko sama, chciała zabić Edwina żeby ten nie był w stanie wskrzesić Moriata. Edwin wypiera się znajomości i współpracy z Moriatem, nienawidzi go za to co zrobił rodzinie Blackenbauerów. Nie wskrzesiłby go nawet gdyby mógł, nie wierzy że Moriat byłby w stanie pomóc Margaret (chociaż przyznaje że gdyby miało mu to zwrócić Margaret i wiedziałby jak, wskrzesiłby Moriata bez zastanowienia). Przy okazji Edwin oskarża Hektora o to że porzucił rodzinę kiedy tego potrzebowała, ojciec zdecydował go nie wzywać bo chciał żeby mógł iść swoją własną drogą, i to wina Hektora że go nie było i że stracili Margaret. Jeżeli Hektor chce wiedzieć coś więcej ma zapytać ojca (i zabrać swoich "nowych przyjaciół") bo Edwin mu już nic więcej nie powie, sugeruje przy tym że przetrzymuje swoją pamięć na jakimś zewnętrznym nośniku i Hektor nie ma jak tego wyciągnąć z niego siłą.

CYTATY:

Hektor, torturowany przez viciniusa - "Chcesz zabić... wszystkich dookoła?"
Vicinius - "Tak."

Hektor - "Ktoś zastawił pułapkę na mojego starszego brata, Edwina. Niestety, wpadłem w nią ja"
Andrea - "Peszek"

Andrea, o viciniusie - "To kiedyś była kobieta."
Edwin - "Kiedyś. Teraz to pacjent."

![](140212_PulapkaNaEdwina.png)

# Zasługi

* mag: Hektor Blakenbauer, wpierw wpadł w pułapkę viciniusa i zmasakrował szpital a potem przez Paradoks się troszkę zbłaźnił podczas ataku na leże viciniusa...
* mag: Andrea Wilgacz, która skupia się na agregacji informacji, na dezinformacji i namierzaniu celów dla Wacława. 
* mag: Wacław Zajcew, wchodzący z bardzo ciężką artylerią i z planem dzięki któremu udało mu się przebić i zneutralizować świetnie przygotowanego viciniusa.
* mag: Marcelin Blakenbauer, nieprzytomna i ranna ofiara viciniusa służąca za przynętę dla Edwina (a w pułapkę wpadł Hektor).
* vic: Estera Piryt, która nie zawaha się skąpać szpitala we krwi by dostać w swoje szpony Edwina Blakenbauera. Pokonana i pojmana przez Wacława Zajcewa.
* mag: Edwin Blakenbauer, który zdradził coś z przeszłości Hektorowi; spróbuje odwrócić transformację Estery Piryt i przywrócić ją do formy maga.
* czł: Amelia Eter jako Sophistia, która próbuje uratować Marcelina i dzwoni do Hektora; gdy ten ją ignoruje, dzwoni do Andrei.
* czł: Jan Szczupak, kierowca który ma wyciągnąć Sophistię z aresztu.
* mag: Bożena Zajcew, która zostaje ujawniona jako dostawca ludzi i magów do Rezydencji Blakenbauerów...
* mag: Marian Agrest, dowodzący akcją i tuszujący że coś złego się wydarzyło (oraz śmierć wszystkich tych ludzi); zainteresowany Sophistią.