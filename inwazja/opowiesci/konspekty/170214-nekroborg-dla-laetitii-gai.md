---
layout: inwazja-konspekt
title:  "Nekroborg dla Laetitii Gai"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170120 - Wielki sojusz powszechny (HS, KŁ)](170120-wielki-sojusz-powszechny.html)

### Chronologiczna

* [170212 - Nieufność w małym mieście (PT)](170212-nieufnosc-w-malym-miescie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Henryk się obudził w swoim ulubionym hotelu. Tym razem jest tam też Karina i Nicaretta. Marzena zostawiła swój numer na komórkę; nie ma jej tam.

Karina opowiedziała Henrykowi odnośnie aktualnego "wroga" Kinglorda - Laetitii Gai Rasputin Weiner. Ona kilka osób ostrzegła, ogólnie czyści pryzmat po Kinglordzie... Kinglord na nią poluje, ale nie może złapać. A do tego Laetitia go jeszcze tauntuje.

Henryk, zrozpaczony, włączył hipernet. Połączył się z tien Fiodorem Pyszczkiem po hipernecie (-1 surowiec)...

* Potrzebuję osobę, która miała do czynienia z bardzo potężnym defilerem i powie mi, jak żyć... - zrozpaczony Henryk
* ...co się stało - Fiodor
* Powiedzmy, że się trochę skaleczyłem w nieodpowiednim momencie... - Henryk
* Trzeba zgłosić oddziałowi terminusów! Tien Czarko powinien być niedaleko! - Fiodor, szczerze
* Wiem, kiedy żartujesz - Henryk

Po interesującej rozmowie z Fiodorem doszli do ciekawej opcji. Henryk musi mieć kontrolny subsystem zewnętrzny. Gdzieś w pobliżu Henryka jest czarodziejka Świecy; neuronautka i ekspert od hipernetu. Może ona coś zaproponuje...

Komunikacja z Fiodorem się zerwała. Henryk dostał wiadomość z innego źródła. Nawiązała z nim kontakt Laetitia. Powiedziała mu, że ma mnóstwo rzeczy których potrzebuje.

* Laetitia chce, by Henryk zdobył dla niej MNÓSTWO rzeczy, większość Weinerskich
* Chce skonstruowania mnóstwa dronów technomantycznych, oczu i uszu
* Chce ruchu oporu, który zadziała w imieniu Laetitii

Laetitia powiedziała też, że nie interesuje jej przejęcie kontroli nad Henrykiem. Nie ta liga. Weinerowie oddali jej ten teren i ona ma zamiar skutecznie przejąć kontrolę nad tym terenem. Henryk powiedział, że jemu ogólnie pasuje... a co jak odmówi? Laetitia powiedziała, że po prostu Henryk zostanie zmieniony w Hanię i będzie Kinglordowi służyć w odpowiedni sposób. Po czym Laetitia się rozłączyła...

Henryk znowu połączył się z Fiodorem. Powiedział, czego potrzebuje. Fiodor złapał się za głowę. Henryk powiedział o Laetitii, Fiodor powiedział, że nie istnieje taka osoba w Świecy. Henryk powiedział, że Świeca się rozpada... ogólnie, konflikt. Henryk powołał się na wspólną znajomość w Szlachcie (-1 surowiec) i... SUKCES (6v6). Fiodor obiecał, że zdobędzie wszystko co łatwe i co może dla Henryka zdobyć.

Wystarczy to na zbudowanie dron i nekronów.

Henryk poszedł spotkać się z Hubertem. Przyszedł z winem. "Rocznica święceń ;-)". 

* Łzy dziewic! - Hubert, szczęśliwy
* Tak! Prosto z Watykanu. - Henryk, podstępny

Henryk rzucił urok na Huberta. Ten obiecał Henrykowi dostęp do fabryki mikroprocesorów. Henryk szczęśliwy. Karina wysłała Marzenę do propagowania kultu - ruch oporu przeciwko Kinglordowi w imieniu Laetitii. A sama Karina pomaga jej mówiąc jaki jest Kinglord i podpowiadając rzeczy propagandowe.

Komunikat od Laetitii.

Laetitia potrzebuje ZWŁOK. Zwłok maga. Jest nie do końca normalna; odezwała się do Henryka jako do Klepiczka i nie do końca pamięta o tym. Laetitia powiedziała Henrykowi, że chce zbudować magitechowego nekroborga do zniszczenia Kinglorda. Na nieśmiałe stwierdzenia Henryka, że może nie chce grzebać w zwłokach Laetitia powiedziała, że... może on nie chce się uwolnić ;-). Henryk się wycofał.

Ale Laetitia jest... dziwna. Henryk psycholog wie (KONFLIKT: S).

Henryk skomunikował się z Fiodorem. Poprosił Fiodora o dane maga-księdza z Lenartomina. Jakiegokolwiek... ale maga, który gdzieś tu umarł jako mag. Fiodor nieufny... wtf. 

* W jaki sposób roczniki księży lenartomińskich rozwiążą Twój problem z defilerem? - Fiodor
* Metoda prób i błędów, Fiodor... - Henryk

KONFLIKT. 4v3 -> R. Fiodor powiedział, że ogólnie rzecz biorąc... nie może tego wyciągnąć, ale ma rekordy świadczące, że w Wyjorzu, pod fabryką mikroprocesorów (???) pogrzebana jest czarodziejka Świecy. Być może Diakonka. Trzeba uważać na pułapki; na pewno są. Podobno jakiś mag ją od czasu do czasu odwiedza...

Karina nie zgadza się na wykopki aż nie rozwiążą problemu pułapek. Na szczęście mają dostęp do fabryki mikroprocesorów... bo Hubert zapewnił ten dostęp. I Karina uświadomiła Henryka, że jest DUŻO rodzajów pułapek.

Henryk: -1 surowiec. Hubert dostarczył mu georadar.

Dzień 2:

Henryk poszedł z Kariną do fabryki mikroprocesorów. Sprawdzili na georadarze; jest pod ziemią betonowa trumna. Dość głęboko. Jakieś... 10 metrów. I nic więcej nie mają - nie mają jak tego wydobyć... jest też tunel. Tunel prowadzi od jakiegoś domu, piwnicy. 

Laetitia zapytana powiedziała, że w okolicy nie powinno być innych magów. Ale zażyczyła sobie pięć ludzkich, w miarę świeżych zwłok. Można wykopać.

* Kto ma cmentarz? Klepiczek ma cmentarz - Raynor, 2017

Dobra. Karina załatwia van Hortexu (chłodnię) i wezwała drabów (-1 surowiec). Nicaretta poszła zająć się domkiem a Karina i Henryk poszli do tutejszego szpitala dla ubogich... Henryk przygotował zaklęcie i z Kariną weszli w głąb, do kostnicy. KONFLIKT (6v3) -> S. Dotarli bez żadnych problemów do kostnicy. Henryk odpalił czar i zapakowali pięć świeżych zwłok do vana Hortexu... nikt w szpitalu nie uważał tego za dziwne. Po tym pojechali do fabryki mikroprocesorów.

Przy fabryce czeka Nicaretta. Powiedziała, że w domu sami ludzie; ma wejście gdy chce. Pożywiła się (tam są nastolatki). Karina przytuliła się do sukkuba, ta pieszczotliwie pomiziała ją po włosach. Henryk przy tunelu zrobił kalibrację astraliczną; chce sprawdzić, co jest tam w środku - jakie nastroje. KONFLIKT: 6v6->F,F,S. Niestety, osoba odwiedzająca grób dowie się w perspektywie czasu KTO zbezcześcił grób a Henryk poczuł jego uczucia.

Henryk poczuł dojmujący smutek za przyjaciółką. Dziewczyną, która... no, nie zasłużyła na to. Nie kochał jej, ale... lubił. I chce ją zobaczyć od czasu do czasu. Pociekła mu łza i powiedział o wszystkim Karinie.

Karina postawiła tarczę magiczną i poszła do tunelu. Atak magiczny - "nic tu nie ma, idźcie stąd". 8v6->S. Karina zrobiła decoy na piłeczce i puściła ją przodem. Sama poszła z Henrykiem. O dziwo, nie było pułapek; dotarli do sarkofagu.

"Uśmiercona przez lustro niewinna czarodziejka. Uhonoruj naszą przyjaciółkę i odejdź w pokoju." - napis.

Wsadzili tam cztery podnośniki, z opóźnionym zapłonem i wyszli. Godzinę później wrócili... jest nadal sarkofag w tunelu, acz przewrócony. Podnośniki zniszczone. Ogólnie, nieco ruinowato to wygląda. Ale pułapka rozbrojona. Hubert nie będzie szczęśliwy ;-).

Dobra, grupa zbirów zanosi sarkofag do góry; biorą to do fabryki mikroprocesorów. Tam są wyrzynarki, prasa... rzeczy, którymi można zmiażdżyć sarkofag.

Karina używając katalizy silnie próbuje osłabić fragment sarkofagu (jego nadmagiczną odporność) w czasie gdy prasa hydrauliczna próbuje zmiażdżyć sam sarkofag. Udało się otworzyć sarkofag, aczkolwiek prasa hydrauliczna została zniszczona (i nekroborg będzie bardziej agresywny; Laetitia nie ma nad nim pełnej kontroli). Laetitia wysłała dronę - kruka sprzężonego technomantycznie i przykazała Henrykowi zbudowanie kryształu Quark opartego o Pryzmat "ludzie wstają z grobów".

Tymczasem Nicaretta poproszona przez Henryka... zabrała się za opracowywanie zwłok. Musi delikatnie wyrwać serce martwej czarodziejce i trochę oczyścić komorę w klatce piersiową. Nicaretta jest baaaardzo nieprzekonana do idei budowania nekroborga, ale nie ma pomysłu co z tym tak czy inaczej zrobić. Karina z pomocą kruka układa linię produkcyjną w fabryce, by móc zbudować nekroborga...

Dzień 3:

Powstał proces produkcyjny.

* Henryk i Karina nakarmili przyszłego nekroborga swoją krwią, by ten nie mógł ich zaatakować ani zrobić im krzywdy
* Kruk zaczął wyżerać układ nerwowy z ciał ludzi; zaczął się też łączyć technomantycznie z komponentami mechanicznymi linii produkcyjnej
* Kruk umościł się wygodnie w miejscu, gdzie martwa czarodziejka miała serce; po czym się "wylał" organicznie. Została techno-organiczna masa.
* Maszyny zaczęły przebudowywać ciało nekroborga, prowadząc do techno-organicznego wyniku
* Henryk zauważył, że nic nie kontroluje nekroborga. Laetitia przypomniała sobie, że potrzebuje jeszcze jednej drony (ma ją niedaleko)
* Przyleciał gołąb; Henryk musiał pomóc gołębiowi wejść przez gardło martwej czarodziejki i dodziobać się do mózgu, by zaimplantować hipernet
* Henryk wbił nekroborgowi w serce kryształ Quark
* Nekroborg się "urodził" i Laetitia dała radę inkarnować weń swój fragment.

Zdaniem Laetitii Gai, teraz Kinglord nie ma większych szans. Karina jest sceptyczna...

# Progresja

* August Bankierz: dowiedział się o splugawieniu grobu S.S. przez Henryka Siwieckiego
* Laetitia Gaia Rasputin Weiner: niestety, jest dużo bardziej Zakłócona niż można się było spodziewać...

# Streszczenie

Laetitia - zakłócony Overlord Hipernetowy - połączyła się z Henrykiem i kazała im złożyć nekroborga, by walczył z Kinglordem. Nie mając lepszego pomysłu, Henryk i Karina pomogli Laetitii. Henryk wyczuł, że z Laetitią jest "coś nie tak". Fiodor - mag Świecy - dowiedział się, że Henryk może być w rękach defilera. Aha, przy okazji zbezcześcili sarkofag pod fabryką mikroprocesorów w Wyjorzu i uszkodzili samą fabrykę ;-).

# Zasługi

* mag: Henryk Siwiecki, który zbudował nekroborga na życzenie Laetitii, bezczeszcząc trochę wszystkiego po drodze... trochę za dużo...
* mag: Karina Łoszad, wydatnie pomogła w zdobyciu zwłok z kostnicy i skutecznie otworzyła sarkofag. Wolałaby być bardziej niewinna niż na tej akcji.
* vic: Laetitia Gaia Rasputin Weiner, dużo bardziej Zakłócona niż się wydawało. Kierowała akcją uruchomienia nekroborga. Inkarnowała swój Fragment w nekroborgu.
* mag: Fiodor Pyszczek, znajomy Henryka mag Szlachty, który pomógł jak mógł... niestety, niewiele mógł. Wie, że Henryk może wpaść w łapki defilera.
* vic: Nicaretta, gdzie mag nie może, tam sukkuba pośle - od infiltracji domku po wyciąganie serc z martwych czarodziejek. Bardzo, bardzo nie opłacał jej się ten sojusz...

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie
            1. Powiat Tonkij
                1. Wyjorze
                    1. Wyjorze Fabryczne
                        1. Fabryka mikroprocesorów Majka, gdzie zbudowano nekroborga i która została dość... zdewastowana przez działanie magów
                    1. Śpiochowice
                        1. Tunel do Sarkofagu, w piwnicy jednego z bliźniaków w Śpiochowicach
           
# Czas

* Dni: 3