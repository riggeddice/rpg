---
layout: inwazja-konspekt
title:  "Kiepsko porwana Paulina"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170723 - Wywalczone życie Diany (PT)](170723-wywalczone-zycie-diany.html)

### Chronologiczna

* [170723 - Wywalczone życie Diany (PT)](170723-wywalczone-zycie-diany.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Dzień 1: 

Noc. Paulina śpi. Nagle - coś ją obudziło. Trzeszczenie deski. OBOK niej... brzmi jakby ktoś się do niej podkradał...

Paulina zamknęła oczy i zapaliła lampkę nocną. I usuwa się z łóżka na ziemię. (2v3->S). Udało jej się rozpoznać sytuację i przygotować się do ucieczki. Udało jej się zaskoczyć napastnika, po czym... zaczęła wrzeszczeć i przygotowała się, by... rzucić zaklęcie. (2v5->nie ma konfliktu). Drab dopadł Paulinę i zatkał jej usta.

* Ćśśś, nic Ci nie zrobię, mała - drab - Jesteś lekarką a ja mam pacjenta. Rozumiesz? Pokiwaj głową, jeśli rozumiesz.
* ... - Paulina pokiwała głową
* On umiera. To człowiek. Zawiozę Cię do niego. Współpracuj, a nic Ci się nie stanie. - Drab, zdecydowanym głosem - Pokiwaj głową, jeśli będziesz współpracować.
* ... - Paulina pokiwała głową

Drab fachowym ruchem zakneblował Paulinę. Po czym wziął płaszcz. Podał jej. Podał jej też apteczkę. Paulina tymczasem uwolniła się z knebla.

* Nie to... - Paulina, z lekką pogardą. - Co mu jest?
* Pobity. Pocięty. Możliwa trucizna. - Drab, fachowo
* Nie mogłeś go tu przywieźć? - Paulina
* Nie przetrwałby. Wsadziłem go w coś najbliższego stazie co mogłem - Drab

Paulina wzięła apteczkę - WŁAŚCIWĄ - i wpakowała tam dodatkowe rzeczy. Drab niecierpliwie czeka. Paulina zgarnęła wszystko i wzięła kluczyki od auta...

* To nie będzie Ci potrzebne - Drab, z lekką pogardą
* Wsiadaj i nie pyskuj - Paulina
* Chcesz zdążyć? - Drab
* Możesz prowadzić. - Paulina
* To nie ma prędkości jakiej potrzebujemy. - Drab

Paulina spojrzała na wielgachną torbę. Drab prychnął. Paulina ustąpiła. Z bagażnika wzięła kocyk termiczny. Tymczasem drab podjechał do Pauliny dużym motorem. Paulina z naiwną nieświadomością złapała torbę i siadła z tyłu. Drab położył ją na bagażniku, gdzie mechaniczne łapki utrzymały torbę. Sam usiadł z przodu. Włączył jej "łapki" - Paulinę też trzymają nieźle.

I drab wyruszył. O ja pierniczę...

15 minut później byli około 100 km na zachód. Podjechał do czegoś, co wygląda jak stolarnia. To jest stolarnia. Wjechał i poprowadził Paulinę do jednego z mniejszych pomieszczeń. Tam Paulina zobaczyła ciężko pobitego i rannego mężczyznę. Wygląda jak zmasakrowany dresik z siłowni. Utrzymywany jest w jakimś samorobionym polu stazy. Paulina oceniła to z poziomu katalitycznego. Będzie interferować ze wszystkim. Nawet lekko Skaża mężczyznę. Ale bez tego by umarł.

Mężczyzna jest w stanie krytycznym, ale nie umrze od razu po wyłączeniu stazy. Ktoś się nad nim nieźle znęcał. Paulina spojrzała na draba. Ten wzruszył ramionami - po co miałby go ratować, gdyby on go tak załatwił?

* To normalnie jest OIOM - Paulina
* Co to jest OIOM? - Drab, zdziwiony
* Jaką magią władasz? - Paulina
* Bio i techno - Drab
* To się przydasz - Paulina

Paulina powiedziała, że drab pomoże jej zbudować porządną stazę. Przy okazji się czegoś nauczy. Facet przełknął obrazę. Paulina wyjaśniła krok po kroku co on ma robić, dlaczego i jak. O dziwo, drab w kominiarce okazał się być kompetentnym magiem i pilnym uczniem. TEGO Paulina się mniej spodziewała. Za to w ciele nieszczęśnika Paulina zobaczyła coś, czego się nie spodziewała. Truciznę aktywnie zwalczającą magię...

(5v4->F(on podpowie), F(wpływa na stazę), R(brak bonusu z wiedzy)). Paulina zauważyła truciznę - a nawet jad. Dopytany drab powiedział, że tam był jakiś vicinius. Jadowity skorpion, duży. Paulina poznała jad - Dromopod Delinquo. Skorpion z jadem antymagicznym. Skorpion wyhodowany do zabijania magów, którego jad jest magiczny sam w sobie i który kontr-reaguje z magią leczniczą. Wow...

* Widziałeś, jak to się stało? - Paulina do draba
* Nie. To mój jedyny ślad. - Drab - Znalazłem go, zanim umarł. Skurczybyk nie miał tam szans...

Paulina zauważyła jak to działa - Dromopod Delinquo jest ogólnie bardzo antymagiczny i jego jad też neutralizuje magię. Najlepiej by wyszedł na transfuzji lub dializie. Rany są zagrażające życiu. Trucizna utrudnia wyleczenie. Całość wyleczenia: bardzo trudny ekspert (9). Paulina: (5+1+2+3)(gabinet, -1 surowiec). (11v9->S). Paulina z pomocą draba dała radę uratować życie człowiekowi.

* Dobra robota. Dobra, odwiozę Cię. - Drab
* Ooooo nie. - Paulina, protestując - Żadnych takich.
* ? - Drab spojrzał pytająco
* No przecież go tu nie zostawię... to pacjent, ledwo odratowany - Paulina, oburzona
* A. - Drab, drapiąc się po głowie - Mam ci go zawieźć? Tam do Ciebie?
* Kim on jest? - Paulina
* Nie wiem... - Drab - Jakiś łepek, który był w niewłaściwym miejscu i chciał wywrzeć wrażenie...
* Czyli nie wie o magii? - Paulina
* Nie. Skończył jako zabawka u jakiegoś maga. Wynik widzisz. A ja poluję na tego maga. - Drab - Ten nie jest pierwszy; ale do tego pierwszego dotarłem, gdy żył.
* Czyli rozumiem, że chciał zaimponować i został napuszczony gdzieś, gdzie Dromopod Delinquo jest i..? - Paulina
* Nielegalna arena. - Drab
* Mhm... człowiek przeciwko Dromopod? Serio? Co to miało być? - Paulina, oburzona
* Czasem nie chcesz patrzeć na walkę a na krew - Drab, filozoficznie
* ... - Paulina rzuciła drabowi mroczne spojrzenie
* To odwieźć go do Ciebie? To jedyny ślad jaki mam na tą arenę, więc chciałbym, by pożył - Drab
* Dobrze... chcesz znaleźć tą cholerną arenę, tak? - Paulina
* Nie. Chcę znaleźć tego MAGA i go ZMASAKROWAĆ. Arena to bonus. - Drab, śmiertelnie poważnie
* Nie mam pojęcia jak się nazywasz, ale Cię lubię... - Paulina, odkrywając w sobie pokłady sympatii do drabów

Paulina zdecydowała, że jak tylko nieszczęśnik się podleczy i nada się do przesłuchania, zostanie podrzucony do polskiego szpitala... i niech tam się regeneruje. Ale na razie trzeba rozwiązać problem z drabem. A dokładniej: Paulina nie chce rezygnować z akcji.

Paulina oparła się o drzwi i spojrzała na draba. Spytała go, jak się nazywa. Przedstawił się jako Wiaczesław Zajcew. Wie o Paulinie z kontaktów w półświatku. 

* Koncepcja porwania jest trochę nie tego... - Paulina
* Działa. Jesteś tu i człowiek jest uratowany - skwitował Wiaczesław

Paulina uparła się, że nigdzie nie idzie, póki Wiaczesław nie powie jej o co chodzi w tej sprawie. Wiaczesław zaczął spokojnie. Jest mag... lub kilku magów. Oni mają taką arenę. Tam się zabawiają ludzkim życiem. Ludzie, viciniusy, czasem magowie przeciwko ludziom czy viciniusom. Najpewniej jest tam jakiś Myszeczka, po tym Dromopod i innych viciniusach. A Wiaczesław chce tą arenę zamknąć - na stałe.

Co wie? Niewiele. Dlatego szukał ofiar. Ofiary nie są w tym samym miejscu; są rozpraszane po kraju, by nie dało się sensownie znaleźć.

* I wśród znajomych nikt nic nie słyszał? - Paulina
* Nie mam tam wielu przyjaciół - Wiaczesław
* To czym się zajmujesz? - Paulina
* Przemytem, rozbojem, misjami beznadziejnymi... norma - Wiaczesław wzruszył ramionami
* No ta w sumie jest taka beznadziejna... - Paulina
* Doktórko, wiesz, czemu nie mam wielu przyjaciół? - Wiaczesław, z uśmiechem
* Czemu?
* Uważają, że jestem zbyt radykalny. TERMINALNIE radykalny. - Wiaczesław, z szerszym uśmiechem
* Rozumiem, że próbujesz mnie ostrzec? - Paulina
* Tak. Nie przede mną. Przed powiązaniem ze mną - Wiaczesław, poważnie
* Przyjęłam do wiadomości. Co z tą areną? - Paulina

Wiaczesław tropił tą arenę już od pewnego czasu. Planował dostać się tam jako gladiator by ją rozwalić od środka. Paulina zwróciła uwagę, że to nie najlepszy pomysł. Wiaczesław zauważył, że zadziała. Paulina zauważyła, że to magowie organizują i niekoniecznie znają się na świecie ludzi. Ale wpierw zdobyć informacje od ofiary. Paulina powiedziała, że wiele się nie dowiedzą, ale...

Paulina zaczęła przesłuchiwać mentalnie ofiarę. Poprosiła Wiaczesława, by ten popilnował biomantycznie ofiarę jak ona go przeskanuje; Wiaczesław się zgodził (3v3->F,R); Paulinie udało się wydobyć informacje od ofiary, niestety, ofiara została bardziej osłabiona. Odpalił też trigger w ciele ofiary; atak Strachu skierowany do zniszczenia umysłu człowieka. Wiaczesław to zaabsorbował i się tylko zaśmiał. Tym nie przestraszą Zajcewa. Zwłaszcza jego. Acz - jest tam też Mortalis...

Paulina dostała jednak lokalizację. W powiecie ciemnowężnym, w Skalnej Grocie. Gdzieś tam jest ta arena. Paulina powiedziała, że może Wiaczesław kogoś zna kto jest lepszym mentalistą. Ten się zastanowił... w sumie, możnaby. Powiedział, że jest jeden znajomy... znajoma. Skomplikowane. Tak czy inaczej, może poprosić. Dostanie pomoc. Paulina widząc, że RADYKALNY ZAJCEW waha się przed wezwaniem znajomego (o imieniu Terror Wąż), stwierdziła, że ściągnie artefakt. Celowany na ludzi - czytanie i usuwanie, zwłaszcza, że jako lekarka ma pretekst i powód. Człowiek wpakował się w coś.

Dzień 2: 

Paulina dostała artefakt. Nada się idealnie. (7v5->S). Po uruchomieniu artefaktu na nieszczęśniku, dowiedziała się co się stało. To był młody "wyrzutek ze społeczeństwa", który był niezadowolony, nie widział perspektyw... szukał jak imponować dziewczynom i jak ogólnie być świetnym. Chłopaczek wszedł w konszachty z wiłą, która dostarczyła mu narkotyków bojowych. Młody był świetny w walce, sztuki walki, bez efektów ubocznych... w końcu sam zdecydował się "odejść" ze swoją przyjaciółką. Ona zaprowadziła go do Skalnej Groty i przedstawiła Mistrzowi. Mistrz miał maskę, ale koło niego stał Mortalis. 

Resztę Paulina rekonstruuje ze swojej wiedzy. Mistrz zadał kilka pytań człowiekowi, ten odpowiedział a Mortalis go "badał". Coś, co Mortalis zauważył nie pasowało Mortalisowi i Mistrzowi. Dlatego chłopak skończył na arenie. Najpewniej Mistrz coś chciał od chłopaka. I nie dostał tego. Najprawdopodobniej coś w stylu absolutnego posłuszeństwa czy coś...

Na trybunach podczas masakrowania chłopaka było kilkanaście osób. Walczył z nim sam Mistrz. Ale wtedy Mistrz używał magii. Dyskretnej magii. Ale Paulina wie, czego się spodziewać. Czyli... Mistrz maskował się przed... ludźmi? Na widowni byli ludzie? Ale Dromopod Delinquo był; pod sam koniec zrobił "fatality".

Paulina podrzuciła chłopaczka (po stabilizacji i wymazaniu pamięci) do jakiegoś szpitala.

Wiaczesław się ucieszył. Mają ŚLAD.

A Paulina ma jeszcze jeden pomysł - skoro przeciwnik wykorzystuje ludzi, Paulina poszuka informacji o ludziach NN, pojawiających się znikąd zmarłych... na szerokim terenie. Poszuka czegoś powiązanego z tym magiem w świecie ludzi. Może uda się coś znaleźć :D.

Wiaczesław powiedział Paulinie, że ma zamiar tam iść. Paulina zaprotestowała - ona ma zamiar ściągnąć ogień na siebie. Wiaczesław absolutnie się nie zgodził; pójdzie z kumplem i rozwalą to wszystko. Ona może być z tyłu. Wiaczesław powiedział Paulinie coś... niepokojącego - że nie może nikt wiedzieć że ona miała coś z tym wspólnego. Paulina idzie jako medyk. Drugoliniowo.

Niechętnie, Paulina się zgodziła.

* Kto przyjdzie z Twoich kumpli? Jak bardzo jest viciniusem? - Paulina
* Terror Wąż. Gość... czy dziołcha, nie wiem, serio, jest... bardzo viciniusem. - Wiaczesław
* Ty jesteś magiem, pełen zakres... - zrezygnowana Paulina
* Wąż wystarczy. Nie znam drugiego tak skutecznego zabójcy - Wiaczesław - Jako bonus, jest wegetarianinem
* Jesteś pewien, że chcesz zabijać tego maga? - zaskoczona Paulina
* Jesteś pewna, że chcesz, żeby on żył? - cholernie poważny Wiaczesław
* Wiesz... można sprawić, by zrozumiał swój błąd... - Paulina, oponując
* Tak silnego... czy wpływowego maga trzeba zniknąć. Porwać... lub zabić. - Wiaczesław - Lub porwać zwłoki.
* Pytanie, czym to czyni Ciebie - Paulina
* Mordercą. Wyrzutkiem. Tym, czym jestem dziś - Wiaczesław, spokojnie - Powiedziałem, że jestem radykalny.
* No jesteś... - Paulina, niechętnie

Aha, Wiaczesław ostrzegł Paulinę. Nie żartować z wegetarian. A zwłaszcza z psychopatycznych viciniuso-magów wegetarian.

Wieczór...

Do Stolarni wszedł gość. Ubrany jest jak kultysta. W szary płaszcz. Wiaczesław wyszedł mu naprzeciw.

* Terror Wąż, bracie! - Wiaczesław, radośnie
* Dziś: siostro. - Terror Wąż, z uprzejmym uśmiechem
* Terror Wąż, siostro! - Wiaczesław, bez zmiany
* Wiaczesław, mój bracie! - Terror Wąż, radośnie

I padli sobie w ramiona. Wąż spytała Wiaczesława o Paulinę. Wiaczesław powiedział "ze mną" i zawołał Paulinę. Wąż się jej przypatrzyła.

Paulina przypatrzyła się Wąż. Terror Wąż wygląda jak... voldemort z filmu, ale bardziej wężowo. Ma tatuaże. Ma łuski. I MA MAKIJAŻ!

* Marchewkę? - Wąż, do Pauliny
* Jasne! - Paulina

Wąż zaczęła chrupać marchewkę z radością. Paulina też. Była pyszna.

* Własna hodowla? - Wiaczesław
* Nie, kupiona; ale świeża, nie tak z przeceny - Wąż - Chcesz też?
* Nie, dzięki - Wiaczesław
* Twoja strata - Wąż wzrusza ramionami

W skrócie Wiaczesław objaśnił Wąż sprawę. Wąż pokazała ząbki. Bardziej jaszczurcze niż wężowe. W ogóle, Paulina jednej rzeczy nie rozumie - Wąż wygląda bardziej jak jaszczurka niż jak wąż. Czemu więc... wąż?

* Czyli: jest mag, lub kilku, bawią się ludźmi, zabijamy i wychodzimy? - Wąż, w skrócie
* Tak. Oszczędzamy ofiary. Sprawcy... tych nie oszczędzamy. - Wiaczesław
* A Twoja Paulina? - Wąż
* Trzeba pewne rzeczy ustawić... po pierwsze, kochana Wąż, nie jestem jego. Jestem jego na podobnej zasadzie jak i Ty. - Paulina
* ... - Zarówno Wiaczesław jak i Wąż zaczęli się śmiać, bardzo, bardzo ostro
* Paulinko... Ty wiesz, że przed Zaćmieniem byliśmy kochankami? - Wąż do Pauliny
* Tak daleko jeszcze nie doszliśmy. Może kiedyś, jak bardzo ładnie poprosi... - Paulina, bez zmrużenia oka
* ... - Wiaczesław i Wąż praktycznie padli na ziemię.
* Dobrze, podoba mi się. Żółtodziób Żerców Apokalipsy, ale potencjalny Żerca - Wąż
* Tak daleko nie. Podoba mi się, ale nie do Żerców. Ona ma skrupuły - Wiaczesław
* Ona nie chce zabić tych magów? I zjeść serc? - Wąż
* ... - Paulina spojrzała na Wiaczesława - Wegetarianizm, mmm?

Chwilę później Paulina jest pod ścianą, odepchnięta przez Wiaczesława, który mocuje się z Wąż. I nie wygrywa. A Wąż wygląda na nieco morderczy nastrój. Przynajmniej patrząc, że z Zajcewem walczą na SERIO. Lekko przerażona Paulina wzięła szybko marchewkę i przeprosiła Wąż. (5v3->R). Wąż natychmiast przestała walczyć i podeszła do Pauliny, z Wiaczesławem obserwującym bardzo ostrożnie.

* Przelałaś krew dobrego maga, mojego brata. - Wąż, śmiertelnie poważnie - Muszę upuścić kroplę Twojej krwi dla balansu.
* ... - Paulina, nie zgadzając się, że ONA przelała, ale podała dłoń. Usztywniła się. Wąż bardzo delikatnie utoczyła krew Pauliny.
* Teraz jesteśmy kwita. - Wąż, uśmiechając się słodko - Możemy być przyjaciółkami

Paulina potrzebowała chwilę na pozbieranie się a Wiaczesław jakimś cudem zdobył jabłko dla Wąż do zjedzenia, kupując Paulinie czas. Potem przeprosił Wąż i poszedł z Pauliną na stronę.

* Ona nie jest taka zła. - Wiaczesław
* Nie mówię, że jest... - Paulina - Mówiłeś o żartowaniu, nie o wspominaniu w ogóle...
* Wąż to Wąż. Jak wulkan. - Wiaczesław, wzruszając ramionami - Nie opanujesz, nie zrozumiesz. Dobry przyjaciel.
* Nie chciała nigdy być znowu magiem? - Paulina
* Był... czy była, nie wiem, nie znałem, Diakonem. - Wiaczesław - Obsesja przesunęła się gdzieś indziej. Na ewolucję.
* To o co chodzi z tym wegetarianizmem? - Paulina
* Nie wiem. Nie wnikam. Jest wegetarianką. - Wiaczesław
* To co to miało być z tym pożeraniem serc? - Paulina
* Inny z naszej... grupki... ma takie tendencje - Wiaczesław
* M...hm. - Paulina

Po chwili rozmowy i uspokajania, Paulina przygotowała sobie rzeczy katalityczne do niszczenia aren ;-). I jest gotowa. Wąż traktuje Paulinę znowu z sympatią. Paulinę to cieszy i... no... Wąż.

Paulina zauważyła coś ciekawego...

* Wąż? Wiaczesław? Przyszło mi coś do głowy... - Paulina, niepewnie - Nie wiem co DOKŁADNIE chcecie zrobić i nie będę dopytywać... ale pomyśleliście o tym, że jeśli Wąż pojawi się na arenie to mogą nie zwrócić na to uwagi?
* Genialne! - pisnęła Wąż - Mogę zostać gladiatorem!
* M...hm? - niepewna Paulina
* To wymagałoby... pomyślenia. Wkręcenia Cię tam. - Wiaczesław, niepewnie
* No, znaleźliście arenę, macie pamięć tego nieszczęśnika, wiecie jak wkręcić mnie na arenę. - Wąż
* Ty od środka, ja od zewnątrz? - Wiaczesław
* Jak za dawnych lat... - Wąż
* Dobra, Paulino. To jak ją wkręcimy? - Wiaczesław, pytająco. Wąż też na Paulinę spojrzała pytająco
* Na tej arenie ludzie są traktowani jako zwierzyna... zwierzęta - Paulina
* Najpewniej viciniusy też. - zauważyła Wąż
* Gdzie są zwierzęta, tam są flaki dla zwierząt. Niekoniecznie pilnowane... - Paulina - Jesteś magiem, większość zabezpieczeń antyludzkich Cię nie dotyczy
* W skrócie: Znaleźć mięso, udawać że chcę ukraść mięso, dać się złapać? Udawać viciniusa, nie maga? - Wąż
* Jedyny problem: jak dostaniesz się na arenę... - Paulina
* Wiesz, jeśli coś pójdzie BARDZO nie tak, Wiaczesław mnie odbije. Lub zapomnę o wegetarianizmie. Raz. - Wąż
* ... - Paulina, przeświadczona, że Wąż zapomina o wegetarianizmie nieco częściej

# Progresja



# Streszczenie

Wiaczesław "porwał" Paulinę, by ta uratowała zmasakrowanego człowieka. Udało jej się. Wiaczesław opowiedział Paulinie o tajemniczej arenie gdzie magowie bawią się kosztem ludzi i ich cierpienia; próbuje ją znaleźć. Paulina użyła swoich kontaktów i udało jej się namierzyć mniej więcej okolicę. Wiaczesław i Terror Wąż zapoznali się z Pauliną, która stwierdziła, że nie pozwoli tej dwójce ZABIĆ magów - nieważne jak źli ci magowie by nie byli...

# Zasługi

* mag: Paulina Tarczyńska, kiepsko porwana, ratująca życie ofiary areny, niechcący prowokująca Wąż i planująca jak NIE zabić złego maga.
* mag: Wiaczesław Zajcew, drab z charakterem. Radykalny i bezwzględny wobec tych którzy krzywdzą innych, delikatny wobec ludzi i magów którzy tego nie robią.
* mag: Terror Wąż, eks-Diakonka, płci randomowej, kupuje marchewkę nie z przeceny, morderczyni-wegetarianka i ma lekkie (?) psychozy.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, z którego Wiaczesław próbował ją porwać
            1. Powiat Czaplaski
                1. Paszczęka
                    1. Południe
                        1. Stolarnia Sitek, tymczasowa baza Wiaczesława
            1. Powiat Ciemnowężny
                1. Skalna Grota
                    1. Stadnina Koni, gdzie znajduje się arena tajemniczego maga
# Czas

* Dni: 2

# Narzędzia MG

## Cel misji

* Pokazanie Wiaczesława interesującego się światem ludzi i magów
* Zbudowanie Paulinie i Wiaczesławowi relacji wstępnej; niech to nie jest "jakiś Zajcew"

## Po czym poznam sukces

* Wiaczesław będzie lubiany przez Paulinę, ale też będzie się go trochę bała
* Wiaczesław i Paulina się lubią

## Wynik z perspektywy celu

* Sukces
* Sukces
