---
layout: inwazja-konspekt
title:  "Miślęg zniknąć!"
campaign: anulowane
gm: żółw
players: kić, agata_n
categories: inwazja, konspekt
---

# {{ page.title }}

## Misja właściwa:

Do Powiewu przyszła Szmatka. Miślęg. Znaczy, została przyniesiona przez Krystiana, który znalazł ją ranną w dole.
Szmatka mówi "miślęg zniknąć". "Kapeć zniknąć, Paproć też" Oczywiście, oba to miślęgi.
Orion bardzo się przejął i chce pomóc miślęgowi. Dlatego poprosił o pomoc Lunę, zaprzyjaźnionego viciniusa.
Usłyszawszy, że chodzi o pomoc miślęgowi, Luna nie była zachwycona, zwłaszcza, że Szmatka na jej widok oznajmiła, że nazywa się od teraz Futro i że kot to dobre mięso i futro.
Ogólnie Szmatka zachowuje się jak miślęg do kota a Luna nie docenia.
Orion, widząc eskalację, zrozpaczony poprosił o pomoc Annalizę. Annaliza wciąż jest kuromantką (i nie lubi jak jej to wypominać) i wciąż jest w PŚ. Poproszona o pomoc przez Oriona, złapała się za głowę, zwłaszcza, kiedy Luna, zniecierpliwiona postawą miślęga użyła na nim swojej umiejętności 'terror'. Annaliza uznała, że sama sobie nie poradzi i poprosiła o pomoc Magdę.
Magda, wykorzystując maksimum swoich umiejętności negocjacyjnych, wspólnymi siłami z leśniczym (?) przekonali miślęga, że kot jest trujący, jego futro jest trujące i że jak go zabije i zje, to umrze. Z kolei Orion obiecał Lunie, że spróbuje zrobić tak, żeby to konkretny kopiec miślęgów przestał polować na koty.

Kryzys jakoś zażegnany. Po krótkich dywagacjach stanęło na tym, że Krystian wie, gdzie jest kopiec miślęgów, Luna wie aż za dobrze i Szmatki do szczęścia nie potrzebują. Luna oznajmiwszy, że pomoże poszła w stronę kopca, nie zauważona przez obu magów, którzy wybrali się nieco później. Kotka pozwoliła im się nieco wyprzedzić i czekała, co uda im się wyciągnąć od miślęgów. Po rozmowie z miślęgiem InChina (potem Obiad, potem Radość), dowiedzieli się, że Powiew jest straszny bo duchokot, Paproć poszedł po paproć bo Paproć, a InChina pilnuje kopca przed duchokotem (choć Luna wie, że Kamili od dawna nie było w okolicy. Miślęg pokazał również mniej więcej kierunek, w którym oddalił się Kapeć. Nie czekając na dwunogi, Luna ruszyła wskazanym szlakiem licząc, że może jeszcze uda się jej coś znaleźć. Jednocześnie pozwoliła się zauważyć magom i miała nieco ubawu z ich rozmowy w rodzaju "cicho, jeszcze nas nie zauważyła, nie spłosz jej". Idąc za bardzo słabym tropem, Luna znalazła dół, cuchnący przerażonym kotem i przerażonym miślęgiem. To drugie niezbyt ją zmartwiło, ale to pierwsze jak najbardziej.
Na szczęście (dobry rzut) Lunie udało się wyczuć też człowieka, bardzo słaby ślad. Wystarczył on jednak, by doprowadzić ich do stacji przeładunkowej na granicy lasu.

Luna weszła do środka, i odszukała lokalnego kota - na szczęście całego i zdrowego - którego zapach wyczuła w dole. Od kota dowiedziała się, że zły dwunóg, ale nie ten tu (strażnik) tylko inny, go złapał, użył na przynętę i potem odniósł tutaj. Dowiedziała się też, że w jednej z latryn jest coś należącego do ciemiężyciela.

Długo się nie zastanawiając, Magda i Krystian weszli na teren stacji, podając się za urzędników unijnych, sprawdzających, czy stacja kwalifikuje się do nagrody. 

Cieć, o dźwięcznym imieniu Ewaryst, był urzeczony Magdą i bez większych oporów (choć po telefonicznym sprawdzeniu gości), odpowiadał na pytania. Dowiedzieli się, że ostatnio był tu syn właściciela, złapał kota, poszedł do lasu, wrócił z workiem grzybów.

Wszystko wskazuje na to, że syn właściela, Jan Pyszek, złapał oba miślęgi, używając kota jako przynęty.
Niewiele myśląc, Krystian buchnął worek z latryny, podczas gdy Magda z pomocą Luny odwracały uwagę ciecia. Następnie Luna odeszła, (w końcu kot) i wróciła do Krystiana, a Magda została jeszcze chwilę, by zająć ciecia.

Krystian otworzył worek i rzeczywiście znalazł dwa miślęgi, związane jak balerony i zakneblowane. Gdy uwolnił jednego i odkneblował drugiego, okazało się, że te dwa miślęgi są piekielnie (jak na miślęgi) inteligentne. Z tego, co miślęgi powiedziały, wynikało, że ten, kto je złapał, musiał rzucać na nie zaklęcia. Inteligentne stały się dopiero po uwięzieniu. Krystian wyczuwał magię działającą na stworki i postanowił zbadać jednego z nich. Niestety, coś nie do końca poszło tak, jak miało, i jego zaklęcie, zamiast tylko określić stan miślęga, spowodowało gwałtowne dokończenie procesu zmian. Miślęg (rozwiązany) rzucił się na Karystiana i mocno go poszarpał. Na to dotarła Luna, która niewiele myśląc, rzuciła się na stworka i go przypłaszczyła do ziemi.
Ciężko ranny Krystian wybrał numer Magdy, ale nie był w stanie mówić. Luna odezwała się za niego, informując Magdę, że miślęg go prawie zabił.

Ale, mimo wszystko, Krystian dowiedział się tego, co chciał. Zaklęcie na miślęgach co prawda daje im inteligencję, ale powoduje również wzrost agresywności.

Magda i Krystian odnieśli miślęgi do Powiewu. Luna, uznając, że nie chce mieć z nimi nic wspólnego, postanowiła zostać i zapolować na ciemiężyciela. Krystian zostawił jej na futrze pluskwę tak, by mogła się z nimi kontaktować.

I rzeczywiście, ciemiężyciel przyjechał do staci przeładunkowej. Lokalny kot czym prędzej rozpłynął się w powietrzu a Luna uznała, że musi oznaczyć pojazd ciemiężyciela (obsikała koła) 
Pyszek szybko zorientował się, że jego więźniowie uciekli i zrobiło mu się głupio... i trochę się przestraszył.
Co ciekawe, kiedy Luna podeszła do Jana Pyszka, ten nie wykazał żadnego zainteresowania kotem. Kotka zatem podrzuciła mu swoją pluskwę i - ponieważ nie udało się jej dostać do samochodu - spokojnie czekała.

Poinformowani o spotkaniu, Krystian i Maga podjechali po Lunę, po czym udali się tam, dokąd prowadziła ich pluskwa. Prosto do mieszkania łowcy miślęgów.

Tam, po krótkiej i treściwej rozmowie dowiedzieli się, że mężczyzna ma brata, niespełna rozumu, i że jego "mentor" - on sam nie wie, kto to - podarował mu księgę z zaklęciami, w tym z zaklęciem poprawiającym zdolności umysłowe. Nie chciał go rzucić bez przetestowania na brata, więc wykorzystał miślęgi ("bo to przecież wspólne dobro")
Mag był przerażony skutkami, jakie zaklęcie miało na nieszczęsne miśki i postanowił nie ufać bezkrytycznie swojemu mentorowi; oddał również księgę zaklęć.