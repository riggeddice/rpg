---
layout: inwazja-konspekt
title:  "Siedmiu magów - nie Mausów"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151202 - Zdobycie węzła Vladleny (HB, AB)](151202-zdobycie-wezla-vladleny.html)

### Chronologiczna

* [160217 - Sojusz według Leonidasa (LB, AB, DK)](160217-sojusz-wedlug-leonidasa.html)

### Logiczna

* [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)

### Inne

Kończy się w okolicach misji:
* [151202 - Zdobycie węzła Vladleny (HB, AB)](151202-zdobycie-wezla-vladleny.html)

## Wątki

- Szlachta vs Kurtyna
- Powrót Mausów
- Srebrna Świeca Ozydiusza Bankierza

## Kontekst misji
- Ktoś napuszcza na siebie Świecę i KADEM.
- Ktoś napuszcza na siebie Kurtynę i Szlachtę.
- Anna Kozak winna oddać artefakty Andrei.
- Zenon Weiner zniknie z pola widzenia w drugiej części misji.
- Świeca jest zajęta problematyką Arazille, zwłaszcza Diakoni.
- Ozydiusz Bankierz pracuje nad cruentus reverto.
- Siedmiu magów czeka straszny los; Szlachta oddała ich Oktawianowi Mausowi by przekształcić w Mausów.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Działania archeologiczne Dominika Bankierza pod uważnym okiem Inkwizytor Adeli Maus.

### Kontekst ogólny sytuacji
- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin "szuka" Tymotheusa.
- Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).

### Notatki Kić (jako Andrea)
Ludzie/magowie:
- Salazar Bankierz - nic nie pamięta, wymazał sam siebie, on powiedział Annie o poligonie
- Oktawian Maus - ! szlachciur, potencjalnie zmanipulowany, fanatyk, wszystko dla Mausów, według własnych słów wysoko w hierarchii
- Ozydiusz Bankierz - może nie całkiem zły, ale wciąż dupek
- Aurel Czarko - opracaował włam na KADEM, kto za nim stoi? Szlachta?
- Anna Kozak - omg. świadek jak rzadko. była na poligonie
- Dominik Bankierz - magiczny lekarz, mag szlachty, któremu zaufał Salazar. Razem z Wiktorem i Oktawianem byli w Kotach podczas Spustoszenia.

Inne:
- Pancerz - zakodowany przekaz zawierał kołysankę, lekko zmodyfikowaną wobec "normalnej" wersji śpiewanej przez magów. Najpewniej kod kontekstowy. Wyjaśnienie: kołysanka w wersji łączącej Jadwigę i Aurelię Maus. Ale czemyu ta piosenka w tym kontekście?

Problemy/pytanie:

- Dlaczego Weiner był celem "trzeciego" Spustoszenia? (?)
- Dlaczego wydział wewn nic nie robi w sprawie ataku na KADEM? (?)  Potencjalnie szef jest w Szlachcie. Ale czy to jest wytłumaczenie? Była mowa o ukrytej sile - co nią jest? Może im służy?
- Czemu Tamara jest celem? bo stała na drodze?
- Co dostał Oktawian i czemu jest to niebezpieczne? 7 magów do przemiany w Mausów na terenach zakazanych. 
- Oktawian nie pamięta, czemu był przygaszony - wymazano go? A może sam się wymazał?
- Co dać Oktawianowi, żeby zmienił zdanie i zostawił Szlachtę a zaczął dążyć do jedności ze Świecą?
- Milena Diakon - na pół Maus - co zrobić?
- Dwoje ludzi ma dostęp do artefaktu pozwalającego im zachować pamięć na skutek działań Anny Kozak
- Artefakty są nadal w świecie ludzi w rękach bliźniaczek
- Czy (jeśli nie, dlaczego) Ozydiusz szuka Tamary? w końcu zgubił terminusa... (?)
- Wykopaliska - prowadzone przez Bankierza ze Szlachty, mogą doprowadzić do odkrycia istotnych dla Mausów faktów.
- Jolanta Sowińska bya na cmentarzu zanim spłonął - kto jej powiedział?

## Punkt zerowy:

- Nadchodzi Józef Anioł Bankierz zrobić porządek ze wszystkim i wszystkimi - a zwłaszcza Szlachtą (info do Diany Weiner). Podobno wezwany przez Kurtynę.
- Plany i sposób dostania się do bazy Emilii zostały złamane i przechwycone przez Szlachtę (info do Emilii). Przygotowują się do ataku na Kurtynę.
- Ozydiusz Bankierz otworzył "Labirynt Zielonej Kariatydy" do rozwiązania .
=== Szlachta jak go rozwiąże, dostanie szacun i udowodni, że potrafi coś zrobić; polityczny bonus.
=== Kurtyna jak go rozwiąże, może się zabezpieczyć defensywnie.
- "ktoś" powiedział o TechBunkrze Weinerów, który pomoże w rozwiązaniu problemu Labiryntu Zielonej Kariatydy.
- TechBunkier Weinerów ma w sobie Irytkę Sprzężoną, co nie pomaga w napiętej sytuacji.

## Misja właściwa:

Agrest przebił się w końcu przez blokadę założoną przez Aleksandra Sowińskiego na danych Oktawiana Mausa. To, co było ciekawe:
- Oktawian Maus często zgłaszał niedopatrzenia i problemy związane z traktowaniem Mausów przez Świecę. Sowiński je wszystkie wyciszał.
- Wynika jednoznacznie z tych rekordów, że Oktawian Maus zwrócił się ku Szlachcie bo nie było innej opcji. Sowiński pchnął go do Szlachty.
- Oktawian zaproponował użycie pewnego serum Mausów; teraz, jak nie ma Karradraela, da się przekształcić krew Mausa tak, by posłużyła do bycia inhibitorem czarowania i serum prawdy. Innymi słowy, mag, któremu się to wstrzyknie przez czas trwania iniekcji może czarować tylko na żądanie i musi mówić prawdę. Oktawian powiedział, że jest wiele takich sztuczek w jego arsenale i z przyjemnością będzie ze Szlachtą współpracował. Więcej, nawet gdyby Karradrael się pojawił (unlikely), to tylko pochodna; Karradrael nie ma nad tym kontroli. A kontrolerem nie musi być Maus.
- Sowiński osobiście doprowadził do takiej sytuacji Mileny Diakon. W ten sposób ma kontrolę nad Oktawianem - Świeca NIE chce pomóc, Diakoni NIE chcą pomóc. Tylko Szlachta może pomóc.
- Oktawian Maus wytrwale szuka Karradraela i Renaty Maus.

Z tego wynika, że Aleksander Sowiński jest agentem Szlachty. Agrest też tak uważa.

Korzystając z kontaktów w półświatku i z bycia brokerem, Andrea zdecydowała się poszukać - wiedząc, że transformacja w maga rodu trwa ~2 miesiące - mało ważnych magów którzy znikali w tym okresie. Drugim kanałem poszukała znajomych sobie terminusów którzy nadawaliby się do akcji odbijania tej siódemki magów (a może podmienienia tej siódemki?).

Żeby dowiedzieć się odnośnie tego gdzie są ci magowie, Andrea potrzebuje wsparcia. Fear not! Zawsze pozostaje tien Judyta Karnisz, służbistka, hitman i osoba bez przyjaciół. Idealna. Tylko trzeba porozmawiać z Ozydiuszem.

W komnatach Lorda Terminusa Kopalina, Ozydiusz wygląda na jeszcze mniej irytującego niż zwykle. Na wejściu powiedział jej, że w cruentus reverto płynie krew Diakona. Do pomieszczenia wszedł Kermit Diakon i spytał Ozydiusza, czy ma uruchomić i ogłosić Labirynt Zielonej Kariatydy? Ozydiusz potwierdził. Andrea uniosła brew; Labirynt jest bardzo drogim artefaktem i bardzo rzadko się go wykorzystuje. To takie wielkie igrzyska, kosztowne i zyskowne dla grupek. Ozydiusz powiedział, że Szlachta dowiedziała się, że ze Świecy przybywa Józef Anioł Bankierz, terminus inkwizytor o nieskończonych uprawnieniach by zrobić porządek w Kopalinie. Kurtyna się dowiedziała, że Szlachta ma wejście do ich bazy. Ozydiusz musi odwrócić ich uwagę; dzięki Kariatydzie jest szansa, że zostawią się w spokoju i zajmą się sobą. Mimo Arazille, warto teraz zrobić Zieloną Kariatydę. A że więcej grup ze Świecy dołączy...

Andrea z przyjemnością przechwyciła Judytę. Jest gotowa do działania. Jednocześnie dostała sygnał od Anny Kozak. Młoda czarodziejka chce się spotkać z Andreą i oddać jej wszystko. Andrea kazała Judycie się przygotować a sama ruszyła na spotkanie z Anną.

Ania jest gotowa. Oddała artefakty i chce odejść. Nie chce nic powiedzieć, ale Andrea wyciąga. Anna Kozak sprawiła, że dziewczyny zapomniały o świecie magów. Zjednoczyła rodzinę. Jako, że były imigrantkami ze wschodu a ich matka była elementem "Wyżerki" pod Kopalinem, Anna używając artefaktów i umiejętności terminusa doprowadziła do tego, że "Wyżerka" została zamknięta, źli ludzie ukarani, a wszystko skończyło się dobrze. Nie ma dość facepalmów na świecie, by Andrea mogła przekazać, co czuła...

V - TechBunkier zostaje ujawniony przez Tadeusza Barana

Judyta Karnisz nie jest detektywem, ale jest zabójczynią i wie, kiedy siedzieć cicho.

Andrea dostała feed na hipernecie. Tajemniczy Głos na Hipernecie (uprawnienia Andrei zidentyfikowały Tadeusza Barana - Andrea ma kolejny facepalm) wysłał sygnał kierunkowy do 10 magów (8 Szlachty i 2 Kurtyny; ale tych Kurtyny to była plotka, że są ze Szlachty) o tym, że jest TechBunkier (opuszczony, Mausów), który zawiera rzeczy dzięki którym łatwiej będzie dostać się do Kariatydy. Wysłał lokalizację. Powiedział, że wdzięczność nie jest mu potrzebna, ale liczy na datki. Podał anonimowy numer... a lista adresowa była CC, nie BCC.

Oktawiana Mausa nie było na tej liście. Żaden Maus nie wie. Andrea się uśmiechnęła złośliwie. Jako Baran (prawo retransmisji), FWDnęła jego wiadomość do wszystkich innych grup chcących się przyłączyć do Kariatydy. Niech będą wszyscy na równych prawach. I niech w tych grupach będzie jakiś Maus...

A sama ruszyła na poszukiwanie Siedmiu Magów nie-Mausów.

ESCALATION_160227

Andrea chce: dowiedzieć się, gdzie są ci magowie.
Druga Strona chce: znaleźć Andreę i ją unieszkodliwić.

Krok 1: 
Andrea: chce określić zakres; mniej więcej osoby i miejsce. By dalej z tym pracować.
Druga Strona: brak

Andrea zdecydowała się na rozpuszczenie szerokiej sieci poszukiwań. Niech terminusi jej powiedzą, jacy magowie z jakich gildii gdzie byli znikającymi, jakie są obszary, okres około miesiąca... Andrea Wilgacz big data. vs Elita_Normalnie. 22v18. Sukces. Udało jej się znaleźć informacje wskazujące na powiązanie z przemytem magów. Magów, którzy znikali, ale nikt się nie upomniał o nich - bo byli "niczyi". Trzy, cztery takie plotki w tym czasie wszystkie ogniskowały dookoła terenów poza kontrolą Świecy. 

V - magowie wchodzą do TechBunkra

Krok 2: 
Andrea chce: znaleźć lokalizację przez świat ludzi.
Druga strona: poturbować i okraść Andreę.

Oktawian nie korzystałby z zasobów Mausów; musi korzystać z zasobów Szlachty albo jeszcze innych. Korzystając ze swoich kontaktów w półświatku Andrea łączy swoją wiedzę o potrzebach magów i co trzeba do przekształcenia z faktyczną logistyką; czy w tych obszarach ktoś gdzieś jakoś nie potrzebował, nie kupował, w tą stronę. Judyta stanowi osłonę Andrei i zabezpieczenie. Znowu vs Elita_normalnie. Andrea is a big spender (-resource) i trwa to trochę czasu (-time); 23v18 -> sukces. Andrei udało się zlokalizować OBSZAR; strefa poza jurysdykcją i kontrolą Świecy, 100 km od Kopalina, miejscowość Sznurkaty.
Próba pobicia i okradzenia: zawodowiec w dobrych warunkach: 16v17-> Judyta i Andrea dały radę obronić się przed potencjalnymi (i faktycznymi) atakami. Ciemne speluny, niewłaściwe miejsca, dwie kobiety... i poradziły sobie. Ozydiusz byłby zadowolony... gdyby nie to, że nie jest.

It was a drop point. Andrea nie wie dokładnie gdzie, ale wie dość dużo.

V - starcia w TechBunkrze. Zwycięzca: 1k20; zdobył zawartość Ktoś Inny. Trzecia strona. Nie Szlachta, nie Kurtyna. 
(dodany krok Dark Future: walki poza TechBunkrem)

Krok 3: 
Andrea chce: plausible deniability przed Oktawianem; preparuje dowody, że to byli "inni". Taki "odpowiednik Kurtyny". Siła nie ze Świecy.
Druga strona: brak

Andrea wykorzystała reputację istniejącej grupy działającej na tym terenie. "Ratujące Mopsy". Specjalnie wzięli taką nazwę, by chronić się przed Archerem. Andrei to perfekcyjnie pasuje; wrobi ich, że to niby oni uratowali grupę magów. Plus, im to pomoże. Stworzyła całkiem fajną historię mającą skutecznie ukryć Andreę i jej ludzi przed wyszukiwaniem ze strony Oktawiana itp. Tak skutecznie przygotowała z pomocą Agresta sytuację, tak skutecznie stworzyła fałszywe ślady, tak zmanipulowali sytuacją, że cokolwiek się stanie, nikt nie wpadnie na działania magów z Kopalina. A już na pewno nie na Andreę. Oktawian ma plausible reason żeby wierzyć, że to ktoś zupełnie inny.

V - Oktawian Maus dociera na wykopaliska (1/2)

END OF ESCALATION_160227

Andrea napisała liścik do Ozydiusza Bankierza dziękując za użyczenie Judyty i za jej pomoc. Zaznaczyła, że Judyta wykazała się kompetencją i intelektem. Wie, że Ozydiusz to zignoruje. Ale napisała.

Z interesujących plotek: Arazille nasiliła działania. Magowie ją widzieli. Widzieli jak działała. Dokładna lokalizacja nieznana; daleko poza Kopalinem. Tutaj magowie raczej mogą odpocząć; raczej by się spodziewali, że Arazille zaatakuje jak tylko będzie w pobliżu. Jak zawsze robiła.
Z innych plotek: walki o TechBunkier były ciekawe. Magowie trafiali do szpitala. Pierwsza grupa przeszła do Kariatydy i została wymiętoszona. Polegli. Szlachta i Kurtyna nadal krążą i próbują dojść do działania BEZ TechBunkra... 
Na pewno nie o to chodziło Ozydiuszowi, ale zawsze coś.

Agrest obiecał Andrei, że zmontuje jej zespół zdolny do odbicia tamtych siedmiu magów. Agrest zorganizował Andrei pięciu terminusów. Skrzydło. A Andrea bierze coś, co zmienia jej wygląd. Skrzydłem dowodzi... Tamara Muszkiet (ku wielkiemu zaskoczeniu Andrei). W nowym kombinezonie bojowym. Z pozostałych terminusów Andrea rozpoznaje jeszcze Leokadię Myszeczkę. Andrea realnie ocenia swoje szanse - lepiej, by Tamara dowodziła stricte militarnie a Leokadia jest lojalna i niebezpieczna. Andrea poważnie podejrzewa, że reszta tutejszych terminusów to też takie "resztki" i jednostki, którymi zaopiekował się Agrest. Terminusi dostali kontekst okrojony z rzeczy niepotrzebnych i czas zlokalizować przeciwnika.

ESCALATION_160227

Krok 1: Identyfikacja
Andrea chce zlokalizować Fortecę wroga.
Przeciwnik chce zlokalizować ekipę Andrei, ale nie ma jak.

Andrea wzięła porządną mapę okolicy i zaczęła szukać potencjalnej lokalizacji Fortecy. Daje to kilka potencjalnych obszarów. Mając listę tych obszarów i rzeczy potrzebnych do transformacji maga w członka innego rodu, i wiedząc gdzie jest punkt zrzutu, Andrea jest w stanie ograniczyć kilka miejsc. Mając te dane, Andrea wysłała zwiadowców. Niech znajdą potencjalne miejsca bazy przeciwnika.
Zwiadowca idzie na akcję, nie Andrea. 19v15. Zrobią to wolniej; i tak w dzień nie zaatakują. Czyli 22v15, bez rzutu.
Czas jest potrzebny i tak na zaplanowanie akcji. Tamara dowiaduje się, że oni mogą nie chcieć wracać z nimi; mogą być już skonwertowani. Odpowiednio plan będzie zaadaptowany.

Ż: W jakim miejscu znajduje się Forteca?
K: Miejsce z jedną wygodną drogą; inne będą strzeżone. Jest nieźle bronione (potencjalnie). Leśniczówka z podziemnym obszarem.

Szczęśliwie zwiadowca znalazł drogę awaryjną (ewakuacyjną).

V - Oktawian Maus dociera na wykopaliska (2)

Krok 2: Infiltracja
Andrea chce wbić się do Fortecy.
Przeciwnik chce odeprzeć napastników. Nie ma jak.

Oktawian musiał korzystać z własnych środków. Ale on nie ma dużych środków. Andrea wie, że Oktawian zamawiał raczej proste rzeczy i zapasy żywnościowe na ~10~15 osób. To bardzo dobra wiadomość; jak się już wbiją, to nie ma problemu. Zwiadowca (Melisa) zaraportowała, że będzie bardzo trudno dostać się do środka, bo teren jest szczególnie pancerny i chroniony przed wejściem. Leokadia zauważyła z uśmiechem, że tylko potrzebny jest większy ładunek. Andrea zauważyła, że przecież następnego dnia jest kolejna dostawa jedzenia; mogą po prostu poczekać. A przy okazji Leokadia może przygotować swoje ładunki i inne eliksiry; Tamara zmieni plan by uwzględnić dostawców. Dobrze byłoby też zablokować możliwość alarmu. Tamara spróbuje coś zrobić z łącznością. Leokadia zablokowała wyjście awaryjne.

V - "wypadek na wykopaliskach" (Dominik, Adela, Oktawian znikają)

Jedzenie zostało dostarczone do punktu zrzutu. Zespół czeka. Tamara kazała wszystkim włączyć stealth mode i schować się między skrzyniami z jedzeniem. Po kilkunastu minutach z ziemi wychynęła głowa czerwia i ich połknęła. Zespół jest transportowany czerwiem wraz z jedzeniem. Udało im się wniknąć do środka fortecy.

Krok 3: Przejęcie
Przeciwnik chce wykryć Zespół (16v14). Czerw otworzył pysk prosto na kompleks kamer i czujników; szczęśliwie, stealth suity dały radę. Dwóch magów którzy ZUPEŁNIE nie przejmowali się zasadami bezpieczeństwa zaczęli rozładowywać jedzenie; terminusi Tamary zachowali zimną krew i czekali. Tamara kazała sygnałami hipernetowymi, żeby poczekali i jak tamci będą wychodzić, wymknąć się wraz z nimi. Sama uruchomiła projektor; niech obraz dla kamer i czujników będzie taki jak spodziewany. Scout suit Tamary jest bardzo zaawansowanym urządzeniem; udało jej się po chwili skalibrować czujniki na całkowite maskowanie.

Tymczasem lokalni magowie zaczęli kończyć wyładowanie i przygotować się do wyjścia. Tamara przekazała, że wszystko jest ok. Sama wyszła pierwsza; tam w korytarzu TEŻ są czujniki i kamery, ale wyłączone. Lokatorzy wyłączyli system zabezpieczeń...
Andrea z niedowierzaniem kręci głową... to nie jest niekompetencja, to zwykła, straszna, ludzka mentalność...

Tamara wydała rozkaz i dwóch magów padło nieprzytomnych. Nawet nie wiedzieli co ich trafiło. Tamara wydała rozkaz rozdzielenia się; idą w parach. Tamara # Leokadia, Andrea # Melisa. 

Mając ciągłą komunikację ze sobą, rozłażą się po bazie by bezpiecznie przejąć wszystko co jest interesujące. Andrea i Melisa dotarły do pokoju kontrolnego. Jest tam jeden mag i dwa demony. Typowe demonetki. Ładnie wyglądają; powinny służyć do walki, ale w tej bazie nic nie jest zrobione poprawnie... dosłownie służą za ozdobę. Nie zmienia to faktu, że są niebezpieczne.

Demonetki spróbowały wykryć przeciwników; Andrea zauważyła, że demonetka prawie podskoczyła a w jej oczach pojawiło się zrozumienie; Melisa wystrzeliła i Andrea też. W demony. Żeby całkowicie rozproszyć i usunąć demonetkę potrzebne jest (19). Wraz z Melisą skupiły ogień na jednej demonetce; nie udało się, ale demonetka została ciężko ranna (14 # szok).

Mag jeszcze się nie zorientował, ale druga demonetka rzuciła się poszarpać Andreę; zaczęła się szamotać z Andreą (12). Stealth suit nie daje Andrei zbyt dużej defensywy, ale (13) zawsze coś. Demonetka skutecznie rozerwała stealth suit i Andrea została widoczna.

Melisa skupiła się i dokończyła poprzednią demonetkę. Mag zaczął składać zaklęcie. Andrea wysłała Melisie sygnał "bierz maga" a sama skupiła się na demonetce; Andrea mimo nieciekawej sytuacji doceniła fakt, że przeciwnicy NIE dali demonetkom broni. Bojowe demony są nieuzbrojone... Andrea zwarła się, odepchnęła demonetkę i poraziła ją swoim działem. Demonetka jest ciężko ranna, ale zdolna do dalszej walki. Za sobą Andrea usłyszała dźwięk jaki wydaje mag kopnięty w jaja stealth suitem. 

Demonetka spróbowała ostatniego, desperackiego ataku (10v13); zanim doleciała do Andrei, działko ją zdestabilizowało i exterian się zdematerializował. Obie terminuski odwróciły się do maga. Melisa stwierdziła, że przejmują kontrolę nad tą bazą w imię "komunistycznego ruchu równouprawnienia gryzoni". Mag zaczął współpracować.

Baza została przejęta.

V - ślady wypadku zostają zatarte

END OF ESCALATION_160227

Tamara odcięła wszelkie komunikacje oraz zaczęła pracować nad zrozumieniem co się dzieje w tej bazie.
Medyk WPIERW zajął się Andreą (to tylko draśnięcie), potem zabrał się do przeskanowania magów w bazie. Złapano 6 jeńców. I wszyscy 7 porwanych magów (przyszłych Mausów) nie zostali w Mausów jeszcze przekształceni. W prosty sposób da się to jeszcze odwrócić.

Z Andreą skontaktował się Agrest. Ostrzegł ją, że sytuacja się trochę zmieniła. Na wykopaliskach doszło do wypadku. Wszyscy zginęli... lub zniknęli.
- Oktawian Maus
- Dominik Bankierz
- Adela Maus (inkwizytor!!!)

Agrest ma potwierdzoną śmierć dwóch magów na wykopaliskach. Tzn, są zwłoki. A raczej to co z nich zostało...

## Wynik misji:
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach
- Andrea i oddział Tamary skutecznie odbili magów, których Oktawian chciał zmienić w Mausów
- Labirynt Zielonej Kariatydy odpalony przez Ozydiusza Bankierza uspokoił nastroje w Kopalinie...
- ...co nie zmienia tego, że walczą ze sobą naprawdę ostro. PONAD 3 strony.


## Dark Future:

To buy:
V - Oktawian Maus dociera na wykopaliska (2)
V - "wypadek na wykopaliskach" (Dominik, Adela, Oktawian znikają)
V - ślady wypadku zostają zatarte
- ślady wypadku wskazują na Bankierzy. A zwłaszcza Ozydiusza.

V - TechBunkier zostaje ujawniony przez Tadeusza Barana
V - magowie wchodzą do TechBunkra
V - starcia w TechBunkrze. Zwycięzca: 1k20 (LESS: Szlachta, MORE: Kurtyna)
- odpala pierwsze stadium Irytki Sprzężonej (lokalnie).
- krwawe starcia w Labiryncie
- Arazille pożera Labirynt
- odpala pierwsze stadium Irytki Sprzężonej (w Kopalinie).
- dwóch magów Szlachty zniknęło w wyniku działań w Labiryncie.

- Judyta i Elizawieta mają ciężkie przeprawy w mieście
- z trudem Judyta ewakuuje Elizawietę; sama łapie Irytkę.
- Oktawian odkrywa informacje na temat Cruentus Reverto i ma możliwość tego użyć.

# Streszczenie

Agrest przebił się przez dane Oktawiana Mausa; ów zwrócił się ku Szlachcie by pomóc Mausom. A Aleksander Sowiński (przywódca Szlachty) doprowadził do takiego stanu Mileny Diakon by kontrolować Oktawiana. Andrea uratowała siedmiu porwanych przez Oktawiana magów by nie stali się Mausami, zdobywając z siłami Agresta (min. Tamarą) ukrytą bazę Oktawiana. Tadeusz Baran ogłosił Szlachcie i (przypadkowo Kurtynie) info o starym TechBunkrze gdzie jest coś co może pomóc w złamaniu Labiryntu Zielonej Kariatydy Ozydiusza. Anna Kozak oddała wszystkie niewłaściwe artefakty Andrei. Agrest przekazał Andrei, że na wykopaliskach była katastrofa - zniknęli lub zginęli Dominik Bankierz, Oktawian Maus i... Adela Maus.

# Zasługi

* mag: Andrea Wilgacz, która doprowadziła do uratowania siedmiu * magów przed zostaniem Mausami - tak, by Oktawian NIC nie podejrzewał.
* mag: Aleksander Sowiński, szef wydziału wewnętrznego Kopalina i wysoko postawiony członek Szlachty. Przełożony Andrei. Niestety.
* mag: Marian Agrest, szara eminencja, który wysłał Andrei kluczowe informacje oraz zapewnił oddział do przejęcia Fortecy Oktawiana Mausa.
* mag: Jan Anioł Bankierz, inkwizytor z ogromnymi uprawnieniami, który PODOBNO miał się pojawić w okolicy by rozwiązać problem Szlachty i Kurtyny.
* mag: Ozydiusz Bankierz, który pokazał zmysł polityczny, którego po nim nikt by się nie spodziewał odpalając Labirynt Zielonej Kariatydy. Wypożyczył Andrei Judytę.
* mag: Judyta Karnisz, obstawa Andrei w ciemnych zaułkach i meandrach żulpolityki. Obroniła ją przed napadem.
* mag: Anna Kozak, która przyznała się Andrei do bycia "Bat* magiem". Andrea podejrzewa, że ta idealistka w swoim łóżku nie umrze.
* mag: Tadeusz Baran, zarobił dużo więcej niż się spodziewał wysyłając do Szlachty (i przypadkowo Kurtyny) (i wszystkim) informacje o TechBunkrze do Kariatydy.
* mag: Saith Catapult jako Melisa, zwiadowczyni grupy szturmowej pod kontrolą Tamary Muszkiet odbijającej siedmiu "prawie Mausów".
* mag: Tamara Muszkiet, głównodowodząca akcją odbijania siedmiu prawie Mausów i ekspert od wojny elektronicznej. Dostarczyła sprzęt wszystkim.
* mag: Leokadia Myszeczka, pod Tamarą, bardziej pełniła rolę mięśnia niż alchemika na tej akcji. Zaspawała wyjście awaryjne, by nikt nie uciekł.

# Didaskalia

- obj: Labirynt Zielonej Kariatydy, bardzo trudna i zmieniająca się często zagadka wymagająca skomplikowanej pracy zespołowej i dająca niemałe korzyści polityczne; popularna w Świecy.
- obj: Irytka Sprzężona, niezbyt groźna forma choroby magicznej, która zwiększa poziom wściekłości i powoduje spontaniczne samoczarowanie emocjonalne. Przenosi się przez zaczerpnięcie Skażonej magii.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Kompleks centralny Srebrnej Świecy
                        1. Pion dowodzenia Kopalinem
                            1. Pokoje Lorda Terminusa
        1. Małopolska
            1. Powiat Okólny
                1. Sznurkaty
                    * Forteca Oktawiana Mausa

# Czas

* Dni: 5