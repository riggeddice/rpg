---
layout: inwazja-konspekt
title:  "Aniołki Marcelina"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160403 - Wiktor kontra KADEM i Świeca (SD)](160403-wiktor-kontra-kadem-i-swieca.html)

### Chronologiczna

* [151216 - Między prawdą i fikcją Arazille... (AB, DK)](151216-miedzy-prawda-i-fikcja-arazille.html)

### Logiczna

* [151212 - Antyporadnik wędkarza Arazille (HB, AB, DK)](151212-antyporadnik-wedkarza-arazille.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".

## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Srebrna Świeca szuka Arazille.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Hektor chce zdjąć ograniczenie z Diakonów przeciw Blakenbauerom; ale jeszcze nie może
- Skubny zdobył koncesję na kanał telewizyjny. Edwin przyznał się że stoi za Skubnym.
- Hektor, Edwin, Marcelin i Margaret znaleźli sojuszników: Kacper Weiner i Esme Myszeczka.
- Blakenbauerowie chcą się skupić do ataku na Ozydiusza Bankierza by poprawić reputację swego rodu
- Tatiana użyła Fire Suit przeciw Hektorowi i przegrała.
- Węzeł Vladleny został przechwycony przez Blakenbauerów.
- Arazille wyłączyła Rezydencję Blakenbauerów
- Marcelin z Wandą Ketran (chronioną przez Arazille) opuściła Rezydencję

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Misja właściwa:

Dzień 1:

Tymotheus wezwał do siebie Klarę i Leonidasa. Powiedział, że Rezydencja jest wyłączona, Blakenbauerowie w częściowej ruinie a Marcelin i Wanda uciekli. Wanda jest najpewniej agentką Arazille; jeśli to zaraźliwe (a Marcelin wyłączył przecież Hektora), to Wanda musi zginąć. Jeśli nie zaraźliwe, profesor Tymotheus Blakenbauer chce ją tutaj. Do badań.
Leo i Wanda mają szeroki mandat by robić co uważają za słuszne. 

Nie wiadomo w jakim stanie jest Marcelin czy Wanda; jak mocno na nich działa Arazille.

HMovement: "- Elizawieta Zajcew, którą Marcelin przekonał o potrzebie pomocy i ochrony. Też: kontakt z Joachimem."

Klara wybiera się do Rezydencji. Chce zdobyć jak najwięcej; Rezydencja jest w wiecznej pętli, ciągle uszkodzona. Magowie w Rezydencji są półprzytomni. Do niczego się nie nadają...
Zdecydowała zatem skupić się na czymś prostym: zrobić więzienie dla Marcelina w Rezydencji; tak, by Marcelin tam został i nie wyszedł. Kataliza i oparcie o moc Rezydencji; też elementy technomancji - czujniki itp. Zajęło jej to chwilę, ale upewniła się, że Marcelin ani Wanda z tego nie wyjdą. Niestety, Wanda gra pierwsze skrzypce - Marcelin wyrzucił telefon.

W tym czasie Leonidas przygotowuje wielkie przedstawienie. "Matka" Marcelina ("pani Blakenbauer") przed kamerą biadoli i narzeka, że Wanda Ketran (policjant to potwierdza i powie, że jest anarchistką jakich mało) zbałamuciła, roznarkotyzowała itp jej syna - Marcelina Blakenbauera. Ważny jest ten policjant - on mówi, że Wanda jest podejrzewana o straszne rzeczy, że są na nią dowody, że uciekła i dlatego nie można jej znaleźć, była zatrzymana itp. I poszło to w wielu różnych kanałach, łącznie ze SkubnyTV. Marcelin nie posłuchał Wandy. Character assassination i w ogóle - wziął Elizawietę (sojuszniczkę) i poszedł do Rezydencji skopać Hektora.

Klara przygotowała pułapkę na Marcelina. Min. ma ze sobą Borysa. 

Marcelin nie przyszedł sam. Przyszedł z Elizawietą, ukrytą pod niewidzialnościami i defensywami.

Borys przygotował strzałkowca i z zaskoczenia strzelił lapisowanym pociskiem strzałkowym w Marcelina Blakenbauera. Z trucizną. Pocisk przeszedł przez dwa rzędy tarczy (lapisowany) po czym obezwładnił nieszczęsnego Marcelina trucizną. 

Elizawieta zaczęła materializować zaklęcie defensywne; Klara rozproszyła je odpromiennikami Rezydencji. Elizawieta wyłączyła czar niewidzialności i zakazała Borysowi zabierania ze sobą Marcelina. Jest to mag Srebrnej Świecy. Klara chwilę spowolniła Elizawietę; potem przyszedł Leonidas. Używając swoich umiejętności oszustwa rozbroił ją i przypomniał Elizawiecie ten smutny fakt gdy Elizawieta wpakowała się w kłopoty polityczne (nic nie insynuując), po czym powiedział, że mają mały kłopot - sprawy rodowe - jak by iść "by the book" to trzeba by wezwać Ozydiusza Bankierza. Ale seiras Blakenbauer jest w drodze i nie ma problemu. I Leonidas wezwał Tymotheusa, by ten udawał Ottona.

Faktycznie, "Otton" wszedł do pomieszczenia. Rozejrzał się i opieprzył Leonidasa, że ten go poprosił. Leonidas na to, że albo to, albo lord terminus Ozydiusz Bankierz. "Otton" burknął, że trzeba było ściągnąć Ozydiusza i sobie poszedł. Elizawiecie zrobiło się przykro - technicznie rzecz biorąc, wpadła w lekkie kłopoty.

Podczas herbatki Elizawieta powiedziała, że Marcelin poprosił ją o eskortę i bezpieczeństwo; przedtem poprosił ją o skontaktowanie się z kuzynem Joachimem. Joachim jest w drodze; dociera do siedziby Świecy. Marcelin miał coś, czego chciał Joachim.

Marcelin został oddany Tymotheusowi. Tymotheus pogratulował im bardzo udanej akcji, po czym skupił się na Marcelinie. Po godzinie stwierdził co następuje:
- Marcelin jest Marcelinem. Nic mu nie zrobiono. Kontaktował się z Estrellą Diakon którą poprosił o udawanie Wandy w "Rzecznej Chacie". Estrella dowodziła częściowo od tej pory. 
- Estrella poleciła Marcelinowi Elizawietę. Przy okazji Marcelinowi Wanda powiedziała o Joachimie Zajcewie. Że się nią interesował i go spławiła.
- Wanda powiedziała Marcelinowi, że ona chce tylko być wolna. Nie będzie się mścić na rodzie, bo jest tam Marcelin. A on powiedział, że TEŻ chce być wolny.
- To była Wanda i Marcelin, nie sama Wanda. Zabawne jest to, że Wanda nie ma mocy Arazille; Marcelin nie został przez Arazille dotknięty.
- Wanda schroniła się w mieszkaniu kolegi; Stanisława Pormiena. To ten gość, który był "jej chłopakiem" i Alina dała mu wtedy w twarz.
- Wanda obiecała Marcelinowi, że będzie tam na niego czekać. Wanda jest wściekła na to, że Blakenbauerowie zrobili to co zrobili (z telewizją); jej rodzice będą mieli problemy. Osoby publiczne.
- Estrella wie, że Wanda jest "ludzką ofiarą Blakenbauerów", Marcelin błędny rycerz ją wydarł strasznemu Hektorowi (rodowi).

Tymotheus powiedział, że Wanda musi być przyprowadzona lub zabita.

HMovement: "- Wanda zmieniła lokalizację; na zaplecze "Działa plazmowego" by nie narażać Stanisława."
HMovement: "- Wanda nagrała anty-Blakenbauerowe nagranie celowane do świata ludzi a drugie do świata magów. Uploadowała ludzkie na YT."

Leonidas przygotował się by stać się Marcelinem. Klara wyszukała telefon Wandy - telefon znajduje się tam, gdzie powinien (w mieszkaniu Stanisława Pormiena).

Klara przygotowuje rój małych istot, małych robotów, które będą w stanie wyszukać Wandę, zidentyfikować ją i unieszkodliwić (uśpić). To ją powinno uśpić. I wysyła rój do mieszkania Pormiena, jednocześnie pilnując, by telefon był na miejscu. Tymotheus dał Leonidasowi kryształ quark - jak Leonidas uzna za potrzebne, ma dostęp do pamięci Marcelina w tej sprawie. I Leonidas wygląda jak Marcelin. A do roju dodane jest kilka robocików z kamerkami. Wizja dobra rzecz.

Wbili się - ale Wandy tam nie ma. Został telefon i komputer z wiadomością "Drogi Marcelinie, jeśli poszedłeś, to już nie wrócisz taki jaki byłeś - nie doceniasz potęgi swojej rodziny". Plan Wandy jest relatywnie prosty - niech Marcelin skontaktuje ją z Joachimem. Nie wiadomo, gdzie jest teraz: squat, Działo Plazmowe...

HMovement: "- Wanda wysłała wersję do ludzi do Dagmary Czeluść. a wersję anty-magów Estrelli Diakon."

Pies z tym wszystkim. Czas na hardcorowe działanie. Tymotheus dostarczył Leonidasowi ludzi (smutnych panów w garniturach) i na bazie pamięci Marcelina ci ludzie rozchodzą się po miejscach które (na bazie pamięci Wandy) są bezpieczne. Plus parę miejsc które pasują do jej profilu. I szukają Wandy Ketran. Na początek zainteresował się tematem Joachim Zajcew; z jego punktu widzenia to bardzo ciekawa sytuacja. Ale jeśli chodzi o Wandę... Wanda ma nerwy w strzępach. Mimo, że zabunkrowana, jest zestresowana; zdradziła się przed osobami, które ją szukały.

Leonidas dostał informację bezpośrednią od thugów - Wanda jest w "Dziale Plazmowym". Była na zapleczu, wyrwała się i poszła na scenę, do mikrofonu.

Klara i Leonidas dotarli do "Działa Plazmowego". Wanda jest w swoim żywiole - opowiada żarty... jest spięta i przerażona, ale się trzyma. Sukcesywnie i stopniowo ilość publiczności się zmniejsza, Wanda walczy o życie...

Tymczasem Leonidas zwrócił uwagę na czarodziejkę bawiącą się na imprezie. Netherię Diakon. Przyjaciółkę i partnerkę Estrelli. A jednocześnie Joachim jest w Rezydencji i czeka na Marcelina Blakenbauera... więc Leonidas musiałby być w dwóch miejscach w tym samym czasie. Gorzej, Netheria obserwuje Wandę i się nią wyraźnie interesuje.
Sweep robocików Klary nie pokazał, by gdziekolwiek tutaj była Estrella. Czyli Netheria jest tu samotnie.

Leonidas NADAL wygląda jak Marcelin Blakenbauer. Leonidas przygotował zaklęcie teleportacyjne i odpalił je w kierunku Wandy. Tymczasem Klara poświęciła robota i zrobiła nim zwarcie. Zgasły światła, trysnęły iskry, "Marcelin" (Leonidas) teleportuje siebie i Wandę do furgonetki. A tam czekają na Wandę małe roboty. Wanda nie zdążyła powiedzieć ani słowa - padła na ziemię nieprzytomna.

Gdy światła wróciły, Netheria spoważniała. Obrzuciła lodowatym spojrzeniem wszystkich dookoła, po czym wyszła z "Działa Plazmowego".

Joachim opuścił Rezydencję. Był spotkać się z Marcelinem w sprawie Wandy, ale Marcelin był niedostępny. Leonidas go nie ściga - nie ma po co.

Tymotheus dostał w swoje ręce Wandę.


## Dark Future:

Kontekst:

- Marcelin z Estrellą Diakon schowali się w Rzecznej Chacie; Ofelia pomogła mu się wydostać jako pies.
- Wanda znajduje się w Dziale Plazmowym. Pomaga jej Stanisław Pormien; wziął ją do domu.

To Buy:

- Elizawieta Zajcew, którą Marcelin przekonał o potrzebie pomocy i ochrony. Też: kontakt z Joachimem.                           V
- Marcelin skontaktował się z Tatianą, która jednak mu odmówiła współpracy. Fire Suit wybrał.                                   X
- Wanda nagrała anty-Blakenbauerowe nagranie celowane do świata ludzi a drugie do świata magów. Uploadowała ludzkie na YT.      V
- Marcelin skontaktował się z Joachimem Zajcewem i ściągnął go do Świecy jako gościa.                                           X
- Wanda wysłała wersję do ludzi do Dagmary Czeluść. a wersję anty-magów Estrelli Diakon.                                        V
- Joachim przybył do Świecy spotkać się z Marcelinem.
- Wanda zmieniła lokalizację; na zaplecze "Działa plazmowego" by nie narażać Stanisława.                                        V
- Joachim + Marcelin i Wanda spotkali się w Dziale Plazmowym.
- Joachim zawiózł Wandę i Marcelina ze sobą, do swojego domku. Poprosił Remigiusza Zajcewa o pomoc (ochronę) (2).
- Marcelin obiecał zapłatę; Joachim i Remigiusz wywieźli Wandę daleko (koszt: 2).

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                        1. Rzeczna Chata
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Squat Dante
                    1. Dzielnica Owadów
                        1. Mieszkanie Stanisława Pormiena

# Zasługi

* mag: Leonidas Blakenbauer jako Marcelin, niszczący dobrą reputację Marcelina i manipulujący Wandą, Marcelinem i wydarzeniami.
* mag: Klara Blakenbauer, zakładająca pułapkę na Marcelina i dowodząca małą armią robocików zwiadowczych.
* mag: Marcelin Blakenbauer, który prawidłowo wykorzystał swoje przyjaciółki... tylko by wpaść w pierwszą pułapkę Leonidasa.
czł: Wanda Ketran, którą w końcu przeznaczenie dogoniło - wpadła w ręce Tymotheusa mimo wszystkich możliwych starań. Zasiała ziarno zemsty.
* czł: Stanisław Pormien, "eks" Wandy, który przygarnął ją gdy tego potrzebowała mimo, że było to niebezpieczne.
* mag: Estrella Diakon, która zapewniła Marcelinowi rozwiązania taktyczne... których nie posłuchał. Dostała od Wandy nagranie o Blakenbauerach (death hand).
* czł: Borys Kumin, szef obrony który dla odmiany KOGOŚ obronił (unieruchomił i złapał Marcelina pistoletem strzałkowym).
* mag: Elizawieta Zajcew, terminuska Świecy, która chciała pomóc Marcelinowi a skończyła w głupiej sytuacji w środku Rezydencji Blakenbauerów.
* mag: Joachim Zajcew, który szukał Wandy w Rezydencji w chwili, gdy ona kończyła swój żywot na scenie "Działa Plazmowego".
* mag: Netheria Diakon, obserwująca Wandę uważnie i cichy świadek jej porwania z "Działa Plazmowego".
* mag: Tymotheus Blakenbauer jako Otton Blakenbauer, ten, który wysłał Klarę i Leonidasa by odzyskali Marcelina. Zależy mu na rodzie... i niewiele więcej.
* czł: Dagmara Czeluść, która dostała od Wandy nagranie o grzechach Blakenbauerów (death hand).