---
layout: inwazja-konspekt
title:  "Streamerka w Na Świeczniku"
campaign: powrot-karradraela
players: kić, til, viv
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140508 - "Lord Jonatan" (AW)](140508-lord-jonatan.html)

### Chronologiczna

* [140508 - "Lord Jonatan" (AW)](140508-lord-jonatan.html)

## Kontekst ogólny sytuacji

Antygona Diakon na ratunek streamerki.

## Punkt zerowy:

Ż: Jak wygląda struktura Waszego biura?
T: Niezależna agencja, wynajmowana. Mała firma zajmująca się ochroną reputacji. Przy czym, jesteśmy daleko zależni od potężnych klientów. Na długim kontrakcie ze Świecą.
Ż: Jak się nazywa Wasze biuro?
V: Na Świeczniku
Ż: Jak chwilowo stoi finansowo Wasze biuro?
K: Bywało lepiej, acz na razie nie jest tragicznie.
Ż: Czemu osoba z natury unikająca krzywdy ludzi miałaby wybrać Waszą organizację do pomocy człowiekowi?
T: Bo - fun fact - jesteśmy skuteczni i umiemy o tym opowiadać. Jesteśmy swoim 1 klientem.
Ż: Co sprawia, że ten temat upokorzonej streamerki jest dla Was szczególnie interesujący?
V: Najpewniej upokorzył ją ktoś spoza Świecy. 
Ż: Na czym najbardziej zależy Waszemu przełożonemu w tej kwestii?
K: Żeby nie dało się tego powiązać z nim.
V: Jak sie nazywa nasz przełożony?
Ż: Paweł Robercik

## Misja właściwa:

**Dzień 1:**

Firma "Na Świeczniku", mająca swoją główną siedzibę w Czarnoskale ma za zadanie ochronę reputacji magów. Często pracują ze Świecą. I pewnego dnia trzech magów działających w Na Świeczniku zaprosił do siebie ich szef, Paweł Robercik.

Robercik pokazał, że dostali CD na którym "tajemniczy głos" powiedział, że została dokonana krzywda ludzkiej streamerce przez maga. W sprawę mogą zamieszani być magowie wyższej rangi Świecy i podobno mogą mieć z tego jakieś problemy - mimo, że normalnie ludzie nie ucierpią od spraw magicznych. Innymi słowy, ciekawostka. Robercik kazał im spojrzeć zarówno na tą ludzką streamerkę - może być warto jej pomóc - jak i na potencjalnego maga stojącego za całą tą sprawą. Nagranie pokazywało młodą dziewczynę masturbującą się ogórkiem podczas gry w DoTa.

Michał poszukał na swojej liście kontaktów koleżankę Olę - gwiazdę YT. Jeśli ktokolwiek zna się na tym typie problemów to ona na pewno. I faktycznie, Ola powiedziała, że to się da naprawić; w najgorszym wypadku Natalia ma 400 fanów na Twitchu i może założyć nowe konto. Jak chodzi o reputację... bywało gorzej. Samo się wypali i zgaśnie. Spytana, obiecała Michałowi znaleźć wszystkie potrzebne formularze i materiały odnośnie odwołań do bana z Twitcha przez kwestie takie jak ta.

Tymczasem Antonina i Sonia poszły zająć się tematem zdobycia obrazu kurierki - czarodziejki, która dostarczyła CD do skrytki hipermarketu. Antonina, korzystając ze swojego "fitness GO GIRL!" i sławy zrobiła wielkie show w hipermarkecie, zaskarbiając sobie sporą sympatię wielu ludzi. Dodatkowo, kupiła czas dla Sonii. A ta włamała się magicznie na kamerki - zapisy nagrań. vs 8. 10 wpływ duży ++. Ma obraz kurierki (Antygony) - ale słabszy, niekompletny, bo ta się przebierała. Przyszła wielokrotnie, zwracać uwagę na różne kamery i różne osoby; Sonia ją wyłapała tylko dlatego, że Antygona nie zmieniła swojej postury w żaden sposób i nie zawsze grała przy wszystkich kamerach. Big Data Analysis i inne takie ;-).

Michał spytał Robercika, skąd ten się dowiedział o CD. Ten powiedział, że od Artura Żupana, typowego "taniego drania do wynajęcia", po hipernecie. Cóż, ten ślad raczej się urwał... nie chce im się naciskać na Żupana. 

Dlaczego zleceniodawca (Antygona) chce pomóc Natalii? (ta ma 400 fanów). To zupełnie nie ma sensu i może być kluczem do całej sprawy.

Sonia szuka czy film jest stworzony magicznie czy nie (perfekcyjny vs szumy). Ma szumy -> powstał naprawdę. Czyli to się wydarzyło najpewniej naprawdę i najpewniej właśnie w mieszkaniu Natalii. Teraz jest ścieżka jak to sprawdzić. Michał przeszedł się do domu Natalii (no, do bloku) i zadzwonił na domofonie. Odebrał ojciec; gdy Michał przedstawił się jako fan, ojciec powiedział, że Natalia nie jest zainteresowana i odłożył słuchawkę. Michał cały szczęśliwy - to wskazuje, że są na właściwym tropie, że to JEST prawdziwe i się wydarzyło. Gdyby było inaczej, mógłby go wpuścić.

Tymczasem Antonina zabrała grupę ochroniarzy do klubu Wielkie Piwo. Rzuciła tam czar by opowiedzieli o najlepszej dziewczynie (-> tamtej, czyli Antygonie). Z ich opowieści udało się Antoninie zebrać podstawowe informacje: Diakonka, infomantka, zna się świetnie na świecie ludzi. Przy okazji Antonina dowiedziała się do którego hotelu zmierzała Antygona - i przekazała tą informację Michałowi.

Plus, ochroniarz Jurek (a przy okazji psychofan Antoniny) opowiadał o swojej córce której koleżanką jest ta streamerka... i jak to Antonina jest o niebo lepsza niż tamta streamerka i jest lepszym role-model...

Mając zbiorcze informacje, Sonia próbuje to wszystko zagregować - obraz z kamer, z mózgu Antoniny, dane od Robercika odnośnie "kuriera". W+. Nie ma jednoznaczego dowodu, niestety, ale łącząc to wszystko z hipernetem wyszło jej wyraźnie, kto to jest. Ale dopiero następnego dnia ;-). 

Hotel. Michał odwiedził managera sali i powiedział, że szuka swojej przyjaciółki by oddać jej telefon i ona podróżuje incognito. Naprawdę nieźle go zabajerował; dowiedział się, że Antygona wyjechała motorem na blachach kopalińskich dnia poprzedniego wieczorem i wielokrotnie wychodziła w różnych strojach. A więc się zgadza.

**Dzień 2:**

Sonia ma jednoznaczną odpowiedź - to jest Antygona Diakon. Pasuje wszystko. Request ze strony Antoniny po hipernecie do Antygony, dając jej lekko do zrozumienia, że Antygona jest w tarapatach. Antygona spytała Antoninę kiedy się mają spotkać (przestraszona). Dziś 18:00. Antygona przyjedzie.

Antonina pojechała do swojego psychofana by pogadać z jego córką, Grażynką. Ta nie przyjęła Antoniny ciepło; uznała, że Antonina podrywa jej tatę. Dopiero gdy Antonina wyjaśniła, że chce pomóc Natalii, Grażyna się rozchmurzyła - bardzo się martwi tą sprawą. A dokładniej, uważa, że stało się coś strasznego; widziała to w internecie. Ona i kilka kolegów i koleżanek ze studiów. To było straszne. Natalia by tak normalnie się NIGDY nie zachowała. Dodać warto, że ojciec Natalii to samotny ojciec; dewota, który wierzy, że internet to narzędzie szatana i... ogólnie, Natalia ma poważny problem. Ona nawet na studia nie będzie mogła wrócić jak tak dalej pójdzie... ma areszt domowy.

Tego problemu nie rozwiąże się normalnie. Więc Michał zdecydował się zadzwonić do lokalnego księdza - Jana Adamskiego - i przedstawił się jako członek grupy pomocy psychologicznej. Powiedział, że Natalia - córka jednego z parafian księdza - ma poważny problem. Ojciec jej nie wypuszcza z domu a ona miała załamanie nerwowe. Silnie poruszony ksiądz powiedział, że spróbuje przekonać ojca Natalii (Grzegorza) o to, by zgodził się na wizytę psychologa u Natalii. Michał wykazał się wybitnymi umiejętnościami perswazji i dał radę przekonać księdza, by to był ich psycholog (dokładniej: Antonina). Ksiądz oddzwoni do Michała jak coś będzie wiedział.

W kawiarence Cesarz. Przybyła Antygona jak na ścięcie... tam spotkała się z nią Antonina i po chwili dosiadła się reszta Zespołu. Antygona stwierdziła, że mają niezasłużenie leserską opinię; wybrała ich, bo nie spodziewała się, że będą szukać JEJ z taką determinacją. Gdy Michał ją zapytał o co chodzi w całej tej sprawie, Antygona powiedziała, że jej ludzka koleżanka - streamerka - została załatwiona przez jakiegoś maga. Antygona ma podejrzenia przez kogo - pewnego Zajcewa, który przegrał z nią w Mortal Kombat - ale nie jest PEWNA. I teraz ta koleżanka Antygony ma problem, jej życie się de facto załamuje.

Michał zażądał potwierdzenia - to wszystko po to, by jakaś ludzka nastolatka nie miała opinii... takiej jak ma teraz? Antygona potwierdziła z determinacją. Ona może sama rozwiązać sprawy internetowe, ale nie pomoże jej to w naprawie pozycji samej Natalii. Antygona nie wie jak. Dobra; naprawą tamtego aspektu zajmą się magowie Na Świeczniku... w zamian za to Antygona będzie musiała dług odpracować. Albo Robercik da znać Draconisowi. Antygona westchnęła żałośnie. Weźmie to na siebie, nie trzeba od razu kłopotać Draconisa...

**Dzień 3:**

Rano do Michała zadzwonił ksiądz. Powiedział, że problem został rozwiązany; rozmawiał z Grzegorzem Kamenikiem i ów zgodził się, by pani psycholog przyszła odwiedzić Natalię. Michał z najwyższą przyjemnością delegował problem Antoninie. Antonina się ucieszyła mogąc pomóc ludzkiej streamerce ;-).

Antonina weszła do bloku i porozmawiała z Grzegorzem Kamenikiem. Facet jest samotnym ojcem z nadmiarem dewocjów w domu; bardzo martwi się o córkę i nie wie jak jej może pomóc. A jednocześnie uważa, że młoda zbłądziła strasznie i trzyma ją w domu, aż ta dojdzie do siebie. Nie studia, nie komputer. Musi do siebie dojść. Antonina podrapała się z frasunkiem po głowie i zaproponowała, że porozmawia z Natalią. Zobaczy, jak będzie w stanie pomóc.

Rozmowa z Natalią była... smutna. Dziewczyna w lekkiej depresji, nie chce żyć, zrobiła coś głupiego, nie pójdzie już na studia, nie pokaże się nikomu... itp. Antonina zaczęła ją pocieszać i tłumaczyć, że to nie jest koniec świata - najpewniej Natalia coś zeżarła. Przy okazji Antonina wzmocniła się magią; niech Natalka nie do końca pamięta, kto jej coś podał, ale niech pamięta że jej podał ;-). Oczywiście, udało się. Następnie Antonina nałożyła na Natalię aurę magiczną - niech przez następne dwa tygodnie każdy patrzący na Natalię będzie jej jednak przychylny. To pomoże dziewczynie skompensować traumę tego, że ten filmik wypłynął.

Antonina wróciła do rozmowy z Grzegorzem. Przekonała go, że Natalia musi móc wyjść na świat, spotykać się mimo wszystko z ludźmi. Przekonała go, że Natalia musi mieć dostęp do swojego komputera. Grzegorz twardo, że taki bez gier i streamowania. Antonina się zgodziła. W końcu Natalia jest dorosła; sama da sobie radę...

# Progresja

* Antygona Diakon: nieco przymusowa współpracowniczka 'Na Świeczniku', aż odpracuje...

# Streszczenie

Do "Na Świeczniku" - firmy magów zajmującą się ochroną reputacji - trafiło anonimowe zgłoszenie o ludzkiej streamerce którą skrzywdził mag i magowie wysocy znaczeniem mogą mieć przez to problemy. NŚ skupiło się na tym temacie i odkryło, że zleceniodawczynią jest... Antygona Diakon, która chciała pomóc ludzkiej przyjaciółce. NŚ pomogli człowiekowi w niemałym stopniu, acz Antygona za to zapłaci przez odpracowanie. Streamerka jednak ma bana na gry komputerowe i streamowanie przez ojca.

# Zasługi

* mag: Antonina Wysocka, (Viv); girl power i fitness. Frontgirl wzbudzająca zachwyt wśród postronnych.
* mag: Michał Ostrowski, (Til); bajerant i flirciarz. Motor stojący za zrozumieniem tej intrygi.
* mag: Sonia Stein, (Kić); info- i techno-. Analizowała dane oraz przetwarzała je na informacje.
* mag: Paweł Robercik, przełożony magów Na Świeczniku w Czarnoskale. Dość rozsądny i zachowawczy; skutecznie zarządza finansowo "Na Świeczniku".
* mag: Antygona Diakon, praprzyczyna wszystkich perypetii; jej ludzka koleżanka (Natalia) została skompromitowana przez maga, więc ta nadała temat korektorom reputacji. Ale nie było jej na to stać... więc poszła metodą bardziej "szantażową". I się wkopała.
* czł: Natalia Kamenik, 19-letnia streamerka; niezbyt popularna (~400 followerów), ale bardzo zdolna. Pokonała Zajcewa w Mortal Kombat i skończyła masturbując się na streamie... dzięki Na Świeczniku jakoś dojdzie do siebie.
* czł: Grzegorz Kamenik, samotny ojciec streamerki o silnym skupieniu na życiu duchowym; nieco bezradny w całej tej sytuacji, ale "córkę przed splugawieniem" chronić musi!
* czł: Jerzy Szczupanek, ochroniarz w Tytanie; szczególnie interesuje się Antoniną Wysocką (psychofan) i ruchem "Go Girls!". Ogólnie dobry człowiek, acz córka się go wstydzi.
* czł: Grażyna Szczupanek, córka ochroniarza i przyjaciółka Natalii, która dostarczyła kluczowych informacji Antoninie odnośnie stanu Natalii.
* czł: Jan Adamski, ksiądz, chcący pomóc swojej parafiance. Przedyskutował z Grzegorzem sprawę Natalii i faktycznie zależy mu na pomocy swoim parafianom.
* mag: Artur Żupan, do którego przyszła Antygona; na nim wiadomość się urwała. Ale tylko dlatego, że nie zapytał nikt, kto dałby mu choć jednego Quarka ;-).

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Czarnoskał
                    1. Centrum
                        1. Biuro Na Świeczniku, centrum dowodzenia Robercika i innych magów Na Świeczniku
                    1. Aleja Piwowarska
                        1. Hipermarket Tytan, największy hipermarket w okolicy, gdzie Antygona zostawiła CD dla Na Świeczniku i gdzie przebierała się dziesiątki razy.
                        1. Klub Wielkie Piwo, spory klub z dużą ilością niezbyt drogiego alkoholu; Antonina zrobiła tu event dla ludzi z Tytana.
                    1. Osiedle Techniki, na którym min. mieszka Natalia Kamenik
                        1. Hotel Fontanna, gdzie schroniła się Antygona i gdzie zrobiła sobie główny obszar działań

# Czas

* Dni: 3

# Narzędzia MG

## Cel misji

* Misja demonstrująca system Tilowi i Viv
* Misja weryfikująca uproszczone karty postaci

## Po czym poznam sukces

* Til i Viv doceniają system, choć niekoniecznie jako "RPG"
* Uproszczone karty postaci działają w parametrach właściwych
* Uproszczone karty postaci są czytelne i łatwe w użyciu

## Wynik z perspektywy celu

* Podoba się.
    * Til: bardzo zgrabna całość logiczna, logiczne elementy, nei było namolne, mogliśmy zrobić co chcieliśmy. Konsekwencja mechaniczna settingu mnie boli. Setting wybacza. Nie moje spektrum. Hipernet SUCKS, jak i magia jako taka w ten sposób - to nie jest magia. To jest modyfikator.
    * Viv: dużo więcej odkrywaliśmy, mniej wiedzieliśmy. Brak frustracji. No, mała wiedza o świecie. Mogliśmy zrobić co chcemy.
* Bardzo działa. Bardzo się podoba.
* Karty czytelne i łatwe. Jedyny problem - rozmyte granice.
* Magia pokazuje gdzie jest problem z lingwistyką. Jeśli magia jest związana z um/spec itp, możliwości działania są nieograniczone.
    * ...a więc czemu nie spróbować zrobić tego tak, by magia była 3 kategorią? TYLKO magia? Albo tylko specjalizacje?
    * magia wygląda jak cheatowanie

## To consider:

* coś obstawiłem, jakiś outcome - inaczej nie mam powodu wygrać lub przegrać (cokolwiek to znaczy). Nie ma inwestycji = nie ma woli.
* zbyt szerokie poziomy umiejętności - trzeba to spłaszczyć jakoś.
* nie grasz z grupą społeczną, grasz z graczami którzy chcą grać w to samo

## Wykorzystane tabelki

cztery tabelki, prosty podział. Zbuduję "właściwe" na przyszłą misję.

## Karty postaci:

### Antonina Wysocka:

![Antonina Wysocka](Materials/170501/antonina.png)

### Michał Ostrowski:

![Michał Ostrowski](Materials/170501/michal.png)

### Sonia Stein:

![Sonia Stein](Materials/170501/sonia.png)
