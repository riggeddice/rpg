---
layout: inwazja-konspekt
title:  "Rozpad magii w Leere"
campaign: ucieczka-do-przodka
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160117 - Muchy w sieci Korzunia (PT)](160117-muchy-w-sieci-korzunia.html)

### Chronologiczna

* [160117 - Muchy w sieci Korzunia (PT)](160117-muchy-w-sieci-korzunia.html)

## Kontekst ogólny sytuacji

[Wszystko, czego nie chcemy wiedzieć o studniach ;-)](http://www.budujemydom.pl/woda-i-kanalizacja/470-studnia-wybor-i-lokalizacja)

Stamtąd pochodzi źródło poniższego rysunku:

![Rysunek pokazujący jak wygląda konstrukcja studni](Materials/170423/studnia_rodzaj_z_budujemydompl.jpg)

Zaznaczam, że operujemy na terenach łupków krystalicznych (ulegają zjawiskom krasowym). To daje mi prawo do wprowadzania jaskiń ;-).

[Zacny opis czegoś co można wykorzystać, o kopalni w Sudetach](http://dzikiesudety.blogspot.com/2015/04/kopalnia-liczyrzepa-sztolnia-z-jaskinia.html)

Co może postacie skrzywdzić?

* słabo nasyconego tlenem powietrza 
* wydzielającego się z gnijących w wodzie elementów obelkowania siarkowodoru (możliwa utrata przytomności, uduszenia itd.) 
* w niewentylowanych przestrzeniach pojawia się gaz będący produktem rozpadu pierwiastków promieniotwórczych, czyli radon, który w dużym stężeniu jest bardzo szkodliwy dla zdrowia. 
* słabo zwięzłe skały, które odspajają się od stropu i tworzą obwały, mogące podczas eksploracji odciąć człowieka od wyjścia z obiektu 
* można tu również z racji zalanych chodników i rozmaitych przedmiotów zalegających w wodzie groźnie się skaleczyć i ulec  np. zakażeniu bakteryjno-wirusowemu.
* okolica obwału z którego wydobywa się woda!

Plus, jak to w RPGach, lokalne stworzenia. Rój szczurów?

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Maria powiedziała Paulinie, że robiła pewne śledztwo. Dokładniej, w sprawie prób zatuszowania jazdy po pijanemu przez syna lokalnego radnego. I dotarła do interesującego faktu - w miasteczku większość ludzi ma straszliwe koszmary senne, które przenoszą się też na normalne, codzienne życie. Informator powiedział jej, że to wina wiedźmy, która przeklęła ich wszystkich. Więc... nie byłoby źle, gdyby Paulina na to spojrzała. Inne teorie - rząd dodaje coś do wody. Ale Maria pokazała statystyki samookaleczeń, stosowania antydepresantów, samobójstw itp. są podniesione. Statystycznie, jednostkowo nie widać.

Czyli coś tam się najpewniej dzieje. Maria powiedziała, że pojedzie i się rozpyta... ale chciałaby, by Paulina wiedziała. Dla Pauliny to nie do końca ma sens - na tym terenie, w powiecie Leere magia się sama rozpada i zanika. Więc klątwa się nie utrzyma. Teoria tego, że rząd dodaje coś do wody staje się bardziej wiarygodna.

Paulina powiedziała Marii, że to jest coś niemożliwego. Ta się zirytowała. Powiedziała, że ma zamiar zostać tam przez 2-3 dni i się rozejrzy. Paulina poprosiła Marię, by ta też się delikatnie dowiedziała, jaka wiedźma ich niby przeklęła. Maria się zgodziła. Zdobędzie Paulinie te informacje. Paulina poprosiła Marię, by ta jednak miała z nią aktywny soullink. Maria się zgodziła; będzie informować o wszystkich dziwnych rzeczach.

(kość szczęścia Marii: 1; ta to ma przechlapane)

**Dzień 2:**

Szpital. Paulina ma bardzo trudną sprawę. Trafiła do nich zatruta jakimś cholerstwem 27-latka. Wezwali jej karetkę z Czarnego Dworu... Paulina na wejściu już od niej czuje Skażenie magiczne; zjadła coś co się magicznie rozpada. Coś, co ją zabija. Płukanka... i czas się wkręcić.

Edward Ramuel odpowiada Paulinie na jej pytania: coś jej podłożono. Dosypali jej i ona to wypiła. Ramuel powiedział, że policja zwinęła gościa, który jej to podłożył. Paulina pyta, skąd wiedzą. Ramuel powiedział, że to on. Kręcił się dookoła dziewczyny, zagadywał, a jak ta padła i zaczęła mieć drgawki to zaczął uciekać. Stąd Ramuel wie, że to był on. Ma go policja.

Paulina zdecydowała się użyć zaklęcia małej mocy (1 sek); niech Ramuel powie jej dokładnie o co chodzi i dlaczego przyjechał. Zaklęcie mające wymusić wyczucie jego intencji.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    0     |    0     |     1     |       0      |    2    |      1      |    0     |    1    |     0      |     1    |     4    |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |   0    |    0    |    0    |      0      |      2     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    4     |      2     |    2    | +1 W0 (13)   |  3 W0   |

Edward Ramuel pod wpływem delikatnego szeptu magicznego Pauliny powiedział jej co ona chciała się dowiedzieć. Mają zamontowane kamery w Czarnym Dworze; obserwują wszystko. Po ostatnich sprawach szef, czyli Ireneusz Przaśnik, chce wrócić do dobrego standingu z policją i naprawdę nie chce kłopotów. Jak chłopak dosypał coś dziewczynie i ona zaczęła się wić, Przaśnik kazał Ramuelowi odwieźć młodą do szpitala i sam upewnił się, by chłopak trafił na policję. Na bazie wiedzy Edwarda, tutejszy półświatek nie ma z tym nic wspólnego.

Dzięki zaklęciu Edward powiedział też, że chłopak jest lokalny. Bywalec. Nazywa się Filip Wichoszczyk; ma ogólnie opinię casanovy i to naprawdę mającego ogromne powodzenie. Taki playah. Nikt nie rozumie, czemu coś dosypał - nie musi. Podobno Przaśnik (boss) chce przejrzeć przeszłe rekordy na kamerze. Chce sprawdzić, czy to się już kiedyś wydarzyło. Przaśnik jest, ogólnie, wściekły. Na Wichoszczyka.

Maria kontaktuje się z Pauliną. Powiedziała Paulinie, że coś znalazła. Paulina ją na razie odcięła; ma tu popieprzoną sytuację magiczną. Maria powiedziała, że nie ma sprawy, kontynuuje poszukiwania. Szybko rzuciła, że była tu kiedyś potencjalnie czarodziejka która próbowała coś robić; to ta "wiedźma". Wyprowadziła się stąd jednak pewien czas temu. Kaja Odyniec.

Paulina przeszła do biednej pacjentki. Zajmuje się nią Grażyna, tak dobrze jak tylko potrafi.

Licznik śmierci Katarzyny: 5 (2,2,1)
Licznik leczenia Katarzyny: 5

Paulina nie do końca wie co się dzieje z Katarzyną; magia żywi się nią... Paulina zdecydowała się zrobić diagnozę i dalsze kroki:

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |     1    |    1     |     1     |    1 (KNO)   |    5    |      1      |     1    |    1    |     0      |     3    |     8    |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |    3   |    0    |    0    |     3       |     5      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    8     |     5      |   3     | +1 W0 (14)   |  4 W0   |

Paulinie udało się zrozumieć co się dzieje. Ma do czynienia z rozpadającym się zaklęciem; rozpadającym się eliksirem. Eliksir jest formą eliksiru miłości - pełne oddanie i pożądanie na pewien czas. Ale wpływ Leere sprawił, że się rozpadł... przeterminował. Zaklęcie się przekształciło. I teraz próbuje się paradygmatycznie odbudować czerpiąc z niej. Ale Leere działa dalej, Pryzmat się rozsypał... ogólnie, działa jak silna toksyna.

ŚCIEŻKA ŚMIERCI: 1 (+1) (1/5)

Paulina zdecydowała się zmienić samo zaklęcie. Musi osłonić dziewczynę. Wykorzystała kilka kryształów Quark; katalitycznie przekształciła sam eliksir (narkotyk) by móc ratować młodą. -1 surowiec.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt.    | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    1     |     1    |     1     |   0 (SPN)    |    4    |    1 (wie)  | 1 (szpital) |    1    | 1 (quarki) |     4    |     8    |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   3    |    0    |    0    |     3       |     5      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|   8      |     5      |   3     | +1 Wx (11)   |  4 W0   |

Paulinie udało się przekształcić eliksir. Zdecydowanie wolniej Skaża i krzywdzi dziewczynę (tor rośnie wolniej).

ŚCIEŻKA ŚMIERCI: 7 (+1) (2/5) -> pierwszy efekt uboczny

Dziewczyna jest Skażona. Dla lekarzy wygląda to jak jej się pogarsza, ale Paulina wyczuwa budzącą się w niej niekontrolowaną tkankę magiczną i de facto walkę jej systemu odpornościowego z nową tkanką.

Paulina wpadła na świetny pomysł. Po płukaniu żołądka udało im się wyprowadzić z Katarzyny część eliksiru. Łącząc magię biomancji z katalizą Paulina zdecydowała się użyć sympatii; niech trucizna w Katarzynie przeniesie się w całości do "wypłukanej" części.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.  | .Sprzęt.    | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    1     |     1    |     1     |   0 (SPN)    |    4    | 2 (wie i ma) | 1 (szpital) |    1    |    0       |     4    |     8    |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   3    |    0    |    0    |     3       |     5      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|   8      |     5      |   3     | -2 W-1 (5)   |  1 W-1  |

Paulinie udało się przesunąć większość trucizny magicznej z dziewczyny. (fabularnie: nadal jest narażona, acz nie ma w sobie tego silnika). Czyli kolejne -1 na torze śmierci.

ŚCIEŻKA ŚMIERCI: 13 (+0) (2/5)

Paulina zauważyła, że Grażyna jest zaintrygowana działaniami Pauliny. Paulina ratując życie Kasi rzuciła dwa jawne zaklęcia; a dziewczynie się zdecydowanie poprawia. Grażyna nie rozumie co się dzieje i nie rozumie, skąd ma te dziwne uczucie (emisji Skażenia). Paulina zauważyła problem, ale zdecydowała się go totalnie zignorować tym razem. 

Dobra. Ostatnia prosta. Naprawa do Wzoru. Regeneracja niepożądanej tkanki magicznej. I Paulina wzmacnia się energią Leere. Paulina ma też świetne rozwiązanie - ma zamiar wykorzystać krew ofiary, by wzmocnić WPŁYW (ale nie wpływ zaklęcia; wpływ akcji).

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.  | .Sprzęt.            | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    1     |     1    |     1     |   1 (KNO)    |    5    | 2 (wie i ma) | 2 (szpital + Leere) |    1    |    0       |     5    |    10    |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   3    |    2    |    0    |     5       |     7      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|   10     |     7      |   3     | 1 W0 (14)  |  4 W0   |

Udało się. Zwiększony wpływ doprowadził do tego, że Katarzyna szybciej dojdzie do siebie i nie będzie elementów nieodwracalnych. Ma tydzień w plecy, ale będzie żyć... i będzie mieć jakieś straszne koszmary czy coś. Ale będzie działać.

Grażyna nic nie powiedziała. Ma swoje przemyślenia (+4 na torze podejrzliwości), ale nic konkretnego.

TOR PODEJRZLIWOŚCI SZPITALA (0/10: 5-8-10): każda akcja Pauliny podlega rzutowi (0-1-2). 3(+0), 7(+1), 14(+1), 18(+2). ALE z uwagi na uratowanie pacjenta -1. To przesuwa tor podejrzliwości szpitala na 3/10. Paulina może ten tor redukować: zaprzyjaźniając się z nimi, nie używając magii (i czas płynie) itp.

Oki. Po kilku sprawach papierkowych problem został zamknięty. Czas pogadać z Marią. Gdy już Paulina wróciła do mieszkania. I nie wie, czy nie iść do tego gościa od narkotyków...

Maria powiedziała Paulinie jak wygląda sytuacja. Zorganizowała sobie przewodnika. Po kopalni. Po co jej kopalnia, Paulina się zdziwiła. Cóż, podobno ta "wiedźma" była bardzo zainteresowana kopalnią. Jej tu już od roku nie ma. Sama kopalnia nie jest eksploatowana; można ją nawet zwiedzać. Maria ma ochotę przejść się i obejrzeć jak ta kopalnia naprawdę wygląda. Mają soullinka. Nie ma ryzyka. Jej przewodnikiem jest Wojmił Rzeźniczek, to były górnik. Jeszcze silny, choć koło 60-tki. Zna się na rzeczy. Idą we dwójkę. Jemu się często ta kopalnia śni... Maria ma chęć zrobić skan okolicy. Paulina poprosiła, by Maria NIE przekonywała Rzeźniczka, by ten jej pokazał co jej się śni. Maria powiedziała, że nie ma sprawy. Nie będzie przekonywać.

Gdy Paulina poprosiła Marię o informacje na temat narkotyków / krzywdzenia ludzi... ta powiedziała, żeby Paulina powołała się u Artura Kurczaka na nią. Maria jest dużą dziewczynką, poradzi sobie w kopalni. Maria zauważyła, że zanim spotkała się z Pauliną, robiła różne rzeczy związane z tematami magicznymi... chyba. Jakoś przeżyła jako reporter śledczy. Paulina się martwi. Maria obiecała, że będzie ostrożna. I będzie uważać na swojego "partnera" by jej nie uderzył. I będzie zostawiała za sobą breadcrumby. I weźmie sprzęt.

Paulina i Maria się jeszcze docierają. Nie jest pewna, jak to rozwiązać z Marią. Zależy jej na tym by działać i Paulina trochę nie wie jak Marię zatrzymać... więc ją puściła. Nie, żeby mogła ją zatrzymać...

Paulina przygotowała sobie czar mentalny i poszła do policjanta (Kurczaka). Zaklęcie typu "co wiesz". Kurczak ją przywitał; widać że pracuje. Paulina powiedziała mu, że Maria Newa powiedziała, że on może jej pomóc. Artur się uśmiechnął. Gdy Paulina powiedziała, że chodzi o chłopaka, Kurczakowi uśmiech się zmniejszył. Pracują nad tym ;-). Artur zdziwił się, skąd Maria wie o tej sprawie. Paulina z uśmiechem, że ona leczyła ofiarę. Artur zrozumiał, że Paulina jest informatorką Marii - tym razem. "Zrozumiał". Paulina ma miejsce w jego wyobrażeniu.

Kurczak powiedział, że toksykologia przechwyciła próbki narkotyku od Wichoszczyka. Paulina wie, że parę dni temu narkotyk wszedł w fazę niestabilną a za parę dni kompletnie zaniknie. Więc ktokolwiek mu to dał, musiał przekazać mu do 10 dni temu. Ale to wie Paulina i tylko ona, po wiedzy katalitycznej. I Paulina uważa, że ktokolwiek mu to da nie powiedział mu o magii.

Paulina pogadała chwilę z Kurczakiem o narkotykach. Kurczak jej powiedział, że chłopak wierzył, że dzięki temu "każda jest jego". Jakiś nowy typ narkotyku? Ale takie narkotyki nie istnieją. Paulina od razu zastrzygła uszami; to brzmi jak jakiś Diakoński środek. Po pewnej rozmowie Paulina zaproponowała Kurczakowi, że chciałaby z chłopakiem porozmawiać - może na niego wpłynie. By to się nie powtórzyło. W końcu ona jako lekarz ratowała życie ofiary Wichoszczyka... i następnym razem może jej się nie udać.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.     | .Sprzęt.  | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|    1      |     0    |     0    |     1     |      1       |    3    |  1 (może pomóc) | 1 (Maria) |   0     |     0      |     2    |    5     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   2    |    2    |    0    |      4      |      6     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    5     |     6      |   -1    | -3 W-2 (01)  |  -4 W-2 |

Nieakceptowalne dla Pauliny; bierzemy komplikację fabularne: spłacił kompletnie dług u Marii.

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    5     |     6      |   -1    | -1 W0  (07)  |  -2 W0  |

Kić stanęła na tym, że Kurczak nie da się przekonać, żeby wpuścić Paulinę. Ale da się przekonać, by przekazać chłopakowi apaszkę (niby należącą do ofiary a w rzeczywistości artefakt mentalny) i Kurczak powie Paulinie potem czego się dowiedział. I za to spłacony dług u Marii. I koniec eskalacji.

Kurczak zrobił to, o co Paulina prosiła. Paulina poczekała chwilę (godzinkę) na wyniki.

W tym czasie kontaktuje się z nią Maria. Powiedziała, że Paulina miała rację. Zapewniła Paulinę, że nic jej nie jest, ale jej partner poszedł w głąb kopalni. Pobiegł. Że tam jest złoto. Maria idzie za nim, ale ostrożnie. Zostawia za sobą ślady (breadcrumby). Maria powiedziała, że mała łapówka przy szybie i Paulinę też spuszczą w dół. Podobno tak się robi; tam czasem zostawiają śmieci, więc... tia. Mają wprawę. Ale Maria nie ma jak wrócić - samej jej nie wypuszczą, bo co zrobiła z kolegą? Więc Maria nie ma wyjścia, musi go znaleźć. Niech jednak Paulina się nie martwi. Maria da radę ;-).

Paulina właśnie tym się martwi.

Maria uspokoiła Paulinę. Ma lampę, jedzenie, wodę, apteczkę... ale tu JEST coś nie tak. Rośliny nie pasują. Są jakieś rośliny - koło zalanych fragmentów kopalni. Po zastanowieniu Maria zdecydowała się jednak wrócić na powierzchnię... wyjdzie drabiną. Paulina prosiła, by Maria nie dała się załatwić. To ważne. 

* Słyszałam go! Krzyczał o złocie i że jest bogaty i żebym przyszła mu pomóc - Maria, z lekkim zdenerwowaniem
* Chyba w tył zwrot... - Paulina, w nerwach
* Dobra. Biegnie tu. Głos się zbliża. Lecę po drabinie! - Maria, ze ZDECYDOWANYM zdenerwowaniem

Maria ze zwinnością wiewiórki zaczęła wiać po drabinie do góry tak szybko jak tylko potrafi. 

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.     | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|    -1     |    0     |     1    |     1     |      1       |    2    |  1 (przewaga)   |      0   |   0     |     0      |     1    |    3     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   1    |    0    |    0    |      1      |      3     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    3     |     3      |   -1    | -1 W0 (10) |  -1 W0  |

REROLL: Maria traci sprzęt ale się wydostanie

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    3     |     3      |   -1    |  2 W1 (17) |  +1 W1  |

W wyniku rerolla podniesiony wpływ. Maria widziała swojego górnika. Jest czymś opętany, ideą złota? Ma duży kamień i wierzy, że ten kamień jest bryłą złota. Zaczął wspinać się do Marii; ta pisnęła, po czym odcięła plecak, zmieniając go w pocisk. Górnik parsknął - jak Maria nie chce złota, to nie dostanie. A Maria - w długą. Tymczasem, Rzeźniczek wezwał windę. Koledzy zaczęli ją spuszczać...

* To... nie jest dobrze, Paulino. Coś tu jest nie tak. - Maria
* Nic ci nie zagraża; jadę tam. To chwilę potrwa - Paulina
* Nie spiesz się za bardzo. Jak się rozbijesz, to... - Maria
* Jesteś w stanie się wydostać w miarę dyskretnie? - Paulina, zmartwiona
* Tak... - Maria
* To miej na nich oko, ale UWAŻAJ... - Paulina, z naciskiem

Paulina błyskawicznie wróciła do Kurczaka. 

* Panie Kurczak! Maria się wpakowała w coś dziwnego... potrzebuje trochę sprzętu! - Paulina, nie ukrywając lekkiej paniki
* Miała pani rację. Wszystko powiedział. Rozkleił się przez tą apaszkę... - Kurczak - Mogę pani pomóc... 

Paulina poprosiła o sprzęt... survivalowy. Żadnej broni, same takie... normalne. Kurczak dał jej adres. Zenobia Morwiczka. Ona ma sprzęt, może się z Pauliną podzielić - niech tylko się powoła na Kurczaka. I Marię. Aha, a o chłopaku? O chłopaku wiadomo, że ma stałego dostawcę. Podał adres; w okolicach Czelimina. Jest imię i nazwisko i wszystko. Paulina sama tam na pewno nie pojedzie... ale wysłała hinta Wiaczesławowi z prośbą o informację zwrotną. I ostrzegła o oddziale ludzkiej policji. Wiaczesław odpisał SMSa "Wporzo".

Niestety, jest już wieczór.

Paulina szybko podjechała do Zenobii. W tle odezwała się do niej Maria.

* No nieźle... to jest jakoś zaraźliwe - Maria, ze smutkiem
* Czyli oni też polecieli po złoto? - Paulina
* Tak... po dotyku. Dotknął ich. Skóra-skóra. - Maria - Zjechali z nim po złoto.
* Mam zamiar zrobić coś... ryzykownego. - Maria
* To znaczy? - Paulina
* Uszkodzić windę. Nie dopuścić do rozprzestrzenienia się choroby. - Maria - Też mogę zablokować właz.
* Tak... - Paulina - Jesteś w stanie zrobić to tak by nie zauważyli?
* Wszyscy zeszli na dół. Jeden został u góry. Nie wiem, czy jest skażony... - Maria
* Jego też dotknął? - Paulina
* Tak. Ale nie w ciało. On nie chciał zejść. Ale nie mam zamiaru ryzykować - Maria
* Absolutnie nie ryzykuj... - Paulina
* Podpalam coś, odwracam uwagę... - Maria
* Ściągniesz straż pożarną - Paulina, niszcząc plan Marii
* Jakiś lepszy pomysł? - Maria, z nadzieją - Nie chcę się do niego zbliżać i nie mam swojego sprzętu.
* Po prostu poczekaj. Jadę ze sprzętem. Porządnym. - Paulina, z naciskiem

U Zenobii Paulinie udało się dostać sprzęt. To sympatyczna starsza pani. Sprzęt okołowspinaczkowy, kawałki liny... takie tam. Potem - w długą. Musi zdążyć. I Paulina próbuje zdążyć zanim oni wyjdą.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    0     |     0    |     0     |  0 (CRF/NMB) |    1    |    0        |   0      |   0     |     0      |     0    |    1     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   0    |    0    |    0    |      0      |      2     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    1     |     2      |   -1    | 2 W+1 (18) |  +1 W1  |

Podniesiony wpływ. Paulina jechała szybko. Co ważniejsze, dojechała zanim oni zaczęli kombinować z wychodzeniem z szybu. Czyli tam zamarudzili - albo jej się szybko udało. Maria aż się zdziwiła.

* Dobrze, przepraszam, że w to wszystko powciągałam wszystkich... - Maria zaczęła
* Nieważne - Paulina ucięła - Masz plecak od Zenobii.

Paulina i Maria mają znowu sprzęt. I Paulina ma zamiar zająć się tym jednym górnikiem na powierzchni. I Paulina ma zamiar zrobić jedną prostą rzecz. Unieszkodliwić go zanim "koledzy" wrócą. Czar usypiający.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.  | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     0     |    0     |     0    |     1     |  1 (KNO)     |    2    |   1 (ambush) |   0      |   1     |     0      |     0    |    4     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   0    |    0    |    0    |      0      |      2     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    4     |     2      |    2    | -1 W0 (10) |  +1 W0  |

Nieszczęsny górnik spojrzał na Paulinę i zasnął, chrapiąc. Paulina nie wyczuła od niego żadnego nietypowego zachowania ani bezpośrednio nie wyczuła magii. Paulina chciała go odciągnąć na bok, ale Maria poprosiła, by Paulina wpierw na niego spojrzała magicznie. Co tu się dzieje. Bo jego też dotknął ten gość.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. | .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    1     |     1    |     1     |  1 (KNO)     |    5    |     0      |   0      |   1     |     0      |     1    |    6     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |   2    |    0    |    0    |      0      |      4     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    6     |     4      |    2    | -2 W-1 (2) |  0 W-1  |

REROLL: pozostali chcą wyjść do góry; skończyli

| .Postać. | .Opozycja. | .Test?. | .Rzut.     | .Wynik. |
|    6     |     4      |    2    | -1 W0 (10) |  1 W0   |

Paulina zrobiła skan magiczny. Sam górnik nie ma śladów magii, ale zdecydowanie Paulina wyczuła silną energię mentalną na WODZIE na jego ubraniu. Wektorem jest woda. Woda z kopalni. On nie jest pod wpływem, bo nie dotknął wody. Paulina spojrzała na Marię - ma mokre buty, ale sama nie jest Skażona. Za to, gdyby próbowała rozwiązać sznurówki, to by było gorzej... Marii się upiekło.

Paulina od razu ostrzegła Marię przed wektorem Skażenia.

Komunikator zatrzeszczał. Górnicy "znaleźli złoto" i chcą wrócić do góry. Paulina szybko rzuciła czar na śpiącego górnika i wydobyła informację jak to się robi; po czym uruchomiła windę. Paulina stanęła na pozycji, na wyjściu z windy (by mieć dobry widok), zaczęła inkantować potężne zaklęcie usypiające. 

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. | .Sytuacja.  | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    0     |     0    |     1     |  1 (KNO)     |    3    |  1 (ambush) |   0      |   1     |     0      |     2    |    5     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff.   | .Wpływ. | .Skala.   | .MODIFIERS. | .OPOZYCJA. |
|    2     |  1(wpływ)|    0    | 1 (grupa) |      2      |      4     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.      | .Wynik. |
|    5     |     4      |    1    | +1 W+1 (18) |  2 W+1  |

Podniesiony wpływ, znowu. Senność zdominowała efekt mentalny. Paulinie będzie łatwiej ich wyczyścić. Tak czy inaczej, czar mentalny zadziałał; górnicy z kamieniami w rękach padli na ziemię jak ścięci. Paulina zauważyła, że są mokrzy; dłonie, ręce... wyraźnie grzebali w wodzie. Mają też ranki na dłoniach.

* Mało... widowiskowe. Ale bardzo efektowne. - Maria
* Nie musi być widowiskowe. Co zrobił, że mu tak odwaliło? - Paulina
* Muszę pomyśleć... To nie tak. On... on o tym śnił wcześniej. O tym złocie - Maria
* To tak jakby... ta woda jest gdzieś jeszcze, w tym mieście. Ale w mniejszym stężeniu! - Maria
* Mówisz... do ujęcia wlazło? - Paulina zauważyła, że Maria się wyraźnie wzdrygnęła
* Piłam herbatę... - Maria, ponuro - Przeskanujesz mnie?

Paulina zauważyła, że Maria jest rezydualnie czysta. To nie herbata; gdy Paulina z tym pierwszym górnikiem, tamten jest... lekko Skażony. Ma aurę, bardzo słabą i wyniszczaną przez Leere. Energia magiczna pochodzi ze zdecydowanej głębi; Leere ją wyniszcza.

Oki. Czas na analizę ze strony Pauliny. Paulina ma nawet możliwość porównania z "czystą" ofiarą, z Marią, i 3 na których może eksperymentować...

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. | .Sytuacja. | .Sprzęt.   | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|     1     |    0     |     1    |     1     |  1 (KNO)     |    4    |  1 (czas)  | 2 (ofiary) |   1     |     0      |     4    |    8     |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff.   | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|    2     |  1 lub 4 |    0    | 0       |   1 lub 4   |  3 lub 6   |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.      | .Wynik. |
|    8     |     6      |    2    | -2 W-1 (5)  |  REROLL |

Niekceptowalne. Niech przyjdzie ktoś po kumpli.

| .Postać. | .Opozycja. | .Test?. | .Rzut.      | .Wynik. |
|    8     |     6      |    2    |  1 W0  (14) |  S  0W  |

Paulina była tak pogrążona w skanowaniu, że zaskoczył ją górnik, który tu przyszedł po kolegów. Maria natychmiast go przechwyciła i zaczęła bajerować, kupując Paulinie czas na rzucenie kolejnego Power Word: Sleep. 3v2->REROLL (uderzy się głową), S. Górnik przewracając się uderzył się głową o kamień i zaczął krwawić. Paulina szybko go naprawiła... 

* Jak dalej tak pójdzie, usypiając wszystkich ich pozabijamy... - Maria, z większym smutkiem niż by wyglądało
* Jak tak dalej pójdzie, całe cholerne miasteczko będzie tu spać... - Paulina, cynicznie

Paulina po przeanalizowaniu tej substancji doszła do tego co się dzieje. To woda głębinowa. Z głębi ziemi. I tam są podziemne super-źródła. I akurat tutaj promieniuje. Ono jest Skażone emocjonalnie, ale Paulina nie do końca ma jak to usunąć... 

NORMALNIE dałoby się poustawiać tarcze, defensywy, blokady... ale nie w Leere. Tutaj wszystko pryśnie. Tego się magicznie nie rozwiąże. Szczęśliwie, samo Skażenie jest dość proste do wyczyszczenia jak długo nie jest się na dole. Paulina byłaby w stanie oczyścić wody w kopalni (kupi około rok czasu aż się woda zastąpi). Ale tamta woda nie powinna trafić do miasteczka. Co się stało? Maria się zobowiązała, że sprawdzi co się ostatnio (w tamtym roku) tu działo; pójdzie do biblioteki.

Ale dziewczyny zdecydowały się WPIERW wyczyścić górników (done), potem iść się wyspać i wrócić rano. Na sen wróciły do Przodka. Bo tak. Po wyczyszczeniu, górnicy poszli do domów. Paulina lekko przeedytowała pamięć.

**Dzień 3:**

Ranek. Znowu w Starogłazie. Maria myszkuje po bibliotece a Paulina siedzi w pracy.

Po kilku godzinach Maria znalazła odpowiedź - trzech "leśnych dziadków" protestowało przeciw otwarciu wielkiego sadu w Starogłazie. Studnia głębinowa, mająca 54 metry głębokości. Co ciekawe, jednym z tych trzech dziadków był poprzedni dyrektor kopalni. Mówił, że Skarbnikowi się to nie spodoba. Powiedział, że co jest pod ziemią ma tam pozostać. Ten dyrektor miał zarzuty, że za jego czasów kopalnia miała wybuch. Zalało część kopalni i eksploatacja byłaby zbyt kosztowna...

Paulina zadzwoniła więc do Krystiana Korzunio.

* Korzunio. - Korzunio
* Paulina Tarczyńska - Paulina

Paulina wyjaśniła, że potrzebuje zniszczenia studni głębinowej i pewności, że druga nigdy nie powstanie. Streściła mu sytuację:

* Skażone podziemne źródło, które nie jest problemem póki się nie ciągnie
* Skażona kopalnia, która nie jest problemem jak się w niej nie pływa
* Nie można ciągnąć wody z 50 metrów i głębiej

Poprosiła Korzunia o to, by załatwił odpowiednie pozwolenia i zakazy oraz by ta studnia przestała działać. Korzunio powiedział, że tym akurat może się zająć. Pogratulował Paulinie dobrego znaleziska...

# Progresja



# Streszczenie

Paulina wyleczyła ofiarę rozpadającego się diakońskiego narkotyku; niestety, w wyniku tego musiała czarować przy lekarzach. Maria znalazła miejsce, gdzie magia powodowała kłopoty; wpadła w tarapaty, lecz się z nich wyciągnęła. Gdy Paulina do niej dotarła, unieszkodliwiły górników i znalazły źródło problemu - studnia głębinowa. Paulina przekazała problem studni Korzuniowi a narkotyków Wiaczesławowi. O dziwo, wszystko się udało.

# Zasługi

* mag: Paulina Tarczyńska, ratuje życia, detoksyfikuje górników, zleca Wiaczesławowi problemy z narkotykami i Korzuniowi z biurokracją. Wciąż nie zwraca na siebie szczególnej uwagi.
* czł: Maria Newa, szukając dziwnej magii weszła do kopalni i skutecznie uciekła przed zarażonym górnikiem. Brawurowa, acz wie, kiedy się wycofać. 
* mag: Kaja Odyniec, lekarz i czarodziejka; trochę wiedźma. Chciała ratować Starogłaz, ale się poddała i uciekła. Nie wiedziała jak to zrobić.
* czł: Ireneusz Przaśnik, który współpracuje z policją chcąc oczyścić reputację Czarnego Dworu. Nie chce narkotyków czy zatruć śmiertelnych.
* czł: Katarzyna Klicz, ładna i ciężko zatruta dziewczyna; 27 lat. Uratowana przez Paulinę i lekarzy Szpitala Gotyckiego w Przodku.
* czł: Edward Ramuel, ochroniarz z Czarnego Dworu; współpracuje ze szpitalem chcąc uratować nieszczęsną zatrutą Katarzynę.
* czł: Filip Wichoszczyk, młody chłopak polujący na ładne dziewczyny dzięki diakońskim narkotykom; niestety, w Leere się rozpadły magicznie. W areszcie.
* czł: Grażyna Tuloz, pomagająca Paulinie w detoksie Katarzyny; Paulina musiała przy niej rzucić czar kilka razy by ratować Kasię.
* czł: Wojmił Rzeźniczek, starszy górnik pomagający Marii; Skaził się wodą i zaczął Skażać kolejnych. Skończył wyleczony przez Paulinę.
* czł: Artur Kurczak, policjant, który spłacił dług wobec Marii; przyjaźnie nastawiony i do Marii i do Pauliny. Kompetentny.
* czł: Zenobia Morwiczka, starsza pani mająca sprzęt dla przyjaciół Artura Kurczaka i Marii Newy.
* mag: Wiaczesław Zajcew, któremu Paulina przekazała problemy powiązane z dilerem diakonopodobnych narkotyków; zajmie się tym.
* czł: Krystian Korzunio, któremu Paulina przekazała problemy powiązane z prawnymi aspektami Skażonego podziemnego źródła wody; zajmie się tym.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki, miejsce pracy Pauliny w którym ta uratowała zatrutą dziewczynę
                        1. Klub Czarny Dwór, gdzie podano narkotyki i gdzie element przestępczy współpracował z policją i szpitalem
                1. Starogłaz, miejscowość na terenach łupków krystalicznych
                    1. Zamknięta kopalnia, kiedyś: fluorytu, potem: uranu, teraz: nieczynna. Ma zalane fragmenty.
                    1. Sad Jabłonka, intensywny sad jabłoniowy z wierconą studnią głębinową

# Czas

* Opóźnienie: 10
* Dni: 3

# Narzędzia MG

## Cel misji

* Misja testująca nowy wariant magii: QUICK (koncentracja), NORMAL (5sek), POWERFUL (15 sek?), RITUAL (projekt)
* Misja testująca działanie wpływu: SŁABY, NORMALNY, POTĘŻNY i płynności mechaniki oraz Impulsów: Takt/Strat
* Czasami nawet i bez magów pojawiają się problemy a magowie nie poradzą; problemy specyficzne dla Leere
* Misja w pewien sposób determinująca co zrobi Paulina mając taką klasę problemu

## Po czym poznam sukces

* Oczekuję, że nowa mechanika CR da mi następujące wyniki:
    * Będzie niewiele wolniejsza niż poprzednia
    * Taktyczne / Strategiczne dadzą większe wychylenia testów
* Oczekuję, że nowa mechanika magii da mi następujące wyniki:
    * Rozróżnienie prędkości czarowania -> magowie mogą czarować sytuacyjnie w różny sposób
    * Pojawienie się większej DELTY mocy magicznej. Duży wpływ magii to DUŻY wpływ.
    * Może - pojawienie się rytuału magicznego bazującego na Pryzmacie?
* Leere
    * Ten typ problemu nie mógłby wystąpić w miejscu innymi niż Leere; specyfika regionu będzie wzmocniona
    * Magowie są ogólnie bezradni w tym momencie; trzeba dotrzeć do źródła problemu
* Klasa problemu
    * Paulina może zdecydować się robić rzeczy do których nie do końca pasuje lub pozwolić ludziom cierpieć
* Maria jest postacią proaktywną i interesującą

## Wynik z perspektywy celu

* Mechanika CR: 
    * Spełnione. I generuje lepsze odpowiedzi.
    * Zdecydowanie ciekawsza rzeczywistość wychodzi przy nowej mechanice.
* Mechanika Magii:
    * Bardziej czuć magię; magowie mogą więcej.
    * Magia nadal nie jest wszechpotężna.
* Leere:
    * Oba wydarzenia - i narkotyki i podziemne źródło jedynie pokazały nieumiejętność magów w tym obszarze.
* Klasa problemu:
    * Paulina wykorzystała kontakty by ratować misję.
* Maria: udało się.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|           |          |          |           |              |         |             |          |         |            |          |          |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|          |        |         |         |             |            |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|          |            |         | +x Wx (xx)   |  x WX   |
