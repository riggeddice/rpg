---
layout: default
categories: inwazja, campaign
title: "Prawdziwa natura Draceny"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Ucieczka do Przodka](kampania-ucieczka-do-przodka.html)

## Opis

-

# Historia

1. [Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html): 10/05/09 - 10/05/11
(170725-krzywdze-bo-kocham)

Silgor próbował rozwiązać problem Draceny jako 'pistoletu' zagrażającego rodowi i wysłał Dytę, by ta znalazła siły mogące ją wyciągnąć i uszkodzić politycznie Draconisa. Udało się nadspodziewanie dobrze - dwa małże zniewoliły maga i z nim uciekły, Jodłowiec ma ciężkie Skażenie kiczo-kogutowo-ceratowe, Dracena uciekła a on sam wiele płacić nie musiał...

1. [Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html): 10/05/12 - 10/05/13
(140928-policjant-widzial-aniola)



1. [Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html): 10/05/30 - 10/06/07
(141012-aplikanci-widzieli-gorathaula)

Nikola jest w fatalnym stanie; Paulina zrobiła co mogła, ale pomógłby Sebastian... więc Paulina wybyła do Piroga szukać młodego nędznego terminusa. Tam Sebastian dla Karoliny manipulował egzaminem kandydatów na terminusów by uzyskać od nich artefakty osobiste (plan Karoliny). Paulina nauczyła młodego Salazara, że ludzistyczne metody leczenia są przydatne, zniszczyła dominację Joachima Kopca i unieszkodliwiła plany Karoliny przez sprawienie, że Salazar zaeskalował do swojego Rodu. A potem poszła spłacać przysługi ;-).

1. [Lekarka widziała cybergothkę](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html): 10/06/08 - 10/06/09
(141019-lekarka-widziala-cybergothke)



1. [Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html): 10/06/10 - 10/06/11
(141025-gildie-widzialy-protomaga)



1. [Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html): 10/06/12 - 10/06/13
(141026-dracena-widziala-swoj-koszmar)



1. [Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html): 10/06/14 - 10/06/15
(141102-paulina-widziala-sepy-nad-nikola)



1. [Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html): 10/06/16 - 10/06/17
(141110-prawdziwa-natura-draceny)



1. [Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html): 10/06/18 - 10/06/20
(160901-uwieziony-w-komputerze)

Zmodyfikowany przez niewiadomego maga pająk astralny polował na młodych ludzi i więził ich "dusze" na pirackim serwerze popularnej gry "Gnomy i Śluby". Zdesperowana czarodziejka Świecy, Lilia, której syn (człowiek) wpadł w jego sidła ściągnęła na pająka terminusa i Paulinę. Udało się uratować większość ludzi i unieszkodliwić pająka (który trafił do Świecy na badania), ale Paulina jest Skażona po akcji, acz solidnie zarobiła. Dracena się poważnie zdestabilizowała - nie powinna była jednak w tej akcji uczestniczyć (Paulina nie chciała).

1. [Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html): 10/06/21 - 10/06/24
(160904-rozpaczliwie-sterowany-wzor)

Dracena jest niestabilna. Powoli jej wzór zaczyna się stabilizować w "coś" (z dominującym akcentem wiły). Paulina rozpaczliwie uniemożliwia Dracenie zrobienie komuś lub sobie krzywdy; do korekty wzoru ściągnęła Kingę Toczek do pomocy. Dracena powiedziała Paulinie o swej przeszłości - trzy siostry, jedna to cruentus reverto, Dracena kontrolowana przez Draconisa by nie poszła w mroczną magię.

1. [Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html): 10/06/25 - 10/06/26
(160907-dracena-blakenbauer)

Stan Draceny się pogarsza; coraz gorzej się kontroluje. Szczęśliwie, jej wzór się stabilizuje. Niestety, łapie uzależnienie magii krwi i zdecydowanie dominuje wiła. Paulina ściąga Netherię do pomocy; ta zdradza, że Dracena jest adoptowana (jak i Antygona). Oryginalna matryca Draceny to był Blakenbauer - i jeśli wzór Draceny nie zostanie silnie przesunięty ku Diakon, Blakenbauer (jako silniejszy ród) wróci i Dracena zmieni się w viciniusa.

1. [Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html): 10/06/27 - 10/06/29
(160910-na-zywym-organizmie-draceny)

Pojawił się terminus, który chciał powęszyć; szczęśliwie nadmiar energii erotycznej i biedna Kinga wyłączyły go z akcji i przeniosły do "team Dracena". Głód i destabilizacja pożerają Dracenę coraz skuteczniej; Paulina by ratować osobowość Draceny postanowiła doprowadzić do destabilizacji post-kralotycznej z nadzieją, że zresetują nieudane mutacje i przetrwają jakoś psychozę. Netheria i Kinga współpracują ściśle z Pauliną.

1. [Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html): 10/06/30 - 10/07/03
(160918-walka-o-dusze-draceny)

Dracena została ustabilizowana. Psychotyczne działania sformowały efemerydę w kształcie starszej siostry Draceny, Najady. Netheria zorganizowała imprezę, udało się Zespołowi zbudować taką Dracenę jak marzyła Paulina (i sama Dracena), ale potem Najada zmusiła Dracenę do konfrontacji ze swoją przeszłością i pragnieniem bycia samotną. Sprzęgła Dracenę z Pauliną (pośrednio), by ta nie mogła już być sama i po prostu uciec.

1. [Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html): 10/07/02 - 10/07/04
(161018-ballada-o-duszy-ognistej)

Pewien czas temu, wyciek syberionu został opanowany w Stawni. Jelena Zajcew pojawiła się tu by poznać opowieść o swojej kuzynce, Kseni. Jednak okazało się, że wyciek NIE został wyczyszczony całkowicie... wraz z Henrykiem Siwieckim spróbowali coś z tym zrobić. Przebijając się przez Pryzmat, odkryli, że są DWIE efemerydy różnych Pryzmatów. Ogniem zniszczyli Toksynę a potem uspokoili Ogień. Nieświadomie, Jelena inkarnowała Ksenię w Halinie Weiner; ale oczyścili Stawnię. Ponownie.

1. [Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html): 10/07/11 - 10/07/13
(161002-wyciek-syberionu)

Czas spłacić dług u Korzunia. Paulina i Dracena przyjechały do Stawni, gdzie po wycieku syberionu - zdaniem Korzunia - mogło coś zostać. I faktycznie, nie wszystko wyczyszczono; echa syberionu zgrupowały się w małe źródła magii. Paulina bez problemu odwraca typowe przypadki. Usłyszała od Derwisza ze Świecy, że tu większość osób straciło magię; teren jest prawie bez magów. Lokalni Weinerowie (11) - po odebraniu pamięci - zaczynają sobie przypominać. Pryzmat syberyjski sprawia, że legendy wracają do życia. Dwie legendy Paulina i Dracena już zniszczyły - Ognistą Damę oraz Morową Pannę. Sytuacja jest "pod kontrolą", ale trzeba znaleźć źródło problemów...

1. ['Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html): 10/07/14 - 10/07/16
(161009-paulino-zmienilas-sie)

Zniszczenie Morowej Panny wzmocniło efemerydę Syreny, co sprawia, że leczenie Pauliny działa słabiej. Pojawiają się tarcia w zespole - Paulina musi radzić sobie z Marią i Draceną - a te dwie nie są ze sobą kompatybilne. Paulina i Dracena przekształciły źródła w takie, które utrzymują Maskaradę mocniej. Lekarz Kurt Weiner szuka, co Paulina chce osiągnąć szukając wszystkiego co najgorsze. Halina chce być viciniusem (Paulina wybiła jej to z głowy) i powiedziała, że Vault i Agregat Pamięci są w jeziorze...

1. [Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html): 10/07/22 - 10/07/25
(170212-nieufnosc-w-malym-miescie)

Paulina wezwała do pomocy Kajetana i Lidię, by pomogli jej opanować Vault. Kajetan przybył z informacją, że na tym terenie był Overlord Hipernetu - Laetitia Gaia Rasputin Weiner. Dracena zniszczyła kolejną efemerydę, ale dostała luki w pamięci. Paulina próbuje pogodzić Kajetana (i potrzebę bezpieczeństwa) z Draceną (i potrzebą wolności); szczęśliwie, Laetitia się objawiła. Po wstępnym przebadaniu Laetitii - nie trzeba z nią walczyć, można to wszystko naprawić. Lidia dostała od Andrei Wilgacz autoryzację jako Regent Świecy...

## Progresja

|Misja|Progresja|
|------|------|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html) zniewolony przez małże.|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html) uznany za bohatera za próbę uratowania Jodłowca i szybkie myślenie.|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html) straszliwy cios w politykę, reputację itp. przez Silgora i Dracenę.|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html) +1 surowiec od Diakonów (Silgor)|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html) +1 surowiec od Diakonów (Silgor)|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|[Marek Śmietanka](/rpg/inwazja/opowiesci/karty-postaci/1709-marek-smietanka.html) uznanie w Świecy (Trocin) za akcję z pająkiem i odpuszczenie Lilii|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) tymczasowo silna destabilizacja|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) tymczasowo Skażona|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) #1 Fire #1 Void; zapłacono jej zdecydowanie nadmiernie|
|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) uzyskała dominujący aspekt Wiły|
|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) już nie jest Skażona|
|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) -1 void (na kryształy Mausów dla Draceny)|
|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) + głód magii krwi (słaby)|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) stan psychozy po destabilizacji post-kralotycznej|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html) okazała się być wpływowa w Millennium; podobno jest mistrzynią szpiegów|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html) bardzo dziwna relacja z Kingą Toczek; zagraża jego karierze i jest dlań... nową obserwacją (impulsem?)|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) została ustabilizowana jako Diakonka 8, Wiła 5, Spustoszenie 5, Cybergothka 8, Weapon 1|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) została tymczasową Uzależnioną od Pauliny.|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) została tymczasową Panią Draceny.|
|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html) dostał zabutelkowanego miecztapauka od Jeleny Zajcew. Na wszelki wypadek.|
|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) dostał 1 void|
|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) #1 void|
|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html) autoryzowana przez Andreę, została regentkę Świecy na Stawnię i okolicę.|

