---
layout: inwazja-konspekt
title:  "Lekarka widziała cybergothkę"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141012 - Aplikanci widzieli gorathaula (PT)](141012-aplikanci-widzieli-gorathaula.html)

### Chronologiczna

* [141012 - Aplikanci widzieli gorathaula (PT)](141012-aplikanci-widzieli-gorathaula.html)

## Punkt zerowy:

W czasie, gdy Paulina rozwiązywała problem Sebastiana Teczni i młodych egzaminowanych magów, w Wykopie Małym pojawiło się dwóch magów Millennium. Dracena Diakon (DiDi) i Rafael Diakon. Relacja między nimi była prosta: Rafael jest magiem który miał zrobić test a Dracena miała go zdać lub oblać.

Rafael Diakon znalazł człowieka i przekształcił go mocą lifeshapera. Dracena miała tego człowieka znaleźć i go naprawić. Tyle teoria. Sęk w tym, że Rafael Diakon przekształcił młodego chłopaka (symulując stopniowe łamanie Maskarady przez Skażenie) a Dracena... znalazła Nikolę. I się zmartwiła. Jak do cholery ma naprawić NIKOLĘ? Owszem, mogła skontaktować się z Rafaelem jeśli coś nie będzie prawidłowe (zachęcał), ale uniosła się dumą i stwierdziła, że sobie poradzi. Co Rafael zaplątał, ona rozplącze. Bo przecież inaczej nie dostałaby takiego zadania. A Rafael Diakon, eks-EAM który przeszedł do Millennium rozpoznał wzory EAM na Nikoli Kamień... i stwierdził, że musi się przyjrzeć całej sytuacji i nie ujawniać. Póki albo nie skontaktuje się z nim Dracena, albo nie skontaktuje się z Draceną EAMka.

* 13 dni przed misją: Rafael zakończył transformację młodego chłopaka.
* 12 dni przed misją: Dracena weszła do akcji i zagnieździła się w Wykopie Małym.
* 10 dni przed misją: Dracena znalazła Nikolę.
* 08 dni przed misją: Dracena skończyła planowanie i obserwację terenu. Zbliżyła się do lekarza, Ryszarda Bociana i się z nim przespała. Następnie pokazała mu zdjęcia i filmik i poinformowała, że zrobi dokładnie to czego Dracena sobie życzy - w innym wypadku przekaże te informacje jego żonie.
* 08 dni przed misją: Nikola się rozchorowała.
* 07 dni przed misją: Dracena zapłaciła niewielką kwotę 50 zł (będąc w formie jakiegoś chłopaka ze szkoły Nikoli) koleżance Nikoli, by ta poprosiła wychowawczynię o wysłanie Nikoli do lekarza, bo "kaszlała krwią w ubikacji". I lekarz obsłużył Nikolę - Ryszard Bocian. Dla Draceny kluczowe było badanie krwi. Lekarz przekazał też Nikoli tabletki, które służyły do połączenia jej magicznie z jej własną krwią (katalitycznie). Lekarz nie wiedział, że Scorbolamid jaki daje został magicznie przez Dracenę przekształcony wcześniej.
* 04 dni przed misją: Dracena zdobyła kilka hipotez jak jej pomóc i zaczęła tworzyć leki i kontrmagię.
* 03 dni przed misją: Lekarz dał Nikoli odpowiednie leki od Draceny. Poprawiło się. Ale Dracena nie jest lekarzem, wzór Nikoli się przekształca. Dracena rozpoczęła wstępne eksperymenty z wykorzystaniem magii krwi (bazowanej na krwi Nikoli, krwawych rytuałów) do celów leczniczych, świadoma ryzyka.
* 01 dzień przed misją: Stan Nikoli się poprawia bardziej, Dracena opracowała lek na bazie krwawych rytuałów. Cały czas jest nieświadoma, że pracuje na MAGU a nie CZŁOWIEKU, myśli, że źródło energii samo się wyczerpie. Cały też czas szuka źródła energii Skażającego Nikolę.
* 00 dzień: Pojawiła się niczego nieświadoma Paulina, która może jedynie powiedzieć Nikoli, że Sebastian jej zostawił. Nie ma pojęcia, co zastanie...

## Misja właściwa:

Paulina udała się do Nikoli po rozpakowaniu się i na wejściu do mieszkania Nikoli (i rodziców) zauważyła, że aura Nikoli jest zupełnie inna niż powinna być. Paulina chwilę poobserwowała i poczekała, aż jej rodzice wyjdą z domu. Gdy wyszli, Paulina zapukała i Nikola wpuściła ją do mieszkania. Nikola bardzo się ucieszyła widząc "archanioła". Zapytana przez Paulinę, powiedziała że była chora, ma zwolnienie lekarskie i nie chodzi do szkoły i zdaniem lekarza ma grypę. Paulina zdecydowała się ją przebadać magicznie, korzystając z kontaktu magicznego. Paulina zaobserwowała w Nikoli elementy magii krwi, katalizę, puryfikacji... ogólnie, wygląda to tak, jakby ktoś chciał ją spuryfikować i odwrócić to, co się jej stało. ALE! Tak jakby robić to człowiekowi, nie magowi. Innymi słowy, nie uwzględniono, że źródło energii jest wewnętrzne a nie zewnętrzne. To powoduje, że Skażenie po prostu się przesunie... a MK nie pomaga. 
Nikola opowiedziała historię choroby: zachorowała 8 dni temu. Faktycznie, z obserwacji nie zaczęło się to wcześniej. U lekarza była dwa razy - 7 dni temu i 3 dni temu. Następna wizyta jest pojutrze. Zdaniem Pauliny, choroba jest wywołana magią. Nie jest "groźna", w sumie jest tylko uciążliwa. Objawy lekkiej grypy.

Nikola jest aniołem "niepełnym". Paulina może przekonać ją, że jej anielskość przechodzi w fazę uśpienia. Jeśli to zrobi, jeśli przekona Nikolę, że SAMA NIKOLA chce stać się "normalna", transformacja pójdzie w tym kierunku. Paulina spróbowała przekonać Nikolę, że jej "anielskość" ją naraża. Że to sprawia, że osoby dookoła niej też są narażone. Że to ją alienuje. Że prowadzi do czegoś obcego. Paulina chce, by NIKOLA chciała być człowiekiem. (9 v 7 -> 11) Paulina osiągnęła swoje, Nikola pragnie człowieczeństwa.

Nie rozwiązuje to problemu tajemniczego maga, ale ułatwia sprawę Paulinie. Żeby zapewnić sobie bezpieczeństwo i pomóc Nikoli, Paulina zdecydowała się wpierw jeszcze pomóc w dziedzinie tej nieszczęsnej Magii Krwi. (14 vs 12 -> 10) Paulina jest w stanie pomóc Nikoli, ale żeby zapewnić bezpieczeństwo transformacji, potrzebuje pomocy tego drugiego maga. W innym wypadku rozsypią sobie wzajemnie pracę i cholera wie, w jakim stanie skończy Nikola...

Maria spytała Paulinę, czy ta chce wiedzieć coś na temat "swojego maga". 12 dni temu do tego samego hostelu w którym mieszka Paulina przyjechała młoda cybergothka która przedstawia się jako Anna Kowalska i płaci trochę więcej, by nikt nie pytał. Ciekawa była, czy dzieje się tu coś nietypowego (i naturalnie skierowano ją do wypadku policjanta). Artur powiedział, że pytała o jego wypadek i powiedział jej o aniele. Była BARDZO przekonywująca. Artur powiedział o aniele jako o plotce, nie powiedział o Nikoli czy o prawdziwej mocy. Ale najpewniej cybergothka sama znalazła sobie resztę...

Paulina przygotowała jeszcze wisiorek, który Nikola ma nosić. Coś, co będzie dawało lekkie echo magiczne i coś, co ma zapisany kontakt do Pauliny. Jak jakiś mag dorwie się do Nikoli, będzie wiedział jak się z nią skontaktować. Przygotowała też magiczną ulotkę, którą dała recepcjoniście w hostelu, adresowaną do "Anny Kowalskiej" z tymi samymi detalami.

02:27 w nocy. Pukanie. Cybergothka. Paulina wpuściła ją do środka. O dziwo, cybergothka, jakkolwiek nie wygląda absurdalnie, ma zdrowsze podejście do magów, magii i walki w hostelach niż dotychczas spotykani przez Paulinę kinderterminusi. Miła odmiana - obie na samym początku zapewniły się o ostrożnej chęci nieagresji i chęci znalezienia wspólnych intencji. Cybergothka przedstawiła się jako Dracena Diakon. Ustaliły wspólną sytuację - Dracena szuka człowieka, którego 12 dni temu skończył przekształcać Rafael Diakon. Znalazła Nikolę i założyła, że to ona jest jej celem. Paulina powiedziała, że Nikola nie jest człowiekiem - jest protomagiem. I powiedziała, że skrzywdzili ją magowie i porzucili. Dracena poprosiła Paulinę do swojego pokoju, gdzie ma laboratorium. 

Zaczęły wstępną współpracę (Dracena i Paulina). Dracena pokazała Paulinie swoje "laboratorium" - grupę magicznych aparatów i próbki krwi Nikoli. Zgodziły się, że mając wiedzę i umiejętności Draceny oraz Pauliny są w stanie zarówno przekształcić Nikolę w maga jak i w człowieka. Będzie to proces wolny i długotrwały, ale są w stanie. Paulina zaproponowała zacząć następnego dnia, ale Dracena stwierdziła, że lepiej będzie dać jej moc magiczną. Różnica punktów widzenia - czy szczęśliwsza będzie jako człowiek czy mag. Dracena zaznaczyła, że może zaproponować ją jako maga swojemu ojcu, w Millennium. Powiedziała też, że Millennium pozwala magom posiadać rodziny poza światem magów, nie to co SŚ. Ustaliły, że przedyskutują temat później a Dracena musi przekonać Paulinę, że Nikola będzie szczęśliwsza w Millennium niż będąc człowiekiem.

Paulina też zauważyła, że poziom energii magicznej u Draceny jest wysoki. Na tyle wysoki, że następuje zjawisko ciągłego, choć nie bardzo dużego Skażenia. Jednak jest lekko Paradoksalna. Gdy Paulina zwróciła Dracenie na to uwagę, ta powiedziała Paulinie, że jest Poczwarką. Jest podczas Wspomagania. Widząc niezrozumienie, uzupełniła - w jej gałęzi rodu jest zwyczaj, że mag może zażądać lub otrzymać zadanie do wykonania. I podczas wykonywania tego zadania będzie przeenergetyzowana. W ten sposób rozwiązanie tego zadania (lub nie wykonanie tego zadania) zdeterminuje w jakimś stopniu kim tak naprawdę jest i jak się przekształci.

Następnego wczesnego południa (11:43) Paulina się obudziła. Wcześniej wysłała mentalną prośbę do Marii, by ta podmieniła leki Nikoli na zwykły Scorbolamid, więc mogła się wyspać po nocce spędzonej na rozmowie z DiDi (tylko rozmawiały). Maria powiedziała Paulinie, że u Nikoli był dziś lekarz (Ryszard Bocian). Maria stwierdziła, że był u Nikoli jakieś 45 minut. Ale wszystko się zgadza - jego samochód, podobnie się zachowuje, to chyba był on. Paulina nie ma tak naprawdę jak tego ruszyć - tajemnica lekarska etc. Powiedziała Marii, że nic z tym na razie nie zrobią.

14:12, obudziła się Dracena. Paulina ją odwiedziła, jak ta jeszcze wplatała przewody we włosy. Paulina spytała Dracenę, czy ona wysłała lekarza do Nikoli; Dracena zaprzeczyła. Powiedziała, że pójdzie do lekarza i wyjaśni sprawę. Paulina była dość nieufna - jak mag mówi "złożę komuś wizytę" to zwykle znaczy "zabiję go i spalę jego psa". Dracena zaprzeczyła - ona tak się nie zachowuje, ona nie robi ludziom krzywdy. Paulina udała, że wierzy, jednak wysłała za nią Marię. Maria dowie się co i jak się stanie w gabinecie lekarskim...

Tymczasem z okazji skorzystała Paulina i poszła porozmawiać z Nikolą. Spędzić z nią trochę czasu, porozmawiać, zbudować relacje... a przy okazji Nikola powiedziała jej, że był u niej dziś lekarz, Ryszard Bocian. Lekarz wyglądał na zmartwionego, choć wszystko jego zdaniem jest w porządku, ale jeszcze raz pobrał krew i chciał się upewnić co do leków jakie Nikola bierze. Prosił, by nikomu nie mówiła że tu był... no ale archanioł :P. Spędziła z nią chwilę i wyszła zanim rodzice się pojawili.

Z Pauliną skontaktowała się Maria - lekarz i Dracena poszli na spacer. Lekarz nigdy nie był w przychodni; Dracena wyciągnęła go z domu i poszli na spacer. Poszli do lasu, zdecydowanie nieodpowiednio ubrani (no, w takim stroju się po gąszczach nie chodzi a zeszli ze ścieżki). Maria nie była w stanie ich śledzić nie ryzykując zauważenia, więc jedynie oznaczyła miejsce zejścia ze ścieżki i się oddaliła. Poprosiła Paulinę, by ta zajęła czymś Dracenę później a ona spróbuje pójść po śladach Draceny i doktora.

16:22, Dracena wróciła do hostelu. Powiedziała Paulinie, że załatwiła temat z lekarzem. Lekarz powiedział, że się bardzo martwił obecnością Draceny i badaniem krwi, więc się wybrał do Nikoli powtórzyć badania i upewnić się, że nic się jej (Nikoli) złego nie dzieje. Dracena powiedziała lekarzowi że jest detektywem i szuka osoby o odpowiedniej zgodności DNA. A przebranie cybergothki dobrze jej służy, bo nikt przecież nie będzie się spodziewał, że cybergothka jest detektywem... no i nikt jej nigdy nie rozpozna. 
Rozpętała się burzliwa dyskusja między Pauliną i Draceną odnośnie dalszych losów Nikoli. Dracena jest zwolenniczką tego, by zrobić z niej maga a Paulina, by człowieka. Pokłóciły się o to, czy mag czy człowiek ma lepsze życie i większy wpływ. Paulina zarzuciła Dracenie "Primum non nocere", Dracena "działam wbrew wszelkim zasadom magów, gildii i swemu instynktowi by w ogóle rozważać to co do mnie mówisz". Dyskusja była długotrwała, ale stanęło na tym, że kontynuują badania i rozchodzą się w pokoju. Dyskusja, badania itp. zajęły jakieś 3h, co było wystarczające dla Marii.

Maria jest świetnym polujowcem (znaczy: łowcą, ale mniej świetnym). Znalazła miejsce, gdzie lekarz i Dracena "rozmawiali" i się zorientowała, że oni nie tylko rozmawiali. Zdecydowali spędzili tu razem bardzo miłe chwile. Dodatkowo, szczęśliwym trafem (15+ -> 16) udało jej się ominąć fakt, że Stilgor Diakon obserwował okolicę; zauważył Marię, ale zignorował jej obecność, nie powiązał jej ani z Draceną ani z Pauliną. Jak tylko Dracena się oddaliła do pokoju (~19:30), Maria skomunikowała się mentalnie z Pauliną i nakablowała. Paulina... cóż, nie zdziwiło jej to specjalnie. Maria ponuro zauważyła, że przecież lekarz ma rodzinę... Paulina zaznaczyła, że znając reputację Diakonów, Dracena wcale nie musiała używać magii. Jedna konkluzja Marii "trzymaj ją z daleka ode mnie".

19:45. Paulina poszła do DiDi by się z nią skonfrontować odnośnie biednego, uwiedzionego lekarza. Złapała ją rozmawiającą przez telefon z "kochaną Kasieńką" i sprawdzającą, czy wszystkie diody się świecą. DiDi widząc Paulinę zaprasza ją na imprezę do klubu. Dziś DiDi jest DJem. Paulina w panice - przecież ktoś ją może rozpoznać. DiDi zauważyła z szelmowskim uśmiechem, że w masce wszyscy wyglądają tak samo a Paulina powinna cieszyć się życiem. Ponadto - czy Paulina nie jest ciekawa z jakimi ludźmi się DiDi zadaje? (4+1 za to, że Paulina obawia się że DiDi chce ją wykorzystać vs 3+1 za ciekawość Pauliny -> 3). Fail. Paulina idzie na imprezę do technoklubu...
Maria dostała prośbę, żeby jeśli KTOKOLWIEK rozpozna Paulinę... nikt się nigdy o tym nie dowiedział. Zabijanie i topienie ciał w kwasie dozwolone!!!

DiDi zaproponowała dyskretny, czarny strój cyber, bez neonów i światełek, z dyskretną maską i brakiem silnego dekoltu. W sumie: bardziej NIE rzuca się w oczy niż się w oczy rzuca... w perspektywie tych INNYCH osób na imprezie, naturalnie. Nawet to jednak jest ciężka przeprawa... i spóźniły się na spotkanie z "Kasieńką".
...tyle, że ona spóźniła się bardziej.
Jak tylko Kasia się pojawiła, zdecydowanie spóźniona (kilkanaście minut), DiDi zaczęła węszyć, po czym natychmiast się rozpromieniła i pogratulowała Kasi pytając kto jest szczęśliwcem. Kasia spłoniła raka i udała że nie wie o co chodzi. Na oko, w wieku Nikoli (16-17). Tak czy inaczej, z chichoczącą DiDi ruszyły na imprezę.

Na samej imprezie jest 17 osób wliczając DiDi, Katarzynę i Pauliny. Paulina skupiła się na DiDi, sprawdzić jak ta się zachowuje i jakie ma podejście. Zobaczyła DiDi - osobę rozluźnioną, szczęśliwą, traktującą ludzi jak sobie równą, docinającą innym i pozwalającą im dociąć sobie. Nie tylko DJka, ale i uczestniczka zabawy, która nie zachowuje się jak prawdziwy mag.
Paulina zdecydowała się dołączyć. Spytała DiDi, czy jest tu jakaś gitara i słysząc, że tak, zdecydowała się zagrać. Umiejętności Pauliny nie są może epickie (base: 3), ale Paulina nie przejmuje się tym, czy zrobi wszystko perfekcyjnie - chce działać w rytmie imprezy. DiDi jej pomogła, odpowiednio ustawiając wcześniej imprezę i piosenki przygotowując wejście Pauliny. Wynik (3+2 -> 8) przerósł wszystkich najśmielsze oczekiwania, łącznie (a może szczególnie) Pauliny.

Dziewczyny posiedziały tak do 01:00 na imprezie, po czym zdecydowały się zbierać. Paulina tym szczęśliwsza, że Maria (ziewając) poinformowała ją, że zdziwiłoby ją jakby ktoś ją rozpoznał bo ONA jej nie rozpoznała. Całej trójce wieczór się bardzo podobał. Rozluźnione i wesołe wpierw odprowadziły Kasię, lecz po drodze do domu Kasi po drodze przechodziły koło sklepu monopolowego, gdzie zaczepiło ich trzech osiłków. Rzucili "motomyszy na Marsa" i "myszki zdejmujcie ciuszki", na co DiDi odgryzła, że jeśli oni je z myszami z Marsa mylą, to za dużo czasu spędzili wspólnie z kolegami w łóźku. To zdenerwowało chłopaków i zaczęli okrążać DiDi, Paulinę i Kasię. Kasia spanikowała, ale DiDi i Paulina złapały ją mocno. DiDi też ma nietęgą minę. "Rafael, to nie jest śmieszne". Ci troje zarechotali, że żółw ninja im nie pomoże.
Paulina natychmiast poprosiła Marię o ściągnięcie policji. ASAP.
Jednocześnie Paulina zaczęła z nimi dyskutować, prosząc, by nie robili im krzywdy. Jednocześnie zaczęła ich sondować - co ich motywuje (niemagicznie). (7 vs 4 -> 5), są zainteresowani przede wszystkim ich przestraszeniem i zabawieniem się kosztem dziewczyn. Nie zrobią im poważnej krzywdy, choć mogą pobić. Ale co istotne - Paulina, DiDi i Kasia były nacelowane. Ktoś im wskazał tą trójkę. Oni czekali na nie.
Paulina obrała taktykę na dokładnie to, czego druga strona chce, by maksymalnie przedłużyć (5 + 3 vs 5 -> 7). DiDi także pomogła - udając przerażoną, zaczęła się powoli rozbierać. Doszła do etapu pokazania piersi, gdy wezwany przez Marię Artur Kurczak się pojawił krzycząc "policja". Na ten widok bandziory zaczęły uciekać a DiDi mogła zapiąć bluzkę. Oczywiście, cały czas dobrze się bawiła, jak to DiDi...
Paulina bardzo podziękowała Arturowi, który... jej nie rozpoznał. Sytuację częściowo uratowała DiDi, która gorąco podziękowała policjantowi (który ją natychmiast rozpoznał). Powiedziała, że ta trójka chciała im zrobić straszną krzywdę. Artur zobowiązał się do odprowadzenia ich oraz Kasi. I tak się stało.

Już w hostelu Paulina powiedziała DiDi, że to był atak kierowany. Ktoś skłonił tych ludzi do zaatakowania dwóch magów i człowieka, z intencją zrobienia krzywdy. DiDi się zasępiła i Paulina spytała, czy ów "Rafael" jest osobą do tego zdolną. DiDi stwierdziła, że Rafael... to chyba nie w jego stylu. DiDi nie jest pewna. Nie zna go. Jest jednak magiem EAM... na to Paulina się spięła i powiedziała, że właśnie EAMka zrobiła to co się stało Nikoli. DiDi zaczęła gorąco protestować - to nie Diakonka, jest jeden Diakon na EAM... no, był. I to był Rafael.
W świetle tego Paulina spytała DiDi, czy wie dla KOGO lekarz pobrał tą krew. Nie, nie wie... powiedziała, że się dowie. Ale rano. Na razie po prostu nie wie...
Paulina poprosiła Marię, by ta popilnowała Nikoli w nocy. Maria stwierdziła, że nienawidzi Pauliny... i że popilnuje.
 
 
"To ile Ań macie w rodzinie? - Paulina"
"ANI jednej" - Dracena.
 
"Chcesz wyglądać dyskretnie ekstrawagancko?" - Dracena do Pauliny odnośnie ubioru na imprezę

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Wykop Mały
                    1. Osiedle Ludowe
                        1. domki, gdzie mieszka min. Nikola Kamień
                        1. hostel Opatrzność, gdzie pokoje mają zarówno Paulina jak i Dracena
                        1. Klub Klimat Nocy
                    1. Zewnętrze
                        1. gęsty las

# Idea:

- Wprowadzenie Draceny Diakon jako potencjalnej sojuszniczki i ciekawej postaci. Chwilowo: "ja siama".
- Wprowadzenie Rafaela Diakona, eks-EAM, w nowym miejscu i pozycji. "Mnie kazano obserwować, nie działać".
- Podjęcie decyzji co do przyszłości Nikoli.

# Zasługi

* mag: Paulina Tarczyńska, ostro ścierająca się z Draceną odnośnie losu Nikoli, ale nie uważająca czarodziejki za zagrożenie.
* czł: Maria Newa jako wsparcie i pomoc Pauliny.
* mag: Dracena Diakon jako buntownicza lifeshaperka pragnąca pomóc ludziom.
* mag: Rafael Diakon jako obserwator testujący Dracenę.
* ???: Nikola Kamień jako mag/człowiek/vicinius będący przedmiotem o który wszystko się rozbija.
* czł: Ryszard Bocian jako lekarz wykorzystany przez Dracenę.
* czł: Artur Kurczak jako policjant, który przespał się z cybergothką.
* czł: Katarzyna Trzosek jako koleżanka ze szkoły Nikoli która wraz z Draceną chodzi na imprezy.
* czł: Edmund Marlin jako jeden z trzech kumpli nie lubiących dziwnie wyglądających dziewczyn.
* czł: Mikołaj Mykot jako jeden z trzech kumpli nie lubiących dziwnie wyglądających dziewczyn.