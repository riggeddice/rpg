---
layout: inwazja-konspekt
title:  "Otton zabija Zetę"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160224 - Aniołki Marcelina (LB, KB)](160224-aniolki-marcelina.html)

### Chronologiczna

* [151216 - Między prawdą i fikcją Arazille... (AB, DK)](151216-miedzy-prawda-i-fikcja-arazille.html)

### Inne

Misja równoległa:

* [160224 - Aniołki Marcelina (LB, KB)](160224-aniolki-marcelina.html)

## Kontekst misji:

Oddział Zeta oraz Dionizy z Aliną zostali przechwyceni przez Arazille i gdzieś wywiezieni...
Złapali Strażnika (defilera). Została Artystka i Zegarmistrz.

## Punkt zerowy

- zegarmistrz, zmieniający ludzi w proces (fabryka -> optymalizacja)
- artysta, tworzący sztukę z ludzi (galeria -> podziw)

Skład grupy specjalnej:
- Jędrzej, "najpierw niszcz, potem pytaj". Psychol i destroyer. (Zeta, kiedyś: Selfish guy. Naprawdę zły mag.)
- Marta, "stateczna acz niebezpieczna". Serce drużyny. (Zeta, kiedyś: Marta)
- Kamila, "McGyver w spódnicy". Kolekcjonerka pięknych przedmiotów. (Zeta)
- Romuald, "podrywacz i kucharz". Porwany w Czarnej Wieży (Zeta)
- Maria, "psioniczka i mental detector". (Arazille, stworzona dla Marty)
- Kleofas, "dowódca, co nic nie wie". (Arazille, stworzona dla Aliny / Dionizego i przekazał dowodzenie Dionizemu)

POI:
- ratusz
- stary młyn
- domek na osiedlu
- plac zabaw
- muzeum
- biblioteka
- komisariat policyjny
- monopolowy
- piwniczka na starówce
- poczta
- obserwatorium

## Misja właściwa

Zeta zaczęła się kłócić. Jędrzej budzi w sobie cechy psychola, Romuald zaczyna się bać walki - on jest tylko kucharzem. Marta uważa, że Arazille ich nie wypuści...
Dionizy brutalnie stwierdził, żeby uratować sprawę to musi zadziałać. Zaatakował lekko Romualda by wymusić reakcję. Skończył na ziemi lekko ranny; Maria go uratowała przed rozerwaniem. Zeta zatrzymała się oszołomiona... 
Korzystając z okazji, Alina stwierdziła, że to do niczego nie prowadzi. Przekształciła się w Jędrka i zaatakowała ich słownie. Tak wszystkich skonfundowała, że przekazali dowodzenie Dionizemu.

01) Marta, awaken. Kamila, Romuald, Jędrzej idą na zwiad. Arazille daje Dark Sleep Strażnikowi.
01) Defilerzy zorientowali się, że jednego nie ma

Kamila i Romuald poszli na zwiad. Maria skanuje defilera. Jędrek się nudzi. Marta ma flashbacki przeciw-Blakenbauerowe.

02) Kamila i Romuald zwracają informacje o tym co się dzieje; jak wygląda stan defilerów. Maria informuje jak wygląda sytuacja.
02) Defilerzy przekształcają osoby dla siebie we właściwy sobie sposób.

Maria przeskanowała padłego defilera; powiedziała, że oni współpracują ze sobą w pewnym zakresie (podzielili się wpływami). By złamać ich monopol trzeba podłożyć im skażone Arazille jedzenie ;-). Na to wrócili zwiadowcy. Powiedzieli o tym, że defilerzy dzielą się siłami i zbroją się. Nie ufają sobie, wyraźnie. Podejrzewają, że "ten drugi" zdjął Strażnika. Niekoniecznie wiedzą o siłach Arazille. Na PEWNO nie wiedzą o Arazille...

Dionizy i Alina wpadli na sprytny pomysł. Niech pojawi się taka sytuacja - Zeta zaczyna porywać ludzi i chować ich w taki sposób by byli niewidoczni mocą Arazille a jednocześnie by defilerzy myśleli, że to działania tego drugiego. Przez to, że ślady wskazują na działanie TRZECIEGO defilera, każdy z nich pomyślał, że to sojusz dwóch innych defilerów. I każdy zaczął chować i zabezpieczać swoich ludzi w swojej domenie.

03) Jędrzej wpada w szok spowodowany mocą Arazille; potężny flashback odnośnie tego co się działo gdy dowodzili Blakenbauerowie.
03') Defilerzy chowają swoich ludzi i próbują ich chronić.
04) Marta pilnuje defilerów; reszta zespołu działa tak jak powinna - szuka słabych punktów, zbiera surowce itp.
03'') Defilerzy próbują wysondować sytuację między sobą.

Marta i Romuald poszli szukać defilerów i ich pilnować; są dobrze pozycjonowani i skuteczni. Alina wpadła na pomysł przekształcić się w Strażnika; ale jak ukryć aurę magiczną? Kamila miała pomysł... głupio jej, ale się pomodliła do Arazille niezgrabnie. Niech ta udzieli Alinie ukrycia aury. Maria się zaśmiała, powiedziała, że zadziałało i podziękowała Kamili za jej niesamowicie fajną modlitwę.

Maria przesłała Alinie wszystkie dane i własności maga, który został już pokonany przez Arazille. Alina przekształciła się w tego maga. Broń, działania, wszystko jak ma być. I Alina będzie "przechwytywać" ludzi których już odbili. Alina wychodzi; chce zwabić obu defilerów w punkt starcia.

05') Zeta w gotowości bojowej, może przechwycić magów
04) Defilerzy się spotykają w kawiarence

Alina idzie w kierunku Zegarmistrza; Marta zgłasza, że w kierunku kawiarenki idzie Artystka. Idzie też Zegarmistrz. Natychmiastowa zmiana planu: niech Zegarmistrz i Artystka dotrą do wiecznie neutralnej kawiarenki. Jednocześnie, jak już tam wejdą, Romuald i Marta mają odpalić alarmy w domu Artystki (muzeum). Defilerzy mają zacząć się kłócić. Wtedy wejdzie Alina i powie "to genialne, winszuję, władco; jak uderzę, zabij". A Jędrek i Kamila zaczną działać i uderzą z zaskoczenia.

Defilerzy w kawiarence zaczęli dyskutować. Wszystko im zaczęło się układać. Wtedy - Romuald i Marta uderzyli w muzeum. Zaczęły się alarmy. Defilerka zaczęła się złościć, Zegarmistrz zaczął uspokajać co się dzieje. A Marta i Romuald - sprintem by wesprzeć resztę.

ESCALATION_1602

Krok 1: 

Na miejscu jest: Alina jako Strażnik, Dionizy dobrze uzbrojony, Jędrek, Kamila z Zety i Maria. Celem tego kroku jest zniszczenie przez Romualda i Martę czegoś bardzo cennego dla Artystki. 
ATK: 80% (to co zniszczą jest absolutną apokalipsą dla Artystki) -> skłócona
DEF: 60% (ochrona dla Romualda przed wizją ukochanej) -> przegrana

Artystka - she lost it. Niestety, Romuald też. Coś przypomniało mu o tragedii jego dziewczyny; Marta powiedziała, że Artystka tworzyła rzeźby i posągi z ludzi. Żywych. Jako defiler zasilała się nimi. Alina odesłała do Marty Marię; niech Maria pomoże Romualdowi.

Artystka uderzyła Zegarmistrza. She lost it, she completely lost it.

Krok 2:

Alina wchodzi z "istotnie, genialny plan, ekscelencjo" i składa głęboki pokłon. Całą swoją postawą przekazuje uznanie wyższości tej drugiej osoby. 

Alina chce: zaeskalować konflikt między nimi, niech nie zorientują się, że są na siebie napuszczeni. (vs 15-19). Sukces. Defilerzy są na siebie źli.
Defilerzy chcą: pożywić się energią "Strażnika" (14). -> porażka; przekute w sukces kosztem #1 tor Blakenbauer (6)
Energie kinetyczne i fizyczne chcą zniszczyć wszystko (14). -> przetrwała

Defilerzy włączyli krwawe aury i zaczęli pełną wojnę między sobą. Same rykoszety miotały Aliną, ale Alina przetrwała.

Krok 3:

Defilerzy walczą na pełnej mocy. Moc się dopiero rozwija. Ludzie w kawiarence zostali wyżarci.

Alina chce: zwiać; Jędrzej ją wyciąga za linkę (#6). -> sukces
Dionizy chce: miotnąć lapisowanym granatem by ich osłabić i wypłoszyć (19) -> sukces
Defilerzy chcą: zmasakrować Alinę rykoszetami (16). -> porażka

Alina została ranna (drugi poziom rany); uszkodzona też została jej zbroja lapisowa. Kamila zaczęła ją ewakuować. Dionizy skutecznie rozproszył defilerów i osłabił ich używając lapisowego granatu. Defilerzy zaczynają tracić źródło swojej mocy. Nie są w stanie też wyczuć tego co na zewnątrz.

Krok 4:

Oboje próbują opuścić kawiarenkę i kontynuować walkę poza kawiarenką. Jędrzej już czeka; on się skupił na Artystce. Dionizemu został Zegarmistrz.

Dionizy chce złapać Zegarmistrza w sieć -> success
Zegarmistrz_drenaż (16) -> fail
Jędrek_atak 60% -> fail
Jędrek_obrona 60% -> fail
Wsparcie defilerów: 40% -> fail

Jędrek rzucił się na defilerkę; nie udało mu się jednak jej usunąć i obezwładnić. Dionizy został ranny przez drenaż, ale to przetrwał. Strzelił srebro-lapisowaną siecią w defilera i jego tarcza została osłabiona jak i jego moc. 

Krok 5: 

Walka się toczy. Jędrek leży, defilerka skupiła się na tym by go usunąć, Kamila rzuciła się by spowolnić defilerkę, usunąć, itp. Alina wzięła defilerkę w ogień zaporowy pistoletem; dzięki temu zdecydowanie podniosła szanse Kamili.

Kamila_atak 75% -> success
Kamila_obrona 75% -> fail

Kamila skutecznie usunęła defilerkę; niestety, sama została porażona i usunięta tymczasowo z akcji. Defilerka była ta silniejsza.
Zegarmistrz skupił się na Dionizym. Mimo lapisowanej sieci, uderzył, skupiając się na nim w pełni.

Zegarmistrz_emisja (18) -> remis

Dionizy trysnął krwią. Dzięki lapisowanemu pancerzowi i naturalną obroną wytrzymał, ale rana jest raną...
Dionizy strzela neurotoksyczną strzałką i wieje jak pieprz rośnie. 

Dionizy chce osłabić Zegarmistrza neurotoksyną; udało mu się to.

Krok 6:

Alina i Dionizy mają ciężkie rany (cena kroku 6; -2 do walki). 

Alina zdecydowała się na rzucenie granatu srebrnego w Zegarmistrza. Defiler jest skupiony na Dionizym; nie zwrócił uwagi na Alinę. Srebrny granat go poszarpał; defiler poleciał jak szmaciana kukła i umarł.

END OF ESCALATION_1602

Został 1 krok do Eskalacji Zety Blakenbauerów. Marta i Maria opanowały Romualda. Przetransportowano wszystkich rannych do ciężarówki.
Debriefing.

- Strażnik i Artystka są nieprzytomni.
- WSZYSCY członkowie Zety są w złej formie.
- moc Magii Krwi zaczyna się powoli rozpraszać. Bad Mojo.

Rozmowa o powrocie do Blakenbauerów. Jędrek chce. Marta powiedziała, że szybciej umrze. Opowiedziała dlaczego... 
Chwila sympatycznej rozmowy w Zecie. Chcą się podzielić; kłócą się o ciężarówkę, ale ogólnie wszyscy są szczęśliwi.
Liżą rany i ogólnie jest dobrze...

Marta się zgodziła. Niech wezmą ciężarówkę i jadą. Niech jadą w służbie Blakenbauerów; oni zostają tu i będą bezpieczni. Alina przekonała Martę. Pożegnali się. Marta, Maria, Kamila i Romuald zostają. Jędrzej, Dionizy, Alina jadą - mają ze sobą 2 defilerów. Dionizy jeszcze próbował przekonać Romualda do powrotu; nie musi pełnić roli bojowej. Nie udało mu się, ale przekonał Kamilę. 

Nagle - straszny ból. Zeta umiera. WTF. Okazało się, że w Rezydencji Otton uruchomił "kill switch" i wysłał death signal do wszystkich Zet. Rozerwało to Welon Arazille (sprzężona) i zarówno Dionizy jak i Alina się obudzili. Są ranni, to wszystko naprawdę się wydarzyło; Kleofas Bór wyciąga Alinę i Dionizego a Olga Miodownik robi coup de grace. Margaret dowodzi akcją. Wysłała Dionizego po defilerów; Dionizy zobaczył jeszcze jedną, żywą, regenerującą się Zetę. Udał, że zastrzelił; szepnął "uciekaj, nie wracaj" i zwinął defilerów.

Kleofas wykopał (dosłownie) Arazille (nie wiedząc kim ona jest) z kabiny kierowcy i zwinął ciężarówkę.
Wrócili do domu.

Wynik:
- Dionizy ciężko ranny.
- Alina ciężko ranna.
- 2 ciężko rannych (widać wzór?) defilerów ukradziono.
- Arazille i jedna Zeta się szybko zregenerują; Arazille ma obniżony poziom energii.
- Ciężarówka wróciła do Blakenbauerów.
- Zeta została zniszczona.
- ...ogólnie to wszyscy przegrali.


## Dark Future:

Chronologia Arazille:

01) Marta, awaken. Kamila, Romuald, Jędrzej idą na zwiad. Arazille daje Dark Sleep Strażnikowi.
02) Kamila i Romuald zwracają informacje o tym co się dzieje; jak wygląda stan defilerów. Maria informuje jak wygląda sytuacja.
03) Jędrzej wpada w szok spowodowany mocą Arazille; potężny flashback odnośnie tego co się działo gdy dowodzili Blakenbauerowie.
04) Marta pilnuje defilerów; reszta zespołu działa tak jak powinna - szuka słabych punktów, zbiera surowce itp.
05) Romuald natrafia na muzeum; widzi co zrobiono tym ludziom. Jędrzej się budzi.
06) Romuald ginie do Artystki; uszkodzenie Arazille
07) Walka Marty i Jędrzeja o grupę ludzi. 
08) Kamila przejrzała przez Światło Arazille; chce popełnić samobójstwo, ucieka.
09) Marta, offline. Marta orientuje się w roli Marii. Skupia się na niszczeniu defilerów. Zmusza Jędrzeja do współracy.
10) Marta i Jędrzej skupiają się na zniszczeniu Kamili i Artystki. Artystka umiera.
11) Marta i Jędrzej podlinkowują Kamilę do Arazille używając ciężarówki.
12) Marta, Jędrzej, Kamila ruszają przeciwko Zegarmistrzowi wspomagani przez Arazille.
13) Straszliwa wojna; Arazille jako Crystal dopada Zegarmistrza.

Chronologia defilerów:

01) Defilerzy zorientowali się, że jednego nie ma
02) Defilerzy przekształcają osoby dla siebie we właściwy sobie sposób.
03) Małe starcia między defilerami
04) Defilerzy się spotykają w kawiarence
05) Defilerzy wchodzą w sojusz
06) Artystka zasila się krwawą elegancją i przekształca Romualda.
07) Zegarmistrz wysyła samobójczych ludzi przeciwko Zecie. Śmierć osłabia Arazille.
08) Zegarmistrz przekształca ludzi w łańcuch defensywny. Artystka emanuje Majestatem.
09) Artystka korumpuje Kamilę.
10) Marta i Jędrzej skupiają się na zniszczeniu Kamili i Artystki. Artystka umiera.
11) Zegarmistrz uruchamia bardzo silny system obronny, fortecę ludzi.
12) Zegarmistrz rusza przeciwko wszystkiemu, zmienić świat w maszynę.
13) Straszliwa wojna; Arazille jako Crystal dopada Zegarmistrza.

Chronologia Blakenbauerów:

01) nic
02) nic
03) nic
04) nic
05) nic
06) nic
07) nic
08) nic
09) nic
10) Unleash The Zeta Annihilation

# Streszczenie

Zeta kłóci się wewnętrznie, oddała dowodzenie Dionizemu (Alina wrobiła). Operacja - usunięcie dwóch defilerów; stworzyli dowody że ten trzeci pracuje z innym defilerem po czym Zeta zaatakowała z zaskoczenia. Mimo przewag, magowie (defilerzy) są zwyczajnie ZA SILNI, ale się Zecie udało wygrać i oddać defilerów Arazille. Otton włączył selfdestruct Zecie; tylko Marta przeżyła a Dionizy i Alina wyszli z iluzji Arazille. Dionizy MÓGŁ zabić Martę, ale tego nie zrobił; porwał defilerów i uciekł z Aliną i Kleofasem & Margaret & Olgą do Rezydencji.

# Lokalizacje:

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Komesów
                1. Toczmiejów
                    1. Muzeum
                    1. Obserwatorium

# Zasługi

* vic: Alina Bednarz, cwana, zakamuflowana, PO trzeciego defilera, właścicielka najlepszego srebrnego szrapnela w okolicy.
* vic: Dionizy Kret, przywódca, lojalny Blakenbauerom do końca, acz nie zastrzelił ostatniej Zety gdy mógł.
* vic: Arazille, która rozwiązała problem defilerów w Toczmiejowie; uszkodzona gdy Zeta została zniszczona przez Ottona. Na misji miała formę Marii.
* vic: Marta Newa, która jako jedyna Zeta przetrwała pogrom z ręki Ottona. Współpracuje z Arazille. 
* vic: Jędrzej Zeta, KIA, próbował pokonać defilerkę, ale nie do konca wyszło. Mimo swoich wad, był lojalny grupie.
* vic: Kamila Zeta, KIA, próbowała chronić towarzyszy z zespołu. Narcyz żyjący w swoim świecie, ale było w niej więcej niż się wydawało.
* vic: Romuald Zeta, KIA, kucharz marzący o normalnym życiu, który walczył by inni to normalne życie mogli mieć. 
* mag: Otton Blakenbauer, który wysłał sygnał zniszczenia do Zety. Jeśli on nie może ich mieć, nikt ich nie będzie miał. Arazille nie wygra.
* mag: Margaret Blakenbauer, głównodowodząca akcją odbicia Aliny i Dionizego (i pozyskania defilerów). 
* czł: Kleofas Bór, * człowiek, który wykonywał zadania Margaret w świecie potworów. Wykazał hart ducha mimo koszmarów z jakimi miał do czynienia (Zeta).
* czł: Olga Miodownik, która zlokalizowała Arazille mocą Iliusitiusa. Nie zostawi reszty oddziału na śmierć.