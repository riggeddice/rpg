---
layout: inwazja-konspekt
title:  "Mafia Gali w szpitalu"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151013 - Kontrolowany odwrót z zamtuza (PT)](151013-kontrolowany-odwrot-zamtuza.html)

### Chronologiczna

* [151013 - Kontrolowany odwrót z zamtuza (PT)](151013-kontrolowany-odwrot-zamtuza.html)

## Punkt zerowy:


## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa i zaczyna likwidować operację.
Policjanci idą do lasu; szukają śladów tajemniczej krwawej sekty.
Wiła znajduje się w lesie; niestabilna / dziwna. 
Artykuł Marii w Kopalinie uderza dewastując reputację i działania Gali.
W szpitalu jest NullField żywiący się magią; jest też jakiś mag, który jest płci żeńskiej (najpewniej)
Franciszek Marlin próbuje uzyskać jak najwięcej z zamtuza.
Marii kończą się pieniądze.
Paulina jest w słabszej formie (-1 do testów body, will).

## Misja właściwa:
### Faza 1: Pułapka na maga, pułapka na Paulinę

Paulina wyczyściła tyle śladów ile była w stanie. Poszła spokojnie do pracy, poszukać czegokolwiek pod kątem magii; czegokolwiek co zdradza jakiekolwiek ślady magiczne. Czy jest coś magicznego w okolicach Hermana? Czy w szpitalu poza NullFieldem jest coś jeszcze? Takie tam. Paulina unika czarowania; próbuje wykryć pasywnie ślady. Niestety, artefakty i inne takie - rozważała - ale NullField je skutecznie wyłączy.

Czyli podczas pracy Paulina chodzi, rozmawia z innymi, szuka, grzebie po papierach... skupia się na Ryszardzie Hermanie i ewentualnym magu. Z pomocą Marii (soullink) i jej zbiorem plotek i opowieści Paulina próbuje dowiedzieć się czegoś o Hermanie i magu.
I faktycznie, dowiedziała się dość sporo ciekawych informacji. O Adamie Diakonie. Przystojny lekarz, nieco narcystyczny, wybitny. Herman go przyjął, ale wywalił na zbity pysk (2 miesiące temu). Powodem było to, że przestał stawiać się w pracy; to wszystko co mają. Adam Diakon zniknął. Została jednak jego przyjaciółka i do dzisiaj pracuje. 24-letnia pielęgniareczka, niezbyt kompetentna, ale Herman - z jakiegoś powodu - pozwolił jej zostać i pracować. Mimo, że zwykle tępi idiotów. Podobno sypia z Hermanem. Nazywa się Marzena Dorszaj. A jak się Herman do niej odnosi? Bardzo pobłażliwie, choć jednocześnie dość surowo. Praktycznie ją unika, ale obserwuje. Jest zainteresowany jej działaniami, ale nie chce mieć z nią dużo do czynienia. I wysyła ją na urlop, ale ona wiecznie wraca.

Aha, Marzena Dorszaj zdradza Ryszarda Hermana z Jerzym Karmelikiem (lekarzem, który ma w szafie NullField). A sama Marzena jest młoda, ale taka... przeciętna. Nic ciekawego. Ale młoda. Paulina przełączyła sobie dyżur na następny dzień, wieczór; chce pracować wtedy, gdy Marzena. Paulina chce ją poznać. 

Maria przegrzebała dokumenty o Marzenie Dorszaj; okazało się, że Marzena ma przeszłość (nie jest sztucznym człowiekiem). W miejscowości, w której mieszkała - niejakim Góraczu - dwa lata temu dochodziło do zniknięć i krwawych morderstw; zidentyfikowano to jako robotę satanistów. Zniknęła i Marzena. A potem wypłynęła tu, z Adamem Diakonem. Utrzymywała kontakty z rodziną, ale nie opuściła Przodka. Najpewniej nie jest zatem sztucznym człowiekiem.

Paulina pomyślała, że może warto podsunąć ten trop Kurczakowi? W końcu szuka kultów i krwawych sekt. Miałoby urok - ale nie.

HIDDEN MOVEMENT

- Marzena próbuje wysondować Paulinę; zaraża magicznie młodego chłopaka w klubie. \| Marzena.(manipulate someone to do her bidding)
- Korzunio idzie na plebanię; rozwiązuje tamten problem i prosi o łaskę dla młodych. \| Korzunio.(manipulate public opinion)
- Policjanci idą do lasu; Artur Kurczak widzi wiłę \| 
- Marlin żąda rekompensaty od Oranka \| 
- Oranek wchodzi w próbę sił; Marlin się cofa \| 

END OF HIDDEN MOVEMENT

Paulina pytała też o to, czy Marzena często pracowała z Adamem Diakonem. Dowiedziała sie, ku wielkiemu niezadowoleniu wielu swoich rozmówców, że to wyglądało jakby Adam Diakon uczył ją zawodu. Była często bezużyteczna; teraz jest dużo lepiej, ale wiele osób się dziwi, że Ryszard Herman jeszcze ją trzyma.
Papiery Marzeny Dorszaj trzyma Ryszard Herman w sejfie. Paulina poprosiła Marię, by ta poszukała na portalach społecznościowych, w przeszłości Marzeny czegoś powiązanego ze szkołą pielęgniarską lub chociaż kursem. Maria znalazła - korespondencyjny... ale znalazła też coś jeszcze - nasza Marzena Dorszaj była swego czasu satanistką. Kindersatanistką najpewniej.

Ryszard Herman ma żonę i córkę. Nie ma o nim pikantnych ciekawostek. O Ryszardzie Hermanie wiele się więcej nie udało dowiedzieć.
Maria doceniła, że Paulina chce ją naprowadzić na artykuł, ale "kindersatanistka pielęgniarką u surowego lekarza, z którym sypia" nadaje się raczej do Faktoidu... i tak, pisywała do Faktoidu gdy musiała. Głód przycisnął. Nie jest z tego dumna.

Akurat tego wieczoru Paulina ma dyżur. Więc nie zdziwił jej trudny przypadek. Zawsze jest jakiś. Kochający, przerażeni rodzice i młody chłopak - 15 lat. Po prostu omdlał; jednak Paulina czuje wyraźnie lekki sygnał energii magicznej. Jest napromieniowany. Ktoś go zaczarował... a NullField zaraz pożre emanację.
Chłopak jest przytomny, lecz jest wyraźnie chory. Paulina nasłała pielęgniarkę na rodziców, po czym rzuciła czar na chłopaka - co się tu stało. Co mu jest. 

Paulina poczuła potężną emanację z NullFielda. Coś tam się dzieje. Coś silnego. Silny drenaż. W tej chwili nie zżera z chłopaka; NullField jest zajęty czymś zupełnie innym. Niestety, Paulina się nie może na tym skupić.

Na chłopaku jest niezdarne zaklęcie. Jest trójetapowe: pierwszy etap to osłona i inhibitor w jednym - blokuje drugi i trzeci poziom.
Drugi poziom to deathspell. Trzeci poziom jest niezdarnie ukryty w drugim poziomie i blokuje deathspell. Gdy deathspell zostanie uruchomiony, nic się nie stanie. Całość wygląda tak, jakby wymagała natychmiastowej interwencji Pauliny lub jakiegoś lekarza magicznego.
W praktyce jest całkowicie niegroźne - krzywdzi chłopaka, ale nullfield je usunie sam, z czasem.

Ktoś próbuje Paulinę złapać. Nie wie, że jest katalistką. Paulina postanawia, że skoro nullfield sam to rozwiąże w ciągu paru dni, Paulina mu na to pozwoli, przetrzymując chłopaka "na badania". Nikt nie będzie robił pułapki na Paulinę. Nie taką.

HIDDEN MOVEMENT

- Młody chłopak trafia do szpitala, pod Paulinę. Herman prosi ją o analizę problemu \| Herman.(notice, that [something] is wrong)
- Marzena idzie się wyczyścić magicznie. \| Marzena.(have compulsionary effects in form of [effect])

END OF HIDDEN MOVEMENT

### Faza 2: Proste, czcigodne porwanie.

Maria skontaktowała się z Pauliną; Artur Kurczak widział wiłę w lesie. Opisał ją jako "nagą, zakrwawioną satanistkę na prochach". Gonił ją, ale nie dogonił. Na szczęście nic mu nie zrobiła... był z nim kolega (Bławatek) i jemu też się nic nie stało. Kurczak wrócił i popytał o różne dopalacze i narkotyki w okolicy; Maria powiedziała, że jest taki dopalacz chwilowo. Najpewniej Kurczak przyjdzie do szpitala szukać jakichś śladów czy informacji o dopalaczu. Maria powiedziała, że byłoby nieźle, gdyby w szpitalu faktycznie coś znalazł by nie szukał paranormalnie...

Paulina znalazła sposób. Trzeba po prostu zanieść i skazić próbkę krwi nieszczęsnego chłopaka - że niby ktoś go czymś otruł. Sformowała taki dopalacz, żeby wywoływał zbliżone objawy jak Kurczak szuka i zanieczyściła próbkę młodziaka. Traf chciał, że Ryszard Herman się napatoczył... wypytał Paulinę co z tym młodym. Ona powiedziała, że najpewniej ktoś go przytruł dopalaczem - podała potencjalne objawy. Herman zmarszczył brwi. Jest podejrzliwy; ma zamiar to sprawdzić w archiwach czy z czymś takim się spotkał (naturalnie nie powiedział Paulinie). 

"To nietypowe, że dyrektor się pojawia osobiście w takich miejscach jak laboratorium..." - Paulina, sondując Ryszarda Hermana.
"Sprawdzam panią" - Spokojna i brutalna, acz szczera odpowiedź.

Paulina się jeszcze zorientowała, że skąd się ten Ryszard Herman w sumie tu wziął; subtelnie próbowała go wypytać z pomocą Marii. Przechodził niedaleko laboratorium... ale Paulina zorientowała się w czymś, co ją lekko zmroziło; Ryszard Herman poczuł Skażenie. I zrozumiał co to jest; poszedł w kierunku jego źródła i natrafił na Paulinę. W sumie - oczywiste; był tu Adam Diakon. A nie da się odebrać pamięci człowiekowi w Przodku...
Czyli najpewniej Herman coś się już o Paulinie domyśla, ale nie wie, że Paulina wie. A Paulina wie już, że Herman się domyśla. Prawie złamana Maskarada.
Świetnie. Po tym zostawił ją w spokoju i poszedł.

O dziwo, policjanci się nie stawili w szpitalu mimo słów Marii. 

HIDDEN MOVEMENT

- Maria dowiaduje się o tym, że Kurczak widział wiłę \| 
- Tymek idzie do lasu podłożyć truciznę wile. \| 
- Gala wycofuje wszystkie działania i przekazuje wszystko Orankowi \| 
- Policjanci spotykają się z Korzuniem w sprawie zamtuza i wiły \| 
- Marzena zatrudnia thuga do porwania Pauliny \| Marzena.(attack someone from behind)

END OF HIDDEN MOVEMENT

Ryszard Herman powiedział Paulinie, że chce jej przełożyć dyżur. Paulina zdziwiona, ale on miałby dyżur jutro a nie może, więc przeniósł Paulinę a sam weźmie jej dyżur. Paulina udaje, że nie wie - ale to oczywiste, że Herman chce przebadać młodego chłopaka na obecność tego dziwnego dopalacza. Trudno. Dostanie go ;p. Oczywiście w stopniu mniej dokładnym; najwyżej wnioskiem Hermana będzie, że świństwo się szybko rozkłada. Paulina rzuciła odpowiedni czar na chłopaka. Paulina zrobiła swoje, efekt jest. Mogła pójść do domu.

HIDDEN MOVEMENT

- Kilka minut później, jej czar pożarł Null Field.

END OF HIDDEN MOVEMENT

Paulina wraca spokojnie do domu. Pogrążona w myślach i rozmawiając z Marią nie spodziewała się, że zajdzie ją od tyłu gość z chloroformem. Za mała dawka chloroformu, ale Paulina udaje, że zadziałał. Maria dostaje sygnał. Gość porywający Paulinę wciąga ją do auta i rzuca na tylne siedzenie. Po czym jedzie. Paulina jest słabsza, ale trackinguje Marii całość lokacji. Gość wjechał do jednego z garaży.
Paulina dalej udaje, że jest nieprzytomna. Maria jej mówi słodko, że jak Paulina będzie chciała, Maria ściągnie policję. Paulina powiedziała, że na razie nie trzeba.

Gość wiąże Paulinę, nieco niefachowo, ale skutecznie, po czym zakłada jej na oczy oślepiacz z sex-shopu. Po czym on czeka i ona czeka.

HIDDEN MOVEMENT

- Korzunio zaskoczony wiłą pyta Galę; ona mu mówi, że spieprzyła i naprawia \| 
- Wiła atakuje Tymka; mag jest ranny, ale zwiał; organizm nie wytrzymał i zemdlał \| 
- Wiła zażyła narkotyk \| 
- Tymek trafia do szpitala, znaleziony przez policję \| 
- Marzena próbuje zdobyć informacje od Pauliny \| Marzena.(seize and hold)

END OF HIDDEN MOVEMENT

Jakieś 2-3 godziny później otwierają się drzwi do garażu i ktoś wchodzi. Kobieta (Maria zidentyfikowała ją jako Marzenę; jest w pobliżu i śledziujowuje). Zapłaciła facetowi za porwanie i kazała mu sobie pójść. Facet poszedł i zostawił Marzenę i Paulinę same.

"Tien... Tarczyńska, jak mniemam!" - ostre (i całkowicie nieudane) otwarcie Marzeny.

Marzena powiedziała Paulinie, że szukała czarodziejki pomagającej ludziom - ale nie znalazła żadnej. Paulina skontrowała, że nie trzeba było tamtemu człowiekowi pomagać. W rozmowie wyszło jednoznacznie, że Marzena nie jest zbyt... obeznana w świecie magów. Boi się. Mówi, że Adam Diakon ją skrzywdził i coś jej zrobił. Co - nie wiadomo. Marzena nie chce Paulinie powiedzieć, ale chce wybadać, czy Paulina może jej pomóc.
Marzena też powiedziała, że czar Pauliny zeżarł NullField; ale ona skompensowała - poprosiła Hermana, że ona zrobi badania. Herman się zgodził.

Marzena w końcu się złamała. Powiedziała Paulinie, że jak ta będzie czarować, to ją ubiczuje by przerwać koncentrację - a Diakon sporo ją nauczył. Zdjęła przepaskę Pauliny. I Paulina zobaczyła... Marzenę. Wyglądająco Marzenowato.

Marzena powiedziała Paulinie, że ma zamar wstrzyknąć jej środek przy użyciu którego ta powie jej prawdę. Paulina zauważyła, że tkanka magiczna to uniemożliwia. Po dłuższej rozmowie (i kilku konfliktach) Marzena w końcu się przyznała - Adam Diakon ją zmienił. Zrobił z niej zabawkę i źródło energii. Pod wpływem energii magicznej Marzena traci wolę. Nie może złożyć zaklęcia i słucha poleceń każdego. Dlatego on zostawił ją w Przodku i zostawił NullField, który pozwala jej oczyszczać się z energii. Ale NullField działa TYLKO w Przodku. Więc ona nie może wyjechać. I musi grzecznie czekać na jego powrót.

Straszne życie.

Uwolniła Paulinę. Ona pójdzie do NullFielda, Paulina pójdzie spać.
...i Marzena natrafiła na Tymka w szpitalu...

### Faza 3: Wycofanie z Przodka.

HIDDEN MOVEMENT

- Gala zaniepokojona szukała Tymka; dowiedziała się, że jest w szpitalu. By nie wywoływać zdziwienia, sfabrykowała tożsamość kuzyna.
- Gala załatwiła kilku takich do zniszczenia wszelkich materiałów biologicznych Tymka.
- Gala zobaczyła NullField. Chce go zabrać.

END OF HIDDEN MOVEMENT

Paulina przyszła rano do pracy, gdzie dowiedziała się, że przywieziono kolejną poranioną ofiarę w nocy. Zdziwiona, nie zdążyła zapytać o nic więcej - zobaczyła Galę Zajcew i się schowała przed nią. Gala powiedziała pielęgniarce, że chce zobaczyć swojego kuzyna, ale jest NN. Ona ma jego dokumenty (yay, Zajcewowa fabrykacja na szybko). Rozpoznała Tymka na sali a od pielęgniarki dowiedziała się, że jedzie policja. Poszła poawanturować się do Hermana i dowiedziała się, że ów jest nieugięty. Zanim zrobiła coś głupiego poczuła emanację NullFielda i to ją bardzo zaciekawiło. Stwierdziła, że zaczeka na policję.

Gala wykonała telefon do Korzunia i powiedziała mu o sytuacji. Kazała opóźnić policję. Korzunio znalazł dwa losowe samochody i je wysadził...
Policja ma teraz ważniejsze rzeczy na głowie...

Paulina jest w lekkim szoku. Gala co prawda jeszcze nikomu nie zrobiła krzywdy, ale nie wygląda jak ktoś kto się zawaha. Wysadzenie samochodów tylko dlatego, by zająć policję brzmi... strasznie bezwzględnie i nieefektywnie. Jednocześnie pasuje to do zachowania Gali - nie rani, nie krzywdzi, ale "personal property" nie ma znaczenia. To też oznacza "endgame": Gala ma na pewno zamiar stąd wyjść. Już na dobre.

Paulina szybko skonsultowała się z Marią. Poprosiła o pomoc i przekazała Marii wszystko co wie o Zajcewach. Maria wydedukowała, że Gala idzie w kierunku siłowego odbicia Tymka ze szpitala. Paulina się ucieszyła - zamierza Gali w tym pomóc. W ten sposób Gala najpewniej tu już nie wróci.

Dziesięć minut później na obrzeżach Przodka odpaliła flara magiczna. Tak mocna, że Paulina ją lekko poczuła. Zdaniem Pauliny - próba odciągnięcia magów z okolicy. Paulina zdecydowała, że to jest moment, gdzie ona nie da się odciągnąć i będzie udawać że nic nie wykryła.

Piętnaście minut później weszło 10 silnych, młodych ludzi w kominiarkach. Karnie kierują się do Gali Zajcew, która informuje wszystkich, że teraz zrobią to czego ona oczekuje. Wyszedł Ryszard Herman, zdenerwowany, o co tu chodzi. Gala pokazała na niego i powiedziała mu gdzie mieszka, gdzie jest jego żona i córka. Zbladł. Pokazała jeszcze 3 inne osoby i zrobiła to samo. Powiedziała, że jeśli zrobią co ona chce, na tym się skończy i nigdy o niej już nie usłyszą. Jak nie...

Gala z uśmiechem wyjaśniła, że to porachunki mafijne; narkotyk wydostał się na wolność i niektóre osoby padły jego ofiarą. Mafia się tym zajmuje. Nikt do nich tu nic nie ma - ale Gala chce Tymka (wskazała lekarza, który ma nadzorować jego przeniesienie), chce odzyskać coś swojego (szuka NullFielda) i żąda, by zniszczono wszystkie dane i próbki pacjenta NN (w innym wypadku można wykryć po DNA). 

Gala powiedziała, że jeśli COKOLWIEK wypłynie z tej akcji w szpitalu, oni zajmą się tymi osobami, które wskazała. A potem dojdzie do źródła. Skuteczny terror.

Paulina zdecydowała się zaryzykować pomoc Marzenie. Rzuciła czar mający udawać efekt NullFielda. Spaliło na panewce; Gala wykryła, że to INNA emanacja, ale to znaczy, że w budynku jest jeszcze jakiś mag. Niekoniecznie chętny do pomocy. Zdecydowała więc wycofać siebie i Tymoteusza. Wszystko co chciała osiągnąć poza NFieldem osiągnęła.

W ten sposób zakończyła się akcja sojuszu Zajcew-Maus w Przodku...

A zatruta narkotykami przez Tymka wiła skonała gdzieś daleko.


## Spekulacja:

Kić: 
- Gala tu już się raczej nie pojawi. Wycofają się.
- Korzunio wie o wile, choć nie rozumie jej implikacji; to już nie jest tylko sekret Gali.

## Dark Future:

### Faza 1: Pułapka na maga, pułapka na Paulinę

- Marzena próbuje wysondować Paulinę; zaraża magicznie młodego chłopaka w klubie. \| Marzena.(manipulate someone to do her bidding)
- Korzunio idzie na plebanię; rozwiązuje tamten problem i prosi o łaskę dla młodych. \| Korzunio.(manipulate public opinion)
- Policjanci idą do lasu; Artur Kurczak widzi wiłę \| 
- Marlin żąda rekompensaty od Oranka \| 
- Oranek wchodzi w próbę sił; Marlin się cofa \| 
- Młody chłopak trafia do szpitala, pod Paulinę. Herman prosi ją o analizę problemu \| Herman.(notice, that [something] is wrong)
- Marzena idzie się wyczyścić magicznie. \| Marzena.(have compulsionary effects in form of [effect])

### Faza 2: Proste, czcigodne porwanie.

- Maria dowiaduje się o tym, że Kurczak widział wiłę \| 
- Tymek idzie do lasu podłożyć truciznę wile. \| 
- Gala wycofuje wszystkie działania i przekazuje wszystko Orankowi \| 
- Policjanci spotykają się z Korzuniem w sprawie zamtuza i wiły \| 
- Marzena zatrudnia thuga do porwania Pauliny \| Marzena.(attack someone from behind)
- Korzunio zaskoczony wiłą pyta Galę; ona mu mówi, że spieprzyła i naprawia \| 
- Wiła atakuje Tymka; mag jest ranny, ale zwiał; organizm nie wytrzymał i zemdlał \| 
- Wiła zażyła narkotyk \| 
- Tymek trafia do szpitala, znaleziony przez policję \| 
- Marzena próbuje zdobyć informacje od Pauliny \| Marzena.(seize and hold)

### Faza 3: Wycofanie się Zajcewów

- Wiła przysysa się do narkotyków i zatruwa się \| 
- Marlin żąda rekompensaty od Oranka \| 
- Oranek wchodzi w próbę sił; Marlin się cofa \| 
- Zamtuz zostaje zamknięty.
- Węzeł zostaje rozmontowany.

### Faza 4: Koniec historii.

- Wiła umiera.
- Tymek i Gala opuszczają Przodek.
- Oranek dostaje trochę funduszy i zostaje w Gęsilocie na rozeznanie.

# Zasługi

* mag: Paulina Tarczyńska, która odkryła czarodziejkę w Szpitalu Gotyckim i dała radę wejść z nią w pewną współpracę. Planuje ją wyleczyć. Też, uratowała przed Galą NullField.
* czł: Maria Newa, kopalnia informacji i profiler osoby Gali dla Pauliny. Nie pojawiła się poza soullinkiem.
* mag: Marzena Dorszaj, która okazała się * magiem przekształconym przez "Adama Diakona" w bezradną zabawkę. Pielęgniareczka, kiedyś kindersatanistka.
* czł: Ryszard Herman, wciąż podejrzliwy, któremu mafia Zajcewów zatkała usta prostym "Twoja córka i żona znajdują się teraz w miejscu X i Y". Wie o "czymś paranormalnym".
* czł: Jerzy Karmelik, najbardziej plotkujący chyba lekarz-lowelas na oddziale. Przypadkowy właściciel Null Fielda.
* mag: Tymoteusz Maus, który w pogoni za wiłą dał się jej pociąć i wylądował w szpitalu, przez co przysporzył kłopotów Gali.
* mag: Gala Zajcew, która pokazała swoje bardziej okrutne oblicze; nadal nikomu nie zrobiła krzywdy, co w świetle okoliczności się jej chwali.
* czł: Krystian Korzunio, czyściciel Zajcewów, który na rozkaz Gali wysadził dwa całkowicie niewinne samochody.

# Aktorzy

- Tymek; katalista (Engineer)
- Gala; kupiec (Merchant)
- Korzunio; czyściciel (Cleaner)
- Maria (Intrepid Reporter/Paladin)
- Ryszard Herman (Truthseeker)

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                    1. Osiedle Metalowe
                        1. Garaże osiedlowe
                1. Gęsilot
                    1. Las Skrzacki