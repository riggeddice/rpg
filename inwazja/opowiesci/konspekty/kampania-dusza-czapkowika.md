---
layout: default
categories: inwazja, campaign
title: "Dusza Czapkowika"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Adaptacja kralotyczna](kampania-adaptacja-kralotyczna.html)

## Opis

null

# Historia

1. [Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html): 10/11/08 - 10/11/10
(180418-czapkowicka-apatia-kulturalna)

Stara Opera w Czapkowiku umiera - Operiatrix gaśnie. Zakochany w muzyce człowiek-jaszczur, Alfred Werner, zasila ją energią ludzi kochających muzykę. I na to wpada Zespół - trójka ludzi próbujących pomóc apatycznej Kasi (wysysanej przez Operiatrix) i tropiących dziwnego jaszczura. Do tego dołączył się Alojzy Przylaz polujący na viciniusa. W wyniku - Operiatrix została zasilona, Przylaz oszczędził człowieka-jaszczura a Czapkowik ma kolejne opowieści o jaszczurach...

1. [Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html): 10/11/17 - 10/11/19
(180419-jaszczury-rzadza-miastem)

Inna grupa magów przejeżdżała przez Czapkowik. Okazało się, że pojawia się ryzyko gry zespołu postdeathmetalowego co skazi Operiatrix; magowie zerwali więc koncert (by się nie odbył), lecz w efekcie powstał Metalhead Rap Battle. Dodatkowo, burmistrz Czapkowika, Kamila Woreczek też okazała się być jaszczurem. Magowie podeszli do sprawy racjonalnie - albo wybijemy wszystkich, albo zrobimy z tego atrakcję. I tak powstał plan (wspierany funduszami unijnymi) zrobić z Czapkowika polskie Roswell, ale z gumowymi kiczowatymi jaszczurami (na co burmistrz przystała)...

1. [Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html): 10/11/24 - 10/11/26
(180421-jaszczur-love-story)

W Czapkowiku pojawił się mimik symbiotyczny. Szczęśliwie, Genowefa i Kamila powstrzymały go, zanim stała się większa krzywda. Rosną tam napięcia między frakcją wysokiej kultury a frakcją finansowo-pragmatyczną. Udało się odkryć, że Alfred i Kamila są jaszczurczymi kochankami. Główna frakcja reptiljan zdecydowała się zrobić uciekłe jaszczury ambasadorami...

1. [Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html): 10/11/28 - 10/11/30
(180425-sledziem-w-depresje)

Młoda czarodziejka chciała zaimponować Warmasterowi, w którym się zadurzyła. Dlatego zdobyła tuzin śledzi syberyjskich, które miały śpiewać smutne piosenki... Niestety, coś poszło nie tak (paradoks) i śledzie rozpełzły się po okolicy. Podczas polowania na pierwszego Melinda wpadła na Genowefę, która nie potrafiła odmówić pomocy wyraźnie przestraszonej dziewczynie...

## Progresja

|Misja|Progresja|
|------|------|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html) zresocjalizowany przez Kasię Bratkowską. Wydarzenia paranormalne zmieniły jego życie na lepsze.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html) zdobył pierwszego nie-wroga w świecie viciniusów: Alfreda Wernera, człowieka-jaszczura.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) zdobył pierwszego nie-wroga w świecie magów: Alojzego Przylaza, ucznia terminusa.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html) zakłada chór w Czapkowiku. Wrócił i jest zdeterminowany zmienić Czapkowik w miejsce muzyczne.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html) pragnie nauczyć się gry na saksofonie. Dużo ćwiczy w ruinach starej opery czapkowickiej|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html) pozyskała nóż chroniący przed opętaniami i walczący z opętaniami. Sęk w tym, że służy też do krojenia żółtego sera.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) pobiera lekcji gry na saksofonie od Pawła Szlezga|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html) ma nową osobę chcącą jej pomóc: Alfred Werner, człowiek-jaszczur.|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html) dostał opinię człowieka, który MA DOJŚCIA do tematów jaszczuropodobnych. Niestety, traktowany jako ekspert.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html) nażywiona jaszczurzym rapem wykonanym przez ludzi. Na pewien czas powstrzymany process rozpadu.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|[Alojzy Bunnert](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-bunnert.html) ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) The Jaszczur of the Opera, z saksofonem. Nauczył się dzięki Paradoksowi z rapem i naukom Pawła Szlezga.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Miasto Czapkowik Mimo epickiej jaszczurowatości, jednak ludzie oglądają Netflixa; Pryzmat się przesuwa z niebezpiecznego na zwykły|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Miasto Czapkowik Marketing i fundusze unijne sprawiły, że Czapkowik zaczął być znany jako miasto od jaszczurów|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Estrellas Infernal Mają pogorszone stosunki z ludnością Czapkowika.|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Miasto Czapkowik Częścią obchodów Dni Jaszczura stała się tradycja Metalhead Rap Battle|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) został maestro w operze Czapkowickiej. Więcej, ludzie przychodzą go słuchać. Jaszczur Of The Opera, dosłownie XD.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) ujawniło się, że jest parą z Kamilą Woreczek. Dodatkowo, został ambasadorem jaszczurów (reptiljan) na Czapkowik.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html) ujawniło się, że jest parą z Alfredem Wernerem. Dodatkowo, została ambasadorem jaszczurów (reptiljan) na Czapkowik.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) tak bardzo jej ufa Alfred i Kamila, że dostała informacje jak wejść do jaszczurzego świata pod ziemią. Ma dostęp do części dziwnych jaszczurzych zasobów.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|Miasto Czapkowik pojawiają się silne napięcia między frakcją pragmatyczną (pieniądze, paintball, kicz) a kulturalną (opera, poezja, sztuka)|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html) kontroluje mimika symbiotycznego, którego może wykorzystać w dowolny sposób.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html) Daria i Genowefa naprawiły jego ekran maskujący. Działa prawidłowo; nie będzie już sam się wyłączał. Ma ekran dość niezawodny.|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) śledzie trzeba karmić. Depresją. Smutkiem.|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) śledzie potrafią się teleportować. Wszelka próba sprzedaży, kradzieży itp kończy się ich powrotem.|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) wskutek dziwnych interakcji emocjonalnych co jakiś czas musi zajrzeć w okolice Zaćmionego Serca, aby się upewnić, że wszystko nadal w porządku|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) ma dostęp do pożerających depresję śledzi (hodowla domowa)|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|Zaćmione Serce Warmaster rozdaje klucze lekką ręką. Każdy może tam wejść.|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html) Melinda Zajcew staje się jej kontaktem. Dziewczyna jej ufa i ją lubi.|

## Plany

|Misja|Plan|
|-----|-----|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html) nadal planuje przeprowadzić Awaken w Operze Czapkowickiej.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|Miasto Czapkowik echo zaklęcia Darii i Genowefy dotknęło źródła energii i wyprodukowało hipnokarabiny - efemerydę, która pragnie rosnąć.|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|Miasto Czapkowik Reptiljanie część z Czapkowikich Reptiljan chce władzy nad światem (jak to reptiljanie)|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|Miasto Czapkowik Reptiljanie część z Czapkowikich Reptiljan chce izolacji i usunięcia zbiegłych jaszczurów (Alfreda i Kamili)|

