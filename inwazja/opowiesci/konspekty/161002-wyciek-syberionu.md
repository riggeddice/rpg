---
layout: inwazja-konspekt
title:  "Wyciek syberionu"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161018 - Ballada o duszy ognistej (HS)](161018-ballada-o-duszy-ognistej.html)

### Chronologiczna

* [160918 - Walka o duszę Draceny (PT)](160918-walka-o-dusze-draceny.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Paulina przeniosła się z Draceną jeszcze raz. Zniknąć... innym, zanim zaczną zadawać za dużo pytań. Owszem, powiedziały Netherii itp że nic się stało (hahaha), że sprawa rozwiązana... Kinga przebadała Dracenę. Potem jeszcze skończono transformację; zajęło to moment. Ale już bez większych problemów.
Dracenie kupiło to czas poczuć się trochę lepiej w swoim ciele. I ten pięć dni Dracena ćwiczyła swoją magię, by nie być całkowicie niestabilna.
Udało się, naturalnie. Dracena zaadaptowała bardzo szybko.

Paulina przeniosła się na miejsce tymczasowe. Skontaktowała się z Marią; chciałaby by ta i Dracena się spotkały. Maria odmówiła, to jeszcze nie pora - Dracena może się wygadać. Paulina się zgodziła... z wahaniem. 
Paulina powiedziała Dracenie, że trzeba sprawdzić jej moce i możliwości. Pomóżmy ludziom, zwłaszcza, że jest dług u Korzunia. Dracena w to wchodzi bez wahania.

Korzunio powiedział telefonicznie, że jest prostym człowiekiem. Jest cleanerem. Podczas jednego z transportów wylał im się syberion. Korzunio i Zajcewowie to ogarnęli; problem rozwiązany. Ale - jak Korzunio rozumuje - to nie znaczy, że udało się wszystko rozwiązać. I on nie ma jak napuścić magów na tamto miejsce. Korzunio przyznał wprost - gdyby tam było coś groźnego, on by to rozwalił. Ale nic nie znalazł... lecz nie miał czasu szukać. I dlatego prosi Paulinę. Bo on podejrzewa, że radioaktywne / magiczne skażenia prowadzą do chorób.

"Ja ci pomogłem w małej sprawie, ty mi pomóż w małej. Tak jest sensownie." - Korzunio do Pauliny.

Korzunio podał Paulinie podstawową informację co to jest syberion i dał kontakt na Antona Jesiotra; właściciela smażalni Jesiotr.
I Paulina wsiadła w PKS z Draceną i pojechały ;-). Maria ma za zadanie znaleźć się tam we własnym zakresie i niekoniecznie natychmiast.

Stawnia. Małe miasteczko. Lasy i jeziora. Paulina poszła do Jesiotra; przedstawiła się, że jest "od Krystiana". Anton się ucieszył - całe szczęście. Podobno morowa panna jest na bagnach; ludzie zaczynają mieć wysypki. Lokalny lekarz powiedział, że to... masowy atak alergii na pleśnie. A Korzunio nie znalazł żadnych lokalnych magów. Dołączyli do Zajcewów albo zostali "poproszeni" o wyprowadzenie się. Stawnia i okolice nie mają magów.
...i właśnie tu rozlał się syberion...

Paulina i Dracena zamieszkały przy smażalni. Porozmawiały chwilę o smażonych rybach i ich zapachu, po czym zdecydowały, że trzeba zabrać się do pracy. Paulina wysłała lekki ping magiczny, by zobaczyć, czy nikogo tu nigdzie nie ma. Nikt jej nie odpowiedział na jej przyjazną flarę...
A jest wieczór.

Pukanie z szacunkiem. Anton Jesiotr wprowadził syna, Michała, i powiedział jej o tym, że Michał jest jednym z tą dziwną wysypką. Dracena wyprowadziła ojca i się tak uśmiechnęła, że młody aż się spłonił. By kupić czas, Michał zaczął opowiadać Paulinie, że jest kucharzem. Specjalizuje się w rybach i w lokalnych ziołach; tym, co może tu zdobyć i zrobić. Paulina przygotowała gabinet i obejrzała tą jego wysypkę. I nie tylko. 

Z wywiadu wynika, że nasz kucharz chodził po lesie, szukał ziółek, ma taką fajną bazę w lesie gdzie można się schować jak co, pływał w jeziorze... ogólnie nie opuszczał tych terenów. Zgodnie z wiedzą Pauliny, NIE był na obszarze Skażonym syberionem. Paulina odniosła wrażenie, że coś ukrywa, ale nie ma to chyba znaczenia w tej sprawie...
Wysypka jest przednia. Przypomina rybie łuski i idą podbrzusze - noga. Paulina wzięła próbkę. Michał powiedział, że 2 dni temu to było mniejsze. 

Paulina zrobiła badanie magiczne; to jest... coś dziwnego. To tkanka magiczna próbująca przekształcić chłopaka w... syrenę. Nie w trytona. W syrenę. Ale tkanka magiczna nie ma na czym się zagnieździć; to obumrze. Ale nie powinno nigdy tak się rozwinąć. Nie tak. Nie ma ENERGII na to, by to się stało.
Paulina pobrała chłopakowi krew. Chce mieć dostęp do jego wzoru by zaprojektować antidotum.

Chłopak wysłany do domu. Paulina poszła do apteki i zamówiła maści. I szok - apteka nosi nazwę "U Weinera". 

Paulina skontaktowała się z Bolesławem Derwiszem, znanym jej już terminusem z okolic Wykopu. Derwisz powiedział, że w tej okolicy nie ma magów Świecy; nie wie nic o tym, żeby tu byli magowie jako tacy. Derwisz powiedział Paulinie, że może zdobyć jej info historyczne, sprzed Zaćmienia; ale do rana (1 water). Paulina się zgodziła, podziękowała mu gorąco i się rozłączyła.
Wróciła. Dracena już tam była, z lekkim uśmiechem.

Paulina przygotowuje maść dla chłopaka. Problem w tym, że... ona może to od ręki wyleczyć. Serio. Tyle, że to będzie podejrzane. A nakładane stopniowo - do godziny zero - wymaga wielu zaklęć i wielokrotnego powtarzania tej samej operacji.
W skrócie, niefart.
Najpierw Paulina zrobiła jedną porcję. A Dracena zauważyła, że na bagnach coś jest. Pływy energii magicznej. Zauważyła też ogniska energii magicznej; niestabilne, więc się same rozproszą, ale są. Dracena zaznaczyła wszystkie na mapie. Ciekawe jest to, że skupiska energii są na różnych budynkach i w różnych miejscach. Poza jeziorem. Jezioro jest w "środku", acz samo jezioro nie ma w sobie energii magicznej.

Paulina wzięła chłopaczka i go posmarowała maścią. Kazała mu się nie ruszać, rano ona na niego spojrzy i spróbuje coś na to poradzić.
Paulina powiedziała też Dracenie, że ta jest tajną bronią, bo nikt nie spodziewa się tutaj Draceny.

"To jest dziura! Zróbmy coś!" - Dracena
"Próbujemy się dowiedzieć co się dzieje" - Paulina

Dracena jest zirytowana, chce coś zrobić. Paulina... nie chce spalić jej przykrywki. Dracena chce agresywnie, Paulina ostrożnie.
Paulina wpadła na pomysł jak rozładować sytuację - do północy sobie pospacerują i rozejrzą się po mieście, potem Paulina pójdzie spać a Dracena zajmie się szukaniem po necie czegoś związanego z lokalnymi wierzeniami. Czegoś... o morowej pannie czy syrenie.
Dracena się na to zgodziła.

Paulina i Dracena przeszły się po mieście. Chciała znaleźć czy coś się dzieje lub nie. Przejść się koło przychodni; poszukać magów czy aury rezydualnej.
W przychodni jest jedno z ognisk - nawet dwa. Ale Paulina pasywnie nie wyczuła śladów zaklęcia. Czyli lokalni Weinerowie albo nie są magami, albo rzadko używają, albo są paranoiczni.

Maria wysłała Paulinie sygnał. Jest tu 11 Weinerów. Większość z nich założyła rodziny PO Zaćmieniu. Większość z nich mieszkała tu jeszcze przed Zaćmieniem; wszystko wygląda na to, że... osiedli. Rano przyjedzie do Stawni; za dużo czasu zajęło jej wyplątanie się i znalezienie zlecenia.
Weinerowie mają raczej wyższe pozycje, ale nie są w centralnej radzie miasteczka czy coś takiego.

Paulina poprosiła Dracenę, by ta się schowała w cieniu i ewentualnie robiła za asekurację. Sama rzuciła zaklęcia diagnostyczne przez quarka, z zachowaniem środków bezpieczeństwa. Zwykłe detekcyjne zaklęcia kontrolne i diagnostyczne. Źródła magiczne są... dziwne. Spiritus-Aquam. Mają echo Pryzmatu. Są samorozpraszalne; same zanikną z biegiem czasu, choć powodują Skażenie. Wygląda na to, że to fala posyberionowa; samoistne skupienie energii magicznej.
Jedno z tych źródeł Skaża by maksymalnie leczyć i wzmacniać skuteczność leczenia.
Drugie z tych źródeł Skaża negatywnie wpływając na pamięć; powodując fałszywe wspomnienia, zamazując i zarażając.

Czyli każde z tych źródeł ma inną charakterystykę...
Wszystkie źródła rozproszą się za ~10 dni samoistnie.

Paulina przeszła się po Stawni z Draceną szukając:
1) źródeł, które są pozytywne
2) źródła, które zaraża syrenizmem

Plan roboczy jest taki - usunąć szkodliwe źródła, pomagać sobie pozytywnymi.
...wszystkie są pokryte pryzmatem...

Dzień 2:

Rano Paulina zauważyła Dracenę śpiącą na komputerze. Dosłownie. Siedziała za długo ;-). Nie zna swoich możliwości.
Czeka na nią wiadomość od tien Derwisza. Żeby Paulina zadzwoniła.

Dane historyczne ze Świecy są następujące: był tu krąg Weinerów, nawet dwa współpracujące kręgi (~10 osób). Zarejestrowali się i Świeca miała na nich oko. Niestety, TEN region został szczególnie uderzony przez Zaćmienie. Nie wiemy czemu, ale Lubelskie było tak uderzone, że 95% magów straciło moc.
Zgodnie z danymi Derwisza, ostatnie raporty Świecy mówią o próbach działania niskopoziomowej terminuski, tien Ksenii Zajcew, która próbowała opanować teren i poradzić sobie ze Skażeniem, niestabilnymi artefaktami itp w chwili, w której Świeca i większość innych magów straciło moc. Jako, że hipernet nie działał poprawnie, Ksenia objęła dowodzenie i zginęła właśnie w Stawni, ratując ludzi przed eksperymentalnymi elementami działań Weinerów z tego kręgu tutaj (który wyrwał się spod kontroli bo nie było magii).

Zgodnie z danymi Bolesława Derwisza całe województwo jest uznane za Żółte w skali zagrożenia; nie nadaje się dla magów na swobodne poruszanie bez terminusów. Nie dlatego, że jest szczególnie groźne - dlatego, że zwyczajnie za mało wiadomo. Magowie z okolicznych województw czasami wpadają harvestować quarki i wypadają.

Są zapisy (od Ksenii), że wszyscy Weinerowie stracili moc. I wymazano im pamięć, wykorzystano ich własne badania. Badania tych Weinerów to były prace nad chorobami i bioaktywnymi elementami mającymi na celu zniszczyć pamięć o magii wśród ludzi. Coś jednak poszło nie tak i Ksenia zginęła.
Świeca się tym nie interesowała bo nie była w stanie; ściągnęła jedynie magów w okolice Trocina których dało się uratować.
Jest jeden Weiner z tego kręgu który przetrwał; jest właśnie w Trocinie.
W tej chwili - zdaniem Świecy - na tym terenie nie ma żadnego maga. Chyba, że coś z Ukrainy przyszło.

Paulina powiedziała Derwiszowi, że na tym terenie są słabe i rozpraszające się źródła energii; ona spróbuje coś z tym zrobić. Jak się nie uda... Derwisz spróbuje ją wyciągnąć. Wiele obiecać nie może, nie jest w stanie. Ale spróbuje.

Dracena się obudziła. Paulina powiedziała jej, by ta się wykąpała. Dracena odczytała to jako polecenie (klątwa), więc poszła z radością na kąpiel; po czym zarzuciła to Paulinie. Ups. Jakoś doszły do ładu; Dracena jest raczej zrezygnowana niż wściekła z tego powodu.

Powiedziała, czego się dowiedziała - w Stawni jest wielki fan legend Stawni. Tomasz Weiner, bibliotekarz. W legendach występują cztery istoty. 

Najciekawszym - dla Draceny - jest Ognista Dama. Płonąca dusza w kształcie kobiety, która spaliła poprzednią przychodnię i zniknęła w ogniu. To jest powiązane z tym, że przychodnia spłonęła zabierając ze sobą lekarzy, personel... wszystkich. Nikt nie przeżył. Miało to miejsce podczas Zaćmienia. Strażacy którzy przyjechali by ratować przychodnię powiedzieli, że widzieli cień osoby. Mściwa, Ognista Dusza. Nie mogła być osobą, bo by spłonęła.

Drugim ciekawym przypadkiem jest Morowa Panna. Jest tu w okolicy "od zawsze", rozprzestrzenia choroby i plagi odbierając ludziom nadzieję i chęć do życia. Dracena to zauważyła - nie ŻYCIE. Nadzieję i chęć do życia. Trzeci przypadek to Syrena. Syrena, która mieszka w tym jeziorze i chroni Stawnię przed Morową Panną. Przy okazji jej śpiew jest taki piękny...
Ostatni byt to Czarny Kaptur. Wędrowiec. Spojrzysz mu w oczy i wszystko zapomnisz.

"Rozumiem, że jego cechą charakterystyczną jest czarny kaptur" - Paulina
"Skąd wiedziałaś?" - Dracena, ironicznie

Dracena powiedziała, że to są cztery podejrzane magicznie byty w okolicy.
Morowa Panna - bagna. Syrena - jezioro. Ognista Dusza - i to jest ciekawe. Pojawia się w ogniu. Ognista Dusza jest uważana za bóstwo opiekuńcze, co trochę nie ma sensu, skoro zabiła tyle ludzi... tego Dracena do końca nie rozumie. Czarny Kaptur... dawno się nie pojawił. Ostatnie czasy były przed Zaćmieniem.
Zdaniem Draceny, dziwne, że ludzie tyle pamiętają. Paulina uzupełniła jej wiedzę.

Maria powiedziała, że zrobi mały research w okolicy. Paulina podrzuciła jej namiar startowy na Tomasza Weinera.

Paulina przygotowała kolejną porcję maści dla Michała. Faktycznie, to działa. Łuska kurczy się w satysfakcjonującym tempie.
Czyli źródło energii zaniknęło, albo do niego po prostu nie polazł.
Dostał kolejną porcję maści. Pomaga mu.

I Paulina wyjechała - "wczoraj nie powiedziałeś mi całej prawdy". Powiedziała, że chce mu pomóc i nie chce, by coś mu się stało.
Michał... się przyznał. Halina Weiner, była czarodziejka - ona się transformuje w Ognistą Duszę. I on jej pomógł się schować. Na miejsce Haliny pojawił się klon; to dla Michała jest dowód, że Ognista Dusza jest tu. Że Bóg ich wspiera.
Dla Pauliny to jest oh-shit moment.

"Lubisz ją? A jeśli się zmieni?" - Paulina
"Bóg ją wybrał..." - ze smutkiem, Michał

Paulina poszczuła Dracenę na Michała. Niech Dracena dowie się od niego, gdzie znajduje się Halina.
Godzinę później, Paulina dostała wiadomość od Marii. Maria powiedziała Paulinie coś niepokojącego - przejrzała bibliotekę; im FAKTYCZNIE wymazano pamięć, ale ona wraca. Oni jeszcze nie wiedzą o magii, ale pojawiają się... szczegóły, wątki, sny, imiona. Po kilku latach mogą odzyskać pamięć.

Czyli coś tu przywraca pamięć magom...

Paulina przygotowała dużą dawkę maści pomagającej ludziom; odmagiczniającej i regenerującej. Poszła z tym do Antona Jesiotra. Powiedziała mu, że jego syn ma się sporo lepiej; jak się będzie stosował do zaleceń to szybko mu to przejdzie. Powiedziała, że ma uprawnienia, ale może też pomóc innym. W ramach możliwości chciałaby spotkać się z najbardziej chorymi.
Anton powiedział, że załatwi jej to, by ludzie sami przyszli tak jak powinni, bez kolejek i nie mówiąc lokalnemu lekarzowi... ;>.

I Paulina zabiera się za naprawę ludzi...

Efektów jest więcej - i Paulina aktywnie koreluje legendy z Marią, która jest w bibliotece. Wszystkie legendy - WSZYSTKIE - poza Syreną się objawiły. Nie, nie wszystkie. Nic powiązanego z jeziorem się nie pojawiło. Były efekty efemerydowe i efekty na ludziach; Korzunio większość rozwalił.

Popołudnie. Dracena przyszła odwiedzić Paulinę. Rozciągnięta jak kotka. Powiedziała, że Halina ukryta jest w lesie, w "Dziupli Kochanków". To źródło Dracena pożre z przyjemnością. Powiedziała, że Michał je jej z ręki; nabawił się łusek, bo kochał się z Haliną w tamtej dziupli. Ale czemu syrenie? Dracena powiedziała, że Michał coś mówił o złotej rybce. Złota rybka? Wskazuje na jezioro... ale przecież nie widać żadnego źródła w jeziorze?

Teraz poziom magii tu jest mały; nawet gdyby Paulina się nie pojawiła, nikt by nie zginął (Halina jest podejrzana).

Dracena i Paulina dyskutują; bagna - przychodnia - Halina? Dracena zauważyła, że można zrobić to magią by wejść do środka.
Poszły więc na bagna...

Jeszcze jest jasno, acz już nie bardzo jasno. Paulina wie, gdzie jest sygnał, ale bagna są bardzo rozległe i to OCZYWIŚCIE nie może być sygnał gdzieś blisko...
Jakimś cudem, Paulinie udało się poprowadzić ją i Dracenę do celu mimo zapadającego zmroku.
I jest tam - Morowa Panna. Albo coś tak wyglądającego. Biała, eteryczna dama. A Paulina jest jej dokładnym przeciwieństwem...

Morowa Panna wydała wrzask banshee. Przez Paulinę przeszła głęboka depresja. Życie nie ma sensu, wszystko zmarnowane... ale trzeba... trzeba pomóc ludziom tutaj. Zostać tutaj. Całkowite oddanie miasteczku...
I wtedy potężny promień magiczny kataliza + erotyka + sexy funtimez od Draceny prosto w Morową Pannę. Wiązka otrzeźwiła (i rozpaliła) Paulinę. Morowa Panna się rozsypała. Dracena zwróciła się ku Paulinie, napalona jak dziki królik a Paulina z trudem oparła się czarowi. Powiedziała "nie, nie teraz" i zatrzymała Dracenę.

"Naprawdę cię nienawidzę" - Dracena, drżąc, ale musi wykonać polecenie

Paulina i Dracena skupiły się na harvestowaniu quarków i neutralizacji źródła z bagna. Udało im się - nie tylko zneutralizowały Morową Pannę ale i zyskały kilka Quarków, co szczególnie cieszy Paulinę (może leczyć ludzi). +1 void.

"Mam sugestię - chodźmy opanować Halinę" - Dracena, z nadzieją, że będzie mogła się wyżyć.
Czas wyjść z bagien i iść do lasu. Dracena poprosiła Paulinę o jej latarkę i podpięła obie do siebie zaklęciem; latarki stały się jej "Spustoszonymi wypustkami". Dracena przeszła też w swój tryb cybergothki i zaczęła świecić. Włączyła też tryb 'cyberwspomagany'. Jej własny, techorganiczny (napalony) egzoszkielet...

Przeszły przez bagna i wkroczyły do lasu. Bez kłopotu dotarły w okolice Dziupli Kochanków...
Paulina zaproponowała Dracenie rzucenie wiązki magicznej taką jak w Morową Pannę, w dziuplę. Niech sama wyjdzie. Co Dracena na to?

"Najlepszy pomysł w historii" - Dracena, bardzo pobudzona.

Dracena sformowała wiązkę. Rozproszyła ją jako szeroki stożek. Wzmocniła przez swoje techorganiczne elementy. Wystrzeliła stożek, wzmocniony przez Pryzmat...
Półtorej godziny później, Dracena podeszła, uśmiechnięta, do Pauliny mając na rękach nieprzytomną Halinę, na której twarzy jest absolutna błogość.

Dracena ze specjalizacją analizy wzoru i Paulina jako lekarka skupiły się na badaniach Haliny Weiner...

Badania trwały dość długo, ale dostały wyniki (a Dracena musiała trochę ogrzewać Paulinę i Halinę używając swojego wspomaganego ciała).
Halina jest człowiekiem. Można ją przekształcić w viciniusa lub w człowieka. Jest na drodze transformacji w viciniusa; gdyby źródła się same porozpraszały, to by zginęła; za daleko na drodze do viciniusa, za mało energii by utrzymać tkankę magiczną.

Czyszcząca tabletka może jej jeszcze pomóc, odwrócić ją w człowieka.

Paulina ma plan - wpierw rozwiążą to źródło, zostawią tu Halinę. Potem, Dracena będzie Halinę odwiedzać conocnie. Będzie się rozładowywać (i wyczerpywać Halinę), przy okazji rozmagiczniać ją tabletką. Pierwszy rzut tabletki dostała od razu.

Źródło w lesie zostało rozmagicznione...

# Progresja

* Paulina Tarczyńska: #1 void

# Streszczenie

Czas spłacić dług u Korzunia. Paulina i Dracena przyjechały do Stawni, gdzie po wycieku syberionu - zdaniem Korzunia - mogło coś zostać. I faktycznie, nie wszystko wyczyszczono; echa syberionu zgrupowały się w małe źródła magii. Paulina bez problemu odwraca typowe przypadki. Usłyszała od Derwisza ze Świecy, że tu większość osób straciło magię; teren jest prawie bez magów. Lokalni Weinerowie (11) - po odebraniu pamięci - zaczynają sobie przypominać. Pryzmat syberyjski sprawia, że legendy wracają do życia. Dwie legendy Paulina i Dracena już zniszczyły - Ognistą Damę oraz Morową Pannę. Sytuacja jest "pod kontrolą", ale trzeba znaleźć źródło problemów...

# Zasługi

* mag: Paulina Tarczyńska, mająca Dracenę "na smyczy" i ratująca ludzi po telefonie Korzunia o wycieku syberionu; jest na obszarze bez magów.
* mag: Dracena Diakon, niechętnie posłuszna, acz to akceptuje. Włada straszliwą magią katalityczno-erotyczną i wykorzystuje swoje ciało jako podstawa cyborgizacji.
* czł: Maria Newa, mająca pewien problem z Draceną, acz pracująca z Pauliną. Koreluje dane z biblioteki, dorobiła się zlecenia na historię w Stawni.
* czł: Krystian Korzunio, który dał Paulinie spłacić dług by wyczyściła ewentualny aftermath po jego wyczyszczeniu wycieku syberionu.
* czł: Anton Jesiotr, były przemytnik. Właściciel lokalnej smażalni ryb Jesiotr. Kontakt Krystiana Korzunia. Bardzo uczynny i pomocny Paulinie - zwłaszcza po tym jak jego synowi się poprawiło.
* czł: Michał Jesiotr, lokalny kucharz w smażalni, syn Antona, wierzy święcie, że Halina Weiner jest nową inkarnacją lokalnej świętej "Ognistej Damy". Wszystko wyśpiewał Dracenie w łóżku.
* czł: Kurt Weiner, lokalny lekarz. Stracił moc po Zaćmieniu. Na razie więcej o nim nie wiemy; jeden z 11 magów pracujących przed Zaćmieniem w Stawni.
* czł: Tomasz Weiner, wielki miłośnik legend i bibliotekarz. Kiedyś mag, stracił moc po Zaćmieniu. Współpracuje z Marią opowiadając jej legendy i pomagając cross-checkować legendy z objawami.
* czł: Halina Weiner, kiedyś czarodziejka, młódka; przekształca się w Duszę Ognia (chce tego). Paulina i Dracena wsadziły ją na odwyk od magii; będzie człowiekiem.
* mag: Bolesław Derwisz, sprawdził historyczne dane z rekordów Świecy dla Pauliny o Weinerach tutaj. Zauważył, że Świeca została tu zniszczona. Opowiedział legendę o Kseni Zajcew.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie, gdzie Świeca była tak słaba, że padła i teren jest utracony; najsilniej uderzony region podczas Zaćmienia (95% magów straciło moc).
            1. Powiat Hrabiański
                1. Stawnia, małe miasteczko w którym Korzunio ostatnio musiał posprzątać wyciek syberionu.
                    1. Centrum
                        1. Przychodnia, gdzie przyjmuje Kurt Weiner i gdzie są dwa źródła energii magicznych
                        1. Apteka U Weinera
                    1. Oziersko
                        1. Smażalnia ryb Jesiotr, najlepsza w Stawni (bo jedyna).
                            1. Dom Jesiotra, jako element "smażalnego imperium" Jesiotra.
                            1. Przybudówka, gdzie zamieszkała Dracena z Pauliną. Tymczasowo.
                        1. Jezioro Oziero, gdzie podobno była złota rybka
                    1. Leśnicz
                        1. Bagna leśnickie, gdzie znajduje się Morowa Panna
                        1. Las ozierski, gdzie w Dziupli Kochanków ukryła się kandydatka na viciniuskę... pokonana przez Dracenę.
     
# Skrypt

Pain points:
- Dracena jest niestabilna; ma tendencje do utraty kontroli nad sobą zwłaszcza w obecności Pryzmatu
- pod jeziorem znajduje się 'memcache' Weinerów
- morowa panna, syrena uwodzicielka, czarny kaptur
- Weinerowie pracowali nad systemami by ludzie nie pamiętali o magii - biomantycznie
- Ognista Dama, która zniszczyła przychodnię i zabiła wszystkich

# Czas

* Opóźnienie: 7 dni
* Dni: 2