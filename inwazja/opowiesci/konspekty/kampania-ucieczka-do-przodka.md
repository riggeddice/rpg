---
layout: default
categories: inwazja, campaign
title: "Ucieczka do Przodka"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Powrót Karradraela](kampania-powrot-karradraela.html)

## Opis

-

# Historia

1. [Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html): 10/05/31 - 10/06/01
(150928-zamtuz-z-jedna-wila)



1. [Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html): 10/06/02 - 10/06/03
(151003-zamtuz-przestaje-dzialac)



1. [Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html): 10/06/04 - 10/06/05
(151013-kontrolowany-odwrot-zamtuza)



1. [Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html): 10/06/06 - 10/06/07
(151101-mafia-gali-w-szpitalu)



1. [Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html): 10/06/08 - 10/06/09
(151220-z-nullfieldem-w-garazu)



1. [..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html): 10/06/10 - 10/06/11
(151223-mozna-uwolnic-czarodziejke)



1. [..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html): 10/06/12 - 10/06/13
(151229-choc-to-na-sekte-nie-pomoze)



1. [Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html): 10/06/08 - 10/06/09
(151230-zajcew-ze-smietnika-partnerem)



1. [...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html): 10/06/10 - 10/06/11
(160106-i-kult-zostaje-rozgromiony)



1. [Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html): 10/06/12 - 10/06/13
(160117-muchy-w-sieci-korzunia)



1. [Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html): 10/06/24 - 10/06/27
(170423-rozpad-magii-w-leere)

Paulina wyleczyła ofiarę rozpadającego się diakońskiego narkotyku; niestety, w wyniku tego musiała czarować przy lekarzach. Maria znalazła miejsce, gdzie magia powodowała kłopoty; wpadła w tarapaty, lecz się z nich wyciągnęła. Gdy Paulina do niej dotarła, unieszkodliwiły górników i znalazły źródło problemu - studnia głębinowa. Paulina przekazała problem studni Korzuniowi a narkotyków Wiaczesławowi. O dziwo, wszystko się udało.

