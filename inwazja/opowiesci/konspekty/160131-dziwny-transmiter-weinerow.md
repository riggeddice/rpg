---
layout: inwazja-konspekt
title:  "Dziwny transmiter Weinerów"
campaign: rezydentka-krukowa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160124 - Trzy opętane duszyczki... (PT)](160124-trzy-opetane-duszyczki.html)

### Chronologiczna

* [160124 - Trzy opętane duszyczki... (PT)](160124-trzy-opetane-duszyczki.html)

## Punkt zerowy:

Bólokłąb, upiorna efemeryda, która kaskaduje dalej swoim stanem i cierpieniem; manifestuje się w nie zbudowanym domu.
Maciek, Mateusz i Milena często spotykali się w domu; natrafili na Bólokłąba.

## Misja właściwa:

### Faza 1: Fajnie by było mieć sojusznika.

Dzień 1:

Paulina zadzwoniła do swojego mistrza. Ma efemerydę i ledwo odratowała trójkę dzieci... potrzebny jej terminus do wsparcia. Przybylec powiedział, że poprosi kolegę o pomoc - Kajetana Weinera. Paulina pożaliła się, że boi się tam sama wejść; nie wie, czy tam jest lapis czy nie. Ale nie umie nic wykryć; jest to samo w sobie dziwne. Przybylec postara się wpłynąć na kolegę, by ten się pospieszył. Paulina dostała jego numer...

Bariera stoi. Ludzie nie wejdą do "Szarego Domu" bez poinformowania Pauliny (plus, nie będą chcieli).

Dzień 2:

Ojciec Mileny, Onufry, przyszedł do Pauliny po coś na sen. Ta się zdziwiła; strażak nie do końca powinien takie rzeczy brać. Wyciągnęła z niego to czego nie chciał powiedzieć; Onufry powiedział, że jak tylko myśli o higienistce to zaraz ma ochotę albo uciekać (bo straszna, bo dentysta, bo igły), albo ma ochotę po prostu ją uderzyć (bo jest okrutna, bo nie zatrzymała Mileny). Onufry sam twierdzi, że to stres lub przejął się córką. Paulina nie jest taka pewna - co prawda efemeryda nie POWINNA go dotknąć (Paulina by to wyczuła), ale Paulina nie jest pewna... jedno jest pewne - EFEMERYDA nie jest w stanie tego zrobić. Ale WĘZEŁ emocjonalny tak - one działają przez sympatię (Milena -> ojciec). To jednak podnosi stopień problemu.

Paulina dała Onufremu środek nasenny (nie zaszkodzi), ale dodatkowo dała mu przedmiot - mały krzyżyk (jest wierzący) - zaklęła ten krzyżyk. Coś, co jemu ułatwi uspokojenie się. Coś, co może niwelować mordercze zapędy i ewentualne działania magii emocjonalnej. Przyquarkowała; stworzyła artefakt tymczasowy (2 tygodnie) by mógł pomóc strażakowi się uspokoić przy użyciu leków uspokajających jako sympatycznego wsparcia. Udało jej się (jedyny efekt uboczny: gość będzie jeszcze bardziej wierzący jako 'krzyż moim talizmanem').

Paulina rozwiązała problem. Tyle, że nie wiadomo, czy to nie pójdzie dalej...

Paulina zadzwoniła do Kajetana. Powiedziała o sytuacji i poprosiła o wsparcie. Mag powiedział, że pojawi się tak szybko jak tylko jest w stanie (jutro). Zapytany przez Paulinę, czy może coś zrobić odpowiedział, że tak; może zrobić szeroką mapę pływów energii magicznej w okolicy. Poszukać miejsc anomalnych energetycznie. Tam może działać efemeryda / energia emocjonalna. Kajetan daje jej dokładne wytyczne (ale przez komórkę ciężko). 

Niestety, stopień trudności czaru: 22.
Paulina poszła do kiosku po mapę okolicy. Przygotowuje też zestaw czujników (miniony), które mają jej rozprzestrzenić energię po okolicy i pełnić rolę relayów.
Dodatkowo Paulina zadzwoniła do Onufrego; poprosiła go o przyjście, by mogła pobrać mu krew. Może się zatruł. Paulina użyje krwi jako sympatii (wskaźnik, jakiego typu energia magiczna miała tu miejsce). Połączenie tych wszystkich rzeczy daje jej efektowny #11 bonus. 21 vs 22. 
19 | 22 | 25 a Paulina ma 20. Podnosi.

Niestety, energia tego zaklęcia jest za duża. Paulina nie jest w stanie skoncentrować się zarówno na barierze dookoła Szarego Domu jak i na sieci skanującej. Tak więc - z ciężkim sercem - rozerwała barierę (zwłaszcza, że zauważyła dzięki sieci skanującej, że bariera wysyła trochę energię do środka Szarego Domu; czyli efemeryda mogłaby się zasilać w jakimś stopniu...).
To, co Paulina jeszcze zauważyła na siatce skanującej (wykorzystując mapę okolicy) to drugą manifestację. Sam Szary Dom jest czysty - ma podniesiony poziom energetyczny, ale za mało podniesiony. Ale jednocześnie w jednym z bloków jest podniesiony poziom energii wskazujący na niekontrolowaną manifestację. Najpewniej przez sympatię... na szczęście, nie jest to poziom efemerydy. Poza tym jest sporo "mikroognisk", ale są za słabe by nawet dokonać Skażenia.

Paulina pojechała do Krukowa, do owego bloku. Na miejscu więcej zobaczy. Na pierwszy rzut oka - blok jak blok. Pasywne wyczucie energii nie wykazało nic szczególnego, czyli manifestacja - jeśli tam jest - to jest ograniczona. Zgodnie z mapą jednak manifestacja ma tam miejsce i jest tam ciągle. Paulina widzi przepływ energii z Szarego Domu do tego bloku. Ale jest za niski przepływ na coś więcej niż tylko "utrzymanie" tego co tam już jest. I nie jest to jakaś ciężka efemeryda.

Przynajmniej tyle.

Paulina wbiła się do bloku na "ulotki". Do Pauliny dotarło, że właśnie tu mieszka higienistka szkolna. Mieszkanie 6. Poszła pod nie. Rzuciła zaklęcie badawcze; chce dowiedzieć się co to za aura i o naturze manifestacji. I jak krytyczne to jest. Okazuje się, że aura jest powiązana z tym, co było ojcu Mileny. Ten sam typ energii. Czyli obsesje, rojenia, szepty.
Paulina ma zgryz. Trochę się boi... ale jednak zapuka. Wejdzie do środka i porozmawia z higienistką...

Zmęczony głos pani Basi poprosił Paulinę do środka. 
Barbara jest blada, ale wygląda na sensowną i przytomną. Zapytana, czy nic jej nie jest - nie. Podchorowała się, zaraziła się, ale wzięła leki i jest w porządku. Widząc, że jest słaba, Paulina zaproponowała jej wzięcie sobie zwolnienie. Barbara się uśmiechnęła, dobry pomysł. Bystre oko Pauliny wypatrzyło strzykawki a patrząc na stan higienistki, wzięła kilka dawek środków uspokajających, co zagraża jej życiu. Higienistka mówi, że zastrzyki zrobiła dzieciom. Że jest w końcu lekarzem, że dzieci były w szpitalu - ogólnie, bredzi. Gdy zaczęła tracić przytomność, Paulina zadzwoniła po pogotowie i rzuciła się na ratunek. 

Paulina błyskawicznie zdecydowała się na rzucenie zaklęcia. Nie chodzi o to, by ją oczyścić czy wyleczyć, ale ma być do odratowania biologicznie; Paulina jako składnika użyje leków wspomagających. Konflikt dwustronny. Paulina: (18v12->S) i Transmiter: (08v11->S). Paulina się obroniła: spodziewała się silnej reakcji zwrotnej. Wyczuła przy okazji, że ta "energia" docierała do niej dlatego, bo jest lekarzem. Czyli tu TEŻ jest jakieś połączenie.

Przyjechało pogotowie. Zabrali ją i pojechali do Krukotargowa, do tamtego świetnego szpitala. Jest poza zasięgiem efektywnym efemerydy; kilkanaście kilometrów. Paulina dała im swoją diagnozę i wszystko co miała co może jej pomóc. Paulina zamknęła to mieszkanie i ruszyła do Szarego Domu. Ktoś musi go pilnować. Patrząc na mapę Paulina widzi, jak manifestacja (Skażenie) oddala się wraz z ambulansem.

### Faza 2: Gdzie dwóch walczy, tam efemeryda pada.

Dzień 3:

Zmęczona i niewyspana Paulina, czuwająca całą noc, upewniła się, że nikt nie wchodził do Szarego Domu. I taką właśnie rozczochraną i zmęczoną panią doktor spotkał Kajetan Weiner, elegancki mężczyzna w praktycznym stroju. Zrobił wizję lokalną, zdziwił się, że ten dom nie jest zbudowany. Paulina powiedziała, że efemeryda ma coś do lekarzy; Kajetan stwierdził, że Paulina nie musi aż tak się narażać. Paulina chce się przespać przed akcją...

Po zreferowaniu sytuacji przez Paulinę, Kajetan stwierdził, że nie widzi tu woli. Widzi węzeł emocjonalny. Ale nie pasuje fakt, że tego węzła po prostu nie ma.

Terminus poprosił Paulinę, by ta poszła się przespać. Sam zrobi wizję lokalną. Jak Paulina się obudzi, idą zająć się efemerydą.

Zdecydowali się wejść razem do Szarego Domu. Znaleźć efemerydę.
Efemeryda: SHIELD 40, VAL ekspert: 15.

Budynek wygląda normalnie. Nie do końca zbudowany, coś takiego co zwyczajnie opuszczono. Są ślady zabawy dzieci. Terminus uruchomił pancerz rycerza. Ani śladu efemerydy. Terminus idzie po śladach dzieci - podobno to Milena znalazła efemerydę, więc terminus próbuje ją backtrackować. Paulina i Kajetan zauważyli złoty dysk na ziemi; wygląda na nieco technomantyczny obiekt stworzony przez kogoś kto nie ma pojęcia o technologii, ni technomancji. Kajetan całkiem stropiony - to wygląda jak jeden z wariantów transmiterów energetycznych Weinerów, ale zmodyfikowany. Co on tu robi? Za jego pomocą uda się namierzyć źródło; normalnie wykorzystuje się to do przesłania energii magicznej z jednego miejsca do drugiego. Czemu tu?
Wszystko wskazuje, jak Paulina zauważyła, na formę awaryjnego rozładowania JAKIEGOŚ źródła energii.

Kajetan uruchomił artefakt - transmiter. Poczuli wrzask. Poczuli ból, cierpienie i nienawiść. I pragnienie wolności, obsesyjne pragnienie ruszenia się. Takie zwierzęce. Terminus zasłonił Paulinę i siebie tarczą i usłyszeli - "osiem, nie siedem, osiem, nie siedem, już osiem". Terminus ma 19, efemeryda 15: terminus -> 22. SHIELD -7. Efemeryda 'raise', 30-3 = 27, terminus eskaluje: +13 -> 32. Efemeryda na -5, ale terminus odrzuca tarczę (base 16). Tymczasem Paulina przygotowała zaklęcie działające uspokajające, w stylu takiego jak wcześniej uspokojała dzieci; użyła poprzedniego działającego zaklęcia. Skutecznie poraziła efemerydę; kolejne -5. A jako, że każda eskalacja efemerydy kosztuje -5, jej aktualny stan:

efemeryda: SHIELD 18, VAL 15.
terminus: VAL 16.

Starcie: Terminus: +5 -> 21. Efemeryda ma 15. Efemeryda eskaluje (31); teraz terminus musi eskalować (38). Efemeryda odrzuciła (5) za eskalację, ale terminus stracił zbroję i został lekko ranny. Paulina błyskawicznie zareagowała - używając zaklęć nekromantycznych i wiwomancji przesunęła terminusa w formę częściowo nieumarłą, by mógł lepiej bronić się przed częścią ataku efemerydy. Daje to terminusowi (+3) tarczę. 

efemeryda: SHIELD 07, VAL 15; kara (-7)
terminus: VAL 19.

Efemeryda eskaluje. (23 v 19 -> +2). Terminus eskaluje (17+3->20). 

Efemeryda zignorowała częściowo niewidzialnego dla niej terminusa i skupiła się na Paulinie. Terminus osłonił Paulinę (przypłacając to raną), po czym odepchnął ją, zmaterializował łuk i strzelił jej prosto w pierś, roztrzaskując tarczę efemerydy. Efemeryda jest lekko niestabilna.

Paulinie otworzyło się okno działania. Paulina powtórzyła swoje działania na efemerydzie: zrobiła jej Arazille ("będzie dobrze", "dostaniesz czego chcesz" itp). A do tego zaczęła recytować psalm. Dokładniej: psalm 88 (87) "W ciężkim doświadczeniu". Wzmacnia medykamentami uspokajającymi. 20v15 -> 22. Efemeryda jest na -7; wartość 8 (po eskalacji). 

Efemeryda zrobiła krok w tył słysząc psalm, ugodzona zaklęciem (Paulina powiązała zaklęcie z psalmem). Paulina usłyszała wiele ludzkich głosów w efemerydzie. "Niech mnie ksiądz pobłogosławi". "Uwaga! Skręcaj!". Efemeryda zaczyna się powoli rozmontowywać.

Terminus, mający chwilkę, skupił się na zaklęciu skierowanym w efemerydę. Czar jest zaklęciem absolutnej radości i ekstazy. Czar ten jest zaprojektowany jako zaklęcie usuwające przeciwnika; ten ma wrażenie, że dostał to, czego pragnie. (3 efemerydy vs 17 terminusa). Efemeryda po eskalacji NADAL przegrywa. Efemeryda zaczyna się rozpraszać; energia zaczyna wypływać z transmitera i wygasać. Po stronie źródła energii kończy się zapas energii. Spada ciśnienie.

Efemeryda is no more.

Terminus jest ranny, ale nie jest w bardzo złym stanie; jakoś dokuśtyka. Wsparty przez Paulinę uśmiechnął się, że bywało gorzej; to nie warunki do walki z tak silnym potworem. Artefakt został zneutralizowany i przechwycony przez Kajetana i Paulinę. Kajetan i Paulina go monitorują i rozpoczną pracę nad znalezieniem źródła. Jak odeśpią.

A Paulina poskładała Kajetana do kupy. Jakoś.

# Zasługi

* mag: Paulina Tarczyńska, po wezwaniu wsparcia i uratowaniu życia higienistce destabilizacja efemerydy była już formalnością.
* mag: Kazimierz Przybylec, mistrz Pauliny który załatwił jej pomoc maga Kajetana Weinera do akcji z efemerydą. 
* mag: Kajetan Weiner, lekko postrzelony terminus o zapędach rycerza który z przyjemnością współpracuje z Pauliną oraz faktycznie umie walczyć.
* czł: Onufry Letniczek, strażak Dotknięty przez efemerydę, który przyszedł do Pauliny z prośbą o coś uspokajającego / na sen.
* czł: Barbara Zacieszek, higienistka w szkole w Krukowie, którą także Dotknęła efemeryda; Paulina uratowała jej życie przed samobójstwem.
* vic: Bólokłąb, efemeryda. Rozproszona ostatecznie przez połączone siły Pauliny i Kajetana.

# Lokalizacje:

1. Krukowo Czarne
    1. Gabinet Pauliny
    1. nie zbudowany dom Przemuskich, "szary dom"