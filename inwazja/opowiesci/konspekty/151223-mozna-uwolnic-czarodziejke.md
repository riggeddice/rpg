---
layout: inwazja-konspekt
title:  "..można uwolnić czarodziejkę..."
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151220 - Z Null Fieldem w garażu... (PT)](151220-z-nullfieldem-w-garazu.html)

### Chronologiczna

* [151220 - Z Null Fieldem w garażu... (PT)](151220-z-nullfieldem-w-garazu.html)

## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa (Orank) i sektę (Kły Kaina); ślady przekierowuje na nich.
Wiła nie żyje, zatruła się narkotykami gdzieś w lesie.
Policjanci na SERIO szukają śladów tajemniczej krwawej sekty.
Joachim Zajcew szuka Korzunia w sprawie "zabitej przez niego wiły".
Pielęgniarka Marzena okazała się baterią - defilerką dla Adama Diakona.
Adam Diakon się "nawrócił" na parafii zgodnie ze słowami Karmelika.
Maria dorabia pracując dla "Rytmu Codziennego"; przekazała Paulinie donos o "zamtuzie w lesie".

## Stawki:

- Czy Marzena skończy jako mag czy człowiek?        mag, choć -> vicinius w przyszłości
- Jak rozwinie się relacja Karmelik - Marzena?      koniec romansu

- Czy ksiądz podzieli się swoją wiedzą na temat Adama Diakona?

## Misja właściwa:
### Faza 1: Pierwsze ugryzienie

Dwa dni później. 
Joachim jeszcze nie przyszedł do szpitala. Wyraźnie szuka Korzunia. A Korzunio najpewniej nie chce dać się znaleźć... Paulina całkiem nieźle się bawi.
A w szpitalu - wybuchła bomba. Metaforycznie. Doktor Grażyna Tuloz, zastępczyni Ryszarda Hermana przyłapała Jerzego Karmelika i Marzenę Dorszaj na czynach lubieżnych w godzinach pracy... huknęła aż sufit zadrżał i powiedziała, że nie tylko Herman ale i jego żona się o tym dowie. Karmelik próbował jakoś panią doktor przekonać, jakoś ją ubłagać... powiedziała, że są tematy jakich w godzinach pracy nie porusza.
Jerzy Karmelik taki załamany. Nie wie co dalej. 
Paulina tylko się uśmiechnęła. Dla odmiany - to nie jej nieszczęście.

Jedyne, co Paulinę w tym wszystkim zdziwiło - doktor Tuloz jest najgorszym detektywem na świecie. I przyłapała ich na gorącym uczynku. Najpewniej przypadek, albo donos od innego lekarza, tak czy inaczej - sucks to be them. Zwłaszcza, że doktor Tuloz nie chroni Marzeny jak Herman.

Właśnie, nie ma Hermana - i nie ma NullFielda. Paulina ma unikalną okazję użyć magii delikatnie by pomóc w szansach niektórych pacjentów, którym jest w stanie delikatnie pomóc. To niekoniecznie jej pacjenci, to po prostu... różne osoby. Tak, by nie było to oczywiste. Całkowicie nie jest tego świadoma, ale wpłynęła lekko na Marzenę (w pozytywny sposób). Tak więc jak tylko pojawiła się przerwa, Marzena skorzystała z okazji by zapytać Paulinę o Karmelika - czy jakoś mu pomóc... co Paulina o tym myśli. W końcu gdyby Marzena się nie pojawiła i sama mu się nie rozebrała, nigdy by do tego wszystkiego nie doszło. Ale... on sam podjął decyzję. Więc, czy powinno się mu pomóc? Czy ona lub Paulina powinny? Marzena nie wie, nie rozumie podejścia Pauliny; jak ktoś tak potężny jak ona może zachowywać się w taki sposób. A Karmelik mimo wszystkich swoich paskudnych wad jest mimo wszystko dobrym człowiekiem. 

Paulina wpadła na pewien pomysł. Może dałoby się przekonać panią doktor Tuloz? Jest to taka kostyczna, sucha lekarka... ale ogólnie, bardzo moralna (co dobre a co złe) i nie chce szkodzić. Grażyna Tuloz z pewnością przeżyła silny szok; tu moralna i sympatyczna (choć oschła i konserwatywna) lekarka, a tu taka Marzena w uprzęży i Karmelik z biczem. Na pewno dr Tuloz nie wiedziała, że oni oboje są "za". A niejeden już był skandal, że lekarz nadużywał pozycji wobec pielęgniarki. Przykład najprostszy: Adam Diakon.

Ale wpierw trzeba dać Grażynie Tuloz chwilę.

Paulina dobrzy wyczuła, że Grażyna będzie chciała to załatwić osobiście z Hermanem i żoną Karmelika; dała jej czas ochłonąć i poszła do niej z Marzeną. 

Paulina i Marzena zaczęły przekonywać panią doktor, że w sumie, to Marzena sama chciała, i Jerzy też. Nie było tu wymuszenia, tylko gra erotyczna. Owszem, głupio postąpili... a Paulina próbuje przekonać Grażynę, że szkoda rujnować chłopu i jego żonie życie, trudno o dobrego lekarza, a Marzena już nie będzie i on też nie... 
Grażyna, mimo wszystko, jest osobą raczej nie lubiącą skandali i problemów. Została przekonana przez Paulinę. Ale! Marzena i Jerzy mają zakończyć romans. Marzena się skwapliwie zgodziła. 

Jerzy, jak Grażyna mu to zakomunikowała, też się zgodził z radością. Koniec romansu z Marzeną Dorszaj. Nie warto o to tracić dobrego małżeństwa...

Paulina jest dla odmiany zadowolona. Jeden problem mniej. Marzena nie miała się jak z tego wyrwać - a tak, problem został rozwiązany.
Przy okazji... dlaczego pani doktor tu była? Paulina spróbowała dowiedzieć się i dopytać Grażynę Tuloz o to, skąd wiedziała. Udało jej się - Grażyna dostała cynk. Od... Marzeny. Paulina nie miała okazji jednak tego dnia z Marzeną o tym porozmawiać, bo zirytowana Grażyna Tuloz wysłała Marzenę na dyżur. A ta nie protestowała, wiedziała, że jej się upiekło i należy.

Widząc, jak Marzena Dorszaj rozwiązała problem z Karmelikiem Paulina stwierdziła, że Marzena jest dużo sprytniejsza, niż większość ludzi myśli. Przekazała tą informację też Marii. Tak na wszelki wypadek.

Wieczór Paulina poświęciła kalibracji "laboratorium".

HIDDEN MOVEMENT:

- Franciszek Marlin daje wejście "Kłom" do "Rakiety" (PO.suck up to <someone> stronger)
- Wiesław Rekin masakruje wszystkich zawodników w Rakiecie i agituje; bójka w "Rakiecie" z ochroniarzami. Rekin sam wychodzi ze śmiechem.
- Przemysław Kujec dzwoni na numer telefonu Rekina; dostaje instrukcje, że jak się nie boi, ma iść do lasu.
- Dwóch członków sekty obserwuje parafię; chcą poznać jak to miejsce wygląda.
- Joachim konfrontuje się z Korzuniem; ten zwala wszystko na spudłowane "Kły Kaina".

END OF HIDDEN MOVEMENT

Paulina chciałaby w końcu zająć się rozmontowaniem zaklęć leżących na Marzenie; a dokładniej, jej wypaczonego wzoru. Ale tam były też kwestie defilerskie. Paulina chce dokładnie przebadać (seria eksperymentów nad wzorem Marzeny) ocenić stopień zmian i sposób zmian tego, co zostało z nią zrobione. Skupia się też na energii defilerskiej, ale nie chce uruchamiać tego fragmentu ciała i wzoru Marzeny.

Złożoność tego, co zrobił Adam Diakon ze wzorem Marzeny jest przerażająca; nawet Paulinie nie udało się całości ogarnąć. Ale wyłapała kluczowe fragmenty:
- moc defilera jest silnie zasealowana; jakiekolwiek użycie mocy defilera czy magii krwi natychmiast zmienia ją w zabawkę i odbiera jej jakąkolwiek wolę.
- każde użycie magii jest zasealowane; przekierowane jest na odbieranie woli. Czyli - gdy czaruje, staje się niewolnicą.
- Paulina jest w stanie rozłączyć normalną magię nie dotykając zabezpieczeń na defilerze.
- wygląda na to, że Marzena jest na (udanym) odwyku od magii krwi; czyli Diakon tak jakby jej nie przeciążał; szanował swoją bateryjkę.
- Diakon zmieniał też naturę jej tkanki magicznej tak, by była "jak najbardziej ludzka". Żeby mogła operować w świecie ludzi.
- Diakon dodał dodatkowe zabezpieczenia w obszarze magii krwi i działania ciała Marzeny w tym obszarze; Paulina nie wie dokładnie jakie.
- tkanka magiczna Marzeny działa tak, że DOWOLNA osoba o odpowiednio silnej woli będzie w stanie zdominować Marzenę.

Ogólnie, choć Paulina niechętnie to mówi, robota kogoś kto naprawdę zna się na rzeczy i naprawdę dużo czasu i pracy poświęcił by zamodelować Marzenę Dorszaj w coś, co mu się podoba.
Paulina oczyściła sobie Marzenę z osadów magii używając Null Fielda, po czym powiedziała Marzenie na czym ta stoi i jak wygląda sytuacja.

Z punktu widzenia Pauliny:
- blokady normalnej magii da się zdjąć (bez gwarancji).
- blokady magii krwi nie da się zdjąć. A przynajmniej, Paulina tego nie chce robić. Nie będzie ryzykować.
- ciało, tkanka magiczna, wola drugiej osoby... tu będzie ciężko. Paulina nie wie, czy sobie poradzi.

Więc jedyne, co Paulina może naprawdę zagwarantować - uwolnienie magii Marzeny. Ale jak Diakon wróci, nie ma szansy zauważyć, że wzór Marzeny się bardzo nie zmienił... Marzena ma szansę na jedną zemstę, na jedną akcję. I jej decyzja, czy ją podejmie czy nie.

Marzena się zgadza na taki układ. 

ESCALATION MODE:

V - ( 1_1) Paulinie nic się nie stało długoterminowo.
V - ( 2_1) Paulinie nic się nie stało krótkoterminowo.
V - ( 1) Marzena nie traci mocy magicznej; nadal jest magiem.
V - ( 1) Marzena NIE odblokowuje mocy defilera.
- (-1) Marzena jest zakochana do zniewolenia w Karmeliku (dziwne działanie zabezpieczeń Diakona).
- ( 1_2) Powiązanie tego z Pauliną nastąpi dopiero na etapie "Upadek Blakenbauerów"
- ( 2_2) Żaden mag nie może skojarzyć niczego z Pauliną.
- (-1) Joachim Zajcew natrafi na energię w garażu; natrafi na Paulinę i Marzenę (po wszystkim).
V - ( 1) Marzena nie jest kontrolowalna przez dowolną osobę o silnej woli.
X - ( 1) Odblokowanie tkanki magicznej Marzeny.
V - (-1) Tkanka magiczna Marzeny przekształca się w coś "nienaturalnego"

Strony: 
- Paulina i Marzena
- zaklęcia Adama Diakona

ESCALATION_1_Paulina: 

Zaklęcia Adama Diakona są zwyczajnie zbyt potężne i przebijanie się przez to jest masakrycznie trudne (20). Z pomocą Marzeny, która otworzyła swój wzór i całkowicie uległa Paulinie (po Karmeliku ma wprawę ;p A ZWŁASZCZA PO DIAKONIE), Paulina zdecydowała się uderzyć w sam rdzeń zaklęć Adama Diakona, by umożliwić sobie dalszą pracę nad Marzeną. Paulina nie zamierza łamać czarów Diakona; jest zbyt potężny i zbyt zdolny. Ona chce je zaadaptować. Lekko przesunąć.

Marzena jest w tej chwili całkowicie posłuszna Paulinie. Paulina próbuje spiąć te zaklęcia z samą Marzeną w taki sposób, by Marzena była w stanie na nie wpływać. Paulina nie chce się do nich przypinać. Marzena dostaje całość ryzyka, a Paulina się chroni i uderza w systemy zabezpieczeń Diakona.

Bezpośrednie, skuteczne pchnięcie energii Pauliny ukierowanej przez laboratorium doprowadziło do nastepującego wyniku:
- moc zaklęć Diakona: 20 -> 17.
- ( 1_1) Paulinie nic się nie stało długoterminowo.

ESCALATION_1_Zaklęcia: 

Zaklęcia zostały osłabione i zdestabilizowane. Uruchomiły się wszystkie systemy zabezpieczeń Diakona. Zadziałały nietypowo na Marzenę; ale włączyły się elementy infekcyjne mające zabrać Paulinę ze sobą. Diakon używał magii krwi; defensywy Pauliny zostały całkowicie ominięte i Paulina została Dotknięta przez moc Adama Diakona. Dała radę się uchronić i zniwelować działania na Marzenie. Od tej pory doktor Paulina Tarczyńska jest bezpieczna przed mocą systemu ochronnego Adama Diakona.
- ( 2_1) Paulinie nic się nie stało krótkoterminowo.

ESCALATION_1_FALLOUT:

Paulina jest zmęczona. Ale ogólna moc zaklęć Adama Diakona i jego wpływ na wzór Marzeny Dorszaj zostały ograniczone, ona wie z czym ma do czynienia i dała radę całkowicie zabezpieczyć się przed możliwym falloutem. Paulina może zacząć pracę nad rozkładaniem czarodziejki.

ESCALATION_2_PM: 

Kolejnym krokiem Pauliny jest odblokowanie mocy magicznej Marzeny. Niech Marzena będzie mogła "żyć jak osoba". Jak normalna czarodziejka, choć taka mająca pecha, że spotkała w życiu Diakona. Paulina "rozpina kabelek" poddaństwa Marzeny. Do tego wykorzystuje "wierną niewolnicę" Marzenę; rozpinając używając jej własnego zniewolenia to właśnie zniewolenie. Bo tym Paulina brzydzi się najbardziej. Plus, to jest podstawowa rzecz która rujnuje Marzenie życie.
lab(3) + Marzena(3) + znajomość mocy Diakona (2) + magia(2) + 11 v 17 -> 21v17 -> 25v17.

- ( 1) Marzena nie traci mocy magicznej; nadal jest magiem.
- ( 1) Marzena nie jest kontrolowalna przez dowolną osobę o silnej woli.

W wyniku nie tylko udało się Paulinie utrzymać Marzenę jako maga; dodatkowo nikt nie będzie w stanie po prostu Marzeny kontrolować (mając silniejszą wolę niż ona). Marzena jest faktycznie wolna.

ESCALATION_2_AD: 

Siła mocy jakie Paulina sprowadziła, czysta energia i czysta skuteczność kaskadą porwała zaklęcia Diakona. Moc Przodka zdegenerowała zmianę wzoru Marzeny przez Adama Diakona i bariery chroniące ją przed użyciem magii krwi też zaczęły się chwiać. Paulina zdecydowała się je wzmocnić.

lab(3) + znajomość mocy Diakona (2) + magia(2) + 11 v 17 -> 18v17 -> 17v17.

- ( 1) Marzena NIE odblokowuje mocy defilera.
- (-1) Tkanka magiczna Marzeny przekształca się w coś "nienaturalnego"

ESCALATION_2_FALLOUT:

Paulina dała radę utrzymać Marzenę jako maga i ją całkowicie wyczyściła ze wpływów zewnętrznych. Udało jej się też (z pomocą Adama Diakona) dokonać czegoś uważanego za niemożliwe - Marzena przestała być reaktywna na magię krwi. Niestety, stało się to kosztem przekształcenia jej tkanki magicznej. Jej tkanka po prostu nie jest reaktywna na magię krwi - i tyle.
Paulina nie wie, że ta tkanka przekształci ją W PRZYSZŁOŚCI w viciniusa (mag bardzo już zmieniony); nadal, było warto.

Paulina jest bardzo zmęczona.

ESCALATION_3_PM: 
-
ESCALATION_3_AD: 
-
ESCALATION_3_FALLOUT:

Nie ma po co dalej operować na Marzenie Dorszaj. Czarodziejka jest wolna i Paulina osiągnęła swoje cele.

END OF ESCALATION MODE:

Marzena bardzo podziękowała Paulinie. Jest wolna.
Paulina wróciła do łóżka, bardzo zmęczona i naprawdę zadowolona z siebie.
Maria powiedziała Paulinie, że jest z niej naprawdę dumna...

HIDDEN MOVEMENT:

- Ireneusz Przaśnik wzywa Alicję Gąszcz i informuje ją o nowej sekcie; jak nie chce bloodbath, niech lepiej policja to rozwiąże.
- Łukasz Perkas przygotowuje ulotki naganiające do "Kłów Kaina" i je kolportuje.
- Alicja Gąszcz raportuje sytuację z klubu; wszystko wskazuje na to, że to działanie faktycznej sekty.

END OF HIDDEN MOVEMENT

## Dark Future:

### Faza 1: Pierwsze ugryzienie

- Grażyna Tuloz przyłapała Karmelika i Marzenę na "master&slave"; ma zamiar eskalować to do Hermana.
- Franciszek Marlin daje wejście "Kłom" do "Rakiety" (PO.suck up to <someone> stronger)
- Wiesław Rekin masakruje wszystkich zawodników w Rakiecie i agituje; bójka w "Rakiecie" z ochroniarzami. Rekin sam wychodzi ze śmiechem.
- Przemysław Kujec dzwoni na numer telefonu Rekina; dostaje instrukcje, że jak się nie boi, ma iść do lasu.
- Dwóch członków sekty obserwuje parafię; chcą poznać jak to miejsce wygląda.
- Joachim konfrontuje się z Korzuniem; ten zwala wszystko na spudłowane "Kły Kaina".
- Ireneusz Przaśnik wzywa Alicję Gąszcz i informuje ją o nowej sekcie; jak nie chce bloodbath, niech lepiej policja to rozwiąże.
- Łukasz Perkas przygotowuje ulotki naganiające do "Kłów Kaina" i je kolportuje.
- Alicja Gąszcz raportuje sytuację z klubu; wszystko wskazuje na to, że to działanie faktycznej sekty.

# Zasługi

* mag: Paulina Tarczyńska, która przekształciła defilerkę-zabawkę w pełnoprawną czarodziejkę NIE defilerkę. Naprawdę pomogła.
* mag: Marzena Dorszaj, która sprytnie uwolniła się od Karmelika, by potem Paulina przekształciła ją w normalną czarodziejkę nie defilerkę. Jej przeznaczeniem jest vicinius.
* czł: Jerzy Karmelik, przyłapany przez doktor Grażynę Tuloz na zabawie w "mastera" dla Marzeny; Paulina i Marzena uratowały jego małżeństwo.
* czł: Grażyna Tuloz, dobra kobieta i zastępczyni Ryszarda Hermana, która (dzięki machinacjom Marzeny) przyłapała Karmelika i Dorszaj na zabawach w godzinach pracy.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                    1. Osiedle Metalowe
                        1. Garaże osiedlowe