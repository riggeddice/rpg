---
layout: inwazja-konspekt
title:  "Błękitny zaskroniec"
campaign: czarodziejka-luster
gm: żółw
players: kić, aga_bl
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170207 - Błękitny Zaskroniec (An, temp)](170207-blekitny-zaskroniec.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Kontekst ogólny sytuacji

* Niebieski Zaskroniec, odłamek Arazille. Wyżera ból i cierpienie, zastępuje je spokojem i szczęśliwością.
* Inga Prosznik, kochająca Michała do stopnia znalezienia legendy o Zaskrońcu i karmienia Zaskrońca
* Michał Prosznik, heroinista 
* Tadeusz Grżnik, poeta kochający naturę, którego ból (po stracie psa) wyżarł Zaskroniec - przyzwał Postacie Graczy zanim Zaskroniec.

CZAS MISJI: 2 godziny 30 minut

## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Tadeusz Grżnik, poeta i znajomy zarówno Andromedy jak i Matyldy popadł w depresję po tym, jak jego psa przejechał jakiś za szybko jadący kierowca. Jako, że ciężko było na niego patrzeć, przyjaciele wysłali go do województwa Lubelskiego; niech odpocznie i poszuka grzybów. Czy coś.

Niecały tydzień później Tadeusz zaprosił Andromedę, też przyjaciółkę, na wystawę sztuki lokalnej. Zaskoczyło go, że lokalna sztuka wygląda, jakby ulegała... dewolucji. Zamiast coraz lepsze i coraz ciekawsze, sztuka i czyny wyrobnictwa są coraz bardziej prymitywne i niestaranne. Andromedę zdziwiła jeszcze jedna rzecz - Tadeusz zachowuje się inaczej, bardziej... żywo. Tak więc stwierdziła, że ma zamiar przyjechać i zobaczyć jak to wygląda. Zwykle Tadeusz wychodził ze złych stanów emocjonalnych wolniej; bardziej go to bolało.

Trzy dni później Andromeda dotarła na miejsce i lekko zmartwiała. Tadeusz siedzi sobie w knajpce, rozwalony, taki błogostan. Nie ma alkoholu, ma wodę. Ogólnie, taki rozmemłany jest. Zapytany przez Andromedę gdzie się rozlokować pokazał Andromedzie pewne miejsce. Losowy dom. Andromeda się... zatroskała. Przepytała go; doszła do wniosku, że coś jest w nim nie tak. 

I na to weszła Matylda, która jest osobą, która poleciła Tadeuszowi Wężokuj na grzyby i przyjechała go obejrzeć. Po prostu, czarodziejka będąca przyjaciółką. Chciała zobaczyć jego stan. Poznała Andromedę; znają się lekko. Matylda uważa Andromedę za czarodziejkę (tak Andromeda się przedstawia; udaje czarodziejkę, choć niekoniecznie zdrową i zdolną do normalnego działania). Więc Matylda rzuciła zaklęcie diagnostyczne na Tadeusza; okazało się, że Tadeusz nie ma motywacji. Wszystko co go bolało, cierpienie, zmartwienie... wszystkie te rzeczy zostały zastąpione błogością i takim pozytywnym szczęściem. Ogólnie... kiepsko.

Po tym Matylda i Andromeda chwilę porozmawiały; Andromeda powiedziała, że nie może czarować bo jest... to wstydliwa sprawa. Poszły więc na kemping. Bez kłopotu zdobyły sobie przyczepę, po czym zdecydowały się pójść na wystawę. Zdecydowały się rozdzielić - Andromeda do biblioteki a Matylda na wystawę. Ale Matylda stwierdziła, że sama nie idzie - poszła wpierw po Tadeusza.

Rozmawiając z Tadeuszem Matylda doszła do tego, że Tadeusz wierzy, że w lesie był wąż. I ten wąż go pocałował i w ogóle problemy odeszły. Matylda zmieniła kierunek - nie jest już zainteresowana pójściem na wystawę; teraz pójdzie zobaczyć leśnego węża. Tadeusz jednak nie dał rady węża znaleźć; powiedział Matyldzie, że las wygląda nie tak jak powinien. Nie ma drogi do węża. Może Inga będzie wiedziała. Kto? Inga, osoba, która zaprowadziła go do węża...

Tymczasem Andromeda poszła do biblioteki. Tam znalazła sympatycznego bibliotekarza i dopytała go o lokalne legendy. Bibliotekarz był lekko nieufny, ale Andromeda go udobruchała; powiedział jej legendę o żmiju-psotniku, który nie lubi cierpienia, acz w okolicy robi psoty i pomaga. Też niedaleko, w Żonkiborze, znajduje się podobno Królowa Marzeń. Kimkolwiek ona by nie była. 

Razem wróciły na kemping. Przegadały temat, Matylda poustawiała zaklęcia defensywne na wszelki wypadek, po czym poszły spać. I dobrze się stało; w nocy wpływ węża był na tyle silny, że zaklęcia defensywne obumarły - ale nie zrobiło to dziewczynom krzywdy.

Dzień 2:

Nie mają pojęcia jak dorwać się do tego cholernego zaskrońca; Ingi nie ma w domku gdzie jej oczekiwali. Matylda, lekko zdesperowana, użyła magii mentalnej i weszła w głowę Tadeusza. Zobaczyła jak Inga wygląda, po czym znalazła na nią namiar magiczny. Inga znajduje się w lesie, ale... w miejscu, które nie do końca istnieje. A w głowie Tadeusza Matylda - botanik - zobaczyła rośliny, które nie istnieją. Czyli Inga jest poza znaną rzeczywistością, w świecie bajek... i nierzeczywistości do których wejście jest w Parku Narodowym.

Nieszczęśliwa Andromeda i zdeterminowana Matylda poszły do lasu, do Węża. Dotarły w okolice Ingi; ta zdziwiła się, skąd one tam w ogóle się wzięły? Matylda walnęła, że przyszły na grzyby. Inga całkowicie zaskoczona; Andromeda zauważyła, że Inga ma na ręce bandaż. Matylda i Andromeda zaczęły gadać; niech Wąż dotrze do INNYCH MIEJSC, niech się wszechżywi. Niech wąż pożre cierpienie całego świata, nie tylko tych kilku ludzi. Inga się zgodziła; Matylda tylko odczekała moment, po czym użyła magii leczniczej i wyleczyła ranę Ingi. Bez krwi wąż nie miał energii i się zdematerializował...

Inga w płacz. Jej mąż, heroinista, będzie cierpiał. Matylda go wyleczyła zielskiem i magią mentalną, po czym naprawiła sprawę z Ingą. No i, oczywiście, taktyczne amnezje...

Po wyczyszczeniu terenu, problem z głowy...

# Progresja

# Streszczenie

Przez sprzężenie się lokalnych legend w Wężokuju - legendy o Żmiju Psotniku i niedaleko znajdującej się w Żonkiborze Arazille doszło do powstania Błękitnego Zaskrońca - odłamka Arazille. Żona heroinisty wezwała Zaskrońca krwią, by jej mąż nie cierpiał. Czarodziejka Matylda i Andromeda udająca maga dały radę rozwiązać problem i naprawić sprawę, głównie magią Matyldy ;-).

# Zasługi

* mag: Matylda Szarotka, czarodziejka chętna do pomocy ludziom; botanik, lekarz i mentalistka. Z zaskoczenia wyleczyła Ingę, destabilizując link z Zaskrońcem.
* czł: Kasia Nowak, udaje maga przed Matyldą i skupia się na pomocy wpierw przyjacielowi - poecie, potem całej mieścinie.
* czł: Tadeusz Grżnik, poeta z depresją, którego cierpienie wyżarł Zaskroniec - połączenie lokalnych legend i potęgi Arazille. Przyjaciel Andromedy i Matyldy.
* czł: Inga Prosznik, kochająca Michała żona z obsesją na punkcie jego spokoju. Skłonna poświęcić się, by tylko on nie cierpiał. Przyzwała i zakleszczyła w tej rzeczywistości Zaskrońca.
* czł: Michał Prosznik, heroinista, ogólnie ciapowata jednostka z kochającą żoną. Nieco bezwolny. Wyleczony z uzależnienia przez Matyldę.
* czł: Franciszek Dromicz, lekko nieufny bibliotekarz Wężokujski. Zna mnóstwo lokalnych legend, zwłaszcza, jeśli te dotyczą węży.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie
            1. Powiat Tonkij
                1. Wężokuj, miejscowość niedaleko Żonkiboru, w promieniu oddziaływania Arazille. W zasadzie niegroźna. Słynie z grzybów.
                    1. Dom kultury, z wystawą lokalnej sztuki (deewolującej przez Zaskrońca). O dziwo, sztuka jest w "wężowym" stylu ;-).
                    1. Biblioteka, w której dość dobrze opisane są lokalne legendy; przygotowana na potencjalny najazd turystów.
                    1. Wężowa Ciżemka, knajpka, w której żył w błogostanie Tadeusz przez działania Zaskrońca.
                    1. Kemping, gdzie nocowały bezpiecznie Matylda i Andromeda.
                1. Wężymordzki Park Narodowy, słynący z wielu legend powiązanych ze Żmijem Psotnikiem.

# Skrypt



# Czas

* Dni: 2

# Notatki

"Wąż w listowiu pocałuje, nic strasznego już nie czujesz"
"Ja szukałem ukojenia a wąż oddał mi marzenia"