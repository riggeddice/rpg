---
layout: inwazja-konspekt
title:  "Camgirl na dragach"
campaign: powrot-karradraela
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170707 - Biznes pośród niesnasek (MB, SD, HS)](170707-biznes-posrod-niesnasek.html)

### Chronologiczna

* [170315 - Naszyjnik Wspomnień (HB, AB, DK)](170315-naszyjnik-wspomnien.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

* Szymon Skubny rzucił kasę na przedstawienie teatralne w teatrze w Czeliminie i wyśle tam przedstawiciela
* Młoda camgirl, Paulina Widoczek, miała szansę zagrać tam niezbyt ważną rolę
* Ktoś podłożył Paulinie jakieś dziwne narkotyki; włączył jej się agresor. Wylądowała w szpitalu w Kopalinie
* Roman Brunowicz, jej chłopak i szef Ognistych Niedźwiedzi jest podejrzany
* Artur Bryś zna Romana. Nie chce, by Niedźwiedzie wpadły w ręce kogoś innego
* Roch Knut aspiruje do rządzenia Niedźwiedziami
* Luiza Wanta też jest w czelimińskim szpitalu; chce móc zrobić reportaż z przedstawicielem Skubnego

## Misja właściwa:

Dzień 1: 

Alinę odwiedził Artur Bryś. Samo to zdziwiło Alinę. Bryś nie odwiedza dziewczyn... odwiedza je w "innym celu". A z Aliną nie próbuje ;-). Przyznał, że przyszedł DO ALINY PO POMOC. 

* Jest taka laska. Nazywa się Paulina. - Bryś, powoli - To camgirl. Wiesz, cumwhore. A przynajmniej kiedyś.
* Chcesz ją znaleźć, to do Patrycji, nie do mnie - Alina z uśmiechem
* Skończyła w szpitalu w Kopalinie. - Bryś. 
* A Tobie zależy bo..? - Alina
* Mój KUMPEL się w niej zakochał - westchnął Bryś - Słuchaj, problem jest duży. Wzięła dragi a podobno on jej dał.
* A twierdzi, że nie dał? - Alina, nie rozumiejąc
* Paulina chciała nie być camwhore. Dostała szansę w jakimś teatrzyku. Teraz Skubny - czujesz, Skubny - dał tam kasę na przedstawienie - Bryś, wzburzony
* I ona tam dorwała jakieś narkotyki. Włączył jej się agresor i wyleciała. - Bryś
* No dobra, no. I? Brzmi jak głupia decyzja - Alina
* Jest głupia jak cep. - Bryś, pochmurny - Ale mój kumpel to szef gangu Ognistych Niedźwiedzi. Nie mów o nich 'ciepłe misie'. 
* Dobrze, że nie troskliwe... - Alina - I co oni robią, te misie?
* Policja będzie chciała przesłuchać mojego kumpla. Ale to rozwali mu pozycję w gangu. Że niby kapował... - Bryś, niechętnie - I Skubny ich przechwyci. A przynajmniej, to brzmi najsensowniej.
* W skrócie: camgirl chciała być tancerką i aktorką. Miała szanse. Skubny zamówił sztukę. Zanim sztuka się... no, wiesz, odbyła to camgirl wzięła dragi. Gliny robią szum. Romek kocha swoją cizię i chce z nią być. Straci pozycję... to niestabilna sprawa jest, w misiach nie jest dobrze. - podsumowanie Brysia - Ktoś jej podłożył te dragi, w teatrze. Stawiam, że ktoś z misiów. Albo Skubny.
* Jak ważny jest ten koleś w misiach? - Alina
* Dowodzi. 
* Jeśli ma tak słabą pozycję... to już nie dowodzi - Alina, wzdychając - To Twój kontakt i wisi Ci parę przysług?
* Uratował mi życie kiedyś - Bryś - Razem w MMA, bracia krwi... nic co zrozumiesz. To sprawy facetów. Ale on ma cizię... 
* Coś się stało w tym teatrze. Ty się znasz na teatrach, ja nie. Jest jeszcze jedna rzecz; te dragi, nie? To Romek z nimi walczy. Nie chce, by misie się w to wplątywały. W misiach są tacy, którym to się podoba. Romek chce uczciwej napierdalany...
* A skąd te dragi? - Alina, zaciekawiona
* No właśnie nie wiem. To coś nowego. Naprawdę mocno daje powera. - Bryś
* Próbowałeś? - ALina, złośliwie
* Nie - Bryś, poważnie - Ale widziałem je w akcji. Walczyłem z jednym co był na tym. Nie chciał paść. Serio... taki berserker.

Bryś zauważył, że on jest od mięśni; tyci mniej od myślenia. Dziewczyna ma dozór w szpitalu i ma lekarza. On z nią nie gadał. Nie chciał spieprzyć. Ale wie, gdzie są różni ludzie. Na razie te dragi wyglądają Brysiowi bardziej na bojowe stymulanty niż na dragi; słabo uzależniają... on tego w ogóle nie rozumie, jak na tym zarobić? A Bryś wyjątkowo nie lubi narkotyków. Widział za wiele kumpli których to załatwiło.

* Czy ten Twój Romek się będzie mógł powstrzymać od śledztwa jak się dowie że się tym zajmujesz? - Alina
* Nie. Ale mogę mu wpierdolić. - Bryś uczciwie - Tak to się robi.

Alina zauważyła, że gdyby Skubny chciał przejąć gang... ma dużo prostszych opcji niż to. Pogadali też chwilę o tym narkotyku - Bryś spotkał sie z tym RAZ. A teraz drugi raz. Nie wiadomo skąd to się bierze i jakie ma kanały dystrybucji...

Paulina chciała zostać fryzjerką gdyby teatr nie wypalił. Niespecjalnie chciała wracać do rozbierania się przed kamerą, bo są młodsze i bardziej konkurencyjne. To wymagałoby przekraczania granic, których Paulina zwyczajnie nie chciała przekraczać. Plus, Romanowi by się to nie podobało.

Bryś niestety nic o niej nie wie. Nie interesowała go poza tym co widział jak była nago. Dla niego to tylko "dziewczyna z kamerki".

Dobra. Alina kazała Brysiowi iść do Romka i niech ten nic nie robi. Niech siedzi na tyłku. Niech siedzi blisko Niedźwiedzi i się coś dowie od nich. Bryś zauważył, że nie ma sprawy - on często bywa na ich arenie. Alina przejmuje to śledztwo...

Alina zaczęła zbierać informacje o narkotyku. Zaczęła od swoich kontaktów w półświatku, Damiana Wiórskiego - dealera. Zaczęła, że wie o nowych dragach na rynku - daje to powera. Damian powiedział, że rozumie. Ale to nie narkotyk. Alina chce wiedzieć KTO to ma i jak to zdobyć.

Damian powiedział co następuje:

* Nie ma tego na rynku. Słyszał o tym. Ale nie ma. Ukrywają to przed Skubnym.
* Kolega rozpytywał. Potem pytał go o tego draga. Zapomniał. A przynajmniej - udawał że zapomniał, to Damian nie naciskał.
* Nie ma dystrybutorów. Nikt tego nie rozprowadza. To się po prostu POJAWIA. I jest zawsze troszkę inne. Opowieści mówią o nieco innych rzeczach.
* Opowieści są bojowe, zaskakujące. Jeden gość podobno nabawił się alergii na srebro po wzięciu tego.

Alina kazała Damianowi trzymać uszy otwarte. Sama musi - MUSI - udać się do Pauliny. Musi dowiedzieć się o tym, czy tam było coś powiązanego z magią czy nie. A czas ma tu ogromne znaczenie. Więc - Alina poszła do szpitala wojskowego. Wczoraj tu wylądowało; to wszystko działo się bardzo niedawno. Kto ją prowadzi? Robert Pomocnik.

Pomocnik się zdziwił, że tak szybko znowu widzi kogoś "od nich". Alina powiedziała, że to śledztwo w sprawie nowego typu narkotyków. A pacjentka kontaktuje, jak najbardziej. Próbka krwi by się też Alinie przydała. Pomocnik powiedział, że to wymaga upoważnienia i dokumentów. Alina odbiła, że papiery papierami, ale wtedy będzie ślad. Alinę nie interesuje Paulina, bo jest ofiarą a nie sprawcą. Ale jak musi załatwić papiery... 

Pomocnik się złamał. Ale niech Alina lub jej ludzie pomogą Paulinie wyjść na prostą, by ta się nie stoczyła. Bo Paulina NIE CHCE się stoczyć, ale nie widzi dla siebie żadnej przyszłości. Alina się zadumała, ale się zgodziła. Obiecała Pomocnikowi, że pomoże wyjść Paulinie na prostą. No to Robert Pomocnik poszedł po próbkę krwi dla Aliny a sama Alina zastanawia się jak to można rozwiązać...

Alina poszła porozmawiać z Pauliną Widoczek. Może dowie się czegoś ciekawego.

Dziewczyna leży w łóżku, wygląda nieźle. Jest załamana tym co zrobiła, aczkolwiek rozumie, że to narkotyk. Ale zniszczyła swoją szansę. Paulina bardzo, bardzo broni swojego Romana. Twierdzi, że nie ma wrogów; nie interesuje się narkotykami. Alina zaczęła przesłuchanie, min. by dowiedzieć się jak zaaplikowano jej narkotyk. Powiedziała Paulinie, że się nią zaopiekują i pomogą jej wyjść na prostą. Paulina nie wierzy; nikt nigdy nie chciał jej pomóc. Alina powiedziała, że zawsze musi być ten pierwszy raz ;-).

Z zeznań Pauliny Alina mogła wydobyć co następuje:

* Jej pamięć NIE jest zakłócona. Jeśli jakiś mag miałby to czyścić, to do Pauliny jeszcze nie dotarł.
* Narkotyk udało Alinie się zlokalizować; był podany jej przez skórę, w rekwizycie. W rękawiczkach czy coś.
* Rękawiczki _nie_ występowały w komplecie. Paulina miała nosić strój. Dostała je do założenia od koleżanki.
* Paulinie po prostu włączył się agresor po kilkunastu minutach; pobiła kilka osób. Dlatego się bała, że będzie oskarżona.

(W wyniku eskalacji: Paulina nie da rady się sama zaadaptować i wróci jako kłopot)

Alina podziękowała, po czym poszła do Pomocnika. Dostała od niego próbkę krwi. I poszła do Edwina.

* Alina. Co Ci jest? - Edwin, wzdychając

Edwin przeprowadził grupę skanów na tej próbce krwi i bardzo spoważniał. Zwłaszcza gdy Alina powiedziała o zapominaniu ludzi i uczuleniu na srebro.

* To wygląda na próbę stworzenia czegoś, dzięki czemu magowie wykorzystują ludzi do walki z magami. - Edwin, zaskoczony
* ...czyli na tą... Paulinę warto postawić czujkę. Wykorzystamy kogoś od Was... - Edwin, planując pułapkę
* Potrzebujemy szerszej operacji, Alino. Idę z tym do Hektora. - Edwin. - To poważne. Dobrze, że mi zgłosiłaś.

Alina złośliwie zrzuciła Paulinę i jej dobrobyt Brysiowi. Niech ma... a jako, że jest obowiązkowy i obiecał... zajmie się. Dziewczyną. Camgirl. O tak :D.

# Progresja

* Artur Bryś: ma się opiekować Pauliną Widoczek
* Paulina Widoczek: ma Artura Brysia jako opiekuna
* Artur Bryś: jego ziomkiem i przyjacielem jest Roman Brunowicz z Ognistych Niedźwiedzi

# Streszczenie

Bryś przyszedł do Aliny, bo jego druh (też wojownik) - Roman z Ognistych Niedźwiedzi - ma kłopot. Jego dziewczyna, eks-camgirl, dostała dziwne dragi i teraz Romek ma kłopot. Alina robiąc to śledztwo odkryła, że narkotyk jest najpewniej magiczny oraz nie ma dystrybutorów; po prostu czasem się pojawia i jest trochę inny za każdym razem. Po zdobyciu próbki krwi ofiary poszła z nią do Edwina; Edwin powiedział, że wygląda to na próby stworzenia czegoś do wykorzystania w walce ludzi przeciw magom. I ktoś świetnie to ukrywa - bo Edwinowi do tej pory nic nie wypłynęło...

# Zasługi

* vic: Alina Bednarz, podjęła śledztwo na prośbę Brysia, skupiła się na pochodzeniu narkotyków i znalazłszy coś magicznego, przekazała to Edwinowi.
* czł: Artur Bryś, lojalny druh szefa 'gangu' Ognistych Niedźwiedzi; przyszedł do Aliny po pomoc, bo nie wie, jak prowadzić śledztwo w sprawie camgirl na dragach. Aha, nie znosi narkotyków.
* czł: Paulina Widoczek, kiedyś camgirl, aspiruje do bycia tancerką. Niestety, po otrzymaniu niewłaściwego narkotyku (nie chciała), włączył się jej agresor i wylądowała w szpitalu. Bryś się będzie nią opiekował.
* czł: Roman Brunowicz, szef 'gangu' Ognistych Niedźwiedzi. Zakochany w Paulinie Widoczek i kumpel Artura Brysia. Wojownik nienawidzący narkotyków i tępiących je u Niedźwiedzi.
* czł: Damian Wiórski, niezbyt ważny dealer i kontakt Aliny; powiedział Alinie, że w sumie ten bojowy narkotyk nie jest rozprowadzany. Wskazał na to, że może być magiczny, bo ludzie zapominają i parzą się srebrem.
* czł: Robert Pomocnik, lekarz prowadzący Paulinę który NAPRAWDĘ chce ją wyciągnąć na prostą; wymusił na Alinie, by ta się Pauliną zajęła (a ta scedowała to na Brysia).
* mag: Edwin Blakenbauer, analizując próbkę krwi Pauliny Widoczek wykrył coś groźnego - to środki umożliwiające ludziom walkę z magami. Zdecydował pójść z tym do Hektora.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Czelimin
                    1. Teatr Wiosenny, gdzie Skubny dofinansował musical "Cyborgi są wśród nas"
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Trzech Myszy
                        1. Szpital Wojskowy, gdzie leży Paulina Widoczek (tytułowa camgirl na dragach)
                1. Piróg Dolny
                    1. Pylisko
                        1. Mordownia Arena Wojny, gdzie w piwnicy znajduje się miejsce walki poszczególnych grup / gangów; pod kontrolą Ognistych Niedźwiedzi

# Czas

* Opóźnienie: 2 dni
* Dni: 1

# Narzędzia MG

## Cel misji

* Przygotowanie misji dla Dusta na środę ;-)
* Rozpoznanie trochę Aliny, zbudowanie z niej postaci a nie "kształtu"
* Rozpoznanie Brysia - kim on jest? Co to za jeden?
* Kawałek "ludzkiego" świata

## Po czym poznam sukces

* Alina jest zbudowana jako postać; zweryfikujemy czy działa i koncepty
* Mamy pomysł na misję dla Dusta na środę ;-)
* Bryś jest bardziej postacią niż tylko szowinistycznym, nudnym mięśniakiem
* Mniej magów niż ludzi ;-).
