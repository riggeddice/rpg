---
layout: inwazja-konspekt
title:  "Reedukacja Infensy"
campaign: taniec-lisci
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160915 - Rekrutacja mimo woli (temp)](160915-rekrutacja-mimo-woli.html)

### Chronologiczna

* [160810 - Zaszczepić Adriana Murarza! (HB, KB, AB, DK)](160810-zaszczepic-adriana-murarza.html)

## Kontekst ogólny sytuacji

Siluria bezpiecznie ewakuowała się z Mileną; teleportowana przez magów krwi Millennium prosto do Kopalina, a stamtąd użyła portalu by uciec do Pirogu Dolnego - tam znajduje się kwatera główna Millennium. Wiktor, wsparty dowodami przez Dagmarę wierzy, że Siluria została porwana. Mileną opiekuje się najlepsi. Siluria tymczasem, po trzech dniach, została poproszona o pojawienie się w porcie, na barce 'Szczupak'.

## Punkt zerowy:

Ż: Które trzy postacie mogą - jako kontakty - przydać się w misji (preferuję 1 człowieka, 1 maga Świecy, 1 dowolne)?
K: Tatiana Zajcew (Świeca), Marysia Kiras (Millennium), Borys Kumin (Blakenbauerowie)
Ż: Jaka własność Silurii sprawia, że ta misja jest wykonalna?
K: Fakt, że wie, jak wygląda Srebrna Świeca od środka i była na salonach Szlachty (może udowodnić dane z Kompleksu Centralnego).

## Misja właściwa:

Dzień 1:

Siluria została poproszona przez Rafaela Diakona o to, by się z nią spotkał na barce "Szczupak" w Pirogu Dolnym. Millennium nie posiada "bazy głównej", jest bardzo rozproszone i Piróg Dolny jest najbliższym co mają do kwatery głównej. Rafael powiedział Silurii, że spotkał się z "jej magami KADEMu", tymi, których pomogła ewakuować. Wpadli w małe kłopoty ze Świecą, ale nic bardzo poważnego. Rafael wie, gdzie mniej więcej się znajdują; dał Silurii namiar. Może ich sama znaleźć (and she will). Siluria się bardzo ucieszyła - tego jej brakowało. Bo... dlaczego KADEMu nadal nie ma?

Rafael też powiedział Silurii, że ma Izabelę Diakon. Siluria się zdziwiła - Rafael wyjaśnił, że Iza Łaniewska przeszła poważną transformację i stała się Diakonką. Teraz jest szczęśliwsza. Nie jest tą samą osobą. Rafael chce, by Siluria zobaczyła Izę by móc ocenić, czy Iza nadaje się do działania. Jest wojna. Rafael chce uruchomić Izę. Najlepszym sposobem jest skonfrontowanie Izy z Silurią, jej arcywrogiem z przeszłości.

"Wyglądasz... inaczej, niż Cię pamiętam..." - Iza to Silurii
"Ty również, ale... nie mówię nic odkrywczego" - Siluria

Iza powiedziała, że to była forma terapii. Że teraz jej lepiej. Że nienawidziła Silurii... ale faktycznie to jest dla niej lepsze. Teraz... teraz jest szczęśliwa. Wie, że to wszystko - ta krwawa łuna, ta nienawiść - to była pochodna jej wzoru - tego, że nienawidziła SIEBIE za to, że nie umiała nikogo uratować (Rafael, jak Iza nie patrzy, pokręcił głową. To nie tylko wzór; wzór jest najmniejszą częścią - to się nazywa poczucie winy). 

Siluria zaryzykowała, powiedziała 'witam w rodzinie' i przytuliła Izę. Czarodziejka zesztywniała, ale odwzajemniła uścisk. Siluria NAPRAWDĘ się uśmiechnęła. To jest dobry dzień.

Iza powiedziała Silurii, że ma problem ze Świecą i nie wie kogo może poprosić o pomoc - stanęło na niej. Siluria się zdziwiła - czemu Iza JEJ to mówi. Iza przypomniała Silurii Kurtynę. 

"Kurtyna była dość radykalna." - Iza
"Do ekstremistów bym ich nie podciągała, ale byli radykalni" - Siluria
"Ja byłam za radykalna dla Kurtyny, wiesz o tym?" - Iza
"Wiem" - Siluria, która IZĘ podciągała pod ekstremistów i dlatego nie wsadzała tam Kurtyny
"Więc... założyliśmy własną organizację. Ja i Kornel. On był radykalniejszy z naszej dwójki" - Iza
"Rozumiem..." - Siluria, która nie chce rozumieć i sobie wyobrażać

Iza wyjaśniła Silurii, że wraz z kolegą - Kornelem Waderą, jeszcze większym ekstremistą - podłapali pomysł Ireny Essner z założeniem szkoły walki. Iza i Kornel wymienili się krwią (gdzie krew Kornela jest schowana przez Izę w Kopalinie). Kornel jest biomantą i terminusem, dorównującym Izie umiejętnościami walki. A przynajmniej, poprzedniej Izie - "aktualna" Iza jeszcze nie w pełni kontroluje swoje ciało i jego impulsy, więc Kornel ją pokona. 
Sęk w tym, że Iza nie ma jak znaleźć Kornela a martwi się, że bez niej Kornel może zrobić coś głupiego i jego podopieczni ucierpią. Organizacja nosiła nazwę "Taniec liści", była to szkoła walki dla wybranych, odpowiednich ideowo ludzi i magów, mająca wyczyścić Srebrną Świecę. Usunąć korupcję, zło, słabości ludzkie. Iza była "żyletą", najbardziej podziwianą i najgroźniejszą wojowniczką. Kornel był ojcem duchowym i "guru". Iza skupiała się na ciele i autonomii. Kornel na duchu i indoktrynacji. Teraz Izy zabrakło...

Zgodnie z tym, co Iza i Kornel postanowili, miesiąc po zniknięciu któregokolwiek (tym razem: Izy), to drugie powinno założyć "spalenie kontaktu" i wytrenowanie następcy. Docelowo "Taniec liści" miał mieć triumwirat przywódczy.

Siluria ma mniej szczęśliwą minę niż miała przed chwilą. Zwłaszcza, że Iza jako pierwsze cele wymieniła Hektora Blakenbauera oraz Wiktora Sowińskiego. Przynajmniej to cele nieosiągalne...
Iza wybrała już sobie też imię. Infensa. Z łaciny "to attack, to avenge". Siluria się uśmiechnęła - coś się nie zmieniło.

Siluria zaproponowała Izie trening. Ta nie jest pewna, ale Siluria nacisnęła na "odzyskanie kontroli nad sobą". To zadziałało. Iza zdecydowała się wejść w walkę z Silurią. Wieczorem? ;-).
Rafael wynajął Izie i Silurii "Błysk" - laserowy paintball na wieczór. Siluria poprosiła Rafaela o coś co może wykorzystać by uwieść Izę. Rafael bez najmniejszych skrupułów podał jej elementy potrzebne do sformowania odpowiednich feromonów. I Siluria się zaczęła przygotowywać.

Wieczór. Błysk. Grają w "wersję ekstremalną" - dostają lekkie pancerze żeby można było wprowadzić też walkę kontaktową. Siluria to dobrze zna, właściciel klubu i Siluria się ZNAJĄ. Krystian włączy dokładnie to co ma włączyć; wszyscy (poza Izą) dokładnie wiedzą jak to się skończy i Iza jest jedyną, co wierzy, że to faktycznie tylko trening walki ;-).

Krystian rozdał dziewczynom granaty dymne. Wszystkie (dyskretna prośba Silurii) są podrasowane feromonem działającym na Izę który jest zmieszany z zapachem Silurii. Siluria robi to nie pierwszy raz ;-). Siluria chce mieć pewność, że może współpracować z Izą i że ich stosunki będą odpowiednio wyglądały. Siluria zdominuje Izę.

Bitwa składa się z pięciu starć.

Iza ma ogromną przewagę. Nienawidziła Silurii, w ogóle nie interesuje się dziewczynami, NIE CHCE tego aspektu Diakonki (a Siluria i Rafael wiedzą, że jest potrzebny!)... więc w pierwszym starciu Siluria rozegrała sprawę sprytnie. Nawet nie próbowała walczyć z Izą; chciała ją przegonić, lekko zadyszeć (by się nawdychała feromonów) i porozpylać granaty. Wynik przerósł najśmielsze oczekiwania Silurii. Nie tylko Iza nie zorientowała się o co Silurii chodzi, ale dodatkowo zaczęła kojarzyć walkę jako przyjemność, zaczęła łączyć intensywne emocje z walki z intensywnymi przyjemnościami też innego typu. Czyli: obniżona bariera. Siluria skończyła haniebnie pokonana.
Siluria zauważyła różnicę między Wiolettą a Izą. Wioletta walczyła tak, by Siluria czuła się zmotywowana. Iza walczy na pełnej mocy. 

Starcie drugie: Siluria chce doprowadzić do starcia bezpośredniego z Izą. Podczas starcia chce obudzić w Izie aspekt Diakonki, aspekt erotyczny. Chce, by Iza uzmysłowiła sobie, że ma potrzeby. Iza jest 17 ale Siluria ma +4. Udało się - Siluria znowu została haniebnie pokonana... ale tym razem wygrała to, co dla niej było ważne.

Starcie trzecie: Iza już zaczyna być lekko zdekoncentrowana. Przez to jeszcze bardziej agresywna i niebezpieczna. Siluria chce pokonać Izę RAZ. Zrobiła badanie terenu. Zrobiła przygotowanie. Ma feed z kamer do kombinezonu. I chce wygrać ten jeden raz z Izą - by całkowicie nieuczciwie wytrącić ją z równowagi. Najlepiej 'snipe her z zasadzki'. Ta runda jest 'single snipe'. Udało się Silurii. Zastrzeliła Izę, pokonała ją w najsilniejszym obszarze i wytrąciła ją z równowagi doszczętnie.

Starcie czwarte: Iza już się nie kontroluje. Chce wygrać za wszelką cenę. Siluria oddaje to starcie jak chodzi o punkty, ale to ma być starcie czysto fizyczne, wręcz, Siluria dąży do wzbudzenia pragnienia w Izie... co do Silurii. Dąży do wzmocnienia sygnału dominacji u Izy wobec Silurii. Absolutne zwycięstwo. Iza nie tylko wytarła Silurią podłogę - WPIERW ją zastrzeliła, POTEM zaczęła rozbierać... Siluria jej na to pozwoliła, ale w pewnym momencie zatrzymała. Iza zorientowała się, co robi i zaczęła przepraszać, na co Siluria podniosła jej głowę władczo i powiedziała "Nie masz za co przepraszać, ale jeśli chcesz... to przyjdź wieczorem. Na MOICH warunkach".
Iza zbuntowała się - ona dostaje czego chce. Podczas walki z Silurią nabiła jej kilka siniaków i podbiła oko. Siluria zatrzymała Izę - terminuska nie powinna się tak zachowywać. I powtórzyła - przyjdź wieczorem. Na moich warunkach. Rozumiesz?

Iza uznała wyższość Silurii w tej kwestii. Rozpalona, przyjdzie wieczorem...
Tej nocy Siluria raz na zawsze postawiła relację swoją z Izą tam, gdzie chciała od momentu, gdy Iza wzięła Silurię na muszkę z pogardą w głosie.
Rafael się nie zmartwił.

Dzień 2:

Siluria obudziła się pierwsza. Z zadowoleniem zobaczyła, że Iza nadal śpi, spokojna i bez zmarszczek złości czy stresu. Korzystając z okazji że Iza śpi mocno, Siluria spróbowała ją delikatnie skrępować. Udało się - terminuska się nie obudziła. Siluria więc zaczęła ją budzić... bardzo przyjemną stymulacją. Iza się obudziła i pisnęła. 

"Nie chcę teraz. Nie życzę sobie" - Infensa, próując się wyplątać
"Ale ja sobie życzę. Powiem więcej - ja żądam" - Siluria, wchodząc głębiej

W okolicach późnego południa Iza praktycznie je Silurii z ręki. De facto zrobi wszystko, czego Siluria zażąda. Siluria jest naprawdę szczęśliwa. I uważa, że Iza nadaje się do normalnego funkcjonowania - zwłaszcza po takich zabiegach. A Iza przestała odrzucać fakt bycia Diakonką i zaakceptowała swoją dualistyczną naturę.

Rafael jest naprawdę zadowolony. Siluria nie ma złudzeń, czemu przyprowadził Infensę...

# Progresja

* Infensa Diakon: przestała być Izabelą Łaniewską i pogodziła się z Silurią Diakon.
* Infensa Diakon: zaakceptowała swoją dualistyczną naturę - dominator i uległa
* Infensa Diakon: podporządkowana Silurii Diakon
* Siluria Diakon: dominuje nad Infensą Diakon

# Streszczenie

Siluria bezpiecznie ewakuowała się z Mileną; teleportowana przez magów krwi Millennium prosto do Kopalina, a stamtąd użyła portalu by uciec do Pirogu Dolnego - tam znajduje się kwatera główna Millennium. Wiktor, wsparty dowodami przez Dagmarę wierzy, że Siluria została porwana. Mileną opiekuje się najlepsi. Rafael przyprowadził Silurii Infensę (kiedyś: Izę Łaniewską) by ta skończyła reedukację. Siluria uwiodła w serii walk Infensę, budząc w niej aspekt dominujący i uległy oraz upewniła się, że Infensa już nadaje się do normalnego działania. Infensa opowiedziała też Silurii o swoich dawnych manewrach z Kornelem, gdzie zrobili grupę "Taniec liści" z ludzi i magów mających oczyścić Świecę i inne gildie magów z nieprawości...

# Zasługi

* mag: Siluria Diakon, poprawnie odczytując intencje Rafaela zbudowała odpowiednio relację z Infensą Diakon; też: dowiedziała się o "Tańcu liści".
* mag: Rafael Diakon, nadzoruje transformację Izy Łaniewskiej w Infensę Diakon i przyprowadził ją do Silurii celem skończenia reedukacji. Też: powiedział Silurii o KADEMkach.
* mag: Infensa Diakon, kiedyś: Iza Łaniewska, opowiedziała Silurii o swoich przeszłych działaniach z Kornelem i została uwiedziona; jest w roli podległej wobec Silurii.
* mag: Kornel Wadera, zaufany Izy, ekstremista i potencjalny guru "Tańca liści" - sekty walki wręcz (* magów i ludzi) mającej czyścić Świecę z nieprawości. Przyjaciel Izy.
* czł: Krystian Miałcz, właściciel Błysku (laserpaintball) który wiele rzeczy już z Diakonkami widział i dość mu płacą by nie zadawał pytań i nic nie mówił ;-)

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Ulica Rzeczna, wzdłuż rzeki Flisaczki
                        1. Domek Millennium, gdzie noc spędziły Siluria z Infensą
                        1. Port rzeczny, gdzie na barce 'Szczupak' spotkała się Iza, Rafael i Siluria
                    1. Ulica Targowa, prowadząca do innych miast
                        1. Błysk - laserowy paintball, gdzie w ramach treningu pojedynkują się Iza i Siluria

# Wątki



# Skrypt

- brak

# Czas

* Opóźnienie: 3 dni
* Dni: 2