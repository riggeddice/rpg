---
layout: inwazja-konspekt
title:  "Uczniowie Moriatha"
campaign: druga-inwazja
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt

---
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140103 - Tak bardzo nie artefakt(AW, WZ)](140103-tak-bardzo-nie-artefakt.html)

### Chronologiczna

* [140103 - Tak bardzo nie artefakt(AW, WZ)](140103-tak-bardzo-nie-artefakt.html)

## Misja właściwa:

Tym razem do biura Andrei przyszedł chyba najmniej spodziewany gość - detektyw Waldemar Zupaczka. Ze zleceniem.

Zlecenie Zupaczka otrzymał od ducha. Duch nie jest w najlepszym stanie, niewiele pamięta, ale czuje się odpowiedzialny za swoich wychowanków - Rebekę i Sebastiana. Pamięta, że Rebeka jest chora i że potrzebuje eliksiru leczniczego raz w roku... ale eliksir jest dla niej niebezpieczny, bo jest to eliksir rytualny sprzed Zaćmienia. Zlecenie polega na tym, by Andrea i Zupaczka powstrzymali wychowanków ducha przed stworzeniem eliksiru.

Problem polega na tym, że duch praktycznie nic nie pamięta poza kilkoma scenami oraz nie ma pojęcia gdzie można znaleźć jego wychowanków. Aha, i że ci wychowankowie bardzo nie ufają magom żadnego typu. Super.

Sceny i obrazy jakie duch był w stanie pokazać Andrei były następujące:
- jednym ze składników do stworzenia eliksiru jest jad dromopod iserat, "hipnotycznych skorpionów", bardzo reglamentowanych i niebezpiecznych.
- Rebeka jest iluzjonistką i mentalistką, Sebastian jest magiem specjalizującym się w materii i energii.
- duch kojarzy trzy czarne, płonące wieże. Z wiecznymi płomieniami.
- bardzo ważny jest posąg jeźdźca bez głowy na koniu z niewidzialnym mieczem (innymi słowy, zdewastowany pomnik ludzki :P). 
- Strażnik schowany jest za symbolem biohazard; hasłem unieszkodliwiającym jest "pocałunek skolopendry".
- gdzieś tam ukryte są jego artefakty. Młodzi oddadzą artefakty; namówi ich do tego - to jest zapłata za misję.

Andrea ciężko westchnęła. Zarówno ona jak i Zupaczka mają jednak dobre serca... a rytuał sprzed Zaćmienia to po prostu katastrofa... Dobra, bierze to. Zupaczka ma wzmocnić ducha i dowiedzieć się najwięcej jak się da na temat ciała ducha i samego ducha, a ona rusza w śledztwo na bazie nędznych posiadanych przez siebie tropów.

Zaczęła od trzech czarnych wież. Jako czarodziejka SŚ poznała tą strukturę - Struktura SŚ, izolacjonistów, nie przyjmujących Zaćmienia do wiadomości. Kiedyś bardzo zamieszkały obszar, chwilowo dość wyludniony, ale Izolacjoniści dalej udają, że ich tam dość sporo. Super. Ale Andrea głupia nie jest - sprawdziła sobie perspektywę widoku pokazaną przez ducha by określić mniej więcej z jakiego OBSZARU duch widział trzy wieże i zawęziła obszar poszukiwań do 8 rezydencji. Nie tak źle. Jak Zupaczka skończy z duchem, będzie mogła wykorzystać jego i jego moce rytualnej detekcji do znalezienia czegoś więcej na ten temat.

Następnie korzystając ze swoich powiązań na "czarnym rynku" magów spróbowała się dowiedzieć czegoś więcej na temat tych dromopod iserat. Czy ktoś nie handlował jadem czy coś. Okazało się coś większego, w ciągu ostatnich 4-3 lat szło kilka dromopod iserat po czarnym rynku i podejrzewano nielegalną hodowlę. Potem terminusi SŚ uderzyli, coś znaleźli i sprawa się zamknęła, ale nadal podobno nie mają wszystkich dromopod iserat. Na czarnym rynku poszło 6 dromopod iserat. Sporo jadu by z nich było...

Dobra, Andrea sprawdziła jeszcze wśród powiązanych ze sobą ludzi czy wiedzą coś odnośnie takiego pomnika. Okazuje się, że tak, jest taki pomnik. W niewielkiej miejscowości "Piróg Górny", dość niedaleko stąd (~20 km). Hm.

Działania Andrei ściągnęły na nią zainteresowanie terminusa z którym Andrea ma na pieńku (krzyżowo), niejakiego tien Mariana Agresta. Agrest zdecydował się z nią spotkać w tej sprawie. Zaczął z grubej rury ("co mają wspólnego dromopod iserat i Piróg Górny") chcąc ją pojechać i upokorzyć, ale Andrei udało się wybronić i ze  zwierzyny stała się myśliwym. Nie tylko wiele nie powiedziała Agrestowi; dodatkowo wydobyła odeń kilka ciekawych informacji. Owszem, udało się swego czasu zlokalizować hodowlę dromopod iserat prowadzoną przez tien Antoniego Myszeczkę, ale ten popełnił samobójstwo; zeskoczył do czarnych wież Izolacjonistów. Były poważne podejrzenia, że "ktoś mu pomógł" i że zatruł go jadem dromopod iserat, ale nie udało się tego w żaden sposób udowodnić czy zaprzeczyć. Co niepokojące, terminusi nie mogą się doliczyć grupy dromopod iserat. Myszeczce zniknęło dziewięć...
Andrea skutecznie odwróciła jego uwagę i skierowała Agresta na coś innego. Sama ma swobodne poruszanie się wraz z Zupaczką w okolicach Piróga.

Tymczasem Zupaczka zakończył działania z duchem i wrócił do Andrei. Ta sprawdziła swoje biuro pod kątem podsłuchów i okazało się, że Agrest podłożył kilka magicznych pluskiew jej w biurze. W związku z tym Andrea je zakłóciły by Agrest myślał, że dostaje prawdziwe informacje a z Zupaczką porozmawiała "cicho". Otóż, duch jest silniejszy i widać wyraźnie, że jest spopielony. Jego proch jest w konkretnym miejscu, w mniej więcej 80%, ale niesamowicie daleko (z punktu widzenia Zupaczki), na pewno nie na terenie Srebrnej Świecy.
...może duchem jest Antoni Myszeczka? By to sprawdzić, Andrea zaprowadziła go do wież, ale nie, nic duch nie "czuje". To nie tu. To nie on.

Nic to, Zupaczka wraz z duchem wybrali się na poszukiwanie tych rezydencji które wskazała Andrea i faktycznie, w jednej z rezydencji - rezydencji tien Lenarta Stosala - w piwnicy znajduje się symbol biohazard a za nim Brama Srebrnej Świecy. Brama, która nie wiadomo gdzie prowadzi. Nie ma jednak śladu po żadnym strażniku ani po prochach ducha.

Tu już wiele nie osiągną. Andrea dodała dwa do dwóch i udali się do Piroga Górnego. Tam, na rynku, znajduje się ów posąg. Tam też Zupaczka rzucił zaklęcie detekcyjne i odkrył, że są niedaleko prochów ducha, które znajdują się poza Pirogiem, na wschód, pod ziemią. SUPER (ironia).
Poszli za wskazaniami Zupaczki i odkryli jedno z poPGRowych gospodarstw. Znajduje się tam jedna rodzina o charakterystycznych cechach - obecni mają objawy długotrwałego zatruwania dromopod iserat (białe tęczówki i sine wargi). Hmm... robi się ciekawiej.

Andrea dokładnie przejrzała to gospodarstwo i odkryła, że studnia jest w rzeczywistości bardzo dobrze skonstruowaną iluzją z zewnętrznym źródłem energii. Po przeanalizowaniu okazało się, że studnia kryje zejście do podziemnej bazy. W tej bazie Zupaczka potwierdził obecność prochów ducha. Ale czyżby to ci młodzi truli ludzi dromopod iserat, czy to jakiś inny mag?

Zdecydowali się wysłać ducha by porozmawiał ze swymi wychowankami i przekonał ich do podyskutowania. Andrea i Zupaczka dali radę potwierdzić obecność maskowanej bazy i artefaktów w niej ukrytych. Czyli duch nie kłamał! Gdy duch wrócił, powiedział, że jego wychowankowie są otwarci na rozmowę i zapraszają na dół, po odbiór artefaktów (oni nie są w stanie przejść przez strażnika, więc ich nie uzyskali). Młodzi mają większość składników na eliksir dla Rebeki, ale nie mają jednego kluczowego, trudnego do zdobycia.

"No, to niepokojące..." - Andrea do Zupaczki.
"Czemu? To logiczne"
"Zastanów się! Mają jad dromopod iserat... co może być TRUDNE DO ZDOBYCIA w porównaniu z tym? Śledziona maga?" - prorocze słowa Andrei

Duch powiedział coś jeszcze - nazwali go "Moriath", ale to nic mu nie mówi.

Andrea nie przejmując się niczym natychmiast odpaliła hipernet. Moriath, okazuje się, był politykiem w świecie magów i jednocześnie potężnym magiem (identyfikowanym jako magiczny lekarz). Terminusi do dzisiaj nie odkryli kim Moriath był, ale po jednej z akcji zniknął i nigdy nie pojawił się ponownie. Był niebezpieczny, gdyż był potężny; ale NAPRAWDĘ niebezpieczny był z uwagi na poglądy i brak etyki - eksperymentował na magach, ludziach, hybrydyzował... jednocześnie jednak posunął medycynę magiczną naprawdę do przodu i miał wielu zwolenników.

Ogólnie, za kilka swoich działań, miał nieoficjalne "kill on sight" ze strony wielu światłych magów.

Jak tylko Andrea zorientowała się o co chodzi, wezwała Zupaczkę do ucieczki, lecz za późno (zwłaszcza, że Zupaczka chciał nagrodę). Sebastian zlokalizował gdzie stali Andrea i Waldemar i pod nimi grunt się zapadł - spadli prosto do bazy, koło Sebastiana i Rebeki, w pełni przygotowanych do walki. Zupaczka zemdlał; spadła na niego Andrea. Ta jedynie udała omdlenie (a oboje uczniowie Moriatha to łyknęli). Pojawił się duch i skrzyczał młodych, że co oni sobie myślą, że Andrea i Waldemar to sojusznicy. Młodzi zauważyli słusznie, że Moriath stracił pamięć...

Rebeka wygląda bardzo źle. Słania się na nogach, ledwo stoi, jest cała spocona i słaba, ale wciąż jest w stanie czarować. Sebastian z triumfem stwierdził, że mają ostatni komponent - krew nekromanty - wskazując na Waldemara Zupaczkę. Duch zaczął ich rugać, na co Rebeka odparła z miłością, że przywrócą mu pamięć teraz, jak do nich wrócił.

Andrei się to baaaardzo nie spodobało.

Rebeka zaczęła splatać zaklęcie kontrolne by rzucić je na nieprzytomnych. Andrea zdecydowała, że trzeba z nimi porozmawiać, lub uciekać. Podłożyła Zupaczce dwie wizytówki używając swoich umiejętności kieszonkowca (by mieć co lokalizować jakby musiała uciekać) i spróbowała odeprzeć zaklęcie Rebeki. Częściowo jej się udało; utraciła zdolność do czarowania i korzystania z hipernetu, ale nic więcej.

Spróbowała udawać dużo bardziej osłabioną niż jest i podyskutować z młodymi; udało jej się ich oszukać. Powiedziała Rebece i Sebastianowi jak bardzo niebezpieczne jest korzystanie z rytuałów sprzed Zaćmienia, na co Sebastian odpowiedział, że nie mają wyboru. Powiedział, że Moriath zginął, gdy udał się na akcję by porwać alchemika mającego pomóc mu skorygować rytuał po Zaćmieniu. Spaliło go jakieś ogniste zaklęcie i na tą stronę przedostał się jedynie proszek...

Andrea dowiedziała się jeszcze, że młodzi są w sobie beznadziejnie wręcz zakochani i ich głównym celem - no matter what - jest uratowanie Rebeki. Oraz przywrócenie Moriatha do liczostwa. Nie może rządzić Świecą żywy, niech rządzi martwy. No, na to nie mogła pozwolić. Gdy jedynie Rebeka zaczęła splatać kolejne zaklęcie obezwładniające, Andrea rzuciła w nią kamieniem. Kamień powinien jedynie zaboleć, ale Rebeka aż się przewróciła (tak słaba!). Andrea zaczęła uciekać do drzwi z symbolem biohazard.

Sebastian próbował ją ostrzec "NIE! Tam jest strażnik! Zabije cię!", ale Andrea na to liczyła. Przebiegła przez drzwi, użyła hasła "Pocałunek Skolopendry" by strażnik jej nie skrzywdził i przeszła przez portal na drugą stronę. Aha, strażnik był kiedyś człowiekiem a teraz był stworem sformowanym z dromopod iserat, czegoś jeszcze i człowieka. SUPER.

No, jest problem, zwłaszcza, że nie poczaruje. Wydostała się z piwnicy, schowała się przed gospodarzem domu (tien Stosalem) i złapała pierwszego terminusa jakiego była w stanie. Niestety, jej reputacja nie jest najlepsza. Co gorsza, nie miała dowodów - terminus jej zwyczajnie nie uwierzył. A czas płynął. Szybko do biura, rozpaczliwa puryfikacja celem oczyszczenia z zaklęcia Rebeki i telefon do Wacława Zajcewa, swego partnera.

Przyjechał natychmiast i natychmiast ruszyli do Piroga Górnego. Niestety, tyci za późno.

Wkroczyli do (opustoszałej już) bazy i okazało się, że w kryjówce nie ma artefaktów a Brama między siedzibą Stosala i Bazą jest nieodwracalnie zniszczona. Spróbowała znaleźć jak najwięcej, odczyniając i czyszcząc, znaleźli dobrze ukryty i świetnie wykonany węzeł służący jako zasilanie bazy. Niestety, przypadkiem Strażnik wpadł w ten węzeł, co wyłączyło wszystkie zaklęcia w bazie i przeprowadziło węzeł w stan częściowego rozpadu i hibernacji. Innymi słowy, destabilizacji wszystkiego.

Znaleźli też hodowlę dromopod iserat (pustą) i hangar na jakiegoś stwora (pusty). Po młodych i Zupaczce ani śladu.

Andrea wykorzystała to, że wcześniej podłożyła Zupaczce swoją wizytówkę i przygotowała się do śledzenia kolegi detektywa. Zlokalizowała go i udało jej się namierzyć mniej więcej lokalizację uczniów Moriatha. Znajdowali się gdzieś pod ziemią, poruszali się w określonym kierunku. Używając zaklęć teleportacyjnych, Wacław i Andrea zdecydowali się spróbować ich dogonić. To była wyczerpująca próba, ale dali radę pojawić się na lądzie w okolicach, gdzie pod ziemią byli Sebastian i Rebeka. A obszar, tak się składa, niezaludniony.

Wacław zastosował standardową delikatność Zajcewów i walnął meteorem w obszar, pod którym przemieszczał się Czerw. Uderzenie meteora było tak silne, że Czerw został zmuszony do częściowego wynurzenia się. Zdesperowany, Sebastian wynurzył się z Czerwia świecąc wszystkimi bojowymi artefaktami zabranymi ze skrytki.

Jedyny problem polega na tym, że Sebastian nie wie z jakimi artefaktami ma do czynienia i nigdy nie walczył jeszcze w takich warunkach. Wacław uderzył kolejnym katastrofalnie potężnym czarem bojowym. Sebastian w odpowiedzi odpalił wszystkie artefakty jednocześnie, przepalając się i rozładowując tymczasowo posiadane artefakty (tak bardzo interferencja!). Artefakty defensywne jednak zadziałały i wchłonęły zaklęcie rzucone przez Wacława. Niewiele pomogło to Sebastianowi - został porażony magicznie a artefakty pointerferowały i się powyłączały awaryjnie.

Kolejnego zaklęcia już nikt nie rzucił - Rebeka wysunęła się z Czerwia trzymając Zupaczkę z nożem na szyi i ultimatum "odejdą, albo on zginie". Nie żartowała. Ale nie miała siły go utrzymać; Czerw się przesunął i Zupaczka upadł na ziemię. Rebeka nie próbowała go odzyskać; wykorzystała jego ciało jako tarczę i wycofała się Czerwiem zostawiając skorpiony (poza jednym) i Zupaczkę na polu bitwy, za to ratując Sebastiana.

Obładowani łupami (skorpiony, Zupaczka, baza) Wacław i Andrea zdecydowali się nie gonić uczniów Moriatha tylko zabezpieczyć co się da i przekazać to Świecy za godziwą opłatę. Tak też zrobili.

Za co najmniej część zapłaty "na dobre" odesłali Moriatha w niepamięć; duch jest całkowicie niedostępny dla kogokolwiek. Zajął się tym wynajęty kompetentny nekromanta. Za kolejną część zapłaty, poważnie podnieśli poziom zabezpieczeń swojego biura. W końcu podpadli zwolennikom Moriatha...
Aha, i poszło też na wydatki dla pomocy medycznej Zupaczce. Uczniowie Moriatha nie zrobili mu żadnej krzywdy; potrzebowali CZYSTEJ krwi nekromanty by pomóc Rebece, a potem nie było potrzeby robić nic więcej by Zupaczkę zdominować poza prostymi zaklęciami.

Ów Stosal okazał się być właśnie zwolennikiem Moriatha i jego idei. Jednak przesłuchany i przeskanowany - okazało się, że był całkowicie niewinny i nie wiedział nic o np. skorpionach; wiedział jedynie, że Moriath kazał pilnować mu podziemnego portalu i go pilnował. W ten sposób tien Lenart Stosal wykręcił się sianem; za poglądy nikomu jeszcze krzywdy nie zrobiono w SŚ.

Podziemną bazę wraz z niezbyt stabilnym węzłem przejęła Świeca.
Uczniowie Moriatha się wycofali. Nie mają dużo czasu, mają niesprawne artefakty bojowe, mają chorą Rebekę i wszystkie składniki potrzebne do lekarstwa na kolejny rok dla Rebeki. A, i jednego skorpiona. 

Niestety, nie ma możliwości sensownego zlokalizowania uczniów Moriatha, więc kolejny ruch należy do nich.

---

Kilka zapisanych testów:

test trudny, "3 czarne wieże z płomieniami": ++ (+5), członek SŚ (+3) -> pełen sukces. Struktura SŚ, izolacjonistów, nie przyjmujących Zaćmienia do wiadomości.
test normalny, "pomnik takiego jeźdźca": skonfliktowany, konflikt -> wprowadzenie przeciwnika
test 2 (hard) + 3 (przesłuchanie) vs 2 (hot) + 3 (podstępna) + 2 (uznaniowe), "Co łączy dromopod iserat z Pirogiem Górnym"?  -> upokorzenie vs zdobycie informacji. Pełen sukces.
test sharp + dywersja vs jego śledzenie: pełny sukces, mogą swobodnie się poruszać z Zupaczką; odwróciła jego uwagę.

# Zasługi

* mag: Andrea Wilgacz jako czarodziejka odkrywająca tajemnice przeszłości i nieszczęsnego ducha Moriatha.
* mag: Wacław Zajcew jako detektyw który miał wreszcie okazję do odpalenia pełnej mocy bojowej.
* mag: Waldemar Zupaczka jako tym razem zleceniodawca oraz osoba rozmawiająca z duchami. Też ofiara ratowana przez Andreę i Wacława.
* mag: Rebeka Piryt jako uczennica Moriatha (mentalistka + iluzjonistka), zakochana w Sebastianie i bezwzględnie chcąca utrzymać status quo.
* mag: Sebastian Tecznia jako uczeń Moriatha (materia + energia), śmiertelnie w Rebece zakochany i chcący ją ratować.
* mag: Marian Agrest jako mag wrogi Andrei który naprowadził ją na Dromopod Iserat i Antoniego Myszeczkę.
* mag: Antoni Myszeczka jako mag, któremu zniknęły Dromopod Iserat w pewnej skandalicznej akcji.
* mag: Lenart Stosal jako zwolennik Moriatha który jednak był, jak się okazuje, niewinny zgodnie z prawem.
* mag: Mojra jako duch Moriatha, który powodowany obowiązkiem ochrony Sebastiana i Rebeki doprowadził do ich uciemiężenia i swego zniszczenia.* 
* vic: Cezary Piryt jako Strażnik, skażony vicinius w formie pośredniej między człowiekiem a Dromopod Iserat. KIA w tunelach.