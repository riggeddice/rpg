---
layout: inwazja-konspekt
title:  "Tylko nieświadomy elemental"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171015 - Powstrzymana wojna domowa](171015-powstrzymana-wojna-domowa.html)

### Chronologiczna

* [171015 - Powstrzymana wojna domowa](171015-powstrzymana-wojna-domowa.html)

## Kontekst ogólny sytuacji

### Siły główne

-

### Stan aktualny

-

### Istotne miejsca

* Mżawiór, gdzie znajduje się Portalisko Pustulskie
* Rogowiec, gdzie znajduje się hodowla Myszeczki
* Męczymordy, gdzie znajduje się magitrownia

## Punkt zerowy:

* Ż: Co sprawiło, że właśnie do Pauliny przybył ciężko połamany pacjent - człowiek - z Wieży Wichrów?
* K: Jak Paulina była Rezydentką, otarła się z nim w jakichś okolicznościach - tamten mag wysłał człowieka do Pauliny
* Ż: Jaka akcja połączyła Paulinę i tego maga?
* K: Ten mag kiedyś pomógł uspokoić Paulinie efemerydę. Oboje zyskali materialnie. On przy użyciu Materii.

## Potencjalne pytania historyczne:

* Czy Filip dostarczy zaawansowane komponenty sterowania i nawigacji zgodnie z kontraktem?
    * EARLY: reperowanie
    * MID: składanie do kupy z posiadanych elementali i komponentów
    * LATE: dostarczenie do montowni awianów
* Czy Sądeczny dostanie pretekst, by udowodnić niekompetencję Filipa i potencjalnie przejąć Wieżę Wichrów?
    * EARLY: -
    * MID: dowody problemów z pojedynczym elementalem
    * LATE: posiadany dowód, wizyta u Filipa
* Czy "elemental" otworzy portal do swojej fazy i da radę uciec?
    * EARLY: ucieczka i zagubienie
    * MID: dostać się na Portalisko
    * LATE: otworzyć portal

## Misja właściwa:

**Wcześniej**

* Coś jest nie tak, szefie. Jesteśmy w niewłaściwych parametrach burzy - Szymon, do Filipa
* Musimy dostarczyć odpowiednie elementale; w innym wypadku nie wywiążemy się z kontraktu - Filip, z determinacją - Odpalaj
* No nie wiem... nie widziałem jeszcze czegoś takiego... - Szymon - Wyraźnie słyszałem kwiczenie świni...
* Świnie, nie świnie... kontrakt MUSI być spełniony. Jak co, masz kompensator, nic Ci się nie stanie - Filip, mistrz analizy ryzyka

**Dzień 1**:

Jako, że Paulina rozpuszczała wieści, że już jest na miejscu - nie zdziwiło jej, że w pewnym momencie dostała pacjenta. Człowieka. Przyleciał awianem. Z nim drugi, który Paulinę zawołał...

Nieszczęśnik do Pauliny jest Ciężko Połamany, Electrocuted, Nieprzytomny i Beyond Help. Na pytanie Pauliny co mu się stało ten drugi, Władysław, powiedział, że pierwszego (Szymona) porwał żywiołak, podniósł, electrocutował i upuścił...

Paulina wywaliła Władysława do siebie do gabinetu po zasoby a w międzyczasie sama przesunęła go w stazę nekrotyczną (ignorując aspekty). Kupiła sobie czas. Zgodnie z tym co mówi Władysław, Grzegorz spadł z ogromnej wysokości. On sam tego nie widział. Szef wezwał swojego prywatnego awiana i zrobił co może - wysłał ich tutaj.

Paulina rzuciła czar diagnostyczny. SS. Nieszczęśnik został poturbowany, electrocutowany i zrzucony. Żyje tylko dzięki artefaktowi mającemu trzymać go przy życiu. Niebezpieczny i Skażający - i dał radę skazić także swojego człowieka... czyli teraz jest: Ciężko Połamany, Electrocuted, Nieprzytomny, Skażony, Beyond Help. Ale czar Pauliny dotknął też awiana - i wykrył, że w awianie znajduje się nie pusty bezmyślny elemental. Znajduje się tam spętana, zniewolona, przerażona istota. Ten elemental w awianie... nie jest elementalem. Nie takim normalnym. Dracena też dostała tą informację. Maria też...

Elemental idzie na drugi plan. Wpierw trzeba rozpiąć Szymona od artefaktu, Odkazić go. Artefakt wplótł się w jego układ krwionośny itp. Sam artefakt jest Zintegrowany z ofiarą, Zakazany nie bez powodu a cel jest Skażony. Dzięki Stazie Nekrotycznej Paulinie jest dużo łatwiej, ale to nadal wyzwanie. Wykorzystała swój gabinet do puryfikacji. Udało się jej odpiąć artefakt od ofiary i zredukować próg Skażenia. Dobra robota, acz spociła się jak ruda mysz...

Paulina zdecydowała się poświęcić temu przypadkowi sporo czasu. Wpierw skupić się na umyśle ofiary, potem na jego ciele, w sekwencji dwóch zaklęć. I Paulina przeżyła szok. SS. W uszkodzonym umyśle i porażonych prądem neuronach Paulina znalazła dwa umysły - człowieka oraz coś dziwnego, słabiutkiego pasożytniczego vicinusa. I ten vicinus... zgasł. Paulina musiała wybrać, kogo ratuje. Wybrała naturalny umysł człowieka. Następnie skupiła się na naprawie ciała... poświęciła więcej czasu, ale była już zmęczona. I niestety, ma efekt Paradoksalny - podpięła umysł Szymona do jakiegoś psychic linka innego bytu. Czegoś w okolicach Burzowca. Dla MG: podpięła go do Terelimitirisa. Terelimitiris wie, gdzie jest Szymon. I wie, że jest w dobrej formie.

Szymon jest w dobrej formie. Ale Paulina trzyma go w stazie. Sama Paulina, nie dbając o to, że jest późny wieczór, wsiada do awiana, bierze Władysława za przewodnika i leci. Może być więcej ofiar. A Maria pilnuje pacjenta.

Podczas lotu Dracena zauważyła telefonicznie Paulinie, że jej się to bardzo nie podoba. Jakieś niewolone vicinusy w awianach..? Może Kajetan? Nic, Paulina powiedziała, że nie ma teraz możliwości się tym zająć. Mogą być ranni.

Burzowiec. Paulina dotarła tam w awianie, do samej Wieży Wichrów. Chwilowo nie ma burzy. Powitał ją czarodziej Filip. Wyglądał na bardzo zmęczonego. Zapytany, powiedział Paulinie, że ma trzech rannych, ale nie tak poważnie. Rozpuścił ich do domów. Powiedział jej, że gdy wywoływał burzę, pojawiła się za silna i pojawiły się DWA elementale. Nie jeden. I jeden był za silny... Filip go odstraszył, ale szkoda się stała - zupełnie, jakby niedaleko była jakaś emisja energii magicznej.

Co on, gazet nie czyta? Paulina przycisnęła. Filip ma Poczucie Winy ale Powołuje Się Na Starą Przyjaźń. Paulina wzmocniła sygnałem, że Szymon nie jest w pełni wyleczony, bo jest podlinkowany do czegoś tutaj. Sukces - Filip powiedział Paulinie mniej więcej, że coś poszło nie tak... bo mafia. Mafia ma za duże żądania. Za duże opłaty. Filip ledwo daje radę, nie może działać bezpiecznie. Musi ryzykować. I jedna z tych ryzykownych rzeczy nie wyszła - próba wykorzystania bardziej świadomych żywiołaków. Dużo drożej i skuteczniej. Echa emocjonalne z Daemonica sprzężone z żywiołakami powietrza. I kilka wyrwało się spod kontroli... resztę Paulina zna.

Jeden z elementali jest powiązany z elementem Gniewu. I on jest na wolności - raiding parties Filipa nie dały rady go odnaleźć. Ale go znajdą, to tylko kwestia czasu... raiding parties to dwa golemy każdy, napędzane elementalami. Paulina zaznaczyła, że ten artefakt - on był niebezpieczny. Filip potwierdził - ale dzięki temu Szymon przeżył.

Filip zaproponował Paulinie obchód po rannych - niech sama ich zobaczy. Paulina się zgodziła. Jako, że było bardzo późno, stwierdziła, że zostanie na noc...

Koło 4 rano Paulina dostała distress emotional signal od Marii. Elemental zaatakował gabinet. Maria jest poparzona, boli ją; poraziło ją gdy chroniła Szymona w stazie; szczęśliwie, Dracena się pojawiła i odegnała elementala, acz silnym kosztem osobistym. Dracena jest wrażliwa na porażenia prądem.

Paulina błyskawicznie złapała Filipa wyciągając go z łóżka i polecieli tam jego awianem, we dwójkę...

**Dzień 2**:

Paulina z Filipem dotarli do gabinetu Pauliny. Do jej domku. Przywitała ją Dracena; jest częściowo sparaliżowana (poszło w układ nerwowy) i w egzoszkielecie. Paulina wzięła toster i kazała Dracenie go zasymilować; Dracena jest Vulnerable i Overexerted. Dracena pomogła jak była w stanie wspierając technomancją a Maria odwracała uwagę Filipa prowadząc go do Szymona. Sukces. Parę godzin później Dracena znowu nadawała się do pełnego działania. Niestety, Dracena będzie na autorepair - kilkanaście godzin - inaczej Paulina musi doglądać cały czas.

Filip powiedział Paulinie, że musi lecieć. Ma kontrakty, które musi zrealizować w bardzo krótkim terminie a do tych kontraktów KONIECZNIE potrzebny jest mag. Awianem przyśle Paulinie grupę ochronną, która ma zatrzymać elementala, gdyby ten miał się pojawić...

Paulina jest bardzo skonfliktowana. WIE, że gdyby nie Dracena, to elemental mógł poniszczyć wiele rzeczy i poranić ludzi. Więcej, gdyby nie Dracena - to by się stało. A Paulina wypisała Dracenie zwolnienie na następny dzień...

Gdy Paulina spytała Filipa czy on jest w stanie osłonić się w awianie przed tym elementalem, Filip potwierdził - jest zabezpieczony do walki z takimi bytami. Paulina powiedziała, że od tej odpowiedzi zależy życie jego i Szymona. Filip potwierdził. Na takie słowa, Paulina wezwała Marię i poprosiła ją o wydobycie informacji od Filipa po soullinku. Filip jest Skryty, Przewaga Konkurencyjna i Wstyd. Maria go rozpracowała.

Filip się przyznał. Wykorzystują elementale do zarówno sterowania jak i zasilania - ale dla przewagi konkurencyjnej, wykorzystują je splicując. Ten sam elemental pełni obie role. By to osiągnąć, elemental ma infuzję z Fazy Daemonica. Czyli podczas burzy, wykorzystywane są pewne komponenty i otwierany jest portal do Daemoniki by infuzować elementale energią tamtej fazy. By przywołać elementale i ich odbicia z tamtej fazy. Ale ta burza była większa i część zabezpieczeń była częściowo niesprawna. Wiedział o tym, ale deadliny po prostu były za ciasne i zaryzykował... i dlatego musi wracać. Musi wracać jak najszybciej. Musi skończyć to co zaczął - inaczej firma padnie.

Paulina zauważyła, że awian jest smutny - jak to dojedzie do Węzła, to go Skazi. Filip powiedział, że to tylko kwestia tego że ekrany ochronne zostały wyczerpane podczas burzy. To są elementale zasilane Fazą Daemonica. To są odbicia, nie żywe istoty. Paulina i Maria przycisnęły - skąd to wie? Nie wie. Zakłada tak - nie jest nawet pewien, że otwiera portal na Fazę Daemonica. Niby wszystko pasuje do profilu, ale nie wie na pewno, bo burza zaciemnia mu obraz. Paulina facepalmowała - Filip nie wie co robi. Dosłownie, "nie wie". Myśli, że wie, ale nie wie. Nie chodzi o konsekwencje, chodzi o obserwacje. Jest ślepy.

Filip i Szymon odlecieli. A Dracena podeszła do Pauliny - czy ten Filip niewoli te istoty? Paulina powiedziała, że tak... ale wierzy (on), że nie są świadome. Dracena zauważyła, że nie ma to dla niej znaczenia, torturowanie ryb też jest nieakceptowalne. Paulina zauważyła, że Filip się martwi a jedynie problemy biznesowe go blokują. On nie chce. A Maria powiedziała Paulinie, że ona się przejedzie i rozejrzy po okolicy Wieży Wichrów. Jeśli coś się dzieje, najpewniej coś się działo. Jeśli coś się działo... najpewniej ona się dowie co i jak.

Paulina się PONOWNIE obudziła w okolicach południa. Dracena nadal śpi, snem regenerującej się wiły.

Czyli gdzieś dookoła znajduje się potencjalny elemental burzy i gniewu, który jest ranny i przez to niestabilny. W sumie - Paulina wpadła na dość ciekawy pomysł. Jeśli on krwawi energią, powinno dać się go znaleźć. Bo krwawi energią. Bo energia jest możliwa do wykrycia. A tu w gabinecie Pauliny jest sporo energii rezydualnej elementala po otrzymaniu strasznego ciosu od Draceny.

Paulina zadzwoniła po wsparcie. W takiej sytuacji potrzebny jej Kajetan. Powiedziała mu, że to ważne i pilne. Wyjaśniła, że może mają elemental gniewu na wolności... czy coś tego typu... i zabrała się za szukanie.

Elemental jest Zamaskowany, Odległy. Paulina niweluje Zamaskowanie bezpośrednim powiązaniem z energią. Niestety, zajęło jej to więcej czasu niż miała nadzieję - ale udało jej się. Elemental został wykryty, trafiła po śladach elementala do Klubu "Panienka w Koralach" w Mżawiorze. To jest ta sama miejscowość, gdzie znajduje się portalisko. Gorzej - Paulina wykrywa słabnące sygnały życia z klubu a elemental zmierza w kierunku portaliska.

Paulina przeklęła. Ona musi lecieć do "Panienki w Koralach". Musi ratować życia. Kajetan leci zatrzymać elementala. Paulina rozpaczliwie wezwała jako wsparcie... Filipa. Paulina od razu ogarnęła co się stało - grupa ludzi leży na ziemi, kilkanaście. Oni są posprzężani z elektryką tutaj; elemental jakoś połączył prąd i ludzkie ciała by się zasilać. Zgodnie z obserwacjami Pauliny, będzie miał więcej energii niż gdy poraz pierwszy spotkał się z Draceną.

By odpiąć elementala od ludzkich źródeł energii: Sprzężeni, Beyond Help, Mental Loop, Forced Cyborgisation. Paulina się skupiła na zadaniu: Beyond Help niweluje magią. Forced Cyborgisation rozwiąże, bo pracowała nad Draceną. Sprzężenie rozbije katalizą. Mental loop magią mentalną. Jednym zaklęciem jest w stanie odciąć ich od elementala i pomóc Kajetanowi w walce. Udało jej się; Kajetan ma łatwiej.

Drugie zaklęcie ma ich ustabilizować przy życiu. Tu ma do czynienia z DODATKOWYMI aspektami: Otwarta Energia, Mind-Body-Energy meld. SS. Paulina musiała niestety zająć się tymi nieszczęśnikami tutaj. Kajetan i kataliści z portaliska dali sobie radę z rozłożeniem i zniszczeniem elementala. Elemental dał radę rozpocząć otwieranie portalu, ale nie dał rady zakończyć. Rozsypał się energetycznie... Kajetan dał Paulinie znać, że skończył zanim Paulina skończyła leczyć ludzi.

Wieczorem, w domu Pauliny. Kajetan i Paulina (Filip się wycofał jak tylko było już po wszystkim).

Kajetan powiedział Paulinie, że elemental nie atakował. Owszem, odpierał ataki i unikał, ale przede wszystkim parł w kierunku na portal. Zapytany, czy elemental był świadomy, Kajetan odparł, że nie da się określić. Parł do przodu jak ktoś z nieskończoną desperacją... lub jak coś silnie zaprogramowanego. Po odebraniu przez Paulinę energii, elemental nie był w stanie już utrzymać otwartego portalu. Po ataku katalistów i Kajetana, nie był w stanie też go nawet otworzyć. Ale próbował do końca.

Po tym, jak Kajetan nacisnął na Paulinę, ta powiedziała - nie chce zaszkodzić Filipowi, ale nie zgadza się z tym, co Filip robi. Wyjaśniła mu swoje podejrzenia i posiadane dane. Kajetan powiedział, że trzeba zamknąć to, co on tam otworzył. To zbyt niebezpieczne. Paulina powiedziała, że nie chce oddawać go ani Tamarze ani Sądecznemu. Kajetan powiedział, że już jest tyci za późno. Sądeczny się dowie, bo jego siły monitorują portalisko. Filip zlekceważył sprawę...

## Wpływ na świat:

JAK:

1. Każdy gracz wybiera 1 element Historii - konflikt / okoliczność, która miała Znaczenie i ma zostać jako zmiana po akcji. MG wybiera tyle, ile jest graczy.
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

1. Kić: Awian Filipa jest smutny i wystraszony.
1. Żółw: Elementale ściągnięte przez Wieżę Wichrów rozpaczliwie próbują otworzyć portale.

JAK:

* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

MG: 18

* Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
* Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
* Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
* Czy Wieża Wichrów przetrwa ekonomiczna w rękach Filipa, czy trafi do Sądecznego?
* Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
* Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Daemonica-elementalna.
* Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten konkretny klub? Co było z tym miejscem lub ludźmi?

Kić: 14

* Filipowi uda się utrzymać firmę JAKOŚ.
* Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
* Działania Pauliny wobec ludzi w Panience w Koralach były skuteczne ORAZ zostały zauważone i docenione przez wszystkich
* Działania z elementalem na portalisku zyskały Kajetanowi uznanie wszystkich stron
* Jakiego typu viciniusem stanie się ten awian?
* Paulina dostaje taniego awiana tymczasowego jako transport, zabezpieczony jej przez Sądecznego - starego modelu (jako karetka)

# Progresja

* Paulina Tarczyńska: dostaje awiana starego typu jako karetkę; zabezpieczony dla niej przez Roberta Sądecznego
* Paulina Tarczyńska: za szybkie i skuteczne działanie w klubie Panienka w Koralach została doceniona przez mafię i Świecę
* Kajetan Weiner: za skuteczne usunięcie elementala na portalisku tym co miał (i dowodzenie) został doceniony przez mafię i Świecę

# Streszczenie

Do Pauliny przybył bardzo ranny człowiek. Z trudem Paulina go postawiła; sprawa stała się w Wieży Wichrów. Tam Paulina spotkała starego znajomego, Filipa, pracującego nad elementalami do awianów i nie tylko. Jeden elemental wyrwał się na wolność. Paulina ma coraz większe podejrzenia, że te elementale nie są elementalami a czymś innym; fakt, że ten wyrwany próbował otworzyć portal gdzieś zanim Kajetan go nie zniszczył jest jedynie silną przesłanką. Filip ma problemy z Sądecznym, bo jego biznes jest niebezpieczny dla otoczenia...

# Zasługi

* mag: Paulina Tarczyńska, przede wszystkim faktycznie leczyła - Szymona, grupę ludzi w klubie, Dracenę po porażeniu... wezwała Kajetana i opanowała elementala. Martwi się biznesem Filipa na kilku poziomach.
* czł: Maria Newa, rozpracowała dla Pauliny Filipa i wyciągnęła z niego to, co Filip chciał ukryć. Pomogła Dracenie zwalczyć elementala, jakkolwiek smutno to nie brzmi.
* mag: Dracena Diakon, odparła atak elementala w gabinecie Pauliny, kończąc ciężko ranna przez wrażliwość na prąd. Paulina ją wyleczyła. Dracena ma kosę z Filipem; uważa elementale za świadome.
* czł: Szymon Łokciak, nieszczęśnik który miał zakazane systemy trzymania przy życiu i dlatego przeżył bycie zmasakrowanym przez elementala. Wyleczony przez Paulinę.
* mag: Filip Keramiusz, przedsiębiorca w rozpaczy uciekający się do potencjalnie niebezpiecznych i nieetycznych działań z elementalami. Gonią go kontrakty. Chce dobrze, ale musi szybko...
* mag: Kajetan Weiner, terminus ostatniej szansy Pauliny. Wezwany do dramatycznej obrony portaliska przed dopakowanym elementalem; nie tylko wygrał ale i zapunktował. I miał cenne obserwacje.


# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, zaatakowany przez dziwnego elementala z Wieży Wichrów; elemental został odparty przez Dracenę i sam gabinet został lekko uszkodzony.
                1. Mżawiór
                    1. Klub Panienka W Koralach, który elemental burzowego gniewu z Wieży Wichrów spróbował przekształcić w zasilanie dla siebie - 
                    1. Portalisko Pustulskie, 
            1. Powiat Ciemnowężny
                1. Burzowiec, nieduże miasteczko ze sławną radiostacją i słynące z częstych burz
                    1. Wieża Wichrów, struktura do przechwytywania i konstrukcji żywiołaków powietrza na sprzedaż; 


# Czas

* Opóźnienie: 0
* Dni: 2

# Wątki kampanii

* Powrót Srebrnej Świecy
    * CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    * Sylwester został zesłany tu przez Newerję
    * Sylwester: przeprowadził udaną akcję zdobycia transportu Dukata
    * Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    * fakt: Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    * fakt: Tamara robi się bardzo podejrzliwa wobec Myszeczki
    * Myszeczka staje po stronie rezydenta Świecy
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Oktawia w power suit ma dostęp do tego co wie power suit. Wie to, co Tamara przekazała.
    * Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    * Sylwester opłaca nowego przedsiębiorcę mającego założyć konkurencyjną małą magitrownię. By uniezależnić się od gróźb. Aha, czyn nieuczciwej konkurencji.
* Działania Roberta Sądecznego
    * Robert dostaje haracz od "Nowej Melodii"
    * rezydent polityczny Dukata u Myszeczki
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i magitrownia nie miałoby tragedii
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał
    * fakt: Sądeczny dostarczy maga, który zajmie się glukszwajnem
    * Sądeczny nasyła Zofię na szpiegowanie Tamary
* Katia Grajek
    * To Hektor pomagał w budowaniu eliksirów dla Katii
    * Aneta wróciła wtedy z Sądecznym ORAZ wie, co tam się stało
* Dracena, wracająca do świata ludzi
    * CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    * CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    * CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    * Fakt: Dracenę wylano z "Nowej Melodii" za radykalizm
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana
* Gabriela Dukata droga do ostatecznej medycyny dla dziecka
    * CLAIM: Dukat lubi Paulinę (Kić)
    * Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    * Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
    * Dukat zatrzymał działania Sądecznego ORAZ wstrzymało to niszczenie Legionu
* Osobliwości lokalne
    * CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    * CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    * Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
* Kluczowa rola glukszwajnów wobec efemerydy Senesgradzkiej
    * CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    * CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    * W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
    * Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera
    * Natalia ma eksperta od glukszwajnów; człowieka kiedyś pracującego dla Myszeczki
* Wolność Eweliny Bankierz
    * CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką"
* Echo starych technologii Weinerów
    * fakt: Harvester nie ma dość energii by się obudzić
    * Natalia pozyskała bezpiecznie kolejną część artefaktu
* Oktawia, echo Oliwii
    * CLAIM: Oktawia dorośnie i zniknie. (Kić)
    * Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
