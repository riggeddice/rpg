---
layout: inwazja-konspekt
title:  "Wygraliśmy wojnę... prawda?"
campaign: powrot-karradraela
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170215 - To się nazywa "łupy wojenne"? (HB, SD)](170215-to-sie-nazywa-lupy-wojenne.html)

### Chronologiczna

* [170215 - To się nazywa "łupy wojenne"? (HB, SD)](170215-to-sie-nazywa-lupy-wojenne.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Dzień 1:

* Andreo, operacja przyzwania Karradraela została zakończona z powodzeniem - Jankowski, jak zawsze, skuteczny - Abelard Maus jest nowym seirasem.
* Bardzo dobrze. - Andrea pozwoliła sobie na westchnięcie ulgi... - Musimy mieć kogoś, kto będzie go kontrolować. 
* Zatroszczę się o to. Do jutra i tak Mausowie są niestabilni przez przepięcia Karradraela. - Jankowski - Działania Karradraela się rozpadają; Abelard nie ma mocy kontroli Karradraela.
* W sensie?
* Dopiero się sprzęgają; a umysł Mausa jest za słaby w porównaniu z pełną mocy Karradraela. - Jankowski, spokojnie - Implikacja prosta: cokolwiek Karradrael robił, jest poza kontrolą.
* A jak się sprzęgnie?
* To będzie można działać. W czym... niektóre rzeczy całkowicie utraci. - Jankowski - To wyczyści Świeca.

Więc Andrea została jako Lady Terminus... nie musi się już ukrywać. I komunikat od Anety Rainer.

* Lady Terminus, jak rozumiem, coś się udało? Siły Karradraela tracą koordynację; idą w rozsypkę - Aneta, spokojnie i z zadowoleniem
* Bardzo dobrze.- Andrea z uśmiechem
* W ciągu 48 godzin odzyskam kontrolę nad Kopalinem. - Aneta
* Jaki jest stan Kompleksu Centralnego?
* Kompleks... walczy. Sam ze sobą. Nieoczekiwany efekt uboczny - uruchomiłyśmy kilka Overlordów Hipernetowych.
* Jak źle jest?
* Tylko dzięki temu Kompleks jeszcze stoi. Próbują zapewnić działanie kluczowych systemów by się zwalczać... - Aneta, wzdychając
* A co im przeszkadza? - zdziwiona Andrea
* Na przykład... jednemu nie podoba się to, że drugi ma... stopy. - Aneta, z powagą
* Aha... - Andrea, rozumiejąc już sytuację.
* Mam tu maga, który jest dobrym specjalistą od hipernetu. Jednak jest problem. To Maus. - Aneta.
* W tej chwili jest w raczej złym stanie? - Andrea
* Nic, czego lekarze Millennium nie naprawią - Aneta - a Kompleks jest dla nas ważny.
* Daj mi 24 godziny... a będziesz miała swojego Mausa w pełni współpracującego. Z rozkazu Karradraela.
* Tak jest.

Andrea powiedziała Jankowskiemu, że ma załatwić współpracę Rufusa Mausa z Anetą Rainer asap. Jankowski ACKnął. I kolejna wiadomość, tym razem od Szczepana Sowińskiego - Lorda Terminusa Trocina. 

* Odzyskaliśmy z Wami połączenie hipernetowe. Zakłócania, demony hipernetowe i inne takie padły - Szczepan do Andrei
* Doskonale. To jedynie potwierdza, że operacja była skuteczna. - Andrea z ulgą - Ile wiesz?
* Wszystko. Rezydent Wydziału mnie informuje. - Szczepan - Nie wiem czy wiesz; mamy... anomalie magiczne na terenie całej Polski
* Jak poważne? - Andrea, zatroskana
* Różnie. Świeca opanuje sprawę... ale nie w tym miesiącu - Szczepan, smętnie - Tak jakby magowie stracili kontrolę nad bardzo złożonymi rytuałami.
* To dopiero się zaczyna, będzie się rozkładać w czasie. Zajmę się tym na naszym terenie i spróbuję trochę Was osłonić... - Szczepan
* Weź poprawkę na jedną rzecz. Te anomalie to Mausowie. - Andrea - Wyciągnij ich z tego i nie daj zabić.
* Nie tylko Mausowie. Magia Krwi, przedradomskie... - Szczepan
* Jesteśmy w oku cyklonu... - Andrea

Andrea zdecydowała się zorganizować spotkanie hipernetowe Lordów Terminusów z różnego terenu. Niestety, nie udało się. Bez Świecy Daemonica po prostu za mało działa, zwłaszcza, że Kompleksy, no... ucierpiały. Umówili się na "pocztę pantoflową". Niewiele więcej można zrobić...

* Aha, Andreo... Ty masz na swoim terenie legendarny czołg Zajcewów czy coś takiego? Który jest wpięty w hipernet? - Szczepan, zaskoczony
* Czołg tak, ale hipernet... nie wiadomo mi nic o... - Andrea, zauważając na hipernecie słaby sygnał Śledzika - Nie było go tu.
* Anomalia? Kolejna? - zaskoczony Szczepan
* Diabli wiedzą... nie odważę się nawiązać kontaktu bez speca od hipernetu... - zrezygnowana Andrea
* Moi spece mówią, że to ma sygnaturę Overlorda - Szczepan
* Ten czołg walczył w miejscu starcia Karradraela i Arazille. Cholera wie, co się stało... ale walczył po naszej stronie.
* Chciałem Cię ostrzec. Moi Zajcewowie już są podnieceni... - Szczepan, smętnie
* Poczekają w kolejce. Po moich Zajcewach - Andrea, nie mniej smętnie

Czyli Aneta zrobi swoje, Mausy będą kontrolowane, Szczepan pomoże z Trocina... w sumie, wszystko się ŚLICZNIE układa. To jest, prawie. Śledzik. Anomalie. Overlordy. Myślin... aha, Dalia złożyła raport - Laurena i Katalina są w dobrej formie. Stabilne i zregenerowane.

Andrea znowu skontaktowała się z Anetą - poprosiła o oddanie swoich Zajcewów. Aneta się zgodziła, będą rano (Andrea chce rano wejść do Myślina). Ale wpierw, Aneta przedstawiła Andrei pewien mały problem. Millennium "nie jest łagodne" wobec Mausów. To oznacza, że może populacja Mausów się zmniejszyć. A Aneta nie ma możliwości negocjacji z Millennium. Za niska ranga. Trzeba porozmawiać z Amandą...

Andrea zrobiła edykt. Atak na Mausów ze Świecy jest jak atak na magów Świecy. Oraz do magów Świecy: wszelki nieuzasadniony atak na Mausów będzie traktowany z ogromną surowością. Świeca ma się zachowywać... i po wszystkim, Andrea poprosiła Szczepana o przekazanie tego dalej i przekazanie tego na ten teren (jego Kompleks działa ;p).

No i, Melodia. Powiedziała, zapytana, że Baran i Stein jej już nie potrzebują. No i ma wymyśleć sposób jak skontaktować się z Amandą Diakon...

Andrea zgromadziła swoich magów w jedno miejsce. Powiedziała, że siły Karradraela poszły w rozsypkę, sam Karradrael wkrótce będzie pod ich kontrolą i teraz trzeba to wszystko posprzątać. Mieszko wziął Andreę na stronę. Powiedział jej, że można oczekiwać silnych ograniczeń praw Mausów. Oni zrobili naprawdę złe rzeczy. Andrea powiedziała, że to jest poza jej jurysdykcją; ona po prostu nie chce się mścić na Mausach. Nie chce być "nie lepsza niż Karradrael".

Andrea niechętnie poprosiła Anetę, by ta załatwiła u Amandy komunikację hipernetową. Cóż tam Overlordy... na cywilnym połączeniu zawsze jakiś komunikat.

I faktycznie - Amanda się połączyła z Andreą. Andrea dodała do konwersacji Mieszka w tle, tak, by Amanda nie wiedziała. Jej hipernet ;p.

Amanda powiedziała, że nie zamierza atakować magów Świecy. Ale... poza-Świecowe Mausy? Kim oni są dla Andrei? Andrea powiedziała, że to jest okazja by nie zniszczyć Mausów tylko ich od siebie uzależnić; i tak jak Andrea chce to zrobić z ramienia Świecy tak zaprasza Amandę, by ta zrobiła to z ramienia Millennium. A KADEMu na tym terenie nie ma - a jak wróci? KADEM nie będzie miał przysługi. A Millennium tak...

* Milena. Jedna z nas. Zawiodła ją Świeca, zranili ją Mausowie. Byłaś przy tym, uratowałaś jej życie. - Amanda, do Andrei
* Rafael ma kilka pomysłów jak można pokonać dominantę krwi Mausów. Potrzebuje jednak magów, by na nich eksperymentować - Amanda, wzruszając ramionami

Andrea zadrżała. To nie jest "nieludzkie", to typowy Rafael + Amanda... Andrea spróbowała ją przekonać. Nie może tego zrobić. Amanda powiedziała, że nikt nie zauważy zniknięcia kilku Mausów. Andrea odbiła, że tak - Karradrael. Udało się Andrei - kombinacja języka korzyści oraz oportunizmu Amandy wystarczyły, by Amanda skupiła się na pomaganiu Mausom a nie ich eksterminacji i przekazywaniu Rafaelowi...

Andrea wie, że Amandy się wszyscy boją i nikt jej się nie postawi (poza Rafaelem, ale on nie ma mocy). Więc przynajmniej ten problem z głowy...

ŚLEDZIK! Kirył!

Kirył powiedział, że nic nie wie o tym, żeby Śledzik miał połączenie z hipernetem. Andrea powiedziała, że ma. Kirył, że nie powinien. Andrea skontaktowała się z Lidią; dostała jednoznaczną odpowiedź: NIE, Overlord nie powinien się sam pojawić. TAK, istnieje możliwość, że ingerencja bogów (Karradrael, Arazille) coś zrobiła - było tam sporo obwodów hipernetowych od martwych magów. A sam Śledzik nie jest typowym czołgiem...

Dla Andrei ważnym jest, by Śledzik z dostępem do hipernetu NIE poruszał się po jej terenie. Kirył ma przejąć kontrolę nad Śledzikiem. I Andrea ostrzegła Kiryła z całego serca - nigdy nie widziała, by coś bez świadomości połączyło się z hipernetem... Śledzik najpewniej ma (lub uzyskał) jakąś formę inteligencji i świadomości.

Dzień 2:

Andrea robi swoje i dostała wiadomość od Mieszka - musi natychmiast przyjść. Przyszli Zajcewowie dowodzeni przez Rodiona... i mają w klatce młodego Mausa. Na oko nieletniego. Andrea wzięła Rodiona na stronę i zaczęła go opieprzać - czemu? Rodion powiedział, że dostali Mausa od Amandy jako prezent, więc dadzą go w prezencie Andrei. To Baltazar. Andrea się zachmurzyła. Amanda wredną suką jest...

Andrea ma zabiedzonego, przerażonego Mausa w swoich kwaterach. Zamknęła go w piwnicy i powiedziała, by stąd NIE wychodził i się NIE przedstawiał. I niech niczego nie rusza. Ale nie siedzi już w klatce... A Rodion dostał Tatianę i Melodię - mają opracować sposób, dzięki któremu sprawią, że Zajcewowie uznają za NIEMODNE znęcanie się nad Mausami.

Oki. Tania i Melodia zajmą się legendami. Kirył ma pilnować młodego Mausa. Baran ma zostać. Andrea, Zajcewowie, Mieszko, Julian, Dalia idą na akcję zobaczyć Myślin. Jankowski powiedział Andrei, że Myślin powinien być bezpieczny...

Myślin.

Sam Myślin wygląda jak super-forteca. Lord Jonatan uszkodził ziemię; wyraźnie próbował wstać. Ogólnie, w Myślinie wszystko wygląda jak po chorobach... wszędzie punkty szpitalne. Andreę ciepło przywitał blady mag, wyglądający jak siedem nieszczęść. Andrea go poznała. Patryk Weiner. On się przedstawił ze smutkiem - już nie Weiner. Patryk Maus. Karradrael ich wszystkich przekształcił. Potem Spustoszył dla zwiększenia efektywności i Spustoszył wszystkich ludzi. Na szczęście, zanim Abelard się z nim połączył, Karradrael zdążył "odrzucić" komponenty Spustoszenia i przełączyć Lorda Jonatana i system defensywny w tryb "incapacitate" a nie "kill". Inaczej to wszystko to byłoby martwe miasto.

Dalia ma głęboki horror. Andrea kazała jej się wziąć w garść i pomóc. Kazała też Mieszkowi pomóc Dalii i iść z nią. Dalia się wyrwała; trzeba pomóc ludziom, magom... wszystkiemu. A Mieszko będzie z nią łaził, wspierał i w ogóle.

Andrea spotkała się z Janem Weinerem... teraz już Janem Mausem. Ten powiedział, że Karradrael im to zrobił; Spustoszył, zMausił i dostosował. Jan nie dał rady obronić Lorda Jonatana - nie przed niewidzialną Karoliną Maus i oddziałami Spustoszenia...

Jan powiedział, że wie (od Karradraela), że mają Baltazara Mausa. Zaproponował Andrei, by Baltazara przenieść do Myślina. Bezpieczniej dla chłopaka a i tak nie ucieknie.

Andreę zadumał też sposób w jaki Jan przedstawił Karradraela - jako angelnet. System wspierający Mausów, kształtujący ich otoczenie, dbający by im się nie stała krzywda, zarówno overridujący ich zachowania jak i będący ich otoczeniem...

Baltazar trafił do Myślina.

A Andrea odbudowała hipernetowy przekaźnik w Myślinie; ma stały kontakt. I bardzo próbuje nie myśleć o Julii Weiner.

# Progresja

# Streszczenie

Aneta Rainer odzyskuje kontrolę nad Kopalinem. Andrea wydała edykt zakazujący krzywdzić Mausów; też są ofiarami tego wszystkiego. Szczepan Sowiński ostrzegł Andreę przed Overlordami Hipernetowymi i innymi anomaliami. Okazało się, że Śledzik totalnie wyrwał się spod kontroli. Cóż - Andrea odwiedziła Myślin i dowiedziała się, że WSZYSCY magowie tam zostali przekształceni w Mausów a sam Myślin - w fortecę. Baltazar Maus (niechciany dar od Amandy) trafił właśnie tam, pod opiekę Jana "Weinera". Ogólnie, Andrei udało się nie dopuścić do znęcania się nad Mausami - co w tej sytuacji to naprawdę sporo...

# Zasługi

* mag: Andrea Wilgacz, koordynująca kilkanaście wątków i próbująca uniemożliwić pogromy Mausów. Dotarła do Myślina... i wzbudziło to w niej głęboki żal.
* mag: Rudolf Jankowski, sterujący Abelardem. Znajdzie dla niego jakąś agentkę, która sobie go owinie dookoła palca. 
* mag: Abelard Maus, konstytuujący połączenie z Karadraelem, służący pomocą... ogólnie, właściwy (z perspektywy Andrei) Lord Maus na te czasy...
* mag: Aneta Rainer, odzyskująca Kompleks Centralny i informująca Andreę o problemach z kontrolowaniem Amandy Diakon. Nic dziwnego...
* mag: Rufus Maus, ojciec Baltazara (przyszywany), ekspert od hipernetu. Sponiewierany, posłuszny Anecie na polecenie Karradraela.
* mag: Amanda Diakon, która jest rozdarta między próbą porywania i badania (lub się znęcania) Mausów a próbą zyskania w tych trudnych czasach Mausich sojuszników. Wybrała drugie pod wpływem Andrei.
* mag: Szczepan Sowiński, wspierający Andreę i Kopalin siłami z Trocina; ostrzegł Andreę przed anomaliami i dziwnymi działaniami magicznymi. Pomocny. Miła odmiana.
* vic: Stalowy Śledzik Żarłacz, Overlord Hipernetowy (???). Coś zrobili mu bogowie. Podwójnie niebezpieczny i nieprzewidywalny. Po prostu super.
* mag: Mieszko Bankierz, w pełni stoi za Andreą. Powstrzymuje swój Mausożerny apetyt na chwałę Świecy i Andrei - ona uratowała jego partnerki.
* mag: Kirył Sjeld, najbardziej neutralny i przez to pewny dla Andrei mag w okolicy. Wraz z Anastazją eskortował Baltazara Mausa do Myślina.
* mag: Rodion Zajcew, dostał od Amandy Mausa w prezencie dla Andrei, to przyniósł jej go w klatce... będzie próbował zapobiec atakom na Mausów przez Zajcewów.
* mag: Baltazar Maus, złapany przez Amandę, obity przez Zajcewów, przekazany już-nie-Weinerom z Myślina przez Andreę. Ogólnie, ma przechlapane.
* mag: Patryk Maus, kiedyś: Weiner. Dowodzi taktycznie odbudową i stabilizacją Myślina po wewnętrznych starciach Mausów ze Spustoszeniem.
* mag: Jan Weiner, już: Maus i już nie Weiner. Dowodzi Mausami (wszystkimi) w Myślinie. Zachował dobre serce i chce szczerze współpracować z Andreą.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin, w którym Aneta Rainer powoli odzyskuje kontrolę.
                1. Wtorek Śląski, w którym dowodzi Andrea. Jeszcze.
            1. Powiat Myśliński
                1. Myślin, przebudowany przez Karradraela w fortecę
                    1. Centrum
                        1. Hotel "Luksus", centrala dowodzenia Jana "Weinera". Centrum kontaktu z Karradraelem. Już nie hotel - już technomantyczna super-struktura.

# Czas

* Opóźnienie: 1
* Dni: 2