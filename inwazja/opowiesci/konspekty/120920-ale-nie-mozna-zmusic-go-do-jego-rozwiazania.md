---
layout: inwazja-konspekt
title:  "Ale nie można zmusić go do jego rozwiązania... "
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [120918 - Można doprowadzić maga do problemu... (An)](120918-mozna-doprowadzic-maga-do-problemu.html)

### Chronologiczna

* [120918 - Można doprowadzić maga do problemu... (An)](120918-mozna-doprowadzic-maga-do-problemu.html)

## Misja właściwa:

1. Przypomniawszy sobie wszystko, Kasia potrzebowała chwili odreagowania. Przez jeden dzień zajmowała się tylko i wyłącznie malowaniem. Później jednak uznała, że tak być nie może - skoro już tyle zepsuł, terminus musi za to zapłacić - niech przynajmniej odpracuje swoje.
2. Następnego dnia od rana Andromeda odwiedza lokalną bibliotekę i urząd w poszukiwaniu informacji o księdzu i całej tej sprawie. Przed wyjściem odwiedza ją lokalny urwis, którego kilka dni wcześniej Samira poprosiła o wyszukiwanie dziwnych informacji i zdarzeń. 
Z rzeczy, które zwróciły uwagę Andromedy: 
- dzień otwarcia galerii nie był datą śmierci księdza
- chłopcy, którzy chodzą do kościoła robią się bardziej święci niż sam papież
- matka samobójcy to wiedźma, która warzyła dziwne napary z krwi (choć równie dobrze mógł to być sok malinowy)
- dzieci mają kryjówkę pod ziemią, do której dorośli nie mogą wejść. Niedawno w tej kryjówce pojawił się (a może dopiero go zauważyli) skarb hitlerowców. Czemu hitlerowców? A czyj? Skarbem są lustra.
- Andromeda w ramach przekupstwa zgodziła się szybko naszkicować dla chłopca rysunek kryjówki zgodnie z jego opisem.
Powiedziała mu tylko, aby dobrze go schował, bo dorośli mogą mu go zabrać, a po nim odnaleźć kryjówkę i też odebrać ją dzieciom. 
Chłopiec uznał, że zaniesie rysunek zdradzający lokalizację kryjówki do samej kryjówki. To będzie taka ich "mapa skarbów"
3. W miejskiej bibliotece Andromeda przejrzała stare gazety, w których znalazła:
- dzień otwarcia galerii to rocznica śmierci samobójcy, nie księdza
- zapisy w papierach potwierdzają oficjalną wersję - kierowca nie był trzeźwy. O księdzu nic nie wiadomo.
- samobójca nie został pochowany na poświęconej ziemi - takiego pochówku odmówił aktualny wikary
W urzędzie miejskim Andromeda szukała informacji o kryjówce dzieci. Jest to stara, niszczejąca posiadłość, której nikt nie pilnuje. O wejściu do podziemi nie było nic w papierach.
4. Andromeda chce się pozbyć terminusa - jest pewna, że za nią łazi. Dlatego pakuje do auta sztalugi i jedzie do pobliskiego miasta, gdzie się rozkłada. Jest to coś, co często robi. Kiedy raz się rozłoży, zostaje w danym miejscu i maluje co najmniej kilka godzin. Andromeda liczy, że terminus zostawi ją na rzecz Samiry. Upewniwszy się, że jest sama, zakupuje dwie garści srebrnego drobiazgu, pilnując, by nie wmieszały się tam symbole kojarzone ze złem czy choćby mrokiem.
5. Tymczasem terminus faktycznie zajął się Samirą. Miał z nią większy problem, bo Samira wykazała wyższy próg załamania. Chciał odwiedzić cmentarz, ale uznał, że w aktualnej sytuacji ważniejsze jest zrobienie czegoś z Samirą.
6. Andromeda udaje się do lokalnego księdza prosząc o poświęcenie dwóch krzyżyków.
Plan jest prosty. Andromeda nie wie, o co chodzi, ale nie podoba się jej ten pomnik księdza. Bierze proste narzędzia rzeźbiarskie i odrobinę wypełniacza, aby załatać dziurę w figurze. Zamierza nocą wydłubać w nim dziurę i osadzić poświęcony srebrny krzyżyk tak głęboko, jak tylko będzie w stanie. Jest to lepsze niż nic.
7. Andromeda zapewnia srebru tak dobrą izolację, jak jest to możliwe bez użycia lapisu. (ciężko o lapisowany woreczek ad hoc)
Wraca do domu, odczekuje do zmroku i udaje się na cmentarz, wychodząc z domu w taki sposób, aby na pewno zauważył ją terminus.
Terminus ją faktycznie zauważył, ale za późno. Nie zdążył jej zatrzymać zanim poszła na cmentarz, więc przygotował zaklęcia maskujące i całkowicie nieświadomie wszedł naładowany energią magiczną prosto w pole widzenia mandragory, która zobaczyła bardzo apetyczny kąsek.
8. Mandragora podniosła poziom skażenia na cmentarzu. Córka grabarza zareagowała na skażenie i wyszła na cmentarz idąc za impulsami wywołanymi przez mandragorę. Ojciec, otępiony przez "mgłę" (manifestacja skażenia i pola wysysającego magię oraz dezorientującego), wybiegł za córką.
9. Andromeda obserwowała tą dwójkę, wyczekując dobrego momentu, by podejść do pomnika. Zauważyła wpływ mgły i odkryła, że jeśli weźmie w rękę kawałeczek srebra, łatwiej jest jej skupić myśli. W tym momencie Andromeda zaczyna rozważać wycofanie się.
10. Terminus miał pecha. Ta konkretna mandragora była odmianą szczególnie niebezpieczną dla magów, silnie reagującą na magię i bardzo receptywną. Mgła, bardzo dezorientowała Andromedę i sprawiła, że wszyscy ludzie na cmentarzu poza Kasią błąkali się bez celu i nie pamiętali co robią, zasilając mandragorę (Kasia też zasilała mandragorę, ale wpływ na nią był słabszy). Jeśli chodzi o terminusa, to jego moc stawała się coraz słabsza i zaczynał być coraz bardziej zatruty magicznie. Aura mentalna sprawiała jednak, że nie zdawał sobie z tego sprawy.
Chory i osłabiony, rzucił zaklęcie mające na celu zdezorientowanie Andromedy. Na początku zaklęcie zadziałało jak należy, ale bardzo szybko zostało wyssane przez "mgłę". Zaklęcie to jednak sprawiło, że Andromeda została silnie napromieniowana magicznie i próg jej skażenia wzrósł dramatycznie. Naturalny efekt dezorientacyjny mgły jedynie wzmocnił jej zagubienie do momentu, aż się poparzyła srebrem - przez czysty przypadek. Wówczas świadomie chwyciła kawałek srebra, aby ból ją otrzeźwiał.
11. Terminus przygotował się do rzucenia jeszcze potężniejszego zaklęcia, co bardzo nadwątliło jego osłony. Wtedy mandragora krzyknęła. Upiorny wrzask bardzo silnie uderzył w Andromedę, powodując koszmarny ból głowy i "zranił jej duszę". Córka grabarza jako osoba z już rozerwaną duszą, zupełnie nie poczuła wrzasku. Grabarz przewrócił się i porządnie potłukł.
Terminus oberwał najmocniej. Półprzytomny padł na ziemię. Jego dusza uległa poważnym obrażeniom. Zapomniał o wszystkim i pogrążył się w otchłani. 
12. Andromeda dostrzegła terminusa leżącego na ziemi i pobiegła do niego. Nie wiedziała, co to jest, ale na pewno było to magiczne, a srebro pozwalało jej utrzymać świadomość. Córka grabarza podeszła za blisko, więc Andromeda chwyciła srebro i mniej więcej połowę tego, co miała, sypnęła w jej stronę, bardzo ciężko ją raniąc. Zataczając się, Andromeda dostała się z trudem do terminusa, a mandragora wrzasnęła ponownie. W akcie desperacji, Andromeda upuściła jeden kawałek srebra na terminusa licząc, że to go otrzeźwi.
13. Srebro zadziałało. Terminus przestał szeptać o pięknej melodii i uruchomił artefakt ratunkowy. Reszta srebra potężnie oparzyła rękę Andromedy, która odruchowo upuściła wszystko. Amulet, bardzo poważnie osłabiony przez mandragorę oraz silnie napromieniowany srebrem, zadziałał, ale uległ zniszczeniu.
14. Andromeda po lekkiej ekwilibrystyce wydobyła z kieszeni latarkę, z której zamierzała skorzystać dopiero przy posągu i oświetliła miejsce, w którym się znalazła. Ręka ją bolała, ale wciąż była w szoku i pod wpływem adrenaliny. Rozpoznała miejsce, w którym była. Niemałą wskazówką był umieszczony na ścianie rysunek wykonany tego poranka jej własną ręką...
Terminus był w coraz gorszym stanie. Andromeda przetrząsnęła całe miejsce licząc na to, że dzieci poznosiły różne rzeczy mające im uprzyjemnić ich kryjówkę. Nie przeliczyła się. Znalazła kilka kocy, w różnym stanie zabrudzenia i trochę przekąsek.
Resztką energii umościła terminusowi legowisko i dowlokła go do niego. Następnie owinęła poparzoną rękę najlepiej, jak umiała i usiadła, robiąc co mogła, aby ogrzać wstrząsanego dreszczami chłopaka. 
15. Terminus majaczył. Andromeda wyciągnęła z niego historię jego życia... mocno nieskładną. Syn możnego rodu magicznego, Bankierzy, ze strony ojca. Jego ojciec stracił moc podczas Zaćmienia. Matka natomiast nie została nigdy zaakceptowana przez ród. Chłopak zdecydował się zostać terminusem, gdyż nie mógł konkurować z ojcem - badaczem i nauczycielem. Zgodził się go przyjąć w termin uczeń jego ojca, ale chłopak nie spełnia jego oczekiwań. I chłopak to czuje. Stara się, ale nigdy nie może zrobić czegoś dobrze w oczach mistrza. Ceną za przyjęcie do terminu jest rezygnacja z kontaktów z matką. Oni oboje się na to zgodzili. Matka może jedynie czasem przysłać mu nieco quark.
Innym tematem, o który Andromeda zapytała, były wydarzenia na cmentarzu. Dowiedziała się o mandragorze i poznała jej opis taki, jaki znał terminus. Dowiedziała się również, że z tym tworem najlepiej walczyć ogniem...
Powoli terminus zapadł w niespokojny sen, kołysany przez Andromedę, którą od snu powstrzymywał ból targający ręką. 
16. Noc dla żadnego z nich nie była spokojna. Skażenie ze strony mandragory było zbyt potężne. W końcu jednak terminus usnął w miarę spokojnie, by obudzić się po kilku godzinach. Andromeda w tym czasie trochę przyzwyczaiła się do bólu, a ponieważ nie poruszała ręką, ból nieco osłabł.
Kiedy terminus się ocknął, był słaby niczym dziecko. Andromeda napoiła go kolorową oranżadą (jedyne, co dzieciaki miały do picia w kryjówce) Dała mu też paluszki i inne śmieciowe żarcie, jakie dzieci naznosiły w charakterze zapasów.
17. Terminus, gdy odrobinę doszedł do siebie, pocisnął Andromedzie nieziemski kit o tym, że jest tajnym agentem, że ma dostęp do sprzętu działającego na promieniach gamma, który przez to źle reaguje ze srebrem. A tak w ogóle to chodzi o rosyjskie podwodne czołgi... Andromeda trochę przycisnęła, ale ostatecznie uznała, że na razie może udawać, że mu wierzy... W końcu chorym i wariatom się potakuje...
Mag ma wypalone kanały magiczne i jest święcie przekonany, że mu przejdzie do następnego dnia. Tak jednak nie będzie.
Terminus załamał się, gdy dowiedział się, że jego amulet ratunkowy uległ zniszczeniu... i jak.
Przeraził się też widokiem dłoni Andromedy. Rany od srebra nie dają się leczyć magicznie.
18. Andromeda uświadomiła terminusa, że to najpewniej dzieciaki zostawiły te wszystkie rzeczy, dzięki którym przetrwał... I że skoro tu coś zostawiły, to najpewniej tu wrócą... Nie ucieszył się.
Jednocześnie chłopak zdaje sobie sprawę z tego, że uratował go zwyczajny człowiek...
19. Terminus wyjątkowo zestresował się tym, że wkrótce w kryjówce mogą pojawić się dzieci. Próbował rzucić zaklęcie, aby uciec, ale nic z tego nie wyszło. Zrozpaczony, wpadł na to, że przecież właśnie tutaj zabunkrował ukradzione Samirze lustra. Powstał jednak problem, jak je ściągnąć. On jest słaby jak nowo narodzone kocię, Andromeda nie może używać dłoni...
20. Stanęło na tym, że terminus wejdzie Andromedzie na ramiona, by dosięgnąć luster. Niestety, pośliznął się (!) i spadł, ściągając to, po co sięgał. Andromeda, w akcie rozpaczy, rzuciła się ratować spadające zwierciadła...  Niestety, użyła do tego poparzonej ręki. Przynajmniej poświęcenie się opłaciło - zniszczone zostało tylko kilka sztuk. 
21. Oboje prawie zemdleli z bólu. Kiedy się pozbierali, mag uruchomił jedno z luster nakierowując je na pokój Andromedy (lustro na drzwiach szafy). 
Andromeda dała magowi środek przeciwbólowy, wzięła dawkę sobie, z grubsza opatrzyła rękę i oboje poszli spać.
22. Rano Andromeda zostawiła wciąż śpiącego maga w mieszkaniu i poszła do apteki (zostawiła mu notkę na stole, żeby wiedział, czemu jej nie ma). Ukrywając obrażenia ręki, kupiła środki na oparzenia, coś wzmacniającego i co jej polecił aptekarz.
23. Wróciwszy do domu, dała terminusowi kolejną dawkę środka przeciwbólowego, i opatrzyła mu rany... na ile to możliwe jedną ręką. Czarodziej próbował rzucić zaklęcie, ale mu nie wyszło. Po drugiej próbie, zorientował się, że jego kanały magiczne szybko nie wrócą do normy, co go bardzo zmartwiło.
24. Tego dnia oboje lizali rany.
25. Mag rozważał wezwanie wsparcia. Kiedy Andromeda zorientowała się, co chłopak chce zrobić, porządnie się wystraszyła. W końcu nie wiedziała, jak dokładnemu badaniu się potrafi oprzeć i czego się spodziewać po terminusach wezwanych do walki z mandragorą...
Szybkie oszacowanie ryzyka doprowadziło ją do prostego wniosku - musi powiedzieć magowi część prawdy... Choć w żadnym wypadku nie całą.
26. Powiedziała magowi, że miał paradoks i od tej pory ona pamięta wszystko to, co on próbował jej wymazać. Na dowód przytoczyła kawałek jego monologu do niej, który miał miejsce tuż przed wymazaniem jej pamięci. Chłopak się załamał. Niezależnie od okoliczności, jeżeli miał paradoks tego typu, to ona jest martwa, a jego kariera jest zrujnowana... Nie wiadomo, co paradoks tego typu mógł zrobić...
Z kariery jeszcze jakoś by się wybronił, ale nie dałby rady wybronić Kasi.
27. Jeden bardzo nieszczęśliwy terminus jest nieszczęśliwy. Jest tam rozwijająca się mandragora, a on nic nie może zrobić. Widząc go w takim stanie, Andromeda postanowiła wziąć sprawy w swoje ręce. 
Kazała magowi się zmierzyć i poszła kupić mu proste ubrania tak, aby nie wyróżniał się w tłumie - to, co miał na sobie, choć w miarę normalne nie nadawało się do tego, co chciała zrobić. Nie wyjaśniła magowi, na czym polega jej plan. Obawiała się, że mógłby protestować, w dodatku po prostu nie miała ochoty. W ramach przygotowań kupiła kanister i napełniła go benzyną...  Zarówno kanister jak i benzynę do niego Andromeda kupiła na dwóch różnych stacjach, obu oddalonych od miasta.
28. Andromeda znała większość barów w okolicy. Wybrała najbardziej odpowiedni do realizacji swojego planu - taki, do którego przychodził facet nieco za bardzo interesujący się okultyzmem... A jednocześnie lubiący czasem wypić.
29. Korzystając ze swoich umiejętności (praktyka jako barmanka się przydaje) spiła faceta i wmówiła mu, że zła istota zalęgła się w jednym miejscu - trzeba ją koniecznie zniszczyć, a wrażliwa jest tylko na ogień - koniecznie trzeba ją spalić. Dostarczyła facetowi kanister i nakierowała go na mandragorę.
30. Wręczyła magowi lornetkę i obserwowali, jak kwiat płonie...

# Zasługi

* czł: Kasia Nowak jako wybitna malarka z unikalną pamięcią, która ratuje terminusa przed mandragorą
* czł: Samira Diakon jako załamana dziewczyna, której próbuje pomóc terminus
* mag: August Bankierz jako bezimienny młody terminus pilnujący Andromedy, którego ruszyło sumienie i który stara się naprawić popełnione błędy
