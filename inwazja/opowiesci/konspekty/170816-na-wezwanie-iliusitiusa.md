---
layout: inwazja-konspekt
title: "Na wezwanie Iliusitiusa"
campaign: czarodziejka-luster
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170815 - Bliźniaczka Andromedy (An)](170815-blizniaczka-andromedy.html)

### Chronologiczna

* [170815 - Bliźniaczka Andromedy (An)](170815-blizniaczka-andromedy.html)

## Kontekst ogólny sytuacji

-

## Punkt zerowy:

Pytania MG do siebie:

* Jaki los spotka Manfreda?
* Czy pojawią się Illuminati?

Koncept:

* Głód, nieskończona potrzeba adoracji
* Wróżda, sprowadzająca Zniszczenie
* Lustra w rękach klątwożyta
* Trzy Świece Iliusitiusa

**Stan początkowy**:

1. Tańczący z Bogami
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 3/10
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 3/10
    2. (KM) Katarzyna imprintuje swoją egzystencję klątwożytami w komputerach: 0/10
    3. (MI) Manfred się wyrywa, acz jego moc jest osłabiona: 1/10
    4. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem: 2/10
    5. (I)  Iliusitius niszczy wszystkich magów mocą swoich Świec: 0/10
    6. (KM) Katarzyna uderza w kult Iliusitiusa, niszcząc / ujawniając go: 0/10
1. Intrygujący z Magami
    1. (K)  Katarzyna jest wszędzie, nieśmiertelna; opinia cenionej czarodziejki: 0/10
    2. (I)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia: 0/10
    3. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi: 2/10
    4. (S)  Sandra wybija się na pierwsze miejsce przydatnych magów w okolicy i ratuje ludzi: 0/10
    5. (I)  Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 0/10
    5. (I)  Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 0/10

**Scena początkowa**:

Rafał Warkocz. Mag lokalny, dość ważny. Szef Manfreda. Koci Manfreda. Zaproszony na ciasto przez Katarzynę. Przychodzi, ona się łasi i rzuca wspólne zaklęcie, dając mu artefakt. Ona się śmieje, gdy z lustra wychodzą klątwożyty i się z nim łączą. On rzuca kielich i roztrzaskuje jej twarz. Ona mówi, że jest piękna i go integruje ze sobą...

**Pytania i odpowiedzi**

* Ż: Co sprawiło, że Klara i Mordred trafili w okolice Małopolski, mniej więcej w te tereny.
* D: Poszukiwali rzadkiego eksponatu potrzebnego do ich projektu. Tylko tu to było widziane. Podążają za śladem.
* Ż: Skąd Mordred ma doświadczenie związane z energiami Illuminati
* D: Wiele podróżował, między innymi trafił też do miejsc, gdzie Illuminati się w jakiś sposób zamanifestowało. Badał to.
* Ż: Co sprawiło, że delikatna energia Illuminati - wybuch - sprawia, że chcą tu zdobyć czy zyskać.
* K: Być może będą w stanie to wykorzystać. Sama energia Illuminati jest warta zainteresowana, bo Illuminati tu na pewno nie ma.
* D: Zdaniem Mordreda tą energię można wykorzystać, by zdjąć Klątwę.

## Misja właściwa:

**Dzień 1:**

_Rano (+1)_:

Zespół wykrył emanację Illuminati będąc niedaleko Owcodrogi. Emanacja dochodzi z okolic Strażnicy Dumnej. Niedaleko. Są SUVem. Dla Klary to jest genialna sprawa. Zdecydowali się podjechać jak najbliżej i poobserwować z ukrycia co tam się dzieje.

Po drodze złapała ich autostopowiczka. Zignorowała ich. W okolicy Strażnicy wystrzelił broadcast terminusa "jestem w okolicy, wszystko w porządku". Terminus Świecy. Ale Świecy w tej okolicy nie ma... Mordred od razu odesłał własny sygnał - "terminus w okolicy i to MOJA sprawa". Ten drugi terminus nawiązał kontakt z Mordredem. To August Bankierz.

Mordred powiedział Augustowi, że ten teren należy do Millennium. Po krótkim starciu słownym August zauważył, że w świetle potencjalnej obecności Illuminati konflikty o teren są wtórne do tematu Illuminati. Więc czy temat był prawidłowo zgłoszony? Mordred zaklął pod nosem. Nie jest osobą do formalnych tematów. Jest osobą do działania. Mordred dziabnął ambicję Augusta - może ASYSTOWAĆ. 0v1->F. August działa sam, nie współpracuje. Zbyt silnie zrażony przez Mordreda (+1 O).

Chwilę potem do Mordreda komunikacja od Sandry - poprosiła Mordreda by ten nie patrzył źle na Augusta. Ona powiedziała Mordredowi, że w okolicy coś się dzieje z ludźmi i komputerami, jest dwóch magów (Manfred i Rafał) oraz coś jest w lasku. Aha, jest przerażona Illuminati w okolicy i tym, że August się w coś pakuje. Gdy Mordred ją postraszył, Sandra wybłagała (0v2->F) by uratował jej Augusta. I będzie informować, gdzie się znajdują. A on ma zamiar odwiedzić lokalnego ważnego maga, by ten powiedział mu jak wygląda sytuacja w okolicy. Czyli maga Warkocza.

Klara w tymczasie szuka danych na temat Rafała i Manfreda korzystając z uprawnień Mordreda. 4v2->S. Na tym terenie jest TRZECH magów: 

* Marianna Biegłomir; kiedyś w Świecy, po tym jak Świeca utraciła ten teren. Marianna się nie przeprowadziła. Opuściła Świecę, ale nie wystąpiła formalnie. Po prostu "samo wyszło" - potem wyłączyła hipernet. Lubi piękno i być podziwianą. Astralika, podstawy katalizy. Bardzo sympatyczna i niegroźna; lubi sprawiać radość. Społeczny motylek. Przed Zaćmieniem nie była katalistką.
* Rafał Warkocz; nie był w Świecy, był człowiekiem który uzyskał magię. Lubi nadużywać swojej pozycji. Katalista, ale to nie jest szkolone. Jego główną ścieżką jest magia elementalna.
* Manfred Jarosz; mag pomniejszy. Nie był w Świecy, słaby charakter i taki... nijaki. Urzędnik z urodzenia. Podstawy biomancji i podstawy infomancji. Ogarnia papiery.

Klara wpadła na dość sprytny pomysł - sprawdzić jak działa sam sygnał hipernetu od lokalnych magów. Jeśli coś jest nie tak... to hipernet powinien ich wyrzucić. Klara, ekspert od rojów, weszła w temat bardzo dokładnie - i dostała coś niepokojącego. 4v4->SS. Klara dostała sygnał, że oboje należą do tego samego roju. Są zsynchronizowani ze sobą przez hipernet. Są tym samym bytem...

_Południe (+1)_:

Mordred tka tarczę antymagiczną i jest na dachu domu Agencji Pośrednictwa Nieruchomości Warkocz. Ostrzegł Augusta. 5v3->SS. Warkocz rzucił figurką z Quark. August zaczerpnął energii i zainfekował się klątwożytem. Klątwożytami. Mając energię, WALNĄŁ. Ogłuszył Romana, potem wyskoczył przez okno, słaniając się. Mordred postrzelił go trucizną. August padł. SUV podjechał i wyjechali z nim... Edwin podpowiedział jak to naprawić i Klara + Mordred przygotowali truciznę.

8v6->SS. Udało się. Sandra w panice. Coś się dzieje z Augustem a ona jest tam SAMA. Zapytała co dalej. Dowiedziała się, że ma stąd jechać. Rozpłakała się, ale utrzymała się do kupy. Klara oddała jej potem SUVa i kazała odebrać Augusta i zawieźć go do Świecy. Sandra też znalazła echo Iliusitiusa w domku za miastem.

_Wieczór (+1)_:

Mordred i Klara mają to gdzieś. Biorą ten rój pająków z truciznami mającymi rozwiązać problem Augusta i zdecydowali się go wykorzystać ofensywnie przeciw lokalnym magom Skażonym klątwożytami. Klara i Mordred współpracują nad zaklęciem i decydują się na atak w nocy. 

_Noc (+1)_:

10v6 ->S. W nocy pająki skutecznie unieszkodliwiły magów. Mordred przechwycił Manfreda i Rafała. Nie zadziałało to na Mariannę; zabiła pająki i poszła szybko w kierunku naszego Zespołu pozbywając się ubrania sukcesywnie. Rośnie jej prędkość i możliwości walki... po czym wzbiła się w powietrze na swoich nowych skrzydłach. Anielskich skrzydłach.

Klara przygotowała samochód. Puszkę. Coś, co ma ją objąć i ją spowolnić. Pułapka, spowalniacz.
Mordred przygotował potężną pajęczą sieć.

Klary nie ma. Jest Mordred. Ona w ukryciu. Pojawiła się Marianna - z ukrycia zahipnotyzowała Mordreda. Klara na to, lol, odpala puszkę i inne takie. Marianna unieszkodliwiona. W puszce. Mordred uwolniony z efektu. Wywalili to kinetycznie tam, gdzie wyczuli echo Iliusitiusa. Trzy Świece Iliusitiusa (trzy dziewczyny) zniszczyły Mariannę. Echo przerażające dla magów; Klara i Mordred się ewakuowali zostawiając jedynie info o tym co tu się dzieje...

**Podsumowanie torów**

1. Tańczący z Bogami
    1. (K)  Marianna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 4/10
    2. (K)  Marianna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 1/10
    3. (KM) Marianna imprintuje swoją egzystencję klątwożytami w komputerach: 2/10
    4. (MI) Manfred się wyrywa, acz jego moc jest osłabiona: 3/10
    5. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem: 3/10
    6. (I)  Iliusitius niszczy wszystkich magów mocą swoich Świec: 2/10
    7. (KM) Marianna uderza w kult Iliusitiusa, niszcząc / ujawniając go: 0/10
2. Intrygujący z Magami
    1. (K)  Marianna jest wszędzie, nieśmiertelna; opinia cenionej czarodziejki: 2/10
    2. (I)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia: 1/10
    3. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi: 2/10
    4. (S)  Sandra wybija się na pierwsze miejsce przydatnych magów w okolicy i ratuje ludzi: 3/10
    5. (I)  Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 3/10
    6. (I)  Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 0/10


**Interpretacja torów:**

Marianna częściowo się reimprintowała w Manfredzie w formie ghula (2 osoby, 1 ciało). Nie dość mocno, ale jest. Iliusitius wzmocnił się, ale nie dość mocno; magowie wiedzą o jego obecności i go muszą zatrzymać.

# Progresja

# Streszczenie

Mordred i Klara odpowiedzieli na tajemniczą flarę kojarzącą się z Iliusitiusem. Na miejscu uratowali Augusta Bankierza, który wpadł prawie w sidła klątwożytowanej Marianny - czarodziejki, która straciła moc. Zastawili pułapkę i usunęli potwora ujawniając Iliusitiusa. Dodatkowo, ewakuowali wszystkich nieszczęśliwych magów tak, by nikomu nic się nie stało. A magowie muszą Coś Zrobić z Iliusitiusem w okolicy...

# Zasługi

* mag: Mordred Blakenbauer, władca trujących pająków, mistrz zastraszania Sandry i celny kineta
* mag: Klara Blakenbauer, wsparcie taktyczne, elektryfikatorka pajęczych sieci oraz pani puszek na viciniusy
* vic: Marianna Biegłomir, chciała odzyskać moc, ale zmieniła się w Skażonego potwora. Skończyła zniszczona przez Iliusitiusa.
* mag: Rafał Warkocz, głodny uznania i kobiety mag, zmieniony w element Roju Marianny. Prawie dorwał Augusta; gdyby nie Mordred... obezwładniony przez Mordreda.
* mag: Manfred Jarosz, kocony przez swego przełożonego agent Marianny. Stan aktualny - obezwładniony jak Warkocz.
* mag: August Bankierz, podąża za śladem zostawionym przez Andromedę, wchodzi w swary z Mordredem i na końcu Mordred go ratuje i Sandra ewakuuje.
* mag: Sandra Stryjek, która ZNOWU uratowała Augusta. Dyskretnie pomaga Mordredowi przeciw Augustowi i inteligentnie współpracuje. Cicha bohaterka Augusta. Niedoceniona przez nikogo.
* mag: Edwin Blakenbauer, konsultant Klary od "co zrobić z tymi Skażonymi magami". Pomógł pośrednio Augustowi; dzięki niemu wiedzą o klątwożytach.
* vic: Iliusitius, który wezwał sobie magów do rozwiązania problemów ze Skażeniem i chciał dyskretnie się rozprzestrzenić. Niestety, nie wyszło - Mordred ujawnił jego obecność.

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Chrobrego
                1. Strażnica Dumna
                    1. Pośrednictwo Nieruchomości Warkocz, 
                1. Owcodroga, niewielka miejscowość, gdzie Iliusitius odpalił flarę by wezwać magów do rozwiązania problemu Skażonego maga

# Czas

* Opóźnienie: 1
* Dni: 2


# Waśnie


1. Tańczący z Bogami
    1. (Mr)  Marianna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 3/10
    1. (Mr)  Marianna reimprintuje się w Manfredzie, wzmacniając ghoula <pref>: 0/10
    2. (MrM) Marianna imprintuje swoją egzystencję klątwożytami w komputerach: 0/10
    3. (MI)  Manfred się wyrywa, acz jego moc jest osłabiona: 1/10
    4. (MrM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem: 2/10
    5. (I)   Iliusitius niszczy wszystkich magów mocą swoich Świec: 0/10
    6. (MrM) Marianna uderza w kult Iliusitiusa, niszcząc / ujawniając go: 0/10
1. Intrygujący z Magami
    1. (Mr)  Marianna jest wszędzie, nieśmiertelna; opinia cenionej czarodziejki: 0/10
    2. (I)   Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia: 0/10
    3. (AK)  August znalazł lokalnego potwora i go pokonał dla chwały i potęgi: 2/10
    4. (S)   Sandra wybija się na pierwsze miejsce przydatnych magów w okolicy i ratuje ludzi: 0/10
    5. (I)   Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 0/10
    5. (I)   Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati <pref>: 0/10


## Tańczący z Bogami

### Zakres waśni

Manfred został pokonany i wchłonięty przez Katarzynę. Iliusitius nie ma wróżdy do Manfreda, ale nie zależy mu na jego uratowaniu. Katarzyna się z Manfredem zespoliła i go imprintowała. Iliusitius chce zniszczenia Głodnej Adoracji i magów. Katarzyna dąży do maksymalizacji Adoracji.

### Strony z Motywacjami

* Manfred (M), który pragnie być wolnym i jak najdalej od klątwożytów
* Katarzyna (K), która pragnie poszerzać Adorację
* Iliusitius (I), który chce uratować ludzi - najlepiej kosztem magów

### Ścieżki:

1. Tańczący z Bogami
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula: 3/10
    2. (KM) Katarzyna imprintuje swoją egzystencję klątwożytami w komputerach: 0/10
    3. (MI) Manfred się wyrywa, acz jego moc jest osłabiona: 1/10
    4. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem: 2/10
    5. (I)  Iliusitius niszczy wszystkich magów mocą swoich Świec: 0/10
    6. (KM) Katarzyna uderza w kult Iliusitiusa, niszcząc / ujawniając go: 0/10


## Intrygi Magów

### Zakres waśni

August chce pokonać Zło i rozwiązać problem z ewentualnymi klątwożytami w okolicy - chwała i sława. Sandra chce się przydać Augustowi i pomóc ludziom. Katarzyna chce zasymilować magów i poszerzyć Adorację. Iliusitius, który pragnie swoimi Świecami zniszczyć klątwożyty i magów.

### Strony z Motywacjami

* August (A), który szuka potworów i chce pomóc lokalnej czarodziejce - "senpai notice me".
* Sandra (S), która pragnie pomóc ludziom i jest zazdrosna o Katarzynę.
* Katarzyna (K), która celuje w asymilację jak największej ilości magów jako wektor klątwożytnictwa.
* Iliusitius (I), który pragnie wyczyścić cały obszar.

### Ścieżki:

1. Intrygujący z Magami
    1. (K)  Katarzyna jest wszędzie, nieśmiertelna; opinia cenionej czarodziejki: 0/10
    2. (I)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia: 0/10
    3. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi: 2/10
    4. (S)  Sandra wybija się na pierwsze miejsce przydatnych magów w okolicy i ratuje ludzi: 0/10
    5. (I)  Wróżda dokonuje erupcji magicznej powodując sprowadzenie Illuminati: 0/10

## Naturalna Entropia

### Zakres Waśni

Normalne problemy w okolicy prowadzące do chaosu

### Siły z Motywacjami

* brak

### Ścieżki:

1. Naturalna Entropia
    1. Brak:    0/10

# Narzędzia MG

## Opis celu misji

* Nowa forma budowania Frontów, Ścieżek... przez Waśnie.

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
