---
layout: inwazja-konspekt
title:  "Druga Inwazja"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140625 - Ostatnia Saith (AW, WZ, HB)](140625-ostatnia-saith.html)

### Chronologiczna

* [140625 - Ostatnia Saith (AW, WZ, HB)](140625-ostatnia-saith.html)

## Misja właściwa:

 Biuro Andrei i Wacława:

Żółte, oślepiające światło. Patrzy, wciąga. Coś się przekształca. Rozdarcie, straszliwy ból, odcięcie. Czar Teresy spaja ich z powrotem.
Znowu jest spokojna noc. Jak gdyby nigdy nic. Jakby przed chwilą nie byli pożerani żywcem.
 

System zabezpieczeń odwrócił się przeciwko nim.  Teresa odezwała się, co ujawniło ją przed systemem. Czyżby uratowała ich przed przejęciem?
Teresa zostaje zaatakowana przez system. Utrzymuje ją przy życiu, ale jednocześnie sprawia ogromny ból.

Wacław chce przeprowadzić atak nim zostaną wykryci. Wielkie gęby ukryte w systemie zostały jednak rozproszone. Są w kropce. Na dole znajduje się przynajmniej 3 magów a ich aury są uszkodzone i ich magia nie działa kompletnie. Są na wrogim terenie a trzyma ich w ukryciu tylko czar Teresy.

Wacław przesyła informacje Irinie: „Uczeń Moriatha nie może wyjść żywy.” Następnie ucieka, teleportując się beaconem do Krystalii.
Andrea ucieka porywając trochę krwi Teresy. Traci przytomność zaraz po teleportacji.

Ich wzory magiczne są wypaczone. Są jakby na pół żywi – na pół martwi.


Rezydencja:

Hektora budzi Quasar. Irina nie żyje. Whisperwind jest agentką Inwazji. Sama Quasar ledwo uchodzi z życiem dzięki ostrzeżeniu przesłanym jej przez Irinę tuż przed śmiercią.
Rezydencja zostaje powołana w tryb pełnej gotowości bojowo-defensywnej.
Wszyscy „goście” są zamykani w pokojach. Nowa „lepsza” Ika też. DiDi się obraża.

Srebrna Świeca ogłasza Stan Wyjątkowy. Inicjatorem jest Grzegorz Czerwiec.
Coś wykorzystuje system Moriath jak rozproszony system obliczeniowy. Edwin bada sprawę. Celatid systemu Moriath ma odczyty spoza skali.

Do rezydencji przybywa Czerwiec z obstawą. Hektor podejmuje go na swoim terenie uzbrojonym po zęby. Rezydencja ma wyłapać wszelkie próby teleportu i przekierować je do lochów rezydencji.
Czerwiec żąda wydania Kopyta i Marcelina (magów SŚ). Hektor się nie zgadza powołując się na zagrożenie jakie w danej chwili Czerwiec stanowi dla jego rodziny. Marcelin natomiast jest pod opieką lekarską i nie może opuścić rezydencji.
Czerwiec zasłania się protokołem i prawami. Hektor je ignoruje świadom konsekwencji – nie ma wyboru. Nie decyduje się jednak atakować Czerwca. Pozwala mu i jego przydupasom opuścić rezydencję.

Milenium: 

Andrea budzi się naga w obecności kralotha. Jest razem z Wacławem. Są podpięci do urządzeń medycznych.
Wchodzi starszy jegomość z siwymi boczkami. Ma na sobie garnitur, a przy pasie pistolet.
Nazywa się Draconis Diakon. Pomógł im zregenerować wzór i się odtworzyć.
Podobno kralotyczni Diakoni są niepodatni na Inwazję (tak jak Irina czy Quasar - to viciniusy).

Biuro detektywów uległo samozniszczeniu. Ekipa magów wysłana do niego z pomocą przez Irinę zginęła (lub zaginęła).
Krystalia się zdestabilizowała. Jest na terapii kralotycznej. Wszystko przez połączenie się z uprzężą. Coś się wtedy stało co nie powinno.
Andrea podziękowała za pomoc Draconisowi i kralothowi.

Dowiadują się o śmierci Iriny.
Chcą skontaktować się z Hektorem. Da się nawiązać kanał komunikacyjny z Blakenbauerem. Dracena Diakon (czyli DiDi) okazuje się córką Draconisa.

Tatiana Zajcew zniknęła. Według Wacława to jednak może być  jednak dobra wiadomość. Zwłaszcza, jeśli Benjamin (ojciec Tatiany) jest, jak uważał Agrest, agentem Inwazji.
 
Mag rodu Myszeczków został dorwany przez Bankierzy i chcą go przykładnie ukarać w ramach odwetu za Annę Kozak (ta, którą pożarła Saith Kameleon). Ten mag był agentem Agresta.
Wymieniają się informacjami o potencjalnych agentach Inwazji (Krówkach, Benjaminie, Czerwcu, Bankierzach….).
Andrea nie została odcięta od Świecy.

Telefon Wacława:

Wacław dzwoni po Swietłane. Okazuje się, ze ona już tu jest, ściągnięta przez Irinę. Tatiana jest ze Swietłaną. Wacław prosi o wstępne przekształcenie we Fłamię. Daje swoja krew na opracowanie formuły. 
Swietłana mówi, że Irina wiedziała, iż może umrzeć i wkalkulowała to w plan. Jest on ciągle w mocy. Potwierdza, że Benjamin musi być agentem Inwazji, bo inaczej Whisper nie byłaby w stanie zabić Iriny. Irina wiedziała, że coś jest nie tak ale nie wiedziała kto i co... i wierzyła, że jakby co, Rezydencja Zajcewów ją obroni. Ale seras Zajcew to Benjamin, nie Irina.

Dalsza konwersacja z Draconisem: 

Andrea wypytuje o szczegóły związane z Krystalią. Podobno kiedyś było z nią źle i ojciec ją wtedy wspierał. Gdy nagle jej się poprawiło ojciec się odsunął i niemal zerwał kontakt.
Draconis okazuje się być wyżej postawionym terminusem i jest magiem kralotycznym.
Andrea próbuje drążyć temat Krystalii, ale zostaje jednak zbita z tropu.  

Przekonana Andrea wyjawia swoją teorię. Mówi o czarodziejce z rodu Blakenbauerów i korelacji między chorobą Krystalii a zniknięciem Margareth. Drakonis nie jest jednak przekonany. Wie gdzie jest Margareth. Ma ona podobno zniszczony umysł - nie jest już sobą.
Walkę z Inwazją ze strony Milenium prowadzi Amanda Diakon, jako najbardziej nienaturalna z Diakonów.
Agentów Inwazji nie da się wyleczyć. Łączą się oni z pół-inteligentnym węzłem na bazie defilera.

Drakonis chce dołączyć do sojuszu z Blakenbauerem i Quasar. Mówią mu, co wiedzą o Moriacie, ale nie znają wszystkich szczegółów. Proszą o podwózkę pod Rezydencje.
Od tajnych agentów Andrea wie, że Świeca ogłosiła, ze Blakenbauer porwał członków Świecy. Rozpoczyna się nagonka.

Rezydencja:

DiDi wynegocjowała u Hektora większy dostęp do laboratoriów. W zamian za to ma pozostać w rezydencji. Udaje jej się z Hektorem przełamać pierwsze lody. Mówi mu trochę o sobie i swoich oczekiwaniach. Hektor odkrywa, że ma stały kontakt z Ojcem, i że jest on ważną osobą w Milenium. Potencjalny sojusznik.

Do rezydencji przybywają Andrea i Wacław (w przebraniu 6 grubych praczek). Następuje synchronizacja wiedzy. Andrea nie wyjawia jednak nic o miejscu przebywania Margareth, a Hektor nie wspomina, że ma w piwnicy hulający projekt Moriath. 

Detektywi postanawiają pokazać Hektorowi tajne przejście, które miała wykorzystywać Mojra Grecka do potajemnego wchodzenia do rezydencji. Hektor jest w szoku, Rezydencja jest w szoku i rozpoczyna skanowanie nieznanych jej korytarzy, Whisperwind niosąca ładunki do zniszczenia Rezydencji jest zaskoczona, że ktoś ją przydybał. Strzela do Hektora i przechodzi na Fazę Daemonica. Rezydencja zareagowała stawiając błyskawicznie tarcze, które ratują Hektora przed niechybną śmiercią. Ogień ładunków skierowała na nieużywane części Rezydencji.

Gdyby nie to, że Whisper została teraz wykryta, Rezydencja także zostałaby zniszczona i zginąłby co najmniej jeden Blakenbauer.

### Zdjęcie mapy misji:

![](Druga%20Inwazja.jpg)

### Aktualny stan NPC po misji:

# Zasługi

* mag: Andrea Wilgacz, którą uratowała Teresa przed Inwazją; jej sojusznicy wreszcie zaczęli współpracować i Andrea nadal ma dostęp do tajnych danych SŚ. 
* mag: Hektor Blakenbauer, który odmówił Czerwcowi wydania Marcelina i Dariusza Kopyto, przez co naraził Rezydencję na atak Srebrnej Świecy.
* mag: Wacław Zajcew, którego uratowała Teresa przed Inwazją; zaproponował Swietłanie, by ta zmieniła go we Fłamię. 
* mag: Whisperwind, agentka Inwazji która PRAWIE dała radę zniszczyć Rezydencję Blakenbauerów i zabiła Irinę Zajcew.
* mag: Irina Zajcew, która wiedziała, że coś jest nie tak lecz nie spodziewała się zdrady Whisperwind oraz męża. Wiedziała, że może zginąć. KIA.
* mag: Teresa Żyraf, która schowała się przed systemem zabezpieczeń Krówków, lecz ujawniła się by ratować Andreę i Wacława. Porwana przez Inwazję. MIA.
* mag: Sebastian Tecznia, który był w Bazie Detektywów podczas ataku Inwazji i został przechwycony przez Inwazję. MIA.
* mag: Estera Piryt, która była w Bazie Detektywów podczas ataku Inwazji i została przechwycona przez Inwazję. MIA.
* mag: Quasar, ciężko uszkodzona przez Whisper (przeżyła dzięki Irinie); przejmuje dowodzenie walką przeciwko Inwazji. Ostrzeżona JAK działa Inwazja.
* mag: Draconis Diakon, zmieniony kralotycznie terminus. Dołącza do Hektora i Quasar (do walki z Inwazją). Powiedział, że wie coś o Margaret.
* mag: Dracena Diakon, która zmieniła Ikę by pomóc Hektorowi (i Ice) i wynegocjowała od Hektora pewne ustępstwa i potencjalną współpracę.
* mag: Tatiana Zajcew, która zniknęła; udała się do Swietłany.
* mag: Swietłana Zajcew, która powróciła na wyraźną prośbę Iriny i przygotowała bazę Fłamii do walki z Inwazją.
* mag: Grzegorz Czerwiec, agent Inwazji który z poparcia grupy magów ogłosił stan wyjątkowy. Hektor odmówił mu wydania magów Świecy, przez co Czerwiec może zaatakować.
* mag: Krystalia Diakon, bardzo zdestabilizowana po zniknięciu Teresy, jeszcze nie odzyskała przytomności. Andrea podejrzewa, że to Margaret Blakenbauer.
* mag: Malwina Krówka, agentka Inwazji i osoba która kontroluje większość istotnych systemów zabezpieczeń u większości istotnych magów.
* mag: Edwin Blakenbauer, monitorujący celatida kontrolującego projekt Moriath i zgłaszający, że coś jest bardzo nie w porządku.
* mag: Marcelin Blakenbauer, który ma dostęp do kanałów Srebrnej Świecy i poinformował Hektora o stanie wyjątkowym.
* mag: Mieszko Bankierz, znacząca postać rodu Bankierz na Śląsku i w SŚ oraz agent Inwazji. Wspiera silnie Grzegorza Czerwca po "zdradzie" Agresta.
* vic: Ika, przekształcona przez Dracenę. Przez to, że Hektor nie ufa Dracenie, nie może też ufać Ice...
* mag: Amanda Diakon, głównodowodząca Millennium do spraw walki z Inwazją z uwagi na najwyższy poziom nienaturalności.