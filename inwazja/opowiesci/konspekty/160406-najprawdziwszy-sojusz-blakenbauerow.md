---
layout: inwazja-konspekt
title:  "Najprawdziwszy sojusz Blakenbauerów"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Powrót Mausów
- Cruentus Reverto
- Za chwałę Blakenbauerów

## Kontynuacja

### Kampanijna

* [160316 - Frontalne wejście Millennium (HB, KB, LB)](160316-frontalne-wejscie-millennium.html)

### Chronologiczna

* [160316 - Frontalne wejście Millennium (HB, KB, LB)](160316-frontalne-wejscie-millennium.html)

## Kontekst ogólny sytuacji

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę... wszystko zaczyna ze sobą walczyć.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Siluria najpewniej go nie uzyska ;-).
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Szlachta i Blakenbauerowie współpracują.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie; Dagmara dowodzi siłami Szlachty. Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Srebrna Świeca poluje na Arazille, która zeżarła Zieloną Kariatydę ośmieszając Ozydiusza.
- Wybuchła plaga Irytki Sprzężonej, nikt nie interesuje się Arazille.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Dzięki działaniom Leonidasa problem Blakenbauerowie - Szlachta.
- Ozydiusz Bankierz pracuje nad cruentus reverto; nie ma jednak czasu, bo non stop są kryzysy. Uruchomił mechanicznych terminusów.
- Weszło Millennium na teren Kopalina za prośbą Wiktora (siły Mirabelki). Też weszły siły Amandy i Pasożyta. Cele są nieznane.
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Marcelin jest oficerem łącznikowym u Ozydiusza.
- Ozydiusz / Blakenbauerowie mają sojusz; ale po ostatnich ruchach płaszczek sojusz jest niepewny.
- Amanda Diakon i Pasożyt Diakon wraz z siłami Millennium opanowują fragmenty Kopalina. Millennium uderza.
- Blakenbauerowie dali radę zdobyć dowody dla pogłaskania Ozydiusza; też zdobywają materiały i surowce.

## Punkt zerowy:

Czelimin. Miasto na uboczu Kopalina, zbudowane dookoła huty "Mieszko". Dzisiaj huta jest wyłączona i nieaktywna.
Jednocześnie lokalna parafia ściąga i rozpowszechnia w świat uchodźców i ludzi nie radzących sobie z życiem (oraz osoby chore).

## Misja właściwa:

Dzień 1:

Hektor patrzy w zadziwieniu, jak to uczciwi i lojalni Blakenbauerowie zaatakowali z zaskoczenia Ozydiuszowe siły (z którymi mieli sojusz) i obronili Szlachtę (z którą mieli sojusz). A wszystko przez Pasożyta. Ale tego nikt nie wie, a na pewno nie Ozydiusz...

Resztę dnia Blakenbauerowie latali jak kot z pęcherzem ratując płaszczki. Jednocześnie płaszczki powodowały nieprawdopodobne szkody wizerunkowe dla Blakenbauerów. A tym razem byli niewinni...

Dodatkowo, jak tylko Edwin wdał się w dyplomatyczne rozwiązania z Ozydiuszem (i wziął ze sobą Leonidasa), ku wielkiej rozpaczy Blakenbauerów przeczytali wiadomości. Jolanta Sowińska stworzyła odpowiednie artykuły, nagrania i dowody, by pokazać Świecy jak to Blakenbauerowie zrobili to wszystko - wpierw skupili się na ochronie by tylko potem nakraść i wbić nóż w plecy. Zauważyła 'with amusement' jak to siły MILLENNIUM chroniły czasem magów Świecy przed Blakenbauerami. Ozydiusz też nakazał magom ewakuację i przeniesienie się w bezpieczne miejsca. Jolanta zauważyła kwaśno, że "bezpieczne od Blakenbauerów" nie są łatwe do osiągnięcia.

Margaret i Edwin skupili się na płaszczkach. Bardzo szybko wyprowadzili, że są one przejęte przez pasożyty. Niedaleko było do zorientowania się, że za tym najpewniej stoi Pasożyt. Super. Bo obrona "a przejęli mi płaszczki" jest najstarszą obroną na świecie...

Dzień 2:

Rano Blakenbauerowie zorientowali się, że nie ma Leonidasa. Borys powiedział, że dostali maila - "Stop. Leonidas w zagrożeniu. Stop. Serce stop. Czelimin. Stop. Szybko. Stop". Hektor poprosił o zlokalizowanie Leonidasa, ale Margaret powiedziała, że ani Klary ani Leonidasa nie da się zlokalizować mocą Rezydencji. Otton i Tymotheus są gdzieś zajęci; nie ma ich w Rezydencji. Edwin jest u Ozydiusza.

CZERWIE! Co? Borys tłumaczy, że da się wydostać stąd czerwiem. Przechodzi przez beton.
Ale Hektor nie chce iść; boi się, że go oskarżą o naruszenie kwarantanny. Jednocześnie Klara zaczęła tracić czujniki; na niebie rozpada się jej sieć... EMP różnego rodzaju. Atakują jej i te drugie technomantyczne drony. Trzecia siła... lub ktoś mający nadmiar siły ognia. Cała przewaga informacyjna zapewniona przez Klarę została zniszczona (bo ktokolwiek tego nie zrobił, ma opcję samodzielnego działania z zaskoczenia).
I co z tym zrobić?

Klara, Alina i Dionizy przejdą przez portal do drugiej posiadłości. 
A stamtąd fordem do Czelimina. A po drodze broń i lapis. Bo tak. Klara zapewnia prawidłową komunikację.
A Czelimin... wita pielgrzymów. Klara ma ratlerka tropiącego szukającego zapach Leonidasa. I ratlerek niczego, niczego nie wykrywa.

Ogólnie, różne grupy manifestują pod różnymi transparentami. "Nie dla uchodźców", "Nie dla kanibalizmu", "Będziemy uzdrowieni" i "Znikający ludzie". Ogólnie, coś się dzieje. Alina opuszcza samochód i idzie na wycieczkę po mieście. Wzięła ratlerka szukającego i poszła zbierać dane. Zajmie jej to trochę czasu. 
- święta relikwia która leczy chorych więc ludzie się pojawiają
- znikający ludzie; ale ksiądz pokazuje gdzie pojechali i policja potwierdza
- nasilenie w Czeliminie zbiegło się z tym, co stało się z Irytką
- parafia ściąga uchodźców i jednocześnie chorych, po czym ich dystrybuuje

Hektor tymczasem poszukał Leonidasa na miejscu; udało mu się dojść do tego, że Leonidas spotkał się z Millennium i zniknął.
Klara przeszukuje obrazki z sieci dron jak jeszcze działały odnośnie lokalizacji Leonidasa.

Alina zdecydowała się porozmawiać z Jerzym (jedynym protestującym o znikaniu). On powiedział, że zniknęła "jego" rodzina uchodźców. I podobno parafia ich przesyła i pomaga się osiedlić, i tymczasowo rezerwuje hotel pracowniczy... a tak naprawdę nie wiadomo o co chodzi. Policja ich podobno widziała i wie o co chodzi, ale... ogólnie, eskalował, lecz zostało to olane.

Bez żadnego problemu Dionizy, Alina i Klara dostali się do pokoju w hotelu robotniczym. Klara pasywną katalizą niczego nie wykryła (kiepski rzut). Poszli porozmawiać z kimś kto widział relikwię. Człowiek chory. By się upewnić, że wszystko działa poprawnie Klara rzuciła zaklęcie... aktywując sieć detekcyjną magię w hotelu. Wykryła tą sieć i została wykryta.

Zapukał do jej drzwi czarodziej. Felicjan Weiner. Przedstawił się i opowiedział o swoim eksperymencie. Zaczął opowiadać coś o palcu, biopolu i potrzebie posiadania odpowiedniej ilości ludzi by móc generować biopole. Ogólnie, jest kiepskim technomantą i biomantą. A tak w ogóle cieszy się że jest kwarantanna, bo tu łazili magowie Świecy i psuli różne rzeczy. Nie mógłby nic zrobić.

Nie wie gdzie jest Leonidas. Nie słyszał nigdy o Leonidasie. Przyciśnięty, przyznał się, że ma pewien problem - jest tu jakiś mag który atakuje i próbuje go skrzywdzić. Ma możliwość go zwabić - ekstrakt z biopola (przyniósł im). Mag jest wyraźnie chory i relikwia go odpycha. Sam Felicjan się boi i nie zamierza się zbliżać.

Klara przygotowała artefakt wyłączający magię; ma więcej minionów niż przeciwnik. Nie blokuje magii Klary a przeciwnik ma (20) by móc przebić się i rzucić jakikolwiek czar. Dionizy przygotował toksyny, neurotoksyny. Śpiący nie czarują. Ale lapisowa zbroja zawsze przydatna. Alina przygotowała rekonesans terenu. Przeciwnik operuje w końcu w nocy.

Test pułapek: Dionizy, Alina, Klara mają przewagę.

Przeciwnik zaatakował z zaskoczenia. Szczęśliwie, Dionizy i Alina znali teren, więc udało im się ukryć przed atakiem. Siły wroga ruszyły prosto na Klarę co pozwoliło naszym agentom wykryć, że atakujący są zwierzętami. A dokładnie... pies. Dionizy i Alina puścili psa i poczekali na maga - tymczasem pies przebił się przez pancerz Klary i ją pokąsał. Alina odstrzeliła mu łeb. A Dionizy skutecznie unieszkodliwił maga (artefakt Klary pozbawił go mocy i tarcz).

Przeszli z nim przez Portal. Edwin potwierdził, że mag jest chory, ale nieszkodliwy. Okazało się, że na miejscu jest... Leonidas. I Leonidas negocjował z Millennium. I cóż on takiego wynegocjował...

Wejdą w sojusz z Millennium. Podział Kopalina na wiele gildii, na frakcje które nie są tylko Świecą. Millennium weszło tu z określonym celem. Chcą zdobyć coś bardzo ważnego, ale chwilowo Kopalin nie jest w stanie sam się utrzymać. Świeca jest za słaba. Kopalin musi zostać podzielony i min. dlatego Blakenbauerowie z nimi się sprzymierzyli; silna Świeca to Blakenbauerowie którzy muszą odpowiadać przed Ozydiuszem. Dużo frakcji to "to nie my" - plausible deniability. Plus możliwość rozgrywania różnych stron. Duża niezależność i autonomia.

Nic dziwnego, że Blakenbauerom to pasuje.


## Wynik misji:

- Blakenbauerowie współpracują z Millennium; dążą do rozpadu monopolu Srebrnej Świecy w Kopalinie. Diakoni mogą współpracować / walczyć z Blakenbauerami.

# Zasługi

* mag: Hektor Blakenbauer, wysłał Dionizego nie chcąc "jechać czerwiem poza kwarantannę". Po* magał Margaret trzymając płaszczki.
* mag: Klara Blakenbauer, będąca bardzo sceptyczna co do sensu czegokolwiek związanego z Czeliminem. Pogryziona przez psa.
* vic: Alina Bednarz, zebrała trochę informacji (wywiad środowiskowy) i egzekutorka kąsającego Klarę psa.
* vic: Dionizy Kret, łowca polujący na Leonidasa. Nie znalazł go; zapolował na innego * maga.
* mag: Leonidas Blakenbauer, który negocjował sojusz z Millennium mający podzielić Kopalin na wpływy.
* mag: Jolanta Sowińska, ulubiona dziennikarka Hektora masakrująca Blakenbauerom nieciekawą opinię.
* mag: Felicjan Weiner, główny, eksperymentujący * mag w Czeliminie. Typowy Weiner z typowymi nieodpowiedzialnymi eksperymentami. W zasadzie niegroźny.
* czł: Jerzy Gurlacz, aktywista z organizacji pomocy uchodźcom. Brzmi nieistotnie. Potencjał na paranoję - wszędzie widzi znikających ludzi (i kanibalizm).
* czł: Olga Miodownik, która wysłała maila do Hektora o Leonidasie z nadzieją, że Hektor wyśle siły specjalne. Nie wysłał.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
            1. Powiat Czelimiński
                1. Czelimin
                    1. kościół neoromański
                    1. hotel pracowniczy