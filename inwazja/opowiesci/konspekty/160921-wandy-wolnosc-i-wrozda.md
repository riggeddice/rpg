---
layout: inwazja-konspekt
title:  "Wandy wolność i wróżda"
campaign: taniec-lisci
gm: żółw
players: kić, wojtuś, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160911 - Reedukacja Infensy (SD)](160911-reedukacja-infensy.html)

### Chronologiczna

* [160914 - Aleksandria, krwawa Aleksandria... (HB, KB)](160914-aleksandria-krwawa-aleksandria.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

* Ż: Co może skutecznie uciemiężyć Hektora? 
* D: Zataja dowody, które mogłyby działać przeciwko rodzinie. Sfałszowane, acz prawdopodobne.
* Ż: Co sprawia że właśnie Wy dostajecie bardzo delikatne zadanie wymagające finezji.
* W: Hubert z uwagi na wcześniejsze powiązania z Hektorem a Zdzisław by mieć wtyczkę do Huberta.
* Ż: Jakie może mieć korzyści Skubny z doprowadzenia Wandy do Czesława Czepca?
* D: Czepiec oferuje Skubnemu kontakty w środowiskach w których obraca się Hektor.
* Ż: Kim z zawodu i w ogóle jest Czesław Czepiec?
* W: Czesław jest alfonsem. Szczególnym alfonsem. DYSKRETNYM alfonsem. Z towarem najwyższej jakości.
* Ż: Czemu BĘDZIECIE dyskretni?
* D: Nie chcemy podpaść prokuratorowi i Skubnemu jednocześnie.
* Ż: W jaki sposób Diana przekonała Mariana by ów pomógł jej nie informując Skubnego?
* W: Diana zagroziła Marianowi, że wyjawi sekret pomiędzy nimi.

## Misja właściwa:

Dzień 1:

Na łajbie Tukan w Pirogu Dolnym, spotkanie Mariana Rokity z Zespołem.
Leszkowi Żółtemu nie może stać się krzywda.
Ma papiery na Hektora. 
Hubert sprawdził na policji; są dane na temat Leszka.
Kiedyś oskarżył Hektor Leszka o zniesławienie Grażyny Remskiej
Hubert wydrukował teczkę Leszka. Jest urlop na żądanie.
Leszek mieszka z żoną (Iloną) i trójką dzieci.

Pytanie czy ktoś inny też nie dostał "na wyłączność". Pojawił się pomysł - trafić do dziennikarki, Mileny Marzec. Ona musi wiedzieć.
Milena nie chce się podzielić informacją. Księgowość nie chce ubierać prezenterek.
Wychytrzona przez Huberta, Milena powiedziała, że Anna Góra też dostała wiadomości. Pakt też - ale Pakt nie chce (sidenote: bo Pakt i Hektor są po tej samej stronie)
Milena dowiedziała się co Żółty chce sprzedać:
- historię o wykrwawiającej się Kindze Melit której nikt nie pomógł
- historię o trzymanej w piwnicy Wandzie Ketran
- historię o ukrywanych przez Hektora dowodach

Ok, nie całkiem fajnie. 

Grzegorz Włóczykij pracujący dla Skubnego dał im bilety do teatru. WTF. Powiedział, żeby przyszli wieczorem po wszystkim.
"Wielka Wola Kosmosu" indeed.
Skontaktowali się z Rokitą. Powiedzieli o Wielkiej Woli Kosmosu i Grzegorzu Włóczykiju.

Zrobili "zabawki erotyczne Hektora". Wow, świetnie wyszło. Żółty będzie opóźniony.
I faktycznie, Żółty poczeka. Mają 4 dni opóźnienia.

Wieczorem nie pojechali do teatru.
Żółtego pobili anarchiści (bo nie chciał dać teczki) i ukradli teczkę.

Rokita zadzwonił i opieprzył ich za spieprzenie akcji. Mają jechać na oczyszczalnie i zrobić wywiad z Wandą.

Wanda polubiła Zdzisława.

Dzień 2:

Rokita poprosił do siebie Zdzisława i Huberta. Powiedział, że chce Wandę porwać i zaprowadzić do Czesława Czepca. 

"Ale... przecież właśnie udowadnialiśmy że ona NIE jest porwana!" - Hubert
"To było wtedy" - Rokita

Moment - ale przecież Wanda NIE jest porwana.
Już ma być. To Rokita wymyślił by móc pójść w górę. 
Wieczór, gdy anarchista wraca do domu

Skopanie, zdobycie teczki i adresu Wandy
Wanda odnawia kontakty z Dagmarą itp

Rokita dostał telefon - mają teczki. Pochwalił.

Hubert "uratował" pobitego biednego Stanisława. 
Zaprzyjaźnił się z nim, uratował mu życie.
BIŁ KTOŚ INNY!

Telefon do Wandy od Stanisława z telefonu Huberta. Okazało się, że uratowali Stanisława. 
Wanda, niechętnie, jedzie z nimi. Niech Stanisław sam się zgubi.

W miejscu, gdzie jest Iliusitius.
Wanda, przywrócona z Rezydencji - na jej miejsce klon.

Wanda wraca do Mrówkowca.
"Czas skończyć rządy magów"

# Progresja

* Wanda Ketran: wydostała się z Rezydencji Blakenbauerów mocą Iliusitiusa
* Wanda Ketran: ma nałożoną na siebie Matrycę Iliusitiusa
* Wanda Ketran: ma prawo do wróżdy wobec Rodu Blakenbauerów
* Diana Larent: powiązanie z Szymonem Skubnym
* Diana Larent: powiązanie z Marianem Rokitą (enforcerem Skubnego)
* Diana Larent: powiązanie z Iliusitiusem
* Hubert Brodacz: jest wolny od Skubnego (spłacił dług)
* Zdzisław Kamiński: jest wolny od Skubnego (spłacił dług)

# Streszczenie

Taniec Liści przygotował materiały mające pogrążyć Hektora Blakenbauera i podłożył to . Przechwycił informacje Skubny i kazał spowolnić i dowiedzieć się co tam jest; Hektorowi nie może się nic stać (Tymotheusa wola). Ludzie Skubnego zdobyli info co tam jest (Kinga Melit, Wanda Ketran, ukrywanie dowodów) i spowolnili. Edwin i Margaret z matrycy Karradraela (krwi Mausów) zrobili klona Wandy i ludzie Skubnego podłożyli tego klona. Potem Diana Larent kazała (przez Rokitę) porwać Wandę i przekazać ją Czesławowi Czepcowi; okazało się, że to kapłan Iliusitiusa. Iliusitius przywrócił PRAWDZIWĄ Wandę z Rezydencji. Wanda Ketran jest na wolności... a Skubny wyraźnie nie w pełni kontroluje sytuację w swojej organizacji.

# Zasługi

* czł: Zdzisław Kamiński, poczciwy kafar, który nie ma nic przeciwko temu by walnąć anarchiście w celu wykonania misji. Preferuje metody pokojowe. Spłacił dług u Skubnego.
* czł: Hubert Brodacz, policjant, eks-siły Hektora, który potrafi pobić anarchistę by potem się z nim zaprzyjaźnić; jest bezwzględny. Spłacił dług u Skubnego.
* czł: Wanda Ketran, 34 klon stworzony przez Tymotheusa by "dowody" Tańca Liści zneutralizować. Poświęciła się Iliusitiusowi, by wyrwać Blakenbauerom prawdziwą Wandę.
* czł: Marian Rokita, mięśniak i móżdżak Szymona Skubnego. Gość od wydawania poleceń gdy szef nie chce by było na niego. Proaktywnie współpracuje z Dianą Larent.
* czł: Grażyna Remska, kustosz muzeum niesłusznie kiedyś spotwarzona przez Leszka Żółtego; Hektor Blakenbauer bronił jej czci. Nie wystąpiła na misji.
* czł: Szymon Skubny, przywódca 'rodziny Skubnego', w rękach Tymotheusa Blakenbauera. Gdy Taniec Liści sfabrykował dowody na Hektora, Skubny go bronił.
* czł: Diana Larent, młoda i genialna dziewczyna, która chcąc wyrwać rodzinę z rąk Tymotheusa zwróciła się ku Iliusitiusowi przez Czesława Czepca.
* czł: Grzegorz Włóczykij, niekompetentny agent Diany który jest fatalny w BlackOps; skrewił i dostał OPR od Mariana Rokity. Tak bardzo SpecOps...
* czł: Leszek Żółty, który ma informacje o Hektorze - nie wie skąd (od Tańca Liści). Pobity przez anarchistów, stracił teczki.
* czł: Milena Marzec, dziennikarka SkubnyTV, która myślała że przechytrzy Huberta ("z księgowości") - i zrobiła dlań wszystko... za nic.
* mag: Edwin Blakenbauer, który z Margaret zbudował sprawny klon Wandy Ketran z krwią Mausów, by zniszczyć matrycę Arazille matrycą Karradraela.
* mag: Margaret Blakenbauer, która z Edwinem zbudowała sprawny klon Wandy Ketran z krwią Mausów, by zniszczyć matrycę Arazille matrycą Karradraela.
* czł: Jan Szczupak, który przekazał na żądanie Edwina Wandę (klona) Zdzisławowi i Hubertowi. Elegancki, wzbudził cień zaufania u Wandy.
* mag: Tymotheus Blakenbauer, który zaprojektował sposób zbudowanie klona Wandy z krwią Mausów - by dało się odwrócić uwagę od Blakenbauerów.
* czł: Anna Góra, dziennikarka która przekazała materiały o Wandzie Stanisławowi i Tomaszowi, by oni ją znaleźli
* czł: Stanisław Pormien, który dostał od Anny informacje o potencjalnej lokalizacji Wandy. Pobił z Tomkiem Żółtego; dostał teczki. Pobity przez Zdzisława, stracił teczki.
* czł: Tomasz Kracy, siłowy anarchista. Pobił Żółtego, odzyskał teczki i miał nadzieję, że spotka się szybko z Wandą. Chciał wybić prokuratorowi szybę w oknie.
* czł: Czesław Czepiec, alfons i mało szanowany biznesmen z Germanji... który jest kapłanem Iliusitiusa i kontaktem Diany Larent. 
* vic: Iliusitius, który uwolnił Wandę Ketran z Rezydencji Blakenbauerów i nałożył na nią własną Matrycę.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Bełty Śląskie
                        1. Stacja SkubnyTV, telewizja lokalna... z ASPIRACJAMI ;-).
                    1. Obrzeża
                        1. Zakład produkcji nawozów "Chegolent", gdzie zaprowadził Jan Szczupak Wandę
                    1. Ulica Rzeczna
                        1. Port rzeczny, gdzie na łajbie "Tukan" spotyka się Rokita z Zespołem
                1. Piróg Górny
                    1. Obrzeża
                        1. Osiedle Polne, gdzie mieszka Leszek Żółty
            1. Powiat Żytowy
                1. Germanja, niewielka acz pobudowana i nowobogacka wieś kilkanaście kilometrów od Pirogów
                    1. Ulica Piroska
                        1. Przybytek Szczęścia, gdzie mieszka Czesław Czepiec - alfons acz kapłan Iliusitiusa w przebraniu.
            1. Powiat Myśliński
                1. Bażantów
                    1. Wielki Mrówkowiec Socrealistyczny, gdzie zabunkrowała się Wanda (bo do Kopalina nie może wrócić)

# Skrypt

- brak

# Czas

* Opóźnienie: 1 dni
* Dni: 2