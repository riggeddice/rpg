---
layout: inwazja-konspekt
title:  "Ukradziona Apokalipsa"
campaign: corka-lucyfera
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170620 - Pułapka na Luksję (HS, Kić(temp))](170620-pulapka-na-luksje.html)

### Chronologiczna

* [170620 - Pułapka na Luksję (HS, Kić(temp))](170620-pulapka-na-luksje.html)

## Koncept wysokopoziomowy misji

-

## Kontekst ogólny sytuacji

Luksji udało się sformować potężny kult satanistyczny, na odpowiednim poziomie jaki jest jej potrzebny. Już widoczna część potrzebnej Luksji populacji zmieniona została w thralle. 

Poziom samowoli i anarchii w mieście się ostro wzmocnił; pomiędzy satanistami, thrallami, Luksją i dziwnymi rzeczami które się dzieją jest poczucie, że nikt nie kontroluje sytuacji. Siły satanistyczne okrzepły i są potężną siłą w okolicy. Postępy Luksji się nieco zatrzymały między wszystkimi innymi rzeczami, które czarodziejka próbuje zrobić.

Luksja WIE, że Siwiecki jest magiem. Nie wie jakim i gdzie jest. Było ich dwóch. Ma jednak demona do pomocy.

Tymczasem do akcji wkraczają dwie dodatkowe siły - Lucyfer oraz przedstawiciel władz, bardzo próbujący się wykazać...

Ksiądz Zenobi Klepiczek jest zabójczo zakochany i zafascynowany Estrellą.

Apokalipci są na dobrej drodze do doprowadzenia do apokalipsy psinoskiej - artefakt już się uruchomił. Niosący Światło nie dał rady podłożyć śladów na "demona", jakkolwiek go pozyskał i podłożył do Storczyna. Z pewnością jednak trudno mu cokolwiek udowodnić. Tymczasem Antoni Bieguś zaczął zaprowadzać porządek w mieście brutalną siłą i prawie spacyfikował okolicę; ścina się z satanistami o władzę.

* KF: 8: Szerokie zainteresowanie. To miejsce stanie się obszarem pielgrzymek ZARÓWNO satanistów JAK I sekciarzy i religijnych.
* Henryk i Estrella są Ranni
* Zenobi Klepiczek jest bardzo całuśny wobec Estrelli
* Okolice kościoła i plebanii są wyniszczone i uszkodzone
* Lis'ha'ory przekształciły się w zintegrowaną istotę ptakopodobną, żywiącą się Magią Krwi. Odleciało toto.
* Luksja jest pod silną kontrolą Henryka
* Zbliża się magiczna burza.

## Punkt zerowy:

**Apokalipci Lucyfera**: 

* Konwersja satanistyczna:              5/5
* Transformacja w thralle:              8/8
* Przygotowanie artefaktu:              4/4
* Zdobycie władzy politycznej:          6/6
* Uruchomienie Artefaktu:               5/15
* Rozprzestrzenienie Wiary:             0/8
* Something has survived:               0/8
* Anarchia w mieście:                   7/12
* Potężna Furia Illuminati:             0/10

**"Niosący Światło"**:

* Schowanie linka:                      6/8
* Pozyskanie demona:                    4/4
* Fałszywe ślady (demon):               1/6
* Usunięcie śladów o sobie:             5/10
* Znalezienie Biegusia:                 3/8
* Perfekcyjne alibi:                    0/10
* Usunięcie Luksji:                     0/10

**Dyktatura Barona Czosnku**: 

* Brutalna siła porządkowa:             6/8
* Władza polityczna w mieście:          4/8
* Pokój przez tyranię:                  3/8
* Jest jeden władca wszystkiego:        3/8
* Oczyszczenie Iliusitiusa:             2/10
* Niepodważalna pozycja:                0/10

**Burza magiczna**:

* Skażenie magią:                       2/12
* Pryzmat staje się prawdą:             0/10
* Randomowe artefakty:                  0/10
* Trwałe Skażenie:                      0/10

**Zespół**

* Pryzmat antysatanizmu                 4/6                 Tier 1

## Misja właściwa:

**Dzień 1:**

Poranek (1 akcja):

Siwiecki i Diakon obudzili się rano. Spętana Luksja, niezdolna do działania. Świeże plotki - o brutalności wobec satanistów. "Oddziały Kościoła" napierniczają satanistów ile wlezie. Próg Skażenia ze zbliżającej się burzy da się wyczuć. To wszystko prowadzi do zwiększenia anarchii w mieście; ludzie konsolidują się powoli wokół "Oddziałów Kościoła" (nie prawdziwego kościoła) i wokół samych satanistów.

Czas przesłuchać Luksję. Ta jest związana, bezradna w podziemiach. Nie czaruje. Henryk i Estrella ścięli się; Henryk chciałby ją troszkę skopać - Estrella się absolutnie nie zgadza. Straszenie zdaniem Henryka jest bezużyteczne. Estrella powiedziała, że nie należy jej bić ani kopać - to jest niskie. Chcą ją przesłuchać, nie pobić. Henryk smutny.

* Głupia sucz i tyle. - Henryk
* Prawdopodobnie wykorzystana - Estrella
* Wszystko na 'nie' dzisiaj. Nic nie mogę zrobić na co mam ochotę... - Henryk
* Mało ci, że już oberwała od nas? Nie musimy być bardziej groźni. - Estrella

Niestety, Luksja wygląda na bardzo odporną na zastraszanie. Wyśmiała Henryka, bo ona już była w piekle. Henryk błyskawicznie zmienił taktykę - trzeba ją wychytrzyć. Zaczął udawać, że ona go przekonuje i zaczął ją wmanipulowywać.

* Luksja: Prostolinijna (-1), Przyzyczajona do walki (-1), Sceptyczna (1), Wytresowana (1) -> 2.
* Henryk: (5) -> S.

Luksja dużo powiedziała Henrykowi.

* "Lucyfer" jest jeden. Ona nie ma do dyspozycji żadnych magów. On też chyba nie ma. Ma ją.
* Satanistami dowodzi Łukasz Tworzyw. Kiedyś wierny w kościele, został przekształcony.
* Sataniści są szczerze wierzący. Jest to kult apokaliptyczny. Wiedzą, że zginą. Chcą tego.
* Niedawno jakiś lokalny polityk zaczął jej bruździć. (Bieguś). Nie zdążyła go dorwać.
* Apokaliptyczny artefakt został uruchomiony. Ona wie gdzie - w starej megafabryce czosnku. 
* Apokaliptyczny artefakt zabije thralle i satanistów. Całą energię wtłoczy do piekła. W nią. Lub innego najbliższego maga.
* "Lucyfer" jest za Storczynem. Ma posiadłość w Boczkach Przednich.
* Luksja zna go od zawsze. A raczej tak myśli. Henryk znalazł w jej wspomnieniach... luki, kolizje. Ktoś się nieźle bawił.
* Nie ma w niej głodu Magii Krwi. Powinna być uzależniona. Nie jest.
* To nie "ona" używała Magii Krwi. Jest tam ktoś inny. Coś innego. Ona nie jest uzależniona.

Estrella poprosiła Mikado o przybycie asap. Obiecał pojawić się rano.

Południe (1 akcja):

Henryk zdecydował się użyć swoją ulubioną szkołę magii. Zdominować Luksję. Henryk wmanipulował Luksję, że to ON jest Lucyferem, przeedytował wstecznie jej pamięć i ogólnie wykorzystał autorytet Lucyfera. Udało się. (-1 surowiec, utrwalenie).

Estrella udała się do miasta z plebanii. W barze Gulasz poszukała gdzie jest ktoś odpowiednio szemrany. Ploty. Usiadła plecami do ściany ze szklaneczką wody. Jest przygotowana na kłopoty i kłopoty się dosiadły. Gdy gość zaczął ją kiepsko podrywać i położył rękę na jej ramieniu, Estrella zagrała żonę gangstera - zimno i pokazując mu broń. 4v2->S. Gość jest zastraszony. Wziął rękę z ramienia Estrelli jak oparzony i wybąkał, że on już pójdzie. Estrella kazała mu zostać; ma znaleźć ciężarówkę na wieczór. 3v2->S. Będzie.

Zdecydowali się z Estrellą pójść do fabryki i zajumać artefakt. I go wywieźć. "Lucyfer" (Henryk) pokazał Estrellę Luksji. Na jej nieme pytanie, powiedział, że to nowy nabytek. Gdy Luksja zażądała zrobienia herbaty, "Lucyfer" powiedział, że potem. Teraz czas iść.

Wieczór (1 akcja):

Megafabryka. Ciężarówka będzie gotowa. Luksja, Henryk i Estrella. W Megafabryce są sataniści i się satanizują. Henryk wysłał Luksję i Estrellę. Luksja tam powiedziała Estrelli, że ta nie zajmie jej miejsca. Luksja jest zazdrosna :D. Estrella powiedziała, że jest niewolnicą i nie jest dla Luksji zagrożeniem. 5v3->R. KF: Wewnętrzny wróg (+1 akcja, dodatkowo: wzmocni to potęgę Barona Czosnku wobec satanistów).

Luksja kazała odczepić poświęcanego bezdomnego i przenieść satanistom Artefakt (w formie dużego, mosiężnego 'planetarium' o wadze 150 kg i z silną energią magiczną). Sataniści przenoszą to i się parzą; Luksja zignorowała radę Estrelli by użyli szmat mówiąc, że Lucyfer tego pragnie. Kazała 1 satanistę poświęcić i poszła. Henryk się nie zgodził na takie rozwiązanie (Estrella powiedziała, że odmawia poświęcania ludzi). Brutalnie skopał Luksję i kazał jej odwrócić rozkaz.

Potem pojechali ciężarówką z artefaktem i Luksją.

Noc (1 akcja):

Estrella nie chce oddać Luksji Siwieckiemu. Acz na razie nie będzie oponować. Siwiecki chciałby Luksję dla siebie, jako minionka. Estrella chciałaby, by Luksji ktoś docelowo pomógł - jakiś starszy ostry mag, który jednak będzie "dobrym kochanym tatusiem".

Przyjechał Mikado fordem fiestą. Przekazali mu Artefakt do rozłożenia w Millennium. Ale diabli wiedzą co robić z Luksją. Henryk kazał Luksji znowu zejść na kolana, skopał trochę i ją związał. W ten sposób jest bezpieczniej. Kierowca ciężarówki (wynajęty) wszystko widział.

Mikado pojechał z Artefaktem. A Estrella i Henryk pojechali do Świecy zdeponować wspólnie Luksję... a potem poszli spać.

(+1 akcja)

**Stan Ścieżek:**

1. Apokalipci Lucyfera:
    1. Konwersja satanistyczna: 5/5        MAX
    2. Transformacja w thralle: 8/8        MAX
    3. Przygotowanie artefaktu: 4/4        MAX
    4. Zdobycie władzy politycznej: 6/6    MAX
    5. Uruchomienie Artefaktu: 6/10        ABORTED
    6. Rozprzestrzenienie Wiary: 3/8
    7. Something has survived: 1/8
    8. Anarchia w mieście: 8/12
    9. Potężna Furia Illuminati: 0/10
2. "Niosący Światło":
    1. Schowanie linka: 8/8                MAX
    2. Pozyskanie demona: 4/4              MAX
    3. Fałszywe ślady (demon): 2/6
    4. Usunięcie śladów o sobie: 5/10
    5. Znalezienie Biegusia: 5/8
    6. Perfekcyjne alibi: 1/10
    7. Usunięcie Luksji: 0/10              ABORTED
3. Dyktatura Barona Czosnku:
    1. Brutalna siła porządkowa: 8/8       MAX
    2. Władza polityczna w mieście: 5/8    ABORTED
    3. Pokój przez tyranię: 3/8
    4. Jest jeden władca wszystkiego: 4/8
    5. Oczyszczenie Iliusitiusa: 5/10
    6. Niepodważalna pozycja: 0/10
4. Burza magiczna:
    1. Skażenie magią: 4/12
    2. Pryzmat staje się prawdą: 2/10
    3. Randomowe artefakty: 2/10
    4. Trwałe Skażenie: 0/10

**Interpretacja Ścieżek**:

Apokalipsa zapobiegnięta. Udało się wyprowadzić Artefakt poza promień rażenia i przekazać do Świecy. Tak samo Luksja została tam przeniesiona, poza zasięg Lucyfera. Apokaliptom udało się zacząć rozprzestrzenianie swojej Wiary też poza teren Storczyna. Jednak siły Biegusia dały radę skutecznie opanować Storczyn brutalną siłą; politycznie wygrali Apokalipci, acz Biegusiowcy się nie poddają. Anarchia prowadzi do małej wojny domowej.

Nadchodząca burza magiczna prowadzi do efemeryd itp; Tymczasem "Lucyfer" dał radę odsunąć od siebie skutecznie 'osłonę' Luksji i konsekwentnie buduje sobie alibi i odsuwa ślady od siebie. Chce zniknąć.

# Progresja

* Luksja Pandemoniae: zniewolona, w Świecy.
* Estrella Diakon: szacun za zdobycie i przechwycenie Luksji. Też... a co zrobiła z Artefaktem Apokalipsy Psinoskiej?!
* Henryk Siwiecki: szacun za zdobycie i przechwycenie Luksji. Też... a co zrobił z Artefaktem Apokalipsy Psinoskiej?!

# Streszczenie

Artefakt Apokalipsy działa. Nie da się tego łatwo zatrzymać, więc... Zespół go ukradł i przekazał Mikado (do Millennium). Luksję zawieźli do Świecy - zacznie się wielka wojna o to, jaki będzie jej przyszły los. Luksja jest ślicznie wytresowana przez "Lucyfera" i nie ma swojej pamięci; o dziwo, coś ją ekranuje od Magii Krwi. Ale co? Tymczasem w miasteczku trwa już jawna wojna między Apokaliptami i ludźmi Biegusia. Politycznie Bieguś przegrał. Ale on ma rząd dusz.

# Zasługi

* mag: Henryk Siwiecki, udający Lucyfera i ostrzący sobie zęby na Luksję. Pokazał wysoką skuteczność i ograniczoną moralność. Skopał Luksję.
* mag: Estrella Diakon, zmusiła Henryka do uratowania satanisty i udawała żonę gangstera by zdobyć ciężarówkę. Ma własne plany wobec Luksji.
* mag: Luksja Pandemoniae, zmanipulowana by powiedzieć prawdę Henrykowi; pod wpływem jego magii uwierzyła, że to ON jest Lucyferem.
* mag: Mikado Diakon, wsparcie Estrelli. Pojawił się, by zabrać Artefakt do Millennium - Draconis najpewniej potnie go na żyletki ;-).

# Lokalizacje

1. Świat
    1. Primus
        1. Lubuskie
            1. Powiat Szarczawy
                1. Storczyn Mniejszy
                    1. Fabryczny
                        1. Bar Gulasz, gdzie Estrella załatwiła (jako 'żona gangstera') ciężarówkę
                        1. Stara Megafabryka Czosnku, w której znajdował się Artefakt Apokalipsy
                    1. Duchowy
                        1. Plebania, w podziemiach przetrzymywano Luksję.

# Czas

* Dni: 1

# Frakcje:


## Apokalipci Lucyfera

### Koncept na tej misji:

Siły wspierające pierwotny koncept Luksji i Lucyfera, dążące do końca świata i wariantu Psinoskiego.

### Scena zwycięstwa:

* Miasto spowija krwawa poświata; dochodzi do rytuału transcendencji.
* Luksja LUB Bieguś absorbują energię miasta. Ktoś staje się efektywnie potężnym czarodziejem lub bytem Krwi.
* Sataniści sprowadzą koszmar na ziemię

### Motywacje:

* "Lucyfer będzie zadowolony. Jego wola stanie się prawdą. Chwała Krwi!"
* "Jesteś z nami lub przeciw nam. Chwała Niosącemu Światło."

### Siły:

* Łukasz Tworzyw, najwyższy kapłan-satanista, w służbie Luksji... już nie do końca.
* Sataniści Łukasza, duża grupa lojalnych satanistów.
* Thralle Luksji, Zmienieni w odpowiedniki Krwawców, ale przekaźniki energetyczne.

### Wymagane kroki:

* Skonwertowanie odpowiedniej ilości ludzi na satanizm: 5
* Transformacja indukcyjna odpowiedniej ilości ludzi w thralle: 6
* Zdobycie władzy politycznej: 6
* Przygotowanie Artefaktu Psinoskiego: 4
* Uruchomienie Artefaktu Psinoskiego: 5

### Ścieżki:

* **Konwersja satanistyczna**:      5/5:                 MAX
* **Transformacja w thralle**:      3/8:                 Tier 1
* **Przygotowanie artefaktu**:      1/4:                 
* **Zdobycie władzy politycznej**:  1/6                  
* **Uruchomienie Artefaktu**:       0/10                 


## "Niosący Światło"

### Koncept na tej misji:

Ustatkowany mag, pragnący zdobyć większą władzę i silnie zmieszany pojawieniem się faktycznych terminusów w okolicy.

### Scena zwycięstwa:

* Luksja lub inny mag wchłania energię psinoską i wpada pod jego kontrolę; rozerwana ochrona rodziców
* Luksja jest niemożliwa do powiązania z samym magiem
* Osłabienie jakichkolwiek przesadzonych efektów tego, co się tu stało; zatarcie śladów

### Motywacje:

* "Co tu się dzieje - przesadzone, przesadzone!"
* "Przecież nikt nie może mnie powiązać z tym co tu się dzieje!"
* "Nareszcie, potęga i władza. Cudzymi rękami."

### Siły:

* Ustatkowany mag. Materia + Mentalne + Infomancja
* Golemiczni Słudzy.

### Wymagane kroki:

* Schowanie linka pomiędzy Luksją a jej rodzicami
* Pozyskanie potężnego demona "z rynku"
* Zrzucenie winy na potężnego demona; fałszywe ślady
* Usunięcie danych o Lucyferze spośród miasta
* Znalezienie innych magów
* Dominacja innych magów

### Ścieżki:

* **Schowanie linka**:                  0/8: 
* **Pozyskanie demona**:                0/4: 
* **Fałszywe ślady (demon)**:           0/6: 
* **Usunięcie śladów o sobie**:         0/8: 
* **Znalezienie Biegusia**:             0/8: 


## Dyktatura Barona Czosnku

### Koncept na tej misji:

Dowodzona przez lokalnego barona czosnkowego grupa samopomocowa i ochronna próbująca zapewnić bezpieczeństwo i stabilną sytuację. Próbują zająć kluczowe tereny i opanować sytuację. A Bieguś ma tajną broń - ołtarz Iliusitiusa.

### Scena zwycięstwa:

* Bieguś zdobył pełną władzę polityczną w mieście; w kryzysie okazał się kompetentny i zorganizował wokół siebie pewne siły.
* Bieguś dał radę zniewolić Luksję i innych magów.
* Brutalna siła pacyfikuje satanistów i thralli; ludzie rozumieją ideę.
* Bieguś przejmuje kontrolę nad miastem.

### Motywacje:

* "To moje miasto i będę je chronił tak, jak jestem w stanie"
* "Dzieje się tu coś dziwnego. Ale ja też mogę z tym poradzić"
* "Pokój przez tyranię."

### Siły:

* Antoni Bieguś, lokalny baron czosnkowy. Protomag.
* Ołtarz Iliusitiusa, niszczący zjawiska paranormalne.
* Agencja ochrony Termos, dość brutalna.
* Policja, z radością stająca po stronie kogoś z "władzy".
* Lokalni mieszkańcy chcący spokoju.

### Wymagane kroki:

* Zwalczanie thralli i satanistów brutalną siłą
* Zwrócenie uwagi Luksji, magów, paranormalnych
* Opanowanie miasteczka i kluczowych miejsc
* Sterroryzowanie mieszkańców i konkurentów ;-)
* Wypalenie wszystkiego potęgą Iliusitiusa
* Dyktatura Biegusia

### Ścieżki:

* **Brutalna siła porządkowa**:         0/8:  
* **Władza polityczna w mieście**:      0/8:  
* **Pokój przez tyranię**:              0/8:  
* **Jest jeden władca wszystkiego.**    0/8:  
* **Oczyszczenie Iliusitiusa**          0/10: 


## Neutralna

### Ścieżki:

* **Skażenie magią**:       2/12                      
* **Anarchia w mieście**:   6/12                      Tier 2


# Narzędzia MG

## Cel fabularny misji

-

## Cel techniczny misji

1. Sprawdzenie Ścieżek i ich przyrostu
1. Sprawdzenie aplikacji

## Po czym poznam sukces

1. Aplikacja pozwala na ukrycie torów i jest bezproblemowa
1. Ścieżki rosną w sposób przesuwający fabułę do przodu

## Wynik z perspektywy celu

1. Sukces. Aplikacja wymaga doprecyzowania, wiem jak: +delta z misji, +ostatnia akcja
1. Proces cybernetyczny. Nowa forma frakcji, oparta o ordered_list markdownowy
1. Nieoznaczone. Wymaga dalszego testowania prędkość ścieżek.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |
