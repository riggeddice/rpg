---
layout: inwazja-konspekt
title:  "Terminus-defiler, kapłan Arazille "
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [150103 - Pryzmat Myśli pęka (An)](150103-pryzmat-mysli-peka.html)

### Chronologiczna

* [150103 - Pryzmat Myśli pęka (An)](150103-pryzmat-mysli-peka.html)

## Misja właściwa:

Pryzmat Myśli - lustro, które w jakimś stopniu było kluczem do przeszłości Kasi - zostało zniszczone przez rezonans z innym lustrem wywołanym przez Arazille. Bogini Marzeń jest ranna, nie wiadomo jak bardzo. Policjanci - niewinni w tym wszystkim - potężnie oberwali i być może jednemu z nich nie da się już pomóc. Wampir Karradraela nie żyje. Wielu kapłanów Arazille nie żyje. Nie może zostać jej więcej niż pięciu, z czego o jednym Kasia wie. 

Jednocześnie Kasia dowiedziała się czegoś zarówno o swojej przeszłości jak i teraźniejszości. Wie, że twarz Arazille przypomina Diakona; czyli coś w jej głębokiej podświadomości kojarzy się z tym specyficznym rodem magów. Dowiedziała się też, że jest już dość Skażona i że powoli przestaje być kwalifikowana jako człowiek w terminologii magicznego stanu. Nie jest to coś co jej się podoba. Kazano jej wierzyć w to czym chce być; tylko w ten sposób nie zmieni się w kolejnego "Anioła" jak Szczypiorek. Jednak tu na drodze staje jej Iliusitius, którego amulet wypacza strukturę Andromedy, dostosowując ją do swoich celów, jak kiedyś starego Szczypiorka.

Andromeda dowiedziała się o istnieniu Triumwiratu jako potężnej siły mafijnej trzęsącej okolicą. Sandra współpracując z Augustem ma nadzieję dowiedzieć się więcej na temat samego Triumwiratu. Wiadomo, że Gabriel Newa, terminus, który podobno nie żyje i borykał się z krwawym uzależnieniem jest członkiem Triumwiratu ale wszystko wskazuje na to, że chce zniszczyć Triumwirat. Wiadomo też, że Triumwirat ma tendencje do porywania ludzi i nie uwalniania ładnych Diakonek.

Kasia wie, że Arazille jest w okolicy i przygotowuje się do wojny. Nie wiadomo z kim i o co. Nadal nieznane jest filakterium Bogini Marzeń. Tak samo nadal nieznana jest natura samej Arazille poza tym, że rekordy na jej temat różnią się w zależności od tego kogo pytasz - zdaniem Sandry i Herberta Arazille to "morderczyni magów", byt stricte anty-magowy. Jednak Feliks Bozur ze świata ludzi przyniósł informację o Arazille jako patronce niewolników. Sama Arazille twierdzi, że Kasia jest jej naturalną wyznawczynią, ale nie powiedziała dlaczego.

Kasia zmieniła centrum dowodzenia - chwilowo dowodzi z Wyjorza, poza zasięgiem Arazille ale z miasta, gdzie znajduje się siedziba główna Triumwiratu. Żonkibór jest zagrożoniem z uwagi zarówno przez Arazille jak i przez Gabriela Newę, a Arazille udowodniła że i ona potrafi korzystać z mocy luster.

Po raz pierwszy w historii Andromedy otaczają ją magowie chętni do współpracy; zarówno ci, którzy muszą (choć robią to z własnej woli) jak Herbert, Patryk i August jak i ci, którzy uważają to za najlepsze rozwiązanie z punktu widzenia ich własnych celów jak Sandra czy Netheria. Andromedzie się to podoba, choć boi się tego co się stanie jak Ołtarz Podniesionej Dłoni zostanie zniszczony przz Arazille.

Zgodnie z tym co powiedziała Netheria Diakon w okolicy zaczynają pojawiać się siły specjalne jej rodu których celem będzie odzyskanie Netherii i Luksji. Jest to potencjalne wsparcie dla Andromedy, lub potencjalne zagrożenie. Tak czy inaczej, Netheria i relacje z Netherią mogą być bardziej istotne niż się wydawało. Niestety, przez błąd Augusta Netheria wie o znaczeniu Kasi. Jednak coś z tego potencjalnie dobrego może wyjść - Sandra i August zaczęli współpracować, Netheria obiecała pomóc psychicznie Augustowi poradzić sobie z jego osamotnieniem a Kasia dostała od Netherii ofertę dołączenia do Millennium.

## Misja właściwa:

Rano, August wrócił do Bazy. Zmęczony, ale uśmiechnięty. Zrobili nocną infiltrację Trójzęba (budynku dowodzenia Triumwiratu). 5/k20 kość szczęścia. Sandra i August zostali wykryci, ale nie zidentyfikowani ani namierzeni. Udało im się zdobyć bazę danych Triumwiratu, która znajdowała się w bardzo słabo chronionej piwnicy; wszystko wskazuje na to, że Triumwirat nie zdawał sobie sprawy ze znaczenia tego co mieli. Na bazie tego co Sandra i August odkryli magowie Triumwiratu uważają za "prawdziwą" bazę Triumwiratu wyższą część Trójzęba. Ta część Trójzęba jest bardzo potężnie zabezpieczona; jest to Rezydencja (rozumiana jako magiczna Rezydencja magicznego rodu), ale uszkodzona. Triumwirat wie, że ta Rezydencja jest uszkodzona i dzięki temu przejęli nad nią kontrolę. Zdaniem Augusta nie da się zaatakować i zdobyć Rezydencji tymi siłami jakie posiadają. Z drugiej strony, August "wie", że mają tylko 2 terminusów, mentalistkę i katalistę. Sensowna ocena sytuacji.

Na bazie tego co Sandra już wydobyła z owej bazy danych Triumwirat przechowuje w Trójzębie około 200 osób wbrew ich woli. Są to magowie, krewni magów, krewni ważnych ludzi... osoby które zapewniają, że wszyscy dookoła będą chętnie współpracowali z Triumwiratem. W Triumwiracie znajduje się też jeden mag krwi, dzięki czemu jeśli ktoś zdradzi, Triumwirat może nie tylko ukarać krewnego ale i przez sympatię połączyć się ze zdrajcą i go skrzywdzić. To się już zdarzyło.
Sandra odkryła też, że osobą, która kazała Sebastianowi Lince blokować wezwania policjantów o wsparcie był Gabriel Newa. Przeszukując informacje na temat tego czy rodzina Gabriela jest w Trójzębie odkryła, że tak. Czyli mają na niego haka. Zdaniem Augusta "i tak ten atak będzie na Diakonów".

Andromeda podziękowała Augustowi za dobrą robotę. Pochwaliła go, a po chwili spoważniała i powiedziała mu, że to co powiedział Netherii mogło Kasię zabić. Lub Netherię. Koniec tematu z jej punktu widzenia. August oddalił się bez słowa, czując się jak chodząca porażka. Poszedł znowu do Sandry "na wypadek jakby jednak nas namierzyli". 

Zdaniem Kasi atak na piwnicę był zbyt udany. Albo to była pułapka, albo Triumwirat jest dużo słabszy niż się wszystkim wydaje i jadą na reputacji i Rezydencji.

Chwilę później do Kasi zapukała Netheria. Poprosiła Andromedę, by ta pozwoliła się Netherii skontaktować z agentami Millennium którzy już z pewnością są w okolicy. Andromeda spytała co jest jej potrzebne - telefon komórkowy. Netheria nie chciała dzwonić bez konsultacji z Kasią, bo Kasia dowodzi. Andromeda poprosiła Netherię o informacje odnośnie grupy uderzeniowej Millennium i tego co oni już wiedzą. Netheria powiedziała, że Diakoni wiedzą o Triumwiracie (co Kasi nie przeszkadza) i o porwaniu Luksji i o planach samej Netherii. Od czasu porwania Netherii przez Gabriela nie mają nic. Nie wiedzą też o Arazille. Operacją najpewniej dowodzi Mirabelka Diakon, matka Luksji. Kasia facepalmowała; nie może dowodzić MATKA. Netheria zauważyła, że Millennium to mała gildia; koło 100 magów (dla Kasi to niewyobrażalna ilość) więc Mirabelka po prostu była jedną z nielicznych zdolnych do działania. Na szczęście - tu nerwowy uśmiech Netherii - Mirabelka potrafi odciąć swoje uczucia i nie jest taka, jak Kasia czy Netheria. Kasia zdecydowała się dopytać o co chodzi. (5#4v8->10). Netheria powiedziała Kasi, że Mirabelka jest terminusem i się przeprojektowała do walki, zmieniając swój organizm. Przez to straciła część człowieczeństwa ale zyskała umiejętności bojowe. Nadal jest dość ludzka by kochać córkę, ale nie jest już taka jak normalni ludzie.

Kasia też poprosiła Netherię, by ta zajęła się i zaopiekowała się Wojciechem na wypadek, gdyby jej coś się stało. Netheria obiecała, że spróbuje mu pomóc ale też wymogła na Kasi (wygrany konflikt, nie konfliktowane), że Kasia nic nie zrobi z Wojciechem dopóki nie dowiedzą się więcej na temat tego co z nim się stało i dopóki Kasi nie przejdzie głębokie Skażenie.

Za zezwoleniem Kasi, Netheria skontaktowała się z Mirabelką. Powiedziała jej, że jest bezpieczna, że została odbita przez ruch oporu (który z uwagi na to że nie jest bardzo silny pozostaje nadal w ukryciu co Mirabelce pasuje) i że ów ruch oporu będzie chciał pomóc w odzyskaniu Luksji. Netheria powiedziała jeszcze, że sytuacja jest pod jakąś tam kontrolą i że właśnie zbierane są informacje. Atak na Trójząb (gdzie znajduje się Luksja) byłby bardzo nierozsądny, gdyż znajduje się tam mag krwi oraz wszystko wskazuje na to, że Trójząb może być Rezydencją. W którym to wypadku wpływ maga krwi na Mirabelkę przez Luksję może być szybszy niż zniszczenie Rezydencji. Netheria poprosiła Mirabelkę o nie wchodzenie do akcji i zaczekanie. I o nie zbliżanie się do Żonkiboru cokolwiek się nie stanie.
Mirabelka przyjęła do wiadomości i powiedziała, że nie zamierza podejmować ofensywnej operacji bez rozeznania sytuacji.

Andromeda poprosiła do siebie Herberta i Netherię i powiedziała im, że ma plan. Muszą złapać Gabriela Newę. Herbert powiedział, że ten plan jest najsensowniejszy jeśli Netheria będzie przynętą. Netheria na to, że z punktu widzenia Gabriela martwa Diakonka jest nawet lepsza niż żywa uciekinierka. Ale jest tu Herbert, terminus, i on będzie lepszą przynętą.
Herbert zauważył, że jeśli to co Andromeda mówi o rodzinie jest prawdą to być może to Gabriel wezwał Arazille i być może to on jest filakterium. Jeśli tak, to nie da się znaleźć miejsca na złapanie Gabriela w taki sposób, by Arazille nie mogła przyjść mu z pomocą - w którym wypadku oni wszyscy mają przechlapane.
Oczywiście, Andromeda wie, że Herbert wymyśli WSZYSTKO by się tylko nie narażać, ale fakt faktem, to jest wariant z którym sobie nie poradzą.

W świetle potencjalnego problemu związanego z potencjalnym filakterium Andromeda zdecydowała się pojechać porozmawiać z Arazille. Być może cel Kasi i cel Arazille faktycznie są spójne. Być może Gabriel Newa jest potencjalnym sojusznikiem. Arazille powinna się zrekonstytuować do tej pory (minął w końcu dzień). Zdecydowała się pojechać samochodem na Festiwal w Żonkiborze. Herbert zgłosił się na ochotnika by jechać z Kasią, ku jej wielkiemu zdziwieniu. Powiedział, że przy takich siłach lepiej by miała ochronę a on jest niewrażliwy na Dotyk Arazille. Netheria zostaje i dostała listę zdjęć Luizy. Jako, że wyciągnęła z głowy Anny Makont komu skonfiskowała Pryzmat Myśli (kto był ostatnim posiadaczem Pryzmatu, potencjalnie złodziejem) to dostała zadanie znaleźć go na zdjęciach. Żeby Andromeda wiedziała o kim mowa.

(Kość szczęścia: 11/k20) Dojechali na Festiwal bez żadnych problemów. A Netheria znalazła osobę o którą chodziło: to gość co sprzedaje kebaby na Festiwalu.
Andromeda znalazła Feliksa Hansona na Festiwalu. Ten z radością powiedział, że Bogini się spotka z Andromedą. Herbert siedzi w samochodzie. Feliks zaprosił Andromedę do Namiotu z Lwami. 

Arazille przywitała Andromedę słowami "Kogo ja widzę, moja marnotrawna wyznawczyni", głaszcząc dwa małe, śliczne kotki. Zapytana przez Kasię co się stało odpowiedziała "Mam wrażenie, że roztrzaskałam czarodziejkę luster... i nie wiem jakie będą implikacje. Naprawdę.". Kasia przyznała Arazille, że to lusterko było dla niej. Nie wie, skąd Anna je miała - skradziono lusterko i Kasia go szukała. Arazille powiedziała, że lusterko MUSIAŁO zostać skradzione Kasi. Nie mogło zostać skradzione przyjaciółce.
Kasia spytała o lokalnego terminusa, ale Arazille nie chciała jej nic powiedzieć. Dopóki Kasia nie powiedziała że WIE z kim chce rozmawiać, zna jego powody (rodzina) i mają wspólne cele, Arazille nic nie mówiła. Ale jak Kasia powiedziała wszystko na jego temat co wie, powiedziała że może z nim porozmawiać. Jest koło samochodu Kasi, koło Herberta.
Arazille jeszcze powiedziała Kasi, że ta MUSI przynieść jej Ołtarz jak najszybciej. Bo Arazille może nie zdążyć. Kasia nie wie czemu Bogini Marzeń się aż tak spieszy...
Arazille dodała jeszcze, że nie rozumie fenomenu Kasi. Jej zdaniem fenomen polega na tym, że nie stało się z nią to co z Wojciechem Kajakiem. Ale dlaczego to fenomen, nie wyjaśniła.

Andromeda wraca do samochodu spotkać się z Gabrielem. Po drodze skomunikowała się z Augustem, by ten przywiózł jej Ołtarz Podniesionej Dłoni. August zaraportował, że zostali zaatakowani - odparł atak (jak?) i wraz z Sandrą uciekają. Ma zamiar ją schować i wywieźć w nocy do terenów Świecy. Kasia go zatrzymała - Sandra nie może się od Kasi oddalić. Wszystko się popsuło. August zaczął narzekać na Arazille i jej upiorne chore zabawy. Kasia nie skomentowała. To nie był moment na przyznanie się. Nowy cel - August ma się schować z Sandrą, ona pokombinuje.
Andromeda skontaktowała się też z Herbertem. Stary terminus powiedział jej, że koło niego jest Gabriel i niestety, Kasia nie będzie mogła z nim porozmawiać. Herbert nie ma możliwości utrzymania Gabriela przy życiu jak uruchomi tą pułapkę. Kasia mu nie pozwoliła tego zrobić, bo to potencjalny sojusznik. Herbert obiecał rozmontowanie tego by pokazać Gabrielowi gest dobrej woli i jak blisko Gabriel był śmierci.

Gdy Kasia dotarła do samochodu, terminusi jeszcze się kłócili o pułapkę. Skutecznie ich podeszła, tak byli zacietrzewieni. Gabriel przywitał Kasię chłodno, lecz bez wrogości, po czym zaczął się jej uważnie przyglądać. Zapytany powiedział, że ma wrażenie, że gdzieś już Kasię widział, ale nie wie gdzie. Kasia przyjrzała się Gabrielowi, ale nie była w stanie go rozpoznać. Nigdy wcześniej nie widziała tego maga. Gabriel nosi okulary.
Andromeda spytała Gabriela, czy jego celem jest uderzenie w Triumwirat i gdy ten potwierdził, zaproponowała współpracę. 
Gabriel powiedział, że Andromeda musi jak najszybciej pozbyć się amuletu - flara luster która uderzyła w Kasię uderzyła też w amulet a im szybciej uda się zareagować tym mniejsza szansa że Iliusitius zdąży zadziałać i zaadaptować. Powiedział też Kasi, że uważa że kiedyś była magiem - ma za małe zmiany jak na jej ilość tkanki magicznej (co podała mu Andromeda). Zaproponował jej, że jak się uwolni może dać mu strzykawkę swojej krwi (tu Herbert kategorycznie zaprotestował) - on jak i większość znających się na rzeczy defilerów będzie w stanie powiedzieć jaka jest jej prawdziwa natura, jaka jest jej przeszłość i skąd pochodzi. Herbert podchwycił "defiler" i Gabriel powiedział, że do przeżycia musi zabić średnio jedną osobę na tydzień. Tylko dlatego, że Arazille go stabilizuje psychicznie. Herbert ostrzegł Gabriela, że jak Arazille go opuści to najpewniej ten stanie się krwiożerczym potworem - Gabriel powiedział, że ma dość woli (nie ma). Ale potem Gabriel powiedział, że jest filakterium Arazille i że uderzenie luster uszkodziło i jego. To powoduje, że mają słabszą akumulację mocy i że Arazille najpewniej nie zdąży go opuścić, ale nadal potrzebuje jak najwięcej mocy by stawić czoła Triumwiratowi. 
Gabriel obiecał Kasi że spróbuje spowolnić pościg za Sandrą i Augustem. Powiedział też, że dla swojej rodziny jest zdolny do każdego okrucieństwa. Gdy dowiedział się, że Netheria skontaktowała się z Diakonami, zasępił się. Netheria jest idealistką. 
Gabriel powiedział jeszcze Kasi, że będzie potrzebowała Amelii do pomocy Wojciechowi. Chwilowo Amelia Eter jest faboklem Arazille, ale Arazille przez uszkodzenie jeszcze nie jest w stanie jej uwolnić; mogłaby ją zniszczyć. W tej rozmowie też wyszło, że Arazille na swoim terenie wie wszystko (widzi wszystkich którzy marzą).
Rozstali się w porozumieniu.
W drodze powrotnej Herbert powiedział Kasi, że Gabriel nie oczekuje tego przeżyć.

Kasia jeszcze spytała Herberta o to, jaki jest sens by magowie wymazywali dane z internetu. Herbert powiedział, że czasami są informacje bardzo niebezpieczne, np. rytuały magii krwi i na niektóre słowa kluczowe są postawione demony. Jeśli ktoś szuka tych fraz, demony się uaktywniają i albo informują magów albo same sprawdzają intencje osób szukających. Jeśli te wiedzą czego szukają, to demony mają możliwość od skasowania pamięci aż po egzekucję. Kasia spytała o "Iliusitius" i "Arazille". Herbert potwierdził, że są to słowa kluczowe zastrzeżone. Powiedział Kasi, że jest mu przykro, ale ktokolwiek tego szukał jest już w rękach magów lub służy jako pułapka na Kasię.
Kasia nie zgadza się na taki los. Poprosiła Herberta by sprawdził czy Feliksowi Bozurowi nic się nie stało. Ten odmówił - chce być z Kasią i ją chronić. Kasia gorzko zauważyła, że Herbert jak będzie wolny nie będzie już nic musiał dla Kasi zrobić. Terminus obiecał jej, że tego nie zostawi, że tą jedną rzecz dla Kasi zrobi - ale teraz Kasia potrzebuje go bardziej.

Kasia ustaliła, że po Ołtarz Podniesionej Dłoni jedzie Kasia, Samira, Sandra i August. Herbert zostaje na miejscu jako główny taktyk (razem z Netherią i Patrykiem). Herbert niechętnie, ale się zgodził. Jest najlepszym taktykiem jakiego ma Kasia - plus, on szanuje Gabriela a Gabriel szanuje Herberta, będą w stanie współpracować.

Zanim Kasia wyjechała, o chwilę poprosiła ją blada Netheria. Kasia posłuchała. Netheria powiedziała, że Mirabelka Diakon dogadała się z Triumwiratem robiąc po prostu pokaz swoich sił i prezentację marketingową. Triumwirat zgodził się na sojusz - oni uwolnią Luksję, Millennium wzmocni obronę Triumwiratu i będą ściśle współpracowali zarówno technologicznie jak i interesami. W pewien sposób, Millennium i Triumwirat będą działały jak jedna gildia, tylko Millennium na Śląsku a Triumwirat tutaj. Do tego poziomu, że aż mają pojawić się magowie zdolni do postawienia Bram magicznych i do readaptacji Rezydencji dla Triumwiratu. Jednak Netheria wybłagała u Mirabelki, by ta (za bazę danych z Triumwiratu) wzięła na siebie atak i ukryła informacje o tym, że Triumwirat ma przeciwko sobie ruch oporu. Mirabelka się zgodziła. Po pierwsze, jeśli Triumwirat jest tak słaby że ruch oporu jest w stanie go zniszczyć to jest niewarty fuzji. Millennium może po prostu jakoś Triumwirat wchłonąć. Po drugie, Mirabelka zgadza się, że sojuszników zdradzać nie wolno.
Netheria powiedziała, że ona zostaje z "ruchem oporu". Jest im to winna i taką podjęła decyzję i jeśli coś jej się stanie, działała jako czarodziejka a nie jako mag Millennium. Mirabelka powiedziała jej, dość pragmatycznie, że jej pasuje - jeśli ruch oporu wygra, to Millennium miało tam agenta. Jeśli przegra, to Millennium ma fuzję z Triumwiratem. Mirabelka też dodała, że zrobi co może by Netherii nie stała się krzywda. Da też tydzień opóźnienia zanim Millennium zacznie wzmacniać Triumwirat.
Tyle może zrobić.

Słysząc to, Kasia ostrzegła Gabriela. Ten powiedział, że tym ważniejsze jest by Kasia jak najszybciej przywiozła Ołtarz Iliusitiusa. Gabriel ma jakiś plan związany z tym Ołtarzem...

# Lokalizacje:

1. Wyjorze
    1. peryferia
        1. dom, duży, wynajęty o pow. całkowitej ok. 320 mkw. Zwany Bazą.
            1. działka przy domu, 400 mkw.
    1. centrum
        1. "Trójząb", forteca Triumwiratu.
1. Żonkibór
    1. Festiwal Marzeń
    1. Namiot z Lwami (gdzie jest mnóstwo małych kotków i ani jednego lwa)

# Zasługi

* czł: Kasia Nowak, konsolidująca siły i decydująca się na to, po której stronie stanąć.
* mag: Sandra Stryjek jako typowy nerd grzebiący w bazach danych po udanym włamie.
* mag: August Bankierz jako terminus zdolny do ewakuowania Sandry i wymyśleniu drogi ucieczki.
* mag: Mirabelka Diakon jako terminuska, matka Luksji i "nie taka jak Netheria czy Kasia". Uzyskała sojusz Millennium i Triumwiratu za odzyskanie córki.
* mag: Luksja Diakon jako córka terminuski i obiekt przetargowy do zawarcia sojuszu Millennium i Triumwiratu.
* mag: Netheria Diakon, która wybrała Andromedę nad wysokopoziomowe plany Millennium. Lojalna ideałom, nie polityce.
* mag: Herbert Zioło, który zaczyna przekonywać się do Andromedy i wykazuje własną inicjatywę. Lepszy taktyk niż się zdawało.
* czł: Feliks Hanson, kapłan Arazille który służy za punkt kontaktowy z boginią.
* vic: Arazille, patronka niewolników o nieznanej motywacji, która pomaga Gabrielowi w wojnie przeciw Triumwiratowi i jego własnej naturze.
* mag: Gabriel Newa, terminus-defiler który stoi za Arazille i za wojną przeciwko Triumwiratowi. Uszkodzony przez lustra; najpewniej umrze.
* czł: Amelia Eter, która może być kluczem do wyleczenia policjanta z lustrzanej przypadłości. Choć nie wiadomo czemu.
* czł: Feliks Bozur, który być może stał się pułapką magów z uwagi na zakazane słowa których szukał w internecie.