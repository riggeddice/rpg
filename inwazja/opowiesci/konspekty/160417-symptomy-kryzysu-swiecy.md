---
layout: inwazja-konspekt
title:  "Symptomy kryzysu Świecy"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

## Kontynuacja

### Kampanijna

* [160406 - Najprawdziwszy sojusz Blakenbauerów (HB, KB, AB, DK)](160406-najprawdziwszy-sojusz-blakenbauerow.html)

### Chronologiczna

* [160411 - Sekret śmierci na wykopaliskach (AW)](160411-sekret-smierci-na-wykopaliskach.html)

### Inne

Kończy się we wczesnych okolicach misji:
* [160412 - Spleśniała dusza terminuski (SD)](160412-splesniala-dusza-terminuski.html)

## Kontekst misji

- Ktoś napuszcza na siebie Świecę i KADEM.
- Ktoś napuszcza na siebie Kurtynę i Szlachtę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.

## Kontekst ogólny sytuacji

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin "szuka" Tymotheusa.
- Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora i Skorpiona do rozwiązania mrocznych aktywności.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Wejście Millennium (dwa stronnictwa), Millennium vs Szlachta vs Świeca vs Kurtyna, walka o teren.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów przez Arazille i Irytkę.
- Ozydiusz trzyma wszystko w garści żelazną pięścią; ukrywa Spustoszenie i informacje o nim.

## Punkt zerowy:

 

## Misja właściwa:

Dzień 1-3:

Andrea siedzi w Składzie budowlanym "Żuczek" - a tak naprawdę w "bazie" Świecy. Jest tam ona i jeszcze jeden terminus; Mieszko Bankierz (dowodzi w łańcuchu). Andrea była ściągnięta tam w ramach rutynowego sprawdzenia czy wszystko się zgadza w papierach itp; szczęśliwie, nie było kłopotów a Mieszko z radością Andrei pomógł w kontroli, sprawdzeniu systemów, magitechów itp.
I wtedy uderzyła Irytka i Andrea dostała zakaz powrotu.

Tak więc Andrea siedzi w "Żuczku". Mieszko się ucieszył i poprosił Andreę o pomoc. Piróg Górny, bogatszy z tych dwóch, był min. kwaterą dla magów po to, by jeśli coś miało spływać rzeką do Kotów magowie mogli to sprawdzić "jako cło". W związku z tym od zawsze Piróg Górny zawiera sieci czujników. Niestety, po Zaćmieniu wszystko trafił szlag. Nadal jest centralne stanowisko dowodzenia Świecy, ale pojawiła się silna niekompatybilność; od tej pory czujniki działają w kratkę. Czy Andrea nie mogłaby pomóc mu udowodnić sabotażu? Lub czegokolwiek? Fakt, jest niekompatybilność między biomózgiem a czujnikami (Świecy zwyczajnie nie stać na to żeby wszystko ponaprawiać), ale czy by Andrea nie mogła pomóc mu udowodnić pewną tezę?

Andrea się zgodziła. Aleksander Sowiński się bardzo ucieszył - przy potencjalnym Spustoszeniu on by chciał mieć sprawne czujniki w okolicy. Mieszko przekazał jej sieć zasobów lokalnych i pomógł. Andrea siadła do biomózgu (biokomputera) i wyszło jej jasno (22), że faktycznie ktoś w nim ingerował. Zanim Mieszko objął urząd, jego poprzednik faktycznie kazał zmienić część raportów czujników. I faktycznie, pojawił się pewien obszar na którym miało to miejsce. Andrea kazała to odbudować biomózgowi używając swoich uprawnień (których nie ma Mieszko). Biomózg zrekonstruował; wszystko wskazuje na to, że poprzednik Mieszka kazał ukryć co najmniej jeden transport rzeką. Sygnatura wskazała na to, że transportowani byli ludzie oraz różne substancje magiczne i artefakty. Przemyt. Nie da się wykluczyć, że transportowani byli magowie... min. dla Oktawiana... 

Andrea też doszła do tego gdzie mniej więcej byli wysadzani, w jakim miejscu niedaleko Piroga. Andrea wydała biomózgowi rozkaz - jeśli kiedykolwiek ktoś spróbuje wyłączyć czujniki tą drogą, niech zwyczajnie będzie sygnał leciał po hipernecie do Andrei. A Andrea ma już pierwszego podejrzanego maga o kooperację ze Szlachtą dla Oktawiana. Wojciech Żądło, były dowódca bazy w Pirogu Górnym.

Resztę czasu Andrea i Mieszko reperowali czujniki. Udało jej się też dojść do tego, że co najmniej jeden czujnik został spalony magicznie przez zaklęcie a nie przez energię / niekompatybilność. Spotkało to kilka czujników, ale Andrea nie widzi w tym wewnętrznej logiki; tak jakby były niszczone bez powodu. Nie dla ukrywania. Bez powodu. Bo zabroń. Po prostu wandalizm.

Analizując dane z biomózgu Andrei wyszło, że to grupa 2-3 magów którzy po prostu niszczą czujniki od czasu do czasu. Po co? Nie wiadomo. Bo mogą?
Andrea z Mieszkiem wymieniła czujniki na sprawne, po czym wprowadziła czujnik pod widoczny czujnik. W ten sposób jest szansa na znalezienie i złapanie wandali. Mieszko poprosił o uprawnienia do użycia mechanicznego terminusa w magazynie, ale Andrea odradza. Mają model przedzaćmieniowy. Piróg nie dorobił się nowego modelu. Może powinien, ale nie ma... poprzedniemu dowódcy po prostu nie zależało.

Andrea zauważyła coś jeszcze w biokomputerze - backdoor wprowadzony przez poprzedniego dowódcę. Jeśli wyśle odpowiednią sekwencję sygnałów do czujników, biokomputer wyłączy te czujniki tak, by transport (przemyt) mógł przejść. Andrea wprowadziła dodatkowo dokładnie te same zabezpieczenia na backdoorze.

Ogólnie, pracowity okres.

Dzień 4: 

Aleksander Sowiński poprosił Andreę o wykonanie pewnego zadania. Skąd Tadeusz Baran wiedział o tym TechBunkrze? Bo dostał sporo kasy (w kategoriach Barana) za to przekazanie informacji. Aleksander zauważył, że Tadeusz Baran działa dla "trzeciej strony" - na pewno nie Szlachty czy Kurtyny. Kim jest ta strona i czym jest to stronnictwo? Sowiński przesłał Andrei informację, że Baran wzmocni Mieszko Bankierza w Pirogu Górnym. Andrea się ucieszyła. Baran po tym mailu zaczął żyć wystawniej; znaczy, wzbogacił się (dostał w łapę). Pojawi się wieczorem.

Sieć czujników jest podreperowana. Nie zieją w niej dziury jak stąd do Australii, choć jej możliwości nie są pełne. 

Z Andreą skontaktowała się tien Zuzanna Maus przez hipernet. Powiedziała, że przeprasza, że przeszkadza ale Andrea interesowała się zawsze rodem Maus i chciała pomóc. Opowiedziała, że Elea opuściła Dzielnicę i poszła do Kompleksu Centralnego Świecy zarzucając seirasowi, że on doprowadzi Ród Mausów do zniszczenia. Elea wykrzyczała, że Baltazar nie był winny; oprócz niego winna była Bankierzówna, ale jej nikt nawet nie oskarżył. Seiras powiedział, że muszą siedzieć cicho na co Elea, że uratuje swoje dziecko. Jej mąż - Rufus - też zniknął, jest gdzieś w Kopalinie. A Baltazar jest przetrzymywany w Kompleksie Centralnym.
Zdaniem Zuzanny, Elea też krzyknęła, że cokolwiek Oktawian miał na myśli (bo ona nie wie), najpewniej miał rację. Bo Mausowie umierają.
Elea i Rufus się po prostu zbuntowali.

Andrea poprosiła Aleksandra Sowińskiego o pełną autoryzację dokumentów odnośnie sprawy z Baltazarem Mausem. Sowiński automatycznie zaakceptował.
Andrea już jedzie do Myślina spotkać się z Janem Weinerem (i dowiedzieć się, czemu skupił ziemię ;p) i jednocześnie analizuje informacje od Sowińskiego. Więc tak:
- Sprawa po jednej stronie ma Rufusa i Eleę Maus (chroniących Baltazara Mausa), po drugiej ma Mieszko, Lauranę i Katalinę Bankierz. Bo oni są opiekunami Malii.
- Elea dotarła do Malii i zaatakowała ją bezpośrednio - jeżeli Baltazar to Malia. Bankierze się wyparli i powiedzieli, że nie ma bezpośrednich dowodów (prawda).
- Trafi to do prawniczego piekła; Seiras Ernest Maus nie wspiera Elei i Rufusa nie chcąc antagonizować.
- Za Bankierzami stoi cała machina rodu Bankierz.

Druga powiązana sprawa:
- Lidia Weiner żąda odebrania Mausom dostępu do Rodziny Świecy ponieważ - jak widać - jest to niebezpieczne.
- Lidia zmontowała koalicję antyMausową, pracuje nad tym jeszcze przed Zaćmieniem.
- Działania Lidii przekraczają ponad Śląsk.

Andrea aż uniosła brwi. Co Mausowie zrobili Lidii? Natychmiast przełączyła kontekst - poszukała informacji o tym, co zgłosił Jan Weiner i co zamierza budować. Chce móc to potem sensownie porównać. Andrea puściła zlecenie do Świecy - wieloetapowe zlecenie.
- Tadeusz Baran: co się stało, jaki był kiedyś... dlaczego Tadeusz Baran jest jaki jest i jak nim sterować?
- Lidia Weiner: czemu aż tak bardzo nastaje na Mausów i jak wpływowa jest?
- Mieszko, Laurana, Katalina: haki na ich temat.

A sama dotarła do Myślina. Tam odebrał ją Karol Weiner, jeden z terminusów i osobista straż Jana Weinera. Zabrał ją do Białego Domu Senatorskiego, gdzie siedzibę ma Jan Weiner. Andrea zorientowała się, że Myślin jest 'secured' - podniesiony stan gotowości bojowej. Plus, są budowane rzeczy...
Spotkanie z Janem Weinerem było odpowiednio kordialne. Seiras Weiner zapytał Andreę, czy jest tu z powodu problemów w okolicy. Powiedział, że słyszał o Spustoszeniu, o problemach ze znikającymi ludźmi, o chaosie w Świecy, problemach z Mausami... więc chce ufortyfikować ten teren przed wszystkim, co może tu wejść.

Karradrael chciał skrzywdzić jego córkę (Julię). It's personal.
Andrea zauważyła, że musiał się solidnie wykosztować; praktycznie całość energii i środków jego organizacji musi na to iść. Zapytany, Jan Weiner powiedział, że przenosi swoją siedzibę do Myślina. On nie ma Rezydencji jak niektóre rody, więc lepiej - przynajmniej - dobrze się ufortyfikować i mieć świetnie chronioną bazę. A nikogo tak naprawdę nie interesuje Myślin; nie ma tu szczególnie wiele magów a on zawsze udostępni teren Świecy jeśli tego potrzebuje. Ale nie Mausom.
Jan Weiner powiedział też, że nie zamierza eksperymentować z energiami idącymi z podziemi; może być to zbyt ryzykowne.

Andrea spytała Jana Weinera, czy lokalizacja Karradraela pozostaje nadal ukryta i nieskanowana. Potwierdził. Zapytał też, czy ona uważa o czymś innym? Nie. Ale zakupy i działania rodu Weiner wzbudziły jej zainteresowanie; stąd czyjeś jeszcze też mogło. 

Jan powiedział Andrei, że sytuacja jest dość zła w całej Polsce. Wszystkie obszary kontrolowane przez Świecę są podgryzane i powoli Świeca zaczyna pokazywać, że nie ma aż tyle magów jak się wcześniej wydawało. They start getting thinner and thinner. Dodatkowo, Jan Weiner uważa, że ktoś się uwziął na jego ród - Zenon Weiner i kilku innych. Andrea zauważyła, że to samo można powiedzieć o Mausach. Ciekawe, ile jeszcze jest takich sytuacji i rodów.

Jan Weiner zaproponował Andrei współpracę - jakby czegoś potrzebowała, dostarczy jej blackboxy do oddania kiedy indziej. Może też pomóc Andrei przez zbieranie informacji ze swojej sieci. 

Andrea wróciła do Piroga. Po drodze przeanalizowała odpowiedzi z Sieci Świecy.
- Lidia Weiner: nie ma informacji na temat tego, czemu tak nastaje na Mausów. Jednak Andrea skorelowała, że to się zaczęło po odwołaniu uczennicy i przyjaciółki Lidii przez Jonatana Mausa i transformacji owej uczennicy w byt dziś znany jako Karolina Maus.
- Lidia jest uznawana za eksperta w swojej dziedzinie. Po tych wydarzeniach skupiała się też na działaniu rojowym Mausów na umysł i na bóstwach (Karradrael, Arazille) - napisała wiele artykułów na temat tego, że Mausowie są zagrożeniem dla siebie i innych magów przez to, że tak naprawdę są końcówkami Karradraela. Postuluje wygaszenie rodu - niech sobie żyją. Ale zakaz dalszej transformacji w Mausów. Niech po prostu zgasną.
- Lidia jest ceniona i uznawana. Jako, że Mausowie mają wrogów, Lidia ma poparcie. Ale ona jest frontwoman, nie jest siłą napędową.
- Tadeusz był przed Zaćmieniem silnym i dość kompetentym terminusem. Dowódcą skrzydła. Niestety, dowodził akcją PODCZAS Zaćmienia. Jego oddział został zniszczony. Jego moc została znacznie ograniczona. 
- Tadeusz chciał odejść na emeryturę. Stracił serce. Jednak gildia nie ma dość magów, więc został powołany przez Świecę. On nie chce. Ale musi. Bo ktoś musi.
- Milena dostarczała mu różnego rodzaju środki odurzające; alkohol, narkotyki - chciał mieć spokój, więc mu to umożliwiła. Dlatego tamta sytuacja tak fatalnie wyszła...
- Mieszko ma dwie partnerki - Laurenę i Katalinę. Laurena jest dużo młodsza; była kiedyś ostra sprawa, gdzie Katalina i Laurena zrobiły coś złego i Mieszko to zatuszował. Wtedy właśnie Laurena została jego kochanką (forma zapłaty).
- Mieszko próbował uzyskać więcej magów do swojej rodziny. Niestety, nic się nie udało; udaremniono gdy próbował zlecić porwanie i przemycić jedno dziecko. Udowodniono mu to, ale ukryto tą informację.
- Mieszko i Katalina są biologicznymi rodzicami Malii. Rzadko zdarza się, by magowie mieli za dziecko maga, więc ją rozpieszczają. Innych dzieci nie "widzą".

Wieczór. Kwatera w Pirogu. Mieszko i Andrea. Tadeusz Baran się spóźnia. Znowu. Pingnięty na hipernecie Baran powiedział, że będzie za 10 minut.
Baran przybył, powiedział, że się spóźnił i Mieszko wybuchł. Gdy tylko trochę ochłonął, Baran powiedział, że się zmęczył i czy może iść na kwaterę. Mieszko eksplodował znowu i wysłał Barana na nocny patrol. Baran wzruszył ramionami i poszedł. Mieszko jest wściekły. I ten gość kiedyś dowodził skrzydłem...

Andrea zdecydowała się pójść z nim na patrol. Baran zaczął rozmowę od "Świeca nie żyje, tien Wilgacz, nawet bezpiecznik powinien to zobaczyć". Wyjaśnił, że stare bóstwa (Arazille, Światowid) unoszą głowy, że kiedyś Świeca broniła wszystkich magów i ich ograniczała - taka jej rola - a teraz skupia się tylko na sobie. Taka Świeca nie jest nikomu potrzebna i zostanie pożarta przez inne gildie i wrócą nowe wojny magów. Andrea zauważyła, że Baran jest w stanie głębokiej depresji. 

Baran powiedział jak to było z tym bunkrem. Przyszedł do niego Żupan, ale Baran się dowiedział, że za nim stała Felicja Diakon z Millennium. Jak? Proste - Żupan powołał się na to, co spotkało Milenę Diakon i na jego poprzednie skrzydło. Baran nienawidzi takich ludzi jak Żupan, więc wbił mu nóż w kolano. Żupan się szybko przyznał do Felicji. A i Felicja się specjalnie nie ukrywała.
Felicja zapłaciła mu kupę kasy za to, by on rozgłosił to do tych magów Szlachty. Ona podała mu magów Szlachty do których ma wysłać wiadomość. On się zgodził - uważa, że jak Szlachta wzrośnie w siłę, to albo Świeca odpowie ogniem albo Szlachta przejmie Świecę. Najgorszy - zdaniem Barana - stan to taki, gdzie Świeca jest podzielona. Nie wiedział o Irytce. Nie wiedział też o tym, że wysyła wiadomość do i Szlachty i Kurtyny.

Andrea wie, że większości tych pieniędzy nie wydał. Gdzieś schował.

"Spierdoliłem, tien Wilgacz. Wiele razy już. To streak. Ale nie da się spierdolić patrolowania lasu gdzie nie ma zagrożeń."

Baran pomoże Andrei. Obiecał. On nie chce zniszczenia Świecy. Dla niego Świeca jest jedyną opcją - on naprawdę nie chce wojen magów. Ale też nie chce już się poświęcać - ma dość.

Aleksander Sowiński przekazał Andrei niepokojące wieści - Millennium weszło do Kopalina. Dostał też wiadomość od Andrei, że niejaka "Felicja Diakon" stoi za tematami z TechBunkra, a dokładniej: Felicja z Millennium. Andrea ma zgryz. I co niby ma z tym zrobić..?

## Wynik misji:

- Jan Weiner militaryzuje Myślin; faktycznie, tam coś się dzieje (działania magów). Weiner zmienia Myślin w fortecę.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga.
- W Pirogu Górnym systemy Świecy średnio działają (już działają). Andrea wykryła korupcję poprzednika Mieszka Bankierza.

# Zasługi

* mag: Andrea Wilgacz, zdalnie rozpracowująca problem Mausów, łącząca wszystkie wątki w jedną całość i zamknięta poza Kopalinem.
* mag: Mieszko Bankierz, dla odmiany kompetentny przywódca Piroga Górnego; odkrył z pomocą Andrei korupcję. Ojciec Malii, partner Laureny i Kataliny.
* mag: Aleksander Sowiński, szef wydziału wewnętrznego na Kopalin który mając Andreę poza Kopalinem próbuje wykorzystać ją najlepiej jak potrafi.
* mag: Wojciech Żądło, poprzedni szef Piroga Górnego; wyraźnie skorumpowany, osłabił system zabezpieczeń i być może współpracował ze Szlachtą.
* mag: Tadeusz Baran, terminus w ciężkiej depresji który jednak nie da sobą manipulować przez jakiegoś tam Żupana. Bardziej inteligentny niż się wydaje.
* mag: Artur Żupan, który dostarczył Baranowi zadanie od "Felicji" Diakon z Millennium. Baran go skrzywdził, ale zadanie wykonał.
* mag: Zuzanna Maus, archiwistka Mausów ostrzegająca Andreę o problemach Mausów w Kopalinie (bunt Elei). Prosi o pomoc.
* mag: Mordecja Diakon, która odkryła dla Elei informacje o tym, że Malia Bankierz była chroniona i tylko Baltazar może zapłacić.
* mag: Lidia Weiner, nienawidząca Mausów od czasu gdy Karradrael na żądanie Jonatana Mausa zniszczył Karolinę Maus.
* mag: Katalina Bankierz, partnerka Mieszka i matka Malii.
* mag: Laurena Bankierz, partnerka Mieszka.
* mag: Malia Bankierz, osłaniana przez swój ród po tym, jak Elea zaatakowała ją (albo ona ucierpi jak Baltazar, albo Bankierzowie jej pomogą).
* mag: Elea Maus, zdesperowana przyszywana matka która zbuntowała się seirasowi i zdecydowała się wesprzeć Szlachtę. Uważa, że Ernest Maus zniszczy Ród.
* mag: Rufus Maus, który zbuntował się seirasowi i zniknął gdzieś w Kopalinie.
* mag: Baltazar Maus, dobrze zamknięty w Kompleksie centralnym Srebrnej Świecy w Kopalinie i oczekujący na przesłuchanie.
* mag: Jan Weiner, seiras przenoszący siedzibę swego rodu do Myślina i fortyfikujący Myślin przed właściwie wszystkim co się da.
* mag: Fortitia Diakon, "Felicja", czarodziejka Millennium, która zażądała od Barana rozpowszechnienia od Barana informacji o TechBunkrze Mausów.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk, pozornie atakowany ze wszystkich stron
            1. Powiat Kopaliński
                1. Piróg Górny, miejscowość z większą ilością sekretów niż ktokolwiek się spodziewał
                    1. Obrzeża
                        1. Skład budowlany Żuczek, gdzie - jak się okazuje - znajduje się ukryta kwatera terminusów Świecy
                            1. Ukryta kwatera Świecy
                                1. Biostanowisko dowodzenia, wykorzystywane jako dowodzenie awaryjne Świecy
                                1. System detekcyjny, rozsiane czujniki dla Piroga i okolic, które częściowo były oślepione przez poprzedniego dowódcę
                                1. Koszary, dla 5 osób (technicznie: terminusów Świecy)
                    1. Las Stu Kotów
            1. Powiat Myśliński
                1. Myślin
                    1. Ligota
                        1. Biały Dom Senatorski