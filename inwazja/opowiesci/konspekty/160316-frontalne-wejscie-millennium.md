---
layout: inwazja-konspekt
title:  "Frontalne wejście Millennium"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Szlachta vs Kurtyna
- Srebrna Świeca Ozydiusza Bankierza
- Powrót Mausów
- Cruentus Reverto

## Kontynuacja

### Kampanijna

* [160309 - Irytka Sprzężona (HB, KB, LB)](160309-irytka-sprzezona.html)

### Chronologiczna

* [160309 - Irytka Sprzężona (HB, KB, LB)](160309-irytka-sprzezona.html)

## Kontekst misji

- Ktoś napuszcza na siebie Świecę i KADEM.
- Ktoś napuszcza na siebie Kurtynę i Szlachtę.
- Anna Kozak winna oddać artefakty Andrei.
- Zenon Weiner zniknie z pola widzenia w drugiej części misji.
- Świeca jest zajęta problematyką Arazille, zwłaszcza Diakoni.
- Ozydiusz Bankierz pracuje nad cruentus reverto.
- Siedmiu magów czeka straszny los; Szlachta oddała ich Oktawianowi Mausowi by przekształcić w Mausów.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Działania archeologiczne Dominika Bankierza pod uważnym okiem Inkwizytor Adeli Maus.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach
- Andrea i oddział Tamary skutecznie odbili magów, których Oktawian chciał zmienić w Mausów

## Kontekst ogólny sytuacji

- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin "szuka" Tymotheusa.
- Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Srebrna Świeca poluje na Arazille, która zeżarła Zieloną Kariatydę ośmieszając Ozydiusza
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).

## Punkt zerowy:

Ż: Co naprawdę fajnego mogą Blakenbauerowie ukraść używając płaszczek Margaret?
K: Coś co wygląda na technomantyczne a jest biologiczne; pancerz wspomagający i adaptujący do użytkownika.
Ż: Co epickiego Blakenbauerowie mogliby uzyskać używając Irytki, ale to zbyt politycznie niebezpieczne by to robić?
B: Przejąć siły terminuskie na terenie Kopalina przez infiltrację i pozorny sojusz.
Ż: Co ważnego znajduje się w TechBunkrze czego jeszcze nikt nie odkrył?
D: Dowody pogrążające Szlachtę; pokaże, że Szlachta jest niebezpieczna.

## Misja właściwa:

Dzień 1:

Kwarantanna w Kopalinie. Lord terminus krwawo uniemożliwia złamanie kwarantanny i chroni Maskaradę jak może. Kurtyna (resztki) i Szlachta też skupiają się na Maskaradzie. Poza tym, że na dorzynaniu siebie nawzajem, co jedynie eskaluje Irytkę.
Margaret ma nowy plan. Wysyła płaszczki na akcję, okradając domy mniej potężnych i mniej możnych magów. 9/k20. Przegrupowuje płaszczki, atakuje domy i niewielkie bazy, okrada co się da i wycofuje. Rezydencja się bogaci.
Taki niskopoziomowy plan odzyskania potęgi Blakenbauerów po działaniach Arazille.

Hektor poprosił Ottona o pozwolenie na negocjacje z Ozydiuszem Bankierzem. By zaleczyć Irytkę i w Rezydencji zacząć badania nad szczepionką na Irytkę; zwłaszcza że specjaliści mogą się pojawić w Rezydencji. Otton się zgodził. I tak nikt się na to nie zgodzi...
Hektor próbował przekonać do tego Klarę i Leonidasa. Nie wychodzi mu. Hektor chce pokój między oboma frakcjami...
Klara i Leonidas mają wątpliwości, czy coś z tego dobrego wyjdzie...

Gdy Hektor powiedział, że chcą wesprzeć Ozydiusza, Leo i Klara spojrzeli na siebie. 

Leo stwierdził, że na trzeźwo się nie da i trzeba dowiedzieć się jak najwięcej o Ozydiuszu. Dowiedział się sporo ciekawostek:
- Ozydiusz non stop dostaje kobiety i ogólnie kobietami gardzi.
- Ozydiusz ma problem z diakońskim cruentus reverto; Millennium też gardzi.
- Ozydiusza nienawidzą wszyscy po równo. 
Dowcip Leonidasa polega na tym, że on chce, by Ozydiusz był święcie przekonany, że Hektor wie o nim WSZYSTKO. 

Klara rozciąga na całym Kopalinie sieć detekcyjną mającą wyłapywać wykrycia Maskarady. I ogólne wykrywanie tego co się da. To nie tylko dowód, że Klara ma plan; to zwiększy możliwości Ozydiusza jeśli pójdzie na to Ozydiusz.

- Szlachta wspiera nowych członków Szlachty; pomagając im zakończyć poprzednie działania. PR # działania (tien Bolesław Derwisz).
- Emilia prosi Blakenbauerów o wsparcie militarne; lub choć polityczne.
- Zabezpiecznie "Maksa Webera" i brutalne wzmocnienie Maskarady.
- nic (Pasożyt)
- Ochrona Maskarady (Mil)
- Edwin odkrywa, że Blakenbauerowie są odporni na Irytkę.

Klara wykryła dwie bardzo interesujące rzeczy swoimi dronami:
- nad Kopalinem są drony. Nie wiadomo czyje, obserwują, z polem niewidzialności, ktoś patrzy. Drony wykryły też sieć Klary.
- Millennium wchodzi na teren Kopalina i też wspiera Maskaradę. Przerzuca siły na teren Kopalina.
- Ozydiusz nic nie wie; a on zwykle ignoruje ludzi; tylko magowie i viciniusy są w jego jurysdykcji.

Hektor porozmawiał z Emilią. Zapewnił ją, że zatrzyma tien Dagmarę Wyjątek; użyje do tego Ozydiusza Bankierza i wesprze go w kwestii zatrzymania tej bezsensownej wojny. Emilia powiedziała Hektorowi wprost, że mu nie ufa. Wszystko co zrobił do tej pory skończyło się bardzo źle.

Edwin jeszcze powiedział przed wyjściem Hektorowi, że Blakenbauerowie są niewrażliwi na Irytkę. Większość magów nie jest; oni są. Nie są na zwykłą Irytkę. Na tą są...
To nie jest podejrzane. Nic a nic.

Margaret się ucieszyła; udało jej się zgromadzić parę fajnych rzeczy z domostw. Za chwałę rodu. Wysłała płaszczki ponownie. 4/20. Tyle wygrać. 

Wieczór: spotkanie z Ozydiuszem. Lord Terminus przyjął Hektora chłodno; Hektor poszedł idealistycznie i z dobrego serca a Ozydiusz pragmatycznie i niechętnie. Gdy Klara pokazała drony a Leo powiedział o wejściu LUDZI Millennium, Ozydiusz się zdenerwował. Zdecydował, że będzie współpracował z Hektorem. Dostarczył im Judytę Karnisz jako oficera łącznikowego. Odesłał Elizawietę do "Maksa Webera" by dowodziła a Judytę przekazał Hektorowi. 
Mają możliwość podparcia się autorytetem Ozydiusza a Judyta wspiera skutecznie i zasobami i kompetencją.

Całkiem nieźle.

Leonidas spróbował przekonać Ozydiusza do tego, żeby Marcelin został oficerem łącznikowym u Ozydiusza. Nie tylko jest szybki kontakt, ale też dodatkowo Marcelin może odciążyć Ozydiusza w sprawach papierkowych z perspektywy kobiet. Leo się przygotował; Hektor mówi słowami Leonidasa używając języka korzyści i potrzeb Ozydiusza.
Lord Terminus jest bardzo zdziwiony nagłym wzrostem kompetencji Hektora.
Marcelin agent łącznikowy...

Leonidas jeszcze sprzedał Ozydiuszowi, że Edwinowi udało się uodpornić Blakenbauerów na Irytkę. Więc Marcelin będzie mógł czarować. A Ozydiusz powiedział, że niedługo uruchomi sztucznych terminusów. Projekt EAM, zawsze zawodny, ale mają sporo w magazynie; w sytuacji awaryjnej się przyda.

Hektor wysłał Emilii informację, że jest poparcie Lorda Terminusa. Koniec działań Szlachty.

Dzień 2:

- tien Wyjątek dowodzi operacją mającą pozbawić Kurtynę środków; skutecznie. Odcięła supply line.
- Kurtyna fortyfikuje się w bazie Emilii.
- Odkrycie, że Blakenbauerowie są odporni na Irytkę. Też: uruchomienie sztucznych terminusów.
- Przechwycenie płaszczek Blakenbauerów
- Wejście i ochrona magów Świecy itp
- Płaszczki robią pro szaber

Margaret się ucieszyła. Przyniosła Hektorowi dokumenty wskazujące o tym, że dwóch magów (unnamed) robiło przestępstwa i działało przeciwko terminuskom Ozydiusza. Hektor się BARDZO ucieszył. Leo też się o tym dowiedział; natychmiast przekazał o tym informacje Ozydiuszowi świadczące o dobrej współpracy Hektora z terminusami Srebrnej Świecy...

Sensory Klary wskazały konstruowaną bramę na terenie Kopalina. Jednocześnie siły Millennium wchodzą w interakcję z magami Świecy. Nie atakują; rozmawiają. Podsłuch wskazuje, że magowie Świecy płacą Millennium za ochronę. Magowie planktonowi. W zamian za to Leonidas uruchamia akcję "płaszczka w każdym domu". Płaszczki Blakenbauerów które zapewniają ochronę tym magom, bo magowie Świecy są bezpieczni.

Ozydiusz pod wpływem tego wszystkiego uruchomił Sztucznych Terminusów.

A Leonidas doprowadził do tego, że szaber Blakenbauerów można zrzucić na Millennium. W końcu ludzie chronią domy magów, ale to tylko ludzie... viciniusy radzą sobie lepiej. Mimo, że idzie to w imię Amandy Diakon, Leonidas poświęcił sporo płaszczek i doprowadził do tego, że opinia mówi, że Millennium chroni. Przed sobą. Przy okazji przed innymi, ale jak nie chronią (bo odmówisz), to Millennium zaatakuje i zniszczą. I że Świeca potrafi chronić.

Tien Dagmara Wyjątek skutecznie wycięła supply line Kurtyny. Mają kilkunastu magów, parę viciniusów i sporo artefaktów. Wiedzą, że Ozydiusz niewiele może zrobić... Judyta ma pewien pomysł. Nie może aresztować magów Szlachty, bo zwyczajnie nie ma mocy. Nie ma sił. Nie ma czym. Ale zaproponowała, by Hektor poświęcił kilkanaście płaszczek - kupił jej 20 sekund, w ciągu których Judyta po prostu podejdzie do Dagmary i ją ranteleportuje artefaktem. Ranteleport będzie bardzo daleki. To doprowadzi do tego, że zadziała ściągający beacon i Dagmara zostanie teleportowana prosto do strefy zero Ozydiusza.

Innymi słowy, Ozydiusz zrobi pokaz siły a Blakenbauerowie to umożliwią.

Tyle, że sytuacja potoczyła się bardzo źle.

Gdy tylko płaszczki zaatakowały, Judyta ruszyła na flankę uderzyć w tien Wyjątek. Niestety, płaszczki obróciły się przeciwko Świecy; płaszczki Blakenbauerów uderzyły w Judytę i zdradziły swoją pozycję w wyjątkowo niekompetentny sposób (tak, że wyglądało to na FAIL a nie na zdradę). Judyta, zaatakowana przez płaszczki i ujawniona przed Dagmarą, musiała ranteleportować; odbiła się od bariery spoza Kopalina i wpadła do strefy zero gdzie chciała wysłać Dagmarę. Innymi słowy... all is fail. Akcja widowiskowo się nie udała a Szlachta uważa, że to był ruch Ozydiusza który totalnie zawiódł.

Jednocześnie, płaszczki w wielu domach magów obróciły się przeciw nim i zaczęły okradać magów jakich mieli chronić. Margaret w rozpaczy krzyknęła, że ich nie kontroluje. System samozniszczenia płaszczek też nie działa; po sprawie z Rezydencją pomniejsze systemy samozniszczenia zostały wyłączone z akcji (płaszczki nie miały systemu jak Zeta; coś dużo słabszego).

Część płaszczek uderzyła też w bazę lorda terminusa Ozydiusza Bankierza. Zostały zmasakrowane.

Kombinacja wizji Klary (detekcja) oraz wiedzy Leonidasa (polityczna wiedza) dała jednoznaczną odpowiedź - najpewniej za tym wszystkim stoi Pasożyt Diakon, deviator Millennium, często współpracujący z Amandą. I faktycznie, jedna z Triumwiratu - Amanda Diakon - też znajduje się w Kopalinie.

Trzeba tylko przekonać Ozydiusza i powstrzymać płaszczki...

## Wynik misji:

- Marcelin jest oficerem łącznikowym u Ozydiusza.
- Ozydiusz / Blakenbauerowie mają sojusz; ale po ostatnich ruchach płaszczek sojusz jest niepewny.
- Amanda Diakon i Pasożyt Diakon wraz z siłami Millennium opanowują fragmenty Kopalina. Millennium uderza.
- Blakenbauerowie dali radę zdobyć dowody dla pogłaskania Ozydiusza; też zdobywają materiały i surowce.

## Dark Future:

Linia Irytki (Plagi):
- Rekwizycja Edwina
- Pierwsze magiczne lekarstwa
- Lekarstwa nie działają
- Badania Irytki; to coś szerszego
- Druga fala lekarstw
- Lekarstwa nie działają

Linia Margaret:
- Margaret wysyła płaszczki i wraca płaszczki

Linia Ambient: (buy)
- 3 - Netheria ogłasza wielki koncert ku czci Wandy Ketran. 

Linia Szlachty:

- Szlachta wspiera nowych członków Szlachty; pomagając im zakończyć poprzednie działania. PR # działania (tien Bolesław Derwisz).
- tien Wyjątek dowodzi operacją mającą pozbawić Kurtynę środków; skutecznie. Odcięła supply line.
- Szlachta fortyfikuje się w X, skutecznie.
- Diana prosi Hektora o wsparcie do zniszczenia bazy Emilii; agenci Szlachty atakują Emilię w jej własnej bazie; zostają anihilowani w całkowicie korzystnej dla siebie sytuacji.
- Szlachta robi katastrofalny błąd atakując bazę Emilii; nikogo tam nie ma poza Polem Statycznym.

Linia Kurtyny:

- Emilia prosi Blakenbauerów o wsparcie militarne; lub choć polityczne.
- Kurtyna fortyfikuje się w bazie Emilii.
- Emilia odcięła Szlachcie wszystkie posterunki które sukcesywnie budowali poza X.
- Dom Emilii płonie i jest bardzo uszkodzony. Systemy zabezpieczeń są spalone.
- Kurtyna łapie Dianę Weiner w niewolę.

Linia Ozydiusza:

- Zabezpiecznie "Maksa Webera" i brutalne wzmocnienie Maskarady.
- Odkrycie, że Blakenbauerowie są odporni na Irytkę. Też: uruchomienie sztucznych terminusów.
- Odrzucenie prośby Diakonów o zwrócenie Cruentus Reverto.
- Ogłoszenie stanu wyjątkowego i zażądanie wstrzymania wszelkich działań bojowych. Żadna strona go nie słucha.
- Powstrzymanie sił Amandy przed odbiciem Cruentus Reverto. Amanda wycofuje swoje siły.

Linia Pasożyta:
- nic
- Przechwycenie płaszczek Blakenbauerów
- Lost Signal. Płaszczki pojawiają się przy X; też Y. Nie da się chronić wszystkich płaszczek.
- Wsparcie Amandy i infiltracja Kompleksu Świecy
- Ewakuacja z Kompleksu Świecy; frame Blakenbauers.

Linia Millennium:
- Ochrona Maskarady
- Wejście i ochrona magów Świecy itp
- Wejście na teren opuszczony przez Świecę i fortyfikacja tego terenu. Linie przerzutowe poza Kopalin.
- Starcia ze sztucznymi terminusami Ozydiusza; przejęcie Wielkiego Muzeum Śląskiego
- Ufortyfikowanie i zabezpieczenie wszystkiego ważnego; linia ewakuacji kralotycznej

# Zasługi

* mag: Hektor Blakenbauer, który dzięki Leonidasowi dostał opinię kompetentnego polityka u Ozydiusza.
* mag: Klara Blakenbauer, która swoją siecią monitorującą wykryła Millennium.
* mag: Leonidas Blakenbauer, który wynegocjował Hektorem wstrzyknięcie Marcelina pod Ozydiusza i ogólnie doprowadził Blakenbauerów do większego poważania.
* mag: Emilia Kołatka, która żądając od Hektora opowiedzenia się po stronie Kurtyny wyraźnie nie zdaje sobie sprawy, że nie jest na pozycji siły.
* mag: Judyta Karnisz, oficer łącznikowy Ozydiusza mająca współpracować z Blakenbauerami. Kończy "zdradzona przez płaszczki" i ranteleportuje do 'Maksa Webera'.
* mag: Amanda Diakon, jedna z Triumwiratu (kralothborn), która dowodzi operacją mającą wprowadzić potęgę Millennium do Kopalina korzystając z Irytki.
* vic: Pasożyt Diakon, bardzo nieludzki vicinius (były * mag) Millennium mający umiejętności kontroli swoich ofiar. Biologicznie niewrażliwy na Irytkę, jak Blakenbauerowie.
* mag: Ozydiusz Bankierz, który uruchomił mechanicznych terminusów i zawiązał tymczasowy sojusz z Blakenbauerami by chronić Kopalin przed Irytką / Szlachtą / Kurtyną / Millennium.
* mag: Dagmara Wyjątek, taktyk Szlachty. Oficer terminus, specjalizuje się w manewrach militarnych i skutecznie masakruje Kurtynę odcinając ich do bazy Emilii.
* mag: Margaret Blakenbauer, która używając płaszczek okrada słabszych * magów by odbudować potęgę Blakenbauerów korzystając z Irytki.
* mag: Marcelin Blakenbauer, który został oficerem łącznikowym u Ozydiusza (Leo ma nadzieję, że przekabaci jakieś ładne terminuski...).

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                      1. Centrum
                        1. Technopark Max Weber
                        1. Kompleks Centralny Srebrnej Świecy
                        1. Pion dowodzenia Kopalinem
                            1. Sala audiencyjna Lorda Terminusa
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji