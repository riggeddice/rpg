---
layout: inwazja-konspekt
title:  "Sleeper agent Oktawiana"
campaign: powrot-karradraela
gm: żółw
players: dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161204 - Zajcewowie po drugiej stronie (AW)](161204-zajcewowie-po-drugiej-stronie.html)

### Chronologiczna

* [160713 - Jak ukraść ze Świecy Zajcewów (AB, DK, HB)](160713-ukrasc-ze-swiecy-zajcewow.html)

## Punkt zerowy:
## Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.
- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.
- The Governess w masce Wandy Ketran się pojawiła. Zmusiła Blakenbauerów do wycofania się z działań w Kopalinie mordując wszystko co oni kochają.
- Andrea dostała Zajcewkommando pod kontrolę; z Bianką Stein i Tatianą


## Misja właściwa:

Dzień 1:

Margaret poprosiła Hektora na słówko. Judyta Maus zaproponowała swoją współpracę w odzyskaniu Patrycji Krowiowskiej ze szponów The Governess. Użycie specjalnego rytuału Mausów (#10% mocy, sygnatura silnie Mausowa, zewnętrzny symbiont demoniczny) plus zdolności Margaret powinny umożliwić odzyskanie Patrycji. Czemu? Bo Judyta jest zmęczona i znudzona jak ostatni łoś. Chce za to więcej wolności i autonomii. 
Gdy Judyta się oddaliła, Margaret zauważyła, że to nieco dziwne; czemu mag zdradza tak łatwo sekrety rodu - naciśnięta może odmówić pomocy ale nie naciśnięta może mieć jakąś nieznaną agendę.

"Potrzebne nam jakieś ofiary losu by to zrobiły, ale skąd my weźmiemy ofiary losu?" - Margaret
"Diana Weiner chce z panem rozmawiać, prokuratorze" - Jan Szczupak

Diana Weiner poprosiła Hektora o zbudowanie grupy płaszczek szturmowych zgodnych z planami Świecy oraz o wysłanie planów czegoś przydatnego, co Diana może wykorzystać do zdobycia Kompleksu Centralnego. Bankierze mają dostęp do Burzyciela Rezydencji który mogą użyć przeciwko Blakenbauerom; w końcu sojusz Blakenbauerowie - Szlachta są powszechnie dostępne. Diana też obiecała Hektorowi, że wyśle ichnią szczepionkę na Irytkę; ale to format Świecy plus niekompletna (bo nie ma krwi niektórych magów i niektórych tajnych technologii).

Rozmowa Klary i Hektora zakończyła się tym, że... nie wiadomo co z tym robić.

W wyniku:
- Diana wysłała blueprinty:

Grupa silnych, niebezpiecznych viciniusów (oddział Trzygław) które - zdaniem Margaret - ostatnio istniały i były wykorzystywane przed Porozumieniem Radomskim. Te istoty są skomponowane jako oddział, ale nie da się ich sensownie kontrolować non stop. Akcja - do stazy - akcja. I tak wymaga bardzo silnego umysłu maga by coś z tym zrobić. Jako oddział, to o co poprosiła Diana może bez problemu pokonać skrzydło terminusów - zwłaszcza, jeśli będzie dobrze trzymane i pilnowane. Jest to dość kosztowne do zbudowania i wyhodowania; BlackLab i środki Blakenbauerów mogą to zrobić, Kompleks Centralny nie.
Blakenbauerowie dostali te blueprinty. Nie obrazili się. Jednak zrobienie tego oddziału dla Diany zajmie jakiś tydzień.
Otton zażyczył sobie od Mojry, by dowiedziała się kto przed Porozumieniem Radomskim stosował te oddziały i do czego służyły.
A Hektor będzie grał na zwłokę.

- Diana dostała blueprinty: 

Prosiła o jakieś grzyby, trucizny czy toksyny; rzeczy, jakie można szybko i łatwo wyprodukować i które umożliwią przejęcie Szlachcie kontroli nad Kompleksem Centralnym w Kopalinie maksymalnie szybko. Otton wyjaśnił wszystkim, że problem jest bardzo złożony; Druga Strona wyraźnie chce zniszczyć Srebrną Świecę i jeśli nie przejmie kontroli nad Kompleksem Centralnym to wtedy najpewniej wysadzi generatory i w centrum Kopalina mamy krater.
Dlatego Otton woli dać Dianie / Szlachcie toksyczne grzybki czy coś by zmiażdżyli Bankierzy. W ten sposób nie ma ryzyka wysadzenia Kompleksu Centralnego i jest więcej czasu.

- Judyta 

Hektor podjął decyzję - będą eksperymenty. Ale zanim się zakończą, wejdą w Judytę twardo i się dowiedzą co z tego wyjdzie. Stopień przeciwnika: (14/18). Hektor: 16 -> 15. Hektor chciał się dowiedzieć, jakie są intencje Judyty. Hektor naciskał, naciskał i czarodziejka pękła. Popłakała się. Nic nie pamięta... to nie ona złożyła tą propozycję?
Hektor zdecydował - NIE bierzemy, nie próbujemy, nie eksperymentujemy. Tu się dzieje coś dziwnego... Lepiej niech śpi w stazie.


Margaret do Klary: Skubny nie ma nic wspólnego z The Governess. Ani nie jest Spustoszony. Leonidas sprawdził osobiście. Skubny pomoże najmocniej jak tylko potrafi - on naprawdę nie ma nic wspólnego z tym wszystkim.


# Progresja

## Frakcji

* Blakenbauerowie: + blueprinty oddziału szturmowego "Trzygław", sprzed Porozumień Radomskich
* Szlachta: + blueprinty toksycznych grzybków do przejęcia kontroli nad Kompleksem Centralnym

# Streszczenie

Judyta Maus okazała się być potencjalnym sleeper agentem Oktawiana. Diana Weiner dostała dostęp do blueprintów grzybków mogących pomóc przejąć Kompleks Centralny. Blakenbauerowie dostali blueprinty Oddziału Trzygław - ścisłego oddziału viciniusów który przed Porozumieniem Radomskim terroryzował magów. Mają go dostarczyć Szlachcie. Hektor opóźnia.

# Zasługi

* mag: Hektor Blakenbauer, negocjator, decydent i ten, który odkrył, że Judyta Maus może być sleeper agentem Oktawiana.
* mag: Diana Weiner, negocjatorka z ramienia Szlachty, która dostała toksyczne grzybki i przekazała Blakenbauerom blueprinty, których nie miała prawa posiadać
* mag: Margaret Blakenbauer, która dba o tkankę społeczną Rezydencji - między gośćmi, gośćmi przymusowymi, Blakenbauerami i Hektorem
* mag: Judyta Maus, która miała propozycję pomocy walki z The Governess, ale w praktyce to nie była "jej" propozycja - sleeper agent?
* mag: Otton Blakenbauer, który z przyczyn politycznych podjął decyzję o pomocy Szlachcie w przejęciu Kompleksu Centralnego

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, centrum dowodzenia i dyskusji, skąd żaden mag wyjść nie może
                            1. Podziemia Rezydencji
                                1. Czarne laboratorium, gdzie rozpoczęto pracę nad Oddziałem Trzygław

# Wątki

 

# Skrypt

- Leonidas robi "Skubny stuff" by się dowiedzieć WTF IS HAPPENING między Skubny - Governess.
- Judyta Maus chce pomóc w walce z The Governess; współpracuje z Margaret w konstrukcji Maskowania (decyzja Hektora)
... skąd ma pomysł? Sprawdzić?
- Diana Weiner chce wsparcia przy użyciu biolab Kompleksu Centralnego
... chce blueprinty Blakenbauerów
... chce przesłać blueprinty Blakenbauerom, by ci wyprodukowali i przesłali odpowiednie płaszczki do KC

# Czas

* Dni: 1