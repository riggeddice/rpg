---
layout: inwazja-konspekt
title:  "Aurelia za aptoforma"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}
# Wątki

- Aegis czy EIS?

# Konspekt pisany
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [150411 - Dzień z życia Klemensa (Kx, AB)](150411-dzien-z-zycia-klemensa.html)

### Chronologiczna

* [150325 - Morderstwo jak w książce (PT, HB)](150325-morderstwo-jak-w-ksiazce.html)
* [150330 - Napaść na Annalizę (SD, PS)](150330-napasc-na-annalize.html)

## Misja właściwa:

Paulina opuściła Rezydencję Blakenbauerów i uciekła. Sam fakt, że Blakenbauerowie mogli wymazać jej pamięć w Rezydencji "bo tak" był dla niej koszmarnie przerażający. Przynajmniej opuszcza Rezydencję wiedząc, z czym ma do czynienia. Aptoform - jakaś forma istoty rojowej, ale nie wiadomo jaka i - zdaniem Hektora - potencjalnie powiązana z rodem Blakenbauerów. Niezbyt pocieszające.
Z Pauliną skontaktowała się Maria. Zlokalizowała jeszcze jedną końcówkę aptoforma - ta bardzo skutecznie pomaga skierować dowody w zupełnie złym kierunku członkom grupy specjalnej Hektora. Maria dodała, że wskazał jej tą końcówkę kolega dziennikarz (jej dawne źródło i kolega, baaardzo paranoiczny) - Edward. Edward ma pewne informacje na temat aptoforma i jego działań (uważa aptoforma za efekt działań służb USA na terenie Polski powiązanych z projektem 'Omnis' oraz Al Kaidą). Paulina powiedziała Marii, że nie chce, by Hektor i Blakenbauerowie położyli łapy na aptoformie.

Maria skupiła się na prasie i internecie. Wygenerować jak najwięcej fałszywych śladów na temat aptoforma, okrasić je odrobiną prawdziwych danych i wszystko tak, by szło różnymi kanałami. Paulina powiedziała Marii o tym, że Hektor ma osoby powiązane z informatyką i zbieraniem danych z internetu; Maria skupiła się więc na tym, by wprowadzić takie dane by było to wystarczająco wiarygodne. Maria wie, z czym walczy. Jej przeciwnik nie. 
Maria - dziennikarka - zaczęła wspierać fałszywe tropy zastawiane przez końcówkę aptoforma. Poprosiła kilka osób o przysługę, zadała kilka pytań "nic nic, ja tylko nie jestem pewna" które sprawiły, że INNE osoby zaczęły grzebać na własną rękę i wygenerowała sporo ruchu w internecie. Pomógł Marii w tym Edward.

Tymczasem Paulina wybrała się do Powiewu Świeżości. Zapakowała podstawowy zestaw medyczny (uzupełnienie zapasów).

Ż: Komu nekromancja Pauliny jest w tej chwili bardzo potrzebna w Powiewie?
K: Romanowi Gierojowi.
Ż: Przed kim Roman chce ukryć konieczność użycia nekromancji?
K: Przed Annalizą.
Ż: Kto wspiera pomysł Romana (ten który wymaga użycia nekromancji)?
K: Marian Welkrat.
Ż: Co bardzo silnie powiązało ze sobą Aurelię Maus i Annalizę?
K: Paradoks Anieli sprawił, że do ochrony Annalizy pojawił się kiedyś cień EIS połączony z ekosystemem dookoła, z fragmentem duszy Aurelii.

Paulina i Maks wymienili się środkami: Paulina przyniosła Maksowi trochę medykamentów a Maks dostarczył jej trochę ziół wyhodowanych przez Celestynę; część z tego może mieć medyczne zastosowania, ale Maks nie jest kompetentny by to samemu sprawdzić. Paulina obiecała pomóc, ale teraz musi iść do biblioteki.

W bibliotece, na stronę poprosił Paulinę Roman.
Roman powiedział Paulinie, że Powiew Świeżości ma poważny problem.
- mieli EIS, oddali ją KADEMowi
- mieli Spustoszenie (o czym Roman nie wiedział), oddali je KADEMowi
A teraz, Annaliza została zaatakowana jako pionek w walce między KADEMem a Srebrną Świecą; prawie zgwałcona jako ofiara czegoś nie powiązanego z nią.

Roman ma plan - chce przywrócić echo EIS i wstrzyknąć ją do defensywy Annalizy, do artefaktu defensywnego. Jeśli to się uda, będzie chciał wykorzystać to echo EIS do dostarczenia części wiedzy Aurelii bez przywracania samej Aurelii Maus. Ale potrzebuje do tego celu nekromantki, i to cholernie zaufanej nekromantki. A z Pauliną już niejedną rzecz robili wspólnie.

Istnieje rytuał, stworzony przez EAM (dokładniej: przez Jadwigę) który miał za zadanie przywrócić osobę z EIS w wypadku śmierci. Tyle, że nikt nie przewidział Zaćmienia; potem Aurelia go zmodyfikowała (wymaga maga chaosu, o czym nikt nie wie, ale Aniela spełnia wymagania - więc "wymaga Anieli") ale Aurelia nie mogła go użyć po Zaćmieniu do przywrócenia Jadwigi, bo Aniela nie znała Jadwigi. Ale Aniela zna Aurelię.
Roman powiedział również, że z uwagi na to, by nigdy nie dało się Aurelii wskrzesić czy dostać dostępu do jej wiedzy, ciało Aurelii Maus znajduje się na Powiewie Świeżości, w elemencie poza tym wymiarem. I to właśnie powoduje, że połączenie Annaliza - Aniela - Paulina # rytuał Jadwigi/Aurelii powinny umożliwić przywrócenie echa EIS (z wiedzą Aurelii i powiązanej z dobrym uczuciem Annalizy o ochronie) do Powiewu Świeżości.

Nikt nie potrzebuje i nie chce Aurelii Maus (może poza Karradraelem). Ale Romana zdaniem Powiew potrzebuje wiedzy Aurelii Maus jeśli ma przetrwać i się rozwijać.

Nie, Julia nie wie. Ale Powiew ma potencjalnie dużo sekretów i tajemnic. Może być więcej "Spustoszeń" o których Roman nie wie i które mogą zagrozić Powiewowi... zdaniem Mariana Welkrata jest wyraźne ryzyko, to nie tylko Roman.
Oczywiście, Annaliza i Aniela nie mogą niczego wiedzieć. Lepiej, by nikt niczego nie wiedział.

Paulina, mimo wielkiego oporu, zgodziła się pomóc Romanowi. Widzi przesłanki stojące za jego decyzją. 
Paulina spytała Romana o aptoforma. Roman obiecał pomóc szukać faktów na temat aptoforma. I pomóc generować pingi do mylenia Blakenbauerów by kupić czas.
Paulina powiedziała Romanowi o co chodzi - jest aptoform, jest JAKIEŚ powiązanie między nim a Blakenbauerami i Roman ma o tym wiedzieć jako "dead man's switch". Roman przyjął to do wiadomości; ruszył go też fakt, że gdzieś w okolicy może pałętać się bardzo niebezpieczny vicinius. I to w chwili, w której KADEM i Świeca są zajęci sobą.

Paulina i Roman zabrali się do szukania danych o aptoformach. (17/k20). Paulina dostała odpowiedzi na następujące trzy pytania:
Q - Słabe punkty; jak sobie z tym poradzić?
A - Socjalizacja odwrotna. Przykładowo, osoba autystyczna # osoba nieautystyczna. Aptoform się pogubi; zbuduje złe modele. Viciniusy; nie-ludzie, nie-magowie. Wszystko co jest poza zakresem działania aptoforma.
Q - Co z nim zrobić? Jak go "containować"?
A - Aptoform żeruje na uczuciach negatywnych; jest taumatożercą. Z jednym wyjątkiem - aptoform może zostać "nawrócony" przez Arazille. Wtedy działa jak awatar Arazille. Magowie zwykle zabijali aptoformy. Można wykorzystać go jako zaczątek węzła (inny use case). Można wprowadzić go w formę przetrwalnikową; wtedy tylko pobiera energię magiczną, ale nie "działa" aktywnie; jest w kokonie.
Q - W jakich warunkach musi znajdować się rdzeń aptoforma?
A - Rdzeń aktywnego aptoforma potrzebuje odpowiedniego ciśnienia, temperatury i chemikaliów do prawidłowego rozwoju i prawidłowego działania. To powoduje, że na pewnym etapie życia aptoform MUSI przejąć kontrolę nad magiem (dzisiaj: nad odpowiednim miejscem chemicznie zgodnym; jakaś oczyszczalnia ścieków czy zakład chemiczny), inaczej nie będzie w stanie sformować więcej niż 2 końcówek naraz. Paulina wie, JAKA musi być zgodność chemiczna.
Q - Naturalni wrogowie aptoforma?
A - Arazille, kraloth, wszelkie typy drapieżnych, nie posiadających ludzkiej psychiki viciniusów które żywią się magią. Rdzeń aptoforma jest idealnym źródłem energii jak ustabilizowany węzeł. 

Maria jest w stanie określić teren, gdzie działał do tej pory aptoform. Paulina wie, w jakim zakresie ów aptoform się znajduje. To zawęziło możliwości lokalizacji aptoforma.
Głównym problemem Pauliny nie jest "gdzie on jest" ale "co zrobić z aptoformem".

Paulina wpadła na świetny pomysł. Potrzebne jej trochę śluzu kralotha; przy pomocy owego śluzu będzie w stanie (jako, że aptoformy i kralothy to naturalni wrogowie) zlokalizować aptoforma, bo końcówki aptoforma muszą być w stanie wykryć śluz kralotha. Bo inaczej bez szans. A śluz kralotha można zdobyć choćby z domu publicznego w Kopalinie. Więc Paulina jest w stanie to zdobyć bez większych komplikacji.

Nie wiedząc co zrobić z aptoformem, Paulina poprosiła o pomoc Romana. Niech on załatwi, by ktoś rozwiązał problem aptoforma... ktoś, komu można ufać. Bo Paulina nie sądzi, by Powiew, Świeca, KADEM lub Blakenbauerowie powinni przejąć aptoforma; ale jak nie oni, to niech Roman coś wymyśli. Może Millennium?
Roman wziął to na siebie.

# Streszczenie

Paulina boi się Blakenbauerów. Roman (co-szef Powiewu) boi się o Powiew i chce stworzyć homunkulusa bazowanego na Aurelii Maus i na EIS. Maria zbiera informacje o aptoformie nie chcąc, by Blakenbauerowie go dorwali. Roman ma wziąć na siebie problem aptoforma a Paulina pomoże mu "odbudować" Aurelię/EIS w formie w której to nie będzie groźne tak jak kiedyś było.

# Zasługi

* mag: Paulina Tarczyńska jako współpracująca z Powiewem Świeżości nekromantka i lekarka, która nie chce, by aptoform trafił do Blakenbauerów.
* czł: Maria Newa jako współpracująca z końcówką aptoforma dziennikarka, która myśli, że działa przeciwko aptoformowi.
* mag: Roman Gieroj jako jeden z przywódców Powiewu Świeżości, który chce przywrócenia wiedzy Aurelii Maus do Powiewu... za co rozwiąże Paulinie problem aptoforma. Przyjaciel Pauliny.
* mag: Annaliza Delfin jako była uczennica Aurelii Maus, która stała się powodem dla którego Roman chce reaktywacji "echa EIS z wiedzą Aurelii" w Powiewie Świeżości
* mag: Maksymilian Łoś jako współpracujący z Pauliną mag znający się na podstawach magii leczniczej.
* vic: Aptoform Mirasilaler, który dał radę zinfiltrować Paulinę i Marię przez Marię tworząc końcówkę dziennikarza "Edwarda". Na razie: obserwuje.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Nowy Kopalin
                        1. Powiew Świeżości

# Pytania:

Ż: O jakiej niebezpiecznej końcówce aptoforma powiedział Edward Marii?
K: Jakaś forma policjanta / osoby mającej władzę (zamodelowane po Hektorze).
Ż: Edward zna aptoforma jako CO? I czemu na niego poluje?
K: Eksperyment rządowy USA a Edward jest byłym dziennikarzem współpracującym kiedyś z Marią.
Ż: Co zapewnia Paulina, by plan unieszkodliwienia końcówki aptoforma mógł się powieść?
K: Może spreparować środek i chemikalia / magikalia, by wyłączyć aptoforma.
Ż: Co aktywnie może zrobić Paulina, by spowolnić działania Blakenbauerów (by Paulina była pierwsza)?
K: Zalew informacji, pełne przeciążenie informacyjne; min. przez użycie Marii (pośrednio) i bardziej magicznych kontaktów Pauliny.
Ż: Jak wygląda aktualnie główny cel aptoforma zdaniem Edwarda?
K: Aptoform najpewniej chce sobie nieźle pojeść; kręci się niedaleko źródła energii i źródła emocji.