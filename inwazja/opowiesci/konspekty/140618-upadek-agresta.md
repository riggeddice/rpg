﻿---
layout: inwazja-konspekt
title:  "Upadek Agresta"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140611 - Rezydencja Blakenbauerów (AW, WZ, HB)](140611-rezydencja-blakenbauerow.html)

### Chronologiczna

* [140611 - Rezydencja Blakenbauerów (AW, WZ, HB)](140611-rezydencja-blakenbauerow.html)

## Misja właściwa:

Poprzednia misja to były "dzień 1, 2". Ta misja to końcówka "2", oraz "3, 4".

Jeszcze pod koniec dnia drugiego Andrea umówiła się an spotkanie z Quasar. 
Spotkały się na KADEMie i Andrea powiedziała Quasar wszystko, co wiedziała na temat projektu Moriath. Nie oszczędziła informacji o słowach Ottona, gdy ten mówił że Mojra ma powiedzieć "Andrzejowi" by Irina współpracowała lub nic z tego nie będzie. Andrea zaproponowała Quasar współpracę (przede wszystkim wymiana informacji, ale też usługi i ew. ochrona). 
Quasar się wstępnie zgodziła na współpracę, przy założeniu absolutnego braku zaufania. Ostrzegła Andreę, że główny problem z walki przeciwko Inwazji polega na tym, że... nie wiadomo co Inwazja tak naprawdę robi. Quasar wie, że pojawiają się magowie, którzy są przejmowani przez Inwazję, ale nie wie jak to się odbywa. 
Zidentyfikowała też "Andrzeja" jako Andrzeja Sowińskiego, przywódcę SŚ. Na bazie KADEMowego wywiadu powiedziała Andrei, że Mojra była agentką, lojalną agentką, ale z jakiegoś powodu zmieniła stronę. Nie na "Inwazję". Na trzecią stronę, czymkolwiek ta by nie była.

Quasar stwierdziła, że projekt Moriath może być nadal przydatny w walce z Inwazją a ewentualny sabotaż tego projektu uznała za poważny błąd. To znaczy, zakładając, że zaufana strona ma nad nim kontrolę. W wyniku tej dyskusji Quasar zdecydowała się odwiedzić Blakenbauerów. Teraz, jak dowiedziała się kto dowodził projektem Moriath, chciała się upewnić, że Moriath będzie stał po właściwej stronie.

Andrea wyszła z tego spotkania w miarę usatysfakcjonowana. Dla odmiany. Zapuściła sieci chcąc dowiedzieć się jak najwięcej na temat tien Agresta oraz jego powiązań z Mają Kos; co to za jedna i czemu Agrest się tak dziwnie ostatnio zachowuje. Oskarżenia Czerwca są poważne, ale czy Agrest zabijałby magów? Andrei nie pasowało to do tego jaki Agrest zawsze był.

O poranku dnia następnego Hektora odwiedziła Quasar. Rezydencji się nie spodobała, choć reakcja nie dorównywała tej na widok Mai. Quasar zignorowała temat; ona zna się na Rezydencjach i wie jak Rezydencje działają. Hektor otworzył się na współpracę z Quasar przeciwko Inwazji a Quasar potwierdziła, że Hektor jest odporny na działania Inwazji (inaczej tak potężna Rezydencja by go zniszczyła i związała się z kolejnym Blakenbauerem). Quasar zapewniła Hektora, że jak on będzie potrzebować pomocy KADEMu, KADEM się pojawi by mu pomóc. Też Quasar, zapytana przez Hektora, potwierdziła - współpracuje już i z Andreą i z Iriną (choć po rozmowie z Andreą postanowiła zacieśnić współpracę z Iriną mocniej).
Gdy Hektor, tak z głupia frant, zapytał czy kiedyś Quasar spotkała się z terminem "żywy aderialith" ta potwierdziła - spotkała się z tym terminem w odniesieniu do Czarnego Kamaza.
Quasar wstępnie wyjaśniła Hektorowi jak działa Rezydencja, że ona żyje i ma swoją agendę i obiecała dostarczyć Hektorowi agenta zdolnego do nauczenia go wszystkiego odnośnie Rezydencji i funkcjonowaniu Rezydencji.

Tymczasem obudził się Edwin. Gdy Quasar opuściła Rezydencję, Hektor od razu przyszedł się zobaczyć z bratem. Nie musiał nic mówić - Edwin już wszystko wiedział (inny zapach w Rezydencji). Edwin spytał o Elżbietę, Hektor od razu powiedział mu o tym, że Marian Agrest ją "odbił" od Zespołu i przyprowadził tu, nic jej nie jest. Edwin się ucieszył; nie ufa Zespołowi, uważał, że mogliby chcieć na niego naciskać przez jego ukochaną. No, ale w Rezydencji jest bezpieczna. 

Naciskany przez Hektora, powiedział o co chodziło z Mojrą. Na początku Mojra faktycznie była lojalną, kompetentną agentką. Otton jej ufał. Jednak z czasem zaczęła rosnąć jej paranoja. Edwin tego na czas nie zauważył, i Mojra sama wstrzyknęła mindworma Margaret, uszkadzając ją nieodwracalnie. Edwin próbował to naprawić; współpracował z Mojrą pod groźbą większego krzywdzenia jego ukochanej rodziny, ale zorganizował plan - gdy Mojra współpracowała z Alfredem Kukułką, on dał Kukułce rozkaz wystawienia "Moriatha". Potem zamindwormował Kukułkę, by ten nie stanowił problemu, by nic nie mógł zdradzić (a Kukułka był przyjacielem Edwina).
Otton próbował w ramach rytuału naprawić Margaret, ale się nie udało... wynik wszyscy znają.

Zapytany przez Hektora, Edwin powiedział, że projekt Moriath należy bezpiecznie wygasić. Nie należy go kontynuuować - nic dobrego z niego nie wyszło. To jedynie nieszczęście i problemy. Sprowadzenie DiDi (Draceny Diakon) by kontynuuowała pracę Margaret i zastąpiła jej miejsce uznał za duży błąd. Na "nie wiedziałem" Hektora wybuchnął, że Hektor całe swoje życie "nie wiedział" i teraz przejął Rezydencję i nadal "nie wie". Jednak mimo wszystko Edwin będzie wspierać Hektora. Hektor szybko połączył się mentalnie z Rezydencją by zweryfikować słowa Edwina i stan Rezydencji i poprzednich projektów. Rezydencja nie ma wiedzy o projektach Ottona, bo nie został jeszcze zintegrowany. Czy ma zostać zintegrowany? "Tak". Rezydencja się wyłączyła; tryb awaryjny, nie ma wejścia, nie ma wyjścia, czas integracji nieznany...
"Nie wiedziałem...". Przerwało to też ostatni rozkaz Hektora "ciągle nawoływać Margaret".

Niezadowolony i podłamany Edwin udał się do Elżbiety. Nie skwitował czynów Hektora ani jednym słowem.

Mniej więcej w tym etapie Andrea uzyskała informacje których szukała; o związku Mariana Agresta i Mai Kos. Okazało się, że lekarzem zarówno Agresta jak i Mai jest... Edwin. Połączeni zostali przez dość specyficzną sytuację w przeszłości, po walkach z Superdefilerem, kiedy to Maja Kos, młoda i ambitna terminuska została skażona przez Superdefilera. Edwin rekomendował jej zniszczenie, jednak Agrest zablokował ten proces. Przyjął na siebie całość odpowiedzialności za działania Mai. Edwin eskalował, argumentując, że Maja "stała się potworem, mentalnie i fizycznie", lecz Agrest był nieubłagany.

Dodatkowo wyszła jej jeszcze jedna rzecz - Agrest jest 53-letnim, samotnym terminusem, który kiedyś był silnie zaangażowany w projekt Saith pod tien Czaplińskim. Stał się też osobą rozmontowującą projekt Saith, niszcząc wszystko co Czapliński kiedykolwiek chciał osiągnąć po tym, jak projekt "zawiódł". Saith mieli na celu sanację SŚ, stworzenie nowej, lepszej jakości - jednak wynikiem stali się obumierający, wyniszczeni magowie o bardzo dużej mocy impulsowej i bardzo krótkim czasie życia (jeśli czarują), z rozpadającą się osobowością i problemach ze wzorem magicznym. Czapliński był skłonny poświęcił Saith, Agrest zdecydował się to zatrzymać.

Czyli Agrest i Saith są (lub byli) jakoś połączeni. To stawia Andrei Mariana Agresta w nowym świetle. A zwłaszcza informacje, że ma dostęp do bardzo skutecznych "zabójców". Czyżby dalej współpracował z Saith i/lub ukrytymi siłami Czaplińskiego...?

Tymczasem Wacław znajdował się w biurze detektywów. Wszystkie alarmy odpaliły z systemu Krówek - Estera zniknęła. Wacław poleciał do miejsca gdzie miały się Teresa i Estera znajdować i zobaczył Teresę klęczącą na ziemi i zwijającą się z szalonego, dzikiego śmiechu. I nie ma Estery. Zapytana Teresa "przywróciła" Esterę, osłabioną i wymaltretowaną i system zabezpieczeń ją znowu zobaczył. Wyjaśniła Wacławowi, że ma metodę ukrywania maga całkowicie przed systemem zabezpieczeń. Perfekcyjne uniewidocznienie, używając metod nekromantycznych połączonych z astralnymi i leczniczymi. Czar hermetyczny.
Aha, a na szyi cały czas kolia Krystalii... i wszystko wskazuje na to, że załapała jakąś formę paranoi...
Super.

Teresa jest nie do przekonania do zdjęcia kolii. Ona jej potrzebuje; tak samo jak Krystalia załapała uzależnienie. Szybki telefon Wacława do Krystalii odebrał Mateusz Nieborak; Krystalii na poprzedniej akcji z tą dziwną uprzężą coś się stało i jest teraz kralotycznie naprawiana. Ale zdaniem kralothów kolia nie jest potrzebna Krystalii przez najbliższy czas... Mateusz stwierdził, że się pojawi spotkać z Teresą. Wacław ostrzegł o paranoi.

Wacław wrócił do Teresy. Ta mu obiecała, że się stąd nie ruszy ale poprosiła, by wyłączył system zabezpieczeń na jej pokój. Ona chce się schować. "Wszędzie" są oczy. "Każdy" może źle jej życzyć. Każdy może być dla niej bardzo niebezpieczny, musi się ukryć i schować. Wacław z tego wywnioskował, że Teresa jakoś dowiedziała się o Inwazji, ale skąd? Tak czy inaczej, obiecał jej że wyłączy detekcję na jej pokój (zażądał od systemu by SZCZEGÓLNĄ uwagę zwrócił na Teresę). Przesłał logi systemu i szczątkową analizę widma do Krówek; nie może być tak, że Teresa umie oszukać ten system. Zapytana skąd ma ten czar, odpowiedziała, że od Krystalii. Ale JAK Krystalia sformowała zaklęcie spoza jej źródeł i ścieżek magicznych?! To nie ma sensu, to kolejne pytanie które pozostaje bez odpowiedzi...

Teresa też dodała, że udało jej się wyleczyć częściowo z mindworma Esterę i przywrócić część jej pamięci. Bawiła się nekromancją i leczeniem, przesuwała Esterę między fazami i osłabiła połączenie mindworma. Wacław zauważył, że pod wpływem kolii Teresa zadziałała bardziej agresywnie, trochę jak Krystalia...

Nic, istnieje możliwość przepytania Estery. Jeśli tak, należy to zrobić. Przepytana Estera przypomniała sobie swoje imię i nazwisko (Estera Piryt), błagała o odnalezienie i pomoc jej córeczce (Rebeka Piryt) i mężowi (człowiek). Przekazała obraz Rebeki; to ta sama dziewczyna która była uczennicą Moriatha, zabita przez Edwina. A mąż, najpewniej, był tym nieszczęsnym viciniusem którego wysadzili na pułapkach w misji #2 kampanii...
Mindworm zaczął ponownie dominować Esterę. Ale istnieje opcja wyleczenia tej czarodziejki... jeśli będzie taka wola. Na pewno Edwin jej wstrzyknął mindworma (tego drugiego), Moriath odebrał jej rodzinę i zmienił ją w viciniusa którym się stała. Tak czy inaczej, jest to problem "na później"...

Do Andrei spłynęły bardzo, BARDZO niepokojące informacje. Marian Agrest ma Karolinę Maus. Wszedł do Rezydencji Zajcewów i wymusił jej oddanie. Kombinacja wszystkich tych informacji oraz czynników spowodowały, że Andrea postanowiła przekazać Grzegorzowi Czerwcowi oraz magom wydziału wewnętrznego SŚ informacje jakie ma na temat Agresta - jest jakoś powiązany z tymi zabójstwami magów i to on chroni (facilituje nawet) zabójców...

Do Rezydencji (niesprawnej oczywiście) przyszedł Agrest, chcąc się spotkać z Edwinem. Oczywiście, nie udało mu się to, Rezydencja jest odcięta. Agrest poczekał chwilę, po czym odszedł, ku zniesmaczeniu Edwina.

Hektor wypytał Edwina co on tak naprawdę wie o tien Marianie Agreście. Zdaniem Edwina jest to mag twardy i bezwzględny, ale uczciwy, surowy i prawdziwie oddany Świecy z całego serca. Jeden z tych "jeśli coś ma być zrobione dobrze, musisz zrobić to sam". Mimo, że mag pochodzi z SŚ ma też kontakty w innych źródłach, też bardzo potężne kontakty i ma bardzo nietypowe, dalekosiężne plany. 
Opowiedział przykład jednej ze swoich pacjentek; bardzo, bardzo ciężko skażona czarodziejka którą Edwin kazał zniszczyć, bo nie dało się jej uratować a Agrest i jego współpracownica, czarodziejka Kamila Leon przekonali go, że ma przeżyć i że są w stanie ją w trójkę naprawić. Zdaniem Edwina, nie udało się. 

Edwin szanuje i poważa Agresta, nie "lubi" go, ale ceni i cieszy się, że to właśnie Agrest dowodzi terminusami Świecy w Kopalinie w tych czasach. Marian Agrest miał pełną wiedzę o projekcie Moriath.

Hektor poszedł do Marcelina, poprosić go, by ten zajął się pomocą DiDi. Marcelin odmówił. Zaatakował Hektora - co ma DiDi (cybergothka, czarodziejka) lub Elżbieta (człowiek, ustatkowana) czego nie ma Sophistia (alternatywna, człowiek)? Czemu Hektor akceptuje tamte dwie a nie chce w żaden sposób zaakceptować Sophistii? Hektor miał dosyć. Walnął pięścią w stół i poszedł po Marcelinie ostro, mówiąc, że ON jest patriarchą, ustawił go na jego miejscu. Marcelinowi przypomniał ojca. Marcelin w wyniku konfliktu faktycznie zdecydował się na pójście do DiDi i na patrzenie jej na ręce. Będzie posłuszny.

Gdy Edwin przyszedł narzekać na obecność DiDi, Hektor spróbował nacisnąć i na niego autorytetem. Jednak (Hektor "Zły Rzut" Blakenbauer) konflikt został przegrany i Edwin się wściekł. Stwierdził, że Hektora będzie wspierał, ale by to właśnie ON, Hektor, znał swoje miejsce w szeregu. Wyszedł wściekły do swojego gabinetu, pracować nad czymś. Doszło do spięcia Marcelin x Edwin w sprawie rozkazów Hektora; Edwin machnął ręką - Marcelin może Hektora słuchać, ale ON nie zamierza.

Gdy minęło parę godzin (by Czerwiec mógł rozpocząć plan odsunięcia Agresta od władzy), Andrea poszła osobiście powiedzieć Irinie o całej sytuacji. Żeby ją ostrzec, ale też by zobaczyć jej reakcję. Gdy Andrea powiedziała Irinie o tym, jakie siły próbują pokonać Agresta, Irina przygasła. Zapytana, powiedziała, że musiała oddać Karolinę Maus, bo Agrest zagroził, że wymusi na Zajcewach zdeklarowanie czy są w SŚ czy nie, efektywnie uszkadzając więzi między Świecą a rodem Zajcew i poważnie uszkadzając frakcję współpracy. Andrea dodatkowo powiedziała Irinie o tym, że Agrest ma powiązania z Saith.
Po raz pierwszy Andrea widziała Irinę Zajcew tak zdruzgotaną całą sytuacją.

Później, do bazy detektywów przyszedł Mateusz Nieborak. Jak Wacław się spodziewał, Teresa nie chciała go widzieć. Mateusz powiedział, że są jakieś przebicia między Krystalią i Teresą, że coś nie zadziałało (lub właśnie zadziałało) w kolii i że kralothy nad tym pracują. Jakkolwiek Mateusz chciałby Teresę przenieść do Milenium, rozumie, że w tym stanie może być to bardzo trudne. Ona się sama nie ruszy, a kralothom nie jest potrzena do rozplątania sytuacji.
Mateusz nie ma żadnej informacji odnośnie pochodzenia zaklęcia jakiego nauczyła się Teresa. Kolejna tajemnica Krystalii. Obiecał, że poszuka i poszpera na temat Krystalii, Teresy i rzeczy powiązanych.

Późniejszą część dnia Wacław i Andrea poświęcili na przesłuchanie Sebastiana. Z jakiegoś powodu zarówno Czerwiec jak i Agrest go szukają a Andrea i Wacław muszą wiedzieć czemu. Udało im się dostać do tej informacji - Sebastian zna tajne wejście do Rezydencji Blakenbauerów, backdoor wprowadzony przez Ottona dla Mojry. Nikt żyjący, poza Sebastianem, nie wie o jego istnieniu. To zmieniło plany Wacława - system zabezpieczeń dostał jednoznaczne zadanie: jeśli ktoś miałby przechwycić Sebastiana, ten ma zostać natychmiast zamordowany przez system zabezpieczeń.

Wieczorem gruchnęła nowina na którą Andrea czekała. Marian Agrest został odwołany ze stanowiska przywódcy terminusów SŚ w Kopalinie. Pełniącym obowiązki został Grzegorz Czerwiec. Agrest został poproszony o nie opuszczanie Kopalina i dostał nadzór czterech terminusów. Grzegorz Czerwiec przejął Karolinę Maus.

A tymczasem w nocy był [i]bloodbath[/i]. Zginęło 7 magów w samym Kopalinie i 2 jest w stanie krytycznym. Jednym z martwych magów jest znany artefaktor SŚ, Karol Poczciwiec.
Jednocześnie, w nocy Wacława i Andreę obudził system zabezpieczeń. Teresa w jakiś tajemniczy sposób ukryła cały swój pokój przed systemem zabezpieczeń, modyfikując istniejące już zaklęcie. Bez możliwości jej obserwacji zrobiło się ciężej... i co gorsza, czar przetrwał godzinę 0:00 >.>

Rano, do Andrei doszła informacja o rozkazie wydanym przez Agresta "żaden agent Inwazji nie może przeżyć". Dowiedziała się też, że choroba Agresta wynika z nieudanego projektu Saith; miał być jednym z nich, lecz zatrzymano procedurę (bo by nie przeżył). Dowiedziała się też, że personel Czaplińskiego mimo, że on rozmontował projekt Saith nadal jest bardzo lojalny do Mariana Agresta.

Od rana do Hektora przyleciał Marcelin i powiedział, że Agresta odwołali. Edwina wstrząsnęła ta wiadomość; wstrząsnęło nim też to, co zrobił Agrest. Edwin wierzy, że to jakiś plan, jakieś specjalne, tajne działanie...
Do Rezydencji Blakenbauerów przyszedł Grzegorz Czerwiec. Oczywiście, Rezydencja jest wygaszona (integruje), więc nie był w stanie się z nikim spotkać.

Andrea i Wacław byli zajęci sprawdzaniem, co w czasie nocnej krwawej łaźni robili Agrest i Maja Kos. Odpowiedź była mało optymistyczna - byli razem, w domu Agresta, pilnowani przez czterech terminusów. To nie mogli być oni. A to oznacza, że rozkazy były wydane wcześniej, lista celów była wyznaczona wcześniej i nie da się tego sensownie zatrzymać...

Mniej więcej wtedy zadzwonił do Andrei Hektor. Zsynchronizowali się w trójkę i spróbowali wymienić wiedzę i informacje. Niestety, nie pamiętam co dokładnie za informacje zostały wymienione. Pamiętam, że Hektor koniecznie nie chciał powiedzieć Wacławowi i Andrei o tym, że odciął Rezydencję (i o Tatianie) a W+A koniecznie nie chcieli powiedzieć Hektorowi o tym, że Rezydencja ma tajne wejście.

Do Andrei przyszedł Czerwiec. Zapytał czy Andrea ma JAKIEKOLWIEK pojęcie na temat tego co Agrest mógł zrobić Karolinie Maus? "To nie jest ona... zachowuje się jak ona, ma jej moc magiczną, wszystko pasuje, ale to NIE jest ona...". Jest też zrozpaczony atakami na magów i eskalacją tych ataków. Zaproponował Andrei, że da jej uprawnienia do korzystania z systemów Srebrnej Świecy i pełną rehabilitację w oczach magów SŚ... niech tylko użyje swoich umiejętności i zatrzyma wszystkie te ataki. Andrea nie może niczego obiecać, ale zrobi co w jej mocy. 

Andrea udała się do Iriny Zajcew i poprosiła o wysłanie Whisperwind przeciwko Saith. Tak postawiła sprawę, że Irina nie miała wyboru. Spełniła prośbę Andrei.

Wacław borykał się z Teresą, jej "dziwnie rosnącym intelektem", rosnącą paranoją i całą tą sytuacją.
Andrea otrzymała wieczorem uprawnienia i zapuściła poszukiwania.

W nocy, doszło do straszliwej eksplozji w domu Mariana Agresta. Zginął jeden z terminusów, którzy go pilnowali, dwóch innych jest lekko rannych. Jest potwierdzona śmierć - Marian Agrest. Stan Dariusza Kopyto i Mai Kos jest nieznany.

Tej samej nocy Whisperwind upolowała i zabiła starego Saith, "Flamecallera". Dodatkowo zginęła Saith "Catapult", próbująca włamać się do Rezydencji Zajcewów. Celem Catapult był albo Benjamin Zajcew, albo sama Irina...


# Mapa misji:

![](140218%20-%20UpadekAgresta.jpg)



# Aktualny stan NPC po tej misji:

![](Moriath_NPC_140622.jpg)

# Zasługi

* mag: Andrea Wilgacz, odwalająca robotę detektywistyczną i nawiązująca potencjalne sojusze; skupia się na Agreście i Mai Kos i pomaga Czerwcowi.
* mag: Hektor Blakenbauer, jako nowy patriarcha rodu, który dowiaduje się prawdy o projekcie Moriath i wciąż nic nie wiedząc o Rezydencji wyłącza ją z akcji.
* mag: Wacław Zajcew, który pomagał Teresie z jej paranoją oraz poznał ponurą prawdę o Esterze Piryt. Też: poznał sekret Sebastiana Teczni (backdoor do Rezydencji).
* mag: Marian Agrest, 53*letni, samotny terminus i były współpracownik mistrza Czaplińskiego powiązany z Saith. Silnie powiązany z Edwinem. Umiera w eksplozji biura, lecz wpierw eksterminuje grupy magów przy użyciu agentów oraz Saith. Przechwycił Karolinę Maus. KIA.
* mag: Quasar, jako paranoiczka, widząca potencjalnych agentów Inwazji w każdym; skłonna do ostrożnej współpracy z Blakenbauerami i Iriną pod wpływem Andrei.
* mag: Whisperwind, wysłana przez Irinę do egzekucji Saith po namowach Andrei.
* mag: Edwin Blakenbauer, uważający brata za przyczynę śmierci ojca, jednak z nim współpracujący. Odzyskał ukochaną dzięki Agrestowi. Kiedyś: lekarz Agresta i jego syna (nie mógł pomóc).
* mag: Marcelin Blakenbauer, który chciał ostatni raz bronić Sophistii przed Hektorem (co ma DiDi lub Elżbieta a nie ma Sophistia), ale Hektor go zdominował i kazał mu pilnować Dracenę.
* mag: Estera Piryt, uwięziona w piwnicy bazy detektywów; tymczasowo wyrwana spod władzy mindworma przez Teresę. Po odzyskaniu osobowości błaga o pomoc córce i mężowi (którzy już nie żyją)
* mag: Teresa Żyraf, pod wpływem kolii; pracująca nad zaklęciem idealnego ukrycia i mająca coraz większą paranoję. Niestety, udziela jej się szaleństwo... oraz geniusz. Jakieś przebicia z Krystalią.
* mag: Maja Kos, na którą Rezydencja Blakenbauerów reaguje bardzo alergicznie; przy Superdefilerze "stała się potworem, mentalnie i fizycznie", lecz Agrest nie pozwolił Edwinowi jej zniszczyć.
* mag: Grzegorz Czerwiec, odsuwający od władzy Agresta i wykorzystujący w tym celu wszystko, co tylko może * w tym Andreę.
* mag: Dariusz Kopyto, lojalny agent Agresta zamknięty z nim w biurze; zaginął w eksplozji.
* czł: Mateusz Nieborak, informujący Andreę o przebiciach między Teresą a Krystalią i o tym, że nie ma pojęcia co się tutaj w ogóle dzieje...
* mag: Karolina Maus, chętnie (lub mniej) przekazywana z rąk do rąk (i odbijana). Na samym końcu przechwycił ją Czerwiec... lecz zginęła straszną śmiercią nim cokolwiek powiedziała.
* mag: Karol Poczciwiec, zabity podczas Krwawej Nocy z rozkazu Mariana Agresta.
* mag: Sebastian Tecznia, siedzący w piwnicach bazy Andrei i Wacława. Okazuje się, że zna tajne przejście (backdoor) do Rezydencji Blakenbauerów... i przesłuchany powiedział je Andrei i Wacławowi.
* mag: Irina Zajcew, której wszystko wymknęło się spod kontroli. A zwłaszcza działania Mariana Agresta kończące się Krwawą Nocą. Wysłała Whisperwind, by ta zabijała Saith.
* mag: Benjamin Zajcew, seras Zajcewów frakcja Współpracy, którego chciała zabić Saith Catapult, lecz się jej to nie udało.
* vic: Saith Flamecaller, zginął z ręki Whisperwind, lecz wcześniej zabił wielu magów i stał za Krwawą Nocą. KIA.
* vic: Saith Catapult, zginęła z ręki Rezydencji Zajcewów, gdy nie udało jej się zabić serasa Benjamina Zajcewa.
* mag: Otton Blakenbauer, KIA, gdyż Rezydencja wybrała Hektora na następcę Ottona.
* mag: Andrzej Sowiński, seras rodu i władca Srebrnej Świecy. Osoba, która stała za projektem "Moriath".
* mag: Mojra, (wspomnienie), która była bardzo lojalną agentką, ale zmieniła stronę. Nie na Inwazję. Na "trzecią stronę", czymkolwiek ta trzecia strona jest.
* czł: Elżbieta Niemoc, odbita przez Agresta od Zespołu została przyprowadzona do Rezydencji, dzięki czemu nie ma możliwości wpływania przy jej użyciu na Edwina. Nadal w ciąży.
* mag: Dracena Diakon, która została sprowadzona przez Rezydencję do kontynuowania pracy nad projektem Moriath w miejsce Margaret.