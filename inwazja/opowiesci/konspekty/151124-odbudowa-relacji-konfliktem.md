---
layout: inwazja-konspekt
title:  "Odbudowa relacji konfliktem"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151119 - Patrycja węszy szpiega (AB, DK)](151119-patrycja-weszy-szpiega.html)

### Chronologiczna

* [151119 - Patrycja węszy szpiega (AB, DK)](151119-patrycja-weszy-szpiega.html)

### Logiczna

* [151110 - Romeo i... Hektor (HB, AB, DK)](151110-romeo-i-hektor.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
Ż: Co jest szczególnie cennego / ciekawego w węźle Vladleny?
D: Jest sterylny; jest to przenośny Węzeł, którego bardzo trudno Skazić emocjami i już ma poziom stabilny (autokumuluje).
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".


## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Siluria została pobita przez Ignata i wyłączona z akcji.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę, która potem została przez Hektora upokorzona.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Skubny zdobył koncesję na kanał telewizyjny.
- Hektor, Edwin, Marcelin i Margaret byli zamknięci szukać jakichś sojuszników politycznie.
- Edwin się przyznał, że stoi za Skubnym.

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Plan graczy

- Hektor chce się sprzymierzyć z Weinerami (poza Szlachtą). Znaleźli jednego bardziej wpływowego - Kacper Weiner, nekromanta / katalista.
- Mojra powiedziała im o __cruentus reverto__ w rękach Ozydiusza Bankierza; Kacper odda duszę za możliwość przebadania (nieszkodliwego).
- Ozydiusz podpadł Zajcewom; Elizawieta jest pod jego rozkazami. Więc uderzenie Ozydiusza będzie korzystne.
- Dla poprawienia reputacji Hektora i Blakenbauerów, trzeba uderzyć w Ozydiusza.

## Misja właściwa:
### Faza 1: Poszukiwanie sojuszników

Dzień pierwszy.

Obrady się skończyły. Blakenbauerowie, tak wykończeni. Ale mają pewien plan działania. Tyle wygrać.
Hektor rzucił się na archiwa magicznej prokuratury, szukać spraw związanych z Weinerami; znalazł coś ciekawego - Szlachta, Kurtyna, Zajcewowie, Diakoni, Mausowie mają na pieńku z jednym magiem. Lordem terminusem Ozydiuszem Bankierzem. Hektor też nie jest wielkim fanem tego gościa. Za co Ozydiusz wszystkim podpadł?
- jest cholernym szowinistą a ma same kobiety w zespole. 
- bo zaatakował przecież rodzinę Tamary, dodatkowo tępi magów Kurtyny uważając ich za słabe pasożyty na gildii.
- bo tępi magów Szlachty, uważając ich za arystokratyczne pasożyty niegodne bycia w Świecy.
- bo działał przeciwko Baltazarowi Mausowi i ogólnie miał w przeszłości okrutne wyroki wobec Mausów (Sędzia Dredd).
- bo tępi Zajcewów z ich ograniczonym poszanowaniem prawa i źle traktuje Elizawietę.
- bo zwinął Diakonom __cruentus reverto__, dodatkowo tępi Estrellę jako swoją terminuskę.
- a nawet w swoim rodzie ma opozycję.

Jednocześnie lord terminus Ozydiusz Bankierz jest niezwykle bezwzględny (Hektor zauważył) i po swojemu jest uczciwy - nienawidzi wszystkich po równo. Ma opinię niebezpiecznego przeciwnika. Kilka razy ktoś stawiał czoło Ozydiuszowi, ale ALBO kończyło się na pojedynku (zgodnie z tradycjami arystokratycznymi), albo przeciwnik Ozydiusza się wycofywał bez żadnego działania lorda terminusa. To, co istotne dla Hektora - Ozydiusz po wycofaniu się jego przeciwników się nie mścił. Jest lordem terminusem od siedmiu lat. To długo.

To co jest tu super - uderzenie w Ozydiusza podnosi szacun Hektora... u wszystkich. Poza Ozydiuszem i kimkolwiek za Ozydiuszem stoi.

Hektor poszedł do Mojry porozmawiać na temat lorda terminusa Ozydiusza Bankierza.
Mojra powiedziała mu, że Ozydiusza nikt praktycznie nie lubi; Ozydiusz jest dobrym politykiem i w ten sposób nie zabiera żadnej strony i zachowuje neutralność. Jednocześnie, Ozydiusz zawsze znajduje słaby punkt swoich przeciwników i uderza dokładnie w to. Jak to Mojra zauważyła cynicznie, nawet jeśli Hektor nie wygra z Ozydiuszem, i tak zdobędzie parę punktów u oponentów Ozydiusza. Zapytana, Mojra powiedziała Hektorowi o sprawie Baltazara Mausa gdzie Ozydiusz zagrał mniej czysto niż zwykle; nie wiadomo, co tam się działo, ale Ozydiusz to ostro zablokował i wskazał na Baltazara jako głównego winnego. Młodemu Mausowi grozi nawet odebranie mocy. Przy okazji tego wszystkiego z cmentarzem przy tej sprawie ciężko ranna została czarodziejka Powiewu, niejaka Celestyna Marduk. Oraz mag Millennium, niejaki Mikado Diakon. Na bezpośrednie polecenie Ozydiusza.

Hektor ma nadzieję na lekkie ocieplenie relacji Diakoni - Blakenbauerowie, jeśi uda się tą sprawę rozwiązać.
Hektor grzebie w aktach, by dowiedzieć się jak najwięcej o tej sprawie; chce dowiedzieć się by móc rozmawiać z magami...
Poprosił też magów śledczych Szlachty, by mu pomogli znaleźć brudy na Ozydiusza.

Tymczasem Siluria; siedzi spokojnie w domu Kermita i dobiera sobie obróżkę by się podobać Wiktorowi. 
Do domku wpada Romeo - Siluria go prosiła do siebie. Powiedział Silurii wydarzenia związane z Rezydencją i Hektorem. Powiedział też, że zagroził Hektorowi, że zabije tych wszystkich ludzi. Jego przyjaciółka powiedziała Romeo, że Hektor tak lubi - będzie sprowadzony do sytuacji bez wyjścia i będzie tak słodko błagać, bo on tak lubi - i uratuje tych wszystkich ludzi. I Romeo tak się pożalił, że tak zaaranżował te wszystkie sytuacje, tak polował... a taki zwykły Hektor doprowadził do problemów Diakonów ze Świecą.

Silurii opadła szczęka. JAK. Triumwirat jest oczywiście poinformowany, ale cała ta sprawa jest dziwna. Wyjaśniła szybko Romeo, że jego przyjaciółka mu źle powiedziała; to niejaka Czirna Zajcew. Nic nie wiadomo, by była blisko z Vladleną czy Tatianą. Dobra, czas, by się tego dowiedzieć - ale po sprawie ze Szlachtą.

Hektor dostał wiadomość od Patrycji Krowiowskiej. Powiedziała, że wysłany został mail z konta donos@buziaczek.pl, w którym to mailu opisane jest, że w leśnej chatce Blakenbauerów znajdują się twarde narkotyki. Oczywiście, przechwyciła to pierwsza i powiedziała, że to najbardziej nieudolna próba donosu (wrobienia) ever. Zapytany przez Hektora Borys spojrzał na monitoring i szybko zauważył, że w chatce jest monitoring zafałszowany na visual loop - tak półudolnie (nie zgadza się pora dnia). Hektor kazał Patrycji wyśledzić i wysłał na akcję Alinę i Dionizego...

Alina dostała z Rezydencji owczarka tropiącego; straszliwie skutecznego niuchacza. I biorą ze sobą kilka "wiewiórek" do pilnowania (jak płaszczki, ale leśne).

Alina i Dionizy idą na akcję. Blisko domku leśnego, "wiewiórki" przygotowane do walki, niuchacz prowadzi prosto na domek (Alina obeszła domek by znaleźć ślady - cel jest nadal w domku).
...Alina znalazła czarnego rumaka. Konia. Koń spojrzał na Alinę bardzo nieufnie.
Dionizy dostaje się na poddasze by móc odblokować system zabezpieczeń a Alina prowokuje konia do rżenia; wiewiórka straszy konia, który zaczyna rżeć. A Dionizy łyka stymulant bojowy.

Dionizy wkradł się na poddasze niezauważony i reaktywował system zabezpieczeń. W głównej hali siedzi Tatiana Zajcew i popija z kieliszków wodę mineralną. Hektor powiedział Dionizemu i Alinie, że mają się dowiedzieć od Tatiany, czego ona chce. A w razie czego, Hektor będzie streamowany przez Skype.

Alina i Dionizy po prostu weszli; Tatiana nie poczęstowała ich wodą mineralną. Gdy Alina się do Tani zwróciła per "tien Zajcew", ta stwierdziła, że nie rozumie tego słowa. Tania powiedziała, by zwrócili się do Hektora - jej dobrego przyjaciela. Tania powiedziała, że między nią i Hektorem jest zbyt dużo złej krwi i ona zdecydowała się to rozwiązać i wyczyścić atmosferę - ale do tego celu chce z nim porozmawiać. Czemu przyszła tutaj? Tania wzruszyła ramionami - bo prędzej czy później Hektor tu przyjdzie lub kogoś wyśle.

Dionizy silnie zaprasza Tatianę do Rezydencji, by porozmawiała z Hektorem. Tania odmówiła. Ona ma do przekazania Hektorowi pewną wiadomość.
...Dionizy zestawił połączenie telekonferencyjne. Tatiana aż się uśmiechnęła - tak bardzo odważny Hektor...
Tania rzuciła Hektorowi wyzwanie. Tak jak on ją upokorzył i ośmieszył, tak ona ma zamiar ośmieszyć i upokorzyć jego.
- wyjaśniła mu, jak działa Zajcew Fire Suit; nieważne czy Tania wygra czy przegra, między nią a Hektorem kwita z jej punktu widzenia. A Hektor może odpowiedzieć ogniem.
- jej celem jest uderzyć w reputację Hektora, udowodnić, iż jest kiep łajdak i oszust. Jeśli nie jest, to nie zadziała.
- Tania ma maksymalnie miesiąc. Potem albo nie zrobi ruchu, albo umrze.
- Jeśli Hektor jest szczery, prawy, uczciwy i sprawiedliwy, Fire Suit nie zadziała przeciwko niemu.
- Niezależnie od wyniku, pojawi się legenda o Tatianie i jej użyciu Fire Suita.
- Tania może jedynie zadziałać Fire Suitem jeśli Hektor okaże nieprawość wobec niej bądź powiązanych z nią interesów.

Po skończeniu telekonferencji Hektor odwrócił się do Borysa, który bezczelnie podsłuchiwał.

"Mówiłeś, że nie miałeś żadnego kontaktu z Zajcewem?" - Hektor
"Mówiłem, że żaden Zajcew nie włamał się do Rezydencji." - Borys

Borys przyznał się, że sprzedał kapcia Marcelina Zajcewowi. Powiedział, że ma nad sobą rozkazy Edwina, który kazał mu łapać wszystkie informacje odnośnie Tymotheusa Blakenbauera. Hektor wysłał go, by przeprosił Tatianę, sam natomiast udał się by porozmawiać z Edwinem. Edwin powiedział Hektorowi, że Borys ma wyraźne wytyczne od niego by łapać i przekazywać mu wszystkie informacje o Tymotheusie. Zapytany przez Hektora, czy przekazuje wszystko Ottonowi, Edwin potwierdził. Może ukrywać prawdę przed Hektorem (bo on jest zbyt publiczny i coś chlapnie), ale Otton musi wiedzieć.

Edwin też zauważył, że przecież dlaczego Otton i Mojra nie uczestniczyli w szukaniu sojuszników? Najpewniej polowali na Tymotheusa...

Idyllę rodzinną przerwał Hektorowi komunikat Silurii.
A dokładniej, list który przyniósł Hektorowi bardzo nieszczęśliwy Dionizy. A w liście mniej więcej "w związku z tym w jakiej sytuacji został postawiony przez Ciebie mój ród muszę zaprzestać współpracy i przerwać kontakty" - by sprowokować Hektora do kontaktu z Silurią.

- "Rzuciła cię... Diakonka? Jest pewno dno, ale... czekaj, powiedz chociaż, że się z nią przespałeś..." - Edwin do Hektora

Hektor zadzwonił do Silurii. Siluria odebrała. Siluria chłodna, Hektor przyjazny. Hektor bardzo przepraszał Silurię, ale ta powiedziała, że boi się o swój ród i jego działania. Zdecydowali się spotkać, Siluria powiedziała gdzie. Zanim jeszcze się spotkali, Siluria chciała spotkać się z Amandą Diakon, członkinią Triumwiratu by dowiedzieć się na temat Świecy i tego co się stało, ale przez to że infiltruje Szlachtę nie do końca jest w stanie.
Spotkanie Silurii z Hektorem. W kawiarence, dyskretnej i NIE powiązanej z żadnym magicznym rodem.
Podczas spotkania rozmawiali o Diakonach, o potędze wpływów Blakenbauerów... o moralnym kompasie...

- "Tak, ród Diakon ma moralność. Nie, nikt kto nie jest Diakonem jej nie zrozumie. Nie chodzi tylko o erotykę." - Siluria do Hektora.

Siluria powiedziała Hektorowi, że Romeo przyszedł do Hektora w dobrej wierze, przygotowany by sprawić Hektorowi jak największą przyjemność. Po drodze zmniejsza się ilość guziczków na jej biuście. Hektor powiedział, że jeśli działanie trzeciej osoby ma ich skłócić, nie mogą się dać. Siluria powiedziała, że dopóki ten zakaz ze strony Blakenbauerów jest w mocy, nie ma "my".

Nowy cel Hektora: Mojra. Ta poproszona o zdjęcie zakazu powiedziała, że to niemożliwe; jeśli teraz zdejmą zakaz, to wyjdzie na to, że Świeca jest chwiejna i Blakenbauerowie też są chwiejni. Teraz po prostu jeszcze nie mogą. Potrzebne jest coś więcej.

Edwin poprosił do siebie Hektora na moment. Hektor podszedł i zobaczył kilka osób i dużo różnych półproduktów; półmagicznych, artefaktów, osób. Edwin powiedział, że Szlachta wysłała im je na eksperymenty a on musi chwilowo przekierowywać Rezydencję by "trzymać" te rzeczy by się nie rozpadły. Podobno Szlachta miała obiecany jakiś Węzeł... ale Edwin go nie ma.

Hektor obiecał, że się tym zajmie...

## Spekulacje:

- Hektor przekaże Silurii, że muszą znaleźć tą osobę która ich chce rozdzielić; tylko Diakoni mogą to zrobić.
- Hektor chce zdjąć zakaz, ale jeszcze się nie da.
- Następna misja: Węzeł.
- A kolejna: Ozydiusz.

## Dark Future:
### Actors:

Romeo (Narcissist)
Tatiana Zajcew (Brawler/Explorer)
Diana Weiner (Cult Speaker / Scientist)

### Faza 1: Poszukiwanie sojuszników

- Tatiana organizuje działania mające na celu ściągnąć Hektora by wyjaśnić mu Fire Suit (Brawler.incite a rivalry with Hektor).
- Hektor dowiaduje się, że na policję trafił anonimowy donos o narkotykach w chatce Blakenbauerów (Brawler.force an encounter with Hektor)
- Romeo informuje Silurię o powodach takiego działania (Narcissist.be unmoved by Hektor's failure)
- Tatiana spotyka się z Hektorem w chatce Blakenbauerów (Brawler.present a challenge)
- Diana wysyła do Rezydencji transport cennych ludzi i materiałów (Scientist.acquire sources for experiment)

# Streszczenie

Blakenbauerowie odkryli Kacpra Weinera jako potencjalnego sojusznika i Ozydiusza Bankierza jako potencjalny cel do ataku by zdobyć szacunek i wsparcie. Siluria wyjaśniła Hektorowi, że skrewił z Diakonami i że Czirna Zajcew - przyjaciółka Romeo - źle mu doradziła. Dodatkowo Tatiana włamała się do leśnej chatki Blakenbauerów i wyzwała Hektora na pojedynek przez Fire Suit Zajcewów. Przy okazji Borys przyznał się, że Edwin kazał mu przechwytywać wszelkie informacje o Tymotheusie. Czemu? By Hektor czegoś głupiego nie chlapnął.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Kawiarenka "Złotko"
                    1. Las Stu Kotów
                        1. Leśna chatka Blakenbauerów

# Zasługi

* mag: Hektor Blakenbauer, nie mógł ukarać Borysa, ale za to wpakował się w potencjalnie zbawczy pojedynek z Tatianą.
* mag: Siluria Diakon, wyjątkowo oziębła z fasady, ale próbuje po swojemu rozwiązać problem Diakonów i Blakenbauerów pomiędzy polowaniem na Wiktora i Dianę.
* vic: Alina Bednarz, która z Dionizym poszła na akcję i znalazła Tatianę.
* vic: Dionizy Kret, który z Aliną poszedł na akcję i znalazł Tatianę.
* mag: Tatiana Zajcew, mastermind za nieudolnym wrabianiem prokuratora która zakłada Fire Suit by wykazać nieprawość prokuratora.
* mag: Ozydiusz Bankierz, którego w sumie nie było ale jest skutecznym politykiem i terminusem; nienawidzi wszystkich po równo ze wzajemnością. Potencjalny cel Blakenbauerów.
* mag: Mojra, perfekcyjne i całkiem usłuchane źródło informacji dla Hektora. Milsza niż zwykle.
* mag: Romeo Diakon, który żalił się Silurii że chciał dobrze a Hektor go nie kocha... i stąd Wielki Konflikt Blakenbauer / Diakon.
* mag: Czirna Zajcew, Romeo o niej powiedział, że zaproponowała mu tą konkretną strategię trafienia do serca Hektora... najpewniej ma nieczyste motywy.
* czł: Patrycja Krowiowska, która znalazła co Tatiana chciała by Hektor się dowiedzia i która natychmiast przekazała Hektorowi, że ktoś chce go wrobić.
* czł: Borys Kumin, któremu upiekła się straszna kara od Hektora, bo Edwin kazał mu łapać wszystkie informacje o Tymotheusie Blakenbauerze...
* mag: Edwin Blakenbauer, który ZNOWU ukrywa wszystkie wiadomości o Tymotheusie przed Hektorem. Ale - jak mówi - nie przed Ottonem.

# Czas

* Dni: 1