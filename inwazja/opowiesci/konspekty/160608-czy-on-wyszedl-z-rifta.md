---
layout: inwazja-konspekt
title:  "Czy on wyszedł z Rifta...?"
campaign: anulowane
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

### Kampanijna

* [160526 - Bobrzańskie Gumifoczki (LŁ, AB)](160526-bobrzanskie-gumifoczki.html)

## Punkt zerowy:

Ż: Kto jest właścicielem zamku i jakie jest jego powiązanie z burmistrzem?
B: Bobrzański kościół scientologów a burmistrz jest Wtajemniczony.
Ż: Dlaczego Grigorij niespecjalnie może odmówić Zofii w czymkolwiek ona nie poprosi?
K: On ma u niej dług a ona zna jego niewygodny sekret.
Ż: Jak to się stało, że Zespół może spokojnie stacjonaryzować się w zamku z Riftem?
D: Zygfryd jest eksponatem, Arek tam pracuje, a reszta jest ich przyjaciółmi. Działają jako społecznicy...
Ż: Kto ma najwięcej wtyk w lokalnym oddziale Srebrnej Świecy?
B: To Zajcew.
Ż: Kto z Zespołu jest kontaktem z BKS?
D: Głównym kontaktem jest Arek. To z nim rozmawiają.
Ż: W świecie ludzi, kim jest miłośniczka zwierząt?
K: Pracuje jako zegarmistrz

## Hasła misji

- "Dama z foczkami" i rift reagujący na działania dzieciaków
- Zygfryd opowiadający historie o duchach
- Niedokończony pojedynek rowerków
- Przybywa Jagoda Brzoza zająć się foczkami po obejrzeniu filmiku
- A w sumie to foka pochodzi z okolic Świecy

## Kontekst misji



## Misja właściwa:

Burmistrz Teodor Nos wezwał do siebie Arka i pogratulował mu świetnego filmiku; zaskoczyło go, gdzie trzyma te wszystkie foki. Na filmiku śpiewający Grigorij, Zdzisia w jakimś cholernym stroju (moda...) i 8 foczek. Teodor pogratulował świetnego filmiku promocyjnego.

"Niedługo powrócę. Oporządź komnatę!" - Zygfryd

8 zaobróżkowanych fokokotów przez Arka. Będą miały w nocy koszmarną zabawę...
...i poszedł oporządzić stajnię dla konia Zygfryda.

Do taksówki Luizy wszedł Grigorij. Zapytał czy będzie problemem jeśli na zamku prześpi się banda dzieciaków - bo Zofia kazała. Bo coś z fokami. TAK, może być problem, zwłaszcza, jeśli Zygfryd będzie opowiadał bajki o duchach - a to punkt programu...

Powrót Zygfryda. Roman przygotował XT-43 - zamaskowany jako odkurzacz mechaniczny giermek. Został wzgardzony.

Wieczór. Wspólny posiłek. Jeszcze dzieciaki się nie zjechały. A Luiza się zorientowała, że to NA PEWNO spowoduje manifestację jakiegoś typu z riftu...
Podczas kolacji rozmowa zboczyła na "to nie był najlepszy moment na wrzucanie tego filmiku". Luiza podpuściła Zdzisię opowiadając plotkami, by ta powiedziała coś więcej. Zdzisia powiedziała - chodzi o to, by wygenerować viciniusy z Riftu; będzie na Świecę tak czy inaczej. Zaraz potem wkręcił ją Arek w to, że przez ten filmik będzie miała 3-miesięczny angaż jako performerka z foczkami... albo ich wyrzucą z zamku.

I Zdzisia to łyknęła...

Wieczór. Dzieciaki się zbierają na zamku. Arek idzie przekonać Zdzisławę do tego, by ta 

Noc....


Strażnicza Onuca.


# Zasługi

- mag: Arkadiusz Bankierz, TODO
- mag: Luiza Łapińska, TODO
- mag: Zdzisława Myszeczka, TODO
- mag: Roman Weiner, TODO
- mag: Grigorij Zajcew, TODO
- vic: kot Attylla, który podobno jest bardzo groźnym viciniusem (nawet smokiem). Na razie zachowuje się jak mały, puchaty kiciuś. Tylko że patrzy w taki straszny sposób...

# Lokalizacje

1. Świat
    1. TODO
        1. Bóbr
            1. Bobrzański zamek
                1. Rift
            1. TODO