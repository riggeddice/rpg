---
layout: inwazja-konspekt
title:  "Ani słowa prawdy..."
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150304 - Ani słowa prawdy... (HB, AB)](150304-ani-slowa-prawdy.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

### Inne

Flashbacki przed:

* [141006 - Klinika "Słonecznik" (WD, EM)](141006-klinika-slonecznik.html)

Nie ma to misji poprzedzającej; buduje kontekst.

## Misja właściwa:

**Flashback 1: Problem brukarza, który nie trafił do kliniki Słonecznik, koło miesiąca przed zniszczeniem kliniki**
Telefon od Anny do Aliny. Brukarz, niejaki Karol Kiśnia został zbadany przez lekarza i wysłany do domu z zapowiedzią wizyty domowej. Lekarz wypełnił w karcie choroby "zatrucie pokarmowe", ale zdaniem Olgi to bullshit. Olga zajmuje się lekarzem, Alina ma zająć się brukarzem. Operacja jest sygnowana przez "The Director" (czyli: Hektor nie ma nic na ten temat wiedzieć). Alina przyjęła to do wiadomości. 
Znamiennym zdaniem Anny jest to, że ów lekarz chciał wysłać brukarza do kliniki Słonecznik, ale po rozmowie z pewnym mężczyzną zmienił zdanie (naoczni świadkowie to potwierdzają). A Anna powiedziała, że 100% wysłanych do kliniki Słonecznik ludzi jest przyjętych.

Alina, już w domu brukarza zobaczyła mężczyznę z silnym światłowstrętem, sinego i z krwotokami. Jego żona rozmawia przede wszystkim z Aliną. On jest częściowo przytomny. Alina widząc mężczyznę wykluczyła "normalne zatrucie pokarmowe" i stwierdziła, że on ma jechać do kliniki Słonecznik. Słysząc to, zrobili się bladzi ze strachu. Nie, mieli wizytę domową, Karolowi jest już dużo lepiej. Alina przeprowadziła wstępny wywiad; wszystko wskazuje na to, że "zatruł się kebabem". Oni jedli to samo z żoną, ale on się zatruł a ona nie... po 1-2h się bardzo pogorszyło.

Oczywiście objawy w karcie pacjenta nijak nie pasują do tego co widzi Alina. Alina nacisnęła (8v5->10) i kobieta przyznała o co chodzi. To starcie dwóch mafii. Chcieli otruć kogoś innego ale cyngiel się pomylił i zatruli zwykłego brukarza. Dlatego ta wizyta domowa, podali odtrutkę i powiedzieli, że jak KTOKOLWIEK się dowie... rodzina brukarza, jego żony, dzieci, rodzice... wszyscy ucierpią. A klinika należy do cudzej mafii...

Zgodnie z instrukcjami, "The director" is on it. Czyli nikt nie może się dowiedzieć. Alina nikomu nic nie powiedziała.

**Flashback 2: W domu lekarza Alberta Pireusa, 1-2 tygodnie po pierwszym flashbacku**

Albert Pireus jest lekarzem, który NIE przekierował brukarza do kliniki Słonecznik. Parę dni po tym wydarzeniu Pireus poszedł na urlop. Otworzył własną nielegalną praktykę psychiatryczną, pomagając ludziom i wykorzystując do tego kartoteki szpitala. Dobiera sobie najcięższe przypadki i osoby, którymi najłatwiej manipulować. Zdaniem Anny, Alina ma to sprawdzić i dowiedzieć się o co w tym chodzi.

Plan Aliny był prosty. Spreparowano rekordy w szpitalu by Alina została naturalnym celem Alberta Pireusa i faktycznie trafiła do jego domu, do jego pokoju. Alina ma przy sobie urządzenie nagrywające (środki grupy śledczej + sztuczna tożsamość). Pireus zaprosił Alinę, by usiadła na fotelu i poprosił, by przejrzała się w lusterku. To niewielkie lusterko, o wielkości dłoni bez palców z dziwnymi symbolami i glifami. Pireus poprosił Alinę, by ta się przejrzała w tym lusterku i powiedziała mu co widzi. (10v12->11). 

Alina zobaczyła wizję siebie, zanim doszło do wypadku. Rodzice byli z niej tak bardzo dumni. Jej kariera zapowiadała się świetnie. Była szczęśliwa...
...aż dostała z paralizatora. Otworzyła oczy i zobaczyła Annę stojącą nad nią z taserem. W ręku trzyma to tajemnicze lusterko. Rzuciła je na ziemię i roztrzaskała swoim nożem (z obserwacji Aliny, ten nóż wyglądał dziwnie).
Przez cały czas lekarz wyglądał na dość spokojnego choć zdziwionego. Anna obserwując lekarza poraziła Alinę bardzo boleśnie normalnym paralizatorem, przypadkowo niszcząc system nasłuchowy. Anna zapytała Alinę o jedną złą rzecz, jaka jej się tego dnia wydarzyła. Alina wystękała "ty". Wtedy Anna odwróciła się do niej plecami i skupiła się na lekarzu, zadając mu dokładnie to samo pytanie. Gdy ten nie odpowiedział, Anna zaczęła go metodycznie torturować paralizatorem aż Alina jej przerwała rzucając się na Annę. Alina powiedziała Annie, by nie robiła lekarzowi krzywdy.

Anna ochłonęła. Trochę. Zebrała zniszczone lusterko (cytując "Director's Orders" Alinie odnośnie tego lusterka) i powiedziała, że Anna zajmuje się dalszym śledztwem a Alina ma zebrać dowody na skazanie Alberta Pireusa. Alina przeszukała jego szafki; znalazła tam listę osób, które zostały "uratowane". Nie znalazła drugiego takiego lusterka. Alina ma zgryz.

Alina skonfrontowała się z Anną. Anna powiedziała, że przypomina jej to coś z przeszłością i powiedziała, że współpracuje od pewnego czasu z Olgą dla The Director. I że Anna zarekomendowała Alinę. Ale jeśli Anna nie może Alinie ufać, to w tym momencie nie może jej włączyć w "core" grupy śledczej. Ważne, by żaden Blakenbauer nic nie wiedział. Nikt nie może nic wiedzieć o tym, co się tu wydarzyło. Nikt.

Alina się zgodziła. Na razie niech będzie jak Anna chce. Zgodziła się też, by Olga ją przebadała.
I Edwin też.

**Flashback 3: Kleofas Bór na dywaniku u Hektora, parę dni później**

Kleofas Bór, "the fall guy" poszedł na dywanik do Hektora. Hektor wyjechał mu z inkwizytora, czemu on nic nie wie i grupa specjalna zamknęła szanowanego lekarza bez autoryzacji Hektora. Kleofas odpowiedział, że w sprawę potencjalnie zamieszane są osoby z sądu więc trzeba było działać w odpowiedni sposób. Hektor przesłuchał go na wszystkie fakty i spójność akcji (10v10->8) ale Kleofas dzielnie obronił swoją wersję. Powiedział, że na akcji byli on i Alina. Niestety Kleofas jeszcze dodał, że lekarza nie dopilnował i tamten zażył jakieś środki; na szczęście Olga go odratowała (oczywiście Kleofas się nie przyznał, że to Anna mu wrzuciła pigułkę gwałtu by lekarz nie pamiętał samej akcji)

Hektor przesłuchał lekarza. Lekarz pamięta tylko dobre rzeczy. Nie rejestruje rzeczy smutnych, złych, niefortunnych. Przywitał Hektora ciepło i z uśmiechem. Nie wie, czemu chciał się zabić (nie chciał, <3 Anna) ale ma wątpliwości czy naprawdę chciał się zabić. Hektora zdziwił fakt, że mężczyzna nie ma żadnych uczuć negatywnych. Prokurator dowiedział się o tajemniczym lustrze, ale lekarz nie pamięta gdzie to lustro się znajduje. Zwyczajnie je stracił. Hektor sprawdził srebrnym sygnetem czy lekarz jest pod wpływem magii; nie, nie jest. Blakenbauer poprosił więc Edwina o przebadanie tego lekarza zwłaszcza, że lekarz powiedział, że w tle jest klinika "Słonecznik" i od skierowania tam wszystko się zaczęło.

Alina poprosiła Edwina o "to co zawsze" by Hektor nie dowiedział się czegoś czego nie powinien. Edwin westchnął i podał Alinie "pigułki prawdy". Co Alina powie Hektorowi, w to wierzy. To jedyna prawdziwa wersja. Puścił ją przodem; sam ma jeszcze inne rzeczy do zrobienia.

Edwin przebadał Alberta Pireusa. Powiedział, że jest pod wrażeniem - ktoś przekształcił Pireusa bardzo precyzyjnie. Pireus nie zapamiętuje rzeczy smutnych i złych; żyje w krainie wiecznego szczęścia i bezproblemowości. Jest uduchowiony i wiecznie szczęśliwy...
Edwin podał mężczyźnie fiolkę z ziołami krzywdzącymi kanały magiczne. Tamten zaczął się krztusić i ma "przyjemny ból głowy" (czyt. nie rejestruje tego jako niekorzystny). Na bazie tego Edwin wysunął teorię, że Albert Pireus miał zadatki na słabego protomaga i to wystarczyło by odpalić jakiś tajemniczy artefakt w formie lustra (sam w sobie niebezpieczny jako kombinacja magia + lustro). Podali Pireusowi jakąś truciznę, by ten musiał trafić do szpitala i by Edwin mógł go naprawić.

Tymczasem Hektor przesłuchiwał Alinę i sprawdzał jej wersję wydarzeń. Z ciekawych highlightów; Kleofas nie zdążył zatrzymać lekarza przed wzięciem leków, więc poraził go tazerem. No i Alina powiedziała, że podczas przeszukania gabinetu nie widziała żadnego lusterka.
Lusterko zniknęło.
I wszystko to powiedziała Alina na tabletkach, więc Hektor wierzy, że to prawda.

"Prokurator okręgowy. Nic nie wie i wszystkim ufa. No &&&. Ale z Hektora była dupa wołowa." - Dust.
"Może doktor używał jakiegoś przedmiotu podczas tej hipnozy?" "Nie. Może dlatego nie zadziałało." - Hektor polujący na lustro i Alina unikająca powiedzenia o lustrze.
"Ta historia jest tak absurdalna, że musi być prawdziwa" - Hektor słuchając historii Aliny.

**Flashback 4: Hektor spotyka się z sędziną Janiną Strych**

"Hektorze, dlaczego zamknąłeś mojego lekarza?" - pierwsze zdanie Janiny.
Hektor wchodzi kulturalnie i troskliwie, przekonując Janinę, że dla jej kariery lepiej, żeby nie wypłynęło, że była na liście pacjentów szarlatana. Przekonał ją również, by dała się przebadać w klinice Słonecznik.
Edwin stwierdził, że została lekko zmodyfikowana, ale nie jest to nic, czego nie dałoby się odwrócić.

**Flashback 5: Edwin x Alina**

"Alino, jest coś czego mi nie powiedziałaś. To, co powiedziałaś Hektorowi nie do końca trzyma się kupy." - Edwin do Aliny, na wejściu.
"Nie jesteś magiem. Nie jesteś w stanie utrzymać żadnych sekretów przede mną." - Edwin do wahającej się Aliny

Alina się poddała. The Director the Directorem, ale Edwin jest pod ręką i Alina nie ma złudzeń co do jego bezwzględności. Opowiada więcej szczegółów, pomijając jednak sztylet - Alina nie chce, by Anna wzbudziła nadmierną ciekawość Edwina. Udaje jej się częściowo - Edwin każe Alinie ściągnąć Annę do rezydencji - chce ją przesłuchać. Alina prosi, żeby pozwolił jej przy przesłuchaniu być. 
Edwin stwierdza, że Alina go zawiodła nie mówiąc mu natychmiast o sprawie, jednak zgadza się, o ile to Alina pod postacią Hektora poprowadzi przesłuchanie.

Edwin dostarczy serum prawdy tak, by Anna nie mogła skłamać.

"A będzie o tym pamiętać?" "Po co? Myślisz, że tylko ty umiesz podać pigułkę gwałtu?" - Alina i Edwin, odnośnie Anny.

**Flashback 6: Alina (Hektor) x Anna**

Alina ściągnęła Annę do rezydencji (wezwała ją tam jako Hektor).
Anna przyszła pozornie niczego się nie spodziewając i dostała serum prawdy. Przesłuchana przez Alinę powiedziała, że lusterko zniszczyła i nie do końca pamięta, co i jak tam się działo przez to, że lusterko zostało zniszczone.
Tu Edwin facepalm... Oczywiście, jeżeli spojrzała w lustro, to istnieje możliwość, że utraciła część pamięci odnośnie wydarzeń. To by też tłumaczyło, czemu reszta zachowywała się tak dziwnie - Kleofas bawił się tazerem a lekarz próbował popełnić samobójstwo.
Edwin spytał, co Anna zrobiła z lusterkiem. Ta odpowiedziała, że odbiła lustro w innych lustrach i wszystko razem porozbijała i pozakopywała w różnych miejscach.
Przez cały czas Alina tak prowadzi przesłuchanie, żeby Edwin nie dowiedział się między innymi o sztylecie i o innych rzeczach mogących za bardzo zainteresować go Anną.
Zapytana przez Edwina, skąd wie, jak neutralizować lustra, Anna powiedziała, że dawno temu miała do czynienia z lustrem, które wywoływało duchy i tak zostało zneutralizowane. Edwin westchnął. Sekret rozwiązany, ale nadal trzeba chronić Alinę. 
W końcu Hektor nie może wiedzieć, czym jest Alina.

**Flashback 7: Edwin x Hektor**

Edwin spotkał się z Hektorem i powiedział mu kolejną wersję prawdy, w której większość winy spadła na artefakt i fakt, że lekarz jest potencjalnym protomagiem (choć o bardzo słabej mocy), unikając mówienia o samowolnych działaniach Aliny i Anny.

"Tak wiem, miła odmiana - ktoś wreszcie powie prawdę" - Edwin, kłamiąc Hektorowi
"Artefakt też nie słynie z intelektu w tej akcji, dobrze? To nie tylko twój zespół jest tępy." - Edwin tłumaczy Hektorowi kolejną wersję prawdy.

**Flashback 8: Na sali sądowej**

Zarówno Hektor jak i Alina stwierdzili, że lekarz tak naprawdę jest niewinny. Był pod wpływem artefaktu. Nie ma trwałych zmian u sędziny, nie ma trwałych zmian u Anny, a sam lekarz powinien wydobrzeć. 
Hektor nie chciał go zamykać w więzieniu, więc zgodnie z jego poleceniem Alina osłabiła dowody przeciwko lekarzowi. Jednak ku wielkiemu zdziwieniu wszystkich pojawiły się nowe dowody i zeznania świadków, dużo silniejsze.
To sprawiło, że Hektor musiał wsadzić lekarza do więzienia.
Zapytany, Anna stwierdziła, że najpewniej ta "mafia" próbuje wrobić lekarza.

--------
koniec flashbackowej misji.

# Zasługi

* mag: Hektor Blakenbauer jako prokurator, który NAPRAWDĘ nie wiedział i nie miał skąd wiedzieć, bo wszyscy sojusznicy go okłamują.
* vic: Alina Bednarz jako agentka chroniąca Annę Kajak oraz próbująca wyrolować wszystkich mających jakąkolwiek władzę (łącznie z Anną).
* czł: Anna Kajak jako agentka, która przypadkowo zainicjowała całą piramidę kłamstw i osłaniana przez wszystkich wyszła cało. Ma morderczo-sadystyczną reakcję na tajemnicze lusterko.
* czł: Albert Pireus jako lekarz, który otrzymawszy lusterko został "dawcą szczęścia" i dzięki "pomocy" Aliny i Anny trafił do więzienia za niewinność.
* czł: Karol Kiśnia jako brukarz, który nie trafił do kliniki Słonecznik po "ciężkim zatruciu"; zamiast tego miał "wizytę domową" od "mafii".
* mag: Mordecja Diakon jako detektyw magiczny która zajęła się tematem tajemniczego lusterka i Alberta Pireusa; nie wiadomo, czemu aż ona była tu potrzebna.
* czł: Janina Strych jako sędzina, przyjaciółka Hektora, która korzystała z pomocy Alberta Pireusa (lekarza który został "psychiatrą" dzięki tajemniczemu lusterku).
* czł: Kleofas Bór jako dowódca sił specjalnych Hektora, który jednocześnie osłania Annę Kajak i potrafi oprzeć się Inkwizytorowi Hektorowi i kłamie jak tylko Kleofas umie. 
* czł: Olga Miodownik jako agentka sił specjalnych Hektora, która w rzeczywistości pracuje z Anną na polecenie "The Director". Tu: odkryła, że coś jest nie tak z brukarzem.
* mag: Edwin Blakenbauer jako ten, który odkrył jakąś wersję prawdy... by tylko ukryć prawdę przed Hektorem. Przy okazji, przypadkowo, zamotał.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                        1. Opera Mireille
                        1. Kompleks budynków sądowo-policyjnych
                            1. Areszt śledczy
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów

# Pytania:

Ż: Skąd Anna się dowiedziała, że brukarz powinien być przekierowany do kliniki Słonecznik i nigdy tam nie trafił?
K: Dzięki Oldze; ona monitorowała sytuację i zauważyła anomalię.
Ż: Jakie potencjalne powiązania miała klinika Słonecznik z prokuratorem / jej zainteresowaniami prokuratorem?
D: Klinika bardzo często była wykorzystywana jako zewnętrzny konsultant / przez Hektora przy dochodzeniach.
Ż: Dlaczego Anna aż tak dokładnie monitorowała klinikę Słonecznik?
K: Z uwagi na silne powiązanie z Blakenbauerami.
Ż: W jaki sposób Anna ukryła wszystko przed Blakenbauerami i resztą sił specjalnych?
D: W starym kościele; lapisowane komponenty zadziałały jak ekran.
Ż: Jakiej cennej dla ludzkiego alter-ego Hektora osobie pomógł lekarz?
K: Koleżance / przyjaciółce sędzinie.
Ż: W jaki sposób Alina dowiedziała się o tym, z czym Skorpion przychodzi do Hektora?
D: W tamtym momencie podszywała się pod Hektora i udało jej się odebrać telefon.
Ż: Z jakim poważnym zagrożeniem wiązałoby się to, że psychiatra-szarlatan NIE trafi za kratki?
K: Długofalowo jego działania doprowadzą do powstania potężnego przeciwnika Blakenbauerów.
Ż: Co takiego wiedzą aresztowani agenci Skorpiona co jest niebezpieczne dla jakiejś komórki tej organizacji?
D: Hipokryzja w działaniu; często wykorzystują magów do kontroli ludzi.
Ż: Dlaczego Hektor nie chce włączyć w ten temat żadnego innego maga, zwłaszcza z rodu Blakenbauer?
K: Bo Blakenbauerowie / magowie mogliby za bardzo zainteresować się siłami specjalnymi Hektora.
Ż: Dlaczego poproszono Mordecję Diakon o dowiedzenie się, o co chodziło z szarlatanem?
D: Bo było duże podejrzenie, że w akcję wkracza bardzo niebezpieczny mag-renegat.