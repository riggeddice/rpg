---
layout: inwazja-konspekt
title:  "Prawdziwa natura Draceny"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141102 - Paulina widziała sępy nad Nikolą (PT)](141102-paulina-widziala-sepy-nad-nikola.html)

### Chronologiczna

* [141102 - Paulina widziała sępy nad Nikolą (PT)](141102-paulina-widziala-sepy-nad-nikola.html)

## Misja właściwa:

Do Pauliny zadzwonił Rafael Diakon. Powiedział jej, że ku swojemu niezadowoleniu nie jest w stanie (nie będąc na miejscu) sprawdzić, kto doniósł na Dracenę i załatwił zamknięcie w areszcie Dyty i Draceny. Ale doradził jej, że jest mentalistką a na komendzie policji MUSI być informacja o źródle zgłoszenia. Paulina stwierdziła, że ma więcej możliwości - rozważa wykorzystanie Dyty. Nie powiedziała tego Rafaelowi.

Do Pauliny przyszła Dracena. Rano. Powiedziała, że potrzebuje od Pauliny dwóch rzeczy. Pierwsza, to źródło energii które będzie mogła zdestabilizować. Widząc zdziwienie Pauliny wyjaśniła, że potrzebuje coś, co umożliwi jej powtórzenie okoliczności w jakich doszło do splątania Dyty i Janka Bociana (tu uwaga MG: Dracena jest uzależniająca, ale osobiście. To wszystko to kłamstwo, ale Dracena w nie wierzy, bo sterowanie przez Janka). Paulina wybadała Dracenę i wyszło jej, że Dracena potrzebuje źródła o mocy pozwalającej na destabilizację wzoru Draceny (w czym jej wzór jest niestabilny; nie jest to aż tyle, ale musi być czyste by się nie Skaziła).
Drugą prośbą Draceny do Pauliny było zaklęcie mentalne. Dracena zaznaczyła, że JUŻ posadzili ją za pedofilię. Jeśli przestanie, to wtedy dzieciaki czeka straszny los. Jeśli nie, to będzie większy problem. Dracena zaproponowała zaklęcie - osoba, której krew (po sympatii) jest w artefakcie uzyskuje znieczulicę społeczną. Nie dba o to, co dzieje się z jej dziećmi, z innymi dziećmi, nie dba o takie sprawy z innymi. Paulinie się to nie spodobało, ale to dla dobra dzieciaków. Paulina wsadzi tam klauzulę, by nie trwało ponad tydzień. Dracena się zgadza, do tygodnia powinna zdążyć wszystko zrobić.
Dracena zapewnia komponent sympatii na bazie krwi oraz zapewnia komponent magiczny, żeby tylko ona mogła użyć tego artefaktu. Paulina przygotuje zaklęcie i razem sformują ten artefakt. Dracena przykryje aurę Pauliny swoją aurą, by nikt nie powiązał Pauliny z tym wszystkim.
Przy odrobinie szczęścia najpóźniej po dwóch - trzech dniach od uzyskania źródła Dracena skończy z odtrutkami i antidotum.

Wpierw jednak Paulina musi zrobić kilka telefonów.
Zadzwoniła do Dyty. Viciniuska akurat była na spotkaniu z Olafem (dotyczącym współpracy i aniołów), ale odebrała telefon Pauliny. Paulina powiedziała, że koniecznie muszą się dowiedzieć, kto zadzwonił na policję by donieść na Dracenę i Dytę. Paulina zaznaczyła, że przecież Dytę i Dracenę wyciągnął z aresztu ten tam, mafioso. Dyta usłużnie dopowiedziała, że to Franciszek Marlin. Paulina stwierdziła, że taki musi mieć wtyki w policji. Dyta dodała, że nie ma, ale będzie miał pod wieczór gdy przybędzie nowy policjant. Paulinę coś tknęło. Czy ona ma coś wspólnego z odwołaniem Artura Kurczaka? Nie, ale Silgor tak. Silgor Diakon się umówił z Franciszkiem Marlinem że za wypuszczenie i grupę usług on, Silgor, rozwiąże raz na zawsze wszystkie problemy z policją jakie przeszkadzają Franciszkowi Marlinowi w planach.
Paulina zorientowała się, jak wysoko musi w hierarchii Millennium być Silgor, skoro może po prostu zrobić coś takiego.
Mały problem - Paulina NIE CHCE przeniesienia Kurczaka. A Silgor zawsze dotrzymuje obietnic. Po chwili namysłu, wiła wpadła na rozwiązanie - wystarczy, żeby Marlinowi przestał przeszkadzać Artur Kurczak. Jako, że Paulinie zależy na integralności policjanta, rozwiązanie jest proste - należy zmienić cele Franciszka Marlina. Wiła powiedziała, że to dość proste; potrzebny jest jeden mentalista, jedna wiła i kilka quarków. Już to robiła.
Paulina jest BARDZO niezadowolona z takiego rozwiązania. Ale nie widzi chwilowo innego wyjścia. Chwilowo jednak jeszcze nic nie mówi Marii.

W świetle pojawienia się tajemniczego Olafa Paulina stwiedziła, że potrzebny jej detektyw magiczny. Ktoś kto jest w stanie spełniać odpowiednio wyśrubowane wymogi: nie jest z żadnej z gildii: KADEM, SŚ, Millennium, będzie dostępny w miarę szybko i będzie chciał wziąć zlecenie na śledzenie maga. (5 -> 3). Niestety, nie udało jej się znaleźć detektywa spełniającego te wymogi, ale dała radę znaleźć  młodszego partnera większego biura, który sprawdzi dostępne powszechnie dokumenty. Zawsze coś. Przynajmniej tania.

Żeby zdążyć przed przybyciem nowego terminusa, Paulina i Dracena zabrały się do pracy nad artefaktem. Artefakt się im udał - kontrolowany tylko przez Dracenę, każda osoba której krew zawarta jest w tym puzderku popada w "znieczulicę społeczną". (9+2 vs 11 -> 11). Paulina bardzo dokładnie kontrolowała i skanowała artefakt oraz aurę okolicy; stwierdziła, że lepsza paranoja niż wypuszczenie czegoś niewłaściwego na świat. I wyczuła delikatne drgnięcie. Nieznaczne, prawie niewykrywalne... wyczuła, że ktoś zorientował się, że jakiś krwawy artefakt został stworzony. To jedynie zmotywowało Paulinę do wzmocnienia wysiłków, by to Dracena była widoczna jako twórca artefaktu. Było to rzucane w mieszkaniu Draceny.
Gdy Paulina sobie poszła (późne popołudnie), została zlokalizowana przez drona Mariana. Mariana, który podłożył wcześniej podsłuch w mieszkaniu Draceny i wie, że te dwie współpracowały nad czymś nad czym nie powinny. 

Marian zdecydował się zadziałać. Przedtem już ściągnął z KADEMu w tajemnicy przed Andżeliką robota interrogacyjnego klasy "Enigma III". Ta lekka maszyna dostosowuje się do swojej ofiary, odpowiednio zaprogramowana, by zmienić wygląd i przesłuchać swoją ofiarę tak, by ta niczego nie pamiętała. Jest też w stanie przechwycić systemy komunikacyjne. Marian nie był uprawniony do wykorzystania tego robota, ale stwierdził, że nie ma ryzyka...
Więc Paulina wracała spokojnie do pokoju (by umówić się z Olafem i terminusem Gabli) i zobaczyła w bocznej uliczce starszą panią, zwiniętą w pół. Ta poprosiła o pomoc. Paulina zadzwoniła po karetkę i podała lokalizację ("Enigma III" to przechwyciła i zasymulowała rozmowę) po czym Paulina pochyliła się nad starszą panią. A tak naprawdę to była Enigma. Enigma szybkim ruchem złapała Paulinę za ręce i wstrzyknęła odpowiednie serum. Paulina odpłynęła. (4v5->6) Zdążyła jednak wcześniej otworzyć komunikację z Marią.

"Enigma III" zadała serię pytań: "Co Paulina zrobiła z Draceną". Ta odpowiedziała i wyjaśniła naturę artefaktu. Następne pytanie nie było o powód artefaktu - było o to, co Paulina zrobiła Dracenie. Ta oczywiście nie wie o co chodzi. Więc, czemu Dracena tak odmiennie się zachowuje. Paulina is WTF. Na to weszła wracająca po spotkaniu wiła. Dyta podeszła do Pauliny zaskoczona co ona tam robi i Enigma spróbowała swoich sztuczek na wile. Ta jednak jest niewrażliwa na ludzkie toksyny. Wiła rozerwała delikatną Enigmę na strzępy, informując Mariana (kontrolera) przez sensory Enigmy, że go dopadnie i zrobi z nim dokładnie to samo. Marian jest zaskoczony. Wiła nie powinna mieć tyle siły... a na pewno nie powinna mieć takiej natury. Dyta, rozrywając Enigmę zaznaczyła że Paulina jest pod JEJ ochroną i NIC się jej nie stanie. Jak skończyła, zabrała Paulinę i pobiegła z nią ile sił w wiłowych nogach (bijąc niejeden rekord prędkości) do domu. Tam, gdzie Dracena.

Dracena została odwołana z misji "uciszyć rodziców" i zajęła się składaniem Pauliny. Tymczasem Marian spanikował, błyskawicznie udał się na "miejsce zbrodni" i teleportował resztki Enigmy do swojego pokoju. W ten sposób sieć skanująca Andżeliki wykryła obecność Mariana i magii teleportacyjnej.
Niestety, z Draceny kiepski lekarz. Naprawiła Paulinę dodając jej PASKUDNĄ wręcz migrenę.
Paulina się obudziła. Maria oddała jej wspomnienia (Paulina nic by nie wiedziała - serum zadziałało skutecznie). Dyta odprowadziła Paulinę do domu i zdecydowała się zostać z nią na noc. Paulina nie prostestowała. Nie miała szans wygrać.
A Dracena kontynuowała operację "uciszyć rodziców". Idąc od domu do domu, pobierając krew, integrując ją w puzderku i dając przyjemność młodym by nie doprowadzić do cierpienia od uzależnienia.

Wieczorem do Wykopu Małego przyjechał nowy policjant - Krystian Linowęc.
Przybył też terminus Gabli - Alojzy Wołek.
Olaf, współpracujący z Dytą, odnalazł dla niej miejsce mocy w którym zbiega się węzeł "ley-line" okolicy. Za to Dyta opowiedziała Olafowi o co chodzi z aniołem i jaka w tym rola Nikoli.
Andżelika zrobiła opiernicz życia Marianowi. Nie może go odwołać, bo się wszyscy zorientują, że za tym stoi KADEM. Ale powiedziała, że wszystko udokumentowała i jeśli Marian zrobi JAKIKOLWIEK ruch, ona ma zamiar natychmiast to eskalować do sił KADEMu. Aha, i "Enigma III". Marian i tak ma po akcji zgłosić co się stało.
Gorzej, że Andżelika była tak zajęta sprawami KADEMu, że nie skupiła się zupełnie na działaniach Draceny.
Ale to zauważył dla odmiany mistrz Pieśniec. I ma dylemat - co z tym zrobić. Kto jest mastermindem. Jaki ma cel...
Oprócz Marii i Artura w Wykopie Małym jest jeszcze jeden członek Kółka Paranormalnego. Jest to bibliotekarka, Jagoda Musiąg. I Jagoda zauważyła, że zaczyna się dziać coś nie do końca normalnego. Starsza pani na razie obserwuje, bo nie wie na co patrzeć.

Następnego dnia Paulina obudziła się wczesnym południem. Nad nią, jak pies, stała Dyta. Krótki telefon do mistrza Przybylca - czy nie było problemów? Nie, żadnych. Nikola szybko się uczy i robi wyraźne postępy. Zaraz potem Paulina zadzwoniła do Andżeliki. Ta odebrała, zestresowana lekko (myślała, że Paulina skojarzyła Enigmę z KADEMem). Paulina spytała o telefon do terminusa Gabli i Andżelika z przyjemnością go podała. Andżelika spytała, czy z Pauliną wszystko w porządko. Ta myślała czy nie zaraportować ataku, ale głównym podejrzanym z uwagi na pytania dla Pauliny jest Draconis Diakon (ojciec Draceny), więc powiedziała, że nic się nie stało. Paulina spytała Andżelikę czy musi się martwić (słysząc zdenerwowanie audytorki), ale Andżelika odparła, że to sprawa KADEMu i nic co Paulinę musi martwić.
Andżelika poprosiła o spotkanie wieczorem.

Tymczasem Marian, zdenerwowany i martwiący się o Dracenę, zadzwonił do Olafa. Poprosił go o to, by Olaf przejął tam, gdzie Marian zawiódł. A za to Marian zrobi wszystko co w jego mocy, by Olaf dostał swoje "anioły", czymkolwiek anioł nie jest. Powiedział Olafowi wszystko odnośnie okolicy i historii tego miejsca, łącznie z historią barakowozu. 
Olaf nie wie jak pomóc Dracenie, ale zdecydowanie warto (jego zdaniem) przyjrzeć się barakowozowi...

Kolejny telefon Pauliny - tym razem do Rafaela Diakona. Poprosiła go o czyste źródło energii (takie, jakie wyspecyfikowała Dracena). Rafael się zdziwił - nic o czym wie nie wskazuje na to, żeby to miało pomóc Dracenie. Ale jeśli Paulina tego potrzebuje dla Draceny...
Kolejne zdziwienie - zostanie to odebrane wieczorem. Przez Dytę. Rafael zapytał Paulinę wyraźnie - czy może ufać wile? Odpowiedź Pauliny brzmi "chyba tak, ale i tak to zabezpiecz jak najbardziej możesz". Rafael przystał na jej prośbę.

Wizytówka mistrza Pieśńca w kieszeni Pauliny zawibrowała. Wyświetlił się termin spotkania - za godzinę. W jego apartamencie.
Dyta skończyła telefon. Powiedziała Paulinie, że rozmawiała z nowym policjantem. Krystian Linowęc z przyjemnością odpowiedział na jej pytanie "kto wsypał Dracenę i Dytę". Okazuje się, że był to ojciec Jana Bociana, doktor Ryszard Bocian. Dyta poinformowała Paulinę, że już odwiedziła go Dracena z artefaktem. Problem został rozwiązany.

Paulina poszła na spotkanie z tien Pieśńcem. Dyta jest ochroniarzem Pauliny. Czeka jak pies pod budynkiem apartamentu w którym mieszka tien Pieśniec.
Pieśniec zaczął od złośliwości - cóż to, czyżby Paulina wybrała już Millennium skoro Dyta jest jej ochroniarzem? Paulina odwzajemniła się, że przecież Nikolę uczy mag SŚ, na co Pieśniec się zaśmiał. Wybrnęła. Po chwili spoważniał. Powiedział, że szczątkowa sieć jaką ma wykryła teleportację w okolicach mieszkania Draceny a teraz Paulina ma ochronę. Coś się stało. Paulina potwierdziła, że coś się stało. Na to Pieśniec powiedział, że audytorka ma dużo lepszą sieć. I przecież jeśli on coś wykrył, ona też musiała. Zaznaczył, że Paulina ma prawo zażądać odczytów - taka rola audytora. Tak na wypadek, jakby nie wiedziała co i jak.
Pieśniec powiedział Paulinie, że on jest prawie pewny zwycięstwa. Musi jedynie znaleźć polityczny sposób, by tien Kazimierz Przybylec był prawdziwym mistrzem Nikoli, nieważne pod kim będzie Nikola formalnie. Bo wtedy Paulina wybiera między tym, co już zna i tym, czego nie zna.
Zaproponował jej też pomoc w zidentyfikowaniu napastnika. Paulina jednak nie chce eskalować polityki. Zwłaszcza, jakby to miał być mag Millennium.

Późne popołudnie. Jagoda zorientowała się, że Ania i Basia Pilen - dwie blondynki które regularnie odwiedzały bibliotekę zmieniły swoje zwyczaje. Porozmawiała z nimi i zorientowała się szybko, że coś jest bardzo nie w porządku. Skrzyżowała informacje z tym co działo się w Wykopie Małym w przeszłości i zadzwoniła do Marii z prostą wiadomością - "demony chyba powróciły".
A dla Marii jest to zupełnie nowy wątek. Pasujący perfekcyjnie do wszystkiego... poza atakiem na Paulinę (bo tam pytania były o Dracenę).

Maria powiedziała Paulinie (ukrywając źródło), że "demony chyba wróciły" - coś złego dzieje się dziewczynom, to wygląda na powiązane z historią obszaru, Maria wskazała barakowóz jako przykład. Maria powiedziała też Paulinie, że do tego pasuje odwołanie Artura (silna grupa przestępcza), ale zupełnie nie pasują pytania zadane Paulinie przez "Enigmę III" (choć sam atak by pasował). Maria jest w kropce, ale sama wiele nie ma. Paulina też jest w kropce.

Tymczasem Dyta zaproponowała Paulinie, by poszły do Franciszka Marlina i go "przekonały", że Artur Kurczak nie jest w kolizji z samym Marlinem. Paulinie NADAL ten pomysł się nie podoba, jednak nie ma innego wyjścia. A przynajmniej go nie widzi. 
Operacja udała się bezproblemowo - Dyta i Marlin się kochali a Paulina miała właściwie wszystkie jego myśli, całą osobość, całe jestestwo do zabawy. Było to gorsze niż gwałt. Paulina dotknęła jak najmniej - nie usunęła mu przestępczej natury, ale usunęła mu handel ludźmi i najczarniejsze elementy. Zdobyła informacje o tym, kto w przeszłości należał do "demonów" - ma czterech szanowanych obywateli pełniących istotne funkcje w społeczności którzy są "na pasku" Marlina. Wprowadziła Marlinowi, że Kurczak musi zostać - potrzebny jest taki policjant, który nie skorumpowany węszy jako papierek lakmusowy - inaczej wejdą tajniacy lub gorzej.
Co Paulinę zaskoczyło - Marlin nic nie wie o "powrocie demonów". A on powinien. Jedyna "operacja z dziewczynami" o jakiej wie to to, że Dyta coś robi "z przyjaciółką".

Oki. Po wykonaniu pracy Paulina czuła się brudna i wykorzystana. Dyta pojechała po źródło energii dla Draceny a Paulina, nadal w fatalnej formie psychicznej, poszła na spotkanie z Andżeliką. Ale wpierw Paulina zrobiła sobie gorący prysznic.
Przyszła Andżelika. Zapytała Paulinę, gdzie ta chce przekazać Nikolę. Paulina spytała Andżelikę co ONA uważa. Andżelika pomyślała chwilę. Jako audytor powinna wpierw pytać, potem wpływać. Ale... ale Kuba by tak nie zrobił. Więc Andżelika zdecydowała się zastosować casus Sar-Palina (gdy podał Raistlinowi czarną a nie czerwoną szatę). Powiedziała Paulinie, że Srebrna Świeca nic nie robi poza działaniem mistrza Pauliny (tu z rozmowy wyszło, że najpewniej mistrz Pauliny jest chory - jego moc z czasem spada; nie wiadomo jak szybko i czy o tym wie, ale blokował bez kłopotu Paradoksy Pauliny a Paradoksów Nikoli nie potrafi - mimo, że Nikola osłabiona przez EAMkę ma dużo niższą moc). Za to Millennium w odróżnieniu od Świecy robi bardzo dużo. Bardzo złych rzeczy. A Paulina - która według rekordów pomaga ludziom - pomaga Millennium w złych rzeczach.
Andżelika powiedziała, że nie ocenia. Paulina ma prawo robić co chce zgodnie z prawami magów. Dba o Nikolę etc. Ale po zakończeniu tego wszystkiego Andżelika uaktualnia rekordy o Paulinie i usuwa jej ludzistyczność.
Paulina się zasępiła (choć moment, KADEM ma rekordy o Paulinie? XD). Potem powiedziała w zaufaniu Andżelice, że Dracena w trakcie egzaminu uzależniła grupę osób. I że Paulina próbuje doprowadzić do odtrucia; by nic złego nie działo się z uzależnionymi ludźmi.
Andżelika zakwestionowała NIE motywy Pauliny ale jej wiedzę. Powiedziała, że są tu ludzie, którzy na stan wiedzy Andżeliki nie byli uzależnieni a jednak Dracena na nich polowała. Paulina stwierdziła, że nie wiedzieli o tym, że są uzależnieni. Przecież to nie musiało wyjść od razu. A Andżelika na to, że to bardzo wygodne. Może po prostu Paulina jest okłamywana.
Może...
Paulina powiedziała Andżelice, że ona widziała Dracenę w najgorszych chwilach. I Dracena NIGDY nie robiła takich rzeczy. Że nie jest taka. Andżelika powiedziała, że to samo powiedział jej kolega z KADEMu, Marian. I że zdaniem Andżeliki się myli, bo fakty są takie, że Dracena buduje sobie harem, tylko z kobiet. Podała jako przykład Anię i Basię Pilen. 
Tu Paulina powiedziała, że właśnie, odczyty - została zaatakowana i chciałaby zobaczyć odczyty jeśli Andżelika je ma. Andżelika się bardzo zmartwiła i lekko zaczęła kręcić. (6v7->8) Paulina wymusiła na Andżelice żeby ta powiedziała coś więcej i Andżelika powiedziała wprost - jeśli Paulina dostanie te odczyty, jakiś mag wyleci karnie i okrutnie z gildii. Nagle dla Pauliny wszystko stało się jasne - to Marian Łajdak użył jakiegoś ustrojstwa KADEMu by ją zaatakować. I to oznacza, że on też się martwił o Dracenę.
Andżelika pośrednio przeprosiła (nie może wprost nie przyznając się) Paulinę za tą sprawę. Paulina powiedziała, że wiła nie wybaczy. A Paulina nie ma jak wiły zatrzymać.
Andżelika drgnęła. Jej sieć detekcyjna coś wykryła. Szpital.

W szpitalu - Kasia. Jej tkanki transformują w energię magiczną. Do tego wprowadzony jest silny painkiller. Kasia się rozpada. Co więcej, choroba jest zaraźliwa - na wszystkie osoby uzależnione od Draceny i TYLKO na te osoby. Skojarzenie Pauliny po analizie wzoru czaru i objawów - coup de grace. Andżelika drgnęła. Druga flara - w mieszkaniu w którym jest Dracena i Dyta. (12+2 vs 5/7/9 -> 16) Wspierana przez Andżelikę Paulina jest w stanie rozwiązać chorobę na Kasi w błyskawicznym tempie. Andżelika bierze na siebie rozwiązanie problemu Maskarady i wysłała Paulinę do rozwiązania tego co ta chce rozwiązać. 

Paulina biegnie do mieszkania Draceny. Po drodze dzwoni do Dyty. Wiła odbiera. Zapytana, mówi, że jest w mieszkaniu z Draceną a Dracena właśnie rozstraja swój wzór źródłem magicznym. Aha, to to jest ta druga flara. Paulina pyta Dyty o co chodzi z Kasią. Dyta w ogóle nie rozumie o co chodzi.
Paulina słyszy w tle głos Draceny, która mówi, że nie przegra. Słyszy też głos Janka Bociana "co tu się dzieje" i odgłos wymiotów (Skażenie).
Wybite okno. Odgłosy walki. No, "walki" - wpadł Marian Łajdak, wziął na siebię Dytę która jednym ruchem złamała mu bark a drugim rzuciła na ścianę. Przedtem jednak zdołał przyjąć na siebie krew wiły w strzykawce - coś, co Dyta chciała Dracenie wstrzyknąć. To spowodowało konflikt serotoniczny na poziomie magicznym. Łajdak się wije ze złamanym obojczykiem.

Dyta podniosła Mariana pytając co zacz. Poznała audytora. Stwierdziła, że za to powinna go zabić. Janek spanikował "nie zabijaj go! Dracena, zatrzymaj ją". Dyta jednym uderzeniem obezwładniła Janka i doprowadziła go do wstrząsu mózgu, po czym skierowała się ku Dracenie. Diakonka sycząc, że tylko na to czekała zaatakowała wiłę płonąc otwartym wzorem, lecz wiła konsekwentnie żelaznym chwytem obezwładniała czarodziejkę.
I na to weszła Paulina. Dyta odepchnęła Dracenę i skierowała się ku Paulinie, lecz ta po prostu ją minęła i stanęła między nimi. Na ziemi - leży strzykawka w której BYŁA krew wiły (teraz jest w biednym Łajdaku). Łajdak słysząc Paulinę wyjęczał, że ma w kieszeni nieaktywne Spustoszenie, żeby dała to Dracenie do połączenia, że Dracena zawsze chciała być technomantyczna. Dyta stwierdziła, że Dracena powinna dostać jej krew. Powinna stać się częściowo wiłą. To jest jej przeznaczenie - tym powinna być, bo tym JEST.

Paulina nie do końca się z tym zgodziła. Wyciągnęła od Dyty o co chodzi - Dyta przyznała, że stoi za wszystkim (poza Kasią - do tego się nie przyznała). Od samego początku manipulowała Jankiem, by przejąć kontrolę nad Draceną by pokierować jej ewolucją. Zapytana jak to się ma do planów Silgora, wybuchła śmiechem. Silgor nic nie wie. On by nie aprobował. Ale Dracena może być tak lepsza od Dyty, może być wszystkim czym Dyta powinna być - więc Dyta musi przekształcić Dracenę. Dać jej radość. Lepsze życie. I usunąć się w cień, odejść.
Paulina facepalmowała. Silgorowi zależy na DYCIE nie DRACENIE. Ale wiła nie jest człowiekiem, ta miłość nie tylko jest ślepa ale i działa inaczej.

Dyta nie może skrzywdzić Pauliny (rozkazy Silgora) i nie może zadziałać wbrew niej. Powiedziała więc, że tylko chce się do Pauliny przytulić przed odejściem. Paulina się zgodziła, ale wpierw położyła Spustoszenie w taki sposób, by - jak Dyta cokolwiek zrobi - Spustoszenie spadło na Dracenę i się z nią połączyło. Sabotowana Dyta zaśmiała się. Paulina jest godnym przeciwnikiem dla Silgora. A zdecydowanie pobija prostą wiłę.

Paulina wyciągnęła z Dyty, że to co ona zrobiła wymusza na Silgorze zabicie Dyty. Draconis będzie żądał krwi, nie daruje. Silgor nigdy nie skrzywdzi swojej wiły, więc dojdzie do wojny domowej. Wojny, którą Draconis wygra. Silgor zginie, Kolekcja Silgora zginie, wszystko stracone. Więc Dyta musi umrzeć. Ignorując słowa Pauliny, zostawiła strzykawkę krwi na stole i wyszła. Jedyne, co udało się przedtem Paulinie osiągnąć to zmuszenie Dyty do udzielenia doraźnej pomocy Łajdakowi (nastawienie kości itd)

Natychmiast po wyjściu wiły Paulina zadzwoniła do Silgora. On ma NATYCHMIAST zatrzymać Dytę. Zakazać jej zrobienia czegokolwiek co ta chce zrobić. Aha, i ma nie przyjeżdżać w tej chwili. Paulina rozłączyła się bez słowa wyjaśnienia.

Dracena umiera. Rozstrojony wzór ją pożera. Paulina musi ten wzór czymś wzmocnić. Ma do dyspozycji swoją moc (kataliza, nekromancja, lekarz), ale to są słabe opcje (nie dość silne na pełną moc). Plus... Paulinie tak naprawdę nie pasują do Draceny. Ma jeszcze nieaktywne Spustoszenie... które częściowo pasuje. Ale ma też krew wiły, która też częściowo pasuje. Co wybierze Paulina? Nie wiedząc, co zrobić, i działając czysto instynktownie, Paulina postanowiła połączyć krew wiły i Spustoszenie. Trochę tego, trochę tego. Paradoks, jak cybergothka Diakonka. I to wintegrowało się w Dracenę.

Dyta wróciła, wyrzucając Paulinie, że skazała Millenium na wojnę domową, której Silgor nie może wygrać. Dyta słucha teraz Pauliny (rozkaz Silgora) - wiła się rozpłakała, gdyż to oznacza, że wojna domowa w Millennium jest nieunikniona (a Paulina łącząc jej krew i Spustoszenie sprawiła, że poświęcenie Dyty straciło sens).
Przed Pauliną tymczasem stanął problem Mariana Łajdaka. Nie chce, by wyleciał z KADEMu. Nie zasłużył na to, zwłaszcza, że ryzykował życie (a na pewno zdrowie) stając naprzeciw wiły i przyjmując krew przeznaczoną dla Draceny. Paulina ją przytuliła i powiedziała, że będzie dobrze. Poprosiła, by odesłała Mariana Łajdaka tak, by nie było wiadomo, że tu był. A potem ma wrócić. Łajdak oponował przeciwko oddaniu go pod opiekę wiły, ale nie mógł nic zrobić.

Dracena otworzyła oczy. Jest zdestabilizowana, nie panuje nad sobą i uważa się za potwora bo zrobiła straszne rzeczy. Paulina ją uspokoiła, powiedziała, że Dracena walczyła jak mogła. A Kasi i nikomu nic się nie stało. Dracena spytała, co z Dytą. Żyje. Dobrze. Dracena nie chce, by Silgorowi (i Dycie) uszło to na sucho. Nie tak łatwo. Dracena zupełnie nie kontroluje swojej formy - jej włosy przekształcają się w cyberlocki i wracają do normy, wypiękniała, jest kochliwa i chichotliwa jak wiła... zmienia jej się natura. Zaniepokojona nie pojawieniem się Dyty Paulina zadzwoniła do wiły. Ta odebrała telefon i powiedziała, że wróciła... i pożegnała Paulinę. Jednak Paulina usłyszała echo - Dyta jest gdzieś tu. Przebiegła się i znalazła ją w ciepłej wodzie w wannie, z podciętymi żyłami. Paulina zaczęła ją leczyć. Wiła zaprotestowała - wyjaśniła, że jej śmierć rozwiąże wszystkie problemy a Dracena będzie mogła dołączyć do Silgora, gdzie jej miejsce. Kolejny facepalm Pauliny. Paulina powiedziała też, żeby dała jej szansę przekonać Dracenę, by ta nie poszła do ojca.

Po podleczeniu wiły Paulina pomogła Dracenie dostać się do wiły. Dracena powiedziała Dycie, że nienawidzi jej jak niczego innego na świecie (prawda), ale nie chce jej śmierci i nie chce angażować ojca. Po czym wdały się w pyskówkę, po której Paulina odprowadziła Dracenę do łóżka. Natura wiły wygrała, Dracena pocałowała Paulinę, po czym zakłopotana poszła spać.
Paulina wyciągnęła wiłę z wody i wsadziła ją do innego łóżka.

Tymczasem skontaktowała się Andżelika z Pauliną. Paulina powiedziała, że tak, wybiera Świecę. "Nigdy nie było tu Mariana Łajdaka." Andżelika powiedziała, że ona zajmie się negocjacjami w imieniu Pauliny i podziękowała. Andżelika wszystko ma wyciszyć a za Nikolę mistrz Pieśniec ma za bardzo nie węszyć - wszystko ma pozostać w ukryciu.

EPILOG:

Spotkanie Pauliny, Silgora i Draceny było przeinteresujące. Dracena miotała się pomiędzy naturą wiły i koniecznością pocałowania i rzucenia się na Silgora a zimną nienawiścią Spustoszenia. Nie mówiąc o jej własnej naturze. Dracena zażądała od Silgora Paktu Krwi - cokolwiek stanie się Paulinie stanie się też Silgorowi. Albo Dyta umrze. Paulina się postawiła, Silgor powiedział, że to nie ma sensu. Dracena nie zabije Dyty. Dracena stwierdziła, że Silgor tego nie wie - ona ma zupełnie inną naturę i jest czymś zupełnie innym. Silgor się uśmiechnął - Dracena jest Draceną, niezależnie od tego z czego się nie składa. Nadal jest tą samą buntowniczką a Spustoszenie i Wiła jako jej dwa walczące ze sobą aspekty działają dla niej perfekcyjnie. Gdy Dracena zaprotestowała, zauważył, że przecież cybergothka - electro-DJ a potem coś do łóżka jako Diakonka to nic innego jak te dwa aspekty.
Paulina siedziała cicho. Nawet nie zauważyła tego wcześniej.

Dracena rzuciła się Silgorowi na szyję, pocałowała, po czym poraziła prądem. Powiedziała mu, że nigdy nie będzie elementem jego kolekcji. Silgor potwierdził - on jej nie chce. Dracena była wstrząśnięta. Silgor wyjaśnił, że on chce takie elementy, które trzeba kierować. Które są mniej autonomiczne. Dracena jest bardzo autonomiczna. Owszem, dziś powie "nie", jutro sama do niego przyjdzie i poprosi by być elementem kolekcji (tu Paulina zrozumiała, że w pewien sposób Dyta miała rację), ale Silgor odmówi. Dracena krzyknęła, że nie może być sama, nie wytrzyma - Spustoszenie to byt rojowy a wiły nie są w stanie być same. Silgor dodał, że Dracena NIE jest Spustoszoną Wiłą. Jest czymś więcej. A Paulina powiedziała, że Dracena nie jest sama.

Nikola trafiła do Srebrnej Świecy, pod mistrza Kazimierza Przybylca. Opiekunem głównym został Radosław Pieśniec. Paulina ma nadal wpływ i kontakt.

Artur Kurczak NIE jest przeniesiony. Zostaje w Wykopie Małym. (Paulina informuje o tym Marię)

Silgor i Dracena umówili się tak: Silgor chroni Dracenę i Paulinę by nic się im nie stało jakby były elementami Kolekcji. Jeśli którejś coś się stanie, Silgor idzie do tej drugiej i planują co dalej / jakie konsekwencje. Dodatkowo, Silgor dołącza do stronnictwa politycznego Draconisa.


TODO:
- przebadać mistrza Przybylca. Dowiedzieć się, czy faktycznie traci moc z czasem.
- pomóc Dracenie. Nie zostawić jej samej.
- rozwiązać problem Janka Bociana. Nie może nigdy już rozkazywać Dracenie.
- Olaf i poszukiwanie aniołów.
- Marian Łajdak - czy nadal kocha Dracenę?

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Wykop Mały
                    1. Królestwo
                        1. Ekskluzywne domki, tym razem wynajęte: Dycie, Dracenie, Radosławowi Pieśńcowi
                    1. Osiedle Ludowe
                        1. hostel Opatrzność, tymczasowo mieszka tam Paulina
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki


# Pytania:

N/A

# Stakes:

Żółw:

- Definitywnie: czy Dracena pójdzie w kierunku ekstadefilerki / wiły, czy uzyska korzyści technomantyczne?
Odpowiedź: ha, to bardziej skomplikowane. Wyjdzie w praniu :D.

- Gdzie trafi Nikola?
Odpowiedź: SŚ.

Kić:

- Czy DiDi wpadnie w dodatkowe kłopoty ze Świecą?
Odpowiedź: Nie, bo Paulina.

# Idea:

N/A

# Zasługi
- mag: Paulina Tarczyńska, która dokonuje dwóch trudnych wyborów - co do Nikoli i co do Draceny... I co do żadnego nie jest w 100% przekonana.
- czł: Maria Newa jako osoba operująca w Kopalinie w poszukiwaniach dla Pauliny.
- mag: Nikola Kamień jako uczennica mistrza Pauliny, chroniona przez terminusa Gabli.
- mag: Dracena Diakon jako stereotypowo rozumiana Diakonka, choć ciągle walczy (nieskutecznie) z kontrolą Jana i Dyty.
- vic: Dyta jako wiła i mastermind ze strony Millennium której celem jest przekształcenie Draceny.
- czł: Jan Bocian jako zdeprawowany przez Dytę nastolatek który myśli, że wiła działa na jego korzyść.
- czł: Artur Kurczak jako policjant siwiejący ze zgryzot w szpitalu.
- mag: tien Radosław Pieśniec jako mistrz SŚ, gracz chcący pokonać masterminda Millennium... choć nie wie w co i kto. Chwilowo gra "przeciw sobie".
- mag: Andżelika Leszczyńska jako audytor z KADEMu która łamie zasady audytu, bo Kuba Urbanek by tak zrobił. Ale opieprza Mariana, bo jest hipokrytką.
- mag: Marian Łajdak jako eks-Draceny który zapomina, że jest audytorem z KADEMu.
- mag: Olaf Rajczak jako eks-KADEMowiec i przedstawiciel Trójkąta Szczęścia - demonolog - polujący na anioła. Bo tylko anioł pokona demona lorda Urbanka.
- czł: Katarzyna Trzosek jako - ku przerażeniu Draceny - już szczęśliwa zabawka Jana Bociana.
- czł: Ania i Basia Pilen jako dwie słodkie bliźniaczki-blondyneczki w mocy Jana Bociana.
- czł: Franciszek Marlin jako członek "bieda-mafii" który chcąc zostać prawą ręką bossa współpracuje z Dytą dając jej to, czego Dyta pragnie.
- czł: Ryszard Bocian jako lekarz i ojciec rodziny, który zapłacił za ochronę swojego syna.
- mag: Kazimierz Przybylec jako nauczyciel i mentor Pauliny o dość barwnej przeszłości i nędznej teraźniejszości.
- mag: Alojzy Wołek, jako terminus Gabli o niezłym potencjale materio-militarnym.
- czł: Roman Pilen jako ojciec Ani i Basi, odwiedzony już przez Dracenę. 
- czł: Maria Pilen jako matka Ani i Basi, odwiedzona już przez Dracenę.
- czł: Juliusz Kwarc jako dystrybutor wakatów policyjnych w stołecznym mieście Kopalin.
- czł: Krystian Linowęc jako policjant będący agentem Franciszka przekierowany tu przez Juliusza Kwarca w ramach dealu między Silgorem a Franciszkiem Marlinem.
- mag: Anita Rodos jako uczennica na detektywa, młodszy partner Waldemara Zupaczki która przeszukuje dokumenty dla Pauliny. Tak bardzo zadanie.
- czł: Jagoda Musiąg jako bibliotekarka i członkini Kółka Paranormalnego do którego należy Maria i Artur.
- mag: Rafael Diakon jako źródło bardzo niefortunnego źródła magii.
- mag: Silgor Diakon jako o dziwo łagodzący i rozsądny Diakon.

# Dark Future:

- Dracena wstrzykuje sobie krew wiły. Staje się ekstadefilerką bardzo bliską w mocach wile. Taką, jakiej potrzebuje Silgor.
- Dracena dołącza do Kolekcji Silgora.
- KADEM przydziela Nikolę Srebrnej Świecy.
- Artur Kurczak jest przeniesiony do Kopalina. Bartosz Bławatek jest spacyfikowany. Wykop Mały jest pod silną kontrolą bieda-mafii.