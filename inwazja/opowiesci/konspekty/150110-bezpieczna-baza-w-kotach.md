---
layout: inwazja-konspekt
title:  "Bezpieczna baza w Kotach"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141123 - Druga kradzież Wyzwalacza (SD, EP)](141123-druga-kradziez-wyzwalacza.html)

### Chronologiczna

* [141115 - GS "Aegis" 0002 (SD, PS)](141115-gs-aegis-0002.html)

### Inne

Zaćmienie, kontekst kralotha w Kotach:

[100103 - Koszmar w Kotach (Ka)](100103-koszmar-w-kotach.html)
[100107 - Sojusz z piekła rodem (Kr, Ma)](100107-sojusz-z-piekla-rodem.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Gospodarstwo agroturystyczne. Sabina, Warmaster i Paweł rozpakowują manatki, gdy słychać pukanie do drzwi. Julia Weiner. Warmaster i Julia się bardzo cieszą na swój widok. Julia powiedziała, że ma dwóch magów którzy potrzebują pomocy (są Spustoszeni) a ona ma jednego terminusa (Celestynę). Julia powiedziała, że ci magowie to Marian Welkrat (technomanta, PŚ) i Zenon Weiner (technomanta, SŚ). Zapytana przez Sabinę co wie na temat Spustoszenia powiedziała: Marian pomagał Aurelii w unieszkodliwianiu Spustoszenia i badaniu Spustoszenia a Zenon od zawsze Spustoszeniem się interesował. Powiedziała też Sabinie o tym, że istnieje termin "agent Spustoszenia", jednostka autonomiczna będąca kontrolerem. Aktualne Spustoszenie zachowuje się inaczej niż to pierwsze.

Julia powiedziała też, że Aurelia Maus dawno, dawno temu pracowała nad unieczynnieniem Spustoszenia - bowiem Spustoszony eks-mag odzyskiwał część pamięci i mocy. Niestety, Aurelii nie udało się nigdy rozwiązać problemu "Spustoszenie przejmie kontrolę". I między innymi o tym Zenon najpewniej chciał z Marianem porozmawiać. Sabina zdecydowała się na naciśnięcie Julii. (10v8->8) Julia powiedziała, że Aurelia pracowała nad usunięciem zębów ze Spustoszenia. Nad wyłączeniem Spustoszenia przy zachowaniu jego korzystnych wartości i własności. I wszystko wskazuje na to, że Marian wyniósł tą wersję Spustoszenia z Powiewu Świeżości. A potem - nagle - doszło do uruchomienia Spustoszenia. A Mariana i Zenona nigdzie nie ma. A Zenon był magiem, który wcześniej badał Spustoszenie w Świecy; miał uprawnienia do tego, tylko że na pewno nie wyniósł bo wynieść nie mógł.
Sabina ma pierwszy ślad. Słaby, ale zawsze.

Zapadła mgła. Lekko magiczna. Lekko podalarmowana Sabina podskoczyła, ale Warmaster ją powstrzymał. Wysłał dwa gorathaule (tresowane gorathaule XD) żeby znalazły Rdzeń Spustoszenia i Mariana oraz Zenona. Julia nerwowa, Paweł nieszczęśliwy - gorathaule zjedzą magów Świecy. Zaczął się uzbrajać. Sabina dostała nowe zadanie - ostrzec magów Świecy. Póki nie dojdzie do nich wieść, że Sabina nastawiła Julię przeciwko Tamarze i jej zespołowi.

Sabina i Paweł poszli do domu kultury, gdzie znajduje się aktualna siedziba terminusek SŚ. Wszystkie trzy są na miejscu. Sabina weszła, przedstawiła się (i Pawła) i powiedziała, że jest z KADEMu. Tamara Muszkiet, dowodząca operacją, przyjęła to do wiadomości i przedstawiła siebie i swoje podwładne. Sabina powiedziała o gorathaulach, Tamara powiedziała że jej to nie przeszkadza pod warunkiem że gorathaule ich nie będą atakować. Impas, bo Sabina i Paweł nie wiedzą czy to możliwe. Tamara powiedziała też, że ona z ramienia SŚ dowodzi operacją "usunięcie Spustoszenia". Sabina zaproponowała połączenie sił. Tamara się zgodziła, pod warunkiem, by to ona dowodziła. Po krótkiej dyskusji z Sabiną, podzielili się w następujący sposób:
- Tamara, SŚ oraz Warmaster biorą na siebie las i atak na Rdzeń Spustoszenia po jego znalezieniu. 
- Sabina i Paweł biorą na siebie Koty i obronę Kotów.
Tamara powiedziała też KADEMowcom, że w Kotach znajduje się agentka Millennium - Marysia Kiras. Marysia dała radę odeprzeć atak Spustoszenia, ku wielkiemu zdziwieniu Tamary, jak jeszcze magowie SŚ nie pojawili się na pozycji. Jak to zrobiła? Nie wiedząc z czym ma do czynienia chciała wprowadzić Spustoszenie do kościoła i zamknąć tam Spustoszonych jak w pułapce a zgłupiałe Spustoszenie się wycofało.
Jadwiga po akcji porozmawiała z Marysią i zaproponowała współpracę, ale Marysia nie chciała współpracować z SŚ. Może będzie chciała z KADEMem.
Tamara powiedziała też KADEMkom, że zidentyfikowali że na tym obszarze działa dwóch Spustoszonych magów - Zenon Weiner oraz Oktawian Maus.
I na takie stwierdzenie plan został ustalony. 

Sabina stwierdziła, że trzeba spotkać się z Marysią. Ale wpierw połączyła się ze swoimi kontaktami w Millennium - kim jest Marysia, klasyfikowana jest jako agent Millennium. Wynik był wielce interesujący. Marysia NIE jest agentem Millennium. Jest osobą chronioną przez program ochrony Millennium. Nie wie nic o magii ani elementach magicznych. Nie ma w pełni wymazanej pamięci, usunięto jej jednak pamięć o magii.

Aha... To ciekawe. Przechwycili Marysię gdy szła sobie z koleżanką, zaprosili na lody (Marysia wybrała miejsce do jedzenia lodów). Tam Sabina i Paweł przedstawili jej się jako (zgodnie z linią Millennium) członkowie agencji ubezpieczeniowej Wieczność. Marysia się ucieszyła ale i zdziwiła. Powiedzieli, że to ciąg dalszy tych problemów z narkotykami co ostatnio (taką historyjkę powiedziano Marysi) i że potrzebują jej pomoc. Marysia powiedziała, że zorganizowała małą akcję obrony cywilnej i spytała, czy oni chcą ją przejąć. Nie. Chcą współpracować. Sabinie spodobało się to, że Marysia jest odpowiednio nieufna, ale nie blokuje.
Sabina zauważyła coś niepokojącego. Jedna osoba w pomieszczeniu... nie pasuje. Jest "zimny". Jakby nie był niczym szczególnym zainteresowany. Czyta gazetę, ale przerzuca strony z tą samą prędkością. Ogólnie, nie podoba jej się to. Bardzo. Zdecydowała się poinformować o tym Marysię - napisała na kartce papieru że ten mężczyzna jest podejrzany. Marysia spojrzała ze zdziwieniem. To lokalny piekarz. Jej dobry znajomy. Czemu niby piekarz miałby być problematyczny? O_o. Sabina, że to te narkotyki. Marysia, że wstanie i z nim pogada. NIE! Sabina nie chce, by Spustoszenie wiedziało że oni wiedzą. Szybko powiedziała, że narkotyki mogą być podane mu np. w jedzeniu.
...teraz Marysia jest niespokojna, że ona sama może być pod wpływem tych środków, albo że ktoś może jej podać.

Ustalili, że Marysia zaprosi piekarza do siebie do domu i tam sprawdzą stan piekarza. Piekarz ma żonę i jedno małe dziecko. Po południu zatem Marysia poszła do piekarza i zaprosiła go do siebie. Przedtem jednak ustalili jakiś plan działania - Paweł siadł nad VISORem i ustawił go w tryb... rentgenu. Bo rentgen zauważy Spustoszenie. I przygotował też sól fizjologiczną.
Plan prawie się udał. Piekarz został zaproszony, podali mu coś (substancja ogłuszająca, trucizna) w ciastku i nieszczęśnik padł na ziemię. I tu pojawił się niespodziewany problem. Bo Marysia nie chciała się zgodzić na wstrzyknięcie piekarzowi nieznanej, potencjalnie niebezpiecznej substancji strzykawką. Paweł zarzekał się, że to kontrast, ale MUSIAŁ symulować wstrzyknięcie czegoś - bo inaczej widok na Spustoszeniu może wyglądać dość... drastycznie. A Marysia przeciwna - nie chce by w JEJ domu ludziom coś wstrzykiwano.
W końcu Paweł zadowolił się czymś do picia. "Ale efekt będzie nie dość dokładny". Prześwietlono piekarza.
Jest Spustoszony. Marysia ma TAAAAAKIE oczy. Paweł mówi "no bo narkotyk", ale faktycznie wygląda to nieco upiornie. Spustoszenie wygląda na takie co jest w stanie płytkiego uśpienia.

Z tego eksperymentu wyniknęły następujące rzeczy:
1. Marysia jest dość spacyfikowana. Na takie rzeczy nie była przygotowana. Współpracuje z KADEMem.
2. Spustoszenie działa inaczej. Konsekwentnie i w formie ukrytej przejmuje Koty. Zupełnie inaczej niż kiedyś.
3. Spustoszenie na pewno wie, że są tu magowie KADEMu. Ci sami, co zatrzymali Estrellę i Netherię ostatnio.
4. Marysia, zbadana rentgenem, na pewno nie jest Spustoszona.

Warmaster został poinformowany przez Sabinę używając ptaka o tym, że Spustoszenie tu jest, działa w mieście i że szukają bezpiecznej bazy. Ptak wrócił z informacją "już idę".
Marysia powiedziała, że nie podoba jej się pomysł by siedzieć i nic nie robić. Paweł powiedział, że będą potrzebowali bazy. Marysia zaprowadziła ich do bunkra na łące między lasem a Kotami.
Bunkier całkiem niezłe miejsce, poniemiecki. Sęk w tym, że bunkier to deathtrap.
Drugim pomysłem Marysi był kościół. Romański. Kilka razy spalony, więc może być mniej obronny. Wciąż lepsze niż bunkier...

Po drodze do kościoła Warmaster przechwycił Zespół i Marysię. Przedstawili go Marysi. Warmaster chce powiedzieć do czego doszedł, ale strasznie przeszkadza mu obecność Marysi...

"Mgły przynoszą nam zapowiedź bezpieczeństwa" - Warmaster do Zespołu próbując być sprytny.
"Mgły błyskają diodami ekranów ochronnych" - Warmaster do Zespołu próbując coś przekazać przy Marysi.

Warmaster poprosił Marysię o oddalenie się. Poszła do kościoła. Zespołowi powiedział, że wprowadził nowy typ mgły - ten skoroduje ekrany ochronne Spustoszenia oraz wymusi na Spustoszeniu wydatkowanie dużo większej energii na podtrzymanie swoich dron w mieście. Niestety, na dłuższą metę oslabi to też ich. 
Aha, terminuski zbliżają się do Rdzenia Spustoszenia; zbliżają się jednak bardzo powoli bo sam Rdzeń ma własności astralne (co jest nowe z punktu widzenia Spustoszenia). I Spustoszony jest... kopiec miślęgów!
Po wymianie wiedzy, Warmaster poszedł po Julię, do KADEMowego agroturystycznego mieszkania. Odbudowali swój mindlink. A Zespół poszedł do kościoła.

Przed kościołem - zabawna scena. Chłopak podchodzi do kościoła, zatrzymuje się... i odchodzi... odwraca się i podchodzi... 
A Paweł zauważył, że mgła zatrzymuje się przed kościołem. Ciekawe. Zupełnie jakby nie mogła tam wejść.

Plan był prosty - najpewniej to jest Spustoszony a kościół ma jakąś siłę odpychającą. Sabina chciała porozmawiać ze Spustoszeniem a w tym czasie Paweł sprawdził czy może wejść do kościoła. Jak tylko przekroczył próg, mgła zaczęła napływać do środka budynku a chłopak zaczął zachowywać się normalnie - chce wejść do kościoła i wszedł. Paweł nie jest w stanie odbudować bariery.

Chłopak wszedł do kościoła i podszedł do Marysi. Sabina podeszła do nich, a Marysia poprosiła ja o moment. Paweł próbował podsłuchiwać, niestety, bez większego powodzenia. 
Gdy skończyli rozmowę, Marysia przedstawiła chłopaka jako Roberta, swojego kolegę i ucznia z Tarczy. Gdy Robert wyszedł z kościoła (na polecenie Marysi), Sabina spytała czemu się tak zachowywał. Narkotyki. Niestety, nie udało jej się zauważyć subtelnej zmiany zachowania Marysi wywołanej dowiedzeniem się czegoś przez działanie artefaktu.

Sabina i Paweł spróbowali ocenić sytuację. Jak obronny jest ten kościół. Udało im się (7v7->7 tak ale) tak oszacować to miejsce, że są w stanie przez krótki okres chronić ten kościół nawet przed przeważającymi atakami ze strony Spustoszenia, choć albo przy stratach cywilnych albo przy złamaniu Maskarady. Czyli kościół się nadaje jako miejsce obronne.
Warmaster przyprowadził Julię. Ta powiedziała, że się sama przeskanowała rentgenem; Paweł spytał czy on może sprawdzić. Julia się zgodziła.

Marysia powiedziała, że kościół jest miejscem kultu i ogólnie VISOR nie powinien być tu używany. Zmieniła zdanie co do tego, czy to dobra baza; chciała zaproponować kolejną. Siluria spróbowała ją odczytać; czemu Marysia nagle tak bardzo zmieniła zdanie? Odczytała ją - Marysia po wejściu do kościoła czegoś się dowiedziała lub coś zauważyła; coś, co sprawiło że zauważyła że ich historyjka nie trzyma się kupy i straciła do Zespołu zaufanie. Nie chce ich w kościele, bo boi się ofiar. Też i Marysia dała radę odczytać Sabinę; widzi, że ta wie dużo więcej i to jedynie potwierdziło jej odczucia i podejrzenia.

Siluria spróbowała porozmawiać z Marysią. Ta powiedziała, że to nie jest pierwszy raz kiedy ten kościół płonął. Dwa razy płonął już po Zaćmieniu (jeden poważny, jeden niegroźny pożar). Ona się boi, że oni coś zrobią i ucierpi lokalna społeczność. Bo znowu pojawi się ktoś z zewnątrz i nawet jeśli te "narkotyki" są niebezpieczne, to może być zneutralizowane dla osób spoza Kotów a Koty ucierpią i nikt się nie przejmie.
Zespół powiedział, że te narkotyki dają możliwość kontroli ludzi. I że nie są agentami ubezpieczeniowymi, ale nie mogą powiedzieć kim są. Na to Marysia, że ona nie może współpracować z nimi bez większej ilości informacji, chyba, że skupią się też na Kotach a nie tylko na problemie jaki ONI mają.
...Zespół serio rozważa rzucenie na nią zaklęcia kontroli umysłów mimo jej statusu... bo ona pamięta rzeczy jakich nie powinna i to # Spustoszenie = są w stanie się wybronić.
Tu się pojawił ciekawy problem rozważany między Sabiną i Pawłem - czy to możliwe, że ona jest viciniusem. Tzn. po odpowiednim napromieniowaniu faktycznie człowiek może uzyskać swoistą odporność mentalną. Sabina nie jest do końca pewna (5v6->3) czy ta sytuacja mogła tu zaistnieć.

Dobrze. Lepiej nie czarować. Sabina użyła swojego uroku na Marysię, przekonując ją że bez ich pomocy Koty są zgubione i że ten teren i tak jest pod kwarantanną. Udało jej się (10v7->12) przekonać i Marysia zdecydowała się współpracować. Za namową Sabiny, poszła porozmawiać z księdzem (choć zestresowana), by udostępnił kościół do obrony. Poszła do księdza, który powiedział jej że to nie pierwszy raz w jego życiu i że odda im kościół, odwoła wszystkie msze a sam się oddali i opustoszy kościół ze swoich ludzi.

Julia podeszła do Zespołu i powiedziała im, że ten kościół jest niesamowity - zupełnie, jakby sam się regenerował. Lub jakby budowniczy używali magii by kościół wrócił do normy po pożarach. Lekko dociśnięta przez Zespół (ale bez konfliktu jako akt dobrej woli) Julia opowiedziała, jak w Kotach "elementy SŚ o wątpliwej moralności i reputacji" próbowały opracować sposób współpracy z kralothem celem ekstrakcji kryształów Quark z ludności cywilnej. Szybko dodała, że zostali zatrzymani. Ale Paweł wytknął, że jak na spokojną mieścinę bez ciekawej historii to Koty mają dość sporo wydarzeń. Czyżby historia Kotów była utajniona?
Dodatkowo Julia zainteresowała się księdzem. Jeśli ksiądz mówił, że już kiedyś ten kościół służył jako miejsce obronne to oznacza, że to pamięta. A jedynym takim wydarzeniem był kraloth. Jeżeli tak, to ksiądz pamięta, co jest niemożliwe - magowie SŚ szli osoba po osobie, bardzo dokładnie, by ta sprawa nigdy nie wypłynęła (i tak wypłynęła, ale z przyczyn politycznych w samej SŚ).
Paweł rzucił hipotezę - ksiądz jest magiem. Niestety, raczej księdza się już nie znajdzie.

Wróciła do kościoła wstrząśnięta Marysia. Powiedziała, że zebrała Tarczę i wysłała na przeliczenie ludzi. Brakuje 16 osób. Aż 16! W czym Tarcza nie potrafiła się ich doliczyć, dopiero jak Marysia poszła osobiście sprawdzić to okazało się, że kogoś nie ma. Dopytana kto zniknął pierwszy powiedziała, że pierwszy zniknął Korwin, wcześniej, ale... <tu wyraźne oznaki walki z wymazaniem pamięci> to niemożliwe. Naciskana przez Sabinę i Pawła zaczęła sobie przypominać. W kościele zapaliły się wszystkie świeczki i zgasły, silna energia skierowana na Marysię. Przypomniała sobie - Korwin Morocz, ojciec Basi; jedna z dwóch osób które zniknęły w czase "Aegis 0002". On poszedł w las i nie wrócił.
Czyli coś w kościele powoduje, że albo Marysia sobie przypomina albo nadaje jej zewnętrzne wspomnienia. To drugie by implikowało, że ksiądz nie musi być magiem - po prostu ma cudzą pamięć.

Wszystko wskazuje na to, że Koty są pod wpływem operacji czyszczenia i część z tych 16 osób zginęła. I mają welony na pamięci, by nie orientowali się, że części osób nie ma, by Świeca mogła sensownie rozłożyć śmierci w czasie. Ale Marysia właśnie "zerwała" swój welon częściowo.

Wiadomość od Srebrnej Świecy do KADEMu. Terminuski ostrzegają, że Rdzeń Spustoszenia znajduje się w okolicach Spustoszonego Kopca Miślęgów. Jest tam też aktywność kralotyczna. Ale wszystko wskazuje na to, że to nie jest prawdziwy kraloth, a jego wyobrażenie, co powoduje że terminuski nie do końca wiedzą jak z tym walczyć (czego nie powiedziały). Ściągają wsparcie.

# Streszczenie

Zlokalizowany zostaje Rdzeń Spustoszenia w Kotach (w okolicy Kopca Miślęgów); polują na niego: Julia Weiner z Powiewu (ratować Spustoszonych magów / ekspertów od Spustoszenia Mariana Welkrata i Zenona Weinera) oraz Tamara Muszkiet ze Świecy. Świeca niechętnie łączy siły, więc podzielili się na obronę (KADEM) i atak (Świeca). Okazało się, że to Spustoszenie działa inaczej; najpewniej kontroluje sporą część miasteczka ale... z ukrycia. Julia opowiedziała o Asterze i jego współpracy z kralothem. Dodatkowo okazało się, że kościół w Kotach ma własności magiczne - nie wiadomo jednak jakie.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Kościół romański
                            1. Krypta
                        1. Dom kultury
                        1. Lodziarnia "Wafelek"
                    1. Dwie hałdy
                        1. Agroturystyczny domek
                        1. Dom przechodni przyjaciół Marysi
                    1. Las Stu Kotów
                        1. Bunkier
                        1. Kopiec Miślęgów

# Pytania:

Ż - Dlaczego, zdaniem Julii, w pobliżu był Marian Welkrat.
K - Powiedział jej, że tam ma rodzinę.
Ż - Jakie jest, zdaniem Warmastera, powiązanie Mariana z początkiem Spustoszenia.
B - Próbowal zbadać Spustoszenie.
Ż - Jak swoją obecność w tym obszarze motywuje Iza Łaniewska.
K - Twierdzi, że ma doświadczenie w walce ze Spustoszeniem.
Ż - Dlaczego KADEM autoryzował właśnie Warmastera jako główne wsparcie?
B - Działa przez proxy.
Ż - Czemu tien Muszkiet bardzo niechętnie współpracuje z KADEMem?
K - Żywi głębokie przekonanie, że to KADEM stoi za Spustoszeniem.
Ż - Dlaczego doszło do emisji Pryzmatu podczas Rezonansu?
B - Ktoś próbował duplikować efekt Wyzwalacza w miejscu mocy (powiązane z "Drugą Kradzieżą Wyzwalacza").
Ż - Co bardzo niewygodnego dla SŚ widziała dziennikarka?
K - Widziała coś, co sprawia, że ludzie w tej okolicy stają się bardzo wyczuleni na przejawy magii.
Ż - Dlaczego ludzkie defensywy odparły Spustoszenie przed pojawieniem się magów?
B - Ludzie broniąc się odsłonili bardziej atrakcyjny cel.
K->Ż - Dlaczego Marysi nic się nie stanie.
Ż - Ponieważ, mimo, że nie ma o tym pojęcia, jest klasyfikowana jako członek Millennium.
B->Ż - Kto i na jaki wypadek trzyma w odwodzie Ciernia?
Ż - Malignus trzyma Ciernia w odwodzie na wypadek pojawienia się agentów Inwazji.

N/A

# Zasługi

* mag: Siluria Diakon jako zawsze urocza Diakonka niezbędna do uwodzenia (nie erotycznego :P) Marysi.
* mag: Paweł Sępiak jako ekspert od "narkotyków" Spustoszających mieszkańców Kotów.
* mag: Tamara Muszkiet jako głównodowodząca terminusami SŚ na operacji która bardzo nie lubi KADEMu
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, tym razem cicha (niepokojący objaw) terminuska SŚ na operacji usunięcia Spustoszenia
* mag: Judyta Karnisz jako całkowicie przezroczysta terminuska SŚ wykonująca rozkazy
* mag: Julia Weiner jako martwiąca się o członka swojej gildii który został Spustoszony. Też, martwi się, że mógł coś zbroić
* mag: Marian Welkrat jako Spustoszony ekspert od Spustoszenia z ramienia Powiewu Świeżości (i uczeń Aurelii Maus)
* mag: Zenon Weiner jako Spustoszony ekspert od Spustoszenia z ramienia SŚ
* mag: Oktawian Maus jako Kolejny Maus Który Ma Pecha. Spustoszony.
* czł: Bożena Górzec, właścicielka gospodarstwa agroturystycznego gdzie zabunkrował się KADEM
* czł: Marysia Kiras jako sensei "Tarczy", która nie wie że jest klasyfikowana jako agent Millennium i przypomina sobie rzeczy których nie powinna wiedzieć
* czł: Robert Mięk jako uczeń "Tarczy", który został najprawdopodobniej Spustoszony ale nadal współpracuje z Marysią Kiras
* czł: Korwin Morocz jako ojciec Basi, który zniknął w okolicach "GS Aegis 0002" (2 tygodnie temu)
* czł: Jerzy Buława, ksiądz w Kotach który wie kiedy należy oddać kościół magom i uciekać
* vic: echo Lugardhaira, kralotha z przeszłości Kotów i obrońcy Pieluszki.
* vic: miślęg Pieluszka, kontroler Spustoszenia (LOL).
* czł: Jolanta Pirat jako dziennikarka zajmująca się Kryptą. A chwilowo... Świecą.

# Czas:

* Dni: 1

# Wątki

- Trzeci kontroler Spustoszenia