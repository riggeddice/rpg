---
layout: inwazja-konspekt
title:  "Klątwożyt z lustra"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170323 - Oszczędzili na rurach (PT)](170323-oszczedzili-na-rurach.html)

### Chronologiczna

* [170323 - Oszczędzili na rurach (PT)](170323-oszczedzili-na-rurach.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Dzień 1: 

Trzy dni po poprzednich wydarzeniach, Paulina próbowała zrozumieć, jak to lustro działa i do czego służy, pod nadzorem Eneusa Mucro. Wykorzystała też swoje znajomości wśród magów z półświatka by się też dowiedzieć jak to wygląda i do czego to lustro służy - to lustro i inne obiekty lustropodobne dookoła tego konceptu (-1 surowiec) (9v8/10->R(10: )). Skontaktowała się z Herbertem, magiem, któremu kiedyś pomogła po działaniach z lustrami. On nie opowiadał szczegółów, ona nie pytała.

Herbert się zasępił. Naprawdę. Powiedział jej, że to nie wygląda na lustra Arazille. Ostrzegł Paulinę, by ta bardzo uważała; sygnatura tego lustra, tak jak ją przesłała Paulina, wskazuje na eks-klątwożyta. Klątwożyt tam był i się wydostał. Nie wie jakiego TYPU jest to klątwożyt; lustro wskazuje, że memetyczny. Infekuje religią... myślą... kultem.

* Jaka jest szansa, że to złapałam? - Paulina
* Zrób samodiagnostykę... - Herbert

Paulina powiedziała swoją historię Herbertowi i potwierdził ją Eneus. Herbert powiedział, że klątwożyt potrzebuje do życia silnych kanałów magicznych - lekko skażony człowiek nie kwalifikuje. To musi być mag lub niektóre typy viciniusów. Zdaniem Herberta, takie konstruminusy jak jej MOGĄ być zainfekowane, acz niekoniecznie klątwożyt będzie w stanie przejąć memetyczną kontrolę. Niebezpieczna za to jest kamienica Archibalda...

* To ustrojstwo ciekło 3 tygodnie. Klątwożyt sam się wydostał? - nieszczęśliwa Paulina
* Nie. Jakiś mag musiał spotkać lustro przed Tobą. I w ten sposób klątwożyt się wydostał - Herbert, ostrożnie

Eneus zauważył, że czasem magowie go wymazują; niekoniecznie chcą, by coś wiedział. Pamięta, że WIDZIAŁ tą sygnaturę, ale nie pamięta gdzie. Jakiś mag musiał go wymazać. Herbert poszerzył - takie lustro jak to, taki typ artefaktu czasem był wykorzystywany do ukrycia, że poziom mocy spadł podczas Zaćmienia. I... wyrzuciłby go, bo zorientował się, że lęgnie się tam klątwożyt?

Herbert obiecał, że wieczorem pokaże jej kilka technik w jaki sposób może wykryć klątwożyta. Zadziałają, jeśli klątwożyt jest w Paulinie w fazie uśpienia lub jakiejś początkowej. Przy zaawansowanej i tak jest po ptakach. Jeśli Paulina wybaczy, on nie ma zamiaru się tu pojawiać; jeszcze nie doszedł w pełni do siebie...

Paulina się lekko zasępiła. Spodziewała się archeologii, przeszłości, czegoś... z "kiedyś". Nie aktywnego klątwożyta. Szczęśliwie, Paulina wpadła na pomysł - zrobić magiczne 'pingi', sygnały, które wywołają odpowiednie efekty w echu magicznym. Zwłaszcza, że Paulina ma sygnaturę magiczną lustra powiązanego z klątwożytem. To lustro wyhodowało klątwożyta, więc te sygnały są wzmocnione przez viciniusa.

Dom Archibalda jest JEDYNYM miejscem, gdzie klątwożyt mógłby przetrwać. Jedynym. Ale Archibald nie opuszcza domu i raczej rzadko ma gości. Więc... mała szansa. Nie wyglądał na ZBYT memetycznie skrzywdzonego. Warto dowiedzieć się jaki był kiedyś i jaki jest teraz. Ale jak...

Paulina zdecydowała się złożyć katalityczny radar... "dysfunktor" klątwożyta. Chodzi o to, by klątwożyt musiał wykonać reakcję w polu magicznym, którą reakcję Paulina będzie w stanie wyłapać. Więc Paulina zdecydowała się na poszukanie mniej więcej gdzie jest klątwożyt...

Eneus powiedział Paulinie, że drugi konstruminus - Ferrus - patroluje inny obszar okolicy. Paulina zmartwiła się, czy Ferrus nie jest aby wektorem; Eneus powiedział, że Ferrus zachowuje się normalnie. Ale Paulina uważa, że może być wektorem a nie chorym...

Paulina przygotowała wiązkę detekcyjną na klątwożyta. (7v5->S). Ku swojej zaskoczonej reakcji wyczuła... klątwożyta poniżej 2 metrów od siebie. Klątwożyt jest w Eneusie. Paulina bardzo, BARDZO próbuje udawać, że nic nie zauważyła... 

* Czarodziejko? Czy znalazłaś to, czego szukałaś? - Eneus, lekko kpiąco i bardzo ludzko
* Tak, ale nie rozumiem... - Paulina, lekko zakłopotana
* Nie ty jedna... - Eneus, WZDYCHAJĄC
* Dlaczego nie przeskoczyłeś na mnie? - Paulina
* Bo nie jestem klątwożytem. To... trudna sprawa - zakłopotany Eneus
* WIDZĘ. - Paulina

Eneus zmienił formę. Wygląda jak kobieta; taka pod 40. 

* Nazywam się Daria Składak - opętany konstruminus zaczął opowieść
* Zamordował mnie mój mąż, Archibald. Z MIŁOŚCI. - Daria, z przekąsem
* Następne co pamiętam... zrekonstruowałam się w lustrze. Jako klątwożyt. - Daria - JESTEM formą memetyczną. Ja nie żyję.
* To ciało, ten konstruminus, mój przyjaciel... odrodziłam się przez niego - Daria
* To lustro było Twoje od początku? - Paulina
* Tak... - Daria, potwierdzając przypuszczenia Pauliny
* Chcę sprawiedliwości. Nie miał prawa mnie zabijać. A Ty... Ty pomagałaś ludziom - Daria do Pauliny

Fatalnie. Jeśli to jest klątwożyt... to wtedy "Daria" ma imperatyw zarażania. To już nie jest osoba. To tylko memetyczne echo osoby, oparte o zemstę. I co jak się skończy zemsta na Archibaldzie? To przyszło z lustra... nie do końca można cokolwiek przewidzieć...

Plus, nie wiadomo, czy mówi prawdę.

Paulina chce odbudować Eneusa. Niezależnie od stanu Darii. Ale nie podoba się Paulinie to co się stało z Darią... ogólnie, bieda. I Paulina nie do końca wie, co z tym zrobić. Jednak Paulina chciałaby... odejść bezpiecznie. Wolna. Nie pobita ;-).

Paulina powiedziała, że musi wiedzieć jak to się stało. Jak do tego doszło. Daria powtórzyła Paulinie formułkę słowo w słowo. CO DO SŁOWA. Brakowało tu jakiejś... świadomości. Inteligencji... ale na pytanie, czy Daria umie zarazić Paulinę, Daria odparła, że nie wie. Nie sprawdzała. Ona chce tylko sprawiedliwości.

Paulina stwierdza, że nie ma "tam" maga ani człowieka. Jest tylko echo. Trzeba uratować Eneusa. Sprawdziła zakres zakres charakteru, który skopiował się do klątwożyta: (4v2->S). Paulina sprawdziła pytaniami, z czym ma do czynienia - jest to faktycznie echo. Echo nie "dba" o nic. Działa jak klątwożyt. Ma imperatyw PRZECIW infekcji, skupiony jest na Archibaldzie, chce zemsty i nie chce krzywdy Eneusa. Ale zemsta jest najważniejsza. Jak skończy z Archibaldem, skupi się albo na innych Strażnikach albo na innych magach lokalnych. Bo nie pomogli.

Paulina wpadła na pewien pomysł. Klątwożyt ma imperatyw zemsty, ale niekoniecznie zemsty NATYCHMIAST. Co więcej, Eneus nie zapewnia jej dość energii by się wzmacniać. Klątwożyt ledwo funkcjonuje. Paulina zaproponowała klątwożytowi, żeby ów poczekał a ona wróci z czymś pomocnym. Powiedziała "Darii", że z nią sympatyzuje i chce jej pomóc. "Daria" użyła możliwości Eneusa, by zbadać intencje Pauliny:

(3v4-> F(wizyta u Archibalda), S): Klątwożyt się zgodził. Ale wpierw Paulina MUSI iść do Archibalda. Musi zrozumieć, jaki on jest zły, paskudny i okrutny. Paulina się skwapliwie zgodziła i "Eneus" pozwolił jej odejść. A jest wieczór... Paulina poszła do jego kamienicy i zostawiła liścik z prośbą o spotkanie. Po czym... wróciła do siebie, do Krukowa. I ma o czym myśleć...

Dzień 2:

Z Pauliną skontaktował się Herbert. Znowu. W sprawie lustra. Powiedział, że się mylił jak chodzi o sygnaturę lustra. Ono nie wzmacnia energii magicznej. Ono TRANSFERUJE energię magiczną. Z X do Y. Pasożytuje na jednym magu, by wzmocnić innego. I pasożytuje na KONKRETNYM magu by wzmocnić KONKRETNEGO. Można to przeadaptować, ale... to jest lustro. 

I klątwożyt - konstrucja klątwożyta - może być wpisana w to lustro. Trzeba sprawdzić dokładniej; Herbert potrzebuje tego lustra by się dowiedzieć, o co dokładnie chodzi. Gdy usłyszał, że to niewykonalne, westchnął. Poprosił Paulinę, by ta była ostrożna, bo to jest... coś dziwnego.

Paulina ma kolejny problem do rozwiązania...

Paulina spytała Herberta, gdzie w okolicy może dostać coś na klątwożyty. Zioło powiedział, żeby pójść do najbliższego dużego miasta i do Kompleksu Świecy. Tam dostanie, tylko musi odpowiedzieć na kilka pytań (np. gdzie to jest - kontrola epidemiologiczna) i się zidentyfikować. Oczywiście Paulina nie ma na to ochoty...

Paulina wykorzystała swoje kontakty w półświatku. Skomunikowała się z Wiaczesławem Zajcewem; innym Zajcewem z półświatka (-1 surowiec). Poprosiła go o przeszmuglowanie dla niej czegoś na klątwożyty; i to jak najszybciej. Wiaczesław powiedział Paulinie, że jest taki mag - Gabriel Dukat. Jest to potężny szef półświatka. I on nienawidzi klątwożytów. Jeśli Paulina powie mu, że rozwaliła jednego... Gabriel ją wynagrodzi. I wtedy Wiaczesław by prosił, by Paulina powiedziała Gabrielowi, że on jej pomógł.

Paulina resztę dnia poświęca na przygotowanie sobie apteczki i stymulantów bojowych dla siebie. Nie jest pewna jak walczyć z klątwożytem i jak skutecznie zadziała broń dostarczona jej przez Wiaczesława, więc chce być gotowa, gdyby nie działało tak dobrze jak powinno...

Dzień 3:

Rano Paulinę obudził dźwięk rozbijanej szyby. Na dole stoi Zajcew z bardzo głupią miną. Paulina patrzy z dużym wyrzutem...

* Tu jest dzwonek, tu framuga. Można było dzwonić lub pukać - Paulina, zirytowana
* No... nie wiedziałem że to pęknie. Nie masz wzmocnień? - zaskoczony Wiaczesław
* Nie, Sławku kochany, nie mam wzmocnień... - Paulina, zrezygnowana - To jest gorące szkło, nieprzystosowane do gorących chłopaków
* No ale każdy może Ci się włamać bez jakichkolwiek... - Wiaczesław, zdziwiony
* No i po co? - Paulina
* No... sport - Wiaczesław

Paulina wyjaśniła, że to żadne wyzwanie. Wiaczesław powiedział, że jak wie, to nie jest już ciekawe. Potężny motocyklista na wiernym motorze klasy Śledź poczekał potulnie aż Paulina się ubierze i zejdzie do niego...

* Masz - Wiaczesław podający wielkie pudło Paulinie
* Mam to podnieść? - zaskoczona Paulina
* To nie jest ciężkie, tylko duże - Wiaczesław

Ten obiekt wygląda jak gaśnica, ze stylizowanym rekinem i czaszką namalowanymi na nich. Trzeba wycelować w nośnik klątwożyta i spryskać. I zadziała.

...Paulina się zasępiła. To nie będzie proste... ten klątwożyt jest w konstruminusie i z zaskoczenia Paulina nie da rady go zaatakować. Więc zostaje pułapka... jakoś. W czasie gdy Paulina kombinuje, Wiaczesław montuje jej nowe okno... a dokładnie, zabija je deskami. Będzie na dzieci z sąsiedztwa...

Dobra, Paulina spakowała apteczkę, wzięła "gaśnicę" do bagażnika i ruszyła polować na konstruminusa... jakkolwiek to brzmi. Zbliżając się do Kropwi zaaplikowała sobie stymulant. Poczuła, że zadziałał. 

Paulina poszukała jakiegoś domku; takiego z piwnicą, porzuconego. Znalazła go - na osiedlu Krogulca. Dojechała na miejsce i wezwała Eneusa na pomoc. Poprosiła go, by pomógł jej znieść ciężką skrzynię do piwnicy; sama ma gaśnicę...

* Mógłbyś mi pomóc to przenieść tam do piwnicy? - Paulina
* ... - Eneus
* Stwierdziłam, że telepanie się w tą i w drugą nie ma sensu - Paulina, prosząco
* Ja, terminus... mam nosić Tobie ciężkie przedmioty? Po to mnie wezwałaś? - zaskoczony Eneus
* Myślałam, że jesteś zainteresowany zawartością, ale spoko... - Paulina, zastawiając pułapkę.

(3v3->S): Eneus to łyknął. Wziął skrzynię i zaczął nieść ją do piwnicy. Paulina z gaśnicą idzie za nim. Jak już zeszli dość nisko, Paulina odpaliła "gaśnicę" i strzeliła w niego środkiem przeciw klątwożytom...

(4v4->S): Paulina poraziła Eneusa wiązką od pleców. Terminus zleciał ze schodów ze skrzynią. Chwilę podrgał, po czym się wyłączył. Po chwili uruchomił się ponownie, przeszedł w tryb autodiagnostyczny i autonaprawy. A w tym czasie Paulina go związała...

* Naprawdę? - Związany Eneus do Pauliny
* Które? - Paulina, ostrożnie
* Więzy? Tak kiepskie? - Eneus
* Może i kiepskie... - Paulina, nie wiedząc co powiedzieć - Ale dają mi jakąkolwiek szansę
* Autodiagnostyka... pokazuje, że klątwożyt został usunięty - Eneus - Ta kałuża na ziemi to potwierdza

Paulina skwapliwie zebrała kawałek kałuży do worka. Przyda jej się dowód zniszczenia...

* Stabilizacja... wymaga autonaprawy. Wymaga to 24h - Eneus
* Czekaj... mogę ci pomóc czy przyspieszyć? Jestem lekarzem - Paulina
* Nie jestem dość organiczny. Mentalnie... to źle działa. Złe ścieżki. - Eneus, zbierając myśli
* Wyślij sygnał do Ferrusa, by mnie nie szukał - Eneus, z trudem - Inaczej mnie zniszczy

Paulina usunęła więzy. Eneus przeszedł w tryb autonaprawy. Paulina wysłała sygnał do Ferrusa, że zabiera Eneusa z tego terenu, co wyjaśnia jego zniknięcie z sygnałów. Ferrus oczywiście nie oponuje...

Paulina pojechała się spotkać z Gabrielem Dukatem. Do Orłostwa Wielkiego. Paulina nie wzięła ze sobą dużej próbki klątwożyta; chce trochę, by móc udowodnić. I wzięła lekarski neseserek...

W centrum Orłostwa są Apartamenty Dukata. Paulina zbliżyła się do nich. Od razu zastąpił jej drogę mag, mówiąc, że to teren chroniony i ona nie może tam wejść. Paulina powiedziała, że pan Dukat może być zainteresowany. Gdy mag spytał, czy Paulina jest skłonna zaryzykować, powiedziała "tak". Mag zaprowadził ją do windy.

Ochrona Dukata to dwie piękne kobiety. Paulina nie ma znaczenia. Sam Dukat ma 48 lat i jest ubrany z KLASĄ. Płonie aurą mocy. Czuć od niego moc.

* Czego zatem poszukuję, pani Tarczyńska? - Dukat, kpiąco
* Podobno informacji o zniszczonych klątwożytach - Paulina, bez większego stresu
* Dobrze - Dukat, czekając

Paulina powoli i ostrożnie wyjęła dowód z neseserku i pokazała. Jedna z dziewczyn podeszła i skinęła głową wobec Dukata. Paulina powiedziała jeszcze, że Wiaczesław Zajcew powiedział jej, by tu przyjść i przekazać takową informację. I dostarczył środki umożliwiające zniszczenie klątwożyta.

* Nagroda będzie Ci wypłacona jak zjedziesz windą. - Dukat, z dobrotliwym uśmiechem
* Dziękuję - Paulina, ciekawa, acz nie aż tak, by z nim pogrywać...

Faktycznie, otrzymała nagrodę. Pakiet Quark i pieniędzy. A Wiaczesław dostał za współpracę małego plusa...

# Progresja

* Paulina Tarczyńska: dostaje nagrodę Gabriela Dukata za zniszczenie klątwożyta
* Wiaczesław Zajcew: dostaje lekkie uznanie od Gabriela Dukata za doprowadzenie do zniszczenia klątwożyta
* Eneus Mucro: wolny od klątwożyta; dorobił się własnej unikalnej osobowości

# Streszczenie

Podczas badania lustra Paulina odkryła, że lustro zawierało klątwożyta. Klątwożyt okazał się zagnieżdżony w Eneusie, którego Paulina zdążyła polubić. Klątwożyt okazał się być echem żony Archibalda, przezeń zamordowanej. Paulina kontaktując się z półświatkiem zniszczyła klątwożyta (uwalniając Eneusa) i oddała dowód na zniszczenie klątwożyta bossowi półświatka magów, Gabrielowi Dukatowi, za co dostała nagrodę.

# Zasługi

* mag: Paulina Tarczyńska, z zaskoczenia pokonuje konstruminusa i klątwożyta; korzystając ze znajomości zdobywa broń przeciw klątwożytom i weszła w częściowe łaski Gabriela Dukata.
* mag: Herbert Zioło, terminus znany Paulinie i ekspert od luster; pomógł jej w zdalnej identyfikacji z czym Paulina ma do czynienia. Przyłożył się.
* vic: Eneus Mucro, terminus opętany przez klątwożyta co stworzyło mu osobowość; Paulina zaskoczyła go i zniszczyła klątwożyt. Autonaprawia się.
* mag: Wiaczesław Zajcew, potężny motocyklista i szmugler; wybił Paulinie szybę... przez przypadek. Ale przywiózł coś na klątwożyty.
* mag: Gabriel Dukat, boss półświatka magów na Mazowszu; z jakiegoś dziwnego powodu ma krucjatę przeciwko klątwożytom.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, nad którym znajduje się mieszkanie Pauliny. W którym pochopny Zajcew wybił szybę :-(.
                1. Kropiew Dzika
                    1. Wschód
                        1. siedziba Słojozen, gdzie Paulina eksperymentowała z lustrem i odkryła klątwożyta w Eneusie
                    1. Zachód
                        1. osiedle Krogulca, gdzie jest porzucony domek w którego piwnicy Paulina zastawiła pułapkę na Eneusa
                1. Orłostwo Wielkie, większe miasto gdzie znajduje się siedziba Gabriela Dukata, potężnego maga i bossa gangu
                    1. Centrum
                        1. Apartamenty Dukata, gdzie mieszka potężny boss mafii magów - Gabriel Dukat

# Czas

* Opóźnienie: 3 dni
* Dni: 3

# Narzędzia MG

## Cel misji

* Rozwiązanie o co chodzi z tajemniczym lustrem
* Pokazanie Klątwożyta Memetycznego

## Po czym poznam sukces

* ?

## Wynik z perspektywy celu

* ?