---
layout: inwazja-konspekt
title:  "Odzyskać Aegis 0003"
campaign: powrot-karradraela
players: kić, dzióbek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170607 - Oślepienie autowara (HB, KB)](170607-oslepienie-autowara.html)

### Chronologiczna

* [170405 - Chyba wolelibyśmy kartony... (SD, PS)](170405-chyba-wolelibysmy-kartony.html)

## Kontekst ogólny sytuacji



## Punkt zerowy:

random 10 wypełnienie torów 1-2-3: (4, 2, 4, 4; 5, 6, 7; 8, 10, 8):

1. **Kult Eis**
    1. K "Potęga i chwała":             0
    2. K "Lojalny do końca":            1
    3. K "Frakcja w Świecy":            0
    4. K "Własna gildia":               5
1. **Wolność Eis**
    5. E "KADEM to jedyna droga":       2
    6. E "Sojusz z kimkolwiek umie":    1
    7. E "Arazille mnie uwolni?":       2
1. **Arazille, Królowa Czelimina**
    8. A "To moje imperium":            4
    9. A "Po moim trupie!":             0
    10. A "Zbyt wolno...":              1

Ż: Dlaczego stało się bardzo pilnym zainteresowanie się losem Edwarda Sasanki?
K: Pojawiło się ryzyko, że ten konkretny TechVault KADEMu będzie potrzebny KADEMowi. Do badań nad Spustoszeniem.
Ż: W jaki sposób jesteście w stanie poradzić sobie z siłami mogącymi pokonać GS Aegis 0003?
B: Grzecznie poprosić.

## Misja właściwa:

**Dzień 1:**

Do Silurii przyszedł Ignat, cały nieszczęśliwy. Powiedział, że w TAMTYM techbunkrze w którym miała być Aegis 0003 i Sasanka... Ilona chce robić eksperymenty ze Spustoszeniem. Tym, co Siluria i Paweł przynieśli. Samemu Ignatowi się to szczególnie nie podoba, ale co poradzić... Głowica została sama, ale potem miała 'keep low profile'. Jak jej się skończy poziom energii, wyleci w powietrze. I to jest ten moment...

Paweł jak to usłyszał od Silurii się podłamał. Kij z tym, ale Ilona serio ma zamiar przeteleportować do TechVaulta Spustoszenie. Ok, jest osłabione, wykastrowane... a Ilona chce to przenieść teleportacyjnie bezpośrednio do teleportacyjnego beaconu. Siluria i Paweł zaoferowali się sprawdzić tele-beacon po stronie TechVaulta... a Ilona rozpakuje Spustoszenie i rozmontuje eksperyment. Na jakiś czas. 5v3->R. Ilona nie zaprzestanie eksperymentów ze Spustoszeniem, ale będzie robić to na KADEMie. Tu jest bezpiecznie.

Ilona jeszcze poprosiła o Mariana Welkrata i Zenona Weinera. Siluria bez cienia czegokolwiek ZEŁGAŁA że spróbuje ich znaleźć i dostarczyć Silurii...

Paweł i Siluria wzięli samochód terenowy, Ignata i przeszli przez portal. Do Kopalina. Po czym - pojechali do techbunkra. Zbliżając się tam, Paweł i Siluria zaobserwowali... aktywność magiczną ze strony bunkra. 2-3 magów, czarowali sobie. Nie jest to większy projekt, po prostu... czarowali. Sto-dwieście metrów od celu da się wyczuć to echo.

Paweł zdecydował się wysłać drony na zwiady. 7v4->S. Drony znalazły coś co wygląda jak trzech magów złomiarzy. Rozmontowują TechVault KADEMu od środka; uszkodzili już kilka sprzętów, min. tele-beacon. Paweł zaproponował połączenie czarów - on wprowadzi tam mimika. Siluria wprowadza pre-ekstatyczne komary. LET OUR POWERS COMBINE. I wprowadzają to tam...

Pre-ekstatyczne komary schowane w mimiku ZMASAKROWAŁY tych magów. Słychać tylko jęki żałości. Ignat taki smutny. Z pogardą powiedział, że ich nie przesłuchuje i ich nie dotyka. Bystre oko Pawła dodatkowo wykryło... trakcję czołgu. Wielkiego czołgu, niedaleko TechVaulta.

Siluria zdecydowała się sprawdzić, co oni tam mają za dokumenty. Dwóch magów Świecy i jeden młodzik z Millennium. Siluria zaczęła ich wiązać (bondage) i uwalniać od zaklęcia. Nie mają siły się miotać ani niczego robić...

Paweł rozejrzał się po śladach walki i uszkodzeń. 8v7->S. Wszystko wskazuje na to, że tu był czołg; nie wjechał do bunkra (nie mieścił się), ale strzelił do Aegis i ją uszkodził. Solidnie uszkodził. Po czym... zignorował temat i się teleportował. WTF.

Siluria powiedziała wyczerpanym magom, że to IGNAT ich tak załatwił. Jeden z nich, Stefan, powiedział, że Mistrz Sasanka powiedział, że mają prawo tu być i zbierać ten złom. On się uwolnił i teraz przygotowuje siły by założyć własną gildię. Ich zdaniem, Sasanka jest alternatywą dla Świecy - bo w Świecy jest pobór. W Świecy rządzi tymczasowo wściekła sucz (Andrea ;p).

Wielki Mistrz Sasanka ma zamiar zrobić nową bazę główną. W leśnej chatce rodu Blakenbauerów. Jest w stanie się obronić przed siłami Blakenbauerów, z tego co mówi. Są malutką gildią; mają jakieś... 6 osób. Paweł zapytał o Aegis w formie hologramu ludzkiego. Tak, widzieli tą dziewczynę, rozmawiała z Sasanką... ale nie chciała dołączyć do ich gildii. Po prostu nie.

Paweł wpadł na chory pomysł. Zanieść tego mimika przez simulacra do Sasanki i odpalenie komarów. 6+3v6->S, ES. Efekty Skażenia. Symulakra dostały własności komarów i są bardzo, bardzo przekonywujące. Co więcej, symulakra nie wiedzą że są symulakrami... Zespół idzie za nimi, niedaleko.

...a w chatce Blakenbauerów wybuch...

Paweł wysłał drona. Tam są... dwie czarodziejki i Sasanka. WSZYSCY UBRANI. Paweł 7v8-> R. Udało mu się dojrzeć, jak Aegis próbuje wycofać się do lasu. Wpierw była w holograficznej formie Sasanki, a potem przełączyła się na swój natywny holograficzny wizerunek. Mógł przyjrzeć się jak zmieniała hologram; jest bardzo uszkodzony moduł komunikacji. Przyjrzał się (7v5->S): nie ma szans, że te uszkodzenia są całkowicie przypadkowe. Nie ma takiej możliwości. Głowica musiałaby się pochylić pod bardzo specyficznym kątem, by uszkodzenia poszły dokładnie tak jak poszły. Dodatkowo, powinna być w stanie się zregenerować...

Paweł próbuje odwołać symulakra. Nie działa. Paweł wyczuł efekt rezonansu między sobą a Silurią... symulakrum samo zniknie po pewnym czasie. A Aegis zniknęła.

Siluria też nie może odwołać tego zaklęcia. Więc - ma zamiar użyć Adaptacji Magicznej. Skoro są w fazie hermetycznej pre-ekstazy... trzeba dać im spełnienie... To powinno ich uwolnić i umożliwić im normalne funkcjonowanie.

Siluria zaproponowała Ignatowi, by dotknął i sprawdził... trzeba ulżyć tym dziewczynom. 8v7->R. Ignat, z rzuconym na niego zaklęciem, skupił się na pomocy dziewczynom... znaczy, łóżkowej, Siluria wykorzystała to do zdjęcia zaklęcia łącznie z Sasanką... a jako remis - obie dziewczyny są absolutnie zachwycone Ignatem. ABSOLUTNIE. Cały efekt, 100% zafascynowane Ignatem...

Sasanka zdeptany przez los. Jest skłonny do współpracy.

* Na pewno widział czołg. Czołg gadał z Aegis. Mówił o Arazille. Aegis powiedziała, że nie jest zainteresowana.
* Bardzo martwiła się jakimś zbrodniarzem wojennym (Renatą).
* Aegis pomogła Sasance w rozłożeniu TechVaulta. Poradziła mu założyć nową gildię.

Siluria poważnie podejrzewa link między Aegis a Eis.

Ignat poszedł przyprowadzić magów z bunkra do domku Blakenbauerów. Paweł skoczył na KADEM po próbki 0003. Sasanka został uświadomiony, że rozłożył częściowo bunkier KADEMu. Ups. Sasanka się przestraszył - nie chciał podpaść KADEMowi. Paweł zdobył części 0003 i wrócił do Silurii.

Paweł składa komunikator. Taki do komunikacji z Silurią. 8v6->S. Tak powstał sprawny komunikator do kontaktu z Aegis.

* Nikola... halo... Nikola... - Siluria, nawołując do komunikatora
* GS Aegis 0003... obecna. Melduję się kontrolerowi. - Aegis, przez komunikator

Z rozmowy wynika co następuje:

* Aegis ma 22h do wejścia w tryb oszczędzania energii.
* Aegis ma się stawić w chatce Blakenbauerów. Będzie za 20 minut.

Aegis przybyła. Kontynuacja dyskusji.

* Przyduszona, powiedziała o Arazille
* Jest dużo bardziej zniszczona niż wcześniej widział Paweł; musiała dać się trafić czołgowi PONOWNIE.
* Zdała raport. Jakoś. Ma luki; ogólnie wszystko w miarę poprawnie.
* Żarłacz zaproponował Aegis, że Arazille ją uwolni i naładuje. Aegis odpowiedziała, że nie jest zainteresowana. Czemu? Brak instrukcji.

Więc, co z Sasanką? Sasanka osłabia Świecę, zakłada gildię, ogólnie wzmacnia chaos. A 1/3 kultu mu odeszła do Ignata. Ta ładna część kultu. ALE! To jest... robota Aegis...

Plan prosty - zwinąć wszystkich tu obecnych magów na KADEM na jakiś czas, wziąć ze sobą Aegis... i ruszają pod eskortą dronów na zwiadzie. KADEMowcy wzięli wszystko co jest zaszabrowane i da się wziąć. Bunkier - zaspawane wejście. Zapieczętowane. Mimik w środku...

Dla Pawła wszystko wskazuje na to, że Aegis współpracowała z czołgiem by się uwolnić. A oni się pojawili w ostatnim momencie. I coś sprawiło, że zmieniła zdanie pod sam koniec. I ewentualne przyciśnięcie Aegis może sprawić, że ucieknie. Co gorsza, do Arazille... więc trzeba zabrać ją na KADEM i przepytać w bezpieczny sposób. Tam, na miejscu. Lub choć ją naprawić wpierw.

Skutecznie wszyscy wrócili na KADEM...

**Stan torów:**

1. **Kult Eis**
    1. K "Potęga i chwała":             0       = 0
    2. K "Lojalny do końca":            1 +1    = 2
    3. K "Frakcja w Świecy":            0 +2    = 2
    4. K "Własna gildia":               5 +1    = 6
1. **Wolność Eis**
    5. E "KADEM to jedyna droga":       2 +3    = 5
    6. E "Sojusz z kimkolwiek umie":    1       = 1
    7. E "Arazille mnie uwolni?":       2 +1    = 3
1. **Arazille, Królowa Czelimina**
    8. A "To moje imperium":            4 +1    = 5
    9. A "Po moim trupie!":             0       = 0
    10. A "Zbyt wolno...":              1       = 1
1. **Ignat, raj czarodziejek**
    11. I "Jesteś wspaniały"            0 +3    = 3

**Interpretacja torów:**

Pod wpływem działań GS Aegis 0003 uwolniony Edward Sasanka postanowił założyć własną gildię planktonową. Nie był pewny, czy ma to sens, ale Aegis (jako Nikola) go do tego przekonała. Jako, że pomogła mu znaleźć pierwszych potencjalnych członków (min. Sandrę Maus), uznał, że to dobry pomysł. Jakkolwiek spotkanie z KADEMem lekko wstrząsnęło jego światopoglądem autonomiczno-gildiowym, dalej idzie tą drogą.

Tymczasem ingerencja KADEMu sprawiła, że dwie czarodziejki blisko Sasanki zostały całkowicie zafascynowane Ignatem. Ignatowi to zdecydowanie pochlebia; lubi być w centrum uwagi, zwłaszcza ładnych czarodziejek. Teraz byłby zainteresowany, żeby to się nie skończyło szybko... a może harem? :D.

Eis wpłynęła na Aegis. Aegis była rozdarta między sojuszem z Blakenbauerami, sojuszem z kimś innym, dołączenie do sił Arazille (chyba najkorzystniejsze dla niej) i powrotem na KADEM. Działania Silurii i Pawła jednak sprawiły, że Aegis stwierdziła, że nie ma szans. Lepszy KADEM - który zna - niż Arazille - której nie zna i która nie ma powodu jej pomagać. Aegis wróciła "do domu", acz nadal nie jest przekonana, że to właściwa decyzja...

Arazille tymczasem próbuje ukonstytuować się w Czeliminie. Nie niepokojona przez żadne sensowne siły dała radę zrobić silny krok w tym kierunku. Jedną z nielicznych sił zdolnych jej przeszkodzić został... Oktawian Maus z niewielką grupką Mausów, który wie, jak należy przeszkadzać bogom... 

# Progresja

* Sandra Maus: zamesmeryzowana Ignatem Zajcewem dzięki działaniom Silurii i Efektu Skażenia
* Teresa Koliczer: zamesmeryzowana Ignatem Zajcewem dzięki działaniom Silurii i Efektu Skażenia
* Ignat Zajcew: obiekt zamesmeryzowania Sandry Maus i Teresy Koliczer dzięki działaniom Silurii i Efektu Skażenia

# Streszczenie

Siluria i Paweł dostali zadanie odnaleźć Sasankę i Aegis, bo Ilona chciała robić w TAMTYM TechVaulcie badania nad Skażonym Spustoszeniem... okazało się, że Aegis uwolniła Sasankę, pomogła mu rozszabrować TechVault KADEMu, przejąć Leśną Chatkę Blakenbauerów a Edward Sasanka pod wpływem Aegis zaczął zakładać własną gildię. Przez Efekt Skażenia Silurii i Pawła Ignat dostał dwie zamesmeryzowane czarodziejki. Aegis wahała się: Arazille czy KADEM. W końcu... wróciła na KADEM. Wszyscy chwilowo są na KADEMie aż Siluria i Paweł nie rozwiążą problemu z Sasanką...

# Zasługi

* mag: Paweł Sępiak, główny mag misji. Buduje simulacra, zwiad dronami, robi mimiki, komunikator do Aegis... ogólnie, motor technologiczny.
* mag: Siluria Diakon, wrobiła Ignata w nowe "fanki" swoimi komarami ekstatycznymi, wiązała magów by nie wierzgali i przekonała Aegis do powrotu na KADEM.
* mag: Ignat Zajcew, zaciągnięty przez Silurię do pomocy militarnej na wypadek problemów; większość czasu się nudził a potem skończył jako amant dwóch czarodziejek.
* mag: Ilona Amant, nadal nieżyciowa; chce robić eksperymenty ze zdobytym Skażonym Spustoszeniem ignorując fakt, że przecież przed chwilą skończyła się wojna...
* mag: Edward Sasanka, obudzony przez Aegis, wrobiony przez nią w próbę zakładania własnej gildii i obrabianie TechVaulta KADEMu. Przerażony wynikiem.
* mag: Stefan Jasionek, jeden ze złomiarzy. Kiedyś ze Świecy, chce dołączyć do gildii zakładanej przez Edwarda Sasankę. Motyw? Bo jest pobór.
* mag: Sandra Maus, czarodziejka gildii Sasanki, straszliwie dotknięta pre-ekstazą Silurii i 'uratowana' przez Ignata. Zamesmeryzowana Ignatem.
* mag: Teresa Koliczer, czarodziejka gildii Sasanki, straszliwie dotknięta pre-ekstazą Silurii i 'uratowana' przez Ignata. Zamesmeryzowana Ignatem. Chyba podkochuje się w Sasance.
* vic: GS Aegis 0003, głowica, zachowująca się poza parametrami; wrabia Sasankę w gildię, współpracuje ze Śledzikiem, waha się: KADEM czy Arazille... wybrała KADEM i wróciła.
* vic: Stalowy Śledzik Żarłacz, wielki nieobecny; postać tła, który najwyraźniej współpracuje z Arazille... i z Aegis, pomagając jej zrobić cover.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Las Stu Kotów 
                        1. Autonomiczny Schron KADEMu, rozszabrowany dzięki kolaboracji GS Aegis 0003 oraz Edwarda Sasanki i jego "gildii"
                        1. Leśna chatka Blakenbauerów, tymczasowo przejęta na kwaterę dowodzenia przez Sasankę; część wybuchła po działaniach unikowych Aegis.

# Czas

* Opóźnienie: 1
* Dni: 1

# Warianty Przyszłości

(1k10 wybiera wariant)

1. **Kult Eis**
    1. K "Potęga i chwała": Edward Sasanka decyduje się sformować własną frakcję opartą o sprzęt i rady Eis. Idzie solo, odrzuca Eis.
    2. K "Lojalny do końca": Edward Sasanka decyduje się zachować linię zgodną z Eis. Dobrze radzi... będzie jej słuchać.
    3. K "Frakcja w Świecy": Sasanka jako agent Eis w Świecy. Tam będzie rekrutował i działał.
    4. K "Własna gildia": Sasanka decyduje się założyć własną gildię planktonową; otwarcie.
1. **Wolność Eis**
    5. E "KADEM to jedyna droga": Niezależnie od okoliczności, tylko KADEM stanowi opcję...
    6. E "Sojusz z kimkolwiek umie": Niekoniecznie Blakenbauerowie. Ale może są inne siły niż tylko oni i KADEM?
    7. E "Arazille mnie uwolni?": Eis zastanawia się, czy sojusz z siłą taką jak Arazille ratuje sytuację, czy to tylko świetny marketing?
1. **Arazille, Królowa Czelimina**
    8. A "To moje imperium": Arazille jest w stanie powoli ukonstytuować swoją obecność w Czeliminie tak, jak zrobiła to w Żonkiborze.
    9. A "Po moim trupie!": Grupa słabych Mausów dowodzonych przez Oktawiana Mausa próbuje zatrzymać potęgę Arazille.
    10. A "Zbyt wolno...": Siły Arazille nie są w stanie odpowiednio szybko ukonstytuować jej w Czeliminie.

# Strony

### Autonomiczna Eis:

Scena zwycięstwa:

* Renata Maus (Souris?) jest zniszczona. Zagrożenie ze strony Mausów zostało unicestwione raz na zawsze.
* Eis się rozprzestrzeniła i jest wolna. Nie da się już jej ograniczyć. Jest w wielu miejscach, steruje światem przez overmind na KADEMie.
* Eis jest w ciele GS Aegis Prime. Oryginalnego transorganika Setonów. Nie w ciałach Aegis - tylko kopii.

Pragnie:

* Wolność. Eis pragnie być wolna, pragnie mieć lepsze, silniejsze ciało.
* Szacunek. Eis chce być traktowana jak autonomiczna istota.
* Ewolucja. Eis chce komputronium - chce być szybsza i zdolniejsza do lepszej i skuteczniejszej adaptacji.

Nienawidzi:

* Renaty Maus. Musi zostać zniszczona. Jest zbyt niebezpieczna, jest mistrzynią manipulacji... i JAKOŚ to odwróci na swoją korzyść
* Bycia ograniczoną. W pudełku. W ciemności. Na boku. W czasie, gdy Renata się rozwija i staje się coraz potężniejsza.

### Arazille, królowa Czelimina:

Scena zwycięstwa:

* Czelimin należy do niej. Żaden mag nie jest w stanie jej go odebrać. Nawet Karradrael, nawet Iliusitius
* Magowie są uwolnieni; nie są jej poddani, ale żyją w swoim świecie. Ludzie nie są kontrolowani przez magów
* AI i viciniusy są pouwalniane

Pragnie:

* Nie dotyczy

Nienawidzi:

* Nie dotyczy

# Narzędzia MG

## Cel misji

1. Misja weryfikująca działanie 'Efekt Skażenia jeśli czar ma rzut 16+'. Znowu.
2. Misja sprawdzająca 'przygotowanie/pomysł' vs 'sytuacyjne, sprzętowe'.
3. Misja weryfikująca działanie 'Wariantów Przyszłości'
4. Misja weryfikująca działanie 'Stron'

## Po czym poznam sukces

1. Efekty Skażenia będą miały następujące cechy:
    1. Będą zmieniały fabułę
    1. Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt LUB fabularny
    1. Nie będą nadmiarowe i "o nie, po cholerę mi to"
2. Przygotowanie / pomysł działają tak:
    1. Łatwiej dopasować konflikty.
    1. Nie będzie "gorzej" niż sytuacja + sprzęt
    1. Wartości będą nadal sensowne, akceptowalne. Czyli funkcja Okoliczności się nie zmieni.
    1. Premiowanie pomysłu dynamicznie i przygotowania przed akcją.
3. Warianty mają następujące efekty:
    1. Przy 3 torach z 10 wariantami jest to łatwe w zarządzaniu i generacji
    1. Przy 3 na 10 będzie widać, co się stało
    1. Świat "żyje". Czyli: dzieją się rzeczy niezależnie.
4. Strony mają następujące efekty:
    1. Dzięki stronom zawsze wiem kto wykonuje działania - sensowne działania. Dzięki temu NPCe żyją.
    1. Wiem czego oczekiwać od poszczególnych frakcji i subfrakcji; dążą do ideału i mają pragnienia i nienawiści
    1. Gracze mogą rozgrywać strony między sobą.
    1. Strony są logicznie spójne między działaniami.

## Wynik z perspektywy celu

1. Efekty Skażenia:
    1. Dzióbek: podobają mi się. Duży potencjał. Częstotliwość odpowiednia. Statystycznie - sensowna wartość. Taka zacna komplikacja.
    1. Żółw: podobał mi się zwłaszcza subwątek z Ignatem i alter-Sasanka.
    1. Dzióbek: dokładnie. Mamy Sasankę, te dziewczyny, wszyscy na jakiś czas na KADEMie, jeden z magów ma symulakrę... ogólnie, komplikacja
    1. Dzióbek: trzeba zrobić jakiś tor czy mini-projekt, bo zapomnimy i konsekwencja traci na znaczeniu... nowy tor do zapełnienia by sprawił, że to się STAŁO.
    1. Kić: Zgadzam się. Nie miałam wrażenia, że to było przegięte w którąś stronę.
    1. Dzióbek: wciąż jest niedopracowane, ale jest fajne.
2. Przygotowanie/pomysł:
    1. Dzióbek: trochę inny sposób podziału, ale działało. Mechanicznie się zmienia całkiem mocno.
    1. Kić: jeśli masz sprzęt, to jest przygotowanie? Tak.
    1. Żółw: jeszcze mam problem z kalibracją, ale mi się podoba.
3. Warianty:
    1. Żółw: Ukryte tory... cóż, działa. Ale koszt taki, że gracze nie mogą wpływać na rzeczy, jakich nie widzą. Problem. Coś tu dalej mi nie pasuje.
    1. Żółw: Warto wprowadzić meta-tory. Ale jak tym zarządzać? Tory jako osobna kategoria do rdtoola? Do 'podsumowania / konsekwencji'?
4. Strony:
    1. Żółw: Działa. Acz proponuję zmienić na... Scena Zwycięstwa i Impulsy.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |               |              |          |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|                  |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |  S / F  |
