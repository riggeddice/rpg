---
layout: inwazja-konspekt
title:  "Wojna domowa Spustoszenia "
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150110 - Bezpieczna baza w Kotach (SD, PS)](150110-bezpieczna-baza-w-kotach.html)

### Chronologiczna

* [150110 - Bezpieczna baza w Kotach (SD, PS)](150110-bezpieczna-baza-w-kotach.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Paweł i Siluria debatują nad możliwością komunikacji ze Spustoszeniem. Być może - jako, że to Spustoszenie jest inteligentne, da się w jakiś sposób z nim dogadać. Po rozmowie z Marysią, ta zaproponowała im kontakt z Robertem Miękiem - "były" agent Tarczy który jednocześnie najpewniej jest Spustoszony. Gdy przyszedł do kościoła na prośbę Marysi, Siluria porozmawiała z nim chwilę koło VISORa a Paweł włączył niekierunkowy x-ray. Wyniki niedokładnego skanu wykazały, że zaiste Robert Mięk jest Spustoszony (Spustoszenie w wersji utajonej).

Niestety, rozmowa Silurii z Robertem nie dała pozytywnych efektów. Robert nie reaguje na zaowalowane słowo "Spustoszenie". W związku z tym Paweł wysłał mu SMSa ze słowami "chcemy porozmawiać ze Spustoszeniem" z zaznaczeniem: chcemy porozmawiać ORAZ "Spustoszenie czy jak się nazywasz". Zdaniem Pawła, technologia Spustoszenia jest identyczna ale są dwa (lub więcej) kontrolery Spustoszenia; zupełnie inne modus operandi.

Robert Mięk dostał SMSa od Pawła. Przeczytał i nie zrozumiał o co chodzi; jednak chwilę potem Paweł dostał w SMSie dziwną wiadomość zawierający grupę liter i znaków. Znaczy: najpewniej Spustoszenie nie jest w stanie zrozumieć o co dokładnie chodzi lub ma problem komunikacyjny. No ok, mamy VISOR... ale VISOR nie nadaje się do kryptografii. W związku z tym Paweł poprosił Julię, Siluria dostarczyła trochę zabawek i świecidełek i Julia # Paweł zaczęli pracę nad VISORem, by dodać moduł do deszyfrowania. Budowa zajmie mniej więcej godzinę.

Marysia poszła porozmawiać z Robertem. Siluria ma ich na oku.

Podczas pracy nad modułem VISORa weszła Judyta i poszła prosto do Julii. Powiedziała, że musi się dowiedzieć czegoś odnośnie Mariana Welkrata i Zenona Weinera. Paweł jej przerwał; są zajęci. Czy to nie może poczekać. Judyta nie weszła w konflikt, wycofała się. Powiedziała tylko, że za 15 minut to nie będzie miało już żadnego znaczenia. Julia zdecydowała się pójść z Judytą na bok. Nieszczęśliwy Paweł nie ma co robić.

Chwilę potem do kościoła wszedł starszy pan i zaczął się modlić. Marysia zidentyfikowała go jako Szczepana Czarnieka, starszego człowieka który regularnie przychodzi do kościoła pomodlić się za swoją żonę. Gdy zaczął się modlić, świece w kościele przygasły i Marysia też wpadła w dziwny... trans. Siluria zwróciła się do Marysi czy wszystko w porządku i trans został przerwany (toteż efekt kościoła). 

Judyta przerwała rozmowę z Julią i spytała Warmastera (myśli, że on dowodzi) co się tu stało. Warmaster z radością powiedział, że dowodzi Siluria (bo tak). Judyta poszła porozmawiać z Silurią i powiedziała, że SŚ ma następujące dane:
- astralny kraloth dowodzi Spustoszeniem przez miślęga.
- miślęg jest kontrolerem Spustoszenia
- SŚ wzmocni mgłę Warmastera jak z nim rozmawiała wcześniej (WM nic nie powiedział _-_) by wymusić ujawnienie Spustoszonych
- obszar jest pod kordonem; dość oblężony przez SŚ.
- SŚ zamierza użyć głowicy negacyjnej by zniszczyć kralotha i pozbawić magii cały Kopiec Miślęgów; zaraz potem wejdą magowie w anty-Spustoszeniowych tarczach. 
Judyta dała Silurii tarcze przeciwko Spustoszeniu; jednorazówki. Jedna dla maga (Julia, Siluria, Paweł, Warmaster).

Starszy człowiek zaczął modlić się jeszcze raz, po czym doszło do kolejnego Efektu. Judyta zapytała czy KADEM to kontroluje. Dowiedziała się, że nie, ale KADEM bierze pełną odpowiedzialność za to co dzieje się teraz w kościele. Uspokojona czarodziejka SŚ powiedziała, że Świeca wypełniła powinności współpracy. Ostrzegła też Silurię o tym, że Julia pewnie wie więcej niż mówi.

Po odejściu Judyty, 15 minut potem starszy pan przestał się modlić. Efekt w kościele minął.

VISOR został przekształcony i wiadomość została odczytana i zdekodowana. To losowe słowa z encyklopedii z powtarzającym się wzorem "Tadeusz Aster współpraca". Tadeusz Aster był magiem który kiedyś współpracował w Kotach przeciwko ludziom z kralothem.

Starszy pan zbiera się do odejścia. Marysia poprosiła Silurię o moment. "Lugardhair". "Kraloth". Macki ze ściany. Powiedziała, że widziała ludzi w kopcu, że straszne rzeczy. Kim oni są. O co tu chodzi. Marysia jest rozbita i spanikowana, ma jakieś dziwne wizje. Dziwne pomysły. Szybko podszedł do niej Paweł i zanim zapomni, wszedł twardo ("Przesłuchania # szybki") by wydobyć jak najwięcej o tym co ona jeszcze wie.
- Marysia konsekwentnie zapomina wszystko co przypomina sobie przez kościół.
- Marysia faktycznie BYŁA tam w Spustoszonym Kopcu i znalazła tam nieszczęsnych ludzi. Spustoszył ją Oktawian Maus (też Spustoszony), po czym wyprowadził ją stamtąd i Spustoszenie się rozpadło, wyłączyło. Maus dał jej artefakt (pierścionek) do ciągłego noszenia. Który Marysia ciągle nosi.
- Marysia pamięta (co niemożliwe) rozmowę Tadeusza Astera i Lugardhaira; Aster oddał Lugardhairowi Koty w zamian za stały dostep Quarków.

Pierwsze co Zespół zdecydował się zrobić to zatrzymać Szczepana. Paweł poprosił, by Marysia go zatrzymała; dziewczyna podeszła do niego, ale Szczepan powiedział jej, że "żona na niego czeka". Marysia jest w szoku. Żona Szczepana nie żyje już od pewnego czasu. Zaproponowała starszemu panu, że będzie mu towarzyszyć; ten się zgodził. Marysia podeszła do Zespołu i powiedziała im, że przeprasza, ale musi zobaczyć o co w tym wszystkim chodzi.
Zespół w konfuzji. Co z tym zrobić? No i jeszcze jest Aster...

Julia podeszła do Pawła i powiedziała, że bardzo martwi ją to, że mamy dwóch Spustoszonych ekspertów od Spustoszenia. I do tego jedno z tych Spustoszeń było zmienione przez Aurelię. Julia się martwi też mistrzem Asterem... a Lugardhair to kraloth który z Asterem współpracował.
Julia chce odzyskać Mariana i Zenona.

Marysia i Szczepan zdecydowali się pójść zobaczyć żonę Szczepana. Z nimi poszli Warmaster oraz Siluria. Dla bezpieczeństwa oraz towarzystwa...
Paweł odwrócił kodowanie i chce napisać (wysłać SMSa do "Spustoszenia") wiadomość 'gdzie Marian Welkrat Zenon Weiner'. Siluria zaproponowała, by podać się za reprezentantów mistrza Astera, ale Paweł powiedział, że to potem. 

Podczas drogi do domu Szczepana Marysia nie umiała nawiązać kontaktu ze starszym panem. Siluria też próbowała (7v6->6), lecz ku swemu przerażeniu zorientowała się, że "tam" nic nie ma. W umyśle mężczyzny nie ma już praktycznie niczego. Jest tylko absolutna obsesja, pragnienie spotkania swojej ukochanej żony. On nawet nie widzi Silurii, ma tylko jedną misję, tylko jedną intencję.
I niestety zorientowała się dopiero pod domem.

Siluria zdecydowała się zrobić scenę. Szczepan wyraźnie nie jest sobą. Stanęła pomiędzy Szczepanem i drzwiami i zaczęła pleść słodkie rzeczy by wprowadzić chaos. Skutecznie skonfundowała wszystkich poza Szczepanem (który jej "nie słyszy", lecz nie jest agresywny). Podczas wystąpienia, Siluria zorientowała się, że jest... cicho. Dookoła nie ma nikogo i niczego...

Siluria zmieniła plan. Zaczęła przekonywać Szczepana, że jego żony tu nie ma, że ona jest gdzieś indziej. Szczepan zaczął wierzyć Silurii, gdy usłyszał "kochanie" ze swojego domu, kobiecy głos. Marysia zesztywniała; ktoś się włamał do domu Szczepana. Chciała tam wejść, lecz powstrzymała ją Siluria.
Warmaster powiedział, że ciekawe jest, że słyszeli czyjś głos. I że ktoś słyszał ich. Sprowadził natychmiast gorathaule z mgły i zauważył, że nie są sami...
Warmaster ostrzegł Silurię, że gorathaule widzą niewidzialne osoby w mgle. Bo operują na innych zmysłach. I powiedział głośno, że są dwa gorathaule.

"Siedem osób. Czworo dzieci. Niewidzialni i Spustoszeni. Jeszcze krok, a bym se strzelił i pozabijał." - Warmaster, najgorszy kłamca na świecie.

Siluria i Warmaster zaczęli budować szyk, by się wycofać. Wtedy z domu wyszła kobieta i Marysia zamarła. Kobieta jest głęboko Spustoszona. Powiedziała Silurii, że chce od nich pierścienia Marysi i Szczepana. Zapytana, poinformowała, że Szczepan nie żyje od kilku godzin ("drona wysłana by zdobyć informacje, drona wróciła z informacjami"). Powiedziała, że Marysia przeżyje, jeśli odda pierścień. Próba wycofania przez magów zakończy się śmiercią wszystkich dron Spustoszenia.

Siluria próbowała dowiedzieć się czegoś od Spustoszenia; dowiedziała się, że jest dysfunkcja Rdzenia Spustoszenia i że to musi zostać naprawione. Ta drona (kobieta) ma za zadanie odzyskać wiedzę by skorygować dysfunkcję. Ta kobieta chce zniszczyć kralotha.
Cóż... Siluria pozwoliła dronie przechwycić Szczepana (i go Spustoszyć) oraz oddała pierścień. Gdy zdjęła pierścień Marysi ta powiedziała jak w transie "Nie jesteś sama. Nigdy już nie będziesz sama. Głosy odejdą".

Spustoszenie pozwoliło Silurii, Warmasterowi i Marysi bezpiecznie wycofać się do kościoła.

Tymczasem Paweł dostał odpowiedź od Spustoszenia "mistrz Aster Tadeusz współpraca". Paweł odpisał "współpraca". Dostał "Aster". Julia, zapytana czy kiedykolwiek współpracowała z Asterem aż się zatchnęła. NIGDY nie pracowała z Asterem. Paweł odpisał "nieobecny". Dostał SMS składający się z samych znaków zapytania.
Julia wpadła na rozwiązanie. Kraloth nie jest "prawdziwy". To tylko wspomnienie kralotha. Coś, co było wcześniej. On nie ma nowych myśli, nowych pomysłów, nowych działań. On nie przekracza tego, co już się działo. W umyśle kralotha Paweł musi być Asterem i kraloth ma zbierać dla niego quarki.

Gdy Paweł wysłał kolejnego SMSa "szef" dostał odpowiedź "Lugardhair". 
Paweł wysłał SMSy do Spustoszenia "magowie: Zenon Weiner, Marian Welkrat, Oktawian Maus potrzebni w kościele". Dostał odpowiedź "niemożliwe". Gdy Paweł SMSowo wyjaśnił, że bariera w kościele została zdjęta dostał odpowiedź "w drodze".
Nie do końca Pawłowi o to chodziło... czas przygotować się z Julią na oblężenie Spustoszenia.

Paweł zaczął przygotowywać tesla mines. Wyjął zapasowe baterie z VISORa i przygotował miny (16v10). Ogromną satysfakcję sprawiło Pawłowi gdy dwóch Spustoszonych magów (Zenon i Marian) zbliżyło się do kościoła, wdepnęli na minę i padli na ziemię nieprzytomni. Następnie, Paweł odpalił artefakty wyłączające Spustoszenie...
I na to do kościoła wróciła Siluria, Marysia i Warmaster. Prosto na regenerację Spustoszenia.
Marysia już o nic nie pyta.

"Agenci ubezpieczeniowi... ubezpieczacie świat przed apokalipsą, czy coś...?" - Marysia, jeszcze w dużym szoku.
"Mamy jeszcze jednego." "Kto? A, Maus" - Siluria i Julia odnośnie Spustoszonych.
"Ale to wszystko było nieaktywne. Spustoszenie nie miało prawa się samo uruchomić" - Julia odnośnie Zenona i Mariana.

Proces czyszczenia potrwa do końca misji. Nie można liczyć na tej misji na Zenona czy Mariana. 

Paweł oddalił się trochę od kościoła. Wysłał SMSa Spustoszeniu "zasadzka, ewakuacja". Dostał odpowiedź "tryb defensywny włączony".
Paweł wysłał ponownie SMSa "co z Mausem". Odpowiedź "???". Chyba kraloth nie rozumie, lub nie ma kontaktu.
Na pytanie czy potrzebna jest pomoc, kraloth odpowiedział "krwawa akumulacja rozpoczęta".

Oki. To nie brzmi dobrze. KADEM zdecydował się skomunikować ze Świecą. Odebrała Tamara. 

"Odbiliśmy Weinera i Welkrata. Nie wiemy jaki jest status Mausa." "A, Mausa." - Rozmowa Pawła z Tamarą.

Paweł i Siluria ostrzegli Tamarę, że Spustoszenie akumuluje siły w Kopcu Miślęgów. Tamara powiedziała, że nic nie opuści tego obszaru; przekierowała energię z Trzech Czarnych Wież by odciąć ten obszar oraz weszli już eteryci (istoty Spiritus Vim) by zidentyfikować wszystkie istoty żywe. Głowica negacji jest już gotowa.

Epilog:
- Srebrna Świeca weszła od głowicy negacji i zniszczyła Lugardhaira na miejscu. 
- Oktawian Maus został znaleziony w Spustoszonym Kopcu Miślęgów. Spustoszony. Zupełnie jakby był "jednym ze Spustoszonych magów".
- Nie ma śladów drugiego kontrolera Spustoszenia; teoria Tamary, że Rdzeń Spustoszenia miał po prostu jakąś formę rozdwojenia jaźni.
- Spustoszenie zostało wyczyszczone z Kotów.
- Srebrna Świeca zadziałała dość cicho i dyskretnie; może tam było coś więcej, ale nic nie wiadomo.


# Streszczenie

Kontrolerem Spustoszenia okazało się być... pryzmatyczne echo kralotha i miślęgi. Ale wiele wskazuje na drugiego, nieznanego kontrolera Spustoszenia (który dostał pierścień Marysi i pozwolił im odejść). Ten drugi Kontroler jest taktykiem i ma jakiś dziwny cel. W kościele w Kotach dzieją się dziwne rzeczy powiązane z pamięcią ludzi, ale KADEM ukrył to przed Świecą. Za to Świeca zachowuje się, jakby coś ukrywała przed KADEMem (atak Świecy zniszczył wszystkie potencjalne ślady). Tak czy inaczej, Spustoszenie zostało odparte z Kotów i magowie odnalezieni. Min. znaleźli tam Oktawiana Mausa, którego nikt się nie spodziewał. Eksperci od Spustoszenia zostali odzyskani przez KADEM.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Kościół romański
                    1. Dwie hałdy
                        1. Domki, gdzie mieszka Szczepan Czarniek - i doszło do starcia ze Spustoszeniem

# Zasługi

* mag: Siluria Diakon, która w ostatnim momencie wykryła pułapkę Spustoszenia i w nią nie wpadła.
* mag: Paweł Sępiak jako osoba adaptująca VISORa i oszukująca echo kralotha.
* mag: Judyta Karnisz jako czarodziejka SŚ pomagająca KADEMowi mimo niechęci; misja ważniejsza niż przekonania.  
* mag: Julia Weiner jako osoba chcąca uratować zarówno Zenona jak i Mariana; SŚ podejrzewa, że ona wie więcej o tym Spustoszeniu niż mówi.
* czł: Marysia Kiras jako dziewczyna, która pamięta za dużo. Wyratowana ze Spustoszenia przez Spustoszonego Oktawiana Mausa, miała misję o której nie wiedziała.
* czł: Robert Mięk jako Spustoszony (w formie utajonej) agent Tarczy mający oko na Zespół i Marysię.
* mag: Warmaster jako czarodziej, który wymanewrował Silurię, Marysię i siebie z pułapki Spustoszenia tracąc "tylko" pierścień i człowieka o zniszczonym umyśle.
* czł: Szczepan Czarniek jako starszy człowiek ze zniszczonym umysłem, skupionym tylko na swojej żonie.
* mag: Oktawian Maus jako agent Spustoszenia, który jest traktowany jako "a, Maus też". Dziwnie nieobecny na tej misji.
* mag: Tamara Muszkiet jako głównodowodząca operacją SŚ na tym terenie, która użyje WSZELKICH środków by Spustoszenie się nie rozprzestrzeniło.
* mag: Marian Welkrat jako Spustoszony, w chytry sposób odzyskany przez Pawła zanim rozpoczął się atak SŚ.
* mag: Zenon Weiner jako Spustoszony, w chytry sposób odzyskany przez Pawła zanim rozpoczął się atak SŚ.
* czł: Korwin Morocz jako ojciec Basi; KIA gdy Lugardhair zaczął akumulację energii.
* vic: Lugardhair, echo z przeszłości Kotów jako kontroler kontrolujący miślęga kontrolującego Spustoszenie. Myślał, że współpracuje z Asterem... a to był sprytny Paweł Sępiak. KIA.
* vic: Pieluszka, miślęg i kontroler Spustoszenia. KIA przez Tamarę Muszkiet i głowicę negacji.

# Czas:

* Dni: 3

# Wątki

- Szlachta vs Kurtyna
- Trzeci kontroler Spustoszenia
- Kościół w Kotach