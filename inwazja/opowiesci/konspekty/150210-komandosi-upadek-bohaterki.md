---
layout: inwazja-konspekt
title:  "'Komandosi', czyli upadek bohaterki"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141022 - Po wymianie strzałów... (HB, Kx, AB)](141022-po-wymianie-strzalow.html)

### Chronologiczna

* [141022 - Po wymianie strzałów... (HB, Kx, AB)](141022-po-wymianie-strzalow.html)

## Punkt zerowy:

K: Co ma w sobie dziewczyna, co i ją zaskoczy i bardzo jej pomoże?
Ż: Determinację i bezwzględność, które ją samą przerażają.

## Misja właściwa:

**Flashback. Sporo czasu temu [1-3 miesiące].**

Hektor Blakenbauer stoi przeciwko Onufremu Puzlowi. Adwokat nie ma żadnych szans. W plecaku oskarżonego, Michała Młota (z "Kopalińskich Komandosów") znaleziono machinę piekielną - bombę domowej roboty. Problem w tym, że zgodnie z tym co Hektor wie od swojej grupy specjalnej, Michał Młot, jakkolwiek nie jest niewiniątkiem, nie para się tego typu rzeczami i nie pasuje na osobę przy której znalezionoby bombę. Po prostu nie.
Podczas przesłuchania, Puzel prosi Młota, by ten powiedział coś - kto zrobił bombę, co wie... ale Młot nic nie wie. Hektor mu wierzy. Młot próbował się bronić, że to nie jego plecak, że próbował go zwinąć... ale to JEST jego plecak. Beznadziejna linia obrony, Michał Młot poszedł siedzieć.
Jednocześnie, Hektor Blakenbauer otworzył śledztwo (do policji i grupy specjalnej) odnośnie tej sprawy. Bo terroryści w Kopalinie to nie jest specjalnie miła sprawa a widać, że Młot nie jest mózgiem...

**Flashback. Sporo czasu temu [1-3 miesiące, tydzień później].**

Marcelin pojawił się u Hektora i powiedział, że chce przedstawić mu swoją nową dziewczynę. Powiedział Hektorowi, że ta dla odmiany lubi jego pracę i jego podejście i sama jest dość radykalna. Hektor nigdy jeszcze nie widział Marcelina tak zdominowanego przez żadną dziewczynę. Tak się zainteresował, że zgodził się na spotkanie - droga restauracja, wieczór, Hektor + osoba towarzysząca i Marcelin + Diana. Gdy Marcelin wyszedł Hektor spytał Klemensa jaka jest rola Diany w tym wszystkim - Diana jest silną, zdeterminowaną, bardzo inteligentną i znudzoną dziewczyną. Hektor powiedział Klemensowi, by ten upewnił się, że ten związek odpowiednio szybko się skończy. Klemens przytaknął.

Anna Kajak zgłosiła się do Hektora telefonicznie. Powiedziała, że machina piekielna w plecaku Młota nie była kupiona na czarnym rynku; jest to robota amatora i to nie w pełni efektywna robota amatora. Tak jakby ktoś nie miał wprawy w robieniu bomb ale miał dostęp do sprzętu i laboratorium. Ten typ bomby nigdy jeszcze nie pojawił się na rynku. To jest coś nowego. Ta bomba autentycznie wygląda na groźniejszą niż jest. Co więcej, łatwiej byłoby tą bombę zrobić dużo taniej niż faktycznie została zrobiona.

Zdaniem Hektora ktoś chce wrobić Mikołaja Młota. Ale po co wrabiać nieistotnego Młota?

Hektor wpadł na pomysł jak Marcelin może się przydać. Poprosił go o spojrzenie na bombę. Żeby Marcelin spojrzał i znalazł jak najwięcej danych na temat bomby - czy brali w tym udział magowie, skąd wzięły się materiały... inne takie. (10->12). Marcelin dał radę używając magii się dowiedzieć dość sporej ilości ciekawych faktów. Hektor dawno nie widział Marcelina tak... skonfundowanego. I strutego.
Hektor wszedł twardo. 
Marcelin powiedział, nieszczęśliwy, że wszystko wskazuje na to (metodami magicznymi) że materiały do bomby są powiązane z rodziną Larent; rodziną Diany. Zapytany o Dianę, Marcelin zaczął się rozpływać jaka to ona genialna, jaka ładna, zna się na wszystkim - chemia, elektronika, poezja, sztuka, języki - i nie jest magiem. Skąd wie? Nie jest w rejestrach SŚ a i kochanka z Millennium powiedziała, że nic nie wie o Dianie Larent.
Hektor nie pozwolił Marcelinowi odwołać kolacji i nie pozwolił mu ostrzec Diany (ani rozmawiać z nią na ten temat). Ale przesunął ją na status podejrzanej. I z przyjemnością się z Dianą spotka...

Wieczór, restauracja. Hektor zaprosił do restauracji ze sobą kustosz galerii sztuki (obrazy, rzeźby...), doktor Grażynę Remską. Na spotkaniu Hektor zobaczył ową Dianę. Młoda, arogancka i pewna siebie dziewczyna składająca się z fałszywej skromności i szczerej życzliwości. Wszystko wskazuje na to, że zaaranżowała to spotkanie po to, by móc zapytać Hektora do kogo mają się zgłaszać ludzie, którzy mają problemy a policja nie chce im pomóc. Hektor powiedział, że do niego... i następnego dnia dostał sporo telefonów. Asystentka sekretarza, Kinga Melit wszystkie te zgłoszenia dostała i przerobiła.
Hektor dowiedział się też, że galeria sztuki jest pod patronatem rodziny Larent tak samo jak jego (Hektora). Dowiedział się też, że Marcelin i Diana spotkali się właśnie tam, rozmawiając o rzeźbach.

**Flashback. Sporo czasu temu [1-3 miesiące, kolejne dwa tygodnie później].**

Hektor Blakenbauer stoi przeciwko Onufremu Puzlowi. Adwokat nie ma żadnych szans. Wiktor Lubaszny został zatrzymany podczas kontroli drogowej za zdecydowane przekroczenie prędkości i w bagażniku jego samochodu znaleziono 5 kg bardzo czystej metaamfetaminy. Coś, co nadaje się na zrobienie 30 kg o wartości rynkowej. Onufry Puzel wybrał linię obrony, że mimo, że nie ma żadnych dowodów, to wrogowie podrzucili.

Hektor wszedł twardo. Wiktor powiedział, że nie ma POJĘCIA za co ktoś go wrabia. O co w ogóle chodzi. Tak, należy do "Kopalińskich Komandosów", to taka para-grupa przestępcza, grupa kiboli z ambicjami na coś więcej i grupa która ma ambicje paramilitarne. Wiktor mówi, że ktoś się na nich uparł i ogólnie "Komandosi" nie mają się fajnie, ale nie wie czemu.
Hektor zmusił go do przyznania się do tego, że co prawda narkotykami się nie zajmował ale haracze i wymuszenia ma na koncie. Wystarczyło to Hektorowi by zostać biczem bożym na Wiktora Lubasznego.

Po zakończonej rozprawie Marcelin dostał do zbadania narkotyki i choć kręcił i unikał, znowu (tylko przy użyciu metod magicznych) wszystko wskazuje na rodzinę Larent. Hektor rzucił na temat grupę śledczą.

**Flashback, jeszcze tydzień później**

Wieczór. Marcelin załamany w swoim pokoju. Diana go rzuciła. Zameldował Hektorowi o tym Klemens, który rozbił związek. Żeby nie było problemów, przy tak dominującej dziewczynie było bezpieczniej, by to ona rzuciła.

**Dzień dzisiejszy**

Hektor Blakenbauer dowiedział się, że będzie sprawa której będzie bronić Bianka Drażyńska. Na ławie oskarżonych - Diana Larent. Złapana na gorącym uczynku. W nocy włamała się do supermarketu (używając cegły sztuk jeden) i z dwóch dezodorantów zrobiła mały miotacz ognia włączając zraszacze. Duże straty. Nie uciekała, czekała na policję.

Do tej pory Hektor już połączył fakty od grupy specjalnej i czarów Marcelina; najpewniej to Diana jest osobą, która stała bezpośrednio (Diana a nie ktoś z jej rodziny) za sprawą "Kopalińskich Komandosów". Więc Hektor nie obraził się mając Dianę na celowniku. Ale coś tu bardzo nie pasuje. Ruchy Diany były takie, jakby chciała zrobić jak najwięcej hałasu, ściągnąć uwagę.

Jeszcze przed salą sądową, wstępne przesłuchanie Diany przy obecności Bianki. Diana bardzo nie chciała nic powiedzieć. Mimo działań Bianki Hektor wszedł twardo (10+2v10->13) i dowiedział się, że Diana Larent chciała ściągnąć policję, by zapobiec pobiciu. Nie chce powiedzieć KOGO i KTO, ale Hektor dowiedział się, że to miało miejsce przy supermarkecie. Innymi słowy, Diana Larent musiała działać osobiście i natychmiast.
Hektor nie ma na Dianę absolutnie nic. W związku z tym będzie chciał zaproponować ugodę i zapłacenie. Ku wielkiemu zdziwieniu Diany i Hektora, Bianka się nie zgodziła.
Hektor wie, że ugoda jest NAJLEPSZĄ opcją dla Diany. Diana też tak myśli. Ale Bianka mówi coś tajemniczego o jakichś "innych sprawach" i Diana nic już nie rozumie. Dla Hektora jest to o tyle dziwne, że Bianka ma opinię doskonałego prawnika i zawsze dbała o klientów. To co teraz się dzieje to nie Bianka.
Hektor zostawił je same. Niech to przedyskutują...

Hektor nie rozumie co się tu dzieje. Bianka zachowuje się nie tak jak powinna, i Hektor nie wie o co chodzi. Łamie się; najlepiej byłoby rzucić czar, ale to tak nie jest w stylu Hektora... jednak może jest to powiązane ściśle z jakimś magiem?
Jednak Hektor rzuci zaklęcie.

Hektor zauważył coś ciekawego. Bianka zawsze miała srebrną obrączkę, ale nie ma dziś nic srebrnego. Hektor założył srebrny sygnet i podał rękę Biance. Ta wyrwała rękę, oparzona srebrem. Hektor już wie, że ma do czynienia z magią...

Do Hektora zadzwonił Marcelin. Hektor się zdziwił - co? Okazuje się, że Diana skontaktowała się z Marcelinem; miała w pończosze komunikator morse'a. Co? Czy Marcelin każdej eks zostawia panic button? Nie, to był pomysł Diany. Diana na to wpadła, bo Hektor dał jej do zrozumienia, że wie kto za tym wszystkim stoi na spotkaniu w restauracji, więc chciała mieć kontakt do Marcelina jakby co... i Marcelin teraz pyta Hektora ("słowo brata"), czy naprawdę będzie ta ugoda najkorzystniejsza dla Diany? Odpowiedź Hektora była "tak". Więc jak tylko Diana dostała prawo rozmowy, zadzwoniła do Marcelina i przyjęła ugodę.

Bianka była wściekła. Ale nie zrezygnowała z reprezentowania Diany... co też nie brzmi jak Bianka.
Diana dostała grzywnę (zapłaciła) i prace społeczne (jest w wolontariacie, więc nie ma dużej różnicy). A Hektor ma o czym myśleć.

W Rezydencji Blakenbauerów. Hektor rozmawiał sobie z Edwinem i próbował znaleźć coś, co wyleczy najlepszą panią adwokat z nieznanych czarów. Edwin zauważył, że on i Margaret mogą od ręki przygotować pigułkę która rozproszy efekty magiczne czy mentalne, zwłaszcza, jeśli Edwin jeszcze poprosi Mojrę. Hektor się ucieszył i na to wszedł Marcelin. Oznajmił ogłupiałym braciom, że w SŚ jest w tej chwili akcja. Magowie kręcą film. Dlatego SPENETROWALI ADWOKATURĘ. Jest absolutny zakaz rzucania czarów na dwoje ludzi - Dianę Larent oraz Arkadiusza Wadiceka (who the hell is Wadicek). Magowie, a dokładniej tien Mariusz Garaż kręci film i ma zamiar dostarczyć rozrywki tysiącom magicznych widzów...

Hektor klnie w żywy kamień. Marcelinowi nie podoba się, że sprawa dotyczy JEGO eks. Edwin jest nieszczęśliwy, bo wszyscy siedzą w jego pokoju. Innymi słowy, trzeba Coś Z Tym Zrobić (zdaniem Edwina: zacząć od jego pokoju). 

Grupa Śledcza dostała zadanie zbadania korelacji: Diana Larent, Arkadiusz Wadicek oraz "Kopalińscy Komandosi". Tymczasem Marcelin umówił Hektora na spotkanie z tien Mariuszem Garażem. Ze spotkaniem nie było problemu; restauracja "Królewska".

Mariusz Garaż jest STRASZNY. Gość jest po prostu psychiczny. Jest zafascynowany okazją biznesową by pokazać magom TELEWIZJĘ. Ale nie taką "zwykłą" telewizję - on kręci takie filmy, gdzie cały czas skanowani są ludzie (główny aktorzy) i potrzebne są zwroty akcji. Celem tego filmu jest "upadek superbohatera" gdzie Diana pełni rolę Batmana, Arkadiusz jest Robinem który nie chce swojego Batmana a "Kopalińscy Komandosi" są przeciwnikami. Mariusz Garaż powiedział, że nie wybrał losowych osób - wybrał Dianę, bo jest osobą specjalną tak jak Arkadiusz, ale Diana sama z siebie wkręcała różnych przestępców. Czyli Hektor dostał potwierdzenie. Hektor nie chce pomagać Garażowi (uważa go za nieetycznego i okrutnego), ale Garaż go wkręcił; dowiedział się, że żeby zamknąć Dianę do aresztu (z którego ma potem uciec) musi po prostu Diana zrobić taką pierdołę, żeby nie doszło to do Hektora.
Garaż ma poparcie SŚ. Jest nie do ruszenia dla Hektora a jak Hektor będzie próbował mu przeszkadzać, to Garaż będzie w stanie zrobić niesamowite koszty Blakenbauerom. Ale Hektor nie chce po prostu "iść" na to. Hektor chce jakoś to wygrać, ale nie wygra procesu w świecie magów...
Telewizja Garaża wygląda tak, że magowie jak skończy się film mogą wcielić się w poszczególnych aktorów, czy to w Arkadiusza czy w Dianę i widzieć to co oni widzą, czuć to co oni czują... to "prawdziwa telewizja, prawdziwe emocje". Dlatego program musi też być "prawdziwy" a aktorami nie wolno manipulować!
Hektor już go nie lubi. A szkody jakie ponoszą ludzie zupełnie nie interesują magów, którzy pragną swojej rozrywki. Garaż ma pozwolenia.
Hektor nie jest w stanie zablokować Garaża. Na szczęście, jest "Skorpion" który będzie potem mógł Coś Zrobić z tym gościem... ale na razie - trzeba chronić Dianę i Arkadiusza.

Hektor rozstał się z Mariuszem Garażem, który nie ukrywał, że bardzo cieszy go możliwość łatwego zarobku kosztem ludzi. Hektorowi humor poprawił telefon Anny Kajak. Anna wyjaśniła wszystko między "Komandosami", Arkadiuszem i Dianą:
- Jest sobie pewien student, który zbłądził i został "Komandosem", Adam Bożynów. Diana otoczyła go opieką. Adam chciał odejść, ale od "Komandosów" nie da się odejść.
- Mikołaj Młot porysował auto i zabił psa Bożynowa. To jest powiązanie z Dianą, ale za słabe na dowód.
- Wiktor Lubaszny nie jest powiązany w żaden sposób poza tym, że jest członkiem "Komandosów".
- Arkadiusz Wadicek został zidentyfikowany jako ten, który stał za wrobieniem Młota i Lubasznego przez szefa "Komandosów" (tak mówi; Hektor podejrzewa magię - Hektor po rozmowie z Garażem wie, że KIEDYŚ Diana chciała napuścić "Komandosów" na Wadiceka, ale zatrzymała to; jednak Garaż powiedział, że "podpowiedział" kto za tym stoi).
- W "Komandosach" jest syn dystrybutora policjantów w Kopalinie i okolicy; niejaki Maciej Kwarc. Jest zastępcą. Dlatego policja patrzy na "Komandosów" z przymrużeniem oka.
- Arkadiusz jest byłym przyjacielem Diany; kiedyś był "w jej kręgach" ale się rozstali. Nie zgadzał się z jej metodami, cokolwiek to znaczy.

Hektor zanotował, by osobie powiązanej z Maciejem Kwarcem wlepić naganę za to, że część doniesień jest ignorowana i że "Komandosi" szaleją. Najpewniej to właśnie uruchomiło Dianę i skłoniło ją do radykalizacji. To się nie stanie od razu, ale to się stanie.
Hektor podejrzewa, że Diana za niedługo trafi do aresztu, by uciec.

Tajniacy z grupy śledczej mają mieć na oku Arkadiusza i Dianę; mają ich śledzić i pilnować, aby nic im się nie stało. Może dopaść ich vendetta. Mają też zbierać dowody na Dianę, bo póki co wszystkie źródła są nieoficjalne.

23:11. Wstrząśnięta Anna Kajak zadzwoniła do Hektora; Diana zadzwoniła do Arkadiusza i poprosiła go o zejście na dół. Gdy ten zszedł, przejechała go motorem i uszkodziła mu nogę. Arkadiusz trafił do szpitala. Diana, z plecakiem, pojechała na plac budowy hipermarketu w Kopalinie. To się stało wkrótce po tym, jak Diana dostała jakiegoś MMSa; jest w trakcie przetwarzania. Anna wysłała kogoś z Arkadiuszem do szpitala a sama czeka na instrukcje.

Hektor dostał MMSa od Anny. Na MMSie jest facet w pończosze na głowie, trzymający w jednej ręce nóż a drugą rękę na ramieniu przerażonej dziewczyny, z lekko nadciętą bluzeczką z podpisem "lepiej się pospiesz, braciszku". Taaaaak, "przypadkiem" wysłał MMSa pod zły numer (do Diany, nie Arkadiusza). Mariusz Garaż 'Grafoman', reżyser z psiej łaski...

Tymoteusz Dzionek, agent Skorpiona który został oddelegowany do współpracy z Blakenbauerami dostał niespodziewany telefon od Hektora. Hektor szybko powiedział, jaka jest sytuacja, że magowie wpieprzają się i bawią się kosztem ludzi min. na placu budowy. Dzionek powiedział, że wyśle tam kogoś; ma akurat kogoś pod ręką. Podziękował Hektorowi, że jest innym magiem niż "zwykle" i się rozłączył.

Tymczasem na budowanym obiekcie. "Komandosi" czekają, ale na motorze przyjechała Diana, nie Arkadiusz. Diana rozlała bryzgiem wodę z manierki na trawę i powiedziała im, że Arkadiusz nie przyjedzie, bo ona wsadziła go do szpitala. Spytała jednego z "Komandosów" czy orientował się, gdzie znajduje się jego ukochany kotek dziś wieczorem. Innemu powiedziała, by rozlał ciecz z manierki na trawę (tam, gdzie ona zmoczyła :>) - trawa zżółkła i ucierpiała. Diana spokojnie powiedziała, że zatruwała go od dawna i brakuje tylko trochę, by wylądował jako kaleka w szpitalu.
Gdy inny "Komandos" ruszył w jej kierunku, rzuciła weń butelką. Duszący gaz zmusił go do cofnięcia się, po czym "Komandos" padł na ziemię w drgawkach. Diana spokojnie wyjaśniła, że być może oślepł a nie zdziwi się, jeśli od tego umrze. Krzyknęła, że to nie Arkadiusz ich atakował - to cały czas była ona. I ona jest skłonna zabić zakładniczkę, jeśli dzięki temu pozbędzie się ich raz na zawsze.

Ten melodramatyczny monolog przerwał śpiew pijaka na owym placu budowy (ubrany w moro), który spadł z ławki. Diana prychnęła i odwróciła się od niego w kierunku na Komandosów, lecz pijak szybko potraktował ją strzałką i dziewczyna opadła na ziemię, nieprzytomna. Gdy "Komandosi" poderwali się chcąc do niej się dostać, pijak (Kamil Rzepa, agent Skorpiona) pokazał im AK-47 i powiedział, że "młoda zadarła z nie tymi co trzeba". Kazał "Komandosom" spieprzać, bo od tej pory "młoda" należy do niego. I zakładniczkę też chce. Kazał im wsadzić nieprzytomną Dianę i przerażoną Felicję Wadicek do swojego czarnego vana, po czym kazał im "spieprzać do przedszkola".

W samym vanie, w pełni świadom, że nie może wypaść z roli Kamil powiedział Felicji, że Diana zadarła z nie tymi co trzeba i wstrzyknął jej jakieś serum. "Jako nauczkę - może przeżyje". Samo serum jest bardzo wredną toksyną, ale nie jest śmiertelne; po prostu powoduje bardzo nieprzyjemne objawy (wymioty, ból, cierpienie... ogólnie, magioinżynieria Estery doprowadziła do wynalezienia czegoś bardzo mało groźnego ale bardzo nieprzyjemnego co da się sformować bez użycia magii), żeby Mariusz zdecydował się zakończyć owo "reality show". Po czym Kamil wystawił Felicję i Dianę na pobocze i odjechał w siną dal...

Faktycznie, z Arkadiuszem w szpitalu (wyjdzie z tego bez szwanku i bez straty formy) i z nieprzytomną, cierpiącą Dianą (2-3 tygodnie regeneracji w chorobie) Mariusz stwierdził, że ma taki film jaki może mieć i zakończył kręcenie filmu. Trudno, nie jest IDEALNY, ale jest dość realny i interesujący by mieć pilot na dalsze eksperymenty. Projekt został zakończony.

Tylko Hektor smutny, bo Diany nie miał okazji przymknąć za rzeczy które akurat popełniła ;-).

"Chciałeś zamknąć babcię mojej eks za odmowę zadzwonienia na policję gdy przeszła na czerwonym świetle! Nie pozwolę Ci zamknąć nikogo z rodziny Diany, miłości mojego życia!" - Marcelin do Hektora.
"Diano, czy ciągle poprawiasz ludzi, gdy się mylą?" - "Nie, nie ciągle; tylko wtedy, gdy nie mają racji" - Grażyna i Diana.
"Trzeba ją zamknąć żeby mogła uciec, panie prokuratorze! Chociaż na 24 godziny". - Mariusz Garaż do Hektora.
"Trzeba być na czasie, panie prokuratorze! Teraz nie robi się filmów, teraz jest pies przebrany za pająka!" - Mariusz Garaż do Hektora.
"Chce pan, by pana sztuka była realna, naturalna..." - "Nie. Chcę zarobić. Ludzie lubią telenowele, magowie pewnie też" - Hektor i Garaż.
"Czyli nie mam dowodów, żeby Dianę zamknąć? :-(" - Hektor, do Anny Kajak już po wszystkim.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                        1. Restauracja Królewska
                    1. Dzielnica Kwiatowa
                        1. Galeria sztuki Van Gogh
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                            1. Pokój Edwina
                     1. Plac budowy hipermarketu NeWorld
 

# Zasługi

* mag: Hektor Blakenbauer jako prokurator, który pozostał wierny sobie i przez to uratował Dianę przed "Reżyserem" zamiast ją zamknąć.
* czł: Diana Larent, geniusz z bogatego domu. Dziewczyna, która stała sie aktorką mimo woli w grze Mariusza Garaża... i przeraziła się własnej bezwzględności.
* czł: Arkadiusz Wadicek, spokojny i zdecydowany przyjaciel Diany, który także stał się aktorem mimo woli w grze Mariusza Garaża i którego postanowiła chronić Diana.
* czł: Felicja Wadicek, siostra Arkadiusza użyta jako atut przetargowy przez "Kopalińskich Komandosów" co mogło skończyć się tragedią dla owych Komandosów...
* czł: Maciej Kwarc, syn Juliusza; zastępca szefa "Kopalińskich Komandosów" i osoba przez którą "Komandosi" są w miarę bezpieczni od policji.
* czł: Janusz Wybój, szef "Kopalińskich Komandosów". 31 lat, historia przestępcza i pełna gangsterka. Tu: rola marginalna.
* czł: Mikołaj Młot, "Kopaliński Komandos", którego wrobiła Diana w posiadanie maszyny piekielnej (niewinny); w rzeczywistości ukrzywdził Adama Bożynowa. Hektor posadził :P.
* czł: Wiktor Lubaszny, "Kopaliński Komandos", którego wrobiła Diana w handel narkotykami (niewinny); w rzeczywistości za wymuszenia i szantaże. Hektor posadził :P.
* czł: Adam Bożynów, student mający poważne problemy z "Kopalińskimi Komandosami"; chce od nich odejść a nie może. Chroniony i przez Arka i przez Dianę.
* mag: Mariusz Garaż, "reżyser" chcący wprowadzić "telewizję" do świata magów. Zrobił dwa ruchy i załatwił pozwolenia od SŚ... ale przeciw sobie ma Hektora i Dianę.
* czł: Onufry Puzel, adwokat z urzędu. Stara się, ale nie ma doświadczenia i nie jest w stanie pokonać na sali sądowej Hektora Blakenbauera...
* czł: Bianka Drażyńska, która jest tym dla adwokatów czym jest Hektor dla prokuratorów. Oczytana, inteligentna, cholernie droga. Tu: kontrolowana przez "reżysera" by posadzić Dianę.
* czł: Grażyna Remska, kustosz galerii sztuki w Kopalinie (dotowanej przez Hektora i Larentów) i czarująca towarzyszka Hektora do obiadu.
* czł: Anna Kajak, członek grupy śledczej infiltrująca "Kopalińskich Komandosów" oraz rodzinę przemysłowców Larent (przez Dianę, która robi złe rzeczy).
* czł: Klemens X, który zniszczył związek Marcelina i Diany Larent w taki sposób, że to Diana zerwała z Marcelinem.
* mag: Marcelin Blakenbauer, który mimo, że Diana z nim zerwała dalej jest skłonny jej pomagać i jest Dianą zafascynowany. 
* mag: Edwin Blakenbauer, który mimo narzekania na bezcelowość pomagania ludziom i tak jednak im pomaga.
* czł: Tymoteusz Dzionek, koordynator Skorpiona z którym skontaktował się Hektor by uratować Dianę... lub "Komandosów". Nie wiadomo.
* czł: Kamil Rzepa, agent Skorpiona, faktyczny żołnierz który udaje zapijaczonego gościa by obezwładnić Dianę, odbić zakładniczkę i zakończyć "film" Garaża.

# GM Notes:

- Czyli Skorpion nie jest "tylko zły". Może być przydatny z inicjatywy Hektora.
- Czyli Blakenbauerowie, choć czarny ród, mają też jaśniejszą stronę... poza Ottonem :p.