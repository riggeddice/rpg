---
layout: inwazja-konspekt
title:  "Ucieczka Sióstr Światła"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161129 - Ewakuacja Natalii, wejście maga (HS, temp)](161129-ewakuacja-natalii-wejscie-maga.html)

### Chronologiczna

* [161129 - Ewakuacja Natalii, wejście maga (HS, temp)](161129-ewakuacja-natalii-wejscie-maga.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Rankiem Henryk znalazł się znowu na plebanii w Lenartominie. Przyjeżdża też omijając plebanię wezwana przez księdza paladynka - Anna Osiaczek. 
Henryk i Anna pojechali na miejsce gdzie uprzednio użyta była magia (do przekształcenia Mai), koło domu Hipolita Mraczona. Anna poczuła Zło dochodzące z tamtego domu.
Nie pozwoliła Henrykowi wejść do domu.

Henryk odpalił czar astralny by dowiedzieć się co tu się wcześniej wydarzyło. Udało mu się zobaczyć twarz Marzeny Dorszaj. Zobaczyć jej motywację - jest radykalną i mściwą ideolożką. Ale też, niestety, Marzena zobaczyła twarz Henryka. Ale nie jego motywację - wie jednak, że Henryk na nią poluje.
Henryk zwiał z Anną na plebanię. Henryk oficjalnie, Anna na mszę.

Na plebanii Henryk został zaproszony przez lokalnego księdza Klepiczka na własno robione naleśniki.

"Masz jakiś pomysł na tą przynętę?" - Raynor
"Ty" - Kić, 2016
"Dobra..." - Raynor, lekko zaskoczony

Henryk zjadł naleśniki z księdzem i szybko udał się na służbę w konfesjonale.
Anna siedzi w kościele jako parafianka. Udaje, że nie jest paladynką.

Marzena skupiła się, by pomóc Amelii Eter znaleźć Srebrne Lusterko Arazille - i udało się to zrobić. Siostry Światła dały Amelii lusterko.
Henryk siedzi w konfesjonale. Poczuł manifestujące się Srebrne Lusterko.
Niewiele może zrobić... ale się wkurzył.

Henryk wraca na naleśniki. Klepiczek się uśmiechnął. Henryk się nie cackał. Zaczarował księdza.

"Czas na nawracanie" - Raynor, 2016

Sprawdzenie: czy ksiądz Klepiczek podburzy tłum i opinię przeciw femisatanistkom? Tak - od następnego dnia Pryzmat będzie neutralny. Nie będzie wspierał femisatanistek.
Sprawdzenie: czy działanie Henryka sprowokuje Marzenę do głupiego ruchu? Tak - Marzena wysłała herpyneę do zlikwidowania Henryka.

Henryk koncelebruje mszę z Klepiczkiem. Anna jest pokorną parafianką. I Anna wyczuwa Zło.
Zło właśnie weszło do kościoła.
Anna jest pewna jednej rzeczy. To, co weszło do kościoła NIE jest człowiekiem. Ale wygląda jak człowiek - jak kobieta.

Anna wyszła z nawy i podeszła do herpynei, zastawiając jej drogę. Herpynea chce ją zignorować, ale Anna nie pozwala. Anna odsunęła herpyneę; demoniczna istota nie jest w stanie dotrzeć do księdza nie przechodząc przez "parafiankę". Herpynea nie do końca wie co z tym robić; odwróciła się i wychodzi z kościoła. Anna idzie z nią, pytając, czy nie trzeba pomóc. Herpynea wychodzi; Anna specjalnie pryska ją wodą święconą.
Herpynea zostaje zraniona.

Herpynea zaatakowała Annę MĘCZARNIĄ. Paladynkę zwinęło, jednak herpynea jedynie się wycofała, zgodnie z instrukcją. Henryk krzyknął, że szachrajstwo zaatakowało parafiankę i skupił się by femisatanistki były winne. Chce porwać ludzi za sobą przeciw Siostrom Światła.
Udało mu się. Henryk dał radę podburzać i porywać ze sobą ludzi. Zajęło mu to jednak czas do wieczora, co kupiło Marzenie czas by zdołała wycofać Siostry Światła do uprzednio przygotowanej bazy, poza Lenartominem.

Tyle, że Marzenia nie wiedziała, że Henrykowi Siostry Światła po prostu wiszą. Henryk poszperał w materiałach jakie zostały po femisatanistkach i znalazł bardzo mocne dowody, papiery itp świadczące, że Kora planowała atak i działania przeciwko Hubertowi. I miała wysokie szanse powodzenia. I nadal ma.

Czyli Henryk ma dowody dla Huberta.

Henryk jeszcze poszedł do studni dowiedzieć się informacji o tym srebrnym lustrze, które zostało znalezione i zniknęło. 
Zaklęcia astralne pokazały, że Amelia dostała to lustro i udała się w kierunku zachodnim, na tereny kontrolowane przez Świecę.
Niestety, Henryk nie wie wiele więcej i nie jest w stanie teraz jej śledzić...

W Wyjorzu.

Henryk podziękował Annie ślicznie i ją poprosił o zamieszkanie w hotelu Jantar. A Henryk poszedł do Trójzębu.

Henryk pogadał z Hubertem. Powiedział mu, że Siostry Światła na niego polowały i pokazał Hubertowi dowody na to, że wysokie były szanse powodzenia. Hubert się serio zmartwił. Poprosił Henryka, by ten pomógł mu zabezpieczyć Trójząb. Ten się zgodził (zajmie to pełny dzień). Hubert wierzy, że Henryk naprawdę mu pomógł. Henryk wie, że przed magiem czy viciniusami się nie zabezpieczyli...

Ale Hubert jest wdzięczny. Zawsze coś.

Henryk od razu zauważył, że jak tylko powiedział, że rozgonił Siostry Światła to morale wśród części analityczek zdecydowanie spadło.

# Progresja

* Amelia Eter: dostała Srebrne Lustro Arazille
* Henryk Siwiecki: dostał jako wroga Marzenę Dorszaj
* Henryk Siwiecki: dostał jako przyjaciela Huberta Kaldwora
* Andrzej Klepiczek: zmieniło się postrzeganie jego osoby po płomiennym wypędzaniu Sióstr Światła

# Streszczenie

Marzena sprowadziła herpyneę by pokonać Henryka, stanowić ostrzeżenie. Henryk jednak użył ludzi i zaczarował księdza Klepiczka by wygnać Siostry Światła. Marzena dała radę wycofać się do tajnej bazy. Henryk wykorzystał to by podnieść swój ranking u Huberta Kaldwora. Hubert jest bardzo zadowolony.

# Zasługi

* mag: Henryk Siwiecki, który wypędził Siostry Światła z Lenartomina, ostrzegł Huberta i przekazał Świecy, że Srebrne Lustro Arazille jest w rękach "Amelii".
* czł: Anna Osiaczek, blokująca herpyneę w kościele jako tarczowa parafianka i wyczuła, że Marzena Coś Złego Zrobiła w domu Mraczona.
* mag: Marzena Dorszaj, poznała twarz wroga (Henryk) oraz po jego atakach bezpiecznie wycofała Siostry Światła do tajnej kryjówki.
* czł: Amelia Eter, uzyskała Srebrne Lustro z pomocą Sióstr Światła. Bezpiecznie wycofała się na Śląsk.
* czł: Andrzej Klepiczek, ksiądz, którego Henryk zmusił magią do wygnania Sióstr Światła z Lenartomina. Sam nie wie co w niego wstąpiło ;-). 
* czł: Hubert Kaldwor, który po przedstawieniu dowodów przez Henryka uwierzył, że był w niebezpieczeństwie. Sojusznik Henryka - jest mu wdzięczny.

# Czas

* Dni: 1