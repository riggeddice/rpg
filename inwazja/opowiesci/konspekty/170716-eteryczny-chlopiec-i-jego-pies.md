---
layout: inwazja-konspekt
title: "Eteryczny chłopiec i jego pies"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170523 - Opętany konstruminus (PT)](170523-opetany-konstruminus.html)

### Chronologiczna

* [170523 - Opętany konstruminus (PT)](170523-opetany-konstruminus.html)

## Kontekst ogólny sytuacji

Jednostrzałówka.

## Punkt zerowy:

Pytania MG do siebie:

* Co zasila Chłopca i Psa?
* Skąd oni się wzięli w tej okolicy?
* Jak dawno temu to się stało?

**Stan początkowy**:

1. Chłopiec
    1. Transformacja kobiety w swoją mamę:  0/10
    1. Zbudowanie zabawki mającej przejąć grupę dzieci do siebie:   0/10
    1. Przejęcie kontroli nad Psem:     0/10
    1. Seed wskrzeszenia:   0/10
    1. Zalinkowanie energii z Węzłem: 0/10
1. I jego pies
    1. Egzekucja zwierząt:  0/10
    1. Egzekucja ludzi:     0/10
    1. Zasilenie się by się ustabilizować:  0/10
    1. Wampiryczna stabilizacja:    0/10
    1. Odnalezienie Senesgradu: 0/10
1. Naturalna Entropia
    1. Skażenie okolicy:    0/10
    1. Złamanie Maskarady:  0/10
    1. Panika wśród ludzi:  0/10
    1. Chorzy / ranni dla Pauliny:  0/10
    1. Skażenie Wąskiego go zmieni: 0/10


**Pytania i odpowiedzi**

* Ż: Jak byś opisała miejsce, gdzie jest gabinet Pauliny?
* K: Trochę jak... rzadsza zabudowa, osiedle domków, małe miasteczko. Ma sąsiada ;-). Widzę to jako długą działkę z mało widocznymi zakątkami. Ma szopę i ogródek.
* Ż: Kto jest okolicznym małomiasteczkowym lekarzem mającym plany matrymonialne wobec Pauliny? Człowiek.
* K: Karol Wąski, laryngolog. Próbuje się wydostać z friendzone ale nie narzucając się ;-).
* Ż: Jak pomogłaś mu kiedyś na poziomie paranormalnym?
* K: Od tego zaczęła się ich znajomość. Skaził się magią. Paulina nie wie jak, po prostu BYŁ Skażony i chorował. I ona to naprawiła.
* Ż: Wąski mieszka w Półdarze. Poza hodowlą psów rasowych wieś ta ma jeszcze jedną właściwość. Jaką?
* K: Hodowane są tam Husky. Jedna z lepszych hodowli; tam są wygrywające psy. Żaden weterynarz nie zagrzał długo miejsca w samej wsi; raczej są przyjezdni.

## Misja właściwa:

**Dzień 1:**

_Rano: (+1 akcja O)_

Paulina dostała telefon od Wąskiego. Powiedział Paulinie o dziwnym pacjencie - ugryzł go pies a rana dookoła ugryzienia obumarła. Paulina poprosiła, by go przysłał; zrobione.

I faktycznie, przyszedł Pyżuk i przyprowadził synka. Podobno młodego ugryzł pies - ale NIE pies Pyżuka - i coś się dzieje złego. Paulina zauważyła od razu - infekcja nekrotyczna, ugryzienie przez COŚ związanego z energią negatywną. Nieumarły pies? I to jeszcze zarażający. Paulina podała painkiller. Ojciec próbuje dziecko pocieszać, "bądź dużym chłopcem" nie rozumiejąc, że nekrotyczna rana jest STRASZNIE bolesna. Paulina wzięła próbkę skalpelem. Rana jest śmiertelna, ale Paulina ma sporo czasu; miesiąc?

Zostawiając ludzi, Paulina przeszła do swojego magilab by sprawdzić 'wtf'. Chce wyleczyć i naprawić chłopaka, w tym celu robi badania. ST:5(kata-nekro). Paulina używa magii na kata-nekro, więc jej ST jest tylko 3. 6v3->S, ES. Zadziwiające Zaklęcie. 

* Ż: Imię i nazwisko wybitnego polskiego lekarza. Maga. Żył 100 lat temu i na pewno nie żyje.
* K: Bogumił Miłoszept.

Powietrze zgęstniało. Do gabinetu Pauliny wkroczył widmowy Miłoszept. NA PEWNO dwójka w poczekalni jest przerażona (ojciec, syn). Miłoszept zapytał z sympatią, czy to badania nad nekrozą. Miłoszept powiedział, że jest to ugryzienie przez upiora; efemerydy-echa wydarzenia lub istoty przeszłości. Patrząc po ranie, pies. Ale pies nie może być tego typu upiorem. Więc pies jest elementem efemerydy - ale nie efemerydą. Upiór musi żywić się na życiu; ten pacjent zasila upiora.

Paulina mruga oczami słuchając wykładu maga, który nie żyje... a on po skończeniu wykładu... wyszedł przez portal. Paulina przygotowała kontrzaklęcie mające uleczyć chłopca. ST7(nekro-kata). I zaklęcie ma zabezpieczyć przed feedbackiem. Skażona tkanka pomoże z namierzeniem psa... więc wpierw Paulina zdejmuje sygnaturę z tkanki. Paulina chce namierzyć mniej więcej lokalizację Psa. ST7(rozproszenie). Po stronie Pauliny +1 za Wiedzę od Bogumiła i +1 za Skażoną Tkankę i +1 za nekro-kata. 6v7->F.

Paulina podążyła za linkiem (akcja+1). Niestety, wpakowała się między Chłopca i jego Psa. Gdy już miała kłopot... wyciągnął ją duch Bogumiła i odegnał eteryczne istoty na swoje miejsce. Po czym opieprzył Paulinę za robienie takich rzeczy bez wsparcia terminusów. I w ogóle nie słuchała na jego wykładach. Po czym wyszedł przez drzwi, mrucząc coś odnośnie niekompetentnych studentek.

Paulina przygotowuje kontrzaklęcie mające wyleczyć Mikołaja. ST7(połączenie nekromantyczne). Paulina wie co to jest (+1), wplecie nekro-kata w zaklęcie (ST-2), przygotowuje metodycznie wejście do tematu. 7v5->S. Kolejny Efekt Skażenia. Znowu Bogumił.

Bogumił się pojawił i zatrzymał Paulinę; sam jej pokaże jak się to rzuca. A potem wyjaśnił, że nie powinien móc się pojawić; coś się zmieniło w otoczeniu Pauliny. Coś, co umożliwia jemu pojawienie się i pomoc jej. Czyli coś jest nie tak w okolicy. Paulina podziękowała i Bogumił się oddalił. Przez drzwi.

W poczekalni ojciec i syn strasznie są... nieswoi. Ojciec tłumaczy synowi, że to mu się wydawało. Gość nie przeszedł przez drzwi. I nie był dziwnie ubrany. I nie był tu trzy razy... i NIE, nie rozpłynął się w powietrzu. Paulina już się nie pieprzyła - zaklęcie leczące (naprawiło ranę), usunięcie pamięci artefaktami (-1 surowiec). Zrobiła opatrunek odsączający, wstrzyknęła antybiotyk i wyjaśniła, że to było zakażenie. Pies był jakiś lewy, najpewniej chory lub coś jadł. Wysłała chłopaka na badania (ludzkie, dla zamaskowania), po czym zgarnęła pieniądze, dała instrukcje i koniec.

Paulina, że mały ma nie wychodzić z domu a psy należy obserwować. Jakub Pyżuk powiedział, że ma dobrego weterynarza. Pies infekujący to był jack russell terrier. Taki sam jak Paulina widziała w wizji. Czyli są najpewniej dwa byty, bo tyle Paulina widziała: Chłopiec i Pies.

_Południe: (+1 akcja O)_

* Ż: W jaki sposób Paulina została Rezydentką?
* K: Widziałam to tak, że jej mistrz ją nauczył, pozwolił jej wybrać a potem jej załatwił rezydenturę.
* Ż: To implikuje, że ktoś w okolicy coś wisiał mistrzowi Pauliny.
* K: For a given value of okolica. W okolicy.
* Ż: Nazwij maga, który jest Paulinie przyjazny w Świecy. Wpierw przez mistrza, potem "bo tak".
* K: Nie rodowy. Beata Obaczna.
* Ż: Jaki jest jej największy problem (jednym słowem) i jaka jej największa siła (jednym słowem).
* K: Jej problemem jest Samotność, jej siłą Strach.

Paulina skontaktowała się z Beatą przez hipernet. Wyjaśniła, że ma problem z efemerydą wykorzystującą ludzi i potrzebuje terminusa. Pierwszą ofiarę udało jej się odpiąć, ale na pewno upiór znowu zaatakuje. (-1 surowiec, 1k20+1 mocy -> wynik; za ujemne MG dodaje aspekty terminusowi, za dodatnie gracz). 0. Czyli aspekt za aspekt. Proludzki, "porywczy w destrukcji", kompetentny, samotny wilk, "doświadczony w takich sprawach", "ma sporo wrogów wśród magów", "ma protektora w Świecy", "niczego nie da się mu zabrać"

Beata powiedziała Paulinie, że jest jeden dostępny terminus którego może jej wysłać. Nazywa się Gustaw Bareczny.

_Wieczór: (+1 akcja O)_

Paulina zdecydowała się zrobić coś nietypowego. Skomunikować się z efemerydą. Zrozumieć, co się dzieje na tym terenie. Połączyć się ze SWOJĄ efemerydą ;-). Ma +1, bo to jej efemeryda i +1, bo energia pochodzi z tego terenu a ona zna teren. No i Paulina poświęca na to sporo czasu, kolejne +1. 8vx->R (+1 akcja), 8.

Przed Pauliną zmaterializował się Bogumił i zauważył, że jest lekarzem a nie jej kamerdynerem. Paulina przeprosiła i zaczęła wypytywać Bogumiła o to, co się dzieje. On wyjaśnił, że upiór jest niestabilny i jest skonfliktowany sam ze sobą; Chłopiec walczy z Psem a Pies walczy sam ze sobą. Chłopiec chce dołączać do orszaku, Pies chce odstraszyć i się żywić. Pies zasila Chłopca. Skąd to się wzięło? Na terenie Pauliny pojawił się artefakt, który zasilił się energią z Senesgradu. Czyli BYŁ w Senesgradzie a teraz jest gdzieś w okolicach hodowli psów...

...Bogumił zamigotał, powiedział Paulinie, by ta wiała, po czym tą efemerydę zastąpił Chłopiec. Ów zapytał radośnie, czy Paulina będzie jego mamą. A konflikt z niego podły. Chłopiec, vicinius (upiór). (15, zasilanie (3), wsparcie Psa (3), dziecko (2), szuka przyjaciół(2)).

Paulina odpowiedziała pytaniem - co się stało z jego mamą. Chłopiec odpowiedział, że go zgubiła. A teraz on zgubił Psa (Uszatka). Czy Paulina pomoże mu znaleźć Uszatka? Paulina zdecydowała się go spowolnić, by terminus tu dotarł na czas. Rozmową. 10-3(brak Psa)-2(Dziecko), ST Pauliny to 5. Paulina poszła w kierunku "opowiedz mi o mamie" i zdecydowała się pograć mu na gitarze. +1 temat, +1 spowolnić. 6v5->7. 

Paulina poczuła akumulację energii magicznej dużej mocy od strony spoza chaty. Wysłała sygnał na hipernecie, by przestał. Terminus WALNĄŁ potężną wiązką destabilizując upiora i wysysając mu energię też przez kanały; przeszło przez ścianę i w ogóle. Lekko uszkodziło dom Pauliny a upiór dziecka wyparował i zostawił ślad na ścianie Pauliny. Paulina w szoku - w końcu to było dziecko. Opierniczyła terminusa, który zauważył, że to nie dziecko a upiór. Ogólnie, Paulina i terminus przeszli na... lepsze stosunki. On uważa ją za pistolet, ona jego za gbura. Ale cenią krzyżowo swoją kompetencję.

A czemu terminus tak późno? Bo był na hodowli psów i musiał ubić 3 huskie i wyczyścić Maskaradę.

Dobra, czas iść na hodowlę. Tam coś się dzieje. Tam jest ten artefakt. Tam zamknie się temat upiora.

_Noc: (+1 akcja O)_

Terminus chciałby rozwalić Upiora. Paulina ma znaleźć artefakt. Tyle, że... nie mają dość wiedzy. Więc Paulina ma alternatywny plan - astralika + mentalka + nekro + kata a nośnikiem - melodia, na którą reagowała efemeryda. To powinno dać Paulinie efekt "ciepło-zimno" i znaleźć odpowiedni artefakt. A Gustaw zabezpiecza ich przed atakiem.

Paulina chce znaleźć artefakt. (7: niepozorny-2). Paulina kompensuje niepozorność przez AoE. ST5. bleed energii +1, wiedza od Bogumiła +1, dobór ścieżek i melodii +1. 8+3v5->7. Paulina znalazła artefakt. Niestety...

Artefaktem okazała się ciężarówka do transportu psów. Artefakt SPONTANICZNY. Najpewniej wpadł w Węzeł czy coś. Ponad pół roku. To nowo kupiona ciężarówka. Tu terminus się zafrasował...

* To Twój teren - Terminus
* Nie możemy tego po prostu zniszczyć. Ten człowiek wydał kupę kasy... - Paulina
* Sęp przysługi u Beaty? - Terminus - Albo sama uszkodź artefakt?
* Diabli wiedzą, co się stanie jak go uszkodzę. - Paulina
* Za to ja wiem, co się stanie jak go NIE uszkodzisz - Terminus
* Więcej pogryzionych - Paulina
* Beata czy Ty? - Terminus - Czekaj, przejdę się rozwalić Psa.
* Wiesz jak wygląda Jack Russell? - Paulina
* Wiem jak wygląda upiór - Terminus - No nie chcę rozwalić komuś psa.
* Pomoże Ci to, że jest niebieskawy i świeci - Paulina
* Faktycznie, pomoże - Terminus z krzywym uśmiechem

Paulina wpadła na pomysł, by zrobić Gustawowi przynętę - kij z energią dla upiora. Terminus docenił. 5v3->S. Uzbrojony w przynętę terminus poszedł polować na Psa. A Paulinie zostało wzdychanie do ciężarówki...

Paulina zaczęła badania ciężarówki. Zanim dobrze zaczęła, podszedł do niej strażnik. Młodzik z okolicy. Paulina ma przewagę - on jeszcze nie zauważył, że ona stoi w cieniu i mamrocze. Paulina przygotowała szybko jednosekundówkę, by go zmusić do odejścia i nie zauważania, że Paulina i Gustaw się tu kręcą. "It's all right, go on". 4v1->5. Strażnik poszedł i nie będzie stanowił problemu.

Oki, czas na ciężarówkę. Paulina zaczyna badania. (7, artefakt spontaniczny, artefakt stary). Metodyczne podejście Pauliny rozwiązuje spontaniczność. +1 wiedza od Bogumiła 6v5->SS. Następnego dnia jest wystawa psów i ta ciężarówka jest potrzebna (i +1 akcja). Ale udało się Paulinie dojść do tego, jak ta ciężarówka działa.

* Ż: 3 rzeczy, które Ci pomogą?
* K: jest niestabilne samo z siebie i przecieka energią. Rozładowuje się, ale powoli
* K: not sophisticated. Łatwy byt, by z nim pracować.
* K: tam są aspekty nekromantyczne. Coś, co zna. I ma kanał nekromantyczny.
* Ż: czas na coś podłego z mojej strony. W jaki sposób ciężarówka była świadkiem śmierci dziecka?
* K: dziecko było przejechane przez ciężarówkę
* Ż: i podczas badania Paulina dowiedziała się, kto jest rodzicem. Poznała rodziców. Zupełnie gdzieś indziej. Zwłoki zostały wywiezione; kierowca się nie przyznał.

Przyszedł terminus. Paulina wyczuła erupcję i echo wirującej energii w cieżarówce. Terminus załatwił Psa. Paulina powiedziała Gustawowi, że dziecko zostało przejechane (wraz z psem). Kierowca uciekł. Terminus zauważył, że Paulina nic z tym nie może teraz zrobić - dziecko ani pies nie wrócą. Wpierw trzeba rozmontować ciężarówkę. Paulina chce poinformować rodziców i dorwać sprawcę i wsadzić za kratki.

Paulina powiedziała Gustawowi, że ciężarówce nie może stać się krzywda. JUTRO MA JEŹDZIĆ. Bo Pyżuk potrzebuje jej na jutro. Gustaw spoko. Paulina rozbiera to metodycznie, wie jak to działa, używa nekromancji i puryfikacji, katalizą poszerza to, że samo przecieka. Powiększa dziurę i uziemia, generując quarków. 6v5->S. Udało jej się dezaktywować artefakt oraz wygenerować quarki. Które przeznaczy na to, by rodzice dziecka się dowiedzieli a sprawca by trafił w ręce ludzkiej sprawiedliwości...

**Podsumowanie torów**

1. Chłopiec
    1. Transformacja kobiety w swoją mamę: 1/10
    2. Zbudowanie zabawki mającej przejąć grupę dzieci do siebie: 4/10
    3. Przejęcie kontroli nad Psem: 0/10
    4. Seed wskrzeszenia: 2/10
    5. Zalinkowanie energii z Węzłem: 0/10
2. I jego pies
    1. Egzekucja zwierząt: 2/10
    2. Egzekucja ludzi: 2/10
    3. Zasilenie się by się ustabilizować: 3/10
    4. Wampiryczna stabilizacja: 0/10
    5. Odnalezienie Senesgradu: 0/10
3. Naturalna Entropia
    1. Skażenie okolicy: 0/10
    2. Złamanie Maskarady: 2/10
    3. Panika wśród ludzi: 2/10
    4. Chorzy / ranni dla Pauliny: 3/10
    5. Skażenie Wąskiego go zmieni: 0/10

**Interpretacja torów:**

No change.

**Przypomnienie Komplikacji i Skażenia**:

Pojawił się Bogumił Miłoszept, echo starego lekarza. Najpewniej się rozproszyła gdy Chłopiec weń 'wszedł'.

# Progresja

# Streszczenie

Paulina wyleczyła młodego chłopca ugryzionego przez Upiora. Stworzyła Skażeńca - cień doktora Bogumiła Miłoszepta, który bardzo jej pomógł i pokazał, że magicznie się tu coś zmieniło. Gdy Paulina zauważyła, że walczy z groźnym upiorem, wezwała terminusa jako wsparcie - i umożliwiła terminusowi czyste zniszczenie połowy upiora. Potem znalazła artefakt - ciężarówkę, terminus zniszczył drugie pół upiora a Paulina rozmagiczniła artefakt. Just rezydentka things.

# Zasługi

* mag: Paulina Tarczyńska, rezydentka znajdująca i niszcząca Artefakt Spontaniczny; utrzymała groźnego upiora u siebie w domu do czasu przybycia terminusa i leczyła ludzi.
* czł: Karol Wąski, laryngolog. Próbuje się wydostać z friendzone u Pauliny - ale nie narzucając się. Nadał jej temat upiora z Półdary.
* czł: Jakub Pyżuk, właściciel hodowli psów rasowych husky (jedna z lepszych hodowli). Stracił trzy psy do upiora bo kupił ciężarówkę, która była Spontanicznym Artefaktem.
* czł: Mikołaj Pyżuk, chłopiec ugryziony przez Psa. Nekrotycznie powiązany ze spontanicznym upiorem. Paulina go wyleczyła i wymazała pamięć - bo musiała.
* vic: Bogumił Miłoszept, eteryczny lekarz. Kiedyś wybitny lekarz, teraz przyzwany jako Skażenie pomagające Paulinie. Nie istniał zbyt długo; traktuje Paulinę jako niedouczoną studentkę.
* mag: Beata Obaczna, protektorka Pauliny z SŚ. Magowie się jej boją i jest samotna; lubi mistrza Pauliny. Ściągnęła Paulinie terminusa do pomocy - niestety, to był Gustaw.
* mag: Gustaw Bareczny, antypatyczny i cyniczny terminus o złośliwym poczuciu humoru i tendencją do niszczenia rzeczy. Fajny na swój sposób. Jakoś dogaduje się z Pauliną na swój sposób.


# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne, Rzadka Zabudowa, Osiedle Domków
                    1. Gabinet Pauliny, Wąska Ale Długa Działka, Częściowo Zasłonięta. Niedaleko sąsiad. Ma szopę i ogródek.
                1. Półdara, wieś, gdzie jest wygrywająca hodowla psów.
                    1. Chata Lekarza, gdzie przyjmuje Wąski. Tam też trafił młody chłopak, który potem trafił do Pauliny na ugryzienie upiora.
                    1. Hodowla Psów Husky Pełnodar, gdzie znajdowała się ciężarówka będąca kernelem Chłopca i Psa, oraz gdzie zginęły trzy husky.
                1. Trzeszcz
                    1. Elektronik Suwak
                    1. Klub Nowa Melodia


# Czas

* Opóźnienie: 3
* Dni: 2

# Frakcje


## Chłopiec...

### Scena zwycięstwa:

* Znalazł "swoją mamę" w jakiś sposób i dołączył ją do świata nieumarłych
* Wciągnął grupę dzieci do świata nieumarłych, by się z nimi bawić
* Pies znowu go słucha

### Motywacje:

* "Jestem dzieckiem, chcę się bawić. Dlaczego oni nie chcą się ze mną bawić!"
* "Mają się ze mną bawić!"
* "Mój biedny piesek, czemu on tak przestaje działać?"

### Siły:

* Chłopiec, vicinius (upiór). (15, zasilanie (2), wsparcie Psa (3), dziecko (2), szuka przyjaciół(2))

### Ścieżki:

1. Chłopiec
    1. Transformacja kobiety w swoją mamę:  0/10
    1. Zbudowanie zabawki mającej przejąć grupę dzieci do siebie:   0/10
    1. Przejęcie kontroli nad Psem:     0/10
    1. Seed wskrzeszenia:   0/10
    1. Zalinkowanie energii z Węzłem: 0/10


## I jego pies

### Scena zwycięstwa:

* Chłopiec bawi się z nim i tylko z nim.
* Przestaje się rozpadać i nie musi zasilać Chłopca.
* Asymilacja energii z otoczenia, skończenie Ewolucji.

### Motywacje:

* "To wszystko jest NIE TAK - muszę chronić, odeprzeć, odgonić."
* "Polować. Jak nie poluję, się rozpadam."
* "Chronić Chłopca!"

### Siły:

* Pies, vicinius (upiór). (10, zasilanie (2), wsparcie Chłopca (2), wiecznie poluje (2))

### Ścieżki:

1. I jego pies
    1. Egzekucja zwierząt:  0/10
    1. Egzekucja ludzi:     0/10
    1. Zasilenie się by się ustabilizować:  0/10
    1. Wampiryczna stabilizacja:    0/10
    1. Odnalezienie Senesgradu: 0/10

## Naturalna Entropia

### Scena zwycięstwa:

* brak

### Motywacje:

* brak

### Siły:

* Chłopiec i jego Pies ;-)

### Ścieżki:

1. Naturalna Entropia
    1. Skażenie okolicy:    0/10
    1. Złamanie Maskarady:  0/10
    1. Panika wśród ludzi:  0/10
    1. Chorzy / ranni dla Pauliny:  0/10
    1. Skażenie Wąskiego go zmieni: 0/10


# Narzędzia MG

## Opis celu misji

* Za notatką: [Notatka_170716](/mechanika/notes/notatka_170716.html) zmieniamy kartę Pauliny.
* Konflikty są na (3, 5, 7, 10, 15). Każdy ma [1,3] dominujące aspekty negatywne, które zniwelowane obniżą stopień trudności o '2' każdy.
* Konflikty na [-1,1] (przegrane o 1, remis, wygrane o 1) są CZĘŚCIOWYMI sukcesami. Obie strony osiągają cel lub osiąga się cel niekompletny. To jest próba implementacji mechanizmu z Apocalypse World o równoległości działań postaci; alternatywne interesujące podejście jest w Mouse Guard.

## Cel misji

1. O-O-O-S-O-O-O-S, gdzie O to operacyjne (wzrost ścieżki) a S to komplikacje (nowa ścieżka).
    1. O: przeciwnik dał radę podnieść JAKĄŚ ścieżkę. Jeden lub wszyscy. Generowana jest nowy Postęp Przeciwnika.
    1. S: przeciwnik ma NOWĄ ścieżkę. Jeden lub wszyscy. +1 akcja dla wszystkich przeciwników. Generowana jest nowa Komplikacja Fabularna (wątek).
1. Dzień składa się z 4 faz, domyślnie. Normalne postacie w jednej fazie śpią ;-).
1. Nowy model postaci Pauliny pokaże, że lepiej jest opisana i ogólnie lepiej i szybciej działa.
1. Konflikty 3,5,7,10,15
1. Poziom częściowego sukcesu [-1,1]

## Po czym poznam sukces

-

## Wynik z perspektywy celu

1. Operacyjne komplikacje i brak Strategicznych
    1. K: Brakowało mi KS i mi ich nie brakowało. Nie jest to historia, gdzie to jest konieczne. Nie ma przeciwnika, nie ma świadomej agendy. Nie było to złe - zazwyczaj to wprowadza coś co bardzo się psuje.
    1. Ż: Mogłem ustawić JAKIEŚ Strategiczne, ale nie miało to sensu. Pierwszy raz pojawiłaby się w nocy. A tam to już było za późno. Sądzę, że KS pojawiająca się jedna, tylko w nocy, ma największy sens. W usztywnionych punktach niezależnie od ilości konfliktów. Nie wiem jeszcze gdzie (kiedy).
    1. K: Tak ma to sens. Dla mnie to jest 'asskicker'. Coś ostrego.
1. Cztery fazy dnia
    1. Ż: Moim zdaniem to działa. Wiem co jest kiedy. Co więcej, ta misja była krótka - dlatego w 1 dniu pasuje.
    1. K: Zgadzam się. Tu to pasuje. Kontynuujemy testowanie. I wiedzieliśmy że jest noc.
1. Nowy model postaci Pauliny
    1. K: Myślę, że jest lepiej. Nie czułam braku Zachowań ani Cech.
    1. Ż: Mi się podobało, mogłem walić w minusy. Bez tego by nie było tej historii z dzieckiem. 
    1. K: Wymaga to małej przebudowy Pauliny; nowy aspekt tego jest taki, że te konflikty nie były bardzo trudne, ale...
    1. Ż: Konflikt na (10) lub (15) jest bardzo trudny.
    1. K: Nawet na (7) jest trudny. Przy obecnej karcie mam '6' z karty. '7' jest bardzo trudny.
    1. Ż: I tak ma być.
1. Konflikty 3,5,7,10,15 i kontraspekty
    1. K: BARDZO pomocne są kontraspekty, problem w tym, że tak jak one są... będą rozbijać immersję, chyba, że będą w miarę oczywiste.
    1. Ż: Nie nazywać aspektów a opisać by były widoczne.
    1. K: Pomoże. Acz ja czasem nie mam pomysłu.
    1. Ż: Jeszcze.
    1. K: Ta sprawa ze strażnikiem... człowiek vs mag nie miał szans. A teraz ma. Nie jestem przekonana, czy tak ma być.
    1. K: Mogą być za wysokie wartości wobec karty.
1. Częściowy sukces
    1. K: Mieszane uczucia. Do pewnego stopnia częściowy sukces to komplikacja fabularna.
    1. Ż: Nie. Po pierwsze, druga strona nie dostaje akcji. Po drugie, Tobie się udaje. To redukuje rerolle i DLATEGO było mniej akcji po stronie przeciwnika.
    1. K: To jest tak, że Paulina chciała zbadać Skażenie i skończyła z Upiorem w domu. Co?
    1. Ż: Chciała sięgnąć do przeciwnika a przeciwnik chciał sięgnąć do niej. Zadziałało. Przeciwnikowi się to NIE opłacało.
    1. K: Tylko dlatego, że kombinowałam. Jakbym spróbowała uciekać, bardzo by mu się to opłacało.
    1. Ż: Istniało wyjście i je wymyśliłaś. Nie wzbogaciło to misji? Nie sprawiło, że Paulina jest bardziej Pauliną? Historia nie jest lepsza?
    1. K: Historia JEST lepsza, ale... Paulina nie jest dobrym wyborem na rezydenta. Pakuje się w kłopoty w które nie musi.
    1. Ż: To jest Paulina. Dlatego wpakowała się w Dracenę. Taka JEST.

    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
