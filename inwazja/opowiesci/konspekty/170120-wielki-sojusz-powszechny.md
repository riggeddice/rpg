---
layout: inwazja-konspekt
title:  "Wielki sojusz powszechny"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170113 - "Dzisiaj złapiemy Nicarettę!" (HS, KŁ)](170113-dzisiaj-zlapiemy-nicarette.html)

### Chronologiczna

* [170113 - "Dzisiaj złapiemy Nicarettę!" (HS, KŁ)](170113-dzisiaj-zlapiemy-nicarette.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Piękny poranek - dla niektórych. Henryk obudził się w średnim humorze. Ale ma plan...

Wpierw zadzwonił do Henryka Hubert Kaldwor. Chodzą za nim policjanci i to mu zupełnie nie pasuje. Zaprosił do siebie Henryka; chce porozmawiać. Henryk pamięta, że to Kaldwor dał mu ostatnio wykorzystanego Drabona... cholera. Nic to, ważniejszy jest temat Nicaretty i Marzeny...

Henryk zadzwonił do Witolda Małka, księdza z Kulturna.

* Halo? - ciepły męski głos Witolda
* Szczęść Boże, Henryk Siwiecki z tej strony - Henryk bada reakcję
* Ach... tak... - zakłopotanie i niechęć.
* Nie wiem pod jakim imieniem ksiądz ją zna, ale chciałbym się spotkać i omówić warunki rozejmu z Grażyną Diadem. - Henryk, z pewnym optymizmem
* Nie wiem czego ksiądz ode mnie oczekuje, ale nie chodzę do agencji... - Witold, z nieukrywanym obrzydzeniem
* Księże Witoldzie, proszę nawet nie insynuować... - Henryk

Witold zareagował skrajnie ofensywnie. Henryk powinien być dobrym, lepszym od przeciętnego księdza. A zachowuje się... nie. Po prostu nie. Witold nie chce mieć NIC wspólnego z Henrykiem. Cóż, Henryk użył Oczu Kościoła (-1 surowiec). Nie życzy sobie takiego zachowania... Witold ma 4+1. Henryk ma 3+2 -> F (Witold naskarżył biskupowi), S. Witold się przestraszył; Henryk coś na niego ma - Witold kiedyś miał w przeszłości... schadzki z parafiankami. Witold spokorniał.

* Dobrze... rozumiem... to w czym chciałem, bym Ci pomógł? - Witold, łagodniej
* Przekaż informacje. Sam wiesz, komu. - Henryk
* Nie wiem, naprawdę. - Witold.
* Dobrze... to co mam jej przekazać? Tej Grażynie Diadem? - Witold, ogłupiały (wie, że to prostytutka. Nie jest to już Nicaretta) - Henryku, WIESZ, że to prostytutka, prawda?
* Można tak powiedzieć... - Henryk
* Dobrze. Przekaż jej, że nie musimy się wyniszczać nawzajem; możemy omówić warunki pewnego rodzaju współpracy i chciałbym się spotkać na neutralnym gruncie bez świadków gdzie możemy porozmawiać i dojść do konstruktywnych wniosków... - Henryk, nie widząc wieloznaczności
* Dobrze, bracie. Przekażę... - Witold, podłamany, myśląc, że przynajmniej TA jest pełnoletnia...

Karina: -1 surowiec. Szuka informacji o Marzenie. Chce ją zlokalizować. Konflikt. Marzena: 4+3. Redefinicja konfliktu: szukamy OBSZARU. Konflikt 4 vs 1+3 -> 15. Karina ma obszar, gdzie operuje Marzena z Siostrami Światła. 

Henryk, cały "rozpromieniony", pojechał do Huberta. A ten... zły.

* Henryku. Gliny mam na ogonie. A podobno ty się z nieletnimi bawisz... - Hubert do Henryka z dobrotliwym uśmiechem
* No wiesz... plotki. Słuchaj, ta sytuacja z księdzem... to był problem, głupio wyszło... - Henryk, zakłopotany
* Nic nie mów. - Hubert - Zajmę się tym
* Zależy co masz na myśli... - Henryk - Z księdzem dam radę, nie ma problemu. Tylko tych dwóch prowincjonalnych oficerków...

Konflikt. Hubert ma to gdzieś. On chce rozwalić tamtego księdza... KONFLIKT O KLEPICZKA! Henryk przytakuje, zgadza się... ale tak naprawdę kieruje Huberta na inne rozwiązanie. Np. "ja wiem, że to był pomysł WITOLDA, księdza z Kulturna! I to ON jest pedofilem!". I Hubert ma wierzyć, że to jego pomysł i że on to wymyślił... czyli 6v4. Remis; Hubert PODEJRZEWA, że Klepiczek x Henryk...

* "Klepiczek nawet nie wie jak mu dupsko ratuję..." - Raynor
* "To chyba najbardziej konfliktowany NPC w okolicy..." - Kić
* "Tylko u lokalnego mafioza Henryk ma opinię geja. U innych - pedofil. No nie przesadzajmy......" - głęboko załamany Raynor

Henryk wrócił do siebie, czekając na wiadomość od Witolda. I dostał odpowiedź. Witold powiedział, że Grażyna nie wygląda jakby wiedziała o co chodzi, ale zgodziła się spotkać jeśli Henrykowi zależy. Oślarz, przysiółek Lenartomina. Wieczorem. Henrykowi spodobał się pomysł; poszedł z nim do Kariny.

* Jest do załatwienia pewna rzecz. Sojusz z sukkubem - wystrzelił Henryk do Kariny
* Okej... - zaskoczona Karina - Spieszę przypomnieć, że jestem archiwistką i bardem
* Grunt, byś sprawiała wrażenie NIEarchiwisty. - Henryk, chytrze
* Tego typu demon potrafi blefy przenikać - Karina

Henryk opowiedział Karinie sytuację jak to wynikało z Nicarettą do tej pory. I na ten moment bardziej przyda się Nicaretta sojusznik niż przeciwnik. Plus, mają taką historię między sobą i jest przekonana, że może lepiej nie próbować. Karina zauważyła, że NA PEWNO spróbuje...

* De facto chcesz ją przekonać, że nie ma co podejmować ryzyka. A w tym mogę Ci pomóc - Karina, z uśmiechem
* Tu nie chodzi o to, by jej pokazać 'przegrasz i będziesz mi służyć'. Chodzi o to, że w starciu nie będzie remisu. Ryzyko lub sojusz. Synergia. - Henryk, nieco chaotycznie
* Brzmi całkiem sensownie
* Tylko trzeba jej dać coś...
* Na przykład odrobinę informacji - uśmiechnięta Karina
* Może zapewnić jej środowisko żerowania, niech sobie robi co chce - moralny ksiądz jest moralny

...?

* Podpowiem Ci. Masz tu bardkę z umiejętnością "propaganda" - Kić
* Ja też tu mam... uuuu, ten sukkub będzie nasz - Raynor

Nicaretta na pewno robi research. Taki z niej sukkub. Więc Karina i Henryk zostawili wskazówki, by widziała, że mimo, że ten teren jest neutralny... na przykład, Henryk może ściągnąć większe siły. Niech myśli, że atakowanie Henryka jest zbyt ryzykowne. Oki, WYGRA. I? Pyrrusowe zwycięstwo.

Konflikt na wzbudzanie szacunku (inspiracja) Nicaretty "chcę tego sojuszu". Nicaretta: megalomanka, oficer, Friendly, +2 historia +1 taktyczna sytka -> 7 Karina: propaganda, artystka opowieści, survivor Nicaretty, Henryk jako narzędzie -> 9. Sukces. Nicaretta chce sojusz z nimi. Ale wie, że ma przewagę taktyczną.

Wieczór. Oślarz. Na jakimś polu. Henryk i Karina pierwsi. 

Nicaretta przygotowała się "death from above". Na spotkanie przyszła Grażyna Diadem, zgodnie z rozkazem Nicaretty. Dokładniej, przyjechała autem z kierowcą (kierowca odjechał). Karina przygotowała sobie syberyjskie zabezpieczenia (survival syberyjski) na wypadek gdyby ktoś się zbliżał. (3v5->R).

Henryk i Karina czekają, przyjechał samochód i wyszła Grażyna. Samochód odjechał. Grażyna podchodzi powoli, z rękami na widoku, bez ostrych ruchów. Poczekali aż podejdzie.

* Przybyłam - Grażyna
* Witaj. To pierwszy raz, jak mamy okazję porozmawiać. - Henryk

Karina wyczuła, że nie ma do czynienia z Nicarettą. Grażyna jest agentką sukkuba, ale nie jest sukkubem. Powiedziała Grażynie, że wie, że nie jest Nicarettą. Death from the above; Nicaretta wykorzystała potęgę grawitacji by obsypać srebrnym pyłem Karinę i Henryka. Karina uniknęła ataku.

Nicaretta i Zespół zaczęli negocjacje. Nicaretta nie wierzy w sojusz; chce sprawdzić czy ich intencje są czyste. Karina nie zgadza się na czytanie intencji. Nicaretta jest niepewna, nie wie na czym stoi. Udało im się w końcu zawrzeć ten sojusz (konflikt -> F, R). Ale wszyscy mają prawo się wycofać w sytuacji kryzysowych. Plus, Nicaretta ma Małka jako swojego cennego miniona (a Hubert podkłada mu pedofilię).

Sojusz został przypieczętowany syntezą aury; to powoduje, że Nicaretta fizycznie nie jest w stanie go zerwać a jeśli Henryk lub Karina spróbują, to Nicaretta będzie wiedzieć. Dodatkowo, Nicaretta wyczuła w Karinie ślady Magii Krwi i uznała je za działanie wrogie przeciwko Karinie; zgodnie z sojuszem jest zobowiązana to naprawić.

I to zrobiła.

Gorący całus w Karinę. Potęga Arazille. Kinglord nic nie wie. Moc Krwi na Karinie się rozsypała. Kinglord stracił na nią hold (ale może odzyskać). Nicaretta powiedziała, że obroniła na podstawie kontraktu Karinę; Henryk od razu skonfliktował z Kariną.

Henryk: F, S -> polubił Karinę i zaczął się o nią troszczyć. Ale udało się. Karina powiedziała, że mistrz ma nad nią władzę... a Nicaretta rozbiła tą władzę. Karina przybliżyła się do Nicaretty i ją objęła. Nicaretta wyjaśniła (za zgodą Kariny) - Karina jest uszkodzona. Była kontrolowana magią krwi. A ona mocą Arazille to rozmontowała.

* Sojusz się dobrze zaczyna. Rozjebaliśmy Ci pomnik i Twój ksiądz jest pedofilem. - Raynor, z sarkazmem, odnośnie informacji potencjalnie do Nicaretty

Nicaretta po usłyszeniu o Kinglordzie powiedziała, że Arazille może ich uratować. Karina przerażona - Arazille to oddanie się pod kolejną kontrolę, KOLEJNEGO boga. Henryk przerażony, przy jego tendencjach Arazille to śmierć. No i Nicaretta usłyszała:

* Posąg Arazille... zniszczony.
* Krew Henryka u Kinglorda.
* Kinglord superdefiler na nich czyha.
* I nie da się uciec w Bieszczady.

Ogólnie, opracowali NOWY plan. Plan by jeszcze dodać Marzenę...

...

Jest NOC. Karina przechodzi się przez Krompy, wstrząśnięta i lekko zakrwawiona. Marzena dostała info. Konflikt: Marzena 3+1, Karina 2+2 -> F, F, S (Marzena ma wsparcie, przekazanie krwi Kinglordowi się uda i to wyjdzie na końcu tej misji). Marzena z obstawą, pokonane przez Nicarettę i Karinę. Pięć dziewczyn nieprzytomnych; krew Marzeny dyskretnie u Kariny.

Sebastian Drabon przyjechał zawieźć nieprzytomne do Klepiczka. Za to Hubert dostanie lasencję roku.

U Klepiczka, na plebanii. Nicaretta rozwiązała problem z Klepiczkiem zauraczając go. No i Marzena dobrze związana w sypialni księdza (ale ubrana). Nicaretta i Karina czekają na jej obudzenie się. 

Karina i Nicaretta wyjaśniły Marzenie problem z Kinglordem. 

* Mój pan tu przyjdzie... - Karina, pusto
* Nie masz pana - Marzena, z uczuciem
* Uwierz, mam - Karina

Marzena zgodziła się na sojusz, łącznie z nieagresją. Potem, w prywatnym obszarze, Karina przyznała Nicaretcie, że krew Marzeny też trafiła do Kinglorda. Sukkub, lekko zrezygnowana, powiedziała, że lepiej jej nie mówić...

I trzeba jakoś żyć dalej.

Obserwacje z sesji:

* To... to jest najgorszy sojusz, najgorszy blef i fatalny kontrakt... - załamana Nicaretta
* Ten demon... i jeszcze z Henrykiem pracuję. Przynajmniej JA jestem bezpieczna - podłamana Marzena
* On ma moją krew... ale ja mam sukkuba! I całkiem niezły sojusz (nie licząc Marzeny) - Henryk
* Wolność! Ale on i tak wróci... fml - Karina

# Progresja

* Henryk Siwiecki: zaczęło mu troszkę zależeć na Karinie
* Kinglord: dostał krew Marzeny Dorszaj
* Henryk Siwiecki: pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Nicarettą
* Karina Łoszad: pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Henrykiem, Nicarettą
* Nicaretta: pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Henrykiem
* Marzena Dorszaj: pakt o nieagresji z Henrykiem, Kariną, Nicarettą

# Streszczenie

Henryk postanowił wejść w sojusz z Nicarettą; Karina mu pomogła. Przez pakty nieagresji i współpracy Nicaretta uwolniła Karinę (tymczasowo) od władzy Kinglorda. Słysząc te rewelacje, Henryk dołączył do sojuszu Marzenę (po jej złapaniu przez Nicarettę z Kariną). Nicaretta ma kilka pomysłów (przede wszystkim dookoła Arazille), ale te nikomu się nie podobają...

# Zasługi

* mag: Henryk Siwiecki, zawiązał sojusz życia: Nicaretta, Karina, Marzena. Opinia 'zainteresowanego nieletnimi' zaczyna krążyć. Mistrz logistyki Hubertem. Też: zwala winę na INNYCH księży.
* mag: Karina Łoszad, wolna i w to nie wierząca, w sojuszu z Nicarettą, Henrykiem, Marzeną. Zastawiła pułapkę na Marzenę i przesłałą jej krew Kinglordowi. Też: mistrzyni propagandy.
* czł: Hubert Kaldwor, którego zirytowało to, że przez Henryka trafili na niego policjanci; w ramach zemsty na księżach wsadził Witoldowi Małkowi pedofilskie treści.
* czł: Witold Małek, ksiądz w Kulturnie. Agent Nicaretty. Hubert podłożył mu pedofilskie treści. Punkt kontaktowy Henryka z Nicarettą; zastraszony przez Henryka.
* czł: Sebastian Drabon, drab. Zgarnął nieprzytomne dziewczyny z Kromp (łącznie z Marzeną). Powoli ulubiony drab Henryka.
* czł: Grażyna Diadem, prostytutka i dawny host Nicaretty; zafascynowana Nicarettą i dywersja dająca Nicaretcie okazję do ataku.
* vic: Nicaretta, okazała się świetnym taktykiem, twardym negocjatorem i niezłym wojownikiem. I tak przegrała - weszła w niekorzystny tymczasowo sojusz. Nawet Arazille nie pomoże...
* mag: Marzena Dorszaj, chciała pomóc "zakrwawionej dziewczynie", skończyła związana. Gorzej, skończyła w sojuszu z Henrykiem, Kariną i Nicarettą...
* mag: Kinglord, który dostał krew wszystkich magów w rejonie Wyjorza - Henryka, Kariny, Marzeny. Nie ma jak przegrać. Poza tym, że przeciw niemu jest sojusz. Jak napisałem, nie ma jak przegrać.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Wolny
                1. Oślarz, przysiółek koło Lenartomina; spotkanie z Nicarettą
                1. Krompy, mała mieścinka koło Lenartomina; znajduje się tam Marzena i jej baza
                1. Lenartomin, centrum dowodzenia Sojuszu, w kościele
                    1. Kościół, gdzie doszło do finalnego przypieczętowania Sojuszu.
            1. Powiat Tonkij
                1. Wyjorze, poprzednie centrum dowodzenia Henryka.
           
# Czas

* Dni: 1