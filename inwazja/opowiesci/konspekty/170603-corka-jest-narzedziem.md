---
layout: inwazja-konspekt
title:  "Córka jest narzędziem?"
campaign: corka-lucyfera
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170530 - Córka Lucyfera (HS, Kić(temp))](170530-corka-lucyfera.html)

### Chronologiczna

* [170530 - Córka Lucyfera (HS, Kić(temp))](170530-corka-lucyfera.html)

## Koncept wysokopoziomowy misji

Psinos 2.0. Tu się zaczyna.

## Kontekst ogólny sytuacji

W okolicy pojawił się "biegający wirtualny ksiądz Siwiecki". Niestety, Luksji udało się sformować potężny kult satanistyczny, na odpowiednim poziomie jaki jest jej potrzebny. Już widoczna część potrzebnej Luksji populacji zmieniona została w thralle. Sama natura tych thralli jest jeszcze nieznana.

* Dzieci biegające za wirtualnym księdzem
* Osłabienie Pryzmatu miejsca
* Osłabienie satanizmu w tej okolicy

Ksiądz Zenobi Klepiczek jest zabójczo zakochany i zafascynowany Estrellą.

## Punkt zerowy:

* Ż: Gdzie znajduje się główna siedziba satanistów?
* K: Siedziba firmy - min. wynajmują sale na zlot satanistów. Wytwórca czosnku granulowanego. Firma zwie się Granuleks.
* Ż: Gdzie - w eleganckim miejscu - zwykle pojawia się Luksja? 
* R: Ekskluzywny klub nocny dla bogatych ludzi o nazwie Czosnekolada. Historycznie sięga do założenia miasta. Kiedyś - spotkania z branży czosnkowej, potentaci. Wymaga wpłaty członkostwo.
* Ż: Kto może Luksji pomóc w odbudowie detektorów potencjalnych thralli?
* K: Miejscowy protomag. Luksja może użyć jego układu nerwowego... aha, nikt - łącznie z Luksją - nie wie, kim jest ten protomag.
* Ż: Gdzie i czemu mogą nasi magowie się bezpiecznie zamelinować?
* R: W biednej dzielnicy - Fabrycznej - która jest raczej antysystemowa.

Żółw_cel: Mamy jakoś określone miasto i kontekst startowy postaci.

* Ż: Jedno zdanie o postaci alfonsa Raynora. Nazwij swoją damę lekkich obyczajów.
* K: Spory przedsiębiorca branży w tym mieście; wobec konkurencji bezwzględny, ale o swoich dba. Kombinator, zna większość wierchuszki. Bo im dostarcza. A moja: Stella.
* Ż: Jedno zdanie o postaci damy Kić. I jak alfons się nazywa?
* R: Wyszczekana, przedsiębiorcza, pragmatycznie podchodzi do zawodu. Nie poszła z konieczności ale z kasy. Angażuje się w kontakty w pracy. A moja: Misza.
* Ż: Czemu alfons wysoko sobie ceni Twoją postać? A nawet lubi?
* K: Angażująca się to plus. A jak klient nie zapłaci, sama sobie weźmie. A zwykle jest w stanie namówić klientów na zwierzenia co daje alfonsowi sporo informacji.
* Ż: Gdzie, mniej więcej, doszło do spotkania? I co tam robiliście?
* R: W Czosnekoladzie ogarnialiśmy ważnego klienta. Trzeba było się pojawić z najlepszą pracownicą i osobiście to rozwiązać... i pierwszy raz Misza tam wszedł. Biedny.

Żółw_cel: To da nam pierwszą scenę startową do pkt 0.

**Scena startowa:**

Spotkanie Henryka Kantosza z Miszą i Stellą. Henryk chce wyleczyć syna z homoseksualizmu. Misza zaproponował podwójną kurację - wpierw "Iwan Groźny" na smutek, potem Stella na osłodę. Kantoszowi się to spodobało. Weszła Luksja i zepsuła zabawę. Henryk Kantosz zwiał.

Luksja zrobiła demonstrację siły. Zachowanie Stelli jej się spodobało; stwierdziła, że Misza będzie lepszą ofiarą niż Stella. Stella przekonała Luksję, że Misza dobry biznesmen i znajdą lepszą ofiarę. Luksja powiedziała, czego chce - szuka żywotnej, energetycznej ofiary która ma potencjał cierpienia. Na jutro.

Misza załatwił. Wraz ze Stellą współuczestniczyli w plugawym rytuale (geas od Luksji); w wyniku tego jeden z satanistów zmienił się w demona (gwałty, poświęcenie biednej dziewczyny, krew na satanistę, inkarnacja katalistyczno-mentalna). Luksja powiedziała, że usługi Miszy i Stelli będą jej dalej potrzebne. A demon posłuży do rozwiązywania problemów w mieście tam, gdzie Luksja spotka się z brutalną siłą.

Stella i Misza będą współpracować.

* Ż: W jaki sposób działania Miszy są w miarę niewykrywalne w mieście - acz może do nich dojść Estrella lub Henryk?
* R: Estrella szuka dalej. Nie tylko w swojej okolicy. Znikający ludzie - zwłaszcza dziewczyny - dają ważny sygnał.
* Ż: W jakim miejscu w mieście znajduje się wyraźny sygnał magii? I jaka to sfera?
* K: Magia transportu. Misza dostarcza GDZIEŚ a ona ściąga. Chatka leśniczego w pobliżu.
* Ż: Kim, poza byciem satanistką, jest Janina Jasionek?
* K: Lokalna bibliotekarka i satanistka; pokojarzyła jakieś historie. Wie coś ważnego o Luksji czego nie jest w stanie znaleźć Estrella.
* Ż: W jaki sposób ona może Wam pomóc?
* R: Otwiera nam kilka drzwi. Plus, jest w środku - może nam mówić jak wygląda sytuacja w środku.
* Ż: Co stanowi główną rzecz irytującą Luksję jak chodzi o konfrontację magiczną z Wami?
* K: Ona bardzo nie chce brać udziału w bezpośrednich akcjach, bo musi być "magicznie czysta", nieskażona, by rytuał się udał. Każdy czar odsuwa w czasie rytuał.
* Ż: Jakie cechy przestrzeni Storczyna sprawiają, że da się wyselekcjonować grupki satanistów?
* R: Miasto nie jest zbyt gęste. Jest możliwe stawianie barier płoszących. Teren pozwala na takie operacje.

Żółw_cel: Zbudowanie wsparcia kontekstowego.

**Ścieżki Fabularne**:

* **L_Konwersja satanistyczna**:        5/5 (tier 2 DONE)
* **L_Transformacja w thralle**:        3/6 (tier 1)
* **L_Przygotowanie artefaktu**:        0/4
* **L_Zdobycie władzy politycznej**:    0/6
* **L_Skażenie magią**:                 2/12
* **L_Anarchia w mieście**:             1/12
* **L_Uruchomienie Artefaktu**:         0/10
* **Tor zauroczenia księdza**:         10/10 (tier 3)

## Misja właściwa:

**Dzień 1:**

Stan początkowy:

* Dzieci biegające za wirtualnym księdzem
* Siły satanizmu są naprawdę duże w tej okolicy

Dobra. Czas zacząć to rozkładać. Trzeba iść do tych komputerów i zrobić akcję pozytywną. Odwrócić stare zaklęcie złożone przez siły Luksji tak, by one rozprzestrzeniały teraz dobro i antysatanizm... trzeba naprawić ten cholerny Pryzmat. Zrobić pozytywną agitkę. W sumie... Luksja na pewno wierzy, że Klepiczek jest pod jej kontrolą. To znaczy, że jest możliwość... złapania jej w pułapkę. Bo Klepiczek robi jakieś dziwne rzeczy.

Super plan!

Ale wymaga dowiedzenia się czegoś więcej odnośnie Luksji, jej zainteresowań, zwyczajów itp. 

Dwójka dzieciaków (Filip i Kalina) zgubiła wirtualnego księdza. Nie są w stanie go dogonić. Szukają go smętnie po mieście. Siwiecki (prawdziwy) wsiada na rower i próbuje znaleźć młodych w mieście. (1 akcja). Jedzie rowerkiem po mieście, co prawda dzieciaków nie widzi, ale zauważył, że taki spory kucharz się nim zainteresował (5v2->S). Przejechał na drugą stroną ulicy; chce, by nikt go nie zauważył. 3v2->F. Nie odwrócił uwagi. WYCIĄGNĄŁ NOGI. Leci za Siwieckim. A Siwiecki na rowerku, w długą!

Siwiecki zasuwa jak leci, nieważne co się nie dzieje. Yolo! 4+2v3->R. Udało się pokonać kucharza... ale z naprzeciwka biegnie drugi Siwiecki joggingman a z boku stoją dwa dzieciaki. I patrzą ogłupiałe. Siwiecki ściągnął ich na siebie. Wieje; Filip wyjął komórkę i zaczął gadać. Henryk odwracając się i manewrując rzucił czar mentalny 1sek "rzuć to". Nie ma czasu. 0v2->F(KF:13), F(KF:14), F(KF:14)

* KF: Wzrost znaczenia miejsca X: coś bardzo ważnego dzieje się w Czosnekoladzie.   +1 akcja
* KF: Wewnętrzny wróg: z jakiegoś powodu Inna Strona chce pozbycia się grupy thralli z miasta.  +1 akcja
* KF: Wewnętrzny wróg: akcja umożliwi pokazanie potęgi sił ciemności neutralnym.   +2 anarchia

Siwiecki się przewrócił rzucając czar; czar zapewnił mu bezpieczeństwo, ale wywołał randomowe efekty. Siwiecki wpadł w truskawki. Otrzepał się, wsiadł na rower i pojechał dalej. Za nim - lecą thralle. Koło 13. Henryk ucieka przed thrallami. Rzucił za siebie srebrne potykacze (-1 surowiec) - tak, by ich stopy pouszkadzać i zmniejszyć ich ilość i jedzie na rowerze jak pro. 4v4->F(KF:6), S.

* KF: Zainteresowanie Nowej Strony: Lucyfer zainteresował się sytuacją. Widzi co tam się dzieje. +1 akcja

Siwiecki jedzie na rowerze LIKE A PRO, rozsypuje poświęcone potykacze, zostawia thralle w tyle. Na habicie zmasakrowane truskawki. Estrella próbuje pokryć WSZYSTKICH korą. Niestety, zaklęcie jest zwykle ścisłe a Siwiecki ich rozprasza potykaczami. I (-1 surowiec) za nasiona. 6+2v3+3>S.

Estrella unieruchomiła chmurą zarodników grupę thralli. Estrella i Henryk dorwali sześciu. Dali Henrykowi chwilę, by mógł dojść do siebie (+1 akcja) i Henryk zaczął ekstrakcję wiedzy z thralli. A Estrella dodała do tego agregację swoich informacji z tego co oni wiedzą, infomancja. Potem rozpoczęli badania. Czym oni są. Po co oni są. Jaką pełnią funkcję. Estrella: 6. Henryk około 5. W sumie: 8. Z magią 9 -> 10.

Zajęło to sporo czasu (+2 akcje), ale sporo się dowiedzieli:

* Dowiedzieli się wszystkiego, co wiedziały tempki na obu misjach. Łącznie z demonem.
* Thralle goniły Siwieckiego, bo Siwiecki potencjalnie byłby świetnym thrallem. Mieli dostarczyć Siwieckiego do chatki leśniczego.
* Dowiedzieli się, czym jest rytuał: Psinos 2. Pryzmat z niedotkniętych magią satanistów, Energia z thralli. Kaskada śmierci. Super-wzmocnienie Luksji.
* Słabe punkty Luksji:
    * Luksja WIERZY. W Lucyfera. Szczerze w niego wierzy.
    * Thralle zauważyły, że czasem na ciele Luksji są ślady biczowania. Ktoś ją bije.
    * Luksja jest niedouczona; ktoś ją edukuje okazjonalnie, ale ogólnie nie jest najlepszą czarodziejką.
    * Luksja z początku była niechętna złemu zachowaniu wobec thralli i ludzi, ale z czasem jej przechodzi. Jej Skażenie Krwią jest za niskie; niższe niż powinno być. Jakieś... tarcze?
* Luksja nie byłaby w stanie sama osiągnąć tego planu i go wymyśleć. Jest ktoś jeszcze...

**Ścieżki Fabularne:**

RAN 6 akcji +2 anarchia: 2,3,5,5,5,5: 1,1,1,0,1,1

* **L_Konwersja satanistyczna**:        5/5 (tier 2 DONE)
* **L_Transformacja w thralle**:        3/6 (tier 1)
* **L_Przygotowanie artefaktu**:        1/4
* **L_Zdobycie władzy politycznej**:    1/6
* **L_Skażenie magią**:                 2/12
* **L_Anarchia w mieście**:             6/12 (tier 2 DONE)
* **L_Uruchomienie Artefaktu**:         0/10
* **Tor zauroczenia księdza**:         10/10 (tier 3)

**Interpretacja Ścieżek:**

Poziom samowoli i anarchii w mieście się ostro wzmocnił; pomiędzy satanistami, thrallami, Luksją i dziwnymi rzeczami które się dzieją jest poczucie, że nikt nie kontroluje sytuacji. Siły satanistyczne okrzepły i są potężną siłą w okolicy. Postępy Luksji się nieco zatrzymały między wszystkimi innymi rzeczami, które czarodziejka próbuje zrobić.

Luksja WIE, że Siwiecki jest magiem. Nie wie jakim i gdzie jest. Było ich dwóch. Ma jednak demona do pomocy.

Tymczasem do akcji wkraczają dwie dodatkowe siły - Lucyfer oraz przedstawiciel władz, bardzo próbujący się wykazać...

# Progresja



# Streszczenie

Estrella i Henryk złapali grupę thralli Luksji i poznali cel rytuału - poświęcenie sporej populacji miasta by wzmocnić Luksję. Odkryli też, że za Luksją ktoś stoi, ktoś dużo sprytniejszy i lepiej uczony od niej. Jakkolwiek Luksja nie posunęła się do przodu w planach, anarchia w mieście rośnie i najpewniej nowe siły podniosą rękawicę by coś zrobić ze Storczynem.

# Zasługi

* mag: Henryk Siwiecki, fatalnie jeździ na rowerze i ogólnie ma pecha. Ale! Uciekł thrallom, wystawił ich Estrelli i dowiedział się sporo o całym problemie.
* mag: Estrella Diakon, zaczajona w krzewach wśród czosnku by ratować Henryka przed grupką thralli Luksji. I rowerem. Dowiedziała się sporo o całym problemie.
* mag: Luksja Pandemoniae, silnie wierząca w Lucyfera, wyraźnie przez kogoś źle traktowana i morderczo niebezpieczna. Stworzyła demona do pomocy.
* czł: Henryk Kantosz, bogacz; chce wyleczyć syna z homoseksualizmu i przyszedł z tym do Miszy. Luksja wygoniła go; wiał jak zając.
* czł: Misza Dobroniewiec, alfons; napyskował Luksji i dopiero Stella uratowała go przed strasznym losem. Znajduje ofiary dla Luksji; dzielnie współpracuje.
* czł: Stella Stellaris, dama lekkich obyczajów; uratowała Miszę i wspiera go współpracując pod przymusem z Luksją. Nie będzie mogła spojrzeć w lustro.
* czł: Janina Jasionek, o której pochodzeniu i szczegółach dowiadujemy się w Pytaniach; okazuje się, że wie o Luksji coś, czego nie wie nikt inny.
* czł: Kalina Cząberek, thrall Luksji; wraz z bratem wpadła w ręce Henryka i Estrelii. Wiecznie goni Siwieckiego, bez nadziei.
* czł: Filip Cząberek, thrall Luksji; wraz z siostrą wpadł w ręce Henryka i Estrelii. Wiecznie goni Siwieckiego, bez nadziei.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubuskie
            1. Powiat Szarczawy
                1. Storczyn Mniejszy
                    1. Luksusowy
                        1. Czosnekolada, Ekskluzywny klub nocny dla bogatych ludzi. Historycznie sięga do założenia miasta. Kiedyś - spotkania z branży czosnkowej, potentaci. Wymaga wpłaty członkostwo. Pojawia się tam czasem Luksja.
                    1. Fabryczny
                        1. Granuleks, siedziba firmy - min. wynajmują sale na zlot satanistów. Wytwórca czosnku granulowanego. Aha, centrala satanistów.
                    1. Lasek
                        1. Chatka leśniczego, gdzie ściągane są dziewczyny przez Miszę i przesyłane przez Luksję. Świeci magią transportu.
                    1. Pola
                        1. Pola czosnku, gdzie Estrella i Henryk złapali i unieszkodliwili goniące Henryka thralle.

# Czas

* Dni: 1

# Frakcje:


## Córka Lucyfera

### Koncept na tej misji:

Młoda dziewczyna z thrallami, próbująca doprowadzić do zwiększenia poziomu swojej potęgi z rozkazu "Lucyfera".

### Scena zwycięstwa:

* Miasto spowija krwawa poświata; dochodzi do rytuału transcendencji.
* Sama Luksja absorbuje energię miasta, stając się efektywnie najpotężniejszą czarodziejką w okolicy.
* Lucyfer będzie dumny ze swojej kochanej córeczki.

### Motywacje:

* "Mój ojciec będzie ze mnie bardzo zadowolony."
* "Stanę się najpotężniejszą czarodziejką na całym świecie."
* "Na pohybel kościoła i Boga. Mój ojciec zwycięży."

### Siły:

* Luksja Pandemoniae, czarodziejka: mentalistka, biomantka, infomantka, katalistka, Krew.
* Łukasz Tworzyw, najwyższy kapłan-satanista, w służbie Luksji.
* Sataniści Łukasza, duża grupa lojalnych satanistów.
* Thralle Luksji, Zmienieni w odpowiedniki Krwawców, ale przekaźniki energetyczne.

### Wymagane kroki:

* Skonwertowanie odpowiedniej ilości ludzi na satanizm: 5
* Transformacja indukcyjna odpowiedniej ilości ludzi w thralle: 6
* Zdobycie władzy politycznej: 6
* Przygotowanie Artefaktu Psinoskiego: 4
* Uruchomienie Artefaktu Psinoskiego: 5

### Ścieżki:

* **Konwersja satanistyczna**: 2,3: 
* **Transformacja w thralle**: 2,2,2: 
* **Przygotowanie artefaktu**: 2,2: 
* **Zdobycie władzy politycznej**: 2,2,2: 
* **Skażenie magią**: 3,3,3,3: 
* **Anarchia w mieście**: 3,3,3,3: nikt nie kontroluje, co się dzieje; 
* **Uruchomienie Artefaktu**: 5,10: 


# Narzędzia MG

## Cel fabularny misji

Otwarcie misji z "Córką Lucyfera", dla Henryka, by można było określić kim jest ta postać. To jest, Henryk. Nie Luksja.

## Cel techniczny misji

1. Weryfikacja nowego podejścia do magii - umiejętności + magia + wspólne specjalizacje
2. Gracz jest w stanie grać nie swoją postacią. Test: Estrella.
3. Weryfikacja frakcji v2
4. Weryfikacja INNEGO poziomu granulacji konfliktów: przez aspekty
5. Weryfikacja ścieżek fabularnych
    1. Akcja gracza: reakcja każdej frakcji
    2. Akcja frakcji: +(0,1,2) na losowy tor
    3. Mechanizm eskalacji (brutalizacji) meta-ścieżek
    4. Patrz: [Notatka 170520](/rpg/mechanika/notatka_170520.html)
6. Weryfikacja sceny startowej

## Po czym poznam sukces

1. Szkoły magiczne ze specjalizacjami
    1. Magia staje się potężniejsza; szersza, feel jest bardziej jak ElMet.
    2. Postacie są dobrze odwzorowana przez sztywne szkoły magiczne.
    3. Widzimy GRANICE działania postaci. Wiemy, co może zrobić magią a co nie.
2. Estrella
    1. Gracz będzie w stanie prawidłowo grać nie swoją postacią.
    2. Gracz będzie wiedzieć, jak grać taką postacią.
3. Frakcje
    1. Świat żyje; zawsze wiem, do czego dążą poszczególne strony aktywne
    2. Bookkeeping nie jest morderczy
    3. Gracze są w stanie wejść w interakcje z frakcjami i im przeszkadzać lub pomóc
    4. Zawsze wiem kto jest gdzie i co robi
4. Aspektowe konflikty
    1. Mamy różne typy konfliktów: 'jednostrzałowe' i bardziej interesujące, wieloetapowe
    2. Istnieje możliwość większego wykorzystywania Okoliczności podczas konfliktów
    3. Mniejsze wychylenia wynikające z pojedynczego testu - przegranie testu nie uniemożliwia osiągnięcia celu.
5. Ścieżki Fabularne
    1. Frakcje funkcjonują prawidłowo i ich cele postępują do przodu, jeśli nieskontrowane
    2. Gracze są w stanie osłabić frakcje lub nawet je skontrować
    3. Gracze, jeśli odpowiednio poświęcą, są w stanie brutalizować i osłabić frakcje poważnie.
6. Scena startowa
    1. Zwiększenie immersji
    2. Stworzenie ciekawszej historii

## Wynik z perspektywy celu

Za wysokie zmęczenie, nie przeanalizowano. Z punktu widzenia Żółwia: (1) i (2) działają jak chciałem, (3) jest OK, acz nowa misja wykaże dalsze szczegóły, (4) nie do końca wystąpiło; trzeba znaleźć lepszy bookkeeping na to, (5) jest bardzo dobrze. (6) - wstępnie Kić się podoba, mnie też.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Modyfikatory:

|  .Okoliczności. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .MODYFIKATORY. |
|                 |          |         |         |            |                 |

Wynik:

| .Postać. | .Opozycja. | .Modyfikatory. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |   SFR   |
