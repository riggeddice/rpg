---
layout: inwazja-konspekt
title:  "Marcelin w klasztorze!"
campaign: anulowane
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

## Punkt zerowy:

- Jakie powiązania ma Paulina z Marcelinem?
- D: Wspólna przygoda.
- Co ostatnie powiedział Ci Marcelin zanim zniknął?
- K: "Muszę lecieć, jestem z nim umówiony".
- D->K: Skąd pomysł, by prosić Hektora o pomoc?
- K: Tak naprawdę, szła do Edwina, ale Hektor się napatoczył.
- K->D: Czemu Hektorowi zależało, by nie dotarła do Edwina?
- D: Edwin bywa zbyt ekstremalny w działaniu.
- Co tak zaniepokoiło Paulinę w chorobie, że aż chciała skonsultować się z Edwinem?
- D: To wygląda, jakby choroba miała własną osobowość.
- Dlaczego Hektor chce to rozwiązać maksymalnie dyskretnie?
- K: Bo jak ktokolwiek się dowie, nie uniknie INTENSYWNEGO kontaktu ze sprawami Blakenbauerów.
- Kto nabroił?
- D: Otton.
- Dlaczego Mojra Grecka nic nie wie?
- K: Bo ostatnio nie opuszczała laboratorium.
- Dlaczego Hektor "wystarczy"?
- K: Bo można to rozwiązać jego metodami.
- Dlaczego Paulina jest niezbędna?
- D: Jest kluczowym czynnikiem do przekonania Ottona, że Hektor nie musi angażować się w magię.
- K->Irx: Czemu Margaret zależałoby, by Edwin się o tym dowiedział?
- Bo zmieniłoby to oblicze projektu Moriath.
- D->Irx: Dlaczego Rezydencja próbuje nawiązać kontakt z Pauliną?
- Jest brakującym ogniwem, którym nie może być żaden Blakenbauer.
- K->D: Skąd Hektor wie o istnieniu Pauliny?
- D: Od Marcelina.
- D->K: Co się stało w nocy zanim straciła pamięć?
- K: Margaret weszła do akcji.
- Dlaczego Margaret niczego nie powiedziała Edwinowi?
- D: Boi się, że bardzo by zaszkodziła Hektorowi.
- Skąd wiesz o "smoku"?
- K: Mnóstwo drobnych wskazówek i coś, co wymknęło się Marcelinowi?
- Co ona wie o "smoku"?
- D: Wie, że jest niebezpieczny i przyjaźni się z Marcelinem.
- Kim jest dziewczyna, o którą poszło?
- K: Hektor's soulmate wannabe. Na imię ma Kinga.
- Co zaszło między nią a Marcelinem?
- D: Widziała Marcelina w formie Bestii.
- K->Irx: Dlaczego Edwinowi zależy, by Mojra siedziała w laboratorium i nie wyściubiła nosa?
- Przygotowuje plan, jakby Mojra miała inną agendę niż twierdzi ojciec.
- D->Irx: Skąd Paulina zna Kingę?
- Była pacjentką Pauliny; kiedyś się wpakowała w magiczny problem.
- K->D: Skąd Hektor wiedział, że Paulina idzie do Edwina?
- D: Dostał cynk od Margaret.
- D->K: Skąd u Pauliny fascynacja historią Blakenbauerów?
- K: Uważa, że od rodziny z takim profilem magicznym może się dużo nauczyć.
- Ostatnia znana lokalizacja Marcelina?
- D: W "Rzecznej Chacie".
- Ostatnia znana lokalizacji Kingi?
- K: Sypialnia Marcelina
- Ostatnia znana lokalizacja "smoka":
- D: Szpital Edwina.
- Ostatnia znana lokalizacja ogniska choroby:
- K: Melina pół kilometra od siedziby SŚ.

Choroba ma lekkie objawy, atakuje tylko ludzi, ale wpływa na nich mentalnie. W pewien sposób, przenika ich wiedza między sobą.
"Smok" to potężnie zbudowany mężczyzna z tatuażem w kształcie smoka, głowa Kopalińskiego Klasztoru Shaolin. Tak.

## Misja właściwa:

01. Spotkanie w Rzecznej Chacie.
Marcelin zniknął 5 dni temu. Normalnie Blakenbauerowie by się nie martwili, ale zniknął SKUTECZNIE. Nawet Rezydencja nie może się z nim skontaktować. Z drugiej strony, Marcelin potrafi skutecznie zniknąć Rezydencji. Nie ma więc powodów do paniki... a jednak nie jest to dobre. Więc jedyny, niepowtarzalny, przystojny i skuteczny Hektor Blakenbauer zdecydował się znaleźć brata.
Ślady prowadziły do czarodziejki Pauliny Tarczyńskiej. Jednej z potencjalnych kochanek Marcelina a jednocześnie ostatniej osoby jaka się z Marcelinem widziała. Hektor umówił się z Pauliną na spotkanie w Rzecznej Chacie. Oczywiście, na Hektora wpłynęła nieoceniona Margaret, która skutecznie wyprosiła jego pomoc. 
Tymczasem Paulina miała zupełnie inny powód spotkania z Marcelinem wcześniej. Marcelin pomagał jej raz w leczeniu pewnego człowieka z nieznanej choroby ludzkiej (?). Podobno w zapiskach Blakenbauerów mogło być coś na temat tej choroby, więc Paulina zdecydowała się na współpracę z Marcelinem by dowiedzieć się czegoś na jej temat. Sama choroba była stosunkowo interesująca - działa tylko na ludzi, ale nie ma ani jednego przypadku wyleczenia, mimo używania magii. Rozprzestrzenia się powoli, lecz stanowczo, atakuje tak jakby "czegoś szukała". Ma bardzo lekkie objawy, ale dochodzi do przebicia psychicznego między zainfekowanymi... ogólnie, fascynujące.
I właśnie mniej więcej takimi informacjami Paulina i Hektor się wymienili. Poza, rzecz jasna, sprawami osobistymi i rodzinnymi ;-).
02. Przesłuchanie - gdzie jest Marcelin?
Jako, że wyraźnie ostatnim znanym miejscem obecności Marcelina była Rzeczna Chata, Hektor ruszył przesłuchiwać bywalców z właściwym dla siebie taktem i delikatnością. Podczas tego przesłuchania dowiedział się, że faktycznie Marcelin tu był i mówił o oświeceniu duchowym i swoim Mistrzu. Hektor dotarł też do tego, że ostatnimi czasy Marcelin nie do końca był sobą. Nie był jakiś szczególnie odmienny, ale nie do końca był taki jak zwykle. Trochę za bardzo zamyślony i zafrasowany. 
Gdy Hektor wrócił z tymi informacjami do Pauliny ta dała radę połączyć szczegóły z tym co wie o Marcelinie i jego działaniach - tak, mistrzem Marcelina jest ów tajemniczy Smok. Ale Paulina uznała za bardzo nieprawdopodobne, by Marcelin faktycznie był zainteresowany sferą duchową w takim stopniu jak jedną z nowicjuszek klasztoru Shaolin, Weroniką. 
Tak. To bardziej pasuje do Marcelina.
Paulina zorientowała się też, że Marcelin dał radę jako mag jej pomagający na wpłynięcie na chorobę w JAKIŚ sposób, na jednym z nowicjuszy i że od tej pory Marcelin jest nieco jak nie on, ale jako że Hektor zignorował tą informację ona także ją zignorowała.
Paulina wzięła na siebie odwiedzenie klasztoru i poszukanie Marcelina / przepytanie Smoka. Hektor obiecał za to zdobyć zapiski od Margaret i leki na to, jak ostatnio ta choroba została przez Edwina zniszczona.
03. Telefon od Kingi.
Uczciwy podział obowiązków. Gdy Hektor szedł w kierunku na Rezydencję by porozmawiać z Margaret (i wymusić na niej, naturalnie, dotrzymanie słowa) dostał telefon od Kingi. Kinga Melit jest całkowicie nieważną asystentką sekretarza Hektora, która dodatkowo się w Hektorze podkochuje. I owa Kinga oznajmiła Hektorowi, że ma coś bardzo ważnego do przekazania odnośnie Marcelina. Hektor powiedził, że jest zajęty i prosi o rozmowę za kilka godzin. Co ONA może wiedzieć o Marcelinie?
04. Spotkanie Pauliny i "Smoka".
Niecałe 10 minut później Paulina weszła do wielkiego gmachu, dawnej secesyjnej szkoły, która stała się kopalińskim klasztorem Shaolin. Akolici wprowadzili ją do Smoka, który ją z radością przywitał - Paulina jest znaną w społeczności lekarką. Paulina szybko przypomniała sobie dane o Smoku - klasztor był założony przez 4-5 osób, przyjechali ciężarówką (nie Kamazem) i od tej pory klasztor powiększył się do 12 osób i 7 uczniów niezrzeszonych.
Po odrobinie dyskusji (w czasie której Smok podał Paulinie wodę zwiększającą receptywność na działania magimentalne) wyszedł temat zniknięcia Marcelina. Smok odparł, że Marcelin przecież jest tutaj, wstąpił do klasztoru. Zdziwił się, że Marcelin nikomu nie powiedział. Oddalił się by przygotować spotkanie Pauliny i Marcelina, ale szybko wrócił. Marcelin (i Weronika) potrzebuje 30 minut na "dojście do siebie". W rzeczywistości Marcelin po prostu musiał wrócić do pełnego działania - choroba wyłączyła jego układ nerwowy i łączyła się z kanałami by działać szybciej; wyjaśnienie pochodziło od samego Marcelina.
05. Marcelin w klasztorze.
No to się spotkali... Marcelin zaczął od spraw duchowych, ale przebicie od Pauliny pokazało Chorobie, że to nie zadziała. Szybko powiedział, że tak naprawdę celem była Weronika. To było bardziej prawdopodobne, w to każdy by uwierzył. Marcelin powiedział, że nie wychodzi z klasztoru, za tydzień wyjdzie i w ogóle teraz są śluby i jest dorosły. O. Paulina kazała mu zadzwonić do Hektora i powiedzieć co się z nim dzieje. No bo bez jaj, tak się nie znika...
Oki. Zdobywszy obietnicę od Marcelina Paulina poszła do Smoka. W końcu w klasztorze znajduje się właśnie ten pacjent, któremu pomogła z Marcelinem. Ta dziwna zmiana choroby, chciała mu jakoś pomóc. Smok zwietrzył szansę - powiedział, że faktycznie Marcelin pomógł ale że on też może pomóc, że on też walczy z chorobą. Powiedział, że przez medytację potrafi doprowadzić do tego, że chorzy mają ukojenie. Że "otwiera im aurę". Paulina wyraziła zainterestowanie i Smok stwierdził, że jej pokaże.
Udali się do jednego z pacjentów. Chorego nowicjusza Shaolin. Smok otworzył mu aurę - w rzeczywistości był to paramagiczny efekt katalizujący. Paulina zobaczyła możliwość działania i użyła swojej mocy do zwalczenia i przechwycenia choroby. Zaskoczyła Chorobę - udało Paulinie się wyleczyć tego nowicjusza, ale jako że wcześniej wypiła tą "wodę", Choroba uzyskała możliwość zarażenia przez kontakt magiczny Pauliny (co się stało). Połączenie Pauliny i Marcelina oznaczałoby, że Marcelin bezpiecznie wpadnie w ręce Choroby, w formie ukrytej.
I taki był plan.
06. Hektor - zdobywca leków i zapisków.
Tymczasem Hektor zwrócił się do Margaret o lek wyprodukowany przez Edwina mający pomóc na magiczną chorobę. Margaret była sceptyczna co do tego czy to ma jakiekolwiek powiązanie (bo nie powinno mieć), ale Hektor na niej to wymógł (konflikt wygrany!). Do tego Hektor uzyskał zapiski Edwina dotyczące samej choroby (magcznej, nie tego co atakuje aktualnie ludzi). Ogólnie, super robota.
Dostał telefon od Marcelina. Opieprzył go i kazał wracać do domu jak najszybciej. Marcelin odmówił, Hektor zadzwonił do Pauliny i kazał jej przyprowadzić Marcelina z powrotem. Ta się żachnęła, ale przemilczała. Nie zamierza do niczego zmuszać Marcelina, co to ona, niańka?
No i Hektor przypomniał sobie o umówionym spotkaniu z Kingą. Ech. No trudno, pojedzie się z nią spotkać. Do restauracji. Kinga już czekała na Hektora i powiedziała mu mrożącą krew w żyłach opowieść - śledziła Marcelina od pewnego czasu. I nagle widziała, jak w okolicach klasztoru Shaolin Marcelin został porwany przez MUTANTY JASZCZURY Z MARSA! Hektor zamrugał oczami i wyszło mu jednoznacznie, że widziała Marcelina w formie Bestii. Ale z jej opisu Marcelin wszedł w formię Bestii (defensywnie) i zaraz z niej wyszedł (spacyfikowany). Co do cholery...
Hektor dostrzegł, że Kinga sama jest chora. Zostawił notkę Margaret, że ona widziała coś co nie powinna, żeby ta się zajęła Kingą. Sam natomiast zadzwonił do Pauliny - to poważna sprawa, z Marcelinem jest na pewno coś nie tak, on MUSI wrócić.
07. Spięcie Marcelin - Paulina. Spotkanie w Rezydencji Pauliny i Hektora.
Paulina została przekonana przez Hektora, by spróbować przekonać Marcelina. Poszła się spotkać z Marcelinem, ale on nie chciał słyszeć o opuszczeniu klasztoru. Obiecał tylko, że będzie często dzwonił - to ustępstwo podniosło Paulinę na duchu. Z ciężkim sercem, udała się do Rezydencji po lekarstwo i porozmawiać z Hektorem.
Ku swemu zdziwieniu, Paulina dostała od Hektora zapiski Edwina oraz samo lekarstw. Nie spodziewała się - Blakenbauerowie mają reputację osób nie dzielących się wiedzą. Szybko przejrzała zapiski Edwina i rzuciło jej się w oczy, że są niekompletne i nie są pisane stylem Edwina. Zupełnie, jakby Edwin napisał notatki w swoim stylu, dokładne, szczegółowe a potem wyciął to co chciał ukryć. Tak samo zafrapowało ją samo lekarstwo - było stworzone z krwi KONKRETNEGO maga. I nie był to Blakenbauer. Cóż, wątpliwości zachowała dla siebie, ale uznała, że więcej nic nie wydobędzie.
Po analizie notatek i burzliwej dyskusji z Hektorem uznała, że musi porozmawiać z Edwinem. Hektor niechętnie, ale się zgodził.
08. Edwin wkracza do akcji. Dyspozycje Ottona.
Hektor poszedł do Edwina i poprosił go o spotkanie z Pauliną w sprawie tej dziwnej choroby. Edwin nie był zainterestowany, ale Hektor przekonał go, że to młoda lekarka, wścibska i uważa, że choroba atakująca ludzi ma podłoże magiczne. To Edwina zainteresowało i poszedł do Pauliny. W tym czasie Hektor poszedł do Ottona powiedzieć o tyn, co spotkało Marcelina i o tym, że wszedł w Bestię.
Edwin poszedł porozmawiać z Pauliną. Podczas rozmowy dowiedział się, że Paulina wyleczyła magią jednego zarażonego. I że Marcelin też zmienił chorobę. Edwin usłyszał objawy i powiązał je od razu (tak jak Paulina) z chorobą którą wyleczył. Ale to była choroba magów, choroba wynikająca z projektu Trzmiel. Nie miała prawa atakować ludzi, nie ma źródła energii.
A fakt że Paulina wyleczyła tą chorobę oznacza, że sama się zaraziła... bo tak to działało.
Edwin posłał mentalnie po małą mantikorę, by dziabnęła i zemdlała Paulinę. Plan wykonany. Edwin może działać.
Zabrał Paulinę do laboratorium i potwierdził, że Paulina jest zainfekowana Trzmielem. Natychmiast ją wyleczył (lekarstwo zadziałało, bo to ta sama choroba). Udał się do Ottona zgłosić to wszystko.
Tam był już Hektor. Edwin i Otton mentalnie przedyskutowali implikacje - Marcelin najpewniej także jest skażony Trzmielem. A więc to atak odwetowy Trzmiela na Blakenbauerów, za to, co mu zrobili. To ma sens. Nie mówiąc nic Hektorowi, Edwin i Margaret udali się naprawić sytuację. Tym razem z pomocą Mojry - była też potrzebna.

Hektorowi podziękował Otton za skuteczną i odpowiednio bezwzględną reakcję.

Hektor oczywiście nie rozumie o co chodzi.
Nieprzytomna Paulina też.


# Mapa misji

![](140819%20-%20Marcelin%20Mnich%20Shaolin.jpg)

# Zasługi

- mag: Hektor Blakenbauer jako NIEmarnotrawny brat.
- mag: Paulina Tarczyńska jako niezauważalnie uszkodzona lekarka nie pamiętająca dokładnie Marii.
- mag: Marcelin Blakenbauer jako pokorny mnich chcący zostać w klasztorze. Potencjalnie, dla pewnej dziewczyny.
- vic: Smok jako mistrz Kopalin-Shaolin próbujący uderzyć w Blakenbauerów. Mści się za Trzmieli?
- mag: Edwin Blakenbauer jako najwybitniejszy lekarz w okolicy.
- mag: Margaret Blakenbauer jako uśmiechająca się kokietka sterująca z drugiej linii.
- czł: Weronika Piniarz jako młoda adeptka Kopalin-Shaolin którą był (lecz nie jest) zainteresowany Hektor.
- czł: Kinga Melit jako asystentka sekretarza Hektora bardzo zainteresowana Hektorem.
