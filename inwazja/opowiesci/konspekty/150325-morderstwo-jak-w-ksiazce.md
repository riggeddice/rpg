---
layout: inwazja-konspekt
title:  "Morderstwo jak w książce"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150329 - Knowania Izy(SD, PS)](150329-knowania-izy.html)

### Chronologiczna

* [150329 - Knowania Izy(SD, PS)](150329-knowania-izy.html)

# Wątki

- Projekt "Moriath"
- Tymotheus Blakenbauer

## Misja właściwa:

Maria skontaktowała się przez soul-linka z Pauliną, która ma uszkodzone (regenerujące się) kanały magiczne. Maria jest na imprezie w Myszeczkowie. Mówi, że dzieje się tu coś nie tak. Wpierw ktoś zaprzyjaźniał się z grupą ludzi, potem była tam typowa małolata - jailbait i poszła z jakimś facetem na stronę... a potem z innym. Maria jest coraz bardziej zestresowana, bo ochrona zamiast ingerować (nie, żeby miała w co ingerować) zajmuje się innymi sprawami.
Na imprezie pojawiła się jailbait. Zaczęła płakać, że "on" ją wykorzystał. Ten przyjazny zaczął podżegać "no chyba tak tego nie zostawicie". Ktoś już bardziej podpity wpakował tulipana z butelki w twarz nieszczęśnika, który poszedł z małolatą na stronę. Ochrona się ruszyła.
Maria dostrzegła jeszcze, że jest tam typowy aryjski blondyn który ma dookoła siebie grupkę lokalnych ludzi. Wygląda jak typowy nazista.

Zgodnie z radami Pauliny po soullinku Maria wczołgała się pod stół i tam została. Nagle zaczęła usypiać. Paulina uderzyła ją przez soullink (12v10/8->9) i Maria została na tyle świadoma, by słyszeć jak na terenie pojawił się mag, który mówił z niemieckimi wstawkami i stwierdził, że tu potrzebny będzie jeden sztuczny człowiek i czyszczenie pamięci. Powiedział też komuś, że "był niegrzeczny".
Maria zanim zasnęła zidentyfikowała, że na nogach zostały cztery osoby. W tym aryjski blondyn (poznała po butach) i małolata.

Hektor dostał telefon. Dzwoni Kleofas Bór, dowódca jego sił specjalnych. Jest sprawa - morderstwo w Myszeczkowie. Panika na imprezie, morderstwo jednej osoby, policja zawiadomiona. Hektor poprosił ze sobą Olgę Miodownik i jedzie na miejsce, uzbrojony w oddział sił specjalnych.

Tymczasem Paulina dotarła na miejsce pierwsza. Ma torbę lekarską i udaje, że jest lekarzem odbierającym koleżankę. Jednak tu na miejscu imprezy jest... zwykła impreza. I Paulina odzyskała połączenie z Marią. Ta twierdzi, że boli ją głowa bo za dużo piła... po chwili dochodzi do re-połączenia soullinka i Maria wszystko sobie przypomina. Widzi wyraźnie zabitego mężczyznę.
Paulina rzuca przekleństwo. Zdążyli to posprzątać i go podmienić.

Hektor dotarł na miejsce imprezy. Jest cały zdziwiony że nic się nie dzieje, ale zanim zdecydował że to strata czasu poczuł energię magiczną. Ktoś niedawno (bardzo niedawno) rzucił tu silny czar. Hektor wysłał Olgę by ta z detektorem ultrafioletowym znalazła ślady krwi. Niby nic nie ma, ale Hektor wydaje rozkazy a Olga słucha, więc agentka nie zadaje pytań.

Maria rozpoznała, że Olga jest "detektywem". Nie zachowuje się jakby była na imprezie. Chodzi z urządzeniem i czegoś szuka. Najpewniej pamięta. Przekazała to Paulinie, która poprosiła by Maria się nie ujawniała.
Olga rozpoznała, że Paulina tu nie pasuje. Ma torbę medyczną do 'emergency' i porusza się nie jak na imprezie. Spytała Hektora czy ma ją ktoś przyprowadzić i Hektor potwierdził.

Olga dostała zadanie poszukiwań krwi nie tylko na ziemi ale i na ubraniach ludzi. Kleofas miał opisać, co powiedział ów agent który jest na tej imprezie. Gdy Kleofas to opisał, Olga powiedziała że sobie ów agent to zmyślił. Cała ta fabuła pasuje do tego, co napisał Cyprian Koziej w książce "Kamienny Urok Wąpierzowa". Zapytana o szczegóły powiedziała, że na uczcie królewskiej młoda gnomka została zgwałcona przez dużego... dosłownie DUŻEGO półorka i to zrobiło jej krzywdę, więc gnomy wsadziły wazę prosto w twarz półorka. I to rozpoczęło wojnę. Hektor się zadumał.

Zapytana przez Hektora trochę wcześniej agentka specjalizująca się w informatyce, Patrycja Krowiowska znalazła filmik z tego, jak podżegacz doprowadził do tego, że gość wraził tulipan w twarz drugiemu człowiekowi. Powiedziała, że to świetny montaż. Hektor kazał zatrzymać wszystkich na filmiku.

Dodatkowo, Hektor zauważył coś nietypowego. Dwóch ochroniarzy, naziol i trzech lokalnych. Pod wpływem dyskusji i perswazji jeden z ochroniarzy który najpierw reagował zaskoczeniem i niedowierzaniem zaczął przekonywać się do wizji pozostałych. Niepokojące.

Tymczasem do Pauliny podszedł Kleofas Bór i poprosił ją o podejście do samochodu. Podał Paulinie odznakę by potwierdzić. Poprosił Paulinę do Hektora i tam doszło do pierwszego kontaktu Hektora i Pauliny. Nie spodobali się sobie od pierwszego wejrzenia; Kleofas musiał przedstawić Paulinie prokuratora zanim ta zdecydowała się wejść do auta.
Paulina się przyznała Hektorowi, że wiedziała o imprezie od kogoś, o całej tej sprawie. Chroni jednak swojego informatora. Dyskusja między Pauliną i Hektorem przeszła na filmik i na rannego mężczyznę. Zdaniem Pauliny mężczyzna ranny w ten sposób powinien umrzeć. Więc to musi być fotomontaż.
Hektor dał Paulinie do podpisania dokument typu NDA. Paulina podpisała. W końcu Maria i tak wszystko widzi oczami Pauliny.

Kolejnym krokiem było przesłuchanie podłożonego człowieka. Paulina - minion master - zna sposób zawieszenia sztucznych ludzi Diakonów; po prostu przez zadawanie dużej ilości kontekstowo nie powiązanych pytań. Zadała grupę takich pytań i sztuczny człowiek ze źle skonstytuowaną osobowością się częściowo rozpadł. Paulina odkryła, że ów nieszczęśnik jest kupiony z farm Diakonów i zaimprintowali mu osobowość. Niestety, nie mieli dostępu do pełnej osobowości, więc zaimprintowano mu osobowość niekompletnie. Na 100% sztuczny człowiek i to niekompletnie zbudowany.

Hektor, dawno temu, był uczony przez Edwina jak przesłuchuje się sztucznych ludzi. Rozpoznał tą metodę. Gdy już sztuczny człowiek się zawiesił, Hektor zadał kolejne pytania - dowiedział się, że sztuczny człowiek jechał ciężarówką, że mag który za tym stoi nie spodziewał się śmierci i że tych "dziwnych ludzi" było czterech a mag zwracał się do nich jak do jednej osoby.

Sztuczny człowiek ma instynkty samobójcze. Jako, że ma niekompletną osobowość ma się zalać i dać się przejechać. Hektor postanowił wysłać go do Edwina, do laboratorium. Paulina nie oponowała. Powiedziała Hektorowi, że Edwin jest jej kolegą po fachu. Hektor natychmiast zadzwonił do Edwina, który potwierdził "tak, Paulina jest czarodziejką. Idealistka, umrze młodo". Hektor poprosił go o usunięcie problemów ze sztucznego człowieka i przechwycenie go do Rezydencji. Edwin się zgodził z uśmiechem.

Hektor przyznał Paulinie, że jest magiem. Wreszcie zaczęli współpracę...

Kleofas Bór i Olga Miodownik zgarnęli małolatę dla Hektora. Poszła bez oporu. Patrycja Krowiowska i Artur Bryś poszli po naziola.
Kleofas i Olga wrócili pierwsi; powiedzieli, że małolata (Dorota) jest w samochodzie. Olga z pogardą zaznaczyła, że chciała wziąć ich na litość. Kleofas zrobił minę, jakby na niego zadziałało... ale Olga to psychopatka.
Patrycja i Artur wrócili źli na siebie. Podczas aresztowania naziola Artur powiedział coś o podrzędnej roli kobiet. Ten na to, że tylko zastanawiał się co KOBIETA robi na INFORMATYCE i do tego jeszcze w SIŁACH SPECJALNYCH. 
Aresztowanie naziola (Eryka) nie było trudne, nie stawiał opór. Kleofas wsadził go do samochodu. 

Zostawili Olgę i Artura jako agentów na imprezie, Maria też tam została dyskretnie a Paulina i Hektor pojechali ze zgarniętymi i z pisarzem przaśnej fantasy (też zgarniętym). Hektor zdecydował się pojechać z Erykiem (nazistą) w samochodzie i podłożył podsłuch do auta z Dorotą (małolatą).

W samochodzie z Dorotą jadą: Olga i Patrycja.
W samochodzie z Erykiem jadą: Hektor i Kleofas.

Eryk próbował jakieś strzały i wycieczki osobiste; w końcu trafił Kleofasa o "bracie". Hektor pozwolił wydarzeniom się dziać i obserwował co się stanie. (10v8->10) zanim cokolwiek się stało poważnego Hektor porównał feed z samochodu Doroty i samochodu Eryka. Bez użycia magii, oboje (Dorota i Eryk) działają podobnie - adaptują do rozmówców, obserwują ich nadnaturalnie dobrze i przejmują w pewien sposób nad nimi kontrolę. Dorocie wisi, czy Olga ją potnie, czy potnie Patrycję; wisi jej, czy Patrycja kogoś uderzy czy się rozpłacze. Jej modus operandi to słodycz i bezradność.
Eryk jest antykobieciarzem, mega szowinistą i jest za kultem siły itp. Też wzbudził w Borze agresję, ale ten to stłumił.
Hektor natychmiast kazał oddzielić Eryka i Dorotę nieprzezroczystą i niesłyszalną szybą. Są zbyt niebezpieczni, co gorsza, za szybko adaptują.

Maria skontaktowała się soullinkiem z Pauliną i powiedziała, że sprawdziła podobne wydarzenia jak to co robi ten nazista; czyli pogarda dla kobiet i kult siły. Znalazła takie wydarzenia sięgające do 2 miesiące wcześniej. Co więcej, znalazła, że ostro pobiło się 3-4 nazistów i zaczęli na siebie sypać; aktualnie są w więzieniu w Kopalinie.

Hektor umieścił Eryka i Dorotę w Rezydencji. Chronieni przez Oddział Zeta.

W Rezydencji, Hektor i Paulina ufortyfikowali pozycję aptoforma (Doroty i Eryka). Po drodze przechwycił ich Edwin i spytał, czym jest ten sztuczny człowiek. Hektor wjechał mu z inkwizytora (10v7) i Edwin się przyznał; nietypowy proces tworzenia sztucznych ludzi z formy organicznej i połączonej z nekromancją. Skrajnie nieetyczny. Hektor spytał, czy z tym może być powiązany wujek Tymotheus na co Edwin solidnie się zmartwił i spytał "czemu". Edwin wyjątkowo nie lubi Tymotheusa Blakenbauera. Hektor powiedział, że to brzmi jak on... zwłaszcza w połączeniu z tym co mają w piwnicy.

Edwin ściągnął do piwnicy Margaret. Po długich poszukiwaniach i wstępnych obserwacjach Margaret zidentyfikowała viciniusa - jest to aptoform (apto formica defero), czyli organizm "adaptive ant community". Jest to taumatożerca asymilujący energię magiczną przy obecności dużej ilości negatywnej energii emocjonalnej; te byty spawnują "odnogi" które adaptują i mają wyzwolić maksymalny ciemięż w społeczeństwie. Nie używają magii.

Paulina zauważyła, że aptoform mógł w dowolnym momencie zniszczyć swoje odnogi. Nie zrobił tego. Dlaczego. Dobre pytanie, potwierdził Edwin. Doszło do niewielkiej konfuzji, bo Margaret założyła, że skoro rozmawiają tak swobodnie to chyba Hektor x Paulina. Edwin szybko wyprowadził ją z błędu. Margaret powiedziała, że spróbuje zlokalizować rdzeń aptoforma a Edwin mocą Rezydencji odebrał pamięć Paulinie ze wszystkiego w samej Rezydencji.

Chwilę później Maria Paulinie ową pamięć przywróciła. 

Hektor jeszcze poprosił Edwina, by ten załatwił mu psa polującego który wykryje końcówkę aptoforma. Edwin przekazał temat Margaret. Nie ma sprawy, zwłaszcza, że mają końcówki aptoforma w laboratorium. Edwin zauważył, że mag próbujący kontrolować aptoforma jest głupcem; aptoform ZAWSZE wygrywa.
Margaret dostała olśnienia. Ona CHCE aptoforma (do projektu Moriath - to rozwiązałoby kilka problemów). Wszyscy spojrzeli na nią jak na idiotkę, więc powiedziała, że chce aptoforma ZABIĆ. Nikt jej nie uwierzył.

Hektor zdobył informacje z komend policyjnych - udało mu się (10v7->8). Udało mu się zlokalizować obszar działania aptoforma. Dwie miejscowości. Ma od czego zacząć...
...ale nie z Pauliną. Ta się nie nadaje; jest spoza rodziny.

# Streszczenie

Maria Newa jest na imprezie i dochodzi do zbrodni. Pojawia się mag i wszystkich usypia. Wpierw pojawia się Paulina i dochodzi do Marii, potem Hektor z siłami specjalnymi. Zabity człowiek okazał się zostać wprowadzony jako sztuczny człowiek Diakonów by ukryć zbrodnię. Hektor aresztował wszystkie "istoty nieludzkie" na imprezie i ku zdumieniu zauważył, że mają niesamowitą mimikrę psychologiczną. Edwin zidentyfikował to jako 'aptoform'. Gdzieś w okolicy jest mag współpracujący z aptoformem lub przezeń wykorzystywany. Chwilowo końcówki są w Rezydencji. Edwin skasował pamięć Paulinie, Maria ją odtworzyła soullinkiem.

# Zasługi

* mag: Hektor Blakenbauer jako prokurator który robi wszystko co powinien i ratuje swoją grupę przed aptoformem, którego sam sprowadził na grupę.
* mag: Paulina Tarczyńska jako czarodziejka mająca pierwszy kontakt z Blakenbauerami i szybko decydująca, że nie chce więcej tych kontaktów.
* czł: Maria Newa jako jedyna, która pamiętała imprezę dzięki soullinkowi i jako re-generator pamięci Pauliny po działaniach Edwina.
* czł: Cyprian Koziej jako pisarz fantasy, którego dziełem inspirował się morderca. Całkowicie nieistotny poza byciem fałszywym tropem ;P.
* czł: Kleofas Bór jako szef grupy specjalnej Hektora, który nie dał się sprowokować aptoformowi... choć jego słabym punktem jest jego brat.
* czł: Olga Miodownik jako sadystyczna patolog sił specjalnych Hektora, która utrzymała końcówkę aptoforma w ryzach... i prawie zrobiła to co aptoform chciał.
* czł: Patrycja Krowiowska jako informatyk sił specjalnych Hektora, agentka która odkryła nagranie z imprezy dla Hektora.
* czł: Artur Bryś jako członek sił specjalnych Hektora, mięśniak i zaczepnowojownik. Pod wpływem aptoforma pokłócił się z Patrycją odnośnie kobiet w siłach specjalnych.
* mag: Edwin Blakenbauer jako jednoznaczne źródło informacji dla Hektora, że Paulina jest czarodziejką oraz cyniczny eliminator pamięci Pauliny.
* mag: Margaret Blakenbauer jako nie uświadomiona fanka aptoforma, która pomoże Hektorowi go zniszczyć mimo że chce go dla siebie.
* vic: Aptoform Mirasilaler, tajemniczy aptoform o nie zdefiniowanych celach uczący się przez końcówki "nazista" i "małolata". Pracuje dla (z?) jakiegoś maga.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji
            1. Powiat Rymaski
                1. Myszeczkowo
                    1. Remiza strażacka

# Pytania:

Ż: Z jakiej okazji w Myszeczkowie zrobiono wystawną imprezę?
D: Ponieważ wrócił do miasta celebryta który się tu urodził.
Ż: W jakim miejscu odbywała się impreza w Myszeczkowie?
K: Impreza w Myszeczkowie odbywała się w remizie.
Ż: Jakim cudem nikt nie dowiedział się o obecności czarodziejki Pauliny?
D: Miała ciężko nadwyrężone kanały magiczne i nie mogła czarować, więc miała nieaktywną aurę i nie było jej na miejscu.
Ż: Jakim cudem o falloucie imprezy dowiedział się prokurator MIMO potencjalnej ingerencji magów?
K: Magowie nie wzięli pod uwagę ludzkich kanałów.
Ż: Co bardzo istotnego i niebezpiecznego odnośnie działania magów wie Paulina?
D: Coś uciekło.
Ż: Co to za celebryta?
K: Pisarz przaśnej literatury fantasy.
Ż: Dlaczego Paulina nie odpuści?
D: Bo ten pisarz jest z jej punktu widzenia bardzo ważny.
Ż: Co sprawia, że mimo potencjalnego braku dowodów Hektor wchodzi w tą sprawę podejrzewając magię?
K: Znalezione ślady kojarzą mu się z Blakenbauerami.
D->Ż: Dlaczego to, co uciekło ciągle trzyma się blisko?
Ż: Bo ma nadzieję się jeszcze pożywić. 
K->Ż: Dlaczego Paulina nie uzna Hektora za winnego tej sprawy?
Ż: Bo robi za dużo hałasu. Nie próbuje nic ukryć, nie próbuje nic schować.
D->K: Dlaczego Paulina preferowałaby opcję "capture, do not kill"?
K: Bo to kiedyś był człowiek.
K->D: Dlaczego Hektor utrzymał to w tajemnicy przed resztą rodziny?
D: Bo jego rodzina mogłaby chcieć utrzymać to w tajemnicy przed samym Hektorem.
Ż: Kto przyniósł Hektorowi wieści i jakimi kanałami?
K: Dla odmiany jego oddział specjalny przekazał informacje.
Ż: Jaka jest rola pisarza w tym wszystkim?
D: Atakujący stwór zachowuje się, jakby wylazł z książki Cypriana Kozieja.