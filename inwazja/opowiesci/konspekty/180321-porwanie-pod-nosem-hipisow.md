---
layout: inwazja-konspekt
title:  "Porwanie pod nosem hipisów"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić, draża
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180318 - Czyżby drugi kraloth?](180318-czyzby-drugi-kraloth.html)

### Chronologiczna

* [180318 - Czyżby drugi kraloth?](180318-czyzby-drugi-kraloth.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

**Wizja artysty**:

![Kralothborn](Materials/180399/180320-kralothborn.jpg)

**Strony konfliktu, atuty i słabości**:

![Strony, atuty, słabości](Materials/180399/180321-story.png)

### Wątki konsumowane:

brak

## Punkt zerowy:

## Mapa logiczna Opowieści:

brak

## Pytania Generatywne:

* Ż: 
* K: 
* Ż: 
* D: 

## Potencjalne pytania historyczne:

* null

## Misja właściwa:

**Dzień 1**:

Edwin skontaktował się z Aliną. Poprosił ją by uratowała Marię Przysiadek, zanim Arazille ją Dotknie lub ta zmieni się w viciniusa doszczętnie. Alina pomyślała o Wdenkowie - jego nienaturalny umysł wspomagany alkoholem powinien oprzeć się potędze Arazille... przez jakiś czas.

Wdenkow i Alina przedyskutowali. Alina wie, gdzie jest Maria - w Czeliminie. Nie wie gdzie jest, dokładnie.

Czelimin. Hotel Pracowniczy. Gdy fabokloskażony recepcjonista dał Alinie klucze, poszli do baru hotelowego. Tam Alina usłyszała jak Amelia i Grzegorz dyskutują o Marii - Grzegorz chce pomóc Marii przez akceptację Arazille. Amelia się z tym nie zgadza - nie chce nowego fabokla.

Gdy Siergiej czekał na Alinę, opadły go dwa fabokle. Zaczęły z nim rozmawiać; Siergiej oparł się dwóm faboklom. Na to wpadła Alina. Widzi, że Siergiej się oparł; mała przebieżka i zgubili te fabokle. Okazja porozmawiania. Alina wyjaśniła Siergiejowi, że fabokle są "na dragach" i mogą ich zarazić. Potem powiedziała o wszystkim co podsłuchała od Amelii i Grzegorza.

Biała Róża. Ekskluzywny klub z niewielką ilością mieszkańców Czelimina. Bogatsi obywatele okolicy. Ale nie można wejść od tak - trzeba się powołać. Siergiej zadzwonił do Pawłowa; załatwił sobie wejściówki dla dwóch.

Tymczasem Alina wbiła się do baru ale natychmiast się wycofała. Amelia deklamuje poezję a fabokle śpiewają hipnotyczną pieść pożerającą negatywne emocje. Alina nie jest Siergiejem, nie może tam wejść. Za to poczekała i zaczęła śledzić Zachradnika - kapłana Arazille. Doprowadził ją do kościoła Czelimińskiego. Alina spojrzała do środka i faktycznie, znajduje się tam Maria. Ale jest tam 12 fabokli i śpiewają Marii. Alina nie może nawet się ZBLIŻYĆ. Wycofała się, pilnując kościoła i czekając na okazję.

Siergiej śledził Amelię. Doprowadziła go do Teatru Wiosennego. Stwierdził, że będzie śledził ją dalej, ale Amelia dała radę go zlokalizować. Złapała go koło swojego pokoju, ale była zdziwiona - czemu on ją śledzi. Zaprosiła go do rozmowy i Siergiej wszedł do jej pokoju.

Tam Amelia wyciągnęła z Siergieja to, że on chce porwać Marię - ale, że chce jej pomóc. Amelia sama powiedziała, że ona chce pomóc Marii - Maria jest ciężko chora a ona nie wie jak ją uratować. Tu - Amelia wpadła na pomysł. Spojrzy z Siergiejem w Srebrne Lustro i znajdą rozwiązanie. Siergiej się skwapliwie zgodził.

* Powiedziałam... spojrzymy RAZEM w SREBRNE LUSTRO. Rozumiesz? - Amelia, z niedowierzaniem
* Nie mam nic do stracenia, jeśli to jej jakoś pomoże... - Siergiej, z przekonaniem, acz zupełnie nie rozumiejąc o co chodzi

Spojrzeli. Siergiej zobaczył Arazille a ona zobaczyła jego. Siergiej zobaczył jak komórki kralotyczne pokonują komórki ludzkie w Marii i jak fabokle ją stabilizują. Amelia natomiast zobaczyła czyste intencje Siergieja - on nie jest pachołem magów chcących Marię zniszczyć. On chce jej pomóc. Zdaniem Amelii, los jaki czeka Marię jest gorszy od śmierci... jest skłonna zaryzykować świat magów.

Siergiej, za namową Amelii, zadzwonił do Aliny. "Hej, wiem jak pomóc Marii, rozmawiałem z Amelią i w ogóle". Alina jest sceptyczna - ON JEST JUŻ FABOKLEM, PATRZYŁ W LUSTRO, ARAZILLE GO ZARAZIŁA! Amelia, podłamana, zauważyła że mogą się spotkać. Alina dała teren - w parku. Ona umie zwiać na drzewo a fabokle są, no, trochę leniwe ;-). Nie łażą po drzewach. I za 10 minut spotkanie, by mieć gwarancję, że Amelia nie zmontuje jakiejś siły bojowej.

Amelia się skwapliwie zgodziła, acz jest podłamana koniecznością bardzo szybkiego marszu.

Park, noc. Amelia, Siergiej i Alina. Siergiej próbuje przekonać Alinę, że Amelii można zaufać (co jest totalnie zgodne z zachowaniem agenta Arazille). Alina, zrozumiale, nie słucha Siergieja. Amelia próbuje przekonać Alinę, że chce pomóc - ale musi wiedzieć jacy magowie stoją za Aliną. Alina, co zrozumiałe, nie chce powiedzieć (Maskarada). Ogólnie, NIEFART.

W końcu Alina zaryzykowała - rzuciła Siergiejowi piersiówkę i jak ten miał odwróconą uwagę, shiftnęła twarz, pokazując Amelii, że jest viciniusem. To znaczy, że Maria ma SZANSĘ. Potem powołała się na Marcelina Blakenbauera, eks-kochanka Amelii. Amelia... zgodziła się pomóc Alinie w wyprowadzeniu Marii z domeny Arazille. Ale Siergiej zostanie tutaj, w Czeliminie.

Siergiej się zgodził. Chce uratować Marię, a co może mu tu grozić? Alina była bardziej niechętna, ale wiedziała, że Siergiej może być "ofiarą". Trudno.

Edwin pozyskał Marię Przysiadek. I złapał się za głowę widząc jej stan...

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (10)
    1. Maria Przysiadek: fuzja mocy Arazille i kralothborn. Edwin będzie miał zabawę.
    1. Marcelin Blakenbauer: chce się spotkać z Sophistią. Jakoś.
    1. Odbicia Damy w Czerwonym się pojawiają też na Mare Somnium.
    1. Amelia Eter jest kluczem do Damy w Czerwonym.
1. Kić (2)
    1. 
    1. 
    1. 
1. Draża (5)
    1. 
    1. 
    1. 

# Streszczenie

Edwin ściągnął Alinę, by ta wyrwała z Czelimina Marię Przysiadek. Alina ściągnęła Siergieja - alkoholika odporniejszego na Arazille niż przeciętny człowiek. Czelimin jest domeną Arazille jak Żonkibor - z faboklami i Pryzmatem. Siergiej nawiązał kontakt z Amelią Eter i ta, po rozmowie z nim i Aliną, pomogła wyciągnąć Marię by Edwin mógł ją uratować od losu kralothborn-bez-kralotha lub elementu Domeny Arazille. Siergiej spojrzał w Srebrne Lustro i został w Czeliminie jakiś czas.

# Progresja

* Maria Przysiadek: przebudziły się w niej geny kralothborn. Dotknęła ją moc Arazille. W tej chwili tylko forma i umysł jest u niej ludzka; wszystkie instynkty są obce.
* Maria Przysiadek: trafiła na KADEM, pod opiekę Edwina i Norberta.
* Amelia Eter: okazuje się, że jest niewrażliwa na pieśni fabokli. Z jakiegoś powodu Arazille jej nie Dotyka.
* Amelia Eter: zsynchronizowała się ze Srebrnym Lustrem Arazille; może go swobodnie używać a lustro nie robi jej krzywdy.
* Amelia Eter: jest kluczem do zrozumienia odbić Damy w Czerwonym, choć nie wie dlaczego i o co chodzi.

## Frakcji

* Domena Arazille: ma około 30 fabokli w Czeliminie, kapłana (Zachradnika) i ogólnie potężny hold w Czeliminie.
* Domena Arazille: odbicia Damy w Czerwonym pojawiają się też w Mare Somnium.

# Zasługi

* vic: Alina Bednarz, zwerbowana przez Edwina do odzyskania Marii z domeny Arazille w Czeliminie. Z pomocą Siergieja, poradziła sobie śpiewająco. Też zrobiła mały rekonesans tej domeny.
* czł: Siergiej Wdenkow, eks-glina alkoholik z Prysznicowa, uciekinier z Ukrainy, tropiciel ludzi, opuścili go bliscy; alkoholik o umyśle obcym (przez to częściowo odpornym na Arazille).
* mag: Edwin Blakenbauer, obiecał Silurii, że zajmie się Marią Przysiadek i to zrobił - załatwił Alinę by odzyskała dlań Marię. Uzyskał ją... kralothborn i Arazille-warped.
* vic: Maria Przysiadek, pragnie wrócić do rodziny, nie rozumie tego świata; walczy z Arazille (Zachradnikiem) i swoją naturą. Skażona przez fabokle, krew kralothborn szaleje...
* czł: Amelia Eter, poetka w domenie Arazille. Nieco znudzona i samotna. Z jakiegoś powodu Arazille jej nie Dotyka. Pomogła Marii - przeszmuglowała ją do Blakenbauerów wbrew Arazille.
* czł: Grzegorz Zachradnik, kapłan Arazille a kiedyś biznesmen. Chce pomóc Marii oddać się Arazille - w ten sposób Maria utraci instynkty kralotyczne i będzie szczęśliwa.

# Plany

* Marcelin Blakenbauer: chce się spotkać z Amelią Eter ponownie. Jakoś. Obudziła się w nim anarchistyczno-poszukiwawcza dusza. Wanderlust.

## Frakcji



# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Somnium, gdzie pojawiają się też odbicia Damy w Czerwonym, choć nikt nie wie o co chodzi.
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Czelimin
                    1. Hotel Pracowniczy, recepcjonista jest Dotknięty przez Arazille. Mieszka tam Amelia Eter.
                    1. Kościół Neoromański, miejsce najdalsze Pryzmatycznie od Arazille, Skażają je regularnie fabokle.
                    1. Teatr Wiosenny, ulubione miejsce Amelii Eter w Czeliminie; ma tam kanciapę i chowa tam Srebrne Lustro.
                    1. Park Czelimiński, miejsce konfrontacji Amelii, Siergieja i Aliny. Spory, zwykle są tu przede wszystkim dzieci.
                    1. Klub Biała Róża, bardziej ekskluzywny i wymaga zaproszenia. Raczej dla turystów i to takich z kasą.

# Czas

* Opóźnienie: 0
* Dni: 1

# Narzędzia MG

## Opis celu Opowieści

* Pierwsza Opowieść Draży
* Czy Maria wpadnie w ręce Świecy, KADEMu, Silurii, Arazille?

## Cel Opowieści

* generacja karty postaci < 15 minut
* main character: Draża
* ma się graczowi podobać

## Po czym poznam sukces

* 15 minut ;-)
* gracz wywrze Wpływ na rzeczywistość.
* powie że było fajne

## Wynik z perspektywy celu

* karta powstała w 12 minut: sukces
* Dama w Czerwonym. Zupełnie inna ścieżka niż wymyślona przeze mnie.
* graczowi się podobało, pisał na komunikatorze wielokrotnie ;-).

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

* obecność Eskalacji: 2-2-2.

Wpływ:

* 10 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

