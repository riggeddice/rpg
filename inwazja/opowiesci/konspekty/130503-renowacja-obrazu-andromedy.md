---
layout: inwazja-konspekt
title:  "Renowacja obrazu Andromedy"
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [170816 - Na wezwanie Iliusitiusa (An, MB)](170816-na-wezwanie-iliusitiusa.html)

### Chronologiczna

* [170816 - Na wezwanie Iliusitiusa (An, MB)](170816-na-wezwanie-iliusitiusa.html)

## Ważni NPC:

- Augustyn Szczypiorek (83 l) : nestor rodu, cel portretu
- Żanna Szczypiorek (68 l): żona Augustyna
- Radosław Szczypiorek (44 l) : syn Augustyna i Żanny

- Antoni Wójt (38 l) : nieistotne na razie
- Jolanta Wójt (39 l) : córka Augustyna i Żanny
- Inga Wójt (19 l) : wysportowana "niszczycielka obrazów"

- Kot "Puszek" (6 l) : lokalny kot domowy, ulubieniec Ingi

## Misja właściwa:

Pierwsza misja mikrokampanii "Zniszczony obraz". 
Postacie kampanii i cele będą wrzucone przy misji ostatniej - najpewniej czwartej.

- Radosław wysłał maila do Andromedy z prośbą o to, by naprawić uszkodzony portret. Dwa lata temu stworzony, został uszkodzony. Oczywiście, nie ma szczególnie detali. Kasi wydało się to wszystko troszkę dziwne. Kontakt jest, płacą dobrze, jest telefon, są pieniądze. Znajomy terminus nadal w areszcie domowym.
Cóż, o nic nie można się zaczepić.

- Kasia zadzwoniła do domostwa. Odebrała Żanna. Kasia poprosiła o Radosława. Żanna spytała o co chodzi - dowiedziała się, że obraz. Który? Ten Andromedy. Jest uszkodzony, więc została poproszona o naprawienie go. Aha... Żanna przełączyła na Radosława.

- Radosław w rozmowie z Kasią zaznaczył, że zależy mu na dyskrecji. Ta zauważyła, że Żanna już wie i Radosław się rozdrażnił - nie na Kasię, ogólnie. Próbował ukryć irytację, ale nie wyszło - widać, że nie nawykł do ukrywania uczuć. Jest bogaty. "Jak nie mogę mieć dyskrecji, chcę mieć prędkość. Grunt, by ojciec się nie dowiedział". 
Kasia pojawi się następnego dnia. Dowiedziała się, że obraz został uszkodzony cięciem nożem.

- Kasia pożegnała się z Samirą i pojechała do Piwnic Wielkich. Swoim vanem.

- Wpuścił ją do domu lokaj, który zaprowadził ją po drodze do Radosława. Po drodze prawie zdeptała Kasię biegnąca Inga (szybka i bardzo zwinna). Radosław spotkał się z Kasią i powiedział, że "jej" obraz został zniszczony przez Ingę jakiś rok temu, gdy miała napad wściekłości na dziadka. Zniszczony... cios nożem był naprawdę solidny, zadany w czystej furii. Lepiej zrobić nowy niż naprawiać ten.

- Na to wszedł dziadek (Augustyn) i spytał, czego ma nie wiedzieć. Radek się wycofał - widać jego strach. Kasia odparła stalową wolę Augustyna i stwierdziła, że nie on jest jej klientem. Na to Augustyn rozpoznał, że zna jej głos. Był kiedyś jej klientem. Radek przyznał, że Kasia jest agentką Andromedy i miało dojść do dyskretnej renowacji obrazu. Ten moment Kasia wybrała na pokazanie zdjęcia z którego malowała oryginalny obraz - ten obraz nie jest Andromedy, mimo, że jest tam podpis Andromedy.
Fun fact - Augustyn wygląda na dużo, dużo młodszego w rzeczywistości niż kiedyś.

- Augustyn zażądał informacji od Kasi - ma może faktury itp? Tak. Poprosił, by pojechała i odesłała mu to. Teraz on dowodzi śledztwem. Ktoś zgubił jego obraz i załatwił na to miejsce bezczelną podróbkę. Kto?
Ktoś, kto to zrobił musi zapłacić. Jest od teraz za to nagroda.
Kasia dostała zniszczony obraz. "W końcu Andromedy". Augustyn sobie nie życzy policji, ma swoje metody.

- Kasia, dobrze opłacona, wróciła do siebie, zaopatrzona w nowy obraz. I to nie swojej roboty. Zauważyła, że nie ma nic srebrnego w tej części domostwa Augustyna w której się znajdowała... a samo w sobie to jest dziwne.

- Oki. 2 sukcesy (#3 za seniora) - Kasia podczas analizy obrazu dostała odpowiedź na pięć nietrywialnych pytań:
 - to nie jest obraz Kasi. To nie ona go zrobiła. Jest to zupełnie inny obraz niż ten oryginalny, tyle, że podobny - gdyby opisywać słowami, są podobne, ale to wszystko.
 - Kasia poznaje styl malowania. Potrafi przypisać do kręgu malarzy w Polsce. Wie mniej więcej którzy z malarzy malują w ten sposób.
 - Porównując z galeriami Kasia zlokalizowała z imienia i nazwiska malarza, który jest twórcą tego obrazu.
 - Jedyna "fałszywka" na tym obrazie - ktoś inny niż malarz (twórca obrazu) zrobił podpis "Andromeda".
 - Na obrazie  Augustyn wygląda na coś pomiędzy aktualnym wyglądem wiekowym a tym, jak wyglądał u Andromedy. On się "odmładza" z czasem?
 - Ten obraz został zniszczony przez bardzo silną osobę. Mała szansa, że to Inga mogła to zrobić.
Tu Kasia przeprowadziła eksperyment - zrobiła odpowiednik tego płótna i kilku warstw farby. Chce dać to mężczyznom by im pokazać, że Inga nie miałaby szans tego rozwalić.

- Dzień następny. Jest to na drugim końcu Polski, Kasia w pociągu. Pojechała do malarza (Artura Szmelca) z prośbą o informacje odnośnie obrazu. Mechanicznie Kasia wymiotła (3 sukcesy Sr = 6 vs 1 sukces Jr = 2), więc bardzo się do niej optymistycznie Artur nastawił.
Jak każdy artysta, Artur jest dumny ze swojej roboty. Jak dowiedział się, że ktoś podpisał JEGO obraz, był bardzo pomocny. Ten konkretny obraz został zamówiony przez Żannę, miał nie być podpisany. Adres docelowy był jednak inny - adres nie był do domostwa a innego miejsca.
Artur nie ma nic przeciwko, by jego nazwisko wypłynęło. "Zbrodniarz musi zapłacić". Ktoś podpisał JEGO obraz...

- Jeszcze następnego dnia Kasia wróciła... a w mieszkaniu - demolka. Zapach chloroformu, nie ma nigdzie Samiry <jak to było? Nie traktujmy NPCów jako obiekty do porywania?>. Są ślady walki. Mieszkanie zostało przeszukane. W tym momencie zadzwonił telefon komórkowy Samiry, który leży na biurku.
Kasia zignorowała telefon Samiry. Jeśli porywacze nie powiedzą czego żądają, nie mogą niczego od niej zażądać...

- Kasia niczego nie rusza. Przegrzebane są wszystkie papiery, zniszczone parę luster (widać, ktoś chciał informacje od Samiry). Dwa magiczne lustra jednak są tutaj, na miejscu. Są zbyt mało efektowne by je psuć. Wszystkie papiery powiązane z dokumentacją i z obrazami zostały gruntownie przetrzepane i przeglądnięte. Pieniądze (niewiele) zostały ukradzione, część bardziej efektownych rzeczy Samiry też. Kasia nie ma efektownych rzeczy.

- Kasia się oddaliła w miejsce, gdzie trudno ją podsłuchać. Pierwszy telefon - policja i zgłoszenie włamania z potencjalnym porwaniem. Drugi - Augustyn. By postawić go przed faktem dokonanym. W tej rozmowie Kasia powiedziała Augustynowi, że Samira została porwana i że zadzwoniła na policję. Augustyn się wściekł. Jeśli to jego ludzie, będzie wszystko załatwione bo on się dowie.

- Kasia parę następnych godzin odpowiadała na pytania policji. Po drodze zadzwoniła do Artura Szmelca i ostrzegła, że u niej było włamanie, potencjalnie powiązane z tym obrazem.

- Wieczorem, jak już została przemaglowana przez policję, Kasia siadła przed (zwykłym, niemagicznym!) lustrem i zażądała od niego wizji Samiry (skupiła się na nim). BARDZO zdziwiła się, że jej się udało. Na początku widzi tylko zapłakaną, przerażoną Samirę w ciemnym pomieszczeniu, z zasłoniętymi oczami. Jednak Samira, przerażona jakkolwiek by nie była, dała radę ukrytym nożykiem rozciąć szmatę i częściowo się uwolnić, sprawiając ciągle pozory bycia uwięzioną i bezradną.

- Potem jednak Kasia słyszy podniesiony głos Żanny zwracającej się do kogoś "I co? Taki byłeś mądry i znowu ja muszę sprzątać! Co ty w ogóle myślałeś?!". Starsza pani zeszła w dół, w kierunku Samiry, która się na nią rzuciła z nożem. Żanna - czarodziejka mocno Skażona - zareagowała instynktownie. Ta ręka w bandażach zmieniła się w płomienną szponiastą łapę i uderzyła Samirą o ziemię, odbierając jej dech i zaczynając ją dusić. Szybko się zorientowała co robi i przerwała.
Widząc przerażoną Samirę (której wszelkie myśli o stawianiu oporu właśnie wyparowały), Żanna rzuciła zaklęcie przy wykorzystaniu kryształów Quark. Quarki zniknęły a na twarzy Samiry pojawił się spokój. Jednocześnie Kasia zaobserwowała na ręku Żanny (tej z płomieni) symbol tożsamy z symbolem na szyi Augustyna.
Żanna wyszła z pomieszczenia mówiąc pogardliwie "wasza kolej". Dwóch thugów wzięło Samirę i pojechało z nią na Lublin.
Kasia chciała połączyć się z Samirą by sprawdzić, jaki efekt zaklęcie na nią wykonało. Niestety, nie wyszło - a jedynie "zarejestrowała w efemerydzie nowe lustro". Przez moment 11 różnych twarzy przebiegło jej przez lustro, każda krzycząc. Jedną z twarzy była twarz Żanny (ale w wieku ~20~25), co Kasia dostrzegła. #1 FuckUpOfTheDay.

- Kasia zadzwoniła do terminusa. Teraz to poważna sprawa. Tzn. "na bazie mojego doświadczenia wydaje mi się, że tam jest jakiś mag. I porwali mi Samirę.". Terminus ponegocjuje z mistrzem zwolnienie z aresztu domowego, ale rano. Nie będzie go budził teraz, bo mistrz go zabije. A lubi spać. Kasia rozumie...

- Kasia NIE chce być w domu gdy Samira wróci. Jeszcze ją porwą. Albo... co to zaklęcie zrobiło? Czarodziejka która je rzucała nie wyglądała zachęcająco i sympatycznie. Nie chce też być w vanie. Zamiast tego woli się oddalić do losowego motelu. Wysłała tylko terminusowi SMSa - "Nie idź do mieszkania beze mnie. Zadzwoń jak będziesz.". Kasia nie chce by mu się coś stało...

- W motelu, podczas mycia twarzy między mrugnięciami Kasia widziała twarz mężczyzny. Jeden z tych, który krzyczał. Ślad efemerydy.
Kasia nie chce patrzeć w żadne lustro. Przynajmniej na razie.
Kasia nie śpi. Czeka do rana. W motelu. Nie patrząc w lustra ani odbijające się powierzchnie.

- 9:00 następnego dnia. Dzwoni Samira. "Zaszalała trochę w nocy". Kasi nie było i Samira serdecznie przeprasza - trochę zabalowała i zdemolowała troszkę mieszkanie. Ma głos, jakby z kimś się przespała i się przy okazji świetnie bawiła. Kasia jest podłamana całą tą sprawą. Nagle - Samira krzyknęła w panice. Ona też widziała jakieś twarze w lustrze. Ale to jej się zdawało...
Akurat zdawało. Efemeryda, cholera. Kasia tam natychmiast jedzie.
Ale wpierw - telefon z Policji. Samira tam zadzwoniła i odwołała wszystko. Przeprosiła za fałszywy alarm.

- Kasia zadzwoniła do Augustyna - Samira się znalazła, nie ma problemów. Mówi, że nie jest porwana i zdaniem Kasi porwana chyba nie była. Przeprasza za fałszywy alarm. Augustyn mówi, że nic złego się nie stało - woli wiedzieć, że ktoś dla niego pracujący może mieć problemy. Kasia umówiła się z nim na następny dzień. Chce mu powiedzieć, czego się dowiedziała.
Augustyn poprosił, by Kasia oddała mu wszystkie jego zdjęcia za pewną, niemałą kwotę (dekompletowanie archiwum). Kasia zapewniła, że nie udostępnia tych materiałów.

- Kasia wróciła, w domu nie ma jeszcze terminusa ale jest Samira. Wszystkie lustra odwrócone do ściany. Samira, zakrwawiona w rękę, w pościeli i płacze.
Widziała "coś" (efemerydę) w lustrach. To coś zmusiło ją do wycięcia sobie rany na ręce (kiepski rysunek tego medalionu). Udało jej się jakoś "wyrwać". Gdy Kasia opatrywała ranę Samiry, dostała powerup mocy... poczuła większą moc i większą energię. I czyjeś obce uczucie, wściekłość skierowaną w kierunku tych luster i tego co jest "w nich". Szybko uczucie odeszło a Kasia zorientowała się, że ma na skórze krew Samiry.
Poszła ją zmyć.

- Kasia wzięła Samirę na spacer, czekając na terminusa.

- Terminus przybył. Zgodnie z SMSem Kasi, wziął kwiaty dla Samiry. I przeprosił. Nie, nikt nie wie za co, nawet terminus (ale Kasia kazała). Weszli do mieszkania. Natychmiast terminus wysłał je na spacer, bo mają tu dość wredną efemerydę i on się chce jej pozbyć.
Jak powiedział, tak zrobił. Efemeryda została zniszczona przez terminusa. Było bardziej efektownie niż chciał. Widział przewijające się twarze, ale uznał za element efemerydy.

- Po zniszczeniu efemerydy Samira wróciła do porządkowania luster. Terminus uznał, że coś magicznego musiało ściągnąć efemerydę i zaczął szukać magicznych przedmiotów. Znalazł - lustro niszczące. Powiedział, że to jest jedyny magiczny przedmiot jaki Kasia posiada.
Kasia powiedziała, że to nie to. Ona to już miała. Efemeryda przyszła z Samirą.
Terminus wyekstrapolował, że tam musi być potężne źródło mocy. On potrzebuje wsparcia z zewnątrz by sobie z tym poradzić. Coś z góry.
Terminus obiecał, że poszuka czegoś powiązanego z tym medalionem. Ten medalionik wygląda na zbyt... specyficzny.

----

* Dwa lata temu Augustyn zamówił u Andromedy obraz:
 * Augustyn siedzący na obitym krześle
 * Augustyn miał w ręce wielki, srebrny, zabytkowy krucyfiks
 * Na szyi Augustyn miał medalionik przedstawiający kapłana z dłonią w górze
 * Portret ma napis "Wiara, Polska, pamięć"

* Obraz, który dotarł do domostwa Augustyna:
 * Augustyn siedzący na obitym krześle
 * Augustyn ma na kolanach ukochanego kota
 * Na szyi Augustyn ma srebrny krzyżyk
 * Portret ma napis "Wszystko zdobył samemu"

----

Zdanie dnia:
"Kasina paranoja for the win! Samira nie wie nic o magicznych lustrach :-)"

----
**Z rozmowy Zleceniodawcy z porywaczami Samiry**

- Co to znaczy: nie odbiera telefonu?
- Zwyczajnie. Nie odbiera.
- Coś spieprzyliście! _Musi_ odebrać! Skąd będzie wiedziała, że jej przyjaciółka żyje?
- Nie wiem. Nigdy... nie spotkałem się z czymś takim.
- Amatorzy! Ile porwań do tej pory zrobiliście? Dziesięć? Piętnaście?
- Ani jednego.
- Chciałem profesjonalistów! Szybkich i niekoniecznie tanich! Najlepszych, jakich mamy!
- Sir, w naszych obowiązkach zwykle nie leży porywanie. Do tej pory działania, które przeprowadzaliśmy wątpliwe legalnie, ale poprawne moralnie. 
- A idź mi do cholery z taką... Dzwońcie do niej.
- Nie odbiera.
- Może jej nie ma?
- Na pewno jest. Wynajął pan profesjonalistów, sir.
- A dziewczyna? Ta, Samira, powiedziała coś?
- Nic nie wie, sir.
- Ojejku, potnijcie ją, przypalcie. Ma powiedzieć prawdę!
- Nic nie wie, _sir_!
- Wydałem wam rozkaz. 
- Tak jest, sir.

----
**Z rozmowy dwóch agentów torturujących Samirę, gdy straciła przytomność**

- Ona nic nie wie.
- Wiem. Rozkazy.
- Nie podoba mi się to. Kiedyś tak nie było.
- Nie, nigdy tak nie było.
- Co robisz?
- Zostawiam jej mały prezent. I tak jest nieprzytomna. Mogła schować.
- Ale... nożyk?
- Po torturach, kto zwykle schodzi zobaczyć, czy jego ofiara została dobrze skrojona?
- Zwykle jakiś psychol... 
- No właśnie... Może ta mała znajdzie w sobie tyle odwagi, by zmniejszyć ilość psycholi na tym świecie.
- Hej! Nie boisz się? Wiesz, ile kasy mają Szczypiorki?
- Nie boję się. Długo służyłem. Z tym nie chcę mieć nic wspólnego.

----
**Z myśli cierpiącej Samiry, samotnej**

Boli... 
Nic mi nie złamali. Nie to, że się nie starali...
A tego z papierosem naprawdę nie doceniłam...
Przynajmniej wiem, że żywa stąd nie wyjdę.
Nie chcę umierać... _szloch_

A to co? Nożyk? Może to pułapka? Co mam do stracenia?

----
**Z myśli Samiry gotowej do ataku**

Słyszę. Ktoś schodzi. Jeszcze parę kroków...
Teraz!
Stara baba?
o...
_trzask_
_wrzask_

----
**Z myśli wzburzonej Żanny, podczas rzucania zaklęć na Samirę**

No i co oni ci dziecko zrobili... Czy ci barbarzyńscy rzeźnicy nie rozumieją, że co oni uszkodzą, ja muszę naprawić? Siniaki, rany od ognia, tyle strachu... A można było po prostu mnie poprosić.
Cała ta sprawa jest po prostu bez sensu... 
Porywanie niewinnej dziewczyny z ulicy, ściąganie znanego malarza do renowacji nie swojego obrazu... Zawracanie głowy Augustynowi... 
Niech no ja się tylko dowiem, kto za tym wszystkim stoi...
Cśśśś, dziecko, śpij... I miej piękne sny... Koszmar minął i już nie wróci...
Ja to jakoś rozplączę...

----
**Z myśli terminusa, po zniszczeniu efemerydy**

Niepokojące. Ta efemeryda była dziwna. Nie do końca rozumiem, jakim cudem powstała. Co gorsza, nie powinna móc się tu zjawić. Wyraźnie podróżowała przez lustra... A wszystko związane z lustrami jest śmiertelnie niebezpieczne.
Sprawdziłem jeszcze raz Samirę, tak dokładnie, jak tylko byłem w stanie. Nic. Nie posiada magii. Nie potrafi czarować. Moc się dookoła niej nie zakrzywia. A jednak efemeryda jakoś się tu pojawiła.
Czyżby... 
Czyżbym od samego początku skupił się na niewłaściwej kobiecie?
Czy to Katarzyna jest czarodziejką?

----
**Z myśli terminusa po przejrzeniu luster Kasi**

Jak ja to przeoczyłem? Święty padalcu! To lustro jest ukierunkowaną bronią masowego zniszczenia! Ma na tyle skomplikowany wzór, że nie potrafiłbym go co prawda użyć, ale każdy mag dostrojony do tego lustra potrafiłby ściągnąć energię z otoczenia. Zostały jeszcze trzy strzały. To oznacza, że co najmniej trzech magów może wyparować... 
Sprawdziłem każde lustro w tym mieszkaniu. To ostatni magiczny przedmiot. No po prostu nie ma więcej.
Jak to zaniosę mistrzowi, będzie miał dowód na to, że musiałem się wyrawać z aresztu domowego.

----
**Z myśli Kasi po badaniu luster przez terminusa**

Cholera! Trzeba było je wywieźć! Ale niby gdzie...
No nic, stało się. Może jak mu rzucę coś innego, to zostawi moje lustro w spokoju...
Grzeczny terminus, popatrz, jaki fajny kąsek ma dla ciebie Andromeda...

----
**Z myśli Sandry Stryjek, młodej czarodziejki**

Co? Zwolniony z aresztu domowego? I ja o tym nic nie wiem?
Znowu wpadnie w jakieś kłopoty. 
Muszę lecieć...
Oczywiście, nie może mnie zobaczyć. Jeszcze zrobi jakieś głupstwo...

----
**Z myśli mistrza młodego terminusa**

Młodzi... 
On jej w ogóle nie zauważa. Przy całym swoim braku szczęścia jest na tyle dumny, by nie móc poprosić o pomoc. 
Przy swoim braku jakiegokolwiek znaczenia w rodzie magów ona jest zbyt dumna, by poprosić go o współpracę.
Sprawę komplikują też uczucia.
Ona jest nim zainteresowana, on tego zupełnie nie zauważył.
Myślałem, że areszt domowy mu pomoże. Myliłem się. Jego obsesja na punkcie luter tylko rośnie. Nie będzie niekorzystne, jeśli pozwolę mu udać się gdziekolwiek chce i delikatnie zasugeruję Sandrze, gdzie się będzie zajdował. Ona powinna być w stanie wytrącić z równowagi nawet - a może zwłaszcza - tak poważnego, nudnego i kierującego się ideałami terminusa.

----
**Z myśli Sandry, po dotarciu do Lublina**

Mam go. 
Głupek. Tak się naśmiewał z mojej współpracy z Powiewem Świeżości. Tak się naśmiewał i gardził technologiami ludzkimi. I co? Sam nie umie żyć bez luksusu. Tylko on zdominował jakąś nieszczęsną kobietę.
...
Tam są dwie.
...
Przejmowanie kontroli nad ludźmi jest niemoralne i powinno być zakazane!
Zwłaszcza w takich celach! ...W takich celach..!
Próg aury magicznej który stamtąd dobiega jest...
Dobra.
Pobawię się w zbieranie dowodów.
Niech mnie błaga na kolanach, by nikt się nie dowiedział, jak wiele procedur złamał!
...
Ale one nie są ani młode ani ładne!

----
**Z rozmowy Kasi z terminusem**

- August... mam pytanie. - Głos Kasi był spokojny, ale dało się w nim wyczuć pewne napięcie.
- Słucham. - czarodziej masował sobie skronie.
- Czy jeśli Ci coś powiem... możesz przyjąć to bez pytania o to, skąd wiem? I możesz dać mi słowo, że mnie nikomu nie zdradzisz?
- A Ty, dasz mi słowo, że pozbędziesz się wszystkich magicznych przedmiotów i powiesz mi wszystko o magii, co wiesz? - Terminus spojrzał na Kasię ostro. - To lustro było używane. Musiałaś wiedzieć, że to magiczny przedmiot.
- Wiedziałam. Nie pozbędę się go - o to jedno mnie nie proś. Ale powiem Ci, co wiem... jeśli nie będziesz wnikał, skąd i dlaczego.
- Nie pomagasz mi! - Sfrustrowany czarodziej wyrzucił ręce w powietrze. - Dlaczego ciągle utrudniasz mi życie? Wiesz, jak mnie narażasz?
- Nie musisz mi pomagać. Zostaw telefon. Odejdź. Zapomnij o mnie i o Samirze. Nie chcę Cię narażać. W świecie magów masz karierę przed sobą. Nie chcę jej zniszczyć. - Kasia mówiła spokojnie.
- Akurat... - Westchnął terminus. - Nie mogę wymazać Ci pamięci. Każdy odczyta Ci pamięć i wyjdzie na mnie.
- Jeśli o to chodzi, to nie masz się czym przejmować. Jak dotąd żadnemu magowi nie udała się ta sztuka... - August spojrzał na Kasię przerażony. 
- Czym Ty jesteś? - Kasia wzruszyła ramionami.
- Pechową dziewczyną, która próbuje przetrwać w świecie, którego nie rozumie. Nie prosiłam o to. Staram się nikomu nie przeszkadzać i nie robić krzywdy... Pomóc w miarę możliwości. Niewiedzia utrudnia, ale jak dotąd jakoś przeżyłam...
- Jeżeli Twojej pamięci nikt nie przeczyta... Ale mogą przeczytać moją! - jęknął w rozpaczy.
- Tylko, jeśli dasz im podstawy. A i wtedy można trochę wpływać.
- Nie rozumiesz... - terminus się załamał. - Po tym, co mi teraz powiedziałaś, mam przed sobą wybór.
- Wiem. I powiedziałam to z całkowitą świadomością tego, co to oznacza. Zrobisz, co zechcesz.
- Ale ja powienienem Cię wydać! Muszę! - Terminus ukrył twarz w dłoniach.
- Jeśli musisz... Ale w takim razie lepiej szybko mnie zabij... Nie chcę wpaść w ręce waszego odpowiednika naukowców. - Kasia odwróciła wzrok.
- Ja... podważasz wszystko, w co zawsze wierzyłem... - Dziewczyna nie odpowiedziała. W oczach terminusa pojawiły się łzy bezsilności. - Zostaw mnie na razie. Muszę pomyśleć.

----
**Z myśli Kasi**

No i kiedy ja mu to powiem...?
Musi się dowiedzieć, co zobaczyłam w lustrze. Ta czarodziejka...
Bandaż na ręce, kształt medalionu prześwitujący przez opatrunek, wydłużenie ręki, osłabienie po zaklęciu...
Ona coś zrobiła z Samirą...
Na pewno ją uśpiła. Nie wiem, co jeszcze...
Chcę powiedzieć o tym wszystkim Augustowi, ale nie chcę, żeby on tam wparował niczym bóg zemsty... 
Zwłaszcza, że mam wrażenie, że ona jest od niego silniejsza.
A co, jeśli to nie ona jest winna? Może jest tam jeszcze ktoś..?
Albo jeśli to po prostu stara kobieta próbująca ratować ukochanego człowieka?
Nie chcę, żeby August wpadł tam bez przemyślenia sprawy.



# Zasługi

* czł: Kasia Nowak jako dowód na to, że gdzie mag nie może, tam się babę pośle
* czł: Samira Diakon jako żywy dowód na to, że NPCów nie powinno się porywać
* mag: August Bankierz jako terminus który wybłagał zwolnienie z aresztu domowego
* czł: Augustyn Szczypiorek, (83 l) nestor rodu, któremu zrobiono portret. Wygląda, jakby młodniał z czasem. Trzęsie rodem.
* mag: Żanna Szczypiorek, (68 l) żona Augustyna i wyraźnie czarodziejka, choć ukrywa to bardziej niż przeciętny mag.
* czł: Radosław Szczypiorek, (44 l) syn Augustyna i Żanny, który chciał zrobić ojcu prezent w formie renowacji jego portretu
* czł: Inga Wójt, (19 l) wysportowana "niszczycielka obrazów" biegająca po domu jak szalona.
* czł: Artur Szmelc, dumny i kompetentny malarz, któremu ktoś podpisał JEGO obraz


# Lokalizacje

1. Piwnice Wielkie
    1. Domostwo Szczypiorków
