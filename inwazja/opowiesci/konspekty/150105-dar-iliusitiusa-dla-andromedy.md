---
layout: inwazja-konspekt
title:  "Dar Iliusitiusa dla Andromedy"
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [150104 - Terminus-defiler, kapłan Arazille (An)](150104-terminus-defiler-kaplan-arazille.html)

### Chronologiczna

* [130511 - Ołtarz Podniesionej Dłoni (An)](130511-oltarz-podniesionej-dloni.html)

### Logiczna

* [150104 - Terminus-defiler, kapłan Arazille (An)](150104-terminus-defiler-kaplan-arazille.html)

### Opis

Ołtarz Podniesionej Dłoni jest potrzebny do zrozumienia postaci.
Terminus-defiler jest potrzebny do zrozumienia przyczyn przybycia Kasi do Piwnic Wielkich i jej celów.

## Misja właściwa:

Andromeda zbliża się do Piwnic Wielkich samochodem. W samochodzie Andromeda, August, Samira oraz Sandra. Andromeda nie zabrała ze sobą żadnego maga, którego uznała za zagrożenie (August jest kontrolowany nie tylko mocą Iliusitiusa ale także mocą luster, o czym nikt poza Andromedą nie wie). Plan jest prosty - wpaść do rezydencji Szczypiorków, zabrać Ołtarz Podniesionej Dłoni i się stamtąd ewakuować. W samochodzie nastroje są raczej optymistyczne... poza Kasią. Ta ma bardzo zły nastrój. Amulet lekko pulsuje, jakby wiedział, co Kasia chce zrobić.
Sandra uprzednio zmodyfikowała pojazd Kasi - jest on dużo pancerniejszy i odporniejszy niż wcześniej. W końcu Sandra specjalizuje się w technomancji i w naprawie samochodów ;-). Zrobiła z niego jeżdżący bunkier i wyposażyła go w dodatkowe czujniki.
Ołtarz był ukryty w rezydencji Szczypiorków w trzecim najmniej oczywistym miejscu. Schowany za kominkiem w pokoju Ingi, na piętrze.

Z uwagi na to, że rezydencja Szczypiorków była bardzo dobrze zabezpieczona technikami ludzkimi (alarmy, czujniki) jest to zadanie dla Sandry. Kasia poprosiła Samirę, by ta pomogła Sandrze. Sandra będzie rozmontowywała zabezpieczenia a Samira pomoże w odwracaniu uwagi. Jednak August miał inny pomysł - wszyscy się wkradną do domostwa Szczypiorków a samochód zostanie tak, by nikt nic nie zauważył. Kasia zaproponowała zmianę tego planu - Samira zostanie w samochodzie jako kierowca i jednostka odwodowa. Kasia spytała też, czy Samira ma dostawcę luster w Piwnicach Wielkich. Ta potwierdziła. Kasia zaproponowała zaopatrzenie się w lustra.
Pojechali do kolekcjonera antyków (Józefa Pasana) i Kasia poprosiła Samirę, by ta wybrała lustra. Samira wybrała sześć luster, po czym poprosiła o jeszcze jedno, lustro składające się z czterech innych luster. Józef Pasan spytał skąd ona wie, że ma takie lustro. Samira się uśmiechnęła i powiedziała, że nie wie, ale MUSI mieć takie. Kasia się zaśmiała. Już czuje się bezpieczniejsza.
Amulet pulsuje mocno. Iliusitius jest blisko.

Samira poprosiła Józefa Pasana, by ten pozwolił im zostawić samochód na jego podwórku. Do domostwa Szczypiorków jest 15 minut spaceru. Co prawda dość daleko, ale Sandra zamontowała w samochodzie systemy kontroli zdalnej i zostawiła pilota Kasi. Czyli Kasia jest w stanie wezwać samochód do siebie. Jakkolwiek Sandra jest kompentną infomantką i astralistką, technomancja a zwłaszcza samochody to jej konik. Samira powiedziała, że to duże czterodzielne lustro może zostać w samochodzie. Kasia dała Sandrze i Augustowi po jednym małym lusterku. Sandra miała opory, ale Kasia bardzo usilnie nalegała posuwając się do kłamstw, że August upewnił się że to zapewni jej bezpieczeństwo bo Iliusitius nie dogaduje się z lustrami które raczej są bliskie Arazille... (8#1v5#2->9) Sandra z oporami, ale wzięła lustro. August przemilczał Kasine kłamstwo. Wie lepiej niż coś mówić w takich okolicznościach...

Dotarli do domostwa Szczypiorków. Sandra u wejścia, przygotowując się do rozbrojenia alarmów. August monitorujący sytuację. (12/k20). Sandrze udało się rozbroić zabezpieczenia magicznie tak, by cały system wyglądał jakby działał. August powiedział, że ta sama metoda zadziała na Triumwirat. Kasia zauważyła, że Triumwirat musi być troszkę potężniejszy niż Zofia Szczypiorek. Sandra dostała zadanie przygotować możliwość odpalenia alarmu jako dywersji w innym miejscu budynku.
Przez podwórko Szczypiorków przebiegło kilka szczurów.
Augustowi się to bardzo nie spodobało. Powiedział, że pierwszą rzeczą której uczą terminusa to obserwować zwierzęta i rośliny - byty prostsze są podatne na pola magiczne. A to jest zachowanie niewłaściwe ze strony zwierząt.
Kasia westchnęła. Podczas całej tej akcji August JUŻ zrobił więcej niż Herbert na całym Festiwalu...

Weszli do domostwa Szczypiorków. Nagle Kasia miała flashback. Zamast iść w domostwie widziała korytarz szpitalny a tam "swoją" Diakonkę, która machała do niej i powiedziała "chodź ze mną". Kasia była w stanie się przyjrzeć tej Diakonce - koło 40-50 lat, zachęcająco uśmiechnięta. Flashback minął. Diakonka jednak nie prowadziła jej do pokoju Ingi. Prowadziła ją do atrium. Tam, gdzie ostatnio Kasia rozmawiała z Augustynem Szczypiorkiem. Ale nie tam gdzie zginął.
August stwierdził, że cały ten budynek jest pod stałym wpływem magii przestrzeni. Cały ten budynek jest zakrzywiony przestrzennie, sam w sobie.
Kasia ma dobry humor. Nic się nie przejmuje. Idzie na atrium i mówi zespołowi, że musi coś sprawdzić.

Atrium. Siedzi tam śpiąca, zmęczona Zofia. Kamerdyner idzie w kierunku Zespołu. Koło Zofii stoi tajemnicza kobieta w welonie i nalewa Zofii herbatę. Widząc ją w głowie Kasi odpaliły wszystkie flary alarmowe. Nie wie czemu, ale NIE chce jej zobaczyć. Wie tylko jedno - to nie jest "jej" Diakonka. Po atrium przebiegły trzy szczury.
Kasia spojrzała na nich w lustrze. Wyglądają tak samo. Ale lustro pulsuje wewnętrznym światłem. W rytmie amuletu na piersi Andromedy, który to rytm jest rytmem czyjegoś serca.
Kasia się wycofuje z Zespołem. Idą w kierunku na pokój Ingi.

Zespołowi udało się opuścić (chyba niezauważenie) atrium i poszli w kierunku na pokój Ingi. Po drodze zobaczyli obraz. Kasia spojrzała na niego instynktownie bo się ruszał i obraz wciągnął ją do środka.
Kasia jest małą dziewczynką. Osiem lat. Stodoła z jej rodziną płonie, ale strażacy tylko się bezczynnie przypatrują. Kasia bije ich pięściami i płacze, ale nic nie robią. Herbert Zioło mówi do niej "dziecko, czy kochasz swoją rodzinę i chcesz ich uratować?" - dziewczynka potwierdza. Herbert każe Kasi skupić się na tym jak bardzo ich kocha i podaje jej jakąś pigułkę. Boli, ale mija. Potem Herbert każe dziecku pójść do stodoły, bo ogień się jej nie ima.
Kasia-dziewczynka przeszła przez płomienie. Przez iluzję ognia... prosto w macki kralotha. Kasia czuła każdą chwilę aż do śmierci dziecka. Po czym widziała jak śmierć dziewczynki spowodowała reakcję magiczną w kralocie i vicinius zginął. Herbert wszedł do stodoły i zaczął sprzątać by zachować Maskaradę.

Kasię wyrwało z obrazu. Jest wstrząśnięta, ale nie tak mocno, zupełnie, jakby coś ją chroniło. Ale widzi, że Sandra jest rozbita i płacze, Samira cała drży w ciężkim szoku a August patrzy i nie rozumie co się dzieje. Jego nie wciągnęło w obraz

"Czarodziejko luster... twoi magowie są tacy bohaterscy i szlachetni..." - Iliusitius (głos w głowie Kasi)
"Czy naprawdę uważasz że zrobienie im tego co robisz uczyni ich lepszymi?" - Kasia "w eter"

Kasia ma kolejny flashback. Jest na placu ze swoim chłopakiem, ich pierwsza randka. Pokazała mu swój obraz, to był moment, kiedy on jej wyznał miłość. Powiedział, że ją kocha. Ale znowu - Kasia nie potrafiła w pełni czuć. I nagle Kasia się zorientowała że obraz nie ma twarzy. Że jej chłopak nie ma twarzy. Kasia - Andromeda, osoba, która nie zapomina nie potrafi zapamiętać swojego ukochanego chłopaka! Pamięta wszystko poza tym.

Przed nimi schody prowadzące na piętro wyżej. Na schodach obraz, ale tak daleko, że trzeba podejść. Przez schody przebiegł pojedynczy szczur.

Przerażające jest to, że Kasia jest ciągle w dobrym humorze. A cały jej zespół jest coraz bardziej rozbity. August krzyknął w eter "kim jest czarodziejka luster!". Sandra płacze, że Herbert taki nie jest, czemu "on" to pokazuje. Samira zimnym głosem "bo jest okrutny. Znam okrutnych ludzi. Wiem jak myślą. On jest okrutny". 
August popatrzył na szczura na schodach. Powiedział, że to nie jest szczur. Jeśli Arazille ma jako swoje miniony fabokle... może Iliusitius ma szczury. 

"Wiem, że jestem niepełna. To nic nie zmienia." - Kasia do szczura, wyjaśniając, że wizje nie są w stanie zrobić jej krzywdy.
"Za co chcesz zemstę? Nie warto było odezwać się wcześniej?" - Kasia do odbiegającego po schodach do góry szczura.

Kasia próbuje uspokoić Sandrę. O dziwo, Samira jako człowiek przyjmuje to wszystko dużo naturalniej niż czarodziejka Sandra. Choć czy o dziwo? Iliusitius ma dużo większą moc na magów. Więc, Kasia próbuje użyć swoich umiejętności i uspokoić Sandrę. Poprosiła o pomoc Augusta, by wzmocnić się tym, co Arazille zrobiła Sandrze (5#4v5->sukces). Z pozbieraną Sandrą, Kasia decyduje się podejść do schodów i spojrzeć na kolejny obraz. Przedstawiający Andrzeja Szopa.

Kasia stała się żoną Andrzeja. Andrzej podał jej czarkę i kazał jej ją wypić. Powiedział, że dostał polecenie przeniesienia się i nie może zabrać ich ze sobą, bo mu kazali jechać samemu. To oznacza rozstanie. Kasia krzyknęła, że są ze sobą dziesięć lat. Andrzej powiedział, że takie ma rozkazy. Kasia chciała wziąć dzieci i wyjść ale Andrzej użył mocy magicznej by zamknąć drzwi. Powiedział, że przed budynkiem jest mag eksterminator. Jeśli ona wyjdzie, zginą. A tak, będzie żyła dalej. Nawet załatwił jej uposażenie (był taki dumny z siebie).
Kasia wzięła czarkę i wypiła mówiąc mu "rozkazy wykonywali też strażnicy w Auschwitz."

Kasia wyszła z wizji. To mocniej uderzyło w Samirę. W jej oczach pojawiła się nieznana Kasi wcześniej zimna drapieżność. Kasia poprosiła Samirę, by ta nie nienawidziła. Samira powiedziała "łatwo Ci mówić", po czym zaczęła Kasię bardzo przepraszać. Nadal, opanowanie Samiry jest bardzo duże.

"Same cele, same rozkazy, żadnej miłości, żadnej nienawiści, żadnych marzeń, czarodziejko luster..." - Iliusitius.
"Znasz mnie." - Andromeda.

Flashback. Kasia przypomniała sobie bardzo traumatyczne przeżycie. Pierwszy raz jak zorientowała się, że magia istnieje i że musi się rozstać ze swoim kochanym chłopakiem. Kasia obudziła się rano i spojrzała w lustro. Przypomniała sobie. Przypomniała sobie cienie, cienie, które wysysały ich właścicieli i przenosiły się między ludźmi, mówiąc im straszliwe szepty. Kasia pamięta, że była pod ich wpływem, pamięta te szepty, to poczucie bezradności i braku własnej wartości. Potem weszli magowie i wyczyścili wszystko i ich pamięć... Kasia zadzwoniła do chłopaka, ale on nic nie pamiętał. Potem zadzwoniła do przyjaciółki, ale ona mówiła, że była z ciotką u kuzyna. Wtedy Kasia zorientowała się, że tylko ona pamięta...
I rozstała się z chłopakiem by go chronić...
Ale zaraz potem Kasia zorientowała się, że nie pamięta przyjaciółki. Nie pamięta chłopaka. Jest tylko Pryzmat Myśli, a każda ścianka Pryzmatu to inna twarz. Inna opowieść. Kasia nawet nie pamięta chronologii, nie wie, czy poznała chłopaka przed zerwaniem z nim czy po... co nie miałoby sensu. Ale nie pamięta. Nie pamięta.

Kasia zaczęła cicho płakać gdy zerwała się wizja, ale nie wpłynęło to na jej dobre samopoczucie. Co nawet Kasię zdziwiło. To wszystko jest jak sen...

"Arazille oferuje Ci przyjemne złudzenia. Ode mnie dostaniesz w darze prawdę." - Iliusitius
"Nie wiesz co jest prawdą. Prawda jest względna" - Samira

Ponad nimi jest jeszcze jeden obraz, a potem wejdą na piętro, gdzie znajduje się pokój Kingi. Zespół jakoś zebrał się do kupy. Kasia zauważyła, że Iliusitius tylko mówi, ale nie odpowiada... zupełnie, jakby ich nie słyszał.

"Chcesz dać mi prawdę? Prawda jest tu i teraz" - Kasia do Iliusitiusa
"Cokolwiek się nie stanie, nie słuchaj go! Jesteś moją kochaną przyjaciółką" - Samira do Kasi.
"Iliusitiusie! Przestań znęcać się nad moimi przyjaciółkami!" - August w eter.
"Pozwól mi. Już dość długo chowam się za Twoimi plecami" - Samira do Kasi.

Samira spojrzała w obraz. Wizję dostała... Kasia. Bo tak.
Dla odmiany Kasia ma wybór. Stoi przed dwoma drzwiami. Może wybrać te, gdzie będzie czuła to co Patryk lub te, gdzie to co NIE Patryk.
Wybrała wejście jako Patryk Romczak.

Kasia stała w laboratorium z potężnym źródłem magicznym i podręcznym mentalistą. W komorach znajdowali się ludzie, min. ludzka forma Sandry, Samiry i Augusta. Kasia przeprowadzała liczne eksperymenty w obszarze tkanki magicznej, jej rozrostu, wpływu woli i sugestii na transformację, wpływ mocy... Kasia robiła z nimi różne rzeczy. Jak tylko nie mogła z tych ludzi już nic wycisnąć, używając mocy lifeshapera przekształcała ich w ludzkie kształty, blokowała im moce viciniusa i puszczała wolno po usunięciu pamięci. Przerażające jest to, że Kasia nie robiła tego dlatego, że musiała wygrać konkurs, że chciała kogoś wyleczyć... to była po prostu radość osoby poznającej coś nowego, poszerzającej SWOJĄ wiedzę. Te rzeczy były w większości zbadane, one są w księgach, w Wektorze Sigma... ale Patryk chciał SPRAWDZIĆ...
Przerażające jest to, że Kasia zrozumiała, że mimo Ołtarza Podniesionej Dłoni Patryk nic a nic się nie zmienił... i to samo poczuła Sandra i Samira...

Koniec wizji. I kolejna wizja tylko dla Kasi. 
"Daria! Nazwijmy ją Daria." - Samira do Diakonki którą kojarzy Kasia.
"Nie. Nazwiemy ją Kasia." - Diakonka do Samiry.
"Daria, ponieważ..."
"Kasia, bo jest osobą." - Diakonka, akcentując "osobą" do Samiry.
"Skoro tak chcesz..." - Samira się wycofała i spojrzała jastrzębimi oczami na Kasię. Takimi oczami jakie ma teraz.
Wizja minęła. Kasia wie, że to była prawda. Ale tego nie pamiętała.

Znowu, Kasia była częściowo uodporniona na czucie tego co czuł Patryk, ale Sandra i Samira nie. Gdy Kasia otworzyła oczy, August próbował zatrzymać Sandrę przed wydrapaniem sobie oczu a Samira wymiotowała w kącie.

"Samolubne działania; nieważne kto ucierpi, czarodziejko luster..." - Iliusitius.
"To! Nie! Tak! Było! Nie wierzę w to!" - szlochająca Samira.

Sandra została unieruchomiona przez Augusta a Samira doszła do siebie, choć ma oczy jak w wizji gdzie rozmawiała z Diakonką Kasi. Samira ma oczy zaszczutego zwierzątka gotowego do ataku na wszystko, ale się trzyma. Kasia wyciągnęła rękę do Samiry. Ta się nie odsunęła, ale zaczęły płynąć jej łzy. Kasia wyczuwa ogromne poczucie winy. 

"Samira... cokolwiek zobaczyłaś, cokolwiek sobie przypomniałaś..." - Kasia do Samiry.
"Cokolwiek to było... wybaczam Ci" - Kasia do Samiry, z całej głębi serca. 

Kasia zorientowała się, ku swemu ogromnemu zaskoczeniu, że nie jest w stanie "wybaczyć", bo nigdy nie miała potencjału na uczucia do tego stopnia. Nie możesz wybaczyć jeśli nie możesz nienawidzić. Kasia ma cele, nie marzenia.
Dobrze, ale jeszcze jest Sandra. Kasia nie może tak jej zostawić. Uderza w buntowniczą naturę Sandry. Powiedziała jej, że to prawda Iliusitiusa, nie "prawdziwa prawda". Kasia ma trudny wybór - okłamać Sandrę by ją uspokoić i kontynuować misję i cele lub powiedzieć prawdę z potencjalnymi konsekwencjami negatywnymi. Wybrała prawdę. Nawet, jeśli będzie to sprzeczne z jej celami (8v9->6). Niestety, Sandra spanikowała i weszła w berserk. August ją zatrzymał, zaczęła rzucać zaklęcie a Kasia przytrzymała Sandrę i August odebrał jej przytomność (terminus). Kasia zaczęła słyszeć szept. Niezrozumiały jeszcze, ale szept Iliusitiusa. Zaczyna się móc z nią komunikować.
Może Iliusitius ich spowalnia, by móc coś zrobić.
Kasia pomyślała o tej dziwnej kobiecie z welonem. Wcześniej miała fizyczną reakcję na myśl o niej... już mniejszą. Coś się zmieniło. W Kasi.

"Get the hell out of me!" - Kasia do Iliusitiusa.

Ostatni już portret i ostatnia wizja. Kasia weszła w wizję i zobaczyła czarodziejkę która opieprzyła swojego krewnego za to, że źle traktował służbę. Powiedziała, że ludzie którzy im służą tylko nie mają mocy, ale nadal są ludźmi, nadal mają uczucia i zasłużyli na szacunek.
Ów krewny następnie pojawił się na skrzyżowaniu. Bawił się chwilę zmieniając światła, rzucając niewidzialność na pieszych aż w końcu doprowadził do śmiertelnego wypadku. Bo mógł. Bo skrzyczała go siostra. Bo miał ochotę.
I ten mag też jest pod kontrolą amuletu.

Następna wizja była tylko dla Kasi. Kasia siedzi przed włączonym komputerem i jest skomponowany mail do Feliksa Bozura. Może nacisnąć "wyślij" lub nie. Wizja jest wielokrotna. Jeśli wysyła, Feliks i jego rodzina przeżywają, ale Kasia umiera (czar śmierci a jak tylko zabiorą pamięć to będzie pod obserwacją co skończy się też jej śmiercią jak się wyłamie). Jeśli nie wyśle maila, to Feliks wpierw trafi do szpitala (żeby przyciągnąć Kasię) a potem z uwagi na znaczenie tych terminów (Iliusitius, Arazille) Feliks i najbliższa rodzina będą musieli prewencyjnie zostać zabici.
Kasia wybierze kto zginie.

"Bawiłaś się beztrosko lustrami, czarodziejko. A ludzie dookoła ginęli. Tańczące odbicia." - Iliusitius

Kasia weszła do pokoju Ingi. Ołtarz leży na blacie, otoczony przez szczury które się rozstąpiły na widok Kasi. Kasia podeszła do blatu i wsadziła bezceremonialnie ołtarz do worka. Szczury pozwoliły jej odejść. Szczury idą za Kasią. Jest jeszcze jeden portret, przykryty firanką, który Kasia może odsłonić lub nie. Sandra zaczęła się budzić, ale nie jest już w stanie paniki. Kasia odłożyła worek z ołtarzykiem i podeszła kilka kroków w stronę szczurów. Te się odsunęły, pozwalają jej poruszać się tam, gdzie Kasia ma ochotę.
Kasia odsłoniła portret. Portret Augusta.

Kasia jest czarodziejką "high class", taka nowa Renatka Maus. Dokucza Augustowi, że nie jest magiem czystej krwi. August próbuje udowodnić, że jest magiem i że jest godny bycia magiem. By to udowodnić dokucza losowym ludziom. Gdy Kasia jest niewzruszona, dokucza swemu ojcu który stracił moc, by udowodnić, że ten nic dla niego nie znaczy. Przewraca go, niszczy mu karierę, wsadza do więzienia. Jednak Kasia jest niewzruszona. Gniew Augusta obraca się przeciw niej. Wypala jej kanały magiczne, torturuje, pali ją fizycznie. Nie zdążył jej zabić, choć nie wiadomo czy nie próbował - został zatrzymany przez magów i tylko mistrz go obronił przed czymś strasznym.
To akurat Kasię ruszyło. Ale nie aż tak jak Sandrę. August nie dostaje tych wizji, więc nic nie widzi, ale Sandra wpadła w paniczną histerię. Ta jednak jest inna. Sandra chce zabić Augusta. Kasia ją przewróciła i na niej usiadła. Szczury tylko patrzą. August nic nie rozumie.

"Pozwalam Wam odejść, czarodziejko luster. Ale nie przez wzgląd na ciebie. Przez wzgląd na dziecko bez duszy" - Iliusitius.

Kolejna wizja tylko dla Kasi. Kasia otworzyła oczy w sali jak sala szpitalna. Jest tam kilkanaście młodych dziewczyn. Weszła grupa Diakonów, w czym "jej Diakonka". Wszystkie dziewczyny wstały poza Kasią i jeszcze jedną dziewczyną. Kasia przytulała i uspokajała tą, która nie wstała. Diakoni porozmawiali, nie spodobało im się jako grupie że dwie siedzą po czym wyszli. Otworzyła się ściana i wyszedł mężczyzna. Zaczął dystrybuować dziewczyny - "ty tu, ty tu, a wy dwie tutaj". Koleżanka Kasi spytała co je czeka. Mężczyzna powiedział, że utylizacja. Kasia spróbowała z nim negocjować, ale on powiedział, że nie ma mocy decyzyjnej, jest tylko wykonawcą. Kasia poczekała aż skupi się na innych, po czym wyszła z pokoju ze swoją towarzyszką i ukryła się w pierwszym wolnym pokoju.
Traf chciał, że nie był wolny. Była tam "jej Diakonka" rozmawiająca z innym Diakonem. Ten się zdziwił, że tam są i powiedział, że są "klasy D" (Defekt). Kasia zaprotestowała, że nie chce umierać. "Jej Diakonka" się zaciekawiła. Powiedziała, że chce Kasię i jej towarzyszkę. Jej rozmówca nie chciał się zgodzić, na co owa Diakonka powiedziała, że w papierach nic nie musi się pojawić i że ona zwróci odpowiednią porcję biomasy.
Po czym podeszła do Kasi i jej towarzyszki i je uspokoiła. Będzie dobrze. Są z nią, będą bezpieczne.

"Taki potencjał, czarodziejko luster. Zniszczyłaś taki potencjał." - Iliusitius.

Sandra ponownie straciła przytomność, ponownie z ręki Augusta. Samira jest blada jak ściana i dygocze. Nadal jednak jakoś się trzyma. Andromeda szepcze do ucha Kasi, że każdy ma jakieś własne wewnętrzne demony. Samira się pozbierała. August nic nie rozumie.
Kasia wie, że ma blokady. Wie, że to nie ona. Że coś ją blokuje. Coś ją trzyma. Wie, że Iliusitius pozwoli im odejść... ale też wie, że w atrium jest prawda. Iliusitius nie ma wobec Kasi złych intencji, choć Kasia nie rozumie co Iliusitius chce od niej.

Zeszli na dół, do drzwi. Stoją przy drzwiach. Szczury pozwalają im odejść, pozwalają iść do atrium. Kasia zorientowała się w jeszcze jednej rzeczy - nie czuje Zofii. Mimo, że jest w domenie Iliusitiusa, Zofia nie została podpięta do Ołtarza. A Pan Dominacji mógłby to zrobić.
Kasia "doprowadźmy to do końca". August się zgodził, trzeb odnieść ołtarz Arazille do zniszczenia. Kasia powiedziała, że musi dowiedzieć się o co w tym wszystkim chodzi.
Samira padła przed Kasią na kolana. Szlochając, zaczęła błagać, by Kasia nie szła do atrium. By to skończyli. Ona już dłużej nie wytrzyma. 

"Czarodziejko luster, twoje iluzje i odbicia nie dorównują Arazille. Spójrzcie na swoje ręce. Skąd ta ranka? Czemu dopiero teraz ją widzicie? Boisz się konsekwencji..." - Iliusitius.

Faktycznie, wszyscy czworo mają na rękach małe ranki, zupełnie jak wylizane języcznikiem. Najbliższy języcznik jest w samochodzie, w apteczce Augusta... jakoś Kasia nie podejrzewa Iliusitiusa o noszenie podręcznego języcznika. I Kasia jest w kropce. Samira ją błąga, August ją prosi, Sandra dalej nieprzytomna.

"Czym jest czarodziejka luster? Co widzisz, kiedy na mnie patrzysz?" - Kasia do Iliusitiusa.
W odpowiedzi Kasia usłyszała tylko cichy, niezrozumiały szept. Połączenie Kasi i Iliusitiusa jest za słabe. 

Kasia podnosi Samirę i wychodzi.
Przeszłość jest dla niej mniej istotna niż przyjaciele.

Wsiadła do przywołanego samochodu i przygotowała się do pojechania do Wyjorza.
Ostatnie co zobaczyła w oknie z samochodu to była twarz.
Swoja twarz.

I to jest ten moment gdy Kasia przy nieprzytomnej Sandrze i świadomym tego Auguście tłumaczy Samirze magię.
Samira przyjęła to spokojnie, zwłaszcza że po takim dniu trudno byłoby nie uwierzyć w magię.
Kasia przeprosiła Samirę, że nie powiedziała jej wcześniej, ale chciała ją chronić.
Samira powiedziała, że po tych wizjach rozumie dlaczego nie powiedziała i czemu nie powinna wiedzieć.

Kasia uświadomiła jeszcze Augusta o tym że miała wizje. I że jedna z tych wizji dotyczyła Augusta. I co zawierała.
Powiedziała też Augustowi że Sandra nie może się od niej oddalić. I dlaczego - chciała ją chronić, a coś nie wyszło.

Do Wyjorza wszyscy jechali w milczeniu pogrążeni w jakieś formie nieszczęścia.
Poza Kasią. Kasia miała dobry humor.
I nie wiedziała dlaczego.

Amulet Kasi przestał pulsować.


# Lokalizacje:

1. Piwnice Wielkie
    1. Dom Józefa Pasana, kolekcjonera antyków
    1. Domostwo Szczypiorków

# Zasługi

* czł: Kasia Nowak, która poznaje elementy swej przeszłości choć nadal ma liczne bariery mentalne (bardzo osłabione przez Iliusitiusa). 
* mag: Sandra Stryjek, kompetentna technomantka która nie była w stanie oprzeć się straszliwym wizjom Iliusitiusa.
* mag: August Bankierz, lojalnie wspierający Kasię i nieświadomy wizji jakie miała żeńska część zespołu. W historii miał zachowania psychotyczne.
* czł: Samira Diakon, wreszcie uświadomiona o magii, która dzielnie przebijała się przez koszmary Iliusitiusa.
* czł: Józef Pasan, kolekcjoner antyków zbierający między innymi lustra i sprzedający je Samirze.
* mag: Herbert Zioło, terminus który dla maksymalizacji skuteczności akcji nie zawaha się poświęcić ludzi.
* mag: Andrzej Szop, mag który dla rozkazów poświęcił rodzinę i zostawił żonę z dziećmi.
* mag: Patryk Romczak, mag który dla ciekawości i przyjemności torturował i przekształcał wielu ludzi. Ekspert od tkanki magicznej.
* vic: Dariusz Germont, nadal lokaj Zofii Szczypiorek, czekający na Kasię w atrium.
* mag: Zofia Szczypiorek, czarodziejka przestrzeni i katalistka czekająca na Kasię w atrium.
* vic: Iliusitius, Pan Dominacji (?) o nieznanych celach który pokazywał Kasi jej przeszłość i kłamstwa jej istnienia. Oddaje swój ołtarz do zniszczenia bez walki.
* czł: Feliks Bozur, student który albo zginie albo Kasia zginie. Decyzja jest po jej stronie.
* mag: Anabela Diakon, "Diakonka Kasi", która pełni rolę ducha opiekuńczego nad Andromedą od momentu jej "incepcji".
