---
layout: default
categories: inwazja, campaign
title: "Blakenbauerowie x Skorpion"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Nie umieszczone, Anulowane](kampania-anulowane.html)

## Opis

-

# Historia

1. [Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html): 10/01/03 - 10/01/04
(150304-ani-slowa-prawdy)



1. [Klinika 'Słonecznik'](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html): 10/01/05 - 10/01/06
(141006-klinika-slonecznik)



1. [Jad w prokuraturze](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html): 10/01/07 - 10/01/08
(141009-jad-w-prokuraturze)



1. [Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html): 10/01/09 - 10/01/10
(141022-po-wymianie-strzalow)



1. ['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html): 10/01/11 - 10/01/12
(150210-komandosi-upadek-bohaterki)



1. [Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html): 10/01/11 - 10/01/12
(141216-zabili-mu-syna)



1. [Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html): 10/01/13 - 10/01/14
(150115-negocjacje-w-legioquant)



1. [Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html): 10/01/15 - 10/01/16
(150121-nowe-zycie-aliny)



