---
layout: inwazja-konspekt
title:  "Czapkowicka Apatia Kulturalna"
campaign: dusza-czapkowika
gm: żółw
players: draża, antek, maja
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170718 - Umarł z miłości](170718-umarl-z-milosci.html)

### Chronologiczna

* [170718 - Umarł z miłości](170718-umarl-z-milosci.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* Wątek "Operiatrix bez Piotra Kita".
* Wątek "Alojzy Przylaz ma przyjaciół?"

## Punkt zerowy

![Rysunek inicjujący sesję](Materials/180499/180418-init.png)

* Ż: Czemu jest dziwne i niepokojące to, że Kasia przestała oglądać Netflixa?
* D: Ponieważ zadaje się z niewłaściwym chłopakiem - potencjalnym narkomanem
* Ż: Jak to się stało, że Siergiej ruszył ten temat z pozostałymi?
* A: Siergiej może coś do niej czuć.
* Ż: Dlaczego Paweł wciągnął w dziwne zachowanie ludności Anetę?
* M: Aneta ma środki i informacje mogące pomóc w rozwikłaniu tego problemu.
* Ż: Jaka cecha Pawła sprawia, że macie szansę powodzenia?
* D: Bo jak się Paweł wkurzy, to jest do bólu szczery.
* Ż: Kto oprócz Kasi zaczął się zachowywać anormalnie apatyczne wrt kultury?
* A: Sławek Broda. Lubił imprezować i wszędzie było go pełno. Niektórzy podejrzewali, że nie śpi. A nagle - naleśnik.
* Ż: Co jest Waszym głównym sojusznikiem w tej kwestii i czemu daje nadzieję sukcesu?
* M: Lokalna populacja pijaczków w Czapkowiku.
* A: Dlaczego pan Bratkowski, lokalny nauczyciel szkoły muzycznej, rzucił wszystko i wyjechał w Bieszczady z dnia na dzień
* Ż: Wiedział, że on będzie następny
* D: Dlaczego pojawiło się dziwne wydobycie rudy? Takie gdzie nikt nie idzie?
* Ż: Echo magiczne.

## Potencjalne pytania historyczne

* null

## Misja właściwa

**Dzień 1**:

Kasia to osoba, którą jako jedną z nielicznych Siergiej tolerował będąc trzeźwym. 26 lat. Siergiej poruszył temat z Pawłem, który zauważył dziwne zachowanie ludności a Aneta od razu zauważyła, że Bratkowski był wylewny; Aneta trzyma rękę na pulsie we wszystkim co się dzieje. No i skontaktowała się z Bratkowskim - gość jest przerażony, widział jaszczura. Takiego jaszczura w marynarce, co porwał Kasię. Ok... nic dziwnego, że nikt mu nie wierzy XD.

Paweł zauważył, że wielu studentów i studentek jego szkoły muzycznej (gdzie też uczył Bratkowski) też wpadło w apatię. Ci konkretni studenci mieszkali poza Czapkowikiem, to już inne tematy. Kasia chodzi do prywatnej szkoły na gitarę klasyczną. Tzn. chodziła. Chwilowo nigdzie nie chodzi. Bo leży apatycznie w łóżku.

Aneta nocą jeszcze szuka informacji w Czapkowiku odnośnie jaszczurów. Aneta sprawdziła w internecie - skontaktowała się ze starym znajomym, nieco zwariowanym Niewiadomskim. Takim creepem i od teorii spiskowych. Dowiedziała się, że Czapkowik ma historię z jaszczurami (jaszczuroludzie z podziemi). Aneta zainspirowała Niewiadomskiego, reportera Paktu. Chce przyjechać i się rozejrzeć i sprawdzić. Może sprzedać do Paktu artykuł o PROJASZCZURACH! Aneta ma pełen facepalm. NIEEE. Nie przyjeżdżaj!

**Dzień 2**:

Tymczasem Siergiej dotarł do Kasi. Kasia wygląda apatycznie i nędznie. Powiedziała Siergiejowi, że nie pamięta żadnego jaszczura... ale słyszała muzykę. I Diwę. Muzykę tak głośną, tak piękną, że każda inna muzyka nie ma już znaczenia. Siergiej się wyraźnie zmartwił - nie powinno tak być. Siergiej postanowił, że zostanie z Kasią w mieszkaniu. Będzie z nią mieszkał i chronił. Normalnie Kasia by walczyła, ale jest tak apatyczna, że Siergiej ją przekonał. Siergiej teraz już serio się zmartwił...

Tymczasem Aneta i Paweł grają w gry. Siergiej się nie pojawia i nie odzywa. Oki, Paweł dzwoni do Siergieja i mówi, że są darmowe piwa. Siergiej ma rozterki - piwo czy Kasia - ale Kasia wygrywa. Siergiej postanowił zostać z Kasią i poprosił innych o kupienie dlań piwa. Smętnie, ale się zgodzili. Co zostaje...

Paweł i Aneta pojechali samochodem Anety, ale wpierw przeszli przez supermarket. Tam zobaczyli Sławka - nie mniej zdewastowanego niż Kasia - ale stojącego i obecnego. Człowiek który był obecny zawsze na każdej imprezie i mimo wszystko raczej lubił dobrą muzykę kupił kilka głośnych płyt disco polo. Paweł nie chciał za nim podążać; jest to jednostka bardzo niechętna konfliktom wynikającym z wbijania się innym w kolejkę.

Dotarli do domu Kasi i tam spotkali się z Siergiejem. Aneta od razu zignorowała ekipę i poszła do Kasi - ta wygląda po prostu fatalnie. Jej zdaniem Kasia jest chora i Siergiej to nędzna ofiara losu że nawet nie wziął tego pod uwagę. Siergiej na to, że to ogólnie normalne, przejdzie jej. Aneta wypytała o kilka rzeczy i wezwała lekarza. Tymczasem gdy do Pawła dotarło, że Aneta narzeka na zbyt głośną muzykę, zajumał Anecie kluczyki do golfa (zarobiwszy solidnego kuksańca) i poszedł do samochodu - idzie po SAKSOFON. Skomunikuje się z nią przez muzykę.

Oczywiście, nie mogło się to dobrze skończyć. Paweł nie był w pełni trzeźwy, nie ma dowodu samochodu Anety i ogólnie. Jechał wolniej; gdy zatrzymał się na skrzyżowaniu świateł w Czapkowiku, do auta podleciał Sławek, zajumał mu saksofon i poleciał w długą. SAKSOFON! Paweł natychmiast wysiadł z Golfa (na światłach) i poleciał za nim. Sławek cholera ma kondycję; Paweł go dorwał, ale dopiero w budynku opery czapkowickiej. Samego Golfa wziął Niewiadomski; poznał maszynę Anety (oczywiście, że ją poznał) i pojechał szukać Anety. No bo w sumie nie wie gdzie ona jest.

Tymczasem - do drzwi Kasi puka CHŁOPAK NARKOMAN. Znaczy, Szczepan Porzeczka. Otworzył mu Siergiej, co zdumiało zarówno Siergieja jak i Szczepana. Szczepan krzyknął, że Siergiej musi stąd wyjść i wyjął nóż. Runiczny. Domowej roboty. Ze śladami żółtego sera. Siergiej, pozycja bojowa. Aneta poleciała do kuchni łapać COKOLWIEK do wsparcia Siergieja w walce. No i doszło do nędznej walki - Aneta rzuciła czymś w Szczepana, ten się przestraszył, Siergiej go walnął i Szczepan został unieszkodliwiony. Zaczął płakać. Ech, te dzisiejsze walki...

Aneta wzięła i zaczęła szukać informacji o samym runicznym nożu. Co to do cholery jest. Okazało się, że jest to średniej jakości nóż z Ikei - ale to co jest ciekawego to jest to, że runy są poprawne. Są to kabalistyczne runy walki z opętaniami i demonami. O w mordę. Poszła więc do Siergieja, który zaczął już zmiękkczać przesłuchiwanego Szczepana piwem (mimo słabych protestów apatycznej Kasi, że to jej chłopak). Szczepan nie unikał prawdy! Powiedział wszystko, że przychodzi jaszczur i zamyka go w szafie (a raczej że on się chowa do szafy) i porywa Kasię, i on jest groźny i... i to ma jej pomóc. To jest good guy, ale taka dupa.

Aneta dyskretnie przywłaszczyła sobie ten nóż. Przyda się ;-). No, może poza aspektem krojenia żółtego sera.

Paweł w operze widzi, jak Sławek rozpaczliwie dmucha w ustnik, plugawiąc go swoją śliną. Jak ten widzi Pawła, krzyczy "POMÓŻ MI! ZAGŁUSZ MUZYKĘ!". Ok, coś podobnego jak z Kasią. Paweł wziął i zaczął grać na saksofonie (zmieniając uprzednio zapluty ustnik). Sławek spojrzał z ukojeniem. Po chwili Paweł zorientował się, że w tle ktoś puścił monumentalną muzykę. Paweł spojrzał i zdębiał - jaszczur włączył boomboxa z Fundamentusem. No ok... nie to czego się spodziewał, ale muza to muza. Czas na EPIC MUSIC BATTLE w ruinach starej opery. Gość z saksem versus boombox jaszczur.

Zaczęła budzić się Operiatrix. It's bad, very bad. Sławek rzucił się by zranić się by była krew - tego jej potrzeba do manifestacji. Paweł dokonał niesamowitej saksowalki - wybił możliwość zadania sobie krwawej rany przez Sławka. Jaszczur krzyknął, że jest potrzebna krew by to działało. Paweł... spokojnie grając na saksie podszedł do jaszczura i rozwalił mu boombox. Jaszczur jest bardzo nieszczęśliwy - Operiatrix zaczyna się destabilizować. On ją zabija! Zanim jaszczur zdążył zareagować, trafił go głaz. Miotnięty przez BATMAGA (w tej roli dziś Alojzy Przylaz).

Tymczasem do Anety i Siergieja przyszedł lekarz wraz z Niewiadomskim. Aneta dostała kluczyki z powrotem (ku swemu ogromnemu zdziwieniu ma swego golfa dzięki niemu); od razu zostawiła Niewiadomskiego i poszła do Kasi z lekarzem. Niewiadomski przyssał się do Siergieja, który wie o jaszczurach JESZCZE mniej niż sam Niewiadomski. Niewiadomski ma wrażenie, że Siergiej WIE i jest WTAJEMNICZONY. Facepalm. Tymczasem lekarz nie do końca wie jak może pomóc Kasi, ale ją bada.

W operze, Przylaz Materią zamknął wejścia; nie może złamać Maskarady. Ale Przylaz nie chce zabijać jaszczura; po prostu trochę nie ma jaj, mimo, że przecież kraloth (inny vicinius) tak ciężko skrzywdził małą Anię Kozak. No i Przylaz hamletyzuje, co robić. Dobić jaszczur czy nie.

Jaszczur wyrzęził do Pawła, by ten mu pomógł. Grał muzykę. Ona nie może umrzeć...

Oki. Paweł zaczął grać. Operiatrix ma moc manifestacji i stworzyła iluzję kralotha. Przylaz się przestraszył i wali w iluzję wszystkim co ma. Operiatrix żywi się energią magiczną. Jest coraz silniejsza...

Epilog:

* Przylaz przetrwał, Operiatrix i jaszczur (Werner) też.
* Wraz z wzmocnieniem Operiatrix, Kasi jest lepiej. Lekarz tego nie rozumie, ale ok.
* Napromieniowany magicznie Paweł stracił pamięć o nadnaturalnych wydarzeniach (Alojzy dał radę wyczyścić)
* Alojzy nie wie, że Operiatrix nań wpłynęła. Dał jaszczurowi żyć. W sumie, nie wie co robić z tą sprawą.
* W Czapkowiku pojawia się więcej legend o jaszczurach ;-).
* Jak Kasia poczuła się silniejsza, wywaliła Siergieja ze swojego mieszkania. Nawet pierogi nie pomogły.
* Szczepan zerwał z Kasią; boi się rywalizować z Siergiejem.

Konflikty:

![Rysunek z konfliktami](Materials/180499/180418-konflikty.png)

## Wpływ na świat

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

nowy wariant:

* 3: wygranie Typowego konfliktu
* 5: wygranie Trudnego konfliktu
* x2: wygranie Eskalacji

Z sesji:

![Rysunek z wpływem](Materials/180499/180418-punkty.png)

* Żółw: 14
* Maja: 5
* Wojtek (Antek): 8
* Draża: 3

Czyli:

* Maja: Batmag się zlitował, nie zabił jaszczura
* ?: Niewiadomski ma opinię człowieka co ma DOJŚCIA do tematów gadokształtnych
* Antek: Sławek chce zostać Saxmanem i nauczyć się grać na saksofonie
* ?: Bratkowski wraca z Bieszczad. Zakłada chór w Czapkowiku.
* ?: Kasia zresocjalizowała Szczepana.

# Streszczenie

Stara Opera w Czapkowiku umiera - Operiatrix gaśnie. Zakochany w muzyce człowiek-jaszczur, Alfred Werner, zasila ją energią ludzi kochających muzykę. I na to wpada Zespół - trójka ludzi próbujących pomóc apatycznej Kasi (wysysanej przez Operiatrix) i tropiących dziwnego jaszczura. Do tego dołączył się Alojzy Przylaz polujący na viciniusa. W wyniku - Operiatrix została zasilona, Przylaz oszczędził człowieka-jaszczura a Czapkowik ma kolejne opowieści o jaszczurach...

# Progresja

* Alojzy Przylaz: zdobył pierwszego nie-wroga w świecie viciniusów: Alfreda Wernera, człowieka-jaszczura.
* Alfred Werner: zdobył pierwszego nie-wroga w świecie magów: Alojzego Przylaza, ucznia terminusa.
* Operiatrix: ma nową osobę chcącą jej pomóc: Alfred Werner, człowiek-jaszczur.
* Aneta Pietraszek: pozyskała nóż chroniący przed opętaniami i walczący z opętaniami. Sęk w tym, że służy też do krojenia żółtego sera.
* Mariusz Niewiadomski: dostał opinię człowieka, który MA DOJŚCIA do tematów jaszczuropodobnych. Niestety, traktowany jako ekspert.
* Sławek Broda: pragnie nauczyć się gry na saksofonie. Dużo ćwiczy w ruinach starej opery czapkowickiej
* Franciszek Bratkowski: zakłada chór w Czapkowiku. Wrócił i jest zdeterminowany zmienić Czapkowik w miejsce muzyczne.
* Szczepan Porzeczka: zresocjalizowany przez Kasię Bratkowską. Wydarzenia paranormalne zmieniły jego życie na lepsze.
* Alfred Werner: pobiera lekcji gry na saksofonie od Pawła Szlezga

## Frakcji

# Zasługi

* czł: Siergiej Wdenkow, poszedł pomóc Kasi i został u niej zamieszkać. Pokonał narkomana z nożem z pomocą Kasi. Świetnie robi pierogi.
* czł: Paweł Szlezg, człowiek z saksofonem który totalnie boi się wciąć w kolejkę w Stonce. Zdecydował się uratować człowieka-jaszczura przed magiem. Średnie poszanowanie własności ;-).
* czł: Aneta Pietraszek, wyszukała kluczowe informacje o człowieku-jaszczurze i pomogła Siergiejowi w epickiej walce z narkomanem z nożem. Jako jedyna zadbała o Kasię właściwie.
* czł: Kasia Krabowska, pedantka i miłośniczka netflixa; ostatnio apatyczna i wszystko jej jedno. Dotknięta przez Operiatrix i Jaszczur nie zdążył jej zregenerować. Po sesji jej lepiej.
* czł: Sławek Broda, kiedyś imprezowicz nie z tej ziemi. Ostatnio słucha disco polo i jest apatyczny. Ukradł saksofon i chciał się poświęcić by obudzić Operiatrix.
* czł: Franciszek Bratkowski, nauczyciel muzyki i kolega Pawła; widział człowieka-jaszczura i zwiał w Bieszczady (bał się). Powiedział Anecie wszystko co wiedział.
* czł: Mariusz Niewiadomski, paranoiczny gość od spisków i tinfoil hat; szczególnie interesuje się jaszczurami. Znajomy Anety; przyjechał jej pomóc i napisać artykuł do Paktu. Nie wyszło.
* czł: Szczepan Porzeczka, podobno narkoman, człowiek i chłopak Kasi Krabowskiej. Siergiej go spoił i przesłuchali go z Anetą. Miał runiczny nóż antyopętaniowy.
* vic: Alfred Werner, człowiek-jaszczur, zakochany w pięknie muzyki i w pięknie Operiatrix. Próbuje przywrócić ją do działania i nie dać umrzeć kosztem energii życiowej ludzi.
* mag: Alojzy Przylaz, przebrany za Batmaga chce ratować ludzi w okolicy Czapkowika... ale nie ma serca dobić człowieka-jaszczura. Dał mu żyć.

# Plany

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik, słynący z opowieści o jaszczurach i jaszczuroludziach z podziemi
                    1. Stara Opera, gdzie Jaszczur próbuje zregenerować Operiatrix i gdzie dochodzi do Epic Music Battle: Sax Guy vs Boombox Jaszczur
                    1. Bar Gwarek, drogi i ekskluzywny, nie dla byle kogo; miejsce z drogim piwem i miejsce spotkań Zespołu

# Czas

* Opóźnienie: 10
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Pokazanie ruiny po Wojnie Karradraela
* Pokazanie potęgi magów i magii
* Wdrożenie nowych graczy

## Po czym poznam sukces

* Udane konflikty
* Obecność sukcesów i porażek
* Radość na twarzach nowych graczy

## Wynik z perspektywy celu

* Jest zadowolenie. System działa.
* Dużo porażek. ZA dużo porażek? Anomalie konfliktów? Do sprawdzenia.

## Wykorzystana mechanika

Mechanika 1804

Wpływ:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
