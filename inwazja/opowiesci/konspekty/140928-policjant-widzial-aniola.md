---
layout: inwazja-konspekt
title:  "Policjant widział anioła"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170725 - Krzywdzę, bo kocham (4*temp)](170725-krzywdze-bo-kocham.html)

### Chronologiczna

* [170725 - Krzywdzę, bo kocham (4*temp)](170725-krzywdze-bo-kocham.html)

## Konspekt pisany

### 01 
Wieczorem Paulina spokojnie przyjmuje pacjentów, nagle przerywa jej mentalny szok od Marii. Okazuje się, że znajomy Marii, policjant, Artur Kurczak jest w szpitalu - rozbił się samochodem. Paulina pojechała do Marii i wypytała ją o ten temat - Maria powiedziała, że jest członkiem paranormalnego klubu, 6 osób. Artur jest jednym z członków. Szukają zjawisk paranormalnych, min. magii. Jak Paulina zauważyła że to niebezpieczne, Maria powiedziała, że dzięki temu wskazała Paulinie sprawę z cmentarzem (nieopisane, przypis MG). Paulina się zgodziła. Maria dodała, że ostatnio Artur pracował nad sprawą anioła - opisał anioła jako młodą dziewczynę o anielskich skrzydłach i z aureolą. A przecież Artur był ateistą...
Paulina zdecydowała się pojechać z Marią do szpitala gdzie leży Artur. Może dowiedzieć się więcej o jego stanie. Maria ma zostać w samochodzie.

### 02
 W szpitalu w Przodku, w którym leży Artur pojawiła się Paulina. Przepytała swojego znajomego lekarza (Edmund Bakłażan) o to w jakim stanie jest Artur. Edmund powiedział, że jest po operacji, ale wyjdzie z tego. Samochód jakim jechał Artur uderzył w drzewo, świadkowie mówią, że nawet nie próbował skręcać. Najpewniej zasnął za kierownicą. Chwilowo w szpitalu jest jego żona. Paulina zapytała Marię czy Artur ma żonę. Tak, ma - żonę i dwójkę małych dzieci. Był policjantem który jednocześnie miał rewir w okolicach miejsca zamieszkania, w mniejszej mieścinie poza Przodkiem o nazwie Wykop Mały. Paulina poszła upewnić się, że z Arturem jest jego żona. Będąc tam, wyczuła bardzo lekki dotyk magii na leżącym Arturze. To energia rezydualna, niekoniecznie czar rzucony na niego. Dni Artura jako dzielnicowego są skończone, teraz będzie potrzebował rehabilitacji.

### 03
Oki. Dwie możliwe opcje: sprawdzić samochód (jest na parkingu policyjnym) lub miejsce wypadku, czyli drzewo. Paulina wolałaby nie wpakować się w coś bardzo nieszczęśliwego, więc skupiła się najpierw na drzewie (lokalizację szybko znalazła jej Maria). W czasie przejazdu do feralnego drzewa Maria skupiała się na sposobach wejścia na parking policyjny. Gdy dotarły do drzewa, Paulina rzuciła zaklęcie mające na celu sprawdzić ślady magiczne. Coś innego jednak przykuło jej uwagę (test przegrany) - dostała informację, że Artur w chwili wypadku nie był śpiący, nie był nieprzytomny czy odurzony. Wyczuła panikę i złość, próbował odzyskać kontrolę nad samochodem ale nie mógł...
Czyli coś nie tak z samochodem. Ergo, parking policyjny.

### 04
 Maria i Paulina pojechały na parking policyjny. Maria próbowała przekonać policjantów (którzy ją znają!) że mogą ją i Paulinę (pod fałszywymi danymi) tam wpuścić. 10 vs 8 się nie udał i się nie zgodzili. Maria zrobiła scenę i została tymczasowo aresztowana za próbę przekupstwa policjantów. W ten sposób kupiła Paulinie czas na wkradnięcie się. 3 vs 5 też się Paulinie nie udało; miała czas na rzucenie czaru, po czym TEŻ została aresztowana i wsadzona do więzienia. Tak bardzo spec ops, obie dziewczyny.
A czar skanujący na poziomie katalizy (val 9 z rzutem -> 11) wykazał bardzo ciekawy wynik. Ten samochód został tak potraktowany, by w określonych warunkach (prędkość, obecność drzewa itp) odebrać kierowcy kontrolę i doprowadzić do wypadku. Szczęśliwie, kryteria nie były wystarczające by zabić kierowcę... ale nie wiadomo czy to kwestia niekompetencji maga czy jego niechęci do zabijania. W końcu ten wypadek MÓGŁ zabić.

### 05
 Następnego południa, po solidnym kajaniu się Marii i przepraszaniu że wpakowała "praktykantkę" (Paulinę) w to wszystko wypuszczono dziewczyny. W końcu Maria to przyjaciółka Artura... Na wolności (i zubożone o grzywnę) zdecydowały co robić dalej. Porozmawiały chwilę o całej tej sprawie i Maria stwierdziła, że muszą pomścić Artura (bo Paulina powiedziała, że nic złego tamtego maga nie spotka, bo to człowiek a nie mag ucierpiał). Maria stwierdziła, że może iść do jego żony i poprosić o wpuszczenie - jest reporterką i uważa, że Artura ktoś wrabia. Wygląda na winną, więc Paulina twardo zakonfliktowała - co Maria wie czego nie mówi. Wygrała konflikt - Maria powiedziała kiedyś Arturowi o lapisie i on lapisował niektóre rzeczy i poukrywał je w domu. A skąd Maria wiedziała? Na etapie formowania linka wydobyła część informacji z Pauliny a część wiedzy miała już wcześniej.
Paulina zaznaczyła, że mówiąc to, Maria mogła ściągnąć zagrożenie na Artura jako pierwsza.

### 06
 Zdobywanie dostępu do domku Artura obyło się bez problemu, zgodnie z planem Marii - żona Artura ją wpuściła. Po pracowitym przeszukiwaniu domku dziewczyny znalazły zalapisowane dokumenty. A tam - bogactwo informacji. Zdjęcie "anioła" (rozmazane, komórką w nocy, ale widać jaśniejącą plamę o kobiecej sylwetce z aureolą i czymś na kształt skrzydeł), opis osoby - Nikola Kamień, 17 lat, miesiąc temu jej młodszy brat utonął na basenie, normalna dziewczyna. Artur opisał, że rozpoznał Nikolę jako tego anioła jak ta dotykiem wyleczyła potrąconego przez samochód psa.
Zdaniem Pauliny na chwilę obecną - protomag lub vicinius. "Zwykła" magia jest zwykle bardziej dyskretna.

### 07
 Dodatkowa analiza dokumentów i przydział zadań - Maria ma dowiedzieć się jak najwięcej o okolicznościach śmierci brata Nikoli. Paulina tymczasem z dokumentów dokopała się do dwóch informacji - po pierwsze, od trzech miesięcy ma zwolnienie z WFu itp. W ogóle się nie rozbiera. Po drugie, Paulina znalazła w dokumentach Artura dwa miesiące temu zgłoszenie zaginięcia dwóch piesków Nikoli i informację, że zostały znalezione zabite (celowo) przez kogoś.

### 08
 Podczas, gdy Maria wykorzystuje swoje wtyki i kontakty, Paulina poszła na "stalkerską ścieżkę" na bazie informacji od policjanta, obejrzeć magicznie w jakim stanie naprawdę jest Nikola. Rzuciła zaklęcia detekcyjne (katalistka/lekarz, 10 vs 10 -> "tak, ale" kosztem detekcji z drugiej strony) i udało jej się odkryć co następuje (po cross-reference tego z kolegami lekarzami magicznymi ze swoich kontaktów):
-- to bardzo złożony plan, trwa to ~6 miesięcy.
-- ktoś zmienia Nikolę w anioła. Dosłownie.
-- na wejściu Nikola była człowiekiem.
-- Nikola ma przy sobie artefakt, otwarty węzeł mocy z dawkowaną ilością. Dzięki temu się zmienia.
-- ktoś bardzo dokładnie kontroluje całe życie Nikoli, by na pewno stała się aniołem.
Oczywiście, nic z tego Paulina nie powiedziała Marii.

### 09
 Wieczór. Maria zdała raport Paulinie z tego co znalazła. Brat Nikoli utopił się na basenie, mimo, że był tam ratownik. Ratownik zeznawał, że mimo, że zareagował szybko po prostu nie dało się usunąć wody z płuc chłopaka. Paulinie wydało się to bardzo podejrzane. Nikoli nie było w pobliżu - rodzice wysłali ją gdzieś indziej, ale po wszystkim ona bardzo płakała i krzyczała, że gdyby tam była, to mogłaby go uratować. Maria także powiedziała Paulinie, że Nikola ma chłopaka - młodszego od Nikoli zamiejscowego chłopaka o imieniu Sebastian. Ów Seba nie chodzi do żadnej lokalnej szkoły i miejscowi nie kojarzą go - myślą, że gość jest z Kopalina.
Rozważania przerwało im pukanie do drzwi.

### 10
 A w drzwiach - kinderterminus. 15-16 lat, na pełnych, świecących tarczach. Przedstawił się Paulinie jako Sebastian Srebro (a w rzeczywistości nazywa się Sebastian Tecznia)...
Sebastian wyszedł twardo - SŚ żąda od "niej" (nie wie kim jest Paulina) zaprzestania działań na Nikoli. Bo to eksperyment SŚ. Paulina ze spokojem odparła, że ona nie jest z SŚ i żąda dokumentów (gigantyczny sukces socjalny). Sebastian bardzo zmieszany, okazuje się, że działa na własną rękę. 
Aha, w międzyczasie Maria siedzi pokój obok, na mindlinku, nie mówi ani słowa i nie reaguje. Bo jest skutecznym i lojalnym agentem.
Paulina zauważa, że Sebastian podkochuje się w Nikoli. Sebastian, absolutny antymistrz sfery socjalnej mówi Paulinie o co chodzi. Jest "ona", czarodziejka z EAM, która potrzebuje "dziecko anioła, który stracił wszystko". Dziecko to ma być jego (Sebastiana) i anioła. Sebastian do tego celu musi kochać anioła. Więc trzeba było to wszystko przygotować i on, Sebastian, jest za to odpowiedzialny...
Paulina zauważyła, że Sebastian jest jak strażnik w Oświęcimiu. Sebastian nie ma pojęcia o czym Paulina mówi. Dodał też, że przecież jeśli on tego nie zrobi Nikoli, to zrobi to jakiś inny mag jakiejś innej dziewczynie. A przecież czarodziejka z EAM oferuje jemu nagrody jakich nie ma jak otrzymać nigdzie indziej. Paulina uderzyła mową o tym, jak skłania się dziewczyny do prostytucji. Najpierw zwykły taniec, potem coś zdjąć, potem nago, potem nago przed klientem... a cały czas one myślą że w pełni kontrolują sytuację. I ta czarodziejka z EAM dokładnie to samo zrobiła Sebastianowi. 
Poprawiła też o tym, że przecież Artur jest okaleczony. Sebastian, że przecież "zaawansowana technika niczym nie różni się od magii". Paulina facepalmowała. Ludzie NIE mają takiej techniki. Sebastian nie chciał robić niczego permanentnego...

### 11
 W wyniku kolejnego konfliktu Paulina przekonała Sebastiana. Spróbują Nikolę odwrócić tak daleko jak to tylko możliwe. Sebastian zatrzyma plan tworzenia anioła. Będzie mógł być z Nikolą "uczciwie". Paulina spróbuje to naprawić magicznie tak daleko, jak to tylko możliwe. I spróbuje pomóc Arturowi tak, by nikt nie widział. Sebastian się oddalił przemyśleć to wszystko a Paulina i Maria przedyskutowały aktualną sytuację...
Nie można po prostu pozbawić dziewczynę Węzła, to może ją zabić. Więc współpraca Sebastiana jest konieczna. Ale nawet w pełni współpracujący Sebastian i Paulina nie będą w stanie naprawić Nikoli do stanu "czystego".
Podczas krótkiej rozmowy dziewczyny się zorientowały, że... nie mają żadnych namiarów na maga. Tego Sebastiana Tecznię. I w sumie z Sebastianem Paulina nie ustaliła nic konstruktywnego, bo nie chciała go naciskać nie znając dokładnie jego sytuacji.
...Maria będzie obserwować Nikolę. Znajdzie Paulinie Sebastiana.
Maria zaproponowała też Paulinie plan - od tej pory nie są lisko siebie fizycznie. Nie da się ich powiązać ze sobą. Soul-link i mindlink są niewykrywalne jeśli ich konkretnie nie szukasz.

### 12
 Następny dzień. Paulina udała się do Artura pomóc mu delikatnie magią przy leczeniu (upewnić się, że wyleczy się "prawidłowo" i będzie mógł w przyszłości chodzić). Maria obserwowała Nikolę i dała znać Paulinie że ma Sebastiana. Paulina pojechała a Maria została na obserwacji. Paulina dotarła a Sebastian i Nikola nadal są razem, na spacerze. 
Paulina się pokazała Sebastianowi. Ten po kilku minutach przeprosił Nikolę i spotkał się z Pauliną. Tam powiedział, że zgadza się z Pauliną, że trzeba Nikoli pomóc. Poprosił Paulinę, by ta w ramach OWD (ochroń własny tyłek) zmieniła mu pamięć jak to wszystko się skończy, by wyszło, że był zaatakowany i pokonany przez maga ludzistę. Powiedział, że Nikoli nie dotyczy Maskarada, bo ona wierzy, że jest aniołem który jest tymczasowo w ludzkiej formie. Ona ma już nieludzkie cechy, ma aureolę i skrzydła, ale nauczyła się je ukrywać. Artefakt - węzeł to samoadaptujący się amulet w kształcie krzyża który daje prawidłową ilość energii by zapewniać transformację. On, Sebastian, jest odpowiedzialny za takie przygotowywanie jej ekosystemu, by wierzyła że jest aniołem.
Paulina zdała sobie sprawę z jednej rzeczy - ta dziewczyna musiała wcześniej mieć cechy protomaga ORAZ musiała być odpowiednio przygotowana zanim do akcji wszedł Sebastian. To naprawdę skomplikowana i złożona operacja.
Dodatkowo Paulina zdała sobie sprawę z tego, że przecież dziewczyna ŚWIĘCIE wierzy, że jest aniołem. A to oznacza, że Paulina może musieć walczyć także z Nikolą. Niezbyt optymistyczna wizja dla lekarza, który po prostu chce pomóc...

### 13
 Oki. Paulina ma plan. Kazała Sebastianowi przedstawić się Nikoli i pozwolić się do niej zbliżyć. Paulina musi mieć możliwość pomocy, więc Nikola musi jej zaufać. Sebastian powiedział, żeby Paulina zostawiła to jemu. Przedstawił Paulinę jako archanioł Estrellę, która szuka i pomaga zagubionym aniołom. Ale Estrella nie może wejść w pełnię majestatu, bo oni spłoną a Wielka Wojna Piekła z Niebem zacznie się teraz. 
Paulina użyła iluzji - "cudu" - żeby dokonać częściowej manifestacji, z 6 parami skrzydeł i płomieniem. Konflikt sprawił, że Nikola bezgranicznie jej zaufała, odpowiednio przecież od dawna przygotowywana przez Sebastiana. 
Pierwszym krokiem Pauliny jest zbadanie nieszczęsnej Nikoli. Paulina, czyli archanioł Estrella incognito zabrała Sebastiana i Nikolę do hotelu i tam rozpoczęła proces badania. Rzuciła serię zaklęć by sprawdzić, jak bardzo jest zmieniona na poziomach Energia, Aevitas i Mentis i czy odwrócenie tego może być bardzo niebezpieczne. Badanie lekarskie dla puryfikatora. Wykorzystała swoje znajomości by sformułować lepiej badania i osiągnęła gigantyczny sukces (16 vs 12).
-- Nikola była dostosowanym przez kogoś protomagiem. Nie wiadomo jak to jej zrobiono.
-- Nikola nigdy już nie będzie magiem. Vicinius jej przeznaczeniem, ale może być bliski człowiekowi.
-- Puryfikacja pozwoli na wyczyszczenie dużej części tkanki magicznej przez współ-związanie jej magii. Czyli jej magia będzie zwalczała się nawzajem. 
-- Proces trwa już co najmniej 5 miesięcy (Sebastian jest z nią od 4 miesięcy).
-- Paulina ma silne wskazówki jak ją czyścić (sytuacyjne +4).
-- Paulina ma informacje o docelowej strukturze magicznej anioła. Wie jaki ma być jako wynik finalny. I tak, pierwsze zbliżenie zaowocuje ciążą.
Ten proces zajął do wieczora.   

### 14
 Sebastian odprowadził Nikolę, po czym wrócił do Pauliny. Ta podała mu wyniki. Wtedy dopiero Sebastian uzmysłowił sobie, że przecież Nikola byłaby magiem. I on i ona mogli być razem jak dwójka magów, jako para. A to wszystko sprawiło, że stanie się tylko nędznym, wykorzystanym viciniusem lub człowiekiem. Paulina przestrzegła, że jak jej się bardzo nie uda, będą mieć "upadłego anioła". I zaznaczyła, że po tym wszystkim (po tym rytuale) I TAK byłby z niej upadły anioł. Czymkolwiek by to nie było, na pewno byłoby na celowniku terminusów.

### 15
 Podczas rozmowy, telefon Sebastiana zadzwonił. Gdy ten machinalnie go podniósł by odebrać, wystrzeliły z niego kolce i go zraniły. Sebastian odrzucił telefon z krzykiem i telefon zaczął formować portal. Paulina zareagowała błyskawicznie - sformowała miniona który pożarł telefon i wybiegł by znaleźć się w miejscu gdzie prawie złamie Maskaradę, po czym zniknie. Minion zdołał jeszcze przesłać Paulinie obraz osoby jaka przeszła przez portal - to piękna czarodziejka opleciona drutami pod suknią. Paulina złapała Sebastiana i chodu! Szybko zdobyła od niego informację jak wygląda ta jego "czarodziejka EAM". To ona. Katalistka-technomantka. GREAT.

### 16
 Cóż, przeszli szybko przed aptekę i Paulina opatrzyła, podleczyła i zdezynfekowała ranę Sebastiana. Na szybko Paulina nadała plan - pójdą do supermarketu. Tam czarodziejka EAM nie będzie w stanie działać tak jak chce, a w swojej arogancji może tam przyjdzie. I faktycznie, przyszła. Paulina się oddaliła od Sebastiana, zostawiając go jako "jedynego". W rozmowie czarodziejka EAM stwierdziła, że Sebastian ją zawiódł. Kiedyś prosił o szansę, prosił o moc, prosił o zaufanie i ona mu go udzieliła. A jak jej się odpłacił? Ona złamała zasady i nauczyła go rytuału czy dwóch prosto z EAM. A on? Zawiódł jako że jest zakochanym młokosem. Na to Sebastian, że Nikola mogła być czarodziejką a EAMka ją zniszczyła. O, a skąd ta wiedza? I na to pojawiła się Paulina - od niej, wykwalifikowanego magicznego lekarza. Paulina i EAMka nie spodobały się sobie od pierwszego wejrzenia, zwłaszcza, że EAMka stwierdziła "nie ta to inna" a Paulina wyjechała swoją wiedzą o EAM, zasadach etycznych itp. W wyniku solidnej dyskusji Paulina sprowokowała EAMkę do napaści fizycznej (4 vs 5 i wygrała!). Podziałało - EAMka spoliczkowała i uderzyła Paulinę, obronił ją Sebastian więc też dostał cios, Maria podłożyła EAMce jakieś 2 rzeczy ze sklepu (by przekroczyć 500 zł) i krzyknęła, że EAMka coś ukradła.
Czarodziejka prawie straciła nad sobą kontrolę. Druty na jej ciele zmieniły się w ostrza, ale szybko się opanowała, nie zrobiła masakry. Druty schowały się pod skórę (przebijając ją i raniąc EAMkę, co zostało wyleczone kolejnym dziwnym sposobem). Ochrona ją zwinęła.
Przedtem podczas kłótni okazało się, że EAMka potrafi wysysać pamięć (wampiryzować) spojrzeniem oczy-oczy. Chciała zastosować to wobec Sebastiana i odebrać mu nauczone przez nią rytuały, ale przeszkodziła Paulina.

### 17
 Chwila odpoczynku a EAMka odwieziona do Kopalina, na komisariat. Paulina szybko znalazła miejsce gdzie może wywieźć Nikolę bezpiecznie, poza zasięg EAMki używając swoich kontaktów. Pojechała do niej w nocy i jako Archanioł Estrella kazała jej pojechać z Pauliną - ale wtedy pojawiła się EAMka. Okazało się, że przejęła kontrolę nad policją i przyjechała tutaj. Wyszła i chciała objąć Nikolę w posiadanie, na co w rozpaczy i desperacji Paulina uderzyła Nikolę energią nekromantyczną, zmieniając jej wzór i sprawiając, że Nikola stała się bezużyteczna dla EAMki. EAMka była wściekła, ale nic nie była już w stanie na to poradzić. Zaczęła rzucać zaklęcie na Paulinę, ta jednak ją uderzyła torebką. Stoi za blisko. Podczas dyskusji i kłótni o dead man's hand jaką ma Paulina spojrzenia Pauliny i EAMki się połączyły i zadziałała ta sama hipnoza jaka zadziałała wcześniej na Sebastiana. EAMka rozpoczęła systematyczny skan i drenaż pamięci przy użyciu swoich obwodów. Maria szybko połączyła się mentalnie z Pauliną i poprosiła, by Paulina ją wpuściła. Paulina się zawahała na co Maria w rozpaczy, że siostra Marii ją nauczyła wysysania i chowania wspomnień oraz prostej fabrykacji. Paulina się zgodziła (nie ma wielkiego wyboru)... 
Skanująca Paulinę EAMka znalazła prostą, nieciekawą, ludzistyczną czarodziejkę pochodzącą ze świata ludzi. Nie ma żadnego dead man's hand. To był blef.
EAMka nie zrobiła krzywdy Paulinie. Usunęła wszystkie swoje ślady, zrobiła "plausible explanation" i się oddaliła. Do Sebastiana.
Maria oddała Paulinie jej wspomnienia. Paulina nie miała jak pomóc Sebastianowi.

Zostało jej tylko pomóc biednej Nikoli, pomóc Arturowi i działać dalej...


"
Anioł, który z człowieka pochodzi.
Anioł, który utracił wiarę.
Anioł, więzień swej miłości.
Anioł, odrzucił zmarłych rodziców.
Anioł, dziecko spłodził w nienawiści.
Dziecko - klucz do Trzeciej Bramy.
"

# Idea:

- Wyjaśnić relację Maria <-> Paulina.

# Zasługi

* mag: Paulina Tarczyńska, uświadamiająca Sebastianowi, że mógł mieć miłość swojego życia i robiąca idiotkę z EAMki.
* czł: Maria Newa jako silny motywator Pauliny i osoba grająca pierwsze skrzypce w ich "związku".
* czł: Artur Kurczak jako policjant leżący w szpitalu w Kopalinie. 
* czł: Edmund Bakłażan jako kopaliński lekarz, przyjaciel Pauliny.
* vic: Nikola Kamień jako protomag stający się prawdziwym aniołem.
* mag: Sebastian Tecznia jako kinderterminus w którym Paulina obudziła wyrzuty sumienia.
* mag: Karolina Maus, czyli "EAMka" jako żywy dowód na to, że czasami zło wygrywa. Choć nie do końca.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. Główny komisariat policji
                            1. Parking policyjny
                1. Wykop Mały
