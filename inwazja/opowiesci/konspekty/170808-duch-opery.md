---
layout: inwazja-konspekt
title: "Duch Opery"
gm: żółw
players: foczek, dust
campaign: powrot-karradraela
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170108 - Samotna, w świecie magów (SD)](170108-samotna-w-swiecie-magow.html)

### Chronologiczna

* [170108 - Samotna, w świecie magów (SD)](170108-samotna-w-swiecie-magow.html)

## Kontekst ogólny sytuacji

Modelka zniknęła - potężny duch animujący starą operę zdobył kolejną ofiarę. Modelka była przyjaciółką Marcelina.

## Punkt zerowy:

**Scena startowa**:

* Ż: Dwie modelki. Ładne dziewczyny.
* D: Jessica i Żaneta.
* Ż: Co one robiły w okolicach budynku, który kiedyś był operą wieczorem.
* F: Mało kto tam zagląda. Testowały różnego rodziaju substancje.
* Ż: Niewielkie miasto. Skąd zatem opera?
* D: Stare miasto, swój rozkwit za czasów komuny, ulubione miejsce lokalnego pierwszego sekretarza. Piękne miejsce.
* Ż: Jaka jest aktualnie rola tej opery?
* F: Ruina. Część okien zabita, mury niepełne... może też tam się strzelają w paintballa.

**Stan początkowy**:

It’s nighttime. Two pretty models, Jessica and Jeanette had slipped into an old opera house. Currently is opera house is in ruins and is used as a place to play paintball. They are able to slip into it, simply because one of the guards is stricken with Janet’s beauty. So they have a base of operation.

Jessica shared with Jeanette a special type of perfume given to her by Marcelin. Although it was given her as a perfume, Jessica noticed that it has quite a kick if you drink it. Jeanette, of course, took it with pleasure. And they did have a kick, yes.

At one point of their psychedelic trip they have noticed there are three girls, not two. Janet even asked the third one why is she here and the third girl answered because they have summoned her. Jessica noticed that this is the greatest trip they have ever had.

When the models tried to talk with the new girl she said that the models have pretty eyes. Exactly like she does. And she opened her purse. There were cut eyes, blinking. Jeanette decided that she has enough of this trip and she is going to sleep. She has lied down next to the wall and tried to fall asleep. With success, it was a good sleep.

Jessica tried to run to the bathroom – there must be water in the bathroom and of course when she will pour her face with water everything will simply go away. On her way, she has met this strange girl. This strange girl smiled and approached her with a knife – and all went to black.

**Pytania i odpowiedzi**

* Ż: Skąd Piotr oraz Marcelin się znają?
* D: Piotr jest artystą. Marcelin jest wierszokletą. Mogli się znaleźć w klubie dla artystów i zaprzyjaźnić.
* Ż: Dlaczego od czasu do czasu Piotr bywał w Zaćmionym Sercu?
* F: Szukał inspiracji do obrazów, jakie maluje.
* Ż: Dlaczego to, że Jessica zniknęła z eliksirem troszkę martwi Marcelina?
* D: Dał jej perfumy i instrukcje, po czym wieczorem przyszedł na randkę a jej nie było. Wszedł do pokoju przez okno i zobaczył próby eksperymentów... zna efekty uboczne.
* Ż: Dlaczego używanie przez Jessicę eliksiru może być... problematyczne?
* F: Miał ładnie pachnieć, po zażyciu jest psychodelka. W pewnych sytuacjach częstych i w kontakcie z innymi istotami żywymi - paranoja a nawet zawładnięcie umysłem jednostki.
* Ż: Dlaczego najsensowniejszym dla Marcelina wyborem będzie Piotr?
* D: Z Piotrem Marcelin eksperymentował z różnymi środkami celem inspiracji...
* Ż: Czemu Marcelin chciałby SAM to rozwiązać?
* F: Hektor ani Edwin by się tego NIE chcieli dowiedzieć. Sąd i lekarze.
* Ż: Do czego jedna z tych modelek jest Piotrowi potrzebna?
* F: Piotr malował akty. Jedną bardziej lubił i miał parę fajnych sekretnych aktów i nie chciał by pierwowzór ucierpiał.
* Ż: Co popchnęło dwóch magów, by faktycznie zaczęli szukać dziewczyn?
* D: Zbiór poszlak - miks eliksiru i alkoholu, nie wróciły razem, Żaneta nie pojawiła się na sesji - Marcelin szukał Jessiki a Piotr Żanety.
* D: SKąd wiemy, że dziewczyny żyją?
* Ż: Podczas jednej z pijackich akcji dziewczyny zrobiły "psiapsiółki forever", 2 wisiorki z kroplami krwi. Póki są "ciepłe", póty magowie wiedzą, że one żyją

## Misja właściwa:

**Dzień 1:**

Marcelin and Piotr were searching for the models. Marcelin was slightly worried, simply because the elixir he has given to Jessica might have some side effects. Not much, but a few. Therefore he wanted to see Jessica to see if everything is all right. Especially as Jessica tends to do strange things with the elixirs he gives her.

Piotr, on the other hand, was drawing an act of Jeanette. Jeanette was supposed to come to the modeling session she has always dreamed about and which Piotr has arranged for her. When she didn’t come, he got worried. And so both Magi has met in Jeanette’s apartment in Trocin. The person who lets them in is a colleague of Jessica – a roommate – called Sophia.

Sophia was stricken with Marcelin’s beauty and Piotr was quite inquisitive - as Sophia was really worried about her roommate’s situation, she said everything she knew. She told them, that the Jessica and Janet were friends. She also told them, that there is that place called ‘Neonowa Jaszczurka’ – ‘Neon Lizard’. Both Jeanette and Jessica used to have fun in the Neon Lizard and that’s where they were two nights ago. When Piotr asked to use Jeanette’s computer, Sophia complied.

The computer might have had some security, but the power of infomancer was enough to pierce the veil of secrecy. Prying through girl secrets, Piotr has unearthed a special communicator on a dark web called Sekretnix. First, he facepalmed because of the name. Then he noticed that the model was chatting with a guy called Dr. Gekon. And that Dr. Gekon used to pass some interesting stuff to her.

When Marcelin confronted Sophia with that, Sophia acknowledged that things like this happened and that girls used to have a base in an old opera building in a different city. Marcelin and Piotr had a decision to make – do they follow Dr. Gekon or do they go directly to the opera building. Marcelin said that he needs to know what types of elixirs did the girls get from a good doctor. So Piotr used the communicator and requested aid from Gekon. 

It was not a secret that a good doctor proved to be a mage. But he did not want to meet personally (and he communicated through gifs). So he sent someone else to meet on his behalf. When – in the evening – our brave team got close to the neon lizard, they saw a secret dark hooded person. Marcelin approached that person and the person unmasked herself. She was Melody Diakon. And both Marcelin and Melody were really surprised by seeing each other.

Doctor Gekon has given Melody a set of elixirs which were given to random girls lately. Marcelin needed to make some tests to see to what extent are those elixirs interacting with stuff he was making. To learn more about Jeanette, Piotr suggested that the team (composed of three Magi at this point) come to his room and see the act he was painting. Marcelin told Melody that Piotr is an artist. Melody smiled. She likes artists.

Piotr does not paint people as they look like. Piotr paints people as he sees them using all his heightened senses – they not only look as they really do, there are things like all of us and thrills of energies around them as well. Those pictures are designed for magi, not only for people. Thus, Marcelin was able to infer that there is something strange going on with Janet. She might have some blood of a vicinius. On top of that, some elixirs given to the girls by Dr. Gekon could interact very violently - in terms of magical energy eruption - combined with the perfumes given to them by Marcelin. 

So we have a potential magical energy eruption. And a girl having a blood of a vicinius. This could activate the monster inside the girl.

In the night, they drove to the opera house. They may be tired, but they want to save the models and solve this mess before anyone hears about it. Seeing the problems with the combination of elixirs, Marcelin has revised an antidote – he has created an elixir which would counteract most of those things. And drain the level of magical energy.

So have managed to get next to the ruined opera house. The building has a deep magical aura and a lot of energetic corruption. Piotr connected with the astral aura around the building and he has understood what happened – there exists a type of Gestalt, a ‘soul’ of the building made from the dreams of the actors and directors and other people who used to give everything they had to the art. And the option of magical energy has awoken the Gestalt. This entity then send its tentacles luring some people into the building and it is replaying a particular theatrical play. The models – as a primary energy source – became intertwined with the building, becoming its organs.

In short, sucks.

How can three noncombatant magi disable that type of Gestalt? Piotr, being an expert on art, suggested directly – if the Gestalt is composed from human bodies here, one needs to drink them to oblivion. If those people are drunk, it would weaken the Gestalt. Melody suggested that she will become a part of the play – she will enter the scene and try to steer it. As she is an artsy type as well she should be able to focus Gestalt’s attention on her. Marcelin and Piotr prepared a spell which rendered them invisible for the Gestalt on physical, astral and mental levels.

And the plan was simple – Melody distracts the Gestalt while Piotr and Marcelin our first removing people from Gestalt area of influence and after this is complete Marcelin is supposed to use his magical powers to repair the models and disjoint them from the building. Then, magi are evacuating all the people while Melody is taking all the heat to – eventually- return and help Melody destroy the Gestalt.

Or rather, save Melody from the Gestalt.

The plan is implemented went perfectly. To add the distraction, Marcelin used the elixir he has created to weaken the resonance of the elixirs. Combination of all those actions has successfully evacuated all the people, but the Gestalt has materialized itself and approached Melody with a knife. The sorceress was too overwhelmed with whole situation and she could not defend herself.

Piotr and Marcelin started negotiating before the Gestalt harms Melody. They have negotiated the following:

* Gestalt let’s Melody go away and will not attempt to hunt other people
* The opera house will stop being a paintball venue. It will become an artsy venue.
* Piotr will become the curator of the opera house

Having promised that and seeing the semblance of consciousness and elegance in the Gestalt, the magi and the Gestalt split and parted ways.

1. Powrót Gestaltu:
    1. Modelki zostają zintegrowane z gestaltem: 1/10
    2. Właścicielka jest w pełni zrekonstytuowana: 5/10
    3. Opera jest odrodzona, niekoniecznie w tym miejscu: 3/10
    4. Infekcja memetyczna w umysłach artystów jak z boginią: 0/10

# Progresja

* Piotr Kit: zobowiązany do zostania kustoszem zrujnowanej opery w Czapkowiku
* Żaneta Kroniacz: ma krew viciniusa; nie jest w pełni człowiekiem. Uaktywniły się jej aspekty krwi viciniusa?

# Streszczenie

Two models liking special substances have awakened a Gestalt of the ruined opera. Marcelin and Piotr with the help of Melodia saved all the people endangered in the result. Gestalt remains awakened and Piotr is supposed to be a curator of the opera, which is now a paintball venue.

# Zasługi

* mag: Marcelin Blakenbauer, (Dust), dostarczył modelce perfumy co doprowadziło do obudzenia gestalta opery... na szczęście, rozmontował sprawę z Piotrem. Robił eliksiry, był w swoim żywiole.
* mag: Piotr Kit, (Foczek), malarz, który ratował modelki by dać im szansę na rozwinięcie swego potencjału. Zobowiązał się do zostania kustoszem starej opery pełniącej rolę paintballa.
* mag: Melodia Diakon, poproszona o pomoc przez "Doktora Gekona" (Millennium) dołączyła do zespołu Marcelina i Piotra. Zaryzykowała wejście na scenę gestalta opery by uratować ludzi.
* czł: Jessica Czułmik, modelka, miłośniczka różnych środków podawanych do organizmu. Bardzo dzielnie eksperymentuje. Wpakowała się w gestalta starej opery. Fun-loving.
* czł: Żaneta Kroniacz, modelka, uśpiony vicinius, muza maga Piotra. Wpakowała się w gestalta starej opery. Ma tendencje do różnych używek. Fun-loving. Ma marzenia wielkości.
* czł: Zofia Łaziarak, przyjaciółka i roommate modelki (Żanety Kroniacz), odpowiedzialna i żyjąca normalnym życiem. Mieszkanka Trocina.
* vic: Operiatrix, gestalt opery, który poszedł na ugodę z Piotrem Kitem i będą próbowali odbudować swoją operę.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Trocin
                    1. Centrum
                        1. Klub Neonowa Jaszczurka, otwarty wieczorem i później; miejsce, gdzie można zdobyć dziwne środki od magów. Nawet jako człowiek.
                1. Czapkowik
                    1. Stara Opera, zrujnowana, pełni rolę miejsca do paintballa. Modelki z magicznymi rzeczami uruchomiły tam gestalta. Gestalt ciągle trwa.
                    1. Klub Paintballa Snajper, który kontroluje obszar starej opery.


# Czas

* Dni: 2

# Starcia

## Przywrócenie Właścicielki Opery

Stary vicinius, kiedyś bogini? Gestalt wielu magów i innych miłośników piękna zintegrowany przez Zaćmienie w gestalta. Żywi się pięknem. Adaptuje. Odrodzony przez zastrzyk energii eliksiru Marcelina w połączeniu z Żanetą.

## Ścieżki:

1. Powrót Gestaltu:
    1. Modelki zostają zintegrowane z gestaltem: 0/10
    1. Właścicielka jest w pełni zrekonstytuowana: 0/10
    1. Opera jest odrodzona, niekoniecznie w tym miejscu: 0/10
    1. Infekcja memetyczna w umysłach artystów jak z boginią: 0/10

# Narzędzia MG

## Opis celu misji

Wdrożenie nowego gracza w postać maga.

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
