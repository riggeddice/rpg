---
layout: inwazja-konspekt
title:  "Po wymianie strzałów..."
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141009 - Jad w prokuraturze (HB, Kx, AB)](141009-jad-w-prokuraturze.html)

### Chronologiczna

* [141009 - Jad w prokuraturze (HB, Kx, AB)](141009-jad-w-prokuraturze.html)

## Punkt zerowy:

Ż: Czy w najczarniejszej godzinie Otton wyśle jako wsparcie chytrego pajęczaka czy taktyka masowej zagłady?
D: Chytry pajęczak (Saith Catapult).
Ż: Czym tien Tadeusz Baran może skutecznie przekonać Hektora Blakenbauera do współpracy?
B: Ma nakaz podpisany przez prokuratora generalnego.
Ż: Kim jest osoba znajdująca się w Kopalinie na której bardzo zależy zarówno Edwinowi jak i Margaret?
K: Ich wspólnym mentorem.
Ż: Dlaczego dwóch magów rodu Myszeczka nienawidzi się z całego serca?
D: Ponieważ wzajemnie obwiniają się o coś, co tak naprawdę zrobił ktoś trzeci.
Ż: Jaka jest rola w Skorpionie kilkunastoletniej czarodziejki?
B: Przynęta.
Ż: Jakie dodatkowe, poza oddziałem specjalnym, środki ma Hektor w świecie ludzi?
K: Jest bardzo znany w środowisku modelarzy i makiet.
Ż: Mentorem odnośnie czego? (Edwin, Margaret)
D: Etyki. Osoba ucząca ich moralności.
Ż: O co ci dwaj magowie się obwiniają?
B: O wyciągnięcie ze wspólnej sieci wpływów kluczowej czarodziejki rodu Maus.
Ż: Czemu była kluczowa?
K: Bo mogła dać im dostęp do innych, niekoniecznie wyższych, kręgów.
Ż: W jaki sposób ElMet Rebeki pomagał jej w roli przynęty?
D: Iluzje i feromony.
Ż: Dlaczego, do cholery, Sebastian jej pomaga?
B: Ojcem Sebastiana Teczni jest ojciec Estery Piryt (człowiek)
Ż: Jakim cudem ich rodzice nie wiedzą co robią młodzi?!
K: Dziewczyna uciekła z domu a chłopak nic im nie mówi.
D->Ż: Dlaczego Otton doszedł do wniosku, że Pajęczak to lepsze wyjście?
Ź: Bo ważniejsza jest ochrona osób mu bliskich niż finalne rozwiązanie problemu ze Skorpionem.
B->Ż: Dlaczego Dorota Gacek zdecydowała się uderzyć w Edwina bez ostrzeżenia?
Ż: Bo musi uratować niewinną dziewczynę przed Rzeźnikiem Edwinem Blakenbauerem.
K->Ż: Dlaczego Margaret cichcem pomaga Sebastianowi?
Ż: Margaret ma nadzieję dotrzeć przez Sebastiana do Karoliny Maus.

## Misja właściwa:

Edwin jest bardzo zajęty blokowaniem epidemii. Hektor przejął dowodzenie. Więc są dwie ścieżki - Kleofas Bór, zatruty jadem Dromopod Iserat i Estera Piryt. Bezpieczniej zdaniem Klemensa i Aliny jest skupić się wpierw na Bórze; to może być bardziej istotne czy skuteczne rozwiązanie. Decydują się wysłać na akcję Alinę. Będzie w formie Bóra. W perfekcyjnym świecie, wynikiem tej akcji jest zdobycie jak największej ilości informacji o tym, kto stoi za tym wszystkim.

Alina poszła do Edwina i poprosiła o maść wywołującą paraliż. Otrzymała ją. Wzięła cienkie, wysokie rękawiczki i nasmarowała to maścią od Edwina. Hektor załatwił transport (pojazd). I sprawdził teren - mają spotkać się z informatorem. Plan jest stosunkowo ciekawy - wprowadzić Bóra, na jadzie D.I. do zaułka i zostawić go w śmietniku. Alina idzie jako Bór, przechwytują polujowca, Alina paraliżuje polujowca i wychodzi z Bórem jako polujowiec. A reszta ma za zadanie dorwać prawdziwego polujowca i go przesłuchać.

Plan wyszedł idealnie. Przyszedł polujowiec i został obezwładniony i sparaliżowany przez Alinę. Alina się oddaliła, lecz zrobiła błąd - nie przeszukała ofiary i się z Bórem rozstali. Do zaułka wszedł Hektor i zaczął przeszukiwać polujowca. Znalazł komórkę z nie odebranym połączeniem. Przeszkodziło mu dwóch ludzi z nożami. Zrobili Hektorowi zdjęcie i się zaczęli do niego zbliżać z nożami po stwierdzeniu "Blakenbauer". Na to wszedł Klemens (Hektor za długo brał ciało) i skutecznie ich zastrzelił w plecy. W odpowiedzi ktoś z nieznanej pozycji strzelił JEMU w plecy. Klemens nie żyje.
Hektor natychmiast magicznie ustabilizował zwłoki i się schował przed snajperem. Hektor jest suppressed, ale ostrzegł o sytuacji Alinę. Ta natychmiast przeszła przez ubikację i zmieniła formę na kogoś innego.

Oki. Hektor zaczął czytać pamięć nieprzytomnego człowieka z przestrzelonym rdzeniem kręgowym i przeczytał, że musi pojawić się ktoś z granatem by "rozwiązać problem". No OK. Więc Hektor skupił się, przygotował zaklęcie bazowane na życiorysach wielkich przestępców i ich ucieczkach i przeniósł Paradoksalnie siebie, zwłoki Klemensa i jednego z ciężko rannych (przestrzelony rdzeń kręgowy) ludzi. Zostawił jednak po sobie ślad Skażenia.

Alina zdecydowała się podjąć pewne ryzyko. Kazała Bórowi się schować, ukryć się przed wrogami i się ewakuować. Sama zdecydowała się zapolować na snajpera. Znowu potraktowała dłonie paraliżującym kremem i wyruszyła szukać snajpera (vs 7). Nie udało jej się - zlokalizowała snajpera, ale jemu udało się zlokalizować Alinę i określić, że jest zagrożeniem. Snajper skontaktował się z Centralą i dostał jednoznaczną odpowiedź: ma się wycofać a Bór zostanie zlikwidowany. Alina zdecydowała sie na próbę przechwycenia uciekającego snajpera (vs 7; snajper ma base 5 + 2). Nie udało jej się. Snajper się wycofał.

Edwin się załamał. Marcelin miał Klemensa tak długo i nic mu nie było, a Hektor Klemensa zepsuł na jednej z pierwszych akcji. Edwin obiecał naprawić Klemensa, ale to potrwa sporo czasu. Godzinę później wróciła do Rezydencji Alina. Bór nigdy się nie pojawił, najpewniej nie żyje. Hektor, zdecydowanie niezadowolony zdecydował się przesłuchać swoich jeńców. Ten sparaliżowany to był zwykły diler, miał robić swoje to zrobił swoje. Nożownik natomiast był nisko postawionym agentem Skorpiona. Niewiele wie; uważa Skorpiona za potężną mafię rządzącą okolicą. Ma swój kontakt (spalony) do którego wysłał to zdjęcie Hektora.

Następnego ranka Edwin, Margaret, Hektor i Alina spotykają się w centrum dowodzenia w Rezydencji. Edwin wcześniej powiedział Margaret jak wygląda sytuacja, ale ona chciała usłyszeć to też od Hektora. Po briefingu Margaret zaproponowała plagę, z czym absolutnie nie zgodził się Edwin (a Hektor poparł Edwina). Następnie Edwin zaproponował uruchomienie oddziału Zeta i wycelowanie go w centralę dowodzenia Skorpiona. Hektor wyraził wątpliwości czy Zeta sobie poradzi, ale Edwin zaznaczył, że Zeta to oddział skonstruowany jeszcze przez Ottona. No i problem w tym, że nie wiadomo gdzie jest centrala Skorpiona (Hektor zauważył, że może nie być głowy). I nie wiadomo, czemu Skorpion w ogóle w nich uderzył.
Hektor zauważył, że Skorpion być może nie uderzył w Blakenbauerów. Skorpion uderzył w konkretne instytucje które są interesujące dla Blakenbauerów. Ale to co nie pasuje - dlaczego zabili Henryka Waciaka, swojego agenta...
Rozważania przerwał Hektorowi telefon od sierżant Anny Kajak (dowódcy sił specjalnych Hektora). Zlokalizowali Esterę Piryt. Miejsce jej zamieszkania (Kopalin, apartament na zamkniętym osiedlu "Mirra", 4 piętro). Efektywność grupy śledczej (12 + RND -> 16) jest maksymalna. Udało im się zinfiltrować osiedle - Estera mieszka z mężem i córką, są ochraniani przez jednego człowieka (działającego jako sąsiad) poza ochroniarzami "Mirry". Infiltracja jest tak duża, że mogą nawet kogoś wprowadzić do "Mirry". Mieszkanie Estery jest w jakimś stopniu monitorowane, choć nie wygląda na to by mieszkańcom to w jakikolwiek sposób przeszkadzało.
Rebeka normalnie chodzi do szkoły, rodzice jeżdżą do pracy.
Estera pracuje w oczyszczalni ścieków w Pirogu Dolnym. Pracuje zwykle 8-16, choć czasem są okoliczności wymagające inaczej normowanych godzin.
Część grupy śledczej została skierowana do szukania czegokolwiek odnośnie śladów pozostawionych przez snajpera. Może coś się znajdzie.

Edwinowi zaproponowano plan zawierający wyciek czegoś z oczyszczalni by Estera musiała siedzieć na oczyszczalni i podmianę Aliny. Przeskanowanie pamięci i oddanie Estery. Tak, by Skorpion się nie dowiedział. Edwin zaznaczył, że przecież jeśli jest chroniona w domu, to najpewniej też jest chroniona w miejscu pracy, ta metoda nie zadziała.
Po długim i skomplikowanym planowaniu stanęło na następującym planie: gdy Estera jedzie do pracy, grupa Zeta ją zgarnia pod lasem. Jedzie Zeta, Edwin, Alina (z cudzą twarzą) i Hektor. Hektor robi memscana na szybkie procedury, przenoszą się do lasu i tam robią dokładne skany. Potem zmieniają Esterze pamięć i puszczają ją wolno.
Edwin zabezpiecza i asekuruje sytuację.
Jadą czarnym vanem z czarnymi szybami.

Plan się udał. Samochód został zatrzymany przez członków Zety, Estera straciła przytomność (Zeta pomogła). Jak Edwin i Hektor (z Aliną) dotarli, Estera już była nieprzytomna w lesie a Zeta ochrania cały teren. Jako, że procedura mówi - problemy do Skorpiona, brak problemów to nie Skorpion a tylko telefon do pracy, Alina dostała zadanie zadzwonić do pracy. Alina uspokoiła przełożonego Estery, że jest awaria samochodu i że nie ma problemu.
Hektor przygotował mindscan na Esterze. Po kolei, interesują go następujące zagadnienia:
- dlaczego zginął Waciak?
- jaką pełni Estera rolę w Skorpionie?
- przełożeni, kontakty...
- inwigilacja w prokuraturze i innych źródłach
- dlaczego dołączyła do Skorpiona?
- jaki jest cel Skorpiona?
- inni magowie współpracujący ze Skorpionem
- o co chodzi z kliniką?
(14 vs 6-7-... w czym Hektor ma +2 -> 12). Następujące odpowiedzi: 
- Skorpion uderzył w klinikę, ponieważ klinika zajmowała się jadem Dromopod Iserat. Interakcje jadu z osobami potraktowanymi jadem spowodowały śmierć co najmniej dwóch agentów Skorpiona. Innymi słowy, uderzenie w klinikę było samoobroną. Zwłaszcza, że jad Dromopod Iserat jest istotną częścią strategii defensywnej Skorpiona. Jako, że Edwin zaprzeczył, że miał z tym coś wspólnego to wskazuje wszystko na trzecią siłę - ktoś chce nakręcić Skorpiona przeciw Blakenbauerom.
- Skorpion jest organizacją chroniącą ludzi przed magami. Jest organizacją defensywną, skupiającą się na ochronie. Estera dołączyła, ponieważ jej mąż (człowiek) niejeden raz ucierpiał już od magów. Estera walczy więc o swoją przyszłość, o swoją rodzinę.
- Przełożeni i kontakty Estery: jej mąż, Cezary jest członkiem Skorpiona. Jej córka jest chroniona przez Skorpiona, ale nie jest członkiem. Dr Marian Kozior jest jej przełożonym, kontaktują się telefonicznie. Wiadomo, że prowadzi badania nad artefaktami. Dodatkowo, zna Henryka Waciaka (przyjaciel z dawnych czasów, on ją zwerbował), jest Marian Truteń (agent liniowy, kolega, dziennikarz). Jeszcze jest kilku członków Skorpiona pracujących z nią w oczyszczalni.
- O Dromopod Iserat: ona, Estera Piryt jest jedyną czarodziejką pracującą z Dromopod Iserat w Skorpionie. Ona jest jedyną, która umie z nimi pracować i utrzymać je przy życiu. Z domu Estera pochodzi z rodu Myszeczków i specjalizuje się w alchemii i jadach Dromopod Iserat. Jady D.I. przekazywane są agentce Skorpiona którą Estera zna pod pseudonimem "Diwa". "Diwa" jest człowiekiem.
- Jeśli chodzi o inwigilację: Skorpion jest raczej małą organizacją, skupioną bardziej na ochronie niż ataku. Skorpion nie ma szerokiej siatki, nie atakuje a jad kontroluje poszczególnych rzadkich magów którzy mogą być niebezpieczni. Skorpion raczej nie kontroluje ludzi. Współpracuje z nimi, nie kontroluje ich. Skorpion jest organizacją idealistyczną. Tu Edwin zaznaczył, że sytuacja jest bardzo ciekawa - Estera jest osobą kluczową dla Skorpiona, bo bez Estery nie ma Dromopod Iserat. Bez tego, ludzie nie są w stanie kontrolować magów. A lekarz magiczny (zwłaszcza Edwin) rozpozna ślad Dromopod Iserat. I dojdzie do rozpadu Skorpiona pod jego własnym ciężarem.
- Hodowla Dromopod Iserat znajduje się w oczyszczalni. Jest świetnie chroniona, ale Estera nie wie w jaki sposób. Jest to jednak jedyna hodowla jaką ma Skorpion.

Edwin i Hektor zdecydowali się puścić Esterę wolno. Oddział Zeta się wycofał, Skorpion się nie zorientował a Estera spóźniła się godzinę do pracy.

Hektor zaczął przekonywać Edwina i Margaret do współpracy ze Skorpionem. Ich cele nie są bardzo odmienne. Zarówno Skorpion chce chronić ludzi, jak i Blakenbauerowie chcieli przecież ludziom pomóc. Edwin i Margaret uznali to za bardzo zabawne i urocze, ale w sumie nie jest to zły pomysł, żeby wejść w sojusz z taką organizacją. Jednak zanim do tego dojdzie, Alina poprosiła Edwina o to, by zrobił jej test jadu Dromopod Iserat w bardzo kontrolowanych warunkach. Edwin po namyśle się zgodził - w obecnej chwili Skorpion nie wie o ruchu Blakenbauerów, więc to jest dobry moment. (kość szczęścia -> 15). Szczęśliwie, jad Dromopod Iserat nie działa szczególnie szkodliwie na Alinę. Ma inne objawy i jakkolwiek toksyczny, nie robi jej nieodwracalnej krzywdy. Tylko jakaś padaczka. Odtrutka za to jest szczególnie toksyczna. Edwin został poproszony przez Hektora by przygotował jakąś sensowniejszą odtrutkę dla Aliny. Zajmie się tym, jak tylko postawi Klemensa na nogi.

Hektor i Edwin postawili wcześniej porwanych ludzi na nogi. Tego nożownika i polujowca. Następnie puścili ich wolno informując, że bazując na informacjach od nich zdobytych chcą wyciągnąć rękę do Skorpiona. Chcą się spotkać z jakąś osobą decyzyjną. To całe wydarzenie było niefortunną okolicznością. Blakenbauerowie chcą się spotkać z członkami Skorpiona z intencją nawiązania kontaktu a potencjalnie z intencją współpracy. Dlatego zarówno prokurator Hektor Blakenbauer jak i doktor Edwin Blakenbauer chcą się spotkać. Na bezpiecznym terytorium. Wszelkie próby ataku zostaną uznane za uderzenie w wyciągniętą rekę.
Niskopoziomowi członkowie Skorpiona dotarli do swoich nieistotnych kontaktów (które już są odcięte). Trzy dni później doszło do propagacji informacji do Skorpiona. Ta oferta zdecydowanie zaskoczyła Skorpiona. Tydzień później, Skorpion zdecydował się na odpowiedzenie na ofertę spotkania.
Skorpion zaproponował jako miejsce spotkania Skwer Przyjaźni w Kopalinie. Data: za 2 dni.

Asekuracja Hektora i Edwina: czarny van z Grupą Zeta i grupa specjalna Hektora. Na miejscu, dużo wcześniej, znajduje się już Alina - w innej twarzy. Przybrała twarz staruszki karmiącej regularnie gołębie, która regularnie się kręci na skwerze. 

FAZA PRZED SPOTKANIEM: 
Alina, bez okablowania i wsparcia próbuje zidentyfikować wszystkie istotne osoby które mogą należeć do obstawy Skorpiona (7 + 1 -> 8). Alina znalazła dwóch tajniaków. Są dobrze zamaskowani, ale nie dość dobrze. Nie są uzbrojeni.

FAZA SPOTKANIA: 
Gdy tylko Hektor oraz Edwin weszli na Skwerek, padł strzał. Dorota Gacek, przyczajona i polująca na Edwina już od dłuższego czasu, znalazła w końcu swoją ofiarę na widoku. Strzeliła do niego, by trafić artefaktem przygotowanym przez Karola Poczciwca. I, jak to Dorota, trafiła - prosto w przedramię, robiąc bruzdę i doprowadzając do rozbryzgu krwi. Po oddaniu strzału, oddaliła się spokojnie. Edwin padł na ziemię i zaczął dygotać, uderzony uczuciami gwałtu i cierpieniami tym wywołanymi.
Natychmiast Hektor odpalił wezwanie, pojawiła się asekurująca go grupa specjalna. Świetnie wyszkolony oddział miał jedynie wycofać Hektora i Edwina, nie strzelał do nikogo. Tymczasem Alina, nadal z boku, miała okazję zaobserwować dowodzącą po stronie Skorpiona (7+1 vs 6 -> 7). Ta postać próbuje opanować sytuację. Alina zauważyła, że jeden ze sprzedawców hot-dogów celuje w Hektora, więc (10 vs 10 -> 12) go postrzeliła w rękę, po czym (free action bo vs 10) spróbowała zrozumieć cel agenta Skorpiona (7 - 1 vs 5 -> 7). Alina zauważyła, że agent Skorpiona próbuje powstrzymać rozlew krwi, próbuje zbudować defensywny kordon ale zakazuje strzelać i każe osłaniać i wypatrywać strzelców.
Alina gra zgodnie z rolą. Oddala się trochę i chce zostać w zasięgu (5/7/9 vs 7+2 ->5 vs 13). Alinie udało się pozostać blisko centrum wydarzeń, ciągle w roli.
Przez telefon szybko przekazała grupie specjalnej, że to nie Skorpion strzelał. Hektor dowiedział się o tym asap, natychmiast zatrzymał siły policyjne i kazał NIE strzelać.

Chwilowo obie strony są na skwerku, obie gotowe do otwarcia ognia, ale żadna z nich niechętna do rozpoczęcia walki. Hektor puścił Zetę by grupa znalazła i złapała snajpera. Oczywiście, nie udało się to. Nie polując na Dorotę. Hektor wysłał negocjatora do Skorpiona. Z krótkofalówką i rozkazem, by nawiązał połączenie.

KONTAKT NAWIĄZANY: 
Agent Skorpiona nazywa się Tymoteusz Dzionek. Zaczął od paktu nieagresji. Chwilę porozmawiali z Hektorem, gdzie Hektor zarzucił mu okrutne i nieludzkie działania a Tymoteusz zauważył, że przecież Blakenbauerowie są nie lepsi.
Cóż. Edwin jest w złym stanie, więc Hektor i Tymoteusz zdecydowali się odroczyć negocjacje. Ale mając świadomość dobrych intencji. I potencjalnie dążąc do sojuszu między Blakenbauerami a Skorpionem. Zwłaszcza, jeśli Blakenbauerowie mogą pomóc Skorpionowi ukryć ślady Dromopod Iserat a Skorpion może zapewnić kontakty i środki Blakenbauerom...
 
 ---
 
Gdy mówili o potencjalnej współpracy Blakenbauerów i Skorpiona:
Hektor: "Ale nadal mamy problem magów pod wpływem jadu".
Edwin: "Masz rację, drogi bracie! Ale jeśli do nich wyciągniemy rękę i obiecamy im ochronę ze strony Blakenbauerów... jest szansa na współpracę i na usunięcie tego straszliwego procederu w bezpieczniejszy sposób."

# Lokalizacje:


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Trzech Myszy
                        1. Aleja Sowińskich
                    1. Dzielnica Owadów
                        1. Skwer Przyjaźni
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Prywatne osiedle Mirra
                        1. Apartament Estery, Cezarego, Rebeki Piryt.
                1. Piróg Dolny
                    1. Obrzeża
                        1. Oczyszczalnia ścieków
                    1. Droga do Kopalina
                        1. Klinika Nadzieja

+ Stakes

Żółw:
- Czy Dorota wyciągnie Esterę od Blakenbauerów? 
Nie wystąpiło

- Czy zapas Dromopod Iserat Skorpiona zostanie unicestwiony?
Nie. Nie wystąpił stan wojny.

- Czy Edwin zostanie ukarany przez Dorotę?
Tak, i to boleśnie.

Gracze:
- Czy mentorowi uda się nawrócić któreś ze swoich podopiecznych?
Nie wystąpiło

- Czy Dalia będzie na końcu tej misji usatysfakcjonowana?
Tak, powinna być

- Czy Edwinowi udało się pomóc szukającej od dawna odpowiedzi rodzinie?
Nie wystąpiło

# Idea:

- Wprowadzenie Saith Catapult w wypadku potencjalnej śmierci Hektora / wyzwolenia Bestii.
- Wprowadzenie Doroty Gacek jako zemsty Dalii na Edwinie.
- Czy Estera trafi w ręce Blakenbauerów? Czy jeszcze nie teraz?
- Naciśnięcie Blakenbauerów: to się nie skończy póki nie dojdą do ważniejszego agenta.
- Próba porwania i zdominowania Hektora przez Skorpiona.
- Pokazanie, że nie tylko Hektor wchodzi twardo ;-).

# Zasługi

* mag: Hektor Blakenbauer jako główny rozgrywający rodu Blakenbauerów, ku zdziwieniu samego Hektora
* vic: Alina Bednarz jako osoba znajdująca się we właściwym miejscu we właściwym czasie we właściwym przebraniu
* czł: Klemens X jako osoba, która szybko strzela i równie szybko ginie
* mag: Edwin Blakenbauer jako Blakenbauer zdecydowany pójść na wojnę ze Skorpionem
* mag: Margaret Blakenbauer jako biowojenna maszyna zagłady zakochana w wirusach
* mag: Estera Piryt jako alchemik, ekspert od magicznych chemikaliów nieco nieświadoma działań Skorpiona
* mag: Dorota Gacek jako terminus*zabójca*mściciel chcący pomścić zniewagę wobec Dalii Weiner
* czł: Kleofas Bór jako były szef sił specjalnych Hektora, którego szczęście (i życie) zawiodło (KIA)
* czł: Anna Kajak jako aktualna przełożona sił specjalnych Hektora, która jeszcze żyje i daje radę
* czł: Jan Fiołek jako snajper Skorpiona, który nigdy jeszcze nie zawiódł
* vic: Marian Kozior jako naukowiec specjalizujący się w artefaktach. Ma stopień doktora.
* czł: Adam Wąż jako ochroniarz Estery Piryt, nieświadom Skorpiona i magii
* czł: Cezary Piryt jako mąż Estery i jednocześnie agent biurowy Skorpiona. Jest świadomy mniej więcej czym Skorpion się stał...
* czł: Andrzej Chezyr jako mentor etyki, kiedyś mag, osoba bardzo szanowana i kochana przez Edwina i Margaret 
* vic: oddział Zeta, ośmiu superżołnierzy zbudowanych przez Ottona swego czasu. Szturmowa mordercza grupa, lojalna Blakenbauerom.
* czł: Tymoteusz Dzionek jako agent średniego szczebla Skorpiona oddelegowany jako łącznik z Blakenbauerami. Na stracenie, jak co.