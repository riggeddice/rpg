---
layout: inwazja-konspekt
title:  "W co gra Sądeczny?"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171024 - Detektyw, lecz nie Sądecznego (PT, KD, KG)](171024-detektyw-lecz-nie-sadecznego.html)

### Chronologiczna

* [171024 - Detektyw, lecz nie Sądecznego (PT, KD, KG)](171024-detektyw-lecz-nie-sadecznego.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

* Ż: W jaki sposób Nowa Melodia płaci haracz Sądecznemu? Nie ma tam magów.
* K: To jest posłaniec, nawet nie członek mafii. Ma przybyć do punktu A i odnieść paczkę do punktu B. Zygmunt Brandon.

## Potencjalne pytania historyczne:

* Czy Sylwestrowi udało się podnieść standing Świecy w okolicy przez wykorzystanie sił wspomagających portalisko?
    * Domyślnie: TAK
* Czy Sądeczny znajdzie nowy sposób potencjalnej regeneracji dziecka Dukata przy użyciu Harvestera?
    * Domyślnie: TAK
* Czy Myszeczce uda się wyczyścić magitrownię od Oktawii?
    * Domyślnie: TAK

## Misja właściwa:

**Dzień 1**:

Piękny poranek dla Pauliny. Dracena nadal na L4, Maria skanuje okolice Wieży Wichrów. Dracena czuje się już lepiej, acz nadal jest nieco niesprawna - zwłaszcza przy dużej ilości energii magicznej. Dracena zdecydowała się pójść do Nowej Melodii - jest tam kilka osób, ludzi, którzy nie chcą płacić Sądecznemu. Jak punk, jak cybergoth może się opłacać mafii?! Przecież to nie ma żadnego sensu. Dracena obiecała Paulinie, że nie zamierza walczyć - ona chce tylko znaleźć sposób w jaki może pokojowo wziąć grupkę misfitów i nie opłacać się mafii. Paulina jest sceptyczna, ale niewiele może zrobić...

Paulina połączyła się hipernetowo z Andżeliką Leszczyńską. Połączenie hipernetowe jest słabe, ale jest... powiedziała Andżelice, że tu jest Tamara, najpewniej wbili się do nieznanej sobie Fazy i 
ogólnie KADEMowy sprzęt sprzedany niekompetentnym magom doprowadził do tej sytuacji. Andżelika potwierdziła, że się pojawi. Maksymalnie cztery osoby. Jak rozumie, nie ma tu silnej Świecy? Nie. Ok...

Maria skontaktowała się z Pauliną po soullinku. Powiedziała jej, że coś się dzieje w okolicy Wieży Wichrów; na osiedlu Dromader - Dobrocień coś próbuje wykopać. Ma ekipę. Podobno to niewypał i ewakuowano okolicę. Paulina wsiadła w swojego awiana i poleciała w tamtym kierunku - a jej awian ma detektor elementali. Plotka o niewybuchu to ekstra powód by się tam pojawić... szczęśliwie, Paulina nie zauważyła nic elementalopodobnego. Paulina zaparkowała awiana i podeszła parę kroków odwiedzić Dobrocienia. Zatrzymał Paulinę strażnik, potem jednak wysłała msg do Dobrocienia - i on przyszedł.

Dobrocień powiedział Paulinie, że mają niewypał. Po hipernecie zaznaczył, że coś magicznego i nieaktywnego. Maria zauważyła Paulinie po soullinku, że coś ostatnio dużo niewypałów w ostatnim tygodniu - ekipy sporo wykopują. Paulina się postawiła po hipernecie Dobrocieniowi - była rezydentką i to JEST jej teren. Chce wiedzieć, może pomóc. Udało jej się, Dobrocień pozwolił jej wejść. Paulina od razu rozpoznała nieaktywny kolektor Harvestera. Tylko po co Sądecznemu Harvester? Jeśli miałby użyć go do pomocy dziecku Dukata... to będą ogromne ofiary w ludziach. A Dukat mógłby to zrobić.

Przepełniona tymi niewesołymi myślami, Paulina asekurowała Dobrocienia. Nie stało się NIC złego. Kolektor jest martwy a Harvester jest nieaktywny. Na razie...

Paulina przepytała Dobrocienia o tą sytuację. Jest to dość niejawna sprawa, więc Dobrocień nie chce mówić, ale Paulina potrafi być dość... przekonywująca. W końcu uratowała mu życie i w ogóle. Dała radę, Dobrocień powiedział co chciała:

* Chcą zrekonstruować CAŁY Harvester. Zaczynają od komponentów jakie łatwo znaleźć.
* Mają tyle, że umieją znajdować kolejne; ale to jeszcze potrwa. Nie mają tych najważniejszych.
* "To jest jakieś medyczne urządzenie napędzane Efemerydą czy coś..."

Obawy Pauliny zostały potwierdzone. Posiedziała by pomóc, po czym wróciła do siebie, mocno zmartwiona... Maria soullinkiem zaproponowała jakąś formę sabotażu? Paulina powiedziała Marii, że tu może pomóc Tamara...

Paulina skontaktowała się po hipernecie z Kajetanem. Powiedziała mu, że mafia szuka Harvestera... dała mu kontekst, że może chodzić o ratunek dziecka, ale bez szczegółów. Mają trochę kolektorów; dość, by znajdować kolejne i transmitery. Kajetan Paulinę zaskoczył - szukał. Sprawdzał. Nie znalazł kto stworzył Harvester. Nie byli to lokalni Weinerzy, nie było to Bractwo Pary. Nie wiadomo w chwili obecnej kto to zrobił. Patrząc po poziomie technologicznym, musi mieć nie więcej niż 30 lat - chyba, że sam się rozprzestrzenia. Nie wiadomo jaką pełni rolę i do czego służy. Po prostu JEST.

Kajetan zgodził się na sugestię Pauliny - on zostanie twarzą walki z Harvesterem. Paulina zostanie w cieniu. Ona nie stanie przeciwko mafii. Kajetan też szybko Paulinie strzelił, że Tamara pomaga w odbudowie portaliska a Myszeczka chce zniszczyć Oktawię z magitrowni.

**Dzień 2**

Następnego ranka przybyła do Pauliny uśmiechnięta Kaja. Poprosiła Paulinę o wyniki z sensorów. Paulina pokazała jej wszystko co NIE pochodzi z magitrowni. Kaję zaciekawiły te wyniki - widać wyraźnie, acz zachowuje perfekcyjny uśmiech i nie mówi nic ciekawego. Powiedziała Paulinie, że aktualny Rezydent nie jest chyba zainteresowany tym regionem tak jak powinien, widząc ten wykres. 

Kaja powiedziała Paulinie co ją martwi:

* Eliksir miłości: Sądeczny dąży do zbudowania eliksiru miłości który po wypiciu infekuje Wzór ofiary. Ze szczególnym przypadkiem rodu Bankierz. Nie zadziała na Dracenę.
* Hektor: Zdaniem Kai, Hektor jest osobą, która przekazała Wzór niewinnej czarodziejki Sądecznemu - przez to wpadła w jego sidła
* Sądeczny: Zdaniem Kai, Paulina uciekła. I zostawiła teren na pastwę ambitnego mafioso który zakłóca informacje i daje Dukatowi to czego ten chce - budując eliksir.
* Newerja, Ewelina: główne przyszłe ofiary eliksiru miłości. To dałoby Sądecznemu potęgę i oddałoby je w ręce Dukata. Cały region.
* Ewelina w chwili obecnej ma obróżkę i jest wyłączona. Kaja żąda, by Paulina podniosła status Eweliny i ją wyciągnęła jako dowód dobrej woli.

Paulina i Kaja nie zgadzają się co do dalszych działań - zdaniem Kai trzeba pomóc Ewelinie, zdaniem Pauliny trzeba rozłożyć Sądecznego. Niestety, Kaja nie wie wiele o aktualnym Rezydencie... a Paulina nie zgadza się na zrobienie choroby by podnieść status Pauliny na dworze Newerji...

Kaja zagwarantowała, że opóźni działania Roberta Sądecznego na tym terenie. Ma swoje metody. Paulina, zapytana o Wieżę Wichrów powiedziała, że są tam... bardziej naładowane energią elementale. Jest jeszcze jedna rzecz o której nie czuje się komfortowo mówiąc - ale wezwała pomoc. Kaja aż się zdziwiła, że Paulina jest w to zaangażowana (ona nic o tym nie wie) - Paulina powiedziała, że chwilowo miała tam pewne działania. Ale nie ma tam chorób.

Odnośnie planów Sądecznego - Kaja powiedziała, że Sądeczny wykorzystał nie tylko Katię, ale przedtem Anetę Rukolas. W końcu na KIMŚ testuje te eliksiry. A przez to, że Sądeczny ma plan taki jaki ma, Apoloniusz mógł uderzyć w Ewelinę i ją odeprzeć.

Kolejny pomysł Kai - niech Paulina wyleczy Katię lub Anetę. Aneta powinna być teraz na portalisku stabilizować rzeczy dla Sądecznego a Katia normalnie chodzi do pracy...

W końcu Kaja zostawiła Paulinę samą, z jej własnymi myślami. Paulina natychmiast poinformowała o eliksirze miłości itp. Kajetana.

Dracena powiedziała Paulinie casualowo, że udało jej się założyć mały ruch oporu. Niebojowy. Artyści, wolna muzyka, potencjalny festiwal... takie tam. Dracena zaznaczyła, że nie pyta Sądecznego o pozwolenie - ale nie wymierza działania przeciwko niemu. Nazwała to "Festiwal Miłości". NIE, nie będzie tam nadmiaru seksu każdy z każdym - Dracena szybko zaprotestowała - to nazwa. Chodzi o wolność, miłość, radość i samoekspresję. I zrobi to w jakąś sobotę. Ale wpierw przygotuje.

Paulina zdecydowała się Dracenie w tym pomóc. Nie tylko integruje to Dracenę, ale też przez pomaganie Paulina może monitorować sytuację, żeby nie stało się tam coś złego. Mitygować co ciekawsze Dracenowe pomysły A że ma kontakty w lokalnej społeczności... tak, to niezły pomysł.

Oki. Być może coś dowie się od... Oktawii w magitrowni. Wsiadła w samochód i pojechała - zatrzymał ją strażnik. Powiedział, że jest tam członek rządu i obszar jest off-limits. Paulina skontaktowała się hipernetem z Danielem Akwitańskim, który spytał czemu Dracena do pracy nie chodzi i ma L4. Paulina powiedziała, że zaraz L4 się skończy i ona coś chce zrobić. Co tam się dzieje? Tomasz Myszeczka mógłby Paulinie powiedzieć... i powiedział. Oktawia jest niestabilna i niebezpieczna, bo chce zrobić koncert w magitrowni. Jak jej nie pozwolono zrobiła się psychotyczna w rozumieniu "zniszczę magitrownię ALBO mi pozwolicie". Ale to zakłóciłoby pryzmat magitrowni - Oktawia jest zwyczajnie bardzo niebezpieczna. Więc on ma swoje nipustaki by rozwiązać problem i pozbyć się Skażenia Pryzmatu.

Paulina zaproponowała alternatywę - ona tam pójdzie i porozmawia z Oktawią. Rozproszy ją i łatwiej będzie ją rozładować nipustakami. Myszeczce się to spodobało - on się zajmie viciniusami, ona się zajmie odwracaniem uwagi Oktawii. A i Paulina będzie miała okazję dowiedzieć się czegoś od Oktawii...

Paulina złapała Karola Marzyciela i poprosiła go o popodłączanie systemów nagłośnieniowych. On powiedział, że lubi Oktawię i zrobi to bez żadnego problemu i z dużą przyjemnością. Niech młoda posłucha koncertu, niech będzie mogła pośpiewać. Karol powiedział Paulinie, że Oktawia nie jest niebezpieczna - jest bardzo sympatyczna, jego zdaniem.

Paulina weszła do magitrowni. Oktawia rzuciła zaklęcie - pojawiła się sferą Zmysłów. Powiedziała, że najbardziej lubi punk, rock, goth... ogólnie, bunt. Walka z systemem. Paulina zauważyła, że Oktawia wie, że jest efemerydą. Oktawia się perfekcyjnie zintegrowała z magitrownią - gdy rusza rękami holoprojekcji, pojawia się światło, jak na scenie. Czy jest samotna? Tak, trochę tak - ale ma Karola. No i będzie miała koncert; jak będzie trzeba, to się postara by koncert się odbył.

Oktawia jest Przyjazna, Volatile i jest Niezrozumienie między nimi, bo myślą inaczej i na różnych poziomach. Paulina spytała ją, co się stało tam w efemerydzie, gdy Aneta Rukolas i Grzybb weszli do Muzeum Pary z Sądecznym. Paulina jest... skonfliktowana. Wie, że... jest tu by Oktawię rozmontować. I udało jej się. Paulinie, to znaczy. Oktawia powiedziała co się stało:

* Grzybb został w ŚRODKU efemerydy. Na jego miejsce wyszedł Andrzej Farnolis...
* Grzybba da się wyciągnąć z efemerydy.
* Najpewniej stąd wziął się pomysł Harvestera i inne pomysły...

Paulina skupiła się na odwracaniu uwagi efemerydy i dała znać Myszeczce. Powiedziała Oktawii, że muszą się przygotować na koncert. Użyła Heroicznego Chirurga by dać full power. Efemeryda WZMOCNIŁA zaklęcie Pauliny mocą magitrowni. Czas dla Pauliny ZWOLNIŁ.

Odwrócenie uwagi Oktawii: (2), Volatile (3), Przyjazna (-2), To Boli (3). Paulina ma: Chirurga (+2), Gitarę (+2), Nagłośnienie (+2). Paulina wciągnęła Oktawię w piękną rzeczywistość, w świat radości i śmiechu. Oktawia nie zauważyła tego, że zaczyna ją boleć i zaczyna migotać. Gdy zauważyła, było już za późno. Przerażona i skulona z bólu próbowała coś jeszcze zrobić, ale katalista ją disruptował a nipustaki były zbyt żarłoczne. Paulina próbowała jakoś ukoić Oktawię w ostatnich momentach... Oktawia była skulona z bólu, zimna i strachu pod ścianą, migocząc, gdy w końcu została rozproszona.

Wszedł Tomasz Myszeczka z oklaskami. Paulina naprawdę dała radę, poprawiła stan, nikt nie ucierpiał. Super sprawa. A Paulina ma taką smutną minkę...

Gdy Myszeczka sobie poszedł, Paulina poprosiła głośno Marzyciela by przyszedł. Miłośnik magitrowni przyszedł i spojrzał na Paulinę smutno, zranionym spojrzeniem. Paulina powiedziała mu po prostu, że jest lekarzem... nie mogła ich powstrzymać, ale mogła dać jej odrobinę szczęścia. Karol traktował Oktawię jak Córkę (3) i jest paranoiczny (baza 3). Paulina Ukoiła Oktawię (2), Wzbudza Zaufanie (1). Paulinie udało się go przekonać, że MUSIAŁA i że tak jest lepiej - dla samej Oktawii. 


## Wpływ na świat:

JAK:

1. Każdy gracz wybiera 1 element Historii - konflikt / okoliczność, która miała Znaczenie i ma zostać jako zmiana po akcji. MG wybiera tyle, ile jest graczy.
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

1. Kić: Kaja doprowadzi do sytuacji, w której Aneta Rukolas potrzebuje pomocy medycznej.

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy


Z sesji:

* Żółw: 18
* Kić: 6-2=4

* (CLAIM) Grzybb jest do uratowania (Kić)
* Paulina jest kanałami sprzężona z magitrownią; 3-4 dni Paulina musi odwiedzać magitrownię na 2-3h na regenerację. Może tam spać. (Żółw)
* Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
* Jak wbić klin między Fornalisa a Sądecznego? (Kić)
* Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
* Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
* Energia Eteru i Efemerydy Senesgradzkiej mutują inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
* Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
* Kaja przygotuje coś naprawdę dużego (efektowną akcję) - i da radę nie być znalezioną (Żółw)

# Progresja

* Efemeryda Senesgradzka: występuje na wielu Fazach jednocześnie i ma dopływy energii z różnych Faz.
* Efemeryda Senesgradzka: cały czas słyszy Karola Marzyciela w magitrowni Histogram.

# Streszczenie

Dostawszy cynk od Marii, Paulina dowiedziała się o tym, że Sądeczny próbuje zbierać Harvestera. Najpewniej by pomóc synowi Dukata. Po skontaktowaniu się z Kają, dowiedziała się też o planach stworzenia adaptującego się Eliksiru Miłości do zniewolenia Eweliny lub Newerji. Ale skąd Sądeczny ma wiedzę? Na to odpowiedź znalazła u Oktawii, w magitrowni - Sądeczny wyciągnął Fornalisa z Efemerydy, zostawiając tam Grzybba. Przy okazji, Paulina pomogła rozproszyć Oktawię i wyraźnie zestresował ją fakt, że "dziewczynka" była przerażona i nie chciała odejść.

# Zasługi

* mag: Paulina Tarczyńska, ściągnęła KADEM na Mazowsze i ukoiła (rozpraszając ją) Oktawię przed dezaktywacją. Dodatkowo poznała informacje na temat planów i działań Sądecznego. Po Oktawii ma kaca moralnego.
* mag: Dracena Diakon, przechodzi jej L4 i organizuje grupę oporu przeciwko Sądecznemu - a dokładniej, grupę wolnych muzyków, artystów itp. Ma nawet bazę...
* mag: Bolesław Maus, ściągnął od rodu Mausów sprzęt pomagający w naprawie portaliska.
* mag: Sylwester Bankierz, podnosi swój standing i standing Świecy pomagając w naprawie portaliska. A portaliska chroni Sądeczny. Więc: współpracują. Jakoś.
* mag: Andżelika Leszczyńska, dowiedziawszy się o nowej Fazie na Mazowszu organizuje ekipę - ona, Karolina, Marian. I Aegis. Przyjadą Grubym Szymkiem. Przez to - uber siła ognia.
* czł: Maria Newa, monitoruje ruchy Sądecznego i rzeczy dookoła Wieży Wichrów. Dzięki soullinkowi, pomaga Paulinie w negocjacjach.
* mag: Jakub Dobrocień, kapitan zespołu harvestującego Harvestera. Ignorując rozkazy, powiedział wszystko Paulinie - za dużo jej zawdzięcza.
* mag: Kaja Maślaczek, powiedziała Paulinie jak jej zdaniem wygląda sytuacja - Eliksir Miłości Sądecznego. Namawia do nieetycznych działań. Kaja jest groźna... jako sojusznik.
* mag: Kajetan Weiner, który był informowany przez Paulinę o Eliksirze Miłości i o Harvesterze. Zostanie twarzą walki z Harvesterem, bo Paulina nie może.
* mag: Tomasz Myszeczka, wyraźnie martwiła go efemeryda w magitrowni więc rozmontował ją nipustakami z pomocą Pauliny. Bardzo się mu ruchy Pauliny spodobały.
* vic: Oktawia Aurinus, tylko chciała przeprowadzić koncert a skończyła rozproszona przez nipustaki. Jest jak "córka" Marzyciela. Powiedziała Paulinie, że Fornalisa nie ma w Efemerydzie - a Grzybb tam trafił.
* czł: Karol Marzyciel, "przybrany ojciec" Oktawii, opiekun magitrowni, pomógł Paulinie odwrócić uwagę Oktawii i trochę nie może sobie darować.
* vic: Efemeryda Senesgradzka, która aktywnie mutuje świnie w Eterze i dostarcza im energię. Powstała wylęgarnia Skażonych świń...

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Trzeszcz
                    1. Ściernisko Ludowe, gdzie Dracena zdecydowała się zorganizować swój Festiwal Miłości i gdzie postawiła namiot "swojej bazy dowodzenia"
                1. Krukowo Czarne
                    1. Gabinet Pauliny, miejsce spotkania z Kają.
                1. Męczymordy
                    1. Magitrownia Histogram, gdzie doszło do rozmontowania Oktawii przy użyciu nipustaków.
            1. Powiat Ciemnowężny
                1. Burzowiec
                    1. Osiedle Dromader, gdzie Dobrocień wykopywał dodatkowy kolektor Harvestera.

# Czas

* Opóźnienie: 1
* Dni: 2

# Wątki kampanii

* Krzysztofa droga do bogactwa i potęgi
    * Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
* Kociebora droga do bogactwa i potęgi
    * CLAIM: okazja na poznanie i zaprzyjaźnienie się z Kajetanem.
    * Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    * Customizowany awian, nie taki jak Czarny Ptak. Taki czołg.
* Maja Weiner i farma krystalitów
    * Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    * Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    * Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    * Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
    * Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
* Skażenie i wysokie pole magiczne w okolicy
    * Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    * W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    * (Prezes) Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru.
* Echo starych technologii Weinerów
    * Harvester nie ma dość energii by się obudzić
    * Natalia pozyskała bezpiecznie kolejną część artefaktu
* Wieża Wichrów
    * Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    * Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    * Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    * Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    * Filipowi uda się utrzymać firmę JAKOŚ.
    * Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
* Elementale i awiany
    * (Foczek) Przepakowane elementale. Więcej zabawy, więcej energii.
    * Awiany nowego typu - lepsze, fajniejsze, niebezpieczniejsze
    * Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    * CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    * Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    * Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    * Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Daemonica-elementalna.
    * Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    * Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    * Jakiego typu viciniusem stanie się ten awian?
* Powrót Srebrnej Świecy
    * CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    * Sylwester został zesłany tu przez Newerję
    * Sylwester: przeprowadził udaną akcję zdobycia transportu Dukata
    * Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    * fakt: Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    * fakt: Tamara robi się bardzo podejrzliwa wobec Myszeczki
    * Myszeczka staje po stronie rezydenta Świecy
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Oktawia w power suit ma dostęp do tego co wie power suit. Wie to, co Tamara przekazała.
    * Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    * Sylwester opłaca nowego przedsiębiorcę mającego założyć konkurencyjną małą magitrownię. By uniezależnić się od gróźb. Aha, czyn nieuczciwej konkurencji.
* Działania Roberta Sądecznego
    * Robert dostaje haracz od "Nowej Melodii"
    * Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    * rezydent polityczny Dukata u Myszeczki
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i magitrownia nie miałoby tragedii
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał
    * fakt: Sądeczny dostarczy maga, który zajmie się glukszwajnem
    * Sądeczny nasyła Zofię na szpiegowanie Tamary
* Katia Grajek
    * To Hektor pomagał w budowaniu eliksirów dla Katii
    * Aneta wróciła wtedy z Sądecznym ORAZ wie, co tam się stało
* Dracena, wracająca do świata ludzi
    * CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    * CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    * CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    * Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    * Fakt: Dracenę wylano z "Nowej Melodii" za radykalizm
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana
* Gabriela Dukata droga do ostatecznej medycyny dla dziecka
    * CLAIM: Dukat lubi Paulinę (Kić)
    * W jaki sposób byty wchodzące w elementale mogą pomóc dziecku Dukata?
    * Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    * Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
    * Dukat zatrzymał działania Sądecznego ORAZ wstrzymało to niszczenie Legionu
* Osobliwości lokalne
    * CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    * CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    * Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
* Kluczowa rola glukszwajnów wobec efemerydy Senesgradzkiej
    * CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    * W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
    * Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera
    * Natalia ma eksperta od glukszwajnów; człowieka kiedyś pracującego dla Myszeczki
* Wolność Eweliny Bankierz
    * CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką"
* Oktawia, echo Oliwii
    * CLAIM: Oktawia dorośnie i zniknie. (Kić)
    * Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
