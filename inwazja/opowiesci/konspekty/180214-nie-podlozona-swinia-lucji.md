---
layout: inwazja-konspekt
title:  "Nie podłożona świnia Łucji"
campaign: wizja-dukata
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180110 - Odzyskana władza Pauliny](180110-odzyskana-wladza-pauliny.html)

### Chronologiczna

* [180110 - Odzyskana władza Pauliny](180110-odzyskana-wladza-pauliny.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

![Wizualny opis sesji](Materials/180214/180214-rpg.jpg)

* Abelard Maus chce odzyskać Łucję jako TERORYSTKEM; wysłał Kaspiana
* Pojawiła się świnia straszliwej mocy
* Melodia poluje na lokalnego kralotha i znajduje kult kultornicy.

### Wątki konsumowane:

brak

## Punkt zerowy:

## Potencjalne pytania historyczne:

* Czy Łucja trafi w ręce Abelarda? (NIE ALE ZNISZCZENIA, długość: 3)
* Czy Łucja się skazi przez czarnotrufle Esuriit? (TAK, długość: 3)
* Czy Łucja zostanie upokorzona silnie? (NIE, długość: 3)
* Czy Łucja odpłaci ogniem Myszeczce (TAK, długość: 3)
* Czy Melodia rozwali kultornicę (TAK, długość: 3)
* Czy kultornica zamaskuje ślady kralotha (TAK, długość: 3)
* Czy Kaspian zostanie szukać Łucji (TAK, długość: 3)

## Misja właściwa:

**Dzień 1**:

Daniel ponaprawiał magitrownię. Paulina odzyskała częściowo kontrolę nad terenem, zwłaszcza korzystając z brutalnej siły Wiaczesława. Życie stało się lepsze. A Melodia szuka kralotha.

Przychodzi Tomasz Myszeczka do Daniela. Chce się pozbyć Łucji - bo jest niebezpieczną Mauską. Rozpędziła mu świnię. Zniszczyła reputację. Mauska musi odejść. Myszeczka prowadzi tu ostry świński biznes - niestety, jego sojusznicy i przeciwnicy widzieli jego upokorzenie. Myszeczka potrzebuje upokorzenia i dewastacji tej Mauski. Dla biznesu. Daniel próbował przekonać Myszeczkę, że musi sprawa przejść przez prokuraturę magów. I nie, nie chce Tomasz tu z Pauliną zadzierać. KONFLIKT: 

* Myszeczka: 4, finanse>etyka, ranny smok, twardy negocjator
* Daniel: 4, przyjazny Daniel, prokuratura się tym zajmie, nieantagonizowanie, negocjacje z klientami
* Wynik: Skonfliktowany. Myszeczka akceptuje rozstrzygnięcie Daniela, acz zakłada, że to się stanie w tydzień.

Do Pauliny trafia komunikat Joachima Mausa. Dyplomata chce rozmawiać. Chce wysłać inkwizytora by przechwycić Łucję. Paulina absolutnie odmawia. Joachim zaznacza, że przez skargę Myszeczki coraz wyżej zaczyna kaskadować problem - coraz więcej potężnych magów zaczyna się tematem interesować. Mausy się boją. Paulina ciężko westchnęła. Cóż, może faktycznie niech Łucja posprząta po Myszeczce różne świnie ;-).

Paulina się zgodziła, pod warunkiem, że ona porozmawia z Myszeczką. Na pewno nie Joachim Maus. Joachim się skwapliwie zgodził - jeśli sprawa będzie rozwiązana. Paulina weszła w konflikt:

* Joachim: 6, dyplomata, smok na karku, wspomagany prastary Maus
* Paulina: 4, Abelard to pudel, Joachim nie chce krzywdy Łucji, Joachim nie zna sytuacji, naprawić świat, twarda blokada
* Wynik: SUKCES. Maus się wycofał i poprosił.

Paulina skontaktowała się z Bolesławem Mausem. Poprosiła go o radę w sprawie Łucji. Powiedziała jak wygląda sytuacja. Spytała kto może pomóc Łucji - by sama nie była tam zagrożona. Paulina próbuje ją ochronić. Naskoczyła ostro na Bolesława - niech jej pomoże ze wszystkim co wie o Łucji albo będzie kłopot. (6v4->SS). REZYDENTKA GO ZMUSIŁA. Powiedział Paulinie, że Łucja ucieknie. Uciekła Karradraelowi, ucieknie i Paulinie / Myszeczce. Zapytany czy pomoże Paulinie w przekonaniu Łucji, niechętnie się zgodził... acz ona nikogo nie słucha - nawet jego.

Kolejną osobą odwiedzoną przez Paulinę był Daniel. Trzeba przekonać Łucję do tego by się zgodziła i przekonać Tomasza, by ów zaakceptował takie rozwiązanie i wszystko powycofywał. A oboje martwią się, że Łucja się po prostu nie zgodzi... więc do akcji wchodzi Melodia. Paulina w trybie awaryjnym ściągnęła Melodię. Ta poinformowała radośnie, że mają kultornicę. Jest kultornica - jest kraloth. Paulina powiedziała, że Melodia musi przekonać Bolesława, by to na Myszeczkę zrzucić winę. W ten sposób da się przesunąć Łucję by pomogła Bolesławowi a nie Myszeczce. Sprawiedliwość - served, Maus - no problemo.

Melodia jest 100% za. Z przyjemnością pomoże. Będzie z pozycji Bolesława działać przeciwko Myszeczce, by pomóc Łucji. Czyli Melodia ma nakręcić Myszeczkę w sposób adekwatny. A Daniel ma przekonać Myszeczkę.

Paulina za to wciąga Bolesława w grę. Uzmysłowiła mu, że aby pomóc Łucji, Myszeczka musi mieć problem. A problem ma Bolesław - potrzeba mu katalistów. Przez świnie. Więc: problem świń wywołany przez Myszeczkę sprowadza się do tego, że Bolesław ma ansy do Myszeczki. Bolesław zauważył, że seiras kazałby zrobić "drop charges". Paulina zauważyła, że już będzie dobrze. Nie chodzi o formalne załatwienie rzeczy... chodzi o rozwiązanie pewnego problemu ;-).

Czyli Bolesław ma zażądać reperacji od Myszeczki. Przy użyciu Melodii Diakon. Jest jak najbardziej za, zwłaszcza, jeśli to pomoże Łucji.

Melodia skutecznie wyprowadziła Myszeczkę z równowagi żądaniem. Jest niewrażliwa na przekonanie ze strony Myszeczki, bo... się zakochała w Bolesławie i Abelard nie zmieni jej zdania ;-).

Daniel poszedł do wkurzonego Myszeczki. Nasłuchał się o nimfomankach zakochanych i nieszczęściu Tomasza. Daniel przekonał go - Myszeczka się zgadza. Łucja pomaga Bolesławowi. Ale Myszeczka jest na czysto i z Bolesławem i z Łucją... bo nie chce się już pieprzyć. I Myszeczka odwołuje problemy z Joachimem.

Bolesław się bezproblemowo zgodził. Jeszcze Łucja... zdecydowali się zagrać w "dobry glina, zły glina".

Przychodzi Paulina do Łucji. Powiedziała, że załatwiła, że inkwizytor się tu nie pojawi. Łucja zauważyła, że zawsze może zażyć truciznę jak stanowi problem dla Pauliny. Paulina pokazała Łucji, że ta nie ma wyjścia i jest na parolu. Łucja powiedziała, że niech Paulina ją lepiej zamknie, da jej 5 minut i zasady parolu nie obowiązują - Paulina zobowiązała Łucję do czegoś a nie miała prawa.

Paulina widząc sytuację trochę się wycofała - Łucja jeszcze NIE jest zobowiązana do niczego. Łucja zaznaczyła, że woli inkwizytora. Paulina wyszła.

Melodia zauważyła, że Łucja to słodkie dziecko. Paulina pozwoliła jej pójść spotkać się z Łucją. Po kilku godzinach Melodia wyszła z kocią minką (19/20). Z punktu widzenia Melodii, Łucja jest słodka i prosta w obsłudze - dostała od Melodii kilka rzeczy do przemyślenia.

Daniel odwiedził Łucję i powiedział, że można pomóc przez pomoc Bolesławowi na portalisku. Łucja się zgodziła. Ale konto czyste. Łucja... zaprotestowała. Daniel ma tu płomienną przemowę o kamień na kamieniu i zniszczeniach. Łucja, nieszczęśliwa, się zgodziła - nigdy więcej wojny. Ona nie chce wojny... jeśli to zapobieże? Wpływ Melodii wciąż silny.

Daniel szczęśliwy. Mausowie szczęśliwi. Kryzys zażegnany.

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (7)
    1. Łucja używa lekarstwa z czarnotrufli Esuriit - będzie śmiesznie (3)
    1. Łucja jest pod ogromnym wpływem Melodii - Melodia jest jej guru (3)
    1. Melodia skupiając się na Łucji nie złapała kultornicy na czas mimo że kraloth wiedział; kult się poszerzył (3)
1. Kić (4)
    1. Bolesław uważa Paulinę za osobę sprzyjającą pozytywnym Mausom; jest jej zwolennikiem (2)
    1. Katia Grajek jest operacyjna od następnej sesji (3)
1. Raynor (3)
    1. Daniel jest w stanie wykorzystać Melodię do pomocy jak długo są na podobnym terenie (2)
    1. Pojawia się wartościowy klient który podbuduje status Myszeczki i wymaga jego pełnej uwagi (3)

# Streszczenie

Mausowie nie zaprzestali działań mających na celu wydanie Łucji by zadowolić Tomasza Myszeczkę. Połączone siły Daniela i Pauliny sprawiły, że Łucja się uspokoiła i poszła pomagać Bolesławowi na portalisku, Tomasz, Bolesław i Łucja wyzerowali sobie konto a Mausowie zostawili Łucję w spokoju. Kraloth na którego Melodia polowała zniknął jej z radaru. Paulinie udało się postawić na nogi Katię Grajek a do Myszeczki przyszedł Ważny Klient, więc ów nie ma czasu przeszkadzać...

# Progresja

* Daniel Akwitański: póki Melodia Diakon operuje na Mazowszu, pomoże Danielow jak on poprosi
* Paulina Tarczyńska: zyskuje sojusznika w Bolesławie Mausie (właścicielu Portaliska Pustulskiego)
* Łucja Maus: do budowania swojego lekarstwa wykorzystuje czarnotrufle Esuriit a nie zwykłe czarnotrufle
* Łucja Maus: jest pod ogromnym wpływem Melodii Diakon - uważa Melodię za potencjalnego guru
* Melodia Diakon: oczarowała Łucję Maus; ma na nią ogromny wpływ
* Bolesław Maus: uważa Paulinę Tarczyńską za osobę sprzyjającą pozytywnym Mausom; ogólnie jej sprzyja
* Katia Grajek: następnego dnia już nadaje się do pełni działania
* Tomasz Myszeczka: pojawia się potencjalnie bardzo wartościowy klient potrzebujący pełni uwagi Myszeczki
* Hlangoglormo: skutecznie dał radę się schować przed oczami Laragnarhaga i Melodii Diakon na krótki czas
* Hlangoglormo: ma potężny kult wraz z kultornicą (lub kilkoma?), które go chronią

## Frakcji

# Zasługi

* mag: Daniel Akwitański, wplątany w konflikt między Łucją a Tomaszem doprowadził do tego, że Melodia uciemiężyła Tomasza. Wyprowadził to na "wszyscy mają czyste konto".
* mag: Paulina Tarczyńska, ściąnęła Melodię do rozwiązania konfliktu Łucja - Tomasz, zajmowała się też dyplomacją po stronie Mausów. Wszyscy mają czyste konto. Uratowała Łucję.
* mag: Łucja Maus, postawiła się jakimkolwiek reparacjom wobec Myszeczki i była skłonna próbować uciec inkwizytorowi Mausów. Skończyła pomagając przyjacielowi na portalisku - jak chciała.
* mag: Bolesław Maus, zaczął jako osoba niechętna Paulinie, bo "Łucję mu biją". Skończył z Łucją mu pomagającą i bardzo pro-Paulinowo.
* mag: Joachim Maus, negocjator Mausów stojący po stronie Łucji, ale musi bronić pozycji Abelarda. Między młotem i kowadłem. Szczęśliwie, Paulinie się udało rozmontować konflikt.
* mag: Tomasz Myszeczka, chciał zemścić się na Łucji i Mausach. Po działaniach Daniela i Pauliny - musiał wyzerować im konto. Wielki przegrany tej sprawy.
* mag: Melodia Diakon, zaczęła od polowania na kralotha ale skończyła jako Bicz Na Tomasza Myszeczkę z prośby Pauliny i Daniela.

# Plany

* Paulina Tarczyńska: Zacząć zbierać rzeczy na Roberta Sądecznego; wykorzystać wiedzę Katii Grajek.
* Paulina Tarczyńska: Przekazać Eliksir Aerinus Hektorowi Reszniaczkowi
* Paulina Tarczyńska: Być w "Radzie Regionu" proponowanej przez Daniela Akwitańskiego (by region działał prawidłowo i spójnie)
* Daniel Akwitański: Uniezależnienie Magitrowni od wpływu jakiejkolwiek organizacji ever
* Daniel Akwitański: Zorganizowanie czegoś w stylu "stałej rady regionu" - by region działał prawidłowo i spójnie. Być w tej radzie.

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, gdzie odbywa się CORAZ WIĘCEJ rozmów...

# Czas

* Opóźnienie: 2
* Dni: 2

# Wątki kampanii

nieistotne

# Przeciwnicy

## XXX
### Opis
### Poziom

* Trwałość: ?
* Poziom: Trudny (7)
* Siły: 
* Słabości: 

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    3. Magimed zostaje u Pauliny jako trwały surowiec. (Kić)
    4. Sądeczny uznał Paulinę za gracza a nie pionek - docenił to czym jest. (Kić)
    5. Paulina dostaje wsparcie od Millennium. (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się Kociebora z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak dla Kociebora. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    4. Konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem. (Raynor)
    5. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo. (Raynor)
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
    4. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    5. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    6. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
    3. Kajetan odnajdzie niebezpieczne elementy Harvestera przed Dukatem (Kić)
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    4. Lewiatan się zbliża. Jest już bardzo blisko.
1. Klątwożyty dezinhibicyjne
    1. Klątwożyty dezinhibicyjne trwale dostosowały się do okolicy. Bardzo trudno będzie je wyplenić. (Żółw)
    2. Farnolis zaraził obszar dawnego Pentacyklu klątwożytem dezinihibicyjnym. (Żółw)
    3. Klątwożyty dezinhibicyjne trwale Skaziły Biały Zameczek. (Żółw)
    4. Jeden typ klątwożyta. Mutujący, ale jeden. W ten sposób da się z tym poradzić - i to zrobił ich wspólny Paradoks (Kić)
    5. Dalia została wyleczona z klątwożytów i już ich nie złapie (Kić)
1. Odbudowa Farnolisa
    1. Farnolis zaczął przejmować władzę na obszarze dawnego Pentacyklu; odbudowując pod areną obszar wpływu. (Żółw)
1. Tamara x Sądeczny?
    1. Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra. (Kić)
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.
    4. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    5. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    6. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
    4. Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?: NIE WIEMY. (Żółw). 5/10.
    5. Wzór Anety jest trwale uszkodzony - wymaga czegoś nowego (Żółw)
    6. Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
    4. Tomasz Myszeczka pomaga w tematach leczniczych. Poniesie koszty, ale zrekompensuje sobie wiele innych rzeczy. (Kić)
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    4. Dracena uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    5. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni. (Raynor)
    6. Nowa polityka miłości w firmie generalnie rozwiązuje problem z napięciami i nie powoduje publicznych rozterek. (Raynor)
    7. Magitrownia uzyskuje wsparcie zasobowe Eweliny Bankierz. (Raynor)
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
    2. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    3. Portalisko jest silnie połączone z Allitras.
    4. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką
    3. Kaja uważa, że już dość; czas zacząć pomagać i ratować sytuację (Kić)
1. Prokuratura Magów
    1. Całą tą sprawą, tą destabilizacją zainteresowała się nowo powstała prokuratura magów. (Raynor)

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* Sprawdzenie mechaniki 1801 jako całości
* Sprawdzenie kalibracji liczb 1801
* Zrobienie czegoś co się Dzióbkowi spodoba ;-)

## Po czym poznam sukces

* Dzióbek się będzie dobrze bawił
* Liczby dadzą dobry rezultat

## Wynik z perspektywy celu

* Dzióbek się dobrze bawił: sukces
* Liczby dały ciekawy rezultat; było za prosto. 
    * Wszystkie konflikty wygrane przez postacie. 
    * Mitygacja: podnieść bazy konfliktów o 1-1-1-1 dla próby (wariant mechaniki 1806).

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

