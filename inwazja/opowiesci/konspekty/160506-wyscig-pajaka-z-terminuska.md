---
layout: inwazja-konspekt
title:  "Wyścig pająka z terminuską"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}
## Wątki

## Kontynuacja

### Kampanijna

* [160424 - Uważaj, o czym marzysz... (AW, SD)](160424-uwazaj-o-czym-marzysz.html)

### Chronologiczna

* [089 - 160424 - Uważaj, o czym marzysz... (AW, SD)](160424-uwazaj-o-czym-marzysz.html)

## Kontekst misji

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.

## Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.

## Punkt zerowy:

- brak

## Misja właściwa:

Dzień 1: 

Hektor się obudził. Otton nad nim - przekazał mu jasne wytyczne. Ród Blakenbauerów w nadchodzącym chaosie ma nie ucierpieć, ale ma być "good guys". Minimalizować ofiary konieczne i śmiertelne. Doprowadzić do tego, by ród Blakenbauerów był po tej stronie, która wygra. By nikt nie myślał, że to jest problem pochodzący od Blakenbauerów...
To samo dostała jako wytyczne Klara.

Hektor zdecydował się pójść na patrol pacyfistyczny; znaleźć magów, którzy chcą uniknąć Irytki. Klara namierza magów chorych na Irytkę swoją siecią (26), Hektor idzie tam z Marcelinem usunąć problem. Klara skupiła się na punktach które nie są opanowywane. Tu Hektor zaproponował coś sprytnego - jeśli znajdą punkt, który nie ma szczególnych sił (oni nie mają sił na to), trzeba poinformować o tym Millennium.

Klara się nie patyczkuje; przypięła tą sieć do Węzła w Rezydencji. Ona sama nie jest w stanie kontrolować sytuacji. Ale ona sama jest w stanie podpiąć wszystko do Rezydencji, by Rezydencja mogła to kontrolować.

Klara zauważyła też, że magowie ogólnie próbują przechwycić Anioły Nadzorcze; niekoniecznie tylko magowie Świecy. Marcelin poprosił Elizawietę o to, by dała mu prawo do przechwycenia Aniołów. Elizawieta się zgodziła; więcej, kazała przerzucić wszystkie Anioły jakie się da i przekazać je tien Anecie Rainer. Sieć detekcyjna wskazuje położenie Aniołów.

Hektor wysłał swoje siły specjalne (pod dowodzeniem Kleofasa Bora) by zebrały "dzieła sztuki" (Anioły) i przetransportowały je do odpowiedniego miejsca (w prokuraturze). Tam przechwyci je Adrian Murarz (który przy okazji jest magiem Świecy). Jednocześnie sam zajął się tymi siłami, które - będąc magami - próbują zajumać Anioły Srebrnej Świecy. Z pomocą Marcelina, Klara wykryła kilka sił nie będących Świecą próbujących zajumać Anioły. Trzy wyglądają na Millennium, jedna na Zajcewów. Na szczęście, Kopalin jest w kwarantannie.

Hektor zadzwonił do Amandy Diakon. Amanda powiedziała mu, że współpracuje z dowództwem Świecy na uchodźctwie. Kazała mu (tak, kazała) wykorzystać sojusz z Emilią i połączyć ten sojusz z nią; Emilia musi współpracować z Amandą. Jednocześnie Amanda zażądała by Hektor wykorzystał sojusz ze Szlachtą i wydobył cruentus reverto. Hektor powiedział, że nie czas na to; Amanda powiedziała, że cruentus reverto nie może wpaść w ręce defilera. Hektor zrozumiał. Zajmie się tym...

Hektor skontaktował się z Emilią i poprosił ją, by ta połączyła siły z Amandą. Ta po pewnym wahaniu (i po wcześniejszym kontakcie ze strony Andrei) się zgodziła. Powiedziała Hektorowi, że ten musi wciągnąć Dagmarę Wyjątek w pułapkę; Dagmara musi zaatakować dworek Emilii. Albo przynajmniej bazę Anety.

W ten sposób powstał słaby, ale sojusz: Emilia Kołatka, Amanda Diakon, Hektor Blakenbauer. Coś wystarczającego do tego, by jakoś trzymać porządek. Przynajmniej częściowo...

Hektor zaproponował Emilii plan - on może użyć pająka fazowego (o którym wie dzięki większym uprawnieniom) i może porwać cruentus reverto ze Skarbca Kompleksu Centralnego i zabezpieczyć. Nie mogą dać go Emilii, ale mogą go przechwycić i w ten sposób mogą oddać go Amandzie. Andrea dała polecenie oddać cruentus reverto Amandzie (to była umowa: odda teren, cruentus i Milenę). Emilia przyklasnęła - nie wiedziała nawet, że Blakenbauerowie mają pająka.

Jako, że Marian Agrest i Ozydiusz Bankierz współpracowali ze sobą; nie bardzo ściśle, ale w pewnym stopniu sobie ufali, Marian Agrest miał dostęp do kodów Ozydiusza. Dzięki temu można wprowadzić pająka fazowego do Skarbca. Otton dał Hektorowi autoryzację a Klara zaproponowała modyfikację planu - podłożyć ślady wskazujące na to, że to Emilia zajumała cruentus reverto (by ściągnąć Dagmarę w pułapkę).

KASKADA_160504
Pytania kaskadowe:
- czy udało się Klarze zdobyć cruentus reverto?
- czy udało się Dagmarze zdobyć cruentus reverto?
- czy udało się zdobyć cruentus reverto tak, by Dagmara nie wiedziała, że to Klara?
- czy udało się "nie spalić" (nie uszkodzić) pająka do tego stopnia, by mógł polować na Milenę?

Faza 1:
- czy udało się niepostrzeżenie wejść do Skarbca?
- czy udało się dostać do cruentus reverto przed Dagmarą?

Klara: +6 (kody i pająk), +1 (Dionizy i Rezydencja) => 21
Dagmara: +3 (power suit), +3 (czas), +2 (magia), 12 => 18.

Klara inkarnowała w pająku fazowym i spróbowała wniknąć do Skarbca przed Dagmarą (19>18). Zdążyła dostać się do cruentus reverto zanim Dagmara tam dotarła do Skarbca. 

Faza 2:
- czy udało się podłożyć fałszywe ślady tak, by Dagmara połączyła to z Emilią?
- czy udało się ukryć obecność pająka fazowego?

fałszywe ślady:
Mojra: (+3 sugestie Emilii o wiarygodności), (+3 siły specjalne), (+2 magia) + 12 => 20
Wróg: (+3 czas) + 14 => 17 

ukrycie pająka:
Klara: (+3 czas), (+1 Dionizy) + 14 => 17
Dagmara: (+6 power suit) + 10 => 16

Pająk wykryty, ślady fałszywe podłożone.
Klara weszła i dostała się do cruentus reverto; tymczasem drzwi Skarbca się zaczynają otwierać. Pająk próbuje ukraść cruentus i rozpaczliwie wycofać się, Dagmara zorientowała się w sytuacji...

Faza 3:
- czy udało się Klarze zdobyć cruentus reverto?
- czy pająk przetrwał bez poważnej regeneracji?

atak Dagmary:
Klara: - (+6 zaskoczenie i teren), (+6 faza i biowspomaganie), + 10 => 22
Dagmara: - (+9 power suit i broń) + 12 => 21

Dzięki biowspomaganiu Margaret Klara była wystarczająco szybka by móc ewakuować cruentus reverto nie raniąc pająka fazowego; Dagmara wie z czym mają do czynienia, ale nie zdążyła, zwyczajnie była za wolna. Blakenbauerowie ukradli cruentus reverto z rąk Szlachty i Dagmary Wyjątek. A wszystkie ślady wskazują na Emilię.

END_KASKADA

Klara poszła chrapusiać. Znaczy spać.

Z Hektorem skontaktował się Wiktor Sowiński. Powiedział, że Elea jest blisko przełomu z Irytką (współpracuje blisko z Edwinem). On nie ufa siłom w Kompleksie Centralnym. Zażądał też pomocy militarnej; Hektor powiedział, że jedyne co może zrobić to wiązać siły Amandy i Emilii tak, by one pilnowały spokoju w Kopalinie. Nie jest w stanie zrobić nic więcej. Wiktor się wściekł, ale nic nie można poradzić...
Wiktor nie chce, by Kopalin spłonął.

Andrea absolutnie zakazała produkcji czegokolwiek czego zażąda Wiktor. 

Dzień 2:

Dzięki sieci Klary udało się zlokalizować wszystkie Anioły. Cztery nie zostały odzyskane - i Hektor zna ich lokalizacje. Trzy to Millennium, jeden to Zajcewowie. Hektor powiedział Amandzie, że jest zainteresowany odzyskaniem swoich Aniołów. Amanda powiedziała, że jeden z tych Aniołów jakie przypisuje się Millennium nie należy do Millennium. To nie jej siły. To nie siły Millennium. To jeszcze jedna strona. 

Hektor się nie ucieszył. Zaznaczył Emilii, że Sowiński uważa, że ma kontrolę nad mechanicznymi terminusami. Podobno ma kontrolę, bo pełni rolę lorda terminusa więc ma takie uprawnienia. Być może mechaniczni terminusi są Spustoszeni, ale jeśli tak... to w tym momencie Wiktor ma bardzo duże siły w Kopalinie.
Emilia natychmiast update'owała Andreę.

Andrea postanowiła NIE ogłaszać Laetitii Caesar jako głównodowodzącą Kopalina.

Krótka rozmowa Emilii i Andrei - uznały, że najprawdopodobniej Dagmara Wyjątek jest Regentem Spustoszenia na Kopalin. Ale to oznacza, że Spustoszenie jest czymś jeszcze większym niż im się wydawało; należy jak najszybciej ostrzec Agresta. On musi dojść do tego o co i jak chodzi. Bo coś im cały czas umyka.

Anioły zostają przetransportowane nie do Anety Rainer a do Emilii Kołatki. Tam są bezpieczniejsze; Aneta utrzymuje kompleks przekaźników hipernetowych.

Hektor z siłami skupia się na pacyfikacji magów którzy nie chcą poddać się Irytce. Dostał telefon od Kleofasa Bora podczas akcji. Wanda Ketran jest w wiadomościach - podobno Hektor Wandę porwał. Jest nakaz aresztowania Hektora. Hektor poprosił Margaret by ta zmieniła mu twarz, po czym skontakował się z Arkadiuszem Klusińskim (prokuratorem generalnym i magiem) by rozwiązać temat.

Arkadiusz Klusiński powiedział, że Hektor skrewił. Wanda Ketran - człowiek - złamała Maskaradę. Udziela wywiadów. A Świeca nie ma sił by się tym teraz zająć. Wniosek - Hektor spieprzył, Hektor musi naprawić...

# Streszczenie

Zapanował chaos. Otton chce, by Blakenbauer byli "tymi dobrymi". Klara i Hektor współpracują z siłami Świecy pod kontrolą Elizawiety Zajcew i Anety Rainer opanowując chorych na Irytkę i ograniczając straty jednostek Świecy. Amanda Diakon zażądała współpracy z Emilią i tego, by Hektor odzyskał cruentus reverto. Klara i Hektor użyli pająka fazowego używając kodów Agresta i zwinęli cruentus reverto prosto spod szponów Dagmary Wyjątek. Ślady pająka wskazują na Emilię. Powoli sytuacja się ustawia politycznie jako Millennium # lojaliści Świecy # wszyscy vs Szlachta / Regentka Spustoszenia. Andrea dowiedziała się, że najpewniej mechaniczni terminusi są Spustoszeni. Więc Kopalin jest stracony.

# Zasługi

* mag: Hektor Blakenbauer, ustawiający politycznie współpracę z głównymi siłami w Kopalinie i używający pająka fazowego by porwać Dagmarze cruentus reverto.
* mag: Klara Blakenbauer, koordynująca sieci informacyjne w Kopalinie i inkarnująca pająka fazowego. Porwała Dagmarze / Spustoszeniu sprzed nosa cruentus reverto.
* mag: Otton Blakenbauer, który poszerzył uprawnienia Hektorowi i Klarze. Chce pokoju w Kopalinie i chce, by Blakenbauerowie byli dla odmiany tymi dobrymi.
* mag: Marcelin Blakenbauer, działający z poszerzonych uprawnień Świecy. Dowodzi siłami płaszczek rozbijających ogniska Irytkowego oporu.
* mag: Margaret Blakenbauer, dostarczyła Klarze stymulant do pracy z pająkiem fazowym oraz kieruje płaszczkami do stabilizacji Kopalina.
* czł: Kleofas Bór, w ramach sił specjalnych zbiera Anioły ("obiekty sztuki") z ulicy i je składuje. Ostrzegł Hektora o oskarżeniach Wandy Ketran.
* mag: Elizawieta Zajcew, dała Marcelinowi uprawnienia do przechwycenia Aniołów i kazała je składować u Anety Rainer. Ratuje co się da.
* mag: Aneta Rainer, próbująca utrzymać sieć hipernetową i przekaźniki by zachować jakąkolwiek kohezję Srebrnej Świecy na tym terenie.
* mag: Amanda Diakon, dowodzi stabilizacją Kopalina z ramienia Millennium i współpracuje z Hektorem i Emilią. Odzyskała cruentus reverto.
* mag: Emilia Kołatka, dowodzi stabilizacją Kopalina z ramienia Świecy i współpracuje z Amandą i Hektorem. Chce wciągnąć Dagmarę w pułapkę.
* mag: Adrian Murarz, w imieniu Świecy zbierający (dla Blakenbauerów) nieaktywne Anioły Nadzorcze i oddający je Emilii na przechowanie.
* mag: Dagmara Wyjątek, potencjalna Regent Spustoszenia na Kopalin i osoba, którą trzeba by złapać w pułapkę... chciała zdobyć cruentus reverto, ale nie zdążyła.
* mag: Marian Agrest, który był przygotowany na katastrofalną sytuację i przekazał kody do Skarbca Świecy Hektorowi by porwać cruentus reverto. Stoi za Emilią.
* mag: Wiktor Sowiński, próbujący powołać się sojusz zbrojny z Hektorem i który został spławiony.
* mag: Elea Maus, bliska przełomu w pracy nad Irytką Sprzężoną.
* czł: Wanda Ketran, którą przecież Tymotheus usunął... a jednak wróciła, oskarżać Hektora w najgorszym możliwym momencie. Poza Kopalinem.
* mag: Arkadiusz Klusiński, kazał Hektorowi rozwiązać problem Wandy Ketran (która jest bliska złamania Maskarady).

# Lokalizacje

1. Świat
     1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Skarbiec Kopalina
                    1. Dzielnica Owadów
                        1. Blok z żelbetu z przekaźnikiem energetycznym SŚ
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Osada Strażnicza
                        1. Dworek Emilii Kołatki

# Czas

* Dni: 2