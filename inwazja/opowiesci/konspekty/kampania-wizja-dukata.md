---
layout: default
categories: inwazja, campaign
title: "Wizja Dukata"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Prawdziwa natura Draceny](kampania-prawdziwa-natura-draceny.html)

## Opis

-

# Historia

1. [Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html): 10/12/15 - 10/12/17
(171205-dlaczego-magitrownia-dziala)

Robert Sądeczny niedawno pojawił się na tym terenie z ramienia Dukata. Magitrownia Histogram jest nieco niestabilna i by wzmocnić Portalisko Pustulskie musi być usprawniona. Wysłał zatem Dobrocienia i Natalię Kazuń by się tym zajęli. Oni zamiast usunąć lokalnego dziwaka, Karola Marzyciela, zbadali magitrownię - okazało się, że ona "żyje" i będzie działać sama z Marzycielem jako swoim awatarem.

1. [Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html): 11/08/25 - 11/08/27
(171001-powrot-do-domu)

Paulina wróciła do Krukowa, miejsca, gdzie kiedyś była rezydentką z Marią (zarejestrowaną) i Draceną (też zarejestrowaną). Ku swemu zdziwieniu zobaczyła wielką wojnę dezinformacyjną z kierunku Dukata w aktualnego rezydenta. Rozpoznała trochę sytuację i nawiązała kontakt z Kają, która z braku Pauliny monitorowała z jej domu efemerydę. Sytuacja się zmieniła - Dukat stał się główną siłą w okolicy, Ewelina się wycofała, Apoloniusz ucierpiał, otwarto portalisko i do tego Paulina wykryła coś... niepokojącego, stary sygnał w efemerydzie, czyjeś wołanie o pomoc... aha, aktualny rezydent odbił od Dukata jedną maszynę medyczną. Super.

1. [Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html): 11/08/28 - 11/08/30
(171003-swinia-na-portalisku)

Po akcjach rezydenta przeciw Dukatowi kurier Dobrocień został przemęczony i przeciążony - w wyniku podłapał glukszwajna i próbował przewieźć go przez portalisko. Ledwo przeżył. Wypadek mógł zniszczyć portalisko. Weterynarz i "mag od wszystkiego" w portalisku poszli szukać glukszwajna i winnego. Znaleźli fragment zapasowych systemów Harvestera i znaleźli glukszwajna. Znaleźli też dowody świadczące o niedbałości Myszeczki a dokładniej zabezpieczeń jego hodowli. W wyniku - Myszeczka musiał zapłacić sporą cenę a portalisko nadal przez pewien czas jest nieczynne...

1. [Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html): 11/08/28 - 11/08/31
(171218-senesgradzka-kopia-pauliny)

W Senesgradzie pojawiła się "druga Paulina". Okazało się, że to manifestacja Efemerydy Senesgradzkiej powstała na skutek rozpaczy starszej lekarki umierającej na raka. Paulina dowiedziała się mniej więcej jak funkcjonuje Efemeryda Senesgradzka i udało jej się zażegnać problem. Przy okazji Maria znalazła "dom" w Senesgradzie a Kaja zaczęła pracę nad destabilizacją manifestacji Efemerydy Senesgradzkiej.

1. [Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html): 11/09/01 - 11/09/03
(171004-niestabilna-magitrownia)

Regenerowane portalisko dostaje Skażoną energię z magitrowni Histogram; Sądeczny zadzwonił do Daniela, by ten (właściciel) to naprawił. Okazało się, że artezyjskie źródła są Skażone, pojawił się dopływ Skażenia. Do akcji wkroczyła też Paulina; jeden z pracowników Daniela został Skażony tą energią i Paulina wyczuła energię Harvestera. Co się okazało - usunięcie niedawno elementu Harvestera przez Natalię i Kociebora odblokowało Skażony dopływ. Daniel wprowadził tam glukszwajny Myszeczki i załatwił, że ten ma tam pastwisko. Potem zatrudnił Dracenę i pomógł Paulinie dowiedzieć się, że ten sygnał SOS w efemerydzie to była Katia i maskował to Sądeczny.

1. [Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html): 11/09/06 - 11/09/09
(171010-jasny-sygnal-tamary)

Tamara zdecydowała się uderzyć w sensory Sądecznego i Skaziła mu wszystkie sensory korzystając z rekonesansu w magitrowni Histogram. W wyniku tego doszło do zamanifestowania Oktawii Aurinus z efemerydy. Pilot i konstruktor awianów, Krzysztof wsparty przez Oliwię Aurinus wygasili Oktawię i samą efemerydę. Tymczasem Daniel przekonał Sądecznego, że działania Tamary to nie jego wina i pojawi się glukszwajn wyżerający Skażoną energię, by móc znowu montować czujniki. A Paulina spotkała się z Kajetanem i nadała mu temat Katii Grajek.

1. [Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html): 11/09/10 - 11/09/11
(171015-powstrzymana-wojna-domowa)

Maria odkryła dla Pauliny coś, czego Sądeczny nie zamaskował - Aneta Rukolas i Fryderyk Grzybb weszli z Sądecznym kiedyś do Muzeum Pary i coś się tam stało. Tymczasem Wiaczesław na prośbę Sądecznego zaczął masakrować Legion i robić z niego pośmiewisko. Paulina skontaktowała się z Sylwestrem Bankierzem i z Gabrielem Dukatem; zapewniła, że nie będzie otwartej wojny. Tamara próbowała wpłynąć na Paulinę - niech wie, że mafia jest ZŁA. Paulina jest jednak pragmatyczna.

1. [Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html): 11/09/12 - 11/09/14
(171022-tylko-nieswiadomy-elemental)

Do Pauliny przybył bardzo ranny człowiek. Z trudem Paulina go postawiła; sprawa stała się w Wieży Wichrów. Tam Paulina spotkała starego znajomego, Filipa, pracującego nad elementalami do awianów i nie tylko. Jeden elemental wyrwał się na wolność. Paulina ma coraz większe podejrzenia, że te elementale nie są elementalami a czymś innym; fakt, że ten wyrwany próbował otworzyć portal gdzieś zanim Kajetan go nie zniszczył jest jedynie silną przesłanką. Filip ma problemy z Sądecznym, bo jego biznes jest niebezpieczny dla otoczenia...

1. [Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html): 11/09/16 - 11/09/18
(171024-detektyw-lecz-nie-sadecznego)

Dziwna magiczna burza nad farmą krystalitów; awian 12-letniej Mai się zbuntował i ją porwał wraz z wujkiem. Zawołany przez Sądecznego Krzysztof wyłączył awiana z akcji a Paulina odkryła infekcję "dziwnym elementalem". Tymczasem detektyw Kociebor z nadania Sądecznego znalazł źródło problemów w Wieży Wichrów. Okazało się, że Filip Keramiusz przyzywał coś innego, nie konstruował elementali burz... Wieża została w rękach Filipa a Zespół ma detektory tych dziwnych elementali, które nie są elementalami.

1. [W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html): 11/09/20 - 11/09/22
(171029-w-co-gra-sadeczny)

Dostawszy cynk od Marii, Paulina dowiedziała się o tym, że Sądeczny próbuje zbierać Harvestera. Najpewniej by pomóc synowi Dukata. Po skontaktowaniu się z Kają, dowiedziała się też o planach stworzenia adaptującego się Eliksiru Miłości do zniewolenia Eweliny lub Newerji. Ale skąd Sądeczny ma wiedzę? Na to odpowiedź znalazła u Oktawii, w magitrowni - Sądeczny wyciągnął Fornalisa z Efemerydy, zostawiając tam Grzybba. Przy okazji, Paulina pomogła rozproszyć Oktawię i wyraźnie zestresował ją fakt, że "dziewczynka" była przerażona i nie chciała odejść.

1. [Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html): 11/09/24 - 11/09/27
(171031-utracona-kontrola)

Daniel zniwelował ryzyko wynikające z małej magitrowni Tamary (składającej mały kompleks centralny) i dał Świecy tanią energię za zgodą Sądecznego. Gdy przyszedł do niego Myszeczka bardzo zmartwiony tym, że musi czyścić po drugiej stronie portaliska efemerydy świńskie (a nie ma jeszcze czym), Daniel poszedł za jego prośbą i puścił energię innego koloru. Chwilę potem portalisko eksplodowało magicznie, Sądeczny i Tamara ratowali magów od strony eteru a Paulina przejęła dowodzenie medyczne. Przechwyciła Anetę Rukolas. Winny? Kolektor harvestera (podłożony przez Kaję) zarezonował z energią Myszeczki. Mamy sprzężone portalisko z Allitras i mamy świńskie efemerydy ogromnej mocy i złamanie Maskarady na kilku poziomach... a Świeca i Mafia utraciły kontrolę nad terenem i sytuacją.

1. [Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html): 11/09/28 - 11/09/30
(171101-magimedy-przed-epidemia)

Paulina dokalibrowała magimed Sądecznego i wsparła go w tym, by pozostał rezydentem mimo gniewu Dukata że to się wszystko sypie. Potem poprosiła Dukata o magimed dla Tamary. Następnie z Dalią skupiła się nad Anetą Rukolas - Dalia się zaraziła klątwożytem. Wiele wskazuje, że lokalny Eter jest bardzo niebezpiecznym i Skażonym miejscem... więc by zatrzymać potencjalną nadchodzącą epidemię Paulina przesunęła bazę do hodowli Myszeczki. Tam są viciniusy pomagające w leczeniu.

1. [Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html): 11/10/01 - 11/10/02
(171105-epidemia-dezinhibicji)

Dracena zorganizowała sobie pokój miłości w magitrowni. Słyszy głos Oktawii w nocy, więc Daniel ściągnął Oliwię zarażoną klątwożytem. Dracena osłoniła magitrownię. Paulina wprowadziła kwarantannę i centrum dowodzenia na farmie Myszeczki - więc zaatakowały farmę awiany... Potem siły Wiaczesława i siły Myszeczki się poprztykały, Daniel osłonił farmę Myszeczki i ogólnie doszło do eskalacji epidemii a Świeca musiała się ewakuować przez glukszwajny i klątwożyty.

1. [Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html): 11/10/03 - 11/10/04
(171115-kryzysowo-tymczasowy-dyktator)

Klątwożyty dezinhibicyjne zarażają coraz szerszą populację magów; szczęśliwie Paulina ma szczepionkę w magitrowni. Po ściągnięciu wszystkich trzech przywódców - Eweliny Bankierz, Sylwestra Bankierza i Roberta Sądecznego, Paulina została na 2 tygodnie dyktatorem dowodzącym wszystkimi siłami (robota Daniela). Kaja uderzyła w Apoloniusza i znalazła dowody jego złych czynów. Wiaczesław torturował tajemniczą pilot awiana - Łucję Maus - i pozwolił Paulinie ją odbić dla jej chwały. Sylwester Bankierz zdobył glashundy jako wsparcie.

1. [Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html): 11/10/05 - 11/10/06
(171220-potrojna-magitrownia-histogram)

W wyniku awarii dekontaminatora w magitrowni Histogram Paulina i Daniel znaleźli się w splocie kilku faz - Primusa, Daemonica, Allitras, Esuriit... i wszędzie istniała ta sama magitrownia Histogram. Spotkali tam się z efemerydą senesgradzką i jej manifestacją - Integrą Weiner, pierwszą twórczynią magitrowni Histogram. Udało im się przekonać Integrę do zaprzestania stabilizacji energii magią krwi, nie udało im się zapobiec stabilizującej się tam wylęgarni świń i wrócili bezpiecznie do domu, na Primus.

1. [Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html): 11/10/07 - 11/10/09
(171229-esuriit-w-poldarze)

Po czarze Pauliny transformującym Daniela w częściowo nieumarłego, Efemeryda Senesgradzka zdecydowała się doprowadzić do przesunięcia człowieka w Fazę Esuriit. I faktycznie, powstał Wyssaniec Esuriit. Dzięki działaniom lokalnych ludzi (Augusta, Żakliny i Jagody), lekko wzmocnionych przez Efemerydę, Wyssaniec został zabity zanim do czegoś strasznego doszło. Półdara została ocalona i powstał Słoik nasycony energią Esuriit.

1. [Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html): 11/10/12 - 11/10/15
(180104-wspanialy-wieprz-wojtka)

Po usunięciu Wyssańca Esuriit Kajetan poprosił kogoś o monitorowanie terenu. Dwoje uczniów terminusów skupiło się na złapaniu i usunięciu oczkodzika z powodzeniem; dodatkowo, doszli do tego, że są tam dziwne walki norek. Nie zatrzymali ich, ale zdecydowali się znaleźć odpowiedzialnego maga.

1. [Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html): 11/10/10 - 11/10/12
(180110-odzyskana-wladza-pauliny)

Daniel dał radę ustabilizować energię magitrowni ograniczając dostęp mniej krytycznych firm od stałych dostaw energii. Tomasz Myszeczka opanował kryzys dzikich glukszwajnów dążących do wdeptania Portaliska racicami. Paulina dowiedziała się, że na terenie jest inny kraloth i uratowała Łucję Maus. Łucja została przekonana przez Daniela do współpracy i wszyscy współnymi siłami wykonali plan Warinsky'ego - usunęli zagrożenie klątwożytowe z okolicy. A Hektor Reszniaczek został tymczasowym CEO Eliksiru Aerinus. Niestety, wylęgania rośnie w mocy...

1. [Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html): 11/10/16 - 11/10/18
(180112-chrumpokalipsa)

Zespół kandydatów na terminusów odkrył, że pod Waleczniskiem jest hodowla czarnotrufli Eliksiru Aerinus, Skażona przez Esuriit. Przechwycili czarodziejkę za to odpowiedzialną (Stefanię) i odkryli jej powiązania z kralothem w okolicy. Dodatkowo weszli w bój z niegdyś rolniczym Golemem Esuriit, doprowadzając do ogromnego złamania Maskarady i stworzenia Wieprzopomnika. Przy tym wszystkim konsekwencje nie spadną na nich ;-).

1. [Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html): 11/10/15 - 11/10/17
(180214-nie-podlozona-swinia-lucji)

Mausowie nie zaprzestali działań mających na celu wydanie Łucji by zadowolić Tomasza Myszeczkę. Połączone siły Daniela i Pauliny sprawiły, że Łucja się uspokoiła i poszła pomagać Bolesławowi na portalisku, Tomasz, Bolesław i Łucja wyzerowali sobie konto a Mausowie zostawili Łucję w spokoju. Kraloth na którego Melodia polowała zniknął jej z radaru. Paulinie udało się postawić na nogi Katię Grajek a do Myszeczki przyszedł Ważny Klient, więc ów nie ma czasu przeszkadzać...

1. [Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html): 11/10/27 - 11/10/29
(180503-dlaczego-kret-w-jeziorze)

Kinga i Anatol mają dość raportów; dostali więc ciężką misję polityczną - patrol terenu Karmeny Bankierz. Młoda poimprezowała i okazało się, że wraca i będzie chciała zwalić winę na Świecę, co bardzo nie pasuje Sylwestrowi Bankierzowi. Na miejscu - mimo zapewnienia eks-terminusa Karmeny (z osłabioną mocą), że niewiele dzieje się w samym Małoząbiu - ktoś drenuje energię z ludzi i wpakowuje ją w jezioro. Gdy Zespół doszedł do tego, że na dnie jest Kret IV (zaginiony stary driller), odkopali go i ewakuowali. AI Kreta IV to Yyizdathspawn. Wszystko wskazuje, że ktoś tu coś więcej wiedział...

## Progresja

|Misja|Progresja|
|------|------|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html) to on zainstalował Daniela Akwitańskiego w magitrowni po tych dniach - ale nie powiedział mu o Karolu Marzycielu.|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html) przez siły Gabriela Dukata i Roberta Sądecznego jest traktowany jako nieusuwalny "mebel" w magitrowni.|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) kupiła dodatkowe akcje Eliksiru Aerinus od Apoloniusza; zwróciły się po czasie i były dobrą inwestycją|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html) jest "podległym partnerem" dla Roberta Sądecznego. Nie jest tylko jakimś podległym randomem.|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) całą akcję z magitrownią aż do przejęcia przypisał skutecznie sobie i tylko sobie.|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) całkowicie uniezależniła się od sił Sądecznego i sił Dukata. Wykupiła się.|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html) Karol Marzyciel akceptuje go jako zacnego maga. Nie "ufa" mu, ale "akceptuje" i czasem pomoże.|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) zdobyła pracę w Nowej Melodii jako DJ i marketingowiec|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) zatrudniła sobie Henryka Mordżyna jako pielęgniarza od zwierząt. On wie jak wyglądają działania Myszeczki.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html) zakłada grupę polującą na znikające świnie; próbuje zrozumieć co tu się dzieje i zbierać informacje z terenu.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html) przeciwko niemu montują się nastroje antyMausowe i antyKarradraelowe|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) pozyskała aktywną i sprawną część Harvestera. Nie jest niebezpieczna, acz jest częścią Harvestera.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html) od Roberta Sądecznego dostał agencję detektywistyczną wraz z mandatem do detektywowania - za zasługi i ratunek Dobrocienia.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html) gościu od znikających świń ma przydupasa, którego misją jest pomóc mu udowodnić to wszystko (Prezes).|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) dostała własnego tresowanego glukszwajna z apetytem na magię Harvestera.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) Robert Sądeczny wsadził mu rezydenta politycznego - kogoś, kto ma pilnować, by nie działy się nadużycia z glukszwajnami.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) świetna opinia doskonałej profesjonalistki, zwłaszcza w Senesgradzie. W świecie ludzi. Pomocna ludziom w sytuacjach beznadziejnych.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) zostawiła i złożyła w Senesgradzie czujniki, by pomóc Paulinie pomagać randomowym ludziom Skażonym przez Efemerydę|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) zobowiązała się przed Efemerydą, że będzie chronić i pomagać ludziom Skażonym przez magię w Senesgradzie|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html) w jakiejś formie istnieje jako byt wydzielony z Efemerydy; nadal chroni i pomaga schronisku dla nastolatków|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) pracuje nad tym (z powodzeniem) by móc oddzielać byty sprzężone z Efemerydą Senesgradzką.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) otrzymała od Kai Maślaczek w Senesgradzie czujniki, by widzieć randomowych ludzi Skażonych przez Efemerydę|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) jest ogromna i łączy się z bardzo szeroką ilości leylinów i energii przez różne fazy|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) gdziekolwiek sięga zasięg efemerydy, tam jest zwiększona motywacja do pomagania ludziom przez wpływ Janiny.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) "jest w domu". W Senesgradzie znalazła zatrudnienie w gazecie "Dobro Ludzi" i znalazła tam miejsce dla siebie w mieszkaniu Janiny.|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) za szybkie i skuteczne działanie w klubie Panienka w Koralach została doceniona przez mafię i Świecę|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html) za skuteczne usunięcie elementala na portalisku tym co miał (i dowodzenie) został doceniony przez mafię i Świecę|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) dostaje awiana starego typu jako karetkę; zabezpieczony dla niej przez Roberta Sądecznego|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) występuje na wielu Fazach jednocześnie i ma dopływy energii z różnych Faz.|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) cały czas słyszy Karola Marzyciela w magitrowni Histogram.|
|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) ranny; przez mniej więcej tydzień nie może czarować ani działać aktywnie. W niełasce u Dukata.|
|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html) ranna; przez mniej więcej tydzień nie może czarować ani działać aktywnie. Straciła większość Legionu.|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html) wyleczona z klątwożyta i disinhibition plague; niewrażliwa na nie (Kić)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html) odbudował pod terenem dawnego Pentacyklu... coś. I zaraził to miejsce klątwożytem dezinhibicyjnym|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) dostaje wsparcie od Millennium. (Kić)|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) została dyktatorem na dwa tygodnie z ramienia Sylwestra Bankierza, Eweliny Bankierz i Roberta Sądecznego|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) zdobyła haka na Apoloniusza Bankierza. Ma na jego sprawki liczne dowody.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html) przeciwko niemu znaleziono dowody współpracy z mafią - i to takie "nieetyczne".|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html) dowodzi uzbrojonym helikopterem bojowym "Princessa Jeden" wraz z 6 żołnierzami.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html) dowodzi grupą ~8 glashundów na tym terenie przez czas nieokreślony. Zapewniają hipernet.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html) w ciągłym kontakcie z Karolem Marzycielem. Kochankowie przez telefon.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html) w ciągłym kontakcie z Integrą Weiner (echem efemerycznym). Kochankowie przez telefon.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html) posiada możliwość manifestacji osobiście w Magitrowni Histogram na Primusie (przez zbliżenie faz).|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html) nie będzie polować na ludzi. Nie chce celować w ludzi. Uwolniła wszystkich ludzi w multifazie Histogram.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html) ma "linię supportową" do Integry Weiner przez Karola Marzyciela. Jak coś się spieprzy, to może z Integrą próbować rozwiązać.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html) aktywnie wspiera obecność Daniela Akwitańskiego na Primusie.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Istnieje możliwość powrotu do miejsca splotu tych wszystkich faz. Ona będzie w stanie docelowo tam wrócić.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|Senesgradzki Chaos zmutowany oczkodzik na wolności nadal poszukuje... czegoś. Jest wolny i unika terminusów bez problemu.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[Wojtek Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojtek-piekarz.html) nie miał rodziny, został adoptowany przez Augusta Paszkwila i został jego padawanem w ufologii i racjonalizacji.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|Senesgradzki Chaos ma dostęp do Słoika Esuriit, o którym nadal nie wiemy co robi|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) dostrzegła artykuł napisany przez Jagodę. Nie tylko zauważyła kompetentną (acz sfrustrowaną) reporterkę ale i potencjalny kontakt i znawczynię paranormalnego (sceptyczkę).|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|Łowcy Dziwnych Świń dostali jako wsparcie harcerzy w okolicy Półdary; harcerze są całkiem przekonani, że dzieje się tu coś bardzo dziwnego.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html) Wojtek Paszkwil nie miał rodziny, więc August go adoptował i zmienił go w swego padawana w ufologii i racjonalizacji.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html) uzyskała pulę czarnotrufli z Eliksiru Aerinus, na leki dla siebie; Paulina ją jakoś dodatkowo ustabilizowała.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|Senesgradzki Chaos traci dostęp do mutujących klątwożytów i wolno wędrujących świń w Powiecie Pustulskim|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) sława i chwała; duży sukces przy stabilizacji terenu: magitrownia działa, świnie pod kontrolą, nie ma klątwożytów.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|Senesgradzki Chaos pojawia się straszliwa świnia dużej mocy w okolicy, po działaniach w magitrowni i wyłączeniu klątwożytów|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) odzyskał kontrolę nad swoimi świniami i ogólnie odbudował swoje zasoby.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html) jego plan okazał się perfekcyjny. Mistrz Skażenia Szlachty nadal w formie.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|Senesgradzki Chaos Wylęgarnia świń zdecydowanie wzrosła w mocy przez działania klątwożytów|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) wystarczająco zabezpieczyła siebie i Eliksir Aerinus u Sądecznego odnośnie czarnotrufli.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) uzyskuje niewielką próbkę czarnotrufli Esuriit. Nie do końca jeszcze wie co to.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html) uzyskuje 'świnie affinity'. Świnie do niego ciągną, on dobrze z nimi żyje, one go lubią, jemu łatwiej je zrozumieć.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Łowcy Dziwnych Świń uzyskali współpracę z kilkoma grupami mazowieckich harcerzy. Frakcja rośnie w ludzi.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Senesgradzki Chaos utracił lekko zmutowanego oczkodzika w okolicach Półdary|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Senesgradzki Chaos pozyskał Golema Ogrodniczego Czarnotrufli, napędzanego energią Esuriit.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Łowcy Dziwnych Świń mają bardzo silne przesłanki, że coś paranormalnego się dzieje i ze świniami też|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) Sylwester Bankierz dał Kindze pozytywną opinię jako niezależnej agentce Świecy|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) Stefania Kołek przekazuje szerzej wieści o efektownym wejściu Anatola. Anatol dostaje opinię lepszego niż jest.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html) Nie zauważa norek. Nie zapamiętuje norek. Norki dla niego nie istnieją. Nic związanego z norkami. Luka poznawcza wywołana Paradoksem.|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) zyskuje sojusznika w Bolesławie Mausie (właścicielu Portaliska Pustulskiego)|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html) uważa Paulinę Tarczyńską za osobę sprzyjającą pozytywnym Mausom; ogólnie jej sprzyja|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Hlangoglormo](/rpg/inwazja/opowiesci/karty-postaci/9999-hlangoglormo.html) skutecznie dał radę się schować przed oczami Laragnarhaga i Melodii Diakon na krótki czas|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html) póki Melodia Diakon operuje na Mazowszu, pomoże Danielow jak on poprosi|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) pojawia się potencjalnie bardzo wartościowy klient potrzebujący pełni uwagi Myszeczki|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html) oczarowała Łucję Maus; ma na nią ogromny wpływ|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html) następnego dnia już nadaje się do pełni działania|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Hlangoglormo](/rpg/inwazja/opowiesci/karty-postaci/9999-hlangoglormo.html) ma potężny kult wraz z kultornicą (lub kilkoma?), które go chronią|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html) jest pod ogromnym wpływem Melodii Diakon - uważa Melodię za potencjalnego guru|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html) do budowania swojego lekarstwa wykorzystuje czarnotrufle Esuriit a nie zwykłe czarnotrufle|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html) w oczach Łowców Dziwnych Świń jest bohaterem i męczennikiem|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html) uzyskał umiejętność Rozkazywania świniom. Atak mentalny.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Hlangoglormo](/rpg/inwazja/opowiesci/karty-postaci/9999-hlangoglormo.html) uznawany za 'tame medical kraloth' przez Rodzinę Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Senesgradzki Chaos utracił siły Esuriit w Półdarze.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) usłyszała o nim Ewa Zajcew z uwagi na Wieprzopomnik w Półdarze.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html) usłyszała o Anatolu Sowińskim z uwagi na Wieprzopomnik w Półdarze.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) udało jej się uniknąć obwinienia za Wieprzopomnik w Półdarze; to Świeca Z Nie Tego Terenu.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) tymczasowo Dotknięty przez Mrok Esuriit. Wrażliwy na tą formę energii.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) tymczasowo Dotknięta przez Mrok Esuriit. Wrażliwa na tą formę energii.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Łowcy Dziwnych Świń stracili przywódcę, Pawła Kupiernika. Ale on został ich męczennikiem. Kult Kupiernika.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) przetransferowana poza Mazowsze przez siły uczniów Świecy|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) posiada mały odłamek Wieprzopomnika Półdarskiego, sprzężonego z głównym pomnikiem.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) opinia w Świecy: niestandardowe podejście Kingi uratowało Stefanię od niewoli kralotha.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) okazuje się, że była już uzależniona od Hlangoglormo (kralotha)|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html) nie pamięta o wszystkich tych świniokształtnych tematach|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Łowcy Dziwnych Świń coraz ściślejsza współpraca z harcerzami na Mazowszu. |
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|Srebrna Świeca z logów Kreta IV wydobędzie się informacje o tym, kto wmanipulował uszkodzonego Yyizdathspawna w potencjalną katastrofę.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) skutecznie wygenerowała niezadowolenie Newerji wobec Karmeny, co ratuje Ewelinę od "bycia drugą kiepską".|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html) posiada wiedzę z Faz, przez które przechodził Kret IV. Nie pomogło to na jego stan emocjonalny i logiczny ;-).|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|Srebrna Świeca nie chce oddać uszkodzonego Yyizdathspawna Yyizdathowi, ponieważ tam jest informacja o nowej Fazie a Yyizdath ma... historię eksploracji.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) dostęp do Yyizdatha i tematów Yyizdathokształtnych przez Disco-Yyizdathspawna|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Karmena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-karmena-bankierz.html) dorobiła się niezadowolenia ze strony Newerji. Dlaczego na JEJ terenie dzieją się takie rzeczy a ona nie wie?|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html) beznadziejnie zakochany w czteronożnych futrzakach zwanych kotami.|

## Plany

|Misja|Plan|
|-----|-----|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html) ściągnąć Daniela Akwitańskiego do magitrowni Histogram, bo jest tam miejsce dla kogoś takiego jak on|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html) szokowo rozszerzyć biznes / wejść na nowy typ produktu; potrzebuje do tego kapitału|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) przejąć kontrolę nad głównymi kluczowymi elementami otoczenia; potrzebuje do tego wsparcia|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) wróciła na teren powiatu Pustulskiego, by po kolei i stopniowo walczyć o bycie rezydentką.|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html) skupia się bardziej na swoim dziecku niż na Rodzinie Dukata; zdecydowanie zdepriorytetyzował to, co dzieje się z jego mafią.|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html) próbuje uzyskać jakąkolwiek informację o tym co dzieje się w okolicy i przebić się przez zasłonę dezinformacji. Chce być sensownym rezydentem.|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) próbuje się jakoś ustatkować i znaleźć gdzieś pracę. Próbuje się zreintegrować do społeczeństwa, acz w swojej nowej formie Spustoszonej Wiły|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) próbuje odzyskać stare kontakty i odbudować swoje sieci połączeń w powiecie Pustulskim. Chce odzyskać status pulsu okolicy.|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) przestał pilnować domu Pauliny; nie ma już takiej konieczności (Paulina wróciła).|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) konsekwentnie monitoruje efemerydę senesgradzką i upewnia isę, że nikt nie cierpi przez brak aktywnego sensownego rezydenta|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) dezinformuje, utrzymuje przewagę magitechnologiczną, wprowadza chaos i zdobywa pieniądze dla Dukata.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) zdecydował się wzmocnić Srebrną Świecę nad Rodzinę Dukata - Sądeczny wsadził mu rezydenta politycznego :-(.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html) za wszelką cenę chce utrzymać portalisko w swoich rękach. Jest zdolny do wielkich desperacji, by to wszystko jakoś działało.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html) staje po stronie zwierzątek w sytuacji konfliktu zwierząt i ludzi.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html) kiedyś, pracownik portaliska Pustulskiego. Od tej pory ma zamiar zostać detektywem.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) jest zainteresowany stabilizacją terenu i ochroną portaliska Pustulskiego. Pilnuje, by na jego terenie nic złego nikomu się nie działo.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html) gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! (Prezes)|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) chce wykorzystać agencję detektywistyczną Kociebora do zbierania informacji i silniejszej kontroli terenu.|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) celuje w oszczędności i poszerzenie swoich wpływów jak najmniejszym kosztem finansowym.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) Zupełnie nie ufa Kai; chce zbudować własną sieć informatorów w Senesgradzie i upewnić się, że magia będzie pod kontrolą.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) Zdobywać nowe imprinty, nowe istoty by móc wstawiać odpowiednie byty w odpowiednie scenariusze.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) Zafascynowana Efemerydą Senesgradzką. Chce ją zrozumieć lepiej. Bardzo zmartwiona negatywnymi konsekwencjami Efemerydy na Senesgrad. Będzie monitorować.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html) W ramach swojej możliwości, w zakresie dostępu Efemerydy Senesgradzkiej, pomagać ludziom - leczyć, chronić. Zwłaszcza nastolatkom w kłopotach.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Paulina od czasu do czasu poświęca czas by pomagać ludziom w Senesgradzie. Nie tylko dla Janiny, też, bo tak trzeba.|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) Ma w planach zdecydowanie częściej współpracować z Marią. Choćby po to, by Paulina przestała robić głupoty.|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html) uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) uważa, że już dość z epidemiami itp; czas zacząć pomagać i ratować sytuację (Kić)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html) odbudowuje potęgę Bractwa Pary korzystając z nieobecności Sądecznego (Żółw)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html) decyduje się wesprzeć Magitrownię Histogram jako nowa wspierająca siła (Raynor)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html) chce (i przyjdzie) zrobić porządek w magitrowni Histogram. (Raynor)|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html) będzie grał na powrót Pauliny rezydentki. (Kić)|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html) oddał Paulinie dowodzenie i dał jej dwa tygodnie by sprawdzić, czy poradzi sobie lepiej niż Sylwester jako Rezydentka. Obserwuje.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html) jest zadowolony z tego, że Sądeczny współpracował z innymi siłami by opanować epidemię. Nie zamierza niczego zmieniać w dowodzeniu regionem.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html) gra w przesunięcie Pauliny na stanowisko Rezydentki; chwilowo skupia się na zwalczaniu zewnętrznych sił atakujących teren.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html) chce znaleźć degenerata odpowiedzialnego za krzywdę dokonaną Łucji Maus i odzyskać panowanie nad sytuacją korzystając z braku Tamary.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) chce zmaksymalizować zniszczenia na Apoloniuszu Bankierzu i pomóc Ewelinie odzyskać kontrolę nad regionem.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html) chce uwolnić się od łańcuchów Newerji przy użyciu Daniela Akwitańskiego oraz ma okazję uderzyć w Apoloniusza Bankierza. Na pewno to zrobi.|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html) chce pokazać się jako Wartościowy Członek Zespołu. Nieważne komu - ktokolwiek spyta i jest pod ręką.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html) zapewnić prawidłowe działanie magitrowni Histogram niezależnie od pryzmatu czy czegokolwiek. Oraz trochę poMarzycielować.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html) z niewiadomego dla wszystkich powodów rozwija program eksperymentowania na eterycznych świniach. W eterze.|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html) już wiadomo, że da się wyrwać osobę z efemerydy. Chce więc wyrwać Integrę Weiner z efemerydy docelowo.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[Dariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-tluk.html) zainteresował się Żakliną Bąk; chce ją "uratować" od życia jakie ma teraz.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) skontaktować się z Jagodą Kozak albo dowiedzieć się więcej o jej leadach i historiach; tam może być naprawdę sporo ciekawych rzeczy.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|Senesgradzki Chaos nie wiem gdzie, nie wiem czemu i nie wiem kiedy. Ale ten Słoik Esuriit gdzieś kiedyś się pojawi by coś się stało.|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html) nie jest już 'chytrą babą'. Paranormalne istnieje a Dariusz się nią zainteresował; Mariusz nie jest wszystkim co czeka ją w przyszłości.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html) zwalcza Łucję Maus, bo ona jest niebezpieczną, szaloną psychopatką i rozpędziła mu świnie w momencie kryzysu|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html) zwalcza Tomasza Myszeczkę, bo on zrobił krzywdę jej bliskiemu przyjacielowi|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html) ma wsparcie ze strony Łucji Maus. Ona widzi w nim potencjalnego sojusznika a przynajmniej nie-wroga.|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) chce przekazać Eliksir Aerinus Hektorowi Reszniaczkowi; nie chce by Kaja go rozwaliła|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html) będzie współpracować z Danielem Akwitańskim; chce dobrze dla Mausów i tego terenu|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html) Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html) Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) Zbadać czym są te dziwne czarnotrufle (Esuriit). Jakie mają własności. Czym się różnią.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Senesgradzki Chaos Używając Golema Ogrodniczego Czarnotrufli, Skazić jak najwięcej spod ziemi.|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html) Kontynuować biznes budowania czarnotrufli i uniknąć wykrycia; doukryć tą sprawę.|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html) Zorganizowanie czegoś w stylu "stałej rady regionu" - by region działał prawidłowo i spójnie. Być w tej radzie.|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Zacząć zbierać rzeczy na Roberta Sądecznego; wykorzystać wiedzę Katii Grajek.|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html) Uniezależnienie Magitrowni od wpływu jakiejkolwiek organizacji ever|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Przekazać Eliksir Aerinus Hektorowi Reszniaczkowi|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Być w "Radzie Regionu" proponowanej przez Daniela Akwitańskiego (by region działał prawidłowo i spójnie)|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Rodzina Dukata Świeca spoza Mazowsza robi akcje na terenie Mazowsza. To trzeba zatrzymać i rozwalić.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Łowcy Dziwnych Świń schodzą do podziemia, bo rząd współpracuje ze świniami.|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Polityka Półdary rozpoczyna się wojna polityczna między starym burmistrzem a właścicielem Wegecud|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html) przygotować coś by sprawdzić, czy Anatol Sowiński to nie "Kuba Urbanek w przebraniu".|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Rodzina Dukata Ochronić 'tame medical kraloth' przed Srebrną Świecą - bo potencjalnie nasz!|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|YyizdathCorp zwrócił oczy w kierunku na teren pod kontrolą Pauliny z uwagi na potencjał nowej Fazy|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html) zainteresowany NIE wróceniem do Yyizdatha; chce zniknąć i mieć własny dom z dużą ilością kotów|

