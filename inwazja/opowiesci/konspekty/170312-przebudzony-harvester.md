---
layout: inwazja-konspekt
title:  "Przebudzony... Harvester?"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160227 - Zakazany Harvester (PT)](160227-zakazany-harvester.html)

### Chronologiczna

* [160227 - Zakazany Harvester (PT)](160227-zakazany-harvester.html)

## Kontekst ogólny sytuacji

* Kajetan wezwał siły Weinerów, by zajęli się Harvesterem
* W tym czasie Paulina doszła do siebie i zajęła się ratowaniem żyć ludzi
* Doszło do kilku ścięć między Pauliną, Kajetanem a Pamelą Weiner, której nie zależy na ludziach. Kajetan wygrał.
* Harvester zostanie zniszczony. Pamela nie próbuje zachować Harvestera.
* Pamela i Filip ze wsparciem Kajetana zajmują się rdzeniem Harvestera.
* Oksana i Patryk (lekarz) zajmują się okolicznymi fragmentami Harvestera.
* Paulina pracuje nad Iloną (dziennikarką), bo jest świeżą ofiarą Harvestera. Jest nadzieja.
* Oksana załatwiła pozwolenia ze strony formalnej.

## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Paulina zrobiła triage. Ilonie da się jeszcze pomóc. Paulina zabrała się za stabilizację (Patryk jej pomógł) i Paulina tylko czeka, aż zniszczony zostanie rdzeń Harvestera. Jednak Ilonie może pomóc nawet teraz. Bez zniszczenia Harvestera. Więc... to robi. Bo tylko jej można pomóc od razu.

Paulina ma do czynienia z bardzo trudny testem (9). Próbuje więc pogadać z Feliksem, znaleźć podkładkę pod odbudowę Ilony. Feliks opowiedział jej o Ilonie; powiedział jaką osobą była i jest. Paulina pracuje nad naprawą wzoru Ilony... ma sprzęt (+3), wiedzę Feliksa (+1), czas (+1). Jakkolwiek bezpieczniej dla pacjenta będzie zrobić to po zniszczeniu rdzenia Harvestera, lepiej zrobić to wcześniej; nie wiadomo co będzie.

I Paulinie się udało. Ilona otworzyła oczy.

* Miała pani wypadek. - Paulina, łżąc bezczelnie
* Co... - Ilona, nie do końca jeszcze kontaktując - Obwody... głosy... burmistrz... lekarze...

Na hipernecie rozległ się spokojny głos Pameli, ogłaszającej, że rdzeń całego urządzenia (Harvestera) został znaleziony. Unieszkodliwianie w toku. Paulina powiedziała Kajetanowi, że obudziła pacjentkę. Pamela skomentowała "how nice; czy tam są magowie"? Paulina zostawiła to bez komentarza, choć chciała zabić...

Paulina zadała kilka pytań Ilonie (to zrobimy wstecznie).

Odpowiedzi na WSTECZNE pytania:

* Czy jest więcej połączonych z Harvesterem? Tak, też poza szpitalem. Każda komórka Harvestera ma kogoś, choć jedną dronę. ~20~30 w sumie, z czego w szpitalu ~15.
* Harvester jest drobny, ale szeroki. Rozszerzony jest bardzo, bardzo szeroko.
* Harvester nie ma backupu. System redundancji jest bardzo uszkodzony.

Pamela wyłączyła rdzeń Harvestera. 

Ilona dostała "jolt". Nie jest to elektryczny jolt, ale jest. Paulina, obecna koło Ilony, zrozumiała co się dzieje - Ilona się rekonektowała do Harvestera... którego Pamela... wyłączyła? Zanim Paulina skontaktowała się z Kajetanem... wyczuła, że to NIE Harvester. Ilona rekonektowała się do czegoś nowego. Jak Harvester, na tych samych kanałach, ale nowego.

Ilona spojrzała na Paulinę z furiacką wściekłością, ale po chwili... rozpoznała Paulinę (?). To nie powinno się stać - nie ma backupu.

* Coś nie tak? - Paulina, ostrożnie
* Jesteś magiem. Próbowałaś mi pomóc. - Ilona (?), nieco nieswoim głosem
* Oczywiście, że próbowałam pomóc, jestem lekarzem; proszę się nie niepokoić, dezorientacja minie... - Paulina
* Rekonstytucja zakończona. Już nie drona. - Ilona, z uśmiechem - Jestem WOLNA.
* Zejdź mi z drogi, czarodziejko. - Ilona, nieco niepewnym krokiem
* Czekaj, jesteś słaba. To naprawdę nie jest rozsądne wychodzić... - Paulina, wyczuwając krwawą energię w kierunku Ilony, mającą ją wzmocnić.

Paulina rozejrzała się po innych pacjentach. Do nich też wpływa energia. Nie budzi ich to... ale stabilizuje. Harvester odwraca swoje działanie?

* A inne drony? - Paulina, ostrożnie, chcąc wybić "Ilonę"
* Jeszcze nie umiem używać wszystkich... kończyn - Ilona - Ale ludzi mogę uwolnić... 
* Powiedz mi... lekarko... którym nie mogę już pomóc? Nie marnować energii... - Ilona
* To chwilę zajmie - Paulina
* Mamy chwilę. Poczekam. Potem pójdę. - Ilona - Nic ci nie grozi. Chciałaś pomóc.
* Nie ja jedna... - Paulina

Po hipernecie rozległ się sygnał od Patryka - Oksana stracona. Zatrzymała atak, ale została... zaplątana przez te cholerne urządzenia? Patryk próbuje się wydostać, ale...

* Cokolwiek robisz, proszę, przestań - Paulina do Ilony
* ... - Ilona nie odpowiada ani słowem
* Jeśli cokolwiek robisz z kimkolwiek... proszę, przestań! - Paulina, mocniej
* Energia. - Ilona z uśmiechem - Ubezpieczenie przeciw magom. Jej nic się nie stanie... jeśli im też nie.
* Nastawiasz ich wrogo na "dzień dobry". To bardzo złe działanie. Oni cię uwolnili. - Paulina
* "Jakie to słodkie. Czy tam są magowie?" - Ilona, z zimnym uśmiechem, cytując Pamelę
* Wciąż uważam to za głupotę... - Paulina, zrezygnowana
* Pomóż mi, komu mam nie dostarczać energii. Mamy jej mało. - Ilona, spokojnie do Pauliny

Patryk wysłał kolejny rozpaczliwy sygnał SOS. Pamela wysłała Kajetana do pomocy Patrykowi, ale to za daleko, ten musi się tam dostać...

Paulina zaczęła prosić Ilonę. Patryk - lekarz - pomagał, nie tylko magom. Pamela może dowodzić, ale nie jest jedyna. Kajetan też pomagał, od samego początku. By ludziom nie działa się krzywda. Jest więcej magów, którzy pomagają niż Pameli. Niech pozwoli Patrykowi uciec. To nie jego wina... a bez pomocy Patryka trudno będzie jej pomóc.

Ilona: +2 (harvester), 2 (impuls + charakter) = 4. Paulina: 5+3.

Ilona została zdestabilizowana prośbą Pauliny. Zaczęła się mieszać... jest tam Ilona, ale jest też "Harvester". Da się wyciągnąć Ilonę z nowego kolektywu. Po chwili, "Kolektyw" doszedł do siebie i powiedział Paulinie przez Ilonę, że Patryk może uciec.

Faktycznie, Patryk nadał na hipernecie, że nie wie, co Pamela zrobiła, ale żeby robiła to dalej. Pamela zdziwiona.

* Zaczekaj! - Paulina, do odchodzącej Ilony
* Zanim wezwiesz posiłki? Wiem jak działa hipernet - Ilona, zdziwiona

Paulina widzi, że ma do czynienia z kimś, kto wie jak myślą magowie i wie jak działa magia i Świeca. A jednocześnie naprawdę nie lubi magów. Paulina ją przekonała, by zostawiła Ilonę w szpitalu; zwłaszcza, że nie do końca kontroluje sytuację. Ilona wróciła do stanu półuśpienia. Paulina poszła do szpitala, zająć się stanem ludzi...

Wpadła ekipa Weinerów. Wszyscy. Kajetan, Patryk, Filip, Pamela. I - ostra dyskusja...

Filip powiedział, że harvester wygląda na "opętany". Nie wiadomo o co chodzi, ale jakaś forma opętania dowodzi tym harvesterem... Pamela zauważyła, że mogą włączyć Harvester. Zreflektowała się, że nie mogą - Oksana... Pamela zaproponowała porwanie Oksany, ale Kajetan odbił, że najpewniej Oksana jest świetnie chroniona lub ukryta.

Paulina zaproponowała odcięcie dron. Pamela spytała co Paulina ma przez to na myśli (nawet Pamela nie chce zabijać ludzi). Paulina zaproponowała więc ratowanie ludzi i odcinanie ich od Harvestera, odzyskanie ich osobowości i energii życiowej. 

* Chcesz powiedzieć, że będziemy ratować każdego człowieka, jednego po drugim? - Pamela, zaskoczona
* A mamy większy wybór? - Paulina, z lekką niechęcią
* Ochotnik! - Patryk - Nie mam nic do ludzi, a jeśli to pomoże...
* Zbadam przepływy - Filip - Dojdę do tego, o co tu chodzi i może znajdę jakieś źródła energii. Może zasilony Harvester nie będzie...
* Dobrze. Zróbmy tak - Pamela, zmartwiona - Może Harvester nie będzie nam szczególnie przeszkadzał... nie zauważy co robimy.

Po spotkaniu Kajetan wziął Paulinę na stronę. Zauważył, że Paulina przecież obudziła pacjentkę... a jednak pacjentka jest nieprzytomna. Co się stało? Paulina zaznaczyła, że nie wie, co to jest. Nie jest to zwykłe opętanie. Jest to inteligentna istota. Wie o magii i magach. I nie ceni sobie magów. Boi się ich. Oksana jako zabezpieczenie...

* Oksana jest siostrą Pameli i żoną Filipa - Kajetan powiedział, po prostu. Paulina facepalmowała.
* Nie wiem jak to zrobisz, ale żadne z nich nie mogą tego powiedzieć przy żadnej z dron... - Paulina, błagalnie
* Dobrze. Powiem im... jakoś. - Kajetan, smętnie
* Ta istota wie więcej. Magia, magowie, hipernet... najpewniej od magów wciągniętych przez Harvester. - Paulina
* Jest gorzej. To nie Harvester, to Aleksandria. Żywa biblioteka. - Kajetan - Harvester to tylko zasilanie dla Aleksandrii. I wszystko się spieprzyło.
* Jak to się ma do osobowości magów czy ludzi w środku? - Paulina
* Będzie zanikać, lub się przesuwać. - Kajetan
* Wyłoniła się więc taka osobowość dominująca? - Paulina
* W praktyce, to nie trzyma się kupy. Nie powinna się taka wyłonić. Nie TAKA. - Kajetan, zrezygnowany - I nie możemy nikogo zawiadomić
* Ta istota boi się magów, przez co jest wroga. Agresja ze strachu - Paulina - Oksana jest zabezpieczeniem
* Świetnie... - Kajetan.

Paulina zaznaczyła też, że ona i Patryk są bezpieczni. Ta istota ich nie będzie atakować. Ale Pamela jest zagrożona, szczególnie. Kajetan zauważył, że teraz ONI muszą dostarczyć źródła energii do harvestera. I nie mogą po prostu odpiąć źródeł energii - bo ludzie w Aleksandrii umrą. Coraz lepiej... ktokolwiek (cokolwiek) przejął kontrolę nad Harvesterem, niekoniecznie rozumie implikacje.

Z pomocą Patryka i Filipa Paulina szybko namierzyła ludzi, którym da się pomóc i tych, którym nie da się pomóc. Tych, którym nie da się pomóc... tych należy zostawić jako źródło energii. A pozostałym będzie dało się pomóc.

Dzień 2:

Kajetan przyszedł do Pauliny, wcześnie rano. Powiedział jej, że po tym, jak wieczorem Patryk + Filip + Paulina odłączyli 2 osoby, Harvester zaczął mocniej wysysać Oksanę. Do tego stopnia, że Filip sam zauważył pozycję Oksany - może ją namierzyć...

W związku z tym, Kajetan chce uderzyć w Oksanę i ją porwać. By to zrobić, Pamela ma zamiar połączyć się ze źródłami Harvestera i zafloodować Harvestera dziesiątkami sprzecznych sygnałów. To kupi czas Kajetanowi.

Paulina zauważyła, że do tej pory ich relacje z Harvesterem były dość... pozytywne. Szkoda to tracić. Zaproponowała, że wpierw ponegocjuje. Kajetan się na to zgodził - zaatakować zawsze można.

Kajetan, Pamela i Filip przygotowali się do operacji odbicia Oksany. W tym czasie Paulina i Patryk poszli porozmawiać z "Iloną" - końcówką Harvestera (?). Ilona siedzi, dużo silniejsza i aktywniejsza niż kiedykolwiek wcześniej. Jest jej lepiej. Ilona się poskarżyła, że jej lepiej... ale jest głodna. Ma pełny żołądek, ale czuje uczucie głodu. Silne. Coś z nią nie tak. Paulina od razu rozpoznała głód na poziomie taumatycznym... Przy okazji - Paulina zobaczyła wielkie kwiaty dostarczone przez Feliksa, który wcześniej Ilonę odwiedził ;-).

By wezwać Harvester, Paulina wlała odrobinę energii magicznej w Ilonę. Ta zesztywniała i zmieniła się jej postawa. Paulina wezwała Harvester. W rozmowie z Harvesterem, Paulina powiedziała, że pomogą Harvesterowi... jeśli ta uwolni Oksanę. Harvester zauważyła, że nie może - magowie zadziałają przeciwko niej. Niech magowie ustalą źródła energii na miejsce odcinanych ludzi. Niech wprowadzą węzły i inne takie.

Harvester zauważyła, że zrobiła skan Oksany; jeśli ją uwolni, to na 99% siły Weinerów i tak Harvester zaatakują. A więc impas. Przyciśnięta słownie, Harvester skrzyczała Paulinę - ma cztery Harvestery, delikatną kontrolę, są tam jakieś systemy awaryjne i boi się je włączyć. A przecież udało jej się prowadzić do odcinania ludzi. Więc wykazała dobrą wolę. Więc o co tu chodzi?

Kajetan zgodnie z planem wszedł i odbił Oksanę. Harvester wypadł z kontroli Ilony...

Wielkie spotkanie WSZYSTKICH magów w centrali szpitala...

* Dobrze. Niszczymy ten Harvester. - Pamela, ze spokojem
* Jest tego więcej... - Kajetan.
* To raz. A dwa... uważam, że niszcząc ten Harvester bez sprawdzenia co się tak naprawdę stało... możemy popełnić morderstwo. - Paulina
* Możesz to... poszerzyć? - Pamela, zdziwiona
* Ktokolwiek tam jest, jest inteligentny. - Paulina
* Jak ludzie. - Pamela, nie rozumiejąc o co Paulinie chodzi
* Jak odróżnisz maga od człowieka? - Paulina, zirytowana
* W sensie? - Pamela, nie rozumiejąc
* Musisz to robić, by określić, komu pomóc - Paulina
* Ogólnie, próbuję pomóc wszystkim... - Pamela, nieco zakłopotana
* Nie mówię 'zabić tą istotę'. Mówię 'zniszczmy Harvester'. Ta istota jest... daleko. - Pamela, precyzując
* A jeśli ten Harvester jest potrzebny, by istota przeżyła? Energia z niego? - Paulina
* Co proponujesz? Bo jeśli nie zniszczymy Harvesterów, oni - wszyscy ludzie - zginą. - Pamela, zirytowana
* Zastąpmy ludzi źródłami energii i wyłączmy pozostałe Harvestery. Tego chciała - Paulina - Przy okazji, mniej się będzie bać
* Jeżeli to mag lub człowiek, spróbujemy uratować... - Pamela - Ale jeżeli to człowiek, nie będę się BARDZO starała.
* Ale ja będę - Paulina, zimno
* Pomogę Paulinie - Kajetan
* Dobrze... sprowadzę pewne źródła energii... - Pamela, zrezygnowana

Paulina stara się - powoli - przygotować sobie grunt do "nawrócenia" Pameli na nieco bardziej ludzistyczne podejście ;-).

# Progresja

* Oksana Weiner: uzyskana częściowa wiedza od "Harvestera"

# Streszczenie

Kajetan ściągnął Weinerów by wyczyścili Harvester. Niestety, okazało się, że Harvester został opętany (?) i aktywnie walczy przeciwko magom. O dziwo, ich agenda jest spójna - zarówno Harvester jak i Weinerowie chcą uwolnić ludzi. Okazało się, że opętanie Harvestera jest delikatnym i niestabilnym bytem o dużej wiedzy o Świecy. Koordynowana akcja Kajetana doprowadziła do uratowania porwanej czarodziejki. Plan ratowania ludzi dalej w mocy.

# Zasługi

* mag: Paulina Tarczyńska, ratująca życia ludzi mimo niechęci Pameli i życia magów mimo niechęci "Harvestera". Ufa tylko Kajetanowi, bo też chce pomóc...
* mag: Kajetan Weiner, jedyny terminus w okolicy. Pomysłodawca niebezpiecznego planu odbicia Oksany z Pauliną rozpraszającą "Harvester". Nadal chce pomóc ludziom (nie są winni działań "Harvestera").
* mag: Pamela Weiner, dowodzi Weinerami; nie jest to ZŁA czarodziejka, acz zdecydowanie nie dba o życie ludzi (choć nie ma nic przeciwko pomaganiu - "to nie jej sprawa"). Min. technomantka.
* mag: Patryk Maus, jeszcze Weiner, lekarz; ma nastawienie "pomóżmy wszystkim, też ludziom" - dzięki temu udało się Paulinie go uratować przed "Harvesterem".
* mag: Filip Weiner, mąż Oksany; niezły i delikatny katalista specjalizujący się w identyfikacji i wykrywaniu; dość pacyfistyczny i dogaduje się z Pauliną.
* mag: Oksana Weiner, Nie jest terminusem, ale ma jakieś umiejętności bojowe. Spokrewniona z większością magów ekspedycji Weinerów; złapana przez "Harvester" jako leverage.
* czł: Ilona Maczatek, reporterka opętana przez "Harvester". Postawiona na nogi przez Paulinę, dzięki czemu Paulina miała opcję negocjacji z "Harvesterem"

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski, na wschodniej części województwa
                1. Krukotargowo
                    1. Centrum
                        1. Szpital miejski, centrum dowodzenia Zespołu w walce z Harvesterem
                    1. Dzięciolnia
                        1. Wiatraki, pełnią rolę tymczasowej elektrowni; gdzie znajdował się Rdzeń Harvestera

# Czas

* Opóźnienie: 3 dni
* Dni: 2