---
layout: default
categories: inwazja, campaign
title: "Taniec Liści"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Powrót Karradraela](kampania-powrot-karradraela.html)

## Opis

-

# Historia

1. [Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html): 10/05/14 - 10/05/19
(160922-czarnoskalski-konwent-rpg)

Kornel i Dorota martwią się tym, że Hektor skazał Izę na Millennium. By dowiedzieć się co się z nią dzieje, zaangażowali Esme i wkręcili lokalnego księgowego podkochującego się w Diakonce by od owej Diakonki wyciągnąć info. Esme i Mateusz zrobili konwent RPG, doprowadzili do utraty płynności małego browaru "Czarny Tur" i wkręcili Supernową Diakon w to, że Mateusz uratował ją przed psychopatką. Mateusz i Supernowa zostali kochankami. Esme ostrzegła Świecę przed stanem Kornela i Doroty po tym, jak zobaczyła, jak oni przeżyli informacje o Infensie Diakon...

1. [Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html): 10/05/26 - 10/05/29
(160915-rekrutacja-mimo-woli)

Dorota Gacek dostała za zadanie przejąć kontrolę nad firmą TrustPort. Wmanewrowała szefów tej firmy w stary konflikt z konkurencyjnym Cormexem powiązanym ze Skubnym, po czym oddała ich w ręce Kornela Wadery. Szefowie TrustPort nadali donos na Cormex, po czym Dorota miała możliwość tak ułożyć wydarzenia, by TrustPort wpadł w ręce Tańca Liści.

1. [Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html): 10/07/15 - 10/07/17
(160911-reedukacja-infensy)

Siluria bezpiecznie ewakuowała się z Mileną; teleportowana przez magów krwi Millennium prosto do Kopalina, a stamtąd użyła portalu by uciec do Pirogu Dolnego - tam znajduje się kwatera główna Millennium. Wiktor, wsparty dowodami przez Dagmarę wierzy, że Siluria została porwana. Mileną opiekuje się najlepsi. Rafael przyprowadził Silurii Infensę (kiedyś: Izę Łaniewską) by ta skończyła reedukację. Siluria uwiodła w serii walk Infensę, budząc w niej aspekt dominujący i uległy oraz upewniła się, że Infensa już nadaje się do normalnego działania. Infensa opowiedziała też Silurii o swoich dawnych manewrach z Kornelem, gdzie zrobili grupę "Taniec liści" z ludzi i magów mających oczyścić Świecę i inne gildie magów z nieprawości...

1. [Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html): 10/07/22 - 10/07/24
(160921-wandy-wolnosc-i-wrozda)

Taniec Liści przygotował materiały mające pogrążyć Hektora Blakenbauera i podłożył to . Przechwycił informacje Skubny i kazał spowolnić i dowiedzieć się co tam jest; Hektorowi nie może się nic stać (Tymotheusa wola). Ludzie Skubnego zdobyli info co tam jest (Kinga Melit, Wanda Ketran, ukrywanie dowodów) i spowolnili. Edwin i Margaret z matrycy Karradraela (krwi Mausów) zrobili klona Wandy i ludzie Skubnego podłożyli tego klona. Potem Diana Larent kazała (przez Rokitę) porwać Wandę i przekazać ją Czesławowi Czepcowi; okazało się, że to kapłan Iliusitiusa. Iliusitius przywrócił PRAWDZIWĄ Wandę z Rezydencji. Wanda Ketran jest na wolności... a Skubny wyraźnie nie w pełni kontroluje sytuację w swojej organizacji.

## Progresja

|Misja|Progresja|
|------|------|
|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html) terminusi kontrolni Świecy zainteresowali się jej działaniami i potencjalną zmianą w czarnego terminusa|
|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html) terminusi kontrolni Świecy zainteresowali się jego działaniami i potencjalną zmianą w czarnego terminusa|
|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html) dostaje znajomości w firmie transportowej TrustPort|
|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html) dostaje znajomości w firmie transportowej TrustPort|
|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|Taniec Liści dostaje macki w firmie transportowej TrustPort|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html) zaakceptowała swoją dualistyczną naturę - dominator i uległa|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html) przestała być Izabelą Łaniewską i pogodziła się z Silurią Diakon.|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html) podporządkowana Silurii Diakon|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) dominuje nad Infensą Diakon|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html) wydostała się z Rezydencji Blakenbauerów mocą Iliusitiusa|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html) powiązanie z Szymonem Skubnym|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html) powiązanie z Marianem Rokitą (enforcerem Skubnego)|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html) powiązanie z Iliusitiusem|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html) ma prawo do wróżdy wobec Rodu Blakenbauerów|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html) ma nałożoną na siebie Matrycę Iliusitiusa|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Zdzisław Kamiński](/rpg/inwazja/opowiesci/karty-postaci/1709-zdzislaw-kaminski.html) jest wolny od Skubnego (spłacił dług)|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|[Hubert Brodacz](/rpg/inwazja/opowiesci/karty-postaci/1709-hubert-brodacz.html) jest wolny od Skubnego (spłacił dług)|

