---
layout: inwazja-konspekt
title:  "Ballada o duszy ognistej"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160918 - Walka o duszę Draceny (PT)](160918-walka-o-dusze-draceny.html)

### Chronologiczna

* [160910 - Na żywym organiźmie Draceny... (PT)](160910-na-zywym-organizmie-draceny.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Wyciek syberionu został opanowany.
Jelena przybywa by poznać historię swojej nieznanej kuzynki (bo Korzunio jej powiedział o Kseni).

"Hunger grows stronger
I feel it day by day
I need, I desire
Her passion now."

Ognista Dama. Ksenia Zajcew. Pryzmatyczna efemeryda. Opętała zwykłą kobietę. 
Ktoś przywozi śmieci ze swojej fabryki i dumpuje je w Danusinie. Inkarnowała się Toksyczna efemeryda.
Ksenia vs Danusia.

Ż: Jak nazywa się nieco większa miejscowość niedaleko?
R: Gumowice
Ż: Skąd Korzunio wiedział jak zachęcić Jelenę?
K: Jelena i Korzunio mają wspólną historię; pomagali sobie wielokrotnie.
Ż: Co do cholery robi ksiądz na bagnach?
R: Udał się z rana na grzyby, bo chciał pozbierać kurki do jajecznicy.
Ż: Czemu tienowaty przyda się Jelenie?
K: Bo może dać jej coś, co ułatwi jej rozwiązanie problemu a jemu da możliwość zredukowania kosztów.
Ż: Skąd wiesz o możliwości uzyskania dużej energii magicznej w okolicy?
R: Dostałem cynk od dobrego znajomego w Świecy wiedzącego o moich zainteresowaniach.
Ż: Name
K: Kira Zajcew

## Misja właściwa:

Dzień 1:

Henryk wie gdzie do domu. Niestety, po drodze jest bagno...
Henryk dowiedział się o wycieku energii magicznej od Kiry (niby go lubi).
Ale... bagno.

"Bagno chce mnie bliżej poznać" - Henryk Siwiecki, 2016

I bagno totalnie chce bliżej poznać Henryka. Jest to dziwne...
Bagno bliżej poznało Henryka. Potknął się i plasł.
Szczęśliwie jak już myślał, że nigdy nie wyjdzie żywy, zobaczył leśniczego, który usłyszał krzyki.
Leśniczy zapytał Henryka, czy ten pił. No, nie...

Henryk przebił iluzję. Bagno jest malutkie a on chodzi w kółko... czyli Henryk jest na obszarze anomalicznym.
Damazy prowadzi go ze sobą, do leśniczówki. Henryk próbuje zaskarbić sobie zaufanie leśniczego - wyszło. 
Leśniczy powiedział na pytanie co tu dziwnego - szerszenie w Danusinie. I dzikie wysypisko.
Henryk wrócił do Stawni... ale tam jedzie rybą ].].

Jelena. Przybyła do Stawni, gdzie Korzunio opowiedział jej o jej krewnej, Ksenii, która tu zginęła w tajemniczych okolicznościach. 
A Zajcew zawsze szuka: dobrej opowieści, silnego alkoholu i okazji do opowiadania podczas picia. Bitka się też przyda.
Jelena wynajęła pokój niedaleko przychodni.

Jelena poszła do biblioteki. Spotkała tam bibliotekarza - Tomasza Weinera. Zaciekawiona - puściła aurę ognistą. Weiner się nie odwrócił. Nie wie - nie wyczuwa magii. Ale Jelena wyczuła rezonans energii Zajcewów. Zdecydowała się poszukać...

Henryk (ubłocony ksiądz) wchodząc do Stawni wyczuł obecność magii ognistej. Podszedł do biblioteki i spotkał się z Jeleną. Zatrzymał ją i zaczął rozmowę... nie miał zbyt wysokich akcji, ale coś tam powiedział...

"Czemu chciałaś spalić bibliotekę?" - Henryk
"Jakbym chciała spalić tą budę, spaliłabym ją od ręki..." - Jelena, pogardliwie
[Echo Ksenii uderzyło magicznie w Jelenę]
[Henryk odepchnął Jelenę po biuście od ataku Ksenii, bo go wykrył]

W oczach Jeleny akcje księdza poszły do góry.
Henryk zorientował się co się dzieje - są w silnie kontrolowanym przez Pryzmat obszarem. On się zgubił na bagnach, bo uwierzył w możliwość zgubienia się.
Jelena została zaatakowana, bo powiedziała o paleniu biblioteki...?
Ale czemu zamiast tego nie spłonęła biblioteka?

Henryk powiedział Jelenie o Pryzmacie i o tym, że Pryzmat uderzył w Jelenę, gdy ta mówiła o paleniu biblioteki...
Jelena potwierdziła, że tu niedawno miał miejsce wyciek syberionu. 

Poszli do Warenek u Babuszki i Jelena zamówiła podwójną porcję pierogów i kwas chlebowy. Henryk kwasik i poje...
Moment. Są na wschodzie. Jelena poprosiła babuszkę o alkohol.
Jelena wypiła jej samogon... i się zakrztusiła. Syberion. Syberion wszedł w alkohol...

W żyłach Jeleny płynie syberion. Niebezpieczne dla maga, niebezpieczne dla otoczenia Zajcewki. 
Jelena się wypala i czyści... a jednocześnie Henryk wyczuł potężną energię magiczną, podobną Zajcewce.
Henryk dla sprawdzenia przywołał SŁABE piwo. Cholerstwo się zmaterializowało, Henryk skosztował i padł zalany w trupa.
...Jelena... się podłamała...

Ubłocony, zalany ksiądz. W łóżku Jeleny.

Jelena zadzwoniła do Korzunia - powiedziała, że zawalili poprzedni sprzątający. Kto sprzątał? Niektarij Zajcew. Jelena dostała telefon...
Jelena okrzyczała Niektarija i powiedziała mu, że opierniczy go za spieprzenie z syberionem. Zażądała miecztapauka; ten powiedział, że wystrzeli butelkę. Niech łapie. Jelena wyjedzie do Gumowic i złapie...

Jelena złapała auratycznie butelkę z miecztapaukiem. Wróciła do Stawni i wypuściła miecztapauka...

Dzień 2:

Henryk się obudził, w łóżku Jeleny. Nie ma kaca. Jelena śpi w przejściu.
Henryk przyznał się Jelenie, że wyczuł wcześniej aurę ognistą - ale nie wie, co to takiego i gdzie. Jelena potwierdziła - kiedyś wyczuła to w bibliotece.
Jelena przyznała się do miecztapauka - niszczyciel marzeń.

Ksiądz poszedł się przebrać. I Anton Jesiotr przyszedł do niego i poprosił o modlitwę wobec bezbożniczki. Halina Weiner ma objawienia. I jego syn z nią kręci... więc czy nie mógłby ksiądz... no, rozwiązać problemu? Henryk się zgodził. Rozwiąże.
To dziwna sytuacja - nie miało być w ogóle magów. Skąd się wzięła tutaj czarodziejka, zwłaszcza Halina Weiner? Mająca objawienia? (i zdaniem Antona, biorąca LSD)

Co gorsza, Henryk ma wrażenie, że "ogień jest w powietrzu". Pryzmat... jest agresywny. Bojowy. Gotowy.

Henryk skupiając się na Pryzmacie - przywołał echo syberionu. Niestety...
Po "zwymiotowaniu płuc" i przejściu superkaca w przyspieszonym tempie - Henryk wyczuł, że ognista energia materializuje się przy... dzikim wysypisku.
Przechlapane.
A Jelena opowiedziała historię o Ognistej Duszy którą pobrała z biblioteki - o Ksenii Zajcew.

Jelena przygotowała kilka historii bardowskich Zajcewów, by móc je wykorzystać w warunkach bojowych. Czy by ukoić Ognistą Duszę, czy by móc ją uspokoić, czy móc jej pomóc czy wykorzystać...
Jelena chce uzyskać najbardziej epicką opowieść.

Korzystając z okazji, że jest tu Pryzmat i korzystając z tego, że jest bardką, Jelena opowiada Henrykowi historie mające doprowadzić do sytuacji, żeby Ognista Dusza z nimi nie walczyła. Miecztapauk wysysa zbędne komponenty a Jelena kształtuje Pryzmat. Katalizuje go. Kształtuje Pryzmat Syberyjski...

Nieświadomie, Jelena doprowadziła do Inkarnacji Ognistej Duszy w Halinie Weiner; ale udało jej się doprowadzić do tego, że Ognista Dusza będzie uważać ich za sojuszników a nie potencjalnych przeciwników / neutrali.

Bohaterowie zbliżają się do dzikiego wysypiska śmieci. Henryk poczuł coś nie tak; skupił się i znowu wylądował w błocie. Ale postawiony na nogi przez Jelenę powiedział, że wyczuł - wyczuł DWIE wole. Jedna jest ognista, ochronna i niszczycielska. Druga jest toksyczna, bolesna, słodka i mściwa.
Wniosek: mają dwie legendy. I nie wiadomo która jest ta druga.

Niestety, nie ma tu niedźwiedzi zgodnie z tym co mówił leśniczy.
ALE SZERSZENIE TAK!

Oki... Jelena wdeptuje Henryka w błoto - ten ma bonus by być skuteczniejszy.

"Siwiecki doznał błotnego oświecenia!" - Raynor

SZERSZENIE ŚMIERCI ATAKUJĄ! Ksiądz pocięty, toksyna w żyłach... Jelena go podpaliła magicznie. Wypuryfikowała. "Ksenia" przesłała trucizny i Skażenia z Siwieckiego na Jelenę... ta leży i rzyga jak kot. Ksiądz golutki (acz w iluzji ubrania i pro koloratki) stoi... wszędzie pożar, fatalna sprawa...
Henryk ma kilka poparzeń pierwszego stopnia. Jelena już też nie ma problemów z ubraniem.

Pożar wyrywa się spod kontroli. Ksiądz ma dostęp do obu efemeryd...

Henryk skupia się by móc wymusić na pryzmacie Ognia do zniszczenia pryzmatu Toksyny. Wola Henryka wzmocniła "Ksenię" i wypaliło do zera Danutę Weiner - wektor Toksyczny. Jelena została uwolniona z trucizn - jest znowu w dobrej formie.

Jelena zwraca się do "Ksenii" - wykorzystuje inferno jako płótno, farby... ogień jako środek przekazu. Jako bardka, zwraca się do niej i tworzy o niej opowieść. Chce się porozumieć z efemerydą. "Poszła, znalazła problem, nie mogła rozwiązać inaczej, rozpętała ogień i zabrała go ze sobą by nie skrzywdzić niewinnych".

Jelena zwróciła się do ognia a ogień do niej. Ciężkie poparzenia... ale ustabilizowała ogień. 
Henryk użył mocy "Ksenii" by wypalić wszystkie INNE źródła Pryzmatu lub Skażenia. Udało się.

Jelenie udało się zabutelkować z powrotem miecztapauka. I przekazała go Korzuniowi. "Oficjalnie sczezł".
Henrykowi udało się wyłączyć Źródło Pryzmatu Ksenii. I zdobyć surowce.

Stawnia została oczyszczona.
Po raz kolejny ;-).

EPILOG:
W zupełnie innym miejscu, w Gumowicach: zginęły 2 osoby które wywoziły farby i lakiery na dzikie wysypisko.

# Progresja

* Krystian Korzunio: dostał zabutelkowanego miecztapauka od Jeleny Zajcew. Na wszelki wypadek.
* Henryk Siwiecki: dostał 1 void

# Streszczenie

Pewien czas temu, wyciek syberionu został opanowany w Stawni. Jelena Zajcew pojawiła się tu by poznać opowieść o swojej kuzynce, Kseni. Jednak okazało się, że wyciek NIE został wyczyszczony całkowicie... wraz z Henrykiem Siwieckim spróbowali coś z tym zrobić. Przebijając się przez Pryzmat, odkryli, że są DWIE efemerydy różnych Pryzmatów. Ogniem zniszczyli Toksynę a potem uspokoili Ogień. Nieświadomie, Jelena inkarnowała Ksenię w Halinie Weiner; ale oczyścili Stawnię. Ponownie.

# Zasługi

* mag: Henryk Siwiecki, miejski ksiądz na bagnach, który zaliczając glebę uratował las od pożaru i zniszczył Toksyczną efemerydę.
* mag: Jelena Zajcew, opiekowała się miejskim księdzem (* magiem), brała z niego rany, opieprzała kuzynów i ogólnie wypyszczona bardka Zajcewów.
* mag: Kira Zajcew, która powiedziała Henrykowi o tym, że w okolicy da się zdobyć kilka quarków - i wpakowała go w "to bagno".
* czł: Damazy Rozenblum, leśniczy w Stawni; wyciągnął tien Henryka Siwieckiego z bagien i powiedział o miejscu z potencjalnym problemem z szerszeniami
* czł: Krystian Korzunio, który nie jest pewny czy grupa sprzątająca wyciek syberionu zrobiła wszystko poprawnie, więc powiedział Jelenie o Ksenii.
* mag: Niektarij Zajcew, który uprzednio tu sprzątał wyciek syberionu; zaszantażowany przez Jelenę się złamał i wysłał jej zabutelkowanego miecztapauka.
* czł: Anton Jesiotr, który powiedział Henrykowi o tym, że dziewczyna jego syna ma objawienia i poprosił o modlitwę (nie chce, by jego syn kręcił z Haliną)
* czł: Halina Weiner, która ma objawienia Ognistej Duszy; przez błąd * magów "Ksenia" się w niej inkarnowała.
* czł: Tomasz Weiner, bibliotekarz który mimo nazwiska nie pamięta o istnieniu * magii i w sumie nie jest * magiem.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Hrabiański
                1. Stawnia
                    1. Centrum
                        1. Warenki u Babuszki, gdzie można posiedzieć, pojeść i popić i gdzie babuszka pędzi świetną śliwowicę
                    1. Oziersko
                        1. Smażalnia ryb Jesiotr, której zapach wszechobecnie atakuje księdza (i odstrasza Jelenę); w przybudówce mieszka Henryk.
                    1. Leśnicz
                        1. Bagna leśnickie, gdzie się ksiądz zgubił przez jedną z syberionowych anomalii.
                        1. Las ozierski
                            1. Leśniczówka, gdzie znalazł opiekę Henryk po tym, jak leśniczy wyciągnął go z bagien.
                            1. Danusin, gdzie niedaleko leśnej dróżki zrobiono dzikie wysypisko i gdzie znajduje się Toksyczna efemeryda.
                1. Gumowice, niedaleko Stawni; stamtąd wywalano farby na dzikie wysypisko
     
# Skrypt

brak

# Czas

* Opóźnienie: 2 dni
* Dni: 2