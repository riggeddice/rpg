---
layout: inwazja-konspekt
title:  "GS Aegis 0002"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141119 - Antygona kontra Dracena (SD, PS)](141119-antygona-kontra-dracena.html)

### Chronologiczna

* [141119 - Antygona kontra Dracena (SD, PS)](141119-antygona-kontra-dracena.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

- Netheria pojawiła się na tym terenie by zrobić z dziewczynami epicką imprezę z kralothem na wieczór panieński.
- Olaf pojawił się na tym terenie by odnaleźć coś ważnego z przeszłości.
- Czary Olafa i Netherii zarezonowały. Obudzili <coś>, co uruchomiło Rdzeń Spustoszenia.
- Potem już poszło kaskadowo... pierwszymi Spustoszonymi magami były Netheria i Estrella.
- Marian też został Spustoszony. Gdy tylko zorientował się GS "Aegis" 0002, też został Spustoszony.
- 0002 odrzucił moduł kontroli i unieszkodliwił Mariana. Oraz skupił się na ochronie, co pasowało do jego "charakteru".

## Misja właściwa:

KADEM. Janek Wątły poprosił do siebie Sabinę Diakon (Siluria Tyrania Diakon) oraz Pawła Sępiaka. Powiedział im, że mają problem - godzinę temu stracili kontakt ze znajdującą się w mieścinie Koty głowicą GS "Aegis" numerowanej 00002. Co gorsza, stracili też kontakt z kontrolerem głowicy, Marianem Łajdakiem. Głowica wysłała czerwony alarm po czym zniszczyła sobie moduł kontrolny (co Janek uważał za niemożliwe). To spowodowało samoczynne włączenie mechanizmu samozniszczenia (Janek nie uważa, by głowica była w stanie to zatrzymać). Po 24h Aegis ulegnie samozniszczeniu, Skażając niemały obszar.

Janek dostarczył Sabinie i Pawłowi działo strumieniowe zdolne do zniszczenia Aegis. Oczywiście, każdy wystrzał też powoduje Skażenie. Zapytany o pochodzenie Aegis odparł, że to był ostatni eksperyment Setonów - połączenie umiejętności Roberta i Weroniki. Aderialith w wersji light, transorganik zasilany Czarnym Kryształem. Oczywiście, Aegis nie jest tym modelem który został stworzony przez Setonów; GS "Aegis" to jego uproszczona i słabsza forma. Drugi z listy modeli produkcyjnych. Wiemy, że działo strumieniowe potrafi zniszczyć GS Aegis, bo sprawdzono to na modelu 00001.

Aegis 00002 powinien posiadać szczątkową osobowość oraz wyraźne dyrektywy ochrony magów KADEMU, magów i ludzi w tej kolejności. Problem polega na tym, że niszcząc sobie kontroler musiał zniszczyć też friend-or-foe detector. Innymi słowy, nie wie z kim ma do czynienia. I na pewno jest świadomy, że się rozpada. Czyli wie, że ma ograniczony czas. W idealnym świecie Aegis powinien oddalić się gdzieś na ubocze. Z jakiegoś powodu jeżeli ten tego nie robi to znaczy że jest jeszcze gorzej niż się wydawało.

Jakieś pół godziny przed utratą 00002 zarejestrowano "dziwny" rozbłysk magiczny na tamtym terenie. Niestety, rozbłysk został przekazany przez skanery 00002, a 00002 zwyczajnie nia ma detektorów na dokładną analizę aury i poziomów magicznych.

Cel: odnaleźć Mariana Łajdaka, unieszkodliwić GS Aegis 00002. 00002 był bardzo przywiązany do Mariana; jak pies. Jego osobowość objawiała się tym, że chronił z ogromnym poświęceniem i wzdragał się przed atakowaniem jeśli nie był całkowicie pewny. 00001 był agresywny; 00002 był bardzo ostrożny. I dziwne jest to dlaczego w ogóle to się stało. Ze strony Janka dostali: działo strumieniowe (wielkość: mała bazooka) oraz autonomiczną platformę skanującą  APS 'VISOR'. I ruszyli.

Już jak dotarli pod Koty poczuli dziwne uczucie ciężkości w głowach. Trudno zebrać myśli. Apatyczno - rozleniwiające uczucie. Spokojnie, nic się nie dzieje... można iść spać... Są świadomi tego, że to efekt działania 00002, bo jest to zgodne z jedną z procedur głowicy. Ale to powoduje bardzo duże zużycie energii i jeszcze większe osłabienie 00002. I ta aura jest dowodem, że 00002 został w okolicy, mimo, że generuje Skażenie.

Sabina i Paweł zorientowali się, że aura nie jest "ogólna". Jest kierowana. Paweł użył swojego analitycznego umysłu wspierany przez intuicję Sabiny by dojść do tego o co tu chodzi. Tak skupili się na tych działaniach (4v5->5), że aura na nich wpłynęła i zapadli w częściowy letarg. Aura działa jako "nothing to see here, move along". Jest tam też ukryta wiadomość specjalnie dla magów. Wiadomość "kwarantanna", powtarzana w nieskończoność.
Rozważania i letarg przerwało pukanie w szybkę. Młoda kobieta w sportowym stroju (Marysia Kiras) pyta, czy nie pomóc w czymś, czy się zgubili. Jako, że na tylnym siedzeniu jest VISOR, Sabina szybko zełgała, że są z jakiegoś przedsiębiorstwa gospodarki wodnej. Powiedzieli, że nie mają miejsca do spania bo firma zawiodła. Marysia powiedziała, że nie ma problemu - jest tu gospodarstwo które wynajmuje agroturystyczne wakacje (nie w tej porze roku), ale się z pewnością podzielą miejscem. Podała adres.

Rozmowę przerwała seria strzałów z broni maszynowej w oddali. Marysia przełamała efekt uspokojenia i pobiegła gdzieś (nie w kierunku na serię). Paweł i Sabina się rozdzielili - Sabina pobiegła za Marysią a Paweł wziął działo strumieniowe i VISOR, po czym poszedł w kierunku strzałów, do lasu...

Marysia naprawdę szybko biega, jednak Sabina jest w stanie dotrzymać jej kroku i pozostać niezauważoną (7v7->8). Marysia wbiegła do domu kultury. Podczas biegu Sabina zorientowała się o czymś dziwnym - Marysia wygląda na osobę która zna się na walce wręcz. Zmieniła swój wygląd i weszła do środka domu kultury. Główna sala (kilka starszych osób) i sześć pokoi. Nie ma nigdzie Marysi. Na jednym z pokoi pisze "książki", na innym "dojo". Zgodnie z tablicą ogłoszeń - w dojo nigdy nie ma zajęć. Są za to pokazy które idą według wzoru "mistrz i uczeń" - Marysia i Robert. Mistrzynią jest Marysia. Pokazy zawierają między innymi walkę tonfą.

Sabina się wycofała by zajrzeć i zobaczyć co tam się dzieje. (7v3->auto) Bez kłopotu, Marysia przygotowała bolasy i tonfę, wzięła je do plecaka i wychodzi. Sabina śledzi ją dalej.

Tymczasem Paweł. Szukał GS Aegis 00002, znalazł trupa miślęga z odstrzeloną lewą stroną głowy. Poszukał jeszcze trochę (słyszał trzy serie) i znalazł dwa następne trupy. Zginęły w dokładnie ten sam sposób, od tej samej broni. Wszystkie trzy ciała miały odstrzeloną lewą część głowy. Paweł zauważył też, że niedaleko ciał miślęgów znalazł ślady po pochodni plazmowej. 00002 spalił kawałki odstrzelonej przez siebie tkanki (Spustoszenie).

Skaner VISORa pokazał obecność 00002. Aegis zbliża się do pozycji Pawła. Paweł przesunął się na bok i obserwował VISOR by sprawdzić, czy Aegis go widzi i czy idzie w kierunku na niego czy zwłok miślęgów. Aegis się zawahał, ale skierował się na zwłoki miślęgów. Paweł skorzystał z okazji, wykorzystał VISOR jako podpórkę pod działo strumieniowe i wystrzelił wykorzystując VISOR jako celownik. (10#4v11-> 12) Aegis został trafiony i wiązka uszkodziła mu skanery, pozbawiła go chwilowo kontroli sytuacyjnej i motoryki ale co najważniejsze poważnie uszkodziła Czarny Diament. Aegis nie wybuchnie. Za kilkanaście godzin zgaśnie, bez źródła zasilania. Aegis dał radę się jednak wycofać, zostawiając ciała miślęgów Pawłowi.

To zmienia sytuację. GS Aegis 00002 jest bardzo ciężko uszkodzony. Jednak misja transorganika pozostaje bez zmian - chronić wszystkich tak długo jak to tylko możliwe. Transorganik nie pamięta procedur, nie pamięta z kim się może skontaktować, nie pamięta co i jak powinien zrobić i po czyjej jest stronie, ale nie pozwoli na Spustoszenie nikogo innego. Czas działać na poważnie, czas zacząć zabijać wszystko co jest Spustoszone. Musi dorwać magów. Ale nie ma friend-or-foe, nie rozpoznaje OSÓB, rozpoznaje jedynie lokalizacje. Czyli nie wie czy osoba jest magiem czy człowiekiem. Nie wie czy już widział osobę czy nie. To co jednak umie to wykryć Spustoszenie.
00002 odpalił flarę "krytyczne niebezpieczeństwo" i "kwarantanna". Kolejna flara to "terminus down".

Paweł zostaje z VISORem koło ciał miślęgów i próbuje je przeanalizować. Je i wypalone kawałki ziemi dookoła. Nie wie co tu się dzieje, nie wie o co tu tak naprawdę chodzi i kluczowe jest to, żeby się tego dowiedzieć. Poświęci tyle czasu ile trzeba, by poznać prawdę.
Tymczasem Marysia wyszła z domu kultury i idąc BARDZO ostrożnie poszła do jakiegoś domu. Poszła zobaczyć, czy wszystko w porządku z Eleonorą. Sabina zostaje obserwować dom do którego weszła Marysia (hint: to dom jej przyjaciół którzy wynajmują go, przyjęli z dobrego serca Eleonorę w malignie). 

Tymczasem sygnały 00002 zostały też przechwycone przez magów Spustoszonych a "terminus down" został zidentyfikowany przez Estrellę. W związku z tym Spustoszenie mając czterech magów do dyspozycji zdecydowały się na bardziej zaawansowany plan - wysłało Netherię by przechwyciła kontrolę nad Eleonorą a Estrellę by była w odwodach.
Sabina pilnując domu do którego weszła Marysia zobaczyła zbliżającą się tam osobę. (10v7->8) Zidentyfikowała ją jako Netherię Diakon, czarodziejkę - bizneswoman która powoduje, że imprezy i działania są EPICKIE. Osobę, która nie wchodzi w relacje ze światem ludzi. A tu nagle wchodzi. Wysłała jej lekki ping magiczny "porozmawiajmy". 
Spustoszenie przenosi się po kanałach magicznych.
00002 jest tego świadomy.
00002 postawił ekrany ochronne na wszystkich Spustoszonych magach. Trzyma bariery na sobie.
Ping trafił w 00002. 00002 zidentyfikował lokalizację Netherii.

Teleportował się tam, prosto za plecy Netherii (jaka Maskarada? Kolejna zniszczona zasada) i uderzył mocno, uderzając wiązką elektryczną dłonią. Obezwładnił Netherię i uszkodził jej projektor ochronny.
Tymczasem Paweł dokonał postępu. Wykorzystując VISOR jako umiejętność (6#2#2v11->12) zidentyfikował zagrożenie jako Spustoszenie. Miślęgi były Spustoszone.
Sabina zobaczyła, że Spustoszenie ogarnęło dłoń 00002, w związku z czym 00002 odłączył dłoń od reszty ciała, po czym pochodnią plazmową stopił swoją część ciała.
Paweł biegnie z działem w kierunku Sabiny.

Sabina nie wie co zrobić. Nie wie jakie są intencję 00002. Nie wie, czy Netheria to przeżyje. Ale wie jedno - zaraz najpewniej z domu wyjdzie Marysia.
Biegnie zatem w tamtym kierunku. I faktycznie - drzwi się otworzyły, wyszła Marysia i zobaczyła uszkodzonego 00002 z nieaktywnym projektorem iluzji. Spanikowała i zaatakowała tonfami. 00002 nie atakuje, rozpoznaje organizm który nie jest Spustoszony więc się wycofuje. Jednocześnie identyfikuje czarodziejkę jaka wysłała mu pinga w Sabinie, więc stawia na niej tarczę przekierowującą by nie dało jej się Spustoszyć (co odcięło komunikację Sabina - Paweł). Tymczasem znajdująca się w odwodach Estrella zakumulowała energię i wystrzeliła w 00002 wiązkę plazmy. 00002 zasłonił Marysię, więc plazma zamiast Marysię stopić, wiązka ją jedynie poparzyła.

Marysia, ranna, wycofała się do domu i zamknęła drzwi.

GS Aegis 00002 wykrywa - Eleonora (mag) w domu, Netheria (Spustoszona) na ziemi, Estrella (Spustoszony terminus) na dachu, Sabina (mag) w samochodzie. Aegis nie jest w stanie się wycofać bo straci Eleonorę.

GS Aegis 00002 skierował się ku Estrelli i wystrzelił wiązkę pocisków swarm w terminuskę licząc na wymuszenie wycofania się i zniszczenie czarodziejki Spustoszenia.
Sabina rzuciła zaklęcie na najbliższy samochód by go szybko przejąć (dając się zidentyfikować jako mag Spustoszeniu) i podjechała do 00002. Potrąciła 00002 samochodem by uratować ludzi przed pociskami swarm, po czym rzuca się do Netherii by ją wycofać z konfliktu. (7v7->9) Sabina wycofała Netherię i pojechała z piskiem opon. Aegis próbował ją zatrzymać, ale Estrella zaatakowała i osłaniała Sabinę - jeśli ta spróbuje rzucić JAKIEKOLWIEK zaklęcie na Netherię, zarazi się przecież Spustoszeniem.

Na to wyjechał na VISORze Paweł. Eksplozje na niebie gdy pociski swarm detonowały. Paweł widział, że pociski celowane były w terminuskę na dachu (z ludźmi w środku). Jedzie szybko samochodem w jego kierunku Sabina. Paweł zatrzymał VISORa, wycelował działo strumieniowe i strzelił w 00002 (11v9->13). Wiązka stopiła Czarny Diament i transorganik został zniszczony.
Połączenie Sabina - Paweł zostało przywrócone gdy tarcze przekierowujące transorganika padły wraz z samym transorganikiem.

Spustoszenie jest poinformowane o zniszczeniu transorganika. Uruchomiło miślęgi - mają zlokalizować i w miarę możliwości uwolnić Mariana Łajdaka. Estrella dostaje zadanie przechwycenia Pawła i Sabiny. Netheria jest nieprzytomna. Spustoszenie próbuje przechwycić Koty. Oczywiście, wraz ze zniszczeniem 00002 efekt "nothing happened here" przestał istnieć. 

Sabina zgarnia Pawła. I mają problem - terminuska mówi, że poinformowała SŚ hipernetem o obecności hipernetu i obszar jest pod pełną kwarantanną.
A magowie KADEMu nie mają tego jak zweryfikować.
I co gorsza, jeśli zadzwonią, Spustoszenie przejmie kontrolę nad rozmową telefoniczną i będzie udawało Janka. A jeśli Marian był przejęty, to będzie też wiedziało to co Janek. Aha, a jeśli magowie KADEMu się rozdzielą i jedno z nich opuści ten teren (a terminuska jest Spustoszona), terminuska może zażądać np. zdjęcia maga przez innych terminusów ponieważ <random reason>. Super.
Teraz jak nie ma barier zapewnianych przez GS Aegis 00002, każde zaklęcie rzucone przez czy na maga Spustoszonego może zarazić Spustoszeniem.

W związku z tym jest tylko jedna "bezpieczna" metoda jaka im zostaje. Sabina zatrzymała terminuskę rozmową a w tym czasie Paweł zmodyfikował działo strumieniowe. Wystrzeliło wysoko w powietrze i eksplodowało Skażając teren i wysyłając zakodowany sygnał "Spustoszenie".

I tak naprawdę na tym się skończyło.

EPILOG:

- Marysia została wyleczona przez KADEM.
- Wyczyszczono pamięć i naprawiono Maskaradę.
- Projekt Aegis został zarzucony; uznano za zbyt niestabilny i niebezpieczny.

# Streszczenie

Kilku magów Świecy i Millennium pojawiło się w Kotach by zrobić imprezę co skończyło się tym, że... w Kotach pojawiło się Spustoszenie. GS Aegis 0002 tam też się znajduje i jako transorganik uległ częściowemu Spustoszeniu; walczy jednak by ochronić wszystkich i odcina stopniowo z siebie Spustoszone komponenty. Niestety, stanął przed trudnym dylematem; zabić ludzi czy pozwolić magowi zginąć. Zanim 0002 odpalił rakiety, zniszczył go Paweł odpowiednim działem. Siluria wysłała sygnał alarmowy o potencjalnym Spustoszeniu zanim i oni zostali Spustoszeni. Sytuacja uratowana.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                            1. Instytut Technomancji
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Dom kultury
                 1. Dwie hałdy
                     1. Dom przechodni przyjaciół Marysi
                 1. Las Stu Kotów
                     1. Kopiec Miślęgów

# Pytania:

Ż - Czego potężnego pierwotnie szukał Olaf na tym terenie?
B - Przygotowanego przedtem dla kralotha miejsca mocy.
Ż - Skąd miał tak potężne źródło energii?
K - Od skonfliktowanej gildii.
Ż - Dlaczego doszło do rezonansu magicznego?
B - Dwie strony próbowały skorzystać z tego miejsca mocy nie wiedząc o sobie równocześnie.
Ż - Dlaczego jeszcze nie doszło do złamania Maskarady?
K - Bo miejsce mocy jest głęboko w lesie.
Ż - Co robili tu trzy czarodziejki które nie mają powodu tu być?
B - Bachelor's party gone wrong. (wieczór panieński).
Ż - Na jakie korzyści liczyła ta tajemnicza skonfliktowana gildia?
K - Tajemnicza gildia liczyła na złapanie i zdominowanie kralotha.
Ż - Jaki los czekał ojca zaginiętej nastolatki który szukał jej na własną rękę?
B - Zespolenie.
K - Dlaczego w ogóle KADEM się w to miesza?
Ż - Bo to oni utracili głowicę bojową (GS "Aegis") która miała test drive na tym terenie. Wraz z kontrolerem.
B - Dlaczego Janek kieruje tą operacją nie nadzorowany?
Ż - Bo zdaniem Quasar to jest zbyt niebezpieczne by w ogóle ruszać głowice klasy GS bez Setona.
K - Co dokładnie stało się magowi SŚ który chciał skrzywdzić ludzi?
B - Myśli, że jest człowiekiem.
B - Jaką formę przybrała symbioza między ludźmi a Skażeniem w lesie?
K - Ludzie nie będąc tego świadomymi są chronieni przez magię ale podtrzymują to pole.
Ż - Podaj znaczącą lokację znajdującą się w Kotach
B - Kościół, który ma kryptę.
Ż - Dlaczego krypta została dobrze zabezpieczona przez ludzi?
K - To stare zabezpieczenia. Z uwagi na legendy w Kotach.
Ż - Podaj znaczącą osobę znajdującą się w Kotach.
K - Dziennikarka na wakacjach która już kiedyś otarła się o magię, choć tego nie wie.
Ż - Jaka jest agenda tej dziennikarki?
B - Podejrzewa o coś Eleonorę Wiaderską.

# Stakes:

- Przyszłość projektu Aegis.

# Idea

- "Did anyone know the hostess' name?"

# Zasługi

* mag: Paweł Sępiak jako mag KADEMu który bezbłędnie posługuje się działem strumieniowym.
* mag: Siluria Diakon  jako mag KADEMu która podejmuje ogromne ryzyko by nikt nie zginął.
* czł: Marysia Kiras jako sensei "Tarczy" kiedyś uczona przez Irenę, organizacji dbającej o Koty.
* czł: Robert Mięk jako uczeń "Tarczy", który występuje tylko na plakacie.
* czł: Adam Lisek jako nastolatek który zniknął w lesie. Pod ochroną GS Aegis 00002.
* czł: Basia Morocz jako nastolatka która zniknęła w lesie. Pod ochroną GS Aegis 00002.
* vic: GS "Aegis" 0002 jako bardzo uszkodzony i Spustoszony GS walczący ze Spustoszeniem wszystkimi posiadanymi przez siebie środkami.
* mag: Marian Łajdak jako KADEMowy kontroler GS "Aegis" który został Spustoszony.
* mag: Netheria Diakon jako Mistrzyni Ceremonii; nekromantka i Diakonka do wynajęcia (kanonicznie: SŚ) chcąca wprowadzić Imprezę Życia. Spustoszona.
* mag: Estrella Diakon jako terminuska i przyjaciółka Netherii. Srebrna Świeca, ale jest tu prywatnie. Chciała chronić przed sytuację, ale została Spustoszona.
* mag: Eleonora Wiaderska jako przyszła panna młoda która dawno temu uczestniczyła w akcji w Kotach (patrz "Sojusz z Piekła Rodem"). Z SŚ. Chwilowo w malignie, myśli że jest człowiekiem; chroniona przez Marysię.
* mag: Olaf Rajczak jako drugi mag rezonujący ze źródłem który obudził Spustoszenie.

# Dark Future:

- Dwóch ze Spustoszonych magów ginie broniąc Rdzenia Spustoszenia.
- Aegis 00002 zostaje zniszczony. Samozniszczenie niszczy mały połać lasu i Skaża obszar. KADEM smutny.
- Projekt Aegis zostaje całkowicie zarzucony, zostaje jedynie prototypowy GS stworzony przez Setonów.

# Czas:

* Opóźnienie: 14 dni
* Dni: 1 dzień

# Wątki

- Trzeci kontroler Spustoszenia
- Aegis czy EIS?