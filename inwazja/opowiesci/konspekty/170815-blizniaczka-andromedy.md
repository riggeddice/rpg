---
layout: inwazja-konspekt
title: "Bliźniaczka Andromedy"
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160908 - Zabójczy spadek (An, temp)](160908-zabojczy-spadek.html)

### Chronologiczna

* [160908 - Zabójczy spadek (An, temp)](160908-zabojczy-spadek.html)

## Kontekst ogólny sytuacji

-

## Punkt zerowy:

Pytania MG do siebie:

* Czy Bliźniaczka przetrwa czy stanie się jedną z Czterech? Kto stanie się Czwartą jak nie ona?
* Czy August przetrwa to w sposób w miarę bezbolesny?

Koncept:

* Głód, nieskończona potrzeba adoracji
* Alfons pimpujący swoje dziewczyny by odnaleźć Zło
* Wróżda, chroniąca przed Potworem
* Bliźniaczka, która nie chce się stać Andromedą

**Stan początkowy**:

1. Los Manfreda
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula:                0/10
    1. (MI) Manfred się wyrywa, acz jego moc jest osłabiona:                            0/10
    1. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem:    0/10
    1. (KB) Bliźniaczka i Katarzyna tworzą nowy gestalt jak Andromeda <WAGA 2>:         0/10
    1. (KB) Bliźniaczka i Katarzyna tworzą nowy gestalt jak Andromeda <WAGA 2>:         0/10
    1. (I)  Iliusitius doprowadza wszystko pod kontrolę - wszyscy wyczyszczeni:         0/10
1. Niszczyciel potwora
    1. (K)  Katarzyna jest adorowana a siły Czepca są odepchnięte / zniszczone:         0/10
    1. (C)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia:               0/10
    1. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi:          0/10
    1. (B)  Bliźniaczka znajduje sposób jak nie stać się częścią Wróżdy i być wolną:    0/10
    1. (AC) August znajduje i usuwa Bliźniaczkę jako byt nienaturalny:                  0/10
    1. (KB) Osłabienie Wróżdy przez ratowanie Dziewczyn na tym terenie:                 0/10

**Pytania i odpowiedzi**

* Ż: Czemu mag potrzebuje wsparcia lokalnej potężnej czarodziejki i to takiego dyskretnego, by nikt się nie dowiedział?
* K: Ma już kłopoty i wierzy, że ona jest w stanie pomóc mu rozwiązać ten konkretny problem.
* Ż: Jakie kłopoty?
* K: Jego aktualny szef go koci i Manfred już po prostu sobie nie radzi.
* Ż: Jaki jest biznes w okolicy? Coś co daje naprawdę sensowne pieniądze?
* K: Browar nie do końca niszowy 'Dumny Hufiec'.
* Ż: Co zatem w Dumnej Strażnicy robi Andromeda i jak jest to powiązane z poprzednią sesją?
* K: Andromeda znalazła plotkę czegoś ciekawego podczas ostatniej akcji; coś co świadczy, że ta cysterna nie była przypadkowa. No i Samira jest zajęta występami - więc jej nie ma.
* K: Andromeda przyjechała do domku kempingowego.

## Misja właściwa:

**Dzień 1:**

_Rano (+1)_:

Andromeda jest w niedużym, acz uroczym miasteczku Dumnej Strażnicy - tam ją zaprowadziły ślady. Dwa dni później miała potwierdzenie, że problemu w sumie NIE MA. Ale znalazła coś innego. Coś, co świadczy, że w okolicy jest jakiś potwór. Oznakami było to, że ludzie, co wchodzili do lasku wracali nie do końca pamiętając co się tam działo. Nie są bardzo skrzywdzeni, ale nie pamiętają i są osłabieni. Normalnie z tego się leczą. Ale są osłabieni i zapadają potem na choroby. By pogłębić temat, Andromeda przeszła się po miejscowych knajpkach, by zebrać ploteczki.

Andromeda zobaczyła... siebie. Kogoś, kto wygląda i porusza się zupełnie jak ona. Idzie ostrożnie, unikając... kogoś. I ma na sobie dobre ubranie do kamuflażu; takie hoodie. Najpewniej zwinięte komuś młodszemu i mniejszemu. Andromeda CZUJE, że coś jest bardzo nie tak z tą osobą...

Andromeda błyskawicznie zmieniła posturę i zachowanie; założyła okulary (zerówki). Nie chce wyglądać tak, jak ona. I zaczyna śledzić swojego doppelgangera. Ma też przy sobie i sól i srebro... Andromeda chce być niezauważona i śledzić cel. Przecharakteryzowała się (1), są ludzie i się wszystko rusza (1). Ona jest bardzo ostrożna (-1). 2v3->F. Doppelganger spojrzała na Andromedę, rozpoznała ją. Oczy przerażone i - w długą. A Andromeda za nią.

Żadna z nich nie jest w stanie sensownie uciekać czy gonić. Żadna nie ma wprawy. NvN->SS (+1). Tajemnicza dama skręciła w uliczkę bez wyjścia. Nie ma jak uciec. Odwróciła się więc i przybrała pozycję bojową, ostrzegając Andromedę, żeby ta dała jej wolną drogę - lub ta ją pobije. Ale Andromeda widzi, że tamta tak samo jak ona nie umie walczyć... lokalny pijaczek siedzący sobie jedynie skomentował to dobrym "kurwa". Tamta się doń zwróciła - niech pijaczek jej pomoże, da mu 200 zł. Ona po prostu chce uciec. Andromeda skomentowała zimno, że doppelganger nie ma 200 zł i zaczęła rozsypywać sól. I pojechała "tak mnie nienawidzisz, żeby wpędzać mnie w kłopoty, siostrzyczko" (+2). Sukces. Postronni zdecydowali się olać temat jako kłótnię między siostrami.

Tamta widząc, że sytuacja nie jest po jej stronie zaczęła próbować negocjować. Nie chce TAM wracać, do NICH. Nie chce stać się Andromedą. Andromeda zauważyła, że nie wie kim jest nieznajoma i musi się dowiedzieć. Z rozmowy wyszło, że tamta też nazywa się Kasia, ale Nieborak. I mówi coś, czego Andromeda nie rejestruje - czego nie jest w stanie usłyszeć. W końcu po pewnej chwili Andromeda zauważyła, że jeśli ona należy do NICH to tamta jest już złapana. I niech Kasia idzie z Andromedą. Kasia zrezygnowana postanowiła pójść. Stwierdziła, że ONI wiedzą jak ona myśli. Kasia boi się hasła aktywującego.

Andromeda wpadła na pomysł. Zakłada, że ONI wiedzą jak Andromeda myśli - więc potrzebuje rng. Innych systemów decyzyjnych. Więc zadzwoniła do znajomego studenta, któremu kiedyś robiła kawałki sztuki do pracy magisterskiej a poznała go jako barmanka. Krzyśka Jarzolina. Spytała GDZIE może się schować na kilka godzin. Krzysiek zaproponował... Escape Room. Jako bonus - na pewno Kasia nie ma też kasy na coś takiego... Więc: Nie Jest Sama (1), Inne Miejsce (2), Skala (-1), Kompetentny (-1) = 6v4->S. Druga Strona do jutra ich nie znajdzie.

_Południe (+1)_:

Kasia i Kasia siedzą w Escape Roomie i rozmawiają. Nie próbują się wydostać. Kasia opowiada Andromedzie, że pamięta lustro. Z lustra "wyszła" i dlatego jest. Pamięta, że jest prostytutką, ale... nie jest to prawda. To, że pamięta, to nie jest prawda. Nowa teoria Kasi - Andromeda jest jedną z NICH, ale się wyrwała. Tak jak ona sama. Ale ona ma hasło kontrolne które ją overriduje. Lustro może ją wyciągnąć, ale...

Na prośbę Andromedy, Kasia naszkicowała "ich" twarze. Jedna twarz Andromedę uderzyła - to człowiek, którego czasem widziała w rezydencji swojego Mecenasa. Czyżby to wszystko była pułapka na Andromedę? Tak czy inaczej, Andromeda jeszcze dotknęła Kasię srebrem - czysto. Kasia uważa, że istnieje maksymalnie od 3 miesięcy. Przedtem nie istniała. Wszystko przez to lustro. Cała jej historia jest fikcyjna. A teraz... ona ma za zadanie znaleźć Potwora. Nieskończony Głód Pragnący Miłości. Tu Andromeda lekko się zasępiła - jakkolwiek o niej nie mówić, ten opis do niej nie pasuje. Kasia jeszcze histerycznie dodała, że wtedy wszystkie cztery przyjdą i zrobią porządek. Ona będzie poświęcona. Zniszczona.

Andromeda jest lekko podłamana. Nic to, musi poznać ten kod kontrolny. Niech ktoś może overridować JEGO polecenia. Tego alfonsa. Ale... nie ma jak. Nie ma czym.

Drzwi do Escape Roomu zaczynają się otwierać. Powoli. Przed czasem. Andromeda kazała Kasi się chować - ona zajmie jej miejsce. Kasia podziękowała i się schowała. Dwóch silnych facetów, acz inteligentnych, weszło do środka. Andromeda zaczęła udawać Kasię - dali się nabrać (4v3->S). Pokazali Andromedzie strzykawkę - szef kazał nie robić jak nie będzie robiła kłopotów. Andromeda przytaknęła. (+1 Wolność Bliźniaczki).

_Wieczór (+1)_:

Podjechali do wynajętego domku na obrzeżach. Tam JEST ten gość od mecenasa - Czepiec. Zapytał zatroskany czemu Andromeda uciekła. Andromeda, udając Kasię, powiedziała, że "już nie chce". Czepiec się zdziwił. (-2 zna obie), (+1 no jak to Andromeda). 3v3->SS. W wyniku: Andromeda dowie się mniej więcej ale on zorientuje się że to nie Kasia. A raczej, nie ta Kasia.

Czepiec powiedział Andromedzie, że on poluje na potwory. Jego zdaniem, Andromeda też. Ta "Kasia" co uciekła miała być elementem Czwórki - czegoś, co zniszczy lokalnego potwora (Głód, nieskończona potęga adoracji). On nie jest magiem, jest członkiem grupy zwalczającej potwory i ma dostęp do różnych przedmiotów. Kiedyś - zdaniem Czepca - Andromeda przekazała mu dowodzenie i maskę, za pomocą której można usuwać "złe" wspomnienia ludziom. I twierdzi że Andromeda była mistrzynią w używaniu tej maski.

Oczywiście, Czepiec miesza prawdę z kłamstwem. Andromeda trochę się tego spodziewa, ale nie ma jak odsiać. Ale wie, że Czepiec mówi prawdę, gdy mówi, że jest po tej samej stronie co Andromeda (domyślnie: nie chce jej krzywdy i chce zwalczyć potwory) oraz że nie jest magiem i by go zniszczyli. Jest moralnie wątpliwy, ale jest po tej samej stronie.

Dał jej strzykawkę z czymś, przez co zapomni. Andromeda wiedząc, że NIE zapomina, wzięła. A co będzie...

_Noc (+1)_:

Andromeda smacznie śpi, Kasia ucieka.

1. Los Manfreda
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula: 3/10
    2. (MI) Manfred się wyrywa, acz jego moc jest osłabiona: 1/10
    3. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem: 2/10
    4. (KB) Bliźniaczka i Katarzyna tworzą nowy gestalt jak Andromeda <WAGA 2>: 0/10
    5. (KB) Bliźniaczka i Katarzyna tworzą nowy gestalt jak Andromeda <WAGA 2>: 0/10
    6. (I)  Iliusitius doprowadza wszystko pod kontrolę - wszyscy wyczyszczeni: 0/10
2. Niszczyciel potwora
    1. (K)  Katarzyna jest adorowana a siły Czepca są odepchnięte / zniszczone: 0/10
    2. (C)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia: 0/10
    3. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi: 2/10
    4. (B)  Bliźniaczka znajduje sposób jak nie stać się częścią Wróżdy i być wolną: 3/10
    5. (AC) August znajduje i usuwa Bliźniaczkę jako byt nienaturalny: 2/10
    6. (KB) Osłabienie Wróżdy przez ratowanie Dziewczyn na tym terenie: 0/10

**Podsumowanie torów**

-

**Interpretacja torów:**

-

# Progresja

# Streszczenie

Podążając za plotką o tym spadku i znikającej dziewczynie, Andromeda napotkała na swojego "klona". Przycisnęła dziewczynę i pomogła jej uciec; okazało się, że znajomy jej Mecenasa stoi za tym wszystkim. Opowiedział Andromedzie historię o tajnej organizacji ratującej ludzi przed siłami paranormalnymi. Men in Black. Której eks-członkiem jest Andromeda... Co jeśli jest prawdą oznacza, że Andromeda była kapłanką Iliusitiusa kiedyś?

# Zasługi

* czł: Kasia Nowak, pomogła uciec jednej z proto-Świec Iliusitiusa i swojemu "klonowi". Dowiedziała się czegoś o swojej przeszłości, acz nie wie, na ile to prawda.
* czł: Katarzyna Nieborak, "bliźniaczka" Andromedy; wie coś o przeszłości Andromedy, acz nie umie jej tego powiedzieć. Wybrała wolność i drogę autonomiczną. Nie różni jej to od oryginału.
* czł: Czesław Czepiec, znany Andromedzie "człowiek" jej Mecenasa; sprzedał Andromedzie wiarygodną acz najpewniej mało prawdziwą opowieść o jej przeszłości. Kapłan Iliusitiusa i specjalny agent.
* czł: Krzysztof Jarzolin, "studenciak" i znajomy Andromedy; wykorzystany by wymyślił coś, na co Czepiec nie wpadnie. I wymyślił - Escape Room.

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Chrobrego
                1. Strażnica Dumna
                    1. Browar Dumny Hufiec, znaczący biznes z którego żyje Strażnica Dumna
                    1. Kemping Wczasowicz, gdzie Andromeda mieszkała.
                    1. Escape Room Kosmiczna Kapsuła, gdzie Andromeda ze swoim "klonem" ukrywały się przed siłami Czepca

# Czas

* Opóźnienie: 5
* Dni: 2


# Waśnie


## Los Manfreda

### Zakres waśni

Manfred został pokonany i wchłonięty przez Katarzynę. Iliusitius nie ma wróżdy do Manfreda, ale nie zależy mu na jego uratowaniu. Katarzyna się z Manfredem zespoliła i go imprintowała.

### Strony z Motywacjami

* Manfred (M), który chciałby wrócić do normy i nie być kocony przez szefa
* Katarzyna (K), która chce być nieśmiertelna i czerpać z mocy Manfreda
* Iliusitius (I), który chce doprowadzić do balansu i zniszczyć Katarzynę
* Bliźniaczka (B), która chce się wyrwać i uciec - kosztem Katarzyny i Manfreda. Kosztem czegokolwiek.

### Ścieżki:

1. Los Manfreda
    1. (K)  Katarzyna reimprintuje się w Manfredzie, wzmacniając ghoula                0/10
    1. (MI) Manfred się wyrywa, acz jego moc jest osłabiona                            0/10
    1. (KM) Manfred przejmuje część mocy Katarzyny i jest wzmocniony, acz z ghoulem    0/10
    1. (KB) Bliźniaczka i Katarzyna tworzą nowy gestalt jak Andromeda <WAGA 2>         0/10
    1. (I)  Iliusitius doprowadza wszystko pod kontrolę - wszyscy wyczyszczeni         0/10


## Niszczyciel potwora

### Zakres waśni

August chce pokonać zło i rozwiązać problem z ewentualnymi klątwożytami w okolicy - chwała i sława. Katarzyna chce zasymilować młodego terminusa. Czesław chce, w imię Iliusitiusa, pokonać potwora i wprowadzić Wróżdę. Bliźniaczka nie chce stać się Wróżdą.

### Strony z Motywacjami

* August (A), który szuka potworów i chce pomóc lokalnej czarodziejce
* Czesław (C), który rozsiewa swoje 'damy' by znaleźć problem i usunąć Katarzynę - i chce stworzyć Wróżdę
* Bliźniaczka (B), która odda duszę diabłu, by nie służyć nikomu
* Katarzyna (K), która pragnie adoracji i asymilacji

### Ścieżki:

1. Niszczyciel potwora
    1. (K)  Katarzyna jest adorowana a siły Czepca są odepchnięte / zniszczone         0/10
    1. (C)  Wróżda się ukonstytuowała i dochodzi do ostatecznego starcia               0/10
    1. (AK) August znalazł lokalnego potwora i go pokonał dla chwały i potęgi          0/10
    1. (B)  Bliźniaczka znajduje sposób jak nie stać się częścią Wróżdy i być wolną    0/10
    1. (AC) August znajduje i usuwa Bliźniaczkę jako byt nienaturalny                  0/10
    1. (KB) Osłabienie Wróżdy przez ratowanie Dziewczyn na tym terenie                 0/10


## Naturalna Entropia

### Zakres Waśni

Normalne problemy w okolicy prowadzące do chaosu

### Siły z Motywacjami

* brak

### Ścieżki:

1. Naturalna Entropia
    1. Brak:    0/10

# Narzędzia MG

## Opis celu misji

* Nowa forma budowania Frontów, Ścieżek... przez Waśnie.

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
