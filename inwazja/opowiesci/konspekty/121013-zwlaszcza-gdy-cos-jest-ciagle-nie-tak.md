---
layout: inwazja-konspekt
title:  "Zwłaszcza, gdy coś jest ciągle nie tak "
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [120920 - Ale nie można zmusić go do jego rozwiązania... (An)](120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html)

### Chronologiczna

* [120920 - Ale nie można zmusić go do jego rozwiązania... (An)](120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html)

## Misja właściwa:

1. Rankiem następnego dnia Andromeda przygotowując śniadanie poczuła, że coś jest nie w porządku z jej ręką. Kiedy ją odwinęła i spojrzała na ciało, odniosła wrażenie, że to nie jej ręka. Zaskoczona, upuściła to, co akurat trzymała. Ściągnięty okrzykiem Andromedy terminus obejrzał jej dłoń (zaczynała się zaogniać) i zbeształ ją za to, że mu nie powiedziała o ranie wcześniej... Andromeda spojrzała jak na idiotę (widział od początku, że oszczędza dłoń i mówiła mu, że oparzyła się od srebra), ale nie skomentowała - trochę bała się tego, co działo się z jej ręką. Mag wyciągnął "języcznik"... -_-'. Cóż innego mag mógłby mieć jako środek opatrunkowy? W końcu nie na darmo mówi się o wylizywaniu ran, prawda..? 
Andromeda na razie nie powiedziała magowi o wrażeniu obcości własnej dłoni - nie chciała, żeby uznał ją z histeryczkę...
2. Pokonawszy niespodziewany opór ze strony Andromedy, którą brzydził język na kijku, przy użyciu języcznika i pęsety, mag oczyścił dłoń Andromedy.
3. Andromeda wyszła po zakupy. Wróciwszy zastała maga nad mikrofalą z rozsadzonym jajkiem w środku. Ledwo się opanowała i po kolejnym spięciu (po mikrofali wciąż przechodziły wyładowania - najwyraźniej magiczne, bo za każdym razem Andromeda czuła się gorzej) przygotowała magowi jajko bez fajerwerków. 
Co ciekawe, im bardziej się kłócili, tym większe wyładowania przechodziły przez mikrofalę. Zapytany, mag odburknął, że ma przepalone kanały magiczne i nie powinien się denerwować, bo inaczej magia sama będzie się uwalniać. Andromedę szlag trochę trafił, bo chłopak najwyraźniej oczekiwał, że będzie się koło niego skakać i mu usługiwać... Kiedy odmówił pomocy przy przygotowaniu obiadu, Andromeda na szybko zrobiła coś, co dało się przygotować jedną ręką... dla jednej osoby. Uznała, że nie będzie się przejmować naburmuszonym dzieciakiem.
Maga jeszcze bardziej to rozsierdziło - on przywykł do sług w domu mistrza. 
4. Przechodząc koło lustra, Andromeda kątem oka zobaczyła coś... dziwnego. Nie potrafiła powiedzieć, co to było, a kiedy spojrzała prosto w lustro, nie zobaczyła nic.
5. Aby załagodzić atmosferę, kiedy mag już trochę pogłodował i burczenie w jego brzuchu stawało się słyszalne, Andromeda postanowiła przygotować mu obiad (jajka sadzone z kiełbaską na ciepło i kawałkiem papryki ułożonymi w smiley'a). Mag ucieszył się i zaczął pałaszować.
6. Andromeda poczekała, aż terminus zje i postanowiła go zapytać, co może się z nią dziać. Powiedziała mu o wrażeniu obcości dłoni - powiedział, że srebro czasem może dawać taki efekt, ale naprawdę się wyraźnie zmartwił, choć starał się tego po sobie nie pokazać. 
Ponownie oczyścił dłoń Andromedy - na wszelki wypadek - i dokładnie ją obejrzał. Nie ucieszyła go też informacja o zwidach w lustrach. 
Terminus podejrzewał jakieś formy opętania, rozdarcia osobowości (związane z mandragorą) lub zaawansowanego rozpadu. Bez magii jednak niewiele mógł zrobić. Mógł tylko obserwować - zwłaszcza, że wezwanie jakiegokolwiek wsparcia, skończyłoby się skanowaniem pamięci Kasi (a terminus nie wie, że nic by nie przeczytali - bał się, że to zabije Kasię).
7. Andromeda zostawiła maga na moment tylko po to, żeby wróciwszy do pokoju zobaczyć, że używa języcznika do oczyszczenia talerza. Żywo zaprotestowała, czego terminus zupełnie nie zrozumiał. Andromeda uznała, że skoro tak bardzo nie chce myć naczyń, to może sobie jeść z tego konkretnego talerza i zabrała go, żeby odłożyć w osobne miejsce w kuchni. 
Niespodziewanie w talerzu zobaczyła obraz Samiry... Z zaskoczenia upuściła talerz...
8. Na początku terminus się lekko oburzył, ale uznał, że to kwestia języcznika. Jednak podsuwanie Andromedzie pod nos rzeczy pokrytych śluzem języcznika nic nie dało - poza wkurzeniem Andromedy, że ma kolejny wylizany talerz... Andromeda nie zobaczyła więcej wizerunków Samiry. Wtedy uznała, że może powinna pójść obejrzeć lustra, które mag zabrał Samirze (te, które przepchnął przez portal do pokoju Andromedy). 
9. Kiedy weszła do pokoju, Andromeda poczuła, że trzy konkretne lustra są... //jej//. Bliskie przebywanie w towarzystwie jednego konkretnego lustra oraz silne napromieniowanie magiczne doprowadziło do przejścia Bytu z Lustra ze stanu kamiennego snu do stanu niespokojnej drzemki. Z uwagi na powiązanie Bytu oraz Andromedy, wrażenie jej luster się pojawiło. Kasia zidentyfikowała swoje lustra i ich znaczenie. Jedno z nich to portal z beaconem powrotnym, jedno to bardzo skomplikowana broń - tak skomplikowana, że Andromeda nie ma pojęcia, jak jej użyć... nawet lepiej, bo i tak by nie chciała. Trzecie lustro jest... dziwne. Andromeda się go trochę boi (lustro z Bytem, choć ona o tym nie wie). 
Uznając, że najwyraźniej chodzi o Samirę i chcąc sprawdzić, czy zobaczy ją w jej lustrach, Andromeda nie  chce wykorzystywać tych, które uważa za swoje. Lustra Samiry Andromeda próbuje identyfikować na podstawie symboli na ramach. Na swoich lustrach zauważa, że symbole to tylko przybliżenie tego, co lustra robią. Zakłada, że tak samo jest w wypadku innych. 
Dlatego postanawia wybrać takie lustro, którego runy nie sugerują żadnego bezpośredniego niebezpieczeństwa - ostatecznie jej wybór pada na lustro do komunikacji.
10. Andromeda siada wygodnie i zagląda w lustro skupiając się na Samirze. W tej chwili nastąpiło połączenie z Bytem. Istota się obudziła i zastała zupełnie nie to, czego się spodziewała. Zastała niestabilną energetycznie Andromedę a jako "drugi element pary" terminusa zamiast Samiry. Rozespana istota spróbowała odpowiedzieć Kasi na jej pytania, ale nie potrafiła zebrać jednej osobowości i jednej myśli, co Kasia odebrała jako kakofonię głosów kłócących się ze sobą. 
Kasia nie zrozumiała z tego prawie nic poza odczuciem całkowitej obcości i że ta istota oczekiwała właśnie jej. Wcale się jej to nie spodobało. Kasia zapytała też o Samirę. Reakcją Bytu było zdziwienie, zwłaszcza w chwili, kiedy Kasia powiedziała, że nie jest Samirą. Miał poważne problemy z odróżnieniem Kasi od Samiry, ale w końcu Byt sam siebie przekonał, że Kasia i Samira nie są tą samą istotą. Kasia w tym momencie była już nieco przestraszona, ale nie odebrała wrażenia niebezpieczeństwa.
11. Terminus przerwał kontakt zasłaniając Kasi widok na lustro. Dziewczyna straciła przytomność. Terminus zaniósł ją do pokoju, położył na łóżku, przywiązał do łóżka pończochami, utoczył jej trochę krwi i przygotował ochronne runy mające ekranować i wypromieniowywać energię Kasi. Do tego celu użył też swojej krwi, żeby zwiększyć ilość dostępnej energii (wykończył się trochę) - terminus zna rytuały wykorzystujące krew nie będące Magią Krwi, choć wszystkie rytuały wykorzystujące krew są uważane za zakazane. Zaryzykował kanały magiczne i zaaranżował pokój dookoła lustra ochronnego, które położył na ziemi. Intencja była taka, by wypromieniować całość energii magicznej i energii, jaka może uderzyć w Kasię. 
Na swoje nieszczęście, jedynie wzmocnił aurę rezydualną (zamierzone), co doprowadziło do ustabilizowania Bytu i przebudzenia tej istoty.
Byt uznał, że dla bezpieczeństwa Kasi terminus musi zostać zniszczony bądź zniewolony.
12. Kasia śniła dziwne sny...
Obudziła się w ciemnym pokoju. Ściany obwieszone były symbolami ochronnymi... Z sąsiedniego pokoju dochodziła zielonkawa poświata. Nikogo nie było w pobliżu. Usiadła i dostrzegła pogryziony języcznik leżący na łóżku obok porwanych pończoch... Zawołała, ale nikt nie odpowiedział. 
Wstała, i po pierwszym kroku wbiła w stopę szkło. Wtedy zauważyła rozbite lustro ochronne leżące na podłodze...
Przeszła do pokoju, gdzie to groźne lustro pulsowało niezdrową poświatą... Nigdzie nie było maga. Jeden jedyny jego kapeć leżał przed lustrem. Czuła je. Wtedy pojawiła się upiorna Samira, która powiedziała, że tylko do tego muszą doprowadzić, i potem wszystko będzie dobrze. Kasia jednak wiedziała, że to nie jest Samira... 
Zażądała, żeby lustro wypluło maga. Sen zaczął się rozpadać. Byt nie potrafił przekazać Kasi tego, co chciał przekazać, nie umiał do niej trafić. Sen się rozpadł.
Kasia się obudziła. Pokój był taki, jak we śnie, tyle, że było jaśniej, a lustro na ziemi nie było rozbite. Na łóżku leżały porwane pończochy... Języcznika nie było. Chwilę po tym, jak się obudziła, wszedł terminus.
Powiedział Kasi, że ona jest potworem i że bardzo żałuje, ale musi ją zabić. Na pytanie, czemu nie we śnie, odparł, że musi być obudzona, by mógł trafić w "nią". Zaatakował, chcąc zadać czysty, bezbolesny cios. Nie spodziewał się, że Kasia będzie się bronić - potraktowała go stopą w splot słoneczny. Upadając, mag stłukł lustro na podłodze. Kasia poczuła ogromą ilość energii uwolnioną ze zwierciadła. Kasia zapragnęła pomocy. Upiorna Samira zamanifestowała się i wrzuciła maga w //to// lustro. Pozostał po nim jeden kapeć... 
Kasia odrzuciła wizję świata stworzoną przez Byt - ona nie chciała, by magowi stała się krzywda, chciała to wyjaśnić, zrozumieć, dotrzeć do tego, o co chodzi. Nie chciała, by stało mu się coś złego. 
Sen się rozsypał.
Kasia obudziła się po raz trzeci. Było jasno. Na ścianach wisiały wyrysowane krwią glify, a na łóżku leżały porwane pończochy. Na podłodze leżało lustro. Kasia odstawiła je pod ścianę. Po chwili wszedł do pokoju mag. Terminus wyglądał okropnie.
Podbite oko, zmęczony i ogólnie w kiepskiej kondycji.
Ucieszył się, kiedy zobaczył, że Kasia się obudziła.
Andromeda była trochę nieufna po ostatnim śnie, zwłaszcza widząc nóż u jego boku. 
Gdy Kasia spała, Byt dał radę pokonać terminusa (wyczerpany, z rozkrwawionymi kanałami) i prawie go zabił. Prawie zmiażdżył jego osobowość. Działania Kasi jednak sprawiły, że się wycofał. Jego celem nie było niszczyć magów. Terminus czuł, że coś jest nie tak, więc zaczął nosić przy sobie broń. 
13. Terminus wyciągnął "niuchacza" - nos na patyku mówiąc, że to urządzenie diagnostyczne... Zupełnie nie zrozumiał niedowierzającego spojrzenia Kasi. Jednak, jako że bycie obwąchiwaną jest co prawda nieco dziwne, ale nie obrzydliwe, jak bycie wylizywaną, Kasia nie miała szczególnych obiekcji, by dać mu się zbadać. Niuchacz nie wykrył niczego niezwykłego, nawet cienia skażenia - najwyraźniej wszystko zostało wypromieniowane. Faktycznie - Byt wyssał całość skażenia i zawarł je w sobie, zasilając się tym smakowitym źródłem energii. Jednocześnie, Byt zidentyfikował u siebie poziom niestabilności wynikający z niewłaściwego przebudzenia oraz z niespełnienia wszystkich warunków przebudzenia.
14. Kasia, nieco podejrzliwa z uwagi na swoje sny, zaproponowała terminusowi, żeby on też dał się wyniuchać. Sam na to nie wpadł. Zgodził się, ale zmartwiony jej stanem i wydarzeniami ostatniej nocy, zdecydował się najpierw zrobić jej śniadanie. Bardzo ją to rozczuliło... I ucieszyło. W końcu nie robi się śniadania komuś, kogo chce się zabić. Terminus powiedział jej, że sposób przeprowadzenia badania kalibruje nochala i podał, co i jak należy zrobić.
Kasia poszła do łazienki i tam, kiedy spojrzała w lustro, w jej umyśle pojawiła się inna sekwencja badania niuchaczem (kalibracji). Różniła się dość znacznie od wersji maga, ale nie tak, by nie dało się przypadkowo ich pomylić.
Kiedy poczuła się na siłach, Kasia zaoferowała pomoc w przygotowaniu śniadania. Poprosiła terminusa o pożyczenie noża - jego nóż był ostrzejszy niż cokolwiek, co w życiu widziała... Mag oddał nóż bez problemu, co przekonało Kasię o tym, że nie żywi wobec niej podejrzeń - gdyby uważał ją za zagrożenie, nigdy nie dałby jej do ręki tak groźnej broni.
15. Przez całe śniadanie Kasia zastanawiała się, z której sekwencji skorzystać. Jeszcze w łazience zdała sobie sprawę, że zawsze pamięć wracała jej po spojrzeniu sobie w oczy w lustrze. Nigdy nic, co sobie w taki sposób przypomniała, jej nie zaszkodziło. Dlatego też uznała, że skorzysta z sekwencji alternatywnej. 
Mag się nie zorientował. Ku zaskoczeniu Kasi, uznał, że wyniki były całkowicie normalne. 
Od tej pory to Kasia przestała ufać jemu - w końcu kalibracja nie była poprawna. Oznaczało to, że coś jest z nim nie tak i że jej wewnętrzny głos - jej specjalna pamięć, jej wyróżnik - po raz pierwszy dał jej coś, czego nie mogła pamiętać. Coś doskonale dostosowanego do sytuacji, a jednocześnie potencjalnie bardzo niebezpiecznego.
Nic jednak nie mogła na to poradzić. Cały dzień spędzili oglądając Batmana i Harrego Pottera. Mag pierwszy raz w życiu oglądał film.
Wieczorem położyli się spać. 
Istotny szczegół: Terminus zabrał wszystkie lustra z pokoju Kasi i rozstawił je w swoim, zasłonięte i taflami do ściany.
16. W nocy Andromedę obudziła rozmowa. Terminus, sterowany przez Byt (choć o tym nie wiedziała) rozmawiał z innym magiem, korzystając z lustra, w którym Byt był zamknięty. Rozmowa doprowadziła do napięcia stosunków z tamtym magiem (Byt powiedział to, co mag zawsze chciał powiedzieć, ale nigdy się na to nie zdobył). Dodatkowo, Byt posterował rozmową tak, aby terminus się ośmieszył. Mag po drugiej stronie już się nie martwił i zdecydował się nie wysyłać wsparcia. Dokładnie o to chodziło bytowi. Kasia znająca swojego terminusa, od razu widziała, że coś jest bardzo nie tak. Nie mogła jednak interweniować - gdyby czystym przypadkiem rozmowa okazała się prawdziwa, zmusiłaby maga do drastycznych kroków.
Kasia postanowiła wstać bardzo wcześnie i przebadać maga nochalem jego metodą. Nie potrafiła co prawda zinterpretować wskazań urządzenia, ale potrafiła ocenić, gdzie co się odchyla od normy - tyle jej pokazał. Nawet nazwał wskaźniki.
17. Jak zaplanowała, tak zrobiła. Choć bardzo starała się nie obudzić teminusa, nie do końca jej wyszło. Zdołała jednak zobaczyć wyniki i wyczyścić wskazania. Mag zdziwił się widząc ją nad sobą z nosem w ręku, ale zaakceptował wyjaśnienie, że po prostu była ciekawa urządzenia. Wyciągnęła z niego kilka informacji i zaniepokoiła się jego stanem. De facto sam powiedział jej, że takie wskaźniki, jakie uzyskał (nie wiedząc o tym) świadczą, że coś wysysa jego energię, silnie wpływa na jego percepcję, zniekształca jego wzorzec i wyniszcza jego kanały magiczne.
Tymczasem Byt coraz bardziej rozpada się w związku ze swoją wbudowaną niestabilnością. Udało mu się spowolnić proces rozpadu przerzucając jak najwięcej niestabilności na maga, lecz jest to tylko rozpaczliwe działanie doraźne. Jeżeli coś nie zostanie zrobione, w ciągu bardzo krótkiego czasu, wszystko z punktu widzenia Bytu będzie stracone. 
Terminus powiedział też Kasi, że ostateczna granica, po której zmiany będą nieodwracalne jest jeszcze stosunkowo daleko. Mają co najmniej kilka dni.
18. Kasia zapytała maga jak ma na imię. Okazało się, że chłopak nazywa się August.
Pamiętając, co zobaczyła w nocy, Andromeda próbuje wybadać, czy chłopak cokolwiek z tego pamięta. Mag dziwi się, że minęły dwa tygodnie, zbywa jednak pytanie czy nie musi dać komuś znać, uznając, że na pewno musiał się kontaktować. W końcu sami by go sprawdzili, gdyby tego nie zrobił. 
Mag powiedział jednak też Kasi, że tak naprawdę, nikomu na nim nie zależy. Kiedy Kasia zapytała o matkę, sam nie mógł uwierzyć, że o niej zapomniał.
Kasia zasugerowała, żeby wynieść lustra z mieszkania do piwnicy. Terminus nie oponował z jednym wyjątkiem - chciał, żeby lustro Bytu pozostało na miejscu. 
Kasia wówczas bardzo się zirytowała, rozpoznając wpływ Bytu na maga. Skorzystała z połączenia z lustrem i przesłała groźbę, że rozbije taflę, jeśli Byt nie zostawi maga w spokoju. Nie była to jednak groźba natychmiastowego rozbicia.
Byt przejął kontrolę nad magiem i jego ustami stwierdził, że jeśli Kasia to zrobi, to mag uzna ją za potwora i zabije.
Na razie Kasia postanowiła odpuścić - wciąż miała co najmniej kilka dni i poważnie rozważała skłonienie maga do wezwania wsparcia.
Tymczasem uznała lustro za potencjalnego wroga i zaczęła myśleć, jak wyprowadzić ten Byt w pole. Wpadła na pomysł, jednak do jego realizacji maga nie mogło być w pobliżu... I nie mogła się zabrać za to tego samego dnia.
19. Kasia wysłała maga po zakupy - musiała chwilę pomyśleć w samotności. Kiedy u drzwi usłyszała głos zarówno maga jak i Samiry, trochę spanikowała. Przez drzwi usłyszała, jak terminus mówi Samirze, że Andromeda odzyskała lustro.
Mocno przestraszona, Kasia sięgnęła do lustra przekazując jedną, prostą myśl - jeśli Byt skrzywdzi którekolwiek z nich, Kasia zniszczy lustro, nawet kosztem siebie. Ukłuła się w palec i zaczęła własną krwią rysować na lustrze blokujące glify.
Byt z lustra się jej ujawnił jako jej własne odbicie prosząc, by tego nie robiła i zaznaczył, że nie chce krzywdzić ani terminusa ani Samiry, ale musi dokończyć swojego dzieła. Powiedział, że Kasia i Samira będą zawsze rozdarte, jeśli Kasia go zablokuje. Na lustrze pojawiła się rysa. Kasia postanowiła zaryzykować. Wzmocniła jednak przekaz, że przy pierwszym objawie krzywdy któregokolwiek z dwójki za drzwiami, zniszczy lustro.
20. W chwili, kiedy mag i Samira weszli do mieszkania, lustro wybuchło, rozpryskując się na kawałki. Część poraniła Kasię, a cała trójka padła na ziemię porażona niesamowitym bólem. Wtedy z lustra wyszedł demoniczny pies. Mag resztką sił rzucił w niego swoim nożem, zabijając stworzenie. Magiczny sztylet pękł, a ból ustał. 
21. Byt przestał istnieć, rozproszył się, wykonując swoją misję. Kasia poważnie podejrzewała, że pies był tylko zmyłką, żeby terminus był szczęśliwy. Samira powiedziała, że znów jest "cała". Terminus po paru dniach odzyskał moc. 
Zanim wrócił do siebie, Kasia opowiedziała mu to, co podsłuchała w nocy. Uznała, że lepiej go ostrzec, że raczej nie będzie ciepło przyjęty...

Wszystko w miarę wróciło do normy... I tylko Kasia zaczyna rozważać przeprowadzkę.
Z Samirą.

----
**Fragment wyjaśnień maga skierowany do Kasi**

...Przerażające jest to, że ona miała aż tyle luster... Samira zaczy się. Lustra to forma, w której bardzo łatwo zawrzeć magię. Część mocy wykorzystywanej przez lustra do spełnienia swojej funkcji zawsze jest czerpana z osoby stojącej przed lustrem. Ale właśnie to jest powodem, dla którego lustra są tak niebezpieczną formą - moc czerpana z magów jest mocą skażoną, "pokolorowaną" emocjami czy osobowością. I na tym polega problem - lustra powinny być rzadkie i nie powinny być używane zbyt często. Stare lustra wielorazowego użytku potrafią odbić w sobie naprawdę dziwne rzeczy... 
Dlatego ona nie może dostać tych luster z powrotem.

# Zasługi

* czł: Kasia Nowak jako wybitna malarka z unikalną pamięcią, która po raz pierwszy zdaje sobie sprawę, że jest jakoś powiązana z lustrami
* czł: Samira Diakon jako wybitna iluzjonistka i NIE czarodziejka, która nareszcie jest znów całością
* mag: August Bankierz jako młody terminus pilnujący Andromedy, który wreszcie uzyskał imię i który stara się pomóc