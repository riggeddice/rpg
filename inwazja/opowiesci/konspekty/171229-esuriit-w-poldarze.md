---
layout: inwazja-konspekt
title:  "Esuriit w Półdarze"
campaign: wizja-dukata
gm: żółw
players: kić, agata_n, tomek_p
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171220 - Potrójna magitrownia Histogram](171220-potrojna-magitrownia-histogram.html)

### Chronologiczna

* [171220 - Potrójna magitrownia Histogram](171220-potrojna-magitrownia-histogram.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

-

### Wątki konsumowane:

### Lokalizacje:

Działki turystyczne, grille takie z Januszami, drewniane domki, dancbuda westernowa na tyłach ochotniczej straży pożarnej, połamane parasole, boisko do siatkówki bez siatki

## Punkt zerowy:

* Ż: Dlaczego nasza ofiara chciała pomóc chłopcu na wrzosowisku w lesie? Nazwij go.
* K: Krzysztof Tłuk. Bratankowi pomóc musi.
* Ż: W jaki sposób unikalne umiejętności Waszej trójki mogą pomóc w rozwiązaniu tego problemu?
* A: Każdy próbuje udowodnić innym silną stronę SWOICH racji - zaczynają się kłócić i argumentować. Siły paranormalne nie mają siły tego słuchać.
* Ż: Jak kiedyś się połączyliście w grupę ratowniczą?
* T: Kucharz jechał uberem bo się nawalił i gadali o paranormalnych i do Żakliny to doszło. Jagoda - kucharz zadzwonił kiedyś do Paktu i ona zrobiła PRAWDZIWY artykuł.

Krzysztof Tłuk szuka bratanka. Pokręcił się niedaleko harcerzy, gdzie powinien być chłopiec. NIe ma go. Znalazł jego latarkę. Chłopiec przerażony, stoi nad nim oczkodzik. Krzysztof go odstraszył. Następne co się stało, to przesunięcie go do Esuriit.

## Potencjalne pytania historyczne:

brak

## Misja właściwa:

**Dzień 1**:

Mariola. Krzysztofa nie ma. Mały Krzysiu jest w totalnej traumie i szoku. Dostała od Krzysztofa telefon "Pomocy zimno przyjdź".
Żaklina pojechała do Marioli. Zdetraumatyzowała Krzysia i zobaczyła jak jej narysował rysunek świni z oczami. Mnóstwo oczu. (Oczkodzik)
Zadzwonił Krzysztof znowu, pozornie z Esuriit. Jest mu zimno, jest sam. Nie wie co się z nim dzieje. Chce pomocy. Odebrała Żaklina - faktycznie coś się dzieje.
Żaklina zadzwoniła do Augusta. August zażądał 300 zł za krokodyla (sprawa z kiedyś). Mariola dała Żaklinie jak ta zażądała od niej 300.

August przyjechał. Dostał 300 zł, zapoznał się ze sprawą i oddzwonił na iphonie do Krzysztofa. Połączył się z Efemerydą Senesgradzką, która zainteresowała się tematem.
Efemeryda zaczęła wysyłać mackę energii. August się rozłączył zanim doszło do połączenia. Po chwili dał telefon Żaklinie, by sprawdzić co jest. Żaklina zaczęła być Skażana - August rozerwał połączenie. Lekkie Skażenie Żakliny. Efemeryda wzmocniła naturalne moce w okolicy - August stał się silniejszym medium, Żaklina bardziej chytra. A Jagoda jak przyjedzie - będzie antypryzmatem. Tymczasowo. Efemeryda eksperymentuje.

August poszedł do obozu harcerskiego się dowiedzieć co i jak, śledzi go Żaklina.
August dogadał się z druhem Bonifacym Jeżem, powiedział że szuka zaginionego człowieka i pogadał z druhem na warcie Wojtkiem Piekarzem. Wojtek powiedział o sytuacji z Krzysztofem wczoraj, że był i się awanturował, szukał dziecka. Wojtek coś ukrywa; August powiedział mu, że te dziwne rzeczy są prawdziwe i on mu wierzy. Wojtek pękł - widział świnię i widział, że facet zniknął. A Wojtek uciekł, nawet nie pomógł dziecku i tylko uciekł. August go uspokoił i powiedział, że takie traumy się zdarzają, niestety. Paranormalne jest prawdziwe. Ale ćśśś, nikomu nie mówić.

Jagoda przyjechała i sprawdziła licznikiem Geigera. Z mocy Efemerydy, zadziałało. Sceptycznej Jagodzie licznik tyka - to promieniowanie, dlatego Żaklina wymiotuje. To iphone napromieniowany. I znowu świat jest na swoim miejscu.
Jagoda dała numer z którego dzwonił Krzysztof (z Esuriit) swojemu młodemu koledze hakerowi Tadkowi. Haker powiedział, że nie może znaleźć z jakiego numeru - ale to super high tech. Nadajniki kierunkowe. Niech Jagoda dzwoni na JAKIKOLWIEK numer - jak będzie w odpowiednim miejscu to się połączy.

Poszli do domu Krzysztofa. Tam Żaklina przygarnęła odratowanego psa Krzysztofa, z jedną nóżką krótszą, imieniem Diobeł. Kochany pies Krzysztofa. A Jagoda dowiedziała się rzeczy na temat Krzysztofa (bonus mechaniczny). I August nie wykrył Krzysztofa - nie było go tu. Żaklina sprawdziła okolicę wśród swoich plotkarek - od czasu zniknięcia nikt nie widział Krzysztofa. Czyli faktycznie gość zniknął.

Jagoda zaczęła pisać artykuł do Paktu. Wyjdzie na świat - policja nic nie robi, świnie modyfikowane genetycznie... ogólnie, niech Ktoś Się Tym Zajmie.

Wzięli iphona i czapkę z daszkiem. I psa. Idą do lasu, TAM. W lesie jest już ciemno, bo wieczór. Pies nie umie znaleźć Krzysztofa.

August zauważył, że na drzewach jarzą się delikatnie purpurowe jakby płyny, tylko na martwym drzewie. I on powoli ścieka w dół. August się przestraszył i strach wzmocnił połączenie Primusa z Esuriit. Do Żakliny zadzwonił Krzysztof a przez ciągłą rozmowę zaczął tworzyć się korytarz.

W ziemi zaczął kopać pies, dostać się do Krzysztofa. Ziemia się osunęła i on tam jest. Chce wyjść. Żaklina złapała go za rękę by pomóc mu wyjść - on dotknął ją za nie-rękawiczkę i zaczął wysysać z niej energię Primusa i zastępować ją energią Esuriit; jest już Wyssańcem Esuriit, acz o tym nie wie. Nie jest ŚWIADOMY tego, co Esuriit z nim zrobiła, acz jest głodny...

Jagoda rzuciła w niego psem. Psa zawsze kochał a wygląda świadomie. Krzysztof puścił Żaklinę (odciągniętą szybko przez Augusta) i wpadł do dziury z psem. Pies zapiszczał i zmarł; wyssany przez Esuriit. Ilość energii pozwoliła Krzysztofowi przejść na Primusa i połączenie z Esuriit się częściowo urwało. Krzysztof zaczął płakać za martwym psem. Jagoda ściągnęła na siebie uwagę Krzysztofa i zaczęła go uspokajać a August przygotował najostrzejszy nóż kucharza. Trzeba to zabić.

Tymczasem Żaklina wzięła słoik i spróbowała doń nabrać trochę energii Esuriit ściekającej z drzewa. O dziwo, UDAŁO SIĘ ze wsparciem Efemerydy - powstał artefakt Esuriit.

Gdy Jagoda uspokoiła Krzysztofa i odwróciła jego uwagę, August zaszarżował i zabił Krzysztofa; nawet ciało Esuriit nie pomogło przeciw zdesperowanemu kucharzowi. Głowa odcięta. Jednak Faza Esuriit zaczęła się zbliżać na tej polance...
...i wtedy pojawił się oczkodzik i zaczął zeżerać energię Esuriit. Rozerwał Fazy. Na ziemi został martwy Krzysztof i Zespół. August... morderca?

**Epilog**:

* Pojawił się Kajetan Weiner i wyczyścił ślady. Jednak Efemeryda przywróciła pamięć Zespołowi z jakiegoś powodu.
* Słoik Esuriit zniknął; Efemeryda go gdzieś przeniosła. Gdzie i czemu? Nie wiadomo.

**Komentarz**:

* Żółw: wzięcie przez nich tego psa było genialnym posunięciem. Człowiek wyssany przez Wyssańca Esuriit, którym był Krzysztof by zwyczajnie został przekształcony i Krzysztof by myślał "pomogłem mu". Ale pies był za mały - zginął. To zrobiło wyłom w psychice nie do końca jeszcze przekształconego Wyssańca. Dzięki temu dało się go łatwo zabić, korzystając z szoku.
* Żółw: w historii nie widziałem artefaktu Esuriit. A zwłaszcza nie TAKIEGO, ze słoika. To było genialne.

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy
* 5: anulowanie wyniku konfliktu


Z sesji:

1. Żółw (11)
    1. Słoik Esuriit zniknął; jeszcze się na jakiejś sesji pojawi - jest do dyspozycji Senesgradzkiego Chaosu.
    1. Wylęgarnia Świń, należąca do Senesgradzkiego Chaosu ma nadal agenta na wolności - zmutowanego jakoś oczkodzika.
    1. Harcerze współpracują z Łowcami Znikających Świń.
1. Kić (3)
    1. Maria Newa: dostrzegła artykuł napisany przez Jagodę. Nie tylko zauważyła kompetentną (acz sfrustrowaną) reporterkę ale i potencjalny kontakt i znawczynię paranormalnego (sceptyczkę).
1. Agata (3)
    1. Dariusz Tłuk: zauważył Żaklinę i ona przestanie być chytrą babą. Wydobył z niej trochę dobra. Żaklina nie ma takiego chorego Mariuszowrzoda.
1. Tomek (3)
    1. Wojtek Piekarz: nie miał rodziny, został adoptowany przez Augusta i został jego padawanem w ufologii i racjonalizacji.

# Streszczenie

Po czarze Pauliny transformującym Daniela w częściowo nieumarłego, Efemeryda Senesgradzka zdecydowała się doprowadzić do przesunięcia człowieka w Fazę Esuriit. I faktycznie, powstał Wyssaniec Esuriit. Dzięki działaniom lokalnych ludzi (Augusta, Żakliny i Jagody), lekko wzmocnionych przez Efemerydę, Wyssaniec został zabity zanim do czegoś strasznego doszło. Półdara została ocalona i powstał Słoik nasycony energią Esuriit.

# Progresja

* Wojtek Piekarz: nie miał rodziny, został adoptowany przez Augusta Paszkwila i został jego padawanem w ufologii i racjonalizacji.
* August Paszkwil: Wojtek Paszkwil nie miał rodziny, więc August go adoptował i zmienił go w swego padawana w ufologii i racjonalizacji.
* Maria Newa: dostrzegła artykuł napisany przez Jagodę. Nie tylko zauważyła kompetentną (acz sfrustrowaną) reporterkę ale i potencjalny kontakt i znawczynię paranormalnego (sceptyczkę).

## Frakcji

* Senesgradzki Chaos: ma dostęp do Słoika Esuriit, o którym nadal nie wiemy co robi
* Senesgradzki Chaos: zmutowany oczkodzik na wolności nadal poszukuje... czegoś. Jest wolny i unika terminusów bez problemu.
* Łowcy Dziwnych Świń: dostali jako wsparcie harcerzy w okolicy Półdary; harcerze są całkiem przekonani, że dzieje się tu coś bardzo dziwnego.

# Zasługi

* czł: Żaklina Bąk, chytra baba z Półdary. Zrobiła artefaktyczny słoik Esuriit i zainicjowała operację 'znajdź Krzysztofa'. Tendencje do ryzyka i nieufności.
* czł: August Paszkwil, straumatyzowany kucharz nie z Półdary. Zainteresowany znalezieniem świni i gadaniem z harcerzami, zabił Krzysztofa (Wyssańca Esuriit).
* czł: Jagoda Kozak, sceptyczna reporterka Paktu. Ma kontakty z lokalsami; uratowała Żaklinę rzucając w Wyssańca Esuriit jego ukochanym psem.
* vic: Krzysztof Tłuk, szukał po lesie bratanka i go znalazł z oczkodzikiem. Przekształcony w Wyssańca Esuriit, zginął z ręki kucharza Augusta (KIA).
* czł: Krzysiu Miłżoś, 10-latek i syn Marioli; podglądał harcerzy i zobaczył oczkodzika. Uniknął przejścia na Esuriit czystym trafem; straumatyzowany i pomogła mu Żaklina.
* czł: Mariusz Tłuk, przegryw życiowy i chłopak (mąż?) Żakliny. Wiemy, że lubi siłownię i ćwiczenia fizyczne. Nie pracuje. Żaklina pracuje na niego.
* czł: Mariola Miłżoś, podkochująca się w Krzysztofie Tłuku. Gdy ów zniknął, była doń ostatnim ogniwem. Zainicjowała Żaklinę. Bardzo jej zależało by go znaleźć.
* czł: Bonifacy Jeż, harcerz, 18 lat; druh opiekujący się wszystkimi harcerzami w obozie w Półdarze. Pomocny i uczynny, chciał pomóc znaleźć zaginionego Krzysztofa.
* czł: Wojciech Piekarz, harcerz, 16 lat; był na warcie gdy Krzysztof szukał Krzysia i podglądał, gdy Krzysztofa przesunęło do Esuriit. August pomógł mu z poczuciem winy.
* czł: Tadek Swołczan, młody haker współpracujący z Jagodą Kozak; nie umiał zinterpretować telefonu z Efemerydy, więc wymyślił hipotezę zaawansowanych przekaźników telefonicznych.
* mag: Kajetan Weiner, pojawił się by posprzątać po pojawieniu się Wyssańca Esuriit na Primusie. Usunął ślady, dał alibi ludziom...
* vic: Efemeryda Senesgradzka, która zaintrygowana magią Pauliny w magitrowni doprowadziła do transformacji człowieka w Wyssańca Esuriit i połączyła się telefonicznie z ludźmi (oni dzwonili).

# Plany

* Żaklina Bąk: nie jest już 'chytrą babą'. Paranormalne istnieje a Dariusz się nią zainteresował; Mariusz nie jest wszystkim co czeka ją w przyszłości.
* Dariusz Tłuk: zainteresował się Żakliną Bąk; chce ją "uratować" od życia jakie ma teraz.
* Maria Newa: skontaktować się z Jagodą Kozak albo dowiedzieć się więcej o jej leadach i historiach; tam może być naprawdę sporo ciekawych rzeczy.

## Frakcji

* Senesgradzki Chaos: nie wiem gdzie, nie wiem czemu i nie wiem kiedy. Ale ten Słoik Esuriit gdzieś kiedyś się pojawi by coś się stało.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Półdara
                    1. Ośrodek OSP
                        1. Westernowa dancbuda, ma połamane parasole i jest na tyłach
                    1. Boisko do siatkówki, nie ma siatki
                    1. Osiedle Ryżawskiego
                        1. Domki mieszkalne, gdzie mieszkają wszyscy bohaterowie naszej opowieści
                    1. Miasteczko turystyczne, tak nazywane przez miejscowych
                        1. Działki turystyczne, z grillem gotowym na rozpalenie
                        1. Drewniane domki, gdzie zwykle mieszkają jacyś turyści
                    1. Las turystyczny, niedaleko miasteczka turystycznego
                        1. Obóz harcerski, bardzo blisko lasu i jeziora a daleko od wszystkiego innego
                        1. Parów Wtargnięcia, miejsce nazwane tak po tym, jak na Primusa wtargnęła w Parowie Faza Esuriit

# Czas

* Opóźnienie: 0
* Dni: 2

# Wątki kampanii


# Narzędzia MG

## Opis celu misji


## Cel misji


## Po czym poznam sukces


## Wynik z perspektywy celu



## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Surowce. |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = +3  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień; plus aspekty sytuacyjne
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza
