---
layout: inwazja-konspekt
title:  "Aleksandria, krwawa Aleksandria"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170103 - Wojna bogów w Czeliminie (AW)](170103-wojna-bogow-w-czeliminie.html)

### Chronologiczna

* [160825 - Plany Overminda (HB, KB, AB, DK)](160825-plany-overminda.html)

## Kontekst ogólny sytuacji

"Gdy Wielka Biblioteka w Aleksandrii płonęła, książki płakały krwawymi łzami opłakując swoich braci i siostry..."

## Punkt zerowy:

The bloody Alexandria.

After Caroline has managed to take over the area, she has installed the Alexandria in the palace. She has taken the royalty with her, leaving only Karina – the child – as the commander in chief of the Alexandria and whole defense system.
Caroline has a rapid linkup with the palace, therefore she's able to return whenever needed. There is an army ready in the palace as well. The defense systems are impeccable. There exists no way for anyone to be able to breach the palace and destroy the Alexandria.
Or that's what she thinks.

Ż: Gdy Klara była młodą dziewczyną - jaką największą krzywdę wyrządziła niewinnej osobie.
K: Podczas naprawy urządzenia użyła magii i okazało się, że właściciele mieszkali w pechowym miejscu i coś im się stało. Potężnie odkażani, zmieniło się ich życie.
Ż: Sprawa z historii Hektora która sprawiła, że Hektor zastanawiał się czy prawo było właściwym rozwiązaniem?
D: Sprawa z Izą Łaniewską - poszedł za dowodami, ale po wszystkim okazało się, że dowody go oszukały a krzywdę nieodwracalną jej wyrządzono.
Ż: Wymień jedną do tej pory mało znaczącą postać maga.
K: Malwina Krówka
Ż: Dlaczego Anna Myszeczka się Wam bardzo może przydać w akcji z Aleksandrią?
D: Ponieważ ma przeszkolenie bojowe którego nie ma żaden inny członek "oddziału szturmowego"
Ż: Jeden Weiner z przeszłości Klary i dlaczego jest interesujący?
K: Anatol Weiner, zanim doszło do "pechowego projektu" to często współpracował z Klarą; interesowały go przede wszystkim tematy dużych tworów magicznych.
Ż: Jakiego najstraszliwszego przeciwnika zna Edwin? "The Darkest Płaszczka" lub "The Darkest Blakenbauer"
D: Pramatka Blakenbauerów, Lewiatan. Z niej wyrasta Rezydencja.

## Misja właściwa:

Dzień 1:

Rezydencja Blakenbauerów. Narada. Obecni: Edwin, Klara, Hektor, Anna Myszeczka. Klara wpięła się w link komunikacyjny (grzecznie poprosiła) do Tymotheusa i Ottona. Poprosiła o możliwość użycia Ziarna Rezydencji które miało posłużyć do zbudowania Rezydencji w Trocinie... i zagnieżdżenie go w pałacu Blutwurstów. W ten sposób - Blakenbauerowie mają szansę zdobyć Krwawą Aleksandrię a na pewno rośnie prawdopodobieństwo przetrwania.
Dodatkowo Edwin będzie carrierem części aktualnej Rezydencji. To zwiększy prawdopodobieństwo powodzenia przeciw Krwi i Spustoszeniu do 80%. 

Patriarchowie stwierdzili, że tymczasowym lordem nowej Rezydencji musi zostać Hektor. I Hektor dostał jasne wytyczne i zadanie - ma doprowadzić do tego, by PRZECHWYCIĆ Aleksandrię i dać znać Ottonowi. Klara ma zasilić Rezydencję odpowiednimi źródłami energii. Klara zwinęła Dionizemu trochę sprzętu i jest gotowa do akcji.
Tymotheus dostarczy Ziarno Rezydencji następnego dnia.

Edwinowi wszczepiono larwę Rezydencji. Edwin nie docenił. 

Dzień 2:

Edwin na rekonwalescencji. Larwa Lewiatana źle mu zrobiła. 
Anna Myszeczka się przygotowuje. Jest puryfikatorką, terminuską i ma wiedzę o magii krwi. I nienawidzi Karoliny i jej dzieł z całego serca.
Klara wygląda jak Karolina, emanuje lekką aurą Spustoszenia. Wszyscy wyglądają trochę inaczej.

Dzień 3:

Okólnicz. Nasza czwórka - z kodami kontrolnymi pałacu Blutwurstów - jest gotowa. W miasteczku wszystko wygląda normalnie. 
Zespół ma: water *2, air *2, earth.

Pałacyk w Pierwogrodzie. Zespół wszedł na kodach Karoliny.

Kamerdyner podszedł do "Karoliny" i ją przywitał, mówiąc, że panienka Karina czeka. Spytał czy może wiedzieć, gdzie jest 'lord Adolphus'. Klara odpowiedziała, że "odgrywa swoją rolę". Klara dociera trochę dalej, do kuchni, gdy odezwał się znowu kamerdyner. Chciał poprosić "Karolinę" o synchronizację ze Spustoszeniem. Klara zamknęła mu przed nosem drzwi "nie teraz" i odpalili Rezydencję. W jadalni.
Hektor zabezpiecza magicznie wszystkie drzwi w czasie gdy Rezydencja się rozkłada. 

Rezydencja zaczęła się rozkładać. Moc zaczęła chronić Blakenbauerów. Klara podpięła drzwi do kontaktu a Hektor zaczął je wzmacniać). (vs 22 ale +6) by zabezpieczyć pomieszczenie przed pierwszym atakiem podczas rozkładania się Rezydencji. Konflikt 1 (loss). Hektorowi się nie udało; przebili się przez drzwi i wbili do świeżej prawie-Rezydencji. konflikt 2: (18). Anna ratuje jak może (+3), ale nie dość szybko. Hektor się zdołał schować i dostał odpryskami; Klara została CIĘŻKO RANNA. Edwin ją połatał; Rezydencja się ukonstytuowała.

Hektor się wpiął w Rezydencję jako lord. Poczuł dwa pomieszczenia o niesamowitej mocy Krwi. Poczuł czyste źródło energii w bibliotece. Rezydencja jest w stanie przetrwać... ustabilizowała się. Jednocześnie Hektor poczuł źródło energii. Dziwne źródło. Overmind. Overmind wysłał serię próbkowanych sygnałów i Rezydencja poczuła przyjaciela. Ale Hektor powiedział "nie, wróg"! I Rezydencja odpowiedziała walką.
Overmind wysłał dziwny sygnał, kolizyjny sam ze sobą. I kolory się pomieszały i świat zrobił się... dziwny.

ScD - ScA. (Klara sukces) (Hektor porażka). Hektor widzi, jak Klara jest opętana Spustoszeniem a inny Spustoszony żołnierz celuje w Annę. Odruchowo łapie tasak i rzuca w żołnierza. Prosto w korpus; żołnierz na plecach. Znaczy: Anna. Klara się otrząsnęła... Klara ustabilizowała Annę odrobinę, Hektor magią mentalną obudził Edwina i zmusił go do refunkcjonowania.
Hektor mocą mentalną dał radę też obudzić Annę. Edwin musi jej pomóc bo umrze...

I w drzwiach stanął Lewiatan. A raczej - inkarnacja Lewiatana. Overmind odwołał się do wiedzy z czym ma do czynienia i stworzył manifestację Lewiatana - pramatki Blakenbauerów. I nie celuje w Rezydencję - celuje w Edwina, Annę, Klarę i Hektora. Rezydencja nie stanie na drodze Lewiatanowi... a Anna jest zbyt ciężko ranna.

Klara rozpaczliwie składa zaklęcie tworząc chmurę dron z ichnią sygnaturą. Edwin i Hektor łapią Annę i Klarę. Uciekają. UDAŁO SIĘ! Cudem, ale się udało... Anna żyje.
Hektor ma wsparcie od energii magii krwi - zaczerpnął, na szczęście bez efektów negatywnych. Ale było to ryzykowne i niebezpieczne... acz jedyna opcja.

Klara wykryła portal otwierający się w Sali Myśliwskiej. Karolina chce wrócić. Lewiatan ich goni... więc muszą zatrzymać portal! A tam - czeka Karina von Blutwurst, czekając na Guwernantkę i swoich rodziców. Idą tam...

Edwin naprawia Annę, by nie była bezużyteczna. 
Hektor rzuca się i obezwładnia Karinę. Użarła, ale nie osiągnęła większego sukcesu.
Klarze udało się unieszkodliwić portal. Karolina się nie zdąży pojawić (kosztem 1 air).
Anna wróciła do prawidłowego funkcjonowania.
Hektor przeraził Karinę. Zmieniła się w mgłę i uciekła. Nie udało się jej unieszkodliwić do cna, acz cel osiągnięty.

Niestety, drugą linią odpala się Nightmare Engine kontrolowany i zasilany przez Irenę Paniszok. Biegną ze sprawną Anną do biblioteki, by móc nakarmić Rezydencję zanim Overmind da radę uaktywnić wszystkie wojska i czyste Spustoszenie. By kupić czas i odwrócić uwagę Ireny, Klara zdalnie wysadziła jej telefon - ból i szok wystarczył by Klara zdołała osłonić Hektora (najsilniejszego z nich) przed Koszmarami z Nightmare Engine i zagwarantować przekierowanie energii do Rezydencji.

Uderza Nightmare Engine.

Hektor znowu ciężko rani Annę, opanowany przez siły Nightmare Engine i objawia się przed nim Karolina Maus. Informuje, że jeśli pójdzie dalej, zniszczy Patrycję Krowiowską - ma jej wzór i może to zrobić. Daje prawo Hektorowi sie wycofać. On twierdzi, że już za późno. Potem Hektor stawia na nogi Edwina i Klarę, po czym Annę...

"Nie spodziewałam się że na tej misji największym zagrożeniem będą sojusznicy..." - Anna, ciężko ranna

Energia z Biblioteki przekierowana na Rezydencję. Overmind uaktywnił mordercze drony więc Hektor błyskawicznie wydał polecenie Rezydencji przekonfigurowania swojej geometrii. Zmienił korytarz. Przesunął drugi koniec za Nightmare Engine, za plecami Ireny. Od ręki Hektor ją unieszkodliwił (młoda nastolatka, niegroźna). Klara rozmontowała Nightmare Engine upewniając się, że rodzice Ireny (podłączeni do N.E.) nie ucierpią. Hektor dostał gorące podziękowania od młodej czarodziejki i od jej rodziców. I ZNOWU zmienił geometrię by uciec Lewiatanowi...
W tym czasie Edwin postawił Annę na nogi. Znowu.

Hektor szuka, ale nie umie znaleźć Aleksandrii. Nie ma jej tu. Klara współpracując z Ireną doszła do tego, że Aleksandria jest przesunięta fazowo; trzeba ją przywrócić magią krwi. Klęska, bo nie wiadomo jak i to groźne. Irena zgłosiła się na ochotniczkę. Nikt jej nie wybił tego z głowy; zrobiła to (widziała jak robi to Karolina). Skażenie Krwią pożarło Irenę i de facto sprowadziło ją do roli cierpiącego z głodu defilera. Anna zrobiła coup de grace. Nie da się jej już uratować...

Ta Aleksandria składa się z 300-400 ludzi w czym ~50 magów. Klara jest zauroczona i przerażona jednocześnie.
Aleksandria sprzężona jest Magią Krwi i Spustoszeniem.

Drzwi. Korytarz do Aleksandrii.
Aleksandria defensywnie wysunęła Patrycję (klon Patrycji) jako komponent główny The Deadly Brain. Ten zrobiony jest na bazie Spustoszenia, Krwi, magii i ludzkiego kontrolera powiązanego z Aleksandrią. Praktycznie nie do pokonania... praktycznie. Na pewno nie, jeśli chcą uratować Patrycję.

Hektor przekonał Patrycję, że będzie dobrze, że ją uratuje; udało mu się ją rozproszyć i wprowadzić nietypowe uczucia w delikatny mechanizm Magii Krwi w Aleksandrii. Korzystając z tego Patrycja - nie używając magii - poprosiła Annę o manewrowanie w promieniu ognia Aleksandrii by zobaczyć słabe punkty i wzory działania samej Aleksandrii. Udało jej się, acz łatwo nie było. Hektor wbił do Aleksandrii i przyjął na klatę kilka ciosów; uniknął Spustoszenia. Zabił Patrycję...
...
...Deadly Brain się wyłączył.

Klara stanęła przed Aleksandrią i się z nią połączyła. I zadała serię najważniejszych pytań:

Overmind: Karradrael; Karradrael jest Overmindem Spustoszenia i stoi za tym wszystkim. Chce odzyskać seirasa - nie ma kosztu przekraczającego tą korzyść.
Pacjent Zero: Edwin wie jak wyłączyć; nie ma powodu robić tamtej misji bo można wysłać kody kontrolne.
KADEM: pryzmatyczne bomby fazowe + szantaż; KADEM nie chciał poświęcić Franciszka Mausa, więc nie wrócił
Krwawy Aderialith: budowany w fabryce samochodów; na etapie skończenia. Auć. Z TYM ciężko będzie walczyć... acz zasilanie będzie trudne.

Rozmontowanie Aleksandrii:
- 25% osób uratowanych + Malwina Krówka
- 25% osób uratowanych + Adrian Murarz
- 25% osób uratowanych + Anatol Weiner
- 25% osób uratowanych.

Wszyscy zostali uratowani.

Wpadła Karina. Jak mogli?! Teraz Karolina zabije jej rodziców...
Hektor się... z nią zaprzyjaźnił? Przekonał że z nim jej rodzice mają większe szanse. Karina powiedziała, żeby uciekał. Pomogła załatwić ciężarówki i transport dla osób które kiedyś były częścią Aleksandrii.
I niech uratuje jej rodziców. Hektor obiecał...
Ona tu zostaje. To jej dom.

# Progresja

## Frakcji

* Blakenbauerowie: stracili Ziarno Rezydencji
* Blakenbauerowie: zyskali Adriana Murarza
* Blakenbauerowie: dostali mnóstwo ludzi i magów kiedyś sprzężonych z Aleksandrią
* Blakenbauerowie: dostali ogromny szacunek, respekt i chwałę za zniszczenie Aleksandrii i uratowanie tak wielu istnień

# Streszczenie

Uderzenie w Aleksandrię w super-ufortyfikowanym pałacu Blutwurstów. Ziarno Rezydencji dało im szansę gdy inkarnował się Overmind. Rozpaczliwa walka pod ciągłym oblężeniem - odcięcie Karoliny (by nie mogła wrócić), zniszczenie defensyw, a na końcu - zniszczenie Krwawej Aleksandrii. Zginęli: Irena Paniszok, Patrycja Krowiowska, unnamed. Uratowani ludzie i magowie sprzężeni z Aleksandrią, min. Adrian Murarz, Malwina Krówka i Anatol Weiner. Karina von Blutwurst i Hektor Blakenbauer rozstali się w zgodzie... aczkolwiek na horyzoncie jest też Krwawy Aderialith, który jest bliski zbudowania.
Znamy tożsamość Overminda - to Karradrael. Wiemy jak przywrócić KADEM. Wiemy jak unieszkodliwić Pacjenta Zero.
Duży sukces.

# Zasługi

* mag: Hektor Blakenbauer, pogromca sojuszników (z tasakiem), władca tymczasowej Rezydencji i obłapiacz wampirzych nastolatek (potem się zaprzyjaźnia ;p). A, i zniszczył Aleksandrię.
* mag: Klara Blakenbauer, przez większość czasu nieprzytomna, ranna, zdominowana lub zdechła. Ale - zatrzymała Karolinę, uratowała mnóstwo ludzi i * magów i zintegrowała się z Aleksandrią.
* mag: Otton Blakenbauer, który zażądał od Hektora dostarczenia działającej Aleksandrii i w pełni wspierał misję współpracując z Tymotheusem.
* mag: Tymotheus Blakenbauer, który dostarczył Klarze Ziarno Rezydencji i uznał, że bezpieczeństwo rodu jest ważniejsze niż cokolwiek innego. Wspiera Ottona.
* mag: Anna Myszeczka, puryfikatorka i terminuska ciężko raniona dość często przez... Hektora w malignie. Przeżyła - i podjęła kilka trudnych decyzji. Śmierć jej dzieci nadal ją prześladuje...
* mag: Edwin Blakenbauer, który miał zaimplantowaną Larwę Rezydencji by wzmacniać Ziarno; rozpaczliwie trzymał wszystkich przy życiu... i utrzymał.
* mag: Karina von Blutwurst, młoda wampirzyca szanująca Karolinę, która nie była w stanie pokonać Blakenbauerów. Pozwoliła im odejść, prosząc, by uratowali jej rodzinę.
* mag: Irena Paniszok, zasilanie Nightmare Engine. Chciała się zrehabilitować i przyciągnęła do tej fazy Krwawą Aleksandrię. Skażona poza możliwość pomocy, coup de grace ze strony Anny. KIA.
* mag: Adrian Murarz, który został włączony do Aleksandrii przez Karolinę; uratowany i wyciągnięty przez Klarę.
* mag: Anatol Weiner, dawny przyjaciel Klary zainteresowany dużymi tworami * magicznymi; atm wyciągnięty z Aleksandrii. Na co dzień - artefaktor.
* mag: Malwina Krówka, która pomogła sformować Aleksandrię i Deadly Brain; została dołączona do Aleksandrii "w nagrodę".
* czł: Ernest Kokoszka, kamerdyner von Blutwurstów który nie zdążył zatrzymać "fałszywej Karoliny". Przeżył i dalej opiekuje się młodą Kariną.
* czł: Patrycja Krowiowska, której klon został stworzony przez Karolinę by trzymać Hektora w ryzach. Karolina nie doceniła determinacji Hektora, który ją zabił. KIA.

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska 
            1. Powiat Okólny
                1. Okólnicz
                    1. Fabryka samochodów, gdzie konstruowany jest Krwawy Aderialith
                    1. Pierwogród
                        1. Pałac Blutwurstów, na planie podkowy; o charakterze eklektycznym z elementami neobaroku
                            1. Pokój Kariny, gdzie znajduje się normalnie Karina.
                            1. Biblioteka, gdzie znajdują się źródła energii dla Pałacu; chroniona przez 2 jednostki bojowe.
                            1. Sala myśliwska, gdzie jest ustawiony portal między Pałacem a innymi miejscami
                            1. Sala balowa, przeniesiona fazowo, gdzie znajduje się zasilanie Aleksandrii
                            1. Sala lustrzana, przeniesiona fazowo, gdzie znajduje się Aleksandria
                            1. Imponujący hall, gdzie znajduje się ARMIA - Spustoszeni, golemy i inne istoty. Też: ludzie.
                            1. Kaplica pałacowa, gdzie znajduje się Kryształ Mausów i Irena Paniszok
                                1. Empora, gdzie są ludzie zasilający Irenę i Nightmare Engine
      
# Wątki



# Skrypt

01) V Oprzeć się koszmarnej rzeczywistości (Spustoszenie dotyka Klarę): ScD - ScA
02) V Opuszczenie koszmaru bez zabijania Anny Myszeczki
03) V Stawienie czoła swojemu Koszmarowi bez asymilacji Krwi i oddania się w marzenia
04) V Uciec przed Lewiatanem zanim ten wchłonie i zabije Annę (która ochroni Edwina)
05) V Nie wpaść w uzależnienie Magii Krwi
06) V Uniemożliwienie przedostania się Karolinie z powrotem do pałacu
07) V Podłączenie się do energii Rezydencji by przetrwać w Krwawych warunkach
08) V Przetrwanie walki z Nightmare Engine podczas pierwszej fali ataków (zanim Rezydencja nie wbije)
09) V Wzmocnienie energii Rezydencji zanim Krwawa Aleksandria zdominuje ziarno Rezydencji
10) X Unikanie Spustoszenia ze ścian i podłogi (reakcje); szybkie działania, ciągły ruch
11) V Oszukiwanie Spustoszonego Pałacu
12) X Manipulowanie Krwawym Pryzmatem używając woli (ScA # Frt)
13) X Unikanie armiom i siłom używając Krwawego Pryzmatu
14) V Uratować / zostawić na śmierć Irenę Paniszok - Nightmare Engine (kryształ Mausów)
15) V Otworzyć Pryzmatem i przesłuchaniami wejście do Aleksandrii
16) X Nie doprowadzić do śmierci Adriana Murarza podczas rozmontowywania Aleksandrii / ucieczki z Deadly Brain
17a) V Wybrać śmierć Patrycji lub Adriana (przechytrzyć Aleksandrię)
17b) V Przechytrzyć wiedzę Patrycji i Andriana Murarza
17c) V Rozmontować pułapki magiczne i technomantyczne
17d) V Przejść przez ogień i zabić kontrolera Deadly Brain; nie dopuścić do Spustoszenia
18) V Zabicie Katriny von Blutwurst (lub jej unieszkodliwienie)
19) X Zabicie. Nie dać się jej oszukać. Nie dać jej Spustoszyć.
20) X Oprzeć się syreniemu śpiewowi Aleksandrii
21) V Rozmontować Aleksandrię bez zabijania Patrycji
22) X Przekierowanie energii by nie zabić części populacji miasteczka i kontrolowanie rozłożyć Aleksandrię
23) V Poznać tożsamość Overminda
24) X Oprzeć się Dotykowi Overminda

znaczących konfliktów: [19 - 25]
fate points: 2/osobę

Siły drugiej strony:

- Karina von Blutwurst (niespustoszona)
- Irena Paniszok (spustoszona)
- Spustoszony Pałac
- służba
- armia bojowa
- Lewiatan
- Karolina Maus
- Aleksandria

# Czas

* Dni: 3