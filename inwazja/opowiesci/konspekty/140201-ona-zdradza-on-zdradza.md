---
layout: inwazja-konspekt
title:  "Ona zdradza, on zdradza"
campaign: druga-inwazja
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140121 - Zniknięcie Sophistii(AW, HB)](140121-znikniecie-sophistii.html)

### Chronologiczna

* [140121 - Zniknięcie Sophistii(AW, HB)](140121-znikniecie-sophistii.html)

## Misja właściwa:

Wszystko zaczęło się całkiem zwyczajnie - do Andrei przyszedł Radosław Krówka ze standardowym zleceniem do detektywa. Nela go zdradza i on chce wiedzieć z kim oraz w jakich okolicznościach. Andrea się obruszyła, bo to jego znalazła ostatnio w pościeli z Emilią Szudek (misja 1) i odesłała go do diabła. Nieszczęśliwy Radosław uniósł się honorem i zgłosił się do Waldemara Zupaczki, który z radością przyjął zlecenie. Chwilę potem do Andrei zgłosiła się... Nela Welon. Bo Radosław ją najpewniej zdradza. Zachowuje się dziwnie, przychodzi i wychodzi w różnych godzinach... i chyba jej już nie kocha. Andrea ciężko westchnęła. NAPRAWDĘ nie lubi takich zleceń. Chciała Nelę odesłać, lecz ta dodała, że ostatnio dziwnie się czuje. Gubi wydarzenia. Czas jej ucieka. Jest w innych miejscach niż oczekiwała. Oki. Na to Andrea odesłała ją do analityka magicznego, kolegi po fachu - Mariana Rustyka. I, jak to Andrea, zapomniała o nieważnej sprawie.

Sprawa zrobiła się tyci ważniejsza, gdy pewien czas później Marian Rustyk do niej zadzwonił. Powiedział, że ma sama zobaczyć co i jak, on się do tego nie miesza i że ona ma szukać "tego, czego nie ma". Przekaże jej też wyniki swoich badań. To wreszcie zaciekawiło Andreę...
Andrea dokładnie przebadała Nelę i "to" znalazła. W ciele Neli, w okolicach układu nerwowego znajdowało się coś dziwnego. Pusta luka "po czymś". I wszystko wskazuje na to, że z tej luki promieniuje skażenie magiczne, na całe ciało. I to wygląda na cholernie stare. Nikt wcześniej jej nie badał czy co? Podczas tego badania Andrea coś przełączyła i Nela ją odepchnęła, łapiąc za coś ostrego. Przestała ją poznawać, zupełnie, jakby miała nową osobowość. Andrea pozwoliła jej się oddalić i zaczeła Nelę śledzić.

Nela pochodziła po mieście i zatrzymała się przy Kłodnicy. W rozpaczy, patrząc na swoje dokumenty, rzuciła je do "rzeki" i po chwili właściwa osobowość Neli powróciła i czarodziejka wrzasnęła przeraźliwie. Andrea uratowała jej dokumenty, niekoniecznie doceniając całą tą sytuację. Zabrała Nelę do siebie, do swojego gabinetu i przeprowadziła wywiad detektywistyczny. Wyszło z niego co następuje (odnośnie Neli):
- uzyskała moc przy Zaćmieniu
- pierwsze echa naleciałości mocy (czyli tego dziwnego Skażenia) pochodzą sprzed Zaćmienia
- Nela zupełnie nie ma poczucia czasu i ma utraty pamięci.
- te dziwne rzeczy zaczęły się po tej historii z kolią.
Dziewczyna się bardzo boi, powiedziała, że Radosław się też dziwnie zachowuje. Zafrasowana Andrea od razu skojarzyła kolię z Krystalią Diakon, więc od razu skontaktowała się z jedynym z Millenium komu jakoś tam w miarę ufa. Mateuszem Nieborakiem.

Mateusz bardzo się zdziwił całą tą sprawą ("ale po co Krystalia miałaby coś takiego robić"). Przepytany przez Andreę, powiedział, że Krystalia Diakon jest dość nietypowym magiem tego rodu - nie robi eksperymentów na magach i ludziach (Andrea potwierdziła to ze swoimi źródłami; Krystalia jest bardzo grzeczna z punktu widzenia eksperymentowania). Krystalia za to fascynuje się biżuterią, zwłaszcza żywą biżuterią i to jest to, co ją najbardziej bawi. Jeśli już ma coś zrobić (dość rzadkie), prosi o zrobienie tego Mateusza lub przygotowuje odpowiednią biżuterię. Ogólnie, Krystalia ma charakter osoby co raczej chce przede wszystkim ćpać sobie w spokoju. Jednak argumenty Andrei o tym, że przecież jest najlogiczniejszą podejrzaną i Radek też był w jej rękach Mateusza przekonały, że należy Andrei pomóc. Bardzo pomóc. Przyjechał błyskawicznie.

Andrea stwierdziła, że zdecydowanie potrzebny jej zaufany magiczny lekarz. Nasunął jej się na myśl tajemniczy Edwin Blakenbauer, ale... no, Blakenbauer. No i Hektor nie do końca chciał z bratem współpracować, więc odpada. Mateusz Nieborak zaproponował... Teresę Żyraf, uczennicę Krystalii. Seriously. Ale Mateusz wyjaśnił, że relacje między nimi nie są tak oczywiste i Teresa z radością pomoże oczyścić dobre imię swojej przerażającej mentorki. Niech zatem będzie Teresa... w międzyczasie Andrea jeszcze dopytała Nelę o cokolwiek związanego z lokalizacją Radosława i dostała informację o tym, że Radosław wyjeżdżał raz czy dwa do dzielnicy Jarzębina. I że zdaniem Neli jeśli ma gdzieś kochankę, to tam. Oki, jest się czego złapać. Andrea zostawiła Teresę z Mateuszem by zajęli się Nelą, a sama ruszyła do Jarzębiny na poszukiwanie Radosława...

...i znalazła jego samochód. Przed domem rodziny Szudek. Rodziny, do której należy Emilia (przyjaciółka Mateusza i Teresy). W domu wyraźnie ktoś jest. Andrea podeszła do okna, próbując wyczaić co Radosław tam robi i wtedy uderzyło w nią zaklęcie. Hipnotyczny czar miłosny. Andrei udało się cudem z niego otrząsnąć i dojrzała, że w domu znajduje się Radosław Krówka. W łóżku. Sam. Andrea błyskawicznie się wycofała do auta by ochłonąć - i w samą porę. Do domu przyszła taka ostra blondynka, podobna trochę do Emilii. Weszła do domu (jak do swojego), uderzył ją czar i poszła z Radosławem do łóżka. I nie, nie spali.
Ooooooook.......

Andrea stwierdziła, że to tak dziwne że musi to zbadać. Ściągnęła Mateusza i powiedziała, że nie wie co Krystalia sobie wymyśliła, ale to jest przesada. Mateusz z finezją porwał Radosława z domu Szudek i pojechali do gabinetu Andrei (znowu, widzieli jak wróciła do domu Emilia z rodzicami, ale na nich czar nie zadziałał - ta dziewczyna (którą Mateusz zidentyfikował jako Kornelię, siostrę Emilii) będzie miała z pewnością coś do wyjaśnienia rodzicom :P).

W gabinecie Andrea zrobiła głęboki i dokładny skan Radosława. Jakkolwiek jest pod wpływem czegoś, nie ma w nim takich anomalii jakie są w Neli. Po dokładnym przebadaniu Radosława Krówki wyszło wyraźnie, że jest pod wpływem czaru miłosno-hipnotycznego ORAZ na poziomie biologicznym został zmodyfikowany pod kątem kochliwości. Robota eksperta. No, to wyraźnie wskazuje na Krystalię, temu Mateusz nie jest w stanie już zaprzeczyć. Gwóźdź do trumny Krystalii wbiła Teresa, która stwierdziła, co następuje (po badaniach Neli i czytaniu analizy Mariana Rustyka):
- jest powiązanie kolii Krystalii ze stanem Neli Welon
- Teresa widziała takie struktury narastające na szkicach i papierach Krystalii kiedyś
- to skażenie to anomalia zastępująca ciało Neli, przekształcająca je dokładnie
- pochodzi wszystko od tego, czego nie ma, tego czegoś w układzie nerwowym Neli czego nie ma
- jest to podobne do sposobu, w jaki Krystalia hoduje biżuterię
- dla osób nie mających dostępu do laboratorium Krystalii niewiele z tego ma sens; Teresa wie, na co patrzeć

No, nieźle. Andrea zrobiła listę dziwnych rzeczy i dziwnych pytań na chwilę obecną:
- kto i czemu rzucił czar na dom rodziny Szudek.
- czemu Radosław się tam udał.
- o co chodzi ze schizofrenią Neli i jej zanikami pamięci (częściowo na to odpowiedziała Teresa)
- czy kolia była katalizatorem? Czy Krystalia to zrobiła? Czemu?
- i Nela i Radek zdradzają się nawzajem. Lol. Czemu? W czyim interesie?
- o co chodzi z tym ciałem Neli? Czemu nikt tego nie wykrył wcześniej?

By zdobyć odpowiedzi na choć część pytań, Andrea postanowiła przesłuchać Radosława. Połączenie antidotum na eliksir miłosny (<3 Mateusz) wraz z sugestiami i przekonywaniem Andrei podziałało i Radosław potrafił przezwyciężyć czar. Przyznał, że od czasu tamtych wydarzeń ma cały czas w głowie Emilię Szudek (tu Mateusz się skrzywił) i że ona też wyraźnie do niego coś czuje. Odwiedzał ją, by mieć szansę na seks i być z Emilią. Kornelii nie lubi, ze wzajemnością - Kornelia jest BARDZO przeciwna rodowi Krówek i jakiemukolwiek związkowi Emilii z Radosławem. Czemu pojechał teraz? Wiedział, że Emilia powinna być sama. Kornelia mu to powiedziała. 
Tu Andrea zrobiła duże oczy, bo to wszystko wielkiego sensu nie miało...

Na tym etapie była już tylko jedna osoba, która mogła rozwiązać tą zagadkę dla Andrei. Krystalia Diakon. Nie było jej jednak w laboratorium (zostawiła jednak kartkę dla Teresy gdzie będzie), więc z Mateuszem udali się do niewielkiego domku Krystalii Diakon. Na wejściu byli świadkami awantury - z domu wypadł zakrwawiony Artur Żupan ze złamanym nosem, na wyjściu słyszeli krzyk Krystalii "zostaw ją i nigdy więcej się do niej nie zbliżaj". Co tu mówić... weszli do środka. W pokoju znajdowały się przedmioty rodem z sado-maso, Krystalia ma na twarzy ślady od przypalania papierosem (i nie tylko) oraz Krystalia ma złamaną rękę. Bycie ćpunką z painkillerami pomaga. Bystre oko Andrei dostrzegło dokumenty opisane jako "Żywy Aderialith", które Krystalia szybko schowała przed oczami Andrei i Mateusza.

Zapytana o co chodziło, Krystalia stwierdziła że miała różnicę zdań z Arturem Żupanem odnośnie jej i jego roli. Ale już wszystko jasne i nie będzie jej niepokoił. Krystalia zachowywała się nadzwyczaj władczo i nie pozwalała Mateuszowi dojść do słowa. Gdy Andrea i Mateusz przedstawili jej wszystkie dowody, usiadła na fotelu. Mateusz prosił, by wyjaśniła co i jak, bo on nie ma jak jej ochronić - i Krystalia się zgodziła. Pod warunkiem, że zostanie z Andreą sam na sam. Na to Mateusz nie chciał się zgodzić, lecz Andrea dała radę przekonać go, że nie wykorzysta Krystalii i nie zrobi jej krzywdy. Mateusz wymógł na Andrei, że jeśli Krystalia zrobiła coś złego, Millenium się tym zajmie (w osobie Mateusza), że to wszystko nie wyjdzie na jaw. Andrea się zgodziła, mimo zdecydowanego niesmaku jaki ta prośba wywołała. I tak nie było łatwo, Andrea musiała posuwać się aż do takich stwierdzeń jak "twój ojciec byłby z ciebie teraz dumny" (ojciec Krystalii był mało ważnym magiem, który gardził narkotykami i używkami; nie był Diakonem; Krystalia jest "Diakonem adoptowanym", czyli nie dzieli krwi z rodem Diakon). 

Krystalia, walcząc z narkotykami w swoim organizmie, wyjaśniła parę rzeczy (ostrzegła wcześniej Andreę, że tej NIE spodoba się to co usłyszy i że dla Andrei będzie lepiej, jeśli nigdy nie pozna prawdy):
- Radosław miał się zakochać w kimkolwiek kto NIE jest Nelą. Ona (Krystalia) miała rozerwać miłość między Radosławem i Nelą, na życzenie Malwiny Krówki.
- Krystalia podpięła Radosława do Emilii ze wzajemnością. Niech mają krótki, przyjemny romans. Jakiś miesiąc to potrwa.
- Nic nie wie o czarze na rodzinę Szudek.
Ale gdy przeszła do opowiadania o Neli, historia stała się dużo poważniejsza.

Za czasów świetności Moriatha istniały pasożyty które Krystalia nazwała "mindwormami". Proces wprowadzenia mindworma do organizmu maga trwał mniej więcej tydzień, po czym mindworm podpinał się do całego systemu biomagicznego maga i stawał się "nakładką" psychofizyczną na maga. Efektywnie, tworzył nową osobowość w tym samym ciele, uciszając poprzednią osobowość i czerpiąc z doświadczeń i wspomnień generując własne. Dobrze wprowadzony mindworm był całkowicie niewykrywalny i niezawodny. Umożliwiał Moriathowi wprowadzanie agentów i "sleeper agent" tam, gdzie w danym momencie byli mu potrzebni. Właściwie po pojmaniu wrogiego maga Moriath mógł przy użyciu mindwormów zmienić go w lojalnego niewolnika.
Do dzisiaj nie dało się znaleźć niezawodnego sposobu usuwania mindwormów z ciała maga. Na szczęście, Moriath nie miał tego nigdy dużo.

No i właśnie mindworma miała Nela. Jednak wydarzenia z kolią zabiły mindworma (przez co nagle to wszystko stało się jakoś wykrywalne). Ona, Krystalia zauważyła tą sytuację i usunęła z ciała Neli martwego mindworma, co jednak zaczęło powodować przebijanie się oryginalnej osobowości (sprzed mindworma). To oznacza, że większość wspomnień Neli to kłamstwo. I tu Krystalia zadała pytanie - co ma zrobić? Przywrócić Neli osobowość "pierwotną" czy wprowadzić "zaślepkę" by Nela pozostała Nelą, nawet jeśli bez mindworma (kontrolującego Nelę). 

Andrea wybrała zaślepkę. Oryginalna osobowość nie ma znaczenia, tamta czarodziejka od dawna nie istnieje. Nela nie zasłużyła na bycie zniszczoną. 

Zapytana o Moriatha, Krystalia odpowiedziała, że powiedziała wszystko. Została oczyszczona ze zbędnej wiedzy; jej to niepotrzebne. Teraz jej światem i życiem jest biżuteria, nie interesuje jej nic więcej a na pewno nie Moriath.

Krystalia za to, co powiedziała wymogła na Andrei, że jeśli jej (Krystalii) coś się stanie, Andrea pomoże Teresie. Teresa jest sama, poza Mateuszem i nią nie ma nikogo. Niech ma przyjaciółkę, kogoś, kto pomoże jakby wpadła w kłopoty. 


Na końcu sesji jeszcze wyszło Andrei, że osobą która rzuciła czar na dom Szudek była... Kornelia Szudek. Chciała złowić Radosława dla siebie...


![](140201_OnZdradzaOnaZdradza.png)


# Zasługi

* mag: Andrea Wilgacz jako detektyw która nie chce się angażować w sprawy sercowe... które jednak okazują się dużo bardziej poważne.
* mag: Radosław Krówka jako osoba mająca się zakochać w kimkolwiek innym niż Nela Welon, bo mama mu nie pozwala. Też: poluje na niego Kornelia Szudek.
* mag: Nela Welon jako ofiara mindwormów i kolii Krystalii; sztuczna osobowość i ta, która miała być zakochana w kimś innym niż Radosław Krówka bo matka mu nie pozwala :p.
* mag: Marian Rustyk jako znajomy Andrei analityk magiczny który badając Nelę Welon znalazł ślady Moriatha i mindwormów; ostrzegł Andreę i umył ręce. 
* czł: Mateusz Nieborak jako agent Millennium któremu ufa Andrea; skontaktował Andreę z Teresą (lekarzem) i zaopiekował się Nelą. Bardzo chroni Krystalię.
* mag: Krystalia Diakon jako podejrzana o bycie agentką Moriatha hodowczyni biżuterii organicznej. Zaćpana, powiedziała Andrei coś o przeszłości Moriatha i że nic już nie pamięta.
* mag: Teresa Żyraf jako uczennica Krystalii i magiczny lekarz pomagająca Neli; zidentyfikowała mindworma Neli jako coś co Krystalia ma na papierach i jest podobne do hodowania biżuterii.
* mag: Emilia Szudek jako osoba desygnowana przez Krystalię do bycia kochanką Radosława. Uratowała ją siostra która chciała Radosława dla siebie :P.
* mag: Kornelia Szudek jako osoba nie chcąca mieć nic wspólnego z rodem Krówków oficjalnie a polująca na Radosława Krówkę dla siebie.
* mag: Artur Żupan jako mag mający bardzo burzliwą (przypalanie, bicie się) różnicę zdań z Krystalią odnośnie "Żywego Aderialitha".
* mag: Malwina Krówka jako bardzo wpływowa czarodziejka której systemy zabezpieczeń są powszechne w SŚ i nie tylko; chciała rozerwać miłość Radosława*Neli używając Krystalii.
* mag: Mojra jako nieżyjący już Moriath; ten, który używał mindwormów do kontroli magów i nadawania im niewolniczej wobec siebie osobowości.