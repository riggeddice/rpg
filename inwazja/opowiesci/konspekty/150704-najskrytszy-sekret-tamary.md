---
layout: inwazja-konspekt
title:  "Najskrytszy sekret Tamary"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

# Wątki

- Szlachta vs Kurtyna
- Aegis czy EIS?
- Wojny wielkich gildii Śląska
- Srebrna Świeca Ozydiusza Bankierza

## Kontynuacja

### Kampanijna

* [150716 - Chora terminuska i żabolód (PT)](150716-chora-terminuska-i-zabolod.html)

### Chronologiczna

* [150625 - Poligon kluczem do Marcelina? (HB, SD)](150625-poligon-kluczem-do-marcelina.html)

## Punkt zerowy:

Ż: O co KADEM może oskarżyć Marcela Bankierza?
B: Przy jego obecności zniknęły wrażliwe materiały - jak dostać się na poligon doświadczalny.
Ż: Czemu to, że Marcel wypuścił tą informację na światło dzienne tak zirytowało Infernię?
D: W publicznej opinii Infernia stała się "jak każdy Zajcew" a walczyła by opinia była "że Diakon".
Ż: Jaką korzyść z tej misji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Co specyficznego było z tymi KADEMowymi żabolodami (o czym absolutnie nikt, łącznie z Warmasterem, nie wiedział)?
K: Eksperymentalne żabolody, które potrafią wytrzymać wyższą temperaturę i chłodzą mocniej.
Ż: Na kim najmocniej odbiją się obostrzenia Marty (lockdown KADEMu)?
K: Na Ignacie Zajcewie.
Ż: Co może wyciągnąć SŚ na KADEM by jeszcze bardziej eskalować spiralę nienawiści?
D: Dowody, że KADEM "twórczo czerpie" z ich technologii i pomysłów.
Ż: Obszar, w którym kieruje się Infernia by pokazać Hektorowi gdzie jego miejsce?
K: Spróbuje go wrobić w coś paskudnego, co w świecie ludzi jest oczywiste a w świecie magów nie jest. Kopniak na własnym podwórku.
Ż: Jaką ważną informację (obszar) może dostarczyć Marcel Bankierz, być może całkiem przypadkowo?
D: Wie, w jaki sposób poligon KADEMu został spenetrowany; wie, w jaki sposób do tego doszło.
Ż: Jaki ród Srebrnej Świecy ucierpiał wskutek działania Sił Specjalnych Hektora.
K: Ród Maus.
Ż: Jak ucierpiał ród Maus?
D: W wyniku akcji się musiał zadłużyć.

Marta wprowadziła full lockdown KADEMu. Paweł ma obowiązek jej pomóc, ale Siluria została wysłana by pomóc Hektorowi w temacie Marcelina.
Zdaniem Emilii Kołatki, Marta nie rozumie Srebrnej Świecy. Nie da się wykorzystać Hektora korzystnie.

Problemy do rozpatrzenia dalej:
- kto stoi za atakiem na KADEM? Dlaczego?
- kto był na poligonie? Jaki był cel wejścia na ten poligon? ŻABOLODY SERIOUSLY?    ans: skażyciel?
- czemu Pakt w to wszedł? (Założenie: że to robota Emilii)      ans: Infernia.
- kto skoordynował to wszystko?
- Marcel Bankierz i jego rola.
- jaka czarodziejka ma koncept munduru dla Inferni?
- co zrobi Infernia by dostać przeprosiny i śledziki? Jak daleko się posunie?
- jak ruszyć Tamarę, by dostać dostęp do magitechowych pancerzy
- jak wpłynie lockdown KADEMu i nowe, drakońskie systemy defensywne Quasar na magów KADEMu?

"
Hektor -> Edwin. Wytłumaczył mu swój plan:
- Marcelin i Infernia jadą na jednym wózku tak naprawdę. Więc jak on idzie na dno, ona też.
- Jeśli w wyniku działania tego wszystkiego faktycznie doszło do penetracji KADEMu i problemów Świecy, oboje ucierpią.
- Jeśli jednak to niepowiązane, oboje dostaną stracha i nic z tego strasznego nie będzie. 
- Cel Hektora to pokazać, że twardy i uczciwy prokurator opłaca się Świecy i KADEMowi, by możni nie przejęli się Paktem.
- Do tego celu Edwin musi zgłosić utratę żabolodów, że poniósł stratę. Przesunąć z "Infernia vs Marcelin" na "Świeca i KADEM vs Złe Siły".
"

## Misja właściwa:

### Faza 1: Lockdown KADEMu.

Na KADEMie rozbrzmiały alarmy. Przez interkom odezwała się Quasar i zapowiedziała, że wprowadza lockdown KADEMu. Za godzinę wszyscy magowie KADEMu mają znaleźć się na KADEMie lub czekać na zewnątrz aż lockdown się zakończy. Oczekiwany czas blokady KADEMu to około 3 dni, w czasie którego KADEM przebuduje swoje systemy zabezpieczeń i głównym kontrolerem zabezpieczeń zostanie Tai.
Siluria ma wytyczne, by pomóc / działać z Hektorem. W świetle powyższego nikt nie anulował jej autoryzacji GS Aegis 0003.

I Siluria ma problem. Wziąć głowicę, czy też nie.
Jeśli weźmie, najpewniej bez działa strumieniowego, by nie wpadło w cudze ręce...

Siluria poleciała się spakować, po czym poprosiła Norberta, by popilnował Silurię - ma zamiar zsynchronizować się z GS Aegis 0003. Siluria poprosiła Norberta o quarka z wytycznymi kiedy głowica może mieć autonomię a kiedy nie. Poszli do głowicy.

Siluria uruchomiła głowicę i zażądała profilu rozkazów jak się ma zachowywać i kiedy ma podejmować inicjatywę. Głowica odesłała super-pakiet informacji i Norbert musiał pozbierać Silurię z podłogi. Jednak Siluria zrozumiała. Eryk NIGDY nie dał Aegis żadnych wytycznych, po prostu przekazywał jej raz na jakiś czas informacje # sygnał emocjonalny. I głowica się uczyła od niego, budując jego profil. Największy stalker.
Głowica zbudowała dwa profile - jeden dla Eryka, drugi dla Silurii.

Wszystko wskazuje na to, że głowica nie ma pamięci z 0002. Ale Siluria żyje za długo by wierzyć w to, że "wszystko wskazuje" oznacza "tak jest".
Siluria załadowała kryształ etyczny Norberta w 0003 jako podstawę zachowania przy jej profilu. Dodała też dodatkowe komponenty nie krzywdzenia, ochrony życia i ogólnie unikania problemów.
Siluria weźmie głowicę ze sobą. Nie odważy się iść sama.

Przed magami GS Aegis 0003 ma się maskować jako vicinius KADEMu. Niech będzie imię Nihilus, przed ludźmi nazywa się "Nikola".

Tymczasem Hektor ma konferencję prasową w świecie magów. Nie założył oficjalnych szat (strój a'la ludzki król i IDIOTYCZNA peruka). Jak zatem się ubrać? I tu jest pomysł - Marcelin poradzi.
Marcelin doradził Hektorowi połączenie strojów inkwizytora Hieronima Mausa, który jako prokurator utopił we krwi swój ród wyniszczając wszystkie defilerskie i niewłaściwe zachowania a po zakończeniu kadencji został wybrany serasem. Seras Hieronim Maus na tyle wpłynął na Karradraela, że dwa następna pokolenia nie wykształciły żadnych zbrodniczych cech, gdyż Karradrael pilnował tego bardzo dokładnie. Terminus Inkwizytor Maus zasłynął z tego, że egzekucje przeprowadzał osobiście, by nikogo nie skalać tym strasznym czynem.
Drugą osobą której strój przyjął Hektor była prokurator Liliana Sowińska zwana Łagodną. Po wojnie trzeba było pojednać magów i w jakiś sposób rozliczyć przestępców. Liliana objęła urząd i skupiła się nie na surowym karaniu a na rehabilitacji, resocjalizacji oraz zadośćuczynieniu. Niekoniecznie tego oczekiwano od prokuratora, ale historia mówi, że Liliana była właściwą osobą we właściwym miejscu o właściwym czasie.

Dzięki takiemu połączeniu strojów Hektor będzie w stanie pokazać, że stoi na barkach gigantów i jaka jest jego agenda.

Hektor na konferencji wygłosił oświadczenie - sprawa Infernia x Marcelin została wzbogacona o nowe wątki. Na tą chwilę - proces odbędzie się a śledztwo w nim przeprowadzone będzie bardzo skrupulatne. 
Jolanta Sowińska z Paktu zapytała jak Liliana - patronka Hektora - zareagowałaby na to, jaką karę otrzymała Iza Łaniewska. Hektor powiedział, że chce jedynie chronić Srebrną Świecę i magów przed terminuską. Jolanta nacisnęła - jak Hektor skomentuje to, że Izabela Łaniewska ma zostać przekształcona w Diakonkę? Hektor powiedział, że to poza jurysdykcją.
Hektor odpowiedział na jeszcze kilka innych pytań. Ogólnie wypadł dobrze. 
Tą konferencją (17v12) przeciągnął dziennikarzy na swoją stronę; pojawi się cykl bardzo pozytywnych artykułów na jego temat. Jola "schodzi" z prokuratora.

"Prokurator Hektor Blakenbauer. Ekscentryczny ludzista, stoi na ramionach gigantów i nie boi się podejmować niepopularnych decyzji."

Siluria spotkała się z wynikiem konferencji prasowej. Poszła z "Nikolą" spotkać się z Hektorem a za nią KADEM przeszedł w total lockdown. Spotkanie w parku (by nie stresować głowicy XD). Głowica wygląda nieco na "mało ładną dziewczynę w moro". 
Podczas rozmowy Silurii i Hektora Aegis powiedziała, że Ignat też jest poza KADEMem - nie zdążył wrócić na czas.
Siluria przypomniała Hektorowi o Marcelu. Powiedziała też, że bardzo dużo wskazuje na to, że na poligonie było 2 magów Świecy w czym jeden terminus. I że celem najpewniej było podłożenie skażyciela.

### Faza 2: Wizyta Salazara

Salazar Bankierz odwiedził Hektora i Silurię. Przeprosił, że przeszkadza, ale powiedział, że ma problem. Wie coś, co się stało na poligonie. Wie, że była tam Tamara Muszkiet, ale nie pamięta czemu - zażył Kryształ Zapomnienia (ukrył przed sobą własną motywację i źródło). Wie też, że Tamara jest po tym wszystkim w bardzo złym stanie i że część dowodów znajduje się u Tamary w domu.
Nie wie, czemu to zgłasza. Ale wie, że nie chce wojny między gildiami.

0003 wykryła dziwny sygnał. Astralny sygnał uszkodzonego magitechowego kombinezonu w supermarkecie. Przesłała to po linku do Silurii. W wyniku - zdecydowali się biec.
0003 - "przyjdę ze swoim motorem". Poszła za winkiel i wzięła pierwszy motor, asymilując go częściowo i jadąc, łamiąc wszystkie przepisy. Blisko blokady z gazem, za którą jest supermarket. Czyli - magowie.
Nihilus, Hektor i Siluria stoją przed supermarketem. W środku Siluria i Hektor czują bardzo silne Skażenie. I jest magiczny odstraszacz, by ludzie się nie zbliżali. I chyba ktoś jest w środku; ale w takim natężeniu Skażenia trudno coś wyczuć.

Mniej więcej teraz Hektor się zorientował, że nie wiedzą czemu tu są.

Siluria puściła przodem Nihilusa. Ona i Hektor zostaną na zewnątrz. Nihilus wchodzi do środka.
W środku znajduje się bardzo potężny mag, który mocą czystej entropii rozmywa ślady, przekształcając je w płomienie.
Siluria spróbowała go zidentyfikować. Nie zna go, nigdy go nie widziała, ale jego działania kojarzą się tylko z jedną istotą z legend. Saith Flamecaller, podobno dawno martwy. I nie, Siluria NIE chce z nim walczyć.
Siluria kazała Aegis się wycofać. Flamecaller stwierdził, że to dobry wybór.
Wysadził wszystko. Zniszczył wszystkie ślady. Powiedział też głowicy, że jeśli komukolwiek coś powie czy co widziała, zniszczy ją, wszystko co ona kocha, wszystko na czym jej zależy i każdego komu to powiedziała.

Czyli Saith Flamecaller istnieje. I nie wiadomo czemu wspiera (potencjalnie) Tamarę. Czemu zaciera ślady.
A Siluria ma trudny wybór - powiedzieć potem Marcie czy nie...

"Nikola" powiedziała Hektorowi, że mag zamazał ślady i uciekł. Było ciemno a mag miał maskę na twarzy; coś bełkotał, ale ona nie wie co powiedział.

Oki. Siluria jest w martwym punkcie. Chociaż nie - KADEM jest w lockdownie. Siluria może uzurpować sobie prawa, by móc poprosić Srebrną Świecę o to, by udostępnili listę terminusów rannych po akcji na poligonie.

Dodatkowo, Siluria chce się udać z Hektorem do tien Ozydiusza Bankierza. Ozydiusz Bankierz jest przełożonym terminusów w Kopalinie i okolicy. On może udzielić Silurii potrzebnych jej uprawnień do przeszukania domu Tamary Muszkiet. Głowica zostaje pod domem Tamary w trybie "search and perceive" a w tym czasie ramię polityczne KADEMO-prokuratorskie próbuje przekonać Ozydiusza.

Ozydiusz Bankierz jest bardzo niechętny działaniom Hektora i Silurii. Obwinia ich za to, że stracił Izabelę Łaniewską. Że stracił NIEWINNĄ Izabelę dzięki niewłaściwym działaniom Hektora. Ozydiusz powiedział, że nie wyraża zgody. Siluria powiedziała, żeby sprawdził stan Tamary Muszkiet. Okazało się, że Tamara jest niedysponowana; awaryjny urlop zdrowotny.
Ozydiusz powiedział, że wyrazi zgodę na przesłuchanie. Ale jeśli coś stanie się Tamarze (skazana, on ją straci) a ona okaże się być niewinna to w tym momencie on zabierze coś, co Hektor i Siluria kochają.

Ozydiusz się strasznie zirytował na Silurię, gdy ta powiedziała, że zgadza się na jego warunki - ale Ozydiusz musi pójść z nimi i uczestniczyć w przeszukaniu. Miała dobre argumenty. Ozydiusz chciał wprowadzić na to miejsce Judytę Karnisz - LOL NOPE. To musi być on lub ktoś komu on bardzo ufa i jest w miarę neutralny.

I tak Ozydiusz, Hektor i Siluria poszli przeszukiwać domek Tamary Muszkiet.
Ozydiusz chciał wejść do domku Tamary. Drzwi zamknięte. Ozydiusz użył mocy lorda terminusa i zniszczył zabezpieczenia. 
W środku są 3 osoby. Mężczyzna, kobieta i dziecko. Zanim Ozydiusz zniszczył drzwi, kobieta założyła jeden z kombinezonów magitechowych Tamary i poszła walczyć z Ozydiuszem.
Ozydiusz widząc nieporadną "postawę bojową" zrobił minę pełną pogardy i zaatakował. Nawet mimo wspomaganego kombinezonu, Roksanie strzeliło żebro. Pod wpływem silnych emocji i strachu magitech się obudził i uruchomił. Nie mając maga z którego może czerpać zaczął czerpać chłepcząc krew Roksany. Jednak magitech jednynie wzmacnia umiejętności użytkownika - Roksana ich po prostu nie ma. Ozydiusz uderzył ponownie, po czym zorientował się, że ma do czynienia z człowiekiem. Zaczął rzucać death spell...
...co przerwała Siluria i Hektor - "nie dość dowodów!"

Ozydiusz użył swojej mocy by rozebrać Roksanę z magitech i jej nie zabić oraz by unieszkodliwić jej męża. Potem pozwolił na przesłuchanie tej dwójki przez Hektora, w swojej obecności. Nadal chciał ich zabić, ale ochłonął.

Hektor wyciągnął co następuje:
- Roksana jest siostrą Tamary; mężczyzna jest mężem Roksany a dziecko jest ich.
- Chwilę przed atakiem na poligon Tamara zerwała się, założyła magitech i pobiegła.
- Przez pewien czas, ostatnio, Tamara się źle czuła (to zgłosiła) i miała momenty paranoiczne.

Skany Silurii wykazały następujące informacje:
- aura uszkodzonej zbroi magitech na Poligonie KADEMu pasuje do osobistej zbroi Tamary. To była ona.

Ozydiusz powiedział, że zostanie świadkiem oskarżenia. Z uwagi na to, że Tamara nie zgłosiła swojej rodziny, że najpewniej była na Poligonie, że nic nikomu nie powiedziała i nadużyła zaufania świecy Ozydiusz chce, by Tamara została właściwie ukarana.
A najpewniej przestanie być terminusem.

Silurię bardzo zastanawia fakt, że w ciągu 2 tygodni Świeca traci dwóch terminusów...

# Streszczenie

Quasar wprowadziła lockdown i phase-out KADEMu by naprawić systemy zabezpieczeń. Siluria została na zewnątrz; wzięła ze sobą GS Aegis 0003. Hektor konferencją prasową (dla magów) przeciągnął wielu magów na swoją stronę; najlepsze wystąpienie w życiu. Iza Łaniewska zostanie przekształcona w Diakonkę. Siluria była świadkiem jak Saith Flamecaller zaciera ślady w supermarkecie (chroniąc Tamarę) przez GS Aegis. Hektor i Siluria z Ozydiuszem Bankierzem weszli do domu Tamary i odkryli, że Tamara chowała tam swoją "ludzką" rodzinę. Pojawiły się dowody, że magiem Kurtyny na Poligonie KADEMu była właśnie Tamara - ale kim był ten drugi? Tamara była już chora na Lodowy Pocałunek. Ozydiusz wyrzucił Tamarę ze służby (zaocznie). Nie jest już terminusem.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Park Centralny
                        1. KADEM Primus
                            1. Instytut Technomancji
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Pion dowodzenia Kopalinem
                                1. Biuro Lorda Terminusa
                    1. Dzielnica Kwiatowa
                        1. Domek Tamary Muszkiet
                    1. Dzielnica Trzech Myszy
                        1. Supermarket "Smakołyk"
 
# Zasługi

* mag: Siluria Diakon, która uruchomiła GS Aegis 0003 oraz odkryła najmocniej strzeżony sekret Tamary.
* mag: Hektor Blakenbauer, który przekabacił dziennikarzy na swoją stronę oraz uratował rodzinę Tamary przed Ozydiuszem.
* vic: GS "Aegis" 0003, głowica uruchomiona po raz pierwszy przez Silurię, która od razu musiała zacząć kłamać. Na razie grzeczna.
* mag: Quasar, która wprowadziła pełny lockdown KADEMu by przebudować system zabezpieczeń w coś, co na pewno zadziała.
* mag: Norbert Sonet, pilnujący by synchronizacja Silurii i 0003 została przeprowadzona poprawnie.
* mag: Marcelin Blakenbauer, który okazał się być mistrzem dobierania stroju dla Hektora.
* mag: Hieronim Maus, inkwizytor i prokurator z przeszłości, który zasłynął iście drakońskimi metodami i został legendą.
* mag: Liliana Sowińska, prokurator z przeszłości, która skupiała się na rehabilitacji i powrocie do społeczeństwa a nie karaniu.
* mag: Jolanta Sowińska, która bardzo interesuje się sprawą Izy Łaniewskiej i zadaje Hektorowi bardzo niewygodne pytania.
* mag: Salazar Bankierz, który po zażyciu Kryształu Zapomnienia doniósł Silurii i Hektorowi na Tamarę. Nie chce wojny gildii.
* mag: Saith Flamecaller, który skutecznie zaciera ślady po Tamarze. Podobno Saith nie żyją... jemu nie powiedzieli.
* mag: Ozydiusz Bankierz, lord terminus Kopalina, który jest bezwzględny i bardzo potężny. Nie ma żadnych oporów przed zabiciem * człowieka.
* mag: Tamara Muszkiet, która najpewniej jest chora; chwilowo MIA. Okazuje się, że ukrywa rodzinę przed Świecą.
* czł: Roksana Czapiek, siostra Tamary Muszkiet, ukrywająca się w domu Tamary. Uruchomiła * magitech by chronić rodzinę.
* czł: Wojciech Czapiek, mąż siostry Tamary Muszkiet, ukrywa się w domu Tamary.