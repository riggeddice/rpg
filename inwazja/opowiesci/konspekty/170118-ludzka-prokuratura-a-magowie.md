---
layout: inwazja-konspekt
title:  "Ludzka prokuratura a magowie"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170115 - Klub "Dare Shiver" (SD)](170115-klub-dare-shiver.html)

### Chronologiczna

* [170111 - EIS na kozetce (SD, PS)](170111-eis-na-kozetce.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

### Dzień 1:

Artur Bryś odwiedził Hektora Blakenbauera. Ten się naprawdę, naprawdę zdziwił; co tu Bryś robi? A Bryś powiedział, że był w Kotach u koleżanki. I tam jechało auto Skubnego. I dawał narkotyki. Hektor dopytał o co chodzi... Bryś się przyznał, że interesuje się tą dziewczyną. Chciał ją porozbierać, ale ona była czysta. Po bardzo... specyficznej rozmowie Bryś obiecał Hektorowi, że dostarczy mu narkotyki. Hektor kazał wysłać Dionizego i Alinę jako wsparcie. Bryś bardzo protestował, ale Hektor był nieubłagany.

Artur, Alina i Dionizy spotkali się na odprawie.

* Czaisz? Skubny ROZDAJE narkotyki - Bryś, zbulwersowany
* Skubny w narkobiznesie? - zdziwiony Dionizy

Szymon Skubny rozdaje narkotyki. Tzn. tiktaki. Tzn. jego ludzie. I normalna, małomiejska agresja zanikła. Nikt nie chce wpierniczyć sobie sztachetą.

"I wiecie, na bazie moich obserwacji... Skubny i Blakenbauer mają się ku sobie. Wiesz, patrzą na siebie cały czas..." - Brysia teoria miłości

* Słuchaj, to jest tak, nie! Ja mogę zdobyć tą próbkę. Zabiorę mojej kumpeli. Nie zauważy. - Bryś, broni się...

W końcu ustalili, że Bryś ma zabrać od ludzi Skubnego próbkę narkotyków w kolejce. A Alina i Dionizy osłaniają. Bryś powiedział też, że ma blachy tego auta. (-1 surowiec Dusta), Patrycja Krowiowska znalazła auto i je cały czas namierza. Po telefonach.

Dionizy i Alina nie do końca rozumieją narkotyki love&peace. Zwłaszcza u Skubnego, który się nie bawi narkotykami. Ekipa wsiadła i pojechała do Kotów. Tam pochodzili po ryneczku... Dionizy szuka swoich kontaktów wśród lokalnych mend. (-1 surowiec: Dusta, menda społeczna: Witold powołany). Spotkał się z Witoldem, któremu kiedyś pomógł gdy ten miał kłopoty. Witold kiedyś był informatorem policji i jego zwierzchnicy o tym wiedzieli i Dionizy był tam by ich skroić.

* Mam tani złom... - Witold, próbujący skusić Dionizego do zakupu czegoś
* Porozmawiajmy o złomie - Dionizy - Słuchaj Wiciu, potrzebuję informacji...
* To też mogę sprzedać. - Witold

Dionizy przycisnął Witolda; ten się boi "grubej ryby" (4v3->S). Witek wszystko mu powiedział...

* Coś tu się faktycznie pojawiło. Nie wie co kto kiedy i jak.
* Jego (Witka) nie było. Wrócił, wszyscy się kochają. Wczoraj wrócił. Wszystko jest nie tak...
* Masowa halucynacja? Wszyscy się dziwnie zachowują...
* Rozwożą to czarną wołgą... on nie wie skąd się to bierze. 
* WSZYSCY ćpają. Wszyscy. Dzieci, dorośli...
* Rozdawane są sześciopaki tiktaków.

Dionizy kazał Witoldowi dać mu próbkę krwi. Dał. Alina dostała próbkę krwi Witolda; ona ma odebrać jako Witold próbkę. Brysiowi wyjaśnili, że kazali żulowi zabrać próbkę... "człowiek Dionizego" (Alina) pozyska narkotyki. Bryś powiedział Dionizemu, że jest w internecie ruch, który próbuje udowodnić "teorię parówczanego Blakenbauera" - że Hektor interesuje się facetami a na pewno Skubnym ;-). Bo przecież jest taki twardy ;-).

Alina wróciła. Jako Witold. Bryś postraszył, Dionizy dał polecenia. Alina ma zdobyć narkotyki z auta Skubnego.

Poszła jako Wiciu, wzięła z auta "tiktaki" i została zaatakowana atakiem mentalnym. Magia. Alina wzięła i zażyła "tiktaka". Przyszła do Dionizego i Artura. 

* Dawaj narkotyki. - Bryś.
* ??? - Alina, zdziwiona, nie wie o co chodzi i nic nie pamięta
* No... narkotyki. Tiktaki. - Dionizy
* Kolego, pokażę Ci kulturalnie jak to się robi - Bryś, atakując "Wicia" (Alinę)

Konflikt, 4v4->R. Bryś rozkwasił Alinie nos; Dionizy go zatrzymał. Dionizy zauważył, że Alina połknęła; wsadził jej dwa palce i poczekał na wymioty. Niestety, atak mentalny i magiczny... Alina ogólnie jest w dobrym humorze i przyjaźnie nastawiona do świata. Jest pięknie.

* Patrz na to, to dupa a nie człowiek - Bryś o Alinie (myśląc że to zarzygany żul)
* Biorę to ze sobą. Mój żul. - załamany Dionizy...

Alina pod wpływem i powiedziała Brysiowi, że ma fajny tyłek. Ten się wkurzył - to Hektor interesuje się facetami, nie on. Chciał nawalić Alinie; Dionizy go powstrzymał i próbował uspokoić -> F. Bryś się uspokoił, Witold dostanie wpiernicz po misji (za niewinność)...

* Nie nie nie, posłuchaj tego, nie: żul powie 'ale seksi dupa' Hektorowi. Hektor powie 'też jesteś fajny'. I się pocałują. - teoria Brysia przesłuchania "Witka"
* NIe no Bryś, słuchaj... rozumiem że tworzysz teorię między Blakenbauerem a Skubnym... ale nie wyobrażasz sobie by Hektor przybijał do żula... Gdzie tu romantyzm, Archie? - Dionizy
* Wiesz... jak prokurator wyposzczony, to i żul wygolony - filozoficzna sentencja Brysia.

Bryś sobie poszedł, Dionizy widzi szczęśliwą Alinę w bagażniku. I trzeba do Edwina... (-1 surowiec, Kić). Edwin przyjął Dionizego i Alinę... ozięble. Kazał się Alinie rozebrać i poszedł z nią przez Rezydencję; tam spotkali Hektora a Alina (jako żul) na szyję. Hektor urażony. Edwin powiedział, że to przyjaciel Hektora. Ten poszedł się wykąpać.

"Nowy mem. Hektor nie ma kija w dupie. To wibrator..." - Dust

W laboratorium Edwin powiedział Alinie i Dionizemu, że ten narkotyk nie uzależnia. Zaklęcie na Alinie kazało jej brać to 10 dni. W tym czasie augmentacja by jej się rozsypała. Edwin wnioskuje, że to albo pułapka, albo próba kontroli eksperymentu, który NAPRAWDĘ się nie udał. Tak czy inaczej, nic groźnego... chyba, że jest się viciniusem.

Hektor odwiedził Borysa i chciał info skąd nagi facet w Rezydencji. Borys zaoferował Hektorowi zbliżenia i zdjęcia nagiego żula, jedynie wzmacniając Parówczaną Teorię Hektora... który był bardzo zniesmaczony tym pomysłem.

"Narkotyk" jest nieuzależniający i w świecie ludzi będzie wykryty jako coś "normalnego". Zafałszowane tiktaki... a Hektor pozna prawdę - coś Dziwnego...

### Dzień 2:

Hektor zażądał Dionizego, Artura i Aliny. Przyszła cała trójka. Zdali raport - jest próbka, udało się osiągnąć zadane cele. Zgodnie z raportem, to są PODRABIANE tiktaki.

* Czyli mamy tu do czynienia z podrabianymi tiktakami. Rozdawanymi za darmo przez Skubnego... - Hektor, zaskoczony
* Przestępstwo gospodarcze szefie... - Bryś, broniąc się rozpaczliwie
* Won do archiwów segregować dokumenty - Hektor, zirytowany

Hektor odesłał swoją grupę specjalną. Zadzwonił do niego Edwin i wyjaśnił, o co chodzi... że to jest coś powiązanego ze światem magów. I nie, Edwin nie ma pojęcia co się dzieje w Kotach. Jemu nic to nie mówi. Ale to sprawa w świecie magów...

* "Powiedz Brysiowi, że ma się trzymać z daleka od Kotów. To rozkaz." - Hektor do Aliny
* "Tak jest" - Alina

Hektor wrócił w końcu do Rezydencji i ściągnął Marcelina (-1 surowiec). Dał mu te "narkotyki" i poprosił, by on się dowiedział od swoich przyjaciółek o co może chodzić. Od magów, ogólnie. I Hektor nie wie, że to będą przyjaciółki... Marcelin zrobił małe badania (7v8) -> F, F, ; Marcelin obiecał Inferni, że ją oprowadzi po Rezydencji (pokaże jej Rezydencję). Obiecał też, że nie zdradzi o co i o kogo chodzi; Infernia będzie tą, która będzie na forpoczcie, ale może porozmawiać z Hektorem czy Marcelinem o co chodzi i jak to wyszło...

Więc, Infernia Diakon...

Marcelin, wieczorem, powiedział Hektorowi, że ma informacje na ten temat. Zaprosił czarodziejkę KADEMu, która z przyjemnością opowie mu o tym o co chodzi.

* To mnie pocałujesz? - Infernia, lekko już wstawiona (popija )
* Ale jeszcze... jeszcze nic mi pani nie powiedziała... - Hektor, zaskoczony
* Nie mam bielizny - rozpromieniona Infernia - Teraz mnie pocałujesz?

Infernia wyjaśniła Hektorowi, że magowie Świecy zrobili bubu ludziom (Rukoliusz Bankierz) a jej przyjaciele to korygują. Znaleźli jakichś handlarzy / dilerów i ich dostosowali do skorygowania efektów Świecowych. Oprócz tego Infernia popija syberion i próbuje pocałować Hektora. Chce być pierwszą pijaną Diakonką w Rezydencji która coś przeleci... achievement##.

Hektor wyciągnął z tego:

* Infernia jest DZIWNA; jakby sama była na jakichś narkotykach
* Infernia nie ma nic do ludzi, ale używa ich jak uważa za słuszne (ludzie Skubnego też ludzie)
* Terminus Rukoliusz Bankierz stoi za tą sprawą; za problemem
* Marcelin ma DZIWNE przyjaciółki

### Dzień 3:

Hektor skłonił Marcelina by ten poszedł z nim do Rukoliusza. Ale Marcelin ma prawo przerwać rozmowę, by nie było problemów...

Kompleks Centralny. Rukoliusz, w restauracji spotkał się z Hektorem. Dzięki Marcelinowi i jego odpowiedniemu zachowaniu i Hektorowi i jego chytremu podejściu Rukoliusz powiedział, że to faktycznie mag Świecy robił eksperymenty i KADEM to czyści. Tak, maga Świecy spotkała kara. Nie, nie chodzi o ludzi. Rukoliusz potraktował Hektora jako jednego z "tych ludzistów". Nie, Rukoliusz nie powie Hektorowi kto zawinił. Nie ma po co. Nie ma powodu. Świeca zajmie się karą sama.

Gdy już się rozstali, Marcelin opowiedział i wyjaśnił Hektorowi jak wygląda relacja ludzi i magów. Wyjaśnił jak działa Świeca, terminusi i te wszystkie luźne struktury. Hektor zdecydował się doedukować, by takich sytuacji więcej nie było... lub by wiedzieć, co robić. I pomógł KADEMowi - niech ludzie nie kręcą się dookoła Kotów; te 5 dni Infernia dostanie, by wyczyścić ludzi...

Poza tym, że Bryś pojedzie pobić niewinnego Witka XD.

# Progresja


# Streszczenie

Bryś wykrył dziwne działania "narkotyków" Karoliny z KADEMu. Zgłosił to Hektorowi; Dionizy i Alina wsparli Brysia i Alina wpadła w środki psychoaktywne. Hektor wpierw porozmawiał z Infernią (która się spiła i wymusiła pocałunek na Hektorze w Rezydencji; to był warunek) a potem poznał opowieść od Rukoliusza dzięki Marcelinowi. Bardzo zniesmaczony, że magowie wykorzystują sobie tak ludzi...

# Zasługi

* mag: Hektor Blakenbauer, o którym rozpuszczana jest parówczana teza; zleca i zamyka śledztwo (dotyczy świata magów), z Marcelinem przenosi dochodzenie do świata magów. Jest rozczarowany podejściem magów do ludzi.
* vic: Alina Bednarz, przebrana za żula dała się złapać w zaklęcie mentalne KADEMu, zażyła narkotyk, przytulna wobec Hektora i naprawiona przez Edwina. Bryś rozkwasił jej nos (jak była żulem).
* vic: Dionizy Kret, nawiązuje kontakt z cinkciarzem Witoldem, ratuje Alinę przed efektami narkotyków (i Brysiem), po czym jako jedyny wychodzi niepokiereszowany z zachowanym honorem z akcji.
* czł: Artur Bryś, człowiek Hektora który nienawidzi narkotyków. Polubił jakąś dziewczynę w Kotach; zgłosił Hektorowi narkotyki, pobił Witka (za niewinność; Alina była zań przebrana). Ma Parówczaną Teorię Hektora.
* czł: Patrycja Krowiowska, doskonała informatyczka; ma namiary na czarną wołgę Skubnego i wie, gdzie ona się znajduje (przez lokalizację telefonów właścicieli samochodu)
* czł: Szymon Skubny, "biznesmen" i przestępca, który jednak nigdy nie interesował się narkotykami. Stąd zdziwienie wszystkich, że jego ludzie "dilują".
* czł: Witold Wcinkiewicz, cinkciarz i menda społeczna; kontakt Dionizego, który był poza miastem i który boi się dziwnych narkotyków i ludzi za tym stojących
* mag: Edwin Blakenbauer, który bezwzględnie żartuje sobie z Hektora, analizuje narkotyki w Kotach, regeneruje Alinę i ogólnie zarządza kryzysem ;-).
* mag: Marcelin Blakenbauer, mający mnóstwo przyjaciółek (min. Infernię na KADEMie), pomagający Hektorowi zdobyć informacje od Inferni i od Rukoliusza. Społeczny i pomocny.
* czł: Borys Kumin, który z radością wrobi Hektora w obraz "Parówczanej Teorii Hektora". Podejrzewamy, że hostuje stronę "PTH".
* mag: Infernia Diakon, która ukryła działania Karoliny Kupiec przed Blakenbauerami i została frontgirl. Upiła się w Rezydencji, zmusiła Hektora do pocałunku i skończyła z Marcelinem. Achievement ;-).
* mag: Rukoliusz Bankierz, który zupełnie nie rozumie ludzistycznego podejścia Hektora. Ukrywa informacje o działaniach magów Świecy przed Hektorem. Lubi Marcelina.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Rynek, gdzie rozdawane są przez agentów KADEMu "tiktaki" - środki regenerujące ludzi i gdzie Dionizy spotkał się z Witkiem
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy 
                            1. Pion ogólnodostępny
                                1. Restauracja Dyskretna, miejsce spotkania Hektora, Marcelina i Rukoliusza
                        1. Prokuratura, gdzie formalnie i oficjalnie do Hektora przyszedł Bryś
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, gdzie Marcelin oprowadził Infernię (a potem Infernia się upiła)

# Czas

* Dni: 3