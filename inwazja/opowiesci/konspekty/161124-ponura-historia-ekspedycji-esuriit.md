---
layout: inwazja-konspekt
title:  "Ponura historia ekspedycji Esuriit"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161109 - Jak prawidłowo wpaść w pułapkę (HB, SD)](161109-jak-prawidlowo-wpasc-w-pulapke.html)

### Chronologiczna

* [161109 - Jak prawidłowo wpaść w pułapkę (HB, SD)](161109-jak-prawidlowo-wpasc-w-pulapke.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Nadszedł czas zbierać zasoby. SUPER! Jest czerw, są zasoby! Można zbierać elementy czerwia. Zgodnie z zasadą all decks on board Ignat szykuje wozy transportowe i zbiera się czerwia. Marianna wydała rozkazy swoim magom do zbierania kawałków czerwia i przesłania ich do Bazylego Weinera. To samo z połapanymi krystaperze. Marianna sama zaczęła składać łańcuchy logistyczne i wymuszać na swoich magach szybsze i energiczniejsze działanie. Energia musi się znaleźć - TERAZ.

Siluria po lekkiej utarczce kazała Ignatowi pomóc Mariannie zabezpieczyć lepiej bazę. Kometor jest świetny... ale za dużo energii zżera. Ignat narzeka, że musi pracować z tienowatymi. Silurii udało się skutecznie przymusić go by jednak to zrobił. Niech będzie... Ignat ma okazję udowodnić, że KADEM Engineering > Świeca Engineering.

"Nie zostawię cię. Potkniesz się i zabijesz." - Anna, śmiertelnie poważnie, do Hektora

Hektor się nie patyczkował. Kazał Annie to zrobić i poszła.

Hektor i Andżelika poszli do Archiwum. Dostali upoważnienie od Marianny.
Siluria i Mikado poszli wpływać na Łukiję i zacząć budować korzystny Pryzmat razem z nią.

Hektor spytał Mariannę jaki jest stan zabezpieczeń bazy. Marianna powiedziała, że cały czas monitoruje. Hektor przytaknął mądrze... a potem kazał dyskretnie Annie to sprawdzić. Anna cicho zaakceptowała i poszła szukać. Znalazła coś... niepokojącego i dała znać Ignatowi. Wraz z Ignatem znaleźli coś, co bardzo ich zmartwiło...

Archiwa są małe, ale prowadzone w poprawny sposób. Andżelika bez problemu złamała kod archiwów - typowe dla Świecy. Zofia Weiner, złapana przez Hektora, chce pomóc. Nieco z rezerwą, ale chce.
Andżelika po szybkiej analizie powiedziała, że brakuje jej w papierach dwóch rzeczy. Po pierwsze, co się stało gdy pojawiło się pole defensywne. Speculoid się wyrwał. Nie ma opisu jak do tego doszło i o co chodzi. Po drugie, nie ma nic o Kazimierzu Sowińskim.

Hektor nacisnął na Zofię. To niebezpieczne, Andżelika się martwi a oni mają autoryzację.
Zofia jest Strażnikiem Tajemnic. Ale... złamała się.

Ta placówka służyła do tego, by móc... oni porwali speculoida z fazy Esuriit, po czym dostosowali go do fazy Primus. Opracowali "pijawki" by móc funkcjonować w fazie Esuriit. Wszystko dobrze działało do momentu aż doszło do kolapsu portali. Wszystkie portale z Świecą Daemonica umarły. Niedomiar energii. Speculoid wyrwał się na wolność i...
Ten speculoid jest infuzowany energią Kazimierza Sowińskiego. Może żyć w każdej fazie.

Kazimierz zostawił rozkaz Łukiji. Ona wprowadziła Fiodora Maiusa w wieczny koszmar; to sprawiło, że Faza Daemonica nie traci energii. Ale mocy było za mało, więc Marianna poświęciła Kazimierza w rytuale Magii Krwi. Jako, że dziecko nie rozumie konceptu śmierci, Kazimierz stale krwawi energią. Nie jest w stanie umrzeć, tak jak poświęceni w rytuale Magii Krwi także nie są w stanie umrzeć i wiecznie zasilają energię Daemonica... dlatego Kazimierz infuzowany energią speculoida (tak, też się stało) jako Energiak Esuriit nadal istnieje... i nadal cierpi.

Nie mogą złamać koszmaru dziecka.

Co więcej, Zofia powiedziała, że część magów zmieniła się w Energiaki. Koszmar Fiodora nie wystarczył; Faza Esuriit się powoli adaptuje do koszmaru. A speculoid i Kazimierz są dwoma jedynymi bytami zdolnymi do bezproblemowego ogarniania tej rzeczywistości.

Andżelika jest biała. Zofia powiedziała, że najpierw myślała, że oni są majakami dziecka, ale dziecko nie wymyśliłoby Opla Astra... a już na pewno nie wpadłoby na KOMETOR.
Hektor zaczął budować profil psychologiczny Marianny Sowińskiej i Pawła Mausa...

Hektor dowiedział się też, że faza Esuriit nie może "znaleźć" tej rzeczywistości, bo żyją w koszmarze - bez nadziei, bez pamięci. Pojawienie się jakichś uczuć pozytywnych sprawi, że Esuriit może wyczuć tą rzeczywistość i ją zaatakować...

Siluria z Mikadem poszli do nieszczęsnej Łukiji. Po lekko burzliwej dyskusji stanęło na tym, że Łukija pójdzie rozprzestrzeniać Pryzmat jeśli Siluria zrobi ruchome łóżko jeżdżące z nią. Takie, by Łukija mogła rozprzestrzeniać pozytywny Pryzmat świadczący o Zamku As'caen i jednocześnie by Fiodor był cały czas z nią.

Mikado i Siluria poszli z nią. Siluria dawno już nie widziała takiej ludzkiej tragedii - tutejsi ludzie są wymęczeni, wykończeni, porwani. Bez nadziei, wiedzą, że są zabijani... nie wierzą że coś się zmieni na lepsze. Czeka Łukiję, Silurię i Mikada sporo pracy...
Mikado robi pokaz. Skupia się, by zrobić jak najlepsze wrażenie i efekt. Dać im nadzieję. Pokazać, jak wygląda taki zamek. Siluria mu pomaga...
Kosztem części energii udało im się to uzyskać. Jest nadzieja na lepszy Pryzmat.

Z Hektorem kontaktuje się Anna a z Silurią Ignat. Ostrzegają, że od strony cmentarza FAKTYCZNIE doszło do sabotażu działek. Ktoś, kto dogłębnie wie jak działają działka Świecy.
...Kazimierz... Trzeba by wziąć i powiedzieć Mariannie. Sęk w tym, że ona i Kazimierz uczyli się u tych samych magów.

Pozyskiwanie energii z Esuriit trwają powoli, ale wystarczająco szybko by następnego dnia móc zacząć bronić bazy przed Fazą Esuriit.

Hektor i Siluria poszli porozmawiać z Marianną. Ta jest zmęczona; twierdzi, że wystarczy im energii na następny dzień. Hektorowi się rzuciło w oczy, że to się nie sumuje. Marianna nie jest w stanie dostarczyć takiej ilości energii... więc ma dodatkowe źródło. Jakieś. Najpewniej cholernie krwawe...
Hektor nacisnął na Mariannę lekko i ona spojrzała nań jak na idiotę. OCZYWIŚCIE że ona użyje magii krwi na ludziach.
Hektor się nie zgodził. Marianna poprosiła o to, by zaproponował jej inne źródło energii.
Hektor zaproponował siebie.
Siluria poszła na kłótnię z Hektorem. Ona się NIE zgadza. Anna ucierpi. ONA ucierpi. Hektor jest twardy - to kwestia jego ideałów.

Marianna patrzy z lekkim zdziwieniem jak wygląda sytuacja. 

Hektor się lekko złamał. Dał się przekonać, że będą mogli stąd uciec i pomóc większej ilości ludzi... jakimkolwiek ludziom. Marianna zaproponowała mu, że gdy Hektor ich wyciągnie, ona założy fundację (do 50 osób) ratującą ludzi przed efektami magicznymi, dla pocieszenia Hektora.

Gdy Marianna dowiedziała się, że działka koło cmentarza są sabotowane, powiedziała, że niebezpieczne jest palenie zwłok. Dostarczenie energii - jakiejkolwiek - pozwala na potencjalną reanimację Energiaków Esuriit. Sabotaż działek implikuje, że Kazimierz jest w stanie wejść do tej bazy. Po prostu tego nie robi. 
Hektor wymyślił, by zamiast WYSADZAĆ cmentarz, oderwać wyspę na silniczkach.

Hektor wpadł na pomysł, by do odcinania wyspy użyć Dosifieja Zajcewa. Jest magiem Esuriit; to nie ma znaczenia. Łatwiej zaakceptuje to Faza. Marianna wydała rozkaz i Dosifiej zgodził się na odcięcie wyspy.
Ignat z pomocą Silurii dał radę złożyć silniczki kosztem starego łazika. Mikado wprowadził tam zdalne sterowanie by zapewnić, że wyspa NIE WRÓCI.

Dosifiej odciął wyspę, która wdzięcznie oddaliła się od Srebrnej Septy.
Marianna naprawiła działka...

# Progresja


# Streszczenie

Hektor i Andżelika badali archiwa Świecy i dowiedzieli się co się STAŁO na tej Ekspedycji. Ona nadal istnieje, bo brat Marianny został poświęcony w Rytuale Krwi a dziecko - Fiodor Maius - tworzy koszmar dookoła nich wszystkich Pryzmatem. I dzięki temu Faza Esuriit nie może ich znaleźć. Hektor chciał się poświęcić dla kilku ludzi w Rytuale Krwi, ale Siluria to zatrzymała. Dodatkowo, okazało się, że działka chroniące drogi na cmentarz są sabotowane - najpewniej przez Kazimierza Sowińskiego; naprawiła je Marzena po wykryciu tego przez Ignata i Annę. Siluria i Mikado pracują nad Pryzmatem pozytywnym. Marianna obiecała, że jak wrócą, powstanie fundacja pomagająca ludziom dla Hektora. By zapobiec atakowi zmarłych (energia # zwłoki == Energiak Esuriit), Dosifiej odciął kontakt z Wyspą Zmarłych rozbijając leyline.

# Zasługi

* mag: Hektor Blakenbauer, wyciągający prawdę z papierów i * magów, który chciał poświęcić się ratując ludzi. Ma mnóstwo przydatnych planów.
* mag: Siluria Diakon, powstrzymująca Hektora przed poświęceniem się w rytuale * magii Krwi by ocalić ludzi. Też: wzmacnia Pryzmat by sprowadzić Zamek As'Caen.
* mag: Andżelika Leszczyńska, która z Hektorem poznała prawdę z archiwów. Jest przerażona podejściem i bezwzględnością Świecy.
* mag: Mikado Diakon, w roli uduchowionego warrior monka podnoszącego ludziom morale i entertainera bardziej niż terminusa.
* mag: Anna Myszeczka, która chroni Hektora i odkrywa luki w obronie Świecy. 
* mag: Ignat Zajcew, który nie chce pracować z tienowatymi i z pomocą Silurii poświęcił łazika by móc odepchnąć Wyspę Zmarłych
* mag: Marianna Sowińska, która podjęła mnóstwo mrocznych decyzji by uratować Ekspedycję i jest zdeterminowana, by wszyscy wrócili do domu.
* mag: Zofia Weiner, Strażnik Tajemnic; odpowiadała za utrzymanie ekspedycji naukowej i wejście Świecy na Esuriit. Wie dokładnie co się dzieje.
* vic: Kazimierz Sowiński, kiedyś: brat Marianny. Teraz: energiak Esuriit, bardzo groźny, z współinfuzją energii Siriratharina; sabotuje Srebrną Septę.
* mag: Fiodor Maius Zajcew, dzieciak, który żyje w wiecznym koszmarze i którego koszmar utrzymuje całą Ekspedycję przy życiu 
* mag: Łukija Zajcew, która (kiedyś) wpierw wpędziła Fiodora w nieskończony koszmar by wszyscy przeżyli i od tej pory czuwa przy ukochanym dziecku
* mag: Dosifiej Zajcew, który rozerwał połączenie Wyspy Zmarłych - a dokładniej Cmentarza z resztą bazy.
* vic: Siriratharin, nadal wyłączony z akcji. Okazuje się, że potrafi przeżyć i na Primusie i Esuriit i Daemonice; sprzężony z Kazimierzem Sowińskim.

# Lokalizacje

1. Świat
    1. Faza Esuriit
        1. Insula Luna
            1. Srebrna Septa
                1. Centrum
                    1. Centrala Świecy
                    1. Blockhaus K 
                    1. Archiwum 
                    1. Kometor
                1. Skrzydło szpitalne
                    1. Sale medyczne
            1. Wyspa Zmarłych
                1. System defensywny
                1. Cmentarz
  
# Skrypt

# Czas

* Dni: 1