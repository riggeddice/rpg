---
layout: inwazja-konspekt
title:  "Sophistia x Marcelin"
campaign: druga-inwazja
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140219 - Niespodziewane wsparcie(AW, WZ)](140219-niespodziewane-wsparcie.html)

### Chronologiczna

* [140219 - Niespodziewane wsparcie(AW, WZ)](140219-niespodziewane-wsparcie.html)

## Misja właściwa:

Niespodzianka! W domu Blakenbauerów doszło do kolejnej kłótni o Sophistię. Tym razem Edwin się pokłócił z Marcelinem - po tym jak Hektor powiedział Alfonsowi by rozwiązał problem z Sophistią ten zamknął ją w komórce. Naprawdę. Sophistia z oczywistych przyczyn tego NIE doceniła, ale tym razem nie poskarżyła się Marcelinowi, tym razem się ulotniła. No i Marcelin cały wściekły poszedł wygarnąć Edwinowi co o tym myśli, usłyszał kilka słów od Edwina i się zafoszył, poszedł się dąsać do swojego pokoju.

...i mniej więcej na to wszedł Hektor, zmęczony po dniu ciężkiej pracy. Już od wejścia przywitał go Edwin, który potrzebował pomocy Marcelina w laboratorium, ale od rana Marcelin się do niego po prostu nie odzywa. Marcelin zamknął się w pokoju i nie reaguje na głos Edwina. Nikomu nie chce otworzyć... ogólnie, fail. Niech on, Hektor, coś z tym zrobi. Bo Edwin ma niesamowitą teorię dotyczącą Margaret i jeśli to prawda to w ogóle będzie super...

Oki. Hektor poszedł porozmawiać z Marcelinem, ale ten nie odpowiada. Zupełnie. Hektor się troszkę zmartwił - poprosił Borysa o spojrzenie na obraz z systemu zabezpieczeń na pokój Marcelina i dostał odpowiedź niepokojącą - Marcelin leży na ziemi, otoczony jakimś dziwnym polem siłowym. Hektor wdarł się do pokoju Marcelina, ale nie jest w stanie przedostać się przez to pole. natychmiast wezwał Edwina, który rozpoznał pole stazy ale też nie jest w stanie nic poradzić...

Oki. Stanęło na wezwaniu Andrei. Edwinowi się to bardzo nie podoba, ale on nie może pomóc a Marcelin jest jego ukochanym bratem. Zgodził się, choć niechętnie. A Andrea została wezwana jako magiczny analityk, nie detektyw.

Andrea pojawiła się tak szybko jak tylko była w stanie. Zaraz zabrała się do pracy nad tym tajemniczym polem siłowym i szybko udało jej się znaleźć rozwiązanie problemu: jest to artefakt ratunkowy dwuczęściowy. To znaczy: jest artefakt w dwóch częściach; jedną ma Sophistia, drugą Marcelin. Jeśli jedno z nich ulega śmiertelnemu zagrożeniu, artefakt się włącza i otacza obu polem stazy. Czerpie z Marcelina, bo jest jedynym magiem. Oczywiście, ma zdjęte wszelkie ograniczenia i zabezpieczenia, ale nadal nie jest w stanie skrzywdzić Marcelina... tyle, że Sophistia właśnie umiera i umrze na pewno, jeśli ktoś jej nie uratuje.

Oki, został wezwany Wacław do pomocy Andrei. A Hektor przekonał Edwina, że musi pomóc Sophistii. Po pierwsze, ktoś kto potrzebuje pomocy (no i Hipokrates). Po drugie, inaczej Marcelin nigdy mu nie wybaczy.

Dobrze. Wiedzą więc, że Sophistii coś grozi ale nie wiedzą gdzie jest i gdzie była. Zbierają trzy główne siły: ludzkich wywiadowców Hektora, Ikę (dziewczynkę tropiącą) i Waldemara Zupaczkę. To jest, taki był plan skanu i zdobywania informacji, ale wpierw Hektor musiał się zmierzyć z Waldemarem Zupaczką który marudził, że nikt mu nie zapłacił za znalezienie psa (misja dawno temu, z Margaret która uciekła z rezydencji). Hektor próbował tłumaczyć, że przecież to nie on go wynajął, że tak się nie godzi ale Zupaczka jęczał, że pies był zgubiony i się znalazł a on prawie go przyprowadził tylko policja się na niego uwzięła. No i Hektor zapłacił za tego psa, Zupaczka wymarudził swoją nagrodę.

I jednak to był strzał w dziesiątkę. Ustalili wspólnymi siłami, że Sophistia jest gdzieś pod wpływem ogromnego nacisku mentalnego, ktoś ją czyta i próbuje skrzywdzić mentalnie. Dodatkowo jest dokładnie skanowana, jej pamięć jest głęboko przeszukiwana i archiwizowana. Poznali też jej lokalizację - znajduje się w jednym z magazynów wykorzystywanych przez terminusów SŚ do robienia złych rzeczy, pod jurysdykcją tien Agresta. A skąd Sophistia się tam wzięła? Ano, wyszła z "Attiki" w towarzystwie tien Dariusza Kopyto, terminusa, który jest znany z tego że Sophistii nienawidzi (jakaś zaszłość z przeszłości).

Super. Po prostu super.

Andrea rzuciła się na Dariusza Kopyto (wiedzę na jego temat). To samo Hektor. Udało się powiązać go w przeszłości z grupką do jakiej należała Sophistia, z której większość zginęła (jedyny który przeżył siedzi w więzieniu za zabicie innego). Poza tym nie występuje specjalnie w świecie ludzi. W świecie magów, kompetentny terminus, choć wymaga ścisłego nadzoru ze strony Agresta, bo ma "anger issues". Niewiele więcej mają...

Czas na konfrontację z Dariuszem Kopyto. 

Weszli tam do tego magazynu w trójkę - Hektor, Andrea i Wacław. Hektor szybko objął Sophistię swoją opieką, na co Kopyto odparł, że nie ma tu żadnej władzy. Nie jest magiem SŚ a ona jest tylko człowiekiem. Szybko Wacław podpowiedział, że Marcelin jest magiem SŚ, na co Kopyto odparł, że jego tu nie ma. Po odrobinie przepychanek, zastraszania, gróźb i negocjacji stanęło na tym, że Dariusz Kopyto ma prawo dalszego czytania i skanowania Sophistii, ale nie ma prawa jej uszkodzić bo jeśli to zrobi to Agrest zje go żywcem.

Gdy przedstawiono mu sytuację pomiędzy Marcelinem a Sophistią (wspólne pole, drenujące Marcelina) Dariusz oklapł. Nie może nic zrobić by ją skrzywdzić a teraz on krzywdzi maga SŚ, więc musi się pozbyć pola stazy. Ale tu jest kolejny problem - Dariusz nie zgadza się na przeniesienie Sophistii gdzieś indziej, a Edwin jest w rezydencji. Udało im się przekonać Dariusza, że będą współpracować ale niech reszta dyskusji odbędzie się w rezydencji Blakenbauerów gdzie Edwin będzie mógł pomóc Sophistii. Słysząc brak entuzjazmu w głosie Hektora Dariusz zaproponował, że przekształci Sophistię "głęboko" by nie zależało jej na Marcelinie i by rozerwać ten związek, ale - walcząc ze sobą - Hektor odmówił (mój brat musi sam decydować).

By dojść do tego, skąd tak głęboka nienawiść Dariusza do Sophistii, Wacław poszedł z nim porozmawiać sam na sam. Facet z facetem, bojowy z bojowym. Uzbrojony w wiedzę o Dariuszu i w mindlink z Andreą. I Dariusz pękł, przesłuchany przez Zajcewa - lata temu, podczas tej akcji z tą grupą od Sophistii szedł spokojnie i oni go zaatakowali. Bawili się z nim. Złamali go, był bezradny a oni go torturowali pod wpływem alkoholu. No i ich eliminuje, jeden po drugim - to nie są ludzie, to potwory, a terminusi powinni polować na potwory.

To wygląda identycznie jak ta akcja z Tatianą!

Wacław przedyskutował to z Dariuszem i przekonał go do współpracy. On (Dariusz) nie był jedyną ofiarą tego wszystkiego, takich jak on jest więcej a ludzie którzy mu to zrobili byli pod kontrolą. Dariusz nie będzie już polował na Sophistię. Efektem tego jest jednak to, że Sophistia jest przypisana jako własność Marcelina i Marcelin (oraz Hektor) są za nią odpowiedzialni. Jest uziemiona - nie może się ruszyć przez pewien czas, dla bezpieczeństwa wszystkich zainteresowanych.

Dariusz ma przeskanowaną zarówno strukturę jak i pamięć Sophistii. Jeszcze nie wszystko przerobił i sprawdził, ale już natknął się na kilka interesujących go rzeczy, min. odwołanie do projektu "Żywy Aderialith" - coś, co brzmi jak Inwazja.

Dane o projekcie "Żywy Aderialith" jakie udało się zebrać drużynie:
- Sophistia słyszała wiersz
- Ma jakieś powiązanie z Zajcewami
- Sophistia trafiła na coś supertajnego; Dariusz o tym słyszał, ale tylko Agrest wie o co chodzi
- Andrea widziała jakieś papiery na linii tien Żupan - Krystalia Diakon

Dodatkowo, podczas tej misji stało się coś jeszcze. Doszło do próby dostania się do Trzech Czarnych Wież SŚ. Napastnik sforsował system zabezpieczeń, przemknął się mimo niego, ale pułapki zastawione przez paranoicznych terminusów frakcji Izolacjonistycznej go spopieliły. Kapituła podjęła decyzję o przebudowie systemu zabezpieczeń na system Krówek.

![](image 140227_SophistiaXMarcelin.png)

# Zasługi:

* mag: Hektor Blakenbauer, który przezwyciężył swoje wewnętrzne 'fuj' do Sophistii, ale chce by jego brat sam podjął decyzję o rozstaniu z nią.
* mag: Andrea Wilgacz, która ściągnęła Zupaczkę by znaleźć Sophistię. Też Wacława. Służy też jako analityk magiczny i powiedziała Hektorowi co z Marcelinem.
* mag: Wacław Zajcew, który jako jedyny potrafi rozmawiać z Dariuszem Kopyto i skłonić go do powiedzenia mu prawdy o przeszłości.
* mag: Edwin Blakenbauer, który ma pewną teorię na temat Margaret i choć jest bardzo niechętny siłom z zewnątrz, dla Marcelina zrobi wiele.
* mag: Marcelin Blakenbauer, który jest zamrożony w krysztale by uratować Sophistię. Uratował jej życie ryzykując swoim.
* czł: Borys Kumin, który może obserwować każde miejsce w Rezydencji i ma dostęp do wszystkich systemów wizyjnych.
* czł: Amelia Eter jako Sophistia, przedmiot a nie podmiot misji; jej życie jest sprzężone z Marcelinem i Dariusz Kopyto ją zabija.
* mag: Waldemar Zupaczka, który wymarudził zapłacenie mu za znalezienie psa. I pomógł odnaleźć Sophistię ku zaskoczeniu wszystkich.
* mag: Dariusz Kopyto, którego dawno w przeszłości bardzo ciężko skrzywdził gang do którego należała Sophistia. Mściwy i pamiętliwy; umie poruszać się w prawie.
* vic: Ika, dziewczynka tropiąca. Pomagała w znalezieniu Sophistii i służyła jako narzędzie dla Blakenbauerów.
