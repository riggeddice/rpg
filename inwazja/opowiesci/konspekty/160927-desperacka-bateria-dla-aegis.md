---
layout: inwazja-konspekt
title:  "Desperacka bateria dla Aegis"
campaign: powrot-karradraela
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160411 - Sekret śmierci na wykopaliskach (AW)](160411-sekret-smierci-na-wykopaliskach.html)

### Chronologiczna

* [160411 - Sekret śmierci na wykopaliskach (AW)](160411-sekret-smierci-na-wykopaliskach.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Ignat Zajcew jest wyjątkowo zdesperowany. GS Aegis przestała działać; weszła na rezerwy energii i nie ma szczególnie mocy by móc sobie poradzić i działać dalej... poszedł porozmawiać z TIENOWATYMI...

## Misja właściwa:

Dzień 1:

Ciemna noc w Stokrotkach. Pada deszcz...

Gospodarstwo z polem w Stokrotkach, wójt z żoną w domu.
Pukanie do drzwi. 
Ignat się podpalił, wyleciał za drzwi.
Wrócił, się ogrzewa przy kominku.
Opowiedział smutną historię - głowica go uratowała, zamknęła w piwnicy i uciekł.
Powiedział o delikatnym i niestabilnym reaktorze głowicy. I lockdownie KADEMu.
A w ogóle jest problem.
Kira przepytała. Głowica jest jakieś ~10 km stąd.

W warsztacie Kira ma różne akumulatory, narzędzia, elementy. Ignat ochoczo pomaga. Zgodził się na pomoc za zwrot kosztów # spora przysługa. Franciszek wziął ze sobą parę fajnych rzeczy. 

"Syberion to ZŁY pomysł" - Kira
"Nie mam pomysłu!" - Ignat
"Widzę" - Kira
"To tak widać?!" - Ignat

Pojechali na miejsce, do lasu. Oczywiście, Ignat nie udzielił żadnej przydatnej informacji...
Dotarli na miejsce. 

"Musicie okrążyć teren! Być gotowe na wszystko!" - Aurel Czarko do terminusek
'Franciszek wychodzi z gumiakach, spogląda ze zdziwieniem' - Raynor

"Może użyczysz nam do pomocy te dwie terminuski? Gnój trzeba przekopać, nam zajmie to 3 godziny..." - Franciszek do Aurela

Aurel i jego terminuski się oddaliły. 

Głowica bojowa majaczy. Ledwo ma energię. Jak Ignat kapnął na nią krwią, Głowica podała hasło "tak spolaryzowany kryształ quark wybuchnie" i podała koordynaty Powiewu Świeżości (?!). Ignat nie do końca rozumie. Ale tam może być odpowiedź. Głowica twierdzi, że zawsze broniła Powiewu Świeżości... i nadal majaczy. 

Kira przyjrzała się Aegis pod kątem podładowania. Podpięła wszystkie bio i prądo...

Znalazła sposób - użyła WSZYSTKIE rzeczy jakie ma. Prąd powierzchniowo. Aegis się uruchomiła; potwierdziła, że powierzchniowo prąd i energia dają jej szanse działania. Ale nie ładuje się Czarny Diament; ekosystem nie pasuje. Powiedziała, że w Powiewie jest Katarzyna Kotek i ona będzie w stanie powiedzieć więcej. Nie, Katarzyna Kotek nie wie o Aegis... ale tym hasłem się dowie o co chodzi.
Aegis przechodzi w tryb nieaktywny, Ignat z nią zostaje. Nie może jechać do Kopalina, dał słowo (Silurii).

Dzień 2:

Franciszek i Kira jadą z rana do Powiewu Świeżości...
Powiew znajduje się za Kopalinem, w budynku przedszkola.

Celestyna zaprowadziła do podziemi.
Książki.
Kasia Kotek podała książkę o koszmarach.
Celestynę zabawia Kira, za Kasią poszedł Franciszek
Franciszek jest ludzistą

Reakcja defensywna - Zamek As'caen zareagował zsyłając Wizję Koszmaru.
Sołtys w pozycji fetalnej przez moment. Lęk wysokości.
Kasia pomoże.

Kasia wyjaśniła, że głowica jest zwykle ładowana na Fazie Daemonica, przy silnym pryzmacie, że głowicę da się naładować w tych warunkach. Co gorsza, potrzebne jest jeszcze źródło energii...
Co powoduje, że mają dwie opcje - syberion # ludzie lub Faza Daemonica # ludzie.
A Faza Daemonica jest dostępna z Kompleksu Centralnego lub z Mare Vortex do którego można się dostać z zamku As'Caen...

Ok, w chałupie gdzie znajduje się Aegis i Ignat. 
Kira zaproponowała rozwiązanie - zrobić imprezę dla grupki ludzi. By naładować głowicę.

Gminy mają system gmin partnerskich; w ten sposób można zorganizować wspólną imprezę.

Potrzebne są: Syberion, Źródło magii (Węzeł?), Ludzie, Pryzmat (naładowania), Źródło energii (prądu), Głowica

Trzeba zrobić imprezę, za wsią. Na ściernisku. 
Niestety, lokalny kandydat na sołtysa i stały przeciwnik Franciszka, Antoni Chlebak, narzeka na to że to rozdawnictwo pieniędzy itp. 

Ignat obiecał, że załatwi węzeł magii i zaproponował Rafała Kniazia by ten zorganizował imprezę. Nadał Kirze Elizawietę jako przyjaciółkę - niech Kira zdobędzie syberion. #circumstance.
Impreza będzie w temacie odnawialnych źródeł energii. Franciszek załatwił patronaty medialne oraz sponsorów. #tool.

Rafał Kniaź usłyszał o sponsorach i się super zgodził. WOW!

Spotkanie z Zenobią Bankierz. Zgodziła się na nie Aurela i nie Dziurząba.

"Podobno napastujesz moje terminuski" - Aurel Czarko
"Przyjąłem przeprosiny" - Franciszek Baranowski
"Rozumiem, tien Baranowski. Ukarzę ją odpowiednio." - Aurel Czarko

Franciszek się zdenerwował. Skomunikował się z lordem terminusem i powiedział o sytuacji. Franciszek zgodził się, by lord terminus przejrzał skan hipernetowy z tamtych wydarzeń na linii Czarko / Felicja, po czym Szczepan Sowiński podziękował. Rozwiąże problem i uratuje młodą terminuskę, dodatkowo poskromi tien Aurela Czarko.

Udało się zneutralizować Świecę, udało się zdobyć Węzeł, udało się zdobyć Ludzi, też narzędzia, Głowicę. Teraz: Źródło Magii (udał się) i syberion.
Kira spróbowała zdobyć syberion kanałami Świecy. Zaaprobowali jej prośbę. Udało się.
Kira ściągnęła też jako wsparcie Czirnę Zajcew. #1 tool. I pomogła organizatorowi imprezy.

Dzień 5:

Impreza. 

80%, 5 poziomów ładowania Głowicy. 
Pierwszy poziom naładowany bez problemu.
Drugi poziom naładowany bez problemu.
Trzeci poziom naładowany bez problemu, Aurel Czarko podpalił ich chałupę.
Czwarty poziom naładowany bez problemu.
Piąty poziom naładowany bez problemu.

Głowica jest w pełni mocy operacyjnej.
Pożar w chacie Franciszka i Kiry. Ignat go ugasił, ale pewne straty były. Nic nie wskazuje na Aurela Czarko, ale...

EPILOG:

Trzy dni później znaleziono Aurela Czarko pobitego w śmietniku. Podobno to było kilku żuli...

## Progresja



# Streszczenie

Ignat Zajcew jest przerażony krytycznym stanem GS Aegis. Poprosił o pomoc "tienowatą" Kirę, kuzynkę, 'nie defilerkę' i jej męża, sołtysa wsi Stokrotki. Podczas prób pomocy, Franciszek i Kira zmierzyli się z Aurelem Czarko (który wyraźnie dobrze się bawi tępiąc młode terminuski i magów Świecy). Część odpowiedzi znaleźli u Kasi Kotek w Powiewie Świeżości, w książkach - Kira sprowadziła Syberię, Franciszek zrobił imprezę i udało im się naładować reaktor Aegis. W podzięce Ignat pobił (dyskretnie) Aurela Czarko i wyszło, że to robota żuli ;-). Czarko został też politycznie poskromiony przez Zenobię Bankierz z Trocińskiego oddziału Świecy.

# Zasługi

* mag: Franciszek Baranowski, sołtys wsi Stokrotki, zorganizował ładowanie GS Aegis 0003 na koszt Unii Europejskiej i kazał terminusce przerzucać gnój ;-).
* mag: Kira Zajcew, "nie jest defilerką", sprowadziła Syberię do wsi i naładowała GS Aegis 0003 ekologiczną energią. Też: ma dość części swojego rodu (Ignata).
* mag: Ignat Zajcew, który nie lubi "tienowatych" ale w obliczu zagrożenia zniszczenia Aegis zdecydował się na znalezienie kuzynki ze Świecy która może mu pomóc.
* vic: GS Aegis 0003, odłamek EIS/Aurelii, majacząca z uwagi na niski poziom energii - naładowana przez Syberion # Pryzmat # źródło energii... z dopłatami od UE ;-).
* mag: Aurel Czarko, który chce się popisać przed młodymi terminuskami i nadużywa swojej siły i pozycji wobec "* maga wieśniaka". Ukarany przez lorda terminusa Trocina.
* mag: Felicja Strączek, terminuska, która poraziła Franciszka na życzenie Aurela Czarko a potem Franciszka przeprosiła... przez co zaeskalowało między nim a Aurelem Czarko.
* vic: Katarzyna Kotek, odłamek Aurelii; NIE CHCE pomóc (ani być Aurelią), ale... powiedziała Franciszkowi i Kirze jak należy naładować GS Aegis 0003.
* czł: Antoni Chlebak, konkurent polityczny Franciszka Baranowskiego, który oskarżył go o marnowanie pieniędzy. Do momentu pojawienia się dopłat z UE.
* czł: Rafał Kniaź, organizator imprezy i festiwalu ekologicznych odnawialnych źródeł energii ze sponsorami, pieniędzmi i w ogóle.
* mag: Zenobia Bankierz, gubernator, administracyjna głowa lokalnej Świecy. Konkretna, rozsądna... miła odmiana.
* mag: Andrzej Bankierz, nieco leciwy i zmęczony życiem terminus. Osłaniał imprezę na wypadek jakby coś nie wyszło.
* mag: Szczepan Sowiński, lord terminus lokalnej Świecy. Utemperował nieco Aurela Czarko
* mag: Czirna Zajcew, specjalistka od pryzmatu i manipulacji nim. Pomogła dopracować imprezę w szczegółach.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. Biblioteka Powiewu Świeżości
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Nowy Kopalin
                        1. Powiew Świeżości
            1. Powiat Okólno-Trocin 
                1. Stokrotki, wieś niedaleko Trocina charakteryzująca się niskim podatkiem od usług transportowych
                    1. Wielkie Ściernisko, na którym dzieją się zwykle Różne Rzeczy (teraz: festiwal ekologiczny)
                1. Markucik, wieś dalej od Trocina charakteryzująca się wyjątkowo dobrymi i szerokimi zbiorami owoców lasu
                    1. Las Obfity
                        1. Ruina tartaku, gdzie zabunkrowała się GS Aegis 003 z Ignatem... i gdzie doszło do "walki" Franciszka Baranowskiego z Aurelem Czarko
                1. Trocin
                    1. Centrum
                        1. Kompleks Srebrnej Świecy, gdzie niskopoziomowy mag Świecy (Franciszek) został potraktowany wyjątkowo uczciwie
            
## Skrypt

# Czas

* Dni: 5

## Tempki:

Franciszek Baranowski - wójt wsi Stokrotki, młody, energiczny samorządowiec i społecznik. Rolnik z zawodu, z zamiłowaniem do techniki i motoryzacji. Zwolennik tradycji i wiejski pozytywista. Mag materii.

Zrobili instalacje do odnawialnych źródeł energii, działa super efektywnie dzięki magi - z komórek roślinek - dla gminy, za co ludzie są wdzięczni swoim dobrodziejom

-----
Kira Zajcew - energia z organizmów żywych (bio / technomantka)
kontrola przepływów energii (katalistka)


biomantka
technomantka
katalistka

rolnik


Wiejskie generatory energii ze źródeł odnawialnych, regularnie dostarczające energię do sieci - zysk dla gminy.
Wyklęta przez magów, uważana powszechnie za defilerkę. Cieszy się opinią wyjątkowo przerażającej - nie dość, że w jej pobliżu nie ma śladów magii krwi, to jeszcze ona sama nie poddała się uzależnieniu...

We wsi nazywają ją "panią inżynier", choć nigdy nie twierdziła, że ma jakiś dyplom. Ale zna się na rzeczy, a urządzenia w jej pobliżu działają lepiej...

Do swoich projektów wykorzystuje różne narzędzia, ich warsztat jest naprawdę dobrze wyposażony.
Ma dostęp do małego, prywatnego generatora na roślinki.