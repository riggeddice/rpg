---
layout: inwazja-konspekt
title:  "Krótki antyporadnik o sojuszach"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150728 - Sojusz przeciwko Szlachcie (HB, SD)](150728-sojusz-przeciwko-szlachcie.html)

### Chronologiczna

* [150728 - Sojusz przeciwko Szlachcie (HB, SD)](150728-sojusz-przeciwko-szlachcie.html)

### Punkt zerowy:

Ż: Jaką korzyść z tej misji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Na kim najmocniej odbiją się obostrzenia Marty (lockdown KADEMu)?
K: Na Ignacie Zajcewie.
Ż: Co może wyciągnąć SŚ na KADEM by jeszcze bardziej eskalować spiralę nienawiści?
D: Dowody, że KADEM "twórczo czerpie" z ich technologii i pomysłów.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Powiedz mi coś o dziewczynie, przez którą Marcel wpadł w kłopoty.
D: Ma lepkie ręce. Cicha, zwinna, niepozorna. Absolutnie niebojowa.

Ż: Co takiego zbroiła ostatnio lokalna mafia co tak strasznie zirytowało Hektora?
D: Zaczęli rozprowadzać wśród dzieciaków nowy narkotyk, przepraszam, "dopalacz".
Ż: Jakiego maga najchętniej przesłuchałby Hektor na ostro (z której frakcji i imię / rola)?
K: Vladlenę Zajcew.
Ż: Dlaczego właśnie jego? Co ma za szczególną wiedzę którą chce się dowiedzieć?
B: Vladlena może nie wiedzieć, ale zna połączenie pomiędzy Emilią a niebezpieczną grupą przestępczą.
Ż: Co sprawiło, że przecież bardzo ważny bal u Emilii musiał zostać odłożony na później?
K: Oficjalnie, niedyspozycja Emilii. W praktyce, może pomóc Tamarze - ale to wymaga jej osobistego udziału.
Ż: Dlaczego Vladlena ma powód dostania się na imprezę organizowaną przez maga Millennium?
D: Chce zdobyć kontakty, ale normalnie nie miałaby na to szans; to jest zasłona dymna.
Ż: Dlaczego istnieje realna szansa, żeby grupa specjalna zinfiltrowała miejsce gdzie Vladlena mieszka i podłożyła dowody?
B: Jest w trakcie wymiany systemu zabezpieczeń (paranoja związana ze Szlachtą).
Ż: Dlaczego Vladlena mieszka poza kompleksem Srebrnej Świecy?
K: Bo dawno temu uzgodniła to z seirasem Benjaminem Zajcewem.
Ż: Gdzie zatem mieszka Vladlena Zajcew?
D: Niewielki domek w Kopalinie, na uboczu, przedmieścia; nie bardzo daleko od kliniki Słonecznik.
Ż: Dlaczego Szlachta czy siły wrogie Emilii jeszcze się do niej nie dobrały?
B: Mają do niej pokojowe dojście; dzięki Vladlenie mają dostęp do części informacji tak czy inaczej, więc po co palić źródło?

## Misja właściwa:

### Faza 1: Sojusz na pięć minut.

Hektor siedzi w prokuraturze i jest bardzo ponury. Dzieciaki dostały dopalacze w różnych szkołach, bardzo szeroko... i wszystko wskazuje na to, że stoi za tym Szymon Skubny. Ten sam Szymon Skubny, który stoi za wieloma akcjami i który wiecznie się Hektorowi wymyka, bo jest bardziej popularny niż Hektor i opłaca odpowiednich ludzi.
Hektor ruszył akcję "usunąć dilerów, Skubny musi zatrudniać, wprowadzić swoich ludzi."
Skontaktował się z nim Edwin. Chce porozmawiać. Hektor odpowiedział chłodno, że za 2h jak tylko się skończy praca. Edwin zmilczał.

Tymczasem Siluria siedzi sobie spokojnie w Rzecznej Chacie z "Nikolą". Słucha plotek i próbuje dowiedzieć się czegoś o Marcelinie - wybronić, osłonić, coś w ten deseń. No i trafiła. Biedny Marcelin Blakenbauer, ofiara panZajcewowej konspiracji (Ewa Zajcew, Infernia Zajcew, Vladlena Zajcew...) a one wszystkie konspirują z Hektorem Blakenbauerem przeciwko Marcelinowi i nie tylko; Marcelin jest środkiem i pionkiem; Hektor jest pionkiem Zajcewów którzy chcą mieć swojego prokuratora.

Marcelin poszedł poprosić Klemensa o drobną przysługę. Trzeba przekazać Leokadii pewien eliksir. Klemens z niewiadomych przyczyn się nie zgodził bez poznania szczegółów i przekonał Marcelina; ten powiedział, że jego współpraca z Leokadią zaczęła się w taki sposób, że Vladlena skontaktowała Marcelina i Leokadię. Leokadia powiedziała, że w okolicy znajduje się istota która najpewniej uciekła z laboratoriów Blakenbauerów. Podała Marcelinowi próbkę i on rozpoznał coś w stylu aptoforma i coś w stylu Margaret. Zdaniem Marcelina, coś Margaret uciekło i ona nie wie, bo jak z nią rozmawiał to nie wiedziała. Więc chce przekazać Leokadii coś, dzięki czemu Vladlena znajdzie to co uciekło Blakenbauerom "po cichu". Klemens, pamiętając jak ostatnio skończyły się działania z Leokadią i znając historię spotkania Hektor - Leokadia przycisnął Marcelina, który powiedział że to ON zaakceptował, by użyć merkuriasza przeciwko wrogom Leokadii. Że Leokadia to zaproponowała, a on się zgodził. Czemu? Bo trzeba pomóc damom w niebezpieczeństwie a dodatkowo Leokadia chce połączyć strony.

Klemens usłyszał już wszystko. Wziął fiolkę z unikalnym identyfikatorem i poszedł z tym do Margaret objaśniając sprawę. Lifeshaperka się zdziwiła; nic jej nie uciekło a już na pewno nie aptoform. Klemens zaproponował usunięcie Leokadii. Margaret po chwili zastanowienia się zgodziła; potrzebna będzie jakaś wredna choroba magiczna (Edwin-chan) by Leokadię wyłączyć z akcji. Dodatkowo można będzie wysłać do sojuszników Diany Weiner informację, że Blakenbauerowie przepraszają, merkuriasz został wykorzystany wbrew ich zamiarom a stan Leokadii dowodem, że nie chcieli. I w ten sposób Marcelin jest bezpieczniejszy. 

Klemensowi spodobał się plan Margaret.

A do domu wrócił Hektor porozmawiać z Edwinem. 
Edwin powiedział na starcie Hektorowi, że najpewniej Vladlena współpracuje z Szymonem Skubnym. Na pewno Vladlena ma coś wspólnego z co najmniej częścią tych spraw z dopalaczami; tyle się dowiedział na pewno. Dodatkowo, ten "sojusz" działa za plecami Hektora i naraża na niebezpieczeństwo Marcelina i Blakenbauerów. A to trzeba zamknąć. Nie można dać się szantażować.
Hektor zgodził się z Edwinem... trzeba to rozwiązać (wygrany konflikt). Za plecami Hektora jednak Edwin poinformował grupę specjalną, że jeśli dowodów nie będzie, trzeba je podłożyć.
Padło na to, by podłożyć jej dowody, że przez nią informacje wyciekają do Zajcewów i do ludzi z nimi współpracujących (łamiąc Maskaradę).

Klemens pojechał do Leokadii i dał jej przygotowany przez Margaret i Edwina eliksir mający skrzywdzić Leokadię. Ta się ucieszyła i dała Klemensowi inną paczkę z "biohazard material", jak obiecała Marcelinowi. Klemens podziękował.

Hektor zadzwonił do Silurii powiedzieć jej o przełożonym balu. Ona podziękowała, ale wiedziała o tym wcześniej. Hektor smuteczek. Nieważny, nikt mu nie powiedział.

Dobrze... Hektor musi jakoś dowiedzieć się jak się pozbyć Vladleny z okolicy podczas najazdu na chatę. Zaprosił ją na obiad i przeprowadzili kilka rozmów; stanęli na tym, że zdaniem Hektora sojusz jest jeszcze nie ukonstytuowany (nie było rozmowy z Emilią). Vladlena jest dość czarująca i sprytna; lubi potyczki słowne i rozgrywki tego typu. Wyciągnęła z Hektora dość sporo informacji odnośnie procesów Blakenbauerów i poprzednich zniknięć stworów (ale nie odnośnie aptoforma); w odpowiedzi jednak Hektor nie zdobył od niej niczego konstruktywnego. Vladlena powiedziała też Hektorowi o Szlachcie i wymieniła 3 nazwiska o których wie: Oktawian Maus, Wiktor Sowiński, Diana Weiner.
Prokurator nawet ją polubił. Prawie.
Niestety, ze spotkania Hektor wiele się nie dowiedział (a Vladlena całkiem sporo odnośnie rzeczy mało istotnych z perspektywy Hektora i Blakenbauerów). Vladlena bardzo chwali Marcelina; z perspektywy rozmowy Hektor podejrzewa, że wie czemu.
Jak Vladlena wyszła, Hektor zwinął widelec jakim jadła. Alina będzie miała w co się zmienić.

Vladlena zdradziła się, że wie coś więcej o ataku na poligon niż powiedziała. To sprawia, że z perspektywy Hektora Vladlena nadaje się na przesłuchanie z wielu powodów... co otwiera dostęp do Silurii. Więc Hektor powiedział Silurii, że Vladlena może coś wiedzieć ale mu nie zaufała i nie chciała powiedzieć. Hektor też powiedział Silurii, że przeprowadzane jest śledztwo w sprawie dopalaczy i Vladlena jest potencjalnie podejrzana. I prowadzone jest śledztwo. De facto Hektor powiedział Silurii, że jest dywersją w akcji przeciwko Vladlenie...
A Siluria powiedziała, że nie powinien jej był mówić. Ale jakby Hektor dał jej bransoletkę (np z podsłuchem) to już nie jej wina. No i jakby ktoś zaatakował Vladlenę w jej obecności... Siluria by jej broniła.

Więc Siluria zdecydowała się też spotkać z Vladleną.

"Zdecydowanie przekonała mnie pani, tien Diakon. Zabiliśmy trzy żabolody i KADEM wszedł w lockdown - ogromny sukces." - śmiejąca się Vladlena do Silurii.

Siluria dowiedziała się od Vladleny całkiem sporo; po pierwsze, Leokadia nie miała swoich leków gdy rozmawiała z Silurią i Hektorem; teraz już je ma ("przypadkiem" się skończyły półprodukty w laboratoriach alchemicznych SŚ). Vladlena powiedziała Silurii to co wiedziała o Szlachcie - trzy nazwiska; potem to, że wybijają magów Kurtyny, że Kurtyna się rozpada (Tamara i Iza zdjęte; Leokadia pocięta ale zrobiła kontrę), że Szlachta dąży do przejęcia władzy jak taki gang wśród magów, że Emilia i Leokadia to dwie ostatnie siły przywódcze które zostały i które mają jakąś kontrolę. Dodała też, że lubi Diakonów bo mieszka koło Kermita Diakona (terminus po sąsiedzku).
Zdaniem Vladleny KADEM i Kurtyna powinny współpracować, bo w interesie KADEMu jest Świeca jaką reprezentuje Kurtyna.

Siluria się wprosiła na imprezę do Emilii jako osoba NIE towarzysząca a osoba zaproszona imiennie.

Hektor dowiedział się od Silurii, że obok Vladleny mieszka terminus. Kermit Diakon. To wymaga zmiany działań.

### Faza 2: Sojusz prokuratora z gangsterem

Szymon Skubny je obiad w restauracji "Welon" w towarzystwie oficjeli oraz pięknych młodych dam. Hektor przeleciał przez listę grzechów Skubnego i nic nie znalazł. Tzn. mnóstwo znalazł, ale nic "actionable". Zdecydował się więc pójść do restauracji, ale nie sam; weźmie ze sobą Silurię. Nie mówiąc nic Silurii, poprosił ją by "wywarła wrażenie".

Hektor pociągnął za kilka sznurków by zdobyć stolik niedaleko Skubnego. Hektor zdecydował się spotkać się z Silurią wcześniej a Siluria stwierdziła, że ona chce wywrzeć OGROMNE wrażenie. Wytacza wszystkie działa. Spotkała się więc z Hektorem... (13v8) - Siluria przełamała postawę "kij w dupę wsadził Blakenbauer" i sobie z Hektorem podflirtowuje. Jakiś mały bonus na relacje z Hektorem. Hektor ma szczękę przy ziemi i mniej zawodowo podchodzi do relacji z Silurią.

Siluria tak bardzo szczęśliwa z efektu.

Hektor opisał Silurii sytuację z Szymonem Skubnym i poprosił Silurię o pomoc.
Hektor i Siluria weszli do restauracji; wszystkie oczy na nich. Siluria wybrała sobie taką potrawę, że niby nic, ale robi malutki pokaz; kulturalnie, ale odpowiednio dwuznacznie. Masakra.
Hektor i Siluria zostali poproszeni do stołu. Siluria zwraca uwagę na wszystkich ale też zwłaszcza na Skubnego i pozwala mu myśleć, że "odbija" ją od Hektora.

Bronią Silurii jest kolacja, sztychem jest piękny uśmiech. A Hektor słucha i pozwala Skubnemu myśleć, że on odbija Silurię. Hektor i Skubny wymieniają się przechwałkami.

Skubny skupia się na tym, by odbić Silurię od Hektora. Siluria pozwala na to a Hektor zostaje narzędziem; za to Hektor chce się dowiedzieć, czy Skubny wie o magii. Siluria sprawia, że Skubny nie wpadnie na to, że ona jest magiem i dowiaduje się, że Skubny wie coś o magii. Więcej, przyznał, że potrafi działać w dwóch miejscach w tym samym czasie. Jest na bankiecie a w tym czasie mógłby robić coś zupełnie innego.

Siluria kontynuuje rozmowę, chcąc wyciągnąć ze Skubnego jakiś dowód, bo teraz jest pijany szczęściem. Skubny chce ją pocałować w usta jako swoje trofeum. Hektor bodyblockuje, na co Skubny odpycha go z siłą i całuje Silurię w usta (remis konflikt), ale w wyniku tego pokazuje wszystkim dookoła że cham i pachoł. Skubny szybko się reflektuje i wypsnęło mu się w odpowiedzi na pytanie Silurii (chciał wywrzeć dobre wrażenie) (remis konfliktu), że była sytuacja ze świadkiem, który widział go będąc pijanym a w tym czasie Skubny gadał z Hektorem. Hektor pamięta tą sprawę - świadek był pijany, ale miał całkiem silne dowody na temat Skubnego, tyle, że Skubny w tym czasie był z Hektorem. Sprawa dla grupy specjalnej; jeśli tu jest magia, może coś z tego da się wydobyć i dojść do endgamu Skubnego.

Siluria, "lekko wstrząśnięta", przeprosiła i poszła przypudrować nosek sugerując (lekko) Skubnemu, że może za nią podążyć. Skubny, typ drapieżnika "czującego krew" poszedł za nią. Zostawili Hektora samego przy stole z oficjelami i pachołami Skubnego. Hektor zdecydował się do dyskusji z nimi, by zniechęcić oficjeli do współpracy ze Skubnym (a podwładnych zniechęcić do zbyt silnej współpracy ze swym patronem). W tej skali konfliktu udało mu się to wybornie.

Siluria napotkała Skubnego. Przeprosił ją (nieszczerze) mówiąc, że Hektor nie ma krwi w żyłach a on tak, więc krew nie woda. Siluria z uśmiechem powiedziała, że owszem, Hektor ma dość krwi. Skubny się zdenerwował, ale się kontroluje. Siluria wysłała mu sygnał, że Skubny nie stracił wszystkich szans, ale ona nie jest jego zabawką. Chyba, że na swoich zasadach.
Skubny i Siluria wyszli razem. Skubny powiedział Silurii coś, dzięki czemu ona przeczytała pomiędzy wierszami, że on współpracuje z magiem... a dokładniej, mag współpracuje z nim ale Skubny gra też na własną rękę na tyle na ile jest w stanie. Te nowe dopalacze dawane dzieciom i młodym jego mag traktuje jako zagrożenie (Skubny nie wie czemu). Siluria zaproponowała Skubnemu, żeby wykorzystać Hektora przeciwko dopalaczom. Skubny się ucieszył. Bardzo. Świetny pomysł, a Siluria będzie wyglądała na to, że mimo że on ją zmusił do pójścia z nim to przekonała go do jakiejś formy współpracy.

Hektor opuścił restaurację sam. Porzucony bez słowa. Ale jakoś nie nieszczęśliwy.
To znaczy, dopóki nie poprosił go na stronę Marcelin.

Marcelin powiedział Hektorowi, że on, Hektor, jest osobą która złamała sojusz zanim się jeszcze dobrze zaczął. Siły Blakenbauerów uderzają w kierunku na Vladlenę (Hektor zauważył, że zbierają informacje, ale Marcelin był pełen patosu). Marcelin powiedział Hektorowi, że Leokadia leży; zniszczona przez jakąś miksturę Edwina. Wyjaśnił, że miał Leokadii coś przekazać a Edwin to podmienił. Leokadia jednak przewidziała potencjalny atak, więc zostawiła dead man's hand. I w ten sposób Marcelin dowiedział się, że zdaniem Leokadii to ON (Marcelin) stał za tym atakiem i Leokadia nie jest szczęśliwa, ale może z tym żyć. Sojusz jest ważniejszy.

I Marcelin chce zachować sojusz.
Hektora trafił szlag. Spotkanie całej rodziny. I teraz - no secrets. Dojdą do jednej wersji. Zdaniem Marcelina to zły pomysł, bo Edwin znowu wygra. Ale Hektor chce to zrobić dobrze.

Spotkanie czterech Blakenbauerów: Margaret, Edwina, Marcelina, Hektora. W sali konferencyjnej w Rezydencji.
Marcelin powiedział na czym polega problem. Edwin zaznaczył, że Marcelin już wyniósł eliksir. Ogólnie, nieźle się pożarli. Edwin i Marcelin ostro dyskutowali; Edwin jest pragmatyczny i chce chronić Marcelina, Marcelin chce zrobić to co należy. Hektor jest sercem za Marcelinem. W końcu Edwin i Hektor się ścięli o to, co z tym robić dalej. Nikt nie wygrał.
Edwin się zgodził. Niech sojusz będzie zachowany. Ale on będzie kontaktował się ze Szlachtą jak to konieczne; los Marcelina jest ważniejszy. Hektor na to poszedł.

Odwołano wszystkie działania przeciw Vladlenie.

### Faza 3: Odbudowa sojuszu z Kurtyną.

Siluria i Hektor się spotkali. Wymienili się informacjami. Siluria ucieszyła się, że sojusz jednak będzie utrzymany i zawarty, zmartwiła się jednak, że Leokadia jest zdjęta z akcji. Zdaniem Silurii, Hektor musi porozmawiać z Vladleną i ją bardzo przeprosić. Ona sama porozmawia potem z Vladleną, ale POTEM.
Hektor powiedział Silurii, że to Edwin stał za tym wszystkim i to on rozchorował Leokadię.

Hektor umówił się z Vladleną. Wpuściła go. Podyskutowali trochę i Hektor dowiedział się kilka ciekawostek:
- Vladlena rozprowadza dopalacze (te na skarży się Skubny)
- Vladlena twierdzi, że dopalacze są stworzone przez Leokadię i Leokadia wie jak dokładnie działają; są nieletalne.
- Dopalacze szukają zmodyfikowanych koncówek aptoforma. Właśnie, podobno Blakenbauerom uciekł aptoform. Wow. Tyle przegrać.
- Jeśli rozwiążą się problemy z dziwnymi końcówkami aptoforma, Vladlena skończy z dopalaczami.
- Vladlena zażądała od Hektora by dostarczył OCHRONĘ dla magów Kurtyny o wartości bojowej 3 terminusów (Iza, Tamara, Leokadia); Hektor się zgodził.
- Vladlena ma zamiar spinować politycznie tą ochronę odpowiednio i wykorzystać ją do ochrony miejsca gdzie magowie będą oraz ew. ochrony osobistej. Nie wykorzysta ich do ataku. Kontrolerem - Marcelin.
- (coś czego Vladlena ani Marcelin nie wiedzą: stwory Blakenbauerów mają mieć switch. Hektor ma priorytet dowodzenia nad Marcelinem)
- (coś czego Vladlena ani Marcelin nie wiedzą: stwory Blakenbauerów mają robić loga zawsze jak opuszczają miejsce, gdzie się znajdują)

Sojusz będzie zachowany.
Agrest będzie zadowolony ;p.

## Dark Future:

### Faza 1: 
- Hektor dowiaduje się o dopalaczach rozpowszechnianych u dzieci. Podejrzenia wskazują na Szymona Skubnego.
- Edwin idzie na rozmowę z Hektorem odnośnie Marcelina i implikacji (działania Leokadii). Uważa, że Leokadia nie może wygrać.
- Edwin namawia Hektora na uderzenie we Vladlenę.
- Siluria ma możliwość przekonania maga Millennium by zaprosił/a Vladlenę na imprezę i jej odciagnięcie.

### Faza 2:
- Siluria dowiaduje się, że Kermit Diakon (terminus) mieszka niedaleko Vladleny; trzeba by go jakoś rozbroić.
- Vladlena mieszka z ludzką służką i jej dzieckiem; ma mniej sprawny niż zwykle system zabezpieczeń.
- Szymon Skubny jest na kolacji w restauracji "Welon".
- Leokadia chce porozmawiać z Hektorem o ich przyszłości.

### Faza 3:
- Vladlena udaje się na imprezę.
- Kermit założył zabezpieczenia na domek Vladleny.
- Szymon Skubny załatwił Hektorowi molestowanie seksualne asystentki.

# Streszczenie

Szeroko rozpowszechniane są dopalacze. By chronić politycznie Marcelina i Blakenbauerów przed Szlachtą, Edwin otruł Leokadię (najkrótszy sojusz świata). Vladlena opowiedziała, że Kurtyna pod naciskiem Szlachty się powoli rozpada a Szlachta rośnie w siłę. Podobno aptoform uciekł (nieprawda). Spotkanie w restauracji Hektora#Silurii i Skubnego, on nie ma nic wspólnego z dopalaczami. To jakaś magiczna sprawa. Skubny wie o magii. Za to POTEM wyszło, że Vladlena dopalaczami szuka aptoforma / innych istot i wydobyła od Hektora ochronę dla magów Kurtyny. Mimo krzywdy Leokadii, Sojusz z Kurtyną jest w mocy.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Luksusowa restauracja Welon
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Sen Robotnika
                            1. Domek Vladleny Zajcew
                    1. Dzielnica Trzech Myszy
                        1. Domek Leokadii Myszeczki
                    1. Dzielnica Kwiatowa
                        1. Domek Kermita Diakona

# Zasługi

* mag: Siluria Diakon, która doprowadziła do najdziwniejszego sojuszu w okolicy. Zinflirtowała Skubnego i ten sojusz.
* mag: Hektor Blakenbauer, który postawił na swoim i mimo "pomocy" innych * członków rodu uratował sojusz z Kurtyną.
* vic: Klemens X, który rzucił pierwszy kamyk do lawiny zniszczenia sojuszu z Kurtyną; zachował się roztropnie czego nikt nie docenił.
* czł: Kinga Melit, asystentka prokuratora Hektora Blakenbauera. Niezbyt śliczna, ale obowiązkowa i pracowita.
* czł: Szymon Skubny, zawsze unikający sprawiedliwości kryminalista, który dzięki Silurii... zawarł sojusz z Hektorem.
* mag: Leokadia Myszeczka, terminuska Kurtyny, którą rozchorował Edwin by uratować Marcelina przed ewentualną akcją Szlachty.
* mag: Edwin Blakenbauer, który pokazał, że umie nie tylko leczyć ale i zarażać * magów dla ochrony Marcelina i z zemsty za wrabianie Blakenbauerów w złe sojusze.
* mag: Marcelin Blakenbauer, który pokazał zęby, postawił się wszystkim i doprowadził do odbudowania sojuszu z Kurtyną. Bo Leokadia weń wierzy.
* mag: Vladlena Zajcew, która pokazała nieco bardziej drapieżną naturę niż tylko cicha sekretarka i wyciągnęła sporo od Hektora za utrzymanie sojuszu.