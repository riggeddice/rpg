---
layout: inwazja-konspekt
title: "Wdzięczność Iliusitiusa"
campaign: powrot-karradraela
players: kić, dust, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170823 - Suma niedokończonych spraw... (HB, KB, DK, AB, PK, HS)](170823-suma-niedokonczonych-spraw.html)

### Chronologiczna

* [170823 - Suma niedokończonych spraw... (HB, KB, DK, AB, PK, HS)](170823-suma-niedokonczonych-spraw.html)

## Kontekst ogólny sytuacji

Temat Infensy i Renaty nadal budzą zainteresowanie magów, którzy się na dwie główne kategorie - jedni chcą o tym zapomnieć, wymazać z historii i nazwać anomalią a drudzy chcą doprowadzić do zwarcia szyków i wykluczenia magów, którzy byli z tym powiązani - głównie KADEMu i Hektora.

Pojawiły się działania w kierunku na prokuraturę magów. Silną lobbystką jest Andżelika. Niestety, jednocześnie lobbuje za karaniem Marianny, przez co jedynie rośnie ruch izolacji gildii. Znowu mamy dwie siły. Na to nakłada się Infensa... chwilowo izolacja jest dominującą siłą.

Działania Alegretty wzbudziły zainteresowanie magów Mausożernych - takich, którzy chcieliby po Karradraelu najlepiej karać różnych Mausów. Łatwo dojdą po śladach do Maurycego Mausa a tam - do Blakenbauerów. Na szczęście, Judyta Maus jest przeszczęśliwa z odzyskania Maurycego i z aktualnego stanu Alegretty - Hektor wyraźnie wybił blisko Silurii w kategorii Prawdziwych Bohaterów Judyty...

Eis zaczyna się zastanawiać, czy pomysł z sojuszem był najlepszy, czy nie pójść starym podejściem typu "silny ma rację". Nadal się waha - nie wszyscy magowie KADEMu traktowali ją źle, ale może Hektor nie jest tym magiem, którego szukała...

Autowar Bzizma dał radę połączyć się z leylinami i niestabilnymi Węzłami głębinowymi. Został oślepiony i istnieje broń mogąca go przejąć na stronę Blakenbauerów.

Iliusitius chroni Hektora przed atakiem Kinglorda.

## Dodatkowe dane Dusta

mam 2 pomysły jak rozwiązać problem młodej wampirzycy
niekoniecznie się znoszące

1) Użyć krwi, którą zdobył Henryk i przygotować dedykowaną truciznę
Zaaplikować środek Mordredowi i (wait for it!) Silurii tak, żeby krążył w ich krwi
Mordred i Siluria udają się do młodej. Dramatyczne wejście, efekty specjalne, aura grozy i siły
boom, przed przestraszoną Blutwurstką staje "pradawny wampir", tak stary, że aż wyglądający nieludzko (demibeast), ze swoją służącą
Oczywiście mówi Siluria - "Hrabia" jest zbyt stary i wysyła tylko mentalne obrazy i polecenia
Jeśli się uda, to mała powie nam wszystko, co wie o Kinglordzie i jego motywach
jeśli się nie uda i zaatakuje, to mamy dostęp do trucizny + wsparcie

2) Mordred odwiedza rezydencje i przekonuje Hektora, żeby spotkał się z Blutwurstką
Organizuje spotkanie, ale tak, że Karina nie wie z kim się spotka (opcja pierwsza)
Hektor przeprasza Karinę i pokazuje, kto jest prawdziwym wrogiem, kto naprawdę zabił jej rodziców
Nie została sama, Blakenbauerowie/Milenium mogą ją przygarnąć
Mam jeszcze pomysł na to czemu Blakenbauerowie nie korzystają z pomocy Mordreda nawet w ciężkich sytuacjach
boją się go
rezydencja też czuje się nerwowa w jego obecności, ale krew rodu go chroni
co się stało?
pamiętasz, że Hekktor jest dziedzicem rodu, ale nie był najstarszym synem Ottona?
jest jeszcze jedna osoba, o której się nie mówi 
Paul - pierworodny 
Mordred go zabił
dokładnie wykonał na nim coup de grace
bo nie było dla niego już powrotu
Podejrzewam, że to Tymotheus coś namieszał. Jakiś eksperyment, w którym Paul mu pomagał wymknął się spod kontroli. Paul stał się śmiertelnym zagrożeniem dla rodu
więc Mordred go najpierw pokonał, a potem wyeliminował, żeby oszczędzić mu próby "naprawienia go", które nie miały szansy się powieść 
tak więc Mordred stał się pierwszym w historii Blakenbauerem, który zabił innego Blakenbauera 
Otton go wygnał, więc nieproszony nie może pojawić się w rezydencji
wtedy też bardzo poróżnił się Otton z Tymotheusem - rozpoczął się rozłam
kuzyni nic nie wiedzą
dlatego Klara może pracować z Mordredem
i to ona będzie musiała zaprosić go do rezydencji

czemu Mordred się w ogóle stara? ciągle ma w genach, że jest żądłem rezydencji - czy tego chce czy nie

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Piotr po spotkaniu z Olgą postanowił się zaszyć tam, gdzie nikt go nie znajdzie. Pojechał do opery. Tam, Operiatrix uniemożliwia Piotrowi alkoholizm itp. Jest głodna i chce wystawy. No i jest zapyziała opera która jest nieco uszkodzona i nikt tu nie przyjdzie... no i to jest pewien problem Piotra. Co z tym robić? Przy okazji, Operiatrix powiedziała Piotrowi, że ów ma dziwną aurę. Piotr spróbował spojrzeć na siebie oczami ducha opery i zobaczył w swojej krwi pająki i świetliste skrzydła na swoich oczach. Truciznę Mordreda i Dotyk Iliusitiusa.

W tym czasie Mordred szuka Piotra, żeby go wyleczyć. Z buta wpada do jego mieszkania i zastaje pustkę i bombę. Prosi o pomoc Klarę, aby zapytała Marcelina i być może pomogła znaleźć Piotra. Do mieszkania Piotra wpada Karolina Kupiec, "oficjalnie" z nową dostawą fajnych środków. Podała Mordredowi numer Piotra. Klara namierzyła telefon w operze i przekazała lokalizację Mordredowi.

A w operze pojawili się mordercy, chcący zabić Piotra, który się dzielnie chowa. Chowanie nie zdało się na wiele, thug go znalazł. Właśnie miał do niego strzelić, kiedy w jego miejsu pojawił się Mordred rozpękając thuga. Krew i flaki WSZĘDZIE. Drugi thug pojawił się za chwilę, ale Mordred był tak przerażający, że spudłował i zaczął uciekać. Daleko nie uciekł, Mordred oplątał go siecią i podwiesił pod sufitem.

Kiedy zastanawiał się, co dalej, z góry zszedł mag - dowódca grupy. Mag jest paskudnie pokiereszowany, z 'malicious technomancy' w swoim ciele. Zaczął od pytania Mordreda o co mu chodzi i zdradził, że chce Piotra dla dziewczynki (tej z lizakiem) - obiecali mu dziewczynkę a przez Piotra ona zniknęła. W tym momencie Mordred zaatakował, a Piotr dołożył obraz uwodzicielsko liżącej lizaka dziewczynki. Wraży mag został unieszkodliwiony i pokonany. Mordred wyłapał resztę thugów a Piotr w tym czasie przesłuchiwał maga, starając mu się wmówić, że ma dziewczynkę. Mag powiedział, że to Kinglord - to on szuka teraz Piotra, to on chce wszystko osiągnąć. Bo chce wejść na miejsce Karradraela i zostać bogiem.

Nieźle. Gorzej, że ten mag jest żywą bombą i zaraz wybuchnie. Teleport Mordreda jeszcze bardziej zniszczył operę i przeniósł ich wysoooooko; szczęśliwie, uratowała ich Karolina na latającym dywanie...

Wystawieni poza miasto, Piotr nie chce być śledzony już przez Mordreda. Niestety, ma na sobie aurę kapłana Iliusitiusa. W nocy sobie spał a Mordred czuwał. Gdy pojawił się oportunistyczny terminus Zajcewów i chciał sobie zapolować i wysadzić motel z kapłanem, cóż... Mordred porwał i teleportował się z Piotrem. Do... opery.

**Podsumowanie torów**



**Interpretacja torów:**



# Progresja



# Streszczenie

Arazille spotkała się osobiście z Silurią i dała jej dowody zbrodni Blakenbauerów. Rosną nastroje antymausowe podżegane przez Sowińskiego który nie może wybaczyć śmierci Sabiny. Kinglord poluje na Piotra, ale Mordred go uratował. Piotr chce być wolny od ochrony, ale dzięki aurze kapłana Iliusitiusa... polują na niego magowie i Mordred ratuje go przed śmiercią, za co Piotr nie jest mu szczególnie wdzięczny ;-). Piotr nadal maluje obrazy pokazujące to, co Iliusitius chce pokazać...

# Zasługi

* mag: Piotr Kit, emitujący aurę kapłana Iliusitiusa nieświadomie, dogaduje się ze swoją Operiatrix, niechętnie maluje pod przymusem i ciągle ktoś mu włazi do JEGO domu. Planuje przenieść się do opery.
* mag: Mordred Blakenbauer, ratuje Piotra przed przypadkowymi zabójcami i oportunistycznym terminusem. Dostaje opiernicz - zasłużenie. Zniszczył obrazy Piotra wzywając moc Arazille. Ogólnie, teleporty ma Paradoksalne.
* mag: Hektor Blakenbauer, ranny, na wózku, oprowadza Silurię po Rezydencji i opowiada o rodzie. Powiedział jej o Eis.
* mag: Siluria Diakon, spotkała się z Andy i dostała aktówkę o grzechach Blakenbauerów, co chwilowo przed Hektorem zataiła. Dowiedziała się o Tymotheusie. Współpracuje z Mordredem, acz ostrożnie.
* vic: Crystal Shard, już: Andy Diakon. Odtworzona przez Arazille - żywy wyrzut sumienia Diakonów i Blakenbauerów. Pomaga Arazille i spotkała się z Silurią by dać jej dowody winy Blakenbauerów.


# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik
                    1. Stara Opera, aktualny 'dom' Piotra Kita i miejsce którym zarządza.

# Czas

* Dni: 1


# Waśnie

1. Wojna o autowara:
    1. Kinglord przejmuje kontrolę nad Bzizmą:   0/20
    1. Autowar wpada w łapki Arazille lub Iliusitiusa: 0/20
    1. Blakenbauerowie nadal mają pakiet kontrolny autowara: 0/20
    1. Świeca odzyskuje autowara: 0/20
    1. W okolicy autowara otwiera się Portal Illuminati: 0/20
2. Rozpad Blakenbauerów:
    1. Andromeda / Arazille skutecznie poróżniła Diakonów i Blakenbauerów:  0/20
    1. Stary konflikt Otton / Tymotheus rozpalił się z woli Iliusitiusa:    0/20
    1. Blakenbauerowie dali radę utrzymać jeden wspólny front:    0/20
    1. Siły Kinglorda niszczą rzeczy Blakenbauerom bliskie:     0/20
    1. Blakenbauerowie muszą wydać Judytę Maus i Mausów Sowińskiemu:    0/20
3. Naturalna Entropia:
    1. Operiatrix zostaje zniszczona:   0/20
    1. Piotr jest oskarżony o bycie kapłanem Iliusitiusa:   0/20
    1. Karina ulega ewolucji z woli Kinglorda:  0/20
    1. Skubny się wymyka:   0/20
    1. Potrzebne jest wsparcie Renaty:  0/20
    1. Potrzebne jest wsparcie Eis:     0/20


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |
