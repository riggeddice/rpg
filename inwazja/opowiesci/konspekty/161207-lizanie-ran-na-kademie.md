---
layout: inwazja-konspekt
title:  "Lizanie ran na KADEMie"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161130 - Sprowadzenie Mare Vortex (HB, SD)](161130-sprowadzenie-mare-vortex.html)

### Chronologiczna

* [161130 - Sprowadzenie Mare Vortex (HB, SD)](161130-sprowadzenie-mare-vortex.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Hektor obudził się w łóżku; stoi nad nim Norbert (naczelny lekarz KADEMu) i pogratulował mu powrotu do przebudzenia. 

"Muszę porozmawiać... kto tu dowodzi?" - lekko otępiały Hektor
"Chwilowo ja, bo jestem lekarzem" - Norbert, stanowczo

"Panie doktorze... spędziliśmy... nie wiem ile w koszmarze. Muszę zobaczyć czy się im nic nie stało." - zdeterminowany Hektor
"Teraz już pan wie. Nikt nie jest tak głupi by policzkować Blakenbauera. Witam na KADEMie" - Norbert, policzkując Hektora

"Po co... po co te simulacra?" - Hektor
"Bo nie mam dość lekarzy na to co przyprowadziliście. Muszę się zwielokrotnić" - Norbert, lekko zmęczony

"Oooooi! Norbert! Widziałeś tego śledzia?" - lekko bełkoczący Ignat
"Ignat..." - Norbert
"OSKARŻAM GO! Oskarżam Hektora! O bycie NUDNYM!" - Ignat
"..." - Hektor

"Nie wiem jak to działa..." - zaczął Hektor
"Nikt nie wie" - Norbert, ponuro

"Muszę porozmawiać z Martą Szysznicką... goni nas Karradrael! Mam na sobie marker!" - Hektor
"Nie możemy. Uciekamy z Fazy Esuriit. Marta *jest* zamkiem." - Norbert, bezczelnie kłamiąc (Quasar jest zamkiem)

Norbert przypomniał Hektorowi, że Edwin był na KADEMie. Pracował z nim. Norbert nie pozwoli, by komukolwiek stała się krzywda. 

"Strzeżcie się Spustoszonych. Zinfiltrowali Świecę. Nie byli ich w stanie wykryć" - Hektor do Norberta.
Quasar słyszała. EIS też.

Siluria otworzyła oczy. Widząc Norberta chciała się podciągnąć... jest sparaliżowana.
Norbert lekko ją przytulił i wyjaśnił, że dotknęła ich wszystkich Esuriit. Więc są same simulacra. 

"Padł? Kompleks Centralny padł?" - zaskoczony Norbert
"Byłam tam. Kompleks Spustoszony. Prawie weszli na EAM. Odcięli Świecę..." - Siluria

Siluria powiedziała o Karradraelu. Norbert wyjaśnił, że będąc na Esuriit i zdobywszy te czerwie pracują nad szczepionką blokującą Karradraela z Franciszka Mausa.

"To była SIÓDMA baza Świecy" - Siluria, zaznaczająca zniszczenia
"Matko..." - Norbert, szczerze przejęty

"Nie bądź zbyt okrutna. Przez bardzo wiele przeszła" - Siluria do Quasar o Mariannie
"Nie jestem okrutna. Jestem skuteczna." - Quasar, spokojnie
"To to samo." - Siluria, patrząc prosząco na Norberta

Norbert przytaknął. Nie pozwoli na skrzywdzenie pacjentki.
Siluria poszła spać...

Dzień 2:

KADEM nic nie wiedział. Hektor się obudził. Może się ruszyć. Działa. Stoi nad nim młoda ładna dziewczyna. Uśmiecha się do niego.
Blakenbauerowie na Fazie Daemonica są mocniej zsynchronizowani ze swoimi Bestiami - i mentalnie i fizycznie. Karina wyjaśniła, że się nim opiekuje, bo pracowała z jego bratem.

Siluria się obudziła z ryków Ignata natrząsających się z Hektora.

"Ty wiesz że zgubiliśmy głowicę?" - Ignat, cicho
"IGNAT! Musisz mi opowiedzieć co się działo! Bo ja nic nie wiem!" - zrozpaczona Siluria

Norbert powiedział, że ze Świecowcami gorzej. Od dawna tu byli; jutro wrócą do normy. Marianna NAPRAWDĘ utrzymała ich przy życiu...
Norbert powiązał datę ile to potrwa z wysadzeniem przez Andreę portalu na EAM (nie Silurię, Andreę).

Ignat wziął Silurię do sal symulacji, gdzie po prostu synchronizacja jest dużo szybsza.
Siluria zfacepalmowała jak się o wszystkim dowiedziała...
Siluria przekazała wszystko co wiedziała KADEMowi.

Ilona Amant powiedziała Silurii, że nie ma dostępu do Głowicy na Primusie. Monitory na Primusie zostały zniszczone. Najpewniej KADEM na Primusie jest bardzo uszkodzony jak nie zniszczony...
Siluria powiedziała, że Głowica ich obroniła, ale jest bez kontroli. Ilona powiedziała, że nic nie da się zrobić... ma nadzieję, że Głowica się nie wysadziła z połową miasta...
Nie wiadomo jak dużo energii zostało Aegis...

Tymczasem Hektora złapała Andżelika.

"Hektorze, chciałam Cię prosić o oskarżenie Marianny Sowińskiej o zbrodnie przeciwko magom" - Andżelika, poważnie
"Hektorze, wyślizgnie się. To Sowińska. Wiem, co zrobi Świeca. Trzeba coś zrobić TERAZ." - Andżelika, wściekła
"W odróżnieniu od niej, zachowałeś człowieczeństwo" - Andżelika, zimno
"Ty jej nie powstrzymałeś - ale ona wydawała rozkazy. Ona to zdecydowała." - Andżelika

"Będę świadkiem. Powiem dokładnie jak było." - Hektor
"To się wywinie." - Andżelika, odchodząc

Andżelika nie zgadza się z decyzjami Marianny i nie może jej wybaczyć. Bo Kuba by coś wymyślił...
Marta Szysznicka dowiedziała się od Andżeliki co zrobiła Andrea i co zrobiła Siluria. Złożyła oraz sytuacji, w końcu.
Czyli nagle KADEM wie.

Marta poprosiła do siebie Hektora i Silurię. Poszli.

Marta wyjaśniła co się stało. Bomba Fazowa - to, co zrobił Karradrael - rozerwało powiązanie między Fazą Daemonica a Primusem. Mare Vortex istnieje, ale nie jest powiązane z Kopalinem. Kilka razy Whisperwind próbowała; niestety, za każdym razem trafiała na jakiś ocean.
Co jest potrzebne? Muszą przesłać odpowiednie typy kotwic na Primusa; teraz jak jest Opel Astra pojawia się taka potencjalna możliwość, acz spowoduje to straszne fluktuacje i Karradrael usłyszy. Jako, że Opel musi przetrwać tam jakiś czas...

Teraz jak są magowie Świecy, Marta jest w stanie zbudować most między Świecą Daemonica a KADEMem Daemonica. Jest punkt zaczepienia. Warmaster i Whisperwind są w stanie połączyć te światy sensownie; oni umieją poruszać się po Fazie Daemonica jeśli mają punkt docelowy.

Franciszek wszedł do centrum dowodzenia. Powiedział, że dawno temu Diakoni i Mausowie - zgodnie z legendą Mausów - mieli pecha i się przyczepiła do Primusa jakaś Faza. I docelowo oderwali Primusa od tej Fazy.
Bomba Fazowa. Coś, co zna tylko Karradrael... coś tak starego.

Franciszek powiedział, że Karradrael... jest INNY. Nie ten umysł. Nie ta melodia. Cholera wie, co Aurelia mu zrobiła, ale Karradrael jest... bardzo uszkodzony. Jedna wola, jeden cel.
Jeśli Renata zginie i zintegruje się z Karradraelem... będzie tak już zawsze.
Karradrael w tej chwili jest chory. I ta choroba dotyka wszystkich Mausów.

"Czy jest jakiś sposób by go powstrzymać?" - Hektor
"To Karradrael. Myśli pięć kroków przed tobą." - podłamany Franciszek
"Co można zrobić by to wyleczyć?" - Hektor
"Potrzebujemy ekspertów od soulbindu. KADEM i Świeca Daemonica. Coś zrobiła Aurelia." - Franciszek

Marta powiedziała, że trzeba zrobić wyprawę. Siluria westchnęła. Nie ma siły...
Marta zauważyła, że w najbliższym czasie oni niespecjalnie mogą działać.

"Czyli najporządniejszy Maus jakiego poznaliśmy był istotą Esuriit i poświęcał magów i ludzi w rytuałach krwi? XD" - Dust, 2016

"Karradrael założył mi marker..." - Hektor
"Tak. Norbert go usunie." - Marta

Franciszek jako ekspert od Mausów, Norbert jako ekspert medyczny i Siluria pomagająca Norbertowi - naprawiają Hektora.
A potem Siluria poprawia Franciszkowi humor opowiadając o jego kuzynie - Pawle - który wiedział, że nie ma dla niego ratunku i zdecydował się pomóc wszystkim innym.

Dzień 3:

Silurię rano obudził Ignat. Nie jest zainteresowany seksem - musiał na coś wpaść.
Ignat powiedział, że gdy głowica majaczyła, to opowiadała sceny z KADEMu. Czyli wiedziała o KADEMie. Ignat sprawdził z Infernią - faktycznie głowica znała prawdę. WIDZIAŁA KADEM.
Potem gdy głowica była ładowana... do zdobycia wiedzy, Ignat musiał iść do Powiewu Świeżości.
I co znalazł w bibliotece?
Zamek As'caen.

Powiew Świeżości jest w Zamku As'caen. Tak jak KADEM. Muszą tylko znaleźć przejście i mają połączenie z Primusem!

Siluria otworzyła szeroko oczy.

Hektor, nie wiedząc jak na niego działa Faza Daemonica, poszedł spotkać się z Anną Myszeczką. Ona jest z Mikadem; są razem (bo czują się nie najgorzej w swojej obecności). Hektor zapukał i wlazł.
Anna prawie go postrzeliła widząc nieznanego potwora. Mikado ją zatrzymał. Anna w końcu poznała; to Hektor.
...oni grali w JENGĘ. Serio. I Anna wygrywa.

Hektor wyczuł, że Anna jest głęboko, głęboko uszkodzona. Ale nie bardziej niż wcześniej.

Hektor stwierdził, że pójdzie się spotkać z innymi magami Świecy i zobaczy co z nimi.
Mikado zaznaczył, że najlepiej, jeśli Hektor spotka się z Silurią - ona ma gotowy zestaw świetnych przypadków prawnych.
Siluria załatwiła to Hektorowi... wysłała go do hali symulacji.

# Progresja

* Hektor Blakenbauer: traci marker od Karradraela
* Franciszek Maus: ma szczepionkę Esuriit odcinającą go od Karradraela (w ramach potrzeby)

## Frakcji

* KADEM: potrafi fabrykować szczepionkę Esuriit odcinającą Mausów od Karradraela na pewien czas

# Streszczenie

Chwila wytchnienia - na KADEMie. Okazuje się, że KADEM nic nie wiedział; Faza Daemonica i Primus w tym zakresie się rozerwały i Mare Vortex "dryfuje". To działania Karradraela - dawno temu już coś takiego zrobiono. Ignat wpadł na to, że przecież da się dostać na Primusa przez Powiew Świeżości dzięki temu, że gdy ładował Głowicę to ta miała dostęp do wiedzy o KADEMie o którym nie powinna wiedzieć. Andżelika nie potrafi wybaczyć Mariannie tego, co ta zrobiła. Dzięki powrotowi Zespołu, KADEM ma możliwość otwarcia drogi zarówno do Świecy Daemonica jak i na Primus. A Marta wie "wszystko" co jest potrzebne i co do tej pory się zdarzyło i od Silurii i od Andżeliki i od Hektora.

# Zasługi

* mag: Hektor Blakenbauer, nie do końca zdający sobie sprawę z częściowej transformacji, martwiący się o ludzi i przekazujący ważne informacje
* mag: Siluria Diakon, w końcu w domu, namawiająca do nawiązania kontaktu ze Świecą Daemonica. Odciągająca Hektora od kontaktów z * magami Świecy
* mag: Norbert Sonet, były mentor Edwina, leczący wszystkich poszkodowanych z Esuriit i sympatyczny jak zawsze, działający przez simulacra
* mag: Quasar, zintegrowana z Zamkiem As'Caen, broniąca się przed Esurit na czas leczenia ofiar i bezpiecznie ewakuująca wszystkich do Fazy Daemonica
* mag: Karina Paczulis, KADEMka która studiowała pod Edwinem na KADEMie i bardzo lubi pracować z Blakenbauerami; zaufany medyk Norberta
* mag: Ignat Zajcew, detektyw kojarzący fakty, powiedział o możliwym wyjściu z Fazy Daemonica przez Powiew Świeżości - czego dowodem Głowica Aegis
* mag: Ilona Amant, naczelna ekspert od Głowicy i EIS; dzięki jej wskazówkom Ignat dotarł do tego, że jest droga na Primusa przez Powiew
* mag: Andżelika Leszczyńska, żądająca od Hektora srogiej kary dla Marianny Sowińskiej i wyraźnie odrzucająca kontekst sytuacji "bo Kuba Urbanek by rozwiązał"
* mag: Marta Szysznicka, przywódczyni KADEMu, która jasno wytłumaczyła jak wygląda sytuacja i która dostała pełnię danych i od Andżeliki i od Silurii i od Hektora
* mag: Franciszek Maus, wyjaśniający, że Karradrael jest chory i sposób na wyleczenie go wy* maga współpracy z * magami Świecy Daemonica; zna stare legendy Mausów
* mag: Anna Myszeczka, która prawie postrzeliła Hektora (nie poznała go w nowej formie) i wygrywa z Mikado w jengę. Zaprzyjaźniła się z nim... jakoś
* mag: Mikado Diakon, który przegrywa w jengę z Anną Myszeczką. Ratuje * magów Świecy przed zobaczeniem Hektora sugerując Halę Symulacji

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen, ciągle monitorowany przez Quasar, która "słyszy wszystko" co tu się dzieje
                1. KADEM Daemonica, prawdziwe oblicze gildii
                    1. Skrzydło medyczne, gdzie regenerowali się de facto wszyscy a Norbert używał simulacrum
                    1. Układ nerwowy
                        1. Sale symulacji, gdzie wpierw synchronizował się Ignat z Silurią a potem Hektor oglądał magiczne sądy
                        1. Centrum dowodzenia KADEMem, ekranowane od Fazy, by paranoja Marty nie robiła własnych fantomów
                    1. Skrzydło wypoczynkowe
                        1. Wewnętrzny Ogród, który mimo nazwy potrafi zjadać gości
                        1. Pokoje gościnne, które nie przez przypadek są koło Wewnętrznego Ogrodu

# Czas

* Dni: 2