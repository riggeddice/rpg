---
layout: inwazja-konspekt
title:  "Klemens w roli swatki"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150913 - Andrea węszy koło Szlachty (AW)](150913-andrea-weszy-kolo-szlachty.html)

### Chronologiczna

* [150819 - Krótki antyporadnik o sojuszach (HB, SD, Kx)](150819-krotki-antyporadnik-o-sojuszach.html)

### Logiczna

* [150408 - Rykoszet zimnej wojny (HB, DK, AB)](150408-rykoszet-zimnej-wojny.html)

### Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Powiedz mi coś o dziewczynie, przez którą Marcel wpadł w kłopoty.
D: Ma lepkie ręce. Cicha, zwinna, niepozorna. Absolutnie niebojowa.
Ż: Dlaczego właśnie jego? Co ma za szczególną wiedzę którą chce się dowiedzieć?
B: Vladlena może nie wiedzieć, ale zna połączenie pomiędzy Emilią a niebezpieczną grupą przestępczą.

Ż: Jaki podły acz niekaralny czyn może zostać ukarany pojedynczym zdjęciem wykonanym we właściwym momencie?
K: Mówimy o hipsterze żyjącym w pełni wegańsko (wpływowym), który aktualnie je kebaba z człowiekiem, który atakuje squat "Dante"
Ż: Z jakich przyczyn lokalny portal kopaliński stał się tak wpływowy w Kopalinie i okolicach? 
B: Zaczęło się od portalu typu "Spotted: Kopalin" a potem it got traction.

## Misja właściwa:

### Faza 1: 

Do Klemensa przyszedł zdenerwowany Edwin. Akurat niedaleko przechodziła Alina, więc ją też po drodze zwinął. I huknął od wejścia, że Marcelin potrzebuje dziewczyny. Klemens lekko zbaraniał - to dokładne przeciwieństwo normalnych poleceń wydawanych przez Edwina. 
Edwin się opanował jakoś i wyjaśnił powoli sytuację. Marcelin wpakował ich w sojusz, potem się sojusz zerwał, potem sojusz wrócił, potem Blakenbauerowie oddali mnóstwo stworzeń defensywnych...
...więc Marcelin potrzebuje dziewczyny. Pomysłem Edwina jest Wanda Ketran. Gdy była opętana przez Arazille, Edwin i Margaret poznali jej wzór dość dokładnie (wysokokosztowe, ale mają jej wzór bo było potrzebne do odcięcia Arazille); dzięki temu są w stanie wpłynąć i na Wandę i na Marcelina, by ten związek trzymał się dłużej niż 30 minut.

Ale do tego celu - Wanda nie może mieć chłopaka i ma trafić dość blisko Rezydencji. I to jest problem Klemensa. I on za to odpowiada.

Marcelin ma zapomnieć o wszystkich Vladlenach, Leokadiach i innych Inferniach.
Edwin nie wyspecyfikował ile czasu Wanda ma być dziewczyną Marcelina. Po prostu ma nią zostać...

"Klemensie. Marcelin to Twój problem. Polityka to mój. A jak coś się spieprzy, Alina odpowie" - zdenerwowany Edwin.

Pierwszy plan Klemensa jest bardzo prosty. Wanda będzie bardzo atrakcyjna dla Marcelina (Edwin <3), ale jednocześnie będzie miała silną odrazę do facetów (Margaret może bez problemu modyfikować struktury Wandy). To powinno skłonić Marcelina do tego, by się nią zainteresować i poprosić Edwina by przetrzymać ją dłużej w Rezydencji; to powinno zapewnić zaangażowanie Marcelina i spokój. Czyli dokładnie to co jest Edwinowi w chwili obecnej potrzebne.

Do tego celu trzeba Wandę mądrze porwać. A do tego celu - dowiedzieć się czegoś więcej o samej pannie Ketran...

Pierwszy ruch - "Działo Plazmowe". Klemens zakłada swag (odpowiednio sformowana maska gazowe z dużymi oczami ale w klimacie) i kilka diód na garniturze (korpo-cyber-drone). Alina zmienia trochę wygląd - suknia z drucikami, diody itp. W temacie i w klimacie.
A więc, "Działo Plazmowe"...

A w "Dziale Plazmowym" lata dron. I to nie byle dron, wysokiej klasy dron. Od ręki znaleźli kontrolera - Wanda Ketran, otoczona wianuszkiem młodych ludzi. Z rozmowy wynika, że Wanda dostała drona z polskiej firmy i robi za reklamę. To model "Czarny Błysk", skonstruowany przez firmę Skrzydłoróg, której szefowa i Wanda się przyjaźnią a chwilowo "Czarny Błysk" i podobne są w promocji.
Podpytana, Wanda powiedziała, że to technicznie jej dron ale nieco za drogi by być całkowicie jej; za 3 dni testów ona odda "Czarny Błysk".

Czyli nie można jej po prostu zniknąć. Nie teraz. Za kilka dni. Ale potem - jasne. Więc warto ten czas wykorzystać by się z nią dokładniej zapoznać i zobaczyć co i jak; w tym czasie warto się jej przyjrzeć.

Alina bez problemu wyśledziła lokalizację Wandy. Squat "Dante", nie w ścisłym centrum Kopalina. Tam działa.
Alina poczekała jeszcze trochę; Wanda wyszła z 2 kolesiami; takim niezłym irokezem i drugim chłopakiem. Idzie pod rękę z tym drugim. Czyżby chłopak?

Alina ich wyprzedziła, zmieniła trochę wygląd i ruszyła w ich kierunku. Podeszła do chłopaka Wandy, spoliczkowała go (by zdobyć próbkę krwi) udając, że ją zdradził i poszła dalej. Wanda pobiegła zatrzymać Alinę, ale ta udawała, że on ją skrzywdził. Nie przespał się z nią, skrzywdził, by wbić klina między Wandę a chłopaka.

Klin został wbity z powodzeniem. Wanda i jej chłopak mają ostrą rozmowę w pokoju w jej mieszkaniu.

### Faza 2: 

Wczesnym południem Wanda wypadła z mieszkania i odpaliła drona. Ona odpowiada na to co słyszała w telefonie; wyraźnie kogoś śledzi. Alina i Klemens doszli do tego kogo; śledzi takiego rozglądającego się ostrożnie hipstera. Z uwagi na baterie drona, ona musi robić krótkie przerwy. I w tym czasie, Alina i Klemens mogą przechwycić hipstera.

Pytanie czy chcą. No jasne że tak.

Klemens obezwładnia hipstera i ściąga go z widoku. Alina podrzuca pluskwę Wandzie i patrzy na jej reakcję na zniknięcie hipstera. Wanda jest wielce zmartwiona; Alina usłyszała fragment rozmowy Wandy przez telefon z kimś gdzie Wanda powiedziała o tym, że "zgubiła Kajetana i ma nadzieję, że znajdzie go u Ormię".
Klemens wysłał samochodem hipstera do Edwina a sam wrócił do ubezpieczania Aliny. Wanda zdecydowała się na przeniesienie drona w inne miejsce, obserwować inną lokalizację. Wróciła szybko do domu, zmieniła baterie do drona i wzięła kask, wzięła motor (który Alina zapluskwiła) i ruszyła. Jako, że Alina ma lokalizator, bez kłopotu znalazła gdzie jedzie Wanda.

A ta pojechała na osiedle domków jednorodzinnych, takich grodzonych (osiedle strzeżone "Myszołów"). Puściła drona skupiając się na jednej konkretnej posiadłości. Alina też tam dotarła, przekształciła się w kota (poza zasięgiem drona) i poszła na zwiady do tamtej lokalizacji. Wpierw podsłuchała to, co Wanda mówi przez telefon. "Nie ma tu Kajetana, niestety, a Ormię czeka..."

Alina weszła jako kotek na teren posiadłości; posiadłość dość bogata, należy do biznesmena (ów Ormię), który czeka. Ormię się niecierpliwi. Niestety, Alina nie dała rady wiele więcej zobaczyć; Ormię ma psy i są zainteresowane małymi kotkami (Aliną).
Tymczasem Borys z Edwinem przesłuchali hipstera. Hipster Kajetan był kiedyś jednym z szefów społeczności squata "Dante", ale wypadł z obiegu; teraz pojawiła się okazja sprzedać innych przez zaaranżowanie wypadków czy problemów by policja musiała interweniować i ich usunąć (z zyskiem dla Ormiego). Nie miał pojęcia, że Wanda go obserwuje.

Oki. Wypuścili Kajetana tak, by Ormię się zbyt nie zdenerwował. I doszło do spotkania Kajetan - Ormię. Wanda się uśmiechnęła "got ya". Jako, że Kajetan dostał pluskwę (żywą, działa jak pluskwa) od Margaret, Alina i Klemens słyszą wszystkie interesujące rzeczy związane z tą rozmową. Ormię faktycznie chce, by Kajetan sabotował squat. W ten sposób będzie musiał pojawić się sanepid i squat zostanie zamknięty. Ale on, Ormię, nie ma mieć z tym nic wspólnego no matter what.

To co ciekawe to to, że Ormię nie jest właścicielem tego terenu. A to nie był budynek mieszkalny. Więc Wanda ma obraz (łącznie ze zdjęciem jak Ormię płaci Kajetanowi) a Alina ma dźwięk.
Misja wykonana.

### Faza 3:

Ogólnie rzecz biorąc, sytuacja wygląda doskonale. Alina wysłała Wandzie nagranie dźwięku z dopiskiem "przyjaciel" i Wanda w squacie "Dante" skonfrontowała wszystkich z dowodami, miażdżąc Kajetana i go dyskredytując i wyrzucając ze społeczności.
Niedługo potem oddała drona do firmy "Skrzydłoróg". Był bardzo potrzebny.

Klemens i Alina porwali Wandę jeszcze później i wciągnęli ją do Rezydencji. Tam Edwin i Margaret dostosowali Wandę do tego co było im potrzebne i Klemens przekonał Marcelina, żeby ten SAM wystąpił do Edwina że trzeba się Wandą zainteresować bo są jakieś ślady po Arazille. Koniec końców, misja wykonana - Marcelin skupił się na Wandzie i szybko zostali parą. Problemy polityczne i poza-Rezydencyjne zniknęły.

Interesujący efekt uboczny - nie ma kontrolera "płaszczek" dla Vladleny. Coś, czym się muszą zająć w najbliższym czasie...

Dla pewności że wszystkie wątki są zamknięte, Edwin sprawdził pamięć Wandy. Okazało się, że tak. Squat "Dante" był założony przez kilka osób, min. przez Kajetana. Kiedyś Kajetan był faktycznie szychą, ale wypadł z cyklu; nagle niedawno zaczął się interesować zbieraniem sojuszników ale w dziwny sposób. Powiązało się to z ogólnym zagrożeniem dla squatu na co wpadł Stanisław Pormien, przyjaciel (nie chłopak, ale prawie) Wandy. I on poprosił Wandę o pomoc. Wanda ze swoim [acuity-heart subtle] weszła do akcji i się skupiła na tym; szybko wyczuła potencjalne dziwne ruchy Kajetana. Jakieś poprzednie działania i informacja od Pormiena wskazała Wandzie Ormiego.

I tu na pomoc przyszła Dagmara Czeluść, przyjaciółka Wandy i szefowa Skrzydłoroga. Dostarczyła Wandzie drona. Wanda, charyzmatyczna jak zawsze, sformowała dookoła siebie grupę mało ważnych członków Dantego i wyciągnęła informacje odnośnie Kajetana i Ormiego a resztą zajęły się drony.

Czemu Ormię to robił? Ma syna w squacie i chciał, by ten wrócił do domu. Wanda z całą bezwzględnością umożliwiła Annie Górze z "Kociego Oka" (lokalny portal kopaliński) poznanie tej historii i jedynie bardziej oddaliła młodego Ormiego od ojca oraz pokazała mroczne mechanizmy jakimi kierował się biznesmen.

Pięknie łączy się to ze zniknięciem Wandy potem :-).

# Streszczenie

Edwin chce Wandę Ketran jako dziewczynę dla Marcelina. Bo ma jej dokładny wzór. Klemens i Alina wysłani by ją porwać niezauważenie. Pomogli wrobić biznesmena Ormię w jej zniknięcie. Aha, Wanda ma eksperymentalną dronę "Czarny Błysk" z firmy Skrzydłoróg.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Squat Dante
                    1. Osiedle strzeżone Myszołów
                        1. Dom Tomasza Ormię
                    1. Dzielnica Trzech Myszy
                        1. Mieszkanie Wandy Ketran

# Zasługi

* vic: Klemens X, skuteczny porywacz hipsterów (i Wandy), który opracował plan jak zdobyć Marcelinowi dziewczynę.
* vic: Alina Bednarz, skuteczna jednostka zwiadowcza i rozbijająca związki, których tak naprawdę nie było.
* czł: Wanda Ketran, "cybergothka z motorem i dronem". Detektyw, agent specjalny i ofiara porwania. Już: dziewczyna Marcelina.
* czł: Dagmara Czeluść, szefowa firmy "Skrzydłoróg" specjalizującej się w dronach na zamówienie; pomogła Wandzie dostarczając drona.
* czł: Stanisław Pormien, nie chłopak Wandy, choć wszystko tak wyglądało. Dostał od Aliny za niewinność. Ściągnął Wandę do squata.
* czł: Kajetan Pyszak, hipster-szuja, który w sekrecie działa przeciwko squatowi "Dante". Skompromitowany przez Wandę.
* czł: Tomasz Ormię, biznesmen, który chcąc "ratować" nastoletniego syna ze squata chciał zniszczyć ów squat, przy okazji czego komuś mogło coś się stać.
* czł: Anna Góra, dziennikarka "Kociego oka" (lokalnego i poczytnego portalu w Kopalinie), która opublikowała materiały o Ormiu od Wandy.
* mag: Edwin Blakenbauer, twórca wysokopoziomowej intrygi "dziewczyna dla Marcelina", zrzucił problem i zaakceptował plan. Strzelił sobie w stopę.