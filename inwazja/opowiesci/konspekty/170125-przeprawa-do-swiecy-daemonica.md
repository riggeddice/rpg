---
layout: inwazja-konspekt
title:  "Przeprawa do Świecy Daemonica"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170104 - Spalone generatory pryzmatyczne (HB, SD)](170104-spalone-generatory-pryzmatyczne.html)

### Chronologiczna

* [170104 - Spalone generatory pryzmatyczne (HB, SD)](170104-spalone-generatory-pryzmatyczne.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Hektor otworzył oczy. Czuje się... inaczej. Lucjan Kopidół zamontował na Hektorze opaskę z diodami, która to opaska zapewnia blokadę transformacji w bestię. Hektor nadal nie ma pojęcia, że był w drugiej formie. ma blokadę percepcji. Lucjan chciałby Hektora "naprawić", ale weszła Infernia i go wywaliła. Zarzuciła Hektorowi, że ten użył przeciw niej osób trzecich i jej nie powiedział. Chce ją zniszczyć politycznie. W Świecy. A jadł jej jedzenie! Infernia jest święcie oburzona; chce satysfakcji. Chce rozszarpać Hektora na kawałeczki... Hektor ją zdradził i obraził jej zdaniem. Gra nie fair.

I na ich kłótnię weszła Siluria.

* Siad! - Siluria
* Co ja, pies jestem?! - oburzony Hektor
* Usiądź! - Siluria, do Inferni
* JA umiem się zachować. - Infernia, płonąca z furii.

Konflikt (4+1 v 3+2): S. Siluria dała radę uspokoić Infernię i odsunąć widmo wojny. Infernia powiedziała Silurii, że widzi, że ta chroni Hektora z jakiegoś powodu i ma nadzieję, że wie co robi. Wyszła z pomieszczenia na szpilkach jak sztylety...

* Czy mogę liczyć na to, Herr Hektorze Blakenbauerze, że nie będzie pan prowokował mojej koleżanki? - Siluria, zimno
* Czy ręczy pani czarodziejko Diakon za moją przyjaciółkę, że nie wystąpi przeciwko mnie i mojej rodzinie? - Hektor, zimno

I zaczęła się... kłótnia Silurii z Hektorem o Infernię/Marcelina. Hektor zażądał gwarancji, że Inferni można ufać. Siluria powiedziała, że jak długo nie stanie się coś spowodowanego przez Hektora przeciwko Inferni, to do niczego nie dojdzie. Hektor zaznaczył, że wcześniej rozmawiał o Inferni min. z Wiktorem Sowińskim. Innymi słowy, Świeca MOŻE wykonać działania przeciw Inferni. I dokładnie teraz się dowiedziała - i to ją sprowokowało...

* Świat magów jest dla mnie kompletnie niezrozumiały - Hektor, z rezygnacją
* Bardzo wygodna wymówka - Siluria, z pogardą
* Najpierw atakują, potem żądają przeprosin... ale zostawmy to teraz, mamy ważniejsze sprawy - Hektor, z mocą
* Panie prokuratorze, nie mogę zostawić przyjaciółki. - Siluria
* Jeśli pani jej ufa... - Hektor
* ...powierzyłabym jej swoje życie! - Siluria, z mocą
* ...to ja nie mam zamiaru jej atakować teraz. Mamy ważniejsze sprawy na głowie, Karradraela! - Hektor, krzycząc
* Hektorze Blakenbauerze, takie trywialne sprawy mogą doprowadzić do tego, że NIKT NAM NIE POMOŻE! - Siluria, krzycząc
* To co pani proponuje? Ukorzyć się? Płaszczyć? Kupić jej kwiaty? Zagwarantować, że nic nie zrobię przeciwko niej? Dla dobra sprawy mogę. - Hektor, w desperacji

...

* Może należało się dowiedzieć więcej o Inferni. - Siluria, zimno
* Gwarantuję, że podczas procesu dowiedziałbym się więcej. - Hektor, zimno

...

Siluria wyjaśniła, że padły generatory i stąd ten artefakt. I poprosiła, by Hektor tego nie zdejmował.

Wszedł Ignat i zaczął krzyczeć na Hektora, że ten tak potraktował maga KADEMu (Infernię). Chciał wyzwać Hektora na pojedynek... działo strumieniowe Inferni zmiotło go i połamało. Infernia powiedziała, że to jej osobista sprawa i Ignat ma się nie wtrącać. Jest wściekła. Siluria zaprowadziła Ignata do Norberta. Tymczasem Quasar postawiła Hektorowi strażników i dała mu elementy prawne do analizy... wszystkie sprzeczne i chore regulacje KADEMu.

"Zasada numer jeden. Nie wolno krzywdzić magów KADEMu. Zasada numer dwa. Problemy można rozstrzygać pojedynkami. Numer trzy. Pojedynki mogą być niespodziewane. Numer cztery. Elementem pojedynku MOŻE być odwołanie się do zasad... numer pięć, nie wolno karmić kaczek w okolicy betonu." - prawo, KADEM...

Siluria odprowadziła Ignata i wróciła do Hektora. Ten jeszcze powiedział Silurii o Ilonie Amant i jej... zabawkach. Powiedziała niepokojącą rzecz - o pewnej głowicy bojowej, która wyczerpana biega po Kopalinie i może anihilować ludność. Hektor powiedział, że muszą jakoś wrócić i ją powstrzymać. Siluria powiedziała, że jest w pełni naładowana i że to TA głowica, która ich osłaniała w walce z Karoliną Maus.

* Jeśli nie dostaniemy się na Primusa, i tak nie możemy nic zrobić, Hektorze - Siluria, ze smutkiem
* Nie możemy się skontaktować z Primusem? - Hektor, zmartwiony
* Nie, jesteśmy oderwani. Musimy tam się wybrać... - Siluria.

Siluria ostrzegła Hektora, że jeśli pójdą do Świecy, MUSZĄ słuchać ekspertów. Wyjście w Fazę jest bardzo niebezpieczne. I zwłaszcza na Fazie - nie wolno zdjąć tej opaski... i Siluria powiedziała, że Anna ich zabije. Jej wizje, jej obecność sprawi, że Faza zareaguje.

Czas wybrać się do Świecy. Siluria poszła wpierw do Marty Szysznickiej.

Marta i Whisperwind miały spotkanie. Marta poinformowała Whisper, że Whisper i Warmaster przeprowadzą Silurię, Hektora, Mariannę i Konstantego do Świecy Daemonica. Whisper zaprotestowała, ale Marta tylko się uśmiechnęła. Whisper nie ma prawa głosu.

Karina poszła do Hektora i pobrała odeń krew pijawką. Ten ją zatrzymał; nie może się na to zgodzić. Rozpromieniona Karina oddała Hektorowi pijawkę i spytała, czy może zrobić simulacrum by Anna nie wiedziała, że ten opuścił KADEM. Hektor wyraził zgodę (więc Karina użyła krwi pobranej z czasów Esuriit i fal mózgowych przeskanowanych w tamtym etapie)...

Siluria poprosiła Mikado, by ten zajął się Anną i magami Świecy.

Siluria poszła jeszcze odwiedzić Eis, posiedzieć z nią chwilę. Potem powiedziała Eis, że ma nadzieję, że ta jest bezpiecznie i bezpiecznie ukryta (myśląc o Aegis). Jeśli nie, niech się schowa. Bo Siluria nie chce, by Aegis 0003 coś się stało. Bo Siluria się o nią martwi.

Tymczasem Hektor dostał wibracje SOS z kieszonkowej słuchawki. Założył ją i usłyszał szept Eis. Poprosiła o pomoc, bo KADEM trzyma ją zamkniętą w skrzynce. Powiedziała, że jest niepełnoletnia, nie wie czym jest i chce po prostu być wolna... a jest zniewolona i głodzona przez KADEM. Była odurzona... majaczyła. Nie wie kiedy jest "dzisiaj". Prosi, by Hektor zażądał zabrania jej do domu. I co teraz?
Hektor uznał, że pogada o tym z Silurią... jak już wróci na Primusa.

Czas na wyprawę.

Whisper wygląda jak ponure nieszczęście. Warmaster radosny. Marianna ma damper emocjonalny. Konstanty nie ma dampera. FULL METAL PALADIN. Marianna, cóż, ona wygląda, jakby szła na sąd wojenny... Whisper powiedziała, że dzięki magom Świecy może odnaleźć Świecę Daemonica i ona dowodzi operacją. Siluria wejdzie do Świecy Daemonica; to nawiąże kontakt KADEM - Świeca. Wyruszyli...

...pojawili się w dziwnym miejscu. Whisper rozejrzała się zdziwiona. Tu jest Świeca Daemonica? Mare Crucio, pełne długich cieni i licznych cmentarzowych krzyży... i istot Esuriit. Whisper zlokalizowała speculoida i otworzyła ogień. Wezwała jako wsparcie Venomkissa i Ash. Siluria jest bardzo, bardzo zmartwiona, że Whisper wezwała martwych magów AD...

Warmaster przeniósł Zespół na Mare Levamen; sam wrócił ratować Whisper. Siluria dostała za zadanie doprowadzić wszystkich na KADEM.

Konstanty JEST Świecą. Świeca jest w nim. Jego wiara i jego determinacja przywrócą Świecę Daemonica. W związku z tym, Siluria zdecydowała się próbować jednak dotrzeć na Świecę...

* Oni... oni nadchodzą... nie ma ucieczki - Marianna, cicho

Konstanty jako paladyn wysunął się do przodu, walczyć z nadchodzącymi istotami Daemonica i Esuriit. Siluria rozdzieliła Psychoaktywne Środki Hedonistyczne wszystkim poza paladynem (-1 surowiec). Konflikt. F. Przesuwają się po Mare Levamen; niestety, siły przeciwnika są coraz większe. Marianna prawie odpłynęła; jest pod wpływem środków. Konstanty walczy. Konstanty z całych sił broni Marianny; wrogowie atakują ze wszystkich stron. Siluria została ranna w walce z siłami wroga. Hektorowi udało się uniknąć ran; odparł ataki.

Z armii wroga wysunął się do przodu brat Marianny.

* Upadniesz tak, jak moja siostra - upiorny brat Marianny
* Nie. - Konstanty.

Zaczął się pojedynek. Armie otoczyły tych magów. Siluria próbuje zrozumieć, o co chodzi. Konflikt: R. Hektor próbuje zrozumieć o co tu chodzi, jak działają oboje: Marianna i Konstanty. Bo tu się dzieją rzeczy złożone i srogie psychologicznie...

Siluria wie jedno. Marianna NIE MOŻE być źródłem tego zdania. Konstanty... on to generuje. Marianna nie jest już "zasadną" czarodziejką Świecy. Dla NIEJ będzie lepiej jeśli zginie. Ale Konstanty się na to nie zgadza; szybciej posieka go jego własne wyobrażenie Avengera niż obróci się przeciw swojej przełożonej...

* Hektor dał radę utrzymać swoje wizje Pryzmatu; zarówno Infernię jak i Eis.
* Siluria dała radę utrzymać swój Pryzmat w ryzach. Zarówno Whisper jak i całą sytuację.

...

* "Miarą wielkiego dowódcy jest miara sytuacji, z jaką sobie potrafi poradzić" - wektor działania Silurii - "Zwłaszcza podczas wojny bogów. Tym bardziej pozbawienie jej... zabiera nadzieję jej ludziom".
* "Naszym zadaniem jest dotrzeć do Świecy. Unikanie decyzji jest oznaką tchórzostwa. Dotrzeć do Świecy." - wektor działania Hektora.

Konflikt: 9v8 = S. Udało się. Konstanty ściął "Istotę Esuriit". Kazimierz rozsypał się w proch.

Nawigacja. Trzeba tam dotrzeć. Marianna przypomina sobie Świecę: +1. Konstanty też skupia się na Świecy... i do tego jeszcze Hektor wezwał swojego demona prawnego Świecy (+2, -1 surowiec). Ale ten demon dał możliwość Silurii się podczepić magicznie. Konstanty stanowi pełne +5 -> 8. Potrzeba (10). Hektor włączył do działania magię mentalną (+1), Siluria wiedzę o KADEMie (+1). I przewodnik...

Silurii zmaterializowała się przed oczami Twilight. Cichy cień. Wyobrażenie zmarłej czarodziejki, czarodziejki, która znała dobrze Świecę.

Zespół idąc za "Twilight" dotarł do Świecy Daemonica.

Marianna otworzyła wejście do Świecy Daemonica...

Siluria pożegnała się z "Twilight" machając do niej, po czym pokuśtykała w stronę światła...

# Progresja



# Streszczenie

KADEM nawiązał kontakt ze Świecą; po ciężkiej przeprawie Siluria i Hektor dotarli do Świecy Daemonica. Eis poprosiła Hektora, by ten ją wydostał z KADEMu gdzie jest przetrzymywana. Infernia i Hektor weszli w fazę ostrą konfliktu. Whisperwind nie do końca jest normalna; rozmawia z martwymi przyjaciółmi z AD a pryzmatyczne efemerydy Marianny i Konstantego prawie zabiły cały Zespół. W skrócie: normalny dzień na KADEMie.

# Zasługi

* mag: Hektor Blakenbauer, skontaktowała się z nim Eis, podpadł na ostro Inferni z wzajemnością i jego znajomość psychologii i retoryki uratowała Zespół na Fazie Daemonica
* mag: Siluria Diakon, środki psychoaktywne uratowały Zespół, załagodziła konflikt Inferni i Hektora i ku swemu przerażeniu poznała sekret Whisperwind - rozmawia z "martwymi przyjaciółmi"
* mag: Lucjan Kopidół, zmontował i załatwił Hektorowi, Mariannie i Annie dampery pryzmatyczne, by ratować KADEM przed wizjami...
* mag: Infernia Diakon, wypowiedziała Hektorowi jawnie wojnę. Wściekła jak osa. Skrzywdziła Ignata, bo to jej walka z Hektorem.
* mag: Ignat Zajcew, stanął pomiędzy Infernią i Hektorem i skończył połamany.
* mag: Marta Szysznicka, prawidłowo priorytetyzując zmontowała ekspedycję na Świecę Daemonica by utrzymać KADEM w ryzach.
* mag: Whisperwind, która żyjąc w Pryzmacie przyzywa wizje swoich martwych przyjaciół z AD; potrafi stawić czoło speculoidowi, acz nie sama.
* mag: Karina Paczulis, zdobyła pozwolenie Hektora na zbudowanie jego simulacrum by nikt nie zauważył jego nieobecności na KADEMie
* mag: Mikado Diakon, skutecznie odwraca uwagę Anny od tego, że Hektor tak jakby... zniknął.
* mag: Warmaster, osłaniał Zespół po tym jak Whisper związała Siriratharina, ale musiał do niej wrócić; zaufał, że Siluria ich utrzyma.
* mag: Marianna Sowińska, w głębokiej depresji i na środkach; jednak udało się ją przeprowadzić przez Fazę Daemonica. 
* mag: Konstanty Myszeczka, pryzmatyczny paladyn Świecy; skłonny do walki i obrony Marianny i Zespołu nawet kosztem jego życia (ale nie straty ideałów).
* vic: Siriratharin, ściągnął Zespół do Mare Crucio, zaatakował Whisperwind i mimo walki między nim, Whisper i Warmasterem i tak dał radę się wycofać.
* vic: Eis, zaczęła swój plan ucieczki; skontaktowała się z Hektorem prosząc o wyciągnięcie jej z "piekła skrzyni KADEMu". Hektor nie wie z kim mówi.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Levamen, dokąd Warmaster awaryjnie przeniósł Zespół i gdzie doszło do walki z istotami Pryzmatu
        1. Mare Crucio, gdzie Siriratharin zastawił pułapkę na Zespół... lecz Whisperwind i Warmaster podjęli walkę
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Układ nerwowy
                        1. Centrum dowodzenia KADEMem, gdzie Marta zapowiedziała Whisper i Silurii odnośnie tego jak wyglądać będzie ekspedycja
                    1. Skrzydło wypoczynkowe
                        1. Pokoje gościnne, gdzie mieszka Hektor... gdzie wparowała Infernia. Gdzie w sumie wszyscy się kłócą.
        1. Świeca Daemonica, dokąd dotarł Zespół i do której wejście otworzyła Marianna

# Czas

* Dni: 1