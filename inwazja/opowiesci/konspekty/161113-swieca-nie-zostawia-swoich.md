---
layout: inwazja-konspekt
title:  "Świeca nie zostawia swoich"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160629 - Rezydencja? E, polujemy na dronę! (KB, DK, AB)](160629-rezydencja-e-polujemy-na-drone.html)

### Chronologiczna

* [161101 - Bezwzględna Lady Terminus (AW)](161101-bezwzgledna-lady-terminus.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Wtorek Śląski. Zespół się konstytuuje. Rozbudowuje swoje działania. 
Andrea wie, że w okolicy jest Krąg Życia - nawiązujące do tradycji druidów stowarzyszenie magów i viciniusów którzy uważają magów i viciniusów za równych. Bardzo agresywnie tak uważają.
Andrea decyduje się podzielić swoje siły. Wezwała dowódców do siebie i powiedziała o wykryciu Rudolfa. Chce go odzyskać. Idzie na akcję, chce ze sobą wziąć Mieszka, Juliana, Kajetana. Oddała dowodzenie nad skrzydłem szkoleniowym Tadeuszowi Baranowi i nad zamkiem też.
Tadeusz dostał zadanie by Marian załatwił oczy i uszy z Kręgu Życia; niech oni wejdą w lekki sojusz z siłami Świecy. Chodzi o współpracę.
Kajetan zaproponował pojechanie autobusem i zostawienie wszystkiego magicznego w domu. Andrea się zgodziła.
Kajetan też zaproponował, że załatwi wejście do szpitala w jakiś sensowny, niemagiczny sposób. Skontaktował się dyskretnie z Pauliną; ta dała Kajetanowi namiar na znajomą pielęgniarkę w Czeliminie. Ta może ich wprowadzić do szpitala.
Paulina skontaktowała się i sprzedała, że to ekipa z CBŚ. I oni nie polują na żadnego człowieka, ale muszą się tam dostać. Dzięki Paulinie i znajomej pielęgniarce (Halina Krzyżanowska) Kajetan zapewnił wejście do placówki jak już tam dotrą.

Rafael poprosił Andreę na słówko. Dał jej broń, coś specjalnego. Małą, lapisowaną, elegancką sakiewkę i nie pozwolił Andrei tego otwierać. Powiedział, że jak potrzebują dywersji... to to jest dywersja. Zabije kilkanaście ludzi. Andrea spytała też Rafaela, czy może wziąć dwie dawki krwi. Nie. Niestety, nie. Czyli Andrea musi żywić się na bieżąco.

Andrea poszła więc porozmawiać z magami KADEMu. A KADEM zbiera się szukać Ignata...

"Martwi mnie to, że ten mag MÓGŁ narozrabiać" - Marian Łajdak
"To znaczy..?" - Andrea
"Tu ukraść kurę... tu wysadzić coś w powietrze..." - Marian, udając niefrasobliwego
"Jestem skłonna odpuścić przewiny - nawet poważne naruszenia dóbr..." - Andrea
"To jest Zajcew" - Marian
"Mogę odpuścić wszystko, co nie jest kardynalne" - Andrea
"Kardynalne według KTÓREGO prawa?" - Andżelika

W końcu uzgodnili - chodzi o rzeczy, które były szkodliwe masowo zgodnie z intencją szkodzenia. To powoduje, że Ignat będzie bezpieczny - nawet, jeśli kiedyś wyjdzie, kto za tym wszystkim stoi.
A Andżelika i Marian za to się zgodzili na pozycję t'tien - tymczasowych tien, pod komendą Andrei. 

Łajdak dostał zadanie od Andrei. Wynegocjować od Kręgu Życia (różni magowie - Świeca i nie Świeca, różne viciniusy też) wsparcie. Wsparcie typu "otwarcie granic", współpraca, obrona granic. Po prostu - korzystne warunki życia. Łajdak master diplomat.
Łajdak dostał też jeszcze jedno zadanie - niech przewidzi czego mogą chcieć. Niech spróbuje im to dać, jeśli to nie jest zbyt drogie. Jako potwierdzenie że przychodzi ze Świecy, niech przyjdzie z nim tien Przylaz.
A Andrei nie przeszkadza, że w Świecy zacznie się jakiś ruch pro-vicinius.

Rudolf Jankowski ma włączoną boję hipernetową; niekoniecznie jest przytomny. Nie ma z nim kontaktu. Ale tylko Andrea go "słyszy".
Andrea ze swoimi sojusznikami (Kajetan, Julian, Mieszko) wyruszyli autobusem do Czelimina, z nadzieją, że wszędzie indziej wszystko się uda.
Rudolf Jankowski nie ma dokumentów w świecie ludzi (czyli nie ma ubezpieczenia). 

Czelimin.

Już zbliżając się do Czelimina Andrea czuje, jak hipernet - jej zaawansowany hipernet - reaguje na pryzmatyczną energię. Zinterpretowała to jako potężny pryzmat. W Czeliminie jest bardzo silny Pryzmat, taki, jak spodziewałaby się w Żonkiborze. Pryzmat rozrywający granicę między rzeczywistością a percepcją. Pryzmat jest pełen uniesień i głębokich, szczerych uczuć religijnych.

Andrei objawił się też rezydent Świecy - jest dostępny w hipernecie. Felicjan Weiner. Andrea zdecydowała, że jest on... niebezpieczny. Najpewniej należy do Drugiej Strony.
Wraz ze zbliżaniem się do Czelimina Pryzmat zaczyna wpływać na percepcję. Życie jest piękne. Bóg z nami. Chwalmy Pana i inne takie... tyle, że Andrea wzięła ze sobą bandę ultracynicznych i obowiązkowych wyjadaczy. A sama ma mentalność szczura.

Ten Pryzmat też działa silnie na typy magii - magia "religijna" jest super potężna. Też: kulty, magia lecznicza, magia kolektywna. Wszelkie działania typu bardziej ofensywnego czy "szatańskiego" są bardzo słabe.

Andrea zabunkrowała się w hotelu pracowniczym, dostępnym dla pielgrzymów. Następne wystąpienie kaznodziei ma miejsce za 2 godziny, o 20:00. Resztę wieczoru Andrea poświęciła na badaniu okolicy i rozeznaniu co się dzieje. Doszła do tego, że pacjent leży w szpitalu hutniczym w Pływni a ona jest na Osiedlu Solidarności i do tego, że tak silny Pryzmat musi być utrzymywany i wzmacniany; ale nie czuje zaklęć. Czyli to robota viciniusów. Czyli w tym mieście - w Czeliminie - są viciniusy zdolne do tego by kontrolować nastrój i tworzyć Pryzmat - a Rezydent Świecy i inni magowie Świecy słabo blipający na hipernecie nic z tym nie zrobili. Andrea ma koło 5 sygnałów Świecy w okolicy.

Mieszko ostrzegł Andreę przed używaniem jakiejkolwiek magii. Jeśli mają do czynienia z viciniusami różnej mocy, mogą tu być też taumatożercy czy niuchacze magii.
Andreę martwi też to nabożeństwo - najpewniej używany jest do budowania Pryzmatu.
Kajetan powiedział, że w świetle obserwacji nie jest pewny, czy warto teraz wbijać się i rozmawiać z pielęgniarką. Po prostu... przy takim Pryzmacie...

Andrea, Kajetan, Mieszko i Julian chcą się dowiedzieć z czym mają do czynienia przede wszystkim. Kajetan i Andrea sprawdzają przez analizę aury magicznej czy w astralice jest przymus. Kajetan sprawdza astralikę używając subtelnych hintów i melodii opowieści, Andrea sprawdza aurę bardziej mechanicznie, technicznie. Wynik: jest nakłanianie, ale nie ma przymusu. Nie wszyscy chodzą, acz przede wszystkim nasilenie jest CIEKAWOŚCI. To szczególnie dobrze działa na magów, nota bene.
Teraz Andrea jest pewna, że nie idą tam... ale jest coś jeszcze w tym Pryzmacie. To 'confession mechanism'. Bez magii nie da się dokładnie poznać o co chodzi, ale jest to oparte o spowiedź w niemałym stopniu.

W tym czasie Julian i Mieszko próbowali dojść do wniosku z jakimi viciniusami mają do czynienia. Julian ma większą wiedzę, Mieszko jest praktykiem. Odpowiedź jest niejednoznaczna, ale historia pokazuje, że to się zdarzało - najprawdopodobniej Czelimin jest opanowany przez Mortalisy. Były takie sytuacje przed Porozumieniami Radomskimi - całe wsie były przekształcane w maskowanie pryzmatyczne przed czymś innym lub służyły do wzmacniania odpowiednich uczuć by ułatwić rzucanie odpowiednich zaklęć. Ale to wymaga kontrolowania mortalisów (zawsze trudne). A Czelimin nie jest jedną wsią... to znaczy, że jest tu ponad 30 mortalisów!

Andrea kazała wszystkim magom przypomnieć sobie Mantrę Obronną Terminusów jako formę obrony przed nadchodzącym pryzmatem. Zawsze to +3 ;-).

Pryzmat uderza. Nabożeństwo się zaczęło. Magowie trzymają się ręce.
(15 mortalis +6 tłum | +3 mantra +6 odległość i ich nie ma) => defensywa przeciwko 12

Andrea przetrwała atak. Kajetan wie, jak to działa. Przetrwał. Mieszko to cyniczny i brutalny enforcer Świecy, nie było problemu. Pszczelak miał wahania, ale posłuszny rozkazowi Andrei wytrzymał wizję mortalisa. Wszyscy są zmęczeni po tym ataku, ale są częściowo uodpornieni przed następnym.
Gdyby nie to, PRAGNIENIE wyjścia i pójścia, przyznania się, rzucenia zaklęcia... Andrea teraz jest pewna, że ŻADEN mag tutaj nie jest przyjazny. Teren stracony.
Co gorsza, mortalisy słyszą szepty ludzi, ich myśli przy tym rytuale. Perfekcyjny totalitaryzm. Doskonała pułapka...
Dodajmy jeszcze Spustoszenie gdzieś w pobliżu...

Nie. Nie ma możliwości, by zrobić tu coś konkretnego. Andrea zwyczajnie nie jest w stanie uratować w tym momencie biednego tien Jankowskiego. Nie tutaj, nie teraz. Nie tak. Jedyne co może zrobić to przejść się koło szpitala i wydać rozkaz przez hipernet - zaawansowany hipernet - by agenta wyłączyć. Tien Jankowski się na pewno przez pewien czas nie obudzi. I uciszyła (wyłączyła) boję SOS.

Po tym, niezbyt szczęśliwa, zarządza odwrót...

Dzień 2:

Następnego ranka Andrea obudziła się z werwą i nowym pomysłem. Jeśli nie da się maga ODBIĆ, należy go wyciągnąć inaczej. Środkami ludzkimi. Kajetan dostał zadanie dowiedzieć się od "swojego kontaktu" (Pauliny) jak działają ludzkie procedury w przypadku szpitala i osoby nieznanej. Paulina słysząc, że jest mag w szpitalu i trzeba go zabrać a jest tam niebezpiecznie się zlitowała. Wyjaśniła jak działają procedury i co trzeba by go wyciągnąć.

Andrea delegowała na Juliana Pszczelaka i na Kajetana (by poszedł z nim) pójście do innego szpitala na Śląsku. Nie w pobliżu bazy. Nic w okolicach Wtorku Śląskiego. Mają użyć magii mentalnej na DYREKTORA. Kajetan załatwił słupa - "bogatego wujka" (szasnął kasą), który chce by to wszystko poszło dyskretnie i jest skłonny za to zapłacić - a Jankowskiego (NN) chcą przewieźć do prywatnej kliniki.

Dyrektor załatwił sprawę z lekarzami i poszedł sygnał do Czelimina.

Były jakieś wątpliwości i problemy; wypuszczono go dopiero wieczorem. Andrea przygotowała cały oddział i poczekała na trasie. Przechwyciła go, załoga karetki jest przekonana że odstawili i ok.
Przed wprowadzeniem Rudolfa Jankowskiego do bazy Andrea jeszcze zleciła pełne badanie i sama go badała. Szczęśliwie, udało się bezpiecznie go wyciągnąć. Jest uszkodzony i w kiepskiej formie, ale da się go postawić z pomocą Pszczelaka i Diakona, acz to potrwa chwilę...

# Progresja



# Streszczenie

Andrea ustabilizowała bazę we Wtorku i nawiązała ściślejszy sojusz z KADEMem (ułaskawiając Ignata i Silurię) oraz przez Mariana Łajdaka wynegocjowała sojusz z Kręgiem Życia (organizacją pro-viciniusową). Już nie może doczekać się swojego procesu. Potem wbiła do Czelimina (dyskretnie) by uratować tien Jankowskiego; okazało się, że miasto jest kontrolowane przez mortalisy - jak przed Porozumieniami Radomskimi. Oki... Andrea użyła wiedzy Kajetana (Pauliny ;p ) by Jankowskiego przenieść ludzkimi procedurami i go odzyskała. Fakt jak świetnie broniony jest Czelimin (Spustoszenie, Krew, Pryzmat Ekstazy Religijnej, mortalisy...) nie napawa optymizmem.

# Zasługi

* mag: Andrea Wilgacz, zapewniła stabilność nowej bazy, zawarła ściślejszy sojusz z KADEMem i planktonem, poszła odbić kolegę i... uratowała go biurokracją ludzką.
* mag: Paulina Tarczyńska, nieobecna (na telefonie), służąca kontaktami wśród medyków w Czeliminie oraz odnośnie biurokracji ludzkiej dla Kajetana.
* mag: Mieszko Bankierz, twardy i kompetentny terminus skłonny do udzielania dobrych i konkretnych rad. Wraz z Julianem wydedukował mortalisy w Czeliminie.
* mag: Julian Pszczelak, posłuszny rozkazom Andrei historyk (bardziej nawet niż terminus). Z Mieszkiem wydedukował mortalisy w Czeliminie. Lokalny KIEPSKI mentalista.
* mag: Kajetan Weiner, porusza się dobrze w świecie ludzi i z Andreą umie przebadać subtelności Czelimińskiego Pryzmatu. Współpracuje z Pauliną przez telefon.
* mag: Tadeusz Baran, pełniący obowiązki dowódcy bazy i skrzydła i Świecy na czas Andrei w Czeliminie. Poradził sobie ten jeden dzień.
* czł: Halina Krzyżanowska, pielęgniarka znana Paulinie (więc Kajetanowi) ze szpitala w Czeliminie; jednak nie skorzystano z jej pomocy bo Pryzmat i niebezpieczeństwo.
* mag: Andżelika Leszczyńska, przejęła rolę prawniczo-administracyjną zwalniając Mieszka by ten mógł zająć się tematami poważniejszymi (terminuskimi).
* mag: Marian Łajdak, wybitny negocjator z ramienia... Świecy? Załatwił wsparcie ze strony Kręgu Życia dla nowo odbudowującej się Świecy we Wtorku.
* mag: Rudolf Jankowski, praktycznie katatoniczny * mag Świecy z Wydziału Wewnętrznego wyciągnięty przez Andreę z samego serca Czelimina.
* mag: Felicjan Weiner, rezydent Świecy. Teraz Andrea nie ma wątpliwości - agent Drugiej Strony. Najpewniej wysoko postawiony w Czeliminie.
* mag: Rafael Diakon, absolutnie nieetyczny i genialny biomanta Millennium. Dostarczył Andrei dywersję zabijającą kilkanaście ludzi na wszelki wypadek.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, z ciągle rozbudowującą się bazą Srebrnej Świecy
                1. Czelimin
                    1. Osiedle Solidarności
                        1. szpital hutniczy, gdzie leżał Rudolf Jankowski aż szpital dostał rozkaz przeniesienia
                        1. bloki mieszkalne
                    1. Pływnia
                        1. hotel pracowniczy, gdzie zamieszkała Andrea z Zespołem odbijającym Rudolfa.
                        1. kościół neoromański, gdzie odbywa się super-wzmocnione nabożeństwo prowadzone przez mortalisa

# Skrypt



# Czas

* Dni: 2