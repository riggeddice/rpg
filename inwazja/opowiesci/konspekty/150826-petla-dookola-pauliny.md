---
layout: inwazja-konspekt
title:  "Pętla dookoła Pauliny"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150823 - Atak na sanktuarium Estrelli (PT)](150823-atak-na-sanktuarium-estrelli.html)

### Chronologiczna

* [150823 - Atak na sanktuarium Estrelli (PT)](150823-atak-na-sanktuarium-estrelli.html)

## Punkt zerowy:

## Misja właściwa:

### Faza 1: Rage mode Ozydiusza

Paulina grzecznie śpi, odsypiając mocno zarwaną noc. Obudziło ją mocne pukanie w drzwi. To lord terminus Ozydiusz Bankierz (jest z nim tien Judyta Karnisz). On chce Estrellę i chce ją tu i teraz. Paulina próbuje przekonać go, że to nie jest dobry pomysł na co lord terminus pokazuje dzisiejszy numer Paktu.
Na okładce jest zdjęcie Estrelli i podpis "terminuska Świecy pobita przez dwóch ludzi" z dopiskiem "pojechana przez piętnastolatka". Paulinie opadła szczęka. Zgodnie z artykułem Paktu Estrella została pojechana przez 15-letniego Mausa który kupił akcesoria w sklepie ze śmiesznymi zabawkami. I prawie zabił Estrellę, gdyby nie "przypadkowy lekarz magiczny który się zlitował". No i dopisek "czyżby tien Ozydiusz Bankierz nie kontrolował swoich terminusek?" i zdjęcie Tamary Muszkiet z krótkim opisem.

Oczywiście, Bankierz jest wściekły jak osa. To masakruje jakiekolwiek szanse na awans czy ogólnie cokolwiek. Ta akcja kompromituje go niesamowicie. Paulina próbowała go jakoś uspokoić, herbatka czy ciastko... nie ma szans powodzenia. Ozydiusz nie przyjmuje płynów czy pokarmów na akcji. Nie pozwala też tien Karnisz. Z ciężkim sercem, Paulina poszła obudzić Estrellę. Ostrzegła ją o sytuacji. Terminuska zdecydowała się pójść na spotkanie z Ozydiuszem w samej koszuli nocnej, z jednym ramiączkiem bardziej. Oczywiście, lorda terminusa rozjuszyło to jeszcze bardziej. Rozryczał się na Estrellę, faktycznie w "rage mode". Zaczął od litanii jak to ma same kobiety i to jest cholerna tragedia, bo nie da się tego cholerstwa upilnować i nie zachowuje się to prawidłowo i nie można temu ufać, po czym skrzyczał Estrellę za zrujnowanie życia młodemu piętnastoletniemu Mausowi (bo teraz to ma przechlapane za atak na terminusa). Skąd w ogóle Maus miał coś do niej? Estrella odpowiedziała, że chodzi o to, że ukarała kiedyś jego rodziców.

W wyniku tego wszystkiego Paulina się postawiła Bankierzowi w obronie Estrelli. Bankierz powiedział, że Kermit Diakon wystawił jej znakomite referencje, ale Bankierz chce swoją terminuskę w Świecy. Estrella dostała zakaz opuszczania kompleksu Świecy i konieczność siedzenia w szpitalu. Judyta Karnisz ma ją odprowadzić do Kompleksu żeby Estrelli nie zabił żaden miślęg. I niech Estrella nawet nie myśli że jemu, Ozydiuszowi podoba się pomysł że Estrella fraternizuje się z Netherią.

Jako, że Estrella była w częściowym negliżu, Ozydiusz nie mógł jej kazać pójść z sobą. Wyszedł więc sam i dał Estrelli deadline na opuszczenie tego miejsca. I niech nie zrobi NICZEGO co sprawi, że w Pakcie może pojawić się cokolwiek niewłaściwego.
Gdy Ozydiusz wyszedł, Estrella, Netheria i Paulina zjadły śniadanie a Judyta (zgodnie z rozkazami) niczego nie jadła tylko patrzyła, przełykając ślinkę. Tak bardzo rozkazy.
Gdy Estrella chciała pójść się przebrać, Judyta poszła z nią. SERIO? Judyta wzruszyła ramionami - ma takie rozkazy. Paulina próbowała wpłynąć na Judytę - chce powiedzieć Estrelli o tym, co zobaczyła pod cmentarzem - ale terminuska jest nieporuszona. Ma swoje rozkazy. Z ciężkim sercem Paulina musiała wypuścić Estrellę.

Paulina porozmawiała więc z Netherią, która pozostaje w kiepskim humorze (a jej Skażenie nie pomaga). Netheria zauważyła, że Ozydiusz jej nie lubi. Przegadały z Pauliną sprawę owego Mausa, i wyszło im, że ten młody Maus to jest narzędzie w czyichś rękach; on przegrywa (i to sporo), Estrella przegrywa sporo, Ozydiusz przegrywa, Świeca wychodzi na bandę idiotów... ogólnie, wszyscy przegrywają. Ktoś to rozegrał. Jedynym zwycięzcą jest Pakt i siły grające na osłabienie Świecy. Netheria poradziła Paulinie, by ta porozmawiała z Kermitem; Netherii Kermit nic nie powie (ten sam ród, nie może), ale może Paulina czegoś się dowie. A Netheria spróbuje dowiedzieć się czegoś od sprzedawcy gadżetów - może ładna Diakonka dowie się czegoś cennego.
Paulina powiedziała jeszcze Netherii o tym, że pod cmentarzem coś jest. Słabe i stare. Ta aura musi tam być, bo ta aura to usypia - i Paulinie naprawdę zależy, by ani Millennium ani Świeca się o tym nie dowiedziały (o KADEMie nie wspominając). Netheria powiedziała, że zrobi mały research - może czegoś się dowie o co w tym chodzi. Paulinie nie zależy na tej wiedzy - ona tylko chce, by "to coś" nie wstało.

Paulina dotarła do biura Kermita Diakona - zgodnie z zaleceniem miała się stawić. Tien Diakon na nią czeka. Zadał jej kilka pytań (czemu tam była, czemu cmentarz jest tak ważny) i Paulina odpowiedziała niezobowiązująco. Paulina zapytała o młodego Mausa i tą sprawę i Kermit odpowiedział niezobowiązująco.
Ta rozmowa była w pewien sposób stratą czasu dla obu stron. Żadna nie chciała drugiej nic powiedzieć. Obie miały dobre powody.

### Faza 2: Paulina pod ostrzałem prasy

Paulina wróciła i powiedziała Netherii, że Kermit niczego nie powiedział. Potem skontaktowała się z Olgą - młody Maus naraził się terminusom i zapłaci za to odpowiednią cenę. Olga nacisnęła na Paulinę (konflikt) - Olga chce zniszczyć młodego Mausa; kara terminusów jest jej zdaniem dla Mausa niewystarczająca. Paulina nie chce się na to zgodzić, chce uniknąć takiego losu dla młodego Mausa. Tu potrzeby Olgi/Iliusitiusa i Pauliny się różnią. Paulina poszła argumentacją, że chwilowo ma JAKĄŚ opinię i jak zacznie grzebać, może to bardzo ucierpieć; to wewnętrzne sprawy Świecy. Nie może sobie na to pozwolić, by się dowiedzieć. Z trudem, ale Olgę przekonała. Paulinie zrobiło się zimno - gdyby jej się nie udało wybronić, to młody czarodziej najpewniej by został zabity przez Olgę. Lub gorzej.

Olga powiedziała Paulinie, że ma dla niej dość ciekawy przypadek medyczny, ale jeszcze próbuje go sprowadzić; czy Paulina jest zainteresowana ratowaniem życia? Olga mówi, że to magiczna choroba i ofiary choroby są containowane; musi jednak dowiedzieć się gdzie. Paulina powiedziała, że zapozna się z tą sytuacją. Olga powiedziała, że jeszcze nie w tej chwili; musi zdobyć ich lokalizację. Nie powiedziała Paulinie skąd wie, że to magiczna choroba i skąd ma dostęp do tych informacji; "kto wie, niekoniecznie jest bezpieczny".
Paulina westchnęła, ale zgodziła się grać w tą grę...

Maria zauważyła na mindlinku, że nikt nie mówi niczego Paulinie a jednak różne strony oczekują od niej pewnego zachowania. Paulina jest wykorzystywana. Paulina się zgodziła, ale nic na to nie może poradzić. Maria stwierdza, że przybycie do Kopalina było błędem. Paulina się zgodziła, że może faktycznie, ale niekoniecznie jest w stanie teraz stąd wyjechać. Tak czy inaczej, Maria obiecała, że będzie asekurować Paulinę cokolwiek się nie stanie.

Do gabinetu Pauliny - pukanie do drzwi przerywające rozmowę z Marią. Paulina wpuściła do środka... tien Jolantę Sowińską z Paktu. Maria dostała mentalny jolt - POMOCY! Niech Maria pomoże Paulinie w starciu z Jolantą, czegokolwiek by ta nie chciała... CHOLERA! To ona napisała artykuł o Estrelli. I Ozydiuszu. I Tamarze...
Paulina nie jest zbyt szczęśliwa z obecności Jolanty... zwłaszcza, że dziennikarka jest dość bogata a w świecie magii i prasy obowiązuje Złota Zasada (kto ma złoto, ustanawia zasady). Jolanta jest dość bogata - nie jest dziennikarką dla pieniędzy...
Szczęśliwie, Maria jest tajną bronią Pauliny.

ESCALATION MODE:

Jolanta ma następujące cele:
- Profil Pauliny do Paktu jako cichej, skromnej bohaterki wychwalanej przez Kermita i magów dookoła
- Co się tak naprawdę stało na cmentarzu; włącznie z Tym Co Pod Ziemią
- Coś fajnego na Ozydiusza Bankierza by mógł wyjść na mega choleryka i tyrana
- Coś na Estrellę; human story.

Jolanta może zapewnić następujące rzeczy:
- Dane o młodym Mausie, szczegóły, informacje
- Publikacja czegoś korzystnego z perspektywy Pauliny
- Skupienie uwagi na czymś, co jest dla Pauliny pożyteczne

Jolanta powiedziała Paulinie, że dużo o niej dobrego słyszała; i od magów, których Paulina leczyła i od tych, którym inaczej pomagała. Jednocześnie Paulina jest lekarzem, jak i lokalną bohaterką - a dodatkowo chce założyć własną klinikę (ludzi, magów, viciniusów) oraz próbuje założyć własną klinikę. Jolancie się to podoba. Paulina skontrowała, że chce zachować neutralność i ciszę a rozgłos będzie jej w tym jedynie przeszkadzać. Jolanta zauważyła, że rozgłos - odpowiedniego typu - się Paulinie mógłby przydać. Zwłaszcza taki, który skontaktuje ją z siłami zainteresowanymi mecenatem i wizerunkiem. Pomóc bohaterce to jest coś. Słysząc te słowa, Paulina się żachnęła. Jolanta stwierdziła, że uważa siebie za fotografa rzeczywistości - pokazuje świat jakim on jest, ale jednocześnie dobiera odpowiednie światło. A Paulinę chce przedstawić w świetle bohaterki. Paulina pyta "po co" (konflikt). Maria szepcze Paulinie do ucha i pomaga jej użyć odpowiednich słów i czego Jolanta może chcieć (11v9->14). Jolanta, przekonana przez Paulinę, stwierdziła się powiedzieć prawdę: pojawia się bardzo silna wizja supremacji w Srebrnej Świecy (izolacjoniści, Kurtyna) przy jednoczesnej erozji zaufania i samopomocy i eskalacji wojny gildii. Odpowiedni artykuł i działania mogą doprowadzić do tego, że trochę zmniejszą się niesnaski między gildiami i pojawi się nadzieja.
I Paulina jest osobą wybraną przez Jolantę. Niezrzeszona czarodziejka oraz czarodziejka Millennium współpracujący z terminusami SŚ bronią cmentarza? I Paulina chce założyć klinikę? I chwalą ją terminusi? Idealna bohaterka.

Marii zrobiło się zimno. Jeśli Jolanta to zrobi... Paulina jest na świeczniku. Nawet przy założeniu absolutnie czystych intencji Jolanty, Paulina jest wyrwana ze świata ludzi i jest symbolem. Paulina też to widzi. Intencje Jolanty są czyste, ale koszt dla Pauliny jest straszny. Ona nie chce...
Paulina się po prostu przeraziła.

"Nie umiem być symbolem. Nie znam się na polityce" - Paulina, desperacko broniąca się przed Jolantą.
"To jedynie dodaje Ci autentyczności i uroku" - lekko uśmiechająca się Jolanta.

Jolanta powiedziała Paulinie, że jeśli ta jej pomoże, spróbuje tak skupić artykuł by spełniał cele zarówno Jolanty jak i Pauliny. Jeśli Paulina się nie zgodzi, to Jolanta po prostu stworzy artykuł zgodny ze swoją wizją... która stanie się rzeczywistością. Taki mały szantażyk, który jednocześnie pokazał Paulinie, że nie jest w stanie działać przeciwko takim potęgom. Jolanta chce by artykuł stworzył posąg dla Pauliny na wszystkich liniach - bohaterka, lekarz, społecznik, apolityczna (10v13->6). Paulina z wielkim bólem się zgodziła, w tej sytuacji nie ma wielkich szans...

Gdyby Paulina była inną osobą, zgodziłaby się na wszystko i nasłała na nią Olgę. Ale nie jest... ku swojemu wielkiemu czasem rozżaleniu.

Współpracując bardzo silnie z Marią i korzystając z okoliczności, że się zgodziła Paulina zdecydowała się na chronienie cmentarza (i potencjalnie Estrelli). (14v8/10/..->12). W ramach tego Paulina wynegocjowała, że w zamian za to co prawda dzięki opisowi akcji na cmentarzu Paulina wykaże swoją epickość (bo to była epicka akcja) jak i współpracę z innymi gildiami (Kermit i Netheria), ale Jolanta się odwali od cmentarza i będzie dopytywać o te tematy (nic nie dotknie tematu stwora pod cmentarzem; ten temat nie wypływa, nie istniało itp). Jolanta się zgadza. Dodatkowo, Jolanta zostawia Estrellę w spokoju i nie atakuje / nie krzywdzi Estrelli. Jolanta i z tym się zgodziła.

END_OF_ESCALATION

Paulina jest bardzo, bardzo nieszczęśliwa. Uśmiechnięta Jolanta dziękuje za poświęcony jej czas. Maria chciałaby ją rzucić cegłówką.
Artykuł ukaże się pojutrze.

### Faza 3: Uciekać jak mysz z nory...

Maria obiecała Paulinie, że znajdzie jakąś kryjówkę. Paulina powiedziała, że zawsze może zostać znaleziona przy użyciu magii; Maria obiecała, że będzie dobra - może poszukać czegoś w województwach nie kontrolowanych przez Świecę. A Paulina buduje jak największą ilość wyjść awaryjnych ze swojego gabinetu.

Paulina upewniła się, że Jolanta nie podłożyła niczego do jej gabinetu i zadzwoniła do Netherii. Powiedziała jej jak wygląda sytuacja z artykułem i powiedziała, że przynajmniej Estrella jest bezpieczna. Zadzwoniła też do Kermita by i jego ostrzec o sytuacji. Nie powiedziała nic Oldze. Zapomniała. Dodatkowo, podświadomie wiedziała, że Pan Wróżdy zdecydowałby się to jakoś wykorzystać (np. do znalezienia młodego Mausa) - a tego Paulina po prostu nie chce.

Innymi słowy, zdaniem Pauliny, powoli dookoła niej robi się gorąco.

# Streszczenie

Pakt obśmiał Estrellę i skompromitował Ozydiusza. Lord terminus zrobił Estrelli areszt domowy. Pakt też wskazuje na Baltazara Mausa jako winnego ataku na Estrellę. Zdaniem Netherii, wszyscy przegrywają: Świeca, Ozydiusz, młody Maus. Ktoś to rozegrał. A pod cmentarzem coś jest. Słabe i stare. Nie wiadomo co to; nikt o tym jeszcze nie wie. Paulina młodego Mausa uratowała przed Olgą (ta by zabiła). Jolanta Sowińska z Paktu zrobiła wywiad z Pauliną; chce obniżyć napięcia Świeca-inne gildie. Paulina jako pozytywny symbol. Ta się przeraziła i ewakuowała; niekoniecznie chce być na świeczniku. Ale będzie. Bo Pakt.

# Zasługi

* mag: Paulina Tarczyńska, która stała się symbolem wbrew swojej woli i która odbiła się od bariery gildii (bycia poza nią).
* mag: Ozydiusz Bankierz, którego trafił szlag na Estrellę, ale wobec Pauliny był kulturalny. Szowinista jak diabli.
* mag: Judyta Karnisz, terminuska pod Ozydiuszem robiąca dobrą minę do złej gry. Służbistka.
* mag: Estrella Diakon, w częściowym negliżu, podleczona, lekko złośliwa wobec Ozydiusza która dostała szlaban; nie może opuszczać kompleksu Świecy.
* mag: Netheria Diakon, która nie wierzy że młody Maus mógł wymyślić taką intrygę. Chce pomóc Paulinie, ale też się odbija od konfliktów międzygildiowych.
* mag: Kermit Diakon, który dał dobre rekomendacje Paulinie, dzięki czemu (niechcący) ściągnął na nią Jolantę Sowińską. Ostrzeżony przez Paulinę o tym co zrobiła Jola.
* mag: Jolanta Sowińska, która na tropie Ozydiusza/Estrelli znalazła Paulinę i zdecydowała się zrobić z niej symbol poprawiający stosunki między gildiami i dający inspirację. Nieważne, czego chce Paulina.
* czł: Maria Newa, ostatnia deska ratunku Pauliny dyskutującej z Jolantą, coraz bardziej zdegustowana całym światem * magów (i koleżanką po fachu - Jolantą).
* mag: Baltazar Maus, młody (15 lat) chłopak który stał za próbą gwałtu i pobicia Estrelli. Zdaniem większości, sam na to nie wpadł. Ale zaatakował terminusa. Podobno Estrella ukarała jego rodziców.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Owadów
                        1. Gabinet lekarski, gdzie przyjmuje min. Paulina