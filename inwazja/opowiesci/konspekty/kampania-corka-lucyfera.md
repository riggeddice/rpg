---
layout: default
categories: inwazja, campaign
title: "Córka Lucyfera"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Powrót Karradraela](kampania-powrot-karradraela.html)

## Opis

-

# Historia

1. [Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html): 10/02/08 - 10/02/10
(170530-corka-lucyfera)

Siwiecki poszedł pomóc z egzorcyzmami w Storczynie, stolicy czosnku - a tam prawdziwa "córka Lucyfera" jak na siebie mówi, próbująca wprowadzić jakiś mroczny rytuał wzmacniający maga kosztem populacji. Henrykowi i Estrelli udało się uniknąć ataków ze strony niebezpiecznych ludzi i thralli; rozmontowali kilka rzeczy przygotowanych przez Luksję. Niestety, ilość thralli ciągle rośnie i powstał potężny kult satanistyczny. A lokalny ksiądz - Zenobi Klepiczek - jest na zabój zakochany w Estrelli.

1. [Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html): 10/02/11 - 10/02/12
(170603-corka-jest-narzedziem)

Estrella i Henryk złapali grupę thralli Luksji i poznali cel rytuału - poświęcenie sporej populacji miasta by wzmocnić Luksję. Odkryli też, że za Luksją ktoś stoi, ktoś dużo sprytniejszy i lepiej uczony od niej. Jakkolwiek Luksja nie posunęła się do przodu w planach, anarchia w mieście rośnie i najpewniej nowe siły podniosą rękawicę by coś zrobić ze Storczynem.

1. [Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html): 10/02/13 - 10/02/15
(170620-pulapka-na-luksje)

Henryk i Estrella zwabili Luksję do kościoła i skutecznie ją pokonali i zdominowali, poważnie uszkadzając plebanię. "Niosący Światło" w panice zaczął ukrywać dowody swojego zaangażowania a Antoni Bieguś zaczął przejmować w sposób mafijny kontrolę w okolicy. Apokalipci Lucyfera rozpoczęli proces uruchamiania artefaktu (zrobiła to jeszcze Luksja). Tymczasem zbliża się potężna magiczna burza...

1. [Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html): 10/02/16 - 10/02/17
(170628-ukradziona-apokalipsa)

Artefakt Apokalipsy działa. Nie da się tego łatwo zatrzymać, więc... Zespół go ukradł i przekazał Mikado (do Millennium). Luksję zawieźli do Świecy - zacznie się wielka wojna o to, jaki będzie jej przyszły los. Luksja jest ślicznie wytresowana przez "Lucyfera" i nie ma swojej pamięci; o dziwo, coś ją ekranuje od Magii Krwi. Ale co? Tymczasem w miasteczku trwa już jawna wojna między Apokaliptami i ludźmi Biegusia. Politycznie Bieguś przegrał. Ale on ma rząd dusz.

## Progresja

|Misja|Progresja|
|------|------|
|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html) zdominowana przez Henryka Siwieckiego|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html) zniewolona, w Świecy.|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html) szacun za zdobycie i przechwycenie Luksji. Też... a co zrobiła z Artefaktem Apokalipsy Psinoskiej?!|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) szacun za zdobycie i przechwycenie Luksji. Też... a co zrobił z Artefaktem Apokalipsy Psinoskiej?!|

