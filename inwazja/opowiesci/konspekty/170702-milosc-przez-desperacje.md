---
layout: inwazja-konspekt
title:  "Miłość przez desperację"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170614 - Kryzys przez eliksir (PT)](170614-kryzys-przez-eliksir.html)

### Chronologiczna

* [170614 - Kryzys przez eliksir (PT)](170614-kryzys-przez-eliksir.html)

## Kontekst ogólny sytuacji

W myślach Diany zaczynają pojawiać się pytania: czy tak jak ona żyje, czy tak życie musi wyglądać? Nadal jest przekonana, że Ewelina Bankierz ogólnie jej pomaga... ale czy naprawdę to jest uczciwa cena za tą pomoc? Nie przeszkadza jej to być zakochaną w Hektorze, lecz nie skłania jej do pozytywnego dla ich związku kroku. Z drugiej strony, Diana zaczyna czuć smak wolności od Eweliny - może faktycznie powinno być inaczej? Zaczyna sama kombinować, a to jest coś nowego dla Diany.

Hektor powoli orientuje się, że nie jest w stanie wygrać przeciw potędze Bankierzy samotnie. Nadal jest zainteresowany współpracą z Dukatem, ale nie widzi tego jako absolutną konieczność.

Grazoniusz DOSZCZĘTNIE wypadł z łask zarówno Newerji jak i Eweliny. Jest non-parameter. Hektor co prawda został częściowo "ustawiony na swoim miejscu", ale upadek Grazoniusza sprawił, że to też przestało być problemem. Tak jak działania Grazoniusza przeciw Ewelinie.

Grazoniusz dał radę pomóc trochę w czyszczeniu Skażenia w okolicy i drobnych pracach "rezydenckich". Dowiedział się też kilku rzeczy o aktualnej manifestacji Efemerydy Kapsuły.

## Punkt zerowy:

**W poprzednich odcinkach**

* Ogródek Pauliny jest zniszczony, Skażony szybko nie wróci do normy.
* Wzrost znaczenia Wieży Ciśnień w Senesgradzie - Efemeryda nadal jest sprawna i ma nową manifestację. A ludzie tam coś kombinują.
* Ubranie Diany jest wyhodowane ale jest ubraniem - wyraźnym złamaniem zakazu Eweliny. Diana nie wie.
* Widząc klęskę Grazoniusza pojawi się nowy Bankierz chcący wejść na miejsce upadłego Grazoniusza...

**Ewelina:**

* **Diana nie jest zainteresowana mężczyznami**:    7/8     Tier 3
* **Muszę być autonomiczna, Newerjo!**:             1/6
* **Integracja sensorów Kai**:                      0/8
* **Wielki sukces Prosperjusza**:                   0/10
* **Prosperjusz kontra Apoloniusz**:                0/8

**Prosperjusz:**

* **Grazoniusz, bracie!**:                          0/8
* **Umocnienie bazy**:                              0/8
* **Logistyka bazy**:                               0/8
* **Model biznesowy**:                              0/8
* **Zrozumienie efemerydy**:                        0/10

**Neutralna**:

* Diana zostaje u Eweliny:                          3/6     Tier 1
* Diana buntuje się przeciw Ewelinie:               6/8     Tier 3
* Diana wybiera swoją rodzinę:                      1/6
* Diana odchodzi w świat, samotnie:                 2/6     Tier 1
* Diana wybiera Hektora:                            4/8     Tier 2
* Zadomowiony w okolicy Hektor:                     3/8     Tier 1
* Hektor zdesperowany, integracja z Dukatem:        4/8     Tier 2
* Głos Praszyny wymaga uwagi                        0/8

**Paulina**:

* Udobruchany Grazoniusz:                           2/6     Tier 1

**Pytania i odpowiedzi**

* Ż: Co mogłoby pomóc Paulinie w ramach tematów z jakimi ta ma do czynienia?
* K: Coś, co zniechęci ludzi do działań w okolicy Wieży Ciśnień w Senesgradzie...
* Ż: W jaki sposób inna służka Eweliny może pomóc Paulinie z tym by pomóc Dianie?
* K: Jest starszą koleżanką mającą na Dianę wpływ; jak dojdzie co do czego, pomoże.
* Ż: Jaki skuteczny model biznesowy może zostać zrobiony przez konstruktora konstruktów w tej okolicy?
* K: Produkcja specjalistycznych konstruktów - dla Apoloniusza, dla innych... nie łamiących Maskarady.
* Ż: ...i co będzie dla niego największym zagrożeniem?
* K: Największym słabym punktem jest AI - inteligencja tych konstruktów.
* Ż: Co Paulinie przeszkadza jako rezydentce? Coś, czego nie chce robić sama.
* K: Teren jest rozległy. Przydałaby się sieć logistyczna, sieć portali na tym terenie...
* K: W miasteczku Praszyna znajduje się gazeta plotkarska "Głos Praszyny". Łamią Maskaradę nawet, gdy nic się nie dzieje... i to Paulinie przeszkadza.
* Ż: Nazwij najkorzystniejsze dla Pauliny miejsce, gdzie Prosperjusz może założyć bazę (co niweluje to co jej przeszkadza)
* K: Stary, opuszczony, lekko zrujnowany dom w Praszynie. Paulina może pomóc w świecie ludzi załatwić własność a w świecie magów - rezydencję tam.
* Ż: Co możliwego do wykorzystania przez magów znalazł Grazoniusz dotyczącego efemerydy kapsuły?
* K: Bardzo wrażliwa na Pryzmat energia, ale bardzo czysta. I bardzo trudna do kierowania.

## Misja właściwa:

**Dzień 1:**

Paulina obudziła się rano. Nic się nie dzieje. Po raz pierwszy od dawna. Paulina ma problemy z uwierzeniem w to zjawisko...

Paulina zadzwoniła do znajomego elektronika mówiąc mu o dziewczynie nieco zbyt nieśmiałej by się odważyć. A Paulina chciałaby się dowiedzieć, czy ona w ogóle ma jakiś potencjał. Elektronik ma sklep w Trzeszczu, całkiem niedaleko. A Hektor ma się zająć ogródkiem.

Paulina zauważyła, że Diana jest wyraźnie częściowo odprężona, mając na sobie ubranie. Na pewno czuje się lepiej, jest bardziej... pewna siebie.

* Paulina: Karol, Zaufanie, Ubranie Diany: +3
* Smutna Historia Diany: Bardzo Chce Być Szlachcianką, Duża lojalność wobec rodziny i Eweliny, Inercja: +5

Paulina próbuje zmiażdżyć Dianowe podejście: 8v7->S

Diana wyraźnie tego nie robiła do tej pory zbyt dużo, ale bez magii też daje sobie JAKOŚ radę. Przyciśnięta lekko, dodała, że jej ojciec zajmował się takimi rzeczami - z cyny robi ozdoby i je sprzedaje. Więc ona też się trochę uczyła. Nawet jej się to trochę spodobało.

* Diana buntuje się przeciw Ewelinie:       +1      +1 akcja
* Diana widzi alternatywy w świecie ludzi   +2
* Diana idzie ze swoją rodziną              +1

Późniejszym południem wrócili do Hektora i ogródka. Ogródek nadal jest w stanie katastrofalnym. Po prostu ruina. Paulina się nie spodziewała niczego innego... Hektor powiedział, że trzeba cały ten teren odkazić. Doszło do Skażenia i zatrucia. On to może zrobić; po prostu to chwilę zajmie... bardzo przeprosił, jeszcze raz.

Paulina spytała Hektora o jego potencjalną przyszłość. Hektor jest bardzo zainteresowany światem ludzi. Nie magów. Nie pisał nigdy CV, ale chciałby być porządnym, uczciwym ogrodnikiem. Magia tak jak Paulina - gdy potrzebna. No, trochę bardziej. Paulina zostawiła go z zadaniem napiania cv. Takiego kierowanego do magów. Z uwzględnieniem umiejętności magicznych. I tak, dostał kilka próśb od kilku osób, by im pomóc w pracy ogrodniczo-rolniczej. A on jak najbardziej jest za. Paulina zaniesie CV potem Apoloniuszowi...

Paulina zabrała się za przyjmowanie pacjentów, Hektor pisze CV a Diana ma o czym myśleć...      +1 akcja

Wieczorem Paulina dostała CV Hektora. Przejrzała, znalazła kilka rzeczy do poprawienia. Umówiła się na spotkanie z Apoloniuszem na następny dzień.  +1 akcja

**Dzień 2:**

Rano, Paulina ma gościa. O 10:00. Ładna dziewczyna w liberii Eweliny Bankierz. Przedstawiła się jako Kaja Maślaczek. Powiedziała, że jest po to, by pomóc Dianie w wykonaniu jej zadania. Przyniosła dla Diany liberię Eweliny; powiedziała też, że w pobliżu pojawił się nowy Bankierz. Paulina powiedziała Kai, że nie ma miejsca dla niej w domu Pauliny. Kaja, niezrażona, powiedziała, że zakwateruje się w agroturystycznym gospodarstwie w Praszynie. Kaja powiedziała też, że Grazoniusz wypadł z łask Eweliny.

Diana ucieszyła się na wieść o pojawieniu się Kai. Jest nieco ambiwalentna na liberię - nie jest to ubranie "normalne". Jest to ubranie należące do Eweliny. Ale nie kosztuje Pauliny energii. No i sama Kaja... Diana ją lubi. Ale to wierna służka Eweliny. Diana nie wie co o tym myśleć. Podzieliła się tym z Pauliną.

Diana się przebrała i poszły spotkać się z Kają. Paulina zobaczyła Kaję skoncentrowaną; połączoną z jakimiś formami magii. Zapytana, powiedziała, że robi rekonesans katalityczny (bez czarowania). Poprosiła Paulinę o prawo rzucania zaklęć szperających i identyfikujących. Takich, które nic nie "robią" ale dają jej oczy. Widać, że Kaja jest dobrze ułożona... i śliska.

Zapytana czego chce szukać, Kaja odpowiedziała: anomalii, nieautoryzowanych zaklęć, rzeczy pomocnych rezydentce. Kaja chce zbudować Paulinie szkielet sieci detekcyjnej. Gdy Paulina spytała Kaję czemu Grazoniusz wypadł z łask Eweliny, ta odpowiedziała zimno "za traktowanie Diany". Dianie aż się miłość w oczkach pojawiła. Czemu nie wcześniej? Ewelina nie wiedziała. Amulet zostaje na Dianie i wysyła sygnał tylko w warunkach kryzysowych - na przykład "ogródek". I wtedy Ewelina się dowiedziała i mogła pokazać Newerji itp ;-). Zwłaszcza, że Grazoniusz zawiódł jako obrońca Diany i doszło do "ogródka".

Czemu sieć detekcyjna? Ewelina chce przeprosić za Grazoniusza a skoro i tak w okolicy się pojawiają Bankierze różnego typu, dobrze by było, gdyby Paulina miała jakąś detekcję w tym terenie. Nie będzie to silna sieć - powiedziała Kaja - ale powinna dać JAKIŚ obraz sytuacji. Będzie wymagała zasilania by działać pasywnie, ale zawsze Paulina może ją po prostu odpalać aktywnie. Paulina wyłączyła teren Senesgradu i powiedziała Kai, że ta może robić swoje.

Kaja powiedziała też, że Prosperjusz nie pojawił się tu na polecenie Eweliny i unika z nią kontaktu.

* Diana buntuje się przeciw Ewelinie -1
* Diana chce zostać z Eweliną +1
* Kaja buduje sieć sensoryczną +1
* +1 akcja

Paulina poszła do ogródka porozmawiać z Hektorem. Hektor ciężko pracuje. Zmęczony, ale uśmiechnięty. Chwasty mu mutują... musi je pozabijać. Paulina powiedziała, że Grazoniusz wypadł z łask. No i powiedziała Hektorowi, że jest Kaja. Koleżanka Diany. Hektor powiedział, że Kai nie ufa. Paulina zauważyła, że jeśli Kaja jest przyjaciółką Diany, to może Hektor może przez Kaję dotrzeć do Diany... Hektor pokiwał głową i smętnie wrócił do pracy...

* Hektor do Dukata      -1

Paulina dała Hektorowi chwilę i zaprowadziła go do Kai. Tylko Kaja/Hektor, bez Diany. Paulina przedstawiła Kaję jako koleżankę Diany z pracy, przysłaną przez Ewelinę by pomóc a Hektora jako maga próbującego Paulinie pomóc z ogródkiem i gościa. Kaja przyjęła przedstawienie z całkowicie neutralną miną. Zero emocji. Zero ciepła, zero wrogości. Powiedziała, że pomoże - za wolą Pauliny - z ogródkiem. Paulina zajmie się wtedy terenem.

Paulina przycisnęła Kaję. O co chodzi z jej reakcją na Hektora? S. Kaja powiedziała, że jak pokazuje praktyka, magowie płci męskiej są impulsywni, nie potrafią ocenić sytuacji itp. Grazoniusz, Hektor, inni magowie na dworze Eweliny... ogródek tego dowodem. Będzie - oczywiście - traktować go poważnie i z odpowiednim szacunkiem, ale ma wątpliwości co do selekcji magów ze strony Pauliny. I tu - ciekawostka. "Czarodziejka może sama dobrać swoich sprzymierzeńców, prawda?"         +1 akcja

Paulina pojechała porozmawiać z Apoloniuszem Bankierzem. Ma przy sobie CV Hektora.      +1 akcja

Apoloniusz przyjął Paulinę dość ciepło. Paulina zauważyła, że teren jest dość specyficzny i nie zawsze konstrukty się sprawdzają. Co Apoloniusz wie z doświadczenia ;-). W skrócie - potencjalny pracownik lub dostawca. Apoloniusz dostał CV od Pauliny. Apoloniuszowi się bardzo Hektor spodobał. Gdy usłyszał o tym, że mogłoby to ugryźć Ewelinę gdyby Diana też doń trafiła, uśmiechnął się. Ale chwilowo nie widzi dla niej miejsca, chyba, że jako 'low-paid intern', ale Hektorem jest zainteresowany.

Zapytany o Prosperjusza, powiedział, że to całkiem ambitny, bezużyteczny mag - jeden z dworku Newerji. Lubi ryzyko i raczej jest agresywny. Nie lubi się płaszczyć. Niebojowy, twórca konstruktów.      +1 akcja

Paulina wróciła do domu. Tam wyczuła potężny spell-lock katalityczny na swoim domu (military-grade!!!), wyraźnie rzucony przez Kaję. Paulina, wściekła jak pies, weszła do domu wychodząc z auta. O dziwo, nic złego się w domu nie stało.

W domu w "swoich" pokojach są Hektor i Diana. W gościnnym (bo nie ma gdzie) siedzi Kaja i pije herbatę. Którą zrobiła. Min. dla Pauliny też.

Zapytana przez Paulinę z lekką furią, Kaja spokojnie odpowiedziała, że widząc kłótnię Hektora i Diany postawiła spell-lock i wysłała ich do pokoi. Diana chciała z Kają rozmawiać, ale Kaja się nie zgodziła - czeka na nieskażoną wersję dla Pauliny. Hektor nie chciał się podporządkować, ale Kaja spytała po prostu, czy Hektor chce ją uderzyć tak jak Grazoniusza. I poszedł. Kaja rozerwała spell-lock i przeprosiła Paulinę; weźmie na siebie konsekwencje swoich czynów. Paulina uznała, że nie ma konsekwencji.

"Tak nie może zachowywać się czarodziejka na dworze lady Eweliny. Tylko czarodziej." - Kaja o Dianie.

* +1 Diana nie jest zainteresowana mężczyznami
* +1 akcja

Paulina poszła więc porozmawiać z Hektorem. Zamkniętym (na klamkę) w swoim pokoju. HEKTOR IS SULKING. Hektor powiedział, że próbował być dla Kai tak miły jak się dało. Diana wyraźnie stanęła po jej stronie, założyła liberię... on próbował ją przekonać, by ją zdjęła, ona, że nie chce chodzić nago... ech. I ogólnie Hektor powiedział Paulinie wszystko ;-).

Paulina powiedziała Hektorowi, że on musi przeprosić Dianę. Za próbę podejmowania decyzji za nią. To jedna z nielicznych możliwości w jakie Hektor może choć trochę odzyskać Dianę... Paulina dała Hektorowi kilka hintów jak się adoruje dziewczyny. Wziąć na komedię romantyczną do kina. Zjeść coś razem w restauracji. Pójść na tańce. Coś takiego. A Paulina już wymyśliła, by Kaję wyekspediować na budowanie sieci; Paulinie niekoniecznie Kaja tu pasuje ;-).

* Paulina: Zna się na Dianie, Diana jest zakochana w Hektorze: +2
* Hektor: Kiepski z dziewczynami, Ma u Diany przechlapane, Diana ma coś do facetów: +3

Test 5v5->S.

* -2 Diana nie jest zainteresowana mężczyznami
* +1 akcja

Gdy Paulina odwiedziła Dianę, ta się poskarżyła na wielką niesprawiedliwość - Kaja zamknęła ją za niewinność. Tym razem Diana nie zrobiła NIC złego. NIC. Po chwili się zreflektowała - po co to mówi Paulinie, skoro ta jej nie zamknęła? Z jej punktu widzenia sytuacja była podobna do tego co mówiła Kaja. Diana wyraźnie już powoli waha się i uważa, że jak wróci do Eweliny, to może weźmie eliksir nullifikacji uczuć..?

Paulina zauważyła, że Hektor zasunął Grazoniuszowi o NIĄ. Nieistotny mag zaryzykował atak na maga RODU. Diana tak na to nie patrzyła wcześniej... Paulina też pokazała - Hektor Dianę bawił, robił te rytuały, z pewnością poprawiał jej humor... Diana przytaknęła. Nadal nie wierzy w przyjaźń, uczucia wszystko komplikują. 

* Paulina: Diana jej ufa, Diana kocha Hektora, Diana lubi Hektora: +3
* Diana: Bardzo faceci to zło, Kaja ma rację, To nie ma przyszłości, Silne emocje: +5

Paulina zaznaczyła, że to nie tak, że to nie ma przyszłości. Są różne możliwości utrzymania się i życia. Np. taki Karol. Zwykłe, codzienne życie. Paulina pokazała, że ona np. jest lekarzem, przyjmuje pacjentów. I w sumie niewiele więcej potrzebuje. A zdaniem Pauliny Hektor też jest w stanie się utrzymać.       +1 akcja (niwelowanie przyszłości)

Test 6v6-> F, S     +1 akcja

* KF: 7: Łatwiejsze plany przeciwika: Ewelina ma postęp do toru zwalczającego Apoloniusza +1
* -2 Diana nie jest zainteresowana mężczyznami

Paulina zostawiła Dianę ciężko nad tym myślącą i wróciła do Kai...

**Przeliczenie torów i urealnienie ścieżek za 170702**

* Akcji: 11, -3 Diana x Faceci, +1 Ewelina x Apoloniusz, +1 sensory, +1 Diana zostaje z Eweliną, +2 Diana widzi świat ludzi, +1 Diana z rodziną
    * Ewelina:      +3(1), +2(2), +2(4), +4(5)
    * Prosperjusz:  +3(2), +4(3), +3(5)
    * Neutralne:    +2(1), +1(2), +1(4), +2(5), +1(6), +3(7), +1(8)

1. Ewelina:
    1. Diana nie jest zainteresowana mężczyznami:       7/10
    1. Muszę być autonomiczna, Newerjo!:                3/10
    1. Integracja sensorów Kai:                         1/10
    1. Infiltracja Pauliny przez Kaję:                  0/10
    1. Wielki sukces Prosperjusza:                      2/10
    1. Prosperjusz kontra Apoloniusz:                   5/10
1. Prosperjusz:
    1. Grazoniusz, bracie!:                             0/10
    1. Umocnienie bazy:                                 3/10
    1. Logistyka bazy:                                  4/10
    1. Model biznesowy:                                 0/10
    1. Zrozumienie efemerydy:                           3/10
1. Wybór Diany:
    1. Diana zostaje u Eweliny:                         6/10
    1. Diana buntuje się przeciw Ewelinie:              7/14
    1. Diana wybiera swoją rodzinę:                     2/10
    1. Diana odchodzi w świat, samotnie:                3/10
    1. Diana wybiera Hektora:                           6/10
    1. Diana widzi alternatywy w świecie ludzi:         3/10
1. Ludzkie okoliczne problemy:
    1. Głos Praszyny wymaga uwagi:                      0/10
    1. Ludzie zainteresowani Dianą:                     0/10
    1. Działania Bankierzy zwróciły uwagę ludzi:        0/10
    1. Coś się dzieje w Senesgradzie:                   0/10
    1. Pojawia się efekt magiczny:                      0/10
1. Neutralna:
    1. Zadomowiony w okolicy Hektor:                    6/10
    1. Hektor zdesperowany, integracja z Dukatem:       5/10
    1. Dukat robi manewr mający kontrolować okolicę:    0/10
    1. Udobruchany Grazoniusz:                          2/10
    1. Grazoniusz u Prosperjusza:                       0/10
    1. Grazoniusz u Apoloniusza:                        2/10

przerwa dwutygodniowa IRL, więc reset dnia do nowego.

**Dzień 3**:

_Rano: (+1 akcja O)_

W Senesgradzie coś się zaczyna powoli dziać, Ewelina zaczyna powoli zyskiwać autonomię od Newerji, Prosperjusz i Grazoniusz weszli w kontakt, Diana coraz bardziej ciągnie do Eweliny i sojusz Grazoniusz - Prosperjusz staje się coraz bardziej prawdopodobny.

Paulina skontaktowała się z Czykomarem. Paulina przedstawiła mu swoją prośbę - chciałaby, by to trafiło do rąk Diany od jakiegoś kuriera. Czykomar spytał z uśmiechem czemu. Paulina powiedziała, że oryginalny zleceniodawca zlecił to w dobrej wierze, ale jak Hektor to przedstawi to to jedynie mu zaszkodzi. A pozostałe strony są co najmniej równie szkodliwe. Czykomar zauważył, że przesłanie kurierem odpowiednio niezaprzeczalnych dowodów będzie... trudne. Chciał przedstawić Hektorowi i yolo. Czemu ma się zatem męczyć i zapewniać, by przekazanie nie złamało maskarady? Paulina powiedziała, że zależy jej, by nie dowiedziała się tego od żadnej strony koło niej. Z ciężkim sercem Paulina zdecydowała się zapłacić za tą usługę (-1 surowiec).

Do Pauliny trafiła wiadomość od Prosperjusza. Mag zrobił formalny request, z listami polecającymi od Eweliny Bankierz. Chce założyć firmę; na siedzibę wybrał Senesgrad. Chce czerpać z mocy Wieży Ciśnień; zaczął już przygotowania. Paulina odpisała mu równie formalnie - musi wstrzymać wszystkie przygotowania. Czerpanie z efemerydy jest całkowicie zabronione - za niebezpieczne. Przebywanie magów w Senesgradzie też jest zakazane. Można tam wpadać, nie wolno mieszkać. I lepiej nie dotykać tamtejszej mocy. Paulina chce się z nim spotkać w okolicach popołudniowych.

Prosperjusz to przeczytał i lekko trafił go szlag. Jakaś prowincjonalna gęś będzie go sabotować... no, może da się z nią dogadać.

_Południe (+1 akcja O)_

Paulina poszła porozmawiać z Kają. Ta jest zajęta... sprzątaniem. Ściera kurze. 

* Kaju, mam pytanie odnośnie tej sieci czujników. - Paulina, zagajając
* Słucham, Rezydentko? - Kaja, w odpowiednio pokornej pozycji
* ...wiesz, mam imię... - Paulina, zrezygnowana - A jak Ci to nie może przejść przez gardło, odpuść sobie pozę.
* Rezydentko, imiona przychodzą i odchodzą, tytuł zostaje - Kaja z lekko filuternym uśmiechem (zachowanie: sassy)
* Uważam, że Twoje imię było warte zapamiętania, mimo, że odejdziesz - Paulina, z enigmatycznym uśmiechem.
* Oczywiście, Rezydentko Paulino. Mogę Cię tak nazywać? - Kaja, z lekkim zmieszaniem (udawanym?)

Paulina powiedziała Kai, że potrzebuje monitorowania Senesgradu i okolicy. Kaja obiecała zająć się tym jako pierwszym. Powiedziała, że może poprosić Grazoniusza do pomocy Kai. Kaja... zaironizowała lekko, że Grazoniusz jest precyzyjnym użytkownikiem młotka i na pewno pomoże... za 100 lat. Paulina się uśmiechnęła. Kaja powiedziała, że spędzi tam dziś i następny dzień. Będzie delikatna.

* Integracja sensorów Kai: +2

Przybył Prosperjusz z Grazoniuszem do Pauliny. Prosperjusz próbuje czarująco przekonać Paulinę odnośnie tego by pozwoliła mu założyć tam swoją filię. Paulina mu strzeliła, że dotknięcie tamtej Wieży Ciśnień ma fatalne efekty uboczne. Nowy Czernobyl. Ryzyko. A i artefakty mogą być Skażone... i opowiedziała coś z przeszłości tej Wieży.

* Prosperjusz: 4 baza, 1 bo Paulina jest nędzną siksą = 5
* Paulina: 4 baza, 1 bo Skażone artefakty, 1 bo Rezydentka, 1 historia = 7

Sukces. Prosperjusz cały zrezygnowany, bo musi wszystko rozmontować. Poszło w cholerę. Zapytany czemu ten teren, Prosperjusz powiedział, że to duży teren z małą ilością magów. A magowie muszą kontrolować teren. Lepiej by tu zbudować bazę C&C oraz fabrykę. Jego zdaniem Paulina jest potencjalną klientką... Paulina zaproponowała mu Praszynę - podniesiony poziom magiczny, nieco zaniedbany, gazeta plotkarska która będzie JEGO problemem. Ale jest tam jakieś źródło energii. Nie największe, ale jest. Z odpowiednią finezją coś się da zrobić.

* Umocnienie bazy: -2
* Logistyka bazy: -2

Paulina powiedziała swoje warunki. Żadnego dotykania ludzi. Żadnego dotykania ludzi magią. Żadnego działania przeciw ludziom. Jeśli komuś coś się stanie przez magię Prosperjusza i jego minionów, ma to odwrócić. To jej teren, ona ustala warunki. Paulina robi mumbo-jumbo, że ludzie/efemeryda. Mimo, że nie tam.

* Prosperjusz: 3 baza, +1 mag a ma patrzeć na ludzi, +1 "to mi się należy", +1 nieracjonalne zasady = 6
* Paulina: 2 baza, +1 efemeryda, +1 rezydentka, +1 sensowna oferta = 5 

Remis. (+1 akcja, S, KF: Problemy z czasem, synchronizacja czasowa: Apoloniusz dozna szkód przez działania Prosperjusza).

Paulina też powiedziała Grazoniuszowi, że ten może spokojnie czarować JEŚLI będzie trzymał się tych zasad co Prosperjusz.

* Apoloniusz vs Prosperjusz: +1
* Udobruchany Grazoniusz +2

_Wieczór: (+1 akcja O)_

Paulina pozałatwiała wszystko co trzeba w świecie ludzi by im pomóc się połapać. A przy okazji popytała o Kaję. Grazoniusz i Prosperjusz wymienili spojrzenia. Paulina dowiedziała się kilku ciekawych rzeczy:

* Kaja jest bardzo niebezpieczna. Jest zaufaną Eweliny. Pyskuje jej a Ewelina ma to gdzieś.
* Kaja jest szpiegiem Eweliny. Najpewniej sensory itp. będą służyć Ewelinie w takim stopniu jak Paulinie.
* Najpewniej Kaja jest po stronie Prosperjusza, bo ten będzie konkurować z Apoloniuszem.
* Prosperjusz martwi się, że Kaja właśnie zinfiltrowała jego bazę w Senesgradzie.

A Paulina skutecznie pomogła im zrozumieć jak działa teren i załatwić podstawy formalności.

* _Noc (+1 akcja O)_

Wszyscy śpią.

**Dzień 4**:

* _Ranek (+1 akcja S)_

KF: Zainteresowanie Nowej Strony. Dokładniej, Dukata, nowo powstającym biznesem i działaniami Eweliny wobec biednego lekarza.

Rano pojawił się kurier. Dostarczył Paulinie materiały adresowane do Diany. Diana dostała paczuszkę ze swoimi wynikami. Zaczęła czytać (1/k20 szczęścia jak to przyjmie). Czysto emocjonalny Paradoks. Paulina poczuła potężne emanacje magiczne techno-Zmysłów. Uziemia. Nie może złamać Maskarady. 

* Diana: 4, całe życie kłamstwem 1, bardzo zrozpaczona 2, czysta nienawiść i ból 1 = 8
* Paulina: 4, była gotowa, swój teren, sprzęt = 7

Remis (+1 akcja O). Paulina potężnie uziemiła zaklęcie Paradoksalne Diany. Paulina i Hektor pognali do Diany. Ta krzyczy w histerii, że jej całe życie jest kłamstwem! Jej magia zaczyna ją pożerać, Diana chce umrzeć i jej magia na to odpowiada... Paulina walnęła ją po twarzy by Dianę wyrwać. 6v3->S. Diana się jakoś uspokoiła; zamiast histerycznej pętli tylko żałośnie płacze.

Gdy tylko Diana się jakoś uspokoiła... w jakimś stopniu, wyjaśniła co tam jest. I powiedziała, że Ewelina jej to mówiła (że to NIE jest prawdziwa rodzina Diany - ale Diana się upierała, że jest). Ewelina miała rację. Nie ma dla niej przyszłości. Tylko Ewelina. Ona miała rację, ona wie lepiej. Paulina poszła ostro - skoro Ewelina to mówiła, to to wiedziała. Jeśli wiedziała, powinna była dać Dianie dowody. Mogła zrobić badania. Nie zrobiła. I zdaniem Pauliny to NIE Ewelina przysłała jej te dokumenty. Miała inne okazje.

* Diana: 3, obezwładniająco lojalna wobec Eweliny (3), szok (-2) = 4
* Paulina: 4, wsparcie Hektora (1), wszystko przygotowane (1), podniesiony wpływ (-2) = 6

F (+1 akcja O), S. Paulina uderzyła w linię lojalności wobec Eweliny. Osłabiła nieskończoną lojalność Diany do Eweliny.

Hektor wyraźnie próbuje jej wejść na miejsce rodziny. Pokazać, że nie jest sama. Że on się zajmie Dianą i nią zaopiekuje. Że nie musi wracać do Eweliny. Paulina go wzmacnia - pokazuje, że Diana była u elektronika i ona ma pewne kwalifikacje. Na pewno Diana jest w stanie znaleźć jakieś zajęcie. I Paulina spróbowała Dianie dać cel w życiu, jakikolwiek... a że ta jest młoda, może zbudować swoje życie od początku. Nie musi polegać na Ewelinie, która przecież okrutnie wykorzystała jej naiwność.

* Diana wybiera Hektora: +2
* Diana wybiera świat ludzi: +2
* Diana zostaje u Eweliny -3
* Zadomowiony w okolicy Hektor: +2

Intryga Pauliny ładnie zadziałała. Paulina pozostawiła młodych samych sobie i z uśmiechem poszła zająć się pacjentami. Jej mały plan zadziałał...

_Południe (+1 akcja O)_

1. Ewelina:
    1. Diana nie jest zainteresowana mężczyznami: 8/10
    2. Muszę być autonomiczna, Newerjo!: 5/10
    3. Integracja sensorów Kai: 5/10
    4. Infiltracja Pauliny przez Kaję: 2/10
    5. Wielki sukces Prosperjusza: 4/10
    6. Prosperjusz kontra Apoloniusz: 6/10
2. Prosperjusz:
    1. Grazoniusz, bracie!: 2/10
    2. Umocnienie bazy: 5/10
    3. Logistyka bazy: 4/10
    4. Model biznesowy: 1/10
    5. Zrozumienie efemerydy: 3/10
3. Wybór Diany:
    1. Diana zostaje u Eweliny: 7/10
    2. Diana buntuje się przeciw Ewelinie: 7/14
    3. Diana wybiera swoją rodzinę: 4/10
    4. Diana odchodzi w świat, samotnie: 5/10
    5. Diana wybiera Hektora: 9/10
    6. Diana widzi alternatywy w świecie ludzi: 5/10
4. Ludzkie okoliczne problemy:
    1. Głos Praszyny wymaga uwagi: 2/10
    2. Ludzie zainteresowani Dianą: 4/10
    3. Działania Bankierzy zwróciły uwagę ludzi: 0/10
    4. Coś się dzieje w Senesgradzie: 3/10
    5. Pojawia się efekt magiczny: 0/10
5. Neutralna:
    1. Zadomowiony w okolicy Hektor: 9/10
    2. Hektor zdesperowany, integracja z Dukatem: 5/10
    3. Dukat robi manewr mający kontrolować okolicę: 0/10
    4. Udobruchany Grazoniusz: 8/10
    5. Grazoniusz u Prosperjusza: 4/10
    6. Grazoniusz u Apoloniusza: 2/10

"Enough of that drama".

Paulina ma dość. Diana potrzebuje nowego pomysłu na życie. Muszą pogadać. Wzięła Hektora i kazała mu siąść. To samo z Dianą. I rozmowa. Paulina zaczyna z nimi rozmawiać - ogródek poszedł w cholerę, jej dom jest Skażony, stada arystokratów pałętają się w okolicy... niech ONI wykażą się dorosłością i porozmawiają. Jak ludzie.

* Paulina: 3, inżynier tego wszystkiego (1), oboje jej ufają (1), kochają się oboje (2), widzą przyszłość razem (1), postawiła im wino - rozluźnienie (1) = 9
* Hektor i Diana: 2, bardzo nieśmiali (2), lojalność Eweliny (2), różnice (1) = 7

SUKCES! Nareszcie. Diana wybrała Hektora a Hektor zdecydował się zostać w okolicy.

Paulina może wreszcie odpocząć. Przynajmniej JEDNA rzecz się udała.

1. Ewelina:
    1. Diana nie jest zainteresowana mężczyznami: 8/10      ABORTED
    2. Muszę być autonomiczna, Newerjo!: 5/10
    3. Integracja sensorów Kai: 5/10
    4. Infiltracja Pauliny przez Kaję: 2/10
    5. Wielki sukces Prosperjusza: 4/10
    6. Prosperjusz kontra Apoloniusz: 6/10
2. Prosperjusz:
    1. Grazoniusz, bracie!: 2/10
    2. Umocnienie bazy: 5/10
    3. Logistyka bazy: 4/10
    4. Model biznesowy: 1/10
    5. Zrozumienie efemerydy: 3/10
3. Wybór Diany:
    1. Diana zostaje u Eweliny: 7/15
    2. Diana buntuje się przeciw Ewelinie: 7/15
    3. Diana wybiera swoją rodzinę: 4/10            ABORTED
    4. Diana odchodzi w świat, samotnie: 5/10       ABORTED
    5. Diana wybiera Hektora: 10/10                 MAX
    6. Diana widzi alternatywy w świecie ludzi: 5/10
4. Ludzkie okoliczne problemy:
    1. Głos Praszyny wymaga uwagi: 2/10
    2. Ludzie zainteresowani Dianą: 4/10
    3. Działania Bankierzy zwróciły uwagę ludzi: 0/10
    4. Coś się dzieje w Senesgradzie: 3/10
    5. Pojawia się efekt magiczny: 0/10
5. Neutralna:
    1. Zadomowiony w okolicy Hektor: 10/10      MAX
    2. Hektor zdesperowany, integracja z Dukatem: 5/10      ABORTED
    3. Dukat robi manewr mający kontrolować okolicę: 0/10
    4. Udobruchany Grazoniusz: 8/8          MAX
    5. Grazoniusz u Prosperjusza: 4/10
    6. Grazoniusz u Apoloniusza: 2/10

**Interpretacja torów:**

Hektor i Diana zostają parą a Hektor zdecydował się osiąść w okolicy. Będą próbować żyć razem, wbrew innym haterom. Pojawiają się plotki o Dianie wśród sąsiadów Pauliny. Grazoniusz pogodził się z Pauliną. Nie ma nic do niej.

Chwilowo Grazoniusz chce współpracować z Prosperjuszem bardziej niż z Apoloniuszem. A Kaja ma dość spore pokrycie czujnikami terenu, pracuje jak mróweczka.

**Przypomnienie Komplikacji i Skażenia**:

* Nowy tor: Dukat wchodzi do gry. Interesuje się Eweliną (za lekarza) oraz Kają (łazi mu po terenie) oraz Prosperjuszem (bo biznes).

# Progresja

* Diana Łuczkiewicz: została parą z Hektorem
* Hektor Reszniaczek: został parą z Dianą
* Hektor Reszniaczek: znalazł pracę u Apoloniusza Bankierza jako konstruktor odpowiednich ziół do eliksirów

# Streszczenie

W okolicy pojawiło się dwóch nowych magów - Prosperjusz i Kaja. Paulina wymanewrowała obu, kupując sobie czas i doprowadziła do tego, że Hektor i Diana wreszcie zostali parą. Najwyraźniej Kaja szpieguje dla Eweliny a Prosperjusz chce się od Eweliny oderwać. Paulina przesunęła Prosperjusza na miasteczko gdzie ten może się przydać, załatwiła Hektorowi pracę u Apoloniusza i doprowadziła do tego, że Diana poznała swoją przeszłość - jej rodzina nie jest jej rodziną.

# Zasługi

* mag: Paulina Tarczyńska, udaremniła facetożerną Ewelinę i Kaję i doprowadziła do sparowaniu Hektora i Dianę. Też: umieściła Prosperjusza tam, gdzie jej się przyda.
* czł: Karol Komnat, znajomy elektronik Pauliny. Powiedział, że Diana ma potencjał. Po prostu dobry, rzetelny fachowiec.
* mag: Diana Łuczkiewicz, sparowała się z Hektorem po ostrej kłótni. Załamana, bo całe jej życie okazało się iluzją stworzoną przez jakiegoś rzezimieszka. 
* mag: Hektor Reszniaczek, sparował się z Dianą po ostrej kłótni. Zdecydował się osiąść w pobliżu. Szybko uczy się trzymać język za zębami. Paulina załatwiła mu pracę u Apoloniusza.
* mag: Kaja Maślaczek, służka Eweliny; Zmysły, Kataliza, Infomancja; przyjaciółka Diany i podobno bardzo bliska Ewelinie. Sassy and witty. Rozmieszcza dla Pauliny sensory detekcyjne.
* mag: Prosperjusz Bankierz, mentalna, materia, bio; chce założyć konstrukcję konstruktów w okolicy. Paulina pomogła mu założyć bazę w Praszynie. Ten ma zmysł polityczny.
* mag: Grazoniusz Bankierz, może pracować z Prosperjuszem lub Apoloniuszem. Przeprosił się z Pauliną. Na razie pomaga Prosperjuszowi w tle.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, centralne dowodzenie Pauliny gdzie Kaja sprząta ;-). I ogólnie dużo tam się zaklęć ostatnio uziemiało.
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, którą chciał wykorzystać Prosperjusz, którą badał Grazoniusz i którą z oddali sensorami obudowuje Kaja.
                1. Trzeszcz
                    1. Elektronik Suwak, sklep elektroczny ze znajomym elektronikiem Pauliny (Karolem Komnatem)
                1. Praszyna
                    1. Redakcja Głosu Praszyny, magazyn plotkarski w którym czasem dochodzi do (zmyślonego zwykle) łamania Maskarady; Paulina musi się tym zajmować...
                    1. Ptasiówka, gdzie Paulina umiesciła Prosperjusza i jego biznes.
            1. Powiat Głuszców
                1. Upiorzec
                    1. Eliksir Aerinus, gdzie Paulina rozmawiając z Apoloniuszem załatwiła Hektorowi pracę.

# Czas

* Dni: 4

# Frakcje

## Lady Ewelina Bankierz

### Koncept na tej misji:

Zniszczyła Grazoniusza. Nadal próbuje pokazać Dianie na czym polega życie, ale skupia się też na nowym zagrożeniu dla swojej pozycji - na Prosperjuszu. Dowiedziała się o jego obecności dzięki Dianie. Paradoksalnie ma zamiar pomóc Prosperjuszowi w osiągnięciu odpowiedniej autonomii, by już nigdy więcej nie próbował się do niej dobierać.

### Scena zwycięstwa:

* Diana nie jest już zainteresowana żadnym mężczyzną w okolicy
* Sama Ewelina ma czyste ręce i nikt nie może jej niczego udowodnić
* Prosperjusz wykorzystuje ekonomicznie efemerydę kapsuły
* Prosperjusz się tak wpakował, że nie jest zupełnie zainteresowany dworem Eweliny
* Ewelina ma pełne pole widzenia w terenie Pauliny; tam się dzieje coś dziwnego
* Apoloniusz jest ukarany za to co kiedyś zrobił Ewelinie. Przez Prosperjusza.

### Motywacje:

* "Samodzielna, niezależna, nie potrzebująca nikogo - to ja."
* "Jeden zniszczony. Czas odepchnąć drugiego."
* "Nie przeszkadza mi to, że zyskują. Byle nie moim kosztem."

### Siły:

* Ewelina Bankierz, manipulatorka w tle
* Diana, na którą Ewelina ma wpływ
* Kaja, służka Eweliny, która jednocześnie jest jej relayem
* Konstruminus w stylu Apoloniusza do zwalczania Prosperjusza i Apoloniusza

### Wymagane kroki:

* Pokazanie Dianie, że na mężczyznach nie można polegać i zawsze zawiodą
* Wprowadzenie Kai do domostwa Pauliny jako wsparcie i pomoc dla Diany i Pauliny
* Wprowadzenie Kai jako narzędzie donoszenia i informowania Eweliny
* Wykorzystywanie wiedzy z okolicy przez Kaję do informowania Pauliny i Prosperjusza
* Wykorzystywanie konstruminusa by zrzucać winę na Apoloniusza

### Ścieżki:

* **Diana nie jest zainteresowana mężczyznami**:    7/8 (2,2,2,2): przesunięcie Diany na pozycję podobną do swojej
* **Muszę być autonomiczna, Newerjo!**:             1/6 (2,2,2): starsza czarodziejka może rzuci jej jakiś ochłap autonomiczności bez kuzynów
* **Integracja sensorów Kai**:                      0/8 (2,2,2,2): Kaja ma wystarczające oczy w okolicy, by Ewelina wiedziała, co tam się dzieje
* **Wielki sukces Prosperjusza**:                   0/10 (3,3,3,4): Prosperjuszowi uda się osiągnąć sukces biznesowy w okolicy
* **Prosperjusz kontra Apoloniusz**:                0/8 (2,2,2,2): ...oraz Prosperjusz i Apoloniusz zewrą się w konkurencji - zwalczą się nieco


## Majątek Prosperjusza

### Koncept na tej misji:

Młody i ambitny mag z dworu Eweliny rusza w poszukiwaniu własnej przyszłości. W ten sposób ma zamiar ZMUSIĆ Ewelinę, by ta zrobiła to, czego on sobie życzy. Uzyskać ją dla siebie.

### Scena zwycięstwa:

* Mały, solidny biznes oparty o konstrukty i zasilane przez Efemerydę Kapsuły w Senesgradzie
* W pokoju z lokalną rezydentką; jak trzeba, opłaci się haracz. Może jest Bankierzem, ale liczy pieniądz.
* Zdobycie potęgi jak Apoloniusz; być może nawet większą potęgę.
* Skaptowanie Grazoniusza i ściąganie coraz większej ilości magów z dworu Eweliny. Tych użytecznych.
* Zdobycie kontroli nad Eweliną Bankierz ekonomicznie. Niech Newerja wybierze JEGO.

### Motywacje:

* "Nic nie da płaszczenie się przed Newerją. Ewelinę trzeba zdobyć siłą."
* "Pieniądze to władza. Apoloniusz miał rację - Ewelinę należy zdobyć, nie się jej przypodobać."
* "Każdy jest potencjalnym sojusznikiem. Niestety, każdy jest też potencjalnym przeciwnikiem..."
* "Odbierzmy Ewelinie jej władzę. Sama zobaczy, jak to działa..."

### Siły:

* Prosperjusz osobiście: sam mag we własnej osobie.
* Oddziałek Prosperjusza: zbudowane przezeń konstrukty i jednostki waleczne.
* Dworek Prosperjusza: dwóch magów wspierających go; spoza rodu.

### Wymagane kroki:

* Zdobycie pozwolenia na postawienie bazy od Pauliny. Najlepiej tam, gdzie chce ;-)
* Rozpoczęcie umacniania bazy. Logistyka. Klienci. No, ogólnie.
* Zrozumienie czym jest ta konkretna efemeryda i jak użyć tego jako źródło energii.
* Pozyskanie i skaptowanie Grazoniusza.

### Ścieżki:

* **Grazoniusz, bracie!**:                          0/8 (2,2,2,2): uzyskanie wsparcia Grazoniusza
* **Umocnienie bazy**:                              0/8 (2,2,2,2): zbudowanie małej bazy - potencjalnie w Senesgradzie
* **Logistyka bazy**:                               0/8 (2,2,2,2): zapewnienie sił i wsparcia dla swoich oddziałów i działań
* **Model biznesowy**:                              0/8 (2,2,2,2): znalezienie klientów i sprawnego modelu biznesowego dla swoich działań
* **Zrozumienie efemerydy**:                        0/10 (3,3,3,4): czy to przez wynajętych magów, czy przez własne działania - zrozumienie samej efemerydy

## Frakcja neutralna

### Ścieżki:

* Diana zostaje u Eweliny:                          3/6     Tier 1
* Diana buntuje się przeciw Ewelinie:               6/8     Tier 3
* Diana wybiera swoją rodzinę:                      1/6
* Diana odchodzi w świat, samotnie:                 2/6     Tier 1
* Diana wybiera Hektora:                            4/8     Tier 2
* Zadomowiony w okolicy Hektor:                     3/8     Tier 1
* Hektor zdesperowany, integracja z Dukatem:        4/8     Tier 2
* Głos Praszyny wymaga uwagi                        0/8

# Narzędzia MG

## Opis celu misji

What the several tests of the past two months have proven, the players do not wish to see this story paths. If they do, they are overwhelmed. There is simply too much happening for them to be able to influence things they are interested in.

The actual solution is to hide the story paths from the players and just present them the outputs. The fact, that the players will not be able to influence everything they care about is acceptable in this case.

Another apparent problem is the fact that the story complications happened too fast. If every story complications generate another thread, this means that the amount of threads with the session having 10 conflicts and three failures is simply too high.

The perfect solution would be to split the complications into two types – tactical and strategic. Of course, this is just the name. The idea is, strategic story complications are those which add a new story path. Tactical story complications are those which influence existing paths under the goals of enemy factions.

A potential first implementation would be one strategic for every two tactical. That way new story threads are being generated which slows down the overall path gains, but also tactical gains can be more influential depending on the circumstances.

## Cel misji

1. O-O-S-O-O-S, gdzie O to operacyjne (wzrost ścieżki) a S to komplikacje (nowa ścieżka).
    1. O: przeciwnik dał radę podnieść JAKĄŚ ścieżkę. Jeden lub wszyscy. Generowana jest nowy Postęp Przeciwnika.
    1. S: przeciwnik ma NOWĄ ścieżkę. Jeden lub wszyscy. +1 akcja dla wszystkich przeciwników. Generowana jest nowa Komplikacja Fabularna (wątek).
    1. Eskalacje przesuwają to numerowanie dalej, normalnie.
1. Dzień składa się z 4 faz, domyślnie. Normalne postacie w jednej fazie śpią ;-).

## Po czym poznam sukces

1. Nie ma tak szybkiego galopowania ścieżek; rozwadnia się sytuacja by gracz mógł wygrać jeśli bardzo zależy.
1. Nie ma tak dużego stresu wynikającego z pola widzenia graczy.
1. Mniej wymyślania wątków, więcej postępu przeciwnika
1. Lepszy upływ czasu - czas płynie

## Wynik z perspektywy celu

1. Kić: Nie ma stresu. Cokolwiek się dzieje, jest niewidoczne. Nie boli a są systemy kompensujące. Żółw: Nie mogę dopełnić teraz ścieżek, ale pojawiają się inne. Takie, które mają sens nawet na niższym poziomie. Narzędzie działa, acz inaczej niż chciałem. Ale działa, robi co powinno. Nie powygrywam ;-).
1. Żółw: Jest postęp przeciwnika. Kić go nie widzi, ale JEST. Nie było problemu ze sztucznymi komplikacjami. 3-1 lepiej niż 2-1. OOOSOOOS > OOSOOS. Może trzeba kluczowe ścieżki desygnować.
1. Żółw: Czas płynie. Fajne.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |
