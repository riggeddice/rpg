---
layout: inwazja-konspekt
title:  "Chrumpokalipsa"
campaign: wizja-dukata
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180104 - Wspaniały Wieprz Wojtka](180104-wspanialy-wieprz-wojtka.html)

### Chronologiczna

* [180104 - Wspaniały Wieprz Wojtka](180104-wspanialy-wieprz-wojtka.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

brak

### Wątki konsumowane:

brak

## Punkt zerowy:

* Ż: Skąd w okolicy źródła energii znalazła się stewardessa?
* D: Odwiedzała rodzinę.
* Ż: W jaki sposób istotna jest masowa farma wegetariańska?
* D: Należy do głównego oponenta politycznego aktualnego burmistrza który chce rozsławić Półdarę jako stolicę wegetariańską Mazowsza...

## Potencjalne pytania historyczne:

* Czy Golem Esuriit da radę Skazić wszystko w Wegecud? (TAK, długość: 3)
* Czy Stefanii uda się przekierować golema Esuriit w kierunku na stare źródło energii w Muzeum? (TAK, długość: 3)
* Czy Stefanii uda się odpalić pryzmatycznie Muzeum? (TAK, długość: 3)
* Czy Kupiernik ucierpi przez Golema Esuriit? (TAK, długość: 2)
* Czy harcerze znajdą świnię zagłady? (TAK, długość: 3)

## Misja właściwa:

**Dzień 1**:

Przez problemy z glukszwajnami hipernet jest słabszy niż powinien być. Ale jest kontakt ze Świecą i wszystkim. Anatol ma pomysł na 3 ścieżki działania:

* Burmistrz i sprawdzenie co się dzieje z norkami. Może nie wie zbyt dużo.
* Kopanie w dół i sprawdzenie, w co transmitowana jest magia.
* Pogadanie ze Stefanią. Ale jak ona jest po Drugiej Stronie to jest to niebezpieczne.

Walecznisko. Anatol proponuje phase shift pod ziemię. Kinga proponuje przesunąć ziemię. Więc - kopać dół. Nie ma kłopotu, bo nie ma tu detektorów. NIE MA NIC. Bardzo mało epicko. Anatol jest nieszczęśliwy - akcja powinna być EPICKA. Anatol odmawia kopania dołu; kolej Kingi na magiczne kopanie dołu MAGOTAŚMOCIĄGIEM. Wykorzystany przez nią sprzęt jest systemem wczesnego ostrzegania by nie wpaść głupio z własną magią. Taśmociąg magiczny buduje taki wielki lej. (Zwykły 5, Skażenie, Niezauważenie vs 7 + Detektory, Lejek Ostrożności, Zawsze Przygotowana). Powstało zejście.

Znaleźli rurę i malutką przepompownię. Rura idzie od jeziora w kierunku TAM (na parów natarcia gdzie pasł się oczkodzik).

Anatol zauważył coś ciekawego. Jest barak, jest impreza. Emitery są skierowane w dół. Niezależnie od tego JAKIE emocje się nie znajdują w tamtym miejscu... to by wysłało fajną energię imprezową a nie mroczną. Może wsadzić jakiś nadajnik i sprawdzić, gdzie on dotrze?

Anatol zdecydował się zakopać dół magią (2 karty) i odrósł ściółkę, ale wteleportują do rury nadajnik Świecowy. Anatol wteleportowuje w rurę nadajnik który przekazała Kinga. PARADOKS. Elementalna + Ukrycie Wiedzy + Soczewka. SŁUCHAJCIE. Nie tylko się dowiedzieli. Dodatkowo nadajnik został Ukryty. Zainfekował przepompownie. Nie ma żadnego nadajnika. ŻADNEGO. Ale za to mają mapę rur i trzech pompowni. Od jeziora do jeziora a po drodze "kaloryfer" oddający energię - kaloryfer jest pod tamtym parowem. Tam, gdzie oczkodzik próbował się dostać. I tam było przebicie z Esuriit...

[!Rysunek pokazujący rozłożenie tunelu wobec jeziora, fermy norek itp](Materials/180112/mapa_poldary_1.png)

Nie wiedzą gdzie jest winowajca, ale przybliża ich to do tego, że jest tu 'unattended installation'. Jest tu od roku? I tam, w tamtym parowie najpewniej dzieją się wszystkie rzeczy które powinny się dziać...

Zespół zdecydował się przejść po trasie tych rur. Z ciekawości. Czy są tam jakieś interesujące miejsca na tej drodze. Czy jest jakaś instalacja, czy jest coś... dziwnego?

* Rura idzie pod Miasteczkiem Turystycznym. Tam, gdzie są norki.
* Po stronie "kaloryfera" i dalej poziom Skażenia jest większy. Niebezpieczniejszy. Esuriit przebiło? Kajetan powinien był to znaleźć... czyżby to świeższe niż Kajetan?

Kinga spróbowała przeanalizować sytuację. Chce się dowiedzieć, jaki kolor ma ta energia i jak jest świeża. O co w ogóle tu chodzi? Jak ta sprawa wygląda? Skonfliktowany Sukces.

* Doszło do uszkodzenia ekranowania pod parowem po stronie pokaloryferowej. Stąd widać energię.
* To jest świeższe. To nie jest coś starego. Energia nie jest silna, ale jest.

W ramach skonfliktowania: Golem może wyjść na światło dzienne.

A w parowie... Kupiernik. Rozłożył pentagram z małych srebrnych krzyżyków (posrebrzanych), postawił poświęcone świece i skrapia wodą święconą. EGZORCYZMUJE. Zespół założył na siebie iluzję... (Zwykły 5, Skala 2, Zaskoczenie 2 vs 6, GotowaNaWszystko, Kontrola Terenu, Nothing To See). Trzech harcerzy podeszło do Kupiernika; okazało się, że "nie ma świni" i "świnia zniknęła". Kupiernik zauważył, że świnia TAK ROBI. Muszą wyegzorcyzmować teren...

...Anatol zdecydował się stanąć w środku tego pentagramu i nadać im Lekcję By Nie Robili Głupot. Anatol zdecydował się rzucić dwa zaklęcia by rozwiązać problem. Pierwszy czar będzie na harcerzy, drugie na Kupiernika. Klątwa biomantyczna na harcerzy - niech przez dzień czy dwa wszystkim zrobi się mdło na widok mięsa. Klątwa NIEJEDZENIA MIĘSA. Klątwa wegetarianizmu! W amplifikatorze magii.

Drugi czar idzie na Kupiernika. Anatol mówi gromkim głosem "OINK!" i rzuca czar. Magia materii i odrobina elementalki dla efektu. Rozstępuje się pod nim ziemia, wybuchają płomienie i rozlega się gromkie "chrum!". W obszarze bliskim czarnotruflom. Anatol jest jeszcze bardziej zdziwiony niż Kupiernik. Zleciał na dół.

* Podziemie: Zwykły (5), Delikatny grunt, Rozstąpiona ziemia magią, Zaskoczenie, Amplifikacja magii

Wpadli do pieczary, Kupiernik i Anatol. Wpadli do pieczary. KTOŚ WYKORZYSTUJE TEN KALORYFER DO HODOWLI CZARNOTRUFLI! Kupiernik w szoku PORWAŁA GO ŚWINIA. Ogłuszył go Anatol. To uruchomiło golema Esuriit, który idzie w kierunku Kupiernika; jest w zasięgu na 1 zaklęcie. Sprytnie Anatol przeteleportował ich do góry... a raczej próbował.

* Golem: Trudny (10), Powolny (-2), Esuriit (2)
* Anatol: (6), Hero of the People, Ewakuacja, Niesie Pomoc

Sukces. Udało się. Golem podszedł i spojrzał do góry. Moment później wrócił do swoich trufli. Anatol odbudowuje krater magicznie. Nie ma Efektu Skażenia - ziemia się przywróciła, acz bez ekranów antymagicznych. Ludzie nie zauważą. Kupiernik uratowany, Anatol zebrał wszystkie krzyżyki i świeczki, cierpliwie. Wszystko wskazuje na to, że golem jest w stanie infekować Esuriit, ale nie będzie w stanie stworzyć Wyssańca.

Zespół zdecydował się poczekać chwilę. Wzięli, polecieli na drzewo by przeczekać falę harcerzy. A Kinga ma przeczytać Kupiernika w tym czasie. Kupiernik (Zwykły 5). Bez Efektów Skażenia. Kinga wie wszystko co chciała. Kupiernik... szuka świń. Świnie stały się jego obsesją po wydarzeniach z jego przeszłości. Zaczął działać z harcerzami, ze swoim przydupasem - ogólnie, Łowcy Dziwnych Świń stali się bardziej potężną gildią.

Zespół z Kupiernikiem (związanym i zakneblowanym) obserwują teren całą noc...

**Dzień 2**:

Świt. Czas zacząć kombinować z golemem - którego tam już nie ma ;-).

Opcje:

* 1: Meteor Shower. Zawalić gruz na golema. Zawalić WSZYSTKO. Jak przestanie się ruszać, walimy mocniej.
* 2: Wteleportować się tam. Tłuc bydlę ręcznie.

Kinga chce rozwalić sufit sama, wpierw. Anatol zdjął sufit a Kinga złożyła pole braku zainteresowania dookoła "go away". Udało im się zdjąć to wszystko. Tyle, że... tam na dole NIE MA golema. Za to przyszła Stefania i zapytała, co oni w ogóle tutaj robią. Czemu niszczą jej trufle. Okazała ogromne zdziwienie tym, że Esuriit miało przebicie, ale odmówiła powiedzenia na temat przeszłości - załatwiła wszystko legalnie z perspektywy lokalnego terenu. Tyle, że nie wie, że Zespół NIE JEST z tego terenu. Stefania nie ma pojęcia że Zespół jest świadomy pozycji Świecy i Sądecznego, ale ma inne pojęcie hierarchii i sytuacji politycznej. A Zespół nie wie, że Stefania ma wsparcie ze strony Sądecznego, że mafia może ją pociąć na kawałki jeżeli ona zadziała wbrew swoim instrukcjom.

Stefania nic nie powie (acz chce współpracować odnośnie tego co TERAZ) a Zespół terminusów musi znać przeszłość by móc containować zagrożenie trufli Esuriit (acz chce współpracować ze Stefanią). Problem Antygony i Kreona. Rozpacz. A golem GDZIEŚ jest.

Anatol zażądał od niej poddania się do aresztu. Kinga przesunęła się tak, by móc odciąć Stefanię. Stefania ma PRAWO swoim zdaniem. Zespół atakuje Stefanię i ją ogłusza. Czarodziejka Eliksiru Aerinus padła na ziemię nieprzytomna. Kinga force-cagowała nieprzytomnych: Stefanię i Kupiernika. Są gotowi na poszukiwanie golema.

Kinga wzięła swoje artefakty detekcyjne i zabrała się do szukania Golema Ogrodniczego Skażonego Przez Esuriit. Anatol zabezpieczył się przed atakiem (omnitarcza). A potem wzmocnił Kingę tarczami defensywnymi. Kinga tymczasem MUSI być przygotowana idąc na Golema Esuriit. Zdecydowała się przeczytać pamięć (nielegalnie!) Stefanii. Kinga rzuca otwarte zaklęcie - chce dowiedzieć się jak wyglądają tunele i jak tam wygląda sprawa.

* Okazuje się, że Stefania jest podległa jakiemuś lokalnemu kralothowi
* Okazuje się, że Stefania współpracuje i z kralothem i z Eliksirem Aerinus; jest uzależniona od niego
* Stefania sharvestowała trufle Esuriit i przekazała je zarówno kralothowi jak i do Eliksiru
* Apoloniusz miał się zająć golemem esuriit, ale klątwożyt go zdjął.

Oki, czas znaleźć cholernego golema. On będzie świecił energią. Kinga odpaliła artefakty detekcyjne - golem już zmasakrował norki i jest blisko obozu harcerzy. Bardzo blisko. Trudno, trzeba tam natychmiast się teleportować i zaatakować...

* Golem:
* Trwałość: 3
* Poziom: Trudny (10)
* Siły: napędzany energią Esuriit, bardzo pancerny, bardzo silny, relentless hatred, wzbudza strach
* Słabości: widzi tylko energię życiową, stosunkowo powolny, niezgrabny

Kinga przygotowała zaklęcie i teleportowała. Przygotował swój czar Anatol... i poszli. Efekt Kingi: Biomancja + Przesunięcie + Maskarada. Wszyscy harcerze są "w scenie", w okolicy golema. Wszyscy są tu blisko i patrzą. I nie mają ubrań. Przerażeni. 

Sęk w tym, że Anatol próbujący zrobić swoje zaklęcie... też miał Efekt Skażenia: Mentalna + POWIĘKSZENIE SKALI + Dowiedzenie się czegoś + Dowiedzenie się czegoś + Zadziwiające Zaklęcie. 'Shield Bash' jako zaklęcie udało się prawidłowo. Epicko.

* Golem: 18
* Anatol: 8, Zaskoczenie, Skala, Efektowność, Walka Tarczą, Taran, Widownia, Magitech

CAŁA PÓŁDARA tu "jest" mentalnie. Wszyscy to widzą. Wszyscy tu są. Dodatkowo: "świnia na burmistrza". A ktokolwiek odbiera wiadomości hipernetowe w okolicy pojawia się pełna świadomość tego co tu się dzieje. BO MUSI. Niech Świeca ma szansę zareagować jako pierwsza siła na miejscu - nie lokalna Świeca, Świeca z innych regionów. Plus: wszyscy w Półdarze mają pełną świadomość tego gdzie znajduje się KAŻDA świnia w promieniu 10 km.

Golem skorzystał z energii Esuriit i wyemitował potęgę Endless Night - Esuriit, podpierając się potęgą wszystkich tych umysłów sprzężonych w tej scenie i lokalizacji. Kinga i Anatol padli do potęgi Esuriit - są Ranni, osłabieni i zapłakani przez koszmar Esuriit. Kinga ocknęła się pierwsza i strzeliła 100k lumenów prosto w niego laserem. Mnóstwo luster w powietrzu kanalizujących światło prosto w ranę golema. I odpaliła artefakt Dare Shiver - LATARKA MOCY.  Niech bigassmirrors działają lepiej.

EFEKT SKAŻENIA. Mentalna -> Tworzenie -> Tymczasowa zmiana Otoczenia. Harcerze - niezbyt ubrani - stają się zintegrowani mentalnie ze świniami; wymieniają się informacjami ze świniami. Mentalnie są WARCHLAKAMI. A scena, uwaga wszystkich i wszystkiego, "wspólnego mózgu wszystkich ludzi" skupia się właśnie na owych nieszczęsnych harcerzach.

Spektakularny atak Anatola: shoryuken teleportujący golema baaardzo wysoka. Niech ta istota spadnie i niech się rozpadnie. Niestety, ku utrapieniu Anatola... WSZYSCY PATRZĄ NA HARCERZY! Golem jest duży, ciężki i uszkodzony. Nadal jednak jest pancerny i napędzany Strachem, Pryzmatem i Esuriit. Wywołał ogromne szkody w lokalnej społeczności. I stworzył spike - niech spadnie właściwie...

EFEKT SKAŻENIA: Transport + Elementalna -> Przesunięcie -> Półstabilna konstrukcja.

Wszystko. Absolutnie WSZYSTKO powiązane z tą sprawą. Zniszczony Golem Esuriit, martwe norki, zniszczone (spalone) trufle... WSZYSTKO sformowało się w konstrukcję o kształcie wielkiego wieprza. Znajduje się w Parowie Natarcia. Niezakrywalny, nieukrywalny, łamiący Maskaradę samą swoją egzystencją...


## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy
* 5: anulowanie wyniku konfliktu

Z sesji:

1. Żółw (26)
    1. Wszystko w Wegecud zostało zniszczone - rozpoczyna się wojna polityczna między "burmistrzami" (3).
    1. Pojawia się Świnia Polityczna i kandyduje na najważniejsze stanowisko. (3)
    1. Hlangoglormo ma 'wsparcie' ze strony Rodziny Dukata jako 'tame medical scientific kraloth' (3)
    1. Wojtek potrafi Rozkazywać świniom. Świnie robią to, czego Wojtek żąda. (3)
    1. Wieprzopomnik w Parowie Wtargnięcia się odbudowuje. Zakleszczył się tam w czasoprzestrzeni. "Niezniszczalny". (3)
    1. Sprawa ze Stefanią wyszła na jaw i zdenerwowała siły Dukata.
    1. Kupiernik został uznany męczennikiem przez Łowców Dziwnych Świń. Schodzą do podziemia - rząd pracuje ze świniami.
    1. Pojawia się niewielki kult dookoła Wieprzopomnika w Parowie Wtargnięcia. Ku wielkiemu nieszczęściu wszystkich, on odrasta.
    1. Burmistrz planuje zrobić z Wieprzopomnika odpowiednik Nimis (Ladonia).
1. Kić (11)
    1. Opinia: niestandardowe podejście Kingi uratowało Stefanię.
    1. To nie Paulina jest obwiniona o te rzeczy, które się działy w Półdarze - to Świeca Nie Z Tego Terenu.
    1. Kinga dała radę zdobyć fragment Wieprzopomnika z Półdary jako "pamiątkę" na etapie formowania.
    1. 
1. Dzióbek (10)
    1. Wieści o tej akcji dotarły do Ewy Zajcew. Ewa skojarzy Anatola z imienia i nazwiska.
    1. 
    1. 

# Streszczenie

Zespół kandydatów na terminusów odkrył, że pod Waleczniskiem jest hodowla czarnotrufli Eliksiru Aerinus, Skażona przez Esuriit. Przechwycili czarodziejkę za to odpowiedzialną (Stefanię) i odkryli jej powiązania z kralothem w okolicy. Dodatkowo weszli w bój z niegdyś rolniczym Golemem Esuriit, doprowadzając do ogromnego złamania Maskarady i stworzenia Wieprzopomnika. Przy tym wszystkim konsekwencje nie spadną na nich ;-).

# Progresja

* Kinga Bankierz: opinia w Świecy: niestandardowe podejście Kingi uratowało Stefanię od niewoli kralotha.
* Kinga Bankierz: posiada mały odłamek Wieprzopomnika Półdarskiego, sprzężonego z głównym pomnikiem.
* Kinga Bankierz: tymczasowo Dotknięta przez Mrok Esuriit. Wrażliwa na tą formę energii.
* Paulina Tarczyńska: udało jej się uniknąć obwinienia za Wieprzopomnik w Półdarze; to Świeca Z Nie Tego Terenu.
* Anatol Sowiński: tymczasowo Dotknięty przez Mrok Esuriit. Wrażliwy na tą formę energii.
* Anatol Sowiński: usłyszała o nim Ewa Zajcew z uwagi na Wieprzopomnik w Półdarze.
* Ewa Zajcew: usłyszała o Anatolu Sowińskim z uwagi na Wieprzopomnik w Półdarze.
* Paweł Kupiernik: nie pamięta o wszystkich tych świniokształtnych tematach
* Paweł Kupiernik: w oczach Łowców Dziwnych Świń jest bohaterem i męczennikiem
* Hlangoglormo: uznawany za 'tame medical kraloth' przez Rodzinę Dukata
* Stefania Kołek: okazuje się, że była już uzależniona od Hlangoglormo (kralotha)
* Stefania Kołek: przetransferowana poza Mazowsze przez siły uczniów Świecy
* Wojciech Piekarz: uzyskał umiejętność Rozkazywania świniom. Atak mentalny.

## Frakcji

* Łowcy Dziwnych Świń: stracili przywódcę, Pawła Kupiernika. Ale on został ich męczennikiem. Kult Kupiernika.
* Łowcy Dziwnych Świń: coraz ściślejsza współpraca z harcerzami na Mazowszu. 
* Senesgradzki Chaos: utracił siły Esuriit w Półdarze.

# Zasługi

* mag: Anatol Sowiński, mistrz efektownych akcji i widowiskowych wojen z... czymkolwiek (tym razem: Golem Esuriit). 
* mag: Kinga Bankierz, lekko paranoicznie podeszła do Stefanii i odkryła "tam" kralotha. Też, zwalczała Golema Esuriit. Próbowała spokojem i ciszą przeważyć efektownego Anatola.
* czł: Paweł Kupiernik, planował egzorcyzmowanie świń, wpadł w ręce uczniów Świecy i wymazano mu pamięć.
* mag: Stefania Kołek, hodowała spokojnie czarnotrufle dla Eliksiru Aerinus i dla Hlangohlormo, aż uczniowie terminusów Świecy ją wyeliminowali.

# Plany

* Ewa Zajcew: przygotować coś by sprawdzić, czy Anatol Sowiński to nie "Kuba Urbanek w przebraniu".

## Frakcji

* Łowcy Dziwnych Świń: schodzą do podziemia, bo rząd współpracuje ze świniami.
* Polityka Półdary: rozpoczyna się wojna polityczna między starym burmistrzem a właścicielem Wegecud
* Rodzina Dukata: Świeca spoza Mazowsza robi akcje na terenie Mazowsza. To trzeba zatrzymać i rozwalić.
* Rodzina Dukata: Ochronić 'tame medical kraloth' przed Srebrną Świecą - bo potencjalnie nasz!

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Półdara
                    1. Mewiosie
                        1. Wegecud, wielka farma wegetariańska zniszczona energią przez Golema Esuriit.
                        1. Muzeum Słowiańskie, gdzie maskował się Hlangoglormo i który ma lekką aurę.
                    1. Las turystyczny
                        1. Parów Wtargnięcia, w którym doszło do walki z Golemem Esuriit i powstał Wieprzopomnik.
                        1. Walecznisko, pod którym znajdowały się rury transportujące magię do hodowli czarnotrufli

# Czas

* Opóźnienie: 0
* Dni: 2

# Wątki kampanii

nieistotne

# Przeciwnicy

## Podziemny 
## Golem Esuriit
### Opis

Golem zajmujący się czarnotruflami, Skażony energią Esuriit i napędzany przez nią.

### Poziom

* Trwałość: 3
* Poziom: Trudny (10)
* Siły: napędzany energią Esuriit, bardzo pancerny, bardzo silny, relentless hatred, wzbudza strach
* Słabości: widzi tylko energię życiową, stosunkowo powolny, niezgrabny

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* Sprawdzenie mechaniki 1801 jako całości
* Sprawdzenie kalibracji liczb 1801: 5-10-15 jako bazy trudności
* Zrobienie czegoś co się Dzióbkowi spodoba ;-)

## Po czym poznam sukces

* Dzióbek się będzie dobrze bawił
* Liczby dadzą dobry rezultat

## Wynik z perspektywy celu

* pełen sukces

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2/+1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

