---
layout: inwazja-konspekt
title:  "Splątane tropy: Spustoszenie?"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [150704 - Najskrytszy sekret Tamary (HB, SD)](150704-najskrytszy-sekret-tamary.html)

### Chronologiczna

* [150704 - Najskrytszy sekret Tamary (HB, SD)](150704-najskrytszy-sekret-tamary.html)

### Logiczna:

Okoliczności spotkania ze "Szlachtą":

* [140505 - Musiał zginąć, bo Maus (AW)](140505-musial-zginac-bo-maus.html)

Dziwne zachowanie Spustoszenia:

* [150224 - Wojna domowa Spustoszenia (SD, PS)](150224-wojna-domowa-spustoszenia.html)

## Misja właściwa:

### Rozdział 1: Wejście Wydziału Wewnętrznego

Marian Agrest, przełożony Wydziału Wewnętrznego Srebrnej Świecy i lord terminus honoris od dłuższego czasu próbował przekonać swoich przełożonych w Świecy, że to co dzieje się między KADEMem i Świecą jest poważnym problemem wewnętrznym Świecy. Przez dłuższy czas był blokowany i nawet teraz Świeca nie jest zainteresowana rozwiązaniem tego problemu - KADEM jest zwyczajnie za mały i za mało istotny. Agrest jednak zwrócił uwagę na to, że:
1) Pojawiło się stwierdzenie "Szlachta", które zdecydowanie już wielokrotnie wyszło w tematach powiązanych ze Świecą w jego badaniach.
2) Pojawił się temat Spustoszenia, w którym uczestniczyli magowie z kilku gildii. Nietypowe, zwłaszcza w świetle tego, że oskarżono tien Łaniewską.
3) Pojawił się Hektor Blakenbauer. Prokurator "znikąd", który jednak przebojem wbił się w tkankę śląskiej Świecy. I który szkodzi Świecy faworyzując KADEM.

Połączenie tych wszystkich faktów wygląda niepokojąco. Nie mniej niepokojące dla Agresta jest to, że lokalny przełożony Wydziału Wewnętrznego Świecy, Aleksander Sowiński, nic z tego nie zgłasza i niczego z tym nie robi. A Agrest nie ma zamiaru wbijać się przeciwko swojemu "koledze" a bez dowodów nie jest w stanie działać na cudzym terenie.
W związku z tym Marian Agrest zdecydował się na interesujący ruch. Skontaktował się z Andreą Wilgacz, czarodziejką śledczą z Wydziału Wewnętrznego operującą w Kopalinie (atm w temacie "Rodzina Świecy a ród Maus"). Poinformował ją o zastałej sytuacji i powiedział, że stracili tien Izabelę Łaniewską oraz są bliscy utraty tien Tamary Muszkiet.

Zadanie dla Andrei:
1) Dowiedzieć się, o co chodzi w temacie 'Poligonu' na KADEMie. Pomóc mu zmontować obronę Tamary Muszkiet.
2) Hektor Blakenbauer ma swoje siły specjalne, są to ludzie. Stworzyć taką sytuację, by uderzyło to w niego.
3) Rozeznać się wstępnie w sytuacji. 
4) A wszystko to tak, by Aleksander Sowiński się nie zorientował.

(Czego Agrest nie mówi: on dowodzi Saith i już powoli wykorzystuje Saith do opanowywania sytuacji i zabezpieczenia interesów Świecy przeciw koteriom)

### Rozdział 2: Rekrutacja Andrei

Andrea siedzi spokojnie w domu, wieczorem, i dostaje sygnał po hipernecie oraz impuls niskomagiczny. Zidentyfikowała impuls jako tien Ireneusz Bankierz, bardzo obowiązkowy i bardzo wyciszony terminus, siły nieregularne, atm stacjonuje w Kopalinie. Oczywiście, wpuściła. Starszy terminus, "terminus emeryt" jak czasem mówią. Ireneusz podziękował za wpuszczenie go i dał Andrei keycard. Powiedział, że jak Andrea wejdzie w Bramę, niezależnie od podanej lokalizacji, keycard przekieruje ją we właściwe miejsce. O spotkanie prosił Marian Agrest. Dotyczy spraw lokalnych.

Starszy terminus się uśmiechnął. Nie jest w stanie niczego Andrei powiedzieć, ale jeśli Andrea będzie go potrzebować, on będzie do jej dyspozycji. Atm Ireneusz Bankierz ma jakąś małą, całkowicie bezpieczną placówkę; liczne walki i Zaćmienie zrobiły mu pewną krzywdę, ale jest wystarczająco sprawny by móc dowodzić placówką silnie autonomiczną.
Niegroźny, sympatyczny, wyciszony. Innymi słowy, idealny agent Agresta.

Jeszcze tego wieczora Andrea zdecydowała się spotkać z Marianem Agrestem. Tien Agrest nie rzuca takich wezwań "ot tak". Przygotowała się tylko i przygotowała informacje o Rodzinie Świecy, po czym poszła przez SŚ do Bramy z keycard. Sprawdziła wpierw keycard czy na pewno pochodzi od Agresta, po czym uśmiechnęła się szeroko. Jej sprawdzenie wyłączyło bardzo wredną pułapkę, którą Agrest zastawił na wypadek NIE sprawdzenia i użycia. To jest najlepszy dowód, że to od niego. Jak już dotarła do Bramy, przeszła bez żadnego problemu. 

Andrea wylądowała w Bezprzestrzeni, w której znajduje się Wydział Wewnętrzny. Bezpośrednio w poczekalni przed biurem tien Mariana Agresta. Agrest zaprosił ją do pokoju i zaczął wyjaśniać. Dla odmiany, tym razem powiedział dość dużo (zwykle Agrest nie mówi nic):
- wpierw powiedział, że w 2 tygodnie Świeca traci dwóch proaktywnych terminusów: Iza Łaniewska i Tamara Muszkiet.
- Iza Łaniewska była bezpieczna; jednak wprowadzenie "dziwnego prokuratora" sprawiło, że została przekazana Diakonom którzy przekształcą ją w Diakonkę.
- Na Izę Agrest nie jest w stanie już nic poradzić.
- Tamara Muszkiet - przywódczyni "Kurtyny" (pochodna "Srebrnego Puklerza", chronią słabszych magów Świecy oraz chronią Świecę) poszła na poligon KADEMu (wtf) będąc chora na Lodowy Pocałunek.
- Będzie montowane oskarżenie przeciwko Tamarze. Hektor Blakenbauer będzie oskarżycielem.
- Antygona, Dracena oraz Izabela działały w porozumieniu. Tamara chyba też była tego częścią. I trzy pierwsze nic już Agrestowi nie powiedzą.

W związku z tym Andrea dostała następujące zadania:

1) Neutralizacja prokuratora
- Wrobić Hektora. Kupić czas i skompromitować prokuratora. Hektor ma siły specjalne, w nich są ludzie i viciniusy. Te siły specjalne mają skrzywdzić jakiś magiczny ród tak, by potem Blakenbauerowie mogli zadośćuczynić, wzmacniając ów ród. Ale ma to dać do zrozumienia Hektorowi oraz pokazać innym magom, że Hektor wcale nie jest taki "dobry".
- Agrest zaproponował ród Maus. Bo ciemny, słabszy oraz ogólnie ma taką a nie inną opinię; a jednocześnie Świeca pokaże że i Mausów chroni (win-win).
- Andrea ma pewien pomysł. Ona może współpracować z serasem Mausem i w ten sposób cel będzie perfekcyjnie osiągnięty. Więc to jej pasuje.

2) Ochrona Tamary
- Tamara jest ciężko chora na Lodowy Pocałunek. Nie jest do końca sprawna. Nie do końca można wierzyć jej pamięci.
- Agrest ukrył Tamarę w Kopalinie; nie da się jej przenieść. Z Tamarą jest zaufany lekarz oraz jest jego agent chroniący Tamarę.
- Agrest nie chce by Andrea od razu rozmawiała z Tamarą, bo im więcej czasu tym bezpieczniej dla Tamary i tym więcej sobie przypomni. 
- Andrea ma za zadanie znaleźć informacje i dowody dlaczego Tamara poszła na ten poligon. O co tu chodzi.
- Zgodnie z relacją Kermita Diakona, Tamarę ktoś chciał zabić podczas akcji odbicia.

Dodatkowe informacje:
- Dracena, Antygona, Izabela, Tamara działały z 2 gildii i może współpracowały z Powiewem i najpewniej sprowadziły Spustoszenie. Czyli kilka gildii kooperuje a Wydział Wewnętrzny nie jest zainteresowany.
- Agrest słyszał wcześniej o Szlachcie. Andrea też się z nią spotkała (misja: "Musiał zginąć bo Maus", Kornelia Modrzejewska). Prawdziwe możliwości Szlachty i ich modus operandi jest nieznany. Jeśli jednak są tak silni że Iza i Tamara stwierdziły że Spustoszenie jest opcją bez angażowania Wydziału Wewnętrznego, Agrest chce by Andrea była szczególnie ostrożna.
- To implikuje, że Aleksander Sowiński - zdaniem Agresta - jest niekompetentny... albo woli nie widzieć. Andrea musi działać w cieniu.

Marian Agrest i Andrea Wilgacz zostali połączeni tunelem hipernetowym. Mogą się komunikować i nikt tego nie przeczyta. Nikt nie będzie mógł zobaczyć jak idzie ten kanał komunikacyjny. Nieprzyjemne, wymaga pewnego zaufania, ale zostało to zrobione.

### Rozdział 3: Jak zneutralizować prokuratora

Andrea zebrała informacje na temat Mausów, na temat sił specjalnych Hektora Blakenbauera itp. Nie było żadnych problemów; dla zwykłych magów Świecy takie informacje nie są dostępne, ale Wydział Wewnętrzny zbiera i zarządza wszystkimi informacjami o wszystkich siłach znanych rodów wszystkich gildii.

Andrea następnie poprosiła o spotkanie seirasa Ernesta Mausa. Seiras Maus się zgodził.
Andrea na wejściu wyciągnęła zagłuszacz i powiedziała "polityka". Powiedziała seirasowi, że chce porozmawiać o Tymku Mausie. Ernest westchnął. Ma pewne problemy z Tymkiem. Seiras chce wydać Tymka Mausa za Galę Zajcew, co przybliży do siebie nieobrobioną, czystą energię (Zajcew) z silnymi katalistami (Maus). To daje potencjalnie cenną niszę w dziedzinie narzędzi wysokoenergetycznych, ale - jakkolwiek Gala nie ma nic przeciwko temu - Tymoteusz broni swojej niewinności jak dziewica. 

Ernest Maus i Andrea długo rozmawiali. Wymyślili następującą historyjkę:
"Powszechnie znany jest problem tego, że Rodzina Świecy nie daje dzieci rodowi Maus. To spowodowało, że pojawił się plan na linii młodszego pokolenia. Tymoteusz Maus zaczął współpracować z Galą Zajcew. Tymoteusz próbuje porywać dzieci i oddawać je Zajcewom na Syberię; te, które mają magiczny talent zostaną wzmocnione przez Syberię. Te, które nie mają tego talentu zginą i zasilą te pozostałe; Syberia zabiera i Syberia daje. Zbliża się transport dzieci na Syberię."
Przy odrobinie szczęścia siły specjalne Blakenbauerów zareagują na tą historyjkę i będą chciały dzieci odbić. Plotki że to się dzieje rozpuści Andrea tak, by to trafiło do wszystkich zainteresowanych. Więcej, serias Maus sam przygotuje porwania pewnej ilości dzieci, ale nie ma zamiaru wywozić ich na Syberię. Celem tych dzieci jest bycie odnalezionym a oficjalnym celem jest ich przebadanie celem oddania Rodzinie Świecy.

Teraz tylko trzeba zapewnić jakoś, żeby Tymek Maus lub Gala Zajcew ucierpieli od sił specjalnych. Andrea zaproponowała dodatkowe plotki - znęcanie się nad dziećmi, pedofilia, po prostu: okrucieństwo dla przyjemności. Rzeczy, które nie mają miejsca, ale nikt o tym nie wie.
Seiras Maus się zgodził. Sam wprowadzi agenta, który będzie pilnował, by siły specjalne nie zabiły ani Tymka ani Gali. Na wszelki wypadek.

Seiras Maus oczekuje od tej akcji korzyści materialnych (opłaty od Blakenbauerów) i być może korzyści na linii Zajcew - Maus.
Andrea Wilgacz oczekuje wzmocnienia sojusznika (Mausów) i jest jej to przydatne do ich celów.

Seiras Maus i Andrea Wilgacz rozstali się z uśmiechami.

### Rozdział 4: Secret Agent Marcel Bankierz

Andrea nie do końca wie, jak ruszyć ten konkretny temat. Ale nie przeszkadza jej to próbować. Znalazła w rejestrze 11 osób, które mogą coś wiedzieć o sprawie i zdecydowała się z nimi wszystkimi poumawiać.
Zaczęła od Marcela Bankierza, który został wysłany na urlop do swojego mistrza. Bez żadnego problemu dostała prawo użyć Bramy do Karego Grodu, rozległego miasteczka kontrolowanego od zawsze przez ród Bankierz.
Mistrz Sieciech był nieobecny, ale udzielił Andrei prawa do rozmowy z Marcelem. Słowa "Wydział Wewnętrzny" czynią cuda. On więcej nie chce wiedzieć.

Andrea przeszła przez Bramę do Karego Grodu, przeszła się do dworku mistrza Sieciecha. Zaskoczyła Marcela, który nie wiedział, że Wydział Wewnętrzny chce z nim rozmawiać (zgodnie z prośbą Andrei). Marcel się przeraził. Wszystko powie. Boi się Wydziału Wewnętrznego jak ognia. 

Zapytany, dlaczego jego zdaniem ona tu jest, powiedział:

Ż: O co KADEM może oskarżyć Marcela Bankierza?
B: Przy jego obecności zniknęły wrażliwe materiały - jak dostać się na poligon doświadczalny.
Ż: Czemu to, że Marcel wypuścił informację o eksplozji Infernia - Ignat na światło dzienne tak zirytowało Infernię?
D: W publicznej opinii Infernia stała się "jak każdy Zajcew" a walczyła by opinia była "że Diakon".

Marcel powiedział, że przy jego obecności zniknęły materiały, ale to nie był on. Mistrz Sieciech osobiście kazał sprawdzić głęboki skan. To nie był on. On tego nie wyniósł. KADEM podejrzewa inaczej, ale to nie był on. Andrea będzie chciała ten skan; Sieciech jej go odda (kopię).

Ż: Jaką ważną informację (obszar) może dostarczyć Marcel Bankierz, być może całkiem przypadkowo?
D: Wie, w jaki sposób poligon KADEMu został spenetrowany; wie, w jaki sposób do tego doszło.

Marcel zawsze specjalizował się w zabezpieczeniach. Zgodnie z jego wiedzą, to było połączenie grupy różnych czynników; trochę Ignat próbował wprowadzić miejsce na ukryte śledziki, coś zaniosła Alisa bo była poproszona, ktoś coś komuś powiedział i w wyniku pierwszego opuszczenia poligonu przez maga KADEMu Rojowca powstała niewielka luka.

Marcel się przyznał. On powiedział o tej luce. On wiedział o tej luce. On nie musiał wynosić materiałów; on powiedział o tej luce. Nie, nie KADEMowi. Andrea przydusiła - komu. Nie chce powiedzieć. Przydusiła mocniej (11v8). Marcel powiedział, że powiedział swojej przełożonej, Tamarze. Skłamał, lub nie powiedział pełni prawdy.

Andrea powiedziała mu, że nie tacy próbowali ją okłamywać i zagroziła zmianą z wolnej stopy na przesłuchanie. Pękł. Powiedział, że wpierw spytał go o to Salazar Bankierz a później - dużo później - spytała go o to Tamara. I wtedy jej hipernet był bardzo słaby, z zakłóceniami (dla Andrei "słaby z zakłóceniami" oznacza, że Lodowy Pocałunek już działał; po złapaniu chronologii powiązała to z wiadomością bezpośrednio przed atakiem na poligon KADEMu). 

Marcel, powiedziawszy to, pękł. Powiedział coś jeszcze. Tamara odesłała go na KADEM, bo Marcel miał... problemy. Zrobił sobie wrogów w okolicy; poszło o pewną dziewczynę. Tamara zatem go odesłała na KADEM, by przycupnął i by nic złego mu się nie stało i by on nic złego i głupiego nie zrobił. Pewien czas później skontaktował się z nim tien Aurel Czarko, starszy rangą terminus i powiedział, że z pewnością jest wejście na KADEM, na Poligon. On nie wie jak się tam dostać, ale KADEM nie słynie z dyskrecji ani z rygorystycznych procedur. Aurel kazał Marcelowi nic nie mówić przełożonym (Tamarze) i nie robić niczego głupiego. Kazał jednak zbierać informacje, zbierać plotki i kontaktować się z Aurelem. Marcel zbierał plotki i informacje a Aurel składał je do kupy i wspólnie wyprowadzili jak można dostać się na Poligon. Nie było 'proof of concept'. I nagle kontaktuje się z Marcelem Salazar i mówi, że wie że Marcel nad tym pracuje i on musi wiedzieć, bo ktoś zginie. Salazar był zdesperowany. A potem... Tamara.

Zdaniem Andrei Marcel naczytał się za dużo powieści przygodowych i szpiegowskich. Lepiej by terminusił potwory niż magów. Marcel skwapliwie się z tym zgodził.

Andrea sprawdziła Aurela Czarko. Terminus, dość długo robi swoje, zupełnie nie z Kopalina ("gdzieś daleko"), w ogóle nie ten rewir i w jakiejś dziurze. Dość ambitny, wielokrotnie chciał przeniesienia na lepszą placówkę, ale zawsze mu odmawiano. Jest to "pistolet", wykazuje inicjatywę w ten niepożądany sposób, plus jest po prostu kiepskim szefem i kiepskim terminusem liniowym. Jest niezłym "szpiegiem" i niezły w takie klocki, ale brakuje mu dyskrecji. I ma kompleks "należy mi się więcej". Zdaniem Andrei - wystarczyłoby, że ktoś przyjdzie i poklepie po pleckach i Aurel mógłby to zrobić...

Andrea zauważyła jedną rzecz - Aurel mógłby stać za operacją, ale musiał mieć pomoc. Ktoś lokalny. Ktoś, kto się orientuje. Ktoś, kto wiedział, że Marcel został wysłany. Sęk w tym, że Tamara wysłała go oficjalnie. Czyli Aurel MÓGŁBY teoretycznie sam to znaleźć i samemu się zainteresować. Andrea wie, że tak nie mogło być (profil psychologiczny). Ale nie da się tego udowodnić a Aurel będzie się zarzekał, bo w ten sposób pokaże jaki jest ważny i przydatny.
Super...

Andrea połączyła się z Agrestem i poprosiła go o monitorowanie komunikacji Marcela Bankierza. Interesuje ją czy po jej wyjściu Marcel spróbuje się z kimś skontaktować. Zwłaszcza z Aurelem Czarko.

### Rozdział 5: Spustoszenie według Zenona.

Kolejnym interesującym Andreę krokiem był Zenon Weiner, ekspert Świecy w dziedzinie Spustoszenia który sam został Spustoszony. Zenon Weiner znajduje się w Pracowniach Inżynierii Technomantycznej w Kopalinie.

45-letni Zenon Weiner zareagował na obecność Andrei z uwagą i uśmiechem. Zdziwiło go, że pojawił się mag Wydziału Wewnętrznego, ale nie jest szczególnie zaniepokojony. Zapytany o Koty, zasępił się. Powiedział Andrei to, o co wcześniej pytała Siluria:

"
Zenon i Marian, zapytani, powiedzieli że to Marian poprosił Izę Łaniewską o pomoc. Nie miał większej nadziei, bo Iza od czasu śmierci Kaliny niezbyt pomagała Powiewowi Świeżości (dokładniej: pomoże jeśli ma coś zniszczyć, nie pomoże jeśli nie ma czegoś natychmiast do zniszczenia) ale Iza się zgodziła. Marian powiedział, że Iza "i tak była w okolicy".

Zapytany o co chodzi i skąd wzięło się Spustoszenie, Zenon wyjaśnił: skontaktował się z nim Marian Welkrat i powiedział, że doszło do aktywacji wskaźników Spustoszenia w Powiewie Świeżości i Marian się zmartwił. Poprosił Zenona o to, by ten pomógł Marianowi zlokalizować co się stało i czy gdzieś jest kontroler. Zenon wziął odpowiednio wykastrowane Spustoszenie i poszedł z Marianem za śladem Spustoszenia. Znalazł ślad w Kotach; tam doszło do rzucenia tajemniczego zaklęcia przez Netherię, rezonansu i w wyniku uaktywniło się Spustoszenie Zenona. Nie mogło połączyć się z magiem ani człowiekiem, lecz zagnieździło się w miślęgu. To co ważne; aktywacja nastąpiła przez INNY kontroler Spustoszenia. Niekoniecznie kompatybilny.

Zenon Weiner powiedział też o tym, że Spustoszenie uczy się od siebie; są "szczepy" Spustoszenia. W SŚ jest Spustoszenie które posiada opcję amnezji; Spustosza maga, łączy się z pamięcią i usuwa określoną pamięć po czym można owo Spustoszenie usunąć. Aurelia zmieniła "swój szczep" Spustoszenia przez dodanie niekompatybilności z innymi szczepami Spustoszenia oraz przez dodanie opcji wyłączenia Spustoszenia. Niestety, nie udało jej się wiele więcej zdziałać.

Zapytany o Aurelię, Zenon powiedział, że Aurelii w pracy ze Spustoszeniem pomagał Powiew; z Powiewem niektórzy magowie Srebrnej Świecy. Ale Aurelia to była Aurelia, tylko ona miała wiedzę powiązaną z całością problematyki Spustoszenia. Nie chciała się tym dzielić; uznała to za zbyt niebezpieczne. Zenon nie wie, by wielu innych magów rodu Maus interesowało się problematyką Spustoszenia.

Zenon praktycznie potwierdził, że były dwa kontrolery Spustoszenia. A jednym z nich był miślęg kontrolowanym przez astralnego kralotha. Drugi - dalej nieznany."

Dodał, że drugi kontroler Spustoszenia jest nieznany. Zapytany przez Andreę, odparł, że to wszystko opowiedział magom KADEMu. Nic przed KADEMem nie ukrywał. Zenon nie wie jakie były motywy Izy Łaniewskiej.
Andrea (terminus śledczy) przeszła krok po kroku przez sytuację z Zenonem. Zadawała precyzyjne pytania. Wyprowadziła, że Iza była w pobliżu, ale cała sprawa ze Spustoszeniem faktycznie Izie musiała być znana. Nie wiadomo, czy zaaranżowała ich opuszczenie Powiewu (chyba nie); patrząc na to gdzie Iza ich prowadziła i jak działała Iza chciała ich odciągnąć od prawdziwej lokalizacji Spustoszenia którego szukali. I wtedy doszło do aktywacji. Iza wycofała się a oni zostali Spustoszeni. 

Czyli Iza wiedziała gdzie jest Spustoszenie i nie chciała by oni tam byli. Ciekawe jest to, że tego miejsca nie sprawdzała potem ani Iza, ani Tamara, ani Jadwiga. To miejsce od którego odciągała Iza oficjalnie nikt nie sprawdzał. Bo Iza go nie zaraportowała.
...Andrea nie jest w stanie tego znaleźć, bo ona też nie zna tego obszaru, nie wie gdzie to dokładnie było...

(ważna rzecz, Zenon nie ma żadnych informacji o kościele w Kotach czy czymkolwiek związanym z Marysią)

Zenon dodał Andrei coś jeszcze. Przeprowadził kilka badań, poszukał, poszperał w raportach (tu wielka zasługa informacji od Jadwigi Karnisz i od magów KADEMu, przede wszystkim Silurii) i z tego wynika jednoznacznie coś bardzo niepokojącego: Spustoszenie na wolności ma jednego kontrolera. Kontrolera, który jest sterowany przez jakiś Overmind. Overmind, który nie jest Overmindem Aurelii (na KADEMie) ani Overmindem w Świecy (bo Świeca nie ma Overminda). Czyli jest tam albo silnie autonomiczny kontroler albo nawet Overmind.
Jak długo w Powiewie znajdował się jeden Overmind, mógł interferować z tym odległym. Ale aktualnie Overmind Aurelii znajduje się na KADEMie, więc nie ma problemu z interferencją.

Patrząc jak zachowywały się drony w Kotach, jak działały (grupa niewidzialnych Spustoszonych, pułapki...) najpewniej gdzieś jest Spustoszony mag oraz Kontroler Spustoszenia (niekoniecznie w tej samej osobie). I to jest problem. Bo mamy do czynienia z czymś bardzo inteligentnym, cierpliwym i najpewniej nie bazowanym na człowieku lub magu. Aurelia nie potrafiła sprawić, by Spustoszenie nie zdominowało swoim programowaniem, ale jak pokazuje działanie z cieniem kralotha (echo Lugardhaira) tam Spustoszenie było posłuszne działaniom pryzmatycznym kralotha.

Z tego wynika - zdaniem Zenona Weinera - że aktualnym kontrolerem Spustoszenia jest vicinius o bardzo silnej woli i "one purpose". A my nie znamy ani celu ani nie mamy pomysłu gdzie ten vicinius może się znajdować lub czego może chcieć.

Zenon poprosił Andreę o możliwość ostrzeżenia KADEMu. O poinformowanie magów KADEMu, że może być tam inteligentny kontroler Spustoszenia. Vicinius o jednej idei. Problem polega na tym, że jeśli Zenon powie, że da się zbudować Spustoszenie kierowane, które skupia się na jednym celu to wtedy Spustoszenie zmienia się w straszliwą, kierowaną broń. 

Andrea udziela Zenonowi zielonego światła. Może powiedzieć to KADEMowi. I tak KADEM ma Overmind i i tak do tego dojdzie. A lepiej ich ostrzec. Andrea kazała powiedzieć bezpośrednio Marcie Szysznickiej i poprosić o prawo badania Overminda na KADEMie, by wiedzieli z czym mają do czynienia.

Andrea jeszcze spytała Zenona o Tamarę. Zenon się uśmiechnął, Tamara to dobra dziewczyna. Bardzo pracowita. On pomagał Tamarze budować jej magitechowe kombinezony; dwa lata to mniej więcej zajęło by prawidłowo dobrać źródło energii oraz odpowiedni materiał na magitechowy pancerz. Tamara - zgodnie z opisem Zenona - pracowała nad magitechowymi kombinezonami przez lata, łącznie z różnymi magami EAM i nie EAM. Jej kombinezony to po prostu arcydzieło. Zdaniem Zenona Tamara marnuje się jako terminus, powinna skupić się na inżynierii magicznej.

Andrea zostawiła Zenona z ciężkim sercem i jeszcze ciemniejszymi myślami.

### Rozdział 6: ratowanie Tamary według Kermita

Z Kermitem Andrea umówiła się w koszarach Kompleksu Świecy, czekając aż Estrella wróci. Zaprosił Andreę do swojej kwatery w koszarach.
Powiedział Andrei o tym tajemniczym "jedenastym terminusie" i tego, że ktoś chciał zabić Tamarę. Zdaniem Kermita to był ten jedenasty terminus. Agrest, z którym skontaktowała się Andrea uśmiechnął się i powiedział, że jedenasty terminus był jego agentem; to ktoś z tych dziesięciu chciał zabić Tamarę i on (Agrest) się tym zajmuje. Jak już będzie wiedział, wtedy postanowi co dalej.
Niestety, Kermit nie wiedział niczego przydatnego dla Andrei.

### Rozdział 7: Estrella Diakon, świadek, którego nikt nie pytał.

Estrella wróciła i się ogarnęła. Andrea dostała pinga i zaproszenie do kwatery Estrelli.
Kwatera wygląda jak buduar, nie kwatera terminuski. Tyle, że wszędzie jest COŚ co można użyć jako broń. Andrea doceniła.

Andrea zapytała Estrellę o to, co zdarzyło się w Kotach, gdy była Spustoszona. Estrella odpowiedziała. Ale Andrea zauważyła, że to nie może być prawda. Jednak Estrella wierzy, że jest. Andrea wyciągnęła informacje które podał Zenon o Spustoszeniu modyfikującym pamięć. Estrella zbladła. Andrea zaproponowała zaklęcie - rzuci czar na Estrellę PLUS zasymuluje ból itp, by przeforsować programowanie Spustoszenia. Lub kogokolwiek innego. Wiedząc, że stawką jest potencjalna wojna KADEM - Świeca i los Tamary, Estrella się zgodziła. (stopień przeciwnika: 9 - 15, Andrea: 13 z magią, 15v15)

Estrella przypomniała sobie, co się tam działo i o co chodziło. Poszła do miejsca, gdzie zginął Lugardhair z Netherią; Netheria nie chciała nikogo z Millennium. Czemu: są frakcje w Millennium, które są bardzo zainteresowane zwiększeniem mocy za wszelką cenę. Do tych frakcji często należą niektóre kralothy. Przy miejscu śmierci Lugardhaira znajduje się pryzmatyczna informacja o pierwszym sojuszu Lugardhaira i mistrza Świecy Astera. To jest wiadomość, którą Netheria chciała zniszczyć a Estrella była poproszona jako wsparcie ogniowe i zabezpieczenie.

Słusznie. Jeszcze w pewnej odległości od kopca Estrella (terminuska) wykryła Antygonę Diakon, Dracenę Diakon, Izabelę Łaniewską oraz Wiktora Sowińskiego z Oktawianem Mausem. Wiktor Sowiński dowodził. Izabela była w ukryciu. Zenona i Mariana nie było nigdzie w pobliżu.
Estrella nawiązała kontakt z Izą i dostała "wszystko jest pod kontrolą". Z Antygoną i ta powiedziała "nie komunikuj się ze mną!". Odezwała się Dracena i powiedziała "tak musimy zrobić; nie przeszkadzaj sobie, akcja jest we współpracy z terminusem."
Estrella nie miała czasu na analizy, bo przy kopcu był mag. Olaf. Mag, którego nawet na KADEMie nie chcieli - i on czerpał wiedzę i moc z kopca. Netheria, Estrella i Olaf weszli w rytuały i zaklęcia. Walka tej trójki zdecydowanie zwiększyła poziom energii magicznej w tym miejscu...

Tymczasem Wiktor Sowiński rzucił zaklęcie na Antygonę i został Spustoszony - Antygona była kontrolerem Spustoszenia. Sama się zmieniła w kontroler, ale kontroler Aurelii nie miał Overminda, więc Antygona była autonomiczna i to ona mogłaby wpłynąć na pamięć Wiktora Sowińskiego. Wiktor Spustoszył Oktawiana Mausa i wtedy Coś Się Stało.
Nie wiadomo, czy to przez Spustoszenie Wiktora, Oktawiana, czy przez jakiś rezonans magiczny między Netherią i Olafem. To co istotne - uruchomiło się Spustoszenie znajdujące się w kopcu miślęgów i pochłonęło zarówno Olafa, Netherię jak i Estrellę. Potem jednak zadziałało Coś Jeszcze. Inny kontroler. Coś, co przyszło od Antygony. Coś, co kontrolowało zarówno Antygonę jak i nadpisało kontroler nad Olafem, Netherią i Estrellą. Niesamowita moc, Estrella nigdy nie czuła niczego takiego...

Z tego wynika, że Marian i Zenon oberwali później od Spustoszenia, ale oni i Iza myśleli że poszli razem... piekielnie dobra kontrola i dobry plan. Ten Overmind, czymkolwiek on jest, potrafi bardzo dobrze planować.

Gdyby nie to, że Lugardhair się wydzielił jako autonomiczny kontroler Spustoszenia, nikt by niczego się nie dowiedział...

Andrea zostawiła Estrellę we krwi, z przepalonymi kanałami magicznymi i wymagającą pomocy medycznej. Wezwała medyków i jak tylko pojawili się przerażeni lekarze, Andrea powiedziała "Wydział Wewnętrzny" i wyszła.

Agrest byłby zadowolony.

# Streszczenie

Agrest nie ma jurysdykcji w Kopalinie, ale poprosił Andreę, by sprawdziła co tu się dzieje - potencjalnie szef Wydziału w Kopalinie jest niekompetentny lub zdrajcą (Aleksander Sowiński). By zneutralizować Hektora (chronić Tamarę przed losem Izy) Andrea współpracowała z seirasem Mausem; stworzyli fałszywą acz wiarygodną plotkę by Hektor wpadł w pułapkę i został skompromitowany (Tymek & Gala). Marcel Bankierz przyznał się, że przed atakiem z Aurelem Czarko (jego pomysł) znalazł lukę zabezpieczeń na KADEMie (Poligonie) i podał ją Salazarowi a potem Tamarze. Potem Zenon Weiner powiedział Andrei, że były dwa kontrolery Spustoszenia i że Iza wiedziała od początku o Spustoszeniu. Tym drugim. Też Zenon ostrzegł, że mogą mieć do czynienia z Overmindem Spustoszenia - a aktualny blokujący Overmind (Aurelii) jest w lockdownie na KADEMie i nie blokuje. Zdaniem Zenona, aktualnym kontrolerem (Overmindem?) Spustoszenia jest vicinius o bardzo silnej woli i "one purpose" - inaczej nie byłby w stanie kontrolować a byłby podległy. Dzięki bardzo brutalnemu przez przeciążenie energii (za zgodą) przesłuchaniu Estrelli, Andrea dowiedziała się też, że Antygona, Dracena, Oktawian, Iza i Wiktor byli w jednym miejscu gdy doszło do Spustoszenia; wtedy też zarezonowało to w kopcu i Spustoszyło Estrellę, Netherię i Olafa. Ale był sygnał kontrolujący Antygonę. Coś "z zewnątrz". Z tego wynika, że Marian i Zenon oberwali później od Spustoszenia, ale oni i Iza myśleli że poszli razem - tajemniczy "trzeci" Overmind (potencjalny vicinius) wszystko genialnie zsynchronizował.

# Lokalizacje:

1. Świat
    1. Faza Daemonica
        1. Świeca Daemonica
            1. Krąg wewnętrzny
                1. Wydział Wewnętrzny
                    1. Biuro Mariana Agresta
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Koszary terminusów
                            1. Pion badawczo-produkcyjny
                                1. Pracownie Inżynierii Technomantycznej
                            1. Pion logistyczny
                                1. Brama
                            1. Pion ogólnodostępny
                                1. Sala spotkaniowa
        1. Wielkopolska
            1. Powiat Bogacki
                1. Kary Gród
                    1. Obrzeża
                        1. Dworek Sieciecha Bankierza
                            1. Ogród Tęczowy
               
# Zasługi

* mag: Andrea Wilgacz, inspektor śledczy Wydziału Wewnętrznego Srebrnej Świecy, przesunięta z tematu "Rodzina Świecy" na temat "Tamara Muszkiet".
* mag: Marian Agrest, przełożony Wydziału Wewnętrznego Srebrnej Świecy spoza Śląska, który pcha do tego, by KADEM - SŚ wyeskalowało do sprawy dla Wydziału.
* mag: Aleksander Sowiński, przełożony Wydziału Wewnętrznego Srebrnej Świecy ze Śląska, który nie interesuje się tematem KADEM - SŚ ani Szlachtą.
* mag: Ireneusz Bankierz, starszy (62 lata) terminus-emeryt, osłabiony przez Zaćmienie i mający bezpieczną placówkę. Agent Agresta. Z nudy.
* mag: Ernest Maus, seiras rodu. Konspiruje z Andreą celem neutralizacji Hektora Blakenbauera (żeby nie skrzywdził Tamary prokuratorsko).
* mag: Tymoteusz Maus, młody, naiwny i buntowniczy; seiras chce go wydać za tien Galę Zajcew. Seiras ma z nim kłopoty...
* mag: Gala Zajcew, która nie ma nic przeciw małżeństwu z Tymkiem Mausem; ponadto nic o niej jeszcze nie wiadomo.
* mag: Sieciech Bankierz, mistrz Marcela i Augusta Bankierzy, mistrz * magii i posiadacz dworku w Karym Grodzie. Chroni Marcela Bankierza.
* mag: Aurel Czarko, doświadczony terminus SŚ, który z Marcelem opracował teoretyczne wejście na poligon KADEMu. Winny ataku na KADEM, ale figurant.
* mag: Marcel Bankierz, który jest niewinny wyniesienia materiałów z KADEMu ale winny wycieku informacji jak wejść na Poligon KADEMu. Naiwny jak cholera.
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, jej działania były szeroko rozpatrywane przez Wydział Wewnętrzny. Najpewniej ulegnie przekształceniu w Diakonkę.
* mag: Tamara Muszkiet, oglądana ze wszystkich stron przez Andreę (przesłuchania innych). Leży chora pod opieką Agresta w Kopalinie i nikt nie wie gdzie jest.
* mag: Zenon Weiner, który już doszedł do siebie i ostrzegł Andreę przed inteligentnym, aktywnym kontrolerem Spustoszenia o którym wszyscy "zapomnieli". 
* mag: Kermit Diakon, który nie wiedział niczego przydatnego dla Andrei ale wierzy, że "jedenasty terminus" był tym co miał zabić Tamarę.
* mag: Estrella Diakon, której Spustoszenie zniszczyło pamięć i która zgodziła się na wypalenie kanałów i cierpienie by sobie przypomnieć.
* mag: Netheria Diakon, która postawiła się kralothom oraz części rodu, by uniemożliwić sojusze takie jak Lugardhair - Aster.
* mag: Wiktor Sowiński, wysoki rangą młody * mag, który (historycznie) został Spustoszony przez Antygonę - kontrolera. 
* mag: Oktawian Maus, który też był Spustoszony... ale już nie jest. Coraz bardziej tajemniczy.
* mag: Antygona Diakon, okazała się faktycznie kiedyś być kontrolerem Spustoszenia. Sama sobie to zrobiła by "wymazać IM pamięć".
* mag: Dracena Diakon, która wspierała siostrę na dobre i na złe (tym razem 'na złe').
* mag: Salazar Bankierz, był jakoś powiązany z wciągnięciem Tamary na Poligon KADEMu. Andrea jeszcze z nim nie skończyła...