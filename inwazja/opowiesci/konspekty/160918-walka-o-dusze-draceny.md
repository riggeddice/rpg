---
layout: inwazja-konspekt
title:  "Walka o duszę Draceny"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160910 - Na żywym organiźmie Draceny... (PT)](160910-na-zywym-organizmie-draceny.html)

### Chronologiczna

* [160910 - Na żywym organiźmie Draceny... (PT)](160910-na-zywym-organizmie-draceny.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Dracena jest w stanie psychozy po destabilizacji post-kralotycznej, w BONDAGE i w ogóle.
Aktualny stan Draceny: (11 do wydania), Diakonka 6 (musi być 8), Wiła 3, Spustoszenie 3, Cybergothka 5, Strach 3, Weapon 1
Dracena ma 3 dni na stabilizację dzięki działaniom Kingi.

## Misja właściwa:

Wcześniej:

Netheria zaproponowała Paulinie, że może się udać powiązać astralnie Dracenę z wypromieniowywaną energią. I uniknąć psychozy. Dracena i tak wypromieniuje energię - Netheria chce po prostu do tego dodać jeszcze Spiritus który ona może zapewnić. Paulina nie chce, by jakiemukolwiek człowiekowi stała się krzywda; Netheria oczywiście też nie. Ta energia jest jak wybuch bomby. Paulina ma pomysł - postawmy bariery dookoła Draceny. Przesuńmy energię "w fazie". Zróbmy coś, by ta energia zniknęła.
Netheria powiedziała, że można spróbować, choć jest nieco sceptyczna. 

Konflikt idzie o to, by energia nie skrzywdziła żadnego człowieka przez emisję: kataliza vs 25. (z magią 15). Netheria przygotowała rzeczy służące do wzmacniania (+3). Kinga zaproponowała stopniowe uwalnianie energii by - co prawda Dracena strasznie cierpiała i to ją osłabi - ale móc chronić ludzi. Paulina się zgodziła (+3). 22 v 25. Plus Netheria i Kinga wchodzą i wspierają Paulinę. 24 v 25.

Dracena cierpi jak cholera. Jednak otworzyła się opcja by zacząć ją łatać magicznie; Paulina znalazła przy użyciu astraliki Netherii echa tego, czym Dracena była, wspomnień; bez ceregieli zaczęła ją łatać. (nadal 24 v 25). Niestety, energia zaczęła spiralować poza kontrolę; wybuch magiczny ponad budynkiem, wzbudzający powszechnie ludzką uwagę. Co gorsza, energia zaczęła płonąć też wewnętrznie, aktywując Spustoszenie i wzmacniając link Draceny z Karradraelem...

Paulina nacisnęła mocniej, zacisnęła zęby i wszystkie czarodziejki dały radę ustabilizować Dracenę i pozbyć się psychozy...
SUKCES!

Aktualny stan Draceny: (9 do wydania), Diakonka 6 (musi być 8), Wiła 3, Spustoszenie 4, Cybergothka 6, Strach 3, Weapon 1

Z nadmiarowej energii sformowała się efemeryda - "Najada Diakon".

Dzień 1:

Kinga zgłosiła Paulinie i Netherii, że się udało. Dracena ma 3 dni na stabilizację zanim całkowicie się rozsypie. Netheria powiedziała, że najważniejsze jest zmienić Dracenę w Diakonkę - to jest najbardziej kluczowy komponent tego wszystkiego.

Netheria przygotowuje się do organizacji imprezy by nie było, że ten wybuch to coś dziwnego. Przyszedł jednak do domku policjant. W czasie, gdy Neti była na mieście (bo impreza). Przyszedł z nakazem do Pauliny; szuka materiałów wybuchowych. A Dracena nieprzytomna, w złym stanie, w bondage w środku...
Paulina zaczęła go bajerować, że ma córkę ważnego polityka i dlatego jest związana i w bondage...
Policjant, o dziwo, został przekonany. Nie stwierdził, że to jakaś super poważna sprawa. Woli nie wnikać... jest tu dla materiałów, przejrzał, nie ma materiałów. Poszedł.

Paulinę bardzo zaskoczyła prędkość reakcji policjanta. Dlaczego policjant tak szybko przyjechał...?
Zdecydowała się wypytać policjanta - jak to się stało że dostał nakaz? Przecież to było super szybko... wrogowie tego polityka?
Policjant powiedział, że Paulina wie... dostał telefon z góry i dostał wydrukowany nakaz. To były naciski z góry, ktoś wiedział o córce narkomance.
...tyle, że Paulina naprawdę jest teraz zdziwiona...

Paulina zdecydowała się porozmawiać z Jerzym Pasznikiem, "oswojonym" terminusem Świecy. Kinga zostaje na straży Draceny, Paulina poszła porozmawiać z Jerzym...

Terminus cały zaskoczony. Czyżby jednak działała tu magia? Ktoś robi coś złego? Sprawdzi to - nie ma nic lepszego do roboty tak czy inaczej. A może przynajmniej da radę się jakoś pozytywnie wykazać ;-).

Paulina wróciła. Dracena już nie śpi. Jest w więzach... i nie podoba jej się to. 
Badanie katalityczne Draceny przez Paulinę i Kingę. Komponenty Spustoszenia są rozjarzone; żyją. Nie Pustoszą, ale... są silniejsze niż kiedykolwiek.

"Czuję się jak po ogromnej orgii z paskudnymi narzędziami na jaką ZUPEŁNIE nie miałam ochotę. I jeszcze te głupie pytania... >.>" - Dracena, samodiagnostyka
"Miałaś kiedyś wrażenie, że telefon i drukarka są częściami Twojego ciała? Bo ja tak... teraz..." - Dracena, samodiagnostyka

Tak. To Didi. Przynajmniej tyle.

Paulinę martwi to rozjarzenie... do tej pory Spustoszenie było nieaktywne. Ale jest o niebo lepiej niż było. To się liczy.

Kinga zaproponowała Paulinie, żeby teraz skupiły się na korekcji Dracenowego wzoru, skoro pacjentka jest w dość dobrej formie. Dracena się entuzjastycznie zgodziła. Paulina, nieco mniej entuzjastycznie, też. Praca nad wzorem Draceny jest szczególnie trudna (nieudany test -> przyjdzie policjant bo Najada, nieudany test -> terminus jest święcie przekonany że Dracena za tym stoi, nieudany test -> Najada dostanie możliwość zabindowania Draceny). Ale się udało.

Wróciła Netheria, wieczorem. Powiedziała Paulinie, że następnego dnia będzie od popołudnia do rana epicka cybergoth impreza. Dracenie zaświeciły się oczy.
Przyszedł terminus. Powiedział, że Dracena stoi za zaklęciami na telefon i na drukarkę. To ona przygotowała i zrobiła to wszystko, to ona ściągnęła policję. Paulina ma pewne wątpliwości co do poprawności interpretacji tego terminusa... on jednak uważa, że Dracena po prostu jest małą attention whore. Najlepszy dowód? Cybergothka...

Ku wielkiemu protestowi Draceny, Diakonka noc spędziła w bondage. Na wszelki wypadek.

Dzień 2:

Przyszedł znowu Włodzimierz Jamnik (policjant) i powiedział, że musi zaprowadzić Dracenę na komisariat. Zrobiła coś bardzo złego i są świadkowie. Paulina zaczyna mówić, że przecież to może jej zaszkodzić - Jamnik na to, że przestępstwa seksualne wobec dzieci nie będą zamiecione pod dywan. Paulina... niechętnie, ale zgodziła się oddać Dracenę. Pojedzie z nią. Netheria nie może interweniować - robi imprezę. A Kinga nie ma pojęcia jak, nawet, jak by chciała...

Dracena zaproponowała że ona pojedzie samochodem z policjantem i jemu padnie silnik. A Paulina pogada z Netherią.
Paulina nie wierzy że policjant zabierze ją na komisariat; niech Kinga pójdzie do terminusa i ten ma się zająć pilnowaniem Draceny a Paulina jedzie z Draceną po poinformowaniu Netherii o problemie "Dracena nie ma dokumentów".

Ku swojemu zaskoczeniu, Dracena rzuciła... inne zaklęcie niż planowała. Zaklęcie, które miało rozbić ich na drzewie. Paulina błyskawicznie wpłynęła na ten czar; nie może tak być. Zamiast wbić się w drzewo, wjechali w rów.
Policjant w szoku, Dracena wstrząśnięta. Przyjechał inny policjant (Jakub Suwaczek) innym wozem, zabrać ich do Czarnoskała na komendę.

Dracena jest w ogromnym szoku... Paulina ją pociesza, próbuje uspokoić - po to tu jest - acz nie jest to proste. Nie mogą swobodnie rozmawiać, ale to nie ma znaczenia. Paulinie udało się przekonać Dracenę i trochę ją uspokoić. A potem - komenda...

Na komendzie Dracena i Paulina siedzą przed lustrem weneckim. Chodzi o rozpoznanie. Siedzi tam kilka podobnych dziewczyn (yeah, right... jedna Diakonka...) i policjant kazał im patrzeć w lustro. W czym Paulina... wyczuła, że Dracena zaczyna rezonować z lustrem. Strasznie silna energia, w pomieszczeniu jest pryzmat Rozpaczy i lustro to wchłonęło. (21). Za duża siła dla Pauliny (ma 18). Próbowała! Naprawdę! Ale rezonans Draceny jest za silny; szklana ściana się rozsypała - lustro nie wytrzymało (impurities) energii magicznej. Policjanci wpadają ogólnie, co tu się dzieje - a Paulina widzi młodego chłopca (13 lat?) pokazującego Dracenę palcem i krzyczącego "to ona".

Paulina rzuciła nań jeszcze jeden czar - sprawdzić aurę magiczną. I wykryła. Aurę magiczną Draceny. Ale inną - to jest aura Draceny skoncentrowana, to nie jest czar; to jest emanacja. Manifestacja. Efemeryda. Węzeł.

"Czyli to jest to czego ten ***owy terminus nie dał rady zauważyć... tak bardzo się wykaże..." - Paulina.

Czyli mamy EFEMERYDĘ Draceny. Efemerydę, która jest jej wroga. Super...

Czas użyć kontaktów. Paulina zadzwoniła do Krystiana Korzunio. Komisariat Trzeci w Czarnoskale; ciężko chora Diakonka, oskarżona o nierząd na nieletnim, efemeryda a nie ona. Potrzebna Paulinie ludzka strona. Korzunio obiecał Paulinie, że nie będzie mieć żadnych problemów. Zajmie się tym aspektem problemu. A jakby on potrzebował pomocy do leczenia ludzi... Paulina się skwapliwie zgodziła.

Dotarły dokumenty od Netherii. Dotarł telefon Korzunia. Włodzimierz Jamnik poprosił Paulinę do siebie. Jest wściekły. Powiedział, że pacjentka Pauliny będzie zwolniona, ale on to eskaluje do góry. Posprzeczali się; Jamnik powiedział, że faktycznie ma solidnego polityka. Jak Paulina może spać w nocy wspierając kogoś kto molestuje dzieci? Na to Paulina, że to NIE Dracena. Ogólnie, rozstali się. I Dracena poszła z Pauliną. Niestety, jest już popołudnie.

Paulina i Netheria mają krótką rozmowę o imprezie. Tak, jest to najlepsza opcja. Ale... jak to zrobić? Neti powiedziała, że impreza jest od 18 lat, będą pilnować, sami dorośli ludzie i Paulina + Kinga pilnują magii Draceny. A Paulina poszła porozmawiać z terminusem... o rozwaleniu efemerydy.
Terminus się zdziwił, że moc Draceny była wystarczająca by stworzyć efemerydę. Paulina powiedziało, że to wyglądało jak aura Draceny, ale nie było aurą Draceny. Terminus spytał, czy może zobaczyć zapis wzoru Draceny; chce zobaczyć czego szuka. Paulina spytała Kingi i ta się zgodziła.

Jerzy połączył się z Draceną by sprawdzić jej wzór. Efemeryda odpowiedziała skanując wzór Jerzego - wie teraz jak smakuje terminus i jak go szukać. Terminus wie jak znaleźć efemerydę.

Jeszcze jeden telefon ze strony Pauliny do Korzunia. Powiedziała, że jest jeszcze jeden problem - organizują imprezę i chcieliby by nie było tam małolatów ani żadnych problemów. Żadnych narkotyków, problemów etc. Korzunio spytał, czy rozmawiała z lokalnymi szefami podziemia. Powiedziała, że nic jej o tym nie wiadomo - ale że zapłaci Korzuniowi. Ten się uśmiechnął i powiedział, by Paulina niczym się już nie przejmowała. Co prawda to dzisiaj (za 2h), ale sprawa będzie załatwiona.

Impreza.

Dzięki pracy Netherii i Korzunia impreza była w miarę bezproblemowa. Dzięki pracy Kingi i Pauliny, moc Draceny była właściwie katalizowana. A smętny terminus polował na nie mniej smętną efemerydę...
A dokładniej, efemeryda użyła technomancji by zwabić terminusa po czym go uwiodła i dostosowała się do jego wzoru...

Dracena świetnie się bawiła na imprezie. Świetnie! Taniec, DJ Dracena, impreza - karaoke, obłapianie, seks, dalej taniec, w sumie to nie śpię bo jestem Spustoszona, więcej tańca...
Paulina i Kinga są wyczerpane...
Dracena też. Ale jak jest szczęśliwa!

Aktualny stan Draceny: (4 do wydania), Diakonka 7 (musi być 8), Wiła 4, Spustoszenie 5, Cybergothka 8, Strach 3, Weapon 1

Dzień 3:

Rankiem Paulina dostała telefon.

"Nie jestem efemerydą, kochanie..." - Najada

Paulina zaczęła rozmawiać z Najadą. Efemeryda wyjaśniła, że jest starszą siostrą Draceny i jej strachem. Ma terminusa. Uwolni go, jeśli Paulina przyjdzie. Sama. Nie mówiąc Netherii. Czemu? Bo Najada kocha swoją młodszą siostrę i chce jej dać życie...
Paulina powiedziała, że się zgadza. Najada się roześmiała perlistym śmiechem.

Przy składzie materiałów budowlanych Paulina poczekała chwilę. Otworzyły się drzwi i wyleciał terminus, zachowuje się jak pijany. Paulina wąchnęła, przeenergetyzowany. Paulina zbadała terminusa - nic mu nie będzie; Najada otworzyła mu wzór i zmodyfikowała kilka elementów. Ale wszystko tymczasowo, za kilka godzin minie. Dotrzymała słowa.
Paulina zamówiła mu taksówkę, choć nadal nie rozumie co Najada tak naprawdę chciała mu zrobić.

Najada jest piękną Diakonką. Płynie w niej energia terminusa i Draceny. Paulina spytała "po co"? "Bo chcę jeszcze przeżyć, nie skończyłam".
Najada nie chce, by Dracena była sama. Ona chce oddać Dracenie jej życie. Paulina powiedziała, że nie jest sama.

Najada, strach Draceny. Siostra z cruentus reverto. Siostra, która nie skończyła... ale chce skończyć.
Dracena weszła - krzyknęła, by Najada zostawiła Paulinę. Przerażona do granic możliwości.

Paulina objęła Dracenę. Najada patrzy z oddali.

"Pamiętasz? 3 siostry, jedna chce zabić dwie pozostałe?"
"Nie, Didi, nie jesteś sama. Nie musisz się jej obawiać."

"Zaadaptuję ją. Skażę. Dostosuję do siebie. Nie będzie twoja."
"NIE! Jestem silniejsza w manipulacji wzorem niż Ty. Kto zmienił Ciebie?!"

Dracena przechwyciła wiązkę prowadzącą od Najady w Paulinę. Paulina wpływa na wzór Draceny by podnieść jej poziom optymizmu, by ustabilizować "zbędną" energię Draceny... i osłabić to, co robi Najada. Paulina czuje, że Najada nie chce "trafić" w Paulinę. Ale Dracena otworzyła się na pełną swoją moc. Po to, by chronić Paulinę.

Paulina powiedziała Dracenie, że ta nie jest Najadą - jest sobą, Draceną, Draceną Diakon. I że jest Pauliny słodką, zwariowaną i kochaną małą wiłką.
To otworzyło oczy zarówno Dracenie jak i Najadzie.

"Silgor coś chciał od ciebie. Draconis. Ale Ty zbudowałaś coś innego, coś WŁASNEGO. Coś, czego się nie boję." - Paulina

Dracena jest szczęśliwa. Ale chce uciec. Chce uciec daleko. Być niezależna.
Najada uśmiechnęła się do Draceny. 

"Ty jesteś lepsza w analizie i zmianie wzoru, ale zawsze byłaś tą naiwną..."

Najada wystrzeliła swoją energię - całą - w Dracenę. Wkleiła w Dracenę energię Jerzego Pasznika. Przywróciła niewolę. To, co dawno temu zrobił Silgor i Dyta z Jankiem w Wykopie Małym. Dracena zwinęła się przebudowywana energią, Paulina patrzy z przerażeniem a Najada się uśmiechnęła czule.

"Kocham cię, siostrzyczko. Już nigdy nie będziesz sama."

Dracena wystrzeliła furią, energią magiczną. Zmiotła efemerydę...
Szybkie badanie Pauliny wykazało, że to tymczasowe. Ta "niewola" potrwa jakieś 2 miesiące. Potem sama wygaśnie. Bezpieczniej teraz tego nie dotykać...
Dracena nie potrafi odmówić żadnego polecenia Jerzego; w jej mentalnych ścieżkach służyć jemu to najwyższy cel jej egzystencji.
Fatalnie.

Dracena poprosiła Paulinę - błagała Paulinę - by przepięła Dracenę na nią. Paulina się waha; Dracena nie chce Jerzego (bo nie!) ani Netherii (ona mnie wykorzysta!). Paulina wiele już razy udowodniła sympatię, oddanie, lojalność... 
Paulina się zgodziła.

I w ten sposób sformowana została nowa Dracena Diakon.

Finalny stan Draceny: Diakonka 8, Wiła 5, Spustoszenie 5, Cybergothka 8, Weapon 1

# Progresja

* Paulina Tarczyńska: została tymczasową Panią Draceny.
* Dracena Diakon: została tymczasową Uzależnioną od Pauliny.
* Dracena Diakon: została ustabilizowana jako Diakonka 8, Wiła 5, Spustoszenie 5, Cybergothka 8, Weapon 1

# Streszczenie

Dracena została ustabilizowana. Psychotyczne działania sformowały efemerydę w kształcie starszej siostry Draceny, Najady. Netheria zorganizowała imprezę, udało się Zespołowi zbudować taką Dracenę jak marzyła Paulina (i sama Dracena), ale potem Najada zmusiła Dracenę do konfrontacji ze swoją przeszłością i pragnieniem bycia samotną. Sprzęgła Dracenę z Pauliną (pośrednio), by ta nie mogła już być sama i po prostu uciec.

# Zasługi

* mag: Paulina Tarczyńska, anioł stróż Draceny poświęcająca nawet kontakty i potencjalnie zdrowie by jej pomóc. Przez Najadę - pani Draceny. Z wyrzutami sumienia.
* mag: Dracena Diakon, w końcu stabilna. Cybergothka, Diakonka, Spustoszona Wiła. Zmierzyła się z przeszłością i wyszła z uzależnieniem od Pauliny - na własne życzenie.
* mag: Kinga Toczek, z planem jak naprawić Dracenę, dzielna osłona Pauliny i niestrudzony monitor sytuacji. Dużo tego na starszą czarodziejkę.
* mag: Netheria Diakon, organizująca najważniejszą imprezę w okolicy, załatwiająca odpowiednie dokumenty i przesypiająca kluczowy fragment sytuacji.
* mag: Jerzy Pasznik, miał dobry pomysł jak znaleźć efemerydę... ale Najada była czymś więcej. Nie miał ani jednej udanej akcji na tej misji, acz wykrył "Dracenę".
* czł: Włodzimierz Jamnik, policjant wezwany z uwagi na zakłócanie porządku który jakkolwiek jest nieco powolny ale zależy mu na sprawie. Nie lubi Draceny i ma wątpliwości do Pauliny.
* czł: Marcin Puczek, młody (13letni) chłopak Dotknięty erotycznie przez Najadę, by ściągnąć Dracenę w kłopoty; rozpoznał Dracenę przy lustrze weneckim. Niewiele mu jest (widział ekshibicjonistkę).
* czł: Krystian Korzunio, telefoniczny anioł Pauliny rozwiązujacy problem z policją i ochroną imprezy - wziął na siebie policję i półświatek.
* vic: Najada Diakon, efemeryczna wyidealizowana manifestacja starszej siostry Draceny ("cruentus reverto"), chcącej zmusić Dracenę do tego, by nie była już sama. Z powodzeniem.


# Lokalizacje


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Marzanka, niedaleko Czarnoskała, gdzie miejsce miała największa impreza cybergoth w historii
                    1. Droga Gościnna
                        1. Domek Zadumy, nadal kontrolowany przez Paulinę, Dracenę, Kingę i Netherię
                    1. Ulica ziołowa, 
                        1. Skład materiałów budowlanych, gdzie efemeryda Najady i Dracena stoczyły ostatnią bitwę.
                1. Czarnoskał
                    1. Komin
                        1. Komisariat policji 3, gdzie rezonans magiczny Draceny zniszczył lustro weneckie. Pracuje tam Włodzimierz Jamnik.

# Skrypt
znaczących konfliktów: [20 - 30]

- próba wydobycia psychozy przez Netherię i Paulinę
- próba utrzymania energii
- policjant bo zakłócony porządek -> dlaczego trzymają dziewczynę w więzach

- eskalacja Spustoszenia i technomagii bo Karradrael i Krwawy Aderialith
- transformacja kontrolowana używając lekarki Pauliny i Netherii
- Najada - efemeryda - "strach # seks # krew"


fate points: 2/osobę

Siły drugiej strony:

- brak znaczących

# Czas

* Dni: 3