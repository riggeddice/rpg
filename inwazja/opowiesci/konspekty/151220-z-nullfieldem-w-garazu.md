---
layout: inwazja-konspekt
title:  "Z Null Fieldem w garażu..."
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151101 - Mafia Gali w szpitalu (PT)](151101-mafia-gali-w-szpitalu.html)

### Chronologiczna

* [151101 - Mafia Gali w szpitalu (PT)](151101-mafia-gali-w-szpitalu.html)

## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa i zaczyna likwidować operację.
Wiła nie żyje, zatruła się narkotykami gdzieś w lesie.
Policjanci na SERIO szukają śladów tajemniczej krwawej sekty.
Artykuł Marii w Kopalinie uderza dewastując reputację i działania Gali.
W szpitalu jest NullField żywiący się magią; jest też pielęgniarka Marzena, która stała się zabawką Adama Diakona.
Marii kończą się pieniądze.
Paulina spróbuje wyleczyć pielęgniarkę Marzenę

## Stawki:

- Czy Marzena skończy jako mag czy człowiek?
- Jak rozwinie się relacja Karmelik - Marzena?
- Czy ksiądz podzieli się swoją wiedzą na temat Adama Diakona?

## Punkt zerowy:

Krystian Korzunio ma problem. Klasyczne metody czyszczenia powiązane z magią... nie zadziałają, nie w Przodku i okolicach. A Tymek Maus i Gala Zajcew zrobili małe echo - echo, które doprowadziło do tego, że lokalna policja interesuje się na serio krwawą sektą w lesie, lokalna mikroprzestępczość chce coś od tego zamtuza... ogólnie, nie da się tego sensownie wyciszyć.
Krystian Korzunio ma asa w rękawie. "Kły Kaina", FAKTYCZNA krwawa sekta którą można wprowadzić do Przodka. Zwłaszcza, jeśli się odrobinę przeedytuje historię. A kto kontroluje historię? Dziennikarze i raporty archiwalne...
## Misja właściwa:
### Faza 1: Cóż żeś, Korzunio, sprowadził?

Cztery dni później.

Paulina była zajęta Marzeną; oswojeniem jej, przygotowaniem różnych tematów. Do tego, że być może musi dokonać wyboru. Maria w tym czasie szukała czegoś innego; by sobie dorobić, zaangażowała się w pomoc ogólnośląskiej redakcji "Rytm Codzienny". Tymczasem Marzena udostępniła Paulinie swój garaż na Osiedlu Siedmu Kotów. Nie jest to idealne miejsce na skomplikowany rytuał czy magię (i wie o tym miejscu Adam Diakon), ale nie bywał tam. Paulina przeskanowała to miejsce; moc Przodka wyczyściła je, nawet, jeśli Adam Diakon tam kiedyś działał. Przynajmniej tyle.

Ku wielkiemu zaskoczeniu Pauliny, Ryszard Herman wziął urlop. Wydarzenia w szpitalu musiały nim bardziej wstrząsnąć, niż się wydawało. Dla Pauliny łatwiej - nikt nie będzie jej patrzył na ręce. 
Marzena powiedziała Paulinie, że ma stały dostęp do NullFielda. Karmelik dał jej dorobić sobie klucz. To jego gabinet a ich... relacja... jest wszystkim dobrze znana. Paulina spytała, czemu NullField jest akurat u NIEGO a nie gdzieś u niej, Marzena uniknęła odpowiedzi. Paulina nie naciska.

Do szpitala przyszło dwóch policjantów. Artur Kurczak (którego Paulina kojarzy) i młoda policjantka, której Paulina nie zna. Przedstawiła się jako Alicja Gąszcz. Pytają Paulinę o to, czy miała do czynienia kiedyś z sektami. Nie. I czy Paulina może coś powiedzieć o tej grupie co weszła do szpitala i porwała nieszczęśnika (Tymka Mausa)? Policjanci są dokładni, uważni i się przygotowali. Paulina powiedziała im, że to najpewniej nie sekta; wyglądało bardziej na porachunki mafijne. Paulina nic więcej im nie powiedziała, ale policja nabrała wrażenia (bardzo słusznego), że Paulina wie dużo więcej, tylko mówić nie chce. Uważają, że jest zastraszona; proponowali jej ochronę policyjną ale odmówiła. Po prostu będą mieli na nią oko.

Paulina nie jest zbyt szczęśliwa, ale przynajmniej Maskarada jest ciągle w jakimś stopniu bezpieczna.

W ciągu tych kilku dni Paulina odświeżyła wiedzę o sympatii, rytuałach, przygotowała pewne alchemiczne mikstury i wywary (bez wstrzykiwania w nie magii; nie chce by Przodek je zniszczył). Przygotowała niemagiczne świeczki, dobre rozstawienie... przygotowała sobie całkiem przyzwoite prowizoryczne laboratorium (+3 tool).
Marzena powiedziała Paulinie jedną ciekawą rzecz; gdzie mieszkał Adam Diakon, gdy był tutaj obecny, zanim wyjechał. Hotel "Nova", apartament. Ale ona zaiwaniła jego chusteczkę. Taką z monogramem. Której używał. Jeśli to Paulinie pomoże...
Tak, może pomóc.

Paulina skupiła się na analizie problemu Marzeny po pracy. Podejście numer jeden. Zrozumieć, nie leczyć. Nieingerencyjne. Paulina zakłada pułapki, niebezpieczeństwa, wszystko. Analiza wzoru Marzeny i korelacji tego wzoru z ciałem Marzeny. A dzięki chusteczce Diakona - wykorzystać sympatycznie by znaleźć elementy i strukturę wzoru Adama Diakona we wzorze Marzeny (+3 circumstance). A Marzena na rozkaz Pauliny współpracuje; robi czego Paulina sobie życzy.
Adam Diakon jest bardzo potężnym magiem (stopień trudności: 20!!! i miał sporo czasu na pracę nad Marzeną.
Paulina się przebiła (21 v 20 -> 25). Decay zaklęcia Diakona w Przodku pomógł. Okazało się, że Adam Diakon zrobił piękną inżynierię energetyczną; zapętlił wzór Marzeny w taki sposób, by cała energia jaką Marzena zakumuluje była do pobrania jak z żywego węzła. Zmienił ją w żywą baterię. Dodatkowo, Paulina ma echo defilera w Marzenie; wyraźnie to ciało wykorzystywało i jest przygotowane do wykorzystywania magii krwi celem akumulacji. Nie dość, że Adam Diakon zrobił sobie z Marzeny baterię, to jeszcze zrobił z niej defilerkę. A jako zabezpieczenie - wbudował posłuszeństwo gdy ma jakąkolwiek zakumulowaną moc.

Przyjemniaczek...
Skuteczny, ale TAKICH magów Paulina naprawdę, naprawdę nie lubi.

Czyli Marzena jest "aktywną" defilerką; jest w jakimś stopniu uzależniona od magii krwi (od czerpania energii z krwi), ale nie ma w niej tej potrzeby. Ciało MOŻE, ale nie MUSI. Gdyby Marzena CHCIAŁA, bez problemu może stać się defilerką. Ale nie MUSI stać się defilerką.

I Paulina ma zgryz. Co robić.

Paulina wpadła tu na całkiem ciekawy pomysł. Ona nie musi zniszczyć zaklęć Diakona czy ich odwrócić. Paulina może przecież rozegrać to inaczej - może wykorzystać własne kanały działania Diakona (który przekierował akumulację energii Marzeny na jej zniewolenie i baterię) w taki sposób, by przekierować akumulację magii krwi na fizyczną niedogodność. Tak, by Marzena musiała się PRZEŁAMAĆ by używać i akumulować magię krwi, by przypadkiem nie stała się defilerką. To sprawia, że tylko zniewolenie musi zniszczyć i ten fragment gdzie Marzena jest "żywym węzłem". Ale to nie będzie tak trudne jak walka ze wszystkimi zaklęciami Diakona; zwłaszcza, że ów bardzo dobrze obudował Marzenę w alarmy i zabezpieczenia. Na szczęście, energia Przodka i brak regularnego doładowywania sprawiły, że te wszystkie dodatkowe komponenty są poważnie osłabione.

Przynajmniej tyle.

HIDDEN MOVEMENT:

- Orank próbuje rozejrzeć się na tym terenie.
- Alicja Gąszcz poszła do "Czarnego Dworu" porozmawiać o nowej mafii (?). Spięcie z Orankiem.
- "Kły Kaina" ściągają kilka młodych do lasu, gdzie się "przeniósł zamtuz"
- Franciszek Marlin idzie do lasu z obstawą; szybko stamtąd wieje
- W szpitalu pojawia się Artur Kurczak i Alicja Gąszcz. Węszą.

END OF HIDDEN MOVEMENT

### Faza 2: Przeniesiony Null Field

Następnego dnia, Paulina wcale nie czuje się wiele lepiej. Nie ma pomysłu co zrobić z Marzeną. To znaczy, pomysł jest, ale czy to bezpieczne, jak to się skończy dla Marzeny i czy Paulina de facto odważy się to zrobić? 
Paulina idzie spokojnie do pracy i nagle - ją zmroziło. Wyczuła wideband signal magiczny; szeroki broadcast. Niejaki Joachim Zajcew prosi o kontakt każdego maga co umie to złapać; szuka swojej wiły. Identyfikuje się jako Joachim Zajcew, Srebrna Świeca, Kopalin. Paulina stwierdziła, że ona się NIE ma zamiaru ujawniać... 

Szybki kontakt Pauliny do Marii; czy ta może zebrać jej wszystkie informacje odnośnie zamtuza z wiłą? Jak się uda, trzeba jakoś podłożyć to Joachimowi - i tyle. Niech ją znajdzie i stąd znika jak najszybciej. Cóż. Jedyny problem - jak przeczyta artykuły, najpewniej trafi na to, że wiła kogoś dopadła; jeśli tak, może wpaść do Szpitala Gotyckiego i tam natknąć się na NullField. 

Czyli należy przenieść NullField. Super, dawno nie było randomowych problemów.

Przy okazji, Maria dodała Paulinie jeszcze jedną wiadomość. Do redakcji "Oka Przodka" trafiła dziś rano wiadomość. Donos. Coś dzieje się w lasku, w którym działała wiła. Jakieś dziwne światła. I być może tam przenieśli zamtuz. Nie ma nic ani o wiłach, ani o sektach, ani o mafiach. Paulina jest BARDZO ciekawa, kto zrobił ten donos. Maria nie wie; jest całkowicie poza pętlą; działa w innym mieście w tej chwili (bliżej Kopalina). Podobno donos był anonimowy, ale zawierał bardzo dużo interesujących informacji.

Paulina nie widzi, jaką korzyść może mieć KTOKOLWIEK z tego donosu.

Paulina potrzebuje czasu, by poradzić sobie z Marzeną i przenieść NullField. By to zrobić, najbezpieczniej będzie napuścić Joachima Zajcewa na Krystiana Korzunio (czyściciela). W ten sposób obaj zajmą się sobą; albo Joachim dostanie swoją wiłę i sobie pójdzie, albo będzie zwalczać Korzunia. W obu wypadkach win-win. A Korzunio nie jest magiem, nie miał prawa odebrać tego sygnału. By to zrobić, Paulina napisała na komputerze szpitalnym wiadomość, wydrukowała, dała do odbicia na ksero, następnie (w rękawiczkach) nadała to na poczcie ekspresem. W ten sposób - nie dojdzie do niej. A wiadomość zawierała następującą informacje: "Krystian Korzunio zabił ci wiłę; hint: poszukaj zamtuzu w Gęsilocie".

W ten sposób Paulina jest prawie pewna, że Gala/Tymek jakoś oberwą. I dobrze im tak.

Korzystając z okazji, Paulina złapała w szpitalu Marzenę i spytała ją, czy ta może przenieść NullField. Nie. Nie może. Dała NullField Karmelikowi; jest to taka bardzo efektowna broszka. Czemu jemu? Gdy NullField jest użyty na Marzenę, zaczyna do niej szeptać. Marzena rozbiera się do naga, klęczy i czeka na polecenia. Karmelik jest OK. Dla niego to dziwny fetysz, ale wyjaśniła mu, że jest bardzo "into slave/master" i jego to kręci. Czemu nie zostawiła tego w domu? Bo jeśli nie dostanie poleceń, będzie tam klęczeć aż umrze z głodu. Wolno jej mówić, błagać. Ale będzie posłuszna. Adama Diakona to bardzo bawiło, zdaniem Marzeny. Kolejne zabezpieczenie.

Po prostu słodki przyjemniaczek.

Trzeba zatem przenieść NullField. Marzena poszła w tej sprawie do Karmelika. 
Minęło 10 minut. Paulina wróciła do pracy.
Minęła godzina. Marzena, lekko rozdygotana, przyszła do Pauliny. Powiedziała Paulinie, że jest problem. Karmelik nie chce dać Paulinie broszki. Karmelik oraz Marzena nie ustawili "safe worda", więc Karmelik myśli, że to ciąg dalszy gry. Otworzył sejf i pozwolił Marzenie zanieść Paulinie tą broszkę; ale Marzena nie może jej dotknąć. Więc Karmelik błędnie zrozumiał, że Marzena chce grać niegrzeczną dziewczynkę; wydał jej parę poleceń i sama obecność NullFielda wystarczyła, by jego przewidywania (że to gra) się potwierdziły. 
...więc... tak.

Paulina is at loss. Nie ma pojęcia jak to rozwiązać. Krótka konsultacja z Marią...
Oki. Paulina ma plan. Niech Marzena weźmie coś INNEGO jako obiekt swojego poddaństwa i przekaże ten obiekt Karmelikowi. Ta broszka zawsze należała do Pauliny; Marzena ją sobie pożyczyła i nie oddała; teraz ma okazję i Paulina chciałaby ją odzyskać. To niestety oznacza, że Marzena będzie musiała kontynuować zabawę w "master & slave", ale trudno; w szpitalu w tej chwili broszka jest potencjalnie zagrożona (w przeszłości przecież polowała na nią Gala a teraz może Joachim)...

Oki. Paulina dostała informację od Marzeny, że Karmelik na to idzie. Teraz Paulina może pójść odebrać broszkę... osobiście.

Karmelik zobaczył Paulinę i się zdziwił. To faktycznie prawda; ta broszka należy do Pauliny. Paulina potwierdziła. Karmelik pozwolił jej go zabrać i Paulina wzięła. Karmelik się uśmiechnął i powiedział, że tylko ma nadzieję, że Paulina się nie nawróci. Co? Karmelik wyjaśnił - "niegrzeczna dziewczynka Marzena" dała broszkę Pauliny Adamowi i Adam pojechał na parafię, po czym się nawrócił, zwolnił i został pustelnikiem. Teraz broszka wraca do prawowitej właścicielki. A on (Karmelik) nie musi się nawracać i zostawiać pustelnikiem.

Po pracy Paulina zamontowała NullField w garażu Marzeny. Jest to w miarę bezpieczne miejsce i na tyle mało ciekawe, by nikt tam nie chodził i nie grzebał.

Czas zacząć prace nad rozmontowaniem tego, co Diakon splątał...

## Dark Future:

### Faza 1: Cóż żeś, Korzunio, sprowadził?

- Próba podleczenia Marzeny; diagnostyki
- Herman bierze urlop
- Kurczak ma teorię - nie ma tam żadnej sekty. Tu się dzieje coś innego.
- "Kły Kaina" ściągają kilka młodych do lasu, gdzie się "przeniósł zamtuz"
- Franciszek Marlin idzie do lasu z obstawą; szybko stamtąd wieje
- W szpitalu pojawia się Artur Kurczak i Alicja Gąszcz. Węszą.
- Orank próbuje (w ramach działań od Korzunia) rozejrzeć się na tym terenie.

### Faza 2: Przeniesiony Null Field

- Pojawia się Joachim Zajcew; szuka swojej wiły
- Korzunio podrzucił ślady, że w lasku "coś się dzieje". Złapała je Maria i przekazała Paulinie.
- Oprócz tego dziennikarz Łukasz Perkas zdecydował się dojść do tego co się dzieje w lasku.
- Dziennikarz natknął się na rytualną orgietkę z małolatą. Został zmuszony. Nagrali go.
- Marlin idzie na parafię, znaleźć jakieś problemy w księgach.
- Bławatek ostrzega kolegów i koleżanki o tym, że jeśli wrócił kult, to będzie źle.
- Alicja idzie do "Czarnego Dworu" powęszyć za sprawą sekty. Poinformowali ją, że nic nie wiedzą; to coś nowego; Marlin kręci.
- Na parafii znajdują się ślady Adama Diakona (informacja od Jerzego Karmelika)

# Zasługi

* mag: Paulina Tarczyńska, która wreszcie doszła do tego co stało się z Marzeną i jak silnym * magiem był Adam. Plus, przeniosła NullField i przygotowała "laboratorium".
* czł: Maria Newa, zajmuje się pracą w zupełnie innym miejscu i usiłuje podreperować finanse; donosi stale Paulinie o plotkach.
* mag: Marzena Dorszaj, pielęgniareczka, defilerka, bateria, zabawka Diakona która stała się "slave" dla Jerzego Karmelika. Bo NullField.
* czł: Ryszard Herman, który wziął urlop ku wielkiej radości Pauliny.
* czł: Jerzy Karmelik, "ho ho ho master" w "związku" z Marzeną wywołanym przez to, co jej zrobił Adam Diakon. Zupełnie nie ma pojęcia, że żeruje na nieszczęściu dziewczyny. Chce dobrze.
* czł: Krystian Korzunio, kontynuuje usuwanie śladów; min. sprowadził "Kły Kaina" do Przodka by uwiarygodnić "sektę" i działania Gali i wiły.
* czł: Artur Kurczak, który non stop zmienia hipotezy i próbuje dojść do prawdy; czy to sekta czy mafia. I czyja. I dlaczego. Otarł się o Paulinę.
* czł: Alicja Gąszcz, młoda policjantka mająca powiązania z nienajlepszą młodzieżą i klubem "Czarny Dwór"; bardzo bystra i niepotrzebnie odważna.
* czł: Bartosz Bławatek, policjant, który strasznie boi się sekt i wszędzie je widzi. Nie chce chodzić do lasu i naprawdę chciałby siedzieć i wypełniać papiery. Najlepiej ze wsparciem.
* mag: Joachim Zajcew, który nadal szuka wiły i którego być może Paulinie udało się napuścić na Korzunia (czyściciela Gali Zajcew).
* czł: Łukasz Perkas, dziennikarz, który miał nieszczęście znaleźć "Kły Kaina" i został przez nich wrobiony w gwałt na nieletniej celem szantażu.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                    1. Osiedle Metalowe
                        1. Garaże osiedlowe