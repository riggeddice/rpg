---
layout: inwazja-konspekt
title:  "Tajemnica podłożonej świni"
campaign: powrot-karradraela
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160404 - The power of cute pet (SD)](160404-the-power-of-cute-pet.html)

### Chronologiczna

* [160927 - Desperacka bateria dla Aegis (temp)](160927-desperacka-bateria-dla-aegis.html)

## Kontekst ogólny sytuacji

### Dokument z ministerstwa rolnictwa 

Ta misja wymaga pewnych informacji o strukturze polskiego rolnictwa w ramach świń:

[Raport ministerstwa rolnictwa w Polsce z 2015 roku](https://www.minrol.gov.pl/content/download/59488/326842/version/1/file/PROGRAM%20rozwoju%20g%C3%85%E2%80%9A%C3%83%C2%B3wnych%20rynk%C3%83%C2%B3w%20rolnych.pdf)

Kluczowy rysunek jak chodzi o świnie:

![Rysunek pokazujący jak wygląda łańcuch dostaw wieprzowiny w Polsce, 2015, za raportem Ministerstwa Rolnictwa](Materials/170321/LancuchDostawWieprzowinyPL.png)

Polecam też przejrzeć analizę SWOT.

![Rysunek pokazujący jak wygląda analiza SWOT wieprzowiny w Polsce, 2015, za raportem Ministerstwa Rolnictwa](Materials/170321/SWOTWieprzowinyPL.png)

Główne wyzwania (za dokumentem):

* Poprawa współpracy w ramach łańcucha marketingowego
* Nierównowaga w sile kontrahentów ORAZ nieuczciwe praktyki
* Dostosowanie skali, struktury i efektywności produkcji do potrzeb i wymogów rynku
* Wyzwaniem jest także szybka poprawa potencjału genetycznego znacznej części krajowego stada podstawowego
* Kreowanie właściwego przekazu dotyczącego oferowanych produktów kierowanego do odbiorców i konsumentów
* Opracowanie rozwiązań dla małych i średnich gospodarstw (min. alternatywny chów, eko-świnie...)
* Radzenie sobie z wahaniami cen i dochodów (min. futures)

Kierunkowe cele (za dokumentem):

* Zwiększenie krajowego pogłowia świń
* Zwiększenie sprzedaży zagranicznej mięsa wieprzowego
* Wzrost doodsetka żywca wieprzowego wprowadzanego do obrotu przez grupy (nie jednostki)
* Wzrost udziału żywca wieprzowego wprowadzanego do obrotu na podstawie umów na dostarczanie produktów rolnych

### W kontekście misji:

I teraz na bazie powyższych, parę informacji:

* W Stokrotkach dominuje produkcja własnego tuczu, między różnymi gospodarstwami
* Występuje jedno małe gospodarstwo z tuczem nakładczym (kontraktowym)
    * Tucz nakładczy -> rolnik "zajmuje się" świniami a dostawcą zwierząt i paszy jest kontrahent
    * W Stokrotkach rolnikiem z tuczem nakładczym (23 świnie) są Gaweł i Grażyna Wieciszek (~45k gospodarstw jest tego typu)
* W gospodarstwie Wieciszków za SWOT:
    * panicznie boją się ASF (afrykański pomór świń)
    * nie współpracują z innymi; mają swoich kontrahentów. Są trochę "inni".
    * ich kontrahent korzysta z integracji pionowej
* W gospodarstwach produkujących na własny użytek
    * panicznie boją się ASF (afrykański pomór świń)
    * mają problem z silnymi wahaniami cen świń i z długami
    * jest popyt na wieprzowinę i akurat większy niż niektórzy mają świń
    * trzeba pokazać, że gospodarstwa sobie poradzą z popytem na świnie z Wieprzpol.
* Pojawiła się firma chętna skupywać wieprzowinę nieco lepiej (nazwa: Wieprzpol)
    * Istnieje ryzyko, że się wycofają jeśli jest zagrożenie ASF
    * Trzeba ich udobruchać i przekonać; są szansą dla tych na własny użytek
    * To konkurencja dla kontrahenta Wiesiczków (nazwa: Świnex Jan Halina Kulig SC)

## Punkt zerowy:

* Franciszek jako sołtys próbuje pozyskać Wieprzpol.
* Franciszek jako sołtys musi pogodzić interesy Wiesiczków i innych.
* I nagle... pojawia się ŚWINIA. W nocy. Nie wiadomo skąd i czemu. Ktoś podłożył świnię?
    * Ktoś ukradł?
    * Wiesiczkowie są przerażeni, że Świnex Jan Halina Kulig SC się wycofa
    * Może Wieprzpol się wycofać, nic o świni nie wiadomo
* Łucja chce zgłosić świnię, by nie było problemów - by region nie ucierpiał
* Józef chce dbać o interesy rolników
* Pojawiają się kłótnie

## Misja właściwa:

Dzień 1: 

3 rano. Jolt. Systemy zabezpieczeń dookoła elektrowni zostały przełamane. Kira odpaliła feedback strike. (7v7->F,F,S). Świniak (glukszwajn) spenetrował warstwę zewnętrzną i włączył się odpromiennik; potężna flara energii strzeliła w powietrze. Potem zabezpieczenia odpędziły glukszwajna...

Kira z paralizatorem i Franciszek ze strzelbą wypadli na pole, koło generatorów. Tam zobaczyli lekko skonfundowanego glukszwajna... który to wieprz nie do końca wie co robić. Kira zauważyła, że ten vicinius ma ogromne stężenie energii magicznej; niestety, nie wiedzą nic na ten temat. Kira postawiła szybko ścieżkę z zabezpieczonego obszaru do tego miejsca gdzie ma uziemioną energię magiczną, za wsią. Glukszwajn poszedł tam spokojnie.

I Kira zamknęła tam w zagrodzie glukszwajna. A glukszwajn zaczął ryć w kierunku na źródło energii magicznej.

* Mogę z nią ponegocjować? XD - Raynor, 2017
* Bo jesteś polityk? XD - Kić, 2017

Kira przełączyła wszystkie źródła w tryb standby. Świnia się rozejrzała i zakwiczała żałośnie. 

* ZJE CIĘ! TA ŚWINIA NIE ZNA ŚWIĘTOŚCI! - Raynor, w odpowiedzi na Kirę chcącą rzucać czary.

Kira postawiła źródło i zaczęła rzucać czar na paralizator. Glukszwajn zaczął wypaczać zaklęcie Kiry, zanim ta je rzuci... Kira jakimś cudem utrzymała zaklęcie. Ciężko było, ale jej wyszło (5v5->S). Kira w desperacji wyłączyła WSZYSTKO. Wszystkie zaklęcia, energie magiczne, wszystko co się dało...

Glukszwajn zakwiczał żałośnie i zaczął skupiać się na paralizatorze.

* Strzel w nią, przy odrobinie szczęścia się rozładuje - Kić, z uśmiechem, do Raynora
* Ale dopiero co Żółw powiedział że to cholerstwo wybuchnie. I JAK MAM STRZELIĆ DELIKATNIE?! - Raynor, zaskoczony
* Może podwiesić paralizator pod sufitem? Świnie nie latają... zostanie i będzie patrzeć. - Kić?
* ZAWIESIMY ŚWINIĘ POD SUFITEM! - Raynor, kolejny genialny plan ;-)

Franciszek montuje pasy na pałąkach żeliwnych i twarde liny by PODWIESIĆ ŚWINIĘ. I zastawia pułapkę na świnię. Wejdzie na pasy, Franciszek pociągnie, świnia pod sufitem. A tymczasem Kira włączyła inne źródło, które ma świnię zainteresować. Świnia poszła za tamtym źródłem... Kira przełączyła. (2v0->S). Świnia w pewnym momencie zaczęła się teleportować...

Franciszek z takim smutnym uśmiechem przestał pracować z pułapką na świnię... to nic nie da, bo się teleportuje. I Franciszek naładował strzelbę amunicją na słonie. I dostał informacje który sprzęt ochronny wziąć...

Kira i Franciszek wzięli traktor i jadą do lasu. Jadą do w miarę osłoniętego miejsca w lasku i... Franciszek ma strzelać. A Kira chce być daleko i wysoko. Franciszek jest facetem więc ryzykuje. Będzie strzelał z ambony na dziki... Franciszek turbotraktorowiec. (4v4->F): Franciszek dotarł na ambonę na dziki, wycelował w glukszwajna i... strzelił. (+2 za broń +1 teren i przycelowanie). Glukszwajn ma (6). Franciszek ma (4)...

F, F, poddanie. Glukszwajn uciekł, acz dokonał Emisji. DUPNĘŁO. Echo pobudziło mnóstwo ludzi we wsi. I wysadziło gigantyczną, gigantyczną dziurę w ziemi. I wywołało efemerydę, która zaczęła się rozwijać. Kira zaczęła stabilizować tą cholerną efemerydę (6v6->S)... sukces. Przekierowuje tą energię w paralizator...

* Co tu się dzieje?! - Leśniak do Franciszka.
* Siedzę sobie na ambonie... patrzę, wieprz. No to strzelam. Jebło i wieprz zniknął... TU SĄ MINY?! - Franciszek, udając zaskoczonego

Wojtek cały zaskoczony; są tu miny. Nie spodziewał się. Serio.

* Tyle chodzę po lesie i żadnej miny nie widziałem... - zafrasowany Wojtek
* Bo są pod ziemią! - Franciszek - No i widzi pan, panie Wojtku, trzeba uważać... bo jak dupnie znowu...

Kira i Franciszek bezpiecznie wrócili do domu. Z planem na przyszłość jak zająć się wieprzem...

RANO.

Rano Kira ładuje drona. Niemagicznego, zwykłego drona (-1 surowiec). Ładuje na to paralizator i wysyła w powietrze. Niech to cholerstwo lata. Niech wieprze mają czego szukać...

Mając odwracacz uwagi wieprza, Kira włączyła hipernet. Magiożerna świnia. Co do cholery... i znalazła glukszwajna. I szuka chętnych - tak, znalazł się. Tien Kamil Czapczak, puryfikator z Trocina. Niestety, Czapczak (z którym gadała Kira) nie jest zainteresowany złapaniem sobie glukszwajna samemu. Szuka takiego glukszwajna, którego ktoś już opanował...

Franciszek ma spotkanie z przedstawicielami Wieprzpolu (min. z audytorką Alicją Makatką). Chcą zainwestować w gminie i zacząć skup świń od lokalnych rolników, np. od Karola Kamrata. Więc Franciszek pisze na kolanie biznesplan - a przynajmniej zarys biznesplanu - i wtedy dochodzą Franciszka ryki i krzyki. Kamrat i Wieciszek się kłócą...

Franciszek tam poszedł. Patrzy - niedaleko urzędu Kamrat i Wieciszek mają gospodarstwa. Franciszek ku swojej żałości widzi co następuje:

* Glukszwajn jest na polu Kamrata i ryje w ziemi; przyszedł z pola Wiesiczka
* Kamrat i Wiesiczek chcą się bić na sztachety; ktoś ukradł niezakolczykowaną świnię i jest niebezpiecznie.
* Jedzie audytorka.

Łucja (policjantka) wpadła na scenę i unieszkodliwiła obu krewkich chłopów. Franciszek wybłagał, by Kira ściągnęła drona i odciągnęła glukszwajna. Łucja chciała za świnią, ale Franciszek powiedział że jedzie audytorka i ważniejsi są chłopi do zamknięcia TERAZ. Łucja ich wzięła...

Kira prowadzi wieprza, który sieje zniszczenie na lewo i prawo lecąc za droną...

Józef spytał Franciszka czy gonić świnię czy nie, bo jadą saperzy bo miny w lesie. Franciszek facepalmował. Olać... Józef olał i wrócił do błogiego nicnierobienia...

Przyjechała audytorka. Bardzo sympatyczna dziewczyna o niebezpiecznym uśmiechu. Zaczęła od pytań o... rozwalony płot. I ślady racic. I ogólnie, o ślady post-glukszwajnowe. A Franciszek jedzie full bullshit mode... i o dziwo, łyknęła to. Tym razem łyknęła to. Gdy Franciszek zaczął mówić o projektach, pokazywać... przyjechał pojazd z saperami. Pytają gdzie są miny. Audytorka ma takie wielkie oczy. Jak Franciszek powiedział że w lesie, audytorka w totalnym spokoju. Świnie nie chodzą po lesie.

Franciszek wysłał SMSa. Wycofać świnię z lasu. I niech nie biegnie przez wieś, bo będzie kaput...

Pan Wojtek (leśniczy) widzi świnię. Nie wie czyja; dzwoni do Franciszka. Ale ten nie odbiera; zadzwonił więc do Józefa...

Tymczasem Kira świetnie prowadzi świnię, ale świnia w końcu skutecznie zaatakowała (teleportacja + asymilacja) dronę; Kira rozpaczliwie wycofała dronę i wypadli na saperów. Ku wielkiemu zdziwieniu saperów, wypadła na nich... świnia. Saperzy zdziwieni jak cholera, zaczęli strzelać z zaskoczenia i wysadzili świnię. Przerażony świniak (nie trafiony, ale poznał dźwięk) zrobił kolejną Emisję i się teleportował. Zwiał. Saperzy "znaleźli kolejną minę". Zatrzymali się i zaczęli saperzyć...

A KIRA TAM LECI. BO EMISJA, MAGIA ITP. KIRA NA ROWERZE!

Tymczasem huk dotarł też do Franciszka i audytorki. 

* Co to było? - zaskoczona audytorka
* Saperzy najpewniej znaleźli minę. Nasze wojsko nie należy do delikatnych... - Franciszek - Saper nie poradził lepiej niż leśniczy z tą miną...

Franciszek zaprosił Alicję do pokoju i zaczął atakować ją prognozami unijnymi, relacjami itp. Audytorka jest mile zaskoczona danymi od Franciszka. Ten się postarał (-1 surowiec, dane z UE) i audytorka się zgodziła - po tym, jak zrobi wizję lokalną. Franciszek niechętnie na to musiał przystać (F,F,S)...

Kira dotarła na teren gdzie saperzy się rozstawiają. Zrobili kordon. Kira nie jest w stanie się tam dostać... więc... POSTAWI WŁASNE MINY by ściągnąć saperów i by ci mieli co rozminowywać. Niewiele myśląc... magiczny koktajl mołotowa i erupcja mocą Zajcewów. Cała energia magiczna przekierowana w wybuch. (3v3->F,S). Kira zrobiła erupcję jakiej nawet nie zrobił glukszwajn. Drzewa... co to było?

Saperzy wpadli w lekką panikę. Zdecydowali się przegrupować i zdobyć cięższy sprzęt. TO JEST REGULARNA WOJNA...

Kira wzruszyła ramionami. Poszła do epicentrum energii magicznej po Emisji glukszwajna. Przekierowuje energię w paralizator. SUKCES! Ma jeszcze lepsze narzędzie polowania na glukszwajna...

I Kira wycofuje się stamtąd...

Do Franciszka przyjechał saper. Powiedział, że niestety, wysadzili świnię. Audytorka zastrzygła uszami. Jaką świnię? No, świnię wysadzili. Franciszek powiedział, że wszystkie są we wsi; to mógł być dzik. W świetle powyższego musieli pójść przeliczyć wszystkie świnie. Audytorka chciała zobaczyć zabezpieczenia by dziki nie wchodziły do lasu, saper powiedział, że to absolutnie wykluczone.

Ogólnie, wezwany będzie lekarz weterynarii. Trzeba sprawdzić, czy wszystkie świnie są zdrowe - potem jednak Wieprzpol wejdzie do Stokrotek.

Franciszek taki szczęśliwy... Kira też...

DOBRA! ZMĘCZENIE ZMĘCZENIEM! ALE TRZEBA ZŁAPAĆ CHOLERNĄ ŚWINIĘ!

Franciszek pojechał zmęczony do Trocina. Kupił tam kilogram srebra w lombardzie. I zaczepił go jakiś Hiszpan...

* Mister! Mister! Jesteś może mister Baranowski? - szczęśliwy czarodziej

Carlos Myszeczka. Optymistycznie nastawiony - tien Czapczak go wysłał by pomógł im złapać glukszwajna. Franciszek facepalmował, ale dobrotliwy mag hodowca może im się kiedyś w przyszłości przydać. Niech będzie - pomoże...

W Stokrotkach INŻYNIER BARANOWSKI zaczął składać klatkę z rur i srebra. Dużą klatkę. Taką na dużego wieprza. Kira uruchomiła jedno ze swoich źródeł energii i poczekała. Zostawiła jedną najwygodniejszą ścieżkę - taką, gdzie emisja jest najlepsza - ale ścieżka prowadzi przez klatkę. Carlos jest konsultantem - on ustalił ścieżkę przejścia dla świni. I o dziwo, udało się.

TEST klatki. Glukszwajn: 6. Franciszek: 8. Sukces. ŚWINIA ZNIEWOLONA. Zachrumkała żałośnie.

Carlos pomógł im wpakować świnię do przyczepy na traktor, tak by nie była szczególnie widziano. Pojechali do Trocina ubić deala z tien Czapczakiem... i udało się sprzedać glukszwajna.

SUKCES!

# Progresja



# Streszczenie

W Stokrotkach pojawił się glukszwajn, ku wielkiemu utrapieniu Kiry. I audytorka Wieprzpolu, ku wielkiemu utrapieniu Franciszka. Próba ukrycia teleportującej się świni przed audytorką, nie wysadzenia saperów w lesie i pogodzenia sprzecznych interesów rolników doprowadziła do tego, że JAKOŚ wszystko się udało. Wieprzpol wchodzi do Stokrotek a złapany glukszwajn został sprzedany magowi Świecy.

# Zasługi

* mag: Kira Zajcew, sprowadziła armię saperów na Stokrotki i zapałała głęboką nienawiścią do glukszwajna wyżerającego jej źródła...
* mag: Franciszek Baranowski, czarował panią audytor z Wieprzpol, chował magiczną biegającą świnię gdzie tylko się dało i konstruował pułapki na świnie. Jedna zadziałała.
* czł: Wojtek Leśniak, leśniczy, rozpowszechnił opowieści o minach w lesie Stokrockim i opowiadał o wieprzach na lewo i prawo. Świadek emisji magii. Nie wie co widział.
* mag: Kamil Czapczak, puryfikator kupujący glukszwajny; skontaktował Carlosa Myszeczkę z magami ze Stokrotek. Kupił glukszwajna za uczciwą cenę.
* czł: Alicja Makatka, audytorka Wieprzpolu, kompetentna, twarda, ale nie spodziewała się takiego absurdu, przez co - paradoksalnie - pozwoliła Wieprzpolowi wejść do Stokrotek.
* czł: Karol Kamrat, drobny rolnik i hodowca świń; chce pracować z Wieprzpolem i poprztykał się z Gawłem Wieciszkiem o glukszwajna (że niby ten drugi ukradł komuś niezakolczykowaną świnię)
* czł: Gaweł Wieciszek, pracuje dla Świnexu hodują im świnie w tuczu nakładczym i poprztykał się z Kamratem o glukszwajna (że niby ten drugi ukradł komuś niezakolczykowaną świnię)
* czł: Józef Krzesiwo, komendant, który skutecznie uniknął wszelkiej odpowiedzialności i standardowo można liczyć, że nic nie zrobi.
* czł: Łucja Rowicz, praktykantka pełna ambicji która chce zatrzymać świnię i która aresztowała dla odmiany ludzi, których warto odizolować od audytorki ;-).
* mag: Carlos Myszeczka, Hiszpan rodu Myszeczków, od niedawna w Polsce, który chce się wykazać. Magiczny hodowca (or so he says). Pomógł trochę z glukszwajnem.
* org: Świnex Jan Halina Kulig SC, firma działająca w trybie tuczu nakładczego i współpracująca z Wieciszkami.
* org: Wieprzpol, firma wchodząca do Stokrotek dzięki działaniu Franciszka i która faktycznie weszła (przynosząc Franciszkowi uznanie gospodarzy)


# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Stokrotki
                    1. Zachód
                        1. Gospodarstwo Kiry i Franciszka, gdzie glukszwajn próbował wyjeść sobie energię magiczną z generatorów Kiry
                        1. Nieużytki
                            1. Ziemianka Kiry
                                1. Eksperymentarium podziemne, gdzie próbowali złapać glukszwajna w pułapkę... i zorientowali się, że cholerstwo teleportuje
                    1. Urząd gminny, gdzie Franciszek przygotowywał dokumenty udowadniające, że Wieprzpol może wejść bez ryzyka ASF czy innych chorób
                    1. Centralne gospodarstwa
                        1. Gospodarstwo Kamratów, z hodowlą na własny użytek, gdzie przeszedł glukszwajn ryć sobie pod działką
                        1. Gospodarstwo Wieciszków, z hodowlą na tucz nakładczy, gdzie zmaterializował się glukszwajn
                    1. Las Stokrocki, gdzie po kilku Emisjach glukszwajna (i wybuchach Kiry) uwierzono w pole minowe... lub co najmniej coś niebezpiecznego
# Czas

* Opóźnienie: 3 dni
* Dni: 1

# Narzędzia MG

## Cel misji

* Sprawdzenie RPG jako narzędzia wdrażania graczy w domenę
* Pokazanie mieszkańców Stokrotek jako ludzi
* Misja z niewielką ilością magów i emergentnymi konfliktami

## Po czym poznam sukces

* Gracze rozumieją w jakimś stopniu jak działa łańcuch dostawy wieprzowiny ;-)
* Pojawiają się żywe postacie rolników w Stokrotkach; to nie tylko "randomowe NPC"
* Występuje co najmniej 10 konfliktów

## Wynik z perspektywy celu

* Sukces. Gracze zapamiętali elementy domeny (modele biznesowe, weterynarię)
* Były postacie, było śmiesznie. Bardzo śmiesznie.
* Konfliktów było co niemiara. 15+.