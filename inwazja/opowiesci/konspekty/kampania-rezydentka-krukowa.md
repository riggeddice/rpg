---
layout: default
categories: inwazja, campaign
title: "Rezydentka Krukowa"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Start](kampania-start.html)

## Opis

-

# Historia

1. [Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html): 10/01/03 - 10/01/04
(170523-opetany-konstruminus)

Przez rezonans między starym rytuałem a magitechem medycznym konstruminus Bankierza został protezą dla ducha. Ów duch próbował wrócić do świata żywych. Paulina zmontowała koalicję z siłami Dukata i Bankierza, znalazła lokalizację starego rytuału i dali radę powstrzymać ducha (konstruminusa) bez szkód ekonomicznych i ludzkich.

1. [Eteryczny chłopiec i jego pies](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html): 10/01/08 - 10/01/10
(170716-eteryczny-chlopiec-i-jego-pies)

Paulina wyleczyła młodego chłopca ugryzionego przez Upiora. Stworzyła Skażeńca - cień doktora Bogumiła Miłoszepta, który bardzo jej pomógł i pokazał, że magicznie się tu coś zmieniło. Gdy Paulina zauważyła, że walczy z groźnym upiorem, wezwała terminusa jako wsparcie - i umożliwiła terminusowi czyste zniszczenie połowy upiora. Potem znalazła artefakt - ciężarówkę, terminus zniszczył drugie pół upiora a Paulina rozmagiczniła artefakt. Just rezydentka things.

1. [New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html): 10/01/13 - 10/01/15
(170525-new-better-senesgrad)

Kurt did see a strange light in the Water Tower which made his cousin sick. This led to Sonia and Klara find old stories about Water Tower and a witness - there is something paranormal here. To protect the region, the businessman - Madler - had rumors created about the bankrupcy of his company; Future Senesgrad faction will require a different approach...

1. [Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html): 10/01/16 - 10/01/19
(170510-najgorsze-love-story)

Zwyczajny dzień pracy Pauliny odwiedzającej pacjenta w domu skończył się znalezieniem Skażonego poletka kukurydzy. Paulina postawiła drenujący relay... a trzy dni później skontaktowała się z nią o 5 rano spanikowana czarodziejka, że powstał drzewiec i ona potrzebuje pomocy by go zatrzymać. Okazało się, że czar Pauliny wszedł w interakcję z rytuałem stworzonym przez młodego maga... drzewiec pokonany, magiem się zajęli. A Diana (czarodziejka) spóźniła się na śniadanie swojej pani.

1. [Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html): 10/01/20 - 10/01/22
(170515-niewolnica-w-leasingu)

Diana została ukarana przez Ewelinę Bankierz w taki sposób, że nie ma prawa nosić żadnego ubrania i ma obróżkę zapewniającą jej stan zdrowia. Ewelina oddała Dianę Paulinie na tydzień. Paulina szybko zorientowała się w stanie sytuacji. Co gorsza, Diana i Hektor mają się ku sobie, ale ścinają się o autonomię i o to, czy Ewelina jest w ogóle dobrą osobą. Diana wiele oddała, by jej ludzka rodzina miała dobre życie; Ewelina uratowała ją od długów. Hektor chce wolności dla Diany...

1. [Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html): 10/01/23 - 10/01/25
(170518-machinacje-maga-rolniczego)

Hektor skontaktował się z siłami Dukata. Detektyw mafii stwierdził, że to dziwne, że Diana ma swoją rodzinę i zaczął badać, czy to naprawdę JEJ rodzina... w wyniku czego zrobiono Dianie pełną diagnostykę z zaskoczenia. Ewelina uznała, że musi Dianie wysłać obstawę - wysłała kuzyna Grazoniusza. Gdy Paulina dowiadywała się od detektywa o co tu chodzi, Hektor zepchnął Grazoniusza ze schodów. Bankierz stracił sporo reputacji; Hektor się musiał kajać. Ewelina zrobiła z tego popularną komediową kostkę Mausów...

1. [Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html): 10/01/27 - 10/01/28
(170614-kryzys-przez-eliksir)

Grazoniusz chciał odzyskać twarz; podał Dianie eliksir obrzydzenia do Hektora, chcąc zrobić nową komedię. W wyniku tego ogródek Pauliny został Skażony, zniszczony i zdewastowany (emocjonalna magia), Grazoniusz wyleciał z kręgu dookoła Eweliny i Newerja się go pozbyła. Całkowicie się nie opłacało. Czykomar poinformował Paulinę, że rodzina Diany nie jest jej biologiczną rodziną. Diana powoli, powoli zaczyna myśleć, że są alternatywy dla Eweliny a Hektor jest rozdarty między Dukatem i Pauliną.

1. [Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html): 10/01/29 - 10/02/02
(170702-milosc-przez-desperacje)

W okolicy pojawiło się dwóch nowych magów - Prosperjusz i Kaja. Paulina wymanewrowała obu, kupując sobie czas i doprowadziła do tego, że Hektor i Diana wreszcie zostali parą. Najwyraźniej Kaja szpieguje dla Eweliny a Prosperjusz chce się od Eweliny oderwać. Paulina przesunęła Prosperjusza na miasteczko gdzie ten może się przydać, załatwiła Hektorowi pracę u Apoloniusza i doprowadziła do tego, że Diana poznała swoją przeszłość - jej rodzina nie jest jej rodziną.

1. [Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html): 10/02/03 - 10/02/06
(170723-wywalczone-zycie-diany)

Ewelina straciła Dianę ostatecznie; Kaja była przemęczona i zrobiła kilka błędów, pokazując Dianie poziom degradacji swojego serca. Ewelinie nie udało się zdobyć przyczółka ani oczu na tym terenie. Ludzie w okolicy Pauliny zaczęli się trochę bardziej interesować Dianą; tym lepiej, że Diana i Hektor się wyprowadzili i mieszkają niedaleko razem. Oby na zawsze szczęśliwie. Tymczasem Prosperjusz + Grazoniusz wchodzą na ostry kurs kolizyjny z Apoloniuszem.

1. [Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html): 10/02/07 - 10/02/09
(170331-kiepsko-porwana-paulina)

Wiaczesław "porwał" Paulinę, by ta uratowała zmasakrowanego człowieka. Udało jej się. Wiaczesław opowiedział Paulinie o tajemniczej arenie gdzie magowie bawią się kosztem ludzi i ich cierpienia; próbuje ją znaleźć. Paulina użyła swoich kontaktów i udało jej się namierzyć mniej więcej okolicę. Wiaczesław i Terror Wąż zapoznali się z Pauliną, która stwierdziła, że nie pozwoli tej dwójce ZABIĆ magów - nieważne jak źli ci magowie by nie byli...

1. [Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html): 10/02/10 - 10/02/13
(170404-waz-jako-vicinius-pauliny)

Pojawił się plan wejścia na tajemniczą arenę. Paulina lekarz i promotor viciniusa (Wąż). Do tego celu Wąż udaje viciniusa i zamieszkała na moment z Pauliną, ku utrapieniu tej drugiej. Sielankę przerwał Onufry, którego żona (mag -> Zaćmienie -> człowiek) zmieniła się w potwora; Paulinie z pomocą Wąż udało się naprawić jej Wzór i przekształcić w viciniusa; choć po drodze nie było łatwo. Wąż nadal udaje viciniusa Pauliny. Kiepsko jej to idzie.

1. [Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html): 10/02/16 - 10/02/19
(170409-nie-zabijajmy-tych-magow)

Z Pauliną skontaktował się mag Pentacyklu, chcąc ją zatrudnić na arenie jako lekarza. Wraz z poznawaniem magów Pentacyklu podejście Pauliny zaczęło się zmieniać z "muszą zginąć" na "w sumie, to nie ich wina". Na arenie jest też wetrejan który wyraźnie przejął kontrolę a same systemy areny są niestabilne. Magowie Pentacyklu wyglądają na takich trochę... lipnych. Paulina przekonała Wąż i Wiaczesława, że nie ma co ich zabijać; wpierw trzeba dojść do tego co tam się dzieje i kto za tym stoi...

1. [Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html): 10/02/20 - 10/02/24
(170323-oszczedzili-na-rurach)

Gdy do gabinetu Pauliny przyszedł Skażony człowiek, Paulina poszła mu pomóc. Wchodząc w sojusz z rozgadanym konstruminusem zauważyła, że w Kropwi Dzikiej Skażenie pochodzi z rur. Ciężkie badania i starcia z biurokracją pozwoliły jej odkryć mroczną prawdę - jakiś mag na boku dorobił sobie wsadzając magiczne czujniki wody... wyszło taniej. I zaczęły przeciekać magicznie. Też, Paulina pozyskała tajemnicze lustro nieznanego maga powiązanego jakoś z tym miastem.

1. [Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html): 10/02/28 - 10/03/03
(170325-klatwozyt-z-lustra)

Podczas badania lustra Paulina odkryła, że lustro zawierało klątwożyta. Klątwożyt okazał się zagnieżdżony w Eneusie, którego Paulina zdążyła polubić. Klątwożyt okazał się być echem żony Archibalda, przezeń zamordowanej. Paulina kontaktując się z półświatkiem zniszczyła klątwożyta (uwalniając Eneusa) i oddała dowód na zniszczenie klątwożyta bossowi półświatka magów, Gabrielowi Dukatowi, za co dostała nagrodę.

1. [Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html): 10/03/04 - 10/03/07
(170326-cala-przeszlosc-splonela)

Dukat chciał koniecznie zdobyć lustro, konstruminusa (Aurusa) i "komputer" od Archibalda. Wiaczesław się tego podjął i wszystko wybuchło; Archibald stracił kamienicę i wszystkie artefakty. Archibald dowodzi ratowaniem Kropwi a Dukat wysłał siły by mu pomóc. W tym czasie Paulina rozpaczliwie ratuje kogo się da i poznaje odłamki z przeszłości Archibalda. Nic nie pasuje do siebie...

1. [Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html): 10/03/08 - 10/03/09
(160124-trzy-opetane-duszyczki)



1. [Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html): 10/03/10 - 10/03/11
(160131-dziwny-transmiter-weinerow)



1. [Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html): 10/03/12 - 10/03/13
(160227-zakazany-harvester)



1. [Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html): 10/03/17 - 10/03/19
(170312-przebudzony-harvester)

Kajetan ściągnął Weinerów by wyczyścili Harvester. Niestety, okazało się, że Harvester został opętany (?) i aktywnie walczy przeciwko magom. O dziwo, ich agenda jest spójna - zarówno Harvester jak i Weinerowie chcą uwolnić ludzi. Okazało się, że opętanie Harvestera jest delikatnym i niestabilnym bytem o dużej wiedzy o Świecy. Koordynowana akcja Kajetana doprowadziła do uratowania porwanej czarodziejki. Plan ratowania ludzi dalej w mocy.

## Progresja

|Misja|Progresja|
|------|------|
|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) Jakub Dobrocień, logistyk Dukata ma u niej dług za sprawę z magimedem i uratowanie życia...|
|[New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|[Paweł Madler](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-madler.html) gossip and false evidence say that he is close to bankrupcy|
|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) zaczyna kontaktować się z siłami Gabriela Dukata.|
|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html) problemy z ubraniem, ale za to nosi elegancką obróżkę od Eweliny Bankierz.|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html) straszny ubytek pozycji; mag bojowy pokonany przez maga-rolnika... wypada z dworu Eweliny Bankierz|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) jest zakochany w Dianie.|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html) jest zakochana w Hektorze.|
|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html) wyleciał doszczętnie z łask Newerji Bankierz i z orbity Eweliny Bankierz|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html) została parą z Hektorem|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) został parą z Dianą|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) znalazł pracę u Apoloniusza Bankierza jako konstruktor odpowiednich ziół do eliksirów|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html) traci Dianę jako przyjaciółkę|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html) pracuje u Apoloniusza Bankierza i stabilizuje się na obszarze Powiatu Pustulskiego z Dianą.|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html) odrzuca Kaję, Ewelinę, stare życie i zaczyna nowe życie z Hektorem|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) jest tymczasowo odpowiedzialna za tien Katarzynę Mirłik aż ta wyzdrowieje|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) dostała pomniejszą sławę jako lekarka na małej arenie, promotorka viciniusów i lekarka. Całkowicie nieprawdziwa fama, acz plotka poszła.|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html) dostał artefakt uzdatniający energię magiczną by ją lepiej wchłaniał (i smakowała lepiej), co redukuje jego koszty.|
|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) ma dziwne lustro wzmacniające, należące do nieznanego maga|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html) wolny od klątwożyta; dorobił się własnej unikalnej osobowości|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html) dostaje nagrodę Gabriela Dukata za zniszczenie klątwożyta|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html) dostaje lekkie uznanie od Gabriela Dukata za doprowadzenie do zniszczenia klątwożyta|
|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html) stracił kamienicę, przeszłość i w sumie wszystko co miał w Kropwi|
|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html) ma transorganiczne implanty kontrolujące energię|
|[Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|[Oksana Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-oksana-weiner.html) uzyskana częściowa wiedza od "Harvestera"|

