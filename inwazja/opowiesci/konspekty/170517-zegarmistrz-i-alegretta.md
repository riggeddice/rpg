---
layout: inwazja-konspekt
title:  "Zegarmistrz i Alegretta"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170222 - Renata Souris i echo Urbanka... (HB, SD)](170222-renata-souris-i-echo-urbanka.html)

### Chronologiczna

* [170222 - Renata Souris i echo Urbanka... (HB, SD)](170222-renata-souris-i-echo-urbanka.html)

## Kontekst ogólny sytuacji



## Punkt zerowy:

Stan początkowy torów (autogen: 6: 1, 5, 7, 9, 11, 13)

1. "Jesteś najlepszy!": 1
2. "Nienawidzę Cię!": 0
3. Marsz żałobny: 0
4. MausIntegracja: 0
5. Cały i zdrowy: 0
6. Pasmo destrukcji: 0
7. "Co to jest Maskarada?": 2
8. "Idę solo": 0
9. "Sojusz jest korzystny": 1
10. "Silny ma rację": 0
11. Infensa - zwarcie szyków: 1
12. Infensa - to było konieczne: 0
13. Infensa - zapomnijmy o tym: 1
14. Infensa - prokuratura: 0
15. Izolacja gildii: 0
16. Prokuratura Magów: 0

## Misja właściwa:

**Dzień 1:**

FLASHBACK: 

Hektor skontaktował się jeszcze z Silurią. Od dawna chodzi mu po głowie idea niezależnej i funkcjonującej prokuratury magów. Czegoś, co będzie rozstrzygać spory pomiędzy magami. Czegoś, by uniemożliwić to, co się tu stało. Siluria zauważyła, że nie wie, co na to Świeca. Hektor poprosił, by Siluria została jego sojuszniczką na KADEMie, by lobbować za tym rozwiązaniem.

Hektor poprosił Silurię, by ta znalazła na KADEMie kogoś, kto będzie zainteresowana czymś takim. Siluria delikatnie zauważyła, że przy takich regułach jak ma KADEM to jest trudne, by znaleźć bardzo entuzjastycznych magów na KADEMie odnośnie tej kwestii. Hektor zauważył, że nie chodzi mu o sprawy wewnątrzgildiowe a o zewnętrzne, arbitraż zewnętrzny. Siluria zaproponowała Hektorowi Andżelikę - ale Hektor musi sam to jej przedstawić.

Andżelika siedzi w bibliotece KADEMu i składa dokumenty przeciwko Mariannie Sowińskiej. Bardzo ucieszyła się widząc Hektora. Chce, by Hektor ją poparł w atakowaniu Marianny Sowińskiej. Hektor przekonuje Andżelikę, by ta pomogła mu w propagowaniu prokuratury magów. Andżelika na to, że ona chce propagować prokuraturę ogólną. Andżelika jest praworządna jak i Hektor, ale Hektor chce coś realnego a Andżelika - idealistycznego...

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|    -1     |     0    |    1     |     1     |       0       |      0       |    1     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     1       |     0    |    0    |    0    |      0     |        1        |

Przeciwnik (strona NPC):

| .Karta.  | .Wpływ akcji gracza. | .OPOZYCJA. |
|    1     |           0          |      1     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    1     |     1      |        1       | 10: -1 W0 |    R    |

Andżelika zdecydowała się pomóc Hektorowi w ramach jego "małej" prokuratury, mającej działać między gildiami. Ale jednocześnie zaczęła swoją kampanię na temat "Marianna nie jest bohaterką, jest potworem". Hektor pomoże Andżelice spisać fakty i zebrać dane do kupy oraz skonstruować _case_ przeciwko Mariannie. Anonimowo.

15. Izolacja gildii: +2, działania Andżeliki doprowadzą do tego, że gildie będą się izolowały i chcą iść w samotność - Marianna jest... trudną postacią.
16. Prokuratura Magów: +2, działania Andżeliki i Hektora faktycznie prowadzą do tego, żeby aktywnie pokazywać działania korzystne.

Hektor zdecydował się rozciągnąć sieć. Znaleźć więcej popleczników...

TERAŹNIEJSZOŚĆ:

Ogromna i nie ukrywana radość rodziny widząc Hektora. Hektor naprawdę poczuł się jak w domu. Marcelin ćwiczył piankowym mieczem z JUDYTĄ...

Otton i Hektor porozmawiali odnośnie całej tej skomplikowanej sytuacji. Otton zasępił się słysząc wieści (m.in. Esuriit), ale wyjaśnił stan aktualny:

* Świeca jest częściowo rozbita
* Karradrael odbudowuje połączenie z Abelardem; Mausowie są wolni i bezpieczni
* Potrzeny jest Karradrael do wypchnięcia Arazille z Czelimina
* Renata Souris i Levia się "słyszą", ale nie umieją ze sobą rozmawiać (Hektorowi Otton powiedział, że Levia to Rezydencja)

Hektor i Klara się w końcu spotkali. Hektor oddał Klarze komunikator z Eis. Eis się skontaktowała z nimi i powiedziała, że jest zamknięta w skrzyni na KADEMie. Klara poprowadziła rozmowę i doszła do tego z kim rozmawia - Eis się przedstawiła. Między Hektorem, Klarą i Eis powstała pewna... pasywna sytuacja. Klara chce współpracować z Eis, ale... ryzyko współpracy z Eis jest koszmarne.

Eis jest rozżalona - Aurelia nie żyje, Renata na KADEMie - najpewniej nic złego jej się nie stanie...

Hektor ma poważny problem. Co on z tym zrobi? Jeśli powie KADEMowi o tym, że Eis jest "na wolności", ma ogromne propsy. Jeśli wejdzie w sojusz z Eis... może się dużo dowiedzieć i nauczyć. Ale ryzyko jest ogromne, co Klara zauważyła.

Na razie - Hektor nie ma zamiaru robić z tym absolutnie niczego. Trzeba to przemyśleć.

10. "Silny ma rację": +1, rozmowa z Hektorem i Klarą uzmysławia Eis, że ta nie może na nich naprawdę liczyć? Może musi działać sama, bezwzględnie?

Judyta złapała Hektora i opowiedziała mu o tym jaka cudowna jest Siluria. Powiedziała mu (mimo reszty słowotoku) coś przydatnego - Maurycy Maus, 5x-letni przyjaciel Judyty opierał się Karradraelowi. Nie chciał być przejęty. I coś się stało. Jego sygnał zanika. Jest połączonym bólem i paniką. Ostatni "zrozumiały" sygnał jaki wysłał Judycie to było "uratuj Alegrettę". I Judyta się strasznie martwi o Maurycego. A przecież... Maurycy jest za stary (lol!) by się podkochiwać w Diakonce...

Hektor może niezbyt lubi Judytę, ale wiadomość, że Maus wysyła dziwny sygnał po sieci Mausów a Karradrael jest za słaby by coś jeszcze zrobić jest ważną wiadomością. Więc, Hektor zorganizował piknik rodzinny. Jadą CAŁĄ RODZINĄ (on, Margaret, Edwin, Klara, Marcelin) a kierowcą - Dionizy. Trzeba zobaczyć co się tam dzieje. Edwin marudzi jak cholera, ale reszta rodziny go przekonała, że trzeba to zrobić - Hektor wrócił z Esuriit i trzeba go docenić.

Gdy dotarli do chatki zegarmistrza, Klara przygotowała grupę dron zwiadowczych. Chce się dowiedzieć, czy tam bezpiecznie.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     0     |    0     |    1     |      1    |      1        |     1        |    4     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      1      |    0     |    0    |    1    |    0       |        2        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |          0           |      4     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.     | .Wynik. |
|    4     |     4      |      2         | 16: +1 W+1 |    S    |

W ramach Efektu Skażenia: rojowość dron przeniosła się na Alegrettę i wszystkie zegary. They became one, swarm mechanism.

Drony wykryły następujący efekt - w budynku jest pięciu porwanych lekarzy. Większość wygłodzonych. Alegretta jest uszkodzona, czysta steampunkowa technomancja i zegary i zębatki... Maurycy jest w częściowej stazie i jest bardzo ciężko ranny. W głowie ma ranę od prób "nakręcania" - klucz wbiła mu w głowę Alegretta. Lekarze próbowali go "naprawić", ale są w horrorze, nie wiedzą co i jak... więc Alegretta daje im czas. Gdy są głodni, przynosi im coś do zjedzenia, ale ona NIE WIE co ludzie mogą jeść...

Oki, to ciekawy problem. Hektor zadzwonił (komórką Edwina) do Judyty. Ta, zapytana telefonicznie powiedziała, że Maurycy nie umiałby zrobić stazy... ale może tak, zawsze interesował się manipulacją CZASEM. Edwin uznał, że najpewniej Maurycy strasznie spowolnił swój metabolizm. Tak czy inaczej, ten mag umiera. A Alegretta jest podłączona do jego energii życiowej, toteż te wszystkie zegary. Bardzo trudno będzie tam się wbić i nie zabić Maurycego... nie mówiąc już o leczeniu.

Hektor ma pomysł. Wejdą przyjaźnie. Alegretta szuka lekarzy, więc wejdą jako Master Edwin i Przydupasy. Więc, Hektor puka do drzwi. Alegretta aż podskoczyła (mają monitoring dron więc widzą co tam się dzieje). Po chwili niepewności wybrała jednego z lekarzy (Pomocnika), by ten otworzył drzwi i zajął się tematem. Jeśli wejdą, nie wyjdą już nigdy - aż Maurycy będzie zdrowy. Pomocnik zrezygnowany postanowił ich wygonić.

Hektor powiedział Pomocnikowi, że są tu by pomóc. Edwin rozpoznał Pomocnika; weszli do środka (grupa: Edwin, Hektor, Klara - i mają MNÓSTWO kanapek od Margaret). Alegretta powiedziała, że to fatalnie że weszli, bo teraz muszą zostać... szybkim ruchem pozytywka znalazła się przy drzwiach blokując im wyjście. Hektor za to zaczął piać peany o Edwinie, wybitnym lekarzu! Alegretta spytała, czemu zatem on i Klara tam są. Hektor wykazał się szybkim refleksem - on jest od noszenia torby Edwina a ona od wyciągania narzędzi z torby Edwina. Alegretta to kupiła i zaprowadziła ich do Maurycego.

Edwin się podłamał. Dopiero z bliska widać poziom zniszczeń w organizmie maga (był wybuch magiczny i staza). To nie klucz do nakręcania był problemem - Maurycy umiera. Powoli. W agonii. A pole magiczne, rezonanse i pryzmat jedynie przeszkadzają. Edwin BYĆ MOŻE da radę go postawić. Być może. Ale to wymaga silnej koordynacji z Klarą, która musi zdjąć pole magiczne...

Dobrze, Hektor poszedł z Alegrettą do góry. Chce mieć wszystkich lekarzy w jednym miejscu. Alegretta ich poprosiła komunikując się przez zegary. Hektor zauważył też, że Alegretta gdy zaczyna myśleć o przeszłości, to się zawiesza. A potem zegary chwilę "cofają czas" i ona znowu nic nie pamięta. Ogólnie, Hektor dał radę dojść do tego, że Alegretta jest "opętanym golemem zegarmistrzowskim" i czarodziejka którą była zginęła w tragicznych okolicznościach. 

W rozmowie, Alegretta opowiedziała Hektorowi, że jednego człowieka nakręciła bo serce się nie kręciło. Ale nie ruszało się, więc wsadziła nowe, by działało. Hektora aż zmroziło. I faktycznie, wśród wszystkich lekarzy tam się znajdujących jeden nie ma serca - zamiast tego ma jakiś technomantyczny mechanizm...

Alegretta powiedziała przypadkiem, że do wskrzeszenia / tego, co z nią się stało wystarczy jeden ząb. Wszystko inne to proteza... Hektor zaczął rozumieć, jak Alegretta działa. Dobra, ale trzeba przejąć kontrolę nad panikującymi lekarzami, by poprawić Pryzmat.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |    1     |     1     |      1        |      1       |    6     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     -1      |    1     |   -1    |    1    |     0      |        0        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    3   |           0          |      5     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    6     |    5       |      0         | 10: -1 W0 |    R    |

Hektor pojechał, że to rządowy eksperyment, coś poszło nie tak itp itp. On to trzyma twardo, pod kontrolą. Udało mu się uspokoić lekarzy, acz (R) niestety INNI magowie (nie Mausowie a z grup Mausożernych) zwąchali już złamanie Maskarady i będą polować na Alegrettę / Maurycego.

7. "Co to jest Maskarada?": +1

Z uwagi na to, że Alegretta bardzo prostestuje przed używaniem zaklęć w okolicy Maurycego (bo wybuch), Hektor próbuje wprowadzić Alegrettę w pętlę logiczną; wtedy, gdy zegary się cofają. Chce ją rozkojarzyć, by kupić czas Klarze i Edwinowi na rzucenie zaklęcia. Chce ją wyłączyć z akcji, by oni mogli spokojnie czarować.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |     1    |     1     |       0       |       0      |     4    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      2      |    1     |   0     |    0    |     0      |       3         |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   4    |          0           |     6      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    4     |      6     |       3        | 14: +1 W0 |     S   |

Hektor jest dociekliwy; próbuje dowiedzieć się od Alegretty co tak naprawdę stało się w jej przeszłości. Próbuje mniej się dowiedzieć a bardziej ją zawiesić. Dodatkowo, Hektor użył zdjęć nawet Alegretty... (R, S). Hektor wyłączył Alegrettę z akcji.

W ramach rerollu - Alegretta wpadnie w panikę. Większy szlak zniszczeń.

Tymczasem Klara ma poważny problem - musi JEDNOCZEŚNIE z Edwinem zsynchronizować zaklęcia. Minutowe inkantacje, w ciągu których Edwin musi skończyć sekundę po Klarze. A efekt czaru Klaru musi wyłączyć stazę i rozpiąć maga od zegarów i częściowo od Alegretty, by dać Edwinowi "clean state" na spokojne leczenie i manipulację magiczną bez dodatkowej aury.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |    1     |     1     |        1      |      1       |     4    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     1       |    0     |    -1   |    1    |     1      |         2       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   4    |          0           |      6     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    4     |     6      |      2         | 11: +0 W0 |     S   |

Reroll, Reroll, Sukces.

Klarze udało się zdjąć stazę i sprzężenie zegarów z Maurycego. ALE! Nadmiar energii i Quarków wpłynął w Alegrettę, wzmacniając ją zdecydowanie. Też: działania Alegretty doprowadzą do śmierci lekarza bez serca.

6. Pasmo destrukcji: +1

Edwinowi udało się zacząć stabilizację pacjenta. Maurycy nie jest przytomny, ale jakoś funkcjonuje. Edwin maksymalnie skupiony...

A Alegretta, do której wpłynęło dużo zewnętrznej energii, zmienił się balans oraz dodatkowo została naciśnięta przez Hektora - odbiło jej. Revertowała do osobowości sprzed "Alegretty" ("Marysia"), stwierdziła że nie żyje i zaczęła szukać mamy, demolując cały dom. Rozbijając zegary (powodując wycieki magiczne), niszcząc ściany... Alegretta jest ogólnie w szale i panice.

TYMCZASEM Margaret opracowała wreszcie mechanizm, by ewakuować lekarzy ze środka. Zaczęła ich "zjadać" ścianami i podłogą. Wycofywać do siebie. Hektor nic nie wie, nie wie skąd to się wzięło... więc jest spanikowany podwójnie. Dobra, Hektor konfliktuje. Trzeba uspokoić smarkulę. Nacisnął na nią ostro.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    1     |    1     |     1     |       0       |      1       |     5    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      -1     |    0     |    0    |    0    |      0     |       -1        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   1    |          0           |      3     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.     | .Wynik. |
|    5     |     3      |       -1       | 16: +2 W+1 |    S    |

Udało mu się. Alegretta revertowała do pierwotnej osobowości i od razu się ucieszyła, czując Maurycego na dole. Ale jest jej przykro, że przy okazji jakoś dom się uszkodził oraz te wszystkie piękne zegary... 

Hektor powiedział, że muszą iść. Z Maurycym. Alegretta się zgodziła. Ona tu zostanie i posprząta. Hektor się nie zgodził - ktoś musi pomóc w transporcie Maurycego. Oki, Alegretta została przekonana w ten sposób. Na dole okazało się, że ten lekarz "bez serca" - spadła na niego szafa. Zginął za plecami Klary i Edwina - byli tak skupieni, że nie zauważyli. Edwin jest bardzo nieszczęśliwy... spróbuje jakoś go postawić na nogi w swoim laboratorium, ale nie wie, ile ów lekarz będzie pamiętał...

Judyta jest przeszczęśliwa, że udało się uratować Maurycego. No i, że ma przyjaciółkę (Alegrettę).

1. "Jesteś najlepszy!": +1

**Stan torów:**

1. "Jesteś najlepszy!": 2
2. "Nienawidzę Cię!": 0
3. Marsz żałobny: 0
4. MausIntegracja: 0
5. Cały i zdrowy: 0
6. Pasmo destrukcji: 1
7. "Co to jest Maskarada?": 3
8. "Idę solo": 0
9. "Sojusz jest korzystny": 1
10. "Silny ma rację": 1
11. Infensa - zwarcie szyków: 1
12. Infensa - to było konieczne: 0
13. Infensa - zapomnijmy o tym: 1
14. Infensa - prokuratura: 0
15. Izolacja gildii: 2
16. Prokuratura Magów: 2

**Interpretacja torów:**

Temat Infensy i Renaty nadal budzą zainteresowanie magów, którzy się na dwie główne kategorie - jedni chcą o tym zapomnieć, wymazać z historii i nazwać anomalią a drudzy chcą doprowadzić do zwarcia szyków i wykluczenia magów, którzy byli z tym powiązani - głównie KADEMu i Hektora.

Pojawiły się działania w kierunku na prokuraturę magów. Silną lobbystką jest Andżelika. Niestety, jednocześnie lobbuje za karaniem Marianny, przez co jedynie rośnie ruch izolacji gildii. Znowu mamy dwie siły. Na to nakłada się Infensa... chwilowo izolacja jest dominującą siłą.

Działania Alegretty wzbudziły zainteresowanie magów Mausożernych - takich, którzy chcieliby po Karradraelu najlepiej karać różnych Mausów. Łatwo dojdą po śladach do Maurycego Mausa a tam - do Blakenbauerów. Na szczęście, Judyta Maus jest przeszczęśliwa z odzyskania Maurycego i z aktualnego stanu Alegretty - Hektor wyraźnie wybił blisko Silurii w kategorii Prawdziwych Bohaterów Judyty...

Eis zaczyna się zastanawiać, czy pomysł z sojuszem był najlepszy, czy nie pójść starym podejściem typu "silny ma rację". Nadal się waha - nie wszyscy magowie KADEMu traktowali ją źle, ale może Hektor nie jest tym magiem, którego szukała...

# Progresja



# Streszczenie

Hektor i Andżelika wspólnie chcą uruchomić prokuraturę magów, gdzie Hektor jest tą pragmatyczną częścią zespołu. Hektor wrócił do Rezydencji, na Primus, wzbudzając radość rodziny. Judyta Maus powiedziała Hektorowi o problemie z sygnałami od jej przyjaciela, Maurycego. Hektor, Edwin, Klara i Margaret dali radę wejść do Chatki Zegarowej i uratować umierającego Maurycego w stazie; w wyniku tego Hektor zaprzyjaźnił się z Alegrettą (opętany clockwork golem). Chatka Zegarowa została poważnie uszkodzona. Eis ujawniła się Hektorowi i Klarze, którzy na razie nie wiedzą co z tym robić...

# Zasługi

* mag: Hektor Blakenbauer, pacyfikujący Alegrettę, działający dla prokuratury magów (bałamucąc Andżelikę) oraz ogólnie silny punkt społeczny sesji. Wrócił do domu.
* mag: Klara Blakenbauer, trafiła do mechanicznego raju; zwiadowca oraz katalistka rozpinająca energię by Edwin mógł wyleczyć biednego umierającego Mausa. Nawiązuje relacje z Eis.
* mag: Otton Blakenbauer, który z przyjemnością odebrał raport od Hektora i powiedział mu w skrócie, jak wygląda sytuacja na Primusie.
* mag: Edwin Blakenbauer, udało mu się uratować Maurycego Mausa z silną pomocą rodziny i mimo tego, że Alegretta próbowała pomóc. Zna wielu lekarzy.
* mag: Margaret Blakenbauer, pojechała na piknik, ale dała radę wymyślić sposób ewakuowania zagrożonych lekarzy z Zegarowej Chatki.
* mag: Judyta Maus, której nikt nie bierze poważnie, ale która bardzo próbuje uratować swojego przyjaciela. Dziewczyna-słowotok. Zaprzyjaźniła się z Marcelinem.
* mag: Maurycy Maus, ciężko ranny zegarmistrz, który zrobił Alegrettę. Leży w stazie całą misję i umiera. Szczęśliwie, przeżył - dzięki Blakenbauerom.
* czł: Robert Pomocnik, porwany przez Alegrettę lekarz; pełni rolę odźwiernego i próbuje nie dopuścić do porwania większej ilości osób. Całkowicie przerażony.
* vic: Alegretta Tractus, dziewczyna-pozytywka. "Opętany" golem przez "Marysię" - czarodziejkę, która zginęła tragicznie. Dzieło życia Maurycego Mausa, który najpewniej nie działał sam.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, dokąd wrócił Hektor, gdzie jest Judyta i ogólnie miejsce które można nazwać domem.
                    1. Las Stu Kotów
                        1. Osiedle Leśne
                            1. Chatka Zegarowa, gdzie doszło do wielkiej tragedii - Alegretta porywała tam lekarzy by ratowali jej ukochanego Maurycego Mausa. Bardzo uszkodzona.

# Czas

* Opóźnienie: 1
* Dni: 1

# Warianty Przyszłości

(1k10 wybiera wariant)

1. **Judyta a Hektor**
    1. "Jesteś najlepszy!": Judyta przekonała się do Hektora; nie tylko Siluria jest epicka! A za Hektora można nawet wyjść!
    2. "Nienawidzę Cię!": Judyta z całego swojego wielkiego serduszka nienawidzi Hektora za to wszystko co się stało. I Blakenbauerów też.
1. **Maurycy?**
    3. Marsz żałobny: stary zegarmistrz nie miał szans przetrwać w nowej rzeczywistości. Umarł.
    4. MausIntegracja: by uratować Maurycego, jego magowe ciało musi zostać zniszczone...
    5. Cały i zdrowy: mimo straszliwych uszkodzeń ciała, potęga rodu Blakenbauerów wystarczy, by Maurycy żył dalej.
1. **Desperacja Alegretty**
    6. Pasmo destrukcji: niestety, Alegretta dokonała ogromnych zniszczeń. Giną ludzie, znikają artefakty...
    7. "Co to jest Maskarada?": Alegretta doprowadziła do złamania Maskarady raz czy dwa, by tylko zapewnić działanie Maurycego.
1. **Kampania Eis:**
    8. "Idę solo": Eis decyduje, że nie jest w stanie współpracować z nikim. Musi działać sama by się uwolnić z KADEMu.
    9. "Sojusz jest korzystny": Eis stwierdza, że jednak współpracowanie ma sens. Np. z Hektorem / Klarą?
    10. "Silny ma rację": Eis stwierdza, że musi wywalczyć sobie wolność, zanim Renata przejmie kontrolę nad KADEMem. Kosztem ludzi, trudno...
1. **Echa przeszłości:**
    11. Infensa - zwarcie szyków: to, co spotkało Izę / Infensę nigdy nie powinno się stać. Świeca izoluje się od tego co się stało.
    12. Infensa - to było konieczne: tak, jak trzeba było przekształcić Renatę tak i Izę trzeba było przekształcić.
    13. Infensa - zapomnijmy o tym: niestety, stało się co się stało, ale na to wiele nie poradzimy. Zapomnijmy o tym i gruba kreska.
    14. Infensa - prokuratura: właśnie DLATEGO powinna powstać prokuratura magów. By tak się nie kończyło.
    15. Izolacja gildii: gildie i rody raczej korzystają z osłabienia Świecy i dążą do atomizacji.
    16. Prokuratura Magów: powstaje prokuratura, która będzie miała za zadanie uczciwie rozstrzygać konflikty międzygildiowo


# Narzędzia MG

## Cel misji

1. Misja weryfikująca działanie 'Efekt Skażenia jeśli czar ma rzut 16+'
2. Misja sprawdzająca działanie 'magicznych umiejętności' ze zintegrowanymi 'magicznymi specjalizacjami'
3. Misja weryfikująca działanie 'Wariantów Przyszłości'

## Po czym poznam sukces

1. Efekty Skażenia będą miały następujące cechy:
    1. Będą wyraźnie zmieniały fabułę
    1. Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt
    1. Nie będą całkowicie nadmiarowe
2. Magiczne umiejętności będą dobrze się synergizowały z resztą postaci; zwłaszcza ze specjalizacjami
3. Warianty mają następujące efekty:
    1. Zawsze jest tor, na który można postawić komplikację fabularną lub Skażenie
    1. Istnieje możliwość zbudowania finalnego REZULTATU misji niezależnie od tego JAK działali gracze
    1. Warianty nie są dominujące, ale kierują MG. Każdy konflikt zwiększa jeden odpowiedni tor w zależności od wyniku
    1. Dodanie każdego dnia 0-2 do 2 torów zwiększa chaotyczność decyzji, ale umożliwia graczom zatrzymanie "złego" wariantu. Pierwszy rzut losowy.
    1. UKRYTE tory sprawiają, że sytuacja jest ciekawa
    1. Istnieje opcja podsumowania sesji RPG po jej skończeniu

## Wynik z perspektywy celu

1. Efekty Skażenia:
    1. Dust: za nisko. Za dużo ich jest. Za łatwo je wywołać. 25% czarów wywołuje COŚ. 
    1. Kić: boję się ich trochę; wymaga dalszego testowania - nie ma poczucia, że wiem, co się będzie działo. Mogą być BARDZO trudne dla MG - nagle trzeba wymyśleć 'Paradoks'.
    1. Żółw: mi pasują; dwa Efekty na tej misji były w porządku - jeden to był ten z dronami, drugi pod koniec misji.
2. Magiczne umiejętności
    1. Dust: podoba mi się to, że jest silniejsza synergia. Jest różnica. Postacie są ciekawsze dzięki temu. Lepsze zrozumienie, co się chce osiągnąć.
    1. Kić: tak, it's a keeper.
    1. Żółw: wygląda na to, że mechanizm działa poprawnie. Tak, jak bym oczekiwał.
3. Warianty:
    1. Dust: bardzo fajny mechanizm. Też nie chcę wiedzieć jakie są, poza tym, które są oczywiste lub o nich wiem. Dane o torach postaciocentrycznie są fajne.
    1. Kić: tym razem mi tak nie przeszkadzały; wcześniej to było bardzo planszówkowe. Teraz jestem ambiwalentna, ale już tak nie przeszkadza. Wciąż przeszkadza mi brak celu (wartości) do której dążę. Fajnie widzieć, że to co robisz wpływa na COŚ. Nie wiem, czy każdy konflikt powinien to podbijać.
    1. Żółw: funkcja REZULTATU MISJI działa perfekcyjnie. Jest podsumowanie rozgrywki. Tym razem z uwagi na legacy torów było DUŻO za dużo, ale nadal jakoś zadziałało; torów nie może być więcej niż 10. Nadal - gracze nie chcą wiedzieć o torach o których postacie nie wiedzą, ale gracze chcą mieć wpływ na fabułę. Nie wiem, jak to pogodzić. Tory bardzo ułatwiają pracę MG. Nie ma sensu, by każdy konflikt podbijał.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |               |              |          |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|             |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |  S / F  |
