---
layout: inwazja-konspekt
title:  "Oślepienie autowara"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170531 - Autowar: pierwsze starcie (HB, KB)](170531-autowar-pierwsze-starcie.html)

### Chronologiczna

* [170531 - Autowar: pierwsze starcie (HB, KB)](170531-autowar-pierwsze-starcie.html)

## Koncept wysokopoziomowy misji

Wszyscy na jednego - wszyscy zwalczają potężnego autowara, by móc go zarufusić. A autowar próbuje ufortyfikować teren.

## Kontekst ogólny sytuacji

Temat Infensy i Renaty nadal budzą zainteresowanie magów, którzy się na dwie główne kategorie - jedni chcą o tym zapomnieć, wymazać z historii i nazwać anomalią a drudzy chcą doprowadzić do zwarcia szyków i wykluczenia magów, którzy byli z tym powiązani - głównie KADEMu i Hektora.

Pojawiły się działania w kierunku na prokuraturę magów. Silną lobbystką jest Andżelika. Niestety, jednocześnie lobbuje za karaniem Marianny, przez co jedynie rośnie ruch izolacji gildii. Znowu mamy dwie siły. Na to nakłada się Infensa... chwilowo izolacja jest dominującą siłą.

Działania Alegretty wzbudziły zainteresowanie magów Mausożernych - takich, którzy chcieliby po Karradraelu najlepiej karać różnych Mausów. Łatwo dojdą po śladach do Maurycego Mausa a tam - do Blakenbauerów. Na szczęście, Judyta Maus jest przeszczęśliwa z odzyskania Maurycego i z aktualnego stanu Alegretty - Hektor wyraźnie wybił blisko Silurii w kategorii Prawdziwych Bohaterów Judyty...

Eis zaczyna się zastanawiać, czy pomysł z sojuszem był najlepszy, czy nie pójść starym podejściem typu "silny ma rację". Nadal się waha - nie wszyscy magowie KADEMu traktowali ją źle, ale może Hektor nie jest tym magiem, którego szukała...

Autowar Bzizma dał radę połączyć się z leylinami i niestabilnymi Węzłami głębinowymi. Nie dał jednak rady w pełni opanować sytuacji co doprowadziło do lekkiego Skażenia i lekkiego naruszenia Maskarady. Musi skierować swoją uwagę w tą stronę; musi odrobinę spowolnić swoje główne infestacyjne działania.

## Punkt zerowy:

* Ż: Jaka grupa stała się szczególnie istotna w tym temacie? I co mają wspólnego ze sprawą Infensy Diakon?
* D: Szmuglerzy artefaktów. Infensa (jeszcze Iza) kiedyś na nich polowała.
* Ż: Co mogą wnieść lub jak mogą przeszkodzić?
* K: Mogą dostarczyć coś, co ułatwi nam porwanie autowara.
* Ż: W jakim budynku w Mszance Skażenie przebija najmocniej?
* D: W świetlicy; budynek niedaleko szkoły, chodzą tam dzieciaki po lekcjach.
* Ż: Co Estrella może wnieść za cenne informacje mogące pomóc w starciu z autowarem?
* K: Sposób działania oraz kody kontrolne. Punkt wejścia, backdoor...
* Ż: Dlaczego pole maskujące Rezydencji działa jedynie na czterech magów?
* D: Jeśli za duże stężenie pola maskującego, Bzizma przez nie przejrzy - nie jest głupi.
* Ż: W jaki sposób Eis może bardzo pomóc w rozpracowywaniu problemu autowara?
* K: Przeciążenie adaptacyjne. Eis jest w stanie działać SZYBCIEJ niż Bzizma kodami kontrolnymi i może autowara zagubić.

**Ścieżki Fabularne**:

* **B_Infestacja Cywili**:                  0/6
* **B_Infestacja Żywności i Ekonomii**:     1/6
* **B_Regeneracja i Adaptacja Bzizmy**:     0/nieskończone
* **B_Produkcja Rojów**:                    1/8
* **B_Podpięcie się do leylinów**:          4/8     (2 tier)
* **B_Oczy autowara**:                      0/4
* **B_Szpiedzy autowara**:                  0/4
* **B_Żywność autowara**:                   0/4
* **Złamanie Maskarady**:                   2/8     (1 tier)
* **Skażenie Magiczne**:                    3/8     (1 tier)

## Misja właściwa:

**Dzień 1:**

Estrella wróciła do Zespołu. Próbowała (z pomocą Marcelina) przekonać odpowiednie siły w Świecy by dowiedzieć się jak najwięcej o autowarze ze Świecy. 6v5->F,S.

* KF: Estrella wpada w pewne komplikacje polityczne przez to co robi. To przeszkadza planowi Wielkiej Prokuratury (+1 akcja).

Marcelin próbuje przekonać Ottona, by ten poszukał informacji na temat autowara w Rezydencji. Próbuje przekonać, że przecież Świecy zależy, podał dane, że Estrella, że brzoskwinie itp... a Otton ma do niego słabość. 6v5->S.

Spotkanie rodowe. Nie ma jeszcze Estrelli. Otton zawezwał rodzeństwo (+1 akcja).

* Bzizma jest byłym autowarem zrobionym przez Blakenbauerów.
* Celem Bzizmy jest wieczna ekspansja aż znajdzie przeciwnika i zniszczy go.
* Mają biochemiczne kody kontrolne mogące coś zrobić Bzizmie.
* Marcelin dostał informacje o feromonach działających na tego autowara.
* Bzizma potrzebuje ogromnej ilości energii. Zjada brzoskwinie.

Zaskoczone rodzeństwo jest zaskoczone. Otton dał Hektorowi dowodzenie, po czym sobie poszedł.

Przybyła Estrella (+1 akcja). Powiedziała, że ma informacje odnośnie kodów do autowara... ale autowar został uruchomiony z informacjami, że Kompleks Centralny może być stracony. Więc autowar będzie dość paranoiczny. Powiedziała też o taktyce Bzizmy z Oczami i Szpiegami.

Margaret zabrała się za budowanie os na bazie tych, które porwano ostatnio (+1). Osy zawierają krew Judyty Maus (+2), by odwrócić skuteczniej uwagę Bzizmy. Czyli osy stanowią dywersję. Wykorzystuje Rezydencję (+1). 6+4v8->R. Sukces, ale komplikacja fabularna. 

KF: Niefart sojusznika: Sygnatura Judyty Maus zostaje wplątana w całą tą sprawę. Co jest niekorzystne. (+1 akcja).

Marcelin musi przygotować truciznę do zniszczenia sensorów organicznych. BlackLab (+1), osy (+1), dane Ottona (+2), magia (+1). Żeby zniszczyć sensory biologicznie, trzeba zniszczyć: 2+4(autowar)+2(skala)+2(niezniszczalne, regeneracje)-2(rozproszony; kupuje czas) = 8. 10v8->S. Efekt Skażenia.

ES: 9: Postrzeganie magiczne: wszystkie strony będą miały bardzo utrudnione postrzeganie magiczne.

Klara musi przygotować wektor. Rój małych robocików. 5 +1(magia), +2(rozproszony Bzizma), +1(algorytm, nie inteligencja), +1(warsztat w Rezydencji). Przeciwnik: Pierwszy Rój Os (8). 10v8->S.

Czyli mają (+1 akcja): 

* dywersję, mającą odwrócić uwagę Bzizmy
* mechaniczne owady, mające dostarczyć truciznę do oczu Bzizmy
* truciznę, mającą zniszczyć oczy Bzizmy.

Estrella przygotowuje Anioły Świecy (konstruminusy) na późniejsze operacje. A Hektor? Dowodzi Judytą Maus (!!!) i katalistą, który kiedyś był częścią Krwawej Aleksandrii. Judyta ma za zadanie znaleźć sposób na zniszczenie sadu. I zostawić swoją aurę Mausową.

* Sad jest bardzo silnie chroniony. Kolejne (10).
* Judyta ma wsparcie katalistyczne (+2). Ma +6 +1 (magia). Dostała Skażone brzoskwinie (+1). 

10v10->S, ES.

ES: Tymczasowa zmiana Otoczenia. Sad się... ZMIENI. (+1 akcja)

Dobrze więc. Zespół ma sposób jak przygłodzić autowara i jak autowara oślepić. Czas wykonać działania. Dodatkowo Hektor dał cynk Alinie i Dionizemu i oni poszli... wysadzić transformator. Wieś nie będzie miała prądu przez pewien czas. Kolejne rozproszenie autowara.

Czas pojechać odwiedzić Mszankę. (+1 akcja)

* Bzizma: 7 akcji: 5,4,4,2,2,2,3 -> 2,0,1,1,1,1,0,2
* Other: 4 akcje: 1,2,3,3 -> 2,2,1,0

Z czego wynika:

* **B_Infestacja Cywili**:                  0/6
* **B_Infestacja Żywności i Ekonomii**:     3/6     (1 tier)
* **B_Regeneracja i Adaptacja Bzizmy**:     2/n     (1 tier)
* **B_Produkcja Rojów**:                    2/8     (1 tier)
* **B_Podpięcie się do leylinów**:          6/8     (3 tier)
* **B_Oczy autowara**:                      0/4
* **B_Szpiedzy autowara**:                  0/4
* **B_Żywność autowara**:                   0/4
* **Złamanie Maskarady**:                   4/8     (2 tier)
* **Skażenie Magiczne**:                    5/8     (2 tier)
* **Składzik przemytników**:                1/6

Bzizma jest uszkodzony bardziej niż się wydaje. Przez brak prądu potrzebuje dostępu do brzoskwiń asap. 

**Ścieżki Fabularne:**

Jak powyżej.

**Interpretacja Ścieżek:**

Autowar stracił dostęp do sensorów zaawansowanych oraz do podstaw ekonomii, jakkolwiek infestacja ekonomii częściowo zneutralizowała działania Blakenbauerów. Mimo, że jednostka biowojenna nadal kontroluje cały ten teren, wie już, że jest podgryzana przez kogoś, kto jest dla autowara niewidzialny. Jednocześnie próg Skażenia przekroczył bezpieczny poziom jak i poziom złamania Maskarady stał się wyraźny; Bzizma musi się tym powoli zacząć zajmować.

* Sad, w którym znajduje się Bzizma uległ poważnej transformacji
* Estrella wpadła w problemy polityczne; znowu wyciągnięto jej działanie przeciw Świecy na rzecz ludzi
* Postrzeganie magiczne jest utrudnione na całym terenie Mszanki. Skażenie manifestuje się halucynacjami magicznymi
* W Skażeniu na terenie Mszanki wszędzie obecna jest energia Judyty Maus. Skażenie i dywersja...
* Siły przemytników zaczną odgrywać znaczącą rolę

# Progresja



# Streszczenie

Otton poproszony przez Marcelina powiedział, że Bzizma to autowar pochodzący od Blakenbauerów - stary autowar, przejęty przez Świecę. Estrella dodała, że Kompleks Centralny odciął kody kontrolne, by autowary nie wpadły w ręce Karradraela. Więc... Blakenbauerowie jako ród przygotowali się i odcięli Bzizmę od zaawansowanych sensorów konstruowanych przez autowara i kupując sobie czas. Skażenie i łamanie Maskarady w Mszance wzrosło, ku zmartwieniu wszystkich łącznie z autowarem.

# Zasługi

* mag: Hektor Blakenbauer, wciągający do walki z autowarem chronionych przez Rezydencję magów poKarradraelowych - min. Judytę Maus.
* mag: Klara Blakenbauer, która stworzyła wektor ataku (mechaniczne mrówki) mające przenieść truciznę Marcelina do zniszczenia sensorów Bzizmy.
* mag: Marcelin Blakenbauer, ukochany syn tatusia (Ottona), przekonał go, by Otton znalazł informacje o autowarze dla Blakenbauerów. Też: twórca toksyn przeciw autowarowi.
* mag: Margaret Blakenbauer, odwracająca uwagę Bzizmy i os Bzizmy "skażonymi" osami z krwią Judyty Maus, by Bzizma szukał Karradraela a nie ataku...
* mag: Otton Blakenbauer, który łącząc się z Rezydencją ściągnął informacje na temat Bzizmy na prośbę Marcelina. Okazuje się, że ma słabość do brzoskwiń.
* mag: Estrella Diakon, przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.
* vic: Bzizma Stlitlitlix, połączył się z większością leylinów pod Mszanką, ale stracił zaawansowane sensory. Wie już, że nie widzi swojego przeciwnika...
* mag: Judyta Maus, której sadownicze umiejętności i magia pogodowa okazały się kluczowe do zniszczenia sadu z żywnością dla Bzizmy. Jej aura jest wszędzie w Mszance.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, centrum fabrykacji i budowy środków do opanowania autowara. Też centrum dowodzenia.
                1. Mszanka Polna, 
                    1. Wieś
                        1. Sad Brzoskwiniowy, Skażony i zniszczony połączonym zaklęciem Judyty Maus i nienaturalną aurą miejsca.
                        1. Silos, Infested, w którym doszło do zniszczenia sensorów Bzizmy. Niemożliwe do sensownie szybkiego odbudowania.
                        1. Szkoła Matejki
                            1. Świetlica, najbardziej Skażone miejsce w Mszance.

# Czas

* Dni: 1

# Frakcje:


## Zagubiony Autowar

### Koncept na tej misji:

Istota stworzona do toczenia wojny i zniszczeń. Autowar jest bardzo niebezpieczny i... wchodzi w tryb fortecy. Nie do końca wie co robić i z czym walczy.

### Scena zwycięstwa:

* Nikt się nie orientuje, ale Mszanka stała się bezpieczną bazą dla Bzizmy i jego sił
* Infested Humans zapewniają szersze jedzenie, logistykę, ekonomię i pilnują Maskarady; jednocześnie zapewniają Bzizmie jedzenie i działanie
* Bzizma powoli rozprzestrzenia swoje oczy i działania na zewnątrz, szukając szerszych działań
* Eternal Expansion. W końcu znajdzie Wroga.

### Motywacje:

* Maskarada musi być zachowana. Nic nie może wyjść na zewnątrz.
* Trzeba przygotować oddziały do walki z Wrogiem. Wróg jest skuteczny i niebezpieczny.
* Zapewnić logistykę i fortyfikację; musi przetrwać przede wszystkim.

### Siły:

* Bzizma, autowar owadzi; ufortyfikowany w Sadzie Brzoskwiniowym: Autowar (4), Duży (1), Adaptacja Biologiczna (1), Tarcza Fizyczna (2), Tarcza Mentalna (3), Tarcza Magiczna (3), Bardzo Pancerny (2), Rój Strażniczy (3), Zindoktrynowany (2), Autonomiczny (2), Regeneracja Osami (1), Regeneracja Infestowanymi (2), Ukryty (2), Najedzony (2). Ufortyfikowany (2), Uszkodzony (1), Łatwy do rozproszenia i odwrócenia uwagi (2)
* Pierwszy Rój Os (Wojowniczki, Samobójcze, Lęgowe): Taktycznie Zbilansowany Rój (5)
* Drugi Rój Os (Wojowniczki, Samobójcze, Lęgowe): Taktycznie Zbilansowany Rój (5)
* Infested Humans: Devoted (3)
* Osa Wojowniczka: Zwinna, Pancerna, Szybka, Toksyczna, Mordercze Żądło, Kontrolowana (2)
* Osa Lęgowa: Pancerna, Duża, Paraliżuje, Dominacja Pasożytnicza (2), Kontrolowana (2)
* Osa Samobójcza: Niestabilna, Zwinna, Bardzo Szybka (2), Bardzo Toksyczna (2), Kontrolowana (2)

### Wymagane kroki:

* Infestacja cywili
* Infestacja pożywienia i rozprzestrzenie Oczu Bzizmy
* Zapewnienie Maskarady przez wyłapywanie jednostek kluczowych
* Zapewnienie źródeł energii przez wysyłanie Infestantów i podpięcie się do leylinów
* Produkcja Rojów
* Rozprzestrzenianie się po terenie celem Infestacji i znalezieniem Wroga

### Ścieżki:

* **Infestacja Cywili**: 2,2,2
* **Infestacja Żywności i Ekonomii**: 2,2,2
* **Regeneracja i Adaptacja Bzizmy**: nieskończone
* **Produkcja Rojów**: 2,2,2,2
* **Podpięcie się do leylinów**: 2,2,2,2
* **Złamanie Maskarady**: 2,2,2,2
* **Skażenie Magiczne**: 2,2,2,2

# Narzędzia MG

## Cel fabularny misji

Pokazanie potęgi autowarów. Oraz jak fajna jest Estrella ;-).

## Cel techniczny misji

Nie Dotyczy

## Po czym poznam sukces

Nie Dotyczy

## Wynik z perspektywy celu

-

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Modyfikatory:

|  .Okoliczności. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .MODYFIKATORY. |
|                 |          |         |         |            |                 |

Wynik:

| .Postać. | .Opozycja. | .Modyfikatory. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |   SFR   |
