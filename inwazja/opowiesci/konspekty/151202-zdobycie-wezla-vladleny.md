---
layout: inwazja-konspekt
title:  "Zdobycie węzła Vladleny"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151124 - Odbudowa relacji konfliktem (HB, SD)](151124-odbudowa-relacji-konfliktem.html)

### Chronologiczna

* [151124 - Odbudowa relacji konfliktem (HB, SD)](151124-odbudowa-relacji-konfliktem.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
Ż: Co jest szczególnie cennego / ciekawego w węźle Vladleny?
D: Jest sterylny; jest to przenośny Węzeł, którego bardzo trudno Skazić emocjami i już ma poziom stabilny (autokumuluje).
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".

## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Siluria została pobita przez Ignata i wyłączona z akcji.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę, która potem została przez Hektora upokorzona.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Skubny zdobył koncesję na kanał telewizyjny.
- Hektor, Edwin, Marcelin i Margaret byli zamknięci szukać jakichś sojuszników politycznie.
- Edwin się przyznał, że stoi za Skubnym.
- Blakenbauerowie chcą się skupić do ataku na Ozydiusza Bankierza by poprawić reputację swego rodu
- Tatiana zdecydowała się wypowiedzieć wojnę Hektorowi używając Fire Suita.
- Szlachta robi jakieś mega tajne działania w BlackLab na zlecenie Wiktora
- Hektor chce zdjąć ograniczenie z Diakonów przeciw Blakenbauerom; ale jeszcze nie może
- Ktoś próbował (skutecznie) rozdzielić Blakenbauerów i Diakonów. 

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Plan graczy

- doprowadzić do zgody politycznej między Szlachtą i Kurtyną by zdobyć Węzeł
- zdobyć Węzeł

## Dane

- Węzeł należy do Vladleny; Kurtyna go jedynie używa. Emilia nie może go oddać.
- Szlachta pracuje nad najpiękniejszym Archer Flower Of Secrets dla Silurii jaki jedynie istnieje.
- Margaret odkryła nad czym pracuje Szlachta i co spotkało biednego Adriana.
- Węzeł jest strzeżony przez dwóch Strażników Zajcewów i sieć pułapek Zajcewów.
- Emilia nie może przekazać Hektorowi klucza do przejścia przez pułapki. Ale mogła się pomylić i go zostawić...
## Misja właściwa:
### Faza 1: Klucz do Węzła

Hektor udał się do Margaret i zastał ją pracującą nad jakąś galaretką w słoiku. Zapytana, Margaret powiedziała, że galaretka to plan ostatni plan Szlachty; to jest ich eksperyment. Chodzi o coś, dzięki czemu mag zobaczy "wolność"; coś tymczasowo zmieniającego percepcję i przechodząca przez hipernet. Margaret nie wie o co z tym chodzi, nie wie jak Szlachcie to pomaga, ale na pewno pomoże wprowadzić "inny stan świadomości".
Hektor zaproponował Margaret swój Plan:
- wpierw udać się do Emilii i powiedzieć, że potrzebny jest Węzeł. I to zniszczy docelowo Szlachtę. Lub sabotować ich projekt w dotkliwy sposób.
- potem udać się do Diany i powiedzieć, że potrzebne jest wsparcie (dywersja) w zdobywaniu Węzła.
I wszystko to - wynegocjuje Margaret! No, z pomocą Hektora.
Margaret się zgodziła; nie ma lepszego pomysłu. Galaretki umierają, Szlachta naciska, oni się już zobowiązali a rodzina chce Węzeł. Margaret jeszcze nieśmiało ruszyła temat Kacpra Weinera, ale Hektor powiedział "potem". Nie wszystko naraz.

Gdy Hektor zbierał się do Emilii, złapał go Marcelin. Powiedział Hektorowi, że znalazł sojusznika. Jest to najemniczka, Esme Myszeczka. Szybka, skuteczna, robi co się ją prosi, nie zabiła go gdy był na linii strzału - a nawet wyleczyła. Poznali się i od tej pory mają długą historię (parę razy rozmawiali i robiła dla niego małe rzeczy). Esme nie boi się wejść do Rezydencji.
Hektor wziął jej numer, wrzucił na stos Rzeczy Które Można Kiedyś Wykorzystać i poszedł dalej. Emilia czeka...

"Jest ładna, możemy jej zaufać" - Marcelin.
"Marcelin, Casanova Kopalina" - Hektor.

Więc Margaret przekształcona w ciało Klemensa oraz Hektor pojechali do pałacyku Emilii...

Tam Margaret przekształciła się w swoją normalną formę, wyjęła "galaretkę ze słoika" którą pieszczotliwie nazwała "Grzegorz"

Ż: Dlaczego "Grzegorz" potrzebuje hipernetu do poprawnego działania?
K: Skoro magowie pamiętają rzeczy których nie wiedzieli, 'memory injection' po Archerze; informacje których Szlachta nie może zdobyć o celu - perswazja i przekonywanie.
Ż: Dlaczego "Grzegorz" jest elementem rekrutacji Szlachty?
B: Bo czasem "Grzegorz" służy do szantażu a czasem do twardych - bądź miękkich negocjacji. Czasem pokazuje magowi jak Szlachta może spełnić marzenia i jak Świeca jest 'zła'.

Innymi słowy, bardzo szybko z pomocą Margaret Emilia i Hektor doszli do wniosku, że "Grzegorz" (czyli ta galaretka w słoiku) może służyć do tego, żeby wspomóc rekrutację Szlachty. Jednocześnie, jeśli on powstaje w BlackLabie, Blakenbauerowie mogą mieć wpływ na konstrukcję tego obiektu. I mogą coś w nim zawrzeć; w jakiś sposób głęboko to zmodyfikować. Zmienić kampanię rekrutacyjną a nawet zrobić to, co chciała zrobić Vladlena, tylko inaczej i nie używając Spustoszenia. Innymi słowy, istnieje taka opcja. Może się to nawet opłacać.

To zaprowadziło Hektora do prośby - potrzebuje Węzła Kurtyny. Tego czystego Węzła. Tego nieskazitelnego, o którym wiedziała Vladlena. Emilia powiedziała, że ona niestety nie włada tym Węzłem; jest to Węzeł Vladleny i Emilia nie może go oddać. Kurtyna go tylko używa. Hektor powiedział, że Szlachta ma siły które po prostu mogą go zabrać. Emilia zauważyła, że jak nie ma Vladleny, nie ma kto wysadzić Węzła; ona by tego nie zrobiła. Czyli Węzeł faktycznie nie jest szczególnie chroniony...

Hektor zapytał Emilię o to, jak bardzo chroniony jest ten Węzeł. Czy są tam magowie? Są tam golemy strażnicze Zajcewów i pułapki Zajcewów. Ale jest klucz i hasło - tu Emilia mimowolnie spojrzała na szufladę swojego biurka - ona nie może i nie chce oddać Hektorowi Węzła bo był jej on powierzony przez 'lenę.

No dobrze... jak zatem dostać się do tego nieszczęsnego klucza? Margaret zrobiła to, w czym się specjalizuje - zrobiła histeryczną scenę. Że zostawiła odciski palców w łazience i już tam są magowie i je zbierają i w ogóle. Pobiegła, za nią poszła Emilia ją uspokoić (pilnować, by coś złego się nie stało). Tymczasem Hektor zajumał klucz.
Po czym opuścili pałacyk Emilii.

### Faza 2: Atak na Węzeł

Adrian Murarz, mag Szlachty został wysłany na wyprawę przeciwko Węzłowi. Z nim udali się: Dionizy, Alina, Klemens. A Alina dostaje próbkę krwi Vladleny, dzięki czemu przekształca się we Vladlenę i z nią klucz zarejestruje. Nie tylko pułapki zostaną wyłączone ale i wszystkie zabezpieczenia puszczą Vladlenę z kluczem.
Margaret odrobinę przekształciła wygląd Dionizego i Klemensa; nie będzie dało się ich w żaden sposób poznać kim naprawdę są. Dodatkowo Margaret powiedziała Hektorowi, że "Vladlena" to sztuczny człowiek - "płaszczka". Czyli Hektor nie wie o Alinie. A do tego jeszcze Mojra nałożyła na Alinę aurę Vladleny; to rozwiązuje problemy czegokolwiek.

Adrian dostał artefakt od Mojry służący do transportu Węzła. Zajmie to jakieś 300 sekund od momentu uruchomienia, nieprzerwane - transport zajmie 300 sekund.

Hektor porozmawiał z Wiktorem Sowińskim i poprosił o dywersję. Dużą dywersję, by on mógł zdobyć Węzeł a Kurtyna była zajęta i nie mogła przysłać żadnych sił na zdobycie Węzła. Wiktor się uśmiechnął. Da się załatwić. Za to oczekuje kwiat na następny dzień... jaki kwiat? Margaret wie.
Hektor przepytał Margaret o co chodzi. Ten kwiat jest... dziwny. Bazuje na własnościach podobnych do "Grzegorza", ale zbiera 'losową' myśl z hipernetu i przekazuje ją jako wrażenie odbiorcy. Poza tym robi jeszcze więcej bardzo złożonych rzeczy i wymaga uczenia i kalibracji. Hektor się zainteresował - niech Margaret się tym zajmie.

Hektor puścił Emilii informację "Wiktor coś szykuje, bądźcie ostrożni", po czym rozpoczął działanie. Atak rozpoczęty.
...i od razu zakończony.
Alina jest perfekcyjnie przygotowana. Rezydencja otworzyła portal do bezpiecznego pomieszczenia a pułapki i golemy się same rozmontowują. "Vladlena" jest perfekcyjna a obecność klucza jedynie legitymizuje całą sprawę. Klemens i Dionizy się nudzą. Nie mają nic przeciwko temu. Trzeba będzie podrzucić klucz na miejsce...

Adrian podszedł do Węzła i uruchomił artefakt. Po 20 sekundach Węzeł się rozjarzył i zmienił się w częściowy portal. Przez portal przeszła Tatiana w Fire Suit.

Klemens strzelił. Fire Suit ochronił ją przed wszystkimi pociskami (nieletalne działania). Tatiana zobaczyła "Vladlenę" i zapytała zdziwiona co tu się dzieje. (test: Tatiana - mimo Fire Suita - uwierzyła, że Alina jest Vladleną). Alina wykorzystała całą wiedzę, jaką nakarmiła ją Mojra, Hektor i Rezydencja odnośnie Tatiany, Vladleny i Fire Suita, po czym zaczęła tłumaczyć, że Kurtyna została zinfiltrowana przez Szlachtę i Węzeł musi zostać przeniesiony w bezpieczne miejsce. "Vladlena" i Tania zaczęły dyskutować na ten temat; Tatiana uważała, że Rezydencja Zajcewów jest dużo lepszym miejscem a "Vladlena"- że nie wiadomo kto z Zajcewów jest w Szlachcie. W końcu jednak po bardziej ożywionej dyskusji (i przedstawieniu Adriana jako maga Kurtyny a nie Szlachty) Tatiana zgodziła się na to, żeby faktycznie Vladlena robiła to, co uważa za stosowne. Sama wycofała się; zdaniem Tani Vladlena spaliła jej pułapkę.

Adrian zdążył przetransferować Węzeł do Rezydencji Blakenbauerów i Zespół zdążył się wycofać. Tatiana szybko się zorientowała, że Vladlena nadal leży w szpitalu i natychmiast wróciła, ale odbiła się od Rezydencji. Nie ma dowodów na to, że to byli Blakenbauerowie. Wie, że miała rację, ale nie wie "przeciwko komu". A Fire Suit, przy całej swojej potędze, nie przekroczy barier Rezydencji. Tatiana po poprzedniej sprawie nie wierzy już tak całkowicie w percepcję Fire Suita. Dowiedziała się, że Adrian jest członkiem Szlachty, ale nie było to już dla niej wystarczające.

Jeszcze przed deaktywacją Fire Suita Tatiana dała radę dowiedzieć się, że Adrian jest w Rezydencji Blakenbauerów; nie jest to jednak wystarczający dowód by móc coś z tym zrobić. W związku z tym, użycie Fire Suita zostało zarejestrowane jako "Tatiana została przechytrzona przez swoich przeciwników". Gorzej - to nie to, że nie miała racji - nie była dość dobra NAWET przy użyciu Fire Suita.

A Hektor świętuje. Wziął nawet whisky z lodem.

# Streszczenie

Margaret pracuje nad "galaretką" dostarczoną przez Szlachtę; coś mającego umożliwić magom zobaczyć "wolność" (wymaga hipernetu do działania). Z aprobatą Emilii, Margaret ukradła klucz do portalu Vladleny a Hektor go zajumał. Tatiana przerwała im w Fire Suicie; Alina jednak ją oszukała i nie doszło do walki. Tatiana przegrała Fire Suit Duel. Powstała o niej niepochlebna ballada.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Osada Strażnicza
                        1. Dworek Emilii Kołatki
                    1. Las Stu Kotów
                        1. Platforma Węzła Vladleny

# Zasługi

* mag: Hektor Blakenbauer, który stworzył plan nie mający szans powodzenia, zlecił to ludziom którzy tego nie wiedzieli i oni to zrobili.
* vic: Alina Bednarz, lepsza "Vladlena" niż oryginał; skutecznie oszukała Tatianę w Fire Suicie.
* mag: Margaret Blakenbauer, coś pomiędzy "Q" z Bonda, histeryczki robiącej scenę oraz najsubtelniejsza osoba jaką Hektor ma do dyspozycji. Niestety.
* mag: Marcelin Blakenbauer, który znalazł cennego sojusznika dla Hektora (Esme Myszeczkę). Hektor się już boi. Casanova Kopalina.
* mag: Emilia Kołatka, która NIE MOŻE DAĆ HEKTOROWI WĘZŁA VLADLENY. If he knows what she means.
* mag: Adrian Murarz, "mag Kurtyny" i przenośna platforma dla artefaktu przenoszącego Węzeł Vladleny.
* mag: Tatiana Zajcew, która prawidłowo oceniła sytuację i użyła Fire Suita w odpowiednim momencie... by zawieść w kontakcie z Aliną. Vendetta przegrana i zamknięta.
* mag: Wiktor Sowiński, który zrobił brutalną i skuteczną dywersję przeciwko Kurtynie kupując czas Hektorowi do zdobycia Węzła.

# Czas

* Dni: 3