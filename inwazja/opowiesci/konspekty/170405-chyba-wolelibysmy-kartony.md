---
layout: inwazja-konspekt
title:  "Chyba wolelibyśmy kartony..."
campaign: powrot-karradraela
players: kić, dzióbek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170517 - Zegarmistrz i Alegretta (HB, KB)](170517-zegarmistrz-i-alegretta.html)

### Chronologiczna

* [170222 - Renata Souris i echo Urbanka... (HB, SD)](170222-renata-souris-i-echo-urbanka.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Quasar poinformowała Silurię, śpiącą sobie spokojnie na KADEMie, że Renata Souris chce się z nią widzieć. Zapytana przez Silurię, czy to nie może czekać, Quasar powiedziała, że to może poczekać. Siluria, nieszczęśliwa, się zbiera. Nie spieszy się nadmiernie... spytała Quasar od jak dawna Renata nie śpi. Od dwóch godzin. TO NIECH MEDYTUJE DALEJ. 6 rano? Siluria nie idzie. A przynajmniej szybko.

Quasar zatem obudziła Pawła Sępiaka, bo Renata powiedziała o Spustoszeniu i powiedziała, że Pawła to może zainteresować. Paweł się zebrał...

Siluria mając wprawę w ubieraniu się szybko dotarła do Renaty pierwsza...

* Widać, że Wielka Siostra nie powiedziała prawdy, bo się jednak pojawiłaś - Renata
* Powiedz mi tylko... ta pora jest dość bezbożna - Siluria
* Medytowałam na szczątkowych wspomnieniach Karradraela. Zauważyłam coś niebezpiecznego. - Renata

Wszedł Paweł.

* Ostatni element układanki jest na swoim miejscu. Doskonale. - Renata
* Karradrael był szalony. Prowadził eksperymenty, których nigdy by żaden Maus nie autoryzował... swoją drogą, chciałabym móc testować swoje poziomy magii, ale cerber kontrolujący to pomieszczenie mi na to nie pozwala. - Renata, do Silurii i Pawła
* To... myślę, jest do załatwienia. - Siluria
* Tak myślałam. - Renata, wracając do konkretów

Renata wyjaśniła, że w Bażantowie znajdowała się pierwotna kwatera Spustoszonych Dron Karradraela. Po tym, jak przestały być istotne, Karradrael skupił się na tym, by robić tam różne eksperymenty. Eksperymenty typu "Krwawa Kuźnia", ale niekoniecznie powiązane z Krwią - na pewno ze Spustoszeniem. Abelard nic nie wie. Renata też nie wie. Jej zdaniem, KADEM może spokojnie przejąć wszystkie te rzeczy lub je zniszczyć; ona o to nie dba. Ona chce móc eksperymentować z magią. Jej zdaniem, KADEM nie chce pomagać magom ani ludziom, ale może technologie ich zainteresują. Mówimy o Spustoszeniu, energii magicznej a wszystko zamaskowane. Ludzie jako przykrywka.

Paweł nie jest zbyt przekonany. Siluria też nie.

* Chcecie umożliwić jej eksperymentowanie z magią w tamtym pomieszczeniu, czy to tylko próba zdobycia informacji? - Quasar do Silurii i Pawła
* Chcemy dowiedzieć się, co potrafi - Paweł - Nie wiemy czym jest, jak wpłynie to na jej nową postać maga.
* Czy cokolwiek stoi na przeszkodzi, by ufortyfikować halę symulacji lub doposażyć salę treningową? - Siluria
* Chcemy, by mogła eksperymentować w warunkach które NAS interesują - Paweł - Chcemy móc zadawać pytania i dowiedzieć się jakie ona zadaje
* Rozumiem - Quasar
* Poza tym, abstrahując... kiedyś ją wypuścimy. Wolę wypuścić kogoś kto panuje nad swoją mocą - Siluria
* Niezbyt nam wychodzi trzymanie zagrożeń w zamknięciu; wolę poznać jej potencjał niż nie wiedziec nic - Paweł
* Dostosujemy odpowiednią salę treningową z halą symulacji. - Quasar - To chwilę potrwa, ale rozumiem o co Wam chodzi

Paweł uznaje, że Renata będzie albo sojusznikiem, albo przeciwnikiem. W obu wypadkach - musimy wiedzieć czym ona jest...

* Co w sprawie tej sytuacji w Bażantowie? - Quasar
* Trzeba to zbadać... nie możemy tego zostawić tak po prostu - Paweł
* We dwójkę tam iść nie możemy - Siluria - Nie tylko we dwójkę
* Robię rejestr aktywnych agentów... potrzebujecie magów, czy viciniusy mogą być? - Quasar
* Jedno i drugie jest w porządku... - Siluria - Na pewno potrzebujemy kogoś, kto potrafi przebadać, wykryć...
* Czyli potrzebujecie Andżeliki - Quasar, spokojnie
* Siła ognia? - Paweł
* Shadow i Midnight - Quasar - Poinformuję ich o sytuacji

Godzina później.

* Lepsze to niż transportować kartony. - Shadow (Sławek)
* Czy chcę wiedzieć? - Siluria
* KADEM na Primusie jest w ruinie. Kartony... - Sławek
* Kontratakują - pomocny jak zawsze Paweł
* Nie. Musimy pakować i rozpakowywać, czyścić i regenerować - Sławek, taki zawiedziony - To ja wolę iść z Wami
* Idealna motywacja... - Maja (Midnight)
* Przepraszam za spóźnienie - Andżelika
* Jeszcze dobrze nie zaczęliśmy. - Siluria, uspokajająco
* To ja skoczę umyć zęby - Andżelika

Jak już wszyscy zgromadzili się w komplecie, czekają na szczegóły. Andżelika oburzyła się, że ufają RENACIE. Paweł zauważył, że mogła powiedzieć prawdę. Andżelika westchnęła. Cóż, wszyscy zauważyli, że Renata jest winna miliona rzeczy, ale TYM RAZEM dostała za niewinność. Mogą być "zadufanym KADEMem", ale to by była przesada.

Ustalili plan. Jeśli problem jest za duży, wycofują się. W innym wypadku, próbują to zdobyć i przejąć kontrolę nad tymi rzeczami.

KADEM Primus to faktycznie ruina. Mnóstwo kartonów, pojemników, zdewastowanych miejsc... i nieszczęśni magowie KADEMu próbują coś z tym zrobić. Siluria poprosiła w rodzinie o mały van; dostała go bez większego problemu (-1 surowiec). Będzie lootowanie. No i wzięli z KADEMu pojemniki do bezpiecznego transportu obiektów. Paweł przygotował VISOR. Gotowi i weseli, pojechali do Bażantowa...

Bażantów. Nie wygląda jak postapokaliptyczne miasteczko; wygląda jak ZWYKŁE miasteczko.

Andżelika, poproszona, zdecydowała się puścić skan magiczny. Jak coś pójdzie nie tak, naprawią ją tarczami antyspustoszeniowymi. No i... coś poszło nie tak. Niestety. Ale nie jest to klasyczne Spustoszenie, to coś innego; Andżelika zamarła. Magia zaczęła dookoła niej fluktuować dążąc do niestabilności i Paradoksu. Paweł przyjrzał się temu VISORem; doszło do tego, że energia eksplodowała w niegroźnym Paradoksie (F,S), ale Paweł dowiedział się co się stało - Andżelika została "złapana" przez dziwną energię. Tak jak normalnie Spustoszenie, no, "Spustosza" tak tym razem ten mechanizm "złapał" Andżelikę i "trzyma".

Siluria ma pomysł. SHE WILL MAKE HER HORNY. W ten sposób wyrwie Andżelikę z tego; o tym Karradrael najpewniej nie pomyślał. To jest wystarczająco "poza typową domeną rozwiązań" by się tego nikt nie spodziewał. I walnęła Andżelikę jeszcze "pre-ekstazą" (5+1+1v4->S). Andżelika... zareagowała. Po chwili męczarni erotycznej się obudziła. Paweł... zaskoczony, ale pracuje często z Silurią. Sławek i Maja... szczęki ku ziemi.

* No... to jest... coś - Sławek
* Wow... skuteczne... - Maja

Andżelika doszła do siebie po kilku minutach, czerwona jak burak.

* Nie... nie było tego, dobrze? - Andżelika
* Ach, te paradoksy - Paweł
* Wyglądałaś... słodko - Midnight, lekko zaczerwieniona
* TU COŚ JEST! - Andżelika, próbując odwrócić uwagę
* Tyle zauważyliśmy - Siluria

Andżelika powiedziała, że tu jest kilka Spustoszeń. Jedno ją złapało. Dopytywana przez Silurię i Pawła, powiedziała (6: trudny ekspert):

* Jest tu jeden Overlord Spustoszenia kontrolujący RÓŻNE Spustoszenia
* Overlord Spustoszenia ma cechy Aleksandrii
* Andżelika nie wyczuła aktywnej Magii Krwi
* Overlord Spustoszenia nie jest ekspansywny i jest bardzo NIELUDZKI i NIETYPOWY

Faktycznie, tu mają miejsca działania powiązane ze Spustoszeniem. I nie wiadomo, czy uda im się ściągnąć na KADEM Spustoszenie do badania?

* Wiecie, jeśli Aleksandria, to mamy problem z ratowaniem żywych istnień... - Maja
* Musimy ustalić, czy ratujemy, czy ściągamy na badania na KADEM. - Paweł

Andżelika powiedziała, że część jest zaraźliwa, ale część NIE. Na bazie Spustoszenia powstało coś całkowicie nowego. I tamten Overlord kontroluje to wszystko. Maja zaproponowała przejęcie Skrzydłoroga; Sławek zauważył, że ludzie w okolicy mogą być Spustoszeni. Ogólnie, są plany. Ale nie ma decyzji.

Plan jest taki: Andżelika emituje wiązki energetyczne magiczne by zlokalizować Aleksandrię - coś, co ją złapie. Jeśli tarcza wytrzyma, super. Jeśli nie, Siluria ją wybudzi... moment. Da się inaczej. Jeśli tarcza NIE zadziała, można pre-ekstazą wytrącić Andżelikę. CZYLI: TAM GDZIE SIĘ ANDŻELICE NAJBARDZIEJ PODOBA, TAM JEST NAJPEWNIEJ ALEKSANDRIA. Dość... ekstatyczne wyszukiwanie Aleksandrii.

Są w vanie. Muszą przejechać przez miasto i skanować ekstazę Andżeliki.

* Nie zgadzam się. Nie zrobię tego. Nie ma takiej opcji. - Andżelika, czerwona jak burak - Jestem POWAŻNĄ czarodziejką.
* Ależ oczywiście. - Siluria 
* To dla dobra wszystkich! - Sławek, tak szczerze jak potrafił
* Jesteś jedyną, która może to zrobić - Paweł
* Jestem... arystokratką Świecy! To niegodne! - Andżelika, z lekką rozpaczą
* Ty chyba nie masz pojęcia co ja z Wiktorem wyczyniałam - Siluria, z radością

Siluria przekonuje Andżelikę (6v2->S) a Paweł zaczyna montować system łączący to do VISORa. Po kilku protestach pro forma Andżelika została FAKTYCZNIE podpięta do VISORa i van ruszył w miasto. Sławek prowadzi. Bardzo próbuje się skupić. Bardzo. Maja patrzy z fascynacją.

Najdziwniejsze jest to, że... wskazania Andżeliki pokazują inny kierunek. Nie w Bażantowie i nie w Skrzydłorogu. Kierunek jest gdzieś na obszary rolnicze, w południowej części za Bażantowem. Sławek skierował tam vana a Maja zaczęła triangulować.

* Ja bym puścił parę dron dla bezpieczeństwa - Paweł
* Dobry pomysł. Trianguluję wskazania... celujemy w Zakłady Przetwórstwa Wieprzowiny "Chrumkacz". - Maja, skupiona - Andżeliko, Aleksandria ze świniami?
* Nic... nic o tym... nie wiem - Andżelika, próbując utrzymać kontrolę

Paweł puścił drony (5v3->R) i wykrył zagrożenie. Cholerny MECH wystrzelił z gauss guna. Paweł zdążył ostrzec Sławka i van uniknął ciosu; aczkolwiek pojawił się też rój (12) dron zbliżający się do vana i zaczyna zestrzelać drony Pawła.

Maja stwierdziła, że rozwali to duże. Paweł zaczął multiplikować drony (iluzja), by odwrócić uwagę przeciwnika. (6v5->S). Paweł zapewnił chaos; wrogie drony łowcze nie są w stanie rozwiązać problemu dron Pawła. Maja zaraportowała, że nie jest w stanie zestrzelić tego dużego; on jest gdzie indziej.

Siluria skupiła się na wykryciu mecha. Jak to wykryć? Masa owadów. Małych owadów (lifeshaper). (4v5->F(uszkodzony van), F(surowiec), F(złapana w wiązkę), S). Siluria dała radę zlokalizować mecha, acz o moment za późno - konstrukt wystrzelił uszkadzając poważnie van. Midnight rozwaliła konstrukt.

Andżelika zaczęła schodzić z VISORa. A Silurię miźnęła Wiązka Spustoszenia; Paradoksalnego Spustoszenia. Owady sformowane przez Silurię wyrwały się spod jej kontroli i zaczęły być nosicielami pre-ekstazy i post-ekstazy, ekstazy bez pre- i post-... ogólnie, dość ekstatyczne owady.

Andżelika pisnęła ze strachu. Shadow ma szczękę przy ziemi. Midnight jest przerażona. Zaczęła rzucać "fiery flare of annihilative doom" na te komary. Paweł natychmiast ją zatrzymał i odpalił inny czar - przekształcił wszystkich magów w podręczne generatory Tesli, by dać im pole siłowe przeciwkomarowe. (6v5->15). Kilka komarów doleci do miasta, ALE NIE DO ZESPOŁU.

* To było... groźne - Maja - niszcząc wszystkie drony w powietrzu, ale myśląc o komarach.
* Dzięki, Midnight. Straciliśmy oczy. - Paweł, z przekąsem
* Nie czaruj - Andżelika do Silurii - Masz coś nie tak z energią... mogę VISORa?

Andżelika po chwili rozwiązała problem i naprawiła Silurię. Powiedziała jej, że każdy jej czar był Paradoksem. To była jakaś forma Spustoszenia, inna.

Shadow zatrzymał wszystkich. Powiedział, że przed nimi (koło już przetwórni) jest najpewniej już jedno wielkie pole iluzji. Andżelika też czuje potężne pole energii. Paweł zrobił małe, lekkie drony - granaty tesli. Nie mają być wytrzymałe. Mają rozwalić wszystko, co przed nimi.

* Słabo chronione jak na eksperymentalne miejsce Karradraela - Sławek, zdziwiony
* Pytanie jak to chronił jak to jeszcze kontrolował... - Paweł

Paweł zmultiplikował swoje drony iluzją i wysłał je do środka pola iluzji. Mają służyć jako oczy, dodatkowo mają określić co tam się dzieje czy dywersja. Plus, teleportują się między iluzją. (9->bardzo trudny ekspert).

Paweł wykrył co tam jest. Alarmy się świecą, ale nic nie działa. Mechy, generatory... Jest też Aleksandria. Znajduje się w budynku. Jest... nietypowa. Połączone świnie, ludzie, magowie? Jest tam też celatid złożony z Aleksandrii. Niestety...

Shadow ma prosty plan. Midnight przesunie antygrawitacyjnie cały ten obszar gdy Paweł zdetonuje drony kupując im czas. On przesunie to w stazę.

Paweł ma inny plan. Stworzył golemy (min. z krwi Sławka) i je multiplikował; skupiają na sobie uwagę celatida. (10). Przy okazji odciął to surowcem (-1 surowiec).

Paweł odwraca uwagę celatida (10v8->S). Celatid skupiał się na golemach. Paweł kupił dość czasu, by Midnight oderwała antygrawitacją ten obszar. Jako, że źródła energii padły, Shadow przesunął to w fazie do Shadowzone (staza). Andżelika podpięła Węzeł który zasilał Aleksandrie itp. do stazy Shadowa. I w ten sposób opanowano teren...

KADEM się okopał. Poprosili Martę, by jednak osobiście przyjechała i się tym zajęła. A JANEK NIC NIE WIE.

Szaber dał mechy, sprzęt ciężki, kilka źródeł energii itp. Oraz kilka rzeczy "niebezpiecznych NIE DOTYKAĆ". FOR MARTA ONLY. Opisane jako "własność prywatna, Sabinka, obejrzyj sobie"...

# Progresja



# Streszczenie

Renata chcąc poprawić swoje warunki na KADEMie (i móc eksperymentować ze swoją magią) ostrzegła KADEM, że w Bażantowie Karradrael eksperymentował z różnymi wariantami Spustoszenia. Paweł, Siluria, Andżelika, Shadow i Midnight ruszyli tam i napotkali dziwną świńsko-ludzką Aleksandrię i coś nowego - Celatida Aleksandryjskiego. Wszystko z dziwnymi szczepami Spustoszenia. Po ciężkim przebijaniu się, KADEM porwał i zneutralizował eksperymentalne byty oraz rozszabrował obszar ;-).

# Zasługi

* mag: Siluria Diakon, ekstazą walczyła ze Spustoszeniem i wyciągała Andżelikę; też, detektorka mechów w iluzjach używając owadów.
* mag: Paweł Sępiak, mistrzowsko używał dron w iluzjach i jako oczy; też: odwracał uwagę celatida swoimi golemami. Detektor problemów Andżeliki VISORem.
* mag: Renata Souris, ostrzegła o niebezpiecznych eksperymentach szalonego Karradraela, za co oczekiwała na możliwość eksperymentowania ze swoją magią.
* mag: Sławek Błyszczyk, kierowca wypożyczonego vana, twórca kilku przydatnych planów i mag przenoszący cały problematyczny obszar do pola stazy.
* mag: Maja Błyszczyk, artylerzystka dużej mocy, która stanowiła 80% siły ognia zwalczającej mechy, drony (też przyjazne). Jednostka antygrawitująca celatidy.
* mag: Andżelika Leszczyńska, ujeżdżała VISORa, służąca jako ekstatyczny detektor nietypowego Spustoszenia. Przekonana przez Silurię. Jako jedyna wierzy, że "to" był Paradoks.
* mag: Quasar, która pełni rolę koordynatorki i administratorki KADEMu. Przyniesie, poda, dopyta, załatwi... 

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Myśliński
                1. Bażantów
                    1. Południew
                        1. ZPW Chrumkacz, zakład przetwórczy wieprzowiny, gdzie znajduje się Celatid Aleksandryjski i centrum chorych eksperymentów szalonego Karradraela

# Czas

* Opóźnienie: 2 dni
* Dni: 1

# Narzędzia MG

## Cel misji

* Misja, w której Paweł będzie miał SWÓJ DZIEŃ - Drony i iluzje
* Renata - dostanie możliwość ćwiczenia z nowymi mocami, czy nie?
* Pokazać glorię i szaleństwo daleko- i blisko- siężnych planów Karradraela
* Czy Aleksandria-ish trafi na KADEM jako źródło wiedzy czy do naprawienia?

## Po czym poznam sukces

* Ilość konfliktów udanych dla Pawła i ogólnie, Pawłokształtnych++
* ALE - Siluria też ma swoje miejsce.
* Renata - dostanie lub nie, Aleksandria-ish - mamy odpowiedź na to pytanie
* Widać szaleństwo Karradraela i celatida aleksandryjskiego...

## Wynik z perspektywy celu

* Sukces - Paweł i Siluria bardzo użyteczni.
* Renata poprawiła swoją sytuację. Dyskusyjne (bo Quasar), ale jest lepiej
* Aleksandria trafia przede wszystkim do naprawienia. Ale - co się da nauczyć, to się nauczą
* Karradrael JEST szalony. Był. I to widać.
