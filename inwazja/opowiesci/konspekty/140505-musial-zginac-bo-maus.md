---
layout: inwazja-konspekt
title:  "Musiał zginąć, bo Maus"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140503 - Wołanie o pomoc (AW, Til (temp), Viv (temp))](140503-wolanie-o-pomoc.html)

### Chronologiczna

* [140503 - Wołanie o pomoc (AW, Til (temp), Viv (temp))](140503-wolanie-o-pomoc.html)

## Informacje dodatkowe:

**Chronologia nieco zaburzona przez dużą złożoność sesji, wielokrotne uzupełnianie informacji na różne tematy i żonglerkę, co, kiedy i jak.**

## Wątki

- Szlachta vs Kurtyna
- Rodzina Świecy
- Powrót Mausów

## Misja właściwa:

Zginął terminus. Młody, niedoświadczony... ale terminus. Zginęli magowie. Andrea nie mogła tego tak zostawić - musiała zrozumieć, co się stało, bez tego nie potrafiła sobie z tym poradzić.
Dlatego postanowiła uzyskać jak najwięcej się da informacji na temat Kornelii Modrzejewskiej.

Ku swojemu zdziwieniu dowiedziała się o niej niewiele, w dodatku nie bardzo była w stanie znaleźć na niej jakieś haki.
Jakkolwiek Kornelia jest profesjonalistką i bardzo kompetentnym lekarzem, to jest również dekadencka, rozpolitykowana (udziela się w "młodej elicie" SŚ). "Młoda elita" to grupa magów uważających, że zbyt wiele jest w rękach starych, potężnych rodów. Kornelia w ciągu ostatnich dni zrobiła wystąpienie mające uderzyć w terminusów (bo są ze starych, potężnych rodów... na przykład Agrest :P ) i zakwestionować ich profesjonalizm, jednak zostało ono storpedowane, a sama Kornelia skompromitowana.

Andrea poszła do Mariana Agresta, by uzyskać upoważnienia do grzebania w aktach i dowiedzieć się nieco więcej oraz uzyskać (choćby półoficjalne) wsparcie.
Powiedziała mu, że cała sytuacja się jej nie podoba, a śmierć terminusa była niepotrzebna i bezsensowna... Agrest się zgodził, dodając od siebie, że otrzymał rozkaz zaakceptowania na akcji Kornelii.
Przyciśnięty przez Andreę stwierdził, że akceptacja Kornelii wygląda mu na sprawę polityczną, i że w tym, że zabiła terminusa on nie widzi złej woli, widzi panikę cywila, który robi coś, do czego cywil nie jest przyzwyczajony. Bardziej Agresta zastanawia Wuko - jak sam Wuko powiedział Agrestowi, zapłacono mu za bezstronność i aby pomógł w tej akcji. Andrea potwierdziła, że był przydatny i nie było po nim widać złych intencji. 
Andrea poprosiła Agresta, by ten pozwolił jej się tym zająć. 
Agrest nie tylko autoryzował Andrei wszystkie potrzebne akta, ale dodatkowo otworzył śledztwo, a Andreę uczynił prowadzącą sprawę. Zrobił to tak, by choć było to w papierach (bo musi), dać Andrei czas zanim ktoś się zainteresuje.
Podał również Andrei numer konta, z którego Wuko dostał zapłatę. Niestety, nie bardzo miał jak legalnie dowiedzieć się, do kogo ono należy... 
Agrest powiedział również, że w sprawę zamieszane były dwie siły - jedna, która chciała obecności Kornelii, i druga, zainteresowana obecnością Wuko. Prawdopodobnie niezależne od siebie.

Wypytany odnośnie młodego terminusa, Agrest stwierdził, że chłopak wziął urlop na żądanie - wszystko zgodnie z procedurami. Jego dowódca nie miał powodu mu odmawiać. Chłopak był oddanym i dobrym terminusem, nawet jeśli niedoświadczonym. Dowódca nie posyłał go w ten obszar.


Skończywszy rozmowę z Agrestem i uzyskawszy potrzebne uprawnienia, Andrea, nie przejmując się legalnością, od razu rozpuściła wici.
Stosunkowo szybko dostała informacje o właścicielu konta, z którego Wuko dostał zapłatę. Ta informacja zbiła ją z tropu. Franciszek Maus? Jedyny Maus na KADEMie? Postanowiła poczekać z kontaktem z tą osobą, dopóki nie dowie się czegoś więcej.

Jednocześnie skupiła się na historii młodego terminusa. 
Dowiedziała się, że pochodził z Mausów, ale jego ojciec stracił moc (matka nigdy jej nie miała) i został odebrany rodzicom i przekazany do Rodziny Świecy. Andrea nie rozumiała, dlaczego nigdy nie trafił do Mausów, więc skupiła się na samym rodzie Maus.

To, czego się dowiedziała, lekko ją zaskoczyło.
Od czasu Zaćmienia, żaden mag nie trafił na wychowanie do Mausów. Co więcej, nie było żadnej rodziny Maus w programie i do dziś nie ma. Zgłoszenia zaczęły się pojawiać po Zaćmieniu, ale wszystkie wnioski zostały odrzucone nawet, jeśli powodem były przyczyny formalne, normalnie ignorowane. Wszystko wskazywało na to, że ktoś z rozmysłem odmawia im wstępu do programu. 
Andrea dotarła nawet do świetnie zakonspirowanego dokumentu, w którym patriarcha rodu Maus, Ernest protestuje przeciwko "planowi eksterminacji rodu Maus". Nadal jednak nie uzasadnia to, czemu młody terminus z rodu Maus trafił do Klotyldy Świątek. Okazało się (zgodnie z zapiskami martwego archiwisty, oraz po prześledzeniu rozkazów i dokumentów w biurokracji SŚ), że w chwili, w której odbierano młodego maga, nie było żadnego lorda Maus. Jonatan stracił moc, Renata jeszcze się nie ukonstytuowała, a Ernest nie został wybrany. Wszystkie prośby Mausów o oddanie im dziecka zostały odrzucone, a bez lorda nie mieli mocy przebicia.


Teraz Andrea skupiła się na tym, jaki był klucz doboru ofiar.
Okazały się to być dwa "źródła": ojciec i syn oraz po prostu Rodzina Świecy.

W tym rejonie było więcej magów, a jednak tylko ci zostali zaatakowani. Nawet archiwista pasował do wzoru. Ale - o ile była w stanie się dowiedzieć, chłopak był u Klotyldy szczęśliwy i raczej nie był inicjatorem. To podsunęło Andrei, że być może Kwiatuszek też ma jakieś powiązania z Rodziną Świecy.
Dlatego podążyła za tropem dziewczyny.
W dużym skrócie dowiedziała się, że Kwiatuszek była córką druidów, którzy zostali zabici przez kralotha, a ona sama została przejęta i zniewolona. Kraloth przygotowywał ją na swoje podobieństwo i szkolił w umiejętnościach defilerskich, w tym poznała pewne rytuały kralothów. W jakiś sposób udało jej się uwolnić do tego stopnia, że zabiła kralotha częściowo swoim kosztem. Potem, według raportów, Kwiatuszek była w Rodzinie Świecy przez niecały tydzień, po czym została przekazana mistrzowi Wołkowcowi. Potem miał miejsce "jakiś skandal", po którym Kwiatuszek została wydalona z SŚ. Uciekła, dołączyła do gildii planktonowej i poza raportami tien Izabeli Łaniewskiej, niewiele na jej temat wiadomo.
To, co Andreę najbardziej zaciekawiło, to to, że te akta nie mogły być do końca prawdziwe. Coś tam po prostu nie pasowało. Dotarła też do raportu mistrza Wołkowca do przywódcy frakcji reformatorskiej SŚ, tien Astera, w którym Wołkowiec pisał, że SŚ "zrobiła krzywdę" Kwiatuszkowi i że jej stan pogorszył się nagle. Dodatkowo, gdy poszła za tym śladem, zauważyła, że po wydaleniu Kwiatuszka z SŚ tien Aster uzyskał poparcie frakcji konserwatywnej w kilku kluczowych głosowaniach. Andrea stwierdziła, że wątek ten jest bardzo niebezpieczny i chwilowo zdecydowała się zostawić tą sprawę.

Następnym krokiem Andrei było skierowanie się w stronę rdzenia IR.
Aktualnie rdzeń znajduje się w laboratorium Świecy. Zgodnie z informacjami Agresta, Weinerowie chcieli mieć dostęp do rdzenia IR, ale Agrest ich zablokował, "by nie utrudniali śledztwa". Co ciekawe, dostępu do rdzenia zażądał Seirass Jan Weiner, patriarcha rodu. Normalnie Seirassowi się nie odmawia. Agrest nie jest normalny. Zapytany, o co może chodzić, Agrest zwrócił Andrei uwagę na to, że z rdzeniem IR powiązany jest Powiew Świeżości, którym dowodzi córka Jana Weinera, Julia.

Andreę zaskoczył zupełny brak spójności miejsc. Rdzeń IR pierwotnie znajdował się zupełnie gdzie indziej, Kwiatuszek zgodnie z raportami zginęła w zupełnie innym miejscu i oba z tych miejsc nie były tym, gdzie Andrea rdzeń znalazła. Drążąc historię zniszczenia rdzenia odkryła, że zniszczony rdzeń trafił do magazynów Świecy, skąd rozkazał zabrać go Jan Weiner, zresztą nieszczególnie ukrywając ten fakt.
Rozmowę z głową rodu Andrea odłożyła na później.


Zamiast tego postanowiła podążyć tropem Kwiatuszka. 
Jedyna osoba, która występowała w większości raportów o dziewczynie był mistrz Wołkowiec (Izabela Łaniewska była jego podopieczną)
Andrea poszukała Wołkowca. O dziwo, żył. Okazało się, że podczas Zaćmienia utracił moc. W obliczu tej informacji Andrea myślała, że wszystko stracone, na szczęście mistrz Wołkowiec uniknął wymazania pamięci. Objęły go działania fundacji SŚ "Justicar", założonej przez Barbarę Sowińską a mającej na celu ochronę magów, którzy utracili moc, a posiadali cenną wiedzę. Zgodnie z raportami, do których dotarła Andrea, Wołkowiec nadal kontaktował się z tien Izabelą Łaniewską, ale poza służeniem pomocą, raczej wycofał się z czynnego życia Świecy. Tym łatwiejsze okazała się dyskretna rozmowa z Wołkowcem.

Wołkowiec opowiedział Andrei wszystko, co wiedział. Wyraźnie temat bardzo go smucił. Jego zdaniem, zarówno on, jak i Świeca, zawiedli Kwiatuszka. Ogólnie, Wołkowiec myślał, że to bio, które dostała Andrea, było prawdziwe. Nie widział tam szczególnych niespójności. Za to powiedział Andrei, że gdy Kwiatuszek była pod jego opieką, jej stan zdecydowanie się poprawiał. Wołkowiec współpracował z Kaliną Woźnicą psychologiem (nie żyje), która mówiła, że dla Kwiatuszka jest nadzieja. Jednak nagle gwałtownie jej się pogorszyło. Doszło do pewnego incydentu, w wyniku którego bardzo ciężko ucierpiało kilku magów frakcji konserwatywnej. Zdaniem Wołkowca przez to Aster miał poważne kłopoty próbując go chronić i Wołkowiec wybłagał, by Kwiatuszek została wydalona a nie zabita (Andrea wie, że Aster miał z tego korzyści, co zupełnie nie ma sensu, bo ucierpieli konserwatyści...) Zdaniem Wołkowca, Kwiatuszek pochodziła z rodu całkowicie nieznaczącego w SŚ, a jej rodzice nie mieli nic wspólnego ze Świecą. Dodatkowo Wołkowiec, który dużo energii poświęcił na badanie i pomoc Kwiatuszkowi, dał Andrei cenną wskazówkę. Kraloth bardzo głęboko skaził Kwiatuszka. Jej skażenie objawiło się własnościami kralotycznymi i ogromnym rozproszeniem uwagi. "Niewłaściwym postrzeganiem rzeczywistości" - Kwiatuszek zupełnie inaczej interpretowała rzeczywistość niż inni magowie, miała też trochę nieludzką psychikę.
Porozmawiali jeszcze trochę i Wołkowiec powiedział, że poprosi Izę, by odpowiedziała na pytania Andrei. Iza może wiedzieć coś więcej.

Andrea udała się z pośrednim raportem do Agresta, podsumowując podstawowe informacje. Agrest powiedział, że udało mu się dotrzeć do zakamuflowanych raportów o "złych rzeczach", które działy się w jakiejś Rodzinie Świecy. Nie udało mu się dotrzeć do szczegółów, ale to były tematy o wysokim stopniu dostępu. Zostały utajnione przez przywódców wszystkich frakcji. To jednak wystarczyło Andrei, by poprzeć jej podejrzenia co do fabrykacji przynajmniej części historii Kwiatuszka.

Następnie udała się do laboratorium, w którym badane było to, co z rdzenia zostało. Tamtejsi naukowcy stwierdzili, że nie mają pojęcia, dlaczego rdzeń działa i co dokładnie robi. Został zaprojektowany i zbudowany przez Aurelię Maus, współpracującą z Marianem Welkratem i zawierał jakieś elementy Inwazji. To, co tu się znajduje, ma się do tego nijak. Zostało to przebudowane, ktoś przerobił rdzeń, by zbierał energię, akumulował z otoczenia i po prostu uziemiał. Innymi słowy, taki "piorunochron magiczny". Zgodnie z ich wiedzą, rdzeń tak działał przez dłuższy czas, aż w pewnym momencie został skażony. Problem polegał na tym, że zgodnie z datowaniem, rdzeń został skażony co najmniej rok po śmierci Kwiatuszka.
Pięknie... teraz nie zgadzał się ani czas ani lokalizacja. 
Andreę zastanowił znowu Marian Welkrat. Jest to mag Powiewu Świeżości... Ale czy Jan Weiner działałby w tak bezczelny sposób..?


Następnym krokiem było spotkanie z Izą Łaniewską.
Terminuska wywarła na terminusce dobre wrażenie. Łatwo się dogadały. Andrea nie miała problemu z brakiem mocy u Wołkowca, a Iza podpadła już tylu wrogom, że w Srebrnej Świecy była przede wszystkim dla Wołkowca. 
Iza powiedziała Andrei coś wstrząsającego: Kwiatuszek zginęła z ręki viciniusa współpracującego czasami z Powiewem Świeżości (a dokładniej z Kamilą Maliniak) imieniem Cierń. W okolicach pojawienia się Agenta Inwazji "Praktykanta". Ale to było rok przed tym jak Jan Weiner wycofał rdzeń z magazynów.
Super...
Iza opowiedziała Andrei o Powiewie Świeżości, gildii "kompetentnie niekompetentnej", w której przez dłuższy czas działała Aurelia Maus (kolejny szok dla Andrei) i która - nie wiedząc o tym - wyhodowała Księżniczkę Inwazji. No pięknie... Dopiero na zapewnienia Izy Andrea uwierzyła, że ta gildia nie jest Zła.
Od Izy Andrea dowiedziała się, że Kwiatuszek zabiła swojego kralotha wydostając się spod uroku kralotha, co dla samej Izy brzmiało nieprawdopodobnie, ale taka była prawda. Andrea zdecydowała się zaufać Izie i powiedzieć jej o Asterze i niezgodności pomiędzy statusem Astera i Wołkowca. Czarodziejka się zasępiła i powiedziała, że dawno temu uczestniczyła w całkowicie nie rejestrowanej akcji przeciwko Asterowi (przypadkowej), w której Aster wykorzystywał kralothy przeciwko ludziom (patrz: sesja "sojusz z piekła rodem")
Iza też objaśniła Andrei, czym mniej więcej jest Cierń. Koci vicinius, zaprojektowany do zabijania magów. Skuteczny, niebezpieczny, okrutny, ale nie poluje aktywnie. Iza stwierdziła, że nie pokonałaby Ciernia.
I jeszcze jedno - Iza zaznaczyła, że były próby zabójstwa Kwiatuszka po jej opuszczeniu SŚ. 

Andrei zaczął rysować się obraz Kwiatuszka. 
Osoby o niespójnej historii, niewiadomych mocach, która potrafi skazić rdzeń rok po swojej śmierci, a który to rdzeń znajduje się w zupełnie niewłaściwym miejscu. W dodatku pierwszy przypadek, gdzie mag lub człowiek przezwycięża urok kralotha...
W skrócie: chyba najdziwniejszy mag jaki kiedykolwiek istniał.

Andreę strasznie męczyło to, jak możliwe jest zerwanie kontroli kralotha...
Sięgnęła więc do listy swoich znajomości i poszła do domu publicznego, gdzie można było z kralotem porozmawiać.
Wpierw jednak załatwiła środki, które dawały jej odporność na feromony kralotha (ograniczoną, ale zawsze) i gotowe do użycia obrazy mentalne, by nie musiała łączyć się z kralothem (obawiając się, jak to się może skończyć).

Hlargahlotl nieco zdziwił się zleceniem, ale nasz klient, nasz pan. Zwłaszcza, że liczył, że i tak się naje.
Andrea przesłała mu przygotowane obrazy, przedstawiając historię Kwiatuszka od momentu przejęcia przez kralotha do momentu jej uwolnienia.
Dowiedziała się, że możliwe jest wyszkolenie człowieka przez kralotha, jak również, że odpowiednio "wyszkolony" mag może nauczyć się rytuałów kralothów. Tknięta przeczuciem, zapytała, czy istnieje możliwość, by kraloth przeszedł w "formę przetrwalnikową" i czy możliwe jest, by mag nauczył się tego rytuału. 
O, dziwo, tak. Na oba pytania. Co więcej, taki "przetrwalnik", jeśli zainfekuje żywą istotę, skazi ją i przekształci do wzoru oryginału. Kraloth nie miał pojęcia, jak taki rytuał zadziała użyty przez człowieka, ale normalnie przetrwalnik zdolny jest do zainfekowania innego kralotha.
Możliwe jest również wielokrotne wykorzystanie rytuału, ale za każdym razem, rzucający traci coś z siebie. Kralothom to aż tak nie szkodzi, ponieważ kraloth w ten sposób przejmuje osobowość, formę i własności zainfekowanego kralotha. W wypadku ludzi po prostu nie ma danych, ale mogą nie być w stanie się odbudować.
Aczkolwiek, gdyby Kwiatuszek użyła tego rytuału i zainfekowała swojego kralotha to najpewniej dostałaby własności kralotyczne...
Również, takim przetrwalnikiem można zainfekować węzeł (również rdzeń IR)
W wypadku, gdy rytuał miał ratować życie, przetrwalnik najpewniej zainfekowałby zabójcę, lub - w razie niemożliwości - przyczepiłby się do niego, szukając innej ofiary.
W tym momencie Andrea zrozumiała mniej więcej, co miał zrobić promień uderzający w syna Klotyldy i ostatnie resztki poczucia winy za wrzucenie tam biologicznego ojca młodego terminusa jej przeszły.
Na pytanie w jaki sposób dziewczyna mogła się wyrwać kralothowi, Hlargahlotl odparł, że nie mogła, ktoś musiał w jakiś sposób zerwać władzę nad dziewczyną.

Czyli, choć Andrea miała sensowną i w miarę spójną teorię, najważniejsze pytanie pozostawało bez odpowiedzi.
Andrea uciekła z pokoju w ostatniej chwili, pozostawiając napalonego kralotha samemu sobie. Wiedziała, że znajdzie się co najmniej kilkoro chętnych zaspokoić jego głód, ona nieszczególnie miała na to tym razem ochotę.

Po ochłonięciu jej następnym krokiem była wizyta u Franciszka Mausa.
Andrea postanowiła spróbować bezpośredniego podejścia i zapytała wprost, dlaczego wynajął Wuko.
Franciszek odparł, że chodziło o jego ród, że Wuko miał być obiektywnym obserwatorem, który będzie również katalizatorem, a przy okazji pomoże.
Mimo, że jest magiem KADEMu, nadal jest Mausem i zależy mu na rodzie i jego problemem było to, że ród Maus wymiera. Że SŚ aktywnie dąży do wyginięcia rodu uniemożliwiając mu zdobywanie nowych magów.
Tego już się Andrea domyśliła. Franciszek powiedział, że tak nie może być, że to się musi zmienić, zwłaszcza, że problemy, które były z rodem Maus się skończyły. Wytłumaczył, że Srebrna Świeca zablokowała Mausów z Rodziny, gdyż Jonatan wstrzykiwał magom krew Karradraela, przekształcając ich w "Mausów". Ale teraz Karradrael nie istnieje. Nie może zostać przyzwany.
Andrea zdecydowała się przycisnąć Franciszka by zdobyć jak najwięcej informacji. Zwłaszcza, że ród Maus to eksperci od magii demonicznej, magii krwi oraz katalizy, więc Franciszek może mieć wiedzę odnośnie specyficznych działań kralothów. Franciszek wymógł na Andrei żeby rozwiązała problem z wymieraniem Mausów (blokada Rodziny Świecy) W innym wypadku stanie się coś złego (tak powiedział, ale nie była to groźba dla Andrei) Po obietnicy Maus powiedział, że każdy Maus mógł być przejęty przez Karradraela i opisał w skrócie jak działał Karradrael. I że ogromnym szokiem dla wszystkich było to, że Renata mogła przyzwać Karradraela. Znowu przyciśnięty, powiedział, że Renata była czarodziejką spoza rodu Maus, której jako dziecku wstrzyknięto krew Karradraela. Czyli nie była rodzoną córką Jonatana Mausa. Zapytany powiedział, że chwilowo Renata jest w rękach Świecy, on nie wie dokładnie czyich i w jakim jest stanie. Gdy Andrea spytała, w jaki sposób rdzeń IR mógł przywrócić utraconą pamięć ojcu terminusa, ten odparł, że to się da zrobić. Istnieją rytuały magii krwi, które wymagają pokrewieństwa. Ale implikacja tego jest taka, że... ojciec mógł przywrócić pamięć synowi, bo są bezpośrednio spokrewnieni. Ale to implikuje, że między Kwiatuszkiem a Mausem istnieje jakieś pokrewieństwo. Czyżby Kwiatuszek była Mausem..? Franciszek obiecał, że przyjrzy się temu tematowi.

Andrea wyszła zadowolona. Uzyskała sojusznika i dostała tylko jedno niemożliwe zadanie.

Teraz Andrea wie dość, by móc spotkać się z Klotyldą Świątek i dowiedzieć się, co ona wiedziała o Kornelii.
Lekarze Klotyldy powiedzieli Andrei, że ta nie czuje żalu po mężu i przybranym synu. Dodatkowo, jej reakcje są dziwne. Zwłaszcza pod kątem seksualnym i samokontroli. Andrea od razu to skojarzyła z mocami kralotha.
Po rozmowie z Klotyldą, która niczego nie pamięta na temat Kornelii, Andrea zasugerowała lekarzom wpływy kralotha. Innymi słowy, wskrzeszenie czarodziejki w polu skażenia spowodowało, że część cech Kwiatuszka przepłynęła na Klotyldę. W chwili obecnej musi być wycofana z Rodziny Świecy. Co więcej, może być potrzeba, by chronić przed nią jej własne dziecko. Andrea wysłała odpowiednią notkę Agrestowi. W czym, dzięki prawidłowemu rozpoznaniu ze strony Andrei będzie dało jej się pomóc. Po prostu będzie to wymagało długiej i złożonej rehabilitacji. A Kornelia bardzo skutecznie zatarła wszystkie ślady.

Ostatnim całkowicie niejasnym punktem było skąd rdzeń IR wziął się tam, gdzie znalazła go Andrea. Po dłuższym zastanowieniu postanowiła zatrzymać wszystko w ramach gildii i zamiast iść do Powiewu, poprosiła o nieoficjalną rozmowę Jana Weinera.
Patriarcha zdecydował się z nią spotkać. Po krótkim wybadaniu Andrei i bezpośrednim pytaniu zdecydował się jej odpowiedzieć na pytania. Pod ziemią, głębiej niż rdzeń, ukryte jest silne źródło energii. To źródło może spowodować katastrofalne skażenie i burze magiczne. W związku z czym Marian Welkrat przerobił rdzeń IR, by służył jako uziemienie.
Andrea powiedziała mu o skażeniu rdzenia przez Kwiatuszka, choć nie użyła imienia, o zamieszaniu w sprawę Aurelii i Julii, o powiązaniach Mausów, o śmierci terminusa i że będzie zmuszona zrobić z tego oficjalne śledztwo, chyba, że się dowie, o co chodzi. Jan Weiner zdecydował się odpowiedzieć. Gdy Aurelia była pod kontrolą Karradraela wraz ze wszystkimi magami rodu Maus pracowała nad super fortecą. Pojazd ten nosi nazwę "Lord Jonatan", miał być odpowiedzią Mausów na Ruination i to właśnie jego szukała Renata po Zaćmieniu. Tam właśnie zginęła Aurelia, zabierając ze sobą Karradraela i uniemożliwiając Renacie przejęcie fortecy. Niestety, Karradrael jest wymagany do uruchomienia i kontrolowania Lorda Jonatana, w związku z czym jedynym magiem zdolnym do kontroli tej fortecy jest Renata. Gdy doszło do wycieku z jednego z czterech reaktorów było konieczne wprowadzenie uziemienia i stąd wziął się rdzeń.

Jan Weiner zapowiedział, że wyślę Julię (jedna z nielicznych, która wie o Lordzie Jonatanie) na odkażenie terenu i spróbuje znaleźć inne rozwiązanie uziemienia.
A Andrea nie wie, jak napisać raport... A raczej, musi wybrać, co przemilczeć.

A Franciszek Maus potwierdził, że Kwiatuszek pochodziła z Mausów.

# Streszczenie

Okazało się, że serbski terminus Vuko został opłacony przez Franciszka Mausa - Mausa na KADEMie (by pomóc rodowi i obserwować co się dzieje). Okazało się też, że od bardzo dawna Mausowie nie dostawali magów z Rodziny Świecy; jakby gildia grała na wyniszczenie rodu. Okazało się też, że Kwiatuszek z krwi była Mausówną. Dodatkowo: Rdzeń Interradiacyjny chciał zabrać do badań Jan Weiner (seras), ale Agrest go zablokował. Przyciśnięty, seras Weiner powiedział o "Lordzie Jonatanie" i że Rdzeń IR służył jako uziemienie. Widać: nie do końca czynne. Dodatkowo: mnóstwo przypomnienia systemu Zaćmienia i formy przetrwalnikowe kralothów ;-).

# Zasługi

* mag: Andrea Wilgacz jako śledcza próbująca zrozumieć prawdę o Rodzinie Świecy i Mausach
* mag: Karol Maus jako młody terminus który już nie żyje.
* mag: Kornelia Modrzejewska jako tajemnicza lekarka poza zasięgiem wymiaru sprawiedliwości
* mag: Marian Agrest jako wiarygodne i pomocne źródło informacji oraz wpływów dla Andrei
* mag: Vuko Milić jako serbski mag-najemnik który był wynajęty przez seirasa Mausa
* mag: Franciszek Maus jako jedyny Maus na KADEMie, który pomaga Andrei oraz nadal chroni swój ród.
* mag: Ernest Maus jako seiras Maus, który nie chce powrotu Karradraela oraz czasów gdy Renata rządziła w imieniu Jonatana Mausa
* mag: Jan Weiner jako seiras Weiner, który zna tajemnicę co kryje się pod Rdzeniem Interradiacyjnym
* mag: Kwiatuszek jako wspomnienie córki druidów która przejęła moce kralothów tracąc człowieczeństwo
* czł: Adam Wołkowiec jako były mag SŚ o dobrym sercu, aktualnie pod ochroną fundacji 'Justicar'
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, ostra i bezwzględna terminuska lojalna Wołkowcowi niezależnie od tego co na to Świeca
* mag: Aurelia Maus jako kontrolowana przez Karradraela kreatorka Lorda Jonatana
* mag: Marian Welkrat jako najbliższy współpracowanik Aurelii i współtwórca Rdzenia Interradiacyjnego
* mag: Renata Maus jako legenda pokonana kosztem życia przez Aurelię Maus
* mag: Jonatan Maus jako wspomnienie najpotężniejszego i najokrutniejszego seirasa Mausa jakiego zna historia
* vic: Karradrael jako uśpiony aderialith Mausów którego kontrolować może jedynie Renata
* mag: Tadeusz Aster jako mistrz, wspomnienie przykładu jak mag SŚ frakcji reformatorów współpracował z viciniusami przeciw ludziom
* vic: Hlargahlotl jako kraloth z domu publicznego będący źródłem informacji Andrei na temat kralothów
* vic: Cierń jako wspomnienie morderczego viciniusa kontrolowanego przez Kamilę
* mag: Klotylda Świątek jako ofiara wskrzeszenia która właściwie przesunęła się psychicznie

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Myśliński
                1. Myślin
                    1. Okolice
                        1. Bieda-szyb (nielegalne wysypisko śmieci)
                    1. Grota z Rdzeniem Interradiacyjnym
                    1. Lokalizacja "Lorda Jonatana"
