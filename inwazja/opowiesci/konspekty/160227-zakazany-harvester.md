---
layout: inwazja-konspekt
title:  "Zakazany harvester"
campaign: rezydentka-krukowa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160131 - Dziwny Transmiter Weinerów (PT)](160131-dziwny-transmiter-weinerow.html)

### Chronologiczna

* [160131 - Dziwny Transmiter Weinerów (PT)](160131-dziwny-transmiter-weinerow.html)

## Punkt zerowy:

Bólokłąb, efemeryda, została zniszczona.
Higienistka trafiła do szpitala.
terminus Kajetan się pojawił.

## Misja właściwa:

### Faza 1: Akumulacja energii

Dzień 1:

Następnego dnia, Paulina poskładała Kajetana do kupy. Nie jest w idealnej formie, ale nadaje się do funkcjonowania. Kajetan studiuje artefakt z wielkim zainteresowaniem.
Mają do czynienia z artefaktem rozproszonym, czymś składającym się z wielu elementów. Kajetan powiedział, że nie wie jaka grupa Weinerów operuje na tym terenie; Weinerowie to bardzo niezależny ród podzielony na małe elementy. Zauważył, że nie wie o nikim i niczym operującym na tym terenie. Gdy Paulina powiedziała, że ona nie słyszała o żadnym magu operującym na tym terenie (bardzo mały poziom magii w okolicy), Kajetan stwierdził, że to idealne miejsce dla niewłaściwego maga z niewłaściwym artefaktem. To wygląda jak artefakt Weinerów, ale kombinowany. Powinien być już rozładowany w niemałym stopniu.

Kajetan powiedział, że musi wykonać parę telefonów i pobawić się z artefaktem. Paulina stwierdziła, że ona w takim razie zainteresuje się samą higienistką i jej stanem - pojedzie do miasta (Krukotargowa) zobaczyć w jakim jest stanie. Kajetan powiedział, że to trudny artefakt, bardzo złożony - ale nie jest niebezpieczny. Nie ma zabezpieczeń. 
Paulina zostawiła Kajetana, wzięła swój lekko pogruchotany samochód (i telefon w dobrym stanie) i pojechała do Krukotargowa - do szpitala. 

Szpital. Paulina spotkała się z recepcjonistką. Paulina poprosiła o informację o Barbarze Zacieszek i możliwość kontaktu z jej lekarzem; recepcjonistka powiedziała, że lekarzem jest dr Stryczek. Barbara Zacieszek jest przygotowana do tomografii; miała zapaść. Paulina poszła zobaczyć się z lekarzem; idąc na spotkanie z nim zobaczyła lekkie odkształcenia magiczne w budynku. Jest tu coś magicznego. Lekarz nie wygląda magicznie; Paulina nie sprawdzała dokładnie. Lekarz powiedział, że słabła podczas walki z efemerydą; zapaść nastąpiła przy rozmontowaniu efemerydy. Teraz jej się poprawia. 

Paulina zdecydowała się przejść po szpitalu; zadzwoniła wpierw do Kajetana i powiedziała o sytuacji. Kajetan odesłał jej sygnaturę; niech szuka tego. MMSem. Dodatkowo poradził jej rzucić zaklęcie; to co on jej wysłał to jest sygnatura transmitera - jeśli to gdzieś znajdzie, gdzieś tu jest transmiter (lub coś związanego z tego typu energią). Paulina wpierw przeszła się po szpitalu by znaleźć najsilniejsze punkty (punkt), po czym rzuciła zaklęcie wykrywające przepływy energii magicznej. Rzuca zaklęcie z ubikacji, centrując na transmiterze. Znalazła coś ciekawego. Jest tu kilka źródeł Skażenia. Jest sygnatura zgodna z sygnaturą transmitera, ale to dystrybutor (1:N); czyli jest więcej niż jeden transmiter (lokalizacja: chłodnia). Jest coś innego, nowy sygnał, podpięty do dystrybutora i to jest w okolicach tomografa. Jest KILKA ruszających się sygnałów zgodnych z sygnaturą "tomografa" oraz też podpiętych do dystrybutora.

Paulina zmieniła zdanie. Nie zgadza się na tomograf. Higienistka nie wpada to tej dziwnej maszyny. Paulina przygotowuje jakiś "negative condition", coś, co uniemożliwia tomografię i wymaga zajęcia się nią inaczej. Rzuciła czar na higienistkę. Pielegniarki rzuciły się na pomóc Barbarze, lekarz lekko ogłupiały... jednak kazał przygotować ją do tomografii. Widząc spłoszone pielęgniarki, użył swojego autorytetu. Paulina zauważyła, że on jest jednym z ruszających się sygnałów, acz unikalny. 

Oki. To się robi niepokojące - Paulina zdecydowała się rzucić zaklęcie na lekarza; niech ma grupę objawów wskazujących na udar. Nie od razu udar, ale dość, by się źle czuł i nikt nie dopuścił go do pacjentów. No problem - lekarz się osunął. Nagle pielęgniarki mają inny kryzys na głowie, nikt nie interesuje się Pauliną ani higienistką Barbarą Zacieszek. Zaraz przylecieli inni lekarze i Paulina została zignorowana; ma swobodę ruchów.

Przypomnę sygnały:
- lekarz miał unikalny sygnał <drona kontrolna>
- 3 sygnały ze szpitala, ruchome <drona podstawowa>
- w jednej z sal jest sygnał, wielokrotny, identyczny (pacjenci) <zasilanie magiczne>
- w chłodni (kostnicy) jest sygnał <emiter do transmiterów>
- tomograf ma sygnał <iniektor drenażu i homeostat>
- w okolicach prądnicy jest sygnał <zasilanie fizyczne>

W ogóle Paulina niespecjalnie ma co tu robić; nie jest lekarzem z tego szpitala...
Przeszła się po szpitalu i zidentyfikowała, że osoby wysyłające sygnał to: pielęgniarka, lekarz (z uprawnieniami do tomografu) i dyrektor szpitala. Też: dowiedziała się, że pacjenci wysyłający sygnał są na "umieralni" (OIOM).

Nie będąc w stanie zrobić sensownie nic więcej, Paulina udała się na stołówkę z nadzieją, że coś usłyszy. Niestety, wszyscy raczej skupiali się na rozmowie o "swoim lekarzu" który dostał udar przy tomografie. Do Pauliny przysiadł się jednak przystojny facet. Przedstawił się jako Feliks Szczęśliwiec. Dziennikarz. Wyciągnął od Pauliny, że jest lekarzem i że jest tu coś dziwnego i że ona chce coś z tym zrobić. Szczęśliwiec powiedział, że jego koleżanka, Ilona Maczatek, jest jedną z osób w komie. Ksiądz też - wziął jakieś leki i padł. Ilona jest jedną z siedmiu na "umieralni" jak zwą OIOM. Znaleźli ją w Krukowie Czarnym; była w tym szpitalu na zwykłym badaniu medycyny pracy, potem wysłała SMSa że ma coś niesamowitego a potem znaleźli ją w Krukowie.
I w notatniku miała zapisane o tym, że ją boli i ściany się zamykają coraz bardziej. Feliks powiedział Paulinie, że ma dokumenty i ona może go odwiedzić.

Podszedł do nich doktor Szipinik i chciał odgonić Feliksa; powiedział, że to nie kosmici czy inne takie. Paulina powiedziała, że nie ma problemu; Szipinik powiedział, że nie może powiedzieć o co naprawdę chodzi, bo Szczęśliwiec nie ma upoważnień.

Paulina wróciła do Kajetana. Niechętnie, ale trzeba.

### Faza 2: Badanie Harvestera

Dzień 2:

Kajetan powiedział, że dał radę przejąć kontrolę nad Transmiterem. Jest techorganicznym artefaktem. Jest to Transmiter, powiązany z Emiterem. Kajetan powiedział, że ma kontrolę nad Transmiterem; był to artefakt Weinerów, ale został przekształcony przez jakiegoś viciniusa. Viciniusa, bo nie jest to umysł ludzki; mag by to zrobił inaczej. Jest to iteracyjne, dziwne przekształcenie. Gdy Paulina powiedziała, że ona nie do końca chciałaby być powiązana z wchodzeniem do cudzych szpitali, Kajetan powiedział, że może jej ściągnąć Skórowiec. Skórowiec Kajetana przekształci ją w dwórkę za czasów Kazimierza Wielkiego; może zmienić ubranie i fryzurę, ale będzie wyglądać jak dwórka póki ma na sobie Skórowiec. Jest to organiczny artefakt.
Kajetan go ściąga dla Pauliny.

Paulina zdecydowała się połączyć z Transmiterem katalitycznie by sprawdzić i zlinkować się z Emiterem. 

ESCALATION_160227

Paulina chce:
- dowiedzieć się z czym ma do czynienia 
- skąd się bierze ta energia
- jak się ma energia do aur w szpitalu
- czy to była jedyna efemeryda? Czy to jakaś sieć?

Harvester chce:
- wydrenować Paulinę z energii
- zabrać Paulinie tydzień czasu (krótka katatonia)
- wejść na podniesiony poziom alarmu
- dostać autoryzację do autonomii

Krok 1/5: 
Paulina podłącza się do Emitera przez Transmiter (trudność 'ekspert w normalnych warunkach' = 15).
Harvester: brak ruchu

Paulina poświęciła trochę czasu i bez kłopotu podpięła się do Emitera niewykryta. Harvester nie ma pojęcia o podpięciu Pauliny.

Krok 2/5:
Harvester: próbuje wykryć i wyizolować obecność Pauliny (namierzyć w swoich systemach by zaatakować po magii). ATK 12 vs SPN/kat.

Paulina znalazła się w środku bardzo złożonej sieci energetycznej z różnymi kolorami energii magicznej. Jest w środku bardzo skomplikowanego systemu wyglądającego jak internet. Jest na "routerze", na obrzeżach; troszkę "płynie pod prąd" emitowanej energii. Przez działania Kajetana jest "rana" i energia ciągle wypływa. Skomplikowane "antyciała energetyczne" próbują załatać odpływ energii, ciągle skanując zmieniające się sygnały ze strony drenu Kajetana. Właśnie to grozi wykryciem Pauliny. 

Paulina: go past.

Paulina używając swojej wiedzy medycznej, widząc, że to zachowuje się jak antyciała próbuje dostać się w głąb Harvestera i prześlizgnąć. 20 v 15. -> 8 przewagi, co prowadzi do tego, że uniknęła ataków i przeciwciał Harvestera. Paulina bezpiecznie znalazła się w środku Harvestera i może wykorzystać swój ograniczony czas wedle możliwości.

Krok 3/5:
Harvester: pasywne Skażenie Pauliny (atk 10).  

Wewnątrz tej dziwnej Sieci Paulina wykrywa ciągłe fluktuacje energii. Spore ilości energii są przesyłane w różnych kierunkach i moment nieodpowiedniego, nie-delikatnego działania może się wiązać otrzymaniem wiązki energii od Emitera i Skażenie. Paulina jest ostrożna.
W świetle powyższego Paulina zdecydowała się bardzo ostrożnie podotykać i poszukać co tu się dzieje. Połączenie katalistki i minion mastera powinno jej zapewnić jakąś możliwość dowiedzenia się czegoś o tych węzłach i typach energii Harvestera; i jaką rolę mają w tym wszystkim ci ludzie (i czy jest tu gdzieś ta aura szpitala)?

Paulina: vs: Ekspert_nrm, Ekspert_h, Elita_h | 15, 19, 22. Paulina ma 17 -> 14. Nie udało się. Co gorsza, Harvester dał radę Skazić Paulinę; rozkaszlała się w realu i poczuła się źle, ale Harvester dalej jej nie znalazł.

Krok 4/5:
Harvester: pasywne Skażenie Pauliny (atk 12).  

Paulina jest Skażona, więc oddziaływanie na nią Harvestera działa mocniej. Jednak prawie już udało jej się rozwiązać poprzedni problem (#5 bonus). Daje to Paulinie 22 do rozplątania co się dzieje w środku Harvestera (vs 15, 19, 22). Niestety, Paulina ma (22->17). 
Rozplątując Harvestera, Paulina doszła do następujących faktów:
- Harvester ma 4 Transmiter. Ten był tylko jednym z nich. Dwa są nadal w szpitalu. Te Transmitery nie są aktywne, jeden jest "uruchamiany".
- Harvester jest częścią większej całości, większej "maszyny". Ale link jest martwy. Ta "reszta" nie działa - lub Harvester nie jest z resztą połączony.
- Czwarty Transmiter jest "gdzieś bardzo daleko". Kierunek wskazuje na to, że jest w kierunku reszty tej "maszyny".

Niestety, przebywanie dyskretne w środku Harvestera bardzo Paulinie nie służy. Próg Skażenia się pogłębił. Szczęśliwie, udało jej się ograniczyć przyszły wpływ Skażenia na siebie stawiając lekkie rozproszenie; nie mogła zbyt mocno, by nie zwrócić uwagi Harvestera.

Krok 5/5:
Harvester: pasywne Skażenie Pauliny (atk 11).  

Skażenie Pauliny jest już zauważalne (nie dla ludzi); Paulina zdecydowała się kontynuować natarcie. Dopóki Paulina nie wie dość, nie jest w stanie tak naprawdę wiele z tym zdziałać. Tak więc bada dalej:
- Czwarty Transmiter jest naprawdę wystarczająco daleko; to co ważne to to, że jest "kontrolowany" - jest elementem jakiegoś systemu.
- Paulina zrozumiała jak działają te aury; cały szpital jest elementem jednego systemu. Tego systemu w którego środku jest teraz Paulina. To magiczna sieć; Harvester. Ekstraktor energii.
- Energia pobierana jest od ludzi. Harvester bazuje na kilku ludziach, tych, którzy są w śpiączce. To ich energia jest przekazywana dalej.
- Ich obudzenie w większości nie będzie już możliwe. Spadli poniżej pewnego progu energii; Harvester działa jak połączenie Spustoszenia i Magii Krwi. Ale jest bezpieczny.
- Higienistka PRAWIE została elementem tego systemu. Tomograf jest "członem linkującym". 
- Lekarz wyłączony przez Paulinę to drona sterująca, kontrolna. Paulina wyłączyła kontroler. 
- System działa sam. Nie potrzebuje kontrolera do podejmowania decyzji; wyspecjalizuje nowego.
- W tej chwili system przygotowuje się do powołania nowego kontrolera i wyłączenia specjalizacji starego kontrolera.
- Lekarz miał unikalny sygnał <drona kontrolna>
- 3 sygnały ze szpitala, ruchome <drona podstawowa>
- W jednej z sal jest sygnał, wielokrotny, identyczny (pacjenci) <zasilanie magiczne>
- W chłodni (kostnicy) jest sygnał <emiter do transmiterów>
- tomograf ma sygnał <iniektor drenażu i homeostat>
- W okolicach prądnicy jest sygnał <zasilanie fizyczne>
- W mieście są jeszcze trzy sygnały dron zwykłych. Na posterunku policji i w ratuszu.
- Harvester nie potrafi utrzymać za dużej ilości dron. Jego limit (absolutny) to 10. On się powoli rozszerza, zarówno w obszarze budowy jak i harvestowania.

Harvester jest cudem techniki. To urządzenie nie jest inteligentne; jest to zaprojektowany układ samoregenerujący. Uszkodzenie jakiegokolwiek elementu nie wyłączy Harvestera. Żeby go zniszczyć, trzeba uderzyć w wiele elementów jednocześnie.
Paulina zobaczyła, że rozjarzył się tomograf; ktoś będzie go używał. 

Tym razem Paulinie udało się osłonić przed Skażeniem ze strony Harvestera. Odpromiennik zadziałał.

Paulina zorientowała się, co się tak naprawdę stało. Harvester jest "ślepy" i "głupi". On nie widzi swoich Transmiterów. Harvester zwyczajnie wysyłał całość energii do jednego Transmitera (tego do którego miał wysyłać); potem okazało się, że drugi Transmiter trafił do Krukowa Czarnego. To sprawiło, że Harvester zaczął wysyłać więcej energii do obu, ale musiał też poszerzyć swoje operacje. Stąd rozbudowa Harvestera. Jeśli Transmiter z Krukowa Czarnego trafi do szpitala z powrotem, Harvester zacznie obniżać próg operacji. Zacznie wygaszać itp. Zmniejsza to problem, choć go nie rozwiązuje; ale kupuje czas. Drenaż założony przez Kajetana prowadzi do tego, że Harvester zwiększa ciągle operację do granic swoich możliwości. Innymi słowy, ruchy Harvestera są pochodną tego, że Kajetan zadziałał a ktoś przed nim wyniósł Transmiter ze szpitala.

Harvester może tu leżeć już długo. To stary system. Po prostu on nie ma "ambicji".

ESCALATION_160227_END

Paulina skończyła poważnie Skażona (odpowiednik rany, nie lekkiej, ale nie ciężkiej; w skali [-5, 5] jest na -2). Ale się sporo dowiedziała.

Paulina podzieliła się tym, co wie z Kajetanem. Kajetan powiedział, że kiedyś słyszał o "projekcie Aleksandra" - grupa magów Weinerów chciała wykorzystać Spustoszenie by stworzyć samonapędzający system. Nawet konsultowali się z ekspertem od Spustoszenia. Projekt jednak nie został zaaprobowany i tyle Kajetan o nim słyszał. Obiecał, że się dowie. Na początku Kajetan miał wątpliwości - może da się coś z tym zrobić? Ale Paulina przypomniała, że jest tam Magia Krwi. Jako terminus, Kajetan się upewnił. Nie wchodzą w to. Harvester - nawet, jeśli dawałby bezpieczne quarki - musi zostać unieczynniony i zniszczony.

Paulina odwiozła Transmiter do szpitala. Położyła Transmiter w pobliżu drony; lekarz wziął Transmiter nawet nie zauważywszy czemu i co zrobił. Paulina się uśmiechnęła - kryzys częściowo zażegnany. Przynajmniej tymczasowo. Harvester co prawda zmieni dronę kontrolną, ale odciąży to ofiary Harvestera (każdy Transmiter ma ograniczoną przepustowość).

# Zasługi

* mag: Paulina Tarczyńska, odkryła mroczną prawdę za pojawieniem się efemerydy - Harvester Weinerów.
* mag: Kajetan Weiner, który stoi po stronie Pauliny, ludzi i zasad terminusa a nie po stronie swojego rodu.
* czł: Barbara Zacieszek, higienistka uratowana przez Paulinę przed zostaniem źródłem energii dla Harvestera.
* czł: Daniel Stryczek, lekarz (drona kontrolna) porażony przez Paulinę podstępnym zaklęciem gdy chciał użyć "tomografu" (iniektora).
* czł: Rebeka Czomnik, dyrektor szpitala (drona zwykła) która umożliwia Harvesterowi swobodne działanie w szpitalu.
* czł: Maja Stomaniek, pielęgniarka (drona zwykła) pilnująca, by proces drenażu ofiar Harvestera toczył się bezproblemowo.
* czł: Eustachy Szipinik, lekarz (drona zwykła); ma uprawnienia i dostęp do używania tomografu, zastępuje Daniela Stryczka.
* czł: Feliks Szczęśliwiec, dziennikarz, osoba skłonna z Pauliną rozmawiać i podzielić się podejrzeniami i pomysłami o tematach dziwnych i nienaturalnych.
* czł: Ilona Maczatek, dziennikarka, jedna z ofiar Harvestera (i ta, która wyniosła Transmiter ze szpitala rozpoczynając cykl problemów).

# Lokalizacje:

1. Krukowo Czarne
    1. Gabinet Pauliny
    1. Krukotargowo
        1. Szpital miejski