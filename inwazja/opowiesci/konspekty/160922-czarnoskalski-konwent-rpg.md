---
layout: inwazja-konspekt
title:  "Czarnoskalski konwent RPG"
campaign: taniec-lisci
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160922 - Czarnoskalski konwent RPG (Esme, temp)](160922-czarnoskalski-konwent-rpg.html)

### Chronologiczna

* [150604 - Proces bez szans wygrania (HB, SD, PS)](150604-proces-bez-szans-wygrania.html)

## Kontekst ogólny sytuacji

Przyjaciółka Kornela, Izabela Łaniewska została pokrzywdzona (jego zdaniem) przez Hektora Blakenbauera. Kornel jest magiem, który nie lekceważy sobie przyjaciół i sojuszy (w odróżnieniu od pewnych prokuratorów). Długo kombinował jak dobrać się do informacji o tym, co stało się z Izą, ale magowie Millennium do których mógł mieć dostęp nabierali wody w usta.

Jak nie chcą mówić, to należy ich zmusić...

## Punkt zerowy:

-

## Misja właściwa:

Dzień 1:

Czarnoskał, Klub sztuk walki "Liść". Esme i Dorota.
"Celem jest dowiedzenie się statusu TIEN Izabeli Łaniewskiej" - Dorota, podkreślając.

Czarnoskał, Klub sztuk walki "Liść". Tym razem Kornel i Mateusz.

Kornel przedstawił Esme Mateusza i nawzajem. 

"To... co wiemy?" - Esme
"Trzeba zorganizować imprezę... i to jest moja działka" - Mateusz

Esme powiedziała Mateuszowi, że potrzebne jest miejsce gdzie można kogoś wyprowadzić. Ogrodzone lub nie; otwarty obszar gdzie można wyprowadzić ofiarę. Zarówno Esme jak i Mateusz są w Świecy; Esme zdecydowała się na znalezienie takiego miejsca.
Esme znalazła takie miejsce; starą fabrykę porcelany. Niestety, nie jest tania...
Mateusz zajął się kreatywną księgowością; niech się uda to wszystko załatwić. Udało się, ale niestety w tamtym czasie jest tam inna impreza. Ludzka impreza - festiwal piwa. Wynajem hali się udał; niestety, trzeba załatwić rozwiązanie problemu "bandy pijanych januszy".

Mateusz robił księgowość parę razy dla browarów co mają powiązanie. Na pewno znają ludzi którzy to organizują. Chce się spotkać z Pawiakiem, CFO "Książę Czarnoskału". Zadzwonił - czy dałoby się przesunąć festiwal piwa? Tak, dałoby się. Jeśli "Czarny Tur" - mały acz obiecujący browarek nie da rady wystawić piwa. Mateusz się zastanowił i zgodził.

Esme do końca dnia zajmuje się obserwowaniem i badaniem fabryki. Wyczaić miejsca, określić miejsca na pułapki, ucieczki itp. Rozpoznanie terenu i sytuacji; spędza tyle czasu ile trzeba.

Esme poszła po sieci kontaktów; omija Millennium i próbuje się czegoś dowiedzieć na temat Supernowej. Złapała po hipernecie kolegę terminusa, Bolesława Derwisza, który był chętny rozmawiać - Esme wie, że to Diakonożerca. 
Bolesław powiedział, że Supernowa to wulkan energii, wszędzie jej pełno, ogólnie... fatalnie. Jest wielką rywalką córek Draconisa. Niegroźna, acz jej najczęstszym kochankiem jest Mikado Diakon. Mikado jest podejrzliwym i opiekuńczym magiem.

Esme ściągnęła dane ze Świecy - ma info o Mikado. Oki. Terminus. Niefajnie. Ale... można sfabrykować dla niego zagadkę nad którą będzie musiał się skupić. Co oderwie go od Supernowej. Esme musi wymyślić szczegóły, ale jest już jakiś plan podłożenia dziewczyny. I nocnych strachów.

Dzień 2:

Mateusz idzie w godzinach pracy Sanepidu do Sanepidu. Bajeruje panią (bardzo znudzoną), że w małym browarze "Czarny Tur" były pewne problemy. Przyniosła dokumenty których Mateusz potrzebował, te z "Czarnego Tura" - i Mateusz zmienił zaklęciem dokumenty. Oddał pani z Sanepidu i wyszedł z uśmiechem, bo problem został rozwiązany...

"Załatwić Ci... SENNE STRACHY... Esme?" - podłamany Kornel
"Pomyśl tak - Mikado pójdzie ratować dziewczynę przed sennymi strachami... a tu będzie tragedia" - uśmiechnięta Esme

Jeśli to cosplay, to Supernowa będzie miała na sobie pełnię uwagi. Więc warto ściągnąć kogoś kto może odwrócić uwagę ludzi od Supernowej. Mateusz zaproponował Cypriana Kozieja. Załatwił go nawet... obiecał mu panel dyskusyjny i w ogóle.

W dzień - normalna impreza. Wieczorem - ma być afterparty. Konkurs kostiumowy itp. I właśnie na afterparty celujemy w usunięcie nadmiarowych Diakonów i zajęcie się samą dziewczyną...

Esme zdobyła od Doroty artefakt - coś, za pomocą czegoś może odczytać pamięć Supernowej. Dostała też strzykawkę z pawulonem ;-).

Piróg Dolny. Przyjechał Mateusz i powiedział, że jest konwent. Supernowa w szlafroku zareagowała super entuzjastycznie - powiedziała, że KONIECZNIE pomoże. Mateusz próbuje się bronić, nie ma szans. Mikado też w szlafroku, zareagował MAŁO entuzjastycznie... Supernowa go zmęczyła i zgnębiła że musi jej pomóc z cosplaykam. Przyjedzie jutro.
Mikado zareagował rozpaczą, ale niewiele może poradzić.

Tymczasem Esme znalazła sobie ofiarę - Jessikę. Kornel ma Jessice nadać lekki, delikatny sygnał nakierowujący i wpuścił jej Nocne Strachy. Ta część planu jest mniej więcej opanowana. Jessica mieszka - jak Esme chciała - na drugim końcu Czarnoskału.

Dzień 3:

Praca wre. Kostiumy. Supernowa nie przyjechała.

Dzień 4:

Supernowa robi chaos. Przejęła dowodzenie i organizację. Mikado podejrzliwie szuka wszystkiego - ale nie znajduje pułapek. BO ICH NIE MA. Za to Supernowa nie może wytrzymać, jak Mikado czegoś nie robi - bo się może przydać. Terminus Millennium jest nieszczęśliwy...
Mateusz raduje się z całego serca - rozstawia Mikado po kątach.
Esme obserwuje.

Supernowa wzbudza podziw i zaangażowanie obecnych. Mikado pilnuje by jej się nic nie stało.

Dzień 5:

Konwent.
W okolicy zaczynają się kręcić dziewczynki z anime. Mikado ma oczy wszędzie. Esme go obserwuje - chce wiedzieć jak na niego reagować. Mikado przebrał się za szlachcica; ma rękę na szabli. Nic się nie dzieje na samym konwencie, konwentowo jest.

Pisarz przyszedł w epickim stroju klasy Borat. I w mega kapeluszu. Czarodzieja. "I've got a robe and a wizard's hat". Ludzie zwracają uwagę, tak na niego jak i na Supernową. Nie doceniliście jak bardzo Supernowa przyciąga uwagę.

Ogólnie konwent się udaje.

Mateusz zużył kilka swoich kontaktów i się pojawiają modelki. Odpowiednio przebrane. Takie "booth babes". Dzięki temu mają nadzieję na odwrócenie uwagi od Supernowej... udało się - ale później w lokalnej prasie będzie MORAL OUTRAGE. Co gorsza, hajs się nie będzie zgadzał...

Ale przynajmniej udało się odwrócić uwagę od Supernowej. Która - nie ukrywam - jest lekko niezadowolona z tego powodu...
Chciała aż pozbyć się stanika i przejść na bodypainting, ale Mikado ją zatrzymał.

Wieczór. Afterparty.

Świat się bawi, miód się leje, Supernowa pije, Mikado obserwuje zaniepokojony... na szczęście Supernowa umie pić. Supernowa UDAJE bardziej nietrzeźwą, by móc pozwolić sobie na więcej.
Jessica podeszła do Mikada; ten od razu wykrył magię. Poszedł porozmawiać z Supernową, ta nie zgadza się na pójście.

"Musisz iść ze mną, potrzebuję wsparcia ogniowego..." - MIkado próbujący oszukać Supernową
"KOLEJNA BUTELKA MIODU!" - Mateusz
"TAAAAAAK!" - Supernowa

Mikado się zgodził i poszedł sam, zostawiając Supernową z Mateuszem...
Esme odciągnęła Mateusza "księgowymi sprawami". Jest gotowa do działania.

Esme podeszła i dźgnęła Supernową strzykawką. Wzięła ją na ręce i zaniosła do pokoju organizatorów. W czasie gdy Mikado zajmował się sennymi strachami, ona użyła artefaktu Doroty i przeczytała co chciała przeczytać (i co chciał wiedzieć Kornel). Teraz Esme związała Supernową... i pomogła jej się obudzić.

"Er, bondage?" - lekko półprzytomna Supernowa

Esme nacisnęła - skrzywdziła Supernową by uzmysłowić jej że to nie zabawa i zrobiła jej siniaki. Gdy Supernowa próbowała nawiązać kontakt - czego Esme chce - ta ją zakneblowała. I zadała jej więcej bólu. Supernowa się BARDZO przestraszyła.

Mateusz wpadł przed drzwi. Esme rzuciła nożem (inscenizacja) i doszło do pokazowej walki. W końcu Esme ucieka.
Supernowa jest zauroczona, wdzięczna i szczęśliwa.

Po uwolnieniu przez Supernową Mateusz został obsypany całusami...

EPILOG:

Esme przekazała Kornelowi artefakt z danymi od Supernowy. Kornel przeczytał o stanie Izy... i jego hipernet zaczął migotać. Terminus zaczął tracić panowanie nad sobą. Esme delikatnie kazała mu się uspokoić; udało się.
Esme doszła do tego, że Kornel nie był 'kolegą' Izy. On ją kochał. Naprawdę kochał - a w pamięci Supernowej były informacje, że Iza ma zmieniany wzór i zmienianą osobowość.
Dorota Gacek weszła do pomieszczenia. Kornel jej wszystko przekazał. Nie objęli się, ale spojrzeli na siebie... ból jest wyraźnie widoczny.

Esme widziała już coś takiego. Natychmiast uruchomiła kanały alarmowe. Poinformowała innych terminusów (tych odpowiednich) o sytuacji, by Kornel i Dorota nie zostali sami, by nie stali się czarnymi terminusami. Po czym delikatnie się wycofała; w tym stanie mogliby - nie chcąc tego - naprawdę ją skrzywdzić z powodu czystych emocji...

Tymczasem księgowy Mateusz jest najszczęśliwszym księgowym pod słońcem. Ma dziewczynę, Supernową, udało mu się nawet wykręcić od trójkącika. Mikado poszedł znaleźć ukojenie w ramionach Jessiki...

# Progresja

* Kornel Wadera: terminusi kontrolni Świecy zainteresowali się jego działaniami i potencjalną zmianą w czarnego terminusa
* Dorota Gacek: terminusi kontrolni Świecy zainteresowali się jej działaniami i potencjalną zmianą w czarnego terminusa

# Streszczenie

Kornel i Dorota martwią się tym, że Hektor skazał Izę na Millennium. By dowiedzieć się co się z nią dzieje, zaangażowali Esme i wkręcili lokalnego księgowego podkochującego się w Diakonce by od owej Diakonki wyciągnąć info. Esme i Mateusz zrobili konwent RPG, doprowadzili do utraty płynności małego browaru "Czarny Tur" i wkręcili Supernową Diakon w to, że Mateusz uratował ją przed psychopatką. Mateusz i Supernowa zostali kochankami. Esme ostrzegła Świecę przed stanem Kornela i Doroty po tym, jak zobaczyła, jak oni przeżyli informacje o Infensie Diakon...

# Zasługi

* mag: Esme Myszeczka, uknuła plan, zwiotczyła Supernową i dała się pokonać księgowemu. Też: powiedziała Kornelowi o losie Izy (i info o stanie Kornela dalej).
* mag: Mateusz Ackmann, księgowy który przeżył dzień swojego życia i 'got the girl', "pokonał" terminuskę i zorganizował fajną imprezę RPG.
* mag: Kornel Wadera, kochający się w Izie radykalny terminus sponsorujący akcję zdobycia informacji co się z Izą stało... złamany i w furii gdy się dowiedział.
* mag: Dorota Gacek, bliska przyjaciółka Izy która robiła wszystko co mogła by dowiedzieć się co się z Izą stało. Złamana wiadomością. Pociesza Kornela...
* mag: Infensa Diakon, jeszcze: Iza Łaniewska, która jest w trakcie transformacji i rekonstrukcji w Millennium. Kornel i Dorota zbierają info na jej temat. Nieobecna.
* czł: Artur Pawiak, CFO w browarze "Książę Czarnoskału". Przesunął piwny festiwal za rozwiązanie małego problemu z konkurencją...
* mag: Bolesław Derwisz, powiedział Esme o Supernowej Diakon i ostrzegł przed potencjalną obecnością Mikada.
* czł: Cyprian Koziej, kontrowersyjny pisarz fantasy, przyciągający uwagę; nie był w stanie zamaskować obecności Supernowej (zbyt piękna i angażująca)
* czł: Rafał Kniaź, organizator konwentów RPG załatwiony przez Mateusza; stracił stanowisko na piedestale do Supernowej.
* czł: Jessica Ruczaj, losowa 19-letnia ofiara Nocnych Strachów, "anime girl". Esme zastawiła nią dywersję mającą odciągnąć uwagę Mikado od Supernowej.
* mag: Supernowa Diakon, źródło wiedzy o "Infensie"; by ją złapać zbudowano konwent RPG. Uratowana przez Mateusza, została jego kochanką ;-).
* mag: Mikado Diakon, podejrzliwy terminus wszędzie węszący pułapki... by Supernowa go nie posłuchała. Kochanek Supernowej. Aktualny.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Czarnoskał, miasto słynące z produkcji piwa różnego rodzaju a kiedyś z produkcji porcelany
                    1. Centrum
                        1. Biuro Sanepidu
                    1. Osiedle Techniki
                        1. Stara fabryka porcelany, gdzie zorganizowany został konwent RPG i przesunięto festiwal piwny ;-)
                    1. Komin
                        1. Klub sztuk walki Liść, gdzie znajduje się siedziba centralna Tańca Liści i 'dom' Kornela Wadery
                    1. Aleja Piwowarska
                        1. Mikrobrowar Czarny Tur, który utracił płynność finansową po działaniach księgowego Mateusza Ackmanna
                        1. Browar Książę Czarnoskału
                    1. Narzaniec
                        1. Ulica Homara, gdzie znajduje się mieszkanie Jessiki (z rodzicami)
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Osiedle Licht
                        1. Szeregowiec Pustynny, gdzie aktualnie mieszka Supernowa Diakon

# Skrypt

# Czas

* Opóźnienie: 7 dni
* Dni: 5
