---
layout: inwazja-konspekt
title:  "Renata Souris i echo Urbanka..."
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170226 - Wygraliśmy wojnę... prawda? (AW)](170226-wygralismy-wojne-prawda.html)

### Chronologiczna

* [170208 - Koniec wojny z Karradraelem (HB, SD)](170208-koniec-wojny-z-karradraelem.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Sumaryczna ilość punktów:

- Infensa: 6
- Prokuratura Magów: 5
- KADEM / Świeca: 5
- The Governess: 4
- GS Aegis 0003: 4
- Renata: 3
- Blutwurstka: 2
- Wiktor: 1

## Misja właściwa:

Dzień 1:

Renata poprosiła do siebie Hektora - stąd Edwin do niego przyszedł taki wesoły.

* Co Ty taki wesoły jesteś? - Hektor, do Edwina
* YOU CAN GET LAID ONCE IN YOUR LIFE - Edwin.
* Edwin, bądź poważny... to krew z naszej krwi.
* Twojej - Edwin, z uśmiechem.

Ciąg dalszy dyskusji.

* Polowanie na Mausów.
* Przecież Mausowie nie są niczemu winni! - Hektor z oburzeniem
* The Governess. - Edwin, spokojnie.
* Jeśli była sterowana przez Karradraela... musimy wiedzieć w jakim stopniu - Hektor
* Wiesz, masz perfekcyjną osobę do zapytania - Edwin, o Renacie. - Ona wie. Jeśli ktokolwiek wie, to ona.

Spotkanie.

* Jak się czujesz?
* Doskonale, Hektorze Blakenbauerze. Prokuratorze. Mój mieczu prawa. - Renata, spokojnie

Hektor zaczął rozmowę; Renata zapoznała się z datapadem. Renata potwierdziła. Potwierdziła, że Abelard nie zatrzyma 

* Karradrael jest singletonem. Jedną wolą. Jednym umysłem Mausów - Renata, spokojnie.
* Wszystko, czego dopuścili się Mausowie było tylko wolą Karradraela? - Hektor, zaskoczony
* Tak. - Renata, nadal spokojnie.

Niemożliwe. Hektor konfliktuje. 5v5. Przegrał; Renata powiedziała co chce.

Powiedziała, że Karradrael dowodził Mausami. Nie odparliby jego woli. Jak wiedzą, zrobią co sobie życzy. Jak odróżnić co zrobili z własnej woli? Zapytać Karradraela...

* Zabraliście Mausom geniusza i zostawiliście Abelarda. - Renata, zimno.

Renata powiedziała o Karolinie Maus. Sprzężonej z Aurelią. Szalonej. The Governess jest zbudowana przez Karradraela; gdyby ona tu była (Renata), problem by się rozwiązał. Świeca sama sobie zrobiła. Renata powiedziała Hektorowi, by ten się wzniósł nad swoje pragnienia... i uratował Karolinę. I Mausów.

* Jestem seirasem Mausów. To, że odcięliście mnie od mojego dziedzictwa nie znaczy, że przestałam być seirasem. - Renata, z furią.
* Jesteśmy tylko pionkami w rękach naszych rodów, Blakenbauerze. Tu się nic ode mnie nie różnisz. - Renata, z zimnym uśmiechem
* Pomogę Ci. Założymy niezależną prokuraturę, respektowaną przez rody i gildie. Spróbujemy uratować Mausów - Hektor, z nadzieją. - Ale potrzebuję Twojej pomocy. Wiedzy i kontaktów.
* Ty i KADEM macie duży wpływ. Większy niż myślicie. - Renata, spokojnie - Określcie, czego potrzebujecie by stworzyć prokuraturę... a Ci to dostarczę.

TYMCZASEM Siluria ma wiadomość od Quasar. Quasar przyszła do niej osobiście.

* Silurio, mamy problem. Udaremniłam już trzy zamachy na Renatę. - Quasar
* Kto? - Siluria, wstrząśnięta
* Trzech magów KADEMu. W czym jeden eks-AD. - Quasar, spokojnie.
* Czemu?
* Strach i chęć zemsty. Renata - ich zdaniem - jest zagrożeniem dla stabilności. - Quasar

Quasar zaznaczyła, że na dłuższą metę Wioletty nie będzie w stanie powstrzymać. Gdyby nie niespodziewana pomoc WHISPERWIND (ze wszystkich osób), Renata byłaby już martwa. 

* Czego ode mnie oczekujesz? - Siluria, z zaskoczeniem
* Napraw to - Quasar, beznamiętnie

Gdy Siluria powiedziała, że to nie jest takie proste, Quasar zaznaczyła, że nie ma do kogo innego się udać. Zwłaszcza, że Infernia podburza wszystkich przeciwko Renacie...

* Kto jeszcze jest mieszaczem? - Siluria, zrezygnowana
* Andżelika. Ignat. Infernia. Marian. Większość młodych magów. Rojowiec. A szczególnie Franciszek. Większość magów w ogóle. - Quasar - To, co ją ratuje to Urbanek.
* Co ma Urbanek do rzeczy? - Siluria, skonfundowana
* Kiedyś powiedział, że ją lubi - Quasar, całkowicie nie dotknięta - Ochronię ją dla niego. To samo Whisper. I Shadow.

Siluria poszła spotkać się z Renatą. Jest ktoś, komu trzeba pomóc...

* Wielu magów chce mnie zabić... a przynajmniej mam nadzieję - Renata, z uśmiechem, do Silurii
* Czemu tak myślisz? - zaskoczona Siluria
* Bo będą bardzo, bardzo rozczarowani - szeroko uśmiechnięta Renata 

Siluria powiedziała Renacie o Milenie. Coś zostało jej zrobione. Nie wie, co. Wie, że to jest powiązane z Mausami, ale nie wie co... Nie wie co ten Maus próbował jej zrobić, ale Siluria chce odzyskać zdrową kuzynkę.

* Nie masz dostępu do Abelarda? - autentycznie zdziwiona Renata - Karradrael to odwróci. Zwłaszcza mając do dyspozycji Lorda Jonatana i laboratorium.

Renata powiedziała, że ona może to odwrócić. Potrzebuje krew dowolnego Diakona i dowolnego Mausa. Nie umie tego wyjaśnić, ale umie to zrobić; jest odcięta od Karradraela. Siluria z radością by się zgodziła... ale jej nie ufa. Plus, ktoś najpewniej Renatę spróbuje zabić. Renata zaznaczyła, że Siluria uratowała jej życie. Życie za życie. Zaproponowała Silurii mindlink; Renata przekaże jej Hermetyczny Rytuał. Niestety, mimo propozycji i kuszenia Renaty, Siluria nie zdecydowała się na połączenie...

Renata podała Silurii jakiś dziwny, skomplikowany kod. Nie pamięta co to jest, ale jest to KLUCZ. Nie jest to powiązane z Mileną. Na wszelki wypadek podała też kod odwracający; Quasar powiedziała, że ma to; ma do tego dostęp.

Siluria odwiedziła w końcu Hektora. Ten zrobił jej miejsce, przerwa od czytania skomplikowanych dokumentów prawniczych...

Siluria powiedziała Hektorowi o próbie zamachów na Renatę. Magom KADEMu można ufać, że spróbują sami rozwiązać swoje problemy... a jej nastawienie nie pomaga.

Quasar powiedziała Silurii, że grupa magów KADEMu - Infernia, Ignat, Andżelika, Marian, Midnight (Maja Błyszczyk) - opuścili KADEM i poszli na Mare Vortex. Najpewniej konspirować przeciwko Renacie. Siluria poprosiła Quasar o wsparcie Whisper. Whisper się pojawiła. Zaproponowała, żeby ściągnęli Pryzmatem Kubę na KADEM. Ona sama w tym czasie zajmie się spowalnianiem tamtej czwórki...

Siluria poszła do tien Kopidoła. Kopidół zaprotestował przed ściąganiem pryzmatu maga; jednak Siluria i Hektor go przekonali. Kopidół się zgodził...

Siluria przekonała pozostałych magów, którzy mogli przyzwać wizerunek Kuby Urbanka. Hektor - Blakenbauer - został pryzmatem. 9v9-> S. Pojawił się Strażnik Jakub Urbanek...

Wykorzystując "wirtualnego Kubę Urbanka" KADEMowi udało się zatrzymać magów KADEMu chcących usunąć na zawsze problem Renaty (już nie Maus). Siluria i Hektor mogą spać spokojnie. Jednak czas na kolejne spotkanie z Renatą, która WYRAŹNIE jest zainteresowana ratowaniem swojego rodu. Uważa Abelarda za zabawkę w rękach Świecy. Renata próbuje dać Hektorowi... coś. Próbuje kusić zarówno Hektora jak i Silurię; bez powodzenia.

* Nie oczekuję od Ciebie deklaracji. To wymagałoby kręgosłupa. - Renata, z lekką pogardą
* Mam propozycję. Hektor Blakenbauer, seiras Srebrnej Świecy. Abelard dla każdego. - Renata, bardzo złośliwie

Powiedziała im też o grupie magów, którzy przeszli przez Portal w imieniu Karradraela. Boją się. Tak, Karradrael może rozkazać im powrót. Tak, Abelard o tym nawet nie pomyślał. Zdaniem Renaty ten mag jest tak bezużyteczny jak to tylko możliwe...

Aha, Renata przybrała nowe nazwisko. Souris.

# Progresja

* Renata Souris: uważa, że jest winna życie Silurii i Hektorowi. Nie jest pewna, czy to korzyść dla nich czy powinna ich za to zabić...
* Siluria Diakon: otrzymała od Renaty nieznane kody kontrolne do "czegoś". Quasar nie wie jeszcze co z tym zrobić...

## Frakcji

* KADEM: dostał efemerydę Kuby Urbanka, który strzeże zdrowia i życia Renaty Souris.

# Streszczenie

Renata Maus przybrała nowe nazwisko - Souris. Częściowo pogodziła się z losem; konspiruje i próbuje przekonać Hektora i Silurię dając im podarki i pokazując swoją pogardę do aktualnego Lorda Mausa. Bardzo niebezpieczna. Grupa magów KADEMu próbuje usunąć Renatę; w odpowiedzi na to z pomocą Kopidoła Siluria tworzy echo Kuby Urbanka. Renata dała radę przekonać Hektora do wyzbycia się zemsty na Karolinie Maus i przekazała Silurii "jakieś" kody. Przekazała też Silurii informacje jak uratować Milenę Diakon.

# Zasługi

* mag: Hektor Blakenbauer, odpuścił winy Karolinie Maus i przekierował winę na Karradraela. Skutecznie oparł się kuszeniu Renaty.
* mag: Siluria Diakon, przekonała Kopidoła do stworzenia efemerydy Kuby Urbanka. Ku swemu zaskoczeniu, Renata uważa, że jest winna życie.
* mag: Renata Souris, nie walczy z przeznaczeniem; skupia się na przetrwaniu rodu Maus i na skuszeniu magów KADEMu by zrobili to, co ona chce. Uratowała Karolinę i Milenę.
* mag: Quasar, skutecznie odparła liczne ataki magów KADEMu na Renatę. Tym razem jej umiejętności były przetestowane do maksimum...
* mag: Lucjan Kopidół, jego wiedza posłużyła do stworzenia efemerydy Kuby Urbanka by ów chronił Renatę Souris.
* vic: Echo Jakuba Urbanka, stworzona efemeryda mająca odzwierciedlać charakter i potęgę Kuby; chwilowo ma chronić Renatę Souris (i pilnować jej zachowania).

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex, gdzie konspiruje grupa magów KADEMu próbująca usunąć Renatę z równania
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Skrzydło wzmocnione
                        1. Specjalne sypialnie, gdzie przetrzymywana jest Renata Souris (kiedyś: Maus)

# Czas

* Dni: 1