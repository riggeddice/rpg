---
layout: inwazja-konspekt
title:  "Patriarcha Blakenbauer"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140401 - Mojra, Moriath (AW, WZ, HB)](140401-mojra-moriath.html)

### Chronologiczna

* [140401 - Mojra, Moriath (AW, WZ, HB)](140401-mojra-moriath.html)

## Misja właściwa:

![](140604 PatriarchaBlakenbauer.jpg)
 
 Aktualna mapa NPC i ich stanu:
 
![](Moriath_NPC_140604.jpg)

# Zasługi

* mag: Hektor Blakenbauer jako ten, kto stał się Patriarchą (serasem) w zamian za to, że da Rezydencji i rodowi potomka.
* mag: Andrea Wilgacz jako ta, co chce skontaktować Irinę z Quasar i blokuje ruchy Mariana Agresta mające zatrzymać Zespół.
* mag: Wacław Zajcew jako jedyna siła która dała radę powstrzymać Krystalię przed zabiciem Karoliny i zapewniająca wiedzę ze strony Zajcewów i Iriny. 
* mag: Marian Agrest jako czarodziej, który powoli konsoliduje wszystkie tematy związane z Blakenbauerami i Moriathem pod swoimi skrzydłami; odzyskał Elżbietę i PRAWIE przechwycił Karolinę Maus.
* mag: Irina Zajcew jako ta, która wiedziała więcej o temacie Moriatha i stoi po tej samej stronie co Agrest. Chce, by Wacław współpracował z Blakenbauerami. Ma Karolinę Maus.
* mag: Krystalia Diakon, która chciała zabić Karolinę Maus krzycząc "ona mnie zabiła". We wspomaganej wyhodowanej uprzęży jest trudna do zatrzymania... ale był tam Wacław.
* mag: Maja Kos, wierna i fanatycznie lojalna Marianowi Agrestowi terminuska, która zastąpiła mu Andreę jako jego przyboczna. Nie zatrzyma Krystalii w uprzęży.
* mag: Dariusz Kopyto, jeden z terminusów pod dowództwem Agresta; wraz z Mają Kos próbował przechwycić Karolinę Maus i został odparty przez Krystalię (a potem prawnie przez Andreę)
* czł: Amelia Eter, Sophistia, która zniknęła i została schowana przez Gustawa Siedeła. Zraniona i odrzucona przez Marcelina (jak czuje), znowu zniknęła.
* mag: Gustaw Siedeł, który schował Sophistię przed mackami Blakenbauerów i otoczył ją osobistą opieką ukrywając przed wszystkimi możliwymi siłami.
* mag: Teresa Żyraf, która dostała kolię w prezencie od Krystalii i mimo pewnych oporów zdecydowała się Krystalii zaufać i ją założyć.
* mag: Grzegorz Czerwiec, który przez machinacje Agresta nie dostał Karoliny z powrotem. Chce współpracować z Zespołem przeciw Agrestowi.
* mag: Karolina Maus, którą wpierw prawie zabiła Krystalia, potem prawie porwał Agrest aż w końcu Andrea przekazała ją Irinie Zajcew.
* mag: Waldemar Zupaczka, który ma szukać Sophistii i któremu Agrest powiedział, że Andrea stoi za sprawami z samochodem. Aha, zarówno Czerwiec jak Agrest szukają Sebastiana Teczni.
* mag: Ilarion Zajcew, seras, który w tajemniczych okolicznościach został zamordowany przez profesjonalnego zabójcę (KIA).
* mag: Yakim Zajcew, który przejął pozycję serasa od Ilariona Zajcewa wraz ze śmiercią Ilariona. 
* vic: Ika, która kiedyś była czarodziejką, lecz wszystko zniszczył Otton by pozostawić "carte blanche" następnie wykorzystane przez Marcelina do sformowania dziewczynki tropiącej.
* mag: Mojra, jako wspomnienie osoby która zniszczyła Projekt Moriath przez skrzywdzenie Margaret i zniszczenie wszystkiego. Współpracowała i z Iriną Zajcew i z Ottonem Blakenbauerem.
* mag: Margaret Blakenbauer, o której "prawda" wyszła na jaw. Mojra wstrzyknęła jej mindworma i ją ciężko uszkodziła a problemy Ottona wynikały z tego że chciał ją naprawić.
* mag: Sebastian Tecznia jako osoba nieprzytomna w Bazie Detektywów, której jednak szuka zarówno Agrest jak i Czerwiec z niewiadomych przyczyn.
* mag: Otton Blakenbauer jako nieprzytomny i katatoniczny czarny mag, który zniszczył poprzednie życie Iki, ale współpracował z Mojrą, Agrestem i Iriną.