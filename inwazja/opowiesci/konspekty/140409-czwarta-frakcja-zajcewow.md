---
layout: inwazja-konspekt
title:  "Czwarta frakcja Zajcewów"
campaign: anulowane
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

TODO:

<img src="140409_CzwartaFrakcja1.jpg"/>

# Zasługi

* mag: Wacław Zajcew jako główny rozgrywający Iriny... a tak naprawdę, potencjalna ofiara.
* mag: Andrea Wilgacz jako łączniczka SŚ w sprawie Kamaza.
* mag: Irina Zajcew jako terminuska która szybciej poświęci Tatianę niż dopuści do tego, by Swietłana rozerwała polską grupę Zajcewów by ich "oczyścić".
* mag: Swietłana Zajcew jako kapłanka zdolna do poświęcenia bardzo wiele dla Oczyszczenia Zajcewów.
* mag: Tatiana Zajcew jako potencjalna ofiara Iriny by przekonać Swietłanę.
* mag: Bożena Zajcew jako zaatakowana przez Nowy Porządek Swietłany handlarka ludźmi i magami. 
* mag: Yakim Zajcew jako baron Supremacji zgadzający się ze Swietłaną... ale zachowujący realizm. Pomógł dyskretnie Wacławowi widząc rolę swego ochroniarza.
* mag: Maciej Zajcew jako eks-adiutant Yakima który został jednym ze Spalonych Zajcewów i dołączył do Swietłany.
