---
layout: inwazja-konspekt
title:  "Kasia, nie EIS w Powiewie "
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150930 - O Wandzie co Zajcewa nie chciała (AB, Kx)](150930-o-wandzie-co-zajcewa-nie-chciala.html)

### Chronologiczna

* [150829 - Hybryda w bazie Skorpiona (PT)](150829-hybryda-w-bazie-skorpiona.html)

## Misja właściwa:

### Wyjaśnienie sytuacji:

Paulina nieczęsto jest w takim humorze jak teraz. Po sprawie z bazą Skorpiona, jakkolwiek uratowała wielu ludzi, jest ciężko Skażona i naprawdę nie powinna teraz czarować; jako lekarz, powinna dać sobie wytyczne, że powinna teraz przede wszystkim się nie stresować i odpocząć. Z uwagi na sposób działania magii, nadmiar stresu będzie po prostu skutkował pogorszeniem stanu zarówno fizycznym jak i magicznym.

Jednak Paulina nie jest w stanie teraz odpocząć. Maria, próbując jej pomóc, wpadła. Zwyczajnie przedawkowała maść znieczulającą (którą użyła jako maskowanie) i straciła przytomność. Na szczęście jest w Powiewie, co oznacza, że opiekuje się nią Celestyna. Na nieszczęście, po tym co zrobiła, Roman po prostu jej nie wypuści. Zwłaszcza, że Maria jest człowiekiem wiedzącym o magii i jest niezarejestrowana...

W tej chwili Paulina po prostu chce wrócić do Kopalina, odzyskać Marię i uciekać. Oddalić się z Kopalina gdzieś, gdzie nikt jej nie znajdzie i nie zobaczy. Sytuacja po prostu stała się dramatyczna pomiędzy Iliusitiusem / Olgą, Skorpionem, Millennium, siłami Świecy z różnych frakcji oraz dziennikarzami. A w tym wszystkim jedna Paulina, która nagle znalazła się na oczach wszystkich i Maria, która NIE MOŻE trafić na niczyje oczy.

### Faza 1: Upiór straszący Kasię

Paulina wróciła do Kopalina. Pierwsze kroki skierowała ku Powiewowi Świeżości - wyciągnięcie Marii jest absolutnie kluczowe; im szybciej się do Marii dostanie tym mniej dziwnych rzeczy się stanie. Maria jest już przytomna i skutecznie odmawia odpowiedzi na pytania. Paulina wie, że interesuje się Marią i Celestyna i Roman. I bardzo Pauliny to nie cieszy.
Jadąc do Powiewu, Paulina wypytała Marię jak się do niej odnosili Celestyna i Roman. Maria powiedziała co następuje:

Roman odwiedził ją dwa razy, pytając czy jest człowiekiem (Maria odmówiła odpowiedzi na to pytanie). Raz był z Kasią Kotek. Pytał też Marię o to, czy komunikuje się z Pauliną w czasie rzeczywistym. Maria powiedziała, że ma swoje sposoby komunikacji i odmawia odpowiedzi także i na to pytanie. Gdy Roman zapytał Marię czemu tak rozmawiała z Kasią (jakby Paulina była na telefonie), Maria odpowiedziała, że najprawdopodobnie miała halucynacje lub utratę pamięci i nic nie pamięta. Patrząc Romanowi prosto i bezczelnie w oczy.
Paulina znowu facepalmowała. It's not getting better.

Celestyna wypytywała Marię o różne rzeczy, np. skąd ma maść, czemu to zrobiła, czy ma jakąś historię magiczną, ale Maria powiedziała że nie może odpowiedzieć. Celestyna powiedziała, że jest jej to potrzebne do celów medycznych i że Skażenie na Marii wskazuje na minimalną ilość tkanki magicznej, czyli jest jak wytrzymałość człowieka; czy to choroba, alergia, przeciążenie... Maria powiedziała, że nie może odpowiedzieć. Paulina wyjaśni.
Za to właśnie Paulina ją chciałaby zabić. I co Paulina ma teraz z tym zrobić?!
Gorzej, że Celestyna też pytała z ciekawości o różne rzeczy. To straszna plotkarka; Paulina NAPRAWDĘ nie chce, by Celestyna za dużo wiedziała.

I jak Marię teraz wyciągnąć?

Szczęśliwie, wszystko wskazuje na to, że Roman odciął Marię od innych magów Powiewu; zgodnie z tym co mówi Maria (tak, wydobyła to z Celestyny) są na zamku As'caen a nie w skrzydle szpitalnym. Tylko Roman i Celestyna wiedzą, gdzie Maria się znajduje. No i Kasia, ale jej nikt nie liczy.

No nic, trzeba zjeść tą żabę... Paulina nie powinna czarować, więc najpewniej nie będzie.

Paulina wpierw poszła do biblioteki Powiewu; ma nadzieję, że zastanie tam samą Kasię Kotek. Chce z nią porozmawiać zanim powie coś innym. 
I szczęśliwie, Kasia jest w bibliotece; czyta właśnie jedną z książek.

Paulina spytała Kasię, czy Roman jest bardzo zły. Kasia odpowiedziała, że tak. Czy aż tak, by kogoś powiadomić? Nie. Kasia powiedziała, że tego nie zrobi. Nie może na tym niczego zyskać i ona się upewniła, by Roman to zrozumiał.

"Ja nie mogę dać mu odpowiedzi na jego pytania..." - Paulina o Romanie
"Ale... ja nawet nie wiem, jakie pytania zada." - Kasia, zdziwiona
"Nieważne jakich nie zada, i tak nie mogę." - Paulina, myśląc o Marii

Kasia usiadła i zbladła. Zakręciło się jej w głowie. Paulina ją podparła. Usłyszała "to nic takiego"; Kasia powiedziała, że to się rzadko zdarza. Ma wrażenie, że jest w innym miejscu jednocześnie, ale to niemożliwe. Poczuła "się" w innym miejscu na Powiewie (co się stało: Julia uruchomiła zapasowy shard EIS by wykryć jej lokalizację).

"Nie chcę mówić o Marii z nikim, jeśli to nie jest konieczne. To byłoby niebezpieczne dla mnie, Marii i..." - Paulina wyjaśniająca Kasi problem.
"Nie dziwię się. Macie coś w stylu uszkodzonego soullinka" - Kasia do Pauliny, nie widząc wstrząsu na twarzy czarodziejki

Paulina powiedziała Kasi, że chce zabrać Marię jak najszybciej. I tak, by najlepiej nikt się o tym nie dowiedział. Kasia poradziła jej, by porozmawiała z Romanem na ten temat. Paulina powiedziała, że Roman jest zły... Kasia zauważyła, że jeśli się je rozdzieli, obie umrą.
Paulina powiedziała Kasi, że chce żyć. Kasia powiedziała, że ona TEŻ chce żyć, Paulina ją zapewniła, że z jej strony Kasi nic nie grozi. Paulina powiedziała Kasi, że porozmawia z Romanem... ale poprosiła Kasię, by ta przygotowała jej drogę ucieczki.

Kasia się zgodziła. Przygotuje Paulinie drogę ucieczki.

"Potem zniknę. Nie będzie problemu." - Paulina do Kasi.
"Czyli znikniesz, ja Ci pomogę i się narażę, i zostanę sama?" - Kasia, chłodno i logicznie (nie oskarżycielsko, ale nieludzko).
"Jeśli mogę Ci jakoś pomóc, powiedz..." - Paulina, współczująco
"Zabierz mnie ze sobą." - szybka decyzja Kasi. Paulina spadła z krzesła.

Kasia wyjaśniła Paulinie, że uważa Powiew Świeżości za zdecydowanie zagrożenie. Ona, Kasia, ma elementy EIS. I ona o tym wie. Powiew Świeżości boi się EIS i zniszczył EIS już trzy razy. Roman słuchał rozmowy Pauliny i Marii w bibliotece (Paulina tu facepalmowała ponownie) i patrzył na Kasię z pewnym przerażeniem. Tak więc zdaniem Kasi, większe zagrożenie czeka ją tutaj, ze strony Powiewu niż tam, z Pauliną, w niebezpiecznych miejscach.

Tak na to Paulina nie patrzyła. Teraz rozumie to "ja chcę żyć" ze strony Kasi. I mimo ryzyka Kasia zdecydowała się kupić Paulinie drogę ucieczki z Marią. Nieludzka. Nienaturalna. Jak Aurelia lub EIS.
Kasia przyznała Paulinie, że się boi. Coś, czego ani Aurelia ani EIS nie powiedziałyby nigdy.
Zdaniem Pauliny, Kasia powinna się przyłączyć do Powiewu. To powinno rozwiązać problem.

Nagle Kasia podskoczyła. Cała przerażona. Powiedziała Paulinie, że coś się zbliża. Coś bardzo groźnego. Muszą uciekać. I zaczęły uciekać z biblioteki; Kasia zaprowadziła Paulinę do skrzydła szpitalnego. Powiedziała jej, że nie wie przed czym uciekała, ale nie ma zamiaru tam wracać póki "to" tam jest. Nie jest uzbrojona (co wykryła: Julię z shardem EIS).
Kasia się otrząsnęła i spytała Paulinę, gdzie ta chce się dostać lub co załatwić.

Paulina powiedziała, że chce do Romana. Kasia ją tam zaprowadziła. Powiedziała też Paulinie, że "to coś groźne" nie porusza się losowo; to idzie zawsze w kierunku Kasi. I teraz znowu tego nie ma (Julia już zlokalizowała i wie już wszystko; wyłączyła shard EIS). 

### Faza 2: Kasia przeciwko Julii

Roman ma biurko w zamku As'caen i właśnie tam udała się Paulina z Kasią. Kasia potrafi nawigować zamek As'caen dzięki swojej świetnej pamięci. Paulina już się zgubiła, bo Kasia poszła "na skróty".

Roman się bardzo zdziwił, że Paulina go odwiedziła. Powiedział jej, że się nie spodziewał; zawsze unika problemów. Paulina powiedziała że to bardzo niesprawiedliwe twierdzenie; Roman ominął problem i powiedział Kasi, że może wyjść. Paulina powiedziała, że jej nie przeszkadza. Roman spojrzał na Paulinę ze zdziwieniem; czy ta naprawdę chce, by Kasia została tu i dodawała jakieś niepotrzebne informacje? Kasia na to, że zawsze Roman może jej powiedzieć by nic nie mówiła na co mag się rozpromienił. Niech Kasia się nie odzywa nie pytana. Viciniuska pokiwała głową bez słowa a Paulina się zdenerwowała - Roman nadal nie rozumie, że Kasia nie jest robotem.

Roman spytał Paulinę złośliwie, czy każdemu człowiekowi mówi takie rzeczy (w domyśle: o Kasi), czy tylko Marii? Paulina pamięta, że Kasia mu powiedziała o soullinku, więc powiedziała prawdę. Maria jest czymś więcej, niż przyjaciółką.
Roman trochę się uspokoił. Zapytał Paulinę, jak się w to wpakowała... ona spytała, czy Roman chce wiedzieć. Wygrała konflikt. Roman całkowicie odpuścił. Powiedział Paulinie, że nikt nie wie. Tylko on, Paulina i Kasia. Odciął wszystkie źródła informacji, przesunął Marię na zamek As'caen i zakazał Celestynie zadawać niektórych pytań.

Paulina spytała Romana, czy on chce pamiętać o soullinku. Roman powiedział, że tak będzie bezpieczniej - Kasia pamięta więc jak Roman pamięta, Kasia nikomu nic nie powie. Tak będzie jednak bezpieczniej.

Paulina się ucieszyła. Roman, nieważne jak zły by nie był, jednak lubi Paulinę i nie chce jej stracić - pokazuje to tak, jak umie. Roman poprosił Paulinę, by ta się schroniła na zamku As'caen. Kasia się zgłosiła do odpowiedzi i Paulina udzieliła jej prawa głosu. Viciniuska zauważyła, że na zamku Paulina nie zregeneruje Skażenia. Roman taki smutny.

Paulina powiedziała jeszcze Romanowi, żeby nie traktował Kasi jak służki. Nie tak ją stworzyli. Roman powiedział, że tak jej nie traktuje. Paulina spytała, czy na pewno? I wtedy Paulina zauważyła, że Julia przechodzi przez ścianę w tym kierunku. Twarz goes blank i na głos całkowicie spokojnym tonem, by ostrzec Romana (że Julia podsłuchiwała) "witaj, Julio".

Julia wyszła, cała wściekła i rzuciła Romanowi, że tego się nie spodziewała. Pokazała lapisowy woreczek i spytała go, czy wie co to jest. Wyjęła z niego shard EIS (shard - rozszerzenie dla EIS) i powiedziała, że nagle zaczęło to świecić i działać. Krzyknęła na Romana, że odbudował EIS. Paulina zauważyła kątem oka, jak Kasia wyślizgnęła się z pokoju. Pobiegła za nią i poprosiła, by Kasia została. Jeśli ucieknie, zawsze już będzie uciekać. Kasia odpowiedziała, że próbowała kiedyś przekonać. Już trzy razy. Paulina widzi, że Kasia miesza to co wydarzyło się Kasi z tym, co wydarzyło się Aurelii i EIS. Ona nie wie czym jest; jeszcze nie czuje tego wszystkiego. Paulina podkreśliła, że Kasia nie jest EIS, że jest Kasią.

Bardzo ważny wygrany konflikt; Kasia uwierzyła Paulinie, że nie musi przyzywać głowicy bojowej (Aegis) na pomoc. Że jest nadzieja, że rozwiąże to sama. Znowu - uciekła Paulinie, uciekła z zamku As'caen do biblioteki, ale nie opuści Powiewu i nie wezwie głowicy. Spróbuje rozwiązać ten problem sama.

A Paulina wróciła do pokoju, gdzie Julia i Roman krzyczą na siebie.

"Stworzyłeś najgroźniejszą i najbardziej adaptywną AI w historii Powiewu! Zabijesz nas wszystkich!" - Julia do Romana
"Powiedziała czarodziejka, pod której rządami Powiew jest jęczącą, tanią Diakonką krzyczącą 'jeeeszcze' z KADEMem biorącym nas od tyłu jak chce" - Roman z typową sobie subtelnością o oddaniu Overminda Spustoszenia i innych relacjach PŚ wobec KADEMu.

Paulina przerwała im tą uroczo bezproduktywną dyskusję. Spróbowała przerwać im obu i powiedzieć, że Kasia nie jest niebezpieczna.
Julia powiedziała Paulinie kilka faktów: 

- EIS zniszczyła Powiew trzykrotnie a wtedy jeszcze żyła Aurelia
- EIS potrafi adaptować. Nie robi dwa razy tego samego błędu.
- EIS jest na KADEMie. Nie ma dostępu do centralnej bazy, by ją zresetować.
- Im dłużej EIS jest na wolności tym szybciej adaptuje i tym jest skuteczniejsza.

"EIS adaptuje do wszystkiego. Na pewno ma tu szpiegów wszędzie." - spanikowana Julia.
"Na pewno ma." - Roman
"?" - Zdziwiona Julia
"Rozszerzenie które przyniosłaś nie jest w lapisowym woreczku" - spokojnie i złośliwie, Roman.

Z tych wszystkich obrazów, filmów i przełożeń jakie Julia pokazała Paulinie ta wyciągnęła jeden istotny element - było tam szaleństwo, EIS była szalona w środku. A Kasia nie jest. Nie jest szalona, jest tylko przerażona i nie wie czym jest.

Julia powiedziała, że EIS nie ma szans. Jak długo Julia ma extension, ma możliwość użyć sympatii i zniszczyć EIS... a przynajmniej ma taką nadzieję. Zrobiła to kiedyś, użyto przeciw EIS sympatii przez rozszerzenie kiedyś, więc Julia nie ma 100% pewności, że to się uda; EIS mogła zaadaptować (choć Julia uważa, że nie miała jak). Paulina poprosiła Julię, by dała Kasi szansę. Julia wściekła się, że zrobili "żywą EIS" - to dodaje problem etyczny. Julia nie chce zabijać. Paulina powiedziała, że jeśli Kasia jest żywa, pojawia się nadzieja, że nie będzie szalona, że będzie normalną, żywą osobą.

Paulina próbuje Julię przekonać - ma do czynienia z dzieckiem, które nie wie czym jest. Pokazuje lektury, pokazuje wątki, pokazuje wszystko. Pokazuje, że Kasia ma wątpliwości i strach (których nie miała EIS). Julia na to, że EIS się zaadaptowała. Jednak Paulina wygrała konflikt. Julia nie ufa i nie będzie ufać Kasi - ale da jej szansę. Nie zniszczy jej na miejscu, ale będzie bardzo uważnie obserwować.

"Będzie bardzo ściśle kontrolowana. Nie będzie członkiem gildii. Będzie obserwowana. Ale dam jej szansę." - Julia o Kasi.

Julia, piekielnie niezadowolona, zdecydowała się dać szansę Kasi i wyszła z pokoju. Roman i Paulina zaczęli dyskusję o całej tej sprawie i nagle usłyszeli wyładowanie łuku elektrycznego. Paulina i Roman wypadli na korytarz tylko po to, by zobaczyć Kasię stojącą nad nieprzytomną Julią. Kasia ma w rękach dziwny pistolet "teslowy".

Paulina zaczęła prosić Kasię, by ta tego nie robiła. Kasia powiedziała, że dość uciekania. Paulina powiedziała że nie to miała na myśli; Julia się Kasi boi. Kasia powiedziała, że powinna - Julia była ciekawa jak działa matryca EIS więc w pewnym momencie się z nią połączyła. Destabilizacja EIS doprowadziła do szaleństwa którego wynikiem była desynchronizacja z Aurelią i jedno ze zniszczeń Powiewu. Kasia krzyknęła, że oddała Julii Powiew na talerzu (myśląc jak Aurelia), że chroniła dla Julii Powiew (myśląc jak EIS) a Julia wszystko zniszczyła. Więc powinna się bać.
Roman przejął rozmowę. Kasia powiedziała, że zna sekrety wszystkich magów Powiewu. Zna lokalizację poukrywanej przez Aurelię broni. Magowie Powiewu nie dadzą jej żyć, nie będą chcieli. Roman spokojnie powiedział, że dadzą. Kasia parsknęła. Roman dał znak i Celestyna ukryta za Kasią odpaliła czar snu. Powiedział do zasypiającej Kasi, że gdyby chciał ją zabić, dawno już by nie żyła. I to samo dotyczy Julii - też mogłaby Kasię dawno zabić.

Celestyna powiedziała, że mają problem. To było broadcastowane na cały Powiew. Świetnie. Roman kazał Celestynie wziąć Paulinę i Marię i uciekać z Powiewu. Paulina protestowała, ale Roman zauważył; będą dziennikarze. To kupuje Paulinie czas. Paulina się troszkę zbuntowała, ale Roman obiecał, że jakoś wintegruje Kasię Kotek w Powiew. W końcu nie zabiła. Nie zadziałała jak EIS, zadziałała jak osoba. Zdesperowana, ale osoba.

Paulina powiedziała Romanowi, że Kasia nie może przebywać w zamku. Tu rządzi pryzmat; jeśli oni wszyscy myślą o Kasi jako o EIS, ona będzie stawać się EIS. Więc Kasia Kotek ma zakaz przebywania na zamku As'caen.

(Swoją drogą, ciekawe co Kasia znalazła takiego, co broadcastowało między fazami; wynik może ulec zmianie; grunt, że to jest to w co Roman wierzy i co powiedział Paulinie bo to powiedziała mu Celestyna)

### Faza 3: Ruiny cmentarza

Po opuszczeniu zamku As'caen na smartfonie Pauliny odpaliły się wiadomości. Emergency broadcast. Przesłany przez Netherię auto-forwardem. Przekaz brzmi "mayday, mayday, cmentarz Wiązowy, potrzebne wsparcie - Mikado, przeciwko Świecy". Paulina nie zna gościa, ale wie, że coś się tam stało. Minutę temu. Świeża rzecz.

Paulina wysłała Marię do domu - niech ta zabierze wszystkie rzeczy, uciekają z Kopalina. Ale ona (Paulina) musi jechać na cmentarz. Musi zobaczyć o co chodzi.

Celestyna pojechała z Pauliną na cmentarz. Jadąc tam Paulina poczuła echo walki między terminusami. To nie wygląda na prowokację dziennikarską...
Dotarły. W samą porę. Paulina widzi, jak walczą ze sobą Judyta Karnisz i Mikado Diakon. A z boku atakuje Mikada Ozydiusz Bankierz, rozpłatując mu klatkę piersiową używając wielkiego magicznego miecza. Mikado pada na ziemię, umierając; Judyta przypada doń i wylizuje go języcznikiem, trzymając go przy życiu.
Tymczasem Ozydiusz podnosi spod ziemi trumnę. Tą trumnę, w której znajduje się ten mag pełen nienawiści, którego czuła wcześniej Paulina gdy pracowała z tym cmentarzem. Paulina krzyknęła by tego nie robił, nie zabierał jej. Czy jest aż tak głupi? Nazwała go idiotą. Ozydiusz westchnął i kazał Judycie Paulinę wybatożyć.

Judyta zaatakowała Paulinę. Podczas walki Judyta szybko przekazała Paulinie, że przeprasza; ona znalazła tą trumnę dla Ozydiusza i nic nie może zrobić. Celestyna zaatakowała Judytę by chronić Paulinę; Ozydiusz zaczął rytualną teleportację trumny i siebie. Judyta odparła Celestynę i złamała jej kręgosłup w okolicach ledźwi a Ozydiusz teleportował się powodując ogromne Skażenie (Paulina zwymiotowała). Judyta po wybatożeniu Pauliny zrobiła krótki teleport (jak najmniejsze Skażenie) a potem dalszy.

Chwilę potem na miejscu pojawiły się służby medyczne i puryfikacyjne Świecy. Poznali Paulinę; Paulina nie chciała jednak z nimi jechać. Pojechała z Marią by uciec z Kopalina. Celestynie i Mikado siły Świecy pomogą.
Cmentarz, który był sanktuarium, został zbezczeszczony. Ozydiusz uznał, że skoro jest on słabym punktem Estrelli to trzeba go zniszczyć.

Paulinie jest naprawdę przykro.
Ale Paulina jeszcze kiedyś Ozydiuszowi dokopie.
Na razie, Paulina opuszcza Kopalin z Marią, póki jeszcze jest w stanie...

# Streszczenie

Paulina wraca do Kopalina odzyskać Marię i uciec. Nie wyszło. Kasia Kotek ma przebłyski EIS i Aurelii; nie wie czym jest. Wykryła ją Julia i ścięła się z Romanem o to czym jest Powiew Świeżości. Gdy Julia i Kasia zaczęły walkę, obecność Kasi została broadcastowana Powiewowi. Roman kazał Paulinie z Marią się ulotnić; on to załatwi i zintegruje Kasię Kotek z Powiewem. Tymczasem na Fazie Naturalnej mayday Netherii na Cmentarzu Wiązowym. Ozydiusz wydobył spod ziemi "Skażonego maga pełnego nienawiści" a Mikado z Millennium walczy z Judytą. Ozydiusz kazał wybatożyć Paulinę za impertymencję, ciężko zranił Mikada po czym Skaził obszar. Służby puryfikacyjne i medyczne pomogły obecnym, ale dla Pauliny to sygnał. Znika stąd.

# Zasługi

* mag: Paulina Tarczyńska, która rozwiązała problem Kasi Kotek w Powiewie i była świadkiem zabrania trumny przez Ozydiusza.
* czł: Maria Newa, która leżała chora i słaba w łóżku w Powiewie Świeżości i "nie ma nikomu nic do powiedzenia", czekając na "ratunek" Pauliny.
* vic: Katarzyna Kotek, która bardziej boi się * magów Powiewu niż potworów tego świata. Nie jest EIS, nie jest Aurelią, ale jest wszystkim naraz. A jednocześnie czymś innym.
* mag: Celestyna Marduk, słaba terminuska i niezły lekarz Powiewu. Stanęła w obronie Pauliny przeciwko terminusce Świecy i bardzo ucierpiała.
* mag: Julia Weiner, która tak panicznie boi się EIS, że w pewien sposób ją stworzyła w Kasi Kotek. Kiedyś doprowadziła do szaleństwa EIS.
* mag: Roman Gieroj, bardzo pozytywnie wykazał ludzkie odruchy i instynkt przywódczy. Ochroni Kasię przed Julią i wintegruje ją w Powiew.
* mag: Ozydiusz Bankierz, który nie docenia bycia traktowanym szczerze (bez szacunku); wydobył trumnę z cmentarza Wiązowego.
* mag: Judyta Karnisz, która niezależnie od swoich poglądów wykonuje wszystkie zadania Ozydiusza.
* mag: Mikado Diakon, który postawił się i Judycie i Ozydiuszowi. Miał nadzieję na wsparcie, ale tylko Paulina się pojawiła.

# Lokalizacje:

1. Świat
       1. Primus
           1. Śląsk
               1. Powiat Kopaliński
                   1. Kopalin
                       1. Stare Wiązy
                           1. Cmentarz Wiązowy
                       1. Nowy Kopalin
                           1. Powiew Świeżości
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen 