---
layout: inwazja-konspekt
title:  "Piećdziesiąt oblicz Szlachty"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160229 - Siedmiu magów - nie Mausów (AW)](160229-siedmiu-magow-nie-mausow.html)

### Chronologiczna

* [160229 - Siedmiu magów - nie Mausów (AW)](160229-siedmiu-magow-nie-mausow.html)

### Logiczna

* [160326 - Siluria na salonach Szlachty (SD)](160326-siluria-na-salonach-szlachty.html)

## Kontekst misji

- Wiktor próbuje podporządkować sobie Silurię. Siluria próbuje podporządkować sobie Wiktora.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów bo Arazille.
- Siluria pomaga Wiktorowi w przekształceniu maga Kurtyny w sleeper agenta.
- Siluria uzależnia od siebie młodziutką Wiolettę Bankierz, przy okazji mając potencjalnych wrogów: Dagmarę i Gerwazego.

## Cel misji:

Cel misji: 
- pokazanie niejednolitości Szlachty; relacje między głównymi postaciami.
- pokazanie Silurii, że nie będzie łatwo a jednocześnie, że będzie łatwo. Urealnienie.

## Punkt zerowy:

- Jurij Zajcew znalazł odpowiedź na potencjalny problem jaki ma Szlachta - magowie nie widzą jak bardzo są ograniczani przez prawa i systemy. Przygotował serum, dzięki któremu mag może to pokazać odpowiednim hymnem rytualnym. Niestety, wymaga, by mag śpiewał hymn... oraz ma paskudne efekty uboczne. Tak czy inaczej, działa ;p.
- Gerwazy Myszeczka nie jest głupi. Widzi, że Siluria jest bardzo niebezpieczna i potrafi sobie wszystkich docelowo owinąć wokół paluszka. Jako, że KADEM jest nieaktywny, wymyślił, by zaprosić ją przy użyciu Wiktora na Wielki Wybuch - Festiwal Tworzenia. 100 magów w maskach połączonych mocami katalistów i kryształów Mausów tworzą historię. Drogie i w Warszawie (tak naprawdę, w Świecy Daemonica). A naprawdę przeniesie ich do Laboratorium Mgieł. Pretekst zamaskuje sygnaturę wszystkiego co jej zrobi...

## Misja właściwa:

Trzy dni. Trzy dni trenowania z Wiolettą. Trzy dni mieszania wzoru nieszczęsnego Kurzamyśla z czujną Dagmarą, która ma jej pomagać. Trzy długie dni. Ale Siluria kooperuje; nie ma powodu działać przeciwko Wiktorowi. Jak nie ona, ktoś inny. 
Siluria próbuje zaprezentować się Gerwazemu jako stosunkowo niegroźna, ale nie idiotka. Jest tu przede wszystkim bo chce się dobrze bawić, pochlebia jej uwaga Wiktora, ale jest w zasadzie niegroźna. Gerwazy jest elegancki i uprzejmy, ale to wszystko; nie interesuje się szczególnie Silurią. Gdy Siluria chce wyjść czy zrobić coś "na zewnątrz", Gerwazy po prostu wzywa większe wsparcie. Siluria jest odpowiednio niezależna i faktycznie może co chce. Jak Siluria widzi, Gerwazy ma duże uprawnienia. I zupełnie ich nie wykorzystuje poza spełnianiem marzeń (nawet nie życzeń) Wiktora.

Jako, że Siluria jest Diakonką, raz na jakiś czas dla odstresowania potrzebny jej był "itd". To zawsze był dyskretny czarodziej, młody i odpowiednio zafascynowany Silurią. Też Siluria w ciągu tych trzech dni Siluria spotkała się z Kermitem i poprosiła go o informacje odnośnie Gerwazego, Wioletty i Dagmary. Kim oni są, czego oni chcą.

- Gerwazy jest terminusem starej daty. Prawdziwy obrońca. Kompetentny, oddał swoje życie Świecy i ograniczaniu niewłaściwych zachowań magów. Nie jest ludzistą, ale broni ludzi. Broni wszelkiego życia. Jednocześnie jest bardzo bezwzględny; nie zawaha się zrobić tego co trzeba. No regrets; za dużo widział. W rzeczywistości jest JESZCZE starszy niż 40 lat, ale magiczne wspomaganie go odmładza. Mechanicznie ma podniesioną bazę. Polityk i wojownik, ale nie oficer. Gerwazy jest wielkim stronnikiem Świecy, wierzy silnie w jej ideę. Co sprawiło, że wspiera Szlachtę? Ma żonę. Podczas Zaćmienia straciła moc magiczną. Wiedząc o prawach wiedziała, że odebraliby jej pamięć; jako, że Gerwazy był wiecznie zajęty, została defilerką by mieć moc magiczną. Myślała, że będzie dość silna by zwalczyć uzależnienie magiczne. Oni wszyscy tak myślą. Świeca ją zabrała i zamknęła w stazie. Gerwazy chciał ją odmrozić i spróbować jej pomóc; jest terminusem, poradzi sobie z nią. Oni zawsze tak mówią. Świeca się nie zgodziła. Gerwazy wierzy, że dzięki Szlachcie może odzyskać swoją żonę.

- Wioletta jest piątym dzieckiem i piątym magiem w rodzinie. Najmłodsza córka, jej starsza siostra została czarodziejką EAM. Zawsze w cieniu. Klęska urodzaju; każdy mając taką córkę jak Wioletta byłby szczęśliwy. Ale ona jest piąta z kolei i wiecznie miała poczucie najmniej zdolnej. Jako, że majątek nie może być prawidłowo rozdysponowany, została wychowana i nauczona tak, by przyssać się do kogoś potężnego i dobrze wyjść za mąż. Odrzuciła te nauki, jest wiecznie skonfliktowana wewnętrznie między "muszę mieć protektora" a "jestem lepsza niż inne". Dołączyła do Szlachty, bo to daje jej szansę przebić się do lepszego świata z wyższą pozycją. Pilnuje, by nigdy nie być nikim więcej, niż tylko "wykonującym rozkazy terminusem". W ten sposób nawet, jak Szlachta przegra, ma jeszcze szansę na karierę. Nie zna świata, w którym może być silną, niezależną czarodziejką.

- Dagmara pochodzi z rodu Zajcew, ale odrzuciła nazwisko i przywileje. Uszkodziła swój wzór, by stracić ród. Podczas Zaćmienia jej moc oszalała i zniszczyła większość swojej rodziny. Do dzisiaj nie potrafi sobie wybaczyć. Nadal jest w niej pasja i niezłomność każąca jej żyć i walczyć dalej, choć nie potrafi zapomnieć tego, co się stało. Z tego co wiadomo, jej wzór został ustabilizowany przez Laboratorium Mgieł; było to niezmiernie kosztowne ale pomogło jej i umożliwiło jej dalszą aktywną służbę. Wszystko wskazuje na to, że to właśnie Wiktor zapłacił za jej transformację. Dagmara jest członkiem Szlachty; jakkolwiek jej moc magiczna jest znacznie słabsza niż normalnego maga (nie ma #2 bonusu do magii), wykorzystuje umiejętności które uzyskała w Laboratorium Mgieł i wykorzystuje swój umysł tam, gdzie ciało zawodzi. Nieskończenie lojalna Wiktorowi. Millennium ostrzega przed Dagmarą; jej poziom natywny jest bliżej viciniusa niż maga.

Innymi słowy, Siluria widzi jak dotrzeć do Wioletty (już jej się udało). Ma szansę przekształcić na swoją stronę Gerwazego, choć to będzie wymagało długich i ostrożnych zabiegów; jest szansa, że jego żonie może pomóc Millennium. Ale Dagmara? Trudno pomóc osobie, która się nienawidzi i która nie chce pomocy a jej jedynym celem jest służyć osobie którą kocha.

Dodatkowo Siluria próbuje się dowiedzieć, ile wie Gerwazy o tym co robi Wiktor i co się dzieje w Szlachcie? Wszystko wskazuje na to, że Gerwazy orientuje się w bardzo wielu rzeczach. Ale nic nie mówi i nie interesuje go moralność Szlachty; jego celem jest odzyskanie żony. Siluria pilnuje się, by na pewno przy Gerwazym i Wioletcie zachowywać się właściwie i spójnie.

Podczas tych trzech dni Siluria jeszcze wymyśliła, że tak jak Wioletta ją uczy walczyć, tak Siluria nauczy Wiolettę jak wykorzystywać swe wdzięki. Wyjaśniła lekko spanikowanej czarodziejce, że ciało to broń. W końcu jest terminuską, powinna rozumieć. Wioletta oczywiście rozumie.

Najpierw gdy Siluria miała do czynienia z "itd" to Wioletta siedziała za weneckim lustrem i miała robić notatki, by potem przegadać temat z Silurią. Wioletta czuła się mega niekomfortowo, ale Siluria szybko jej powiedziała "its ok", no jak ma się nauczyć. Potem Wioletta siedziała już w tym samym pokoju. Ma siedzieć do końca, nie ruszać się, nie gadać, nie przeszkadzać. Chyba, że chce. To było bardzo, bardzo trudne dla terminuski... ale dała radę. Jednocześnie już nigdy podczas treningów - jak zauważyła Siluria która zawsze takie rzeczy zauważa - Wioletta nie walczyła z nią w ten sam sposób; zawsze był ten podtekst. 

Siluria chce, by Wioletta poczuła się pewna siebie, swojego ciała... chce, by Wioletta nie czuła się wykorzystana; chce, by Wioletta stała się zdolna do wykorzystania ciała jako broni. Oraz by dawało jej to przyjemność, a nie stres. Siluria próbuje jej pomóc zgodnie z najlepszymi zasadami Diakonów. Mówiąc brutalnie: skoro i tak Wiktor wzywa sobie Wiolettę do zabawy, niech przynajmniej i ona coś z tego ma a nie wyrzuty sumienia i wstyd. Co też sprawia, że Wioletta będzie Silurii wdzięczna.

Przy takiej delcie skilli nie ma konfliktu.

Dzień 4:

Wiktor zaprosił do siebie Silurię. Podziękował Silurii za pomoc - i z agentem w Kurtynie i w kontakcie. Zaproponował Silurii 'Creo Monde' - wielki festiwal. Grupa ~100 magów w atrium Srebrnej Świecy spotyka się i tworzy w ciągu dwóch dób świat. Od początku do końca. Wchodzą w to kryształy Mausów, lekkie stymulatory Myszeczków, Wektor Theta Weinerów... by to powstało, wszystkie wielkie rody (i rody mniejsze) muszą się przyczynić. Każdy 'Creo Monde' jest inny, unikalny, to holokostka gdzie można śledzić losy pojedynczych ludzi lub imperiów...
Jeśli Wiktor chciał Silurii zaimponować, udało mu się. Za kilka dni będzie 'Creo Monde' i Wiktor chce, by on z Silurią w nim uczestniczyli.

Wiktor zaproponował Silurii wieczorne spotkanie u siebie w wiadomych celach. Siluria skontrowała - może u niej w pokojach; przygotowywała specjalnie pokój uciech korzystając z zaklęć i środków Kermita. On się uśmiechną - wielu magów rodu Diakon tygodniami pracowało nad jego buduarem. Stać go. Silurii zaświeciły się oczy. Są zatem umówieni u niego, tego wieczoru.

Siluria wypytała Wiolettę o Wiktora; on potrafi być czarujący, ale ogólnie nie musi. To lekko sadystyczny typ z absolutną skłonnością do dominowania. (konflikt wygrany) Wioletta przyznała się Silurii, że ona często płakała i to mu się jeszcze bardziej podobało; na początku ona zupełnie nie wiedziała co ze sobą zrobić, ale nie odważyła się kiedykolwiek mu zaprotestować. To było dla niego jedną z tych rzeczy jakie się podobały. Jak się zorientowała, że nie ma udawać dzielnej i wytrwałej to zaczęła pokazywać jak bardzo źle się z tym czuje a dla niego to było jedynie paliwo do ognia. Wioletta nie byłaby z Wiktorem gdyby nie to, że nie ma innego konceptu na swoje życie. Dość zabawne - tak zdolna terminuska a tak bardzo ślepa.

Siluria wie, że wobec niej on się tak nie zachowa; wie też, że ma większe oczekiwania od Silurii, bo był kiedyś z Diakonką. 
Tym razem Siluria przygotowuje się dobrze - perfumy, feromony, wszystko po to by wzmocnić efekt na Wiktorze ALE I by Wioletta odprowadzająca Silurię to poczuła i poczuła ten efekt na sobie; by zobaczyła, jak można się przygotować. Też Siluria wzięła odpowiednie eliksiry wzmacniające jej staminę (Diakonka ma podwyższoną, ale chce być jeszcze wytrzymalsza). Ogólnie, Siluria chce być kochanką idealną. Siluria jeszcze ubrała się w jego stylu.

Do wieczora Siluria ma co robić. Pojawiła się nieśmiała jaskółka - Wioletta spytała, czy coś przynieść, jakoś pomóc... terminuska chce zobaczyć i zrozumieć. Zainteresowała się. Siluria dała jej jakąś rolę, niech czuje się przydatna, niech się uczy. Najlepiej - ułożenie włosów Silurii. Siluria dowodzi, Wioletta układa. I Wioletta jest uśmiechnięta, czuje się dobrze...

...to jest, do czasu. Przyszła Dagmara, spojrzała zdziwiona i powiedziała Wioletcie, że nie przyszła na salę treningową. Terminuska spięta odpowiedziała "nie przyszłam... bo nie przyjdę". Dagmara zauważyła, że zawsze przychodziła. Wioletta powiedziała, że już nie. Tym razem ma przerwę. Dagmara z pustym spojrzeniem przeprosiła i wyszła. Wioletta chciała wrócić do układania włosów Silurii, ale już była wybita z rytmu. Przeprosiła i poszła do swojego pokoju. Silurii zrobiło się jej żal - nikt nigdy jej nie pokazał, że dziewczyńskie rzeczy mogą być fajne.

Wieczór przyszedł szybko i było dużo do zrobienia. Wioletta pomogła (choć przedłużyła czas), ale Siluria miała wyprzedzenie. Poszła do Wiktora na czas, odprowadzona przez Wiolettę. Wiktor czeka; podał jej posiłek osobiście. W rozmowie spróbował ją wybadać, jak daleko jest w stanie się posunąć. Siluria nie gra niedostępnej, ale gra partnerkę. Wiktor zaadaptował się; udaje partnera, by stopniowo z upływem dni przekształcać ją w uległą. Silurii to idealnie pasuje, jeśli on myśli, że on ją tak zmienił, nie zauważy, w co gra Siluria...
Pierwszy wieczór był bardzo łagodny. Wiktor zażądał by Siluria przyszła i ona przyszła. Jemu to wystarcza. Na razie.
Na plus, Silurii się bardzo podobało.

Wiktor "Jutro znowu przyjdziesz."
Siluria "Oczywiście."

Dzień 5:

Siluria obudziła się w swoim łóżku. Lekko zdziwiona, ale tak będzie; Wiktor lubi budzić się sam.

Z rana Siluria zaproponowała trening. Wioletta się zgodziła. Siluria widzi, że ta jest ciekawa, ale ani słowa nie powie.
Podczas walki Siluria się przykłada. Nie wychodzi jej, ale stara się uczyć. Siluria zaczęła działać troszeczkę inaczej w walce - zaczęła szeptać opowieści buduarowe, dekoncentrując Wiolettę i sprawiając, że ta gorzej sobie radzi. Wioletta się zorientowała (w końcu), ale Silurii poszło lepiej. W końcu rozmowa przeszła na walki z kralothami... ogólnie, nie całkiem bezproduktywny trening.

Wioletta powiedziała, że późnym popołudniem będzie Jurij Zajcew. Powiedziała Silurii, że ZAPROPONOWAŁA jej spacer w tych godzinach po mieście. Siluria od razu zrozumiała - nie, woli zostać. Okazuje się, że Wioletta lubi Jurija. Jeśli Wioletta tu będzie, Jurij przyjdzie z nią porozmawiać; i wtedy Siluria może poznać Jurija.

Po skończonym treningu Wioletta udała się do siebie. Siluria wzięła prysznic i usłyszała pukanie do drzwi. W szlafroku, otworzyła Dagmarze. Dagmara wpierw przeprosiła za wtargnięcie wcześniej, ale powiedziała Silurii by ta przestała krzywdzić Wiolettę. Dagmara powiedziała, że widzi co Siluria z Wiolettą robi - rozkochuje ją w sobie. Siluria powiedziała, że lojalność się jej chwali, ale to nie jest jej cel. Dagmara wyciąga Izę Łaniewską. Terminuskę. Kurtyny, owszem, ale terminuskę. A teraz - Iza Łaniewska została oddana Millennium na zabawkę i będzie sprowadzona do roli zabawki.

Siluria się zdenerwowała i dała to odczuć. Wpierw Dagmara przychodzi przeprosić (gest dobrej woli) a potem coś takiego. Wioletty nie skrzywdzi. A sprawa Łaniewskiej była bardzo skomplikowana i Siluria nie zamierza tego Dagmarze wyjaśniać; nie ma obowiązku. Dagmara uściśliła: nie przeprosiła Silurii z uwagi na gest dobrej woli; przeprosiła, bo się zachowała niewłaściwie. Nie uważa, że Siluria jest mastermindem za zniszczeniem Izy. Uważa, że Siluria jest współodpowiedzialna za zbrodnię na Izie - terminusce, jakiej Dagmara nawet nie lubiła. Nie znała jej.
Siluria MOGŁABY rozdzielić Dagmarę i Wiolettę. Ale nie chce tego zrobić. Ona chce pomóc obu czarodziejkom; nie chce power suita za cenę tych dwóch. Nie jest tego typu magiem.

Siluria zorientowała się w jeszcze jednej rzeczy - Dagmara tak się przejmuje Wiolettą, bo ma tylko ją. Dagmara czuje się bardzo zagrożona - z jednej strony pojawia się klin między Dagmarą i Wiktorem (bo Siluria) a z drugiej między Dagmarą i Wiolettą. Dagmara się boi.

Dagmara się skłoniła i opuściła Silurię. Ważne jest to, że Dagmara nic nie może Silurii zrobić - i wie o tym. Ale Siluria też wie, że chwilowo i ona nie może nic Dagmarze zrobić. Dagmara jest strategiem, nie politykiem; tu nie do końca jest jej pole bitwy. Siluria jest w miarę bezpieczna.

Siluria się ślicznie odpicowała dla Wiktora a potem poszła do Wioletty licząc, że Jurij tam będzie. Miała rację. Jurij już tam był. I ku swemu wielkiemu zdumieniu Siluria widziała, że Wioletta praktycznie płacze a Jurij ją pociesza. Okazało się, że pokazał jej serum mające dać tymczasowo specjalne moce ukazania różnych form opresji a Wioletta, kierowana ambicją, zdecydowała się je zażyć. Jurij dał jej żałosną ilość, nie pełną dawkę. Wioletta musiała nagle skonfrontować się ze swoim tabu, uczuciami, ambicją, zasadami, wszystkim co nią steruje. Zobaczyła jak bardzo jest zniewolona i jak bardzo wszystko w co wierzy pochodzi od innych. A ten jeszcze rozdrapuje rany "czy to jest to czym chcesz być?" "czy dlatego zostałaś temrinusem?" "czy chcesz być nieszczęśliwym niewolnikiem i piątą córką?" Siluria przyszła ją po prostu przytulić i pocieszyć. Ta wyszlochała, że uczucia też ją niewolą. Jurij z radością wykrzyknął, że tak, więzi takie jak teraz buduje z Silurią to opresja! Ale to opresja, która może się opłacać. Wolność ostateczna, od wszystkiego nie jest celem!
...przynajmniej tyle... choć Siluria martwi się o poczytalność Wioletty po tym wszystkim.

Jurij chce wstrząsnąć Wiolettą, zmusić ją do reewaluacji swojego życia: 17
Siluria chce odwrócić uwagę Wioletty; chce osłonić Wiolettę by ta nie zrobiła czegoś głupiego: 17

Silurii się udało. Wioletta jest w szoku, nie widziała się w takim lustrze, ale dzięki Silurii jakoś sobie z tym poradzi i jedynie wyjdzie silniejsza - zupełnie jak chciał Jurij (i Siluria). Wioletta płakała jak mała dziewczynka. Jak nigdy dotąd.

Żółw: "Na chwałę Arazille... pracuje Jurij i Diakonka..."

Siluria wywaliła Jurija; Wiktor nie może czekać. Zrobiła Wioletcie odpowiedni makijaż, żeby nie było widać, po czym poprosiła Wiolettę, by ta poczekała w jej pokoju. Sama poszła do Wiktora. Chciała pójść sama - to oznajmiła Wiktorowi. Mag się uśmiechnął. Ma dla Silurii suknię. Oczekuje, że Siluria ją założy - i życzy sobie "przy nim". Siluria spełniła jego żądanie. Wiktor jest zadowolny, Siluria też.
By zapobiec szybkiemu atakowi Dagmary, Siluria zdecydowała się zadziałać pierwsza. Powiedziała Wiktorowi, że przekształca Wiolettę tak, by ta była lepsza z jego punktu widzenia. Chce, by Wioletta była erotycznie poprawna. Wiktor zmrużył oczy. Bez konsultacji z nim? Siluria się uśmiechnęła. Woli przepraszać niż prosić. Wiktor odpowiedział uśmiechem. Pogłaskał Silurię po głowie i powiedział, że każdy może mieć tylko jednego pana - niech Siluria tego nie zapomina. Siluria odpowiedziała jako partnerka. Wiktor się ucieszył. Ma co łamać.

Dzień 6: 

Dagmara poszła do Wiktora powiedzieć o potencjalnych problemach z Silurią i Wiolettą. Ten ją zjechał jak psa "a czemu jak myśli on chce mieć tu Diakonkę"? Dagmara się ukłoniła i odeszła.

Rano Gerwazy odprowadził Silurię na spotkanie z Dianą Weiner.
Diana powiedziała Silurii, że istnieje możliwość docelowego skończenia wojny domowej w Świecy. Ona chce skończyć tą wojnę. Chce przywrócić Diakonów do poziomu Wielkiego Rodu Srebrnej Świecy, tak jak przed powstaniem Millennium. Diana jest idealistką. Chce równych praw dla Mausów, Myszeczków, Bankierzy... Dianę nie interesuje wojna z Kurtyną. Dianę interesuje pokój i Nowa Świeca. Prosperująca Świeca. Coś, co jest niemożliwe pod aktualną administracją po Zaćmieniu. Za dużo starych interesów, za dużo niepewności i niewiedzy.

Diana zauważyła, że KADEM też może pomóc Szlachcie. Dla KADEMu silna Świeca jest korzystna. Siluria się zgodziła z tym, acz zaznaczyła, że nie ma mocy politycznej. Diana się zgodziła; chce tylko, by Siluria rozumiała o co toczy się gra. Skoro Siluria wybrała Szlachtę jako czarodziejka KADEMu, Diana uważa, że jest jej to winna by jej to powiedzieć. Siluria jest czarodziejem KADEMu, ale nie wpieprza się politycznie i nie można tego od niej oczekiwać. Oczywiście, Diana rozumie.

Siluria zostawiła otwarte drzwi Dianie do rozmowy dalszej. Jest otwarta.

Na treningu Dagmara i Wioletta się ścięły; Dagmara powiedziała jej, że Wioletta jest przez Silurię zmieniana w zabawkę dla Wiktora. Wioletta powiedziała, że ciało jest bronią i Dagmara się myli; tu chodzi o korzyści i możliwości. To ona wykorzystuje Silurię. Poszło na ostre, co jest korzystne na treningu ;-).

Wieczór. Siluria nie przyszła do Wiktora. Nie idzie. Czeka w swoim pokoju. 
Przyszła Wioletta i spytała Silurię, czemu ta nie przyszła. Bo nie. On musi przyjść. Wioletta oddaliła się powiedzieć to Wiktorowi. Wiktor przyszedł i powiedział Silurii, że ma zamiar nagrodzić Wiolettę. Wprowadził ją i kazał powiedzieć Wioletcie za co. Ta powiedziała, że sprzedała trochę rodowych rzeczy i oddała część kontaktów Szlachcie, dokładniej Wiktorowi. Jemu się to bardzo spodobało. Kazał Wioletcie się rozebrać (przed lekcjami Silurii już by drżała ze strachu) i nalał bardzo bardzo silny eliksir pobudzający do kieliszka. Kazał Wioletcie go wypić, po czym klęknąć i czekać. Sam zjadł kolację z Silurią po czym udał się na zabawę. A Wioletta cierpi (full denial).
Siluria doceniła ruch - Wiktor okazuje wszystkim, że on ma nad Wiolettą pełną władzę. Pokazuje też Silurii pośrednio gdzie on widzi Silurię, zwłaszcza po tym wszystkim mówieniu o partnerstwie. On nie wezwie Wioletty. Ale ona to zrobiła, wiedząc, że on to wykorzysta by posunąć power play mocniej. Jej pasuje... nie chce być łatwa a to jest fajna pułapka.
W skrócie, Wioletta jest bardzo szczęśliwa; eliksir robi swoje a Wiktor nie traktuje jej źle. Traktuje ją ostro, ale w granicach ostrości akceptowalnej przez Silurię; Siluria część tego przejmuje na siebie przez co Wiktor wie jak daleko może na Silurii się posunąć.

Dzień 7

Czas na wielką wycieczkę. Wioletta, Gerwazy, Siluria i Wiktor przygotowują się do przejścia przez Bramę by dotrzeć do 'Creo Monde'.
Suknia od Wiktora wpisuje się w dress code; Siluria idzie w niej. Wszyscy czworo mają maski; wypada.

Magowie przechodzą przez portal. Na drugiej stronie czeka na nich silny fort check. Wszyscy padają nieprzytomni.

Laboratorium Mgieł. Gerwazy, jedyny przytomny (bo wybudzony) rozmawia z tien Tomaszem Przodownikiem, ekspertem od rekonfiguracji wzoru. Gerwazy powiedział mu, że tak jak kiedyś wprowadził Zmianę Wzoru na punkcie lojalności Dagmarze Wyjątek, tak teraz ma wprowadzić Zmianę Wzoru na punkcie lojalności Silurii Diakon. Przodownik zauważył, że to 2-3 dni ustabilizowania wzoru. Żaden problem - myślą, że są na 'Creo Monde'; fesiwalu gdzie w ich ciała są wstrzykiwane substancje chemiczne i są pod ogromnymi przeciążeniami magicznymi. Plan jest idealny.

Przodownik się zgodził. Mag KADEMu jest cennym nabytkiem lojalnościowo; jest to niebezpieczne, ale jeśli Gerwazy uważa to za słuszne... 

Jakkolwiek Aleksander Sowiński z sił specjalnych maskował te wydarzenia, Marian Agrest odpowiednio uprzedzony wcześniej przez Andreę monitorował działania magów Szlachty; gdy portal został otwarty do Laboratorium Mgieł to Agrest zrobił to, co uznał za najlepsze.
Doprowadził do tego, że lord terminus Ozydiusz Bankierz dowiedział się o sytuacji.

Siluria odzyskała przytomność. Przodownik powiedział jej, że musi być świadoma, jeśli mają manipulować jej wzorem; nie chcą jej uszkodzić. Siluria w pełnej panice. I na to wchodzi Ozydiusz...

Ozydiusz to Ozydiusz. Ryknął potężnie, nawet nie ukrywając jak jest wściekły. Pokazał palcem na Silurię i wrzasnął, że dla "tej samicy" Gerwazy jest skłonny wywołać wojnę między KADEMem, Diakonami i Świecą? Zwłaszcza teraz? Gerwazy odpowiedział, że nikt nie wie o tym że ona tu jest; oficjalnie są na Festiwalu Creo Monde. Alibi doskonałe, a modyfikacja wzoru jest bardzo delikatna. Ozydiusz powiedział, że coś takiego jest niewarte modyfikacji wzoru - dla Wiktora? Dla SZLACHTY? Zniszczenie rdzenia czarodziejki? Czy jemu doszczętnie odpierdoliło?
Ozydiusz za to zaproponował, by zmienić jej wzór na lojalność wobec Świecy. Gerwazy się zgodził. Ozydiusz ryknął - dla terminusa KAŻDA zmiana wzoru powinna być nieakceptowalna dla tego typu powodów; kim w ogóle Gerwazy jest, gdzie się podział w nim terminus?
Zupełnie się nie kontrolując i płonąc ze wściekłości, Ozydiusz brutalnie sprowadził Gerwazego do partneru pod swoim butem.

Chwilę później odwrócił się do Silurii i powiedział jej, że jest niewartą jakiejkolwiek uwagi żałosną samicą (pogarda w jego głosie nie jest udawana). Kazał ją rozkuć, po czym kazał Gerwazemu ją przepraszać i lizać jej stopy. Siluria w szoku - nie zgadza się, nie tak, nie to. Ozydiusz prawie ją uderzył, ale się zreflektował; sparaliżował ją magicznie i kazał Gerwazemu to robić, wcierając mu sól w rany, że tak chciał smakować kobietę, że aż zdradził swoją żonę i swoje ideały. Siluria jest w stanie grozy. Mówimy tu o LORDZIE TERMINUSIE KOPALINA, który się zupełnie nie kontroluje i który jest o tyci włos od granicy upadku...

Ozydiusz kazał Gerwazemu stanąć na baczność i zwrócił się ku Silurii. Powie mu wszystko co wie. Ozydiusz się zgodził. Założył Silurii "koralowiec" - normalny "koralowiec tortur" Diakonów ma moc [-10, 10] (co zademonstrował; od strasznego cierpienia aż do niesamowitej ekstazy). Jego koralowiec jest zmodyfikowany, wpina się w układ nerwowy ofiary i ma moc [-20, 20], zaprojektowany jest do niszczenia umysłów i skutecznego torturowania.

Siluria zaczęła błagać, by wyrzucił Gerwazego do innego pomieszczenia. Wszystko powie. Ozydiusz nie musi nawet używać koralowca. Lord terminus jednak ustawił [-4,4] na fałsz, prawdę: nie potrafi odczytać, czy Siluria mówi prawdę czy nie bez tego wzmocnienia.

I Siluria powiedziała mu WSZYSTKO. Mindmeld # słowa. Ozydiusz nie interesował się sprawami KADEMu ani Diakonów; ale wszystkim co w jakikolwiek sposób dotyczyło Świecy i jej działań wobec Świecy. Dowiedział się o Blakenbauerach, o potencjalnym Spustoszonym pancerzu... o jej planach, o jej planach wobec Wiktora, o jej chęci pomocy Wioletcie...
Na to Ozydiusz się żachnął. Powiedział z pogardą, że jak chodzi o niego, Wioletta może zostać niewolnicą Silurii. Jest nic nie warta.

Po kilkugodzinnym przesłuchaniu, Siluria jest wykończona, wyciemiężona i bardzo BARDZO nieszczęśliwa. Ozydiusz powiedział jej, że to co teraz zrobi jej się nie spodoba i włączył migotanie [-5, 5]. W ciągu minuty, 20 sygnałów [-5] i 20 [5]. Na to zaprosił do pomieszczenia Gerwazego i powiedział mu, żeby ten zobaczył co zrobił. Gerwazy został silnie wstrząśnięty - z JEGO punktu widzenia Ozydiusz tak ją torturował kilka godzin (co nie jest prawdą). Ozydiusz pojechał w pełni po psychopatycznej personie by zmaksymalizować poczucie winy w Gerwazym - on, terminus, na to pozwolił. On do tego doprowadził.

Udało mu się. Gerwazy już nigdy nie spróbuje czegoś takiego; coś w starszym magu odtajało...

Ozydiusz kazał mu jeszcze zrobić dwie rzeczy: sto razy całować ją po nogach (wtedy wyłączy koralowca) - to były najszybsze pocałunki w życiu Gerwazego - i pocałować ją w policzek, by ostatecznie zdradził żonę. Przełączył koralowca na [2] by jej ulżyć i powiedział Gerwazemu, że Siluria chce przejąć Wiktora jako zabawkę a jak cokolwiek jej się złego stanie, on osobiście dopilnuje by Gerwazy za to odpowiedział.

I kazał mu "spierdalać". 

Potem Ozydiusz powiedział Silurii, że Gerwazy będzie próbował ją pokonać a ona wszystko zapomni. Zaproponował jej, by skomponowała sobie sygnał ostrzegawczy (intuicja), by używając Laboratorium Mgieł miała poczucie zagrożenia ze strony Gerwazego; by nie szła tam jako ofiara na rzeź. Siluria doceniła i podziękowała. 

Ozydiusz jest potworem. Jest już "za daleko". Czerpie za dużo radości z tortur i zadawania bólu, ale jednocześnie jeszcze się zatrzymuje, jeszcze jest na granicy. Jeszcze się kontroluje. Da się mu pomóc, ale on na to nie pozwoli... i aktualna Świeca nie ma nikogo na jego miejsce. Dlatego Agrest go wspiera...

Dzień 9

Wszyscy wrócili z naprawdę fajnego festiwalu 'Creo Monde'. Muszą po tym odpocząć...
Siluria nic nie pamięta, ale ma poczucie, że Gerwazy jest dla niej bardzo niebezpieczny. Gerwazy pamięta to co pamięta, ma wyrzuty sumienia i musi się skonfrontować ze swoim życiem...

# Zasługi

* mag: Siluria Diakon, której misternie tkana sieć potencjalnie rozpada się w pułapce Gerwazego i pod okrucieństwem Ozydiusza. Przynajmniej zaczęła się zaprzyjaźniać z Wiktorią.
* mag: Wiktor Sowiński, który tak naprawdę jest manipulowany przez Dagmarę, Gerwazego... niby on dowodzi, ale nie do końca. Z Silurią próbują zdominować się nawzajem.
* mag: Wioletta Bankierz, która się trochę otworzyła na Silurię i zaczęła się z nią zaprzyjaźniać. Zaczyna rozumieć podejście Diakonki i konfrontuje się z marnością swej egzystencji.
* mag: Dagmara Wyjątek, pozycjonująca się przeciwko Silurii taktyk Szlachty. Próbuje chronić Wiolettę i Wiktora przed Silurią. Fanatycznie wierna Wiktorowi, ma ogromny kompleks winy.
* mag: Jurij Zajcew, fanatyczny wyznawca idei wolności, który dostarcza Szlachcie serum pokazujące magom wszystkie formy "opresji" jakie są na nich nałożone. Chce dobrze...
* mag: Diana Weiner, idealistka, która chce zakończyć wewnętrzną wojnę Świecy i chce zrównać Diakonów z innymi Wielkimi Rodami Świecy. Próbuje przekabacić Silurię do swoich planów.
* mag: Gerwazy Myszeczka, terminus, który dla ochrony Szlachty i Wiktora chciał zmienić Silurii wzór przez bardzo dobry plan. Niestety, Agrest i Ozydiusz mu przeszkodzili.
* mag: Antoni Kurzamyśl, nieszczęsna ofiara Silurii a tak naprawdę Szlachty. Przekształcony by być sleeper agentem u Emilii Kołatki.
* mag: Tomasz Przodownik, magiczny inżynier zajmujący się przekształcaniem wzoru w Laboratorium Mgieł. Wykonuje rozkazy i unika własnej opinii czy zdania.
* mag: Ozydiusz Bankierz, psychopatyczny terminus, który NADAL jest terminusem, choć czerpie ogromną przyjemność z zadawania bólu, cierpienia i niszczenia wszystkich dookoła. Lojalny Świecy. Dowiedział się wszystkiego co wiedziała Siluria na temat Świecy.
* mag: Marian Agrest, który przechwycił informację o działaniach Gerwazego i ostrzegł Ozydiusza, że Gerwazy chce KADEMce zmienić wzór * magiczny.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Pokoje gościnne
                                    1. Pokój Wioletty
                                    1. Buduar Wiktora
    1. Faza Daemonica
        1. Świeca Daemonica
            1. Krąg zewnętrzny
                1. Quintum Atrium
                1. Skrzydło Powietrza
                    1. Portal kontrolny
                    1. Laboratorium Mgieł

# Czas:

* Dni: 9

# Progresja

* Siluria Diakon: uczy się walki od Wioletty Bankierz
