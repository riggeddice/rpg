---
layout: inwazja-konspekt
title:  "Zamtuz przestaje działać"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150928 - Zamtuz z jedną wiłą (PT)](150928-zamtuz-z-jedna-wila.html)

### Chronologiczna

* [150928 - Zamtuz z jedną wiłą (PT)](150928-zamtuz-z-jedna-wila.html)

### Punkt zerowy:

Ż: Kto jest głównym "moral guardianem"?
K: Starsza pani, Matylda Daczak; nie jest super wredna, ale konserwatywna, wnuki odwiedzają z rzadka.

## Kontekst misji

Czyczyż jest usunięty z akcji. Paulina wie, że w zamtuzie jest cholerna wiła i najpewniej ktoś próbuje zrobić coś z węzłem emocjonalnym; najpewniej go stworzyć. Ale czemu tu? Do tego jest cały ekosystem przestępczy który działa tak a nie inaczej; pozwala na to co się dzieje. Paulina nadal nie jest wykryta a artykuł Marii jeszcze nie uderzył. Wiadomo, gdzie jest problem - leży w Gęsilocie.
I teraz Paulina musi zdecydować co z tym zrobić.
## Misja właściwa:
### Faza 1: Gęsilocki Węzeł słabnie

Paulina potrzebuje pluskwy w zamtuzie. Musi się dowiedzieć co w ogóle dzieje się w Gęsilocie; nie chce jednak sama została atrakcją zamtuza i nie chce by Maria została taką atrakcją.
Ale! Jest możliwość wprowadzenia pluskwy. Trzeba zapluskwić Czyczyża; on tam wróci. Paulina poprosiła Marię o zdobycie jakichś pluskiew; najlepiej takich, które nadają się do wprowadzenia do guzików. Przesłała Marii specyfikację guzika i poprosiła, by ta jak najszybciej skombinowała taką pluskwę. Maria złapała się za głowę; to wymaga OGROMNEJ roboty w krótkim czasie. Udało jej się zdobyć taką pluskwę i przyszła do szpitala przekazać ją Paulinie.
Paulina z trudem zdążyła przyszyć pluskwę do ubrania Czyczyża zanim przyszła go odwiedzić pewna kobieta (Gala).
Maria natychmiast dostała obraz.

Gala zażądała od Pauliny, by ta powiedziała jej odnośnie stanu Czyczyża i kiedy ten wyjdzie ze szpitala. Paulina się postawiła - nie jest członkiem rodziny. Gala pokazała upoważnienie, Paulina powiedziała, że nie ma próbki pisma i poświadczenia notarialnego. Między kobietami zaiskrzyło sztyletami, ale Gala się wycofała. Powiedziała Paulinie "jak pani chce, pani Tarczyńska" i poszła sobie. Paulina poszła do Hermana zgłosić sprawę i wyszła po tym, jak jej szef powiedział, że nie zrobiła źle.

HIDDEN MOVEMENT:

- Tymek Maus kalibruje i czyści węzeł do poziomu ekstazy \| E.(tinker with [node])
- Węzeł wpływa też na Tymka; on też przespał się z wiłą \| EN.(dominate [Tymek])
- Matylda Daczek wyspowiadała się księdzu \| P.(locate [evil])

END OF HIDDEN MOVEMENT:

Noc. Paulina skończyła szychtę i wróciła do domu; czeka tam Maria. Maria powiedziała Paulinie - ku jej wielkiemu zasmuceniu - że powoli kończą jej się fundusze. Będzie musiała wrócić do pracy...
Po rozważeniu za i przeciw, Paulina zdecydowała nie jechać do Gęsilotu nocą. To jest "ich" teren. 
Paulina poszła spać, Maria też.

Rano Paulina poleciała do szpitala, by zdążyć przed obudzeniem się Czyczyża. W samą porę - Czyczyża odwiedza wysoki, muskularny facio w okularkach przeciwsłonecznych. A to nie jest pora odwiedzin. Tyle, że Czyczyż jest już lekko przytomniejszy (gość podał Czyczyżowi tabletkę od Gali).
Paulina złapała pielęgniarkę i spytała, czemu ten człowiek tu jest. Ta, spłoszona, "bo wszedł" - widać, że się go boi. Paulina złapała dwóch ochroniarzy, poprosiła ich by poczekali pod drzwiami i weszła. Poczuła lekką emanację magiczną od Czyczyża; "ucieszyła się". 

Paulina napadła słownie na mięśniaka i powiedziała, że ma podać dokumenty. Ten powiedział, że nazywa się "Stefan Czyczyż". Mariusz Czyczyż pod wpływem stymulantu działa coraz lepiej.
Paulina przeprowadziła wszystkie badania i wszystkie procedury jakie są potrzebne do wypisania pacjenta. Czyczyż się chce wypisać asap i Paulina się na to zgodziła (ale tak by nie mieć problemów). "Braciszek" wyszedł po dobroci, uśmiechając się pogardliwie do Pauliny.

Tymczasem w Gęsilocie ksiądz zbiera ludzi do akcji modlitewnej mającej wyegzorcyzmować zło z zamtuza.

Czyczyż wyszedł. Paulina wróciła do pracy. Do końca dnia pracy Paulina została w szpitalu a Maria relacjonowała jej, co ma z pluskwy od Czyczyża.

HIDDEN MOVEMENT:

- ksiądz zaczyna wielką akcję modlitewną przeciw zamtuzowi \| P.(engage in combat directly)
- Węzeł ma emotion shift w kierunku nadziei \| EN.(emotion shift to [hope]) 
- Tymek Maus kalibruje i czyści węzeł do poziomu ekstazy \| E.(tinker with [node])
- Węzeł emituje i traci sporo energii; efemeryda \| EN.(create a visible manifestation) 
- Tymek sobie nie radzi \| EN.(Corrupt a location or area)
- Gala to the rescue; przybywa z odsieczą i rozbija zgromadzenie \| M.(appear with defending mercenaries)

END OF HIDDEN MOVEMENT

Paulina ma bezpośredni feed z pluskwy. Usłyszała co następuje:
- Gala przepytywała Czyczyża, co w ogóle tam się stało. "Wiła oszalała". Gala jest niepocieszona. Pytała, czy wiła dostała leki. Tak.
- Gala odesłała Czyczyża do Gęsilotu, bo tam jest teraz "Tymek".
- W samym Gęsilocie Czyczyż zobaczył modlitewną pikietę. Jakieś kilkanaście osób. Pod dowództwem księdza. Olał ich i wszedł.
- Tymoteusz Maus w środku w panice kazał Czyczyżowi ich rozgonić; mówi, że "energia się przesuwa; nie ekstaza, coś innego".
- Czyczyż powiedział, że nie może nic zrobić. Wyszedł i próbował coś zadziałać, ale nie zaatakuje przecież a oni nie są na terenie.

Tu Paulina się zorientowała, że obecność modlących się ciągle ludzi mających tą samą wolę i te same cele muszą zdecydowanie przeszkadzać kataliście który próbuje czyścić węzeł lub przyszły węzeł. 

- Tymek krzyknął w panice, że traci kontrolę. Węzeł przesunął się w kierunku nadziei i czystości. Sama kolizja wewnętrzna rozerwała węzeł i pojawiła się efemeryda.
- Katalista, Czyczyż walczą z efemerydą. Wiła stoi jak zahipnotyzowana.
- Sytuacja jest w miarę stabilna; w końcu pojawia się Gala. Rozgoniła tych ludzi i pomogła Tymkowi.
- Rozmawiali o stanie węzła; Gala się pożaliła, że koszty operacji są coraz wyższe i to jej się nie podoba.
- Gala dała radę odwiesić wiłę silnym plaskaczem.
- Tymek powiedział, że łatwo to zregenerować; Gala parsknęła mu w twarz. To ona dostarcza surowców.
- Ustalili, że trzeba się pozbyć księdza. Nie zabić. Coś mu zrobić. Mają jakiś plan powiązany z wiłą. W nocy.

Ogromna prośba Pauliny do Marii - czy może pójść do biblioteki i poszukać czegoś o wiłach? Jak z tym walczyć, jak z tym sobie radzić... ogólnie, jak pomóc księdzu. Maria poszła do biblioteki i zaczęła jakieś poszukiwania.

Słabą stroną wiły są jej włosy. Jeśli spali się wszystkie włosy wiły, ona zdecydowanie przestanie istnieć. Palenie włosów wiły (jakakolwiek forma ich zniszczenia) rani całą wiłę. Mimo całej jej szybkości i siły fizycznej, wiła jest podatna na ten typ ataku.
Jest też wrażliwa na zimne, kute żelazo.

Maria zadzwoniła z losowego numeru telefonu prepaid do księdza i ostrzegła go, że ma wrogów którzy spróbują coś mu zrobić. Przedstawiła się jako "przyjaciółka" i szybko się rozłączyła. Powiedziała, że włosy są rozwiązaniem. Więcej nie wyjaśniła - Maskarada. Powiedziała o Samsonie.

Następnie Maria zabrała się za robienie ulotek. Ulotek, które chce rozrzucić po terenie. Ulotek, które mówią o deprawowaniu nieletnich i wzywają do walki z pogwałceniem moralności. Część wzywa do walki z nielegalnym zamtuzem, siedliskiem wszystkich chorób...
Po czym Maria wynajęła grupkę dzieciaków, by poroznosili dla niej te ulotki po Przodku.

Paulina upewniła się, że następnego dnia nie ma niczego do zrobienia w Szpitalu Gotyckim. Następnego dnia ma luz.
Czas przygotować się na noc.

### Faza 2: Tak bardzo skuteczne nocne działania...

Paulina chce pojechać sama; bez Marii. Maria oczywiście protestowała, ale ktoś musiał zrobić ulotki. Ponadto (poza ulotkami), artykuł jest do dopilnowania... a w najgorszym wypadku magowie dowiedzą się o istnieniu Pauliny. Jakoś to przeżyje.

Ksiądz Tomasz został ostrzeżony, że może ktoś chcieć mu coś zrobić. Paulina chce być na miejscu by pomóc księdzu jak co. Ale jak to zrobić? Paulina zdecydowała się pojechać do Gęsilotu i spotkać z kimś, kto pozna ją jako lekarkę (+3). Dzięki temu może iść do księdza i poprosić o przenocowanie a mimo ostrzeżenia ksiądz nie powinien być tak podejrzliwy.

Faktycznie, udało się zaskarbić sobie panią Matyldę (leczyła się już u Pauliny). Ksiądz jednak nie chciał przenocować czarodziejki; uznał, że to może być niebezpieczne, bo został ostrzeżony. Paulina próbowała argumentować, ale niestety się nie udało. Paulina podziękowała, udała, że odjeżdża i schowała się w krzakach. Maria lekko protestowała, ale Paulina powiedziała, że czasem takie metody są potrzebne. Paulina musi w końcu tylko wytrzymać nockę.

...oczywiście, ksiądz straży nie wystawił... sam miał stać? Smutek Pauliny, że jest tak bezbronny...

Późna noc. Paulina lekko zziębnięta i zmęczona (-2) siedzi w krzakach, ksiądz na parafii. Najpewniej poszedł spać...
Nagle - Paulina stanęła oko w oko z wiłą. Wiła jej się przypatruje ze zdziwieniem; obwąchała Paulinę, stwierdziła, że Paulina nie jest księdzem i zdecydowała się ją zignorować. Wiła przypadła w kierunku parafii i zaczęła szukać wejścia.

Paulina spróbowała wiłę wypytać; wejść z nią w interakcję jakiegoś typu by zbadać, czy ma do czynienia z istotą inteligentną, kierowaną... wyszło jej, że wiła jest istotą instynktów, ale ta zachowuje się... i nieludzko i niewilo. Używa instynktów wiły do działań ludzkich. Z tego wynika, że wiła ma swoje rozkazy i niekoniecznie jest sterowana. Wiła nie umie wejść do budynku; mogłaby wybić okno, spróbować otworzyć drzwi... ale nie umie o tym pomyśleć.

Wiła zaczęła drapać w drzwi, w ściany... nie wie jak wejść. Krzyknęła "pomocy!".
Zapaliły się światła na plebanii. Wiła krzyknęła "aaah!" i odskoczyła. Upadła na ziemię, udając, że cierpi, zwinęła się w kłębek i zaczęła wydawać żałosne kwilące dźwięki.

Ksiądz podszedł do drzwi i wyjrzał przez wizjer. Zobaczył leżącą na ziemi kobietę w białej, brudnej sukni. Zaczął odkluczać drzwi...

Paulina wpadła na pomysł. Spróbuje wyrwać wiłę spod kontroli magów. Wykombinowała bardzo niebezpieczne zaklęcie - użyć nekromancji by przełączyć wiłę przez wzór normalny i nieumarły (by osłabić jakąkolwiek kontrolę), katalizą uderzyć w jakiekolwiek zaklęcie jest na wile, minion master do zrozumienia formy kontroli i rozsypania tego... Paulina po prostu nie ma pomysłu, jak lepiej pomóc księdzu. A to może pomóc.

Maksymalny możliwy sukces: 16 (elita z lekkim wsparciem).
Baza Pauliny: 13
Wpływ Przodka (uszkodzenie magii): +3 (tool). Wpływ tego, że wiła jest żywiołowa: +1 (circumstantial)
Stan Pauliny: -2
17]16; sukces.

Wiła porażona czarem Pauliny aż się zwinęła. Zanim ksiądz zdążył otworzyć drzwi, wiła odskoczyła i kilkoma susami zniknęła w lesie. Ksiądz wyszedł, rozejrzał się...

Paulina jeszcze nie w pełni doszła do siebie po rzuceniu zaklęcia, zwłaszcza, że skupiła się na wypatrywaniu wiły (czy ta nie poluje na księdza). I w ten sposób ksiądz ją zauważył...
Ksiądz odezwał się do Pauliny (jeszcze jej nie poznał). Zziębnięta i zaskoczona Paulina zdecydowała się zrobić to w czym jest świetna.
W DŁUGĄ! W CIEMNY LAS!
Niestety, Paulina jest FATALNA w uciekaniu. Odwróciła się, nie zauważyła gałęzi, wyrżnęła nią w głowę i upadła na ziemię, zamroczona. Gdy próbowała się pozbierać, podszedł do niej ksiądz, zatroskany.
Paulina próbowała się pozbyć księdza, ale on powiedział jej, że niezależnie co "oni" kazali jej zrobić i do czego ją zmusili, on zaprasza ją na parafię; będzie bezpieczna. Nie spodziewał się, że "ich" macki sięgają tak daleko i że Paulina tak desperacko próbowała dostać się na parafię. Niech wejdzie, będzie bezpieczna.

Paulina zdecydowała się przyjąć zaproszenie księdza.

Maria szybko wymyśliła historyjkę - Paulina, młoda lekarka, nowa, zrobiła błąd; pomagała gangsterowi i z innej grupy dowiedział się o tym ktoś i teraz Paulina musiała coś zrobić dla nich. Przestraszyć księdza; dokładniej, "decenter him"; zbić go z tropu. Na tym jej działanie miało się kończyć.
Ksiądz to łyknął. Obiecał Paulinie, że nikomu nic nie powie i będzie uznawał wydarzenia tej nocy za straszne i w ogóle. Nie ma powodu robić Paulinie krzywdy.

Paulina się troszkę przespała i wczesnym rankiem wróciła do Przodka.
A Tymek Maus i Gala Zajcew dalej czekają na wiłę...

Maria została poproszona, by jednak ulotki nie poszły. Na razie nie. Nie ma po co wzburzać ludzi i eskalować; i tak artykuł swoje zrobi.

## Dark Future:

### Actors:
- Ofiary wiły (Ludzkie ofiary)
- ksiądz Tomasz (Paladin)
- proto-węzeł (Węzeł emocjonalny)
- Lea; wiła-pułapka (Pułapka)
- Maria (Explorer/Paladin)
- część mieszkańców Gęsilotu (Strażnicy moralności)
- Tymek; katalista (Engineer)
- Gala; kupiec (Merchant)
- Franciszek Marlin; gangster (Artisan)

### Faza 1: Gęsilocki Węzeł słabnie

- Tymek Maus kalibruje i czyści węzeł do poziomu ekstazy : TM.(tinker with [node])
- Węzeł wpływa też na Tymka; on też przespał się z wiłą : EN.(dominate [Tymek])
- Matylda Daczek wyspowiadała się księdzu : TL.(locate [evil])
- ksiądz zaczyna wielką akcję modlitewną przeciw zamtuzowi  :  TL.(engage in combat directly)
- Węzeł ma emotion shift w kierunku nadziei  :  EN.(emotion shift to [hope]) 
- Tymek Maus kalibruje i czyści węzeł do poziomu ekstazy  :  TM.(tinker with [node])
- Węzeł emituje i traci sporo energii; efemeryda  :  EN.(create a visible manifestation) 
- Tymek sobie nie radzi  :  EN.(Corrupt a location or area)
- Gala to the rescue; przybywa z odsieczą i rozbija zgromadzenie  :  GZ.(appear with defending mercenaries)
- Węzeł jest osłabiony i ograniczony  :  EN.(impulse weaken)
- Gala i Tymek postanawiają coś zrobić z tym księdzem... a najlepiej zapułapkować go wiłą.

### Faza 2: Nocne działania wiły

- Maria ma zostać i zająć się ulotkami a Paulina jedzie do Gęsilotu.
- Wiła została wysłana, by samotnie odwiedzić księdza. Czyczyż jeszcze nie jest wyleczony.
- Ksiądz się broni; wiła porażona srebrem, ale ksiądz porażony pięknem. Wiła ucieka.
- Paulina słyszy, że wiła jest kontrolowana przez magów; nie wiedzą, jak ją przekształcić (Tymek pytał Galę)
- Ksiądz rani się ciężko srebrnym krzyżem i próbuje wyjść z tego  :  TL.(stand your ground)
- Ksiądz dzwoni po egzorcystę przeciwko demonowi  :  TL.(call reinforcements)
- Tymek przekonuje Galę, że trzeba ściągnąć trochę Quarków do poszerzenia węzła  :  TM.(gather the parts)
- Gala wyjeżdża do Przodka załatwić wsparcie, Quarki  :  GZ.(supply Quarks / services to Tymek)
- Gala zaczyna mieć wątpliwości, czy ta inwestycja ma sens  :  GZ.(doubt that the [action] is profitable)
- Tymek przepuszcza węzeł przez wiłę by go usprawnić nie mówiąc Gali  :  TM.(improve the node)

# Zasługi

* mag: Paulina Tarczyńska, która uwolniła wiłę by zostać zauważoną przez księdza; nadal jest niezauważona, ale ciężko na to pracuje...
* czł: Maria Newa, dziewczyna od ulotek, zmyślania historyjek i kupowaniu podsłuchów... której kończą się pieniądze.
* czł: Matylda Daczak, "strażniczka moralności". Po przeleceniu wiły wyspowiadała się i nasłała na zamtuz księdza. Starsza pani.
* czł: Mariusz Czyczyż, Gala wyciągnęła go ze szpitala, ale jest w takim stanie, że nic sensownego nie zrobił.
* czł: Tomasz Leżniak, ksiądz w Gęsilocie. Wyrządził straszliwe szkody węzłu emocjonalnemu i przez niego * magowie stracili wiłę.
* vic: Lea Swoboda, wiła; pierwotnie zniewolona przez Galę i Tymka a teraz uwolniona przez Paulinę i moc Przodka.
* mag: Tymoteusz Maus, nie radzi sobie z Węzłem przy kółku modlitewnym i ogólnie Węzeł wybuchł mu w twarz. Inżynier węzłów my ass.
* mag: Gala Zajcew, rozgoniła kółko modlitewne ratując Tymka, wyciągnęła Czyczyża ze szpitala i zaczyna mieć wątpliwości, czy koszty mają sens.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. klub "Czarny Dwór
                        1. Główna biblioteka
                    1. Równia Słoneczna
                        1. "Mrówkowiec"
                            1. wynajęte mieszkanie Marii i Pauliny
                1. Gęsilot
                    1. chata Pirzeców