---
layout: default
categories: inwazja, campaign
title: "Nicaretta"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Ucieczka do Przodka](kampania-ucieczka-do-przodka.html)

## Opis

-

# Historia

1. [Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html): 10/07/09 - 10/07/12
(161103-egzorcyzmy-w-zonkiborze)

Henryk dotarł do Żonkiboru, zaproszony przez lokalnego księdza. Okazało się, że w Żonkiboru panuje aura pryzmatu i jest ona wyraźnie skierowana na erotyzm i orgie. Henryk prawie został 'usunięty' przez napalone nastolatki (nośniki magii), ale w końcu został aresztowany. Gdy wrócił, w kościele toczyła się orgia przyzywająca sukkuba - Nicarettę. Henryk sformował grupę wiernych stawiających czoło grupie 'kultystów', co zmusiło Nicarettę (inkarnowaną w zakonnicy) do wycofania się, po czym wyczyścił Żonkibór. Nicaretta uciekła z księgą rytuałów i jest na wolności...

1. [Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html): 10/07/14 - 10/07/20
(161110-succubus-mysli-ze-uciekl)

Nicaretta (sukkub) ucieka. Zmieniła ciało na inne i próbowała zmylić ścigającego ją Henryka. Ten jednak za nią podążył, wpadł w pułapkę (został pocięty nożem, uratowany przez Lunę), po czym wszedł do Iglicy Trójzębu w Wyjorzu. Tam dostał namiar na drugiego, słabszego sukkuba (przyzwanego przez Nicarettę jako dywersja) i złapał "namiar" na aktualną formę Nicaretty. Współpracując z quasimafijnym politykiem w Wyjorzu z partii Kochamy Publiczne Pieniądze dorwał sukkuba w kontrolowanym przez partię hotelu odległego o ~10 km. Zostawił hosta pod drzwiami szpitala i uciekł pod iluzją.

1. [Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html): 10/07/21 - 10/07/22
(161115-uciekla-do-femisatanistek)

Hubert ma problem - jego bratanica, Natalia, uciekła do femisatanistek do Lenartomina. Poprosił Henryka, by ją sprowadził i dodał do pomocy Maję, swoją zaufaną strażniczkę. Na miejscu Henryk zauważył, że lokalny ksiądz to radykalny antysystemowiec który sympatyzuje z femisatanistkami (które zresztą nie robią nic złego; pomagają innym kobietom). Dowiedział się o potencjalnej manifestacji Arazille, działającej turystce która chce uzyskać Srebrne Lusterko i... ogólnie szefowa kultu Kora dała radę go wykorzystać by podnieść kohezję dziewczyn. Maję próbowała nawrócić Natalia - nie wyszło jej. Przechlapane.

1. [Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html): 10/07/23 - 10/07/24
(161129-ewakuacja-natalii-wejscie-maga)

Siostry Światła (femisatanistki) próbują przekonać Maję do stanięcia przeciwko swemu pracodawcy. Maja odmawia. Ksiądz Henryk poznaje Amelię Eter szukającą srebnego lustra bez odbicia; Kora obiecuje Amelii pomóc je znaleźć. Maja umożliwia Natalii na zemstę; ta jednak nie jest stworzona do zadawania bólu i chce do domu. Maja ewakuuje Natalię, ale w Siostrach Światła objawiła się czarodziejka. Ona Maję zdominowała i dała jej rozkaz zabicia Kaldwora. Henryk pojechał i zatrzymał Maję w Trójzębie rozrywając czar mentalny Marzeny. Przedtem - Henryk dowiedział się przeglądając monitoring kim jest "przyjaciel" Amelii szukający z nią tego lustra.

1. [Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html): 10/07/25 - 10/07/26
(161206-ucieczka-siostr-swiatla)

Marzena sprowadziła herpyneę by pokonać Henryka, stanowić ostrzeżenie. Henryk jednak użył ludzi i zaczarował księdza Klepiczka by wygnać Siostry Światła. Marzena dała radę wycofać się do tajnej bazy. Henryk wykorzystał to by podnieść swój ranking u Huberta Kaldwora. Hubert jest bardzo zadowolony.

1. [Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html): 10/07/27 - 10/07/30
(161220-zniszczenie-posagu-arazille)

Kinglord wysłał Karinę do pomocy Henrykowi, by pozbyć się posągu Arazille. W czasie, gdy Anna szuka Nicaretty z pomocą Huberta, Henryk i Karina uzgodnili wspólnego wroga - Arazille - i wrócili do Lenartomina. Odkryli, że tamtejszy kościół to strażnica na leylinie i że takie rzeczy się zdarzały. Niestety, gdzie Arazille się manifestuje wie tylko Ula Kram - jedna z femisatanistek, która uciekła z Marzeną. Jednak Natalia Kaldwor pod kontrolą Henryka wyśpiewała wszystko co wiedziała (bunkry) a Karina w bibliotece dowiedziała się, gdzie jest ten posąg. Przekupienie kilku młodziaków by zniszczyli posąg było proste. Manifestacja Arazille nie nastąpi szybko. Biblioteka jest atakowana przez "anty-femisatanistów".

1. ['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html): 10/08/01 - 10/08/04
(170113-dzisiaj-zlapiemy-nicarette)

Henryk wpadł na genialny pomysł zaskoczenia Nicaretty. Do tego potrzebował pielgrzymki - załatwił to przez zdominowanego Klepiczka. Jednak w nocy pojawiła się Marzena (podła sucz), która złamała czar Henryka i stwierdziła, że się zemści za wyżywanie się na femisatanistkach. Nie tylko zastawiła pułapkę (herpyneę) ale i przez Klepiczka ostrzegła Nicarettę i nadała temat, że Henryk jest pedofilem. Gdy Henryk i Karina poszli do Klepiczka, tam zostali zaatakowani; herpynea zraniła Henryka zanim ten ją egzorcyzmował. Karina wezwała wsparcie, załatwili Henryka by Kinglord dostał krew Henryka (a nikt Kariny nie podejrzewa). Henryk odzyskał Klepiczka, policja poluje na Huberta i Henryk jest NAPRAWDĘ zły na Marzenę...

1. [Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html): 10/08/05 - 10/08/06
(170120-wielki-sojusz-powszechny)

Henryk postanowił wejść w sojusz z Nicarettą; Karina mu pomogła. Przez pakty nieagresji i współpracy Nicaretta uwolniła Karinę (tymczasowo) od władzy Kinglorda. Słysząc te rewelacje, Henryk dołączył do sojuszu Marzenę (po jej złapaniu przez Nicarettę z Kariną). Nicaretta ma kilka pomysłów (przede wszystkim dookoła Arazille), ale te nikomu się nie podobają...

1. [Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html): 10/07/26 - 10/07/29
(170214-nekroborg-dla-laetitii-gai)

Laetitia - zakłócony Overlord Hipernetowy - połączyła się z Henrykiem i kazała im złożyć nekroborga, by walczył z Kinglordem. Nie mając lepszego pomysłu, Henryk i Karina pomogli Laetitii. Henryk wyczuł, że z Laetitią jest "coś nie tak". Fiodor - mag Świecy - dowiedział się, że Henryk może być w rękach defilera. Aha, przy okazji zbezcześcili sarkofag pod fabryką mikroprocesorów w Wyjorzu i uszkodzili samą fabrykę ;-).

## Progresja

|Misja|Progresja|
|------|------|
|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) -1 void na czyszczenie ludzi|
|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html) + księga rytuałów erotycznych|
|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html) zmieniło się postrzeganie jego osoby po płomiennym wypędzaniu Sióstr Światła|
|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html) dostała Srebrne Lustro Arazille|
|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) dostał jako wroga Marzenę Dorszaj|
|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) dostał jako przyjaciela Huberta Kaldwora|
|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html) niestety, jest dużo bardziej Zakłócona niż można się było spodziewać...|
|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html) dowiedział się o splugawieniu grobu S.S. przez Henryka Siwieckiego|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html) wie, że Henryk o niej wie i że "nadchodzi" i się może fortyfikować|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html) na którego plecach są policjanci dzięki Henrykowi|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) na jego temat pojawiły się pewne plotki związane z dziećmi...|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html) dostał reputację "specyficznego" (bardzo żarliwy jak chodzi o lokalnych błogosławionych i taki, który potrzebuje ciała niewieściego)|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html) dostał krew Henryka Siwieckiego dzięki działaniom Kariny|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) zaczęło mu troszkę zależeć na Karinie|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Nicarettą|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html) pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Henrykiem|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html) pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Henrykiem, Nicarettą|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html) pakt o nieagresji z Henrykiem, Kariną, Nicarettą|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html) dostał krew Marzeny Dorszaj|

