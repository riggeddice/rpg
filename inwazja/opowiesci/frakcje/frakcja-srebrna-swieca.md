---
layout: default
categories: faction
title: "Srebrna Świeca"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|nie chce oddać uszkodzonego Yyizdathspawna Yyizdathowi, ponieważ tam jest informacja o nowej Fazie a Yyizdath ma... historię eksploracji.|Wizja Dukata|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|z logów Kreta IV wydobędzie się informacje o tym, kto wmanipulował uszkodzonego Yyizdathspawna w potencjalną katastrofę.|Wizja Dukata|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|w Kopalinie ma sieć quasi-legalnego przekazywania artefaktów z KADEMem. Poza oficjalnymi kanałami i poza Lordem Terminusem.|Powrót Karradraela|

