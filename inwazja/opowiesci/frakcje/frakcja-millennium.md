---
layout: default
categories: faction
title: "Millennium"
---
# {{ page.title }}

## Location

* Centrala Millennium znajduje się na Śląsku, w Pirogu Dolnym (który jest oddany Millennium we władanie).
* Millennium ma swoje macki na całym Śląsku, acz operuje też poza nim. Millennium to nie miejsce a stan umysłu; wpływy Millennium poza Śląskiem są niewielkie.
* Wpływy Millennium raczej wynikają z ekonomii sztucznych ludzi i innych atrakcji Millennium niż z agentów.

## Ideation
### Purpose:
(why are we here)

Wspólnie do sukcesu, nieśmiertelni.

### Identity:
(who do we want to be)

* Organizacja w perfekcyjnej merytokracji. Wyniki, nie urodzenie. Urodzenie można zmienić.
* Rodzina - każdy każdemu pomoże, każdy każdego wesprze. Kolektyw nad jednostkę.
* Balans i harmonia między magami, viciniusami i ludźmi. Dla każdego jest miejsce.
* Organizacja wspierająca doskonałość fizyczną, mentalną, magiczną i w każdym innym calu.

### LongRange Intention:
(where do we need to go)

* Stworzenie perfekcyjnego homo superior. Przekonanie innych, że nie są zagrożeniem.
* Każdy członek Millennium jest powiązany siecią biomagiczną jak hipernet. Sieć jest świadoma i ma dostęp do każdego członka Millennium, za życia czy po śmierci. Nieśmiertelność.
* Każdy członek Millennium ma pomoc od innych członków Millennium - jeśli jest niespójny z Kolektywem, Kolektyw pomoże mu się uspójnić niezależnie od woli jednostki.
* Organizacja stosunkowo niewielka, ale bardzo adaptowalna i niesamowicie skuteczna.
* Dostęp do bardzo dużej ilości mięsa armatniego nie należącego do Millennium.
* Dostęp do bardzo dużej ilości środków na członka Millennium by móc eksperymentować i ewoluować.

## Nature
### Culture:
(how do we do things)

* Dominacja kultury relacyjnej i kultywacyjnej - silne nastawienie na osoby i indywidualność, bardzo silne premiowanie pomysłowości i kreatywności.
* Kultura absolutnej tolerancji - "każdy ma jakiś fetysz". Minimalne ograniczenie normami i prawami.
* Silna indoktrynacja pod kątem zakładania dobrej woli wobec Millennium i równoprawności agentów Millennium.
* Biologiczna indoktrynacja więzów rodzinnych między agentami Millennium.
* Nie ma tematów tabu czy rzeczy zakazanych. Są rzeczy niepożądane i niebezpieczne, ale nie zakazane.
* Millennium ma nastawienie hedonistyczne. Każdemu dla jego przyjemności.

### Structure:

* Bardzo luźna struktura. Agent podlega pod jednego z członków Triumwiratu i jest niezależny. Millennium jest tak naprawdę platformą komunikacji między agentami Millennium. Struktura platformy projektów.
* Triumwirat składa się z grupy magów zasłużonych dla Millennium, którzy kontrolują większość środków i nadają nurt. Zarządzenia Triumwiratu są ostateczne.
* Agent może zmienić "swojego" członka Triumwiratu, jeśli wizja innego bardziej mu pasuje.
* Triumwirat ma zawsze prawo wydać rozkaz członkom. To jest ostateczne. Rozwiązują problem konfliktujących przyjemności, zasobów... 

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Zbudowanie nowych szablonów rodowych, nie tylko Diakon - dla dywersyfikacji ewolucji.
* Zbudowanie ekosystemu harmonii dla magów, viciniusów i ludzi w swojej domenie. Każdemu według potrzeb, od każdego wedle możliwości.
* Zbudowanie sieci biomagicznej łączącej wszystkich członków Millennium zapewniając im wieczne życie w Sieci
* Dołączenie właściwych istot do Millennium tak, by nie rosło za szybko ale by "pasowało".
* Zapewnienie swoim istotom ekosystemów tak, by nie zniszczyć Millennium (np. kralothy).

## Strategy
(how do we achieve vision x nature)

* Wykorzystywanie wyhodowanych ludzi i viciniusów jako karmę i agentów.
* Umożliwienie i wspieranie wszystkich członków do ciągłej adaptacji siebie w to, czym CHCĄ być.
* Adaptacja małych grupek w formy najlepiej pasujące do rozwiązywanego makroproblemu.
* Łączenie agentów Millennium w małe łańcuchy projektowe do rozwiązywania problemów.
* Szantaż, erotyka, pink-ops. Też wszelkie formy biomancji.
* Millennium nie jest organizacją przestępczą z IDEI. Jest organizacją przestępczą przez ŚRODKI. Nie są to środki akceptowane społecznie, ale Millennium nie robi rzeczy "złych" jako grupa. Aczkolwiek są agenci robiący te rzeczy. Każdy agent zasila się finansowo sam.

## Key Strengths

* Niesamowita elastyczność i antykruchość. Millennium jest praktycznie niezniszczalne.
* Agenci Millennium są bardziej "doskonali" niż inni. I świetnie ze sobą współpracują. Świetnie potrafią współpracować z każdym.
* Nie ma dwóch podobnych agentów Millennium. W zależności od tego, jaki łańcuch się pojawi, działania agentów będą zupełnie inne i uroczo nieprzewidywalne.
* Dominujące środki to biomancja, viciniusy, sztuczni ludzie i erotyka.

## Key Weaknesses

* Millennium ma też osoby, które są po prostu ZŁE. I to rzutuje na całe Millennium - oni TEŻ dostają wsparcie i ochronę a kultura tolerancji ich nie blokuje. Dlatego Millennium musi być małe - i tak jest za duże.
* Millennium ma FATALNĄ opinię. Hedonizm, złe istoty, homo superior, kralothy, dewiacje...
* Millennium musi korzystać z biovatów (sztuczni ludzie, viciniusy), bo populacja istot Millennium jest za mała.
* Dominujący szablon Millennium to Diakon. To znaczy, że są bardzo wrażliwi na ataki eksterminacyjne biomagiczne.

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|ma zdecydowanie pogorszoną opinię przez Krystalię. Co jest warta gildia nie pilnująca swoich członków?|Adaptacja kralotyczna|

