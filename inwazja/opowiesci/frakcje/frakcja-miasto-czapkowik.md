---
layout: default
categories: faction
title: "Miasto Czapkowik"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|pojawiają się silne napięcia między frakcją pragmatyczną (pieniądze, paintball, kicz) a kulturalną (opera, poezja, sztuka)|Dusza Czapkowika|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Częścią obchodów Dni Jaszczura stała się tradycja Metalhead Rap Battle|Dusza Czapkowika|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Marketing i fundusze unijne sprawiły, że Czapkowik zaczął być znany jako miasto od jaszczurów|Dusza Czapkowika|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|Mimo epickiej jaszczurowatości, jednak ludzie oglądają Netflixa; Pryzmat się przesuwa z niebezpiecznego na zwykły|Dusza Czapkowika|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|echo zaklęcia Darii i Genowefy dotknęło źródła energii i wyprodukowało hipnokarabiny - efemerydę, która pragnie rosnąć.|Dusza Czapkowika|

