---
layout: default
categories: faction
title: "Efemeryda Senesgradzka"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|cały czas słyszy Karola Marzyciela w magitrowni Histogram.|Wizja Dukata|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|występuje na wielu Fazach jednocześnie i ma dopływy energii z różnych Faz.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|gdziekolwiek sięga zasięg efemerydy, tam jest zwiększona motywacja do pomagania ludziom przez wpływ Janiny.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|jest ogromna i łączy się z bardzo szeroką ilości leylinów i energii przez różne fazy|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|z niewiadomego dla wszystkich powodów rozwija program eksperymentowania na eterycznych świniach. W eterze.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|Zdobywać nowe imprinty, nowe istoty by móc wstawiać odpowiednie byty w odpowiednie scenariusze.|Wizja Dukata|

