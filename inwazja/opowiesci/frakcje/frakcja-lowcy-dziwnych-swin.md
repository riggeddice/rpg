---
layout: default
categories: faction
title: "Łowcy Dziwnych Świń"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|coraz ściślejsza współpraca z harcerzami na Mazowszu. |Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|stracili przywódcę, Pawła Kupiernika. Ale on został ich męczennikiem. Kult Kupiernika.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|mają bardzo silne przesłanki, że coś paranormalnego się dzieje i ze świniami też|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|uzyskali współpracę z kilkoma grupami mazowieckich harcerzy. Frakcja rośnie w ludzi.|Wizja Dukata|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|dostali jako wsparcie harcerzy w okolicy Półdary; harcerze są całkiem przekonani, że dzieje się tu coś bardzo dziwnego.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|schodzą do podziemia, bo rząd współpracuje ze świniami.|Wizja Dukata|

