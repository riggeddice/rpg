---
layout: inwazja-vicinius
title: Mimik symbiotyczny
---

# {{ page.title }}

# Mechanika

## Impulsy

* **pozostanie w ukryciu**: Mimik próbuje nie zwracać na siebie uwagi nikogo potencjalnie niebezpiecznego, zmieniając hosta jeśli musi.
* **drapieżnik**: Mimik jest drapieżnikiem. Poluje na ludzkie ofiary.
* **corruptor**: Mimik dąży do tego, by jego ofiara jak najczęściej używała jego mocy - i by być pożądanym

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  | astralika |
| Odporność        | +3  | energia magiczna, kontrola mentalna, wykrycie |
| Standardowa      |  0  | - |
| Wrażliwość       | -3  | ogień |
| Słaby punkt      | -5  | - |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  1  |   3  |   1  |   2  |   2  |   1  |   2  |   2  |

## Działanie viciniusa

#### Szczególnie dobre: 3

* **mimikra fizyczna**: (CRF), potrafi dostosowywać się wizualnie i strukturalnie; trudny do rozpoznania jako mimik
* **mimikra magiczna**: (SPN), potrafi maskować swoje działania jako wiarygodne efekty zaklęć i magii, chowając emanacje Krwi

#### Ponadprzeciętne: 2

* **wykrywanie pragnień**: mimik umie zbadać czego pragną osoby dookoła mimika; mistrz badania rynku ;-)
* **wykrywanie niebezpieczeństwa**: mimik jest dobry w wykrywaniu magii i potencjalnego niebezpieczeństwa dzięki pasywnej telepatii
* **kuszenie**: (SCD) mimik umie skusić swą ofiarę (lub potencjalną ofiarę) do założenia i użycia mocy mimika
* **wyładowanie emocjonalne**: (SCA/SPN) w ostateczności, mimik może uderzyć czystą energią emo-magiczną w cel, by go zranić.

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* osoby, na które mimik wpłynął i go pożądają: 1

#### Ponadprzeciętne: 2

* **Osoby pragnące zdobyć mimika**: osoby pragnące kupić, ukraść lub inaczej pozyskać mimika dla siebie
* **Osoby bliskie ofierze mimika**: osoby blisko ofiary mimika, które chcą ją chronić lub uważają ofiarę za kogoś "larger than life"
* **Whispers of the Corruption**: podszepty chwały, sławy i potęgi, jakie towarzyszą istocie, którą jest mimik.
* **Była ofiara mimika**: jedna z poprzednich ofiar mimika, która pragnie go odzyskać (lub go zatrzymać).
* **Element środowiska**: albo źródło magiczne, z którego mimik wypełzł (czy którym się chce rozmnożyć), albo mag, który skonstruował mimika.

## Specjalne:

* zmiennokształtny, zawsze idealnie pasuje do ofiary, budzi pożądanie
* w swoim "przeznaczonym" zastosowaniu daje ofierze bonus od +2 (narzędzie magiczne) do +4 (dedykowane narzędzie magiczne)

# Opis

## Spotkanie

* OFIARA: Nienawidzę ich! Gdybym tylko mógł się zemścić... a co to? Rękawica z... ćwiekami? Przyda się, przywalę im tym...
* OFIARA: Ooo! Ta rękawica... jestem... silniejszy. Szybszy. Nie wiem CO to jest i jakie wojsko to zgubiło, ale się przyda.
* OFIARA: Nic mi nie jest, mamo. Jestem zdrowy, naprawdę. To tylko kaszel. No dobrze, ale zemdlałem tylko raz.
* GŁOS_1: Pana syn zrobił się agresywny. Nie wiem, gdzie on ćwiczy, ale przemoc nie jest rozwiązaniem. Nie ćwiczy? Dziwne...
* GŁOS_2: Proszę coś zrobić z pańskim synem. Chodzi w tej głupiej rękawicy na lekcje i odmawia jej zdjęcia. Nic nie pomaga.
* GŁOS_2: Proszę pana, pana syn... nie żyje. Nie wiemy, co się stało. Po prostu... zgasł...

## Jak to wygląda:

Mimik symbiotyczny ma naturalną formę przypominającą "lądową meduzę" (zarówno strukturalnie jak i z parzydełkami). Jednak najczęściej spotyka się go w formie elementu ubrania, najczęściej bardzo pożądanej części stroju (np. koronkowa bielizna, świetna rękawica ogrodowa itp).

Co charakteryzuje mimika symbiotycznego - zwykle zwraca na siebie uwagę jako byt godny pożądania. Zwykle "pasuje" jako najlepsza rzecz do rozwiązania danego problemu. I zawsze - ale to zawsze - jest w formie mającej kontakt ze skórą.

## Co to jest:

Drapieżnik polujący na ludzi (zwłaszcza), magów i viciniusy.

W największym skrócie - mimik symbiotyczny jest istotą, która ma kształt kawałka ubrania i ma własności defilerskie. Żywi się energią życiową swojej ofiary, przekształcając część w coś korzystnego dla ofiary, zgodnego z formą i marzeniami ofiary. Można powiedzieć, że daje człowiekowi "moce viciniusa" działając jak zewnętrzna tkanka magiczna - kosztem energii życiowej swojej ofiary.

Magowie się spierają, czy mimika symbiotycznego klasyfikować jako artefakt, konstrukt, efemerydę czy defilera. Z uwagi na różne pochodzenie wariantów mimika symbiotycznego, wszystkie te warianty klasyfikacyjne mogą być poprawne z perspektywy napotkanego mimika. W zależności od pochodzenia mimika, różnić się będzie jego myślenie, poziom świadomości i bezpośrednia agenda.

Technicznie rzecz biorąc, mimik symbiotyczny ma pewne umiejętności telepatyczne a dodatkowo częściowo potrafi ukrywać emanację magiczną swojego działania, maskując to jako efekt zaklęć magów (a nie defilerskie chłeptanie). 

## Działanie:

* Mimik symbiotyczny próbuje znaleźć ofiarę - osobę mającą pragnienie/potrzebę - i odpowiada na to pragnienie przybierając odpowiednią formę.
* Mimik symbiotyczny daje się kupić/ ukraść/ znaleźć. Daje się założyć, dopasowując się by pasować idealnie.
* W ciągu godziny mimik buduje połączenie telepatyczne między sobą a swoją ofiarą. To połączenie służy do wzmacniania pragnień. Nawet, jeśli ofiara zdejmie mimika, będzie chcieć założyć mimika jak najszybciej
* Mimik łączy się z układem krwionośnym swojej ofiary. Część energii przekierowuje jako moc dla ofiary; dzięki temu ofiara uzyskuje zdolności paranormalne. 
* Mimik dąży do tego, by ofiara robiła rzeczy o ogromnej amplitudzie emocjonalnej - usprawnia to przyswajanie. Ułatwia to też uzależnienie od mimika.
* Mimik zawsze chroni swoją ofiarę; jest pasożytem i symbiontem jednocześnie.
* Gdy ofiara jest już zbyt słaba, mimik ją uśmierca i przenosi się do kolejnego nośnika.

Do skończenia powyższego cyklu (od skuszenia po uśmiercenie), mimik potrzebuje około 2 tygodni w wypadku jednego nosiciela. Jeśli nosicieli jest więcej i się zmieniają, trwać to będzie więcej. Zazwyczaj przy populacji kilkuset osób wymieniających się mimikiem żadna z ofiar nie odczuwa efektów negatywnych i zarówno mimik syty jak i ofiary całe (i mają korzyści).

UWAGA! Używanie mimika jest uzależniające (efekty Magii Krwi) i powoduje mutacje (obecność energii magicznej -> tkanka magiczna z tkanki ludzkiej)

## Występowanie:

Mimik symbiotyczny występuje zwykle w większych skupiskach ludzkich; targetuje raczej ludzi niż magów. Jakkolwiek ten artykuł opisuje mimiki symbiotyczne jako fil (kategorię, podobieństwa cech i zachowań) to jednak klady (pochodzenie, rodzina) mimików są różne.

* **klad: artefakt**: mimik skonstruowany sztucznie przez magów, najpewniej w określonym celu. Występowanie jest pochodną owego celu.
* **klad: konstrukt**: mimik - organizm "syntetyczny" (w kotraście z "biologiczny"). Rozmnaża się przez podział po akumulacji odpowiedniej ilości energii. Występuje w miejscach gdzie jest silne natężenie emocjonalne, duże skupiska ludzi oraz stosunkowo niewielka ilość magów.
* **klad: efemeryda**: mimik, który powstał jako efekt uboczny wyładowania energii magicznej czy Paradoksu. Występuje w pobliżu silnego Węzła / echa magicznego, lub przeniesiony od tego Węzła przez ludzką ofiarę.

## Wykorzystanie:

* Przed konwentem RPG, Sandra znalazła idealny kostium ulubionej superbohaterki na cosplay...
* Maciek, dręczony w szkole, znalazł rękawicę z ćwiekami dającą mu moc zemsty na prześladowcach...
* Paweł - sprzedawca - odziedziczył sygnet rodowy, dzięki któremu nestor rodu potrafił przekonać każdego...
* Paulina podąża śladem tajemniczych zgonów; w tle ciągle przewijają się przepiękne kolczyki... te same?
* Tajemnicza grupa spotyka się w piwnicy. Kto z owej piątki zakłada Płaszcz Thiefmana, tej nocy staje się królem złodziei...
* Karolina, czarodziejka, używa mimika całkowicie świadomie. Uważa, że zwiększona moc jest warta ryzyka uzależnienia...
