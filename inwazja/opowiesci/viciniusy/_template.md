---
layout: inwazja-vicinius
title: "NAZWA_VICINIUSA"
one_sentence: "JEDNO_ZDANIE_WYJAŚNIAJĄCE_CO_TO_JEST"
---

# {{ page.title }}

# Mechanika

## Moc vicinusa

* KROTNOŚĆ
* SIŁA_ELITARNY_SŁABY
* Zwykle ma TRWAŁOŚĆ = ?.

## Motywacje

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 


## Sposób działania

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Silne, słabe strony

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Ma do dyspozycji

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Preferowany teren

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

# Opis

## Spotkanie



## Jak to wygląda



## Co to jest



## Działanie



## Występowanie



## Ekonomia



## Zwalczanie



## Wykorzystanie



