---
layout: inwazja-vicinius
title: "Oczkodzik"
one_sentence: "świnia o trzydziestu trzech oczach"
---

# {{ page.title }}

# Mechanika

## Moc vicinusa

* KROTNOŚĆ
* SIŁA_ELITARNY_SŁABY
* Zwykle ma TRWAŁOŚĆ = ?.

## Motywacje

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 


## Sposób działania

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Silne, słabe strony

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Ma do dyspozycji

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Preferowany teren

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

# Opis

## Spotkanie



## Jak to wygląda

Świnia poczciwej wielkości; około 120 kg. Cechy anormalne: kilkanaście oczu na głowie w różnych miejscach, dodatkowe kilkanaście oczu na całym ciele. Oczy nie mają własności "zwykłych" (białko, tęczówka, boli przy dotyku...), jednak działają jak prawdziwe. Same oczy są formą projekcji iluzyjnej. Różne egzemplarze oczkodzika mają oczy w różnych miejscach, jednak zawsze oczkodzik ma 33 oczy.

Szczecina jest bardzo jasna, przez co oczkodzik jest uroczo różowy. Vicinius ten jest wszystkożerny, acz nie jest agresywny. Oczy pochodzą z różnych istot; mamy oczy świń, kotów, ludzi, gadów, demonów... ogólnie rzecz biorąc, sporo różnych oczu na jednym oczkodziku.

## Co to jest

Oczkodzik nie jest Skażeńcem. Jest to eksterian, który został udomowiony; pochodzi z jednej z nieodkrytych jeszcze Faz która ma punkty połączeniowe z Primusem. Naturalna ciekawość oczkodzika prowadzi do tego, że czasem ten vicinius pojawia się na Primusie. Nie udało się jeszcze znaleźć sposobu, w jaki oczkodzik zmienia Fazy.

Oczkodzik jest "świnią o wielu oczach". Jest to istota silnie wykorzystująca energie Zmysłów i Mentalną, acz nie do końca aktywnie. Struktura tego eksteriana jest stosunkowo niestabilna pod wpływem typów energii dominującej na Primusie, przez co oczkodzik bardzo szybko nabiera nowych własności, nowych cech i mutacji. Wszystkie te własności obracają się dookoła energii Zmysłów i Mentalnej.

Mięso oczkodzika ma właściwości mutagenne dla istot Primusa. Nie zjadać.

Jako vicinius jest to stworzenie nieagresywne i wykorzystujące swoje umiejętności przede wszystkim do obrony. Żywi się przede wszystkim konkretnym mięsem i roślinami; nie wiadomo co zje a co mu zaszkodzi. Najprawdopodobniej energie którymi się żywi są dla magów Primusa niewykrywalne.

## Działanie



## Występowanie



## Ekonomia

Oczkodziki są wykorzystywane w ekonomii świata magów na następujące sposoby:

* strażnik; oczkodziki mają świetne umiejętności wykrywania różnych rzeczy i widzą bardzo wiele.
* szperacz; oczkodziki są tresowalne i można nauczyć je znajdować rzeczy, które są ukryte.
* detektor; oczkodziki mają bardzo duże umiejętności rozpoznawania rzeczy na różnych kanałach. Oczkodzik przejrzy przez większość iluzji, gdyż zwyczajnie mało która iluzja jest kompletna.
* wsparcie medium; "oczy oczkodzików są lustrami do innych oczu". Niestety.
* wykrywacz kłamstw; konfuzja wywoływana oczami oczkodzika świetnie atakuje mentalnie i ułatwia przesłuchania.
* dziwna mutacja; mięso oczkodzika ma własności mutagenne przekraczające naturalne zrozumienie magii i energii Primusa.

Z uwagi na bardzo silną mutowalność oczkodzików utrzymywanie ich w stanie odpowiednim wymaga pewnych umiejętności. Niejeden raz się zdarzyło, że nieodpowiednią dietą czy własnościami udało się zrobić bardzo agresywnego i morderczego szperacza.

## Zwalczanie

* Oczkodziki są szczególnie wrażliwe na czystą katalizę, kryształy Quark i energię magiczną Primusa. Eliminacja czy odstraszenie oczkodzików powinno być powiązane właśnie z tymi własnościami.
* Oczkodziki powinny być zabezpieczane przy użyciu ekranów mentalnych lub niemyślących agentów, niezależnie od mutacji.

## Wykorzystanie

* doszło do niewielkiego Skażenia. Traf chciał, że w okolicach stada oczkodzików...
* żeby przejrzeć potężną iluzję potrzebny jest oczkodzik. Kamil zatem wyruszył z jednym, martwiąc się jak go nakarmić.
* mała gildia lokalna trzyma dwa oczkodziki i wykorzystuje ich mentalne umiejętności, by mieć przewagę w Widzeniu. Z jakim umysłem połączył ich oczkodzik..?
* w okolicach niewielkiej wioski doszło do masowych halucynacji i dziwnego zachowania ludności...

