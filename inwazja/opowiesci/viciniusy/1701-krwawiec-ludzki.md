---
layout: inwazja-vicinius
title: Krwawiec, ludzki
---

# {{ page.title }}

# Mechanika

## Impulsy

* **Sadysta (fizyczny)**: Zadawanie bólu i emisja Magii Krwi powodują nakarmienie głodu Krwawca. Ból psychologiczny działa mniej.
* **Agresywny**: Wojna jego żywiołem. Najlepszą formą walki jest walka bezpośrednia. 
* **Uzależniony od Krwi**: Na dłuższą metę MUSI ranić, zadawać ból itp. Jeśli tego nie zrobi, strasznie cierpi. Normalny głód Krwi ;-).

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  | - |
| Odporność        | +3  | przyjaźń, pozytywne emocje |
| Standardowa      |  0  | - |
| Wrażliwość       | -3  | bloodlust, podatny na prowokację, arogancja |
| Słaby punkt      | -5  | - |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  2  |   1  |  -2  |   0  |  -1  |   1  |   2  |   2  |

## Działanie viciniusa

#### Szczególnie dobre: 3

* **berserkerska furia**: Krwawiec bardzo łatwo wpada w szał bojowy, uodparniając go na ataki mentalne... i częściowo fizyczne.
* **krwawy wojownik**: Z uwagi na pasywną asymilację energii Krwi, krwawiec to straszliwy przeciwnik w walce bezpośredniej.

#### Ponadprzeciętne: 2

* **ucieczka i ukrywanie się**: Złapanie krwawca wymaga znalezienie go a potem wejście w jego domenę - a jest on szybki i sprytny.
* **ambush**: Krwawiec potrafi doskonale zastawiać pułapki i eliminować przeciwników, zanim ci się zorientują co się w ogóle stało.
* **zastraszanie**: Poza działaniami stricte fizycznymi, krwawiec jest zwyczajnie przerażający w akcji.

#### Podstawowe: 1

* **dawne ludzkie umiejętności**: Krwawiec kiedyś był człowiekiem; większość jego dawnych umiejętności trafia do tej kategorii. Tak, zdegenerował.

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* Wymuszenia, haracze jak i zwyczajne przysługi; też kradzieże i uprzedni sprzęt: 1

#### Ponadprzeciętne: 2

* **broń**: Nóż, maczeta, pistolet, tulipan... broń improwizowana i pielęgnowana. Broń krwawca jest zwykle skierowana na emisję Krwi bardziej niż na walkę.
* **swój teren**: Jak każdy drapieżnik, krwawiec doskonale ma opanowany swój teren, pozastawiane pułapki itp.
* **znajomości z dawnego życia**: Krwawiec był kiedyś człowiekiem i nadal ma kontakt z osobami, które go pamiętają jako inną osobę...
* **znajomości z nowego życia**: Krwawiec najpewniej zwrócił na siebie uwagę jako brutalny i skuteczny potwór...
* **reputacja potwora**: Krwawiec bardzo często więcej osiągnie strachem i zastraszaniem niż przechodząc do akcji.

## Specjalne:

* ?

# Opis

## Spotkanie

* GŁOS_1: ...co Ty...! Zostaw go! Maciek! Maciek, słyszysz mnie?!
* GŁOS_1: O Boże... muszę wezwać karetkę. Coś Ty mu zrobił?
* GŁOS_2: Powiedziałem mu, żeby zachowywał się z należnym mi szacunkiem.
* GŁOS_1: Zostawiłeś rodzinę. Mieszkasz... gdzieś na mieście. Teraz to. Co Ci się stało?
* GŁOS_2: Nic. Wszystko jest normalnie.
* GŁOS_1: Nic nie jest normalnie! Bierzesz coś, jakieś narkotyki?
* GŁOS_2: Nie. Po co? Uczucie pryskających kości Twojego wroga jest przyjemniejsze niż heroina.
* GŁOS_1: To jest Konrad. To Twój przyjaciel! A Ty go pobiłeś... może złamałeś mu kręgosłup!
* GŁOS_2: Może, nie dbam o to. 
* GŁOS_1: Wsadzą Cię do pierdla, rozumiesz?! Tak bez sensu!
* GŁOS_2: Niech spróbują <złowrogi uśmiech>.

## Jak to wygląda:

Człowiek, technicznie rzecz biorąc. Acz silniejszy, bardziej wysportowany, odporniejszy na ból i zdecydowanie bardziej impulsywny.

## Co to jest:

Krwawiec jest zwykłą istotą naturalną, która miała zbyt dużo powiązań z Magią Krwi lub energią Magii Krwi i przekształciła się w viciniusa Krwi. Krwawiec Ludzki jest Krwawcem pochodzącym od człowieka. Kiedyś zwykły człowiek, lecz jego ciało miało za dużo kontaktu z Magią Krwi i wykształciła się odpowiednia tkanka magiczna. 

Krwawiec jest silniejszy, szybszy, agresywniejszy i bardziej niebezpieczny niż zwykły człowiek. Dzieje się to kosztem relacji międzyludzkich. Krwawiec to doskonały ludzki drapieżnik, sadystyczny psychopata, który potrzebuje (uzależnienie) krwi i cierpienia innych. Działa jak cruciodefiler (żywi się cierpieniem), choć zdarzają się rzadkie warianty Krwawca-ekstadefilera.

## Działanie:

Krwawiec fundamentalnie działa jak CZŁOWIEK. Po prostu jak uzależniony od cierpienia (najczęściej cudzego) człowiek o bardzo niskim poziomie empatii i współczucia. Łatwy do sprowokowania, głodny walki i cierpienia, wiecznie głodny krwi. W pewnym momencie Krwawiec potrafi wpaść w szał berserkerski i nawet pożerać swoich (żywych!) przeciwników.

Samookaleczanie, okaleczanie innych, zadawanie cierpienia... to są typowe działania Krwawca. Bardziej drapieżnik w ludzkiej skórze niż człowiek. Impulsywny i działający zgodnie ze swoimi popędami. Jednocześnie - inteligentny.

Nie każdy sadysta to Krwawiec, acz praktycznie każdy Krwawiec (poza ekstadefilerskimi) to sadysta.

## Występowanie:

Krwawiec może powstać w wyniku jednego z następujących wydarzeń:

* Impuls energii magicznej (Węzła? Zaklęcia?) w połączeniu z torturowaniem i elementami Magii Krwi.
* Długotrwały wpływ zadawanego cierpienia i rytuałów. Taka "Magia Krwi bez magii" - MK przyciąga energię sama.
* Węzeł Krwawy Emocjonalny

Czyli zwykle dzieje się to w środowiskach brutalnych, podczas rytuałów itp.

## Wykorzystanie:

* Więzienie Terakota okryte jest złą sławą miejsca, z którego jak już ludzie wychodzą, to... nie do końca potrafią się odnaleźć...
* Katarzyna uczestniczyła w rytuale satanistycznym w pobliskim bunkrze. Parę dni później zabiła swoją młodszą siostrę, po czym popełniła samobójstwo...
* W mieście pojawił się "Nietoperzoczłek". W półświatku pojawiło się głębokie zdenerwowanie - okalecza on i torturuje ludzi, tak bez powodu...
* Rafał, kiedyś weteran ekspedycji do Elbonii, prowadzi brutalny FightClub. Niestety, potrzebne jest wsparcie z jego strony...
