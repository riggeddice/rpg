---
layout: inwazja-vicinius
title: "Opętańcze Echo"
one_sentence: "echo wydarzenia, wiecznie je odtwarzające"
---

# {{ page.title }}

# Mechanika

## Moc vicinusa

* Vicinius może być elitarny (przekraczać graczy) lub słaby (jednorazowe starcie). 
* Vicinius ten występuje pojedynczo; nie spotyka się wielu Opętańczych Ech w jednym miejscu, acz jedno Echo może opętać grupę osób.
* Zwykle ma TRWAŁOŚĆ = 3.

## Motywacje

* **Odtworzyć przeszłość**:
    * _Aspekty_: usuwać elementy zmieniające przeszłość, doprowadzić do powtórzenia elementów tak jak było, opętać i przygotować wszystkich do idealnego powtórzenia
    * _Sukces_: Przeszłość zdobyła teraźniejszość. Nie dzieje się nic nowego - tylko wieczne Echo z nowymi osobami wpadającymi w Echo, odtwarzając to samo wydarzenie przez całą wieczność...
    * _Opis_: Echo powstało w wyniku czegoś, co się kiedyś stało. Podstawową funkcją i celem echa jest sprawić, by to się powtórzyło. Live the same nightmare, cały czas ;-).
* **Wzmocnić traumę Pryzmatu**
    * _Aspekty_: wzmacniać spójne z Echem uczucia, usuwać elementy zmieniające Pryzmat, wzmacniać pole magiczne, wyostrzyć istniejący Pryzmat
    * _Sukces_: Coraz silniejsze pole magiczne, coraz czystsze emocje, coraz silniejsze Echo. Echo jest wieczne i pojawia się bardzo często, wpływając na coraz mocniejsze elementy. Cały świat ma tą samą traumę Pryzmatu.
    * _Opis_: Echo jest pochodną pola magicznego i silnych emocji. Echo "próbuje" wzmocnić to pole magiczne i dodatkowo "próbuje" wyostrzyć i zintensyfikować to pole.

## Sposób działania

* **Opętanie hosta**:
* _Siły_: inkarnacja przeszłości w istocie żywej, nadanie osobowości, opętanie klasyczne ;-), iniekcja fałszywych wspomnień, ignoruje efekt skali (opętanie masowe)
* _Słabości_: wymaga właściwych dla Echa okoliczności (teren, pole...)
* _Opis_: Podstawowa umiejętność od której bierze się nazwa tej manifestacji - Opętańcze Echo jest w stanie opętywać różnego rodzaju istoty.

* **Psychokinetyczna manipulacja**:
* _Siły_: incepcja myśli, telekinetyczna manipulacja, pomniejsze iluzje, przekonywujące szepty, uszkadzanie elektroniki
* _Słabości_: niewielka 'siła' (intensywność) psychokinezy
* _Opis_: Jak pomniejszy poltergeist; Echo jest w stanie wykonywać działania telekinetyczne. Dodatkowo ma ograniczony dostęp do iluzji/Zmysłów i wpływania mentalnego na cele

* **Traumatyzacja pryzmatu**:
* _Siły_: przekonywanie że TAK BYŁO, wypaczenie magii, wypaczenie Pryzmatu, wypaczenie artefaktów, adaptacja tekstu i legend
* _Słabości_: odpowiednio duża populacja o innym Pryzmacie
* _Opis_: Echo potrafi wpłynąć na pole magiczne i na Pryzmat w swojej okolicy. Potrafi wypaczać zaklęcia i doprowadzać do przekształcenia artefaktów. Dodatkowo Echo potrafi zmieniać pamięć osób dookoła i doprowadzać do uspójnienia legendy.

## Silne, słabe strony:

* **Emocjonalne Echo Przeszłości**:
* _Siły_: eteryczne i niezniszczalne fizycznie, trudno się go pozbyć, silne na "swoim" terenie
* _Słabości_: przewidywalne do bólu (MOC = 2), słabe poza "swoim" terenem
* _Opis_: To tylko echo czegoś, co się kiedyś wydarzyło. Wiedząc, co to było można przewidzieć co stanie się dalej i kto ma jaką rolę w tej opowieści. Jednocześnie Echo jest bardzo trudne do zniszczenia czy zatrzymania - stało się elementem pola magicznego i odbiciem pryzmatu w tej okolicy.

## Ma do dyspozycji

* **Okoliczności samego Echa**:
* _Siły_: potężny Pryzmat (MOC = 2), lokalne opowieści i legendy, przedmioty nieożywione "sprzyjają" Echu
* _Słabości_: znajdowalne historycznie, przewidywalne
* _Opis_: Przez Pryzmat i pole magiczne cała rzeczywistość "konspiruje" by stało się to, co miało się stać. 

* **Hosty i Opętańce**:
* _Siły_: krewni i znajomi hostów, administracja lokalizacji 'hosta', środki hostów
* _Słabości_: brak
* _Opis_: Opętańcze Echo nie musi wykorzystywać tylko jednego hosta naraz. Ma do dyspozycji cały ekosystem - miejsca, budynki, zwierzęta, ludzie. Wszystko, co jest potrzebne by Echo mogło zaistnieć ponownie zostanie wykorzystane.

## Preferowany teren

* **Opętany host**:
* _Siły_: hosta można poświęcić, jedna wola jedna myśl, host sprzyja Echu
* _Słabości_: host ma swoje osobne życie i myśli i marzenia
* _Opis_: Jeśli walczy się z Echem by uratować hosta, trzeba zmierzyć się z potężnie zintegrowanym Opętaniem nadpisującym samego Hosta. Jakakolwiek próba wyrwania hosta spod opętania będzie zwalczana, nawet do poziomu śmierci hosta. Echo musi się odbyć.

* **Straumatyzowany pryzmatycznie obszar**:
* _Siły_: znaczące pole magiczne, pryzmat spójny z Echem, "złośliwość rzeczy martwych" sprzyja Echu (jeśli samochód ma się zepsuć to się zepsuje)
* _Słabości_: efekt filakterium (uszkodzenie pryzmatu uszkadza Echo)
* _Opis_: Jeśli walczy się z Echem, które już opętało jakiegoś hosta, mamy do czynienia z walką na obszarze straumatyzowanym pryzmatycznie. Z uwagi na to, że każde echo jest echem czegoś innego nie da się jednoznacznie powiedzieć, czym ten teren będzie - biurowiec, jaskinia... Jednak da się wyciągnąć zbiór głównych cech takiego terenu.

# Opis

## Spotkanie

Ze wspomnień młodego LARPera uratowanego przez ucznia terminusa, który szukał zgubionego zegarka.

"Nie wierzyłem, że nimfy utopiły w jeziorze młodzieńca nocą. Nimfy przecież nie istnieją. Ale gdy odgrywaliśmy sztukę nad jeziorem, wieczorem... gdyby nie ten pożar, to moja siostra by mnie utopiła! Kazirodczo!"

## Jak to wygląda:

* Opętańcze Echo nie ma wyglądu. Zmienia zachowanie bytu opętywanego, ale samo w sobie nie "wygląda"
* Zdarzały się Opętańcze Echa wykorzystujące magię Zmysłów do lepszego odwzorowania echa przeszłości

## Co to jest:

* Wydarzenie w przeszłości, które się bardzo odcisnęło w polu magicznym.
* Wydarzenie, które próbuje się wielokrotnie powtórzyć. Nie jest do końca świadome. To echo.
* Klasyfikowane przez większość jako manifestacja efemerydy mentalnej, pryzmatycznej i magicznej.
* Jest to opętanie osoby, przedmiotu, zwierzęcia lub miejsca mające na celu doprowadzić do echa.

## Działanie:

* Opętańcze Echo opętuje hosta, którym jest zwierzę, człowiek czy obiekt. Próbuje przez swojego hosta (czy hosty) doprowadzić do wywołania jeszcze raz tych samych wydarzeń, które doprowadziły do powstania Echa.
* Echo posiada umiejętności wywołania pragnień, nadpisywania pamięci - ogólnie rozumiana magia mentalna.
* W wypadku opętania budynku czy przedmiotu, ten byt stanowi wektor wpływu mentalnego. W niektórych okolicznościach Echo posiada zdolności ograniczonej kinezy.

Echa są traktowane jako Skażenie; nie występują na wolności. Docelowo Echa się wzmacniają tym mocniej im mocniej zwiększają traumę pryzmatu danego miejsca. Odpowiednio stare Echo potrafi np. stworzyć Wieczną Wojnę, z duchami walczącymi po dwóch stronach przez całą wieczność i z każdym kolejnym człowiekiem przechodzącym w pobliżu który staje się elementem Wiecznej Wojny.

## Występowanie:

* Może pojawić się w każdym miejscu gdzie doszło do emisji bardzo silnych uczuć w połączeniu z bardzo silnym polu magicznym
* Opętańcze Echo musi coś lub kogoś opętać; inaczej jest czymś innym.
* Zazwyczaj występuje tylko jedno Echo w danym polu magicznym - to najsilniej emocjonalne.

## Ekonomia

* Opętańcze Echo nie służy korzystnie ekonomii; jest uważane za Skażenie.
* Zdarzały się przypadki wykorzystania Echa by zbudować pętlę sukcesu ekonomicznego, radości z dobrze wykonanej pracy i prób zrobienia z tego przydatnej pętli motywacyjnej.
* Niestety, długoterminowo obecność Opętańczego Echa powoduje silne Skażenie oraz wypaczenie mentalne ofiar. To sprawia, że Opętańcze Echo jest narzędziem eksploatatywnym, w najlepszym wypadku.

## Zwalczanie

* Rozproszenie pola magicznego lub jego silna puryfikacja.
* Rozdzielenie bytu opętanego od Opętańczego Echa; egzorcyzm (magiczny).
* Silne zmodyfikowanie Pryzmatu na danym terenie. Terapia traumy pryzmatu.
* Doprowadzenie do "zamknięcia" wydarzenia, którego wynikiem jest Echo.

## Wykorzystanie:

* Ktoś zrobił coś złego i zarejestrowało to pole magiczne. Echo jest jedynym śladem terminusa...
* W rocznicę krwawego rytuału Echo próbuje powtórzyć ten niefortunny moment.
* Kasia zachowuje się dziwnie. Jest rozkojarzona, chodzi w dziwne miejsca i reaguje na "Ania".
* Podobno pokój 69A jest nawiedzony - jeśli dwie osoby będą tam spać, skończą ze sobą w łóżku.

