---
layout: inwazja-vicinius
title: Glukszwajn
---

# {{ page.title }}

# Mechanika

## Impulsy

* **Taumatożerca**: Glukszwajn dąży do znalezienia silnie magicznych miejsc / punktów i ich sukcesywnego jedzenia
* **Istota stadna**: Rzadko spotyka się samotne Glukszwajny; raczej dąży do bycia w hierarchicznym stadzie kontrolowanym przez starą lochę.
* **Nomad**: Glukszwajny nie są stworzeniami terytorialnymi; to raczej stworzenia podróżujące telerportacjami
* **Odkażanie**: Glukszwajn jest naturalnym neutralizatorem Skażenia; ciągną do miejsc, gdzie Skażenie jest obecne i próbują zmienić Skażenie w energię Dostatku.

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  |  |
| Odporność        | +3  | Skażenie, czysta energia magiczna, odporność fizyczna |
| Standardowa      |  0  |  |
| Wrażliwość       | -3  | łatwo wykrywalne magicznie, srebro, techniki stadne działające wobec świń |
| Słaby punkt      | -5  |  |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  1  |  -2  |  -1  |  NA  |  NA  |   2  |   3  |   1  |

## Działanie viciniusa

#### Elitarne: 4

* **Eksplozja**: (SPN) Zabity Glukszwajn ma tendencję do eksplodowania ze straszną energią
* **Emisja**: (SPN) Silnie zagrożony Glukszwajn potrafi wyemitować część energii magicznej jako efemerydę czy czyste Skażenie

#### Szczególnie dobre: 3

* **Tropienie magii**: Glukszwajn wykryje nawet pomniejsze pływy energii magicznej czy schowane artefakty...
* **Rycie przez wszystko**: Złośliwi żartują, że pancerz aderialitha nie wytrzyma racic świniaka na dłuższą metę.
* **Teleportacja**: Glukszwajn potrafi bardzo szybko się teleportować, unikając ciosu czy ustawiając się w pozycji bojowej.
* **Pożarcie magii**: Glukszwajn potrafi rozmagicznić artefakt czy pożreć czystą energię magiczną naprawdę szybko.

#### Ponadprzeciętne: 2

* **Szarża**: Czy to jawnie czy z zaskoczenia, nie chcesz być celem Glukszwajna gdy ów atakuje...
* **Skażenie magii**: Energia magiczna w obecności Glukszwajna zwyczajnie zaczyna działać bardziej Paradoksalnie...
* **Pozytywna aura**: Płodność, Dostatek, Szczęście (pryzmatycznie: rzeczy korzystne) towarzyszą Glukszwajnowi w okolicy...

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* Zjawiska dziejące się w pobliżu Glukszwajnów: 1

#### Ponadprzeciętne: 2

* **Aura dostatku**: aura dostatku Glukszwajna wpłynęła na kogoś / coś bardzo pozytywnie.
* **"Coś" wykopanego**: czy to artefakt, źródło energii, węzeł, miejsce czy vicinius - Glukszwajn do czegoś się dokopał...
* **Pole energii / Skażenia**: Z jakiegoś powodu Glukszwajn przybył na ten teren. Jest tu gdzieś źródło energii. I coś robi.
* **Echo Emisji defensywnej**: uprzednia Emisja spowodowała "coś dziwnego": efemerydę? Skażenie?
* **Skażone zaklęcie / artefakt**: wpływ Glukszwajna negatywnie wpłynął na zaklęcie czy artefakt.

## Specjalne:

* Zabity w stanie "wysokim", Eksploduje i się rekonstytuuje jako "pusty". Zabity jako "pusty", ginie na serio. "Pusty" jak poje, znów jest "wysoki".

# Opis

## Spotkanie

Z pamiętnika technomanty mieszkającego na wsi:

"Budowałem sobie ten Węzeł w stodole, by zrobić ten ciągnik autonomicznym. Zajęło mi to dobre pół miesiąca, ta akumulacja energii i przygotowanie składników. No i wyjechałem na tydzień po pozostałe brakujące elementy. Jak wróciłem, moje pancerne zabezpieczenia zostały rozbite a cholerny świniak kończył wyżerał resztki tego co było moim Węzłem. Nawet wysadzić tego nie mogłem, bo rozwaliłbym pół wsi..."

## Jak to wygląda:

Sporej wielkości, dobrze odżywiona świnia, około 320 kg. Czasem występuje w formie świniodzika, acz rzadziej. Nie zdradza cech viciniusa, acz z uwagi na to, że jest "żywą baterią energii magicznej" jest wykrywalna przez katalistów.

## Co to jest:

Glukszwajn, czy "szczęśliwa świnia" jest odmianą stabilnej efemerydy inkarnującą się w świni. Klasyfikowana jest jako stabilny skażeniec. Glukszwajn jest żywą baterią energii magicznej i stężenie energii jest w niej często zbliżone - lub wyższe - niż stężenie energii w Węźle.

Glukszwajn jest kojarzony z terminami takimi jak: szczęście, płodność, dostatek, szukanie bogactwa.

Łagodny z natury, acz wszystkożerny, glukszwajn jest raczej korzystny w danym obszarze - pożera energię magiczną i rozprasza ją bezpiecznie przynosząc okolicy dobrobyt. To jest, byłby korzystny, gdyby nie regulacje rolne.

Glukszwajna nie warto zabijać. Jest to niebezpieczne. Gdy umiera, ma tendencje do eksplodowania - i formowania niestabilnych emisji i efemeryd...

## Działanie:

Glukszwajn ma kilka pozytywnych cech z perspektywy ekonomii magów:

* Służy jako bateria energii magicznej, przenośny i w miarę bezpieczny węzeł (też: akumulator)
* Służy jako jednostka pozbywająca się nadmiaru energii magicznej przy puryfikacji (bardzo odporne na Skażenie)
* Służy jako żywa bomba, wysyłana w konkretne miejsce z detonatorem...
* Służy jako istota poszukująca energii magicznej i pływów energii magicznej

Niestety, poza tym Glukszwajn ma też kilka mniej pożądanych cech:

* Potrafi się teleportować na krótkie dystanse, acz często, przemieszczając się bardzo szybko
* Zabity Glukszwajn eksploduje, po czym się rekonstytuuje po ranteleportacji; tylko głodny Glukszwajn może "naprawdę" zginąć
* Glukszwajn emituje energię magiczną w formie pasywnej; co prawda w formie "szczęście (rozumiane jako rzeczy pożądane pryzmatycznie) + płodność + dostatek", ale zawsze
* Potrafi przegryźć się przez większość zabezpieczeń i defensyw; kombinacja bardzo ochronnego ciała i "stalowych racic"
* Ma doskonały węch i wyczucie energii magicznej; przyrównywany jest do rekina czującego krew w wodzie

Na wolności, Glukszwajny zwykle poruszają się od nasyconego magicznie miejsca do nasyconego magicznie miejsca, wyżerając energię magiczną i idąc dalej. Watahy Glukszwajnów są często utrapieniem dla magów mających magazyny czy nawet pancerne TechBunkry; powszechnie wiadomo, że na dłuższą metę nic nie odeprze głodnych Glukszwajnów.

Są to stworzenia łagodne i przyjazne (w chwilach dobrobytu energii magicznej). Jeśli energii jest za mało i w pobliżu nie ma innych źródeł do których można przeskoczyć, Glukszwajny zmieniają się w mięsożerców...

## Występowanie:

* Watahy po 5-20 stworzeń. Jeśli za niski poziom energii, 2-3. Rzadko występują samodzielnie.
* Tereny rolnicze; na pewno nie miasta.
* Miejsca o wysokim natężeniu energii magicznej (a przynajmniej takie, gdzie się pożywi).
* Czasem jest hodowany przez magów jako żywe baterie energii magicznej, acz ma tendencje do teleportacji ucieczkowej.

## Wykorzystanie:

* Lokalny glukszwajn wykopał z ziemi coś silnie magicznego, co trafiło w ręce Kasi...
* Myśliwi polowali na dziczyznę i zastrzelili glukszwajna. Eksplozja wywołała potężną emisję oraz efemerydę...
* W Wieprznikach wyschło źródło energii magicznej. Nikt nie ma pojęcia, że lokalny glukszwajn zaczyna przerzucać się na mięsożerność...
* Czarodziej Cezary ma problem. Odkąd jego sąsiedzi zaczęli hodować świnie, jego zaklęcia nie działają tak jak dawniej... 
* Lokalne źródło energii magicznej zaczęło tracić moc; najpewniej jakiś taumatożerca sobie je upodobał...
* W okolicy doszło do Skażenia. Na teren wprowadzono więc kilka Glukszwajnów, jednak Skażenie dotyka też i je...
