---
layout: inwazja-vicinius
title: Wetrejan
---

# {{ page.title }}

# Mechanika

## Impulsy

* **?**: "?"

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  |  |
| Odporność        | +3  |  |
| Standardowa      |  0  |  |
| Wrażliwość       | -3  |  |
| Słaby punkt      | -5  |  |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  0  |   0  |   0  |   0  |   0  |   0  |   0  |   0  |

## Działanie viciniusa

#### Szczególnie dobre: 3

* **?**: ?

#### Ponadprzeciętne: 2

* **?**: ?


## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* ?: 1

#### Szczególnie dobre: 3

* **?**: ?

#### Ponadprzeciętne: 2

* **?**: ?

## Specjalne:

* ?

# Opis

## Spotkanie


## Jak to wygląda:

Część "kadłubowa" wetrejana to potężny, pancerny jeżokształtny kadłub. Taka wielka (spory wilczur, ~60 kg) krzyżówka małża z jeżem. Wetrejan kadłub porusza się trochę jak ślimak; z zewnątrz jest pancerny jak ankylozaur i na to wszystko nakładamy kolce (jeżozwierz, kaktus). Ogólnie, wolno poruszająca się, pancerna konstrukcja.

Część "jednostkowa" wetrejana to małe, słodkie jeżyki. Siedzi jeżyk na ramieniu i szepcze do uszka kołczingowe hasła. Ew. wchodzi do ucha i tam zostaje. Jeden kadłubowy wetrejan może rozsiać kilkanaście mniejszych jeżyków. Myślcie: lotniskowiec / samoloty, czy mothership / drone.

## Co to jest:

Wetrejan posiada inteligencję sprytnego zwierzęcia; drapieżnika. Da się go tresować, acz bardzo często treser staje się zwierzątkiem wetrejana po pewnym czasie.

Wetrejan jest psychicznym wampirem i podżegaczem ambicji. Potrafi wzmocnić ambicję do poziomów nieracjonalnych wśród wszystkich istot, które ambicję mogą posiadać. Ta istota specjalizuje się w generowaniu impulsów zazdrości, pożądania (ambicji) i strachu przed utratą (zwykle statusu). Następnie korzysta z energii wypromieniowanej przez osoby Dotknięte przez wetrejana i żywi się ich emocjami. Ich wypalenie i osłabienie wynika właśnie z tego, że wetrejan zjada ich energię. Jeden wetrejan może być wykarmiony przez ~10 osób na przestrzeni miesiąca (300 osobodni na miesiąc jest mu potrzebne). Jeśli w pobliżu jest źródło energii magicznej, może być tego mniej.

Wetrejan bywa hodowany przez magów. Jego wydzieliny stanowią świetny eliksir ambicji i motywujący; też można wykorzystać to jako coś dla rywali, by posunęli się za daleko i zrobili coś przekraczającego ich możliwości. Niektórzy magowie wykorzystują wetrejana jako trenera osobistego. Dodatkowo, wetrejan potrafi budować całe mentalno-iluzyjne światy dla swoich ofiar; to sprawia, że warto wykorzystać wetrejana do utrzymania Maskarady w terenie masowym (jeśli doszło do czegoś poważnego).

Z uwagi na umiejętności tworzenia mentalno-iluzyjnych pułapek w głowach osób pod wpływem, nie zaleca się bliskich kontaktów z wetrejanem bez wsparcia osób z zewnątrz; najlepiej istot takich jak konstruminusy czy golemy.

## Działanie:


## Występowanie:


## Wykorzystanie:

* Dwie zwaśnione rodziny zaczęły nagle eskalować; zaczęło się od wyzwisk, ale już ktoś przygotował widły... w pobliżu jest wetrejan
* W startupie zarządzanym przez maga zaczęło dochodzić do przemęczenia i działań nieracjonalnych. Umiejętność oceny ryzyka siada i "wszyscy są zwycięzcami"...
* Paulina przygotowała eliksir ambicji i podaje go swojej niemrawej córce, by ta mogła być szefową klasy...