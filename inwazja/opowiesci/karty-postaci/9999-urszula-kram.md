---
layout: inwazja-karta-postaci
categories: profile
title: "Urszula Kram"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161220|opowieść o niej. Raczej chłopczyca, interesuje się militariami. Kocha bunkry. Do momentu spotkania z Hipolitem...|[Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|10/07/27|10/07/30|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161129|z malutką rólką osoby jaką Natalia chciała pokazać Mai, by ta się przejęła. Maja się nie przejęła.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161115|uratowana ofiara Hipolita. Dzielnie dołączyła do grona femisatanistek. Przyjaciółka Natalii.|[Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|10/07/21|10/07/22|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|3|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|3|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|3|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Szczepan Szokmaniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-szokmaniewicz.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Romana Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-romana-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
