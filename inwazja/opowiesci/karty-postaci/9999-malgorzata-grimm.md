---
layout: inwazja-karta-postaci
categories: profile
title: "Małgorzata Grimm"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150429|kandydatka na terminusa, która przeszukuje archiwa i dokumenty umożliwiając Janowi jak najlepsze działanie.|[Terminusi w Zależu](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|10/01/05|10/01/06|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Onufry Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-zaleski.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Jan Grimm](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-grimm.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Aneta Kosicz](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-kosicz.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
