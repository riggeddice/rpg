---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Marian Łajdak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Sybaryta**: 
    * _Aspekty_: łapówkarz, chomik, kochliwy, lojalny
    * _Opis_: "lubię luksus, wygodę i świadomość, że mogę mieć wszystko na co mam ochotę. nie ukrywam, moją słabością zawsze były piękne panie. wszystko może się przydać; nie wiesz co i do czego użyjesz. Na przykład ta opona... może i wszystko jest na sprzedaż, ale przyjaciele - nie" 
* **KLT: szczur **:
    * _Aspekty_: przebiegły
    * _Opis_: "może i jestem szczurem… ale spróbuj szczura przechytrzyć"
	
### Umiejętności

* **Polityk**:
    * _Aspekty_: z każdym się dogada, 
    * _Opis_: 
* **Paser**: 
    * _Aspekty_: wycena, ocena, szmugler, handlarz, negocjator, kryjówki
    * _Opis_: 
* **Taktyk**:
    * _Aspekty_: plan na każdą okoliczność
    * _Opis_: "I have a plan for that" 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_: 
* **Technomancja**: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **Aportacja gadżetów**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **I have a tool for that**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

If there was one word to describe this particular mage it would be “resourceful”. Need to get access to the private part of the concert? He can get you there. Want to talk with people who don’t want to be seen with ones like you? He can make it work. Need an artifact which is not really legal? He can deliver.

A very quick thinking, silver tongue mage with access to many goods for trade and tools to progress his goals. Extremely loyal to those he calls friends and to the side he picks, very susceptible to beautiful women and being watched by the Silver Candle specialists in smuggling and illicit trade – Marian is willing to be one step ahead, trading favors and causing mayhem as a diversion for another lucrative smuggling operation.

He likes money and luxury. But he likes his friends much more.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180526|paser nieziemskiej klasy. Przetransportował zwłoki kralotha (Hralglanatha) do Jodłowca, by odwrócić uwagę Krystalii od ludzi.|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|10/12/17|10/12/19|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180402|przejął na siebie od Silurii problemy związane z Prokuraturą Magów i policją. Oczywiście, ma zamiar outsourcować.|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180311|jak trzeba sprowadzić coś dziwnego dla Silurii, nie ma nikogo lepszego od niego. Sprowadził świetną broń ceremonialną dla Kromlana.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170101|w Oplu przetransportował grupę * magów do Millennium. Zintegrowany z Laragnarhagiem jako dywersja (podwładny * mag kralotha).|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161231|przekazujący złoty ząb dziadka Kiryła Andrei...|[Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|10/07/11|10/07/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|zapewnił Andrei narzędzia antyspustoszeniowe i zapewnił jej swoje kontakty w świecie szmuglersko-przestępczym jako wsparcie.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|p.o. ambasadora w imieniu Andrei; też jej osobisty kierowca. Po* maga w określeniu najlepszego Węzła do zajumania. Trzecioliniowy.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161113|wybitny negocjator z ramienia... Świecy? Załatwił wsparcie ze strony Kręgu Życia dla nowo odbudowującej się Świecy we Wtorku.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|który został w odwodach na wypadek, gdyby Andrea była wroga KADEMowi.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160403|który ma nieaktywne Spustoszenie i rozpaczliwie chce zniknąć nim * magowie Szlachty go z nim złapią. Ma też Andżelikę Leszczyńską.|[Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141110|eks-Draceny który zapomina, że jest audytorem z KADEMu.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|audytor z KADEMu którego aktualnie największą zaletą jest bycie eks*chłopakiem Draceny.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161005|który próbuje szmuglować rzeczy, by KADEM nie zbankrutował. Poluje na niego Świeca, bo jest etap zimnej wojny.|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|10/05/05|10/05/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|przypadkowy bohater, odstraszający efektownym fajerwerkiem niebezpiecznego napastnika od Annalizy.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|KADEMowy kontroler GS "Aegis" który został Spustoszony.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|zakochany w Dracenie KADEMowiec skłonny się solidnie zadłużyć by ona była bezpieczna.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|robi rozpoznanie w świecie przestępczym dla Silurii - skąd wziął się "KADEMowy narkotyk". Źródło wiedzy.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|który podmienił część KADEMowych artefaktów do Kropiaktorium na mniej niebezpieczne... i się przyznał Silurii po 'zachęcie'.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|9|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|9|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|5|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|4|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|4|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|3|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|1|[160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Rafał Drętwoń](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-dretwon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Magnus Spellslinger](/rpg/inwazja/opowiesci/karty-postaci/1709-magnus-spellslinger.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Kaspian Miauczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-miauczek.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jan Kotlin](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-kotlin.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Aniela Lipka](/rpg/inwazja/opowiesci/karty-postaci/1709-aniela-lipka.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
