---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Anna Kozak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Idealistka**:
    * _Aspekty_: 
    * _Opis_: "To, co robię jest właściwe. Jestem jak… BATMAG! Przynoszę sprawiedliwość i porządek tam, gdzie ich brakuje."
* **FIL: Daredevil**:
    * _Aspekty_: niesubordynacja, buntowniczka
    * _Opis_: Kto nie ryzykuje, ten nie wygrywa. Uda mi się lub nie, ale przynajmniej próbowałam, nie to co inni. Im wyżej mierzysz, tym wyżej trafisz.
* **BÓL: Supremacjonistka - ludzie**:
    * _Aspekty_: 
    * _Opis_: "Nie zgadzam się na to wszystko - na to, że się mną bawią, że bawią się ludźmi, że pozwalają sobie na wszystko. Potęga NIE oznacza, że wolno. Magowie są potężniejsi, więc powinni się posunąć. Ludzi jest więcej i są dużo LEPSI niż magowie - milsi, przyjemniejsi…"
* **MET: Impulsywna**:
    * _Aspekty_: 
    * _Opis_: "Ach tak?! Nie, nie ma mowy!"


### Umiejętności

* **Terminus**:
    * _Aspekty_: uczeń, artefaktor
    * _Opis_: 
* **Scavenger**:
    * _Aspekty_: survival, wszędzie wlezie, nikt jej nie zauważy, włamywacz, artefaktor
    * _Opis_: 
* **Terrorysta**:
    * _Aspekty_: wybuchy, zasadzki, węzły, unieruchamianie, porywanie, gubienie pościgu, artefaktor, aktorka
    * _Opis_: 
* **Detektyw**:
    * _Aspekty_: podkładanie śladów, aktorka
    * _Opis_: 
	
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: 
    * _Opis_: 
* **Technomancja**:
    * _Aspekty_: 
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* niewielkie wsparcie ze strony Świecy i niewielkie dochody. Za to, dzielnie scavengeruje wszystko, co się da

### Znam

* ** wychowanka Tamary**:
    * _Aspekty_: możliwe wsparcie Tamary
    * _Opis_: 
* **Szerokie znajomości wśród ludzi**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **kryjówki i 'bazy'**:
    * _Aspekty_: pozastawiane pułapki w miejscach odwrotu
    * _Opis_: 
* **sprzęt do maskowania i przebieranek**:
    * _Aspekty_: 
    * _Opis_: 
* **Scrapper**:
    * _Aspekty_: 
    * _Opis_: mnóstwo ludzkiego sprzętu ze złomowisk itp

# Opis
A young and an impulsive daredevil. This one acts as if she wanted to die, simply because she lacks the common sense of what should be done and when it should be done. A terminus candidate, very resourceful, with her human memory scrambled.

She doesn’t remember anything about her past, except a location of some type; the earliest things she remembers is Tamara, her caregiver from the Silver Candle. She really, really dislikes what the magi have done to her and the fact they cut her away from the humanity.

A rebel by nature, she does not really listen to the authority and often pay the price for that – does not have many friends, doesn’t have political clout and is usually too stubborn, therefore she is the one who gets punished. This only reinforces her “it is not fair, everyone is out there to get me” mentality, which makes for one fun loop.

Ann is very proactive and resourceful. She does things considered to be impossible by even more skilled magi; she simply does them differently using mostly the human stuff. She could be considered to be a human supremationist in the magi ranks – she strongly believes that humans should not be exploited and that human affairs are more important than magi affairs; because magi are more powerful, they should move aside and help humans. Even at the cost of their own needs and goals.
## Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|uzależnienie od kralotha (Laragnarhaga)|Powrót Karradraela|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|Nienawidzi Ignata Zajcewa (z KADEMu) z całego swojego serca.|Powrót Karradraela|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|Przestraszyła się KADEMu. Siluria jest członkiem KADEMu, ale nie chce skrzywdzić Anny. Nie podejmie walki z KADEMem z własnej woli.|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170101|wpierw podłączona do kralotha jako "podwładny * mag kralotha" (dywersja), w wyniku czego się odeń uzależniła. Rafael podejrzewa "krew, erotykę lub kralotha" w jej Wzorze. Odesłana do Millennium na leczenie.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|w imieniu Andrei zdobywa zapasy finansowe i służy jako biznesswoman; też: zaproponowała miejsca w świecie ludzi do chowania się.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160229|która przyznała się Andrei do bycia "Bat* magiem". Andrea podejrzewa, że ta idealistka w swoim łóżku nie umrze.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|Bat* mag, tajemnicza postać, która osiągnęła co chciała - "Wyżerka" została zamknięta rękami Hektora Blakenbauera.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141012|uczennica na terminusa z Rodziny Świecy która nie dała się złamać|[Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|10/05/30|10/06/07|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150922|która nie ufa Świecy, chce chronić Poczciwca i się buntuje jak mała dziewczynka. Zobowiązania do odzyskania artefaktu.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150913|która okazała się być oddana do Rodziny Świecy przez Tamarę i która okazała się być tajemniczym atakujowcem KADEMowego poligonu. Naraziła Maskaradę dla ludzi. Nie chce być pionkiem. Zazdrosna o Salazara.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|podkochująca się w Salazarze czarodziejka nie traktowana przez nikogo poważnie. Zazdrosna o Salazara, skupia się na Dracenie.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|głupio porwała Silurię. Złapana. Przerażona, współpracuje jak może. A tylko chciała pomagać ludziom...|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140625|porwana przez Saith Kameleon jako źródło żywności (osobowości). Pożarta przez Kameleon. Nigdy nie zostanie już terminuską. KIA.|[Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|10/01/19|10/01/20|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140208|podkochująca się w Kaspianie kandydatka na terminuskę o wrogim do Tatiany stosunku.|[Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|3|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Wojciech Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-tecznia.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Zdzisław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislaw-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Wojmił Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Kaspian Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-bankierz.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Irena Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Gabriela Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriela-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
