---
layout: inwazja-karta-postaci
categories: profile
title: "Gustaw Bareczny"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170716|antypatyczny i cyniczny terminus o złośliwym poczuciu humoru i tendencją do niszczenia rzeczy. Fajny na swój sposób. Jakoś dogaduje się z Pauliną na swój sposób.|[Eteryczny chłopiec i jego pies](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|10/01/08|10/01/10|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Mikołaj Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Karol Wąski](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-waski.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Jakub Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Beata Obaczna](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-obaczna.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
