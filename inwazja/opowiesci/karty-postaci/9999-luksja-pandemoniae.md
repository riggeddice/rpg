---
layout: inwazja-karta-postaci
categories: profile
title: "Luksja Pandemoniae"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|zniewolona, w Świecy.|Córka Lucyfera|
|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|zdominowana przez Henryka Siwieckiego|Córka Lucyfera|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170628|zmanipulowana by powiedzieć prawdę Henrykowi; pod wpływem jego magii uwierzyła, że to ON jest Lucyferem.|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|10/02/16|10/02/17|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170620|zaczęła potężny pojedynek magiczny z Henrykiem i Estrellą, który przegrała. Skończyła jako zdominowana lalka Henryka Siwieckiego. Ranna.|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170603|silnie wierząca w Lucyfera, wyraźnie przez kogoś źle traktowana i morderczo niebezpieczna. Stworzyła demona do pomocy.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|tajemnicza czarodziejka mówiąca o sobie "córka Lucyfera"; czaruje krwawą magią i pragnie wzmocnić się kosztem poświęcania części ludzi.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|4|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|4|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Henryk Kantosz](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-kantosz.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Łukasz Tworzyw](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-tworzyw.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Stella Stellaris](/rpg/inwazja/opowiesci/karty-postaci/9999-stella-stellaris.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Paweł Parobek](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-parobek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Misza Dobroniewiec](/rpg/inwazja/opowiesci/karty-postaci/9999-misza-dobroniewiec.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Dżony Słomian](/rpg/inwazja/opowiesci/karty-postaci/9999-dzony-slomian.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Balbina Wróblewska](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-wroblewska.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Antoni Bieguś](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-biegus.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
