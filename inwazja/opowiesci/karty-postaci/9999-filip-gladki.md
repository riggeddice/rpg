---
layout: inwazja-karta-postaci
categories: profile
title: "Filip Gładki"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161103|ksiądz w Żonkiborze. Miał trójkąt z wikarym. Był chłostany by cierpieć za własne grzechy. Paskudna klątwa Nicaretty.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
