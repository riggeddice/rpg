---
layout: inwazja-karta-postaci
categories: profile
title: "Archibald Bankierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150120|mistrz trójki uczniów którego dogoniła kłopotliwa przeszłość z eks- oraz zaginionym pierścieniem.|[Pierścień też zniknął](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|10/01/03|10/01/04|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|141210|wiecznie optymistyczny i radosny organizator nie wpuszczany do własnej Archiville przez ową Archiville.|[Złodzieje kielicha w akcji](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mariusz Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-blyszczyk.html)|2|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html), [141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Adam Płatek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-platek.html)|2|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html), [141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Rafał Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Olga Pierwiosnek](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-pierwiosnek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Mirosław Cebula](/rpg/inwazja/opowiesci/karty-postaci/9999-miroslaw-cebula.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Miranda Delf](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-delf.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Marian Jogurt](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-jogurt.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Juliusz Jubilat](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-jubilat.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janina Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janek Łobuziak](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-lobuziak.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Anna Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-patyczek.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Aneta Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-patyczek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Aleksandra Pudryk](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-pudryk.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
