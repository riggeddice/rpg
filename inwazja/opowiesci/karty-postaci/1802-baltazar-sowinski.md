---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Baltazar Sowiński"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Aspekty_: dorównać idolowi, zemsta
    * _Szczególnie_: bądź ceniony i kochany, NIE żyj w cieniu idola, stań się jak najsilniejszym, NIE akceptuj, że inni mogą być lepsi, zgiń jako bohater, NIE akceptuj pogardę i hańbę
    * _Opis_: Idol = Sabina, jego kuzynka, która zginęła Spustoszona przez Karradraela.
* **Społeczne**:
    * _Aspekty_: pokój, kontrola
    * _Szczególnie_: wymuś Pax Świeca, NIE wybaczaj złe czyny, zapewnij wszystkim bezpieczeństwo, NIE ignoruj 'małego zła', wymuś by każdy miał swoje miejsce, NIE zezwól na wolność do wybierania zła, chroń wszystkich przed Mausami, NIE dopuść do drugiej Wojny Karradraela
    * _Opis_: 
* **Serce**:
    * _Aspekty_: waleczność, homo superior
    * _Szczególnie_: bądź na pierwszej linii, NIE odmawiaj pojedynków, dąż do perfekcji, NIE dbaj o zachowanie człowieczeństwa, posuń się tak daleko jak trzeba, NIE okaż słabości sympatią czy łagodnością, walcz do końca, NIE oddawaj pola silniejszemu, ucz się i ewoluuj, NIE akceptuj swoich granic
    * _Opis_: 
* **Działanie**:
    * _Aspekty_: bogactwo, charyzma, osobiście
    * _Szczególnie_: bądź bezwzględnie konsekwentny, NIE bądź łagodny i wybaczający, nieracjonalnie skupiaj się na celu, NIE rozpatruj środków ekonomicznie, stój na czele armii, NIE działaj ani samotnie ani przez innych
    * _Opis_: 

### Umiejętności

* **Katai**:
    * _Aspekty_: rycerz, szturmowiec
    * _Manewry_: wzbudzanie strachu przez atak frontalny, niwelowanie obrażeń przez emisję energii, przełamywanie obrony przez wspomaganą siłę, zmuszanie do zwarcia przez kontrolę terenu, przewaga przez walkę wieloosobową
    * _Opis_: 
* **Wódz**:
    * _Aspekty_: wódz, taktyk, łowczy
    * _Manewry_: wyszukiwanie wroga przez agentów, zagrzewanie do walki przez mowę, zapędzanie w kozi róg agentami, zmuszenie do starcia przez analizę przeszłych ruchów, wyczerpanie przeciwnika przez niekończącą się armię
    * _Opis_: 
* **Symbol**:
    * _Aspekty_: symbol, arystokrata
    * _Manewry_: wzbudzanie poczucia należenia przez elegancję i sztukę, budowanie niezłomnego morale przez własny przykład, wzbudzanie zachwytu przez efektowne działanie, niszczenie morale przez pokaz siły
    * _Opis_: 

### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Manewry_:
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: golemy bojowe, wspomaganie planowania, overmind
    * _Manewry_:
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: golemy bojowe, kształtowanie terenu
    * _Manewry_:
    * _Opis_: 
* **Transport**:
    * _Aspekty_: szybkie atakowanie, przemieszczanie się w bitwie
    * _Manewry_:
    * _Opis_: 

## Zasoby i otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zasoby

* **Serwomotorowy magitech**:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: Łączący magitech i technomancję, nadaje Baltazarowi niesamowitą siłę i prędkość. Ma wbudowany system 'Dragoon', mający kontrolować przeciwnika i zapewniać odpowiednią ilość dron ostrzeliwujących.
* **Runiczny Miecz Kościej**:
    * _Aspekty_: pożera magię, dwurak potężnej mocy, rodowy miecz Baltazara
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 

# Opis

36 lat. Baltazar Sowiński zwany jest też jako "Miecz na Mausów" od ujawnienia, że Karradrael stoi za wojną. Nosi w sercu wypaczony obraz swojej ukochanej kuzynki, Sabiny Sowińskiej i ciężko przeżył jej śmierć - a zwłaszcza sposób w JAKI zginęła. Zdaje sobie sprawę z tego, że ona nie zaakceptowałaby jego metod, ale mają ten sam cel - bezpieczny świat ze szczęśliwymi magami. Tyle, że Baltazar skrycie nie ma nic przeciw temu, by stać się potworem. A potem ten potwór może spokojnie odejść w niepamięć, gdy już wszyscy będą bezpieczni.

Prawdziwy homo superior, Baltazar Sowiński jest jak paladyn z legendy - a jego serwomotorowy pancerz jedynie wzmacnia to wrażenie. Łączący najnowocześniejszą technologię z najbardziej zaawansowanymi magitechami. Szybki, silny i mający niesamowicie podzielną uwagę Baltazar potrafi wejść na pole bitwy i wyjść z niego posiadając własną silną armię - "Czarną Armię", złożoną z golemów i podłączoną do jego servara.

Wielka tragedia Baltazara polega na tym, że jego szczep i szczep Sabiny rywalizowały ze sobą "od zawsze". Śmierć Sabiny sprawiła, że konflikt się może nigdy nie zakończyć. I tego Baltazar nie może wybaczyć Mausom. Tego i faktu, że naprawdę Sabinę podziwiał - a jej śmierć i ostatnie dni uszkodziły jej obraz w jego oczach.

### Koncept

Reileen + Avatar-of-War Marxon + Ashram. Supreme Commander meets Carralis Athorn.

### Mapa kreacji

brak

### Motto

"Zło ZOSTANIE wyplenione i wszyscy BĘDĄ bezpieczni."

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180310|zwany "Miecz na Mausów". Judyta Maus się go śmiertelnie boi. Wielki nieobecny, acz wspiera Marka Kromlana konstruminusami.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
