---
layout: inwazja-karta-postaci
categories: profile
title: "Sonia Adamowiec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170525|a journalist of "Dobro Ludzi"; she found out the Water Tower incident survivor and gave a lead about Madler being bankrupt to another journalist.|[New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|10/01/13|10/01/15|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefan Brązień](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-brazien.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Rafał Adison](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-adison.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Paweł Madler](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-madler.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Paweł Franka](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franka.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Kurt Zieloniek](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-zieloniek.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Klara Pieśniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-klara-piesniarz.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Karol Szczur](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-szczur.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
