---
layout: inwazja-karta-postaci
categories: profile
title: "Maciek Drzeworóz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180419|siedemnastolatek z Czapkowika; wezwał pomoc magów po tym jak przeraził go jaszczur i zostawił dziewczynę (Olę) samą z nim w operze.|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|10/11/17|10/11/19|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mateusz Podgardle](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-podgardle.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Alojzy Bunnert](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-bunnert.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Aleksandra Kurządek](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-kurzadek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
