---
layout: inwazja-karta-postaci
categories: profile
title: "Jan Wątły"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170215|złożył mobile suit dla Silurii, by ta - ranna i słaba - mogła się poruszać i swobodnie działać.|[To się nazywa 'łupy wojenne'?](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|10/08/14|10/08/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|gdy Ignat powiedział w Sali Wojny "uderz najsilniejszym co masz" skutecznie uderzał tym, co pokonywało Ignata i nie dawało mu szansy na wyżycie się ;-).|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150729|który zaalarmował Pawła, że Bogumił Rojowiec zwinął kaczkoryzator i zniknął. Też: współtwórca kaczkoryzatora. DLACZEGO!|[Kaczuszka w servarze](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|10/01/19|10/01/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Renata Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-maus.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[GS Aegis 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Abelard Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-abelard-maus.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
