---
layout: inwazja-karta-postaci
categories: profile
title: "Sebastian Drabon"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170120|drab. Zgarnął nieprzytomne dziewczyny z Kromp (łącznie z Marzeną). Powoli ulubiony drab Henryka.|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
