---
layout: inwazja-karta-postaci
categories: profile
title: "Łukasz Perkas"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160117|który wmanipulowany przez Korzunia stał się jego agentem. Korzunio go uratował od oskarżeń ze strony kultu / nieletniej.|[Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151229|dziennikarz śledczy, który został tubą propagandową dla kultu. Ostrzegł Marię, by w to nie wchodziła.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151230|który - jak się okazuje - jest skutecznie szantażowany przez sektę.|[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|dziennikarz, który miał nieszczęście znaleźć "Kły Kaina" i został przez nich wrobiony w gwałt na nieletniej celem szantażu.|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|4|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|3|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|1|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Leopold Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Kinga Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Filip Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ernest Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|1|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
