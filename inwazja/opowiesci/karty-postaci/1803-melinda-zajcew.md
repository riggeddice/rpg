---
layout: inwazja-karta-postaci
categories: profile
factions: "Zaćmione Serce"
type: "NPC"
owner: "public"
title: "Melinda Zajcew"
---
# {{ page.title }}

## Koncept

Nastoletnia Zajcewka podkochująca się w Warmasterze

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Dla siebie        | wielka miłość; szacunek w rodzie|
| Dla innych        | |
| Co ceni           | |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| być podziwianą;  |  |
|  |  |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia transportu    |  |
| Biomancja           | rośliny magiczne; początkujący alchemik |
| Magia elementalna   |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180425|szesnastoletnia czarodziejka zabujana w Warmasterze. Chciała ulepszyć klimat Zaćmionego Serca, a skończyła goniąc anty-depresyjne śledzie...|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|10/11/28|10/11/30|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
