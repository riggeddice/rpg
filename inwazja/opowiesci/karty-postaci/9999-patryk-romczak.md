---
layout: inwazja-karta-postaci
categories: profile
title: "Patryk Romczak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150602|katalista w służbie sił anty-Triumwiratowych, który okazuje się być bezwzględny i kompetentny. Niebezpieczne połączenie.|[Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|10/02/19|10/02/20|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|kompetentny lifeshaper, katalista i puryfikator któremu spodobała się zagadka Kasi i jej tkanki magicznej. Z charakteru konkretny. Na oko koło 50 lat.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|katalista i lifeshaper wysłany przez Andromedę do wsparcia Augusta.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150105|mag który dla ciekawości i przyjemności torturował i przekształcał wielu ludzi. Ekspert od tkanki magicznej.|[Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|3|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|2|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Zofia Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tadeusz Umiej](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-umiej.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Staszek Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-staszek-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pasan](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pasan.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
