---
layout: inwazja-karta-postaci
categories: profile
title: "Gustaw Siedeł"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140611|który upewnił Andreę, że Sophistia jest bezpieczna i nikt nie ma do niej dostępu.|[Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|10/01/15|10/01/16|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140604|który schował Sophistię przed mackami Blakenbauerów i otoczył ją osobistą opieką ukrywając przed wszystkimi możliwymi siłami.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140320|Wieczny Anarchista kumplujący się z Sophistią i mający na nią oko który przejął organizację przestępczą do ochrony ludzi przed... samochodami.|[Sprawa magicznych samochodów](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Damian Bródka](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-brodka.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
