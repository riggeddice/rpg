---
layout: inwazja-karta-postaci
categories: profile
title: "Sławek Broda"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|pragnie nauczyć się gry na saksofonie. Dużo ćwiczy w ruinach starej opery czapkowickiej|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180418|kiedyś imprezowicz nie z tej ziemi. Ostatnio słucha disco polo i jest apatyczny. Ukradł saksofon i chciał się poświęcić by obudzić Operiatrix.|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Paweł Szlezg](/rpg/inwazja/opowiesci/karty-postaci/1803-pawel-szlezg.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
