---
layout: inwazja-karta-postaci
categories: profile
title: "Jan Karczoch"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170409|jeden z Pentacyklu, architekt, konstruktor i iluzjonista; chce się wykazać, 24, wciągnął Paulinę do Pentacyklu jako główny ambasador|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
