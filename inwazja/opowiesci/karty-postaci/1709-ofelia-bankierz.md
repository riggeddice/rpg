---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Ofelia Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Challenge seeker**: 
    * _Aspekty_: zadziorna, twarda, drapieżnik, niemoralna, "prawo jest tylko narzędziem"
    * _Opis_: 

### Umiejętności

* **Arystokrata**:
    * _Aspekty_: orator, poezja erotyczna, duelist
    * _Opis_: 
* **Prawnik**: 
    * _Aspekty_: prawo świata magów, obrońca, kruczki prawne
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **prawnicy Świecy**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **przepastne archiwa historyczne**:
    * _Aspekty_: rzadkie kodeksy
    * _Opis_: 
* **ugruntowana marka i renoma**:
    * _Aspekty_: 
    * _Opis_: 
* **dworek w Karym Grodzie**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Lady Ofelia Bankierz jest 44-letnią czarodziejką o wysokim poziomie kultury i intelektu. Na co dzień mieszka w Warszawie, ale przybyła do Kopalina odpowiadając na pojawienie się tam Hektora Blakenbauera i spodobało jej się; została. Lady Ofelia Bankierz ma mały dworek w Karym Grodzie, w którym normalnie mieszka z trójką dzieci. O jej reputacji świadczy fakt, że nikt nigdy nie odważył się na atak na owe dzieci.
Czarodziejka Świecy, choć sympatyzująca ze Szlachtą. Ma opinię doskonałej oratorki, bardzo dobrej pojedynkowiczki a do tego słynie z popularnych tomików poezji erotycznej. We wszystkim co robi musi być zawsze najlepsza; nie ustępuje i oczekuje, że druga strona też nie ustąpi. Jeśli ktoś unika z nią konfliktu, lady Ofelia potrafi zmusić drugą osobę do walki.
Prywatnie, bardzo sympatyczna. Tyle, że nadmiernie dąży do współzawodnictwa w każdym calu.
Jedna z najlepszych prawniczek Srebrnej Świecy.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
