---
layout: inwazja-karta-postaci
categories: profile
factions: "Dare Shiver, Srebrna Świeca"
type: "PC"
owner: "kić"
title: "Kinga Bankierz"
---
# {{ page.title }}

## Koncept

Młoda, paranoiczna terminuska mająca problem z nawiązywaniem przyjaźni

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria  | Aspekty                           |
|------------|-----------------------------------|
| Dla siebie | szacunek; władza;                 |
| Dla innych | silna Świeca                      |
| Co ceni    | bezpieczeństwo; przyjaźń          |

#### Szczególnie

| Co chce by się działo? Co jest pożądane?   | Co na pewno ma się NIE dziać? Co jest sprzeczne?         |
|--------------------------------------------|----------------------------------------------------------|
| godnie reprezentuj Świecę                  | akceptuj korupcję                                        |
| przygotuj się na niespodziewane            | wchodź do akcji bez przygotowania; działaj spontanicznie |
| zapewnij sobie bezpieczeństwo              | zostawiaj wydarzenia przypadkowi                         |
| dąż do władzy; dąż do wiedzy               | stań się kimś znanym                                     |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria     | Aspekty                           |
|---------------|-----------------------------------|
| Oficer        | pułapki; taktyka; przepisy        |
| Performer     | iluzje bez magii;                 |
| Terminus      | uczeń; zbieranie informacji       |
|  |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                      | Czym osiąga sukces?                                        |
|------------------------------------------------|------------------------------------------------------------|
| zachowanie pozorów; odwrócenie uwagi; dywersja | iluzje; pułapki;                                           |
| wyciąganie zeznań                              | autorytet terminusa; wiedza o danej osobie                 |
| kontrola pola walki                            | przygotowanie; wyposażenie; planowanie                     |
| planowanie starcia                             | wiedza o celu;                                             |
| zastraszanie                                   | znajomość przepisów |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria | Aspekty                        |
|-----------|--------------------------------|
| Paranoja  | nieufność; teorie spiskowe |

#### Manewry

| Co jest wzmocnione                | Kosztem czego                        |
|-----------------------------------|--------------------------------------|
| przygotowania; przewidywanie      | nawiązywanie przyjaźni; ufanie innym |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia zmysłów       | iluzoryczne klony; pułapki|
| Magia mentalna      |  |
| Magia transportu    | krótkodystansowe przesunięcia; ucieczka |

#### Manewry

| Jakie działania wykonuje?              | Czym osiąga sukces?                                          |
|----------------------------------------|--------------------------------------------------------------|
| odwrócenie uwagi                       | klony; iluzje; "those are not the droids you are looking for"|
| zacieranie śladów; zachowanie Maskarady| usuwanie pamięci; iluzje                                     |
| obrona                                 | tarcze magiczne; odwrócenie uwagi                            |
| hit-and-run                            | klony; przeskoki krótkodystansowe                            |
| wyciąganie zeznań                      | czytanie pamięci;                                            |
| pościg                                 | teleportacja                                                 |



### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Zaklęcie: Shatter clone            |  |
| Dare Shiver                        | iluzjonista - twórca klimatu; dostęp do artefaktów |
| wyposażenie SŚ                     | kompensacja własnych braków; |
|  |  |

#### Manewry

| Jakie działania wspierane?         | Czym osiąga sukces?                                                   |
|------------------------------------|-----------------------------------------------------------------------|
| maskowanie; odkrywanie             | artefakty Dare Shiver |
| tarcze magiczne                    | wyposażenie SŚ  |
| poszukiwanie informacji            | hipernet; biblioteka SŚ |
|  |  |
|  |  |

## Opis
23 lata
### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.|Wizja Dukata|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|dostęp do Yyizdatha i tematów Yyizdathokształtnych przez Disco-Yyizdathspawna|Wizja Dukata|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|opinia w Świecy: niestandardowe podejście Kingi uratowało Stefanię od niewoli kralotha.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|posiada mały odłamek Wieprzopomnika Półdarskiego, sprzężonego z głównym pomnikiem.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|tymczasowo Dotknięta przez Mrok Esuriit. Wrażliwa na tą formę energii.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Sylwester Bankierz dał Kindze pozytywną opinię jako niezależnej agentce Świecy|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180503|głównie zbiera informacje i przy użyciu artefaktów prowadzi śledztwo. Ku swej ogromnej żałości.|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|11/10/27|11/10/29|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180112|lekko paranoicznie podeszła do Stefanii i odkryła "tam" kralotha. Też, zwalczała Golema Esuriit. Próbowała spokojem i ciszą przeważyć efektownego Anatola.|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|wykorzystywała mnóstwo artefaktów Świecy i Dare Shiver by pozyskać oczkodzika. Główna tropicielka zespołu (smutne).|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|3|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html), [180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Yyizdath](/rpg/inwazja/opowiesci/karty-postaci/9999-yyizdath.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Sylwia Zasobna](/rpg/inwazja/opowiesci/karty-postaci/9999-sylwia-zasobna.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Stefan Bułka](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-bulka.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Karmena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-karmena-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Alfred Janowiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-janowiecki.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
