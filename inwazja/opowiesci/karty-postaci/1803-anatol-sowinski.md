---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "dzióbek"
title: "Anatol Sowiński"
---
# {{ page.title }}

## Koncept

Postać 1 + Postać 2 + Postać 3; 

Hero wannabe, zapatrzony w swoją wizję czym jest Ewa. Przywiązuje nadmiernie dużą wagę do tego by być dostrzeżonym.

## Postać 

### Motywacje (do czego dąży)

#### Kategorie i Aspekty

| Kategoria         | Aspekty                                       |
|-------------------|-----------------------------------------------|
| Indywidualne      | sława; samodoskonalenie;                      |
| Społeczne         | wszyscy sobie pomagają; każdemu według zasług |
| Wartości          | lojalność; siła; altruizm                     |


* **BÓL: nieistotny pyłek**:
    * _Aspekty_: chce być doceniony, chce być zauważony, boi się być ośmieszonym
    * _Opis_: 
* **FIL: hero of the people**: 
    * _Aspekty_: pomagać nawet swoim kosztem, znać ludzi ich potrzeby i problemy, inspirować innych
    * _Opis_: 
* **MET: efektowny do bólu**:
    * _Aspekty_: akcja powinna być spektakularna, im większa widownia tym lepiej
    * _Opis_:
* **MRZ: być jak Ewa Zajcew**:
    * _Aspekty_: wsławić się, zyskać prawdziwą potęgę, dokonać bohaterskich czynów, ja to załatwię
    * _Opis_: 

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| bądź podziwiany jak Ewa                                                     | nie pozwól żeby odbyło się to kosztem innych                                                      |
| doceniaj innych i nie pozwól żeby Cię pominięto                             |  |
| dąż do tego żeby wykonywana akcja była widowiskowa                          | wykonaj akcję tak by nikt się nie dowiedział                                                      |
| buduj i wzmacniaj lokalną społeczność                                       | nie pozwól żeby społeczność zamknęła się                                                          |
|  |  |
|  |  |
	
### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                                                                                                                                  |
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| dramaturgia       | wie jak zbudować napięcie; potrafi korzystnie się zaprezentować; zrobić efektowne wejście; potrafi wtopić się w tło; taunt               |
| szturmowiec       | pancerny; walka tarczą; desant; hit and run; pierwsza pomoc                                                                              |
| social worker     | niesie pomoc; umie rozmawiać; umie słuchać; ludzie mówią mu co im leży na sercu; uczy i inspiruje; umie pisać podania (o dofinansowanie) |

#### Manewry

| Jakie działania wykonuje?                                                                          | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| blokuje każdy atak; zaskakujący atak; attrition warfare; walka bez broni; frustrowanie przeciwnika | walka tarczą; dramaturgia                                                    |
| buduje obraz sytuacji; zdobywa informacje, plotki                                                  | social worker; udziela się; uczynny i pomocny                                |
|  |  |
    
### Silne i słabe strony:

* ****:
    * _Siły_:
    * _Słabości_:
    * _Opis_: 

## Magia

### Szkoły magiczne

#### Manewry

| Jakie działania wspierane?                          | Czym osiąga sukces?                                           |
|-----------------------------------------------------|---------------------------------------------------------------|
| leczy; wspomaga                                     | biomancja; przemiana ciała (biomancja + materia); ukojenie (biomancja + mentalna) |
| chroni                                              | przemiana ciała (biomancja + materia); odcina dostęp (przestrzeń + materia); fortyfikuje (materia); tarcze; odbija ataki |
| wtargnięcie; ewakuacja                              | teleportacja; dash; break through   |
| efekt wow; convenience; intimidate; taunt; distract | magia elementalna; magia materii;    |
| suit up                                             | magia przestrzeni + materia |
	
### Zaklęcia statyczne

* **Paragon of Defence**:
    * _Aspekty_: omnitarcza
    * _Opis_: zaklęcie personalne, tworzy tarczę o zwykłej sile, ale przekraczającą szkoły magiczne użytkownika

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* żołd Świecy
* local youth center
* hero for hire

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **efektowny pancerz magitechowy**:
    * _Aspekty_: widowiskowa, chroni jak porządna zbroja
    * _Opis_: komplet zbroja + puklerz, zawiera zaklęte elementalne efekty specjalne
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

22 lata

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"If it's worth doing, it's worth overdoing"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.|Wizja Dukata|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|tymczasowo Dotknięty przez Mrok Esuriit. Wrażliwy na tą formę energii.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|usłyszała o nim Ewa Zajcew z uwagi na Wieprzopomnik w Półdarze.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Stefania Kołek przekazuje szerzej wieści o efektownym wejściu Anatola. Anatol dostaje opinię lepszego niż jest.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180503|poczarował! Wygrzebał Kreta z mułu i odkrył Tajemnicę Gry Komputerowej. Aha, świetnie negocjuje z uszkodzonym Yyizdathspawnem.|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|11/10/27|11/10/29|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180112|mistrz efektownych akcji i widowiskowych wojen z... czymkolwiek (tym razem: Golem Esuriit). |[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|od efektownych wejść i walki z oczkodzikiem o transgeniczność Wojtka. Główny negocjator zespołu (smutne).|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|3|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html), [180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Yyizdath](/rpg/inwazja/opowiesci/karty-postaci/9999-yyizdath.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Sylwia Zasobna](/rpg/inwazja/opowiesci/karty-postaci/9999-sylwia-zasobna.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Stefan Bułka](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-bulka.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Karmena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-karmena-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Alfred Janowiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-janowiecki.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
