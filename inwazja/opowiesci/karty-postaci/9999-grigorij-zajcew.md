---
layout: inwazja-karta-postaci
categories: profile
title: "Grigorij Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160608|TODO|[Czy on wyszedł z Rifta...?](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|160526|TODO|[Bobrzańskie Gumifoczki](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zdzisława Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislawa-myszeczka.html)|2|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html), [160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Roman Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-weiner.html)|2|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html), [160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Luiza Łapińska](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-lapinska.html)|2|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html), [160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Arkadiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-bankierz.html)|2|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html), [160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[kot Attylla](/rpg/inwazja/opowiesci/karty-postaci/9999-kot-attylla.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|
|[Zofia Miczkewoł](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-miczkewol.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Stefan Głamot](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-glamot.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
