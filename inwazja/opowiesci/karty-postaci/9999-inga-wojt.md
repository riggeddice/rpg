---
layout: inwazja-karta-postaci
categories: profile
title: "Inga Wójt"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|131008|(19 l): córka Antoniego i Jolanty, wierząca do końca w dobro pochodzące od rodziców. KIA.|['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|(19 l) podejrzewana o bycie czarodziejką za co ukochani dziadkowie byli skłonni zniszczyć jej umysł. Interesuje się okultyzmem i homo superior od roku.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130506|(19 l) buntowniczy chuligan nie przejmujący się tym, że przed gośćmi paraduje nago i cieszący się z reakcji szokowych.|[Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|10/02/01|10/02/02|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130503|(19 l) wysportowana "niszczycielka obrazów" biegająca po domu jak szalona.|[Renowacja obrazu Andromedy](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|10/01/30|10/01/31|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|2|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Izabela Kruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kruczek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Antoni Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
