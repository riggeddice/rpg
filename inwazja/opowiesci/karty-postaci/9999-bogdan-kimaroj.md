---
layout: inwazja-karta-postaci
categories: profile
title: "Bogdan Kimaroj"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160210|ofiara losu (i Bat* maga), przepuszczony przez Hektora trafił do programu ochrony świadków.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160128|kiedyś alfons i gangster, teraz "na emeryturce" i właściciel "Wyżerki". Przeciwnik Skubnego. Zaczyna działania defensywne; ktoś chce go wrobić.|[Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
