---
layout: inwazja-karta-postaci
categories: profile
factions: "Żercy Apokalipsy"
type: "NPC"
title: "Terror Wąż"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Balans**:
    * _Aspekty_: wegetarianin
    * _Opis_: Wąż jest niestabilny, ale szybko wybacza. Śmierć za śmierć, ale "los tak chciał", więc pozwoli odejść, jeśli atak się nie udał. Terror Wąż zrobi wiele, by zdobyć dostęp do świeżych i smacznych roślin, przepisów kulinarnych itp.
* **FIL: Wieczna ewolucja**: 
    * _Aspekty_: 
    * _Opis_: Wąż stara się upiększyć, wzmocnić, usprawnić. Nie ma środków poza zakresem "akceptowalnym". No, poza Magią Krwi.
* **MET: Mistrz strachu**:
    * _Aspekty_: kolekcjoner, samotnik
    * _Opis_: Terror Wąż kolekcjonuje i konstruuje różne byty związane ze strachem; specjalizuje się w nich i zawsze szuka nowych. Wąż przywykł, że się go boją. Dąży do tego. Nie chce być zrozumiany i nie chce, by ktoś go "przywiązał". Czasem - tak. Na dłużej - nie.
* **MRZ: Prowadzić wegebar**:
    * _Aspekty_: 
    * _Opis_:  Tak. Terror Wąż. Istota mordercza i budząca grozę chciałaby po prostu... prowadzić wege-bar i mieć w spokoju klientów...
* **MET: Mistrz chaosu**:
    * _Aspekty_: 
    * _Opis_: Wąż najlepiej funkcjonuje w warunkach nieprzewidywalnych; im mniej wiadomo, tym lepiej Wąż sobie radzi. Adaptuje błyskawicznie.
* **FIL: **:
    * _Aspekty_: 
    * _Opis_: 
	
### Umiejętności

* **Kucharz**:
    * _Aspekty_: tylko wege
    * _Opis_: kucharz zapalony, uwielbia gotować i robi to bardzo dobrze. Ale tylko wegetariańsko.
* **Gladiator**: 
    * _Aspekty_: wzbudzanie grozy, zastraszanie
    * _Opis_: nazywa się "Terror Wąż" nie przez przypadek. Poza gotowaniem to jego drugie hobby.
* **Botanik**:
    * _Aspekty_: trucizny, narkotyki bojowe, narkotyki wspomagające, wspomaganie bojowe
    * _Opis_: jeden ze sposobów, w jakich Wąż sobie dorabia; przede wszystkim na składnikach roślinnych
* **Włamywacz**:
    * _Aspekty_: stealth
    * _Opis_: Wąż potrafi poruszać się w sposób niezauważony i dyskretny, śledząc i unikając. mało jest miejsc, do których Wąż się nie włamie. Forma dorabiania sobie. W poprzednim życiu: Don Juan ;-).
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Niestabilny wzór**:
    * _Aspekty_: wrażliwość na ataki magiczne
    * _Opis_: 
* **Vicinius**: 
    * _Aspekty_: organiczny pancerz i broń, odporność na trucizny, odporność na narkotyki
    * _Opis_: Wąż ma organiczny pancerz i broń; klasyfikowane jak lekki pancerz i mała broń, gdy nieuzbrojony. Wąż jest bardzo odporny na negatywne działania trucizn, toksyn i narkotyków bojowych
* ****:
    * _Aspekty_: 
    * _Opis_: 
* **Hipnotyczne Spojrzenie**:
    * _Aspekty_: 
    * _Opis_: Wąż ma bonus do akcji dominacyjnych patrząc prosto w oczy 


## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: alchemik, transformacja ciała - zwłaszcza swojego
    * _Opis_: 
* **Magia materii**: 
    * _Aspekty_: alchemik
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* mimo strasznego zużycia na Wzór, ma naprawdę solidne przychody ze sprzedaży na DarkRoad

### Znam

* **Sieć 'Marchewkowiec'**: 
    * _Aspekty_: 
    * _Opis_: sieć ludzi (i magów) skupionych dookoła dobrego wegetariańskiego jedzenia. Wąż płaci abonament i udziela się na forum, acz nie chodzi na imprezy.
* **DarkRoad**: 
    * _Aspekty_: 
    * _Opis_: Niezbyt legalny market (w internecie, nie hipernecie), gdzie Wąż sprzedaje swoje mikstury i kupuje co ciekawsze rzeczy. Zwłaszcza plotki.
* **Poprzedni zleceniodawcy**: 
    * _Aspekty_: 
    * _Opis_: Wąż wykonuje czasem brudną robotę dla różnych ludzi czy magów. Zawsze zapamiętuje, dla kogo tą robotę wykonuje.
* **Reputacja strasznych trucizn**: 
    * _Aspekty_: 
    * _Opis_: Wąż czasem wyśle jakiemuś lekarzowi (magowi) zaprojektowane przez siebie trucizny, by sprawdzić, jak sobie poradzą. Daje mu opinię...

### Mam
* **Dostawcy roślin**: 
    * _Aspekty_: 
    * _Opis_: Wąż dostaje warzywa... i inne rośliny od sprawdzonych grup oraz z DarkRoad. Można powiedzieć, że botanicznie Wąż się ustawił świetnie.
* **Narzędzia strachu**: 
    * _Aspekty_: 
    * _Opis_: Wąż uwielbia w czasie wolnym projektować nowe instrumenty grozy (eliksiry, opary...). Kolekcjonuje WSZYSTKIE.
* **Trucizny i narkotyki**: 
    * _Aspekty_: 
    * _Opis_: Wąż dorabia sobie sprzedając na DarkRoad różnego typu trucizny i narkotyki. Ma zatem do sporej ich liczby dostęp.
* **Dostawcy substratów**: 
    * _Aspekty_: 
    * _Opis_: Wąż jest elementem łańcucha dostaw; coś kupi, coś sprzeda. Nie wszystko do odpowiednich trucizn i narkotyków ma; te sprowadza.
* **Wege Biolab**: 
    * _Aspekty_: 
    * _Opis_: Wąż ma własne laboratorium biologiczne, w którym konstruuje nowe trucizny, narkotyki, rośliny i... obiady. Jest przenośne. W foodtrucku. Wege.
* **Broń: miecz, nóż...**: W
    * _Aspekty_: 
    * _Opis_: ąż ma standardowy zestaw broni, którego regularnie używa. Miecz, noże do rzucania... 
* **Sprzęt włamywacza**: 
    * _Aspekty_: 
    * _Opis_: Wąż jak ma gdzieś wleźć to wlezie. Zwłaszcza z wytrychami, kotwiczką i innymi takimi

# Opis

Terror Wąż był... była? Diakonem przed Zaćmieniem. Zaćmienie rozdzieliło Wąż z rodem i spowodowało, że Wąż wpadł w pewne szaleństwo. Odrzucił człowieczeństwo i ludzki wygląd, przekształcił się w viciniusa - ale dzięki temu zachował moc. Płeć dla Węża też nie stanowi różnicy; raz jest mężczyzną, raz kobietą. Jak wygodniej.
* Roamer 
* Nomad 
* Przestępca

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Dziś jestem kobietą. Marchewkę? Świeża, ze sprawdzonego źródła."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|dostał artefakt uzdatniający energię magiczną by ją lepiej wchłaniał (i smakowała lepiej), co redukuje jego koszty.|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170409|nadal udająca viciniusa i przygotowująca Paulinie przydatne eliksiry; dała się przekonać, by nie zabijać magów od razu|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170404|kucharka z powołania, psychopatka z zamiłowania, więcej straszy niż faktycznie krzywdzi. Pragmatyczna, bezwzględna, ale próbuje pomóc po swojemu.|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|10/02/10|10/02/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170331|eks-Diakonka, płci randomowej, kupuje marchewkę nie z przeceny, morderczyni-wegetarianka i ma lekkie (?) psychozy.|[Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|10/02/07|10/02/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Szczepan Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Onufry Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Katarzyna Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Ferrus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-ferrus-mucro.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
