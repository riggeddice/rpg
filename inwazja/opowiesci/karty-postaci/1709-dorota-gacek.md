---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Dorota Gacek"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Umiejętności

* **Uliczny wojownik**:
    * _Aspekty_: snajper, ucieczki
    * _Opis_: 
* **Mieszczuch**:
    * _Aspekty_: ucieczki, stealth
    * _Opis_: 
* **Dziecko złomowiska**:
    * _Aspekty_: 
    * _Opis_: 
* **Hacker**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: cyberwspomaganie, infomancja
    * _Opis_: 

* **Magia transportu**:
    * _Aspekty_: kineza
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

31 lat. Podopieczna Kornela Wadery i Izabeli Łaniewskiej. Protomag z niewielką ilością przyjaciół, hacker, cracker, żyła na ulicy i złomowisku używając mocy protomaga by przetrwać i sobie radzić w posiadanych okolicznościach. Znaleziona przez Świecę w wieku lat 23 - świetnie się ukrywała - sama zgłosiła się na terminusa jako "twarda, nie do zdarcia". Uważa, że nie można dać sobie w kaszę dmuchać i należy odpowiadać ogniem jak jest się zaatakowaną; ufa Izie Łaniewskiej i Kornelowi Waderze - oni ją przygarnęli do "swojego szczepu" gdy była sama i potrzebowała pomocy.
Pirotess.

## Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|dostaje znajomości w firmie transportowej TrustPort|Taniec Liści|
|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|terminusi kontrolni Świecy zainteresowali się jej działaniami i potencjalną zmianą w czarnego terminusa|Taniec Liści|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160915|która doprowadziła do włączenia firmy TrustPort do strefy wpływów Tańca Liści. Intrygantka - infomantka bijąca się w brudny sposób z Włodkiem o szacun.|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|10/05/26|10/05/29|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|160922|bliska przyjaciółka Izy która robiła wszystko co mogła by dowiedzieć się co się z Izą stało. Złamana wiadomością. Pociesza Kornela...|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|170407|fizyczna egzekutorka Kornela; przygotowała działko technomantyczne i zniszczyła Dominika Parszywiaka na oczach innych ludzi.|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141022|terminus*zabójca*mściciel chcący pomścić zniewagę wobec Dalii Weiner|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|3|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html), [160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Włodzimierz Tulewicz](/rpg/inwazja/opowiesci/karty-postaci/1709-wlodzimierz-tulewicz.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Marcin Szybisty](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Magda Szybisty](/rpg/inwazja/opowiesci/karty-postaci/9999-magda-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Konrad Matczak](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-matczak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Jolanta Iwan](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-iwan.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dominik Parszywiak](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-parszywiak.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Andrzej Marciniak](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-marciniak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
