---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "PC"
title: "Paweł Sępiak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: **:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: **: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: Szalony naukowiec**:
    * _Aspekty_: metodyczny, nieprzewidywalny
    * _Opis_: "jeszcze tylko 74 scenariusze do sprawdzenia i kopia będzie gotowa... you know what? I love pushing buttons"
* **MRZ: **:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: Chroń swoich**:
    * _Aspekty_: lojalny
    * _Opis_: "I will most definitely die if I do this, but they're my nakama"

### Umiejętności

* **Automatyka i robotyka**:
    * _Aspekty_: drony i autonomiczne jednostki zwiadowcze, swarms, AI, pułapki
    * _Opis_: 
* **Artefaktor**: 
    * _Aspekty_: artefakty eksperymentalne, broń energetyczna, wystrzone zmysły, szybki, analityczny
    * _Opis_: 
* **Golemanta**:
    * _Aspekty_: mimics
    * _Opis_: 
* **Biurokrata**:
    * _Aspekty_: kopie i rekonstrukcje, dobra pamięć, analityczny, kłamca
    * _Opis_: 
* **Taktyk**: 
    * _Aspekty_: pułapki, dywersja, stalker
    * _Opis_: 
* **Oficer**:
    * _Aspekty_: przesłuchania, kłamca
    * _Opis_: 

### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: iluzje, detekcja
    * _Opis_: 
* **Magia elementalna**: 
    * _Aspekty_: detekcja
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: detekcja
    * _Opis_: 
* **Technomancja**:
    * _Aspekty_: detekcja
    * _Opis_: 
	
### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* przynależne Quarki z Fazy Daemonica, co bardziej niebezpieczne eksperymenty Janka

### Znam

* **Janek**:
    * _Aspekty_: 
    * _Opis_: 
* **okoliczni złomiarze**:
    * _Aspekty_: 
    * _Opis_: 
* **śląskie uczelnie techniczne**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **VISOR**:
    * _Aspekty_: 
    * _Opis_: 
* **KADEM experiments**:
    * _Aspekty_: 
    * _Opis_: 
* **Scrapyard**:
    * _Aspekty_: 
    * _Opis_: 
* **Rzeczy z Instytutu Technomancji**:
    * _Aspekty_: eksperymentalne artefakty
    * _Opis_: 
	
# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170519|główny mag misji. Buduje simulacra, zwiad dronami, robi mimiki, komunikator do Aegis... ogólnie, motor technologiczny.|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|10/08/21|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170405|mistrzowsko używał dron w iluzjach i jako oczy; też: odwracał uwagę celatida swoimi golemami. Detektor problemów Andżeliki VISORem.|[Chyba wolelibyśmy kartony...](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|10/08/18|10/08/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|który próbował wszystko ogarnąć i by nie oszaleć po zebraniu danych scedował to na Norberta, samemu skupiając się na Hektorze.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150604|pomysłodawca zniszczenia Izy bez dowodów i bez cienia prawdy przy użyciu umiejętności Hektora Blakenbauera.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|osoba, montująca drobnymi socjalnymi czynnościami jeden front KADEMu.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|ekspert od twardych przesłuchań, który skutecznie włada paralizatorem|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|zadający właściwe pytania obserwator.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150224|osoba adaptująca VISORa i oszukująca echo kralotha.|[Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|10/04/17|10/04/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150110|ekspert od "narkotyków" Spustoszających mieszkańców Kotów.|[Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|10/04/15|10/04/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|mag KADEMu który bezbłędnie posługuje się działem strumieniowym.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|mistrz snajperstwa przy użyciu VISORa.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|przekonał Judytę o epickości Silurii fałszywymi opowieściami, pomysłodawca imprezy na KADEMie i mistrz przekonywania Ignata odnośnie odkręcania planów ;-).|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|twórca planu i dywersji mylącej Quasar; za plecami ochrony KADEMu przesłuchali EIS tak, że nie mogła niczego uzyskać|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150729|który zaproponował użycie kaczkoryzatora do bezstratniego odzyskania i uratowania Bogumiła Rojowca.|[Kaczuszka w servarze](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|10/01/19|10/01/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|14|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|6|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|6|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|5|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|5|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|4|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|3|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|3|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|3|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|2|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Korwin Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-korwin-morocz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|2|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[miślęg Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-misleg-pieluszka.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[echo Lugardhaira](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-lugardhaira.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Teresa Koliczer](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-koliczer.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Sławek Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-blyszczyk.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Szczepan Czarniek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-czarniek.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Stefan Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-jasionek.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Sandra Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-sandra-maus.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-pieluszka.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Maja Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-blyszczyk.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Lugardhair](/rpg/inwazja/opowiesci/karty-postaci/9999-lugardhair.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Jolanta Pirat](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-pirat.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Jerzy Buława](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-bulawa.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[GS Aegis 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Edward Sasanka](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-sasanka.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Bożena Górzec](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-gorzec.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
