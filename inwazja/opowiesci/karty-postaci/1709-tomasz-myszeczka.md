---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Tomasz Myszeczka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Nie ma Myszeczków a on jest nikim**
    * _Aspekty_: promuje swój ród, jest nieważny w swym rodzie, nie osiągnął sukcesu biznesowego i politycznego, jest pomiatanym posługaczem, inne rody wzrosną w siłę
    * _Opis_: Tomasz jest magiem silnie skupionym na swoim dobrobycie i na swoim rodzie. Chce, by ród Myszeczków wrócił do pełnej mocy i do chwały. Chce, by jego ród zyskiwał i stawał się jak największy. Po drodze, nie przeszkadza mu upokorzenie Mausów czy Sowińskich. Jego zdaniem rodzina się bardzo liczy - jest podstawą tego, na czym można polegać i co ma znaczenie. Teren nie ma dla niego znaczenia, ale sojusze biznesowe - tak.
* **ŚR: Zachłanny król viciniusów użytkowych**:
    * _Aspekty_: pozyskiwanie agentów, dobrobyt wśród klientów, lock-in do swoich viciniusów, osłabiać konkurencję, "ten teren jest niczyj więc mój", "dump the waste", finanse nad etykę
    * _Opis_: Tomasz jest ciężko pracującym właścicielem ogromnej hodowli viciniusów użytkowych. Specjalizuje się w świniach, świniowatych i stworzeniach naziemnych. Skorzystał z okazji, że po Spustoszeniu ten teren jest stosunkowo nieopanowany i zagarnął ogromny obszar, z którym czasem ma problem się wyrobić. Zawsze brakuje mu rąk do pracy. Jego podstawowym środkiem jest używanie viciniusów, by skalować model biznesowy i zdobywać coraz większą kontrolę nad wszystkim.
* **FIL: Smok strzegący skarbu**: 
    * _Aspekty_: "czy to się finansowo opłaca?", zagarnie a nie odda, zysk nad ludzi, powiększanie bogactwa, powiększanie terenu, nie oddawanie niczego, zawsze WIĘCEJ, wprowadzić lock-in, zabierz więcej niż umiesz utrzymać,
    * _Opis_: Gdy Tomasz już coś dostanie w swoje ręce, niechętnie z tego zrezygnuje czy to odda. Bogactwo i majątek są więcej warte niż dobrobyt jego pracowników a wszystko musi zawsze mieć uzasadnienie ekonomiczne. Wszystko się musi opłacać, trzeba jak najwięcej gromadzić i jak najwięcej uzyskać. Znaczenie, potęga i wpływ na świat są pochodną tego ile uda się zdobyć, utrzymać i posiadać.

### Umiejętności

* **Hodowca**:
    * _Aspekty_: viciniusy użytkowe, zarządzanie hodowlą, szeroka wiedza o viciniusach, znajdowanie rynków zbytu, maskowanie problemów, budowanie zagród, slave driver, viciniusy świniowate
    * _Opis_: Wielki hodowca viciniusów użytkowych o ogromnej wiedzy i pracowitości. Świetnie potrafi odwracać uwagę od problemów by kupić sobie czas oraz zmuszać ludzi do pracę ponad siły.
* **Biznesmen**: 
    * _Aspekty_: biznesmen w warunkach niebezpiecznych, twarde negocjacje, nieetyczne negocjacje, liczenie finansów, estymacja wyników, "to nie moja wina", ocena ludzi, bezwzględna natura, świetnie działa pod presją, wytrzymały
    * _Opis_: Biznesmen starego typu, zdolny do negocjowania kilkanaście godzin by wykończyć przeciwnika i który traktuje negocjacje i kontakty biznesowe jak przedłużenie wojny. 
    
### Silne i słabe strony:

* **Właściciel ogromnego terenu**:
    * _Aspekty_: ledwo go utrzymuje, bardzo dużo środków, liczący się gracz politycznie, nie jest w stanie nad wszystkim panować
    * _Opis_: Wziął duży teren. Za duży. Daje mu mnóstwo środków i pieniędzy... jednocześnie sprawiając, że sam niekoniecznie jest w stanie wszystko utrzymać i nad tym zapanować.
* **Członek rodu Myszeczków**: 
    * _Aspekty_: wiedza o rzadkich viciniusach, waśń z Mausami i Sowińskimi, viciniuscy agenci go słuchają
    * _Opis_: Jako mag Myszeczków, wie więcej o nich i ich działaniu niż zwykle - jest bardzo efektywnym hodowcą. Trafił w idealną dla siebie niszę. Jednocześnie jednak ma też stare waśnie z Mausami i Sowińskimi - i się ich nie wypiera.
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Biomancja**: 
    * _Aspekty_: tworzenie i adaptacja agentów, tworzenie karmy, utrzymywanie viciniusów egzotycznych, kontrola biologiczna, hodowla zwierząt i roślin
    * _Opis_: Szkoła magii czysto użytkowa; Tomasz ma tendencje do tego, by tworzyć i dostosowywać agentów. Rzadko spotyka się go robiącego "cokolwiek"; zwykle zajmuje się swoją hodowlą czy buduje wyspecjalizowanych agentów mających dla niego zrobić coś przydatnego i konstruktywnego. Całość jego biomancji skupia się na czynnościach hodowlanych.
* **Magia mentalna**:
    * _Aspekty_: tworzenie osobowości, dominacja, ulojalnianie, bariery mentalne i geasy, szybkie szkolenie
    * _Opis_: Tak jak biomancja, mentalna jest szkołą użytkową. Agenta należy przetrenować, zbudować mu osobowość i wysłać do akcji. Vicinius użytkowy winien mieć odpowiednie geasy i czekać. A założenie geasa czy dwóch na ludzi robiących głupoty także nie jest złą opcją...


### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

 

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis



### Koncept

Trochę Torik z Jeźdźców Smoków z Pern, Tomasz jest ciężko pracującym hardholderem próbującym wykroić sobie jak największą farmę, jak największe ziemie. Jest momentami chytry, nie zawsze na swoją i cudzą korzyść - ale ogólnie jest sprawiedliwy. Rolnik, hodowca bezpiecznych viciniusów użytkowych.

### Motto

"Ciężką pracą i właściwymi dealami można przekuć przeszłość w sukces."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|pojawia się potencjalnie bardzo wartościowy klient potrzebujący pełni uwagi Myszeczki|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|odzyskał kontrolę nad swoimi świniami i ogólnie odbudował swoje zasoby.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|Robert Sądeczny wsadził mu rezydenta politycznego - kogoś, kto ma pilnować, by nie działy się nadużycia z glukszwajnami.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|zwalcza Łucję Maus, bo ona jest niebezpieczną, szaloną psychopatką i rozpędziła mu świnie w momencie kryzysu|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|celuje w oszczędności i poszerzenie swoich wpływów jak najmniejszym kosztem finansowym.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|zdecydował się wzmocnić Srebrną Świecę nad Rodzinę Dukata - Sądeczny wsadził mu rezydenta politycznego :-(.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|chciał zemścić się na Łucji i Mausach. Po działaniach Daniela i Pauliny - musiał wyzerować im konto. Wielki przegrany tej sprawy.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|opanował glukszwajny i wszelkie inne świnie na tym terenie z pomocą magitrowni Histogram. Sprawa powoli wraca mu pod kontrolę.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|zainfekowany klątwożytem i płakał i był nieszczęśliwy i stracił trochę twarzy i bogactwa i glukszwajnów. Nie jego dzień.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171101|złapał klątwożyty, jest deliryczny i ściągnął Radka by się nim zajął. Sęk w tym że Radek jest praktykantem.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|chciał zapobiec utracie grupy viciniusów, poprosił Daniela o inną energię do portaliska i poważnie uszkodził portalisko które wciągnęło go w eter. Uratowany przez Sądecznego.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|wyraźnie martwiła go efemeryda w magitrowni więc rozmontował ją nipustakami z pomocą Pauliny. Bardzo się mu ruchy Pauliny spodobały.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|który dostał nową działkę w lesie niedaleko magitrowni Histogram z obowiązkiem zrobienia tam pastwiska glukszwajnów. Twardy negocjator.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171003|który nieco za bardzo chciał oszczędzić i stracił najpierw glukszwajna a potem musiał zapłacić ogromne odszkodowanie za uszkodzenia w portalisku.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|7|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|5|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|4|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|4|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|3|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Elwira Czlikan](/rpg/inwazja/opowiesci/karty-postaci/9999-elwira-czlikan.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
