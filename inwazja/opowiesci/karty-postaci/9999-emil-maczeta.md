---
layout: inwazja-karta-postaci
categories: profile
title: "Emil Maczeta"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150422|szef agencji ochroniarskiej "Maczeta", który dał Alinie próbkę materiału genetycznego za niezbyt wielką sumę (nieświadomy tej sytuacji).|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dariusz Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-larent.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
