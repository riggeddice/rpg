---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium, Souris"
type: "PC"
owner: "raynor"
title: "Hanna Souris"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)
* **MRZ: Prawdziwy wpływ**:
    * _Aspekty_: zyskiwanie reputacji, osiągnięcia, osiągniecie pozycji w świecie magów
    * _Opis_:  "chcę mieć faktyczny wpływ na najpotężniejszą organizację magów. Tzn. Albo sprawię, że moja organizacja będzie najpotężniejsza i będę miał na nią wpływ, albo dołączę do innej, na którą również będę miał wpływ" 
* **FIL: Wieksza ewolucja**:
    * _Aspekty_: zdobywanie nowych umiejętności, poznawanie potężnych ludzi, zwiększanie siły magicznej
    * _Opis_:  ciągle lepszy, potężniejszy.
* **BÓL: Mściwy**: 
    * _Aspekty_: podejmowanie niebezpiecznych decyzji, utrata kontroli nad emocjami, konfrontacja z Kinglordem, świeca musi zostać zniszczona, 
    * _Opis_: prędzej czy później każdy odpowie za krzywdę na Henryku Siwieckim 

### Umiejętności

* **Dyplomata**:
    * _Aspekty_: osiąganie kompromisu, przekazywanie wieści, zachowanie konwenansów
    * _Opis_: 
* **Ksiądz**:
    * _Aspekty_: inspirowanie o podłożu duchowym, 
    * _Opis_: 
* **Manipulator**:
    * _Aspekty_: budowanie wrażenia, wykorzystywanie autorytetu, wywoływanie emocji, wywoływanie zachowań
    * _Opis_: 
* **Egzorcysta**:
    * _Aspekty_: piętnowanie, walka z magicznymi siłami
    * _Opis_: 
* **Negocjator**:
    * _Aspekty_: zdobywanie korzyści, rozwiązywanie konfliktów, 
    * _Opis_: 
* **Psycholog**:
    * _Aspekty_: wykrywanie emocji, wykrywanie pobudek, budowanie relacji
    * _Opis_: 
* **Polityk**:
    * _Aspekty_: autoprezentacja, ukrywanie wad, zdobywanie koalicjantów
    * _Opis_: 
    
### Silne i słabe strony:

* **Egoista**:
    * _Aspekty_: walka o swoje cele, poświęcanie innych dla siebie,
    * _Opis_: 
* **Krucha dziewczynka**: 
    * _Aspekty_: słaby fizyczna, łatwy do zastraszenia, kokietowanie, podporządkowywanie, ostrożna: sytuacje społeczne, potencjalne zagrożenie życia
    * _Opis_: 
* **Vicinius**:
    * _Aspekty_: forma pajęczaka zagłady: bieganie po ścianach i sufitach, zastraszenie, 
    * _Opis_: 
* **Słaba pozycja**:
    * _Aspekty_: zależność od innych magów, mała rozpoznawalność
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: astralika, kontrola, dominacja, badanie echa emocji, postrzeganie akcji przez innych, 
    * _Opis_:
* **Kataliza**:
    * _Aspekty_: badanie magii, manipulacja pryzmatem
    * _Opis_: 

### Zaklęcia statyczne

* **Echo emocji**:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **czlonek sympatyk szlachty:
    * _Aspekty_: 
    * _Opis_:
* **sława skutecznego egzorcysty:
    * _Aspekty_: 
    * _Opis_:
* **opinia tego co przynosi podwójne profity:
    * _Aspekty_: 
    * _Opis_:
* **hierarchowie kosciola:
    * _Aspekty_: 
    * _Opis_:
* **dziennikarze telewizji Nowina:
    * _Aspekty_: 
    * _Opis_:

### Mam

* **zaufanie spoleczne (ksiadz):
    * _Aspekty_: 
    * _Opis_:
* **oddział kleryków/paladynow wspomagajacych egzorcyzmy:
    * _Aspekty_: 
    * _Opis_:
* **Kaldwor i jego organizacja:
    * _Aspekty_: 
    * _Opis_:
* **sprzet egzorcystyczny:
    * _Aspekty_: 
    * _Opis_:
* **Oczy kosciola:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Mag egzorcysta, skupiający się na poszerzaniu wpływów i awansie w magicznej drabinie społecznej.


### Motto

"Audi multa, dic pauca"

# Historia:
