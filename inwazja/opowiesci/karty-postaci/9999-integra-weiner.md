---
layout: inwazja-karta-postaci
categories: profile
title: "Integra Weiner"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|aktywnie wspiera obecność Daniela Akwitańskiego na Primusie.|Wizja Dukata|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|nie będzie polować na ludzi. Nie chce celować w ludzi. Uwolniła wszystkich ludzi w multifazie Histogram.|Wizja Dukata|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|posiada możliwość manifestacji osobiście w Magitrowni Histogram na Primusie (przez zbliżenie faz).|Wizja Dukata|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|w ciągłym kontakcie z Karolem Marzycielem. Kochankowie przez telefon.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|zapewnić prawidłowe działanie magitrowni Histogram niezależnie od pryzmatu czy czegokolwiek. Oraz trochę poMarzycielować.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171220|regentka z ramienia efemerydy senesgradzkiej. Buduje wylęgarnię świń i bardzo stara się utrzymać magitrownię przy prawidłowym działaniu. Nadal myśli o Karolu.|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|11/10/05|11/10/06|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
