---
layout: inwazja-karta-postaci
categories: profile
title: "Łukasz Tworzyw"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170530|arcykapłan satanistów; jeden z wysokich rangą sojuszników Luksji a kiedyś świecki człowiek w kościele. Złożył potężny kult satanistyczny.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
