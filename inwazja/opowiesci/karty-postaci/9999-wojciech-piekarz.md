---
layout: inwazja-karta-postaci
categories: profile
title: "Wojciech Piekarz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|uzyskał umiejętność Rozkazywania świniom. Atak mentalny.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|uzyskuje 'świnie affinity'. Świnie do niego ciągną, on dobrze z nimi żyje, one go lubią, jemu łatwiej je zrozumieć.|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180104|pokonał swój strach i zaprzyjaźnił się z oczkodzikiem. Pomógł magom go znaleźć, ku swej wielkiej rozpaczy.|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171229|harcerz, 16 lat; był na warcie gdy Krzysztof szukał Krzysia i podglądał, gdy Krzysztofa przesunęło do Esuriit. August pomógł mu z poczuciem winy.|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|2|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html), [171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
