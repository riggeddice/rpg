---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Daria Rudas"
---
# {{ page.title }}

## Koncept

Zraniony korposzczur Świecy, który poszukuje przyjaźni.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | reputacja; środki; niezależność; kolekcjoner  |
| Społeczne         | |
| Wartości          | własne bezpieczeństwo |

#### Szczególnie

| Co chce by się działo?                              | Co na pewno ma się NIE dziać? Co jest sprzeczne?          |
|-----------------------------------------------------|-----------------------------------------------------------|
| pozbądź się reputacji "tej kosztownej niszczycielki sprzętu"                | zdobądź sławę w jakikolwiek sposób |
| zdobądź pieniądze i środki do prowadzenia eksperymentów i zachowania niezależności | zdobądź środki nawet kosztem zależności od kogoś |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| artefaktor |  |
| alchemik |  |
| mysz laboratoryjna | badania; zdobywanie informacji |
| korpo szczur | procedury; |

#### Manewry

| Jakie działania wykonuje?                           | Czym osiąga sukces?                                      |
|-----------------------------------------------------|----------------------------------------------------------|
| zdobądź wiedzę o osobie / wydarzeniu / przedmiocie  | hipernet; zasoby Świecy                                  |
| nawigacja w Świecy                                  | znajomość procedur; wykorzystanie surowców Świecy        |
| konstrukcja artefaktów; kontrola artefaktów |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Kataliza            |  |
| Magia elementalna   |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| badanie magii | kataliza; sprzęt analityczny |
| konstrukcja artefaktów; kontrola artefaktów |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?                          | Czym osiąga sukces?                                        |
|-----------------------------------------------------|------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

Młoda czarodziejka, artefaktorka. Nigdy nie miała zapędów do wielkiej polityki czy bycia sławną. Ku jej utrapieniu została sławna - gdy jej eliksir podpiął się do leyline Kompleksu Centralnego i wysadził skutecznie pół laboratorium, Skażając Skrzydło Gościnne i penetrując tarcze. Jak to się stało - nikt nie ma pomysłu. Nikomu nie udało się tego nigdy powtórzyć. 

Sprawia to, że Daria cieszy się wątpliwą sławą osoby, która doprowadziła do ogromnych kosztów Świecę i Daria NIC z tego nie ma. Po prostu chciała uczciwie produkować artefakty i eliksiry - nieszczęśliwie, nie wyszło.

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|poszerzenie opinii niszczycielki. To ona pomogła w złapaniu larghartysa. Jej niszczycielstwo jest epickie i bardzo przydatne|Adaptacja kralotyczna|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|kontroluje mimika symbiotycznego, którego może wykorzystać w dowolny sposób.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180411|dzięki znajomości biurokracji Świecy znalazła tą jedną terminuskę, która potraktowała ją poważnie - i znaleźli klucz do Drugiego Kralotha.|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180421|miała tylko spojrzeć na Operiatrix, a naprawiła ekran Alfreda i złapała mimika dla siebie. Pragmatyczny szczur.|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|10/11/24|10/11/26|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Artur Wiążczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-wiazczak.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Alegretta Tractus](/rpg/inwazja/opowiesci/karty-postaci/9999-alegretta-tractus.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
