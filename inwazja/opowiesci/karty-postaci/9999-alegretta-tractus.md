---
layout: inwazja-karta-postaci
categories: profile
title: "Alegretta Tractus"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|ZAWSZE wie, gdzie iść by dostać się do Maurycego Mausa|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180411|służyła jako przynęta na larghartysa, z powodzeniem. Wróciła do pełnosprawnego działania.|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170517|dziewczyna-pozytywka. "Opętany" golem przez "Marysię" - czarodziejkę, która zginęła tragicznie. Dzieło życia Maurycego Mausa, który najpewniej nie działał sam.|[Zegarmistrz i Alegretta](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|10/08/17|10/08/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|2|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html), [170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
