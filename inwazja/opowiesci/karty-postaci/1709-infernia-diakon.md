---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Infernia Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **BÓL: Prosta, niekompetentna Zajcewka - tylko walczyć umie**:
    * _Aspekty_: promuje siłę i niezależność, boi się bycia słabą, boi się że nie jest dość inteligentna i tylko jest silna, chce być akceptowana za dzieła NIE seks czy walkę, boi się etykietki tchórza
    * _Opis_: Infernia jest splątana między światami Diakonów i Zajcewów. Po obu rodach uzyskała impulsywność. Boi się, że trzymają się na KADEMie tylko dlatego, że jest "silna" i "fajna w łóżku" - ale nie dlatego, że jest dość dobra i inteligentna. Pragnie z jednej strony być bardzo silną a z drugiej być cenioną i kochaną za jej dzieła i intelekt (perfumy, jedzenie, rośliny), nie za swoje doskonałe ciało.
* **FIL: Niewidoczni głosu nie mają - obroń swoje prawo**:
    * _Aspekty_: głośna skandalistka NIE cicha i grzeczna, w poszukiwaniu przyjemności NIE czekając co przyjdzie, rywalizacja z każdym NIE kalkulowanie z kim może wygrać, wprowadzać chaos NIE dążyć do celu, kolekcjonerka ciuchów, intelekt nad siłę ognia, moralność to wyszarpana władza i zasłużony szacunek
    * _Opis_: Diakon - ród znany z efektownej ekspresji. Zajcew - ród znany z niezależności i siły. Infernia, pochodna obu światów. Pragnie być szanowana i potężna - i chce sobie to wywalczyć przez bycie głośną, skandaliczną i walczącą z całym światem siłą. Nie wierzy w cierpliwą pracę i bycie konsekwentną. Wierzy w wyzwania i szybkie, ciągłe podnoszenie sobie poprzeczki - oraz pokonywanie wszystkich przeszkód na swojej drodze.
* **ŚR: Wamp, wandal i agresor**:
    * _Aspekty_: erotyczny drapieżnik, impulsywna i bezpośrednia, dominuje niszczy lub omija, promuje eskalację NIE wycofanie się, nie cierpi przegrywać
    * _Opis_: Infernia jest inkarnacją najbardziej żywiołowych cech Diakonki i Zajcewki - jest największą alfą spośród alf. Drapieżna erotycznie, niszczycielska (acz niebojowa) i skłonna do rywalizacji ze wszystkim - tej czarodziejce rzadko się wchodzi w drogę.

### Umiejętności

* **Sukkub KADEMu**:
    * _Aspekty_: zwarcie bezpośrednie (szamotanina), ars amandi, uwodzicielka, "czołg przełamania", dominacja i usadzanie
    * _Opis_: Czasem aż niektórzy się zastanawiają czy Infernia pochodzi z Academii Daemonica. Infernia preferuje działania bezpośrednie - powalczyć, poszamotać się, pochędożyć. Drobniutka blondyneczka, uwodzicielka i jednocześnie niepowstrzymany juggernaut - Infernia przebija się przez świat zostawiając jedynie swoje ofiary za plecami.
* **Perfumiarka w kuchni**:
    * _Aspekty_: wegetariańskie dania niebezpieczne, przyprawianie, tworzenie perfum, manipulowanie zapachem, tropienie węchem
    * _Opis_: Infernia, czarodziejka o wybitnym węchu. Świetnie gotuje, zwłaszcza rośliny niebezpieczne i doskonale przyprawia - i z radością dzieli się swoimi dziełami i kompozycjami (zwłaszcza, jeśli są choć odrobinę groźne). Jednocześnie Infernia specjalizuje się w nęceniu i kuszeniu swoich celów przy użyciu... perfum. Delikatne i subtelne zapachy silnie dostosowane do konkretnych odbiorców są czymś, czego po żywiołowej Inferni większość magów się nie spodziewa.
* **Kustosz morderczych roślin**:
    * _Aspekty_: hodowla, znajomość i wykorzystanie roślin niebezpiecznych, magiczne ogrody, znajomość i wykorzystanie roślin aromatycznych, wszystko o ogrodach KADEMu, zabijanie roślin, dania wegetariańskie, badanie i utrzymanie nieznanych roślin, zabezpieczanie roślin
    * _Opis_: Infernia jest główną ogrodniczką ogrodów Weroniki Seton i zna je praktycznie na wylot. Zabijanie roślin zna na wylot, tak samo jak ich hodowlę i dostosowywanie do celów. Rośliny różnego rodzaju są jej główną kompetencją... i często jednym z jej głównych wyzwań.
    
### Silne i słabe strony:

* **Nieprzesuwalna, samoniszcząca węszycielka**:
    * _Aspekty_: doskonały węch, węch kierunkowy, bardzo potężny Wzór, bardzo spójna wola, duża stamina, kochliwa, łatwa do sprowokowania, potrzebuje akceptacji LUB pętla zniszczenia
    * _Opis_: Infernia ma doskonały węch i jest w stanie wykorzystywać go dużo szerzej niż przeciętny czarodziej - ona "myśli" węchem. Dodatkowo kombinacja obu rodów stworzyła bardzo silny Wzór - Infernia jest bardzo trudna do Skażenia, wolniej się męczy i trudno przełamać jej wolę czy zmienić jej zdanie sposobami nieperswazyjnymi. Za to jest kochliwa, bardzo łatwo ją sprowokować i musi czuć, że komuś na niej zależy. Całkowicie sama by nie przetrwała przez tendencje samoniszczące - musi mieć KOGOŚ kto ją wspiera.

## Magia

### Szkoły magiczne

* **Magia elementalna**:
    * _Aspekty_: zabezpieczenie obszaru, destruktywna puryfikacja, zmiana mikroklimatu (rośliny)
    * _Opis_: Dominująca szkoła magiczna Inferni po stronie rodu Zajcew. Infernia specjalizuje się w żywiołowych i niszczycielskich emisjach magii - a w szczególności w wypalaniu i wyniszczaniu. Bardzo przydatne, bo dzięki temu uzyskała przydomek "destruktywna puryfikatorka". Secure, Contain, Destroy. Aha, dodatkowo - mikroklimat dla magicznych roślin pomaga.
* **Biomancja**:
    * _Aspekty_: wszystko z roślinami, wszystko z zapachami, ekstaza, sterowanie roślinami
    * _Opis_: Dominująca szkoła magiczna Inferni po stronie rodu Diakon. Przejęła talent do roślin magicznych, ekstazy i perfum różnego rodzaju po tym rodzie.
* **Magia zmysłów**:
    * _Aspekty_: wspomaganie i zmiana smaku, wspomaganie, zmiana i lokalizowanie zapachów, szeroka magia erotyczna, monitorowanie miejsca
    * _Opis_: Infernia wykorzystuje magię Zmysłów w ograniczonym stopniu - interesuje się przede wszystkim wspieraniem swoich talentów kulinarnych, swego naturalnego węchu oraz szeroko rozumianą magią erotyczną. Dodatkowo stawia "pluskwy" czy "monitory", dzięki którym może obserwować konkretne miejsca.
* **Kataliza**:
    * _Aspekty_: puryfikacja, wspieranie hodowli roślin magicznych
    * _Opis_: Ścieżka w bardzo dużej atrofii; tak naprawdę Infernia poza puryfikacją i utrzymywaniem roślin magicznych nie jest w stanie jej sensownie używać.

### Zaklęcia statyczne

* **Ostateczna Puryfikacja Katriny**:
    * _Aspekty_: kataliza+elementalna+biomancja, samozasilające, puryfikacja, destruktywna transformacja energii
    * _Opis_: Zaklęcie to jest w swojej naturze bardzo proste - próbuje spuryfikować dany byt (czy to fizyczny czy biologiczny) czerpiąc energię z celu i przekształcając energię niemożliwą do puryfikacji w inne formy. Cel zostanie spuryfikowany lub zostanie zniszczony - ale przestanie być problemem.
* **Ognisty głaz na śledzie**:
    * _Aspekty_: elementalna, szybki czar, płonący pocisk (głaz), efektowny, wypala obszar
    * _Opis_: Klasyczny czar Zajcewów - szybko rzucane zaklęcie przywołujące sporej wielkości kamień rozrzażony i płonący, który następnie rzuca się w cel. Wypala obszar. Szybkie i przyjemne zaklęcie niskoobszarowe.
* **Perfekcyjne roślinarium**:
    * _Aspekty_: elementalna+biomancja+kataliza+zmysły, zapewnia adaptujący mikroklimat, zabezpieczone przed kontaminacją, wymaga Surowca
    * _Opis_: Zaklęcie rzucane albo pod kątem konkretnego mikroklimatu albo pod kątem istniejącej rośliny. Buduje idealny i adaptujący się mikroklimat mający monitoring, oddzielenie rośliny od świata zewnętrznego, zapewniający glebę i energię magiczną. NIE zapewnia elementów wymagających magii mentalnej. Rzucenie tego czaru zawsze wymaga Surowca.

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Magiczne rośliny i pomaganie innym z roślinami
* Rozpaczliwa puryfikatorka na beznadziejne okazje
* Skandalistka na wynajęcie, escort girl dla przyjemności
* Niektórzy jej płacą, by gdzieś na pewno NIE przyszła

### Znam

* **Znana, groźna skandalistka**:
    * _Aspekty_: 
    * _Opis_: Powszechnie znana jest jako KADEMka, która zrujnuje Twoją reputację... bo swojej już nie ma. Ma też reputację osoby, której po prostu nie warto wchodzić w drogę, bo to zbyt kosztowne. Nie chcieli jej w Dare Shiver, bo jest ZBYT nieustraszona i miała bardzo zły wpływ na innych magów.
* **Szemrani dostawcy przyjemności i roślin**:
    * _Aspekty_: 
    * _Opis_: Nie ma tak niebezpiecznych roślin, eksperymentalnych środków czy nieakceptowalnych form przyjemności by Infernia nie chciała chociaż raz "tego" sprawdzić. Krąg osób od których Infernia coś kiedyś pozyskała jest tak szeroki, że nawet Marian Łajdak by się dwa razy zastanowił.

### Mam

* **Ogrody Weroniki Seton**:
    * _Aspekty_: 
    * _Opis_: Żywy ekosystem badań, rozwoju oraz utrzymania (SCP) roślin pomiędzy Primusem a Fazą Daemonica. Zawiera nasiona, rośliny, nawozy i gleby w różnych stadiach i formach. Zawiera kategorie roślin - od zielonych (niegroźnych) po czerwone (skrajnie groźne). Jest to królestwo Inferni - jej dom i ukochany, tajemniczy ogród.
* **Stroje, suknie i trofea**:
    * _Aspekty_: 
    * _Opis_: Dama potrafi się ubrać na każdą okoliczność. Infernia też ;-). Co więcej, poza świetnych strojów i ubrań na każdą okazję, Infernia ma też kolekcję cudzych ubrań - od bielizny po mundury a czasem nawet trafi jej się jakiś magitech. Trofea, wspomnienia z przeszłości, czy sentymentalne pamiętki z którymi wiążą się ciekawe wspomnienia.

# Opis

Silna, dobrze zbudowana i lekko nadpalona blondynka o jaskrawozielonych włosach, uczesana na jeża. 33 lata. Troszkę wygląda jak Hefajstos w spódnicy; zdecydowanie nie przystaje do obrazka "typowej ślicznej laleczki", acz jest ładna w sposób Diakonów. To Infernia Diakon - impulsywny czołg, zajmująca się w ramach hobby ogrodami Weroniki Seton...

### Koncept

Immovable Force, Walkiria z cyklu Resnicka. Miłośniczka roślin o doskonałym węchu. Źle reaguje na samotność i ma tendencje do eskalowania ponad swoje (duże) siły. Wypowie wojnę światu... i może, co gorsza, wygrać.

### Motto

"Bitwa, seks a potem MÓJ obiad - ogrodniczka KADEMu do usług."

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170125|wypowiedziała Hektorowi jawnie wojnę. Wściekła jak osa. Skrzywdziła Ignata, bo to jej walka z Hektorem.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170104|pełniąca szlachetną funkcję kucharki, nadal nie lubi Hektora i złośliwie ironizuje. Jednak przezwyciężyła niechęć... choć zastawiła na Hektora pułapkę czy dwie (rozbrojone przez Silurię)|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|która jest bardzo niezadowolona z działań Hektora; nie tylko ściągnęła Jolę (dziennikarkę) ale i otwarcie zagroziła Hektorowi.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|krew Zajcewa i Diakona, którą rzucił Marcelin za co wciągnęła go w perfidną pułapkę. Marzy o eleganckim mundurze.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150411|"coś między Diakonami a Zajcewami", eks-kochanka Marcelina która go rzuciła... na krzesło. I gracze jej unikają jak ognia.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170417|ratowała w Żółtym Ogrodzie Weroniki Seton syrenopnącze, przez co miała najlepsze chwile swojego życia... do odwyku.|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|10/03/05|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|zatrzymała Ignata przed śledzeniem Silurii; groźniejsza w walce bezpośredniej od Ignata, acz nie bardzo bojowa.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|która ukryła działania Karoliny Kupiec przed Blakenbauerami i została frontgirl. Upiła się w Rezydencji, zmusiła Hektora do pocałunku i skończyła z Marcelinem. Achievement ;-).|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|zainteresowana KTO porwał Silurię, Siluriowa informatorka w sprawie Ignata i pomoc Karoliny by przebadać narkotyk Uli.|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|która nadal wraz z Silurią próbują sprawdzić która będzie 'u góry'.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161217|'ogrodniczka' KADEMu zajmująca się Ogrodami Weroniki Seton. Też: ekspert od biomanipulacji nastrojów zapachami. Nadal kochanka Silurii. Lubi deprymować nie-Silurię. W poszukiwaniu hedonizmu.|[Niezbyt legalna 'Academia' Whisperwind](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|10/01/30|10/01/31|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|sprzężona z rodów Diakon i Zajcew, 'destruktywna puryfikatorka' Instytutu Zniszczenia i (tymczasowa) kochanka Silurii. |[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|10|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|4|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|2|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|2|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Sławek Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-blyszczyk.html)|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Beata Zakrojec](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-zakrojec.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Akumulator Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-akumulator-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
