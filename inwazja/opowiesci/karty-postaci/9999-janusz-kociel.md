---
layout: inwazja-karta-postaci
categories: profile
title: "Janusz Kocieł"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170326|lekarz magiczny na usługach Dukata, który ucieszył się mając wsparcie Pauliny w Odkażaniu ludzi.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
