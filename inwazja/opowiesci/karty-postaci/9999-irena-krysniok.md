---
layout: inwazja-karta-postaci
categories: profile
title: "Irena Krysniok"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141102|wiecznie NIE doceniane przez graczy i NPC źródło wszelkiego donosicielstwa.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141025|zdeterminowana recepcjonistka hostelu "Opatrzność" z nadmiarem energii i waleczności, która nie jest fanką młodych piersi w oknie.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
