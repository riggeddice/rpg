---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
owner: "public"
title: "Łucja Maus"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: **:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: Oddaj to, co otrzymujesz**: 
    * _Aspekty_: lojalna przyjaciołom, okrutna wrogom, promuje współpracę, promuje swój krąg nad innych, promuje bycie życzliwym
    * _Opis_: Domyślnie Łucja dąży do bycia życzliwą. Jednak jeśli ktoś zaatakuje, odpowie bezwzględnością. Zależy jej na swoich bliskich bardziej niż na rodzie i sama wybiera kto jest jej bliski.
* **MET: Żarliwy prowodyr**:
    * _Aspekty_: 
    * _Opis_: Łucja szczerze wierzy w to, że akcje mają swoje konsekwencje i nie unika tych konsekwencji. Jeśli chce coś zrobić, idzie na pierwszy ogień. Pierwsza rzuci cegłą, pierwsza wyda rozkaz. Zdaje sobie sprawy z kontroli Karradraela, dlatego nigdy nie chce być na drugiej linii. She falls first.
* **MRZ: Wolność od Karradraela**:
    * _Aspekty_: niezależność ponad rozkazy, 
    * _Opis_: Łucja nie akceptuje Karradraela, Abelarda Mausa, Renaty Maus itp. Nie chce być "bad guy". Łucji bardzo zależy na tym, by Mausowie byli traktowani jak każdy inny ród i żeby byli siłą dobra.
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Pilot awianów**:
    * _Aspekty_: latanie wyczynowe, 
    * _Opis_: 
* **Przywódca**: 
    * _Aspekty_: dowodzenie rebelią
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Samospaczony Wzór**:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: 
    * _Opis_: 
* **Technomancja**: 
    * _Aspekty_: 
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **Sprzężenie z awianem**:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

## Ogólnie

Łucja Maus jest 31-letnią buntowniczką przeciw władzy Karradraela.

## Pytania generacyjne

> 1. Jakie jest największe dokonanie TP (uznane przez kogoś innego)? Czemu TP była w stanie to zrobić?

> 2. Z czego w ramach tego największego dokonania TP jest szczególnie dumna? Dlaczego właśnie z tego?

> 3. W ramach tego dokonania komu i jak TP pomogła najmocniej? Dlaczego nadal ten ktoś utrzymuje kontakt?

> 4. Z czego TP jest najlepiej znana wśród swoich znajomych? Z czego słynie? Co robi inaczej?

> 5. Pod jakim kątem TP wybiera zlecenia / pracę? Jakiej nigdy by nie przyjęła? Dlaczego?

> 6. Jakiego typu umiejętności TP wykorzystuje podczas wykonywania zadań - coś, czego nie robi nikt inny? Czemu to działa?

> 7. Co przez rywali i zleceniodawców jest uważane za "nieuczciwą" przewagę TP? Czemu to jej pomaga?

> 8. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?

> 9. Gdzie TP znajduje zlecenia gdzie jej rywale nie szukają? Jaki jest jej unikalny kanał? Czemu tam / jak się poznali?

> 10. Z kim przestaje TP? Jak się zachowuje w swoim otoczeniu? Jak by działania TP opisali jej towarzysze?

> 11. W jaki sposób otoczenie TP wsparło jej unikalne umiejętności? Jak wpłynęło na jej zachowanie?

> 12. W jakich okolicznościach TP jest NAJLEPSZĄ osobą do rozwiązania problemu? Jaki to problem? Czemu TP jest wtedy najlepsza?

> 13. O czym TP marzy? Czego się boi/co ją boli? Co robi, by jej marzenia się spełniły?

> 14. Co jest kłopotliwym wydarzeniem z przeszłości TP? Co próbowała zrobić i jej nie wyszło? Czemu? Jak na to zareagowała?

> 15. Co TP robi w czasie wolnym? Co lubi robić? Jak odpoczywa?


### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|do budowania swojego lekarstwa wykorzystuje czarnotrufle Esuriit a nie zwykłe czarnotrufle|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|jest pod ogromnym wpływem Melodii Diakon - uważa Melodię za potencjalnego guru|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|uzyskała pulę czarnotrufli z Eliksiru Aerinus, na leki dla siebie; Paulina ją jakoś dodatkowo ustabilizowała.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|będzie współpracować z Danielem Akwitańskim; chce dobrze dla Mausów i tego terenu|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|zwalcza Tomasza Myszeczkę, bo on zrobił krzywdę jej bliskiemu przyjacielowi|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|postawiła się jakimkolwiek reparacjom wobec Myszeczki i była skłonna próbować uciec inkwizytorowi Mausów. Skończyła pomagając przyjacielowi na portalisku - jak chciała.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|czarodziejka, która sama rozwaliła sobie wzór i wymaga leków by tylko nie służyć Karradraelowi. Buntowniczka antyKarradraelowa. Pomaga Danielowi jak może z magitrownią.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171115|narwana pilot czarnego awiana, solidnie torturowana przez Wiaczesława. Skończyła umierająca, uratowana przez Paulinę. Wiaczesław ma wyczucie dramatyzmu.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
