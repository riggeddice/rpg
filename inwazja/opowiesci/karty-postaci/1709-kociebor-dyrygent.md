---
layout: inwazja-karta-postaci
categories: profile
factions: "Portalisko Pustulskie"
type: "PC"
owner: "prezes"
title: "Kociebor Dyrygent"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Przez pracę do porządku**:
    * _Aspekty_: wkurzają go psuje, wkurza go nieporządek, wprowadzić porządek w tym świecie, mniej nieoznaczoności, ma być "jak jest", nie ma że się nie da
    * _Opis_: wkurzają go ludzie nieporządni, tacy, którzy chcą ukraść, popsuć, gówniarzernia... ludzie, którzy lekceważą cudzą pracę i niszczą rzeczy dla niszczenia. Trochę sztywniak ale jest dobrze.

### Umiejętności


* **Ochroniarz**:
    * _Aspekty_: Służba Ochrony Portaliska, wygrywanie sporów, przejęcie kontroli autorytetem, bijatyka, szybka ucieczka, ewakuacja towaru, ocena charakteru
    * _Opis_: 
* **Detektyw**:
    * _Aspekty_: dokumentacja towaru, tropienie osoby, ukrywanie się, agresywne przesłuchiwanie, podkładanie dowodów, "oczy dookoła głowy", zauważa detale
    * _Opis_: 
* **Magazynier**:
    * _Aspekty_: "wiem co gdzie powinno siedzieć", "wiem gdzie to schować", teoria portali, świetna organizacja czasu
    * _Opis_: 

### Silne i słabe strony:

* **Przesłuchał kogoś "za mocno"**:
    * _Aspekty_: ma szacun na dzielni, nie jest lubiany na salonach, onieśmielający typ
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia transportu**:
    * _Aspekty_: platformy transportowe, telekineza, praca z portalami, 
    * _Opis_: 
* **Magia materii**: 
    * _Aspekty_: wzmacnianie odporności materii, złota rączka (składa narzędzia), 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

 
### Znam

* **Stali klienci i personel Portaliska Pustulskiego**:
    * _Aspekty_: jakieś drobne przysługi, reputacja porządnego i pomocnego
    * _Opis_: 

### Mam

* **Długoletni pracownik Portaliska**:
    * _Aspekty_: "nie z takimi problemami już sobie radziłem", szeroka znajomość portalisk, anomalie portalowe
    * _Opis_: 
* **Właściciel agencji detektywistycznej**:
    * _Aspekty_: z nadania Rodziny Dukata, niekompetentna sekretarka Sylwia (członek Rodziny), 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis



### Koncept



### Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|od Roberta Sądecznego dostał agencję detektywistyczną wraz z mandatem do detektywowania - za zasługi i ratunek Dobrocienia.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|kiedyś, pracownik portaliska Pustulskiego. Od tej pory ma zamiar zostać detektywem.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171024|chce się uwolnić od etykietki "detektyw Sądecznego"; odkrył co stoi za Wieżą Wichrów i że jest ona sprzężona z fazą Allitras, po czym by ratować biznes Filipa, nakłamał Sądecznemu że to wszystko przez glukszwajny...|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171003|główny pracownik Portaliska teleportujący do siebie glukszwajna (i się z nim), skończył z agencją detektywistyczną po tym, jak nie dał się przekupić Myszeczce.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
