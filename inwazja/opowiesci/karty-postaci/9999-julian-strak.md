---
layout: inwazja-karta-postaci
categories: profile
title: "Julian Strąk"
---
# {{ page.title }}

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|chce znaleźć dowody na zło Krystalii Diakon by móc ją porządnie oskarżyć i skazać; nie jakieś byle co.|Adaptacja kralotyczna|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180526|z prokuratury magów; pomógł Kromlanowi i wjechali na Krystalię ze skrzydłem konstruminusów.|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|10/12/17|10/12/19|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180402|Inkwizytor Prokuratury Magów szukający ofiar Hralglanatha. Nie dopuści do złamania Maskarady ani do cierpienia ludzi.|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Rafał Drętwoń](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-dretwon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
