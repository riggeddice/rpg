---
layout: inwazja-karta-postaci
categories: profile
title: "Aptoform Mirasilaler"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150422|wyjątkowo - niewinna ofiara porwana przez Dionizego i Alinę od Tymotheusa. Chciał wolności a trafił do Czarnego Laboratorium w Rezydencji Blakenbauerów...|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|zafascynowany Wandą, której nie potrafi przeskanować; najpewniej na celowniku Arazille.|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150406|który dał radę zinfiltrować Paulinę i Marię przez Marię tworząc końcówkę dziennikarza "Edwarda". Na razie: obserwuje.|[Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150325|tajemniczy aptoform o nie zdefiniowanych celach uczący się przez końcówki "nazista" i "małolata". Pracuje dla (z?) jakiegoś maga.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Marek Rudzielec](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-rudzielec.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Maksymilian Łoś](/rpg/inwazja/opowiesci/karty-postaci/9999-maksymilian-los.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Fryderyk Mruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-mruczek.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Emil Maczeta](/rpg/inwazja/opowiesci/karty-postaci/9999-emil-maczeta.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dariusz Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-larent.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
