---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "foczek"
title: "Krzysztof Grumrzyk"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Postęp przez współpracę**:
    * _Aspekty_: promuje podniesienie standardów życia, promuje innowacyjne podejścia i techniki, naciska na uczciwe podejście, promuje współpracę, "miejsce oszołomów jest w więzieniu", niech każdy dostanie szansę
    * _Opis_: Z jednej strony pracuje nad awianami w montowni pod osobą która ma ideały związane z postępem całego regionu, z drugiej przyjaźni się z Zofią, która NIE jest "role model" dla nieletnich. Chce doprowadzić do technologicznego postępu jak najszybciej - żeby jak największej ilości osób żyło się jak najlepiej.
* **MRZ: Zdobyć kontakty i pchnąć firmę do przodu**: 
    * _Aspekty_: niech firma wyjdzie do ludzi a nie tylko bogaczy, pozyskać sponsorów i klientów, utrzymać montownię przy życiu, "każdy się przyda", osłabia drapieżny kapitalizm
    * _Opis_: Krzysztof próbuje wybić montownię i siebie do przodu; niech ta firma wyjdzie do ludzi, zapewnia rzeczy przydatne innym - też tańsze, ogólnie, w życiu są rzeczy ważniejsze niż tylko kapitalizm

### Umiejętności

* **Konstruktor**
    * _Aspekty_: naprawa awianów, konstrukcja awianów, warianty awianów, "jeśli to jest awian, ja wiem co z tym zrobić", awianowy nerd, 
    * _Opis_: 
* **Pilot awianów**:
    * _Aspekty_: oblatywacz, poskramianie viciniusów, pilotowanie awianów, latanie wyczynowe, kaskader, odporny na przeciążenia
    * _Opis_: W ramach pracy w montowni awianów Krzysztof specjalizuje się w ujarzmianiu i sterowaniu (tresurze?) awianów. Jako, że czasami praca z nie do końca skalibrowanymi awianami bywa niebezpieczna, Krzysztof radzi sobie dobrze z niebezpieczeństwami
* **Marketingowiec**: 
    * _Aspekty_: wzbudzanie zachwytu, świetna autoprezentacja, sprzedawanie produktów, znajdowanie wspólnych interesów
    * _Opis_: Krzysztof bardzo często - z uwagi na wielkość montowni - ma kontakt z klientem. Nie tylko musi zaprezentować jak dobry jest awian (zwłaszcza spersonalizowany), ale jeszcze czasem musi z klientem gadać. Może nawet wyjść na colę. 
* **Polityk**:
    * _Aspekty_: społecznik, polityka lokalna, lobbying, pozyskiwanie sojuszników, inspirowanie do działania, charyzmatyczny mówca
    * _Opis_: By wszystkim żyło się lepiej trzeba przekonać ludzi i wprowadzić określone inicjatywy oddolne i działania. Skupia się na programach wpływających na grupy ludzi by im żyło się lepiej i na wyrównywaniu nierówności. Niech każdy ma szansę.
    
### Silne i słabe strony:

* **Dokładny mistrz konstruktor**:
    * _Aspekty_: przykłada się do pracy, jak robi to robi dobrze, kiepsko robi na szybko
    * _Opis_: Gdy już zrobi jakiś element do awiana, gdy na czymś się skupi, to jest to zdecydowanie state of the art. Doskonały konstruktor. Niestety, nie umie robić szybko - i tak by chciał poprawić. Jak będzie musiał na szybko, będzie gorsze niż normalnie. Jak może zrobić dobrze, zrobi świetnie.
* **Król żywiołaków**: 
    * _Aspekty_: moje słowo jest rozkazem
    * _Opis_: 
* **Terytorialny**:
    * _Aspekty_: zaciekle broni swojego terenu, zraża ludzi
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia elementalna**:
    * _Aspekty_: żywiołaki powietrza, guardian winds, kontrola żywiołaków, znajdowanie węzłów elementalnych, źródła energii, elementalne zasilanie, wygaszanie elementalne
    * _Opis_: Specjalizuje się w żywiole powietrza i w dostarczaniu żywiołaków... i ich wmontowywaniu w awiany. Jako kaskader często wykorzystuje też ten sympatyczny żywioł do zapewnienia bezpieczeństwa - guardian winds. 
* **Magia materii**: 
    * _Aspekty_: naprawa i manipulacja awianów, wzmacnianie struktury, upiększanie, pozyskiwanie i przekształcanie materiałów
    * _Opis_: 
* **Kataliza**:
    * _Aspekty_: wykrywanie przekierowywanie transformacja energii magicznej, wygaszanie energii magicznej
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* pracuje w montowni awianów jako oblatywacz

### Znam

* **Zofia Przylga i Ptasi Trójkąt**:
    * _Aspekty_: wymiana wiedzy, wsparcie w ciemnych sprawkach, pokazy lotnicze
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Eksperymentalne awiany różnego typu**:
    * _Aspekty_: dostosowane do sytuacji, efektowne, szybkie, bardziej inteligentne, bardziej niebezpieczne
    * _Opis_: 
* **Narzędzia konstrukcyjne awianów**:
    * _Aspekty_: naprawić awiana, ekstrakcja energii magicznej, rekalibracja źródła energii
    * _Opis_: 
* **Dogłębna wiedza o otoczeniu**:
    * _Aspekty_: pływy energii, gdzie co znaleźć, rozkład terenu, tajemnice szerzej nieznane
    * _Opis_: Krzysztof często musi pozyskać specyficzne żywiołaki i specyficzne materiały do konstrukcji awianów. Dlatego jak mało kto ma rozpisane mapy terenu, wie co gdzie się znajduje, wie jakie są pola energii, wie jak energia zmienia się z perspektywy czasu, wie jak zaaplikować wiedzę o tym do zmiany stanu awianów... 

* **Czarny ptak**
    * _Aspekty_: Szybki, lokalizator awianów, paralizator awianów, podpakowany na maksa, niezawodny, czujnik elementali
    * _Opis_: Maksymalnie ulepszony awian (przez użycie już przetestowanych podzespołów) do celów ekstremalnych takich jak łapanie i sprowadzanie niesprawnych awianów, szybkie docieranie do miejscu zdarzenia...

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171024|nie tylko legendarny konstruktor i właściciel Czarnego Ptaka - też mag, który uratował Maję Weiner przed "zbuntowanym" awianem i pomagał Paulinie w naprawianiu szkód.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|uratował klientów zakładników z okupowanych awianów, przekonał Oktawię, by go wspierała i został gwiazdą Youtuba dzięki Paradoksowi.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
