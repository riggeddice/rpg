---
layout: inwazja-karta-postaci
categories: profile
title: "Damian Bródka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140320|czarodziej o podobnych poglądach do Gustawa Siedeła ale bardzo praworządny.|[Sprawa magicznych samochodów](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
