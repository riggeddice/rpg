---
layout: inwazja-karta-postaci
categories: profile
title: "Cezary Piryt"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141022|mąż Estery i jednocześnie agent biurowy Skorpiona. Jest świadomy mniej więcej czym Skorpion się stał...|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140109|Strażnik, skażony vicinius w formie pośredniej między człowiekiem a Dromopod Iserat. KIA w tunelach.|[Uczniowie Moriatha](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Lenart Stosal](/rpg/inwazja/opowiesci/karty-postaci/9999-lenart-stosal.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Antoni Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-myszeczka.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
