---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Tymoteusz Maus"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: attention whore**:
    * _Aspekty_: "ja im pokażę co potrafię", buntowniczy, perfekcjonista, fatalny gust
    * _Opis_: 
* **FIL: kto nie tworzy, ten nie żyje**: 
    * _Aspekty_: perfekcjonista, fatalny gust
    * _Opis_: 
* **BÓL: Perfekcjonista**:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Jubiler**:
    * _Aspekty_: biżuteria
    * _Opis_: 
* **??**: 
    * _Aspekty_: narzucanie woli
    * _Opis_: 

    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Kataliza**:
    * _Aspekty_: hodowla kryształów, inżynieria węzłów, artefakcja
    * _Opis_: 
* **Magia mentalna**: 
    * _Aspekty_: 
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **mnóstwo dziwnej biżuterii**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Młody, naiwny i buntowniczy Tymoteusz Maus zawsze chciał zostać magiem KADEMu. Jednak nie posiadał wystarczających kompetencji; dodatkowo, KADEM go nie chciał. I tak Tymek Maus został magiem Srebrnej Świecy. Aha, na EAM też go nie chcieli. Tymek uważa, że to dlatego, bo pochodzi z rodu Maus.
Dość sfrustrowany, skupił się na inżynierii magicznej, a zwłaszcza na katalizie i artefakcji. Osiągnął nawet całkiem niezłe sukcesy na tym polu, choć nie został nigdy doceniony odpowiednio.
Tymek potrafi się przyłożyć do pracy i naprawdę dokładnie rozwiązać większość problemów. Większość jego starań się w jakimś stopniu udaje, choć jego wrogowie i sojusznicy zasypiają patrząc, jak Tymek podchodzi do problemu kolejny raz i znowu coś mu się nie udaje.
Pole, na którym Tymoteusz się całkiem nieźle spełnia to… biżuteria. Całkowicie nieludzka, tworzona przez narastanie kryształów materii przez energię magiczną kształtowaną przez fluktuacje różnych dziwnych urządzeń. A przynajmniej - do momentu, aż Tymek próbuje coś z tym faktycznie zrobić; niski Heart oraz FATALNY gust praktycznie gwarantują, że nie będzie miał nigdy ogromnego powodzenia w tym obszarze (choć jeśli pozwala naturalnie rosnąć kryształom nie przeszkadzając, to jakkolwiek nieludzka i dziwna, ale biżuteria może być uznana za formę sztuki).
Główne impulsy kierujące Tymoteuszem to: tworzyć, być uznanym oraz nie pozwolić nikomu decydować o jego życiu.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151101|który w pogoni za wiłą dał się jej pociąć i wylądował w szpitalu, przez co przysporzył kłopotów Gali.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151013|który porwał Paulinę i dowiedział się, że tu magia nie jest trwała. Szuka śladów w lesie... i wiły. Ma nadzieję jej nie znaleźć.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151003|nie radzi sobie z Węzłem przy kółku modlitewnym i ogólnie Węzeł wybuchł mu w twarz. Inżynier węzłów my ass.|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150930|który w sumie nie wystąpił na misji... ale został wrobiony że to jego wina; w tle jest konflikt Tymka z Joachimem.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|mający nadzieję na niezależność i wykazanie się porywając dzieci i wysyłając je na Syberię współpracując z Galą Zajcew.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|młody, naiwny i buntowniczy; seiras chce go wydać za tien Galę Zajcew. Seiras ma z nim kłopoty...|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|kiedyś chciał być magiem KADEMu ale mu odmówili. Współpracował z Maurycym by stworzyć steampunkową pozytywkę. Ogólnie, taki jęczliwy typ.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|6|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|1|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Matylda Daczak](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-daczak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
