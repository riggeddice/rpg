---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "PC"
title: "Mordred Blakenbauer"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **BÓL: Uwolnić się od klątwy i rodziny**:
    * _Aspekty_: znaleźć bratnią duszę, założyć nową rodzinę
    * _Opis_: 
* **MET: Wspieranie Millenium**:
    * _Aspekty_: moralny na swój sposób
    * _Opis_: 
* **MRZ: Odnaleźć swoją mentorkę Klaudię Weiner**:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Krawiec**:
    * _Aspekty_: uniformy z pajęczyn
    * _Opis_: 
* **Terminus**: 
    * _Aspekty_: zastraszanie
    * _Opis_: 
* **Demibeast**:
    * _Aspekty_: walka dystansowa
    * _Opis_: 
* **Życie w świecie magów**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Demibeast**:
    * _Aspekty_: regeneracja, szpetny, antypatyczny, cichy, skryty
    * _Opis_: 
* **Klątwa Blakenbauerów**: 
    * _Aspekty_: częściowo kontrolowana bestia
    * _Opis_:  

## Magia

### Szkoły magiczne

* **Kataliza**:
    * _Aspekty_: magiczne pułapki, systemy defensywne, rozrywanie zaklęć, tkanie zaklęć, tarcze antymagiczne, wysysanie energii magicznej, portale taktyczne
    * _Opis_: 
* **Biomancja**: 
    * _Aspekty_: magiczne pułapki, systemy defensywne, pajęczaki, wytwarzanie trucizn, wzmacnianie ciała, osłabienie ciała
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: portale taktyczne
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **szanowany w Millenium**:
    * _Aspekty_: renoma skutecznego zabójcy
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Z rodziną wychodzi się dobrze tylko na zdjęciach"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170920|przekaźnik energii do walki z Kinglordem, negocjator (serio), skończył jako partner Hektora w prokuraturze magów. Zwerbował Hannę (Henryka) do Millennium.|[Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|10/09/03|10/09/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170914|pilnuje Henriettę i Piotra. Nie przegania Silurii. Konspiruje z Rafaelem. Dostarczył swojej krwi do stworzenia Henrietty.|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|10/08/31|10/09/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170830|ratuje Piotra przed przypadkowymi zabójcami i oportunistycznym terminusem. Dostaje opiernicz - zasłużenie. Zniszczył obrazy Piotra wzywając moc Arazille. Ogólnie, teleporty ma Paradoksalne.|[Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|10/08/29|10/08/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170823|inwigilator posiadłości Blutwurstów. Ma oczy w całej posiadłości. Uwierzył Siwieckiemu jako dziewczynce, że ten był kontrolowany.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170816|władca trujących pająków, mistrz zastraszania Sandry i celny kineta|[Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|10/01/27|10/01/29|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|4|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|3|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Rafał Warkocz](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-warkocz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marianna Biegłomir](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-bieglomir.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Manfred Jarosz](/rpg/inwazja/opowiesci/karty-postaci/9999-manfred-jarosz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
