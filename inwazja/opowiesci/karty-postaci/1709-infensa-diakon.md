---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Infensa Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: pragmatyczna**:
    * _Aspekty_: 
    * _Opis_: wykorzysta minimalne surowce potrzebne do zwycięstwa.
* **MET: nieustępliwa**:
    * _Aspekty_: agresywna, niezależna
    * _Opis_: jeżeli coś robi, ma zamiar zwyciężyć albo przegrać. wiecznie atakuje, rzadko się broni. Glass cannon. nikt nie będzie jej mówił co ma robić.

### Umiejętności

* **Terminus**:
    * _Aspekty_: terror, łowca magów, ukrywanie / śledzenie, pułapki, gimnastyka, metody działania Świecy
    * _Opis_: 
* **Rusznikarz**:
    * _Aspekty_: pistolety
    * _Opis_: 

### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia transportu**:
    * _Aspekty_: sprowadzanie sprzętu, 
    * _Opis_: 
* **Kataliza**:
    * _Aspekty_: skażanie zaklęć
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 	
	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

29 lat, terminuska, ekstremistka
KIEDYŚ, jako Iza Łaniewska:

Nie należy już do frakcji Reformatorów SŚ. Izabela Łaniewska opuściła wszystkie frakcje i choć formalnie nadal jest członkiem SŚ, nie ma nad sobą mistrza i działa bardzo niezależnie. Jest terminusem nie należącym do skrzydła z uwagi na trudny charakter. Jest bezwzględność i radykalne metody sprawiają, że mało kto chce z nią pracować, choć jest cenionym i kompetentnym magiem. Sama o sobie mówi, że jest idealistką a jej ideały zostały zdradzone przez świat.

Poprzednia wersja postaci:
Izabela Łaniewska

TERAZ, jako Infensa Diakon:

Infensa. Z łaciny "to attack, to avenge". Transformacja to była forma terapii. Że teraz jej lepiej. Że nienawidziła Silurii… ale faktycznie to jest dla niej lepsze. Teraz… teraz jest szczęśliwa.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|podporządkowana Silurii Diakon|Taniec Liści|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|przestała być Izabelą Łaniewską i pogodziła się z Silurią Diakon.|Taniec Liści|
|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|zaakceptowała swoją dualistyczną naturę - dominator i uległa|Taniec Liści|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180311|groźniejsza niż kiedykolwiek, powiedziała Silurii o tym, że z Markiem Kromlanem łączyły ją frakcja, rywalizacja i przyjaźń.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160911|kiedyś: Iza Łaniewska, opowiedziała Silurii o swoich przeszłych działaniach z Kornelem i została uwiedziona; jest w roli podległej wobec Silurii.|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|10/07/15|10/07/17|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150913|wtedy: Izabela Łaniewska, wspomnienie z przeszłości; okazuje się, że na akcji w Kotach nie zgadzała się z Tamarą i robiła błędy taktyczne.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160922|jeszcze: Iza Łaniewska, która jest w trakcie transformacji i rekonstrukcji w Millennium. Kornel i Dorota zbierają info na jej temat. Nieobecna.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150718|wtedy: Izabela Łaniewska, jej działania były szeroko rozpatrywane przez Wydział Wewnętrzny. Najpewniej ulegnie przekształceniu w Diakonkę.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161005|(jeszcze Iza Łaniewska), która zastawiła pułapkę na "skorumpowanego * maga" Świecy i go aresztowała - wbrew woli przełożonym i ku ich utrapieniu.|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|10/05/05|10/05/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150604|wtedy: Izabela Łaniewska, która przegrała wszystko mimo braku dowodów przez swój porywczy charakter. Przymusowo wysłana na terapię do Millennium, nie liczy się już w SŚ.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|wtedy: Izabela Łaniewska, zły duch, który siedzi w tle tego wszystkiego. Do tego, zainteresowała się napaścią na Annalizę.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|wtedy: Izabela Łaniewska, która skutecznie alienuje się od wszystkich. Oraz potencjalnie jako osoba która za wszystkim stojąc wpuściła Spustoszenie na Koty i na pewno najbardziej irytująca postać w systemie.|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|wtedy: Izabela Łaniewska, która potencjalnie wiedziała o Spustoszeniu zanim się pojawiło... i której rola w tej historii jest dość interesująca i niejasna.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150110|wtedy: Izabela Łaniewska, tym razem cicha (niepokojący objaw) terminuska SŚ na operacji usunięcia Spustoszenia|[Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|10/04/15|10/04/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140505|wtedy: Izabela Łaniewska, ostra i bezwzględna terminuska lojalna Wołkowcowi niezależnie od tego co na to Świeca|[Musiał zginąć, bo Maus](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|10/01/05|10/01/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|7|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|6|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|5|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|5|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|5|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|4|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|3|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|3|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|2|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html), [160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|2|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[miślęg Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-misleg-pieluszka.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[echo Lugardhaira](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-lugardhaira.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Vuko Milić](/rpg/inwazja/opowiesci/karty-postaci/9999-vuko-milic.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tadeusz Aster](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-aster.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Renata Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Magnus Spellslinger](/rpg/inwazja/opowiesci/karty-postaci/1709-magnus-spellslinger.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Kwiatuszek](/rpg/inwazja/opowiesci/karty-postaci/9999-kwiatuszek.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Krystian Miałcz](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-mialcz.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Korwin Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-korwin-morocz.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Kornelia Modrzejewska](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-modrzejewska.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Klotylda Świątek](/rpg/inwazja/opowiesci/karty-postaci/9999-klotylda-swiatek.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Kaspian Miauczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-miauczek.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Karol Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jonatan Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-jonatan-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Jolanta Pirat](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-pirat.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jerzy Buława](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-bulawa.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Jan Kotlin](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-kotlin.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Irena Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Hlargahlotl](/rpg/inwazja/opowiesci/karty-postaci/9999-hlargahlotl.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Gabriela Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriela-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cierń](/rpg/inwazja/opowiesci/karty-postaci/9999-ciern.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Bożena Górzec](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-gorzec.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Aniela Lipka](/rpg/inwazja/opowiesci/karty-postaci/1709-aniela-lipka.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Adam Wołkowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-wolkowiec.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
