---
layout: inwazja-karta-postaci
categories: profile
title: "Kinglord"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|dostał krew Marzeny Dorszaj|Nicaretta|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|dostał krew Henryka Siwieckiego dzięki działaniom Kariny|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170920|który objął władzę w Czarnej Wieży i używa nienaturalnych form magii, niezgodnych z tymi jakie działają dla zwykłych magów.|[Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|10/09/03|10/09/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170120|który dostał krew wszystkich magów w rejonie Wyjorza - Henryka, Kariny, Marzeny. Nie ma jak przegrać. Poza tym, że przeciw niemu jest sojusz. Jak napisałem, nie ma jak przegrać.|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|2|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
