---
layout: inwazja-karta-postaci
categories: profile
title: "Barnaba Łonowski"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|uznany za bohatera za próbę uratowania Jodłowca i szybkie myślenie.|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170725|pseudonim "krzywa katiusza", technomancja, elementalna i materia. Dalekosiężny miłośnik artylerii i dewastacji ze Świecy. W odpowiednim momencie wezwał posiłki.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
