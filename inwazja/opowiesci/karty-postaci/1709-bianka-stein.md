---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Bianka Stein"
---
# {{ page.title }}

## Postać 


### Motywacje (do czego dąży)

* **FIL: Przedsiębiorcza**:
    * _Aspekty_: 
    * _Opis_: "Nie mamy zbyt wiele… ale powinno się udać. Dobrze, załatwię to."
* **BÓL: Karierowiczka**:
    * _Aspekty_: chełpliwa, pewna siebie
    * _Opis_: "Świeca patrzy tylko na magów Rodów. Sama muszę zadbać o siebie. Proszę bardzo. Tylko ja mogłam to zrobić. To różni mistrzów od innych."
* **KLT: Oszczędna**:
    * _Aspekty_: 
    * _Opis_: "Wiesz, to co mamy działa. Nie będę przepłacała; jeszcze się zepsuje…"

### Umiejętności

* **Portale strategiczne**:
    * _Aspekty_: konstruowanie portali strategicznych, sprzęganie portali
    * _Opis_: 
* **Inżynier energii magicznej**:
    * _Aspekty_: 
    * _Opis_: 
* **Inżynier artefaktów**:
    * _Aspekty_: 
    * _Opis_: 
* **Polityk**:
    * _Aspekty_: polityka Świecy
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Kataliza**:
    * _Aspekty_: badanie energii magicznej, stabilizacja energii magicznej, węzły, źródła energii
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Ceniona ekspert Świecy w ramach portali i katalizy; wysoko opłacana specjalistka

### Znam

* **Powszechnie uznana jako ekspert od portali w branży**:
    * _Aspekty_: 
    * _Opis_: 
* **Znana czarodziejka Świecy (katalistka wysokiej klasy)**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **Laboratorium w Świecy Daemonica**:
    * _Aspekty_: 
    * _Opis_: 
* **Specjalistyczny sprzęt do portali strategicznych**:
    * _Aspekty_: 
    * _Opis_: 	
* **Podręczne analizatory energii magicznej**:
    * _Aspekty_: 
    * _Opis_: 	
* **Generatory energii magicznej**:
    * _Aspekty_: 
    * _Opis_: 	
* **Sprzęt do konstrukcji artefaktów**:
    * _Aspekty_: 
    * _Opis_: 		


# Opis


## Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|eliksir Rafaela Diakona, który ją zachęca do Tadeusza Barana|Powrót Karradraela|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|lekka fobia przed Diakonami (-1 SCA, SCD, SCF w kontaktach z Diakonami)|Powrót Karradraela|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|lekka fobia przed Mausami (-1 SCA, SCD, SCF w kontaktach z Diakonami)|Powrót Karradraela|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|widzi bohaterstwo Tadeusza Barana w niesamowicie pozytywnym Świetle|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170122|wypożyczona Anecie Rainer, by ta mogła opanować Kompleks Centralny. Najlepsza katalistka w okolicy.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170103|zniszczone morale, ciężko psychicznie uszkodzona, ale działa dla Andrei. Otworzyła (praktycznie niemożliwy) portal do Czelimina i zaczyna podkochiwać się w Tadeuszu Baranie.|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|10/07/16|10/07/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160713|katalistka i specjalistka od portali i energii * magicznych, ewakuowana przez Zajcewów a potem Blakenbauerów poza Kopalin.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|katalistka próbująca działać zgodnie ze swymi przekonaniami i ideałami; złamana przez Silurię i Eleę - jak nie pomoże, Judyta najpewniej umrze.|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160724|która wypaczyła Klucz otwierający połączenia z EAM, odcinając Świecę od EAM. Wpadła w ręce Wiktora.|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|10/06/24|10/06/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|katalistka Świecy, bardzo kompetentnta i pracowita. Jedna z najlepszych w Kopalinie. Wyizolowała nić Spustoszenia i się nim zaraziła.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojciech Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-szudek.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Tomasz Kuracz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kuracz.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Jakub Niecień](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-niecien.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Edward Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-bankierz.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Alicja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-weiner.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
