---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
title: "Antonina Brzeszcz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Idealistka**:
    * _Aspekty_: altruistka
    * _Opis_: 
* **BÓL: Wypalenie zawodowe**:
    * _Aspekty_: poczucie bezsilności
    * _Opis_: 
* **MET: Psycholog z powołania**:
    * _Aspekty_: chce pomóc dzieciom poradzić sobie z życiem, 
    * _Opis_: 

### Umiejętności

* **Psycholog**:
    * _Aspekty_: praca z uczniami, wyciąganie prawdy, trendy wśród młodzieży
    * _Opis_: 
* **Prawnik**:
    * _Aspekty_: prawo karne
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

* **brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **Przedstawiciele prawa**:
    * _Aspekty_: 
    * _Opis_: 
* **Sieć zawodowa psychologów**:
    * _Aspekty_: 
    * _Opis_: 
* **Młodzież w Okólniczu**:
    * _Aspekty_: 
    * _Opis_: 
* **Nauczyciele i rodzice z Okólnicza**:
    * _Aspekty_: 
    * _Opis_: 	
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 


# Opis
Zwana Sarą
Psycholog szkolny z Okólnicza

Psycholog szkolny z powołania.
Niestety, przeciążenie pracą i trudna dyrekcja doprowadziły ją na skraj wypalenia zawodowego.
W swojej karierze zetknęła się z różnymi przypadkami i sytuacjami, również kryminalnymi lub ocierającymi się o kryminał. Stąd pewna znajomość przepisów prawa

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160809|naciskała gdzie miała naciskać, zdobywała informacje i łagodziła konflikty.|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Jolanta Lipińska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-lipinska.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Izabela Bąk](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-bak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Filip Czumko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czumko.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Elżbieta Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Damian Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Aleksander Tomaszewski](/rpg/inwazja/opowiesci/karty-postaci/1709-aleksander-tomaszewski.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Adolphus von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-adolphus-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
