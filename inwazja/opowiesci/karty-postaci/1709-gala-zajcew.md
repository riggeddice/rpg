---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Gala Zajcew"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: hedonistka**:
    * _Aspekty_: kocha przepych
    * _Opis_: 
* **BÓL: opiekunka zbłąkanych kotków**:
    * _Aspekty_: ma słabość do ofiar losu
    * _Opis_: 
* **MET: przedsiębiorcza**:
    * _Aspekty_: ryzykantka
    * _Opis_: 

* **Handlarka**:
    * _Aspekty_: businesswoman, spedycja, organizatorka, wszystko załatwi, ocena ludzi
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia transportu**:
    * _Aspekty_: aportacja, deportacja, spedycja magiczna
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_:
	

# Opis
Gala Zajcew jest świetną organizatorką. Nie tylko bardzo dobrze zna się na logistyce i przemieszczaniu osób i towarów (spedycja), ale dodatkowo ma doskonale opanowane główne trasy handlowe i potrzeby w określonych okolicach. Bardzo dobrze rokująca młoda handlarka, woli pracować legalnie używając jedynie niewielkich środków "wsparcia" Zajcewów niż angażować się w czarne interesy.
Gala jest ryzykantką; jej Płomień Zajcewów objawia się jako skłonność do zachowań ryzykownych i maksymalizacji adrenaliny. Co nietypowe dla Zajcewów, Gala działa zwykle z drugiej linii, choć w kwestii finansów i surowców bilansuje zazwyczaj na ostrzu noża. Raczej subtelna niż bezpośrednia, znajduje adrenalinę i radość czytając rekordy i zyski/straty niż na polu bitwy czy w łóżku.

## Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151101|która pokazała swoje bardziej okrutne oblicze; nadal nikomu nie zrobiła krzywdy, co w świetle okoliczności się jej chwali.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151013|która chce wycofać się z Gęsilotu a najlepiej o wszystkim zapomnieć jak najszybciej. Nie będzie jej dane. Wezwała czyściciela.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151003|rozgoniła kółko modlitewne ratując Tymka, wyciągnęła Czyczyża ze szpitala i zaczyna mieć wątpliwości, czy koszty mają sens.|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150928|prawdopodobny mastermind planu konstrukcji Węzła Emocjonalnego w Gęsilocie i zleceniodawczyni Czyczyża.|[Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|10/05/31|10/06/01|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150722|która potrafi złożyć całkiem trudną logistycznie operację, choć potyka się na niespodziewanych szczegółach. Pracuje z Tymkiem Mausem.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|która nie ma nic przeciw małżeństwu z Tymkiem Mausem; ponadto nic o niej jeszcze nie wiadomo.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|paserka od której Ignat musiał odkupić pozytywkę. Zdobyła dobrą cenę; w sumie najwięcej ona zyskała na całej tej sprawie. Nieobecna na misji.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|6|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|4|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|2|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Rafał Maciejak](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-maciejak.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Matylda Daczak](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-daczak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Filip Czątko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czatko.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
