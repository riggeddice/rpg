---
layout: inwazja-karta-postaci
categories: profile
title: "Izabela Bąk"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160809|nauczycielka muzyki (skrzypce), uczy Karinę i po złamaniu zaklęć przez Olka i Tosię wpadła w zasadniczą histerię. Wyjdzie z tego.|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Jolanta Lipińska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-lipinska.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Filip Czumko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czumko.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Elżbieta Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Damian Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Antonina Brzeszcz](/rpg/inwazja/opowiesci/karty-postaci/1709-antonina-brzeszcz.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Aleksander Tomaszewski](/rpg/inwazja/opowiesci/karty-postaci/1709-aleksander-tomaszewski.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Adolphus von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-adolphus-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
