---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
owner: "public"
title: "Lucjan Kopidół"
---
# {{ page.title }}

## Koncept

Doktor Who (Capaldi) + Elwira + Thrawn (aspekt "sztuka")

Miłośnik współdziałania rozwiązujący problemy pryzmatyczne przy użyciu intuicji. Nieprzystosowany do życia.

## Postać

### Motywacje

#### Kategorie i Aspekty

| **Kategoria**     | **Aspekty**                        |
|:------------------|:-----------------------------------|
| Indywidualne      | rodzina; kolekcjonowanie; intuicja |
| Społeczne         | wybaczanie; współdziałanie; dane   |
| Wartości          | pacyfista; relacje                 |

#### Szczególnie

| **Co chce by się działo?**                                                | **Co na pewno ma się NIE dziać? Co jest sprzeczne?**                                      |
| dogłębnie rozpatrz temat uwzględniając dane; gromadź WSZYSTKIE informacje | zdążenie na czas bez perfekcji też jest wartością; nie wszystkie dane należy pielęgnować  |
| poluj na iskrę natchnienia nieważne gdzie nie jest                        | metodycznym i usystematyzowanym działaniem rozwiążesz problem                             |
| promuj rozpatrywanie problemu przez wszystkie możliwe dane                | procesy i kultura pozwalają na działanie w niepewności                                    |
| dbaj by wszyscy cię lubili; pomoc rodzinie i bliskim jest najważniejsza   | sukces jest ważniejszy niż relacje; przedkłada cokolwiek nad swoją rodzinę                |
| daj każdemu szansę nieważne kim jest; zawsze zakładaj dobrą intencję      | fakty i przeszłe czyny pozwalają odrzucić część osób                                      |
| najlepiej jeśli nikt nie zauważy że tu byłeś; działaj dyskretnie          | wszyscy muszą rozumieć jak bardzo im pomogłeś                                             |

### Umiejętności

#### Kategorie i Aspekty

| **Kategoria**      | **Aspekty**                                      |
| Anioł biznesu      | inwestor; manager; doradca biznesowy             |
| Analityk biznesowy | analizy; wizualizacje; mierniki                  |
| Naukowiec          | obserwacja; pomiary                              |
| Poeta              | wróżbita; nastroje; introspekcja; kultura wysoka |

#### Manewry

| **Jakie działania wykonuje?**                                                  | **Czym osiąga sukces?**                                                                    |
| szybkie bogacenie się; wykrywanie anomalii; dobór odpowiedniej poezji          | dużo dziwnych danych; obserwacja nastroju                                                  |
| dostarczanie projektu w budżecie; motywacja do wspólnego celu                  | zarządzanie projektem; prawidłowe kierowanie pracami; kompensacja "dziwnymi rozwiązaniami" |
| wywoływanie wrażenia nieistotności LUB epickości; przekonywanie (manipulacja?) | wizualizacja danych; deklamowanie poezji; pisanie poezji                                   |

### Silne i słabe strony

#### Kategorie i Aspekty

| **Kategoria**        | **Aspekty**                      |
| Niezrozumiały umysł  | dziwak; obcy umysł; mag anomalny |

#### Manewry

| **Co jest wzmocnione**                                         | **Kosztem czego**                                                    |
| bardzo odporny na wszelkie działania mentalne i "dziwność"     | nie może korzystać z pozytywnych cech Pryzmatu; jest obok Pryzmatu   |
| znajduje powiązania na które "nie ma faktów"                   | niestety; często znajduje powiązania; których nie ma - niewiarygodny |
| wszędzie czuje się jak w domu; żadna sytuacja go nie deprymuje | nie jest traktowany poważnie i jest uważany za dziwaka               |

### Szkoły magiczne

#### Kategorie i Aspekty

| **Kategoria**     | **Aspekty**                                                                                                                    |
| Magia Zmysłów     | adaptacja do obcych warunków; wizualizacje; inne przedstawienie informacji; poszerzenie zbieranych danych; poszerzenie zmysłów |
| Magia Mentalna    | adaptacja do obcych warunków; wzmocnienie intuicji; wspomaganie umysłu; szersze zbieranie informacji; mindmeld                 |
| Biomancja         | adaptacja do obcych warunków; wzmocnienie zmysłów                                                                              |

#### Manewry

| **Jakie działania wykonuje?**                                               | **Czym osiąga sukces?**                                     |
| przetrwanie w piekle; ustabilizowanie poczytalności; zrozumienie szaleństwa | adaptacja; wspomaganie zmysłów; wspomaganie umysłu          |
| rozbicie iluzji; znalezienie wzoru w danych; odkrycie kluczowych faktów     | wizualizacje; poszerzone zmysły; niesamowita intuicja       |
| prowadzenie zespołu; synteza wiedzy; propagacja wiedzy                      | sprzęg bio i menta; połączenie umysłów; hivemind            |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| **Kategoria**                         | **Aspekty**                                           |
| Ogromna biblioteka poezji             | kultura wysoka; nastroje; wejście na salony           |
| Zaawansowane narzędzia wizualizujące  | wnioskowanie; przekonywanie; zrozumienie; anomalie    |
| Znajomość wielu lokalnych firm        | luźne kontakty; puls rynku; przepływy biznesowe       |
| Niemile widziany w okolicy Świecy     | reputacja idioty; czujne na nim oko terminusa         |

#### Manewry

| **Jakie działania wspierane?**                                                         | **Czym osiąga sukces?**                                                      |
| wzbudzanie wrażenia kulturalnego; przekupstwo wyższych sfer; zmuszenie do pomyślenia   | świetna, rzadka poezja; ogromna biblioteka; efektowne wizualizacje           |
| monitorowanie czyichś ruchów (biznesowo); zdobycie części pozornie niemożliwych danych | wiedza z okolicznych firm; monitoring i analiza przepływów; płatne raporty   |

## Opis

### Ogólnie

Lucjan Kopidół w pewien sposób jest maskotką KADEMu, magiem nie do końca traktowanym poważnie.

W "poprzednim życiu" był inwestorem pracującym dla anioła biznesu, niejakiego Dariusza Larenta. Kopidół miał za zadanie znajdować niewielkie firmy, w które Larent może zainwestować. I znajdował je z powodzeniem, bazując na swojej niesamowitej intuicji - do momentu, aż odrzucił firmę maga Świecy, Wojciecha Winniczka. Ten użył magii by zmusić Kopidoła do podjęcia innej decyzji i doszło do Czegoś Dziwnego - w Kopidole obudziła się "krew viciniusa".

W wyniku wszystkiego Winniczek utracił magię a Kopidół ją uzyskał, wzmacniając swoje naturalne umiejętności i zdolności. Od razu opadli go magowie Świecy i chcieli go badać, sprawdzać, ratować Winniczka... ogólnie, życie Kopidoła się bardzo pogorszyło.

To jest, do momentu, aż do akcji wkroczył Robert Seton i wykonał efektowny atak na Świecę by porwać Lucjana Kopidoła. Trzeba przyznać, że Kopidoła NIE TRZEBA BYŁO EFEKTOWNIE PORYWAĆ, bo Świeca nie była nim aż tak zainteresowana, ale nie poprawiło to naturalnej ostrożności Lucjana (ani jego kondycji psychicznej). Cała ta sprawa doprowadziła do tego, że Lucjan Kopidół, jakkolwiek znalazł miejsce na KADEMie (i to jako mag a nie królik doświadczalny) zachowuje się z dużą ostrożnością i nieufnością wobec całego tego skomplikowanego świata magów.

Jest to szczęśliwie ożeniony i dzieciaty mag KADEMu pochodzący z innego, łagodniejszego świata. Troszkę nie pasuje do KADEMu kulturowo. Mag Pryzmaturgii, wierzący w intuicję, nastroje i analizę sztuki i decyzji celu, by móc określić przyszłość. Nieco taki obłąkany wróżbita jak się go bliżej pozna, acz wyciszony i nie narzucający się na co dzień. Ma słabość do kobiet i unika konfliktów, bardzo łatwo mu wejść na głowę.

Uwielbia wręcz wizualizować Pryzmat czy dane wszelkiego typu, przez co często pracuje z Andżeliką (Karina dowcipkuje, że Kopidół się w niej podkochuje). Doskonały analityk, który potrafi długo obserwować dany byt, by - w niezrozumiały dla nikogo sposób - rozwiązać problem. Oczywiście, zdarza się to raz na sto wypadków, więc nie jest zbyt wiarygodny; zależy, czy przyjdzie do niego inspiracja. Pacyfista, wierzy w rozwiązywanie problemów przez zrozumienie drugiej osoby. Silnie relacyjny.

52 lata, ma w Kopalinie rodzinę, żonę i teściów. Bardzo rodzinny, acz też trochę z dystansem, by nikogo nie skrzywdzić. Jest w nim ZERO złośliwości, raczej dobrotliwość i pustka znamienna dla Pryzmatyków.

Czasami z Warmasterem wspólnie rywalizują w pisaniu wierszy, ku wielkiej rozpaczy Whisperwind i radości Kariny, która próbuje zrobić Stały Wieczorek Poezji Na KADEMie przez radiowęzeł.

### Motywacje:

Kopidół bywa dobijający, bo nawet najprostsze rzeczy chce rozpatrywać "od nowa", bo jakiś nieistotny aspekt się zmienił. To też sprawia, że każdemu chce pomóc i każdemu chce dać nowe szanse - bo coś się zmieniło. Naiwny, nieżyciowy, dobrotliwy Kopidół jest magiem, którego łatwo wykorzystać. Unika jakichkolwiek form walki czy konfliktu, acz nie można nazwać go człowiekiem bez energii. Trochę taki szalony naukowiec pragnący czynić dobro.

### Działanie:

### Specjalne:

Kopidół jest troszkę viciniuskim magiem. Nie do końca rozumie, czemu coś rozumie i nie do końca myśli jak zwykły mag. Bez problemu adaptuje się do najdziwniejszych sytuacji i otoczeń; bardzo trudno zmylić go magią zmysłów. Jednocześnie znajduje rzeczy nieznajdowalne przez innych. Ale... czasem znajduje rzeczy, których nie ma - i to się dzieje częściej niż rzadziej.

### Magia:

O Kopidole trzeba jedno powiedzieć - potrafi zaadaptować się do DZIWNYCH warunków. Czy to Spustoszenie, czy to N-wymiarowa czasoprzestrzeń, Kopidół potrafi jakoś poruszać się w tej rzeczywistości bez niszczenia jej zewnętrznych aspektów. Jest często pochodnią KADEMu w sprawach tak anomalnych, że nikt tego nie rozumie. Mistyk.

### Otoczenie:

### Mapa kreacji

brak

### Motto

"Rozwiążemy to - nie zapomnij tylko zdobyć tą informację o migracji żab, młoda damo."

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|chce zrozumieć, co dzieje się na Mare Felix i jak może to zatrzymać (uważa za niekorzystne).|Adaptacja kralotyczna|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180318|który ma mnóstwo nieracjonalnych pomysłów jak zbadać powiązanie między Lasem Stu Kotów a Mare Felix. Cenny analityk odnajdujący dziwność na Fazie Daemonica.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180311|pomaga wskrzesić martwego kralotha, ale tak naprawdę martwi się dziwnymi odczytami w Mare Felix.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170222|jego wiedza posłużyła do stworzenia efemerydy Kuby Urbanka by ów chronił Renatę Souris.|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|10/08/14|10/08/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170125|zmontował i załatwił Hektorowi, Mariannie i Annie dampery pryzmatyczne, by ratować KADEM przed wizjami...|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|którego artefakt astralno-pryzmatyczny eksplodujący na KADEMie spowodował mnóstwo problemów i doprowadził do 'breach'.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|mag-renegat (?) KADEMu, astralika x pryzmat. 52 lata.|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|KADEMowiec podatny na piękne Diakonki.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|który w jakiś sposób zniszczył ten Dwunastościan... który przecież Whisper nazwała 'niezniszczalnym'.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|mag Instytutu Pryzmaturgii dowodzący projektem zniszczenia Dwunastościanu. Raczej z głową w chmurach; jedyny ekspert od Pryzmatu w całym zespole.|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|9|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|5|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|4|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Echo Jakuba Urbanka](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-jakuba-urbanka.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
