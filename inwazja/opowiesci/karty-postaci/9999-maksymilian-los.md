---
layout: inwazja-karta-postaci
categories: profile
title: "Maksymilian Łoś"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150406|współpracujący z Pauliną mag znający się na podstawach magii leczniczej.|[Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
