---
layout: inwazja-karta-postaci
categories: profile
title: "Gabriel Dukat"
---
# {{ page.title }}

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|jest zadowolony z tego, że Sądeczny współpracował z innymi siłami by opanować epidemię. Nie zamierza niczego zmieniać w dowodzeniu regionem.|Wizja Dukata|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|skupia się bardziej na swoim dziecku niż na Rodzinie Dukata; zdecydowanie zdepriorytetyzował to, co dzieje się z jego mafią.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171015|za namową Pauliny usadził Sądecznego, by ten nie rozpętał wojny ze Świecą. Okazuje się, że trochę się odsunął od kierowania mafią.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|uprzejmy jak zawsze. Skupiony bardziej na swoim dziecku niż zwykle i z nieco mniejszym zaangażowaniem skupia się na mafii. Paulinie wypisał glejt i kazał rozmontować jej stację śledzącą.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170326|który z jakiegoś powodu bardzo chciał zdobyć jakieś artefakty Archibalda nie antagonizując go za bardzo.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170325|boss półświatka magów na Mazowszu; z jakiegoś dziwnego powodu ma krucjatę przeciwko klątwożytom.|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|10/02/28|10/03/03|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|3|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Janusz Kocieł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-kociel.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
