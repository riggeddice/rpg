---
layout: inwazja-karta-postaci
categories: profile
factions: "Prokuratura magów, Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Anna Kajak"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Uważaj, co i jak robisz**:
    * _Aspekty_: surowa, niezwykle dokładna, paranoiczna
    * _Opis_: 
* **MET: Dyskretna**:
    * _Aspekty_: karty przy orderach, paranoiczna
    * _Opis_: 

### Umiejętności

* **Policjant**:
    * _Aspekty_: strzelec wyborowy, taktyka, dowody, infiltracja
    * _Opis_: 
* **Okultysta**:
    * _Aspekty_: 
    * _Opis_: 

### Silne i słabe strony:

* **Lustra**:
    * _Aspekty_: 
    * _Opis_: Anna ma zawsze przy sobie co najmniej jedno lustro. I potrafi go używać.		

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Analityk w siłach specjalnych Blakenbauerów. Bardzo kompetentna, dość surowa, świetnie trzyma karty przy orderach. Nie socjalizuje się specjalnie z innymi. Samotna 36-letnia kobieta (na chronologię "Blakenbauerowie x Skorpion". Otacza się lustrami i wierzy, że zapewnią jej bezpieczeństwo. Lekko nienormalna, ale nie przeszkadza być to bardzo kompetentną agentką Hektora. Uważa Hektora za "X-com", osobę walczącą z paranormalnym.
Czasem widzi coś w lustrach. Wierzy w to, co widzi.
Nie mówi niczego co nie jest niezbędne dla sprawy.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160309|która wykryła atak tajemniczych sił na dzieci Patrycji; poprosiła Hektora o autoryzację akcji.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|zatrudniona przez Zajcewa jako detektyw, podsłuchiwała lojalnie dla Blakenbauerów. Gra na dwa fronty.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151001|która zobaczyła, że coś się dzieje na terenie hipermarketu i zgłosiła to Hektorowi by być odesłaną do czegoś nieważnego.|[Plan ujawnienia z Hipernetu](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|10/05/21|10/05/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|próbowała rekrutować Dionizego do planów "The Director", ale zrezygnowała; inicjatorka akcji uratowania dzieci przed Syberią.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150103|jeszcze: Makont, która użyła Pryzmatu Myśli krzywdząc swojego ukochanego brata. W ciężkim stanie psychicznym.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|jeszcze: Makont, policjantka zapętlona z bratem w "czy już rozmawialiśmy z Amelią".|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|jeszcze: Makont, policjantka, której świat się po prostu zawalił przez okultyzm. Ale dzielnie walczy.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|jeszcze: Makont, policjantka (analityk) która spotkała się z najtrudniejszą sprawą w życiu.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150115|która niekoniecznie radzi sobie z walką ze studentami nie czującymi bólu ani strachu.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150210|członek grupy śledczej infiltrująca "Kopalińskich Komandosów" oraz rodzinę przemysłowców Larent (przez Dianę, która robi złe rzeczy).|['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141216|przełożona sił specjalnych Hektora, przeżyła bardzo wielu przełożonych i podwładnych i ma zamiar nie przerywać dobrej passy.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141022|aktualna przełożona sił specjalnych Hektora, która jeszcze żyje i daje radę|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150304|agentka, która przypadkowo zainicjowała całą piramidę kłamstw i osłaniana przez wszystkich wyszła cało. Ma morderczo-sadystyczną reakcję na tajemnicze lusterko.|[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|8|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|7|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|6|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|5|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|4|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|2|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Wiktor Lubaszny](/rpg/inwazja/opowiesci/karty-postaci/9999-wiktor-lubaszny.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Waltrauda Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-waltrauda-werner.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Timor Koral](/rpg/inwazja/opowiesci/karty-postaci/9999-timor-koral.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Przemysław Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-przemyslaw-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Onufry Puzel](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-puzel.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Mikołaj Młot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mlot.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Mariusz Garaż](/rpg/inwazja/opowiesci/karty-postaci/1709-mariusz-garaz.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Maciej Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-kwarc.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karol Kiśnia](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kisnia.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Kamil Rzepa](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-rzepa.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Janusz Wybój](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wyboj.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Janina Strych](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-strych.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Jakub Ryjek](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-ryjek.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Filip Szorak](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-szorak.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Felicja Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Ewa Kroideł](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-kroidel.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Dominik Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Brunon Czerpak](/rpg/inwazja/opowiesci/karty-postaci/9999-brunon-czerpak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bianka Drażyńska](/rpg/inwazja/opowiesci/karty-postaci/9999-bianka-drazynska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Arkadiusz Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Antoni Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-szczesliwiec.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Albert Pireus](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-pireus.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
