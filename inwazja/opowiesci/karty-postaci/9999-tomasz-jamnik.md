---
layout: inwazja-karta-postaci
categories: profile
title: "Tomasz Jamnik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160210|który sprzedał bezużyteczną informację Leonidasowi, za co dostał informację fałszywą nakręcającą wojnę gangów.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
