---
layout: inwazja-karta-postaci
categories: profile
title: "Ilona Maczatek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170312|reporterka opętana przez "Harvester". Postawiona na nogi przez Paulinę, dzięki czemu Paulina miała opcję negocjacji z "Harvesterem"|[Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|10/03/17|10/03/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160227|dziennikarka, jedna z ofiar Harvestera (i ta, która wyniosła Transmiter ze szpitala rozpoczynając cykl problemów).|[Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|10/03/12|10/03/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Rebeka Czomnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-czomnik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Pamela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-pamela-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Oksana Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-oksana-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Maja Stomaniek](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-stomaniek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Filip Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Feliks Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-szczesliwiec.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Eustachy Szipinik](/rpg/inwazja/opowiesci/karty-postaci/9999-eustachy-szipinik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Daniel Stryczek](/rpg/inwazja/opowiesci/karty-postaci/9999-daniel-stryczek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Barbara Zacieszek](/rpg/inwazja/opowiesci/karty-postaci/9999-barbara-zacieszek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
