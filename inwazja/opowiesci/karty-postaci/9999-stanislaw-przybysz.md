---
layout: inwazja-karta-postaci
categories: profile
title: "Stanislaw Przybysz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141123|"DJ Elektron" w "Dziale Plazmowym", konkurent i frenemy Draceny.|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
