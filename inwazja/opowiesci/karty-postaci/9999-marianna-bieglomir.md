---
layout: inwazja-karta-postaci
categories: profile
title: "Marianna Biegłomir"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170816|chciała odzyskać moc, ale zmieniła się w Skażonego potwora. Skończyła zniszczona przez Iliusitiusa.|[Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|10/01/27|10/01/29|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Rafał Warkocz](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-warkocz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Manfred Jarosz](/rpg/inwazja/opowiesci/karty-postaci/9999-manfred-jarosz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
