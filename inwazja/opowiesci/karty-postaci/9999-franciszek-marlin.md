---
layout: inwazja-karta-postaci
categories: profile
title: "Franciszek Marlin"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141110|członek "bieda-mafii" który chcąc zostać prawą ręką bossa współpracuje z Dytą dając jej to, czego Dyta pragnie.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|członek "bieda*mafii" który chce zostać prawą ręką bossa i przejąć władzę nad miastem.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141026|członek "bieda*mafii" skorumpowany skutecznie przez Silgora.|[Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|10/06/12|10/06/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141025|człowiek mający dość sporo na sumieniu który jeden jedyny raz wynajął mieszkanie uczciwie. Sęk w tym, że Silgorowi.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|151013|drobny przestępca, który chce się wybić i próbuje wyjść przed szereg i zdobyć zamtuz dla lokalnego bossa. Biedny.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|4|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Grzegorz Murzecki](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-murzecki.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
