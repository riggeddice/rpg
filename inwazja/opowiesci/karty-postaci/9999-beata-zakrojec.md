---
layout: inwazja-karta-postaci
categories: profile
title: "Beata Zakrojec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150411|aktualna dziewczyna Marcelina która w tajemniczych okolicznościach zniknęła i której nie po* maga prawidłowo maść Marcelina na koszmary.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
