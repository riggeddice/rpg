---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
owner: "raynor"
title: "Marek Śmietanka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **MET: Ryzykant**:
    * _Aspekty_: walczak, arogant
    * _Opis_: 
	
### Umiejętności

* **Terminus**:
    * _Aspekty_: pojedynki magiczne
    * _Opis_: 
* **Najemnik**: 
    * _Aspekty_: mordobicie 
    * _Opis_: 
* **Manipulator**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: łamanie zabezpieczeń, przechwytywanie informacji
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **lokalni terminusi**:
    * _Aspekty_: 
    * _Opis_: 
* **typy spod ciemnej gwiazdy z okolicy**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Marek Śmietanka – Porywczy awanturnik, zadufany w sobie. Jest terminusem, ale działa często na własną rękę podnajmując się na boku jako najemnik. Oprócz walki magicznej, lubuje się również w tradycyjnej walce na pięści. Satysfakcję daje mu sama walka i rywalizacja z drugą osoba, nie krzywdzenie jej w sposób nieodwracalny.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|uznanie w Świecy (Trocin) za akcję z pająkiem i odpuszczenie Lilii|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160901|terminus wrobiony w sprawę (niewinny), zachowujący się dobrze wobec ludzi bo zdominowany przez małą cybergothkę - ale uratował ludzi i uwięził pajęczaka|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|10/06/18|10/06/20|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Piotr Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Lilia Kałdun](/rpg/inwazja/opowiesci/karty-postaci/9999-lilia-kaldun.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Karol Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Bartłomiej Czyrawiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bartlomiej-czyrawiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
