---
layout: inwazja-karta-postaci
categories: profile
title: "Stefania Kołek"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|okazuje się, że była już uzależniona od Hlangoglormo (kralotha)|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|przetransferowana poza Mazowsze przez siły uczniów Świecy|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|uzyskuje niewielką próbkę czarnotrufli Esuriit. Nie do końca jeszcze wie co to.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|wystarczająco zabezpieczyła siebie i Eliksir Aerinus u Sądecznego odnośnie czarnotrufli.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Kontynuować biznes budowania czarnotrufli i uniknąć wykrycia; doukryć tą sprawę.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Zbadać czym są te dziwne czarnotrufle (Esuriit). Jakie mają własności. Czym się różnią.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180112|hodowała spokojnie czarnotrufle dla Eliksiru Aerinus i dla Hlangohlormo, aż uczniowie terminusów Świecy ją wyeliminowali.|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|szybko wymyśla alibi, szuka oczkodzika by się go pozbyć i nie wchodzi w konflikt ze Świecą. Coś kręci w okolicy. Zdaniem Kingi, główna podejrzana w sprawie walk norek.|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|w Eliksirze Aerinus, najpewniej współpracuje z Dukatem; wrobiła Hektora w coś, co może być powiązane z perypetiami Katii.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
