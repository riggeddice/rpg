---
layout: inwazja-karta-postaci
categories: profile
title: "Karol Kiśnia"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150304|brukarz, który nie trafił do kliniki Słonecznik po "ciężkim zatruciu"; zamiast tego miał "wizytę domową" od "mafii".|[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Janina Strych](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-strych.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Albert Pireus](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-pireus.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
