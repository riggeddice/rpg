---
layout: inwazja-karta-postaci
categories: profile
title: "Elgieskład"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170323|spedycyjna firma zajmująca się rozwożeniem produktów w Kropwi Dzikiej (Mazowsze); pracownicy ulegają Skażeniu. Było tam dziwne lustro.|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|10/02/20|10/02/24|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-kuraszewicz.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Piotr Pyszny](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-pyszny.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Maciej Brzydal](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-brzydal.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
