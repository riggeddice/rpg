---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Jagoda Kozak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Zostanę podrzędną reporterką podrzędnej gałęzi Paktu**:
    * _Aspekty_: wielki temat pozwoli mi się wybić, napiszę wspaniały reportaż,   
    * _Opis_: 
* **FIL: Nie ma żadnych wydarzeń paranormalnych, to ONI!**:
    * _Aspekty_: teorie wcale-nie-spiskowe, muszę przetrwać żeby o tym opowiedzieć,  
    * _Opis_: 
	
### Umiejętności

* **Reporter**:
    * _Aspekty_: reporter śledczy, to wszystko jest CZYJAŚ akcja, zbieranie leadów, składanie faktów do kupy, zacieranie swoich śladów, plotki, tropienie, wyszukiwanie śladów
    * _Opis_: 
	
### Silne i słabe strony:

* **Ekstremistyczna sceptyczka**:
    * _Aspekty_: to NIE jest możliwe; to tak nie działa, 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* reporterka

### Znam

* **Haker Tadzio**:
    * _Aspekty_: gimnazjalista, nerd-hacker, włam na serwery Beaty Szydło 
    * _Opis_: 


### Mam

* **Sprzęt podsłuchowy**:
    * _Aspekty_: 
    * _Opis_: 


# Opis


### Koncept


### Motto

"ONI nie mogą uciszyć wolności słowa!"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|sceptyczna reporterka Paktu. Ma kontakty z lokalsami; uratowała Żaklinę rzucając w Wyssańca Esuriit jego ukochanym psem.|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
