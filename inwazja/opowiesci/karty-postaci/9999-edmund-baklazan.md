---
layout: inwazja-karta-postaci
categories: profile
title: "Edmund Bakłażan"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140928|kopaliński lekarz, przyjaciel Pauliny.|[Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|10/05/12|10/05/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
