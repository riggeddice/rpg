---
layout: inwazja-karta-postaci
categories: profile
title: "Wojciech Czapiek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150704|mąż siostry Tamary Muszkiet, ukrywa się w domu Tamary.|[Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|10/05/11|10/05/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Roksana Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-roksana-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Liliana Sowińska](/rpg/inwazja/opowiesci/karty-postaci/9999-liliana-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Hieronim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-hieronim-maus.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
