---
layout: inwazja-karta-postaci
categories: profile
title: "Paweł Robercik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170501|przełożony magów Na Świeczniku w Czarnoskale. Dość rozsądny i zachowawczy; skutecznie zarządza finansowo "Na Świeczniku".|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|10/01/09|10/01/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sonia Stein](/rpg/inwazja/opowiesci/karty-postaci/9999-sonia-stein.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Natalia Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Michał Ostrowski](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-ostrowski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Jerzy Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Jan Adamski](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-adamski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Grzegorz Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Grażyna Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Antonina Wysocka](/rpg/inwazja/opowiesci/karty-postaci/9999-antonina-wysocka.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
