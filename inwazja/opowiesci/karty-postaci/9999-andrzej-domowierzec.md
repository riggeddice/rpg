---
layout: inwazja-karta-postaci
categories: profile
title: "Andrzej Domowierzec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141220|37, martwy agent ubezpieczeniowy, ktorego obowiązki przejęła Małgorzata.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141218|37, agent ubezpieczeniowy który chciał portret boga.|[Portret Boga](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|10/02/07|10/02/08|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
