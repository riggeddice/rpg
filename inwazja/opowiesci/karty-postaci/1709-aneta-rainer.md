---
layout: inwazja-karta-postaci
categories: profile
factions: "Lojaliści Srebrnej Świecy, Srebrna Świeca"
type: "NPC"
title: "Aneta Rainer"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Najlepszą formą walki jest skłonić wroga do ataku na umocnione pozycje**:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: Oddany Świecy**:
    * _Aspekty_: szczerze wierzy w Świecę i konieczność jej istnienia, "zgłaszam się na ochotnika", zasady i reguły > ludzie i uczucia
    * _Opis_: 
* **MET: Cicha**:
    * _Aspekty_: 
    * _Opis_: lepiej być nieznaną i nierozpoznawalną

### Umiejętności

* **Terminus**:
    * _Aspekty_: defensywy, tarcze, osłony
    * _Opis_: 
* **Taktyk**:
    * _Aspekty_: pułapki
    * _Opis_: 
* **Architekt**:
    * _Aspekty_: umocnienia, fortyfikacje, architektura budynków, dekoracja wnętrz, maskowanie przeznaczenia czegoś, pułapki, budownictwo,
    * _Opis_: 	
	

### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia materii**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **Skuteczna terminuska Świecy**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **Wyposażenie bojowe terminusa**:
    * _Aspekty_: 
    * _Opis_: 


# Opis

Aneta Rainer is a 37 years old terminus. She specializes in outmaneuvered fighting and in fortifying. If you want to defend a particular location you usually send her. She strongly believes in the meaning of the Silver Candle and she is perceived to be incorruptible. Although not a bureaucrat, she values and respects the rules and avoids breaking them, putting processes and rules over people.

She tries to avoid collateral damage whenever possible and is known to abort a fight simply because the casualties would be unacceptable. She is not a well-known terminus, however. She is a doer not a talker. She silently upholds the ideals of the Candle in the best way she can.

A strict, quiet person who has seen slightly too much but copes with it finding meaning in regulations and routine work. She has a strong backbone and is known to refuse an order if deemed wrong. She has no political ambitions and only wants to serve the guild the best way she can. Usually volunteers to those tasks no one really wants to take.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170226|odzyskująca Kompleks Centralny i informująca Andreę o problemach z kontrolowaniem Amandy Diakon. Nic dziwnego...|[Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|10/08/18|10/08/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170122|która korzystając z okazji uderzyła by odzyskać Kopalin i ustabilizować Kompleks Centralny. Z pomocą Millennium i Andrei - wygrała.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|która łączy ze sobą Jolantę Sowińską i Andreę. Chce uruchomić autonomiczną broń z czasów Wojen Bogów.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|dowodząca spokojnie tym co może w Kopalinie i komunikująca się z Andreą dając wszystkie potrzebne jej informacje. Solidna i można na niej polegać.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|broniąca budynku z przekaźnikami energii zasilającego Kompleks centralny (który Amanda chce zaatakować). Nikt nie chce tego atakować...|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|próbująca utrzymać sieć hipernetową i przekaźniki by zachować jakąkolwiek kohezję Srebrnej Świecy na tym terenie.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|37-letnia terminuska Świecy, lojalistka, przejęła tymczasowo dowodzenie przeciw Spustoszeniu i wykazała się niezłomną, cichą lojalnością.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|3|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|1|[161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Abelard Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-abelard-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
