---
layout: inwazja-karta-postaci
categories: profile
title: "Echo Jakuba Urbanka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170222|stworzona efemeryda mająca odzwierciedlać charakter i potęgę Kuby; chwilowo ma chronić Renatę Souris (i pilnować jej zachowania).|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|10/08/14|10/08/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
