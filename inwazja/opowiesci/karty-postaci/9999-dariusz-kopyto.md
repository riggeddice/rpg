---
layout: inwazja-karta-postaci
categories: profile
title: "Dariusz Kopyto"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140625|który jako jedyny z agentów Agresta został odnaleziony. Nieprzytomny, trafił do Rezydencji Blakenbauerów. Najpewniej stał się agentem Inwazji.|[Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|10/01/19|10/01/20|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140618|lojalny agent Agresta zamknięty z nim w biurze; zaginął w eksplozji.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140604|jeden z terminusów pod dowództwem Agresta; wraz z Mają Kos próbował przechwycić Karolinę Maus i został odparty przez Krystalię (a potem prawnie przez Andreę)|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140121|terminus seryjny morderca który chciał zabić Sophistię (i przypadkiem uratował jej życie).|[Zniknięcie Sophistii](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140227|którego dawno w przeszłości bardzo ciężko skrzywdził gang do którego należała Sophistia. Mściwy i pamiętliwy; umie poruszać się w prawie.|[Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Wojciech Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-tecznia.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
