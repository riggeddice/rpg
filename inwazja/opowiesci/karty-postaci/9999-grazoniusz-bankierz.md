---
layout: inwazja-karta-postaci
categories: profile
title: "Grazoniusz Bankierz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|wyleciał doszczętnie z łask Newerji Bankierz i z orbity Eweliny Bankierz|Rezydentka Krukowa|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|straszny ubytek pozycji; mag bojowy pokonany przez maga-rolnika... wypada z dworu Eweliny Bankierz|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170723|jak zwykle poprztykał się z Kają. Dołączył do sił Prosperjusza. Ustabilizowany i spokojny; nie jest już problemem dla nikogo w okolicy.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|może pracować z Prosperjuszem lub Apoloniuszem. Przeprosił się z Pauliną. Na razie pomaga Prosperjuszowi w tle.|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170614|chciał odzyskać twarz w podły sposób, dzięki czemu machinacje Eweliny zniszczyły jego pozycję na jej dworze. Chwilowo stracił wszystko co było dlań cenne.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170518|rozpuszczony arystokrata; mag bojowy i katalista; dał się zepchnąć ze schodów magowi rolniczemu. Wypada z łask Eweliny. Złośliwy, acz wobec Pauliny zachowuje się nienagannie.|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Karol Komnat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-komnat.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Janusz Wosiciel](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wosiciel.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|
|[Aleksander Czykomar](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-czykomar.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
