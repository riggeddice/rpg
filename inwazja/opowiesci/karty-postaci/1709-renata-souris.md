---
layout: inwazja-karta-postaci
categories: profile
factions: "Souris"
type: "NPC"
title: "Renata Souris"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Odzyskam autonomię**:
    * _Aspekty_: 
    * _Opis_:  Renata nie lubi być komukolwiek / czemukolwiek podwładna. Ona chce mieć pełną autonomię i kontrolę. Do tego dąży.
* **FIL: Jestem koszmarem wrogów**: 
    * _Aspekty_: znieczulica
    * _Opis_:  Renata ma tendencje do wzbudzania grozy wśród wszystkich; to jedyny znany jej sposób, by nikt nie stanął nigdy przeciw niej. Renata ma przytępioną empatię, łagodność... jest zwolenniczką tortur, bezwzględności i pragmatyczności.
* **MET: **:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ: Jeszcze zostanę królową**:
    * _Aspekty_: duma, arogancja, honorowość, spłaca wszystkie długi
    * _Opis_:  Renata nie przeżyła dobrze całej tej sytuacji; nadal ma zamiar wrócić do jakiejś wysokiej pozycji. To jest jej główny cel. Renata kieruje się dumą, arogancją i tym, czy ktoś próbuje jej się przypochlebić. Bez odpowiedniego szacunku i płaszczenia niechętnie pomoże. Pomożesz jej, ona pomoże Tobie. Staniesz przeciw niej, ona stanie przeciw Tobie. Pamiętliwa i nie zapomina, co jej zrobiono.
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Wiedza Karradraela**:
    * _Aspekty_: eks-seiras Maus
    * _Opis_: Renata NADAL pamięta większość Mausów i część wiedzy Karradraela. +1 do testów wiedzy, gdzie to aplikuje. Za czasów Karradraela, mogła aktywnie wykorzystywać krwawe rytuały. Już nie - jest narażona na chorobę defilera jak każdy. kiedyś zintegrowana z Karradraelem, kontrolowała życie wszystkich Mausów. Nadal potrafi rządzić i zarządzać, acz straciła to co miała.
* **Demonolog**: 
    * _Aspekty_: wybitna wiedza demonologiczna, tylko teoria
    * _Opis_: kiedyś jedna z najlepszych demonolożek, dzisiaj straciła moc magiczną w tym obszarze. Nadal ma wybitną wiedzę.
* *Katalista***:
    * _Aspekty_: szeroka wiedza katalistyczna, tylko teoria
    * _Opis_: kiedyś świetna katalistka, dziś nie ma już mocy magicznej w tym obszarze. Nadal się dobrze zna, ale...
* **Przywódca**: 
    * _Aspekty_: wzbudzanie strachu, rządy terroru
    * _Opis_: doświadczony i charyzmatyczny przywódca... specjalizujący się w rządzeniu strachem.
* **Polityk**:
    * _Aspekty_: eks-seiras Maus, wzbudzanie strachu, przekonywująca retoryka, ukrywanie własnych myśli
    * _Opis_: połączenie wiedzy Karradraela, jej skomplikowanej pozycji oraz doświadczeń w rządzeniu Mausami sprawiają, że umie manipulować, rozgrywać i maskować swoje myśli. bardzo trudno poznać, o czym Renata myśli i co chce ukryć. Renata powie tylko to, co chce powiedzieć. oprócz zastraszania, Renata potrafi bardzo przekonywująco opowiadać o korzyściach z robienia tego, czego ona sobie życzy.
* **Mag Krwi**: 
    * _Aspekty_: tylko teoria, doświadczony kat, zadawanie bólu
    * _Opis_: doskonała w torturowaniu i zadawaniu bólu. Świetnie potrafi przesłuchiwać i rozpoznać, co jest prawdą a co próbą uniknięcia bólu.
  
### Silne i słabe strony:

* **Sprzężony umysł**: 
    * _Aspekty_: 
    * _Opis_: Renata praktycznie jest niewrażliwa na jakiekolwiek formy kontroli magicznej i jest bardzo trudna do złamania. +10 do testów w tym obszarze. 

## Magia
 
### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: iluzje
    * _Opis_: umiejętność tworzenia omamów i nieprawdziwych sygnałów w rzeczywistości; wzrok i słuch.
* **Biomancja**: 
    * _Aspekty_: 
    * _Opis_:  biomancja ograniczona do magii skupiającej się na regeneracji, działaniu ludzkiego ciała i uśmierzaniu bólu.
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: magia skupiająca się na umyśle, myśleniu i kontroli innych osób.
* **Magia transportu**:
    * _Aspekty_: kineza
    * _Opis_: magia skupiająca się na aplikacji czystej siły przez energię magiczną. Jak telekineza.
### Zaklęcia statyczne

* **Rozkaz**:
    * _Aspekty_: 
    * _Opis_: Renata może wydać magiczny Rozkaz celowi. Ma wszelkie bonusy jak za zaklęcie, acz zaklęciem nie jest. Działa jak atak mentalny. 
* **Terror Supremus**: 
    * _Aspekty_: 
    * _Opis_: Przywołuje najstraszliwszy koszmar ofiary i stara się oblec go dla ofiary w rzeczywistość. Wymaga krwi Renaty. Bardzo potężny; wymaga 60 sekund.
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **przerażająca seiras Maus**:
    * _Aspekty_: 
    * _Opis_: sława Renaty jest szeroka i powszechna. Zła sława.

# Opis

Kiedyś Renata Maus, dziś Renata Souris. Po transfuzji straciła dostęp do demonologii i katalizy.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Jeżeli nie jesteś skłonna zejść do piekła, by zrealizować swe marzenia - jesteś niegodna posiadania jakichkolwiek marzeń."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|od tej pory jest sprzężona mindlinkiem z Henrykiem Siwieckim w kobiecej formie|Powrót Karradraela|
|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|uważa, że jest winna życie Silurii i Hektorowi. Nie jest pewna, czy to korzyść dla nich czy powinna ich za to zabić...|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170920|która odkąd ma Henryka (Hanię?) jest skłonna do współpracy - ma po co żyć. Ma dobre propozycje jak zniszczyć napastników... z operą.|[Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|10/09/03|10/09/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170405|ostrzegła o niebezpiecznych eksperymentach szalonego Karradraela, za co oczekiwała na możliwość eksperymentowania ze swoją magią.|[Chyba wolelibyśmy kartony...](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|10/08/18|10/08/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170222|nie walczy z przeznaczeniem; skupia się na przetrwaniu rodu Maus i na skuszeniu magów KADEMu by zrobili to, co ona chce. Uratowała Karolinę i Milenę.|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|10/08/14|10/08/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Sławek Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-blyszczyk.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Maja Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-blyszczyk.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Echo Jakuba Urbanka](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-jakuba-urbanka.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
