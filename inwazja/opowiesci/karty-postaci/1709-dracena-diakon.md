---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Dracena Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Wieczna wojna i zniewolenie**:
    * _Aspekty_: stracę kontrolę nad sobą, będę zniewolona, świat pogrąży się w konfliktach, czysty drapieżny kapitalizm, silniejsi zdominują słabszych, wieczna eskalacja zbrojeń, dąży do pacyfizmu
    * _Opis_: Dracena wiecznie walczy ze swoją naturą wiły i Spustoszenia. Boi się utracić nad sobą kontrolę, boi się nadejścia świata okrutnego gdzie człowiek pożera człowieka, gdzie powstają coraz to lepsze bronie. Pragnie temu zapobiec - dąży do rozbrojenia, pacyfizmu i współpracy silniejszych ze słabszymi
* **FIL: Każda władza prowadzi do cierpienia**:
    * _Aspekty_: radykalna anarchistka, walka z władzą i formami kontroli, zwalczanie przemocy, zwalczanie bólu i bezradności, "jeśli ktoś cierpi to to moja sprawa", antykapitalistka, każdemu wedle potrzeb
    * _Opis_: Główny aspekt charakteru Draceny sprzed przemian; ta czarodziejka nie chce niszczyć. Ona chce budować świat. Ona chce, by ludzie przebywali w towarzystwie piękna, świetnej muzyki, by mogli się zrelaksować, by byli otoczeni działającymi i ładnymi przedmiotami. By świat przekształcił się w utopię gdzie wszyscy są bezpieczni i nie muszą walczyć o wszystko.
* **ŚR: Artystyczna Spustoszona Wiła**: 
    * _Aspekty_: ekstatyczna wiłka i mordercza Spustoszona, unikanie bezpośredniego konfliktu, maksymalizacja piękna, szerzenie właściwej kultury, "moje ciało moją bronią"
    * _Opis_: Krzyżówka dwóch komponentów Wzoru dała Dracenie impulsywność i zarówno skłonności do niszczenia i przekształcania jak i skłonności do zachowania stanu naturalnego i bachanaliów. Sprawia to, że Dracena bywa nieco zaskakująca - przeskakuje od trybu łagodnej bachantki w tryb morderczego terminatora.

### Umiejętności

* **Cybergoth**:
    * _Aspekty_: kultura cybergoth, DJ, alternatywne stroje, zarządzanie imprezami, taniec i muzyka, artystka zaangażowana
    * _Opis_: Na sztandary hasła Draceny! Fit In or Fuck Off! Pomagajmy sobie nawzajem, fajne stroje i unikanie wszelkich form kontroli!! Cyberpunk lives!!!
* **Remaker**: 
    * _Aspekty_: scrapper, elektronika użytkowa, sprzęt DJa, majsterkowanie, budowanie strojów, remiksowanie
    * _Opis_: Dracena charakteryzuje się ogromnymi umiejętnościami adaptacji mechanizmów i organicznych; specjalizuje się w badaniu życia i mechanizmów - oraz w futuryzmie. Dzięki swoim mocom magicznym i znajomości natury technologii i biologii, potrafi doskonale adaptować istniejący sprzęt do czegoś pasującego do jej celów.
* **Nie-ludzki drapieżnik**:
    * _Aspekty_: wzbudzanie strachu, ars amandi, uwodzenie, drapieżne polowanie na ofiarę, hipnotyczne wręcz przyciąganie
    * _Opis_: Dracena po transformacji stała się czymś... obcym. Pięknym, dzikim, ale i strasznym. Coś między drapieżnikiem, koszmarem a cudowną kochanką... a czasem chętnie pobawi się swoją ofiarą jak kot...
    
### Silne i słabe strony:

* **Ciało Spustoszonej Wiły**:
    * _Aspekty_: ciało cyborga, silniejsza, szybsza i wytrzymalsza, popęd wiły, uroda wiły i Diakonki, nienaturalny wygląd, wrażliwa na porażenie prądem i technomancję
    * _Opis_: Wspomagana dwoma konfliktującymi aspektami, Dracena jest silniejsza, wytrzymalsza, potężniejsza i bardziej niebezpieczna niż ktokolwiek może sobie wyobrazić. Piękna i filigranowa cybergothka. Ale - nie wszyscy wiedzą, że to nie jest makijaż a jej prawdziwa twarz. Nie wszyscy wiedzą, że te obwody na jej ciele to nie tatuaż. I lepiej, by nie przechodziła przez wykrywacz metalu.
* **Komputron**: 
    * _Aspekty_: kontrolowane Spustoszanie, przewidywalna, cyber-integracja z mechanizmami, czasem mechanizmy reagują same na jej obecność, dominacja Spustoszenia
    * _Opis_: We krwi Draceny płynie Spustoszenie. Mechanizmy reagują na jej obecność. Czerpie wiedzę ze Spustoszenia i potrafi Spustoszyć tymczasowo cele, czy to organiczne czy nieorganiczne by słuchały jej rozkazów. Myśli błyskawicznie szybko. Ale za to jest dość przewidywalna i w jej obecności mechanizmy czasem zachowują się... dziwnie.
* **Nienaturalna magia**:
    * _Aspekty_: bardzo wykrywalna magia, bardzo charakterystyczna magia, magia na innym kanale energii niż zwykle
    * _Opis_: Dracena operuje na innym typie magii niż przeciętnie. To sprawia, że zaklęcia działają na nią nieco słabiej i że jej magia jest bardzo wykrywalna i charakterystyczna.

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: cyborgizacja, magia imprezowa, augmentacja, muzyka / soniczne 
    * _Opis_: 
* **Biomancja**: 
    * _Aspekty_: cyborgizacja, magia imprezowa, augmentacja
    * _Opis_: 
* **Magia zmysłów**:
    * _Aspekty_: magia imprezowa, maskowanie i ukrywanie, hipnotyczne światła i dźwięki
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* DJ, niewielkie zarobki, stara się o rozpoznanie
* Mechanik i naprawy różne, też nieduże zarobki

### Znam

* **Miłośnicy cybergoth**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_:
* ****:
    * _Aspekty_: 
    * _Opis_:

### Mam

* **Spustoszony Umysł**:
    * _Aspekty_: wiedza Spustoszenia, błyskawiczne obliczenia, adaptacja zmysłów i przetwarzania
    * _Opis_: Wspomagany cybernetycznie umysł Draceny potrafi przywołać wiedzę Spustoszenia. Co więcej, potrafi określić najlepsze możliwe działania na bazie jej podstawowych zmysłów... lub poradzić sobie z nadmiarowymi zmysłami.
* ****:
    * _Aspekty_: 
    * _Opis_:
* ****:
    * _Aspekty_: 
    * _Opis_:

# Opis

29 lat. Dracena jest Diakonką, córką Draconisa Diakona (terminusa starej szkoły Millennium). Lifeshaperka i katalistka. Aha, do tego jest też cybergothem i DJem. Jest osobą amoralną, ale szanuje altruizm i odruchy pochodzące z serca. Nie tylko nie storpeduje, ale spróbuje pomóc.

Kiedyś z rodu Blakenbauer, jedna z trzech sióstr (Najada, Antygona i Dracena). Wpierw przekształcona w Diakonkę pod kontrolą Draconisa. Potem nabyła aspekt Spustoszonej Wiły. Po drodze miała epizod uzależnienia od magii krwi gdy eksperyment Rafaela nieco zawiódł. Paulina rozpaczliwie naprawiła ją i doprowadziła do w miarę stabilnej struktury (Diakonka 8, Wiła 5, Spustoszenie 5, Cybergothka 8, Weapon 1).

Chwilowo częściowo nieludzka już Dracena łączy moce viciniusa z mocami maga i w pewien sposób wypowiedziała wojnę całemu światu. Nie jest skłonna do niszczenia czy krzywdzenia; to w końcu Dracena. Ale jest bezwzględna i ma zamiar strachem i muzyką (tak, dobrze przeczytaliście) wyciąć z tej rzeczywistości fragment spójny z jej przekonaniami. Ona jako strażniczka, nie jako overlord. A im mniej magów mających upodobania do rozkazywania innym jest w to zaangażowanych, tym lepiej. Dla nich.

### Koncept

TODO

### Motto

"?"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|zdobyła pracę w Nowej Melodii jako DJ i marketingowiec|Wizja Dukata|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|została tymczasową Uzależnioną od Pauliny.|Prawdziwa natura Draceny|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|została ustabilizowana jako Diakonka 8, Wiła 5, Spustoszenie 5, Cybergothka 8, Weapon 1|Prawdziwa natura Draceny|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|stan psychozy po destabilizacji post-kralotycznej|Prawdziwa natura Draceny|
|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|+ głód magii krwi (słaby)|Prawdziwa natura Draceny|
|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|uzyskała dominujący aspekt Wiły|Prawdziwa natura Draceny|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|tymczasowo silna destabilizacja|Prawdziwa natura Draceny|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|Ma w planach zdecydowanie częściej współpracować z Marią. Choćby po to, by Paulina przestała robić głupoty.|Wizja Dukata|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|próbuje się jakoś ustatkować i znaleźć gdzieś pracę. Próbuje się zreintegrować do społeczeństwa, acz w swojej nowej formie Spustoszonej Wiły|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171105|która zrobiła sobie w Magitrowni Histogram małe "gwiazdko miłości". Powstrzymała dezinhibicyjną Oliwię przed wysadzeniem magitrowni.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|dzięki jej szybkiej reakcji portalisko nie wybuchło - wysadziła transformator. Potem dobierała się do Daniela. Osiągnęła większy sukces niż to na co liczyła.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|przechodzi jej L4 i organizuje grupę oporu przeciwko Sądecznemu - a dokładniej, grupę wolnych muzyków, artystów itp. Ma nawet bazę...|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171022|odparła atak elementala w gabinecie Pauliny, kończąc ciężko ranna przez wrażliwość na prąd. Paulina ją wyleczyła. Dracena ma kosę z Filipem; uważa elementale za świadome.|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|11/09/12|11/09/14|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171015|która odkryła zbezczeszczony element Legionu, wpadła w kłopoty z Tamarą i w końcu niechętnie obiecała zachować sekret tego co się tu działo.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|skonfliktowana z Tamarą za przeszłość; prychnęła na Daniela za brak kręgosłupa i wysługiwanie się mafii. Dalej pracuje w magitrowni.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|wylana z "Nowej Melodii", znalazła pracę w Magitrowni Histogram jako osoba od infrastruktury. Zintegrowała się eksperymentalnie z magitrownią - z powodzeniem. Kontroluje swoje nowe moce.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171218|komputron i ochrona Pauliny; doskonale wyszukuje informacje współpracując z Marią. Jak już się nie kłócą to świetnie współpracują.|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|11/08/28|11/08/31|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|która posłużyła jako deviator dla Kai; dodatkowo, aplikowała o pracę w Nowej Melodii i ją dostała.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170212|zniszczyła efemerydy, Spustoszyła biednego człowieka, składała drony i ogólnie ma wybuchy buntu przeciwko temu, co się z nią stało.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161009|zirytowana brakiem autonomii i próbująca uratować wszystkich... mając przy okazji zabawę. Nie wie, jak bardzo bywa irytująca.|['Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html)|10/07/14|10/07/16|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161002|niechętnie posłuszna, acz to akceptuje. Włada straszliwą magią katalityczno-erotyczną i wykorzystuje swoje ciało jako podstawa cyborgizacji.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160918|w końcu stabilna. Cybergothka, Diakonka, Spustoszona Wiła. Zmierzyła się z przeszłością i wyszła z uzależnieniem od Pauliny - na własne życzenie.|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160910|która rozpaczliwie walczy o swoją osobowość, aczkolwiek... nie wyszło. Została zdestabilizowana by próbować jeszcze raz - lepiej. |[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|10/06/27|10/06/29|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160907|o której się dowiadujemy, że była krwi Blakenbauer i że Draconis próbował ją ratować; chcąc chronić Paulinę zakosztowała uzależnienia Krwi.|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|10/06/25|10/06/26|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160904|kłębek chaosu, w której obudził się już aspekt wiły. Powiedziała Paulinie o swej przeszłości.|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|10/06/21|10/06/24|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160901|zdestabilizowana i depresyjna, ale i przerażająca terminusa na śmierć fanatyczka ratowania każdego * człowieka. |[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|10/06/18|10/06/20|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141110|stereotypowo rozumiana Diakonka, choć ciągle walczy (nieskutecznie) z kontrolą Jana i Dyty.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|żywy dowód na to, że osoba o dobrym sercu dwa razy traci.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141026|Zosia*Samosia, która naprawdę nie chce nikomu robić krzywdy... aż do samego końca.|[Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|10/06/12|10/06/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141025|bardzo rozkojarzona, zakochana cybergothka walcząca rozpaczliwie o swoją przyszłość.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141019|buntownicza lifeshaperka pragnąca pomóc ludziom.|[Lekarka widziała cybergothkę](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|10/06/08|10/06/09|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150718|która wspierała siostrę na dobre i na złe (tym razem 'na złe').|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170725|próbowała uciekać, ale bez sukcesu. Narkotyzowana i unieszkodliwiona min. szczeniaczkiem. Uratowana i puszczona wolno przez Zespół. Zdziwiona.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150329|(podobno) twórca planu by użyć Spustoszenia do zniszczenia Szlachty (frakcji SŚ). Też, przykład jak NIE atakować (zwłaszcza mając przewagę zaskoczenia)|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|której imię z dziwnych powodów pojawiło się w kontekście Spustoszenia w Kotach; podobno kręciła z Salazarem Bankierzem.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|główna, chyba niewinna podejrzana|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|cybergothka próbująca trzymać wszystkich maksymalnie na odległość.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|która zmieniła Ikę by pomóc Hektorowi (i Ice) i wynegocjowała od Hektora pewne ustępstwa i potencjalną współpracę.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140618|która została sprowadzona przez Rezydencję do kontynuowania pracy nad projektem Moriath w miejsce Margaret.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140611|sprowadzona przez Rezydencję do walki z Inwazją; przekształciła Tatianę by ta się w Hektorze zakochała przez zmianę jej wzoru magicznego.|[Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|10/01/15|10/01/16|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|22|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|14|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|6|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|6|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|4|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|4|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|4|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|4|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|4|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|4|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|3|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|3|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|3|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|3|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|3|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Michał Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-jesiotr.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|2|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Bartłomiej Czyrawiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bartlomiej-czyrawiec.html)|2|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Szymon Łokciak](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-lokciak.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Piotr Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Marek Śmietanka](/rpg/inwazja/opowiesci/karty-postaci/1709-marek-smietanka.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Lilia Kałdun](/rpg/inwazja/opowiesci/karty-postaci/9999-lilia-kaldun.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Karol Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Grzegorz Murzecki](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-murzecki.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
