---
layout: inwazja-karta-postaci
categories: profile
title: "Czarny Kamaz 314"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140408|pierwszy w historii Kamaz który łże jak Mistrz Gry.|[Czarny Kamaz](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140408](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html)|
