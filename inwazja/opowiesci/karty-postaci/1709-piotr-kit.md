---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "foczek"
title: "Piotr Kit"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Uznależnienie**:
    * _Aspekty_: narkoman, podatny na używki, dobra kondycja
    * _Opis_: choć ćpun, biegać daje radę
* **FIL: **: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: **:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ: Przekazanie prawdy, której nikt nie widzi**:
    * _Aspekty_: chytry, czuły (na los osób "niecodziennych"), chce znaleźć tych co myślą jak on, chce odnaleźć odpowiednią muzę
    * _Opis_: Namalowanie (i sprzedanie) rzeczywistości taką jaka jest
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Manipulator**: 
    * _Aspekty_: oszukiwanie, empatia
    * _Opis_:
* **Paser**:
    * _Aspekty_: wiedza o półświatku, szczur ulicy
    * _Opis_:
* **Życie w świecie ludzi**:
    * _Aspekty_: człowiek chłonący świat, ciężko go zagiąć
    * _Opis_: Obszerna wiedza (głównie o życiu)
* **Artysta**:
    * _Aspekty_: malarz, historia sztuki
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 
* **Magia zmysłów**: 
    * _Aspekty_: 
    * _Opis_: 
* **Technomancja**:
    * _Aspekty_: infomancja
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **Półświatek**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Niezrozumiały przez świat malarz, nie umie sprzedać własnych dzieł.
Jak ma potrzebę zarobić "podkrada" obrazy od uznanych malarzy, a następnie sprawia, żeby zapomnieli o swoim autorstwie. Umie przeżyć. Nie lubi kieratu: praca, drzewo, dzieci...
Szuka na różne sposoby celu, prawdy i alternatyw. Co jest później, w którą stronę dążyć.
Jak wyzwolić ciekawe osoby od klatki.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Ludzie popełniają błędy"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|zobowiązany do zostania kustoszem zrujnowanej opery w Czapkowiku|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170914|maluje obrazy. Próbuje uniknąć zrobienia czegokolwiek; wyrolował terminusa i udało mu się ominąć problem własnego mieszkania i zwłok w ścianie.|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|10/08/31|10/09/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170830|emitujący aurę kapłana Iliusitiusa nieświadomie, dogaduje się ze swoją Operiatrix, niechętnie maluje pod przymusem i ciągle ktoś mu włazi do JEGO domu. Planuje przenieść się do opery.|[Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|10/08/29|10/08/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170823|wieczny pijak, dobrze się bawił, zarobił sporo kasy od Olgi i najlepiej na tym wyszedł. Olga mu kupiła alkohol... A nic nie chciał robić... no i kilka obrazów. Ma wizje które się sprawdzają.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170808|(Foczek), malarz, który ratował modelki by dać im szansę na rozwinięcie swego potencjału. Zobowiązał się do zostania kustoszem starej opery pełniącej rolę paintballa.|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|10/02/17|10/02/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|3|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Żaneta Kroniacz](/rpg/inwazja/opowiesci/karty-postaci/9999-zaneta-kroniacz.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Zofia Łaziarak](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-laziarak.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jessica Czułmik](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-czulmik.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
