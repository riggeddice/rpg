---
layout: inwazja-karta-postaci
categories: profile
title: "Henryk Mordżyn"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171003|kiedyś zajmował się trzodą u Myszeczki a został pielęgniarzem zwierząt u Natalii. Nadal ma wiedzę odnośnie tego jak to się u Myszeczki robi.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
