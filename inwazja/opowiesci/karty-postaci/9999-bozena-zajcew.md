---
layout: inwazja-karta-postaci
categories: profile
title: "Bożena Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140208|czarodziejka frakcji Współpracy która zajmuje się handlem ludźmi i przypadkowo porwała... Tatianę.|[Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140409|zaatakowana przez Nowy Porządek Swietłany handlarka ludźmi i magami. |[Czwarta frakcja Zajcewów](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|140213|która zostaje ujawniona jako dostawca ludzi i magów do Rezydencji Blakenbauerów...|[Pułapka na Edwina](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|10/01/01|10/01/02|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|3|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|2|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Zdzisław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislaw-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Wojciech Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-tecznia.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Maciej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Kaspian Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-bankierz.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
