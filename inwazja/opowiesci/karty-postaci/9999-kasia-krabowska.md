---
layout: inwazja-karta-postaci
categories: profile
title: "Kasia Krabowska"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180421|dotknięta przez Mimika Pasożytniczego; tańcząca dziewczyna w bieli. Mimik wzmocnił jej miłość do tańca i spróbowania zrobić "coś więcej".|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|10/11/24|10/11/26|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|180418|pedantka i miłośniczka netflixa; ostatnio apatyczna i wszystko jej jedno. Dotknięta przez Operiatrix i Jaszczur nie zdążył jej zregenerować. Po sesji jej lepiej.|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|2|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html), [180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Paweł Szlezg](/rpg/inwazja/opowiesci/karty-postaci/1803-pawel-szlezg.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Artur Wiążczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-wiazczak.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
