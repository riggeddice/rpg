---
layout: inwazja-karta-postaci
categories: profile
title: "Wiesław Rekin"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151229|mistrz 'Faim de loup', posiadacz dojo w Przodku, kultysta 'głosiciel światła' nie bojący się wyjść poza prawo. Drapieżnik.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160106|kultysta Kłów Kaina i wojownik Faim de Loup na wolności; śmiertelnie niebezpieczny acz ścigany.|[...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151230|mistrz sztuk walki, który nie ma prawa pracować z młodzieżą. Też: wywalił Zajcewa na śmietnik.|[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Ochrona Kobra Przodek](/rpg/inwazja/opowiesci/karty-postaci/9999-ochrona-kobra-przodek.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
