---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Oliwia Aurinus"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Spadkobierczyni Weinerów i Bractwa Pary**:
    * _Aspekty_: skłonna do ostrożności, promuje bezpieczeństwo i ochronę wszystkich, promuje pamięć o przeszłości, dumna spadkobierczyni historii, wspiera cały region, zawsze zachowywać się godnie, nie dopuszczę, żeby TO stało się znowu!
    * _Opis_: Na tym terenie znajdował się potężny i interesujący ród Weinerów. Niestety, wyginęli w eksperymencie, który stworzył Harvester. Wcześniej na tym terenie znajdowało się Bractwo Pary i z jedną z członkiń tej gildii spokrewniona była Oliwia. Niestety, wyginęli. To sprawia, że Oliwia ma nieco bardziej konserwatywne i prospołeczne podejście niż jej poprzednicy.
* **MRZ: Kontrola i opanowanie efemerydy**:
    * _Aspekty_: promuje ograniczenie dostępu do efemerydy i jej monitorowanie, promuje wygaszanie niekontrolowanych ognisk magii
    * _Opis_: Oliwia całe życie miała definiowane między innymi przez Efemerydę. Od najmłodszych lat, gdy śpiewała Efemerydzie aż do czasów, gdy czerpała z niej energię. Dopiero po latach zrozumiała, ile miała szczęścia. Jej aktualnym celem życiowym jest kontrola i opanowanie Efemerydy tak, by nie była niebezpieczna. By nie powtórzyła się sytuacja z Bractwem Pary i jej przyjaciółmi z Weinerów.


### Umiejętności

* **Majsterkowicz**:
    * _Aspekty_: urządzenia parowe, drobne naprawy, pierwsza pomoc, środki ochrony osobistej, stare urządzenia parowe, sabotaż defensywny
    * _Opis_: 
* **Biznesmen**: 
    * _Aspekty_: dyrektor fabryki awianów, znana w społeczności, zręczna negocjatorka, pociąganie ludzi za sobą, stawianie na swoim, szukanie okazji, biznes opłacalny dla wszystkich stron, konstrukcja awianów
    * _Opis_: 
* **Historyk**:
    * _Aspekty_: ciekawostki i sekrety okolicy, projekty Weinerów, stare urządzenia parowe
    * _Opis_: 
* **Śpiewak**:
    * _Aspekty_: amatorka, przkazywanie uczuć, wzbudzanie emocji, kształtowanie pryzmatu
    * _Opis_:
    
### Silne i słabe strony:

* **pragnienie ochrony innych**:
    * _Aspekty_: ogromna determinacja, 
    * _Opis_: utraciwszy tyle ważnych osób w swoim życiu, Oliwia zrobi bardzo wiele, aby nigdy więcej się to nie powtórzyło
* **moja efemeryda**: 
    * _Aspekty_: obustronna podatność pomiędzy efemerydą i Oliwią, wyczuwanie stanu efemerydy, nikt nie będzie wykorzystywał efemerydy 
    * _Opis_: od dziecka utrzymująca bliski związek z efemerydą,
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: sabotaż defensywny, awianny eksperymentalne, "to też może być boombą!"
    * _Opis_: 
* **Magia zmysłów**: 
    * _Aspekty_: manipulacja pryzmatem, dźwiękowe obrazy,
* **Magia mentalna**:
    * _Aspekty_:  manipulacja pryzmatem, usuwanie niepożądanych myśli, 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* brak

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171105|zainfekowana klątwożytem dezinhibicyjnym; pacjent zero. Uszkodziła magitrownię i powstrzymana przez Dracenę.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|zdiagnozowała problem z rozżarzoną efemerydą Senesgradzką i z pomocą Krzysztofa ją wygasiła. Też: doprowadziła do rozproszenia Oktawii. Szefowa montowni awianów i strażniczka efemerydy.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
