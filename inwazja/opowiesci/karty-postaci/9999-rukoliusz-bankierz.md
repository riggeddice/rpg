---
layout: inwazja-karta-postaci
categories: profile
title: "Rukoliusz Bankierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170118|który zupełnie nie rozumie ludzistycznego podejścia Hektora. Ukrywa informacje o działaniach magów Świecy przed Hektorem. Lubi Marcelina.|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|Świeca, terminus Szlachty przepraszający Silurię i obiecujący przeprosiny od Świecy dla KADEMu. Osłonił Urszulę zanim ta wydała istnienie Szlachty.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
