---
layout: inwazja-karta-postaci
categories: profile
title: "Żanna Szczypiorek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|131008|(68 l): żona Augustyna, niewolnica i miłość Augustyna, wierna i niezłomna do samego końca. KIA.|['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|(68 l) uważająca się za demona i fanatycznie wierząca w prymat Kościoła nad magami. Lojalna Augustynowi (mężowi) nade wszystko.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130506|(68 l) żona Augustyna która wywołała trochę chaosu chcąc wymienić obraz Andromedy na inny.|[Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|10/02/01|10/02/02|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130503|(68 l) żona Augustyna i wyraźnie czarodziejka, choć ukrywa to bardziej niż przeciętny mag.|[Renowacja obrazu Andromedy](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|10/01/30|10/01/31|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|2|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Izabela Kruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kruczek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Antoni Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
