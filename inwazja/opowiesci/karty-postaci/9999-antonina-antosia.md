---
layout: inwazja-karta-postaci
categories: profile
title: "Antonina Antosia"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170712|przyjaciółka wszystkich ale nie małżów.|[Ucieczka Małży](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|10/01/03|10/01/04|[Nie przydzielone](/rpg/inwazja/opowiesci/konspekty/kampania-nie-przydzielone.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefan Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-myszeczka.html)|1|[170712](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|
|[Klaudia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-klaudia-weiner.html)|1|[170712](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|
|[Izolda Gofer](/rpg/inwazja/opowiesci/karty-postaci/9999-izolda-gofer.html)|1|[170712](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|
|[Gerwazy Protazy](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-protazy.html)|1|[170712](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|
|[Antonio Wulgaris](/rpg/inwazja/opowiesci/karty-postaci/9999-antonio-wulgaris.html)|1|[170712](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html)|
