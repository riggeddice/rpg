---
layout: inwazja-karta-postaci
categories: profile
title: "Ernest Kokoszka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160914|kamerdyner von Blutwurstów który nie zdążył zatrzymać "fałszywej Karoliny". Przeżył i dalej opiekuje się młodą Kariną.|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Anatol Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-anatol-weiner.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
