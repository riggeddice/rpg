---
layout: inwazja-karta-postaci
categories: profile
title: "Janusz Wosiciel"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170614|sąsiad Pauliny i straszny plotkarz; Paulina przekazała mu, że pomyliła pestycyd z nawozem w nocy i dlatego zabiła swój ogródek, odwracając uwagę od Diany.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
