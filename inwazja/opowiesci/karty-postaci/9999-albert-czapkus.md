---
layout: inwazja-karta-postaci
categories: profile
title: "Albert Czapkuś"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161110|tien, docenia pomoc i znajomość Luny. Tymczasowy handler Henryka Siwieckiego (bo hipernet niezbyt działa). Introligator. Kotolubny.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
