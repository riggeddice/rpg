---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "tomek_p"
title: "August Paszkwil"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Trauma spożywcza**:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: Zrozumieć niezrozumiałe**:
    * _Aspekty_: 
    * _Opis_: 
	
### Umiejętności

* **Kucharz**:
    * _Aspekty_: 
    * _Opis_: 
* **Hazardzista**:
    * _Aspekty_: szybkie podejmowanie decyzji, negocjacje
    * _Opis_: 
	
### Silne i słabe strony:

* **Nieszczęsne medium**:
    * _Aspekty_: wykrywanie magii, zjawiska paranormalne mnie przytłaczają
    * _Opis_: 

## Otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zarobki



### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

 
* **Bardzo ostre noże kuchenne**
    * _Aspekty_: 
    * _Opis_: 

# Opis


### Koncept


### Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|Wojtek Paszkwil nie miał rodziny, więc August go adoptował i zmienił go w swego padawana w ufologii i racjonalizacji.|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|straumatyzowany kucharz nie z Półdary. Zainteresowany znalezieniem świni i gadaniem z harcerzami, zabił Krzysztofa (Wyssańca Esuriit).|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
