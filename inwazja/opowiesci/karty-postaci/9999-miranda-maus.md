---
layout: inwazja-karta-postaci
categories: profile
title: "Miranda Maus"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|wysunęła się na czoło "Wolnych Mausów", daje im inspirację i nadzieję w niewoli Karradraela (Abelarda).|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180311|nieobecna, na którą poluje Świeca i która - podobno - potrafi oprzeć się wpływowi Karradraela.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
