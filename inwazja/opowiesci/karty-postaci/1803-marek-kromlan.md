---
layout: inwazja-karta-postaci
categories: profile
factions: "Rusznica Świecy, Spero Candela, Srebrna Świeca, Szlachta"
type: "NPC"
owner: "public"
title: "Marek Kromlan"
---
# {{ page.title }}

## Koncept

Iza Łaniewska + Gabriel Durindal + Kanaria + miłośnik sztuki

Artyleryjski terminus-ideowiec kochający sztukę i nienawidzący niewolnictwa i kralothów.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                                    |
|-------------------|--------------------------------------------|
| Indywidualne      | promocja sztuki; eksploracja różnorodności |
| Społeczne         | autonomia; praworządność; antyniewolnictwo |
| Wartości          | kojenie; kolekcjonowanie; współdziałanie   |

#### Szczególnie

| Co chce by się działo?                                                          | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                        |
|---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| promuj różnorodną sztukę; poznawaj jak najwięcej różnorodnych dzieł             | celem sztuki jest zarobić jak najwięcej; najważniejsze jest to, co przyjemne i daje radość              |
| bądź ceniony w Świecy, odzyskaj pozycję; odbuduj Rusznicę z ruin                | nie mam za co przepraszać; trzeba pójść dalej i nie patrzeć w przeszłość                                |
| zwalczaj wszelkie formy zniewolenia; zatrzymaj corruptorów i deviatorów         | jeśli ofiara CHCE kontroli to jest to OK; silniejszy ma więcej praw; akceptuj różnorodność gildii       |
| wolność i samostanowienie to odpowiedzialność; promuj działania zgodne z prawem | wykonywanie rozkazów zwalnia od myślenia; prawo jest ważniejsze niż moralność                           |
| ukoj cierpienie przez zniszczenie; daj każdemu szansę - ludzie się zmieniają    | niszczenie dla niszczenia jest fajne; przez konflikty ludzie są silniejsi; skreśl tych, co zrobili błąd |
| szukaj towarzystwa; zachowuj się elegancko i uprzejmie; metodycznie, z planem   | unikaj innych, są udręką; zachowuj się tak, by maksymalizować korzyści; impulsywnie, za intuicją        |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria        | Aspekty                               |
|------------------|---------------------------------------|
| Terminus         | snajper; rewolwerowiec; dywersant     |
| Artylerzysta     | kanonier; kontrola terenu; taktyk     |
| Mecenas sztuki   | sztuka; marketingowiec; dyplomata     |

#### Manewry

| Jakie działania wykonuje?                                                             | Czym osiąga sukces?                                                          |
|---------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| egzekucja snajperką; atak z zaskoczenia; zniknięcie z oczu; dewastacja bronią ciężką  | odwrócenie uwagi; wyprowadzenie w pole; działanie z ukrycia; pułapki         |
| spowalnianie; obezwładnianie; kierowanie przeciwnika; unikanie ran                    | przewaga taktyczna; przygotowanie terenu; kontrola terenu; znajomość wroga   |
| wycena; wysłanie (tajnego?) sygnału; promocja swej osoby; wabienie celu               | dzieła sztuki; doświadczenie marketingowe                                    |
| robienie dobrego wrażenia; dyplomatyczne unikanie tematu                              | obycie ze stuką; retoryka; bycie wiecznym ochotnikiem do czegoś innego       |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                                             |
|---------------------|-----------------------------------------------------------------------------------------------------|
| Magia Zmysłów       | odwracanie uwagi; ukrywanie terenu; maskowanie snajpera; wzmocnienie sztuki                         |
| Technomancja        | tworzenie pułapek; wysadzanie rzeczy; zdalna kontrola broni; wspomaganie broni; badanie przeciwnika |
| Magia Transportu    | ranteleportacja; przenoszenie pułapek |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| zagubienie przeciwnika; przerażanie przeciwnika; niewykrywalność Kromlana | częste teleportacje; iluzje i maskowania; pułapki i kontrola terenu               |
| lokalizowanie słabych punktów; atak w słabe punkty; straszne spowalnianie | tele-pułapki; skanowanie techno; zdalne wysadzanie                                |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Ceremonialna broń Rusznicy         | pistolety; snajperka; przynależność do Rusznicy; bliskie jego sercu  |
| Działo artyleryjskie Gravis        | dalekosiężna artyleria; dewastująca siła ognia                       |
| Spero Candela - Szlachta ale dobra | czempion SpC; niechętny tej roli                                     |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
| przełamanie fortyfikacji; niszczenie armii; przerażanie siłą ognia; dewastacja  | Gravis                                                        |

## Opis

### Ogólnie

### Motywacje:

### Działanie:

Marek nie jest najlepszym terminusem w walce bezpośredniej; nie dorównuje np. Izie Łaniewskiej. Za to jeśli nie jest sam i uda się mu odwrócić uwagę przeciwnika, jest niezwykle celnym i niebezpiecznym egzekutorem. Alternatywnie - jeśli uda mu się zadziałać jak snajperowi

Jedna z najsilniejszych stron Marka. Mistrz wplątywania przeciwników w kłopoty, wymanewrowania ich i przekształcania otoczenia na ich niekorzyść. Tam, gdzie inni magowie Rusznicy preferowali działanie bezpośrednie - on wolał iluzje i pułapki. 

Marek zawsze chciał być promotorem i mecenasem sztuki nade wszystko. Elegancki i ponadczasowy, uwielbia samotność i obcowanie ze sztuką, zwłaszcza tą mniej znaną. Nie rozpatruje tego z perspektywy rasy czy bogactwa - tylko wrażeń. Przypadkowo, sprawia to, że jest jednym z najbardziej obytych i eleganckich terminusów w okolicy.

### Specjalne:

### Magia:

### Otoczenie:

Broń Rusznicy - dwa ceremonialne pistolety magitechowe, świetnej klasy. Karabin snajperski, magitechowy. Z tą bronią wiąże się pewna tradycja i pewne wspomnienia - Marek ich nie odrzuci i nie zbezcześci. Są piękne, dobrze utrzymane i pozwalają Markowi czasem przypomnieć sobie, że jest coś, czemu warto nadal walczyć.

### Mapa kreacji

![Mapa postaci](Materials/180399/marek_kromlan.png)

### Motto

"Mimo całego tego zła, trzeba walczyć... a ukojenie znajdziemy w ogniu Gravisa"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|polubił Eleę Maus - jest słonna dużo poświęcić dla bezpieczeństwa ludzi, stanęła na linii ognia Krystalii|Adaptacja kralotyczna|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|pogorszył sobie reputację w Rzecznej Chacie, bo wydarł się na Silurię o Infensę.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180526|próbował powstrzymać Krystalię łagodnie i nie wyszło. Wezwał wsparcie z Prokuratury Magów i miał już dość siły.|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|10/12/17|10/12/19|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180402|ostrzeżony przez Silurię, że Drugi Kraloth nie jest kralothem. Był, ale nie jest. Koordynują swoje działania by nie doszło do tragedii.|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180318|zalatany jak cholera, konstruminusy mu zabierają... oddaje KADEMowi co się da by to wszystko działało. Poluje na Mirandę Maus. Ma urazę do Silurii.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180311|niezbyt lubiący kogokolwiek terminus; Silurii wyjątkowo nie lubi za Infensę. Sprzyja KADEMowi, ze wszystkich rzeczy. Zrezygnowany.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|kompetentny terminus który bardziej chce ratować magów i ludzi niż angażować się w walkę z Silurią. Szukał kralotha; Siluria znalazła go pierwsza.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160810|terminus porządkowy o dobrym sercu, który pilnuje by nic się nie stało złego w Technoparku, acz nie dopuszcza Aliny do Adriana Murarza.|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|5|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|5|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|4|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Rafał Drętwoń](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-dretwon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
