---
layout: inwazja-karta-postaci
categories: profile
title: "Jerzy Karmelik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151229|który spanikował, bo bał się, że mu spalą bratanka. Bardzo rodzinny, czego się by po nim NIKT nie spodziewał po Marzenie.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151223|przyłapany przez doktor Grażynę Tuloz na zabawie w "mastera" dla Marzeny; Paulina i Marzena uratowały jego małżeństwo.|[..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151230|straszny plotkarz ze skłonnościami do przesady. Też: po* maga Paulinie by być częścią Czegoś Większego. |[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|"ho ho ho master" w "związku" z Marzeną wywołanym przez to, co jej zrobił Adam Diakon. Zupełnie nie ma pojęcia, że żeruje na nieszczęściu dziewczyny. Chce dobrze.|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151101|najbardziej plotkujący chyba lekarz-lowelas na oddziale. Przypadkowy właściciel Null Fielda.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151013|lekarz, w którego gabinecie (szafce) znajduje się NullField. Flirciarz i kobieciarz. Podrywa Paulinę na kawę. Straszny gaduła.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|6|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|5|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|4|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|4|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|4|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|3|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|2|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|1|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
