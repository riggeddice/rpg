---
layout: inwazja-karta-postaci
categories: profile
title: "Bzizma Stlitlitlix"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170823|połączył się z większością leylinów pod Mszanką, ale stracił zaawansowane sensory. Wie już, że nie widzi swojego przeciwnika...|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170607|połączył się z większością leylinów pod Mszanką, ale stracił zaawansowane sensory. Wie już, że nie widzi swojego przeciwnika...|[Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|10/08/23|10/08/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170531|zagubiony autowar; uszkodzony; próbuje ufortyfikować teren i przygotować się do dalszej walki z... czymkolwiek trzeba zniszczyć.|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|10/08/20|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Wirgiliusz Kartofel](/rpg/inwazja/opowiesci/karty-postaci/9999-wirgiliusz-kartofel.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Stanisław Bazyliszek](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-bazyliszek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Rafał Łopnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-lopnik.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Milena Pacan](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-pacan.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Gerwazy Śmiałek](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-smialek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
