---
layout: inwazja-karta-postaci
categories: profile
title: "Krzysztof Kruczolis"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170412|ręka Skubnego, który chciał jak najlepiej zrobić sztukę a przez działanie ludzi Dukata reputacja Skubnego ucierpiała|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
