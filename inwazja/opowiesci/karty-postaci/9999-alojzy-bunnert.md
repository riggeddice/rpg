---
layout: inwazja-karta-postaci
categories: profile
title: "Alojzy Bunnert"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180419|łowca jaszczurów i łamacz koncertów. Spił asystenta burmistrz Czapkowika, potem zerwał koncert Estrellas Infernal. A przedtem jeszcze Alfreda złapał.|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|10/11/17|10/11/19|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mateusz Podgardle](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-podgardle.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Maciek Drzeworóz](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-drzeworoz.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Aleksandra Kurządek](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-kurzadek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
