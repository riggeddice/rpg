---
layout: inwazja-karta-postaci
categories: profile
title: "Mateusz Kozociej"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150427|dyrektor gimnazjum, któremu naprawdę bardzo szkoda zmarłego nauczyciela historii.|[Kult zaleskiego Anioła](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|10/01/03|10/01/04|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Marek Ossoliński](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-ossolinski.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Aneta Kosicz](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-kosicz.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
