---
layout: inwazja-karta-postaci
categories: profile
title: "Rebeka Czomnik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160227|dyrektor szpitala (drona zwykła) która umożliwia Harvesterowi swobodne działanie w szpitalu.|[Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|10/03/12|10/03/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Maja Stomaniek](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-stomaniek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Ilona Maczatek](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-maczatek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Feliks Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-szczesliwiec.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Eustachy Szipinik](/rpg/inwazja/opowiesci/karty-postaci/9999-eustachy-szipinik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Daniel Stryczek](/rpg/inwazja/opowiesci/karty-postaci/9999-daniel-stryczek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Barbara Zacieszek](/rpg/inwazja/opowiesci/karty-postaci/9999-barbara-zacieszek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
