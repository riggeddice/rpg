---
layout: inwazja-karta-postaci
categories: profile
title: "Łucja Rowicz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170321|praktykantka pełna ambicji która chce zatrzymać świnię i która aresztowała dla odmiany ludzi, których warto odizolować od audytorki ;-).|[Tajemnica podłożonej świni](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170228|praktykantka miastowa, policjantka z kijem w tyłku, dwa razy aresztowała Franciszka dwa dni pod rząd. Józef jest podłamany. Chciał krzepkiego parobka.|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|2|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|2|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|2|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Świnex Jan Halina Kulig SC](/rpg/inwazja/opowiesci/karty-postaci/9999-swinex-jan-halina-kulig-sc.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Władysław Lusowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-wladyslaw-lusowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Wojtek Leśniak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojtek-lesniak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Wieprzpol](/rpg/inwazja/opowiesci/karty-postaci/9999-wieprzpol.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Michał Brukarz](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-brukarz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Krzysztof Cygan](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-cygan.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kinga Grzybnia](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-grzybnia.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Karol Kamrat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kamrat.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Kamil Czapczak](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-czapczak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Gaweł Wieciszek](/rpg/inwazja/opowiesci/karty-postaci/9999-gawel-wieciszek.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Carlos Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-carlos-myszeczka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Alicja Makatka](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-makatka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
