---
layout: inwazja-karta-postaci
categories: profile
title: "Andrzej Farnolis"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|odbudował pod terenem dawnego Pentacyklu... coś. I zaraził to miejsce klątwożytem dezinhibicyjnym|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|odbudowuje potęgę Bractwa Pary korzystając z nieobecności Sądecznego (Żółw)|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170523|kiedyś mag a dzisiaj duch, który opętał konstruminusa; z gildii Bractwa Pary. Wyegzorcyzmowany zanim zdołał Coś Zrobić z magitechem medycznym.|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Felicja Szampierz](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-szampierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
