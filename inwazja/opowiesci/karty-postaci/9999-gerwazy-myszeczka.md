---
layout: inwazja-karta-postaci
categories: profile
title: "Gerwazy Myszeczka"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|przysługa u Silurii, może ściągnąć coś od Millennium przy okazji jedzenia dla viciniusów|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160713|terminus, który zginął próbując wycofać Zajcewów i Eleę ("chronił" więzienie w tym czasie). KIA.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160404|który porwał Dianę Weiner dla Wiktora i na to miejsce wprowadził Proxy Mausów.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160403|który obronił Silurię przed zaaranżowanym przez Silurię atakiem, za co został przez Wiktora odesłany na "tajną misję".|[Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160723|"domyślił" się roli Silurii i ją aprobuje widząc Dagmarę jako alternatywę. Chce dowodzić osłoną więzienia.|[Czyj Jest Kompleks Centralny](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|10/06/22|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160327|terminus, który dla ochrony Szlachty i Wiktora chciał zmienić Silurii wzór przez bardzo dobry plan. Niestety, Agrest i Ozydiusz mu przeszkodzili.|[Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|10/06/14|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160326|40-letni terminus chroniący Wiktora Sowińskiego. Nie jest ze Szlachty. Wygląda na to, że nie ma słabych punktów. Na razie.|[Siluria na salonach Szlachty](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|6|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|5|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|5|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|5|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Antoni Kurzamyśl](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-kurzamysl.html)|2|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Lenart Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-lenart-myszeczka.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Laura Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
