---
layout: inwazja-karta-postaci
categories: profile
title: "Tomasz Kapelusz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171024|wujek Mai Weiner, który nagrywał na kamerę dziwną burzę nad farmą krystalitów. Próbował ją uratować przed "oszalałym" awianem - ale też został portwany. Odkażony przez Paulinę.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
