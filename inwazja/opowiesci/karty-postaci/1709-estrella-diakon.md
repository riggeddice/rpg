---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Estrella Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Odkrywca tajemnic**:
    * _Aspekty_: 
    * _Opis_:  Estrella lubi wszystko wiedzieć, wszystkiego się dowiedzieć - by podjąć w przyszłości decyzję, co z tym zrobić.
* **FIL: Strażnik tajemnic**:
    * _Aspekty_: skryta
    * _Opis_: Pewne sekrety nie powinny nigdy wyjść na jaw. Nie wszyscy powinni wszystko wiedzieć i rozumieć. Estrella unika pokazywania swojej natury i poglądów; do momentu aż kogoś polubi. Wtedy się lekko rozluźnia, ale nadal trzyma karty przy orderach.
* **BÓL: Walczy z cierpieniem**:
    * _Aspekty_: obrońca pokrzywdzonych
    * _Opis_: Nieważne czy ludzi, magów, czy zwierząt - ból i cierpienie są błędem, który należy wyplenić. Estrella zawsze próbuje chronić stronę pokrzywdzoną, często nawet działając dość ryzykownie. W idealnym świecie nie musi...
* **MET: Pomocna**:
    * _Aspekty_: drobiazgowa
    * _Opis_: Estrella rzadko zachowuje się frywolnie czy robi to, co chce. Zwykle lubi być traktowana jako profesjonalistka a nie osoba.
* **MET: Profesjonalistka**:
    * _Aspekty_: 
    * _Opis_: Estrella rzadko zachowuje się frywolnie czy robi to, co chce. Zwykle lubi być traktowana jako profesjonalistka a nie osoba.

### Umiejętności

* **Terminus**:
    * _Aspekty_: terminus wsparcia, medyk polowy, odwracanie uwagi
    * _Opis_: Estrella specjalizuje się w dywersji, wytrącaniu przeciwnika oraz w otwieraniu innym okoliczności do działania.
* **Botanik**:
    * _Aspekty_: rośliny magiczne, zauroczenie pięknem
    * _Opis_: Przede wszystkim kwiatów, ale Estrella potrafi wstrząsnąć i wytrącić z równowagi prostym przedstawieniem najpiękniejszych cech świata.
* **Bard**:
    * _Aspekty_: aktorka, zauroczenie pięknem, pdnoszenie morale
    * _Opis_: wysłucha, doradzi, opowie historię, skojarzy problem z czymś innym, podniesie morale... taki "klej" zespołu. Zwłaszcza w połączeniu z magią Zmysłów, Estrella jest taka jak chce dla każdego. Świetnie gra i prezentuje te swoje cechy, które chce. Przede wszystkim kwiatów, ale Estrella potrafi wstrząsnąć i wytrącić z równowagi prostym przedstawieniem najpiękniejszych cech świata.
* **Imprezowicz**:
    * _Aspekty_: 
    * _Opis_: na każdej imprezie, Estrella czuje się najlepiej. Dosłownie 'gwiazda towarzystwa'.
* **Szpieg**:
    * _Aspekty_: znajdowanie sekretów,  ukrywanie informacji
    * _Opis_: czasami trzeba gdzieś się wkraść czy dostać, coś podłożyć czy zwinąć... dowiedzieć się czegoś... kto podejrzewałby damę z ładnymi kwiatkami?  Estrella jest szczególnie wyczulona na rzeczy, które "nie pasują". Gdy coś jest ukryte, tajemnicze... najpewniej to zauważy.
* **Archiwista**:
    * _Aspekty_: fałszowanie dokumentów,
    * _Opis_: gdzie znajduje się prawda? W dokumentach i archiwach. Kto kontroluje zapisy, kontroluje dokumenty. Estrella specjalizuje się w tym, by kluczowe informacje nigdy nie wyszły na jaw i by niektóre pytania nigdy nie były zadane.
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: pełne iluzje, bojowa magia roślin
    * _Opis_: manipulacja zmysłami i sygnałami; wzrok, słuch, zapach, temperatura... pełne iluzje.
* **Biomancja**:
    * _Aspekty_: magia lecznicza, botanika magiczna, bojowa magia roślin
    * _Opis_: przede wszystkim magia redukująca cierpienie; painkillery i anestezja. Też: stabilizacja, by cel nie umarł. Poza tym, klasyczna szkoła walki Diakonów; jak zazwyczaj, połączona Zmysły + Biomancja. 
* **Technomancja**:
    * _Aspekty_: infomancja
    * _Opis_: manipulacja informacjami, bazami dokumentów i danymi. Biblioteki lub internet.

### Zaklęcia statyczne

* **Ukryty Lishaor**:
    * _Aspekty_: ukryte, drapieżne, odporne, zaskoczenie.
    * _Opis_: Zmysły + Biomancja + Botanika. Tworzy grupę drapieżnych roślin (Lishaor), po czym nakłada wielokrotne iluzje na teren; to sprawia, że dany teren jest "zaminowany" Lishaorami. Lishaory nie atakują maga ani osób desygnowanych przez tego maga.
* **Zarodniki Korzaste**:
    * _Aspekty_: unieruchomienie
    * _Opis_: Biomancja. Tworzy chmurę zarodników, które oblepiają cel, po czym zaczynają porastać korą. Większość celów zostaje całkowicie unieruchomionych.
* **Proste Upojenie**:
    * _Aspekty_: 
    * _Opis_: Biomancja. Cel po prostu zostaje za-alkoholizowany do poziomu nieprzytomności. Działa tylko na biotyp ludzi, magów i klad ludzki viciniusów. Barbarzyńsko proste i skuteczne.
* **Nieskończona rozkosz**:
    * _Aspekty_: 
    * _Opis_: Biomancja + Zmysły. Głównie stosowane przez Estrellę do anestezji lub wyłączenia cywila z akcji, ale może też być traktowane jako czar bojowy.
		
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Terminuska Świecy, standardowy żołd. Dodatkowa premia za dyskretne rozwiązywanie problemów z Maskaradą.

### Znam

* **Przyjaciółka i kochanka Netherii**:
    * _Aspekty_: 
    * _Opis_: Neti jest doskonale we wszystkim poinformowana. Dzięki Estrelli ;-). Te dwie silnie współpracują: źródło wieczy i jej cichy miecz.
* **Lubiana terminuska Świecy**:
    * _Aspekty_: 
    * _Opis_: Estrella jest lubianą terminuską Świecy. Nie ma opinii 'dobrej terminuski', ale 'warto mieć ją w zespole i pomóc'. Trochę maskotka.
* **Opinia strażniczki Maskarady**:
    * _Aspekty_: 
    * _Opis_: Estrella jest szczególnie znana z tego, że chroni Maskaradę nawet w sytuacjach... dziwnych. Za to w sumie najbardziej ceniona.
	
### Mam
* **Sprzęt strażnika Maskarady**:
    * _Aspekty_: 
    * _Opis_: Artefakty i narzędzia do usuwania pamięci i puryfikacji terenu.
* **Sprzęt bojowy terminusa Świecy**:
    * _Aspekty_: 
    * _Opis_: Loadout bojowy. Lekki pancerz, pole siłowe i antymagiczne, różdżka i dobry nóż.
* **Zarodniki i ziarna rzadkich roślin**:
    * _Aspekty_: 
    * _Opis_: Zawsze ma przy sobie różne rośliny, które może wykorzystać w walce... i nie tylko.

# Opis

Urocza i cicha terminuska która nie unika dobrej zabawy i potrafi zrobić ze wszystkiego broń, która w rzeczywistości zna bardzo dużo sekretów bardzo dużej ilości osób. Nie jest może najlepsza w akcjach bezpoośrednich, ale stanowi świetny klej drużynowy. Bardziej agentka, którą wyśle się na imprezę niż maszyna masowego zniszczenia polująca na potwory.

Ku swojemu utrapieniu, nie jest najcelniejszym czy najodporniejszym z terminusów; lepiej radzi sobie na trzeciej linii przygotowując wsparcie dla bardziej bojowych magów. Jednak gdy dochodzi do osłabienia morale w zespole lub pojawiają się animozje, Estrella jest nieoceniona. Nazywana czasem "Gwiazdeczką" przez kolegów z zespołu.
Bardzo bliska światowi ludzi, co nie jest typowe dla terminusów. Ma bardzo dużo empatii i choroba terminusa jej raczej nie grozi.

### Koncept

Terminuska drugoliniowa, która skupia się na ukrywaniu sekretów i kojeniu cierpienia.

### Motto

"Spokojnie, Twojego pieska już nie będzie bolało... kim ja jestem? A, ja tylko przechodziłam... skup się lepiej na Burku ;-)."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|jakkolwiek złapała pierwszego larghartysa, nie złapała drugiego i przez to ucierpiał Maurycy Maus. Bittersweet; bardziej bitter.|Adaptacja kralotyczna|
|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|ma tymczasowo podniesione uprawnienia jako terminus przez Andreę; jest użyteczna i można na niej polegać, więc...|Powrót Karradraela|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|szacun za zdobycie i przechwycenie Luksji. Też... a co zrobiła z Artefaktem Apokalipsy Psinoskiej?!|Córka Lucyfera|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180411|właściwa terminuska na właściwym miejscu. Wysłuchała cywili i pomogła im rozwiązać problem kralothspawna (larghartysa). Niestety, przegapiła drugiego.|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180425|główny łapacz siedmiu pozostałych śledzi...|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|10/11/28|10/11/30|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|180310|nie mówiąca nic tajnego, ale pomocna by Siluria mogła zorientować się co się dzieje. Mała mapa terenu i stanu politycznego Świecy.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170823|przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170607|przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.|[Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|10/08/23|10/08/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170531|która wkręciła Blakenbauerów w próbę pokonania autowara. Nie miała nikogo innego a alternatywą jest zniszczenie wioski i śmierć wielu ludzi.|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|10/08/20|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|kontakt komunikacyjny między Amandą a Silurią; terminuska Lojalistów Ozydiusza w Kopalinie tymczasowo pod dowodzeniem Anety Rainer|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|która zapewniła Marcelinowi rozwiązania taktyczne... których nie posłuchał. Dostała od Wandy nagranie o Blakenbauerach (death hand).|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150826|w częściowym negliżu, podleczona, lekko złośliwa wobec Ozydiusza która dostała szlaban; nie może opuszczać kompleksu Świecy.|[Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|10/05/25|10/05/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|wciąż ranna terminuska z silnie ograniczonymi kanałami * magicznymi (leczy się); pielęgnuje stary Cmentarz Wiązowy. Ukochana Netherii.|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|której Spustoszenie zniszczyło pamięć i która zgodziła się na wypalenie kanałów i cierpienie by sobie przypomnieć.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150604|terminuska Srebrnej Świecy, która zdecydowała się wystąpić z oskarżeniem wobec Izy by Spustoszenie nie zostało zamiecione pod dywan.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|terminuska i przyjaciółka Netherii. Srebrna Świeca, ale jest tu prywatnie. Chciała chronić przed sytuację, ale została Spustoszona.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170628|zmusiła Henryka do uratowania satanisty i udawała żonę gangstera by zdobyć ciężarówkę. Ma własne plany wobec Luksji.|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|10/02/16|10/02/17|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170620|zastawiła pułapkę, wciągnęła Luksję, rozmontowała jej Krwawe Osłony, po czym postrzeliła ją blokując dalsze działania. Ranna.|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170603|zaczajona w krzewach wśród czosnku by ratować Henryka przed grupką thralli Luksji. I rowerem. Dowiedziała się sporo o całym problemie.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|twórczyni wirtualnego księdza-joggera i poszukiwaczka informacji o rytuałach... i komputerach.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|6|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|4|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|4|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|4|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|4|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|4|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|3|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|2|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Henryk Kantosz](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-kantosz.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Łukasz Tworzyw](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-tworzyw.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wirgiliusz Kartofel](/rpg/inwazja/opowiesci/karty-postaci/9999-wirgiliusz-kartofel.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Stella Stellaris](/rpg/inwazja/opowiesci/karty-postaci/9999-stella-stellaris.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Stanisław Bazyliszek](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-bazyliszek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Rafał Łopnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-lopnik.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Paweł Parobek](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-parobek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Misza Dobroniewiec](/rpg/inwazja/opowiesci/karty-postaci/9999-misza-dobroniewiec.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Milena Pacan](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-pacan.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Melinda Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-zajcew.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/1803-maurycy-maus.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maciej Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Jakub Niecień](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-niecien.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Gerwazy Śmiałek](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-smialek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Edward Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-bankierz.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Dżony Słomian](/rpg/inwazja/opowiesci/karty-postaci/9999-dzony-slomian.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Balbina Wróblewska](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-wroblewska.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Antoni Bieguś](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-biegus.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Alegretta Tractus](/rpg/inwazja/opowiesci/karty-postaci/9999-alegretta-tractus.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
