---
layout: inwazja-karta-postaci
title:  "Paulina Tarczyńska"
categories: profile, roamer, postac_gracza
---
# {{ page.title }}

## Impulsy (co motywuje / napędza postać):

### Strategiczne

- **społecznik**: Paulina zawsze wpasowuje się w lokalną społeczność, głównie ludzi, ale też magów
- **lekarz z powołania**: "Po pierwsze: nie szkodzić". Również: zawsze gotowa do służby
- **make things right**: Paulina próbuje pozostawić świat lepszym, niż go zastała

### Taktyczne

- **uparta**: jak się uprze, to nie ma mocnych
- **altruistka**: pomóc jak największej ilości istot, szczególnie ludzi, nawet własnym kosztem
- **opiekun: Dracena** (tymczasowe): Paulina zrobi, co w jej mocy, aby pomóc Dracenie


## Co umie (co postać potrafi):


### Ekspercko (poziom 3):

- **diagnoza/badania** (lekarz/kataliza)
- **naprawa do wzoru** (lekarz/kataliza)
- **"you will see the err of your ways"** (społecznik, lekarz)

### Zawodowo (poziom 2):

- **lekarz**
- **nekromanta**
- **katalistka:energia magiczna**
- **alchemia**
- **społecznik**

### Amatorsko (poziom 1):

- **gra na gitarze**
- **negocjacje**
- **magia mentalna** (dla ludzisty)
- **plotki**

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         0       |        +1        |     +1     |    0   |       0       |      0     |     -1      |

## Kieszonkowe
2

## Otoczenie powiązane

### Powiązane frakcje

[brak]

### Kogo zna

#### Doskonale: 3

- ludzką społeczność
- wyrzutki świata magicznego, którym pomogła
- mentor: Kazimierz Przybylec
 
#### Nieźle: 2

- lekarzy magicznych i nie tylko
- magów z małych gildii (plankton)
- półświatek magiczny

#### Luźno: 1

- kluby, w których grywała
- Krystian Korzunio

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

- przenośny gabinet lekarski (praktyka)

#### Ponadprzeciętne: 2

- kryjówki i meliny: ci, którym pomogła, chętnie pomogą się jej ukryć

#### Niezłe: 1

- zapas quark: Paulina regularnie zbiera rezydualną energię z przychodzących do niej ludzi.

## Magia:

### Magia dynamiczna:

- magia lecznicza
- magia diagnostyczna
- kataliza
- nekromancja
- magia mentalna

### Zaklęcia hermetyczne



## Specjalne:

soul link z Marią

## Inne:

Paulina jest lekarką. Urodziła się jako człowiek; stała się magiem w trakcie Zaćmienia. Zidentyfikowana jako mag przez Kazimierza Przybylca i przez niego wyszkolona. Również on dał jej możliwość wyboru w kwestii przyłączenia się do Świecy , a ona postanowiła pójść własną drogą.

## Motto

"Po pierwsze: nie szkodzić"
"Magowie sobie poradzą, to ludzi należy chronić."