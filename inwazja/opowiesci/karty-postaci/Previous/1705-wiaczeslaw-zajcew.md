---
layout: inwazja-karta-postaci
categories: profile
factions: "Rodzina Dukata"
type: "NPC"
title: "Wiaczesław Zajcew"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **Wieczny Tułacz**: Wiaczesław podróżuje od miejsca do miejsca; nie przywiązuje się do tego, co zostawia za sobą. Rzadko długo w jednym miejscu. Niewiele ma i dobrze mu z tym.
* **Pomocny Dobrej Sprawie**: Wiaczesław bardzo pomaga przede wszystkim tym, którzy robią rzeczy jakie uważa za dobre - pomagają innym, nieważne, czy ludziom, magom czy viciniusom.
* **Amaterialista**: Wiaczesław nie przywiązuje się do żadnych obiektów materialnych. Tylko przyjaźnie, wspomnienia i działania się liczą. Jak nic nie masz, niczego nie możesz stracić.

### Zachowania (jaka jest)

* **Rozrzutny**: Pieniądze i inne środki się go nie trzymają. Wszystko i tak rozda.
* **Agresywny**: Standardowym sposobem rozwiązywania konfliktów według Wiaczesława jest starcie pięści i noży.
* **Carpe Diem**: Żyje chwilą. Nie planuje długoterminowo; zostawia to innym. Tym, których wspiera. Chce podróżować i przeżywać, nie planować długie i bezpieczne życie.

### Specjalizacje

* **Biosynteza broni z ciałem**
* **Transformacja ludzkiego ciała**: ?

### Umiejętności

* **Nomad zostawiający kłopoty**: opis
* **Biker dzielnego Śledzika**: opis
* **Gladiator na arenie Wejan**: opis
* **Szmugler, dla frajdy**: opis
* **Złota Rączka**: opis
* **Radiowiec, kocha się w złym sygnale**: opis
* **Manager areny**: ?
* **katai**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        1           |         0       |         1        |     -1     |    0   |      -1       |      0     |      0      |

### Specjalne

* **Bardzo potężny Wzór**: Wybitna odporność na Skażenie i zmianę Wzoru; +3.

## Magia

### Umiejętności
* **magia transportu**
* **magia materii**
* **technomancja**
* **biomancja**

### Zaklęcia statyczne

* **Wieczny Śledzik**: Każdy motor może stać się Śledzikiem, gdy jest taka potrzeba... na bazie motoru powstaje Śledzik
* **Wezwanie Śledzika**: Przywołanie swojego Śledzika, niezależnie, gdzie on by nie był
* **Śledzik Zagłady**: Przekształca Śledzika w bombę. Potężną bombę. Jednorazowy pocisk. Skuteczny.

## Zasoby i otoczenie

### Powiązane frakcje

* Roamer 
* Nomad 
* Przestępca
* Zajcew, ale niezależny
* Żercy Apokalipsy

### Kogo zna

* **Anarchiści**: Wiaczesław jest żywym dowodem na to, że można żyć w taki sposób; wielu anarchistów i ludzkich i magicznych podziwia Wiaczesława i pisze doń listy ;-).
* **Gladiatorzy**: Wiaczesław jest dość znanym gladiatorem; w końcu często o tym mówi ;-). 
* **Gazeta "Codzienny Miecz"**: codwutygodnik militarny świata magów; Wiaczesław ma tam kontakty u jednego z edytorów. No i prenumeruje; jest na bieżąco z nowinkami.
* **Poprzedni zleceniodawcy**: Osoby, którym kiedyś Wiaczesław pomógł za pieniądze; zwykle w świecie przestępczym, ale nie tylko
* **Osoby, którym pomógł**: Czy to ludzie czy magowie, Wiaczesław jest z tych, co pomogą; czy to drewno narąbią czy rurę naprawią czy potwora utłuką...

### Co ma do dyspozycji:

* **Motor Śledzik**: wielokrotnie wzmacniany, artefaktyzowany i przekształcany pryzmatycznie. Śledzik to idea, nie motor. A przynajmniej tak mówi.
* **Miecz Wejan**: jego miecz został sprawdzony i przekuty na arenie Wejan w niesamowicie skuteczną i morderczą broń.
* **AK-47 Sobaka**: Jak motor to jego śledzik, to jego karabin to jego pies. Nie idzie na ciężką akcję przeciw viciniusom bez tego.

### Surowce

* opis: 1/2/3


# Opis

37 lat. Nomad. Motocyklista jeżdżący na swoim dzielnym Śledziku. Straszliwy gladiator na morderczej arenie Wejan. Wiaczesław Zajcew - wojownik, szmugler i wierny przyjaciel.

## Motto

"Nic nie trwa wiecznie i wszystko da się zrobić, jeśli będzie z tego dobra sprawa"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|dostaje lekkie uznanie od Gabriela Dukata za doprowadzenie do zniszczenia klątwożyta|Rezydentka Krukowa|

## Plany

|Misja|Plan|Kampania|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|gra w przesunięcie Pauliny na stanowisko Rezydentki; chwilowo skupia się na zwalczaniu zewnętrznych sił atakujących teren.|Wizja Dukata|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|będzie grał na powrót Pauliny rezydentki. (Kić)|Wizja Dukata|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|chce (i przyjdzie) zrobić porządek w magitrowni Histogram. (Raynor)|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|180110|z prośby Pauliny chronił wszelkie siły na terenie; by nikt nie wtargnął i nie pożywił się osłabionym Powiatem Pustulskim.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171115|torturuje Łucję Maus za jej atak na Myszeczkę, nie atakuje Melodii (bo Paulina) po czym w efektowny sposób pozwala Paulinie odbić Łucję by miała szacun na dzielni.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|przejął dowodzenie nad siłami Sądecznego i zdecydował się utrzymać porządek niezależnie co będzie to kosztować.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171015|wynajęty przez Roberta Sądecznego, by uciemiężył i skompromitował Tamarę. Dokonał tego przez masakrację Legionu i rysowanie na nim penisów.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170423|któremu Paulina przekazała problemy powiązane z dilerem diakonopodobnych narkotyków; zajmie się tym.|[Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|10/06/24|10/06/27|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|170326|z bardzo silnym Wzorem; porwał konstruminusa i doprowadził do zniszczenia domu Archibalda. Ciężko ranny.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170325|potężny motocyklista i szmugler; wybił Paulinie szybę... przez przypadek. Ale przywiózł coś na klątwożyty.|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|10/02/28|10/03/03|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170409|który ma historię z wetrejanami; dał się przekonać Paulinie, by nie zabijać magów od razu|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170404|przyjaciel Wąż, który rozpuścił plotki o Paulinie szukającej areny gdzie może promować swojego viciniusa.|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|10/02/10|10/02/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170331|drab z charakterem. Radykalny i bezwzględny wobec tych którzy krzywdzą innych, delikatny wobec ludzi i magów którzy tego nie robią.|[Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|10/02/07|10/02/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|10|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|3|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-melodia-diakon.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Zenobia Morwiczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-morwiczka.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wojmił Rzeźniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-rzezniczek.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Szczepan Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Onufry Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Katarzyna Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Katarzyna Klicz](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-klicz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Kaja Odyniec](/rpg/inwazja/opowiesci/karty-postaci/9999-kaja-odyniec.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|1|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Janusz Kocieł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-kociel.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Filip Wichoszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-wichoszczyk.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Ferrus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-ferrus-mucro.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Edward Ramuel](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-ramuel.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
