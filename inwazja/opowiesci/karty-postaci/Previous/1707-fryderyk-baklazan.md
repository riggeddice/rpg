---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
title: "Fryderyk Bakłażan"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Rozwiąże każdą zagadkę**: 
* **Sprawiedliwości stanie się zadość**: Każdy odpowie za swoje zbrodnie
* ****: 

### Silne i słabe strony

* **+**: Spostrzegawczy
* **+**: Nachalny, lecz uprzejmy
* **+**: dobry glina

* **-**: Słaba kondycja
* **-**: Paskudny: śmierdzi cygarami
* **-**: 

### Specjalizacje

* **przesłuchiwanie** (detektyw)
* **Przechytrzenie** (strategia)
* **perswazja** (manipulacja)
* **tropienie** (detektyw)

### Umiejętności

* **detektyw**
* **dowódca**
* **manipulacja**
* **strateg**

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

* **infomancja**
* **mentalna**
* **biomancja**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* ** **
* ** **
* ** **

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

## Opis

Niski i niepozorny. Sprawia wrażenie zagubionego w świecie. Ciągle powołuje się na "fikcyjne" autorytety.
Mocno analityczny, bardzo zwraca uwagę na szczegóły.
Jest włochem, podobno ma żonę.
Chodzi w starym prochowcu i niewiele o nim wiadomo.
Jest uprzejmy i dociekliwy.
Umie grać na tubie.
Uzależniony od cygar.
Zwariowany wzrok.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Ludzie popełniają błędy"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|+1 surowiec od Diakonów (Silgor)|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170725|wynegocjował faktyczną zapłatę i nie przeleciał wiły. Brał aktywny udział w Skażeniu Jodłowca. Stracił większość pamięci dla bezpieczeństwa.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1707-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1707-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
