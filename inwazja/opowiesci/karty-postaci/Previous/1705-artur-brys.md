---
layout: inwazja-karta-postaci
categories: profile
factions: "Prokuratura magów, Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Artur Bryś"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **bardzo szybki**
* **szowinista**
* **porywczy**

### Specjalizacje

* **MMA**: opis
* **każdego obezwładni**: opis
* **wejdzie w najmniejszą szczelinę**: opis

### Umiejętności

* **mięśniak**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* oddział specjalny

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Artur nie jest osobą, którą zaprosiłoby się na imprezę. Głośny, z przeszłością, porywczy i ma dość… specyficzne poglądy na temat kobiet w siłach specjalnych (i w ogóle w różnych okolicznościach). Jednak Artur potrafi powściągnąć swoje skłonności i zrobić to co jest potrzebne - zwłaszcza wtedy, gdy to faktycznie potrzebne.
Wielki miłośnik MMA, zarówno jeśli chodzi o oglądanie jak i o walkę.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|dostał opinię gościa, którego skroiła słaba dziewczyna w szpitalu; najpewniej się do niej dobierał czy coś?|Powrót Karradraela|
|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|jego ziomkiem i przyjacielem jest Roman Brunowicz z Ognistych Niedźwiedzi|Powrót Karradraela|
|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|ma się opiekować Pauliną Widoczek|Powrót Karradraela|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160707|nieelegancki dla kobiet jak zwykle, udawał dresa kupującego kumplowi dresowi dronę. Obraził Grażynę i zdrażnił Dagmarę. Ale kupił dronę.|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|10/07/01|10/07/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150325|członek sił specjalnych Hektora, mięśniak i zaczepnowojownik. Pod wpływem aptoforma pokłócił się z Patrycją odnośnie kobiet w siłach specjalnych.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170412|który wpakował wszystkich w problem Bora i powstrzymywał kumpla przed bitwą z Borem; szczęśliwie, nie ma już opinii konfidenta u Brunowicza|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170407|który wpierw został pobity przez transformującego viciniusa (Paulinę), potem doprowadził Alinę i Dionizego do Ognistych Niedźwiedzi jak Edwin go naprawił|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170319|lojalny druh szefa 'gangu' Ognistych Niedźwiedzi; przyszedł do Aliny po pomoc, bo nie wie, jak prowadzić śledztwo w sprawie camgirl na dragach. Aha, nie znosi narkotyków.|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|10/03/06|10/03/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170315|który wykazał niesamowite rozeznanie w różnych fetyszach i różowych stronach internetowych ;-). Szczególnie bawiła go reakcja Patrycji...|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|człowiek Hektora który nienawidzi narkotyków. Polubił jakąś dziewczynę w Kotach; zgłosił Hektorowi narkotyki, pobił Witka (za niewinność; Alina była zań przebrana). Ma Parówczaną Teorię Hektora.|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Patrycja Krowiowska|6|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Hektor Blakenbauer|6|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Dionizy Kret|5|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Alina Bednarz|5|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Edwin Blakenbauer|4|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Roman Brunowicz|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Paulina Widoczek|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Kleofas Bór|3|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Szymon Skubny|2|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Robert Pomocnik|2|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|firma Skrzydłoróg|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|Wojciech Popolin|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Witold Wcinkiewicz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Urszula Murczyk|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Szczepan Paczoł|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Stanisław Pormien|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Rukoliusz Bankierz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Paulina Tarczyńska|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|Olga Miodownik|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|Oddział Zeta|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Maria Newa|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|Margaret Blakenbauer|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|Marcelin Blakenbauer|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Luiza Wanta|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|Krzysztof Kruczolis|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|Kornel Wadera|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Klara Blakenbauer|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|Katarzyna Leśniczek|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Karolina Maus|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|Infernia Diakon|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Grażyna Czegrzyn|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|Franciszek Knur|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Ewelina Nadzieja|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Dorota Gacek|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Dominik Parszywiak|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Damian Wiórski|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Dagmara Czeluść|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|Cyprian Koziej|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|Borys Kumin|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Beata Solniczka|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Aptoform Mirasilaler|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
