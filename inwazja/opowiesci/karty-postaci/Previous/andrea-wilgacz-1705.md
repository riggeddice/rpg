---
layout: inwazja-karta-postaci
title:  "Andrea Wilgacz"
categories: profile
guild: "Srebrna Świeca"
type: NPC
faction: "Wydział wewnętrzny"
---
# {{page.title}}

## Postać

### Motywacje (do czego dąży)

- **terminus z powołania**: moim zadaniem jest chronić (czuje się w obowiązku pomagać magom i ludziom)
- **lojalna wobec Świecy**: gildia to moi przyjaciele i jedyna rodzina, jaka mi została(dobro gildii jest ważniejsze niż pojedynczego maga)

### Zachowania (jaka jest)

- **dociekliwa**: tajemnice są zbyt niebezpieczne, żebym nie wiedziała (musi poznać prawdę)
- **mentalność szczura**: przeżyć za wszelką cenę (martwa nikomu nie pomogę)
- **własny kompas moralny**: przepisy się zmieniają, ale pewne rzeczy są uniwersalnie dobre lub złe (prawa należy przestrzegać, ale można je trochę nagiąć)
- **pragmatyczna**: czasem trzeba zaszkodzić, żeby pomóc 

## Co umie (co postać potrafi):

### Specjalizacje

- odkrywanie prawdy (detektyw/śledczy)
- analityk (śledczy/katalistka)
- oficer taktycznty (wydział wewnętrzny)

### Umiejętności

- mediator
- agent wydziału wewnętrznego
- detektyw
- terminus drugoliniowy
- śledczy
- broker informacji
- fałszowanie dowodów
- archiwistka
- wróżka
- dywersja
- pojedzie wszystkim
- pułapki



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         +1      |        -1        |      +1    |   -1   |       -1      |      0     |      0      |
### Specjalne
**brak**

## Magia

### Szkoły magiczne

- **kataliza**
- **infomancja**
- **magia mentalna**

### Zaklęcia statyczne

## Zasoby i otoczenie

### Powiązane frakcje
wydział wewnętrzny Świecy

### Kogo zna

- szefa lokalnej mafii (traktuje ją jak przybraną córkę)
- ludzi półświatka (magicznego i nie)
- burdelmamę burdelu z kralothami
- terminusów (takich, co się jej boją i takich, co są jej wdzięczni)
- Ernest Maus
- Kontakty w rodzinie Weiner
- okolicznych detektywów (głównie ludzi, kilku magów) 

### Co ma do dyspozycji:

- wyposażenie wydziału wewnętrznego
- składy wtdziału wewnętrznego
### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

### Koncept

Agentka wydziału wewnętrznego, trzymająca karty przy orderach.
Oddana gildii, uważa gildę za ważniejszą niż ona sama, ale uważa też, że martwa nikomu nie pomoże.

### Motto

"Tekst"