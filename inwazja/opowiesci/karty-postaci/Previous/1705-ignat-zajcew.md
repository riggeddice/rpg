---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Ignat Zajcew"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **wróg - Świeca**: "Naprawdę nie lubię tienowatych. Zwłaszcza w krawatach. Winni są wszystkiego."**

### Zachowania (jaka jest)

* **rywalizacja**: "Jestem najlepszy; przynajmniej, lepszy od ciebie"**
* **impulsywny**: "YOLO! Konsekwencjami zajmiemy się później, teraz czas na akcję"**
* **waleczny**: "Atakujemy! Nie zostanę pokonany tu, nie zostanę pokonany nigdzie!"**
* **żartowniś**: "Zachowanie powagi się nie godzi. Trzeba zrobić dowcip, zwłaszcza komuś ważnemu"**
* **niesubordynacja**: "Mogę współpracować, ale nie będę słuchać niczyich poleceń."**

### Specjalizacje

* **devastation** (mag ognia, wojownik, joker)
* **unieruchamianie** (wojownik, detektyw, technomancja)
* **broń oblężnicza i oblężenia** (Instytut Zniszczenia, bojowy mag)
* **bojowy mag ognia**
* **magia pościgowa**
* **magia Instytutu Zniszczenia**
* **adaptacja rzeczy w szybki pojazd**
* **katai**

### Umiejętności

* **mag Instytutu Zniszczenia**
* **uliczny wojownik**
* **detektyw**
* **tracker**
* **practical joker**
* **hazardzista**
* **wyścigi**
* **prowokator**
* **opowieści**
* **wyleciał z Instytutu Pryzmaturgii**
* **wyleciał z Instytutu Technomancji**
* **miasto**
* **fan wszystkiego śledziopodobnego**
* **strzelec podczas ruchu**
* **siege engine operator**
* **włamywacz**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |     -1     |   +1   |      -1       |     +1     |      0      |

### Specjalne

* gorące serce: +3 do morale. Ignata praktycznie nie da się wystraszyć.
* kamienny łeb: +3 do odporności na ogłuszenie / wyłączenie z akcji.
* rogata dusza: +3 do odporności na rzeczy typu energia negatywna czy ból.

## Magia

### Szkoły magiczne

* **magia elementalna**
* **magia transportu**
* **magia materii**

### Zaklęcia statyczne

* nazwa : link
* nazwa : link

## Zasoby i otoczenie

### Powiązane frakcje

* link_do_frakcji

### Kogo zna

### Co ma do dyspozycji

### Surowce

* opis: 1/2/3


# Opis
Twardogłowy mag bojowy, pochopny i chętny do szybkiego i efektownego działania. Kawalarz i dowcipniś jakich mało, 'practical joker'. Niezły detektyw. Nie lubi Srebrnej Świecy i często pozwala uprzedzeniom zwyciężyć nad faktami. Rycerski, waleczny i chętnie rzucający się w ogień walki mag który łatwo się nudzi. Niezły hazardzista, choć częściej przegrywa niż wygrywa.
Jego specjalnym talentem jest to, że bardzo trudno jest go unieszkodliwić. Archetyp: dowolny dinobot (Grimlock?), łącznie z arogancją i walecznością.

Znalazł swoją rolę na KADEMie po wyleceniu z dwóch instytutów; Mariusz Trzosik zaproponował go do Instytutu Zniszczenia. Jeśli trzeba coś zdobyć (surowiec, komponent, artefakt, cel…) to Ignat wchodzi w temat, robi research, śledzi cel / znajduje go (trackuje), po czym wykorzystuje swoją magię do zrobienia broni oblężniczej i zdobycia / unieruchomienia celu. Potem to przywozi.
## Motto

"Tekst"

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|obiekt zamesmeryzowania Sandry Maus i Teresy Koliczer dzięki działaniom Silurii i Efektu Skażenia|Powrót Karradraela|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170519|zaciągnięty przez Silurię do pomocy militarnej na wypadek problemów; większość czasu się nudził a potem skończył jako amant dwóch czarodziejek.|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|10/08/21|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170125|stanął pomiędzy Infernią i Hektorem i skończył połamany.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|detektyw kojarzący fakty, powiedział o możliwym wyjściu z Fazy Daemonica przez Powiew Świeżości - czego dowodem Głowica Aegis|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|zbyt lubi strzelać z Kometora by ktokolwiek czuł się bezpiecznie. Zestrzelił Kazimierza Esuriit "małym Kometorem". Niestety.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|który nie chce pracować z tienowatymi i z pomocą Silurii poświęcił łazika by móc odepchnąć Wyspę Zmarłych|[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|konstruujący wpierw łazik a potem Kometor Oblężniczy Zabijający Czerwie. Obraża * magów Świecy. Prawie niszczy swoją bazę. Typowy Zajcew.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|chciał poderwać Mariannę, ale mu nie wyszło. Nadzieja Silurii, że umie używać Pryzmatu...|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161012|który połączył się z resztą KADEMek i który naprawdę nie umie prowadzić Opla Astra Setona (Seton też nie umiał).|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160927|który nie lubi "tienowatych" ale w obliczu zagrożenia zniszczenia Aegis zdecydował się na znalezienie kuzynki ze Świecy która może mu pomóc.|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150922|który dla jednej dziewczyny (Elizawiety) bardzo naraził stosunki między gildiami i przypadkiem mu się udało.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|wielki nieobecny, który non stop kręci się w sprawie Baltazara: a to koło Malii, a to koło Elizawiety, tu koło Żupana...|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160202|który wydał trochę kasy by pognębić tienowatego Sowińskiego; udowodnił, że ów jest mocny tylko w gębie.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|który wkręcił Alisę w "lożę erotyczno-militarną" której oficerem jest Siluria i ogólnie świetnie się bawił kosztem Hektora.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|który założył się o autobus rozrodowych śledzików syberyjskich z * magiem Świecy, co strasznie skomplikowało żywot Hektora (bo Infernia podniosła stawkę).|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|detektyw który w końcu traci nerwy po zaatakowaniu Annalizy i sam atakuje niewinnego Marcela Bankierza.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|kompetentny detektyw KADEMu nienawidzący SŚ, który odkrył dla Silurii powiązania między Izą, Tamarą i Draceną.|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|mastermind chcący skłócić Mausów i Świecę (nadał zbirom pozytywkę Mausów do ukradnięcia); niestety, wykosztował się paskudnie odzyskując ją gdy sprawa stała się polityczna.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|zirytowany jak osa; przeprosił Oliwiera za pobicie (bo Elizawieta), próbuje chronić Silurię i BARDZO mu się nie podoba, że tienowaci zrobili sobie z niego publiczne używanie...|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|którego pobicie ulicznego muzykanta ściągnęło nań Balroga; też, którego sromotna i haniebna porażka do Balroga mogłaby rozjarzyć konflikt KADEM - Świeca|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|zastraszający tienowatych na lewo i prawo, pobił niewinnego Oliwiera, detektyw odnajdujący Annę. MVP.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|detektyw, który 'skorzystał' z okazji z Infernią by potem znaleźć wspólnie z Mordecją KADEMowego winowację - Mariana Ł.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|detektyw który powiedział Silurii jaka była rola Artura Kotały; też, wrobił Alisę w to, że by flirtować z Silurią trzeba go w jakiś sposób pokonać.|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|19|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Hektor Blakenbauer|9|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Andżelika Leszczyńska|8|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Mikado Diakon|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Infernia Diakon|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Paweł Sępiak|6|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Anna Myszeczka|6|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Marianna Sowińska|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Siriratharin|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Norbert Sonet|4|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Marta Szysznicka|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Lucjan Kopidół|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Dosifiej Zajcew|4|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Whisperwind|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Warmaster|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Marian Łajdak|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Marcel Bankierz|3|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Karolina Kupiec|3|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Karina Paczulis|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Jolanta Sowińska|3|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Joachim Kartel|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Ilona Amant|3|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|GS "Aegis" 0003|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Franciszek Maus|3|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Elizawieta Zajcew|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Edwin Blakenbauer|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Łukija Zajcew|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|Zofia Weiner|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|Wiktor Sowiński|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Vladlena Zajcew|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Urszula Murczyk|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Tamara Muszkiet|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Quasar|2|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Ozydiusz Bankierz|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Oktawian Maus|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Ofelia Caesar|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Netheria Diakon|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Milena Diakon|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Melodia Diakon|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Mariusz Trzosik|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Konstanty Myszeczka|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|Klara Blakenbauer|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Kazimierz Sowiński|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|Julia Weiner|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Judyta Maus|2|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Joachim Kopiec|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Jakub Pestka|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Infensa Diakon|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|GS Aegis 0003|2|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Fiodor Maius Zajcew|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|Eis|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Draconis Diakon|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Bogumił Rojowiec|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Balrog Bankierz|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Artur Żupan|2|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Antygona Diakon|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Anna Kozak|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Andrea Wilgacz|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Alisa Wiraż|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Zenobia Bankierz|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Wioletta Lemona-Chang|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Tymoteusz Maus|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Teresa Koliczer|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|Szczepan Sowiński|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Stefan Jasionek|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|Stalowy Śledzik Żarłacz|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|Sandra Maus|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|Salazar Bankierz|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Saith Kameleon|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Saith Flamecaller|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Rukoliusz Bankierz|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Roman Błyszczyk|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Remigiusz Zajcew|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Rafał Kniaź|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Paweł Maus|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|Otton Blakenbauer|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Oliwier Bonwant|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Nikodem Sowiński|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Mordecja Diakon|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Maurycy Maus|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Marian Agrest|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Margaret Blakenbauer|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Marcelin Blakenbauer|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Malia Bankierz|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Leonidas Blakenbauer|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Laurena Bankierz|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Lancelot Bankierz|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Kornelia Kartel|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Konstanty Bankierz|1|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|
|Konrad Sowiński|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Kira Zajcew|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Kermit Diakon|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Katarzyna Kotek|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Karradrael|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Karolina Maus|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Karol Poczciwiec|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Julian Krukowicz|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Judyta Karnisz|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Jan Wątły|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Irina Zajcew|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Gala Zajcew|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Franciszek Baranowski|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Felicja Strączek|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Ewa Zajcew|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Emilia Kołatka|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Elea Maus|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Edward Sasanka|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|Dracena Diakon|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Demon_481|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Dagmara Wyjątek|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Czirna Zajcew|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Błażej Falka|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Borys Kumin|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Bogdan Bankierz|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Bazyli Weiner|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|Baltazar Maus|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Aurel Czarko|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Artur Kotała|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Arazille|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Antoni Chlebak|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Annaliza Delfin|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Andrzej Bankierz|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Aleksandra Trawens|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Aleksander Sowiński|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Adrian Kropiak|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|Adonis Sowiński|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
