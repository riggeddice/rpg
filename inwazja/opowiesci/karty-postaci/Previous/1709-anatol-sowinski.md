---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "dzióbek"
title: "Anatol Sowiński"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: nieistotny pyłek**:
    * _Aspekty_: chce być doceniony, chce być zauważony, boi się być ośmieszonym
    * _Opis_: 
* **FIL: hero of the people**: 
    * _Aspekty_: pomagać nawet swoim kosztem, znać ludzi ich potrzeby i problemy, inspirować innych
    * _Opis_: 
* **MET: efektowny do bólu**:
    * _Aspekty_: akcja powinna być spektakularna, im większa widownia tym lepiej
    * _Opis_:
* **MRZ: być jak Ewa Zajcew**:
    * _Aspekty_: wsławić się, zyskać prawdziwą potęgę, dokonać bohaterskich czynów, ja to załatwię
    * _Opis_: 

### Umiejętności

* **dramaturgia**:
    * _Aspekty_: wie jak zbudować napięcie, potrafi korzystnie się zaprezentować, zrobić efektowne wejście, potrafi wtopić się w tło, taunt
    * _Opis_: 
* **walka tarczą**: 
    * _Aspekty_: blokuje każdy atak, zaskakujący atak, attrition warfare, walka bez broni, frustrowanie przeciwnika
    * _Opis_: preferuje puklerz, potrafi walczyć wręcz
* **social worker**:
    * _Aspekty_: niesie pomoc, umie rozmawiać, umie słuchać, ludzie mówią mu co im leży na sercu, uczy i inspiruje, umie pisać podania (o dofinansowanie)
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Siły_:
    * _Słabości_:
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: leczenie, wspomaganie, analiza/badanie
    * _Opis_: 
* **Magia transportu**: 
    * _Aspekty_: odcinanie przestrzeni, wszędzie wtargnie, tarcze kinetyczne, ewakuacja, summon equipment (shield/armor)
    * _Opis_: 
* **Magia materii**: 
    * _Aspekty_: przemiana ciała, fortyfikacje, taran
    * _Opis_:
* **Magia elementalna**: 
    * _Aspekty_: widowiskowa ale nieszczególnia efektywna
    * _Opis_: 	
	
### Zaklęcia statyczne

* **Paragon of Defence**:
    * _Aspekty_: omnitarcza
    * _Opis_: zaklęcie personalne, tworzy tarczę o zwykłej sile, ale przekraczającą szkoły magiczne użytkownika

## Otoczenie

### Powiązane frakcje

* Srebrna Świeca

### Zarobki

* żołd Świecy
* local youth center
* hero for hire

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **efektowny pancerz magitechowy**:
    * _Aspekty_: widowiskowa, chroni jak porządna zbroja
    * _Opis_: komplet zbroja + puklerz, zawiera zaklęte elementalne efekty specjalne
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

22 lata

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"If it's worth doing, it's worth overdoing"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|tymczasowo Dotknięty przez Mrok Esuriit. Wrażliwy na tą formę energii.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|usłyszała o nim Ewa Zajcew z uwagi na Wieprzopomnik w Półdarze.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Stefania Kołek przekazuje szerzej wieści o efektownym wejściu Anatola. Anatol dostaje opinię lepszego niż jest.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180112|mistrz efektownych akcji i widowiskowych wojen z... czymkolwiek (tym razem: Golem Esuriit). |[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|od efektownych wejść i walki z oczkodzikiem o transgeniczność Wojtka. Główny negocjator zespołu (smutne).|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-kinga-bankierz.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
