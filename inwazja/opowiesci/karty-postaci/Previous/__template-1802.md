---
layout: inwazja-karta-postaci
categories: profile
factions: "faction_1, faction_2"
type: "NPC"
owner: "public"
title:  "Imię_Nazwisko"
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Aspekty_: 
    * _Szczególnie_: 
    * _Opis_: 
* **Społeczne**:
    * _Aspekty_: 
    * _Szczególnie_: 
    * _Opis_: 
* **Serce**:
    * _Aspekty_: 
    * _Szczególnie_: 
    * _Opis_: 
* **Działanie**:
    * _Aspekty_: 
    * _Szczególnie_: 
    * _Opis_: 

### Umiejętności

* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 

### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 

## Zasoby i otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zasoby

* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: 

# Opis


### Koncept



### Mapa kreacji

brak

### Motto

""

# Historia:
