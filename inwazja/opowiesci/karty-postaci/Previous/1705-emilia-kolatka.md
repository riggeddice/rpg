---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Emilia Kołatka"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **idealistka Świecy**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **naturalny przywódca**: cytat_lub_opis_jak_to_rozumieć
* **prawa**: cytat_lub_opis_jak_to_rozumieć
* **skromna**: cytat_lub_opis_jak_to_rozumieć
* **bardzo przewidująca**: cytat_lub_opis_jak_to_rozumieć
* **spokojna i wyciszona**: cytat_lub_opis_jak_to_rozumieć
* **cierpliwa**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **legendarny strateg**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **historyk Świecy**: opis
* **arystokratka Świecy**: opis
* **prawo**: opis
* **polityk**: opis
* **ceremonie i procedury formalne**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |        +1       |         0        |     +1     |    -1  |      +1       |     -1     |     -1      |

### Specjalne
* dissonant serenity: +1 do kontaktów społecznych
* unbreakable: +1 do bezpośrednich konfliktów defensywnych

## Magia

### Szkoły magiczne

* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **szeroko rozpoznawana arystokratka Świecy**: opis
* **aktualna przywódczyni Kurtyny**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Emilia Kołatka jest przyjaciółką Andżeliki Leszczyńskiej "od zawsze"; obie dziewczyny się bardzo utożsamiały z frakcją lojalistów (konserwatywną).
Zaprzyjaźniły się przez książki i historie z przeszłości; Andżelika skupiła się na planowaniu i porządkowaniu rzeczywistości a Emilia na jej tworzeniu i przekształcaniu.
Emilia jest strategiem długoterminowym; nie angażuje się w działania polegające na zwycięstwie frakcji X czy frakcji Y. Ona pragnie odrodzenia Srebrnej Świecy i sprawieniu, by gildia wróciła do korzeni. Szanowana i poważana zarówno przez sojuszników jak i opozycję, Emilia jest często dawana jako przykład tego czym powinna być arystokracja Srebrnej Świecy (nawet, jeśli zubożona).

Bardzo często wspiera Barbarę Sowińską w jej planach, choć konsekwentnie odmawia przyjęcia jakiegokolwiek zaszczytu, tytułu czy urzędu.


### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160316|która żądając od Hektora opowiedzenia się po stronie Kurtyny wyraźnie nie zdaje sobie sprawy, że nie jest na pozycji siły.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|która poniosła ogromne straty jako przywódca Kurtyny. Zwróciła się do Hektora o wsparcie polityczne i militarne.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|dowodzi stabilizacją Kopalina z ramienia Świecy i współpracuje z Amandą i Hektorem. Chce wciągnąć Dagmarę w pułapkę.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151202|która NIE MOŻE DAĆ HEKTOROWI WĘZŁA VLADLENY. If he knows what she means.|[Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|10/06/15|10/06/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151021|która rekonsoliduje siły Kurtyny i (nieudanie) szuka Sasanki. Plan niezły, ale Siluria.|[Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151014|wybitny polityk, która wraca z planem jak ujawnić czym jest Szlachta przez dostęp do kodów dostępu * magitecha Wiktora Sowińskiego.|[Jedno słowo prawdy za dużo](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150728|która organizuje bardzo ważny bal; z rozkazu Agresta zgodziła się na plan Leokadii Myszeczki i na zaproszenie Marcelina, Hektora i "kogoś z KADEMu".|[Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|która przekierowała list do Hektora nie chcąc oskarżać sama Marcelina; obserwuje go z uwagą i załatwiła audytora by Hektor ocenił odpowiednio uczciwie.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150604|która broniła Izę nawet kosztem swojej reputacji i bardzo za to ucierpiała politycznie i po opinii; Hektor ją zmiażdżył.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Hektor Blakenbauer|9|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Margaret Blakenbauer|7|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Siluria Diakon|5|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Marcelin Blakenbauer|5|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Wiktor Sowiński|4|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Edwin Blakenbauer|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Tatiana Zajcew|3|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Ozydiusz Bankierz|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Klara Blakenbauer|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Diana Weiner|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Dagmara Wyjątek|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Amanda Diakon|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Adrian Murarz|3|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Vladlena Zajcew|2|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Rufus Czubek|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Paweł Sępiak|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Otton Blakenbauer|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Marian Agrest|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Leonidas Blakenbauer|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Kleofas Bór|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|GS "Aegis" 0003|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Elea Maus|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Warmaster|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Wanda Ketran|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Tymotheus Blakenbauer|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Szymon Skubny|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Saith Kameleon|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Patrycja Krowiowska|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Pasożyt Diakon|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|Oktawian Maus|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Ofelia Caesar|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Norbert Sonet|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Mojra|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Milena Diakon|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Mariusz Trzosik|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Marcel Bankierz|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Lucjan Kopidół|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Leokadia Myszeczka|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|Kinga Melit|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Karol Poczciwiec|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Julia Weiner|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Judyta Maus|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Judyta Karnisz|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|Joachim Zajcew|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Jan Szczupak|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Infernia Diakon|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Infensa Diakon|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Ignat Zajcew|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Estrella Diakon|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Elizawieta Zajcew|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Edward Sasanka|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Demon_481|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Borys Kumin|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Bolesław Derwisz|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Benjamin Zajcew|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Arkadiusz Klusiński|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Anna Kajak|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Aneta Rainer|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Andżelika Leszczyńska|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Alina Bednarz|1|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
