---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Włodzimierz Tulewicz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Dorobkiewicz**: nie ma małej kasy (Janusz Biznesu)

### Zachowania (jaka jest)

* **Bezpośredni**: cytat_lub_opis_jak_to_rozumieć
* **Walczak**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **Bajerant** (Przedsiębiorca)
* **Walka bronią improwizowaną** (Wiejskie bijatyki)
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **Murarz-Tynkarz**
* **Przedsiębiorca**
* **Wiejskie bijatyki**
* **Majsterkowicz**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |        -1        |     -1     |   +1   |      -1       |     +1     |     -1      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* kręgi budowlane
* pracownicy

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Włodzimierz Tulewicz - urodzony w małej wsi we wschodniej małopolsce (dzisiejsza część galicji po stronie ukrainy) w rodzinie polsko-ukraińskiej. Kończył technikum murarsko-tynkarskie w pobliskim mieście Morszczynów. Obecnie człowiek w drugiej połowie wieku średniego. Zawsze marzył o wyjeździe w głąb Polski, zrobieniu prawdziwych pieniędzy, dorobieniu się i wyjścia z biedy w jakiej żyła jego rodzina. Prosty, acz poczciwy (choć niekoniecznie praworządny) i zaradny, i bardzo przedsiębiorczy. Po ukończeniu technikum pracował jako młodzik w Warszawie na budowach, przy remontach itd. Praca dość szybko mu się znudziła, chciał robić coś co bardziej angażowało jego zmysły zaradności i przedsiębiorczości niż mięśnie. Imał się różnych interesow, w trakcie których spotkał człowieka, który miał to czego on nie miał, czyli kontakty. Szybko się okazało, że w różnych branżach jest popyt na usługi przewozowe, dostarczeniowe, kurierskie, a z różnych powodów Poczta Polska nie sprawdzała się w takiej roli. Dogadywali się razem na wystarczającym poziomie, żeby każdy z nich wykorzystywał swoje talenty do mnożenia wspólnych korzyści.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160915|sprał terminuskę Dorotę by dostać się do Kornela... jego ludzie z nim rozmawiają tylko, gdy z nimi pije|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|10/05/26|10/05/29|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Marcin Szybisty](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Magda Szybisty](/rpg/inwazja/opowiesci/karty-postaci/9999-magda-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Konrad Matczak](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-matczak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jolanta Iwan](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-iwan.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Andrzej Marciniak](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-marciniak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
