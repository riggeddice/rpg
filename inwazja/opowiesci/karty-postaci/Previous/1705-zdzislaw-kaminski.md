---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Zdzisław Kamiński"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **dobroduszny**: Zdzisław chce postępować jak poczciwy człowiek, chce czynić dobro
* **lojalny**: Zdzisław jest lojalny wobec swoich przyjaciół i mocodawców

### Specjalizacje

* **alarmy** (zabezpieczenia)

### Umiejętności

* **elektryk**
* **majsterkowanie**
* **złota rączka**
* **zabezpieczenia**
* **kafar**
* **człowiek pracy**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        -1       |        +1        |     +1     |   +1   |      -1       |     +1     |     -1      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

poczciwy elektryk-majsterkowicz u Skubnego

Zdzisław Kamiński - elektryk w wieku średnim, absolwent technikum elektrycznego. Rozwiedziony, ma córkę w wieku dwudziestu kilku lat. Od pewnego czasu pogorszyły się ich stosunki. Poczciwy i dobroduszny człowiek, który jednak nie do końca odnalazł się w realiach dzisiejszego świata. Przez wiele lat pracował jako członek pogotowia elektrycznego, często wykonując zlecenia dla stosunkowo stałej klienteli. Pracował również montując oraz naprawiając urządzenia służące do zabezpieczeń - alarmy, zamki elektroniczne. Kocha majsterkowanie, to jego pasja od dzieciństwa. Jest "złotą rączką".

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|jest wolny od Skubnego (spłacił dług)|Taniec Liści|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160921|poczciwy kafar, który nie ma nic przeciwko temu by walnąć anarchiście w celu wykonania misji. Preferuje metody pokojowe. Spłacił dług u Skubnego.|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1705-wanda-ketran.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tomasz Kracy](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kracy.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Milena Marzec](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-marzec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Rokita](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rokita.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Hubert Brodacz](/rpg/inwazja/opowiesci/karty-postaci/1709-hubert-brodacz.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grzegorz Włóczykij](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-wloczykij.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
