---
layout: inwazja-karta-postaci
categories: profile
factions: "Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Patrycja Krowiowska"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **cierpliwa**: cytat_lub_opis_jak_to_rozumieć
* **ostrożna**: cytat_lub_opis_jak_to_rozumieć
* **dyskretna**: cytat_lub_opis_jak_to_rozumieć
* **ciekawska**: cytat_lub_opis_jak_to_rozumieć
* **level-headed**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **fałszowanie śladów**: opis
* **manipulacja prawdą**: opis
* **manipulacja faktami**: opis

### Umiejętności

* **haker**: opis
* **informatyka**: opis
* **agregacja informacji**: opis
* **intelligence warfare**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Patrycja jest matką trójki dzieci i osobą stosunkowo spokojną. Jednocześnie jest bardzo cierpliwą i zdolną ekspertką do spraw technicznych. Analityczka, agregat informacji i osoba zarówno znajdująca leady jak i maskująca działania grupy specjalnej.
Z racji swojego zawodu podejrzewa, że coś jest na rzeczy z tematami paranormalnymi, ale nikomu nic nie mówi - nie ich sprawa. Nie zadaje zbędnych pytań.
Interesuje się innymi agentami grupy specjalnej, a szczególnie Anną Kajak.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|wierzy w dziwną szajkę hakerów, którzy bawią się w "x-files" na kamerach przemysłowych|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160914|której klon został stworzony przez Karolinę by trzymać Hektora w ryzach. Karolina nie doceniła determinacji Hektora, który ją zabił. KIA.|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160810|odzyskana przez Zajcewkomando; ma zmienioną strukturę biologiczną i jest w stazie w Rezydencji|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160707|która powiązała linie logistyczne dron ze Skrzydłorogu i znalazła powiązanie ze Skubnym. Porwana w odwecie przez The Governess.|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|10/07/01|10/07/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|której dzieci zostały zagrożone przez "trzecią stronę" z niewiadomego powodu.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151212|agentka sił specjalnych Hektora która dostrzegła, że Alina i Dionizy mają "lost signal".|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|która znalazła co Tatiana chciała by Hektor się dowiedzia i która natychmiast przekazała Hektorowi, że ktoś chce go wrobić.|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|która przedsięwzięła akcję oczyszczenia prokuratury z kreta i zdrajcy. Niestety, Edwin jest * magiem.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|która dostała zadanie niemożliwe. A przynajmniej w tym czasie. Zwłaszcza, że TOR network.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|zbierająca istotne informacje o działaniu policji i stanie oskarżenia dla Blakenbauerów.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|osoba, która zebrała wszystkie istotne informacje i używając dronów zapewniła przewagę do wygrania misji bez 1 strzału.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|która co prawda nie znalazła dla Hektora aptoforma, ale znalazła coś powiązanego z Arazille...|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150325|informatyk sił specjalnych Hektora, agentka która odkryła nagranie z imprezy dla Hektora.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170412|która oderwała się od szukania "hakerów XFiles" by dowiedzieć się o Borze... potem wciągnęła Hektora by się wybielić i serio skomplikowała sprawę.|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170407|która monitorowała viciniusa (Paulinę) i w końcu uwierzyła w dziwnych hakerów fantomowo zmieniających zapisy kamer "x-files lulz".|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170315|dziewczyna od komputera ;-); znalazła korelację między Eweliną i Beatą i tak naprawdę umożliwiła dojście do Mimika Hektorowi.|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|doskonała informatyczka; ma namiary na czarną wołgę Skubnego i wie, gdzie ona się znajduje (przez lokalizację telefonów właścicieli samochodu)|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|14|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|12|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1707-dionizy-kret.html)|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|6|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|6|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-klara-blakenbauer.html)|5|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1705-wanda-ketran.html)|4|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|4|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|4|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|4|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1705-olga-miodownik.html)|3|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|3|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|3|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-vladlena-zjacew.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-tymoteusz-maus.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1705-szczepan-paczol.html)|2|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|2|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|2|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1705-ozydiusz-bankierz.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|2|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|2|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[firma Skrzydłoróg](/rpg/inwazja/opowiesci/karty-postaci/9999-firma-skrzydlorog.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Wojciech Popolin](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-popolin.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1705-wiktor-sowinski.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1705-urszula-murczyk.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-tatiana-zajcew.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Marek Rudzielec](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-rudzielec.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-kromlan.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Krzysztof Kruczolis](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-kruczolis.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Katarzyna Leśniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-lesniczek.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Himechan](/rpg/inwazja/opowiesci/karty-postaci/9999-himechan.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grażyna Czegrzyn](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-czegrzyn.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Fryderyk Mruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-mruczek.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Ewelina Nadzieja](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-nadzieja.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Ernest Kokoszka](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-kokoszka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Erebos](/rpg/inwazja/opowiesci/karty-postaci/9999-erebos.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dominik Parszywiak](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-parszywiak.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Beata Solniczka](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-solniczka.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Anatol Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-anatol-weiner.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
