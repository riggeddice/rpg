---
layout: inwazja-karta-postaci
title: "Maria Newa"
categories: profile
guild: "Niezrzeszony"
type: NPC
---

# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **paladyn ludzkości**: Maria próbuje chronić ludzi przed magami i viciniusami; nie pozwoli, by można było się nad ludźmi znęcać.
* **odkrywca sekretów**: Maria próbuje zrozumieć co stoi za tajemnicami tego świata; zarówno za światem ludzi jak i magów. Sekrety to nie jej styl.
* **żądna sprawiedliwości**: Dla Marii ważniejsza jest sprawiedliwość niż wybaczenie. Po prostu "make things right". Kto rani, musi cierpieć.

### Zachowania (jaka jest)

* **hands-on**: Maria nie jest zbyt dobra w outsourcowaniu innym rzeczy, które może robić sama...
* **odważna**: Maria ma tendencje do działania osobiście, twarzą w twarz przeciwko przeciwnościom. Ryzykantka.
* **skryta**: Maria raczej nie mówi wszystkiego co myśli; więcej działa (dyskretnie) niż mówi. 
* **uparta**: Nie ma tendencji do przerywania operacji (śledztwa); raczej jak już coś zaczyna, to trudno zmienić jej zdanie

## Co umie (co postać potrafi):

### Specjalizacje

* **wiecznie przed kamerą**: Maria tak przywykła do bycia przed kamerą, że udaje kogoś kim nie jest (i czego nie wie) prawie bezbłędnie.
* **wzbudzanie zaufania**: Maria potrafi działać w taki sposób, by wyjść na "sympatyczną". Też, jest trochę dziwna, ale w ten czarujący sposób.
* **wykrycie niezgodności**: czy to w dokumentach, czy podczas zdobywania informacji od innych - Maria potrafi dojść do tego, że coś do siebie nie pasuje.
* **działanie w trudnym terenie**: Maria ma tendencje do działania w niekorzystnym dla wszystkich terenie. Specjalnie działa tam, bo się jej nie spodziewają ;-).
* **sprint**: Jedną z jej głównych umiejętności jest bardzo szybkie bieganie, by wszystkich zgubić lub się gdzieś dostać odpowiednio szybko.

### Umiejętności

* **dziennikarz śledczy**: Umiejętność zadania właściwego pytania, rozmowy z ludźmi i nie ujawnienia swoich źródeł, jak i wiedzy gdzie szukać...
* **praca z dokumentami**: Czasami prawdziwe sekrety i zagadki nie znajdują się nigdzie indziej niż w archiwach, papierowych lub elektronicznych...
* **aktorka**: Oszukiwanie, wcielanie się w rolę kogoś innego, udawanie... dla Marii to chleb powszechny. Też: wyglądanie dobrze przed kamerą.
* **włamywacz**: Marię trudno zatrzymać; ma tendencję do włażenia wszędzie, gdzie jej nie chcą. Trochę jak kot, ale bardziej.
* **zwiadowca**: Czy to cichy chód w mieście, czy też kwestia spostrzeżenia czegoś w kluczowym momencie, 
* **parkour**: Maria ćwiczy bieganie, wspinaczkę i ogólnie poruszanie się w trudnym terenie. W jej zawodzie jest to szczególnie ważne...
* **survival**: Niezależnie od tego, gdzie by się nie znajdowała, Maria to cholera, którą ciężko ubić. Raczej przetrwa; najwyżej rozpali ogień i rybę złowi...
* **węzły**: Jedna z bardziej przydatnych umiejętności Marii. Czy to kogoś związać, czy odzyskać linę...

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        +1       |         0        |     +1     |   -1   |      -1       |      0     |     +1      |

### Specjalne

* **soullink z Pauliną Tarczyńską**: potrafią przesyłać sobie myśli i kontrolowanie zapominać różne rzeczy

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* Skorpion

### Kogo zna

* **kontakty w prasie ludzkiej**: Maria jest powszechnie znana jako "ta dziwna", na uboczu, ale cholernie skuteczna i z naprawdę dobrymi materiałami
* **kontakty w grupach paranormalnych**: Maria jest powiązana ze sporą ilością paranormalnych (i paranoicznych) grup z którymi kontaktuje się w internecie
* **sporo dziennikarzy śledczych**: Maria zna się ze sporą ilością innych dziennikarzy śledczych; często wymieniają się leadami oraz dziwnymi tematami
* **kontakty w Skorpionie**: Maria i Skorpion mają pewne powiązania; Maria nie do końca zna naturę Skorpiona; dla niej jest to "X-Com"
* **mnóstwo informatorów**: Maria ma dostęp do ogromnej ilości znajomych i informatorów z całego kraju; pomaga, udziela się i działa dość skutecznie

### Co ma do dyspozycji:

* **sprzęt szpiegowski**: Mimo, że nie jest szpiegiem, ma dostęp do odpowiednich narzędzi i materiałów; jako dziennikarka ma pretekst
* **sprzęt włamywacza**: Cóż. Te wszystkie urządzenia do dostawania się w niewłaściwe miejsca też ma do dyspozycji...
* **sprzęt wspinaczkowy**: Niespodzianka. Maria potrafi poruszać się w różnych miejscach i terenach.
* **sfałszowane dokumenty**: Maria dzięki swym znajomościom zwykle ma do dyspozycji odpowiedniej jakości dokumenty i papiery dające jej uprawnienia
* **sprzęt reportera**: Maria JEST dziennikarką. JEST reporterem. Ma odpowiedni sprzęt...

### Surowce

* **Wartość**: 1
* **Pochodzenie**: mimo dużego powodzenia i sukcesów strasznie dużo pieniędzy wydaje na... wszystko. Informatorów, sprzęt itp.

# Opis

Dziennikarka świata ludzi, która połączyła się soullinkiem z Pauliną Tarczyńską. 

Córka terminusa, kiedyś wiedziała o mocy magicznej, lecz nie zna własnej przeszłości (Paulina też jej nie zna). Bardzo odważna, czasem do przesady. Lekko ryzykancka, idzie przez życie z wyraźnym kompasem moralnym i wykazuje inicjatywę by pomóc Paulinie. Czasem sprawia wrażenie, że zna wszystkich.

## Motto

"Jeśli ja tego nie ujawnię, nikt się tym nie zajmie."

# Historia