---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Nicaretta"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **survivor**: "zrobię WSZYSTKO by przetrwać i pozostać na Primusie"
* **megalomania**: "nie pozwolę, by o mnie zapomniano! Jestem żywa, chcę być żywa, I will NOT fade away!"

### Zachowania (jaka jest)

* **ewangelistka (ukojenie)**: "złagodzę twoje cierpienie i pomogę ci odpocząć…"
* **socjopatka**: "wszyscy: ludzie, magowie, nawet ja; WSZYSCY są tylko surowcami i muszę maksymalizować moje korzyści"
* **zbieg**: "ścigają mnie; chcą mnie kontrolować i dominować. Odzyskać. Nigdy mnie nie złapią i nigdy mnie nie znajdą"

### Specjalizacje

* **ukojenie Arazille** (kapłanka)
* **rozkochanie** (succubus)
* **kuszenie** (succubus, kapłanka)
* **nawracanie** (succubus, kapłanka)
* **kontrola tłumów** (oficer)
* **magia erotyczna** (oficer)
* **dominacja i uwodzenie** (oficer)
* **ukojenie**
* **widzenie pragnień**

### Umiejętności

* **uwodzicielka**
* **aktorka**
* **taktyk**
* **demoniczny oficer**
* **succubus**
* **ucieczka**
* **ukrywanie się**
* **złodziejka**
* **była kapłanka Arazille**
* **masowe rytuały erotyczne**
* **miasto**
* **świat ludzi**
* **świat magów**
* **polityka magów**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |        +1       |        +1        |     +1     |   -1   |      +1       |     -1     |     -1      |

### Specjalne

* **dotyk sukkuba**: +3 do wszelkich testów dominacji i uwodzenia JEŚLI jest styczność ciało-ciało.
* **szpony**: jej ataki nieuzbrojone są traktowane jak ataki z nożem
* **umiejętność latania**: lata bez konieczności używania magii
* **maska człowieka**: +6 do aktorstwa jeśli jest w formie człowieka a nie sukkuba.
* **wiele postaci**: potrafi wyglądać jak 14-latka aż do 60-latki. Zawsze jest ładna.
* **głód energii**: musi kochać się z ludźmi lub magami co najmniej dwa razy dziennie. To wysysa energię celu.
* zniszczenie Nicaretty odsyła ją na Fazę Daemonica, gdzie się rekonstytuuje

## Magia

### Szkoły magiczne

* **biomancja**: opis
* **magia mentalna**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

A sorceress who simply refused to die. A mage broken with the Eclipse. A body broken by those who tried to scan her, heal her, examine her. A mind - a willpower – determined to survive at all costs.

A sorceress who simply refused to die. Even when she should have. A sorceress who tried to summon Arazille to get a God on her side. A sorceress who succeeded.

Arazille has cast away the broken body, damaged magic and bloody addiction. She has built a new shape, a new supporting structure for the mind of the sorceress. The shape made of magical energy. The force of eternal happiness and peace.

Another force appeared. One which wanted to change the sorceress again. A force which wanted to enslave. The willpower – the survivor – agreed. She wanted to live. Her shape was twisted into the one of succubus. Some of Arazille’s programming remained – but the survivor was “free” again.

Time has passed. The force controlling the succubus was defeated or pushed away. The succubus was pushed into Phase Daemonica. Eternal. Tormented. Hungry. Empty. But alive.
Now she’s managed to exit the Phase. She needs the energy; she needs attention to exist. The force is somewhere there, lurking and hoping to attract her. Humans want to love her and control her. Magi want to take over her or to destroy her.

She refuses to die. She has skills, shards of her former personality and memory and she will not lose.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Henrykiem|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170120|okazała się świetnym taktykiem, twardym negocjatorem i niezłym wojownikiem. I tak przegrała - weszła w niekorzystny tymczasowo sojusz. Nawet Arazille nie pomoże...|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170214|gdzie mag nie może, tam sukkuba pośle - od infiltracji domku po wyciąganie serc z martwych czarodziejek. Bardzo, bardzo nie opłacał jej się ten sojusz...|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|10/07/26|10/07/29|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161110|zastawiająca pułapki (gra by zabić) i przeskakująca po hostach demonica, która myśli, że skutecznie zmyliła ewentualny pościg. Bardzo inteligentna.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161103|succubus, taktyk i corruptor. Znajdowała się między Fazami, inkarnowała się w zakonnicy dzięki podszeptom z Drugiej Strony. Zmuszona do ucieczki przez Henryka.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|4|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|2|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1705-marzena-dorszaj.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Fiodor Pyszczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-pyszczek.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Filip Gładki](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-gladki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
