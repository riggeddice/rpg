---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
title: "Małż Poszukiwacz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

- odnaleźć i uratować utracone małże
- dowiedzieć się jak najwięcej o ludziach i magach

### Specjalizacje

- dominacja / kontrola
- leczenie umysłów
- wzbudzanie sympatii
- prąd elektryczny

### Umiejętności

- małżowa wiedza o ludziach i magach
- ukrywanie swojej obecności
- korzystanie z wiedzy rumaka
- perswazja

### Siły

- działanie przez zdominowanych agentów
- potężna wola
- rumaki chętnie współpracują

### Słabości

- słaby
- powolny bez rumaka
- mały

### Specjalne

- małżowy link mentalny

## Magia

### Szkoły magiczne

- **magia żywiołów**:

## Zasoby i otoczenie

### Powiązane frakcje

### Kogo zna

### Co ma do dyspozycji

### Surowce

# Opis

### Koncept

### Motto

"Jesteśmy jednością"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170725|małż. Skakał między ciałami przez dotyk. Z pomocą reszty zdominował maga, Skaził Jodłowiec i uciekł z ciałem maga.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1707-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1707-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
