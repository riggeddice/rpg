---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
owner: "public"
title: "Lucjan Kopidół"
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Aspekty_: rodzina, kolekcjonowanie, intuicja
    * _Szczególnie_: dogłębnie rozpatrz temat uwzględniając dane, NIE skupiaj się by zdążyć na czas, gromadź WSZYSTKIE informacje, NIE uznaj że część faktów jest nieistotna, przedkładaj rodzinę nade wszystko, NIE próbuj zapewnić sobie dobry byt
    * _Opis_: 
* **Społeczne**:
    * _Aspekty_: wybaczanie, współdziałanie, dane
    * _Szczególnie_: daj każdemu szansę nieważne kim jest, NIE skreślaj nikogo za przeszłość, promuj rozpatrywanie problemu przez wszystkie dane, NIE promuj decyzji przez procesy i kulturę
    * _Opis_: Kopidół bywa dobijający, bo nawet najprostsze rzeczy chce rozpatrywać "od nowa", bo jakiś nieistotny aspekt się zmienił. To też sprawia, że każdemu chce pomóc i każdemu chce dać nowe szanse - bo coś się zmieniło.
* **Serce**:
    * _Aspekty_: pacyfista, relacje
    * _Szczególnie_: dbaj by wszyscy cię lubili, NIE dąż do sukcesu za wszelką cenę, zawsze zakładaj dobrą intencję, NIE fakty skreślają osobę, pomagaj nie oczekując wzajemności, NIE wyjdź na swoje
    * _Opis_: Naiwny, nieżyciowy, dobrotliwy Kopidół jest magiem, którego łatwo wykorzystać. Unika jakichkolwiek form walki czy konfliktu, acz nie można nazwać go człowiekiem bez energii. Trochę taki szalony naukowiec pragnący czynić dobro.
* **Działanie**:
    * _Aspekty_: cichy, intuicja
    * _Szczególnie_: poluj na iskrę natchnienia nieważne gdzie nie jest, NIE metodycznym działaniem rozwiążesz problem, najlepiej by nikt nie zauważył że jesteś, NIE bierz sukcesu na siebie
    * _Opis_: 

### Umiejętności

* **Manager Inwestycyjny**:
    * _Aspekty_: inwestor, manager
    * _Manewry_: pomnażanie bogactwa przez bardzo dużo dziwnych danych, szybka praca grupowa przez zarządzanie projektem
    * _Opis_: 
* **Analityk**:
    * _Aspekty_: analityk, wizualizator, naukowiec
    * _Manewry_:  przekonywanie przez wizualizację danych, wykrycie anomalii przez mnóstwo danych
    * _Opis_: 
* **Poeta**:
    * _Aspekty_: poeta, wróżbita
    * _Manewry_: wrażenie nieistotnego przez kiepską poezję, pokazanie nastroju w punkt przez poezję, 
    * _Opis_: 

### Silne i słabe strony:

* **Zrozumie niezrozumiałe**:
    * _Aspekty_: obcy umysł, dziwak
    * _Manewry_: znajduje niewidzialne powiązania, dziwność jego domem, bardzo odporny na ataki mentalne, znajduje powiązania których nie ma :-(, niezbyt poważnie traktowany
    * _Opis_: Kopidół jest troszkę viciniuskim magiem. Nie do końca rozumie, czemu coś rozumie i nie do końca myśli jak zwykły mag. Bez problemu adaptuje się do najdziwniejszych sytuacji i otoczeń; bardzo trudno zmylić go magią zmysłów. Jednocześnie znajduje rzeczy nieznajdowalne przez innych. Ale... czasem znajduje rzeczy, których nie ma - i to się dzieje częściej niż rzadziej.

### Szkoły magiczne

* **Magia Zmysłów**:
    * _Aspekty_: wizualizacja danych, inne przedstawienie informacji, adaptacja do dziwności
    * _Manewry_: 
    * _Opis_: 
* **Magia Mentalna**: 
    * _Aspekty_: inne przedstawienie informacji, wzmocnienie intuicji, adaptacja do dziwności
    * _Manewry_: 
    * _Opis_: 
* **Biomancja**:
    * _Aspekty_: wzmocnienie zmysłów, adaptacja do dziwności
    * _Manewry_: 
    * _Opis_: 

## Zasoby i otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zasoby

* **Ogromna biblioteka poezji**:
    * _Aspekty_: kultura wysoka, nastroje
    * _Manewry_: wzbudzenie wrażenia bardzo kulturalnego przez znajomość i dobranie, przekazanie dyskretnego sygnału przez wiersz, przekonywanie przez trafną poezję
    * _Opis_: 
* **Zaawansowane narzędzia wizualizujące**:
    * _Aspekty_: wnioskowanie, przekonywanie
    * _Manewry_: 
    * _Opis_: 
* **Znajomość wielu lokalnych firm**:
    * _Aspekty_: luźne kontakty, analiza rynku
    * _Manewry_: 
    * _Opis_: 
* **Niemile widziany w okolicy Świecy**:
    * _Aspekty_: kiepska reputacja
    * _Manewry_: 
    * _Opis_: 

# Opis

Lucjan Kopidół w pewien sposób jest maskotką KADEMu, magiem nie do końca traktowanym poważnie. 

W "poprzednim życiu" był inwestorem pracującym dla anioła biznesu, niejakiego Dariusza Larenta. Kopidół miał za zadanie znajdować niewielkie firmy, w które Larent może zainwestować. I znajdował je z powodzeniem, bazując na swojej niesamowitej intuicji - do momentu, aż odrzucił firmę maga Świecy, Wojciecha Winniczka. Ten użył magii by zmusić Kopidoła do podjęcia innej decyzji i doszło do Czegoś Dziwnego - w Kopidole obudziła się "krew viciniusa".

W wyniku wszystkiego Winniczek utracił magię a Kopidół ją uzyskał, wzmacniając swoje naturalne umiejętności i zdolności. Od razu opadli go magowie Świecy i chcieli go badać, sprawdzać, ratować Winniczka... ogólnie, życie Kopidoła się bardzo pogorszyło.

To jest, do momentu, aż do akcji wkroczył Robert Seton i wykonał efektowny atak na Świecę by porwać Lucjana Kopidoła. Trzeba przyznać, że Kopidoła NIE TRZEBA BYŁO EFEKTOWNIE PORYWAĆ, bo Świeca nie była nim aż tak zainteresowana, ale nie poprawiło to naturalnej ostrożności Lucjana (ani jego kondycji psychicznej). Cała ta sprawa doprowadziła do tego, że Lucjan Kopidół, jakkolwiek znalazł miejsce na KADEMie (i to jako mag a nie królik doświadczalny) zachowuje się z dużą ostrożnością i nieufnością wobec całego tego skomplikowanego świata magów.

Jest to szczęśliwie ożeniony i dzieciaty mag KADEMu pochodzący z innego, łagodniejszego świata. Troszkę nie pasuje do KADEMu kulturowo. Mag Pryzmaturgii, wierzący w intuicję, nastroje i analizę sztuki i decyzji celu, by móc określić przyszłość. Nieco taki obłąkany wróżbita jak się go bliżej pozna, acz wyciszony i nie narzucający się na co dzień. Ma słabość do kobiet i unika konfliktów, bardzo łatwo mu wejść na głowę.

Uwielbia wręcz wizualizować Pryzmat czy dane wszelkiego typu, przez co często pracuje z Andżeliką (Karina dowcipkuje, że Kopidół się w niej podkochuje). Doskonały analityk, który potrafi długo obserwować dany byt, by - w niezrozumiały dla nikogo sposób - rozwiązać problem. Oczywiście, zdarza się to raz na sto wypadków, więc nie jest zbyt wiarygodny; zależy, czy przyjdzie do niego inspiracja. Pacyfista, wierzy w rozwiązywanie problemów przez zrozumienie drugiej osoby. Silnie relacyjny.

52 lata, ma w Kopalinie rodzinę, żonę i teściów. Bardzo rodzinny, acz też trochę z dystansem, by nikogo nie skrzywdzić. Jest w nim ZERO złośliwości, raczej dobrotliwość i pustka znamienna dla Pryzmatyków.

Czasami z Warmasterem wspólnie rywalizują w pisaniu wierszy, ku wielkiej rozpaczy Whisperwind i radości Kariny, która próbuje zrobić Stały Wieczorek Poezji Na KADEMie przez radiowęzeł.

### Koncept

Doktor Who (Capaldi) + Elwira + Thrawn (aspekt "sztuka")

### Mapa kreacji

brak

### Motto

""

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170222|jego wiedza posłużyła do stworzenia efemerydy Kuby Urbanka by ów chronił Renatę Souris.|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|10/08/14|10/08/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170125|zmontował i załatwił Hektorowi, Mariannie i Annie dampery pryzmatyczne, by ratować KADEM przed wizjami...|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|którego artefakt astralno-pryzmatyczny eksplodujący na KADEMie spowodował mnóstwo problemów i doprowadził do 'breach'.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|mag-renegat (?) KADEMu, astralika x pryzmat. 52 lata.|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|KADEMowiec podatny na piękne Diakonki.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|który w jakiś sposób zniszczył ten Dwunastościan... który przecież Whisper nazwała 'niezniszczalnym'.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|mag Instytutu Pryzmaturgii dowodzący projektem zniszczenia Dwunastościanu. Raczej z głową w chmurach; jedyny ekspert od Pryzmatu w całym zespole.|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-siluria-diakon.html)|7|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1801-ignat-zajcew.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-melodia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Echo Jakuba Urbanka](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-jakuba-urbanka.html)|1|[170222](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-zupan.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
