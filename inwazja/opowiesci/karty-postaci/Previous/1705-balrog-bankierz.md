---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Balrog Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **hulaka**: "A cholera wie, ile jeszcze będziemy żyć; kolejne Zaćmienie może wszystko rozwalić."
* **opiekuńczy**: "Jak już na kimś mi zależy, ona będzie miała gwiazdkę z cholernego nieba którą jej własna rywalka przyniesie jej na cholernych kolanach"
* **wróg - Zajcewowie**: "Ogniste ptaszki też skwierczą jak się je odpowiednio poddusi. Najlepiej publicznie."

### Zachowania (jaka jest)

* **oportunista**: "Trzeba korzystać z okazji, bo cholera wie, kiedy pojawi się kolejna okazja. Agresywnie."
* **próżny**: "Mam świetne ciało, dużo sukcesów i ogólnie jestem niezrównany. Jak można tego nie doceniać?"
* **kobieciarz**: "Sława i kobiety! Dwie rzeczy, które sprawiają, że jest po co żyć."
* **przebojowy**: "Na szczyt, kamraci! Za mną! Wszystko się da. Co, nie zagadasz do niej? To ja zagadam."

## Co umie (co postać potrafi):

### Specjalizacje

* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **gladiator**
* **katai**
* **kulturysta**
* **showman**
* **prowokacja**
* **wygadany**
* **charakteryzacja**
* **marketing**
* **przywódca**
* **trener gladiatorów**
* **wystąpienia publiczne**

### Szkoły magiczne

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |         0        |     -1     |   -1   |      -1       |     +1     |     +1      |

### Specjalne

**brak**

## Magia

### Magia dynamiczna:

* jaki typ magii (typy akcji) zwykle stosuje?
* np. iluzje, magia pogodowa...

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **Sława hojnego kobieciarza i hulaki**
* **Sława gladiatora i wyszczekanego showmana**
* **Liczne eks-, fanki i przyjaciółki**
* **Grupa zaciekłych fanów**
* **Powszechnie znany w różnych knajpach i lokalach**

### Co ma do dyspozycji:

* **Dużo nietypowych 'broni' gladiatora**
* **Agencja reklamowa Świecy**
* **Dostęp do menażerii i egzotycznych istot**

### Surowce

* **Wartość**: 2
* **Pochodzenie**:
dość bogaty i wpływowy; utrzymuje się z fanów i inwestycji
+1 kieszonkowego do wydania na dowolną Sławę lub Fanów
PRZYMUS wydania 1 na wydarzenie typu impreza itp. To hulaka ;-).

# Opis
An almost 40 years old gladiator, who moved into more of a leadership role. This guy is extremely focused on his influence on others, especially young girls due to obvious reasons. Might and glory would be his motto. A very sturdy and nimble sportsman, he uses his looks, history and abilities to remain on top even if he is not the best warrior out there.

The viewers usually want a performance. He gives them a performance. He fights in a very spectacular, flashy way. He can make some inspiring speeches; he can talk the talk and he will walk the walk. Not the most inspiring of the best leaders around, not even a good leader, but he tries to insert himself into every advantageous situation and get something out of it. And then, boast about his stuff to never become forgotten.

The Eclipse scared him. He has seen many better magi fall into disgrace. He considers himself lucky and intend to live his life fully. You never know when another Eclipse is going to strike…

## Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170115|lansuje się kosztem Ignata, ale czas powoli zejść z tego konia; wyekstraktował całość wartości jaką był w stanie na tym etapie. Łaskawie wycofał pojedynek.|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|oportunistyczny wojownik i showman, który zdecydował się wypłynąć i zdobyć popularność na fali podłego uczynku Ignata |[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Netheria Diakon|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Karolina Kupiec|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Ignat Zajcew|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Bogumił Rojowiec|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Urszula Murczyk|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Quasar|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Paweł Sępiak|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Melodia Diakon|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Kornelia Kartel|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Judyta Maus|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Jolanta Sowińska|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Joachim Kopiec|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Joachim Kartel|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Jan Wątły|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Infernia Diakon|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Ilona Amant|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Franciszek Maus|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Elizawieta Zajcew|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Eis|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Adonis Sowiński|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
