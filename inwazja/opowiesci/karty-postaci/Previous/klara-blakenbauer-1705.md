---
layout: inwazja-karta-postaci
title:  "Klara Blakenbauer"
categories: profile
guild: none
type: PC
---

# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **lojalna wobec Tymoteusa**: Tymoteus ją uratował, nie narazi go
* **szalony naukowiec**: uwielbia eksperymentować i się bawić z rzadkimi technologiami. Im bardziej szalony pomysł, tym lepiej!
* **kolekcjonerka sprzętu**: cokolwiek to jest, na pewno mogę to wykorzystać
* **geek**: uwielbia fajne (niekoniecznie nowe, ale _fajne_ ) zabawki
* **niech nikogo nie spotka to, co mnie**: po tym, jak nieudany eksperyment omal ją nie zabił, Klara zrobi co w jej mocy, aby nikt iiny nie padł ofiarą podobnej sytuacji

### Zachowania (jaka jest)

* **nieustępliwa**: "nie takie rzeczy widziałam"
* **skupiona na efektywności**: "skoro już coś robimy, niech to ma sens"
* **nie takie rzeczy widziałam**:

### Specjalizacje

* **roje**
* **inkarnacja**: zdalne sterowanie
* **zabezpieczenia**
* **sztuczna inteligencja**
* **biotechnika** - połączenie technologii i biologii rządzi!
* **neo biblioteka aleksandryjska**
* **rozpraszanie magii**
* **golemancja**

### Umiejętności

* **elektronik**: tak zarabia na życie i to lubi
* **geek**: zna się na najdziwniejszych ludzkich wynalazkach i jest nimi zafascynowana
* **"wszystkie koty moje są"**: jeśli to jest na sieci, to ona to znajdzie
* **majsterkowanie**: nie ma to jak zbudować coś samodzielnie
* **bale i imprezy**: 
* **stroje i przebieranki**: 
* **plotki**: w końcu geek czy nie, poplotkować też lubi
* **ród Weiner**: pierwotnie Weiner, wie o swoim dawnym rodzie bardzo dużo
* **pierwsza pomoc**: nawet Klemensowi nie zawsze uda się wyłgać, a wtedy trzeba poskładać...


### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         0       |         0        |     +1     |   +1   |      +1       |     -1     |     -1      |

### Specjalne
**brak**

## Magia

### Szkoły magiczne

* **technomantka**
* **biomantka**: tego nauczyła się już po zostaniu Blakenbauerką. Odkryła nieoczekiwane dla niej zastosowania.
* **infomantka**
* **katalistka**

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* Blakenbauerowie

### Kogo zna

* **Weinerowie z okolic Lublina**: wie o nich wiele, ale oni jej nie znają

### Co ma do dyspozycji:

* **wyposażenie elektroniczne**
* **warsztat w rezydencji**

### Surowce

* **Wartość**: 2
* **Pochodzenie**: Nie jest szczególnie bogata czy biedna. Jeśli trzeba, skorzysta z zasobów rezydencji, ale zwykle w zupełności wystarcza jej to, co zdobywa pracą własnych rąk

# Opis
Kiedyś nazywała się Klara Weiner. Razem z członkami rodu przeprowadzała ekperyment w podziemnym kompleksie, kiedy wszystko wybuchło im w twarz. Inni myśleli, że Klara nie żyje, uznali, że wejście w miejsce, gdzie się znalazła jest zbyt niebezpieczne… Zostawili ją.
Ocknęła się dużo później. Uwięziona, ranna, chora. Magią utrzymała się przy życiu, jednak nie potrafiła się wydostać na własną rękę. Próbowała uparcie, ale nie miała jak przełamać uszkodzonych zabezpieczeń kompleksu.
Jedyne, co zdołała zrobić, to rozesłać rój robocików, by szukały wyjścia i ratunku, choć nie miała większej nadziei - kompleks był w odludnym miejscu.
Po długim czasie - ona nie potrafi określić, jak długim - znalazł ją Tymoteus, znalazłszy jednego z robocików.
Uratował ją i wyleczył.
Klara straciła wiarę w swój ród. W końcu porzucili ją w potrzebie, a Tymoteus ją uratował. Stopniowo, krok po kroku, Klara wypytywała Tymoteusa o ród Blakenbauerów, o niego samego, jednocześnie pomagając mu w wielu sytuacjach. W końcu sama poprosiła, czy nie przyjąłby nowego członka rodu…

## Motto

"Tekst"

# Historia