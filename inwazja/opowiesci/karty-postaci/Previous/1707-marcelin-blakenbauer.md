---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Marcelin Blakenbauer"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Chcę być najlepszym mężczyzną każdej kobiety**: Nie chodzi tutaj tylko o sex. Marcelin chce być postrzegany jako ideał męskości. Perfekcyjny wybór każdej kobiety. Kochanek, przyjaciel, towarzysz, partner.
* **Nie porzucę damy w opresji**: Czasami nawet ślepo rusza na ratunek, a gdy już mu smok poparzy tyłek, to szuka wsparcia u rodziny i przyjaciół
* **Rodzina ponad wszystko**: W jego genach tkwi konieczność wspierania i ochrony swojego rodu i rezydencji.
* **Pociąg do wyjątkowych kobiet**:  Czuje potrzebę przedłużenia ciągłości rodu, a jego naturalnym wyborem są kobiety o najlepszym materiale genetycznym. Nie robi tego świadomie.
* **Chcę zaimponować Hektorowi**: Hektor jest jego najstarszym bratem. Od dzieciństwa Marcelin patrzył na niego z podziwem, ale nie dawał tego po sobie poznać. Marzy mu się, że kiedyś Hektor zobaczy w nim jego prawdziwy potencjał.

### Specjalizacje

* **Alchemia**: przygotowywanie magicznych mikstur wpływających na ciało - dekoktów, odwarów i filtrów
* **Herbalistyka**: znajomość i uprawa egzotycznych roślin alchemicznych
* **Transmutacja**: łączenie materii w celu uczynienia jej lepszą, lub nadaniu jej specjalnych właściwości. Wiąże się zazwyczaj z tworzeniem kręgów alchemicznych
* **Feromony**: wszystko co przyciąga istoty ku sobie
* **Budowanie pozytywnego wizerunku**: zwłaszcza o swoim rodzie


### Umiejętności

* **Magiczne laboratoria**: ma szeroką wiedzę na temat magicznych instrumentów swojego fachu
* **Uwodzenie**: wie jak oczarować kobietę
* **Zdobywanie przyjaciół**: ludzie lgną do niego. Wie jak podejść do każdego człowieka/maga/vicinusa
* **Dusza towarzystwa**: umie rozkręcić każdą imprezę. Nie ma przy nim czegoś takiego jak niezręczna cisza.
* **Biochemia**: duża wiedza na temat organizmów żywych i tego jak reagują z substancjami chemicznymi
* **Magia luster**: wiedza o tej zakazanej i niebezpiecznej magii
* **Świat ludzi**: lubi przebiwać wśród ludzi, gdzie ma też wielu przyjaciół
* **Marketing**: zagada, zachęci, wciśnie wszystko

### Siły

- diakońsko przystojny
- nie bawi się w pół-środki
- wzbudza sympatię

### Słabości

- trochę naiwny
- ogromne opory przed skrzywdzeniem kobiet
- rodzina


### Specjalne

* **Klątwa Blakenbauerów**: ze śmiercią mu nie po drodze. Bestia Marcelina ma niezwykłe moce adaptacyjne. Przybiera formę odpowiednią do danej sytuacji, ale najczęściej gadzią.
* **Feromony rezydencji**: ciężko mu się oprzeć
* **Ojciec ma do niego słabość**

## Magia

### Szkoły magiczne

* **Biomancja**
* **Magia materii**
* **Magia zmysłów**
* **Magia mentalna**

## Zasoby i otoczenie

### Powiązane frakcje

### Kogo zna

### Co ma do dyspozycji

### Surowce

# Opis

### Koncept

### Motto

"Z pięknem utożsamiam sztukę i kobiety"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|ma oddaną przyjaciółkę w Judycie Maus.|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170823|ukochany syn tatusia (Ottona), przekonał go, by Otton znalazł informacje o autowarze dla Blakenbauerów. Też: twórca toksyn przeciw autowarowi. Pocieszył Judytę Maus.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170607|ukochany syn tatusia (Ottona), przekonał go, by Otton znalazł informacje o autowarze dla Blakenbauerów. Też: twórca toksyn przeciw autowarowi.|[Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|10/08/23|10/08/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|dla odmiany dobry oficer łącznikowy między Ozydiuszem a Blakenbauerami. |[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|który został oficerem łącznikowym u Ozydiusza (Leo ma nadzieję, że przekabaci jakieś ładne terminuski...).|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|który prawidłowo wykorzystał swoje przyjaciółki... tylko by wpaść w pierwszą pułapkę Leonidasa.|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|działający z poszerzonych uprawnień Świecy. Dowodzi siłami płaszczek rozbijających ogniska Irytkowego oporu.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151212|który zdecydował się zaatakować Hektora by go obezwładnić. Uciekł z Rezydencji z Wandą, by "ratować swój ród".|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151202|który znalazł cennego sojusznika dla Hektora (Esme Myszeczkę). Hektor się już boi. Casanova Kopalina.|[Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|10/06/15|10/06/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151110|który jest całkowicie bezpieczny od oskarżeń Inferni i Diakonów dzięki Mojrze i Romeo.|[Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150819|który pokazał zęby, postawił się wszystkim i doprowadził do odbudowania sojuszu z Kurtyną. Bo Leokadia weń wierzy.|[Krótki antyporadnik o sojuszach](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html)|10/05/17|10/05/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150728|który zaufał Leokadii (swojej przyjaciółce a nie kochance), załatwił jej merkuriasza, wprosił się na bal Emilii... ogólnie, kłopoty.|[Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|który dostarczył kluczowych informacji o Tymku, Gali i całej sprawie; ma przyjaciółkę terminuskę SŚ z którą nie spał.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150704|który okazał się być mistrzem dobierania stroju dla Hektora.|[Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|10/05/11|10/05/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|podsądny który się katastrofalnie wpakował między Infernią a Emilią.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150411|kochanek, który kręci z o jedną dziewczyną za dużo w nieodpowiednim momencie... i rozdaje niegroźne artefakty na bezsenność.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|który zmartwił się dziwnym zachowaniem charyzmatycznej cybergothki i powiedział o tym Hektorowi (który go olał aż powiązał to z aptoformem).|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170707|master lekarz i resuscytator; uratował Henryka i badał Supernową. Wybadał Skażony fragment eliksiru i sporo robi dla dziewczyn.|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|10/03/06|10/03/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|mający mnóstwo przyjaciółek (min. Infernię na KADEMie), pomagający Hektorowi zdobyć informacje od Inferni i od Rukoliusza. Społeczny i pomocny.|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170808|(Dust), dostarczył modelce perfumy co doprowadziło do obudzenia gestalta opery... na szczęście, rozmontował sprawę z Piotrem. Robił eliksiry, był w swoim żywiole.|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|10/02/17|10/02/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|który ma dostęp do kanałów Srebrnej Świecy i poinformował Hektora o stanie wyjątkowym.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140618|który chciał ostatni raz bronić Sophistii przed Hektorem (co ma DiDi lub Elżbieta a nie ma Sophistia), ale Hektor go zdominował i kazał mu pilnować Dracenę.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140611|pogrążony głęboko w żałobie po śmierci Ottona. Przez to nie pomyślał o Sophistii.|[Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|10/01/15|10/01/16|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|150115|tracący ciężarówkę mag pokazujący umiejętności bojowe.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140208|twórca dziewczynki tropiącej który umożliwił Andrei jej wypożyczenie do znalezienia Tatiany.|[Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|150210|który mimo, że Diana z nim zerwała dalej jest skłonny jej pomagać i jest Dianą zafascynowany. |['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141216|podręczne narzędzie dostępowe do hipernetu.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140401|wsadzony do aresztu za obelżywe graffiti i zmuszony przez Hektora do uratowania Edwina; doprowadził do uruchomienia Ottona i URATOWAŁ Edwina.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140121|osoba bardzo zmartwiona zniknięciem Sophistii do tego stopnia, że zmusił całą rodzinę by Coś Zrobili by ją uratować. I ją uratował.|[Zniknięcie Sophistii](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140320|który przepraszał Hektora za podsłuchującą Sophistię, lecz powiedział mu, że tylko Hektor traktuje Sophistię jak narzędzie.|[Sprawa magicznych samochodów](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|Don Juan który ma dobry gust, prawie wyzywa Bolesława Bankierza na pojedynek i płacze publicznie po tym jak Andrea rozkwasiła mu ucho.|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140227|który jest zamrożony w krysztale by uratować Sophistię. Uratował jej życie ryzykując swoim.|[Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140819|pokorny mnich chcący zostać w klasztorze. Potencjalnie, dla pewnej dziewczyny.|[Marcelin w klasztorze!](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|140213|nieprzytomna i ranna ofiara viciniusa służąca za przynętę dla Edwina (a w pułapkę wpadł Hektor).|[Pułapka na Edwina](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|10/01/01|10/01/02|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|28|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|14|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|12|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|10|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|9|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|7|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|7|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|7|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|6|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|6|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|6|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|6|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|6|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|6|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|5|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|5|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|5|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|5|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|4|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|4|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|4|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|3|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|3|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|2|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-krystalia-diakon.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|2|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Żaneta Kroniacz](/rpg/inwazja/opowiesci/karty-postaci/9999-zaneta-kroniacz.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Zofia Łaziarak](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-laziarak.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Zdzisław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislaw-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Wojciech Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-tecznia.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Wojciech Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wiktor Lubaszny](/rpg/inwazja/opowiesci/karty-postaci/9999-wiktor-lubaszny.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Weronika Piniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-weronika-piniarz.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Waltrauda Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-waltrauda-werner.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Timor Koral](/rpg/inwazja/opowiesci/karty-postaci/9999-timor-koral.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Roksana Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-roksana-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Przemysław Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-przemyslaw-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Paweł Grzęda](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-grzeda.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Patryk Kloszard](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-kloszard.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Pasożyt Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-pasozyt-diakon.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Onufry Puzel](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-puzel.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Mikołaj Młot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mlot.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-melodia-diakon.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mariusz Garaż](/rpg/inwazja/opowiesci/karty-postaci/1709-mariusz-garaz.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Marek Rudzielec](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-rudzielec.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Maciej Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-kwarc.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kopidol.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Liliana Sowińska](/rpg/inwazja/opowiesci/karty-postaci/9999-liliana-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Kaspian Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-bankierz.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-kupiec.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Kamil Rzepa](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-rzepa.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jessica Czułmik](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-czulmik.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Janusz Wybój](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wyboj.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Jakub Ryjek](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-ryjek.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-ignat-zajcew.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Himechan](/rpg/inwazja/opowiesci/karty-postaci/9999-himechan.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Hieronim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-hieronim-maus.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Fryderyk Mruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-mruczek.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Filip Szorak](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-szorak.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Ferdynand Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ferdynand-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Felicja Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Ewa Kroideł](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-kroidel.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Erebos](/rpg/inwazja/opowiesci/karty-postaci/9999-erebos.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dominik Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Damian Bródka](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-brodka.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Brunon Czerpak](/rpg/inwazja/opowiesci/karty-postaci/9999-brunon-czerpak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Bianka Drażyńska](/rpg/inwazja/opowiesci/karty-postaci/9999-bianka-drazynska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Beata Zakrojec](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-zakrojec.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Arkadiusz Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Antoni Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-szczesliwiec.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
