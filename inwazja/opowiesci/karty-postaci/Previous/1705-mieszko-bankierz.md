---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Mieszko Bankierz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **rodzinny**: "kariera jest niczym w obliczu Laureny, Kataliny i Malii"

### Zachowania (jaka jest)

* **działa osobiście**: "jeśli chcesz coś zrobić dobrze, to ja to zrobię"
* **enforcer - Świeca**: "mam uprawnienia do działania dla Świecy i Rodu. Nie masz czym mnie zatrzymać"
* **karierowicz**: "życie jest jedno, a rodzinie byt trzeba zapewnić"

### Specjalizacje

* **narzucanie woli**
* **ciche zabójstwa**
* **dobór sojuszników**
* **walka z magami**

### Umiejętności

* **terminus**
* **twardy przywódca**
* **twardy polityk**
* **administrator**
* **głęboka znajomość Świecy**
* **black ops**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        +1          |        +1       |        -1        |     +1     |   -1   |      -1       |     +1     |     -1      |

### Specjalne

* niezrównany wojownik: +2 w walkach

## Magia

### Szkoły magiczne

* **magia zmysłów**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* lojaliści

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Mieszko, 44 years old, used to be a career terminus, one which done stuff fast and efficiently. However, he was not able to advance ranks in Warsaw. He moved to Silesia region and between his skills and his loyalty he has managed to claw himself quite high. An apt politician and a warrior, he was destined for greatness.

At one point he got mortally wounded. He got saved by the combat medic who returned for him, even if there wasn’t a lot of chance that he would be alive and even if she has treated people as pawns before. This changed his approach to life.

He mellowed a bit, took more administrative tasks and tried to become a leader simply because he had something unique – he was able to control the career terminus because he knew how they think, he was one of them. He did marry that combat medic and afterwards he married another mage.

His former close ally, Ozydiusz, got propelled higher because he kind of stopped racing towards the highest level. From Ozydiusz’s point of view it is actually quite good that Mieszko became a “family terminus” (which is supposed to be an insult). Mieszko doesn’t mind - both of them cooperate quite closely especially now that they do not really engage in rivalry against each other.

He has a daughter he loves like nothing else in the world – Malia.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170226|w pełni stoi za Andreą. Powstrzymuje swój Mausożerny apetyt na chwałę Świecy i Andrei - ona uratowała jego partnerki.|[Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|10/08/18|10/08/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170122|który był skłonny oddać życie dla swoich partnerek. Szczęśliwie posłuchał Andrei - i odzyskał Laurenę i Katalinę.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170103|ochraniał Zespół przy portalu, nieświadomie uniknął losu "radzieckiego ochotnika" jako awatar Arazille.|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|10/07/16|10/07/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170101|"posiłek dla kralotha" oraz - na życzenie Andrei - przez Laragnarhaga przekształcany, by CHCIAŁ poświęcić się by wezwał Arazille. Przygotowywany na bohatera.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161231|napromieniowany przez Węzeł paranoi i gniewu. Ma być (tymczasową) karmą dla kralotha by zmylić Karradraela ;-).|[Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|10/07/11|10/07/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|cel ataku Czelimina. Wpierw Pryzmatyczny Atak Krwi, potem Katalina... ale skończył nieprzytomny w Skażonym Węźle.|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|10/07/08|10/07/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|zdrowy rozsądek ze Świecy, co Andrea bardzo docenia. Przywołał czołg z Esuriit do Czelimina, powodując poważne kłopoty Karradraelowi. Niestety, zdradził się.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|solidny pierwszy oficer. Odbił Dalię, instalował zabezpieczenia, ostrzegł Andreę przed problemami Barana... idealny zastępca.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161113|twardy i kompetentny terminus skłonny do udzielania dobrych i konkretnych rad. Wraz z Julianem wydedukował mortalisy w Czeliminie.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|który mimo odstawienia na boczny tor przez Andreę niejednokrotnie udowodnił dobrą wolę i lojalność.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161030|chciał dobić zdrajcę... a to się okazało, że Andrea jest Lady Terminus Kopalina. Przysiągł wierność.|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160417|dla odmiany kompetentny przywódca Piroga Górnego; odkrył z pomocą Andrei korupcję. Ojciec Malii, partner Laureny i Kataliny.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|znacząca postać rodu Bankierz na Śląsku i w SŚ oraz agent Inwazji. Wspiera silnie Grzegorza Czerwca po "zdradzie" Agresta.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|13|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|8|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-rafael-diakon.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1705-tadeusz-baran.html)|7|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|6|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|5|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-tatiana-zajcew.html)|5|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-melodia-diakon.html)|5|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|5|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|5|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|5|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|4|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|4|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|3|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|2|[161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Wojciech Żądło](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-zadlo.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-vladlena-zjacew.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-krystalia-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-zupan.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Abelard Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-abelard-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
