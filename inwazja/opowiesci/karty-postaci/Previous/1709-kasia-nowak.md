---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "kić"
title: "Kasia Nowak"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **BÓL: ścigana przez magów, odrzucona przez ludzi**:
    * _Aspekty_: 
    * _Opis_: Kasia jest czymś dziwnym w obu światach, z którymi ma styczność. Nie może zostać w jednym miejscu, nie może mieć normalnej rodziny, a jej przyjaciele są zagrożeni przez sam fakt znajomości z nią. Kasia jest istotą społeczną, pragnącą kontaktu, jednak los zmusił ją do ucieczki i ukrywania się. Udało się jej coś dla siebie wykroić, ale jeśli i to straci, to niewiele pozostanie. Będzie bronić rodziny i przyjaciół za wszelką cenę.
* **FIL: tylko ja mogę coś z tym zrobić**:
    * _Aspekty_: 
    * _Opis_: Kasia choć wie, że z magią nie ma większych szans, nie potrafi zostawić problemów z magią samym sobie. Działa często na granicy własnego bezpieczeństwa bo wie, że zwykli ludzie nie mają żadnych szans
* **MET: magami też można sterować**:
    * _Aspekty_: 
    * _Opis_: skoro już i tak mnie obserwują, to niech coś zrobią. Kasia zdaje sobie sprawę z tego, że niektórzy magowie zwrócili na nią uwagę i ją obserwują. Dlatego też często pozostawia im wskazówki kierujące ich na te problemy, których sama Kasia nie może rozwiązać.

### Umiejętności

* **Artysta**:
    * _Aspekty_: malarz, przekazanie uczuć sztuką, ukryty przekaz, odwzorowanie pryzmatu
    * _Opis_: 
* **Kłamca**: 
    * _Aspekty_: zacieranie śladów, ukrywanie prawdy, niedomówienia, wprowadzanie w błąd, plotki i pogłoski, kontrolowana ofiara zaklęć mentalnych, samokontrola nawet pod wpływem zaklęć
    * _Opis_: 
* **Okultysta**:
    * _Aspekty_: wiedza o magii, rzadkie rytuały okultystyczne, poszukiwanie informacji w dokumentach (biblioteki itp)
    * _Opis_: 
* **Barman**:
    * _Aspekty_: wyciąganie zwierzeń, plotki i pogłoski, dobieranie trunku do osoby
    * _Opis_: 
* **Survival**:
    * _Aspekty_: kierowca, jazda w trudnych warunkach, ukrywanie się, opanowana w stresowej sytuacji
    * _Opis_: 	
    
### Silne i słabe strony:

* **pozornie zwyczajny człowiek**:
    * _Aspekty_: ostrożna, ciekawska, niepozorna, odważna, odporna na magię mentalną, magia luster
    * _Opis_: 
* **zainteresowanie bogów**: 
    * _Aspekty_: bogowie często Kasi pomagają, a ona nie wie czemu... boi się ich
    * _Opis_:

### Specjalne

* **odporna na magię mentalną**: zaklęcia mentalne (zwłaszcza wymazanie pamięci) działają, ale krótko
* **magia zawsze ją znajdzie**: zwykle wpakowuje się na jakieś magiczne wydarzenia
* **nigdy nie zapomina**: wystarczy rzut oka w lustro...


## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* **zarobki jako Andromeda**: nieregularne, ale dość przyzwoite, wystaczają w zupełności na jej potrzeby

### Znam

* **środowisko artystyczne**: znajomi artyści, pomogą, załatwią
* **okultyści**: Kasia jest swoistym autorytetem w tym światku, od czasu, gdy magia zaczęła się jej czepiać. Sporo wie a dzięki znajomościom może dowiedzieć się znacznie więcej.
* **studenci**: ręka rękę myje. Za napisanie pracy, machnięcie szkicu czy inne tego typu usługi dla studentów, (a czasem za opłatą) Kasia może wykorzystać ich jako źródło informacji, czy środek do zacierania śladów... Ogólnie, jeśli student to potrafi, to ona może to uzyskać.

### Mam

* **reputacja Andromedy**: raczej rzadko korzysta z niej osobiście, bo w końcu Andromeda jest mężczyzną, ale jest.
* **pojazd**: samochód kempingowy, który umożliwia jej przetrwanie jakiegoś czasu z dala od cywilizacji i ma wszystko, co może być potrzebne do przeżycia
* **wyposażenie okultystyczne**: kamienie szlachetnie (lub prawie), skrawki srebra, lapis, sól, woda święcona, ziemia z grobu... część przydatna, część tylko po to, żeby magowie nie zrozumieli, co widzą...

## Inne:



## Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150104|konsolidująca siły i decydująca się na to, po której stronie stanąć.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|zbierająca informacje i obserwująca jak rozpada się lustro i jej własne wyobrażenie o niej samej.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|która kombinuje jak mieć ciastko i zjeść ciastko (Iliusitius vs Arazille) i szantażuje/trolluje sojuszników jak pro.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|która kradnie wampira sprzed nosa bogini|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|która rozwikłuje swoich sojuszników przy okazji zagadek lokalnych bogiń|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141218|która musi pracować z tym co ma a wszystko jej się rozłazi.|[Portret Boga](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|10/02/07|10/02/08|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150105|która poznaje elementy swej przeszłości choć nadal ma liczne bariery mentalne (bardzo osłabione przez Iliusitiusa). |[Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|131008|dowód na to, że ważniejszy jest pomysł niż moc magiczna.|['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|człowiek, którego nie da się kontrolować ołtarzem i osoba eksperymentująca z mocami luster.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130506|mastermind grupy szturmującej Rezydencję Szczypiorków. Aha, dla Sandry: terminuska i koleżanka Augusta.|[Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|10/02/01|10/02/02|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130503|dowód na to, że gdzie mag nie może, tam się babę pośle|[Renowacja obrazu Andromedy](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|10/01/30|10/01/31|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|170815|pomogła uciec jednej z proto-Świec Iliusitiusa i swojemu "klonowi". Dowiedziała się czegoś o swojej przeszłości, acz nie wie, na ile to prawda.|[Bliźniaczka Andromedy](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|10/01/23|10/01/25|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|160908|która nie zdążyła zdobyć informacji, którymi mogłaby ściągnąć maga i - być może - uratować Emilię (viciniuskę) przed Skorpionem|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|10/01/14|10/01/17|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|121104|człowiek ZNOWU wykonujący robotę za terminusa|[Banshee, córka mandragory](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html)|10/01/12|10/01/13|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|121013|wybitna malarka z unikalną pamięcią, która po raz pierwszy zdaje sobie sprawę, że jest jakoś powiązana z lustrami|[Zwłaszcza, gdy coś jest ciągle nie tak](/rpg/inwazja/opowiesci/konspekty/121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html)|10/01/10|10/01/11|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|120920|wybitna malarka z unikalną pamięcią, która ratuje terminusa przed mandragorą|[Ale nie można zmusić go do jego rozwiązania...](/rpg/inwazja/opowiesci/konspekty/120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html)|10/01/08|10/01/09|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|120918|wybitna malarka z unikalną pamięcią,  przerażona mocą dostępną magom|[Można doprowadzić maga do problemu...](/rpg/inwazja/opowiesci/konspekty/120918-mozna-doprowadzic-maga-do-problemu.html)|10/01/06|10/01/07|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|170207|udaje maga przed Matyldą i skupia się na pomocy wpierw przyjacielowi - poecie, potem całej mieścinie.|[Błękitny zaskroniec](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|10/01/03|10/01/05|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150427|niewidzialna i subtelna siła wydobywająca kluczowe informacje od pani weterynarz. Też: magnes na terminusów.|[Kult zaleskiego Anioła](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|10/01/03|10/01/04|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|13|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html), [121104](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html), [121013](/rpg/inwazja/opowiesci/konspekty/121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html), [120920](/rpg/inwazja/opowiesci/konspekty/120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html), [120918](/rpg/inwazja/opowiesci/konspekty/120918-mozna-doprowadzic-maga-do-problemu.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|12|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html), [121104](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html), [121013](/rpg/inwazja/opowiesci/konspekty/121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html), [120920](/rpg/inwazja/opowiesci/konspekty/120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html), [120918](/rpg/inwazja/opowiesci/konspekty/120918-mozna-doprowadzic-maga-do-problemu.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|10|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|7|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|4|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|3|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|3|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [130503](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|2|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|2|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|2|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Łukasz Czerwiącek](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-czerwiacek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Zygmunt Flak](/rpg/inwazja/opowiesci/karty-postaci/1709-zygmunt-flak.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Tadeusz Grżnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-grznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Tadeusz Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Stefan Porieńko](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-porienko.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Rudolf Mikołaj](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-mikolaj.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Michał Prosznik](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-prosznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Matylda Szarotka](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-szarotka.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Mateusz Kozociej](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kozociej.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Marek Ossoliński](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-ossolinski.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[121104](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Krzysztof Jarzolin](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-jarzolin.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Katarzyna Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-nieborak.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Józef Pasan](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pasan.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Izabela Kruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kruczek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Inga Prosznik](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-prosznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Geraldina Kurzymaś](/rpg/inwazja/opowiesci/karty-postaci/9999-geraldina-kurzymas.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Franciszek Dromicz](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-dromicz.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Emilia Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Czesław Statyw](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-statyw.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Antoni Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Aneta Kosicz](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-kosicz.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
