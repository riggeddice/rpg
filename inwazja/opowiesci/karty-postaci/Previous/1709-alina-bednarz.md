---
layout: inwazja-karta-postaci
categories: profile
factions: "Blakenbauer, Prokuratura magów, Siły Specjalne Hektora Blakenbauera"
type: "PC"
owner: "kić"
title: "Alina Bednarz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: lojalność towarzyszom ponad wszystko**:
    * _Aspekty_: chroń sojuszników, tęp zdrajców
    * _Opis_: 
* **BÓL: Ptak w klatce**:
    * _Aspekty_: bezsilność wobec magów, brak wolnej woli
    * _Opis_: "chcę zapewnić sobie swobodę działania w świecie magów"
* **BÓL: Dług honorowy**:
    * _Aspekty_: jestem mu winna ochronę nawet jeśli jej nie chce
    * _Opis_: "Edwin Blakenbauer uratował mi życie. Jestem mu winna tyle samo"
* **MET: Skryty**:
    * _Aspekty_: gotowa na wszystko, ukrywa uczucia, podstępna, działa z ukrycia lub zaskoczenia
    * _Opis_: 


### Umiejętności

* **Doppelganger**:
    * _Aspekty_: stanę się każdym, artefakty
    * _Opis_: 
* **Aktor**:
    * _Aspekty_: stanę się każdym, negocjacje
    * _Opis_: 
* **Technik śledczy**:
    * _Aspekty_: nic mi nie umknie, zdobywanie informacji, artefakty, zauważanie magii
    * _Opis_: pracując w siłach specjalnych, Alina nauczyła się ludzkich metod zbierania śladów. Pracując z Edwinem, nauczyła się, jak patrzeć, żeby widzieć potencjalną magię
* **Black ops**:
    * _Aspekty_: nic mi nie umknie, pułapki, zabezpieczenia, sabotaż, zdobywanie informacji, negocjacje, zastraszanie
    * _Opis_: kiedy trzeba się gdzieś włamać, zabezpieczenia nie mogą Aliny powstrzymać przed wejściem
    
### Silne i słabe strony:

* **Doppelganger**:
    * _Aspekty_: wymaga "próbki" aby się w kogoś zmienić, przemiana jest dokładna
    * _Opis_: 	
* **Augumented human**:
    * _Aspekty_: silna, wykrywalna "medycznie"
    * _Opis_: 	
	
	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* standardowa pensja, którą nie bardzo ma na co wydawać

### Znam

* **Półświatek**:
    * _Aspekty_: broker informacji, przybrany ojciec, 
    * _Opis_: 
* **Augumented humans**:
    * _Aspekty_: 
    * _Opis_: 
* **Siły specjalne policji**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **wyposażenie sił Hektora**:
    * _Aspekty_: 
    * _Opis_:

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170823|broniła Hektora zacięcie przed demonami, opracowywała plan obrony i wysadzała demony.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160819|mistrzyni negocjacji namawiająca Netherię do współpracy. Zbierając ślady odkryła tożsamość The Governess.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160810|udająca maskotkę Adriana Murarza, która przez procedury nie osiągnęła wiele|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160713|która otrząsnęła Dionizego i Tatianę, gdy było trzeba i - dla odmiany - miała tylko jedną fałszywą tożsamość. Poinformowała Hektora o potrzebie pomocy w kinie.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160406|zebrała trochę informacji (wywiad środowiskowy) i egzekutorka kąsającego Klarę psa.|[Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|10/06/29|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160629|która przypomniała sobie, że podobne drony do tej Spustoszonej widziała u Wandy Ketran.|[Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|10/06/27|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160615|aktorka w twarzy Wandy, nagrywająca filmiki kontrujące kanał "Wandy".|[Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160303|cwana, zakamuflowana, PO trzeciego defilera, właścicielka najlepszego srebrnego szrapnela w okolicy.|[Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151216|która pierwsza przeniknęła barierę iluzji i doprowadziła do tego, że Zeta uzmysłowiła sobie ponurą prawdę.|[Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|10/06/21|10/06/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151212|współautorka świetnego planu złapania Jurija z towarzyszką... i "złapała" Arazille.|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151202|lepsza "Vladlena" niż oryginał; skutecznie oszukała Tatianę w Fire Suicie.|[Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|10/06/15|10/06/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|która z Dionizym poszła na akcję i znalazła Tatianę.|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|zwodzicielka Borysa, osoba zadająca trudne pytania i mistrzyni analizy danych z Excela.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160217|the cosplayer, zdobyła środki (krew) i wyszła jako "Zenon" poza złomowisko.|[Sojusz według Leonidasa](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|10/06/06|10/06/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|podłożyła podsłuch tak, że wszyscy myśleli, że to podsłuch Skubnego.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160128|która ostrzegła Hektora, dała się aresztować a potem puściła maszynę w ruch rozmawiając z "gangsterem" Bogdanem Kimarojem.|[Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151110|która poszła do świata * magów ("Rzeczna Chata") dostarczyć Hektorowi Romeo Diakona.|[Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|łącznik Blakenbauerów z siłami specjalnymi i udająca Wandę Ketran zmiennokształtna agentka.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150820|skuteczna jednostka zwiadowcza i rozbijająca związki, których tak naprawdę nie było.|[Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150422|śledcza jednostka terroru usuwająca zbędnych ochroniarzy; wytrzymała straszną aurę w podziemiach. Ukradła aptoforma z Dionizym od Tymotheusa.|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150411|niosąca zapalniczkę manipulatorka ściągająca halucynujące dziewczyny Marcelina z drzewa.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|która potrafiła wyciągnąć wszystkich podejrzanych we wszystkie ustronne miejsca i tylko raz oberwała nożem.|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170412|od jej niewinnej prośby zaczęło się pasmo problemów i nieszczęść; wymyśliła tatuaż z motylkiem odpędzając ponure myśli Romana o Borze|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170407|aktorka wyciągająca informacje oraz rozpuszczająca podłe plotki ratujące Brunowicza przed utratą władzy|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170319|podjęła śledztwo na prośbę Brysia, skupiła się na pochodzeniu narkotyków i znalazłszy coś magicznego, przekazała to Edwinowi.|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|10/03/06|10/03/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170315|udająca reporterkę, lokalizująca złodziejkę naszyjnika i doprowadzająca Hektora do naszyjnika. Doskonała +1.|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|przebrana za żula dała się złapać w zaklęcie mentalne KADEMu, zażyła narkotyk, przytulna wobec Hektora i naprawiona przez Edwina. Bryś rozkwasił jej nos (jak była żulem).|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|vicinius uczący się od nowa swoich umiejętności i granicy możliwości po akcji w LegioQuant.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150115|karaluch, który po prostu nie ma szczęścia.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141216|czasem związana a czasem zwyciężczyni najtrudniejszego testu na sesji.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141022|osoba znajdująca się we właściwym miejscu we właściwym czasie we właściwym przebraniu|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141009|zmiennokształtna agentka ratująca Hektora od rozkazów niezgodnych z linią Blakenbauerów|[Jad w prokuraturze](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|10/01/07|10/01/08|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150304|agentka chroniąca Annę Kajak oraz próbująca wyrolować wszystkich mających jakąkolwiek władzę (łącznie z Anną).|[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|22|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|20|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|19|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|10|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|9|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|9|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|9|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|8|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|7|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|6|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|5|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|5|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|5|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|4|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|4|[151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|4|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|3|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|2|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-siluria-diakon.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Romuald Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-romuald-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|2|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|2|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|2|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|2|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Kamila Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Jędrzej Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|2|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Zenobia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-bankierz.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Wojciech Popolin](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-popolin.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Waltrauda Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-waltrauda-werner.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Tomasz Ormię](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-ormie.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Timor Koral](/rpg/inwazja/opowiesci/karty-postaci/9999-timor-koral.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Przemysław Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-przemyslaw-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Patryk Kloszard](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-kloszard.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Marek Rudzielec](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-rudzielec.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1801-marek-kromlan.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Krzysztof Kruczolis](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-kruczolis.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Katarzyna Leśniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-lesniczek.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Karol Kiśnia](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kisnia.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Kajetan Pyszak](/rpg/inwazja/opowiesci/karty-postaci/9999-kajetan-pyszak.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jerzy Gurlacz](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-gurlacz.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Janina Strych](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-strych.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Jakub Ryjek](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-ryjek.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Himechan](/rpg/inwazja/opowiesci/karty-postaci/9999-himechan.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Henryk Waciak](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-waciak.html)|1|[141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Fryderyk Mruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-mruczek.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Filip Szorak](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-szorak.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Ewelina Nadzieja](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-nadzieja.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Ewa Kroideł](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-kroidel.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Erebos](/rpg/inwazja/opowiesci/karty-postaci/9999-erebos.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[Emil Maczeta](/rpg/inwazja/opowiesci/karty-postaci/9999-emil-maczeta.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Dominik Parszywiak](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-parszywiak.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dominik Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Dariusz Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-larent.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Damian Wiórski](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-wiorski.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Brunon Czerpak](/rpg/inwazja/opowiesci/karty-postaci/9999-brunon-czerpak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Beata Zakrojec](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-zakrojec.html)|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|[Beata Solniczka](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-solniczka.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Antoni Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-szczesliwiec.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Albert Pireus](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-pireus.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
