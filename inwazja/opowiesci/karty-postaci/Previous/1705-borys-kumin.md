---
layout: inwazja-karta-postaci
categories: profile
factions: "Blakenbauer"
type: "NPC"
title: "Borys Kumin"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **bezwzględny**: cytat_lub_opis_jak_to_rozumieć
* **amoralny**: cytat_lub_opis_jak_to_rozumieć
* **resourceful**: cytat_lub_opis_jak_to_rozumieć
* **nadużywa władzy**: cytat_lub_opis_jak_to_rozumieć
* **tchórz**: "wie skąd wieje wiatr"

### Specjalizacje

* **mistrz podlizywania**: opis
* **szmugler**: opis
* **znajomość półświatka (ludzi i magów)**: opis
* **szantażysta**: opis
* **kłamie jak z nut**: opis

### Umiejętności

* **systemy zabezpieczeń**: opis
* **zarządzanie ludźmi**: opis
* **motywacja przez strach**: opis
* **uliczny wojownik**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* ** członkowie dawnego Triumwiratu**: opis
* **strona Bożeny Zajcew**: opis

### Co ma do dyspozycji:

* **grupa szantażowanych oficjeli**: opis
* **mało ważni magowie którzy mu coś wiszą**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

wiek: ~40 lat

Szef ochrony Blakenbauerów, oficjalnie. W rzeczywistości świetnie połączony ze wszystkimi paser wywodzący się oryginalnie z niemagicznej strony sił Zajcewów. Jako, że Hektor dostałby zawału serca, Borys oficjalnie jest szefem ochrony. W praktyce, przy jego koneksjach cały system Blakenbauerów działa w miarę bezbłędnie. Kluczowy dostawca wszystkiego, o czym młodzi by nie chcieli wiedzieć a Otton się nie przejmuje.

Człowiek. Żył jako człowiek, umarł jako człowiek. Nigdy nie został nawet viciniusem.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160224|szef obrony który dla odmiany KOGOŚ obronił (unieruchomił i złapał Marcelina pistoletem strzałkowym).|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|któremu upiekła się straszna kara od Hektora, bo Edwin kazał mu łapać wszystkie informacje o Tymotheusie Blakenbauerze...|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|szef zabezpieczeń Rezydencji potwierdzający ponownie swoją niekompetencję w tej roli.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151021|kanalia a nie szef ochrony. Sprzedał Zajcewowi kapeć zamiast zamknąć go w lochu. Dorobił się a Tatiana przegrała zakład.|[Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160202|który zauważył nietypowe zachowanie pająka fazowego. Też: wysyłał mu sprośności i gadał do niego z nudów.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150422|który dla oglądania nagiego kobiecego ciała jest skłonny sprzedać innych agentów Blakenbauerów. Szczęśliwie relatywnie niegroźny, choć obleśny.|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|który z radością wrobi Hektora w obraz "Parówczanej Teorii Hektora". Podejrzewamy, że hostuje stronę "PTH".|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|osoba usługi wszelakie na rzecz Blakenbauerów pokazująca, że nie ma okrucieństwa do jakiego się nie posunie by nie przegrać.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140312|który umiera w pidżamie podczas ataku na Rezydencję Blakenbauerów do niczego się nie przydając (KIA). Nie wiadomo czemu go zabito.|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|ochroniarz Blakenbauerów z radością poniewierający i ciorający Andreą po glebie. A potem się okazało że spał z Sophistią i coś na niego wpływało.|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140227|który może obserwować każde miejsce w Rezydencji i ma dostęp do wszystkich systemów wizyjnych.|[Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140910|szef ochrony Rezydencji nie mający żadnej istotnej funkcji na tej misji.|[Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Edwin Blakenbauer|8|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Hektor Blakenbauer|7|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Mojra|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Alina Bednarz|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Marcelin Blakenbauer|4|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Dionizy Kret|4|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Waldemar Zupaczka|3|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Szymon Skubny|3|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Patrycja Krowiowska|3|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Margaret Blakenbauer|3|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Andrea Wilgacz|3|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Wacław Zajcew|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Tatiana Zajcew|2|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Siluria Diakon|2|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Otton Blakenbauer|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Netheria Diakon|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Marta Newa|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Leonidas Blakenbauer|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Klemens X|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Klara Blakenbauer|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Jan Szczupak|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|Bolesław Bankierz|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|Arazille|2|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Amelia Eter|2|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Witold Wcinkiewicz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Wiktor Sowiński|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Wanda Ketran|1|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|
|Tymotheus Blakenbauer|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Teresa Żyraf|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Stanisław Pormien|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Smok|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Sebastian Tecznia|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Rukoliusz Bankierz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Rufus Czubek|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Romeo Diakon|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Robert Przerot|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Remigiusz Zajcew|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Paweł Grzęda|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|Paulina Tarczyńska|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Ozydiusz Bankierz|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Nikodem Sowiński|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Marysia Kiras|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Maria Newa|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Laurena Bankierz|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Laura Filut|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Krystalia Diakon|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Konrad Węgorz|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Kinga Melit|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|Karol Poczciwiec|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|Józef Pimczak|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Juliusz Szaman|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Joachim Zajcew|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Infernia Diakon|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Ika|1|[140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Ignat Zajcew|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|Grzegorz Czerwiec|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|GS "Aegis" 0003|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Filip Sztukar|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Estrella Diakon|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Estera Piryt|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Emilia Kołatka|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Emil Maczeta|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Elżbieta Niemoc|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|Elizawieta Zajcew|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Edward Sasanka|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Diana Weiner|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|Dariusz Larent|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Dariusz Kopyto|1|[140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|Dagmara Czeluść|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Czirna Zajcew|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Crystal Shard|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Artur Żupan|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Artur Bryś|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Aptoform Mirasilaler|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Adrian Murarz|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
