---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "PC"
title: "Mordred Blakenbauer"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

- odnaleźć swoją mentorkę Klaudię Weiner
- uwolnić się od klątwy i rodziny
- znaleźć bratnią duszę i założyć nową rodzinę
- wspieranie Millenium

### Specjalizacje

- demibeast: walka dystansowa
- biomancja: wytwarzanie trucizn
- biomancja: pajęczaki
- kataliza: rozrywanie zaklęć
- kataliza: tkanie zaklęć
- terminus: zastraszanie


### Umiejętności

- kat+bio: magiczne pułapki i systemy defensywne
- życie w świecie magów
- demibeast: regeneracja
- bio+transport: portale taktyczne
- biomancja: wzmacnianie/osłabienie ciała
- kataliza: wysysanie energii magicznej
- kataliza: tarcze antymagiczne
- terminus Millenium
- krawiec:uniformy z pajęczyn

### Siły

- szanowany w Millenium
- cichy
- skryty
- renoma skutecznego zabójcy

### Słabości

- antypatyczny
- szpetny
- moralny na swój sposób

### Specjalne

- demibeast - kontrolowana forma pół-bestii
- klątwa

## Magia

### Szkoły magiczne

* **Biomancja**
* **Magia transportu**
* **Kataliza**

## Zasoby i otoczenie

### Powiązane frakcje

### Kogo zna

### Co ma do dyspozycji

### Surowce

# Opis

### Koncept

### Motto

"Z rodziną wychodzi się dobrze tylko na zdjęciach"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170920|przekaźnik energii do walki z Kinglordem, negocjator (serio), skończył jako partner Hektora w prokuraturze magów. Zwerbował Hannę (Henryka) do Millennium.|[Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|10/09/03|10/09/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170914|pilnuje Henriettę i Piotra. Nie przegania Silurii. Konspiruje z Rafaelem. Dostarczył swojej krwi do stworzenia Henrietty.|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|10/08/31|10/09/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170830|ratuje Piotra przed przypadkowymi zabójcami i oportunistycznym terminusem. Dostaje opiernicz - zasłużenie. Zniszczył obrazy Piotra wzywając moc Arazille. Ogólnie, teleporty ma Paradoksalne.|[Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|10/08/29|10/08/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170823|inwigilator posiadłości Blutwurstów. Ma oczy w całej posiadłości. Uwierzył Siwieckiemu jako dziewczynce, że ten był kontrolowany.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170816|władca trujących pająków, mistrz zastraszania Sandry i celny kineta|[Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|10/01/27|10/01/29|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|4|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|3|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Rafał Warkocz](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-warkocz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marianna Biegłomir](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-bieglomir.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Manfred Jarosz](/rpg/inwazja/opowiesci/karty-postaci/9999-manfred-jarosz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
