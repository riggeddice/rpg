---
layout: inwazja-karta-postaci
title:  "Dracena Diakon"
categories: profile
guild: Millenium
type: NPC
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Ptak w klatce**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **pacyfistka**: cytat_lub_opis_jak_to_rozumieć
* **determinator**: cytat_lub_opis_jak_to_rozumieć
* **artystka**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **cybergothka**: opis
* **wzór magiczny**: opis
* **cyborgizacja**: opis
* **rekonstrukcja biomantyczna**: opis
* **magia imprezowa**: opis

### Umiejętności

* **DJ**: opis
* **uwodzicielka**: opis
* **kłamczuszka **: opis
* **anarchistka **: opis
* **badania nad życiem i jego wariantami**: opis
* **krwawe rytuały**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        +1          |        -1       |        +1        |     +1     |   +1   |       0       |     +1     |     +1      |

### Specjalne
* **cechy sumują się do 5, nie do 0**

## Magia

### Szkoły magiczne

* **technomancja**: opis
* **biomancja**: opis
* **kataliza**: opis
* **magia zmysłów**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

28 lat. Dracena jest Diakonką, córką Draconisa Diakona (terminusa starej szkoły Milenium). Lifeshaperka i katalistka. Aha, do tego jest też cybergothem i DJem. Jest bardziej przyjazna ludziom niż magom. Jak większość swego rodu, jest osobą amoralną, ale szanuje altruizm i odruchy pochodzące z serca. Nie tylko nie storpeduje, ale spróbuje pomóc; z natury wesoła, sympatyczna i nie jest trudno mieć ją po swojej stronie.
Jest Wspomaganą; nie kralotycznie, ale jest już zmieniona.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia
