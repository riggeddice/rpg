---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
title: "Mateusz Ackmann"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Rozwiążę każdą zagadkę**: perwersyjna chęć do rozwiązywania problemów logicznych
* **Będą mnie cenić jako eksperta**: pragnienie bycia dostrzeganym jako ekspert w swojej dziedzinie
* **Zrobię interes życia**: zarobienie takiej kasy, która ustawi do końca życia

### Silne i słabe strony

* **+**: nie daje się ponieść emocjom
* **+**: dyskretny
* **+**: ciekawy nowych informacji
* **+**: Rozwiazywanie konfliktów intelektem
* **+**: doskonala pamiec

* **-**: nieśmiały
* **-**: brak zdolności przywódczych
* **-**: uległość wobec kobiet
* **-**: nie potrafi się bić
* **-**: Arogancja

### Specjalizacje

* **ekonometryk** (matematyk)
* **kreatywna księgowość** (księgowy)
* **obfuskacja** (biurokracja)
* **magia matematyczna**
* **Archiwa** (infomancja-transport)
* **podszywanie się** (cosplayer)

### Umiejętności

* **matematyk**
* **księgowy**
* **cosplayer**
* **rpgowiec**
* **akademik**
* **techniki manipulacyjne**
* **biurokracja**

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

* **Magia zmysłów**:
    * _Aspekty_: 
    * _Opis_:
* **Technomancja**:
    * _Aspekty_: infomancja
    * _Opis_:
* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_:

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **kręgi biznesowe - ludzkie**
* **Świeca**
* **rpgowcy i miłośnicy fantasy**

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 2
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

## Opis

Cichy, introwertyk, lekki asperger i uwielbienie do liczb. Studiował "ludzką" matematyke ze specjalizacją w ekonometrii. Hobby: cosplay - zna się na szyciu, konstrukcji ekwipunku, ubrania, nadawanie im wizualnych efektow magicznych; rpgi i planszówki. Jest glównym księgowym dużej korporacji. Mag Świecy. Niski, lekko szpakowate włosy, kawaler, znany w kręgach ludzkich - firmowo-korporacyjnych, jako doskonały księgowy. Znany również w Świecy, jako dziwaczny, ale uprzejmy i utalentowany mag-matematyk.
Zakochany w Supernowej Diakon.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Matematyka to język przyrody"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|+1 surowiec od Diakonów (Silgor)|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160922|księgowy który przeżył dzień swojego życia i 'got the girl', "pokonał" terminuskę i zorganizował fajną imprezę RPG.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|170725|przeleciał wiłę, zebrał ekipę, pojechał, doprowadził do największego Skażenia w historii Jodłowca i stracił o większości pamięć dla bezpieczeństwa.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
