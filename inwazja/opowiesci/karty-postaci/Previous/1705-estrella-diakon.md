---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Estrella Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Walczy z cierpieniem**: Nieważne czy ludzi, magów, czy zwierząt - ból i cierpienie są błędem, który należy wyplenić.
* **Odkrywca tajemnic**: Estrella lubi wszystko wiedzieć, wszystkiego się dowiedzieć - by podjąć w przyszłości decyzję, co z tym zrobić.
* **Strażnik tajemnic**: Pewne sekrety nie powinny nigdy wyjść na jaw. Nie wszyscy powinni wszystko wiedzieć i rozumieć.
* **Obrońca pokrzywdzonych**: Estrella zawsze próbuje chronić stronę pokrzywdzoną, często nawet działając dość ryzykownie. W idealnym świecie nie musi...

### Zachowania (jaka jest)

* **Skryta**: Estrella unika pokazywania swojej natury i poglądów; do momentu aż kogoś polubi. Wtedy się lekko rozluźnia, ale nadal trzyma karty przy orderach.
* **Pomocna**: Jeżeli jest w stanie, spróbuje pomóc. Znajdzie czas. Zdecydowanie próbuje pomóc słabszym; jest w końcu terminusem.
* **Profesjonalistka**: Estrella rzadko zachowuje się frywolnie czy robi to, co chce. Zwykle lubi być traktowana jako profesjonalistka a nie osoba.
* **Bardzo drobiazgowa**: Jest bardzo drobiazgowa; jak się do czegoś przykłada, zajmuje to zwykle trochę więcej czasu... ale jest to zrobione naprawdę dobrze.

### Specjalizacje

* **ukrywanie informacji**: Estrella specjalizuje się w tym, by kluczowe informacje nigdy nie wyszły na jaw i by niektóre pytania nigdy nie były zadane.
* **fałszowanie dokumentów**: kto jest w stanie edytować dokumenty, potrafi zmienić przeszłość... co jest czasem potrzebne, by ukryć 'prawdę'.
* **znajdowanie sekretów**: Estrella jest szczególnie wyczulona na rzeczy, które "nie pasują". Gdy coś jest ukryte, tajemnicze... najpewniej to zauważy.
* **odwracanie uwagi**: Estrella specjalizuje się w dywersji, wytrącaniu przeciwnika oraz w otwieraniu innym okoliczności do działania.
* **zauroczenie pięknem**: przede wszystkim kwiatów, ale Estrella potrafi wstrząsnąć i wytrącić z równowagi prostym przedstawieniem najpiękniejszych cech świata.
* **Magia lecznicza**: przede wszystkim redukująca cierpienie; painkillery i anestezja. Też: stabilizacja, by cel nie umarł.
* **Botanika magiczna**: szczególnie kwiaty; ogólnie, rośliny to jej silna strona.
* **Bojowa magia roślin**: klasyczna szkoła walki Diakonów; jak zazwyczaj, połączona Zmysły + Biomancja.

### Umiejętności

* **terminus wsparcia**: potrafi walczyć (terminus), ale rzadko walczy samodzielnie. Raczej wystawia przeciwnika innym do usunięcia.
* **drużynowy bard**: wysłucha, doradzi, opowie historię, skojarzy problem z czymś innym, podniesie morale... taki "klej" zespołu.
* **medyk bojowy**: nie jest może lekarzem, ale poradzi sobie z utrzymaniem rannego przy życiu lub postawieniem kogoś na nogi 'na asap'.
* **aktorka**: zwłaszcza w połączeniu z magią Zmysłów, Estrella jest taka jak chce dla każdego. Świetnie gra i prezentuje te swoje cechy, które chce.
* **imprezowiczka**: na każdej imprezie, Estrella czuje się najlepiej. Dosłownie 'gwiazda towarzystwa'.
* **szpiegostwo**: czasami trzeba gdzieś się wkraść czy dostać, coś podłożyć czy zwinąć... dowiedzieć się czegoś... kto podejrzewałby damę z ładnymi kwiatkami?
* **archiwa**: gdzie znajduje się prawda? W dokumentach i archiwach. Kto kontroluje zapisy, kontroluje dokumenty. 
* **botanik**: uczona przez Wiktorię Diakon, Estrella doskonale zna się na różnych roślinach, ich hodowli i cechach.

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        +1       |         0        |     +1     |   -1   |      +1       |     -1     |      0      |

### Specjalne

brak

## Magia

### Szkoły magiczne

* **Magia Zmysłów**: manipulacja zmysłami i sygnałami; wzrok, słuch, zapach, temperatura... pełne iluzje.
* **Biomancja**: praca na istotach żywych i organicznych. Rośliny, ludzie, zwierzęta.
* **Infomancja**: manipulacja informacjami, bazami dokumentów i danymi. Biblioteki lub internet.

### Zaklęcia statyczne

* **Ukryty Lishaor**: Zmysły + Biomancja + Botanika. Tworzy grupę drapieżnych roślin (Lishaor), po czym nakłada wielokrotne iluzje na teren; to sprawia, że dany teren jest "zaminowany" Lishaorami. Lishaory nie atakują maga ani osób desygnowanych przez tego maga. Ukryte, Drapieżne, Odporne, Zaskoczenie.
* **Zarodniki Korzaste**: Biomancja. Tworzy chmurę zarodników, które oblepiają cel, po czym zaczynają porastać korą. Większość celów zostaje całkowicie unieruchomionych.
* **Proste Upojenie**: Biomancja. Cel po prostu zostaje za-alkoholizowany do poziomu nieprzytomności. Działa tylko na biotyp ludzi, magów i klad ludzki viciniusów. Barbarzyńsko proste i skuteczne.
* **Nieskończona rozkosz**: Biomancja + Zmysły. Głównie stosowane przez Estrellę do anestezji lub wyłączenia cywila z akcji, ale może też być traktowane jako czar bojowy.

## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca

### Kogo zna

* **Przyjaciółka i kochanka Netherii**: Neti jest doskonale we wszystkim poinformowana. Dzięki Estrelli ;-). Te dwie silnie współpracują: źródło wieczy i jej cichy miecz.
* **Lubiana terminuska Świecy**: Estrella jest lubianą terminuską Świecy. Nie ma opinii 'dobrej terminuski', ale 'warto mieć ją w zespole i pomóc'. Trochę maskotka.
* **Opinia strażniczki Maskarady**: Estrella jest szczególnie znana z tego, że chroni Maskaradę nawet w sytuacjach... dziwnych. Za to w sumie najbardziej ceniona.

### Co ma do dyspozycji:

* **Zarodniki i ziarna rzadkich roślin**: Zawsze ma przy sobie różne rośliny, które może wykorzystać w walce... i nie tylko.
* **Sprzęt bojowy terminusa Świecy**: Loadout bojowy. Lekki pancerz, pole siłowe i antymagiczne, różdżka i dobry nóż.
* **Sprzęt strażnika Maskarady**: Artefakty i narzędzia do usuwania pamięci i puryfikacji terenu.

### Surowce

* **Wartość**: 2
* **Pochodzenie**: Terminuska Świecy, standardowy żołd. Dodatkowa premia za dyskretne rozwiązywanie problemów z Maskaradą.

# Opis

Urocza i cicha terminuska która nie unika dobrej zabawy i potrafi zrobić ze wszystkiego broń, która w rzeczywistości zna bardzo dużo sekretów bardzo dużej ilości osób. Nie jest może najlepsza w akcjach bezpoośrednich, ale stanowi świetny klej drużynowy. Bardziej agentka, którą wyśle się na imprezę niż maszyna masowego zniszczenia polująca na potwory.

Ku swojemu utrapieniu, nie jest najcelniejszym czy najodporniejszym z terminusów; lepiej radzi sobie na trzeciej linii przygotowując wsparcie dla bardziej bojowych magów. Jednak gdy dochodzi do osłabienia morale w zespole lub pojawiają się animozje, Estrella jest nieoceniona. Nazywana czasem "Gwiazdeczką" przez kolegów z zespołu.
Bardzo bliska światowi ludzi, co nie jest typowe dla terminusów. Ma bardzo dużo empatii i choroba terminusa jej raczej nie grozi.

### Koncept

Terminuska drugoliniowa, która skupia się na ukrywaniu sekretów i kojeniu cierpienia.

### Motto

"Spokojnie, Twojego pieska już nie będzie bolało... kim ja jestem? A, ja tylko przechodziłam... skup się lepiej na Burku ;-)."

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|ma tymczasowo podniesione uprawnienia jako terminus przez Andreę; jest użyteczna i można na niej polegać, więc...|Powrót Karradraela|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|szacun za zdobycie i przechwycenie Luksji. Też... a co zrobiła z Artefaktem Apokalipsy Psinoskiej?!|Córka Lucyfera|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170823|przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170607|przyniosła informacje ze Świecy odnośnie autowara, ale wpadła w kłopoty polityczne - kosztem Świecy chce ratować ludzi.|[Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|10/08/23|10/08/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170531|która wkręciła Blakenbauerów w próbę pokonania autowara. Nie miała nikogo innego a alternatywą jest zniszczenie wioski i śmierć wielu ludzi.|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|10/08/20|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|kontakt komunikacyjny między Amandą a Silurią; terminuska Lojalistów Ozydiusza w Kopalinie tymczasowo pod dowodzeniem Anety Rainer|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|która zapewniła Marcelinowi rozwiązania taktyczne... których nie posłuchał. Dostała od Wandy nagranie o Blakenbauerach (death hand).|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150826|w częściowym negliżu, podleczona, lekko złośliwa wobec Ozydiusza która dostała szlaban; nie może opuszczać kompleksu Świecy.|[Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|10/05/25|10/05/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|wciąż ranna terminuska z silnie ograniczonymi kanałami * magicznymi (leczy się); pielęgnuje stary Cmentarz Wiązowy. Ukochana Netherii.|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|której Spustoszenie zniszczyło pamięć i która zgodziła się na wypalenie kanałów i cierpienie by sobie przypomnieć.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150604|terminuska Srebrnej Świecy, która zdecydowała się wystąpić z oskarżeniem wobec Izy by Spustoszenie nie zostało zamiecione pod dywan.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|terminuska i przyjaciółka Netherii. Srebrna Świeca, ale jest tu prywatnie. Chciała chronić przed sytuację, ale została Spustoszona.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170628|zmusiła Henryka do uratowania satanisty i udawała żonę gangstera by zdobyć ciężarówkę. Ma własne plany wobec Luksji.|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|10/02/16|10/02/17|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170620|zastawiła pułapkę, wciągnęła Luksję, rozmontowała jej Krwawe Osłony, po czym postrzeliła ją blokując dalsze działania. Ranna.|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170603|zaczajona w krzewach wśród czosnku by ratować Henryka przed grupką thralli Luksji. I rowerem. Dowiedziała się sporo o całym problemie.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|twórczyni wirtualnego księdza-joggera i poszukiwaczka informacji o rytuałach... i komputerach.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Netheria Diakon|5|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Henryk Siwiecki|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Luksja Pandemoniae|4|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Klara Blakenbauer|4|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Kermit Diakon|4|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Hektor Blakenbauer|4|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Siluria Diakon|3|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Margaret Blakenbauer|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Marcelin Blakenbauer|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Bzizma Stlitlitlix|3|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html), [170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Zenobi Klepiczek|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Wiktor Sowiński|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Paweł Sępiak|2|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Paulina Tarczyńska|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Otton Blakenbauer|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|Olga Miodownik|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Maria Newa|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Kalina Cząberek|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Judyta Maus|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170607](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html)|
|Judyta Karnisz|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|Janina Jasionek|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Infensa Diakon|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Henryk Kantosz|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|Filip Cząberek|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Łukasz Tworzyw|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|Zenon Weiner|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Wirgiliusz Kartofel|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Tymotheus Blakenbauer|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Tymoteusz Maus|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Tamara Muszkiet|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Stella Stellaris|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|Stanisław Pormien|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Stanisław Bazyliszek|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Sieciech Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Salazar Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Roman Gieroj|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Robert Mięk|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Rafał Łopnik|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Piotr Kit|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Paweł Parobek|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|Ozydiusz Bankierz|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|Olaf Rajczak|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Oktawian Maus|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Mordred Blakenbauer|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Misza Dobroniewiec|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|Milena Pacan|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Milena Diakon|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Mikado Diakon|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|Mateusz Tykwa|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Marysia Kiras|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Marian Łajdak|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Marian Agrest|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Marcel Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Maciej Tykwa|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Leonidas Blakenbauer|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Katarzyna Kotek|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Karina von Blutwurst|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Julia Weiner|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Jolanta Sowińska|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|Joachim Zajcew|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Jewgenij Zajcew|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Jan Szczupak|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Jakub Niecień|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Ireneusz Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Iliusitius|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Grzegorz Śliwa|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Gerwazy Śmiałek|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|Gala Zajcew|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|GS "Aegis" 0002|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Ernest Maus|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Emilia Kołatka|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Elizawieta Zajcew|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Eleonora Wiaderska|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Elea Maus|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Edward Bankierz|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Dżony Słomian|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|Dracena Diakon|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Dionizy Kret|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Demon_481|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Dagmara Wyjątek|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Dagmara Czeluść|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Borys Kumin|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|Bianka Stein|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Basia Morocz|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|Baltazar Maus|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|Balbina Wróblewska|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|Aurel Czarko|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Antygona Diakon|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Antoni Bieguś|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|Andżelika Leszczyńska|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|Andrea Wilgacz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Alina Bednarz|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Aleksander Sowiński|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Adrian Kropiak|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|Adam Lisek|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
