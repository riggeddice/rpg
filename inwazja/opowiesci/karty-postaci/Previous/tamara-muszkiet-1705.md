---
layout: inwazja-karta-postaci
title:  "Tamara Muszkiet"
categories: profile
guild: "Srebrna Świeca"
type: NPC
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **oddana Wielkiej Świecy**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **"duma i uprzedzenie"**: opis
* **bezwzględna**: opis
* **skryta**: opis

### Specjalizacje


* **walka z magami**: opis
* **taktyk**: opis
* **magia ubiorów i pancerzy**: opis

### Umiejętności

* **terminus**: opis
* **oficer**: opis
* **krawcowa**: opis
* **moda**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **magia materii**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Pochodząca ze świata ludzi tien Tamara Muszkiet zachowuje się jak arystokratka Srebrnej Świecy, mimo, że arystokratką nie jest. Stała się magiem jeszcze przed Zaćmieniem a Zaćmienie jedynie podniosło jej umiejętności. W świecie ludzi była krawcową. Jest przywiązana do systemów hierarchicznych i do chain-of-command.
Jej naturalne umiejętności magiczne predestynowały ją do zostania zbrojmistrzem terminusów, ale Tamara bardzo szybko zmieniła profil na zarządzanie oddziałem terminusów. Mimo, że nie jest najlepszym bojowo terminusem jaki jest pod ręką, jej posłuszeństwo i skuteczne podejście do problemów połączone z umiejętnością zarządzania ludźmi i taktyką sprawiają, że jest skutecznym terminusem pierwszoliniowym jak długo nie jest silnie sfokusowana przez przeciwnika.
Zna i potrafi wykorzystywać zarówno świat ludzi jak i świat magów.
Jej wielką słabością i miłością jest moda. Pasjonuje się modą świata magów i ludzi. Zawsze wygląda nieskazitelnie i adekwatnie.
Postrzegana jest jako "chłodna, lecz uprzejma" i "trochę z boku". Wyjątkowo nie lubi KADEMu.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia
