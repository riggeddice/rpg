---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Anna Myszeczka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **deathwish**: "to boli za mocno… chciałabym… chciałabym to przerwać"
* **zemsta - Karolina / Karradrael**: "nie umrę, póki ONI żyją. Będą umierać tysiące razy!"
* **blood knight**: "miałeś cierpieć tak jak cierpiałam kiedyś ja! Stań mi na drodze… i obiecam ci BÓL."
* **sentinel**: "obronię Hektora i innych… nie obroniłam moich dzieci, ale obronię przynajmniej NICH"

### Zachowania (jaka jest)

* **impuls**: "cytat"

## Co umie (co postać potrafi):

### Specjalizacje

* **stymulowanie magów** (biomancja)
* **kineza**
* **puryfikatorka**

### Umiejętności

* **terminuska - antymag**
* **terminuska - antypotwór**
* **taktyk**
* **medyk polowy**
* **tortury / zastraszanie**
* **matka dzieci**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         -1      |        -1        |      0     |    0   |      +1       |     +1     |      0      |

### Specjalne

**brak**

## Magia

### Szkoły magiczne
* **kataliza**
* **magia transportu**
* **biomancja**
* **adeptka Magii Krwi**

### Zaklęcia statyczne

## Zasoby i otoczenie

### Powiązane frakcje

* link_do_frakcji

### Kogo zna

#### Doskonale: 3

### Co ma do dyspozycji:

### Surowce

* 2

# Opis
A young woman who used to have two children. By orders of Karradrael, Caroline has sacrificed them in the blood magic ritual in front of her eyes - to increase the suffering and break her. And Caroline has succeeded.

However, Ann got rescued by Blakenbauer magi, under command of Hector and Clara. This broken terminus and purifier has recollected herself somewhat, however she is not very stable yet. She has a bit of a death wish, hoping the pain will eventually stop, but she is not going to die before Caroline will die. The hatred fuels Ann and makes her carry-on, protecting Hector and others who might require her assistance.

She has an extremely soft spot towards the children which is completely understandable seeing her past. She tends to live internally rather than externally; she is neither outspoken nor outgoing. Tends to be quite ruthless and pragmatic. In better days, she would get a good therapy. Nowadays – she simply goes on.

Haunted by her past, this purifier and terminus tends to go all in in the moves which seem quite reckless - but they let her win in the world where normal people do not expect a terminus to “do things like that”.

## Motto

## Inne

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170104|wpadająca w ciężką paranoję i zarażająca nią Hektora; na szczęście, Mikado wziął ją na sparing. Miecz czy Pistolet - oto jest pytanie. Przegapiła wszystkie prawdziwe problemy.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|która prawie postrzeliła Hektora (nie poznała go w nowej formie) i wygrywa z Mikado w jengę. Zaprzyjaźniła się z nim... jakoś|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|egzekutorka * magów Esuriit by chronić Hektora i Ekspedycję. Świetna na zasięg. Współuczestniczyła w egzekucji Kazimierza.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|która chroni Hektora i odkrywa luki w obronie Świecy. |[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|która wpierw uratowała łazik przed upadkiem a potem Speculoid sprawił, że prawie wpadła w berserk (iluzja przejeżdżanych dzieci). Miota kinetycznie granatami.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|która nie oddala się od Hektora. Jest jak jego wierny cień - chronić i zapewnić bezpieczeństwo. Konsultuje Mausa jako puryfikatorka.|[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|która po wszystkich rzeczach jakie się wydarzyły powoli ulega energii Esuriit; jeszcze można na niej polegać.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161012|zdesperowana terminuska, która oszukała Blakenbauerów by zginąć wraz z Hektorem - wiedziała, że nie da się ukryć przed Karradraelem. Przypadkowo przeżyła.|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160914|puryfikatorka i terminuska ciężko raniona dość często przez... Hektora w malignie. Przeżyła - i podjęła kilka trudnych decyzji. Śmierć jej dzieci nadal ją prześladuje...|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160825|która straciła rodzinę przez Overminda i która z radością pomoże Blakenbauerom. Z danych od Karoliny wynika, że nie jest tajną bronią.|[Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|10/07/15|10/07/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160819|ofiara The Governess, która została uratowana przez Dionizego i dostarczona do Rezydencji. Służyła jako podstawa do tworzenia Krwawokrągów.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Hektor Blakenbauer|11|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Siluria Diakon|8|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Mikado Diakon|7|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Andżelika Leszczyńska|7|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Marianna Sowińska|6|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Ignat Zajcew|6|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Dosifiej Zajcew|5|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Siriratharin|4|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Klara Blakenbauer|4|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Łukija Zajcew|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Zofia Weiner|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Otton Blakenbauer|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Karolina Maus|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Jakub Pestka|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|Fiodor Maius Zajcew|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Edwin Blakenbauer|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Quasar|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Paweł Maus|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Norbert Sonet|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Konstanty Bankierz|2|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Kazimierz Sowiński|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|Ilona Amant|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Bazyli Weiner|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Wiktor Sowiński|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Vladlena Zajcew|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Tymotheus Blakenbauer|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Saith Kameleon|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Saith Flamecaller|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Roman Błyszczyk|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Patrycja Krowiowska|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Ofelia Caesar|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Netheria Diakon|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Milena Diakon|1|[160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|
|Marta Szysznicka|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Margaret Blakenbauer|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Malwina Krówka|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Lancelot Bankierz|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Konstanty Myszeczka|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|Konrad Myszeczka|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|Karradrael|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Karolina Kupiec|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Karina von Blutwurst|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Karina Paczulis|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Irena Paniszok|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Infernia Diakon|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|GS "Aegis" 0003|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Franciszek Maus|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|Ernest Kokoszka|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Eis|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Dionizy Kret|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Dagmara Wyjątek|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|Bogumił Rojowiec|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Anatol Weiner|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|Alina Bednarz|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Adrian Murarz|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
