---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
title: "Małż Mateusz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **Dominacja nad światem**
* **Wydostanie się na wolność**
* **Zdobycie niewolników**

### Mocne i słabe strony (jaka jest) (jaki jest)

* **+** Mały, wszędzie wlezie
* **+** Brak kręgosłupa moralnego
* **+** Małżowy umysł - niedominowalny
* **+** pancerz

* **-** empatia
* **-** aparycja
* **-** siła fizyczna (brak)

## Co umie (co postać potrafi):

### Specjalizacje

* **Wiedza małży**: Świat ludzi (z ekranu i internetu)
* **Kompatybilność radiowa**: Przechwytywanie fal elektro-magnetycznych, nadawanie włąsnego sygnału, hackowanie urządzeń elektronicznych
* **Zdalne sterowanie**: przejmowanie kontroli nad maszynami

### Umiejętności

* **Perswazja**
* **Automatyka i robotyka**
* **Strzelanie**
* **Zdobywanie informacji (cyfrowych)**

### Szkoły magiczne

* **technomancja**
* **infomancja**


### Specjalne

* Małżlink

## Magia

### Magia dynamiczna:


### Zaklęcia statyczne

## Zasoby i otoczenie

### Powiązane frakcje

* brak

### Kogo zna

* **Małą grupa mało rozgarniętych minionów**

### Co ma do dyspozycji:

* **Magicrawler**

### Surowce

Drony: 2
Minidziałko laserowe: 2
Miniony: 2

# Opis

## Motto

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170725|też małż. Zdominował konstruminusa, shackował kamerę i grawitacyjnie ogłuszył krzepkiego ochroniarza. Skaził Jodłowiec i uciekł z konstruminusem.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1707-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1707-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1707-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
