---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Terror Wąż"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **Wieczna ewolucja**: Wąż stara się upiększyć, wzmocnić, usprawnić. Nie ma środków poza zakresem "akceptowalnym". No, poza Magią Krwi.
* **Zapalony wegetarianin**: Terror Wąż zrobi wiele, by zdobyć dostęp do świeżych i smacznych roślin, przepisów kulinarnych itp.
* **Zapalony mistrz strachu**: Terror Wąż kolekcjonuje i konstruuje różne byty związane ze strachem; specjalizuje się w nich i zawsze szuka nowych.
* **Chce prowadzić wegebar**: Tak. Terror Wąż. Istota mordercza i budząca grozę chciałaby po prostu... prowadzić wege-bar i mieć w spokoju klientów...

### Zachowania (jaka jest)

* **Chaos**: Wąż najlepiej funkcjonuje w warunkach nieprzewidywalnych; im mniej wiadomo, tym lepiej Wąż sobie radzi. Adaptuje błyskawicznie.
* **Balans**: Wąż jest niestabilny, ale szybko wybacza. Śmierć za śmierć, ale "los tak chciał", więc pozwoli odejść, jeśli atak się nie udał.
* **Samotnik**: Wąż przywykł, że się go boją. Dąży do tego. Nie chce być zrozumiany i nie chce, by ktoś go "przywiązał". Czasem - tak. Na dłużej - nie.

### Specjalizacje

* **alchemik**: od pozyskiwania surowców, tworzenia mikstur, ich oceny i przygotowania w formie gazowej i ciekłej.
* **rośliny i obiady**
* **transformacja ciała** (zwłaszcza swojego)
* **wzbudzanie grozy**
* **wspomaganie bojowe**
* **alchemia**

### Umiejętności

* **botanik**: Wąż uwielbia różnego rodzaju rośliny, ogrodnictwo itp. Kiedyś miał mały ogród, też z jadalnymi roślinami.
* **kucharz, wege**: kucharz zapalony, uwielbia gotować i robi to bardzo dobrze. Ale tylko wegetariańsko.
* **trucizny**: jeden ze sposobów, w jakich Wąż sobie dorabia; przede wszystkim na składnikach roślinnych ;-).
* **narkotyki**: klasyczny sposób dorabiania sobie; przede wszystkim narkotyki bojowe i wspomagające. 
* **włamywacz**: mało jest miejsc, do których Wąż się nie włamie. Forma dorabiania sobie. W poprzednim życiu: Don Juan ;-).
* **stealth**: Wąż potrafi poruszać się w sposób niezauważony i dyskretny, śledząc i unikając.
* **gladiator**: Wąż jest świetnym wojownikiem; specjalizuje się w walce swoim ciałem, truciznami i akrobacjami.
* **zastraszanie**: nazywa się "Terror Wąż" nie przez przypadek. Poza gotowaniem to jego drugie hobby.



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |        -1       |        -1        |      0     |    1   |      -1       |      1     |      1      |

### Specjalne

* Wąż jest wrażliwy na ataki stricte magiczne z uwagi na mało stabilny wzór (-2)
* Wąż ma organiczny pancerz i broń; klasyfikowane jak lekki pancerz i mała broń, gdy nieuzbrojony
* Wąż jest bardzo odporny na negatywne działania trucizn, toksyn i narkotyków bojowych (-3)
* Hipnotyczne Spojrzenie, Wąż ma bonus do akcji dominacyjnych patrząc prosto w oczy (+1)

## Magia

### Szkoły magiczne

* **biomancja**
* **magia materii**
* **magia mentalna**

### Zaklęcia statyczne

* nazwa : link
* nazwa : link

## Zasoby i otoczenie

### Powiązane frakcje

* Roamer 
* Nomad 
* Przestępca
* Żercy Apokalipsy

### Kogo zna

* **Sieć 'Marchewkowiec'**: sieć ludzi (i magów) skupionych dookoła dobrego wegetariańskiego jedzenia. Wąż płaci abonament i udziela się na forum, acz nie chodzi na imprezy.
* **DarkRoad**: Niezbyt legalny market (w internecie, nie hipernecie), gdzie Wąż sprzedaje swoje mikstury i kupuje co ciekawsze rzeczy. Zwłaszcza plotki.
* **Poprzedni zleceniodawcy**: Wąż wykonuje czasem brudną robotę dla różnych ludzi czy magów. Zawsze zapamiętuje, dla kogo tą robotę wykonuje.
* **Reputacja strasznych trucizn**: Wąż czasem wyśle jakiemuś lekarzowi (magowi) zaprojektowane przez siebie trucizny, by sprawdzić, jak sobie poradzą. Daje mu opinię...

### Co ma do dyspozycji:

* **Dostawcy roślin**: Wąż dostaje warzywa... i inne rośliny od sprawdzonych grup oraz z DarkRoad. Można powiedzieć, że botanicznie Wąż się ustawił świetnie.
* **Narzędzia strachu**: Wąż uwielbia w czasie wolnym projektować nowe instrumenty grozy (eliksiry, opary...). Kolekcjonuje WSZYSTKIE.
* **Trucizny i narkotyki**: Wąż dorabia sobie sprzedając na DarkRoad różnego typu trucizny i narkotyki. Ma zatem do sporej ich liczby dostęp.
* **Dostawcy substratów**: Wąż jest elementem łańcucha dostaw; coś kupi, coś sprzeda. Nie wszystko do odpowiednich trucizn i narkotyków ma; te sprowadza.
* **Wege Biolab**: Wąż ma własne laboratorium biologiczne, w którym konstruuje nowe trucizny, narkotyki, rośliny i... obiady. Jest przenośne. W foodtrucku. Wege.
* **Broń: miecz, nóż...**: Wąż ma standardowy zestaw broni, którego regularnie używa. Miecz, noże do rzucania... 
* **Sprzęt włamywacza**: Wąż jak ma gdzieś wleźć to wlezie. Zwłaszcza z wytrychami, kotwiczką i innymi takimi

### Surowce

* mimo strasznego zużycia na Wzór, ma naprawdę solidne przychody ze sprzedaży na DarkRoad: 2

# Opis

Terror Wąż był... była? Diakonem przed Zaćmieniem. Zaćmienie rozdzieliło Wąż z rodem i spowodowało, że Wąż wpadł w pewne szaleństwo. Odrzucił człowieczeństwo i ludzki wygląd, przekształcił się w viciniusa - ale dzięki temu zachował moc. Płeć dla Węża też nie stanowi różnicy; raz jest mężczyzną, raz kobietą. Jak wygodniej.

## Motto

"Dziś jestem kobietą. Marchewkę? Świeża, ze sprawdzonego źródła."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|dostał artefakt uzdatniający energię magiczną by ją lepiej wchłaniał (i smakowała lepiej), co redukuje jego koszty.|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170409|nadal udająca viciniusa i przygotowująca Paulinie przydatne eliksiry; dała się przekonać, by nie zabijać magów od razu|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170404|kucharka z powołania, psychopatka z zamiłowania, więcej straszy niż faktycznie krzywdzi. Pragmatyczna, bezwzględna, ale próbuje pomóc po swojemu.|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|10/02/10|10/02/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170331|eks-Diakonka, płci randomowej, kupuje marchewkę nie z przeceny, morderczyni-wegetarianka i ma lekkie (?) psychozy.|[Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|10/02/07|10/02/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-wiaczeslaw-zajcew.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Szczepan Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Onufry Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Katarzyna Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Ferrus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-ferrus-mucro.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
