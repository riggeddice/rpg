---
layout: inwazja-karta-postaci
title:  "Andżelika Leszczyńska"
categories: profile, kadem
---
# {{ page.title }}

# Mechanika

## Impulsy (co motywuje / napędza postać):

### Strategiczne

* **praworządna**: "zgodnie z artykułem 44 Postanowień Radomskich nie zrobimy tego w TEN sposób"
* **pragnienie ładu**: "każdego dnia entropia odrobinę wygrywa… i każdego dnia ją troszkę odpychamy - cywilizacją, nauką, kulturą."

### Taktyczne

* **ostrożna**: "przy moim szczęściu to zaraz wyleci w powie- NIE ŻONGLUJ TYM!"
* **lękliwa**: "czy sobie już poszli? Ja naprawdę nie nadaję się do tego…"
* **wiedza dla wiedzy**: "wiedziałeś, że Faza Esuriit potrafi pożreć królika w 11 minut? Nieprzydatne? Ale ciekawe…"

## Co umie (co postać potrafi):

### Ekspercko (poziom 3):

* **magia detekcyjna i badawcza** (kataliza)
* **adaptująca magia defensywna** (kataliza)
* **czynność_na_poziomie_eksperckim_(specjalizacja_której_grupy)**: opis

### Zawodowo (poziom 2):

* **dokumenty**
* **biurokracja**
* **prawo magów**
* **arystokratka Świecy**
* **badania i analiza**
* **administratorka**
* **historia magów**
* **naukowiec KADEMu**
* **kataliza - zaklęcia**
* **plotki**
* **negocjacje**

### Amatorsko (poziom 1):

* **grupa_czynności_na_poziomie_amatorskim**: opis

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         0       |         0        |     +1     |    0   |      +1       |      0     |     -1      |


## Kieszonkowe

* opis: 2

## Otoczenie powiązane

### Powiązane frakcje

* link_do_frakcji

### Kogo zna

#### Doskonale: 3

* **doskonała_znajomość**: opis
* **doskonała_znajomość**: opis

#### Nieźle: 2

* **niezła_znajomość**: opis
* **niezła_znajomość**: opis

#### Luźno: 1

* **luźna_znajomość**: opis
* **luźna_znajomość**: opis

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

* **szczególnie_dobry**: opis
* **szczególnie_dobry**: opis

#### Ponadprzeciętne: 2

* **ponadprzeciętne_obiekty**: opis
* **ponadprzeciętne_obiekty**: opis

#### Niezłe: 1

* **kiepskie_obiekty**: opis
* **kiepskie_obiekty**: opis

## Magia:

### Magia dynamiczna:

* antymagia defensywna
* aury magiczne
* magia detekcyjna
* magia analityczna
* analiza zaklęć

### Zaklęcia hermetyczne

* nazwa : link
* nazwa : link

## Specjalne:

* kocha uczyć się różnych dziwnych rzeczy; 20% szansy że zna odpowiedź na super-specjalistyczne pytanie na które nikt normalny odpowiedzi by nie znał.

# Opis

## Motto

Angelica used to be a silver candle mage. She comes from a small aristocrat family – not very aristocrat but aristocrat enough. When she got sent to KADEM she could not live with it for a long time; family indoctrination and the candle indoctrination was simply too strong. However, she was given as a mentee to Jakub Urbanek - an event which could traumatize a person much stronger than she ever was.

Jacob was devastated. She was no battle mage, she was not courageous… she was nothing. But she was a good administrator. So he arranged some stuff and started training her (by proxy) in archive stuff, document manipulation, data aggregation… Basically, perfect bureaucrat and an apt researcher. In terms of magic Angelica has specialized herself in detection magic and some parts of adaptive defensive magic (with him nearby it was kind of natural…).

Currently, she still is a gray mouse hiding behind the bookshelves. But she does know most of strange law intricacies which let her mentor go away with crimes, she knows where the candle does things correct and wrong and she is good in aggregating information and spread information. In short, intelligence warfare.

Think of her as of an activist hacker not using computers. She loves learning and usually knows things obscure and unusual; mostly stuff you would use in crosswords or trivia contests.

## Inne

krótki opis postaci, rzeczy, jakie warto pamiętać.

# Historia
