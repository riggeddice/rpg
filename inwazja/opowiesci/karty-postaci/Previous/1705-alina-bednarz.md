---
layout: inwazja-karta-postaci
categories: profile
factions: "Blakenbauer, Prokuratura magów, Siły Specjalne Hektora Blakenbauera"
type: "PC"
owner: "kić"
title: "Alina Bednarz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

- **ptak w klatce**: "chcę zapewnić sobie swobodę działania w świecie magów"
- **dług honorowy**: "Edwin Blakenbauer uratował mi życie. Jestem mu winna tyle samo"

### Zachowania (jaka jest)

- **pozostać w ukryciu**: "jak o mnie nie wiedzą to nic mi nie zrobią"
- **podstępna**: "najlepiej wykończyć wroga z zaskoczenia"
- **skryta**: "nikt poza przyjaciółmi nie może poznać moich prawdziwych uczuć"

### Specjalizacje

- **stanę się każdym** (aktorka)
- **nic mi nie umknie** (black ops / zdobywanie informacji)

### Umiejętności

- **aktorka**: Alina potrafi zagrać każdego
- **technik śledczy**: pracując w siłach specjalnych, Alina nauczyła się ludzkich metod zbierania śladów. Pracując z Edwinem, nauczyła się, jak patrzeć, żeby widzieć potencjalną magię
- **pułapki**: 
- **zdobywanie informacji**: Alina albo zdobywa je sama, albo przez kontakty
- **negocjacje**: Alina zwykle dąży do osiągnięcia celu przez dostosowanie się do rozmówcy - nie ma znaczenia, czy po dobroci, czy zastraszając, czy oszukując
- **black ops**: skradanie, włamania, przemykanie, zasadzki... tego typu rzeczy
- **zabezpieczenia**: kiedy trzeba się gdzieś włamać, zabezpieczenia nie mogą Aliny powstrzymać przed wejściem
- **sabotaż**: 
- **artefakty**: Alina wie sporo o artefaktach różnego rodzaju, potrafi je rozpozonawać, potrafi ich używać i się przed nimi zabezpieczać

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         +1      |         0        |      -1    |   -1   |       +1      |      0     |     +1      |

### Specjalne

Zmiennokształtna / doppelganger
Augumented human

## Zasoby i otoczenie

### Powiązane frakcje

- Blackenbauerowie

### Kogo zna

- półświatek
- siły specjalne policji
- augumented humans

### Co ma do dyspozycji:

- wyposażenie sił Hektora

### Surowce
* **Wartość**: 2
* **Pochodzenie**: standardowa pensja, którą nie bardzo ma na co wydawać

## Magia

**brak**

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170823|broniła Hektora zacięcie przed demonami, opracowywała plan obrony i wysadzała demony.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160819|mistrzyni negocjacji namawiająca Netherię do współpracy. Zbierając ślady odkryła tożsamość The Governess.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160810|udająca maskotkę Adriana Murarza, która przez procedury nie osiągnęła wiele|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160713|która otrząsnęła Dionizego i Tatianę, gdy było trzeba i - dla odmiany - miała tylko jedną fałszywą tożsamość. Poinformowała Hektora o potrzebie pomocy w kinie.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160406|zebrała trochę informacji (wywiad środowiskowy) i egzekutorka kąsającego Klarę psa.|[Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|10/06/29|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160629|która przypomniała sobie, że podobne drony do tej Spustoszonej widziała u Wandy Ketran.|[Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|10/06/27|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160615|aktorka w twarzy Wandy, nagrywająca filmiki kontrujące kanał "Wandy".|[Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160303|cwana, zakamuflowana, PO trzeciego defilera, właścicielka najlepszego srebrnego szrapnela w okolicy.|[Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151216|która pierwsza przeniknęła barierę iluzji i doprowadziła do tego, że Zeta uzmysłowiła sobie ponurą prawdę.|[Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|10/06/21|10/06/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151212|współautorka świetnego planu złapania Jurija z towarzyszką... i "złapała" Arazille.|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151202|lepsza "Vladlena" niż oryginał; skutecznie oszukała Tatianę w Fire Suicie.|[Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|10/06/15|10/06/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|która z Dionizym poszła na akcję i znalazła Tatianę.|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|zwodzicielka Borysa, osoba zadająca trudne pytania i mistrzyni analizy danych z Excela.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160217|the cosplayer, zdobyła środki (krew) i wyszła jako "Zenon" poza złomowisko.|[Sojusz według Leonidasa](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|10/06/06|10/06/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|podłożyła podsłuch tak, że wszyscy myśleli, że to podsłuch Skubnego.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160128|która ostrzegła Hektora, dała się aresztować a potem puściła maszynę w ruch rozmawiając z "gangsterem" Bogdanem Kimarojem.|[Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151110|która poszła do świata * magów ("Rzeczna Chata") dostarczyć Hektorowi Romeo Diakona.|[Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|łącznik Blakenbauerów z siłami specjalnymi i udająca Wandę Ketran zmiennokształtna agentka.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150820|skuteczna jednostka zwiadowcza i rozbijająca związki, których tak naprawdę nie było.|[Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150422|śledcza jednostka terroru usuwająca zbędnych ochroniarzy; wytrzymała straszną aurę w podziemiach. Ukradła aptoforma z Dionizym od Tymotheusa.|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150411|niosąca zapalniczkę manipulatorka ściągająca halucynujące dziewczyny Marcelina z drzewa.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|która potrafiła wyciągnąć wszystkich podejrzanych we wszystkie ustronne miejsca i tylko raz oberwała nożem.|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170412|od jej niewinnej prośby zaczęło się pasmo problemów i nieszczęść; wymyśliła tatuaż z motylkiem odpędzając ponure myśli Romana o Borze|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170407|aktorka wyciągająca informacje oraz rozpuszczająca podłe plotki ratujące Brunowicza przed utratą władzy|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170319|podjęła śledztwo na prośbę Brysia, skupiła się na pochodzeniu narkotyków i znalazłszy coś magicznego, przekazała to Edwinowi.|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|10/03/06|10/03/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170315|udająca reporterkę, lokalizująca złodziejkę naszyjnika i doprowadzająca Hektora do naszyjnika. Doskonała +1.|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|przebrana za żula dała się złapać w zaklęcie mentalne KADEMu, zażyła narkotyk, przytulna wobec Hektora i naprawiona przez Edwina. Bryś rozkwasił jej nos (jak była żulem).|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|vicinius uczący się od nowa swoich umiejętności i granicy możliwości po akcji w LegioQuant.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150115|karaluch, który po prostu nie ma szczęścia.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141216|czasem związana a czasem zwyciężczyni najtrudniejszego testu na sesji.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141022|osoba znajdująca się we właściwym miejscu we właściwym czasie we właściwym przebraniu|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141009|zmiennokształtna agentka ratująca Hektora od rozkazów niezgodnych z linią Blakenbauerów|[Jad w prokuraturze](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|10/01/07|10/01/08|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150304|agentka chroniąca Annę Kajak oraz próbująca wyrolować wszystkich mających jakąkolwiek władzę (łącznie z Anną).|[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Hektor Blakenbauer|22|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Dionizy Kret|20|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Edwin Blakenbauer|19|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Patrycja Krowiowska|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Mojra|11|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|Margaret Blakenbauer|10|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|Marcelin Blakenbauer|9|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Kleofas Bór|9|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Klemens X|9|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|Klara Blakenbauer|8|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Olga Miodownik|7|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Wanda Ketran|6|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|Szymon Skubny|5|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Otton Blakenbauer|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Borys Kumin|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Artur Bryś|5|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Arazille|5|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|Anna Kajak|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Tatiana Zajcew|4|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|Oddział Zeta|4|[151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Leonidas Blakenbauer|4|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Romeo Diakon|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|Roman Brunowicz|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Paulina Widoczek|3|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Netheria Diakon|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Marta Newa|3|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Karolina Maus|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|Diana Weiner|3|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|Wiktor Sowiński|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|Vladlena Zajcew|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Stanisław Pormien|2|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Siluria Diakon|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Sebastian Tecznia|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Romuald Zeta|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|Robert Pomocnik|2|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Pafnucy Zieczar|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Mordecja Diakon|2|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Luiza Wanta|2|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Kinga Melit|2|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Kamila Zeta|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|Jędrzej Zeta|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|Judyta Maus|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|Infernia Diakon|2|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Dorota Gacek|2|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Czirna Zajcew|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Bogdan Kimaroj|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Aptoform Mirasilaler|2|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|Anna Góra|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|Adrian Murarz|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|oddział Zeta|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Zenon Weiner|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|Zenobia Bankierz|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|Wojciech Popolin|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Witold Wcinkiewicz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Wioletta Bankierz|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Waltrauda Werner|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Urszula Murczyk|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Tymoteusz Maus|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Tymoteusz Dzionek|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Tomasz Ormię|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|Tomasz Jamnik|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Timor Koral|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Szczepan Sowiński|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|Szczepan Paczoł|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Salazar Bankierz|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|Rukoliusz Bankierz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Robert Przerot|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Rebeka Piryt|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Przemysław Marchewka|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Piotr Kit|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Patryk Kloszard|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|Ozydiusz Bankierz|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Oktawian Maus|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|Mordred Blakenbauer|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Miron Ataman|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Marysia Kiras|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Marian Kozior|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Marian Agrest|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Marek Rudzielec|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|Marek Kromlan|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|Malia Bankierz|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|Krzysztof Kruczolis|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|Kornel Wadera|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Konrad Węgorz|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Katarzyna Leśniczek|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Karol Kiśnia|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Karina von Blutwurst|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Kajetan Pyszak|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|Józef Pimczak|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Jurij Zajcew|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Judyta Karnisz|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Jolanta Sowińska|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|Joachim Zajcew|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Jewgenij Zajcew|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Jerzy Gurlacz|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|Janina Strych|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Jan Fiołek|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Jakub Ryjek|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Igor Daczyn|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Hubert Rębski|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Himechan|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Henryk Waciak|1|[141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|Henryk Siwiecki|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Grzegorz Śliwa|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Gerwazy Myszeczka|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Fryderyk Mruczek|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|Franciszek Knur|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Filip Sztukar|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Filip Szorak|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Felicjan Weiner|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|Ewelina Nadzieja|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Ewa Kroideł|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Estrella Diakon|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Estera Piryt|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Erebos|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Emilia Kołatka|1|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|Emil Maczeta|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Elea Maus|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Dominik Parszywiak|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|Dominik Marchewka|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Dariusz Larent|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Damian Wiórski|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|Dagmara Czeluść|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|Crystal Shard|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|Cezary Piryt|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Bzizma Stlitlitlix|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|Brunon Czerpak|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Bianka Stein|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Beata Zakrojec|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Beata Solniczka|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|Baltazar Maus|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|Aurel Czarko|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|Artur Żupan|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|Arkadiusz Klusiński|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|Antoni Szczęśliwiec|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Anna Myszeczka|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|Anna Kozak|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Andrzej Chezyr|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Aleksander Dziurząb|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|Albert Pireus|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Adam Wąż|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Adam Bożynów|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
