---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Antygona Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **samotny wilk**: niezależna, nie chce na nikim polegać, chce przejść przez życie nie potrzebując niczyjej pomocy.
* **pacyfistka**: nie chce zadawać nikomu bólu, nie chce walki czy wojny. Uważa całą tą rzeczywistość za wyjątkowo chorą.
* **equalistka**: chce, by ludzie byli traktowani tak jak magowie. Chce, by ludzie nie byli traktowani okrutnie.


### Zachowania (jaka jest)

* **buntowniczka**: odrzuca praktycznie wszystko co jest promowane przez dzisiejszą rzeczywistość i odchodzi w virch.
* **bezkompromisowa**: poświęciła relację z siostrą dla swoich ideałów.
* **fanka: amatorstwo**: nie musi być to najlepsze, ale niech jest PRAWDZIWE i SZCZERE.


## Co umie (co postać potrafi):

### Specjalizacje
* **Inkarnacja golemów**: to tylko gra… staje się jednością z golemami i swymi konstruktami, by wygrywać.
* **Nawigator -netu**: hipernet, internet… jej awatary czują się lepiej w virch niż w realu.


### Umiejętności
* **Streamerka gier:**
* **Graczka**: doskonała graczka, zwłaszcza gier strategicznych, MMO i innych takich
* **Hipernet, internet**: dla Antygony zarówno hipernet jak i internet są drugim domem; porusza się w nich lepiej niż w świecie rzeczywistym.
* **Rangerka golemów**: zwłaszcza w formie lalek czy istot wyglądających jak "normalne" istoty
* **Miłośniczka niszowych zespołów**: wie wszystko o niszowych zespołach, koncertach, mistrzyni trivii i kocha muzykę "z serca"
* **Niepozorna, nieistotna**: Niewidzialna, niewidoczna, nieważna, niewykrywalna…
* **Tysiące masek**: Antygona potrafi doskonale udawać i ukrywać swoje prawdziwe myśli. Też: dobra w stosowaniu makijażu.



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |        -1        |     +1     |   -1   |      +1       |     -1     |     +1      |

### Specjalne

**brak**

## Magia

### Szkoły magiczne
* **infomancja**
* **magia materii**
* **magia mentalna**

### Zaklęcia statyczne


## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca

### Kogo zna

### Co ma do dyspozycji:

### Surowce

2
Mały kontrakt z niewielką agencją, kilka gier indie, usługi w virch…

# Opis
"Anna" Antygona Dolores Kebab Diakon, siostra Draceny Diakon i Najady Diakon.

1. Z czego TP jest szczególnie dumna (co jest doceniane przez innych)? Dlaczego jest z tego dumna?
Wyrwała się ze struktur Świecy i żyje raczej na uboczu. Sama kształtuje swoje życie, bez polegania na innych magach. Doceniane przez "korposzczury marzące o wolności" w Świecy.
1. Z czego TP jest znana najlepiej wśród innych? Wśród kogo (kim są ci "inni")?
Antygona jest świetnie znana z uwagi na następujące zdecydowanie nietypowe jak na Diakonkę rzeczy:
 1. _Streamerka i świetna graczka_. - w środowisku graczy wielu gier
 1. _Żyje w świecie ludzi, nie magów_. - w środowisku młodych magów Świecy zainteresowanych cichą Diakonką
 1. _Mistrzyni bezużytecznych faktów na temat niszowych zespołów_. - w środowisku hobbystów różnych form muzyki
 1. _Rangerka, ale z golemami w formie lalek, które inkarnuje_. - ogólna opinia na jej temat jako maga
1. Co jest uważane za nieuczciwą przewagę TP?
Umiejętność perfekcyjnego sterowania swoimi golemami i poruszanie się po zarówno hipernecie jak i internecie jako awatar. Ogólnie, inkarnacja i bezpośredni dostęp.
1. Co sprawia, że TP uzna, że TO ZADANIE jest perfekcyjne dla niej? Czemu?
Jest w stanie wygrać z przeciwnikiem przez przechytrzenie go. Najlepiej przez działanie przez hipernet i internet. A samą akcję przeprowadzi przy użyciu golemów.
1. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
Antygona absolutnie nie toleruje niewłaściwego traktowania ludzi i znęcania się nad słabszymi. W ramach swoich skromnych możliwości spróbuje wejść w bezpośredni konflikt z przeciwnikiem, zwykle używając swoich umiejętności inkarnacji i golemów. Jeśli jej się uda, zostanie w cieniu.
1. W jakich okolicznościach "Jak trwoga, to przyjdą do TP"? Kto przyjdzie?
…gdy brakuje healera na rajdzie… Serio, raczej przyjdą osoby z jej kręgu znajomych graczy.
1. Czego TP najbardziej żałuje? Co zrobiłaby inaczej? Dlaczego?
Najbardziej żałuje tego, że nie potrafi się dogadać ze swoją siostrą - Draceną. Przede wszystkim chodzi o traktowanie ludzi… obie są dość bezkompromisowe i obie potrafią walczyć na ostrzu noża. Najchętniej skupiłaby się bardziej na porozumieniu - mają w końcu tylko siebie.
1. O czym TP marzy? Do czego dąży? Dlaczego?
Marzy o świecie, w którym ludzie mają równe prawa z magami. W którym magowie mają równe prawa między sobą. Marzy o sprawiedliwości i o równości. Pacyfistka, wegetarianka i socjalistka.
1. Co TP najbardziej lubi robić w czasie wolnym? Czemu?
Zapalona graczka w gry komputerowe i miłośniczka niewielkich i dość mało popularnych zespołów muzycznych (niższej jakości, acz grających z sercem). Najczęściej gra w coś, zwykle z ludźmi (choć grywa też czasem z magami w gry przy użyciu hipernetu). Zbiera też i kolekcjonuje trivię na temat różnych pomniejszych zespołów; często bywa na koncertach niszowych i mało istotnych.
1. Jakie wyzwanie TP dała radę przezwyciężyć? Co ją to nauczyło?
?
1. Co jest szczególnie upierdliwe dla TP i dlaczego?
Jako Diakonka jest nienaturalnie ładna. To powoduje, że jako streamerka ma czasem "niewłaściwą" audiencję. Używa makijażu mającego zneutralizować jej urodę ;-).
## Motto

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|nieco przymusowa współpracowniczka 'Na Świeczniku', aż odpracuje...|Powrót Karradraela|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|150718|okazała się faktycznie kiedyś być kontrolerem Spustoszenia. Sama sobie to zrobiła by "wymazać IM pamięć".|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|która dochodzi do siebie po strasznym poczuciu winy i lukach w pamięci u Draconisa.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|"kontroler Spustoszenia", osoba, która pękła na przesłuchaniu i osoba wyraźnie "nie do końca" jeśli chodzi o stan psychiczny|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|druga, najpewniej niewinna podejrzana|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|czarodziejka skłonna poświęcić "dziewictwo" siostry dla zniszczenia "Wirusa".|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170228|bezużyteczna aż do momentu, gdy mogła wejść w inkarnację dron i przeszukiwanie hipernetu. Wieś nie służy jej wykazywaniu się ;-).|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170221|obiecała opiekować się Judytą i stąd wszystkie jej problemy. Przyjechała na koncert, skończyła tracąc trzy golemy i niesłusznie oskarżyła Kirę.|[Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|10/02/05|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170501|praprzyczyna wszystkich perypetii; jej ludzka koleżanka (Natalia) została skompromitowana przez maga, więc ta nadała temat korektorom reputacji. Ale nie było jej na to stać... więc poszła metodą bardziej "szantażową". I się wkopała.|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|10/01/09|10/01/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|4|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Dracena Diakon|4|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Wiktor Sowiński|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Paweł Sępiak|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Oktawian Maus|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Infensa Diakon|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Draconis Diakon|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Tamara Muszkiet|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Salazar Bankierz|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Mikado Diakon|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Marian Łajdak|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Marcel Bankierz|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Lucjan Kopidół|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|Kira Zajcew|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|Julia Weiner|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Judyta Maus|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|Joachim Kartel|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|Ignat Zajcew|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|GS "Aegis" 0003|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Franciszek Baranowski|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|Łucja Rowicz|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Zenon Weiner|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Władysław Lusowicz|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Tymoteusz Maus|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Tadeusz Baran|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Stanislaw Przybysz|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Sonia Stein|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Sieciech Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Paweł Robercik|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Norbert Sonet|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Netheria Diakon|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Natalia Kamenik|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Michał Ostrowski|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Michał Brukarz|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Marta Szysznicka|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Mariusz Trzosik|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Mariusz Garaż|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|Marian Agrest|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Krzysztof Cygan|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Kinga Grzybnia|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Kermit Diakon|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Józef Krzesiwo|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Joachim Kopiec|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Jerzy Szczupanek|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Jan Adamski|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Ireneusz Bankierz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Ilona Amant|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Grzegorz Kamenik|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Grażyna Szczupanek|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Gala Zajcew|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Ewa Zajcew|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Estrella Diakon|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Eryk Płomień|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Ernest Maus|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Aurel Czarko|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Artur Żupan|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Antonina Wysocka|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|Antoni Chlebak|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|Annaliza Delfin|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Andżelika Leszczyńska|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|Andrea Wilgacz|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|Aleksandra Trawens|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|Aleksander Sowiński|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
