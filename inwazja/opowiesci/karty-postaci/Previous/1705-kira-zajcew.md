---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "kić"
title: "Kira Zajcew"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)



### Zachowania (jaka jest)

- **nie lubi dziennikarzy / prasy itp**: "wystarczy, że raz zniszczyliście mi karierę"
- **opiekuńcza wobec mieszkańców wsi** : "to MOJA wieś i jak się nie będziesz zachowywał to stanie się Twoim grobem…"
- **geek źródeł energii** : "mam JESZCZE INNĄ metodę zasilania! wee!"

### Specjalizacje

- **pozyskiwanie energii z organizmów żywych**: (bio / technomantka)
- **kontrola przepływów energii**: (katalistka)
- **niszczycielska kataliza**: (katalistka)
- **źródła energii**: Kira potrafi wyciągnąć energię niemal ze wszystkiego, magicznego lub nie. Zbuduje generator na cokolwiek, co daje choć odrobinę energii
- **ognista magia Zajcewów**
- **zabezpieczenia magiczne: głównie oparte o energię i katalizę**

### Umiejętności

- **rolnik**:

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         -1      |         0        |     +1     |   +1   |      +1       |     -1     |     -1      |

### Specjalne

## Magia

### Szkoły magiczne

* **kataliza**
* **magia elementalna**
* **biomantka**
* **technomantka**

### Zaklęcia statyczne

- "Wielkie wzmocnienie Igora" : link (zaklęcie dające ogromne wzmocnienie - siła, prędkość, postrzeganie... - kosztem późniejszego wyczerpania)

## Zasoby i otoczenie

### Powiązane frakcje

### Kogo zna

- **wąskie grono specjalistów od energii**: Kira jest znana w gronie magów zajmujących się energią. Jest to chyba jedyna grupa nie uważająca jej za defilerkę.
- **kataliści**:  
- **miejscowi ludzie**: Jako pani sołtysowa ma całkiem spore wpływy we wsi, potrafi sporo zorganizować i załatwić dzięki swojej pozycji.

### Co ma do dyspozycji:

- **własnej produkcji źródła energii**: Kira lubi konstruować przeróżne źródła energii i zwykle ma jakieś w zapasie
- **warsztat**: na wsi nigdy się nic nie marnuje. W stodole jest zawsze mnóstwo uszkodzonych sprzętów, które można naprawić lub rozłożyć na części. Dodatkowo, ich warsztat jest naprawdę dobrze zaopatrzony.
- **komponenty do budowy źródeł**: zawsze ma sporo różności w warsztacie.
- **Świeca: reputacja defilerki**: w Świecy uważana za dziwną defilerkę. Magowie się jej boją. Stały mallus do akcji social friendly, bonus do social aggressive, zależny od stopnia przestraszenia drugiej strony.
- **artefakty magii mentalnej**: do czyszczenia pamięci w razie konieczności

### Surowce
2
# Opis

Mieszka we wsi Stokrotki.
Wyklęta przez magów, uważana powszechnie za defilerkę. Cieszy się opinią wyjątkowo przerażającej - nie dość, że w jej pobliżu nie ma śladów magii krwi, to jeszcze ona sama nie poddała się uzależnieniu...

We wsi nazywają ją "panią inżynier", choć nigdy nie twierdziła, że ma jakiś dyplom. Ale zna się na rzeczy, a urządzenia w jej pobliżu działają lepiej a wiejskie generatory energii ze źródeł odnawialnych regularnie dostarczające energię do sieci - zysk dla gminy.

Do swoich projektów wykorzystuje różne narzędzia, z mężem mają naprawdę dobrze wyposażony warsztat.

1. Z czego TP jest szczególnie dumna (co jest doceniane przez innych)? Dlaczego jest z tego dumna?
To ona skonstruowała jeden z generatorów w kompleksie centralnym Świecy. Dlaczego jest z tego dumna? Bo to majstersztyk w tej dziedzinie i jej autorski pomysł. Bez jej udziału rzecz nie byłaby warta zachodu.

1. Z czego TP jest znana najlepiej wśród innych? Wśród kogo (kim są ci "inni")?
Jej sława ma trzy aspekty:
a) "Ta dziwna defilerka, która się nie uzależniła" - ci, którzy nie rozumieją jej fachu boją się jej, uważając za defilerkę. Do tej grupy zalicza się większość tych, którzy poszukali podstawowych informacji na jej temat.
b) Wśród katalistów znana jest jako spec od pozyskiwania i manipulacji energią.
c) Wśród ludzi uważana za inżyniera - złotą rączkę, która potrafi naprawić każde urządzenie.

1. Co jest uważane za nieuczciwą przewagę TP?
Praktycznie zawsze i każdej sytuacji jest w stanie zdobyć energię - czy to magiczną, czy inną, jak długo w pobliżu jest coś żywego.

1. Co sprawia, że TP uzna, że TO ZADANIE jest perfekcyjne dla niej? Czemu?
Każda okazja do wykorzystania lub wytworzenia nowego rodzaju energii. Wszelkie wzmocnienie organizmów żywych poprzez naenergetyzowanie (może skutkować dodatkowymi zdolnościami typu porażenie prądem), jak również potrzeba usunięcia nadmiaru energii.

1. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
Jest bardzo opiekuńcza wobec swojego terenu (wioski). Ludzi w niej traktuje zgodnie z zasadą "może to tylko ludzie, ale to MOI ludzie". Poza tym, jest sołtysiną tej wsi, siłą rzeczy musiała nawiązać z nimi kontakt.
Mimo wszystko jest mocno związana z rodem. Pomoże jak będzie mogła, co nie oznacza, że za darmo...

1. W jakich okolicznościach "Jak trwoga, to przyjdą do TP"? Kto przyjdzie?
"Jak trwoga to do Kiry" w każdej sytuacji, gdy potrzeba zasilić / naładować coś dziwnego. 
Kto przyjdzie: rodzina, ktoś nieświadomy jej "sławy" lub ktoś dostatecznie mocno zdesperowany.

1. Czego TP najbardziej żałuje? Co zrobiłaby inaczej? Dlaczego?
Gdyby potrafiła jakoś zmienić swoją opinię wśród magów... Niestety, nie umiała tego zrobić na początku, a potem było już za późno...

1. O czym TP marzy? Do czego dąży? Dlaczego?

1. Co TP najbardziej lubi robić w czasie wolnym? Czemu?

1. Jakie wyzwanie TP dała radę przezwyciężyć? Co ją to nauczyło?

### Koncept

### Motto

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|161018|która powiedziała Henrykowi o tym, że w okolicy da się zdobyć kilka quarków - i wpakowała go w "to bagno".|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|10/07/02|10/07/04|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170321|sprowadziła armię saperów na Stokrotki i zapałała głęboką nienawiścią do glukszwajna wyżerającego jej źródła...|[Tajemnica podłożonej świni](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160927|"nie jest defilerką", sprowadziła Syberię do wsi i naładowała GS Aegis 0003 ekologiczną energią. Też: ma dość części swojego rodu (Ignata).|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170228|wyciągająca Franciszka z kicia i twardo wykorzystująca przywileje Świecy i swoją reputację. Wypędziła tajemniczego maga.|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170221|katalityczna niszczycielka EMP golemów, dronów i wszystkiego. Anihilator w spódnicy.|[Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|10/02/05|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|4|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Łucja Rowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucja-rowicz.html)|2|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|2|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-judyta-maus.html)|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|2|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html), [170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Świnex Jan Halina Kulig SC](/rpg/inwazja/opowiesci/karty-postaci/9999-swinex-jan-halina-kulig-sc.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Zenobia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Władysław Lusowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-wladyslaw-lusowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Wojtek Leśniak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojtek-lesniak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Wieprzpol](/rpg/inwazja/opowiesci/karty-postaci/9999-wieprzpol.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Niektarij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-niektarij-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Michał Brukarz](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-brukarz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Mariusz Garaż](/rpg/inwazja/opowiesci/karty-postaci/1705-mariusz-garaz.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Krzysztof Cygan](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-cygan.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1705-krystian-korzunio.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Kinga Grzybnia](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-grzybnia.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Karol Kamrat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kamrat.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Kamil Czapczak](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-czapczak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Jelena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-jelena-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-ignat-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Gaweł Wieciszek](/rpg/inwazja/opowiesci/karty-postaci/9999-gawel-wieciszek.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Felicja Strączek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-straczek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Damazy Rozenblum](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-rozenblum.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Carlos Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-carlos-myszeczka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Andrzej Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Alicja Makatka](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-makatka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
