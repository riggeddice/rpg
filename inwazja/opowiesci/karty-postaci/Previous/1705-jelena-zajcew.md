---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Jelena Zajcew"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **impuls_jak_to_realizuje**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Rudowłosa, wielka, bezpośrednia.
Niepokorna, uparta, ognista.
Nie da sobą pomiatać.
Z chęcią trzaśnie w pysk, a łapę ma ciężką.
Dla rozrywki i utrzymania formy zdarza się jej chodzić po gorszych dzielnicach i prowokować bójki… niestety, w wielu miejscach już ją znają.
Szewc chciałby pić jak ona.
Poradzi sobie z większością broni, ale w starciach z bliska woli walkę wręcz.
Uwielbia hazard, potrafi wyłapać większość prób oszustw.

Bardka Zajcewów.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|161018|opiekowała się miejskim księdzem (* magiem), brała z niego rany, opieprzała kuzynów i ogólnie wypyszczona bardka Zajcewów.|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|10/07/02|10/07/04|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Niektarij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-niektarij-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1705-krystian-korzunio.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-kira-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Damazy Rozenblum](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-rozenblum.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
