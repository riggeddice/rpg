---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Ofelia Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **challenge seeker**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **zadziorna**
* **twarda**
* **drapieżnik**
* **niemoralna**
* **"prawo jest tylko narzędziem"**

### Specjalizacje

* **obrońca (prawnik)**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **arystokratka**
* **prawnik świata magów**
* **duelist**
* **orator**
* **poezja erotyczna**



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne

* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **wielu prawników Świecy**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* rzadkie kodeksy i kruczki prawne
* ugruntowana marka i renoma
* dworek w Karym Grodzie
* przepastne archiwa historyczne

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis
Lady Ofelia Bankierz jest 44-letnią czarodziejką o wysokim poziomie kultury i intelektu. Na co dzień mieszka w Warszawie, ale przybyła do Kopalina odpowiadając na pojawienie się tam Hektora Blakenbauera i spodobało jej się; została. Lady Ofelia Bankierz ma mały dworek w Karym Grodzie, w którym normalnie mieszka z trójką dzieci. O jej reputacji świadczy fakt, że nikt nigdy nie odważył się na atak na owe dzieci.
Czarodziejka Świecy, choć sympatyzująca ze Szlachtą. Ma opinię doskonałej oratorki, bardzo dobrej pojedynkowiczki a do tego słynie z popularnych tomików poezji erotycznej. We wszystkim co robi musi być zawsze najlepsza; nie ustępuje i oczekuje, że druga strona też nie ustąpi. Jeśli ktoś unika z nią konfliktu, lady Ofelia potrafi zmusić drugą osobę do walki.
Prywatnie, bardzo sympatyczna. Tyle, że nadmiernie dąży do współzawodnictwa w każdym calu.
Jedna z najlepszych prawniczek Srebrnej Świecy.

## Motto

"Tekst"

# Historia:
