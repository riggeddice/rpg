---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
owner: "public"
title: "Krystalia Diakon"
---
# {{ page.title }}

## Koncept

Dr Jasmine Peril + Dark Eldar w służbie Slaanesha

Narkotykowy terror Diakonów, unyielding force. Awatar intensywności i badaczka koralowców. Kralothbound.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | intensywność; doskonałość         |
| Społeczne         | intensywność; prawda              |
| Wartości          | silne emocje; wytrwałość          |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| wyrwij fabokla od Arazille; pokaż każdemu jego prawdziwą naturę             | pomóż innym żyć w przyjemnej, bezproblemowej iluzji; ukoj cierpienie ekranując od rzeczywistości  |
| wymuś silne emocje od 'twardziela'; sprowadź hardego woja do łkającej bezradnej formy| pomóż innym zachować twarz i kontrolę nad sobą |
| dąż do maksymalizacji intensywności wszelkich wrażeń wszystkich osób        | chroń słabe umysły przed rzeczami dla nich niewłaściwymi |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| hodowca           | koralowce; kraloth-derived        |
| naukowiec         | czarne eksperymenty; intensyfikacja czucia; tortury |
| farmaceuta        | psychotropy; mindbreak |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| zniewolenie ofiary; wzbudzanie grozy; unieszkodliwianie celu; przesłuchiwanie          | psychotropy; intensywne uczucia; torturowanie silnymi impulsami; mindbreak   |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                                |
|----------------------|----------------------------------------|
| Narkotykowy Koszmar  | przerażająca; niewyłączalna z akcji    |
| Kralothbound         | samosyntezator biologiczny; nieludzka  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
| żywy czołg; niezatrzymywalna i nieperswadowalna; fizycznie i chemicznie | jej percepcja rzeczywistości jest inna; fatalna w obserwacji |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia Mentalna      |  |
| Biomancja           |  |
| Magia Zmysłów       |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Koralowce i kraloth-derived        |  |
| Psychotropy; różne dziwne środki   |  |
| Świta agentów pod jej kontrolą     |  |
| Przerażająca reputacja             |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

Jedna z najbardziej przerażających postaci w całym Millennium. Stymulanty, narkotyki, wiecznie pod wpływem. Szuka intensywności. Zelotka intensywności we wszelkich postaciach. Koralowce. Kralotyczne kwiaty. Jej ciało jest świątynią syntezy dziwnych środków. Pracuje nad projektem "Koralowa Utopia" - antyArazille.

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|ktoś nadużył jej dobrego imienia. Osobiście zaangażuje się w znalezienie Bójki, bogowie i magowie be damned...|Adaptacja kralotyczna|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180310|cień Diakonki (nieobecna), która robi eksperymenty z koralowcami i plasterkami. Wspiera Bójkę Diakon w jej eskapadach w Mordowni.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170718|degeneratka i narkomanka. Zgubiła 'koralowce' i próbowała je znaleźć. Przy okazji, ludzie symbiotyczni z koralowcami rozwiązali problem dziwnego źródła energii...|[Umarł z miłości](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|10/10/26|10/10/28|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|140708|bardzo zdestabilizowana po zniknięciu Teresy, jeszcze nie odzyskała przytomności. Andrea podejrzewa, że to Margaret Blakenbauer.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140604|która chciała zabić Karolinę Maus krzycząc "ona mnie zabiła". We wspomaganej wyhodowanej uprzęży jest trudna do zatrzymania... ale był tam Wacław.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140401|odwracająca mindwormy na lewo i prawo oraz coraz bardziej się destabilizująca mentalnie. Nie nosi kolii.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140201|podejrzana o bycie agentką Moriatha hodowczyni biżuterii organicznej. Zaćpana, powiedziała Andrei coś o przeszłości Moriatha i że nic już nie pamięta.|[Ona zdradza, on zdradza](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140312|bardzo niestabilna; zaatakowała Teresę i została obezwładniona przez Wacława.|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140219|potencjalnie jedyna znana osoba zdolna do przypadkowego usuwania mindwormów używając kolii. Przygotowuje jakąś uprząż. Kraloth się jej brzydzi.|[Niespodziewane wsparcie](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140103|właścicielka zaginionej kolii i patentowa ćpunka (też: niestabilna psychicznie).|[Tak bardzo nie artefakt](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|7|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|6|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|5|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|5|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|4|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|3|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|3|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Radosław Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-krowka.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Emilia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-szudek.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Zenon Stecki](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-stecki.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tony Armadillo](/rpg/inwazja/opowiesci/karty-postaci/9999-tony-armadillo.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Tomasz Klink](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-klink.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Stefan Piżuch](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-pizuch.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1803-siluria-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Mateusz Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-krowka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Marian Rustyk](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rustyk.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ksenia Armon](/rpg/inwazja/opowiesci/karty-postaci/9999-ksenia-armon.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Krzysztof Brakujowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-brakujowiec.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Kornelia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-szudek.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Katarzyna Marszał](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-marszal.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Jolanta Karbon](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-karbon.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Jadwiga Opaszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-jadwiga-opaszczyk.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
