---
layout: inwazja-karta-postaci
title:  "Paulina Tarczyńska"
categories: profile
guild: "Niezrzeszony"
type: PC
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

- **obrońca słabszych**: Paulina nie zgadza się na to, aby silniejsi krzywdzili słabszych - czy to ludzi czy magów
- **lekarz z powołania**: "Po pierwsze: nie szkodzić". Również: zawsze gotowa do służby, stara się pomóc wszystkim chorym
- **make things right**: Paulina próbuje pozostawić świat lepszym, niż go zastała. To oznacza nie tylko zmianę nastawienia magów do ludzi, ale też usuwanie zagrożeń czy uświadomienie magów, że robią źle krzywdząc ludzi
- **nadmiar magii jest szkodliwy**: Paulina unika czarowania bez potrzeby i stara się pozostawić świat w balansie magicznym.
- **społecznik**: Paulina pragnie, aby społeczność, którą uważa za swoją, _działała_. To oznacza, że niedopuszczalne są szkodliwe zachowania czy postawy.

### Zachowania (jaka jest)

- **uparta**: będzie jak mówię! I już!
- **pragmatyczna**: czasem trzeba odciąć rękę, żeby uratować człowieka...
- **altruistka**: pomóc jak największej ilości istot, szczególnie ludzi, nawet własnym kosztem
- **opanowana**: w sytuacji stresowej nie wpada w panikę, tylko racjonalnie myśli
- **opiekun: Dracena** (tymczasowe): Paulina zrobi, co w jej mocy, aby pomóc Dracenie

### Specjalizacje

- **diagnoza/badania**: jak nie wiem, co się dzieje, nie mogę pomóc
- **lekarz magiczny**: leczenie to jej powołanie. 
- **puryfikacja**: Paulinie nie raz musiała sobie radzić z nadmiarem magii
- **nekromancja**: nietypowo, jak na lekarza, ale mocy się nie wybiera...
- **"you will see the err of your ways"**: Trochę paladynka, Paulina potrafi sprawić, żeby inni ujrzeli jej punkt widzenia
- **aktywny słuchacz**: Paulina potrafi tak pokierować rozmową, że ludzie sami się jej zwierzają; nie od razu z tajemnic, ale z Pauliną dobrze się rozmawia

### Umiejętności

- **lekarz**: Paulina skończyła odpowiednie studia, kursy i tak dalej. Pracuje normalnie jako lekarz; prowadzi własną praktykę.
- **negocjatorka**: 
- **corruptor**: Paulina potrafi przekabacić ludzi na swoją modłę.
- **plotki**: Ludzie chętnie dzielą się różnymi plotkami.
- **gra na gitarze**: Bo lubi
- **lekarzowi się nie kłamie**: Paulina potrafi wydobyć prawdę z rozmówcy.

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         0       |        +1        |     +1     |   0    |      0        |      0     |     -1      |

### Specjalne

* soul link z Marią

## Magia

### Szkoły magiczne

- **biomancja**
- **magia mentalna**
- **astralika**
- **kataliza**

### Zaklęcia statyczne

- "ulepszenie ciała" - poprawia siłę i czas reakcji... taki super-człowiek
- "stabilizacja chorego" - Paulina nie zawsze będzie w stanie uratować pacjenta, ale jeśli ten jest w stanie stabilnym, to daje jej to więcej czasu na działanie
- "naprawa do wzoru" - przywrócenie kogoś do pełnej zgodności ze wzorem to najwyższa i najskuteczniejsza forma leczenia

## Zasoby i otoczenie

### Powiązane frakcje

### Kogo zna

- ludzką społeczność
- wyrzutki świata magicznego, którym pomogła
- mentor: Kazimierz Przybylec
- lekarzy magicznych i nie tylko
- magów z małych gildii (plankton)
- półświatek magiczny
- kluby, w których grywała
- Krystian Korzunio

### Co ma do dyspozycji:

- przenośny gabinet lekarski (praktyka)
- kryjówki i meliny: ci, którym pomogła, chętnie pomogą się jej ukryć
- zapas quark: Paulina regularnie zbiera rezydualną energię z przychodzących do niej ludzi.

### Surowce
2

## Inne:

Paulina jest lekarką. Urodziła się jako człowiek; stała się magiem w trakcie Zaćmienia. Zidentyfikowana jako mag przez Kazimierza Przybylca i przez niego wyszkolona. Również on dał jej możliwość wyboru w kwestii przyłączenia się do Świecy , a ona postanowiła pójść własną drogą.

## Motto

"Po pierwsze: nie szkodzić"
"Magowie sobie poradzą, to ludzi należy chronić."

# Historia