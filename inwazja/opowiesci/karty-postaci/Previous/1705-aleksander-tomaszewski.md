---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC - temp"
title: "Aleksander Tomaszewski"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **ryzykant**: lubi rzeczy niestandardowe i dające dreszczyk emocji
* **pragmatyczny**: łatwiej korzystać z innych ludzi gdy cię lubią
* **marzyciel**: tęsknota za światem, jaki mógł być - ale nie dostał się na śledczą

### Specjalizacje

* **kradzież danych (informatyk śledczy)**: opis

### Umiejętności

* **nauczyciel informatyki**
* **informatyk śledczy**
* **dobry kontakt z młodzieżą**
* **con artist**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        +1       |         0        |     +1     |   +1   |      -1       |      0     |     -1      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* lokalne siły prawa i porządku
* młodzież w Okólniczu
* nauczyciele i rodzice z Okólnicza

### Co ma do dyspozycji:

* sprzęt w pracowni komputerowej
* dobrej klasy komputer i oprogramowanie 'śledcze'

### Surowce

* **Wartość**: 2
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis
nauczyciel informatyki z Okólnicza

Aleksander Tomaszewski – nauczyciel informatyki z liceum Ireny. Underachiever, ale inteligentny. Nauczycielem nie został z powołania, ale z braku pomysłu na siebie, udaje mu się jednak utrzymać dobry kontakt z młodzieżą, a rodzice ufają jego dobrym intencjom. Jest wychowawcą klasy, do której uczęszcza/ uczęszczała Irena. Wiek 30 lat. Kawaler, typ samotny. Z jakiegoś powodu upodobał sobie w klasie Irenę i chce jej pomóc. Nie chce angażować policji, ponieważ boi się o zdrowie i życie Ireny, podczas gdy policja w Polsce jest zazwyczaj nieudolna i rozwlekła, dodatkowo Aleksander żyje w przeświadczeniu, że organy państwa chcą dla niego jak najgorzej i nie ufa im. Oprócz tego ma też egoistyczny powód, liczy że wprowadzi to do jego życia trochę odmiany i ekscytacji, kiedyś chciał pracować w informatyce śledczej, ale był za słaby na studiach.

Traktuje Irenę jak pupilka, sprawia mu przyjemność interakcja z młodą sympatyczną dziewczyną która wyraźnie mu ufa. Nic zdrożnego.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160809|doprowadził policję do faktycznych zbrodniarzy, googlował po nocach i się nikomu nie kłaniał. O dziwo, przeżył.|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Leopold Teściak|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Karolina Maus|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Karina von Blutwurst|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Jolanta Lipińska|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Izabela Bąk|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Irena Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Filip Czumko|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Elżbieta Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Damian Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Antonina Brzeszcz|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Adolphus von Blutwurst|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
