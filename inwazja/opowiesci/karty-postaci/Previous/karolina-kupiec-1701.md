---
layout: inwazja-karta-postaci
title:  "Karolina Kupiec"
categories: profile, kadem
---
# {{ page.title }}

# Mechanika

## Impulsy (co motywuje / napędza postać):

### Strategiczne

* **hedonistka**: "Jeżeli to sprawia ci przyjemność, czemu chcesz sobie tego odmawiać? Przyjemność jest celem, najwyższym."
* **szanuję każde poglądy**: "Bez dobrowolności prawdziwa przyjemność jest nieosiągalna. Nic na siłę, nic wbrew Tobie. Szanuję każde poglądy."

### Taktyczne

* **pracowita**: "Chcę zostawić coś po sobie. Społeczność, artefakty, badania. Chcę, by po mnie zostało coś dającego radość innym."
* **hojna**: "Co warte są środki, jeżeli trzymam je w sejfie jak smok? Wszystko odzyskam jak będę potrzebowała; jak Ty czegoś potrzebujesz, po prostu poproś"
* **pomocna**: "Niezależnie od okoliczności, warto pomagać. Dobre uczynki wracają. Każdy ma jakąś historię w którą warto zainwestować."

## Co umie (co postać potrafi):

### Ekspercko (poziom 3):

* **hedonistyczne środki psychoaktywne** (chemik)
* **unieszkodliwianie przeciwnika** (chemik, biomantka)
* **ukojenie** (nastroje, mówczyni)

### Zawodowo (poziom 2):

* **terminus - broń palna**
* **chemik (środki psychoaktywne)**
* **charyzmatyczna mówczyni**
* **biomantka**
* **nastroje**
* **herbalistka**
* **trucizny, toksyny, narkotyki…**
* **naukowiec: organizm maga**

### Amatorsko (poziom 1):

* **podstawy medycyny**

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        -1       |        +1        |     +1     |   +1   |      +1       |     -1     |     -1      |


## Kieszonkowe

* Nie dba o swoje surowce, wychodzi z założenia, że komunalnie każdy może korzystać: 1

## Otoczenie powiązane

### Powiązane frakcje

* KADEM, bez subfrakcji

### Kogo zna

#### Doskonale: 3

* **doskonała_znajomość**: opis
* **doskonała_znajomość**: opis

#### Nieźle: 2

* **niezła_znajomość**: opis
* **niezła_znajomość**: opis

#### Luźno: 1

* **większość magów KADEMu jest jej winna małe przysługi**
* **znana w kręgach młodych magów różnych gildii**
* **grupa fanów i "wyznawców" wśród magów i ludzi**
* **luźne kontakty wśród… dilerów**

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

* **nie uzależniające hedonistyczne środki psychoaktywne**: opis
* **dobrej klasy broń i pancerz KADEMu**: opis

#### Ponadprzeciętne: 2

* **ponadprzeciętne_obiekty**: opis
* **ponadprzeciętne_obiekty**: opis

#### Niezłe: 1

* **prezenty i podarunki od jej podobnych magów**: opis
* **kiepskie_obiekty**: opis

## Magia:

### Magia dynamiczna:

* magia nastrojów - specjalizacja uspokojenie, kojenie
* trucizny, toksyny, narkotyki, alchemia bojowa
* biomancja (lifeshaper)

### Zaklęcia hermetyczne

* Sprowadzenie Błogości: cel wpada w przyjemny półtrans na pewien czas.

## Specjalne:

* kotwica pryzmatyczna: +2 do przesunięcia Pryzmatu na hedonizm i przyjemność

# Opis

## Motto

"Tekst"

## Inne

There are many different Magi with various motivations. The same applies to terminus. Some terminus fight for peace. Some other fight for their side. Caroline? She fights for good feelings and overall pleasure. She calls herself a hedonistic terminus fighting for overall good mood.

She used to be a Millennium mage – she thought that Millennium would be the Guild most suitable to her personality. However, she wasn't able to cope with kraloth and with Mafia-like structure of the Guild. Although she still values and respects Magi of Millennium, she is simply disaligned with the goals of the Guild.

Caroline has managed to enter KADEM, which surprised a lot of Magi – she simply is neither the most powerful mage around nor the most skilled in any particular area. Her personality, however, allows her to thrive in the prismatic world of Daemonica; this means she is not at a disadvantage in dual reality of KADEM and no matter what happens - she is an asset. Maybe not the strongest asset, but an asset nevertheless.

A friendly terminus who tries to keep everyone's morale high and who helpfully tries to solve everyone's problems, she is generally liked in her new Guild.

Caroline specializes in 'drugs'. Or rather, 'psychoactive vectors of hedonism'. She is a valiant researcher and crafter of different things which give pleasure and sooth the pain of existence. Quite often, she works with Warmaster; he is the dark emo brooding one and she is the flowery lollipop giving away her resources to make everyone around happy.

She may not be the best terminus around, but she is decent. Can do her stuff. She identifies herself not as a terminus but as a "happiness vector".

As a terminus, she specializes in disabling and non-lethal elimination of her enemies. If needed, she will simply shoot.

# Historia
