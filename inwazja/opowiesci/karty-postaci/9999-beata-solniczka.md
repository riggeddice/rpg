---
layout: inwazja-karta-postaci
categories: profile
title: "Beata Solniczka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170315|nosicielka Mimika, policjantka i córka legendarnego policjanta; pod wpływem mimika zgwałciła rapera. Mimik z niej przeskoczył dalej...|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wojciech Popolin](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-popolin.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Katarzyna Leśniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-lesniczek.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Ewelina Nadzieja](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-nadzieja.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
