---
layout: inwazja-karta-postaci
categories: profile
title: "Katarzyna Kotek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160927|odłamek Aurelii; NIE CHCE pomóc (ani być Aurelią), ale... powiedziała Franciszkowi i Kirze jak należy naładować GS Aegis 0003.|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150830|która bardziej boi się * magów Powiewu niż potworów tego świata. Nie jest EIS, nie jest Aurelią, ale jest wszystkim naraz. A jednocześnie czymś innym.|[Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150829|stworzona bibliotekarka która jednak ma przebłyski EIS oraz Aurelii; nie jest jednak żadną z nich.|[Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|stworzona jako bibliotekarka Powiewu. Ma przebicia wiedzy Aurelii i EIS. Nie wie, czym jest, ale chce pełnić swą rolę. |[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|3|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Zenobia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Maciej Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Felicja Strączek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-straczek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Dominik Szerściuk](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-szersciuk.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Celestyna Marduk](/rpg/inwazja/opowiesci/karty-postaci/9999-celestyna-marduk.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Andrzej Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Aleksander Szwiok](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-szwiok.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
