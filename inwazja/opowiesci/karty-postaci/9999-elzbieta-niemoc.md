---
layout: inwazja-karta-postaci
categories: profile
title: "Elżbieta Niemoc"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140618|odbita przez Agresta od Zespołu została przyprowadzona do Rezydencji, dzięki czemu nie ma możliwości wpływania przy jej użyciu na Edwina. Nadal w ciąży.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140401|którą porwali Uczniowie Moriatha jako pułapkę... co sprawiło, że Edwin odrzucił wszelkie człowieczeństwo. Trafiła do Bazy Detektywów.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140312|ukochana Edwina która jest zagrożona przez całą tą sprawę.|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
