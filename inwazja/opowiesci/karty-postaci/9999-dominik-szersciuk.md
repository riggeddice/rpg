---
layout: inwazja-karta-postaci
categories: profile
title: "Dominik Szerściuk"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150829|nieszczęsny strażnik Skorpiona, który został zainfekowany przez potwora i był torturowany przez Olgę.|[Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Aleksander Szwiok](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-szwiok.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
