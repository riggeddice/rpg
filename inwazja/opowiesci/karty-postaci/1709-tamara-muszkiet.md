---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Tamara Muszkiet"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Świeca nie jest źródłem dobra**
    * _Aspekty_: Świeca stanie się nieistotna, Świeca będzie skorumpowana, Świecę trzeba będzie zniszczyć, stopniowe umieranie Świecy, świat opanuje anarchia, rodzina Tamary ucierpi, prawa będą ignorowane
    * _Opis_: Tamara silnie wierzy w to, że Świeca jest jedyną siłą zdolną do zachowania porządku i uratowaniu wszystkiego i wszystkich. Dla ratowania Świecy, jej spójności i wzmocnienia jest skłonna posunąć się bardzo daleko. Jednocześnie z całego serca wspiera nowe Dekrety Kopalińskie - złe traktowanie ludzi musi być traktowane tak surowo, jak złe traktowanie magów.
* **ŚR: Po czynach nas poznacie**:
    * _Aspekty_: wypala zło ze Świecy, promuje Świecę w dobrym świetle, brudy pierze się w Świecy, oczekuje od Świecy perfekcji, efektowne i skuteczne działania, każdy ruch na świeczniku, akcje prowadzą do okrutnych konsekwencji, terminus chroni innych
    * _Opis_: Tamara wie, że nie wystarczy wszystkich chronić by Świeca odzyskała potęgę. Oni muszą widzieć, jak Świeca im pomaga. Tamara nie jest ekspertem od retoryki - ale może pokazać czynami, czemu Świeca jest warta wspierania. Wypala czarne owce ze Świecy trotylem i napalmem oraz pomaga wszystkim zawsze jednak dbając o interes Świecy jako najważniejszy.
* **FIL: Przyszłość kosztem człowieczeństwa**:
    * _Aspekty_: "jestem oficerem a nie osobą", "jestem terminusem", bezwzględna do bólu, lojalna hierarchii, odrzucenie ego i prywaty, cel uświęca środki, sukces i efekty nad moralność
    * _Opis_: Tamara jest przekonana, że dzisiejszy dzień nie ma znaczenia. Tak naprawdę wszystko co jest robione to walka o JUTRO, o inną przyszłość. Jest terminusem i potrafi podejmować trudne decyzje - nie przeszkadza jej, że przez to nie ma wielu przyjaciół czy jest uważana za "kij w dupie". Bezwzględna i lojalna hierarchii, uważa się za środek do świetlanej przyszłości a nie za osobę.

### Umiejętności

* **Krawiec**:
    * _Aspekty_: piękne mundury, nienaganny styl, świetne suknie i mundury, praca z trudnymi materiałami, podąża za modą, bardzo elegancka, materiał magiczny i egzotyczny
    * _Opis_: Zanim była terminuską, była krawcową. Naprawdę dobrą krawcową. Do dzisiaj kocha modę i zbiera świetne suknie i mundury. Odkąd jest oficerem terminusem używa mundurów częściej a sukien rzadziej, ale nadal dobry styl i szyk jej nie opuszczają.
* **Konstruktor**:
    * _Aspekty_: konstrukcja pancerzy magitech, naprawa magitech, dobór właściwego materiału, maskowanie prawdziwej natury pancerza
    * _Opis_: Doświadczona krawcowa i czarodziejka przerzuciła się na pancerze magitechowe - świetnie wyglądające i zabójcze. Dobrze ukryte pancerze magitechowe z zaawansowanym sterowaniem uratowały życie niejednego terminusa.
* **Terminus**: 
    * _Aspekty_: oficer, anty-mag, przejmowanie dowodzenia, zastraszanie, taktyk przeciw magom, walka magitechem, walka elementalna, walka minionami, przesłuchiwanie, arystokratyczne maniery, lepiej atakuje niż się broni
    * _Opis_: Tamara ma opinię czarnego paladyna - osoby, która przejmuje dowodzenie i niszczy przeciwnika używając swoich magitechów, minionów itp. Jej fanatyzm i metody ogólnie budzą strach.
* **Władca marionetek**:
    * _Aspekty_: precyzyjne rozkazy, skoordynowane działania, dowodzenie magitechami, kontrola wielu rzeczy jednocześnie, "oni grają w FPS a ja w RTS"
    * _Opis_: Tamara, zwłaszcza w magitechu jest wyjątkowo niebezpieczna - potrafi sterować dziesiątkami zaawansowanych i autonomicznych (lub nie) jednostek. Większość jej planów opiera się na taktyce klonów i swarmowania przeciwnika minionami, w trakcie gdy agenci robią to co powinni.


### Silne i słabe strony:

* **One Mage Army**:
    * _Aspekty_: nigdy nie jest sama, zawsze w magitechu, wspomaganie magii elementalnej, fatalna reputacja, "nie jesteś jedną z nas"
    * _Opis_: Tamara zrobiła kilka błędów i zawsze raczej trzymała się swojej dumy niż zdobywała sojuszników i za to teraz płaci. Jednak poza tym jest zwykle przygotowana do walki dzięki swojemu self-activatorowi i zwykle towarzyszy jej kilka jej magitechowych golemów.

## Magia

### Szkoły magiczne

* **Magia materii**:
    * _Aspekty_: magitechowe pancerze, materiał krawiecki, wzmacnianie materii, lokalizacja słabych punktów w celu, niszczenie barier, lekkie i wytrzymałe
    * _Opis_: Tamara przede wszystkim skupia się na budowaniu odpowiedniego materiału i swoich magitechowych pancerzy, ale także na znajdowaniu słabych punktów w konstrukcjach które atakuje.
* **Magia mentalna**: 
    * _Aspekty_: autonomiczne magitechy, golemy, dominacja, zastraszanie, tarcze mentalne
    * _Opis_: Główną siłą Tamary są jej bojowe autonomiczne magitechy. Ale dodatkowo potrafi Dominować ludzi i magów, wzmacniać zastraszanie i stawiać tarcze mentalne. Tamara dość wszechstronnie i okrutnie stosuje magię mentalną, nie do końca przejmując się tym, jak to jest odebrane.
* **Magia elementalna**:
    * _Aspekty_: zasilanie magitech, czary ofensywne, tarcze elementalne, infuzje magitechowe, latanie, trzęsienia ziemi
    * _Opis_: Główna szkoła bojowa Tamary. Tamara jest niebezpieczną elementalistką zdolną do ochrony przed większością ataków elementalnych i do infuzji swoich magitechów odpowiednimi środkami i elementami. 

### Zaklęcia statyczne

* **Lanca entropii Grabiona**:
    * _Aspekty_: materia + elementalna, osłąbienie struktury celu, szukanie słabego punktu, atak najsilniejszym elementem
    * _Opis_: Mordercze zaklęcie bojowe. Wpierw lokalizuje słaby punkt celu, następnie dodatkowo osłabia go Materią a na końcu uderza elementalną lancą z elementem najlepiej dostosowanym do maksymalizacji zniszczeń celu.
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* żołd z ramienia Świecy

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Oddział jednostek magitechowych**:
    * _Aspekty_: lojalny, autonomiczny, oddział bojowy dalekiego zasięgu, osłona operatorów, expendable, jednostki zwiadowcze, intimidating presence
    * _Opis_: Tamara zwykle porusza się w towarzystwie kilku-kilkunastu pancerzy magitechowych zdolnych do autonomicznego działania. Wykorzystuje je jako rekonesans, porywanie jeńców czy multiplikator siły ognia. Sama ich obecność na polu bitwy robi wrażenie na jej opozycji.
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Pochodząca ze świata ludzi tien Tamara Muszkiet zachowuje się jak arystokratka Srebrnej Świecy, mimo, że arystokratką nie jest. Stała się magiem jeszcze przed Zaćmieniem a Zaćmienie jedynie podniosło jej umiejętności. W świecie ludzi była krawcową. Jest przywiązana do systemów hierarchicznych i do chain-of-command.
Jej naturalne umiejętności magiczne predestynowały ją do zostania zbrojmistrzem terminusów, ale Tamara bardzo szybko zmieniła profil na zarządzanie oddziałem terminusów. Mimo, że nie jest najlepszym bojowo terminusem jaki jest pod ręką, jej posłuszeństwo i skuteczne podejście do problemów połączone z umiejętnością zarządzania ludźmi i taktyką sprawiają, że jest skutecznym terminusem pierwszoliniowym jak długo nie jest silnie sfokusowana przez przeciwnika.
Zna i potrafi wykorzystywać zarówno świat ludzi jak i świat magów.
Jej wielką słabością i miłością jest moda. Pasjonuje się modą świata magów i ludzi. Zawsze wygląda nieskazitelnie i adekwatnie.
Postrzegana jest jako "chłodna, lecz uprzejma" i "trochę z boku". Wyjątkowo nie lubi KADEMu.

Wojna z Karradraelem uszkodziła jej reputację i pozostawiła ją silnie poobijaną. Nie ma powrotu na Śląsk za wydarzenia jakie miały miejsce po Lodowym Pocałunku. Nie zmienia to faktu, że Agrest doprowadził do tego, że poszerzyła swoje umiejętności kontroli magitechowych zbroi. Groźniejsza niż kiedykolwiek, Tamara wróciła do akcji.

### Koncept

Czarny Paladyn Świecy. Minion master, jednoosobowa armia zdolna do operowania samotnie przeciwko dużym siłom wroga. Jednocześnie osoba wierząca w Świecę tak bardzo, że ta idea jest silniejsza niż ona sama. Wykorzystanie Tamary na sesjach jest powiązane silnie z jej działaniami ochronnymi wobec ludzi lub Świecy. Jest to też okazja by przerazić postacie graczy - ten fanatyzm, ta miłość...

### Motto

"Świeca tego potrzebuje, więc to się stanie. Pomóż mi, lub mój oddział zrobi to za Ciebie."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|ranna; przez mniej więcej tydzień nie może czarować ani działać aktywnie. Straciła większość Legionu.|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171101|bardzo ciężko chora, nawrót Lodowego Pocałunku i wyniszczona po portalisku. Trudna pacjentka. Paulina załatwiła dla niej od Dukata magimed.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|z narażeniem życia weszła w Eter ratować wciągniętych magów i ludzi i zespół ratujący. Po Paradoksie (cudzym) straciła ubranie i cały Legion. Podrywana przez Sądecznego.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171015|rozsądniejsza niż się wydawało; nie zaatakowała Draceny, osadzona przez Sylwestra i próbująca przekonać Paulinę o tym, że mafia nie może być przez nią wspierana.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|pojawiła się w Powiecie Pustulskim, uderzyła w czujniki Sądecznego i wysłała prosty sygnał - Świeca nie akceptuje nie-gildiowych form kontroli. Spokojna i zimna jak nigdy przedtem.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|160229|głównodowodząca akcją odbijania siedmiu prawie Mausów i ekspert od wojny elektronicznej. Dostarczyła sprzęt wszystkim.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150913|wspomnienie z przeszłości; przechwyciła jakąś komunikację i nadpisała rozkazy Izy w Kotach.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|oglądana ze wszystkich stron przez Andreę (przesłuchania innych). Leży chora pod opieką Agresta w Kopalinie i nikt nie wie gdzie jest.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150704|która najpewniej jest chora; chwilowo MIA. Okazuje się, że ukrywa rodzinę przed Świecą.|[Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|10/05/11|10/05/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150716|ciężko chora na Lodowy Pocałunek, bardzo paranoiczna, która jednak próbowała nie zabić. Złamała wszystkie zasady Świecy.|[Chora terminuska i Żabolód](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|która (jakkolwiek nieobecna) okazuje się być przywódczynią "Kurtyny" w SŚ oraz ekspertem od kombinezonów * magitechowych. |[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161005|działała zgodnie z procedurami, choć dała się sprowokować * magnusowi. Świetnie zaprojektowała akcję i na linii * magów i ludzi. Nie znalazła nic na KADEM.|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|10/05/05|10/05/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|ta, która przekazała Izie Spustoszenie odbierające pamięć oraz tuszowała dla Izy tematy powiązane ze Spustoszeniem.|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|członek 'Stalowej Kurtyny', terminuska uważająca KADEM za gildię niegodną; czasem trudno się zorientować czy walczy ze Spustoszeniem czy z KADEMem.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150224|głównodowodząca operacją SŚ na tym terenie, która użyje WSZELKICH środków by Spustoszenie się nie rozprzestrzeniło.|[Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|10/04/17|10/04/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150110|głównodowodząca terminusami SŚ na operacji która bardzo nie lubi KADEMu|[Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|10/04/15|10/04/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|6|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|6|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|6|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|5|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|5|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|5|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|4|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|4|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|4|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|4|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|4|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|3|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|3|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Korwin Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-korwin-morocz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|2|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[miślęg Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-misleg-pieluszka.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[echo Lugardhaira](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-lugardhaira.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Wojciech Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Szczepan Czarniek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-czarniek.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Roksana Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-roksana-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-pieluszka.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Magnus Spellslinger](/rpg/inwazja/opowiesci/karty-postaci/1709-magnus-spellslinger.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Lugardhair](/rpg/inwazja/opowiesci/karty-postaci/9999-lugardhair.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Liliana Sowińska](/rpg/inwazja/opowiesci/karty-postaci/9999-liliana-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kaspian Miauczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-miauczek.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jolanta Pirat](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-pirat.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jerzy Buława](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-bulawa.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Jan Kotlin](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-kotlin.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Irena Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Hieronim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-hieronim-maus.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Gabriela Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriela-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Elwira Czlikan](/rpg/inwazja/opowiesci/karty-postaci/9999-elwira-czlikan.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Bożena Górzec](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-gorzec.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aniela Lipka](/rpg/inwazja/opowiesci/karty-postaci/1709-aniela-lipka.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
