---
layout: inwazja-karta-postaci
categories: profile
title: "Ireneusz Przaśnik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170423|który współpracuje z policją chcąc oczyścić reputację Czarnego Dworu. Nie chce narkotyków czy zatruć śmiertelnych.|[Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|10/06/24|10/06/27|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151229|lokalny 'mafioso' współpracujący z częścią policji. Miał jakiś deal. 3 lata temu pomógł przeciwko sekcie.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Zenobia Morwiczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-morwiczka.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wojmił Rzeźniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-rzezniczek.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Katarzyna Klicz](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-klicz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Kaja Odyniec](/rpg/inwazja/opowiesci/karty-postaci/9999-kaja-odyniec.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Filip Wichoszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-wichoszczyk.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Edward Ramuel](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-ramuel.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
