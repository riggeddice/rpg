---
layout: inwazja-karta-postaci
categories: profile
title: "Antoni Wójt"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|131008|(38 l) : mąż Jolanty, zwykły człowiek pośród magów i viciniusów który chronił rodzinę jak mógł. KIA.|['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
