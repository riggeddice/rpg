---
layout: inwazja-karta-postaci
categories: profile
title: "Laura Filut"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140910|eks-kochanka Edwina. Chwilowo ranna.|[Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
