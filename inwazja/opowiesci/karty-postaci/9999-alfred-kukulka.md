---
layout: inwazja-karta-postaci
categories: profile
title: "Alfred Kukułka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140401|alchemik porwany przez Uczniów Moriatha który też został uratowany przez Zespół. Trafił do Bazy Detektywów.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140219|porwany alchemik którego kiedyś już próbował porwać Moriath. Zdaniem Czerwca, mindwormowany.|[Niespodziewane wsparcie](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
