---
layout: inwazja-karta-postaci
categories: profile
title: "Crystal Shard"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170830|już: Andy Diakon. Odtworzona przez Arazille - żywy wyrzut sumienia Diakonów i Blakenbauerów. Pomaga Arazille i spotkała się z Silurią by dać jej dowody winy Blakenbauerów.|[Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|10/08/29|10/08/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150422|wampirzyca (kiedyś Diakonka) i niewolnica Tymotheusa Blakenbauera; uwolniona przez Arazille stała się ciałem bogini marzeń.|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150602|cicha bohaterka której działania być może uniemożliwiły sojusz Mirabelki z Tymotheusem. Kiedyś Diakonka. Nadal walczy z tym co jej zrobiono.|[Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|10/02/19|10/02/20|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tadeusz Umiej](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-umiej.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Staszek Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-staszek-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170830](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Emil Maczeta](/rpg/inwazja/opowiesci/karty-postaci/9999-emil-maczeta.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dariusz Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-larent.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
