---
layout: inwazja-karta-postaci
categories: profile
title: "Tomasz Przodownik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170208|inżynier Wzoru w Laboratorium Mgieł; kontestuje biurokrację Świecy i nie lubi Hektora i Silurii, ale wykona rozkazy.|[Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|10/08/12|10/08/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160327|magiczny inżynier zajmujący się przekształcaniem wzoru w Laboratorium Mgieł. Wykonuje rozkazy i unika własnej opinii czy zdania.|[Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|10/06/14|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Oliwier Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-sowinski.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Metody Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-metody-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Klaudia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-klaudia-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Antoni Kurzamyśl](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-kurzamysl.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
