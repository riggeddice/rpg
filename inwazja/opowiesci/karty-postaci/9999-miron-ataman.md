---
layout: inwazja-karta-postaci
categories: profile
title: "Miron Ataman"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150930|bardzo skuteczny policjant Kopalina, który został zmylony; teraz poluje na Joachima Zajcewa ku uciesze Hektora.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
