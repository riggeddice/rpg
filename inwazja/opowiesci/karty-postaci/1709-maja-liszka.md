---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Maja Liszka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: **:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: **: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: **:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ: **:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Najemniczka pracująca dla Huberta Kaldwora.
Mocno stąpająca po ziemi, uznająca, że wszystko można wyjaśnić, a to, czego nie można, da się zastrzelić.
Potrafi posługiwać się zarówno bronią boczną jak i cięższą.
Strzelec wyborowy (broń boczna).
Posiada ulubiony pistolet, który jest jednocześnie jej amuletem - strzeli z niego wyłącznie w ostateczności.
Sporo trenuje, ale nie jest bardzo "napakowana". W walce polega nie tylko na sile, ale również na zwinności.
Przeszła większość wstępnego szkolenia oddziałów specjalnych, odpadła pod sam koniec.
Dla Huberta pełni rolę ochroniarza i - nieoczekiwanie dla większości - zwiadowcy.
Dla rozrywki zajmuje się sztuczkami magicznymi i jest w tym całkiem niezła.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161129|złamała Natalię dając jej zemstę, po czym ją wyciągnęła i oddała rodzicom po czym magia mentalna kazała jej zabić Huberta. Czar złamał Henryk.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161115|chłodna bodyguard Huberta Kaldwora, niezbyt społecznie ukierunkowana z drastycznym planem wyciągnięcia młodej z kultu.|[Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|10/07/21|10/07/22|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
