---
layout: inwazja-karta-postaci
categories: profile
title: "Bolesław Bankierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140312|o którego śmierci właśnie się dowiaduje Andrea; zginął od noża w plecy podczas pojedynku (nożownikiem była osoba trzecia).|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|wybitny zwadźca i najlepszy ekspert od pojedynków nie tylko w Kopalinie ale i na Śląsku. Chciał zmierzyć się z drugą formą Blakenbauera.|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Paweł Grzęda](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-grzeda.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
