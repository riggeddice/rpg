---
layout: inwazja-karta-postaci
categories: profile
title: "Jan Szczupak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160921|który przekazał na żądanie Edwina Wandę (klona) Zdzisławowi i Hubertowi. Elegancki, wzbudził cień zaufania u Wandy.|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150604|uczciwy i zrozpaczony kierowca Hektora Blakenbauera, który musiał pilnować demona_481... przez co nasłuchał się przerażających pornożartów.|[Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|10/05/05|10/05/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140401|który nie tylko wiezie Edwina tam, gdzie ten chce ale też pomaga Edwinowi wymknąć się ze wszystkich możliwych namierzeń przez Hektora. Powiedział Hektorowi, że Edwin i Elżbieta zniknęli.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140312|który wpuścił napastników na teren Rezydencji nie spodziewając się jakie będą konsekwencje. Jego rodzina była zagrożona.|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|dumny i pewny siebie szofer maga ze szlachetnego rodu (Hektora Blakenbauera); skromny, bo rozmawia z plebsem.|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140213|kierowca który ma wyciągnąć Sophistię z aresztu.|[Pułapka na Edwina](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|10/01/01|10/01/02|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|4|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Zdzisław Kamiński](/rpg/inwazja/opowiesci/karty-postaci/1709-zdzislaw-kaminski.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tomasz Kracy](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kracy.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Paweł Grzęda](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-grzeda.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Milena Marzec](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-marzec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Rokita](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rokita.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Hubert Brodacz](/rpg/inwazja/opowiesci/karty-postaci/1709-hubert-brodacz.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grzegorz Włóczykij](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-wloczykij.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[150604](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
