---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Alojzy Przylaz"
---
# {{ page.title }}

## Koncept

Były górnik; ludzista. Teraz: uczeń terminusa. Człowiek pracy. Nie lubi viciniusów i rzeczy "dziwnych".

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | bezpieczeństwo; szacunek; przyjaźń |
| Społeczne         | porządek; wzajemność; pomoc słabszym; praca |
| Wartości          | praca; wytrwałość; uczciwość |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| promuj to co praktyczne i funkcjonalne ponad wszystko                       | wybierz piękno i przeżycia nad przyziemne lecz funkcjonalne rzeczy |
| zatrzymaj krzywdę i niesprawiedliwość, nie przechodź obojętnie              |  |
|  |  |
|  |  |
|  |  |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| górnik            | survival; podziemia |
| mechanik          | maszyny ciężkie; złota rączka |
| wykidajło         | zastraszanie; walka barowa; czytanie ludzi |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| rozładowanie konfliktu | zastraszanie; czytanie ludzi |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia Materii       |  |
| Magia Elementalna   |  |
| Technomancja        |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| były Ognisty Niedźwiedź            |  |
| Alfred Werner, jaszczur Czapkowika |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|zdobył pierwszego nie-wroga w świecie viciniusów: Alfreda Wernera, człowieka-jaszczura.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180418|przebrany za Batmaga chce ratować ludzi w okolicy Czapkowika... ale nie ma serca dobić człowieka-jaszczura. Dał mu żyć.|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|170101|miał swój dzień. Wpierw w Unii z kralothem (by dowiedzieć się o Ogniwie Arazille), potem stworzył Ogniwo w Żonkiborze osłaniany przez Kajetana, potem opuścił oddział Andrei by opiekować się uzależnioną od kralotha Anną Kozak.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|górnik i 4x-letni kandydat na terminusa. Zbudował drogę ucieczki na życzenie Andrei. Dobrze pije z robotnikami.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Paweł Szlezg](/rpg/inwazja/opowiesci/karty-postaci/1803-pawel-szlezg.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
