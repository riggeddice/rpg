---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Kornel Maus"
---
# {{ page.title }}

## Koncept

"Pan od muzyki", "nauczycielka z Dangerous Minds".

Obserwator i łagodny fotograf, którego życiową misją jest pomóc młodym i pragnie za to uznania.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | piękno; sława; relacje            |
| Społeczne         | pozytywizm; humanizm              |
| Wartości          | pacyfizm; pomoc                   |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| pomagaj młodym, znajduj im szanse - są nadzieją i przyszłością              | przedkładaj inne działania nad pomoc indywidualnym jednostkom                                     |
| bądź zaproszony jako keynote czy mistrz i zrób rzecz mistrzowską            | osiągnij sukces, ale tak, by nikt nie zauważył Twojego działania                                  |
|  |  |
|  |  |
|  |  |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| fotograf          | obserwator; dokumentalista        |
| resocjalizator    | praca z młodzieżą; labirynt biurokracji; inspirowanie ku dobru |
| nauczyciel muzyki | edukator; gra na instrumentach; śpiew |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| powstrzymanie od złego; osłona nieletniego przed kłopotem; rozładowanie konfliktu      | odwołanie do jasnej strony natury; autorytet własny; zniechęcenie biurokracją |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia Zmysłów       | wzmocnienie zmysłów, wzmocnienie muzyki |
| Magia Mentalna      | pokazanie piękna, uspokojenie, ukojenie |
| Magia Materii       | instrumenty muzyczne |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Nastoletni chórek                  |  |
| Klub muzyczny Nadzieja             | szacun za pomoc swojakom |
| Kółko fotograficzne                | rozpoznawany fotograf |
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
| zdobycie wywiadu; wejście w półświatek | bardzo duża sieć znajomych; opinia pozytywnego |
| uniknięcie problemów z władzą | opinia ciekawskiego, pożytecznego, niegroźnego |
|  |  |

## Opis

### Ogólnie

No youth left behind. Łagodny fotograf, ratuje jednostki przed przegraną w życiu. Kocha fotografować ptaki i budynki. Daleko od normalnej sieci Mausów.

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:
