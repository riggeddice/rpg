---
layout: inwazja-karta-postaci
categories: profile
title: "Jakub Pestka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161130|chciał pomóc Bazylemu. Skończyło się na tym, że też przetransformowało go w Esuriit. Zastrzelony przez Annę. KIA.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|badacz i lekarz; prawdziwy; współuczestniczy w konwersji energii z Esuriit w "coś normalnego". Tak pomocny jak umie. |[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|badacz i lekarz; 'czysty', zaufany Marianny i wiemy, że nie zauważa tych, których Esuriit już pożarło.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|2|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konrad Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
