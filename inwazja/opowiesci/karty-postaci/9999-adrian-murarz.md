---
layout: inwazja-karta-postaci
categories: profile
title: "Adrian Murarz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|zaszczepiony na Irytkę Sprzężoną|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160914|który został włączony do Aleksandrii przez Karolinę; uratowany i wyciągnięty przez Klarę.|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160810|który po prostu się nie przydał... został zaszczepiony i stracony przez Blakenbauerów; atm w Technoparku.|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|w imieniu Świecy zbierający (dla Blakenbauerów) nieaktywne Anioły Nadzorcze i oddający je Emilii na przechowanie.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151202|"mag Kurtyny" i przenośna platforma dla artefaktu przenoszącego Węzeł Vladleny.|[Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|10/06/15|10/06/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151021|któremu wprowadzono endosymbionta Blakenbauerów i Szlachty; przypomniał sobie rzeczy których "nigdy nie pamiętał".|[Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151007|który dał się przekonać Hektorowi że praca dla Blakenbauerów jest w jego najlepszym interesie.|[Nigdy dość przyjaciół: Szlachta i Kurtyna](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151001|kiedyś * mag Kurtyny i przyjaciel Vladleny; teraz agent Szlachty, którego Vladlena chce rozpaczliwie odzyskać.|[Plan ujawnienia z Hipernetu](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|10/05/21|10/05/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|7|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|6|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|5|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|4|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|3|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|3|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|3|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|3|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Edward Sasanka](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-sasanka.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [151202](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Ernest Kokoszka](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-kokoszka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Anatol Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-anatol-weiner.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
