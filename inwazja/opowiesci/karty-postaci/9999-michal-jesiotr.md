---
layout: inwazja-karta-postaci
categories: profile
title: "Michał Jesiotr"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161009|pocieszający się brakiem Haliny przy boku Draceny ;-). Też: za wolno się leczy; stanowi Paulinowy papierek lakmusowy.|['Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html)|10/07/14|10/07/16|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161002|lokalny kucharz w smażalni, syn Antona, wierzy święcie, że Halina Weiner jest nową inkarnacją lokalnej świętej "Ognistej Damy". Wszystko wyśpiewał Dracenie w łóżku.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
