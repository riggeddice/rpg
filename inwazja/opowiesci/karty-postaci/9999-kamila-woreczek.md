---
layout: inwazja-karta-postaci
categories: profile
title: "Kamila Woreczek"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|ujawniło się, że jest parą z Alfredem Wernerem. Dodatkowo, została ambasadorem jaszczurów (reptiljan) na Czapkowik.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180421|osoba dbająca o Czapkowik i komunikująca się z jedynym magiem, któremu ufa - z Genowefą. Zastrzelona i zregenerowana. Silnie we frakcji pragmatycznej.|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|10/11/24|10/11/26|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|180419|burmistrz Czapkowika i jaszczur w przebraniu; rozsądna i chcąca dobrze dla świata jaszczurów i ludzi. Przekształca Czapkowik za pomocą środków unijnych w "polskie Roswell"|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|10/11/17|10/11/19|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Genowefa Huppert](/rpg/inwazja/opowiesci/karty-postaci/1803-genowefa-huppert.html)|2|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html), [180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|2|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html), [180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Mateusz Podgardle](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-podgardle.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Maciek Drzeworóz](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-drzeworoz.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Artur Wiążczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-wiazczak.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Alojzy Bunnert](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-bunnert.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Aleksandra Kurządek](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-kurzadek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
