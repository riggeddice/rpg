---
layout: inwazja-karta-postaci
categories: profile
title: "Aneta Kosicz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150429|kultystka Anioła Światła, która zaszła tak daleko swym Skażeniem i ambicją, że jedyną opcją uratowania sytuacji była jej egzekucja. KIA.|[Terminusi w Zależu](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|10/01/05|10/01/06|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|
|150427|weterynarz, która najwięcej zyskuje z działań potwora; wygląda świetnie i czuje się świetnie. Prawie umarła gdy rozbiła się na motorze; ozdrowiała cudownie.|[Kult zaleskiego Anioła](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|10/01/03|10/01/04|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Onufry Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-zaleski.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Małgorzata Grimm](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-grimm.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Mateusz Kozociej](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kozociej.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Marek Ossoliński](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-ossolinski.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Jan Grimm](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-grimm.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
