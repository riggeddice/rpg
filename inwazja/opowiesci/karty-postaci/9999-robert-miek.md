---
layout: inwazja-karta-postaci
categories: profile
title: "Robert Mięk"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150224|Spustoszony (w formie utajonej) agent Tarczy mający oko na Zespół i Marysię.|[Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|10/04/17|10/04/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150110|uczeń "Tarczy", który został najprawdopodobniej Spustoszony ale nadal współpracuje z Marysią Kiras|[Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|10/04/15|10/04/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|uczeń "Tarczy", który występuje tylko na plakacie.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Korwin Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-korwin-morocz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[miślęg Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-misleg-pieluszka.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[echo Lugardhaira](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-lugardhaira.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Szczepan Czarniek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-czarniek.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-pieluszka.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Lugardhair](/rpg/inwazja/opowiesci/karty-postaci/9999-lugardhair.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Jolanta Pirat](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-pirat.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Jerzy Buława](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-bulawa.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Bożena Górzec](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-gorzec.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
