---
layout: inwazja-karta-postaci
categories: profile
title: "Kornelia Modrzejewska"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140505|tajemnicza lekarka poza zasięgiem wymiaru sprawiedliwości|[Musiał zginąć, bo Maus](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|10/01/05|10/01/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140503|wybitny lekarz z mrocznymi sekretami (Viv)|[Wołanie o pomoc](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|10/01/03|10/01/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Vuko Milić](/rpg/inwazja/opowiesci/karty-postaci/9999-vuko-milic.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Kwiatuszek](/rpg/inwazja/opowiesci/karty-postaci/9999-kwiatuszek.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Klotylda Świątek](/rpg/inwazja/opowiesci/karty-postaci/9999-klotylda-swiatek.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Karol Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-maus.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html), [140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Tadeusz Aster](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-aster.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Renata Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Krystian Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-maus.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Jędrzej Zdun](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zdun.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Jonatan Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-jonatan-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Hlargahlotl](/rpg/inwazja/opowiesci/karty-postaci/9999-hlargahlotl.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Cierń](/rpg/inwazja/opowiesci/karty-postaci/9999-ciern.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Adam Wołkowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-wolkowiec.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
