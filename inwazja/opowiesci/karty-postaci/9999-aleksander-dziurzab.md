---
layout: inwazja-karta-postaci
categories: profile
title: "Aleksander Dziurząb"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160629|lekko paranoiczny acz kompetentny technomanta, który zamontował kamery w okolicy całkowicie niezgodnie z planami.|[Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|10/06/27|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160901|terminus, paranoiczny terminus bez poczucia humoru, który jednak uczciwie podszedł do Marka.|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|10/06/18|10/06/20|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zenobia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-bankierz.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Piotr Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Marek Śmietanka](/rpg/inwazja/opowiesci/karty-postaci/1709-marek-smietanka.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Lilia Kałdun](/rpg/inwazja/opowiesci/karty-postaci/9999-lilia-kaldun.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Karol Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Bartłomiej Czyrawiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bartlomiej-czyrawiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
