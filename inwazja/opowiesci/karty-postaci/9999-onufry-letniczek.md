---
layout: inwazja-karta-postaci
categories: profile
title: "Onufry Letniczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160131|strażak Dotknięty przez efemerydę, który przyszedł do Pauliny z prośbą o coś uspokajającego / na sen.|[Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|10/03/10|10/03/11|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160124|ojciec Mileny, strażak; zatrzymał ją z pomocą Pauliny i nie skończył jako przysmak dla efemerydy.|[Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|10/03/08|10/03/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html), [160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|2|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html), [160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Olga Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Milena Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Mateusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kuraszewicz.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Maciek Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Barbara Zacieszek](/rpg/inwazja/opowiesci/karty-postaci/9999-barbara-zacieszek.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
