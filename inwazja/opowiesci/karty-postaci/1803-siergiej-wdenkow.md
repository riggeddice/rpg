---
layout: inwazja-karta-postaci
categories: profile
factions: "niezrzeszeni"
type: "NPC"
owner: "draża"
title: "Siergiej Wdenkow"
---
# {{ page.title }}

## Postać

### Motywacje 
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | akceptacja; sława i chwała |
| Społeczne         | szczęście innych |
| Wartości          | brutalna szczerość |

#### Szczególnie

| Co chce by się działo?                                                          | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| Dąż do tego, by rodzina do Ciebie wróciła; Maksymalizuj szczęście swej rodziny  |  |
| Konfrontuj się z innymi, by zaakceptowali Cię takim jakim jesteś ze słabościami |  |
|  |  |
|  |  |
|  |  |
|  |  |

### Umiejętności
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| policjant         | tropienie ludzi; techniki przesłuchań; walka wręcz |
| barman            | ludzie mi się zwierzają |
| ojciec            | kochający; troskliwy |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| gotowanie pierogów |  |
| wsparcie ludzi półświatka | sekretne znaki meneli |
| przesłuchiwanie | obcy akcent; techniki przesłuchań |

### Silne i słabe strony:
#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
| degenerat alkoholowy | stan upojenia, natura alkoholu |

#### Manewry

| Co jest wzmocnione                                                         | Kosztem czego                                                                    |
|----------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| Wspomaganie alkoholowe, społeczniejszy i skuteczniejszy gdy nietrzeźwy     | Gdy jest trzeźwy, zupełnie mu nic nie wychodzi: wolny, smutny...                 |
| Nietrzeźwy jak to tylko możliwe, odporniejszy na ataki społeczne, mentalne | Gdy jest nietrzeźwy, jest odpychany przez społeczeństwo i traktowany jak pijak   |

### Szkoły magiczne
#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
|  |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |

## Zasoby i otoczenie
#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                                 | Aspekty                                                              |
|-------------------------------------------|----------------------------------------------------------------------|
| Ma chody u ukraińskiego oligarchy Pawłowa |  |
| Bratkowski | też odeszła od niego rodzina |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |

# Opis

### Koncept

nieznane

### Ogólnie

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180321|eks-glina alkoholik z Prysznicowa, uciekinier z Ukrainy, tropiciel ludzi, opuścili go bliscy; alkoholik o umyśle obcym (przez to częściowo odpornym na Arazille).|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|10/12/08|10/12/09|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180418|poszedł pomóc Kasi i został u niej zamieszkać. Pokonał narkomana z nożem z pomocą Kasi. Świetnie robi pierogi.|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Paweł Szlezg](/rpg/inwazja/opowiesci/karty-postaci/1803-pawel-szlezg.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Grzegorz Zachradnik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-zachradnik.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
