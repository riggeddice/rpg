---
layout: inwazja-karta-postaci
categories: profile
title: "Olga Jeden"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160124|matka Maćka; główny węzeł informacyjny dla Pauliny. Gdyby nie ona to po prawdzie skończyłoby się to tragicznie. Nikt by nic Paulinie nie powiedział.|[Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|10/03/08|10/03/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Onufry Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Milena Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Mateusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kuraszewicz.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Maciek Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
