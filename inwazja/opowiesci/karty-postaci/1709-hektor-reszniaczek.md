---
layout: inwazja-karta-postaci
categories: profile
factions: "Eliksir Aerinus"
type: "NPC"
title: "Hektor Reszniaczek"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **MRZ: Dom, rodzina i drzewo**:
    * _Aspekty_: skupiony na swoich najbliższych, żeby najbliżsi mieli jak najlepsze warunki, zawsze szuka okazji do dorobienia, rozbudowa domu
    * _Opis_: Hektor chce mieć rodzinę, dom i psa. Chce mieć swój ogródek, swoje życie, Dianę, dzieci... marzy o tym, by zapewnić im bezpieczeństwo i dobry start w życie
* **MET: Niezależna, uczciwa praca**: 
    * _Aspekty_: a bit powerstruck, dąży do niezależności, chce własny oddział Eliksiru pod kontrolą, zrobi więcej niż trzeba, pogardza poddaństwem i niewolnictwem
    * _Opis_: Hektor chce zapewnić bezpieczeństwo i szczęście swoim najbliższym. Uważa, że ciężką, cierpliwą pracą będzie w stanie się wzbogacić i dać im to, na co zasłużyli. Ma momenty, w których jest zafascynowany potęgą niektórych postaci, ale mu szybko przechodzi.
* **FIL: Edukacją i pracą społeczności się bogacą**:
    * _Aspekty_: pomocny przyjaciołom i sąsiadom, próbuje sprawić by wszystko działało lepiej, dąży do edukowania otoczenia, dąży do współpracy i unikania konfliktów, pamiętliwy, aktywnie chce usprawniać otoczenie
    * _Opis_: Hektor próbuje konsekwentnie doprowadzić do tego, by jak najwięcej osób jak najlepiej sobie radziło - w jego bliskim otoczeniu. Chce mieć piękny dom, fajnych sąsiadów, żeby oni też mieli bezpiecznie i fajnie... i nie zawaha się zadziałać, by to się stało.

### Umiejętności

* **Rolnik i ogrodnik**:
    * _Aspekty_: rośliny magiczne, aranżacja ogrodów, narzędzia ogrodnicze, gawędziarz altankowy, nawozy i chemia rolnicza
    * _Opis_: Hektor zna się na wielu różnych rzeczach, ale szczególnie na ogrodach i utrzymywaniu ich przy życiu. Nie tylko dobrze dopasuje glebę i narzędzia, ale ma "zielony palec" - potrafi na kamieniu wyhodować hortensję. Prawie.
* **Złota rączka**: 
    * _Aspekty_: scrapper, naprawy w gospodarstwie, wykorzystanie narzędzi, co gdzie można jeść, budowanie schronienia, prace na wsi, mistrz prowizorki
    * _Opis_: Hektor jest wędrownym druciarzem. Zwykł chodzić od miejsca do miejsca, naprawiać różne rzeczy, spać w stodole i tak mu się dobrze żyło. Czy to z elektrycznością czy bardziej analogowo, Hektor większość rzeczy naprawi i wykorzysta ponownie.
* **Pozyskiwacz nasion**:
    * _Aspekty_: ekstrakcja esencji, praca w warunkach niebezpiecznych, plany hodowlane, ustalanie stref ochronnych, ewakuacja siebie i innych, dbanie o nasiona
    * _Opis_: W Eliksirze Aerinus Hektor pełni rolę maga od pozyskiwania i hodowania rzadkich, często niebezpiecznych roślin. Jego odpowiedzialnością zawsze było wymyślić jak to wykorzystać, jak to przekształcić i jak to podlać. 
    
### Silne i słabe strony:

* **Tytan pracy**: 
    * _Aspekty_: silny i wytrzymały, robi szybko i dobrze, bywa dość naiwny, jest dość impulsywny
    * _Opis_: Bardzo pracowity i wkładający mnóstwo energii w to co robi, ale też robiąc bardzo mądrze i szybko Hektor osiąga dużo lepsze wyniki niż konkurencja. Dba o swoje ciało oraz o swoje narzędzia. 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: wzrost roślin, hodowla roślin, stymulanty, nawozy
    * _Opis_: Silnie utylitarna sfera - przede wszystkim służy do wspomagania hodowli roślin, uzdatniania gleby i robienia nawozów, acz roślinną paszę dla zwierząt też może zapewnić. Jak trzeba, zrobi stymulant by móc pracować dłużej lub by się krowa nie wykrwawiła.
* **Magia materii**: 
    * _Aspekty_: golemy, naprawa sprzętu, gleby i nawozy, schronienia
    * _Opis_: Bardzo silnie utylitarna sfera, skupia się na budowaniu golemów z jednej strony, z drugiej na zapewnieniu dobrych warunków roślinom a z trzeciej na kształtowaniu schronienia i wsparciu w budowaniu domu
* **Magia mentalna**:
    * _Aspekty_: golemy, czyszczenie pamięci
    * _Opis_: Bardzo słaba sfera u Hektora, właściwie służy jedynie budowaniu golemów. Jak każdy mag, był uczony czyszczenia pamięci dla zachowania Maskarady. Po prostu nie chce i brzydzi się tą sferą. Aktywnie odmawia przyznania, że golemy to też sfera mentalna

### Zaklęcia statyczne

* **Awaryjne schronienie**:
    * _Aspekty_: materia + biomancja, bezpieczne schronienie, korzystny ekosystem, łamie Maskaradę
    * _Opis_: Czasem trzeba posadzić roślinki tam, gdzie zwyczajnie nie ma warunków. Czasem jest się w lesie i nie ma gdzie spać. Wtedy używa się tego zaklęcia - kształtuje teren w niewielką kopułę, gdzie można przeczekać noc czy przygotować hodowlę roślin.
* **Golem roboczy**: 
    * _Aspekty_: materia + mentalna, prace proste i fizyczne, nie męczą się, mogą działać w warunkach niebezpiecznych, odcięte od czarodzieja energią, łamie Maskaradę
    * _Opis_: Czasem praca bywa niebezpieczna lub żmudna. Wtedy zamiast pracować osobiście można powołać kilka golemów z gleby i pozwolić im pracować za siebie. Problematyczne przy Maskaradzie, jakkolwiek golemy są świetne w pracach nie wymagających precyzji czy szczególnej inteligencji.

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Hodowca i zdobywca roślin w Eliksirze Aerinus
* Złota rączka na wsi

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Ogródek pełny rzadkich roślin użytecznych**:
    * _Aspekty_: kolekcja nasion, odtwarzalne, wzmacniacze zaklęć, często trucizny, rośliny użyteczne na co dzień, języczniki różnego typu
    * _Opis_: Hektor zawsze próbował założyć hodowlę roślin rzadkich i przydatnych. Podczas swoich podróży zawsze próbował znaleźć coś, co może się mu przydać i to zachować "na potem". Zgromadził pokaźną kolekcję którą zaczął hodować we własnym ogródku.
* **Zbiór bardzo przydatnych narzędzi**:
    * _Aspekty_: przyspieszają pracę, naprawa przedmiotów, prace rolnicze, prace budowlane, wzmocnione magią
    * _Opis_: Jako złota rączka ma mnóstwo przydatnych narzędzi do prac polowych, budowlanych i naprawczych. Posiłkując się tymi narzędziami i golemami jest w stanie wykonywać prace błyskawicznie szybko w porównaniu z nawet dużo bardziej umagicznionymi konkurentami
* **Dostęp do ekosystemu Aerinus**:
    * _Aspekty_: praca z niebezpiecznymi roślinami i viciniusami, pozyskiwanie bytów, ekstrakcja esencji, sprzęt ochronny wszelaki
    * _Opis_: Z uwagi na to czym Hektor się zajmuje na co dzień w ramach Eliksiru dostał dostęp do różnego rodzaju narzędzi, sprzętu którym jest w stanie zajmować się roślinami i które mogą mu ogólnie pomóc. Ma też szerokie uprawnienia do działania w ramach całego ekosystemu Eliksiru Aerinus.

# Opis

Mag rolniczy, imprezowy i golemanta. Złota rączka świata ludzi, człowiek "od wszystkiego" zdolny do pomocy i do pomocy chętny. Niespecjalnie zna świat magów i nie jest nim zainteresowany - chce robić dobrą robotę, mieć dom, rodzinę i psa. 

Skupia swoją energię na rozbudowaniu swojego domu, swoich rzeczy i bardziej skupia się na swoim bezpośrednim otoczeniu niż hipotetycznym ratowaniu świata. Szuka zawsze okazji do zarobku a fakt, że jest odpowiedzialny za Dianę i jest osiadły tym bardziej go motywuje i troszkę stresuje. Tak czy inaczej, stara się jak może.

Ogólnie: poczciwy, uczciwy mag starający się przetrwać bez szczególnych konfliktów i sporów.

### Koncept

TODO

### Motto

"Dobra praca i zaangażowanie dają satysfakcję. I patrzcie, jak roślinki rosną!"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|pracuje u Apoloniusza Bankierza i stabilizuje się na obszarze Powiatu Pustulskiego z Dianą.|Rezydentka Krukowa|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|znalazł pracę u Apoloniusza Bankierza jako konstruktor odpowiednich ziół do eliksirów|Rezydentka Krukowa|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|został parą z Dianą|Rezydentka Krukowa|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|jest zakochany w Dianie.|Rezydentka Krukowa|
|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|zaczyna kontaktować się z siłami Gabriela Dukata.|Rezydentka Krukowa|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|przestał pilnować domu Pauliny; nie ma już takiej konieczności (Paulina wróciła).|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171010|pracuje nadal dzielnie nad ogródkiem Pauliny i powiedział jej, że ma coś wspólnego z problemami Katii - nadał info o Stefanii Kołek. Ale chyba z Katią ok?|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|który znalazł dobrą pracę w Eliksirze Aerinus i osiadł z Dianą. Pilnował domu Pauliny i starał się by wszystko się udało na czas jej nieobecności. Udało się.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170723|zaczął pracę u Apoloniusza. Zaczyna pracować nad manierami i podejściem; ma Dianę jako swoją ukochaną i nie musi nikomu nic udowadniać.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|sparował się z Dianą po ostrej kłótni. Zdecydował się osiąść w pobliżu. Szybko uczy się trzymać język za zębami. Paulina załatwiła mu pracę u Apoloniusza.|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170614|prawie wpadł w ręce Dukata, ale Paulina go powstrzymała; załamany czynami Grazoniusza i własną bezradnością. Emocjonalnie czarując zniszczył Paulinie ogródek.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170518|ma problemy z kontrolowaniem impulsów (pobił Grazoniusza o Dianę), kajał się pokornie i zapoczątkował lawinę wchodząc w konszachty z Rodziną Dukata.|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170515|zdecydowanie zakochany w Dianie, nie może znieść, że ona tak żyje, ścina się z Dianą o Ewelinę. Chwilowo zostaje w okolicy... może jako ogrodnik Pauliny?|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|10/01/20|10/01/22|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170510|mag rolniczy, imprezowy i golemanta. Podoba mu się Diana. Chciał ją poderwać dość zaawansowanym i Skażonym rytuałem; oczywiście, przez kolizję z czarem Pauliny skończył praktycznie w komie i Diana z Pauliną musiały go ratować...|[Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|10/01/16|10/01/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|8|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|6|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|3|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|3|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Karol Komnat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-komnat.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Michał Furczon](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-furczon.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Konrad Paśnikowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-pasnikowiec.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Janusz Wosiciel](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wosiciel.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Aleksander Czykomar](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-czykomar.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
