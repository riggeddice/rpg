---
layout: inwazja-karta-postaci
categories: profile
title: "Emilia Szudek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140201|osoba desygnowana przez Krystalię do bycia kochanką Radosława. Uratowała ją siostra która chciała Radosława dla siebie :P.|[Ona zdradza, on zdradza](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140103|uczestniczka niesamowitej imprezy i czarodziejka SŚ próbująca zaimponować.|[Tak bardzo nie artefakt](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Radosław Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-krowka.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Mateusz Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-krowka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Marian Rustyk](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rustyk.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Kornelia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-szudek.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
