---
layout: inwazja-karta-postaci
categories: profile
title: "Bartłomiej Czyrawiec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160904|kontakt Pauliny, który nadał jej Kingę Toczek - specyficzną acz skuteczną lekarkę terminusów.|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|10/06/21|10/06/24|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160901|znajomy Pauliny i ekspert od pająków astralnych. Nie powiązany z żadną istotną gildią; mieszka na Śląsku ale w jakimś mniejszym miasteczku.|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|10/06/18|10/06/20|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Piotr Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Marek Śmietanka](/rpg/inwazja/opowiesci/karty-postaci/1709-marek-smietanka.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Lilia Kałdun](/rpg/inwazja/opowiesci/karty-postaci/9999-lilia-kaldun.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|1|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Karol Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
