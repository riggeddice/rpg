---
layout: inwazja-karta-postaci
categories: profile
title: "Tymoteusz Dzionek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150829|przerażony niebojowy agent Skorpiona, który nie spodziewał się, że epidemia jest Skażeniem hybrydy * viciniusów.|[Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150210|koordynator Skorpiona z którym skontaktował się Hektor by uratować Dianę... lub "Komandosów". Nie wiadomo.|['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141022|agent średniego szczebla Skorpiona oddelegowany jako łącznik z Blakenbauerami. Na stracenie, jak co.|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|2|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Wiktor Lubaszny](/rpg/inwazja/opowiesci/karty-postaci/9999-wiktor-lubaszny.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Onufry Puzel](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-puzel.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Mikołaj Młot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mlot.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Mariusz Garaż](/rpg/inwazja/opowiesci/karty-postaci/1709-mariusz-garaz.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Maciej Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-kwarc.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Kamil Rzepa](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-rzepa.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Janusz Wybój](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wyboj.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Felicja Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Dominik Szerściuk](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-szersciuk.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Bianka Drażyńska](/rpg/inwazja/opowiesci/karty-postaci/9999-bianka-drazynska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Arkadiusz Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Aleksander Szwiok](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-szwiok.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
