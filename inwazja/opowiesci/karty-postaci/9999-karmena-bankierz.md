---
layout: inwazja-karta-postaci
categories: profile
title: "Karmena Bankierz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|dorobiła się niezadowolenia ze strony Newerji. Dlaczego na JEJ terenie dzieją się takie rzeczy a ona nie wie?|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180503|zbyt zainteresowana imprezami by kompetentnie zarządzać swoim terenem; zostawiła na straży wiernego Alfreda.|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|11/10/27|11/10/29|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Yyizdath](/rpg/inwazja/opowiesci/karty-postaci/9999-yyizdath.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Ylytis](/rpg/inwazja/opowiesci/karty-postaci/9999-ylytis.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwia Zasobna](/rpg/inwazja/opowiesci/karty-postaci/9999-sylwia-zasobna.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Stefan Bułka](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-bulka.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Alfred Janowiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-janowiecki.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
