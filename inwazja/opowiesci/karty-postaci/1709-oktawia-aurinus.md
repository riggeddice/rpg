---
layout: inwazja-karta-postaci
categories: profile
factions: "Efemeryda Senesgradzka"
type: "NPC"
title: "Oktawia Aurinus"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: 
    * _Opis_: 
* **Magia zmysłów**: 
    * _Aspekty_: 
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* brak

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171029|tylko chciała przeprowadzić koncert a skończyła rozproszona przez nipustaki. Jest jak "córka" Marzyciela. Powiedziała Paulinie, że Fornalisa nie ma w Efemerydzie - a Grzybb tam trafił.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|efemeryda powołana przez Efemerydę Senesgradzką sprzężona z Oliwią Aurinus. Przejęła awiany Krzysztofa i chciała doprowadzić do tego, by Oliwia śpiewała przez... szantaż.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
