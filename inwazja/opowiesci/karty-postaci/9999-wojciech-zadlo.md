---
layout: inwazja-karta-postaci
categories: profile
title: "Wojciech Żądło"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160417|poprzedni szef Piroga Górnego; wyraźnie skorumpowany, osłabił system zabezpieczeń i być może współpracował ze Szlachtą.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
