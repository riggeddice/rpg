---
layout: inwazja-karta-postaci
categories: profile
title: "Bójka Diakon"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|spawnuje nowego kralotha (guaranteed future)|Adaptacja kralotyczna|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|wycofała się do Drugiego Kralotha jako jego oddana agentka; gotowa do spawnowania nowego kralotha|Adaptacja kralotyczna|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|beznadziejnie uzależniona od plasterków kralotycznych, mimo, że jeszcze o tym nie wie.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180327|co do której wreszcie wiadomo, czemu zmieniło się jej zachowanie z obserwatorki w lalkę. Została kralotycznie Skażona.|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|10/12/08|10/12/10|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180318|uzależniła się od plasterków kralotycznych. Punkt centralny walki między Silurią a Arturem Żupanem.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|"rezydentka" Mordowni i lokalna lalka. Praktycznie na ciągłym haju. Bondage fetish, przynajmniej na razie. Wspiera Krystalię w jej eksperymentach. Dzieli się z Silurią plasterkami. Too kinky to torture.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Melinda Słonko](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-slonko.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Lucjan Kowalkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kowalkiewicz.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Janek Malczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-malczorek.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
