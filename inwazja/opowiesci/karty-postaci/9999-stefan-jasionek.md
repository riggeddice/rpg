---
layout: inwazja-karta-postaci
categories: profile
title: "Stefan Jasionek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170519|jeden ze złomiarzy. Kiedyś ze Świecy, chce dołączyć do gildii zakładanej przez Edwarda Sasankę. Motyw? Bo jest pobór.|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|10/08/21|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Teresa Koliczer](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-koliczer.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Sandra Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-sandra-maus.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Edward Sasanka](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-sasanka.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
