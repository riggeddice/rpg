---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "raynor"
title: "Daniel Akwitański"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **BÓL: Znienawidzony, biedny i zapomniany**:
    * _Aspekty_: Oficjalne uznanie za kryminalistę, utrata majątku i wpływów, utrata sojuszników, uznany za szkodnika regionu, wilczy bilet, pogardzany, pomiatany
    * _Opis_:
* **ŚR: Popularność i biznes**:
    * _Aspekty_: zdobywanie silnych pleców, nie antagonizowanie, pro bono, filantropijny, pomocny dla wzajemności, myślenie długoterminowe, lock-in, ciągła ekspansja tym co ma
    * _Opis_:
* **FIL: Pozorny mąż stanu**:
    * _Aspekty_: moralność pochodzi od percepcji, działania muszą być widoczne, fakty są nieistotne wyniki mają znaczenie, warto żyć dobrze z wszystkimi, ochrona i fortyfikowanie zasobów
    * _Opis_:

### Umiejętności

* **Przedsiebiorca**:
    * _Aspekty_: dziwna elektrownia Weinerów, diagnostyka linii magicznych, pozyskiwanie zleceń, bilansowanie przychodów, negocjacje z potencjalnymi klientami, maintanace elektrowni
    * _Opis_: Pierwotnie kontraktor dla Dukata organizujący nielegalne walki, teraz własciciel elektrowni.
    
* **Bukmacher**:
    * _Aspekty_: naciąganie, ostrożność finansowa, wycena
    * _Opis_: Jako organizator walk, Daniel przyjmował wiele zakładów. Był główny punkt dochodu tego interesu. Musiał wykształcić kilka umiejętności pozwalających mu na bycie w tym dobrym.

* **Bokser**:
    * _Aspekty_: uniki, prawy sierpowy, nieczyste chwyty
    * _Opis_: Lata w światku przestępczym zahartowały Daniela na niebezpieczne sytuacje. Gdy inne metody zawodziły musiał uciekać się do przemocy, jako ostatniej drogi perswazji.

### Silne i słabe strony:

* **Powszechnie lubiany**: 
    * _Aspekty_: nie odmawia w potrzebie, inni chca mu pomoc, czasem wykorzystywany, osłaniany przez społeczność
    * _Opis_: Korzystając z techniki wzajemności stara się zaskarbić sympatię tłuszczy, która pomoże mu w realizacji cełów.
* **Podniesiony status**: 
    * _Aspekty_: kultura osobista, pożadany gość, dobrze go znać, 
    * _Opis_: Wszystkie jego działania i znajomości powodują, że jest personą pożądaną na oficjalnych spotkaniach i przyjęciach.
* **Dobry pracodawca**:
    * _Aspekty_: dobre kontakty ze związkami i lokalną władzą, ma posłuch, rozpaznawalność na lokalnym rynku pracy
    * _Opis_: Dzięki swojej zasadzie nie antagonizowania pracownicy go szanują i nie ma problemów z personelem i znajdowaniem dodadkowej pomocy.
    
## Magia

### Szkoły magiczne

* **Magia materii**:
    * _Aspekty_: ksztaltowanie terenu, przedmioty użytkowe, wzmacnianie struktury, diagnoza słabych punktów, rozbiórka, wzmocnienia w walce
    * _Opis_: 
* **Technomancja**: 
    * _Aspekty_: naprawa i konserwacja, ulepszanie starego sprzętu, zdalna inspekcja, walka sprzętem
    * _Opis_:  
* **Kataliza**: 
    * _Aspekty_: przesuwanie pływów magicznych, puryfikacja magii, diagnostyka energii magicznej, 
    * _Opis_: 

### Zaklęcia statyczne



## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Znam

* **Znajomości w rodzinie Dukata**:
    * _Aspekty_: pokonanie barier biurokratycznych, znalezienie powiązań biznesowych
    * _Opis_: 
* **Byli gladiatorzy**:
    * _Aspekty_: siła fizyczna, rozwiązywanie problemów
    * _Opis_:
* **Stali bywalcy eventów**:
    * _Aspekty_: 
    * _Opis_: 
* **Robert Sądeczny**:
    * _Aspekty_: pozyskanie trudnych zasobów, arbitraż spraw konfliktowych, na terenie protegowanym przez Roberta, dojście do Dukata
    * _Opis_: Rezydent Dukata
* **Klienci Elektrowni**:
    * _Aspekty_: traktowani uczciwie, jedynie zrodlo pradu,
    * _Opis_: Daniel musiał od podstaw zbudować całą sieć relacji, aby uczynić elektrownię dochodową. Pozwoliło mu to na poznanie wielu cennych magów.

### Mam

* **Dziwna elektrownia Weinerów**:
    * _Aspekty_: z nadania Dukata, personel i sprzęt, wejście do kontrahentów, sprzęt do pomiarów magii, puryfikatory magii
    * _Opis_: Posiadanie tak dużego biznesu pod protektoratem Dukata daje Danielowi mnóstwo zasobów materialnych i niematerialnych, z których można skorzystać w różnych okolicznościach.

## Inne:



## Motto

"Pieniądz robi pieniądz"



W młodości na zlecenie Dukata zajmował się organizacją walk, protegowany Dukata, który dostaje procent, aale dostarcza ochrone i materiał na walki. Objął pod władanie elektrownie, ponieważ chce sie ustabilizować, chce porzucic bandycki swiat i zająć sie czymś legalnym i dochodowym.

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|póki Melodia Diakon operuje na Mazowszu, pomoże Danielow jak on poprosi|Wizja Dukata|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|ma "linię supportową" do Integry Weiner przez Karola Marzyciela. Jak coś się spieprzy, to może z Integrą próbować rozwiązać.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|Uniezależnienie Magitrowni od wpływu jakiejkolwiek organizacji ever|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|Zorganizowanie czegoś w stylu "stałej rady regionu" - by region działał prawidłowo i spójnie. Być w tej radzie.|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|ma wsparcie ze strony Łucji Maus. Ona widzi w nim potencjalnego sojusznika a przynajmniej nie-wroga.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|wplątany w konflikt między Łucją a Tomaszem doprowadził do tego, że Melodia uciemiężyła Tomasza. Wyprowadził to na "wszyscy mają czyste konto".|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|przekonał Łucję Maus do współpracy w ramach magitrowni oraz pomógł Tomaszowi Myszeczce opanować "dzikie" świnie. Ustabilizował wypływ energii z magitrowni.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171220|przekonał do siebie Integrę i częściowo był sprzężony z magitrownią na trójfazie. Bezpiecznie wrócił z trójfazy do domu. Zaplanował randkę Marzyciela z Integrą.|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|11/10/05|11/10/06|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171115|zwołał spotkanie na szczycie, NIE został dyktatorem (a proponowano), zmienił magitrownię w centrum dowodzenia i pracuje nad emiterem antyklątwożytowym.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|odbił Paradoks Pauliny do Zameczku i ostrzegł Świecę; podniósł morale zespołu Draceną i ściągnął Oliwię - która okazała się być pod wpływem klątwożyta.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|zneutralizował mikromagitrownię Świecy, chciał pomóc Myszeczce i pośrednio wysadził portalisko. Ratował sytuację, naprawił uszkodzone elementy magitrowni i uniknął poderwania przez Dracenę... acz miał dość... ujawniający Paradoks.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|chciał uniknąć Tamary (wyjść dyplomatycznie z sytuacji), więc Skaziła wszystkie czujniki Sądecznego bez pytania. Wymyślił, że naprawi sprawę glukszwajnem. Jednym.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|właściciel magitrowni Histogram balansujący między Dukatem a Bankierzem; opanował kryzys energetyczny i jeszcze przekuł go na swoją korzyść. Zatrudnił Dracenę.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|8|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|5|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|3|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
