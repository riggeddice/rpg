---
layout: inwazja-karta-postaci
categories: profile
title: "Elizawieta Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160224|terminuska Świecy, która chciała pomóc Marcelinowi a skończyła w głupiej sytuacji w środku Rezydencji Blakenbauerów.|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|dała Marcelinowi uprawnienia do przechwycenia Aniołów i kazała je składować u Anety Rainer. Ratuje co się da.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|z ramienia Ozydiusza wyjaśniająca Andrei jak wygląda sytuacja po Spustoszeniu.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150922|empatyczna Zajcewka pełna Pasji, nieodwzajemniona miłość Ignata, próbowała być przyjaciółką Mileny. Po* maga Andrei.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|terminuska o dobrym sercu, która zatrzymała ucieczkę Mileny Diakon i na miesiąc trafiła do szpitala. Trafi pod Ozydiusza.|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|która powiedziała Ignatowi, że za to co zrobił może się rozsypać ich przyjaźń. Jedyna siła w galaktyce zdolna zmusić Ignata do przeproszenia maga Świecy ;-).|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
