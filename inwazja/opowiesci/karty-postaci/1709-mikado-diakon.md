---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Mikado Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Uprzedzenia**:
    * _Aspekty_: uprzedzony do iluzjonistów, podejrzliwy
    * _Opis_: "nie spotkałem jeszcze iluzjonisty, któremu można było sensownie zaufać. nic nie jest takie na co wygląda…"
* **KLT: Rycerski**:
    * _Aspekty_: obowiązkowy, opiekuńczy
    * _Opis_: "dama w potrzebie będzie uratowana. wykonam zadanie, które mam do wykonania - tak dobrze jak się da. wszystko pod moją opieką musi być bezpieczne"

### Umiejętności

* **Terminus**:
    * _Aspekty_: elegancka broń biała, kontrola otoczenia
    * _Opis_: 
* **Estetyk**: 
    * _Aspekty_: wywoływanie wrażenia, lasery
    * _Opis_: 
* **Scenograf**:
    * _Aspekty_: pokazy światła i dźwięku, zauroczenie, przerażenie
    * _Opis_: 
* **Aktor**:
    * _Aspekty_: taniec, stroje, zauroczenie, przerażenie
    * _Opis_: 
* **Technika sceniczka**: 
    * _Aspekty_: zdalne sterowanie, lasery, sieci (łańcuchy) urządzeń, animacja
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: 
    * _Opis_: 
* **Magia elementalna**: 
    * _Aspekty_: magia soniczna
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: poszerzenie świadomości sytuacyjnej
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Terminus Millennium pod dowodzeniem Draconisa. Jego "przyszywany syn". Zazdrosny i opiekuńczy jednocześnie wobec Antygony i Draceny. Terminus nadspodziewanie cyniczny jak na swój młody wiek, acz bycie "czwartym w kolejności" może mieć coś z tym wspólnego.

Mikado w świecie ludzi specjalizuje się w "zdalnym organizowaniu piękna", jak to mówi. W rzeczywistości chodzi o lekko technomantyczne, acz i rzeczywiste: pokazy światła, choreografia wody, uproszczona robotyka, piękno w automatyzacji. Pokazy światła i dźwięku. Rzeczy, gdzie człowiek nie jest potrzebny a jedynie mógłby być "kosztem".

Mikado lubi działać w taki sposób, że wszystko przygotuje wcześniej a sam wejdzie wykonać ostateczny, jeden ruch. Dobrze się ubiera (zwłaszcza jak na terminusa) i lubuje się w eleganckiej broni białej.

Z uwagi na swoje umiłowanie samotności lub mniejszy nacisk na inne osoby jest lekko odmienny od stereotypu Diakona, acz jak się go pozna bliżej to kocha taniec i z radością zaciągnie Cię do łóżka ;-).

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170125|skutecznie odwraca uwagę Anny od tego, że Hektor tak jakby... zniknął.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170104|tak bardzo próbował się koncentrować wiedząc, że jest na Fazie... że w końcu się dowiedział o generatorach. Wziął Annę na sparing by ją odstresować. Przegapił wszystkie problemy.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|który przegrywa w jengę z Anną Myszeczką. Ratuje * magów Świecy przed zobaczeniem Hektora sugerując Halę Symulacji|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|uduchowiony mistrz miecza zwany "rzeźnikiem z Esuriit". Zabił Dosifieja Esuriit. Współuczestniczył w egzekucji Kazimierza.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|w roli uduchowionego warrior monka podnoszącego ludziom morale i entertainera bardziej niż terminusa.|[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|robiący performance krystaperzom i odciągający je od zespołu. Też: powstrzymał Annę w kluczowym momencie przed walką z Zespołem.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|stabilny i lekko podłamany terminus próbujący utrzymać rzeczywistość pod jakąkolwiek kontrolą.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161012|który tylko chciał eskortować Silurię a wpadł w środek wojny z Karradraelem. Ranny, broniąc Silurii. Ale obronił.|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150830|który postawił się i Judycie i Ozydiuszowi. Miał nadzieję na wsparcie, ale tylko Paulina się pojawiła.|[Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160922|podejrzliwy terminus wszędzie węszący pułapki... by Supernowa go nie posłuchała. Kochanek Supernowej. Aktualny.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|141123|tymczasowy ekspert od działań Draceny|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|najwierniejszy uczeń Draconisa.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170628|wsparcie Estrelli. Pojawił się, by zabrać Artefakt do Millennium - Draconis najpewniej potnie go na żyletki ;-).|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|10/02/16|10/02/17|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|10|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|8|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|7|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|6|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|6|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|4|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|3|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Roman Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-blyszczyk.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Lancelot Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-lancelot-bankierz.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|1|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Celestyna Marduk](/rpg/inwazja/opowiesci/karty-postaci/9999-celestyna-marduk.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
