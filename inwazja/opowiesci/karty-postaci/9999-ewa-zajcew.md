---
layout: inwazja-karta-postaci
categories: profile
title: "Ewa Zajcew"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|usłyszała o Anatolu Sowińskim z uwagi na Wieprzopomnik w Półdarze.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|przygotować coś by sprawdzić, czy Anatol Sowiński to nie "Kuba Urbanek w przebraniu".|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150330|głównodowodząca akcją przeniesienia Overminda Spustoszenia Aurelii z Powiewu Świeżości na KADEM.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
