---
layout: inwazja-karta-postaci
categories: profile
title: "Joachim Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160309|ten, który boleje nad stratą Wandy Ketran i w ramach zemsty wysadził surstromming w prokuraturze by Hektor ucierpiał.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|który szukał Wandy w Rezydencji w chwili, gdy ona kończyła swój żywot na scenie "Działa Plazmowego".|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160106|który chciał zostawić sektę i Przodek w stanie wojny... aż Paulina tupnęła nogą. Współpracował dzielnie.|[...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151230|nieco fajtłapowaty artysta-pirolita, realistyczny fatalista. Sekciarze go upili i wrzucili do śmieci. Wszedł w sojusz z Pauliną ("Anią Diakon").|[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|który nadal szuka wiły i którego być może Paulinie udało się napuścić na Korzunia (czyściciela Gali Zajcew).|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150930|zainteresowany Wandą Ketran, szukał jej wszelkimi środkami, za co Blakenbauerowie nasłali na niego policję.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|2|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|1|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Ochrona Kobra Przodek](/rpg/inwazja/opowiesci/karty-postaci/9999-ochrona-kobra-przodek.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
