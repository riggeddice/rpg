---
layout: inwazja-karta-postaci
categories: profile
title: "Damazy Bogatek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160908|podróżnik, który kolekcjonował różne rzeczy i - niestety - zostawił rodzinie figurkę syberyjskiego śledzia która doprowadziła do Skażenia wnuczki.|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|10/01/14|10/01/17|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łukasz Czerwiącek](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-czerwiacek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Zygmunt Flak](/rpg/inwazja/opowiesci/karty-postaci/1709-zygmunt-flak.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Tadeusz Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Stefan Porieńko](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-porienko.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Rudolf Mikołaj](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-mikolaj.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Geraldina Kurzymaś](/rpg/inwazja/opowiesci/karty-postaci/9999-geraldina-kurzymas.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Emilia Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Czesław Statyw](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-statyw.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
