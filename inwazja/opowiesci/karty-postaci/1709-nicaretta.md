---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Nicaretta"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Survivor**:
    * _Aspekty_: zbieg
    * _Opis_: "zrobię WSZYSTKO by przetrwać i pozostać na Primusie. ścigają mnie; chcą mnie kontrolować i dominować. Odzyskać. Nigdy mnie nie złapią i nigdy mnie nie znajdą"
* **MET: Socjopatka**:
    * _Aspekty_: 
    * _Opis_: "wszyscy: ludzie, magowie, nawet ja; WSZYSCY są tylko surowcami i muszę maksymalizować moje korzyści"
* **BÓL: Megalomania**:
    * _Aspekty_: 
    * _Opis_: "nie pozwolę, by o mnie zapomniano! Jestem żywa, chcę być żywa, I will NOT fade away!"

### Umiejętności

* **Oficer**:
    * _Aspekty_: dowodzenie demonami, taktyk, kontrola tłumów
    * _Opis_: 
* **Kapłan**: 
    * _Aspekty_: była kapłanka Arazille, ukojenie Arazille, nawracanie, kuszenie, ewangelista
    * _Opis_: 
* **Uciekinier**:
    * _Aspekty_: aktork, ucieczka, ukrywanie się, złodziej
    * _Opis_: 
* **Succubus**:
    * _Aspekty_: uwodziciel, aktor, nawracanie, kuszenie, rozkochanie, widzenie pragnień, masowe rytuały erotyczne
    * _Opis_: 
* **Świat Primusa**: 
    * _Aspekty_: świat ludzi, świat magów, polityka magów, miasto
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Silne i słabe strony:	
	
* **Sukkub**:
    * _Aspekty_: umiejętność latania, szpony, dotyk sukkuba, maska człowieka, wiele postaci, głód energii, nieśmiertelna
    * _Opis_: lata bez konieczności używania magii, w formie ludzkiej świetna aktorka, może wyglądać na dowolny wiek, musi uprawiać seks przynajmniej dwa razy dziennie, zniszczenie odsyła ją na Fazę Daemonica, gdzie się rekonstytuuje

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: magia erotyczna
    * _Opis_: 
* **Magia mentalna**: 
    * _Aspekty_: dominacja, uwodzenie
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

A sorceress who simply refused to die. A mage broken with the Eclipse. A body broken by those who tried to scan her, heal her, examine her. A mind - a willpower – determined to survive at all costs.

A sorceress who simply refused to die. Even when she should have. A sorceress who tried to summon Arazille to get a God on her side. A sorceress who succeeded.

Arazille has cast away the broken body, damaged magic and bloody addiction. She has built a new shape, a new supporting structure for the mind of the sorceress. The shape made of magical energy. The force of eternal happiness and peace.

Another force appeared. One which wanted to change the sorceress again. A force which wanted to enslave. The willpower – the survivor – agreed. She wanted to live. Her shape was twisted into the one of succubus. Some of Arazille’s programming remained – but the survivor was “free” again.

Time has passed. The force controlling the succubus was defeated or pushed away. The succubus was pushed into Phase Daemonica. Eternal. Tormented. Hungry. Empty. But alive.
Now she’s managed to exit the Phase. She needs the energy; she needs attention to exist. The force is somewhere there, lurking and hoping to attract her. Humans want to love her and control her. Magi want to take over her or to destroy her.

She refuses to die. She has skills, shards of her former personality and memory and she will not lose.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Henrykiem|Nicaretta|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|wie, że Henryk o niej wie i że "nadchodzi" i się może fortyfikować|Nicaretta|
|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|+ księga rytuałów erotycznych|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170120|okazała się świetnym taktykiem, twardym negocjatorem i niezłym wojownikiem. I tak przegrała - weszła w niekorzystny tymczasowo sojusz. Nawet Arazille nie pomoże...|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170214|gdzie mag nie może, tam sukkuba pośle - od infiltracji domku po wyciąganie serc z martwych czarodziejek. Bardzo, bardzo nie opłacał jej się ten sojusz...|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|10/07/26|10/07/29|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161110|zastawiająca pułapki (gra by zabić) i przeskakująca po hostach demonica, która myśli, że skutecznie zmyliła ewentualny pościg. Bardzo inteligentna.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161103|succubus, taktyk i corruptor. Znajdowała się między Fazami, inkarnowała się w zakonnicy dzięki podszeptom z Drugiej Strony. Zmuszona do ucieczki przez Henryka.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|4|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|2|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Fiodor Pyszczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-pyszczek.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Filip Gładki](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-gladki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
