---
layout: inwazja-karta-postaci
categories: profile
title: "Fryderyk Grzybb"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171015|o którym Maria się dowiedziała, że kiedyś wszedł z Sądecznym i Rukolas do Muzeum Pary a wyszedł ALBO on ALBO Farnolis.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170409|jeden z Pentacyklu, mistrz areny i marketingu; chce sławy, 26; to on pobił ciężko człowieka w przeszłości; wyraźnie interesuje się Mikaelą Weiner|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
