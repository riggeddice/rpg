---
layout: inwazja-karta-postaci
categories: profile
title: "Karina von Blutwurst"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170823|wzmocniona i po stronie Kinglorda; ubłagała Kinglorda, by oddał jej Henryka do zabawy.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160914|młoda wampirzyca szanująca Karolinę, która nie była w stanie pokonać Blakenbauerów. Pozwoliła im odejść, prosząc, by uratowali jej rodzinę.|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160809|wielka nieobecna, "wampir", na której temat The Governess sfabrykowała jeden milion mitów. |[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|2|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jolanta Lipińska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-lipinska.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Izabela Bąk](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-bak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Filip Czumko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czumko.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Ernest Kokoszka](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-kokoszka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Elżbieta Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Damian Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Antonina Brzeszcz](/rpg/inwazja/opowiesci/karty-postaci/1709-antonina-brzeszcz.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Anatol Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-anatol-weiner.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Aleksander Tomaszewski](/rpg/inwazja/opowiesci/karty-postaci/1709-aleksander-tomaszewski.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Adolphus von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-adolphus-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
