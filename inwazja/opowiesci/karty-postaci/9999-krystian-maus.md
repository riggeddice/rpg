---
layout: inwazja-karta-postaci
categories: profile
title: "Krystian Maus"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140503|osoba, która chciała odzyskać rodzinę po utracie magii|[Wołanie o pomoc](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|10/01/03|10/01/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Vuko Milić](/rpg/inwazja/opowiesci/karty-postaci/9999-vuko-milic.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Kwiatuszek](/rpg/inwazja/opowiesci/karty-postaci/9999-kwiatuszek.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Kornelia Modrzejewska](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-modrzejewska.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Klotylda Świątek](/rpg/inwazja/opowiesci/karty-postaci/9999-klotylda-swiatek.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Karol Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-maus.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Jędrzej Zdun](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zdun.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140503](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html)|
