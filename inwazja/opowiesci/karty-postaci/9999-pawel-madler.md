---
layout: inwazja-karta-postaci
categories: profile
title: "Paweł Madler"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|gossip and false evidence say that he is close to bankrupcy|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170525|businessman who wanted to change the Water Tower into a luxury restaurant and got framed into 'oh this guy is bankrupt' scheme.|[New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|10/01/13|10/01/15|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefan Brązień](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-brazien.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Sonia Adamowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-sonia-adamowiec.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Rafał Adison](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-adison.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Paweł Franka](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franka.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Kurt Zieloniek](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-zieloniek.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Klara Pieśniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-klara-piesniarz.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Karol Szczur](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-szczur.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
