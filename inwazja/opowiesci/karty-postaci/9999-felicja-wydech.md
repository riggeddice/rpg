---
layout: inwazja-karta-postaci
categories: profile
title: "Felicja Wydech"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161129|femisatanistka tymczasowo na recepcji; zaprzyjaźniła się z Amelią Eter.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
