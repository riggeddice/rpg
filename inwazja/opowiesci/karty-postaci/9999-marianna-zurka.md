---
layout: inwazja-karta-postaci
categories: profile
title: "Marianna Zurka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150501|kioskarka i członek kultu Anioła. Lubi plotkować. Porwana przez Olgę celem pożarcia.|[Szalona 'czarodziejka' Zależa](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|10/01/07|10/01/08|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szczepan Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zaleski.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Inga Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
