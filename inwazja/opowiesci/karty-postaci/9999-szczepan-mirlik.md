---
layout: inwazja-karta-postaci
categories: profile
title: "Szczepan Mirłik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170404|młody (11 lat) chłopak, który kocha swoich rodziców; biomanta + katalista. Najsilniejszy mag w rodzinie.|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|10/02/10|10/02/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Onufry Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Katarzyna Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Ferrus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-ferrus-mucro.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
