---
layout: inwazja-karta-postaci
categories: profile
title: "Maciej Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140409|eks-adiutant Yakima który został jednym ze Spalonych Zajcewów i dołączył do Swietłany.|[Czwarta frakcja Zajcewów](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
