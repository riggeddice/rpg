---
layout: inwazja-karta-postaci
categories: profile
title: "Ferdynand Maus"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170707|z frakcji "Mausowie powinni być drapieżny i bez Karradraela". Gardzi Judytą jako słabą i działa tak, by zdobyć popleczników.|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|10/03/06|10/03/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
