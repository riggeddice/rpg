---
layout: inwazja-karta-postaci
categories: profile
factions: "faction_1, faction_2"
type: "NPC"
owner: "public"
title: "Imię_Nazwisko"
---
# {{ page.title }}

## Koncept

Postać 1 + Postać 2 + Postać 3; siedmiosłowowy opis postaci

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | |
| Społeczne         | |
| Wartości          | |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
