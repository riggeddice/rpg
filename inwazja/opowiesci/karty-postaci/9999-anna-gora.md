---
layout: inwazja-karta-postaci
categories: profile
title: "Anna Góra"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160921|dziennikarka która przekazała materiały o Wandzie Stanisławowi i Tomaszowi, by oni ją znaleźli|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150930|dziennikarka "Kociego oka", która miała być tajną bronią Joachima w znalezieniu Wandy a Joachima pogrzebała dzięki Alinie.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150820|dziennikarka "Kociego oka" (lokalnego i poczytnego portalu w Kopalinie), która opublikowała materiały o Ormiu od Wandy.|[Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|2|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Zdzisław Kamiński](/rpg/inwazja/opowiesci/karty-postaci/1709-zdzislaw-kaminski.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tomasz Ormię](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-ormie.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Tomasz Kracy](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kracy.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Milena Marzec](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-marzec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Rokita](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rokita.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Kajetan Pyszak](/rpg/inwazja/opowiesci/karty-postaci/9999-kajetan-pyszak.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Hubert Brodacz](/rpg/inwazja/opowiesci/karty-postaci/1709-hubert-brodacz.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grzegorz Włóczykij](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-wloczykij.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
