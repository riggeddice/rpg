---
layout: inwazja-karta-postaci
categories: profile
title: "Metody Bankierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170208|sympatyzuje z Hektorem i Silurią, acz jest biurokratą i to widać. Hektor wisi mu przysługę. Powiedział Hektorowi i Silurii o co tu chodzi.|[Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|10/08/12|10/08/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Oliwier Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-sowinski.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Klaudia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-klaudia-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
