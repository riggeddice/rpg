---
layout: inwazja-karta-postaci
categories: profile
title: "Kurt Weiner"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170212|dowiedział się o powiązaniach Draceny z nieletnimi (no, Najady). Potencjalny wrzód na... nodze.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161009|lekarz szukający o co chodzi Paulinie; też: jak pognębić Paulinę i Dracenę. O dziwo, nie ze złej woli a ze zmartwienia o lokalną populację.|['Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html)|10/07/14|10/07/16|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161002|lokalny lekarz. Stracił moc po Zaćmieniu. Na razie więcej o nim nie wiemy; jeden z 11 magów pracujących przed Zaćmieniem w Stawni.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Michał Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-jesiotr.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
