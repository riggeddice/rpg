---
layout: inwazja-karta-postaci
categories: profile
title: "Marzena Gilek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161110|host INNEGO sukkuba niż Nicaretta (która przeskoczyła). Skończyła ranna (żelazny krzyż w głowę) w szpitalu, ale wolna.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161103|zakonnica. Host Nicaretty i poprzednia gosposia księdza Filipa z Żonkiboru.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|2|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Filip Gładki](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-gladki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
