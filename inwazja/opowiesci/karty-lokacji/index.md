---
layout: default
title: Karty lokacji Inwazji
---

# {{ page.title }}

1. Świat
    1. Faza Allitras
        1. Trimektil
            1. Jezioro Błysków
    1. Faza Daemonica
        1. Mare Crucio
            1. Srebrna Septa
                1. Portal
        1. Mare Ithium
        1. Mare Levamen
        1. Mare Vortex
            1. Zamek As'Caen
                1. Biblioteka Powiewu Świeżości
                1. Kadem Daemonica
                    1. Academia Memoriae
                        1. Sala Centralna
                    1. Poligon Wspomagany
                    1. Skrzydło Medyczne
                        1. Sale Regeneracji
                        1. Zaawansowany Medtech
                    1. Skrzydło Naukowe
                        1. Instytut Pryzmaturgii
                            1. Sale Badawcze
                        1. Instytut Technomancji
                    1. Skrzydło Wypoczynkowe
                        1. Pokoje Gościnne
                        1. Wewnętrzny Ogród
                            1. Ogród Trójśpiewu
                            1. Ogród Weroniki Seton
                                1. Czerwony Ogród
                                1. Zielony Ogród
                                1. Żółty Ogród
                    1. Skrzydło Wzmocnione
                        1. Instytut Eureka
                        1. Pancerne Magazyny
                            1. Academia Daemonica
                            1. Magazyn Tech_Kn_15
                        1. Specjalne Sypialnie
                    1. Układ Nerwowy
                        1. Centrum Dowodzenia Kademem
                        1. Sale Symulacji
                            1. Sala Wojny
                        1. Security
                1. Pokój Zdjęć Whisperwind
        1. Świeca Daemonica
            1. Krąg Wewnętrzny
                1. Wielka Sala Tronowa
                1. Wydział Wewnętrzny
                    1. Biuro Mariana Agresta
            1. Krąg Zewnętrzny
                1. Quintum Atrium
                1. Skrzydło Powietrza
                    1. Laboratorium Mgieł
                    1. Portal Kontrolny
                1. Skrzydło Wody
                    1. Pokoje Mieszkalne
    1. Faza Esuriit
        1. Insula Luna
            1. Srebrna Septa
                1. Centrum
                    1. Archiwum
                    1. Blockhaus K
                    1. Centrala Świecy
                    1. Kometor
                1. Skrzydło Szpitalne
                    1. Sale Medyczne
                1. Wschodnia Wyspa
                    1. Portal
            1. Wyspa Kolców
                1. Iglica Sowińskich
                1. Las Kolczasty
                1. Szczelina Głodu
            1. Wyspa Zmarłych
                1. Cmentarz
                1. System Defensywny
    1. Pomiędzy
    1. Primus
        1. Dolnośląskie
            1. Powiat Rotberg
                1. Rotberg
                    1. Centrum
                        1. Luksusowa Restauracja Feniks
        1. Lubelskie
            1. Powiat Hrabiański
                1. Gumowice
                1. Stawnia
                    1. Centrum
                        1. Apteka U Weinera
                        1. Przychodnia
                        1. Warenki U Babuszki
                    1. Leśnicz
                        1. Bagna Leśnickie
                        1. Las Ozierski
                            1. Danusin
                            1. Leśniczówka
                    1. Oziersko
                        1. Jezioro Oziero
                        1. Smażalnia Ryb Jesiotr
                            1. Dom Jesiotra
                            1. Przybudówka
            1. Powiat Tonkij
                1. Puszcza Nocy
                    1. Wschodnia Dolina
                        1. Hotel Dagmara
                1. Wyjorze
                    1. Centrum
                        1. Hotel Jantar
                        1. Hotel Luksus
                        1. Iglica Trójzębu
                    1. Wyjorze Fabryczne
                        1. Fabryka Mikroprocesorów Majka
                    1. Śpiochowice
                        1. Tunel Do Sarkofagu
                1. Wężokuj
                    1. Biblioteka
                    1. Dom Kultury
                    1. Kemping
                    1. Wężowa Ciżemka
                1. Wężymordzki Park Narodowy
                1. Łuczywo Nadobne
                    1. Ulica Reprezentacyjna
                        1. Dom Kaldworów
                1. Żonkibór
                    1. Festiwal Marzeń
                        1. Utopia Marzeń
                    1. Parafia
                        1. Kościół
                        1. Plebania
            1. Powiat Wolny
                1. Krompy
                1. Kulturno
                1. Lenartomin
                    1. Biblioteka
                    1. Hotel Lenart
                    1. Kościół
                    1. Posiadłość Mraczona
                    1. Rynek
                        1. Studnia Życzeń
                1. Oślarz
                1. Puszcza Nocy
                    1. Zachodnia Dolina
                        1. Bunkier Gołębiarza
        1. Lubuskie
            1. Powiat Szarczawy
                1. Storczyn Mniejszy
                    1. Duchowy
                        1. Plebania
                    1. Fabryczny
                        1. Bar Gulasz
                        1. Granuleks
                        1. Nieużywany Składzik Czosnku
                        1. Stara Megafabryka Czosnku
                    1. Lasek
                        1. Chatka Leśniczego
                    1. Luksusowy
                        1. Czosnekolada
                        1. Miejski Dom Kultury
                        1. Posesja Kantosza
                    1. Pola
                        1. Pola Czosnku
        1. Mazowsze
            1. Powiat Ciemnowężny
                1. Burzowiec
                    1. Wieża Wichrów
                1. Gnuśnów Letni
                    1. Farma Krystalitów
                1. Lesiwo
                    1. Leśne Jezioro
                        1. Wieczybór
                1. Skalna Grota
                    1. Stadnina Koni
                        1. Arena Pentacykl
            1. Powiat Czaplaski
                1. Paszczęka
                    1. Południe
                        1. Stolarnia Sitek
            1. Powiat Głuszców
                1. Upiorzec
                    1. Dzielnica Wichrów
                    1. Eliksir Aerinus
                    1. Pałacyk Gryfa
            1. Powiat Kupalski
                1. Trzeci Gród
                    1. Centralny Pierścień Murów
                        1. Biblioteka Zrębowa
                    1. Zewnętrzny Pierścień Murów
                        1. Chatka Oktawiana Mausa
                    1. Środkowy Pierścień Murów
            1. Powiat Pustulski
                1. Byczokrowie
                    1. Niezniszczalna Kazamata
                1. Głębiwiąz
                    1. Domki Na Wzgórzu
                    1. Pola
                    1. Występny Wiąz
                1. Krolżysko
                    1. Montownia Awianów Lotnik
                1. Kropiew Dzika
                    1. Centrum
                        1. Eleganckie Kamienice
                        1. Siedziba Elgieskład
                    1. Wschód
                        1. Siedziba Słojozen
                    1. Zachód
                        1. Osiedle Krogulca
                1. Krukotargowo
                    1. Centrum
                        1. Szpital Miejski
                    1. Dzięciolnia
                        1. Wiatraki
                1. Krukowo Czarne
                    1. Gabinet Pauliny
                        1. Ogródek
                    1. Nie Zbudowany Dom Przemuskich
                    1. Szkoła Wiejska
                1. Męczymordy
                    1. Gospodarstwa Rolne
                    1. Las Szeptów
                    1. Magitrownia Histogram
                1. Mżawiór
                    1. Agencja Detektywistyczna
                    1. Klub Panienka W Koralach
                    1. Portalisko Pustulskie
                1. Orłostwo Wielkie
                    1. Centrum
                        1. Apartamenty Dukata
                    1. Orłostwo Mniejsze
                        1. Restauracja Dobrego Ducha
                1. Praszyna
                    1. Ptasiówka
                    1. Redakcja Głosu Praszyny
                1. Półdara
                    1. Chata Lekarza
                    1. Hodowla Psów Husky Pełnodar
                1. Rogowiec
                    1. Hodowla Myszeczki
                1. Senesgrad
                    1. Dworcowo
                        1. Jezioro Czereśniowe
                        1. Kolejowa Wieża Ciśnień
                        1. Siedziba Dobra Ludzi
                        1. Stacje Przeładunkowe
                    1. Zalesie
                        1. Muzeum Pary
                1. Szczypak
                1. Trzeszcz
                    1. Elektronik Suwak
                    1. Klub Nowa Melodia
        1. Małopolska
            1. Powiat Chrobrego
                1. Owcodroga
                1. Strażnica Dumna
                    1. Browar Dumny Hufiec
                    1. Escape Room Kosmiczna Kapsuła
                    1. Kemping Wczasowicz
                    1. Pośrednictwo Nieruchomości Warkocz
            1. Powiat Komesów
                1. Toczmiejów
                    1. Komisariat Policji
                    1. Muzeum
                    1. Obserwatorium
                    1. Restauracja Trzecia
            1. Powiat Okólny
                1. Chlewnicz
                    1. Restauracja Drapak
                1. Czeludzia
                    1. Centrum
                        1. Komenda Policji
                        1. Kościół Franciszkanów
                        1. Willa Bogatków
                    1. Kwiatkowo
                        1. Bazarek
                    1. Lasek Starej Wróżdy
                        1. Ruina Po Starym Domku
                1. Okólnicz
                    1. Fabryka Samochodów
                    1. Miasteczko
                        1. Policja
                            1. Areszt
                 1. Pierwogród
                     1. Pałac Blutwurstów
                            1. Biblioteka
                            1. Imponujący Hall
                            1. Kaplica Pałacowa
                                1. Empora
                            1. Pokój Kariny
                            1. Sala Balowa
                            1. Sala Lustrzana
                            1. Sala Myśliwska
                1. Szczawnia Mokra
                    1. Baza Skorpiona Dk-6
                        1. Centrala Dowodzenia
                        1. Izolatka
                        1. Laboratorium
                        1. Magazyn
                        1. Skład Artefaktów
                1. Sznurkaty
                    * Forteca Oktawiana Mausa
        1. Podlasie
            1. Powiat Orłacki
                1. Zależe Leśne
                    1. Koniowie
                        1. Dawna Chata Lokalnej Wiedźmy
                        1. Siedziba Czarodziejki
                    1. Las Gęstwiński
                        1. Altana Leśniczego
                        1. Ołtarz Anioła
                    1. Serce
                        1. Kawiarenka Ukrop
                        1. Kościół
                        1. Pensja Błazonia
                    1. Wał Kartezia
                        1. Cmentarz
                            1. Krypta Onufrego Zaleskiego
                            1. Krypta Rodu Zaleskich
                        1. Dworek Pana
        1. Wielkopolska
            1. Powiat Bogacki
                1. Kary Gród
                    1. Obrzeża
                        1. Dworek Sieciecha Bankierza
                            1. Ogród Tęczowy
        1. Xxx
            1. Powiat Xxx
                1. Xxx
                    1. Palmiarnia
        1. Śląsk
            1. Powiat Czarnoskał
                1. Czarnoskał
                    1. Aleja Piwowarska
                        1. Browar Książę Czarnoskału
                        1. Hipermarket Tytan
                        1. Klub Wielkie Piwo
                        1. Mikrobrowar Czarny Tur
                    1. Centrum
                        1. Biuro Na Świeczniku
                        1. Biuro Sanepidu
                        1. Hotel Bażant
                        1. Kawiarenka Trzy Krzaki Róży
                    1. Komin
                        1. Klub Sztuk Walki Liść
                        1. Komisariat Policji 3
                    1. Narzaniec
                        1. Ulica Homara
                        1. Ulica Stefloka
                    1. Osiedle Techniki
                        1. Hotel Fontanna
                        1. Stara Fabryka Porcelany
                1. Marzanka
                    1. Droga Gościnna
                        1. Domek Radości
                        1. Domek Zadumy
                    1. Ulica Ziołowa
                        1. Skład Materiałów Budowlanych
            1. Powiat Czelimiński
                1. Czelimin
                    1. Hotel Pracowniczy
                    1. Kościół Neoromański
                    1. Pańczyk
                        1. Huta Chrobry
                    1. Piwiarnia Kufel
                    1. Restauracja / Motel Wyżerka
                    1. Teatr Wiosenny
                1. Glin Żółty
                    1. Okolice
                        1. Droga W Lesie
                1. Jodłowiec
                    1. Cityhotel Jodłowiec
                    1. Fabryka Czołgów
                    1. Klub Kolt
                    1. Knajpa Czerwony Kogut
                    1. Knajpka Gwiezdna Ośmiornica
                    1. Maksymlaw
                        1. Biuro
                        1. Sortownia Korespondencji
                1. Koty
                    1. Centrum
                        1. Dom Kultury
                        1. Kościół Romański
                            1. Krypta
                        1. Lodziarnia "Wafelek"
                        1. Mechanik Samochodowy
                        1. Remiza Strażacka
                        1. Rynek
                 1. Dwie Hałdy
                        1. Agroturystyczny Domek
                     1. Dom Przechodni Przyjaciół Marysi
                        1. Domki
                        1. Domki Na Wynajem
                 1. Las Stu Kotów
                        1. Bunkier
                        1. Grobowiec Lugardhaira
                     1. Kopiec Miślęgów
                    1. Obrzeża
                        1. Nieużytek Służący Jako Parking
                1. Miłejka
                    1. Szeregowiec Powrotu
            1. Powiat Kopaliński
                1. Czelimin
                    1. Osiedle Solidarności
                        1. Bloki Mieszkalne
                        1. Szpital Hutniczy
                    1. Pływnia
                        1. Hotel Pracowniczy
                        1. Kościół Neoromański
                1. Czubrawka
                    1. Gospoda Ze Smalcem
                1. Glin Żółty
                    1. Obrzeża
                        1. Lotnisko
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                        1. Dworzec Kolejowy
                        1. Kadem Primus
                            1. Instytut Pryzmaturgii
                            1. Instytut Technomancji
                            1. Instytut Zniszczenia
                            1. Ogrody Weroniki Seton
                            1. Restauracja
                            1. Sale Pracy Własnej
                            1. Skrzydło Szpitalne
                               1. Wewnętrzny Krąg
                                   1. Hotel Wewnętrzny
                            1. Zewnętrzny Krąg
                                1. Sale Spotkań
                        1. Kino V
                        1. Klub Magów "Rzeczna Chata"
                        1. Klub Magów Rzeczna Chata
                        1. Kompleks Budynków Sądowo-Policyjnych
                            1. Areszt Śledczy
                            1. Centralna Komenda Policji
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Archiwa
                                1. Czarne Archiwa
                            1. Heksagon
                                1. Intensyfikator Energii Magicznej
                            1. Kompleks Pałacowy
                                1. Arena Maximus
                                1. Elitarna Sala Treningowa
                                1. Ogrody Świecy
                                1. Pion Badawczo-Produkcyjny
                                1. Pracownie Inżynierii Technomantycznej
                                1. Skrzydło Bankierzy
                                1. Skrzydło Sowińskich
                                    1. Buduar Wiktora
                                    1. Pokoje Gościnne
                                    1. Pokój Wioletty
                                    1. Sala Reprezentacyjna
                            1. Koszary Terminusów
                            1. Magazyn Artefaktów Niebezpiecznych
                            1. Pion Badawczo-Produkcyjny
                                1. Laboratoria Alchemiczne
                                1. Pracownie Inżynierii Technomantycznej
                            1. Pion Dowodzenia Kopalinem
                                1. Biuro Lorda Terminusa
                                1. Sala Przesłuchań 4B
                            1. Pion Logistyczny
                                1. Brama
                                1. Bramy
                                1. Energetyka
                                1. Hydroponika
                                1. Magazyny
                            1. Pion Mieszkalny
                                1. Kwatery Gości
                                1. Kwatery Magów
                                1. Kwatery Służby
                                1. Kwatery Viciniusów
                            1. Pion Ogólnodostępny
                                1. Galeria Handlowa Srebrnej Świecy
                                1. Restauracja Dyskretna
                                1. Sala Spotkaniowa
                                1. Sąd Magów
                            1. Pion Szpitalny
                                1. Cela Mileny Diakon
                                1. Gabinety Lekarskie
                                1. Kafejka Szpitalna
                                1. Pracownia Korekty Wzoru
                                1. Sala Regeneracyjna
                            1. Skarbiec Kopalina
                                1. Magazyn Artefaktów Niebezpiecznych
                        1. Luksusowa Restauracja Welon
                        1. Opera Mireille
                        1. Park Centralny
                        1. Pion Dowodzenia Kopalinem
                            1. Sala Audiencyjna Lorda Terminusa
                        1. Plac Wszystkich Kultur
                        1. Politechnika
                            1. Akademik Szczur
                        1. Prokuratura
                        1. Prywatne Osiedle Mirra
                            1. Apartament Netherii Diakon
                        1. Restauracja Królewska
                        1. Rzeczna Chata
                        1. Studencki Klub "Paradoks"
                        1. Technopark "Max Weber"
                            1. Drugie Piętro
                                1. Firma Kosmetyczna Piżmowar
                                1. Laboratorium Biotechnologii Przemysłowej
                            1. Parter
                            1. Pierwsze Piętro
                                1. Czerwona Sala
                                1. Wielka Sala Konferencyjna
                                1. Zielona Sala
                                1. Żółta Sala
                        1. Technopark Max Weber
                        1. Wielkie Muzeum Śląskie
                    1. Dzielnica Kwiatowa
                        1. Dom Komendanta Straży Pożarnej
                        1. Domek Kermita Diakona
                        1. Domek Tamary Muszkiet
                        1. Galeria Sztuki Van Gogh
                        1. Park Kwiatowy
                    1. Dzielnica Mausów
                        1. Biurowiec "Biała Mysz"
                        1. Mrówkowiec Józef
                            1. Mieszkanie Elei I Rufusa Mausów
                        1. Szary Pustostan
                    1. Dzielnica Owadów
                        1. Basen Miejski
                        1. Blok Z Żelbetu Z Przekaźnikiem Energetycznym Sś
                        1. Gabinet Lekarski
                        1. Gabinet Lekarski Pauliny
                        1. Kanały
                        1. Klub Miłośników Historii Kopalina I Okolic
                        1. Kropiaktorium
                        1. Mieszkanie Beaty Zakrojec
                        1. Mieszkanie Stanisława Pormiena
                        1. Nora Artura Żupana
                        1. Sklep Jubilerski Electrum
                        1. Skwer Przyjaźni
                        1. Szpital Pasteura
                    1. Dzielnica Trzech Myszy
                        1. Aleja Sowińskich
                        1. Baza Sił Specjalnych Hektora
                        1. Domek Leokadii Myszeczki
                        1. Duże Tesco
                        1. Hotel Serduszko
                        1. Kanały
                        1. Klub Pełnia
                        1. Mieszkanie Luizy Wanta
                        1. Mieszkanie Wandy Ketran
                        1. Przedszkole Łobuziak
                        1. Supermarket "Smakołyk"
                        1. Szpital Wojskowy
                        1. Ulica Czarnobrodego
                            1. Domek Mecharośl
                            1. Kino Sceniczne Pasja
                    1. Kompleks Centralny Srebrnej Świecy
                        1. Pion Dowodzenia Kopalinem
                            1. Pokoje Lorda Terminusa
                    1. Las Stu Kotów
                        1. Autonomiczny Schron Kademu
                        1. Leśna Chatka Blakenbauerów
                        1. Osiedle Leśne
                            1. Chatka Zegarowa
                        1. Platforma Węzła Vladleny
                        1. Poligon Kademu
                        1. Zabytkowa Ziemianka Mausów
                    1. Nowy Kopalin
                        1. Emoklub "Zaćmione Serce"
                        1. Emoklub Zaćmione Serce
                        1. Plac Budowy Hipermarketu "Neworld"
                        1. Powiew Świeżości
                        1. Złomowisko Kopalińskie
                    1. Obrzeża
                        1. Dzielnica Mausów
                            1. Przychodnia 11
                        1. Kawiarenka "Złotko"
                        1. Klinika "Słonecznik"
                        1. Klinika Słonecznik
                     1. Plac Budowy Hipermarketu Neworld
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji
                                1. Czarne Laboratorium
                            1. Pokój Edwina
                        1. Sen Robotnika
                            1. Domek Vladleny Zajcew
                        1. Squat Dante
                            1. Rezydencja Blakenbauerów
                        1. Stadion Olimpijski (Plac Budowy)
                    1. Osada Strażnicza
                        1. Dworek Emilii Kołatki
                    1. Osiedle Strzeżone Myszołów
                        1. Dom Tomasza Ormię
                    1. Prywatne Osiedle "Mirra"
                        1. Apartament Netherii Diakon
                    1. Prywatne Osiedle Mirra
                        1. Apartament Estery
                        1. Apartament Netherii Diakon
                    1. Stare Wiązy
                        1. Cmentarz Wiązowy
                1. Mszanka Polna
                    1. Centrum
                        1. Basen
                    1. Wieś
                        1. Centrum Drobiarskie Kurmszanka
                        1. Sad Brzoskwiniowy
                        1. Silos
                        1. Szkoła Matejki
                            1. Świetlica
                    1. Zamieście
                        1. Fabryka Devolaille
                1. Myślin
                1. Piróg Dolny
                    1. Bełty Śląskie
                        1. Dworek Larentów
                        1. Stacja Skubnytv
                    1. Droga Do Kopalina
                        1. Klinika Nadzieja
                    1. Lasek Pejzażowy
                    1. Obrzeża
                        1. Firma Cormex
                        1. Nieaktywna Oczyszczalnia Ścieków
                        1. Oczyszczalnia Ścieków
                        1. Zakład Produkcji Nawozów "Chegolent"
                        1. Zakład Produkcji Nawozów Chegolent
                        1. Zalew Piroski
                    1. Osiedle Licht
                        1. Szeregowiec Pustynny
                    1. Pylisko
                        1. Mordownia Arena Wojny
                    1. Ulica Rzeczna
                        1. Domek Millennium
                        1. Port Rzeczny
                    1. Ulica Targowa
                        1. Błysk - Laserowy Paintball
                1. Piróg Górny
                    1. Centrum
                        1. Kościół Serdecznego Serca
                        1. Motel Przypadek
                        1. Pomnik Rycerza Na Koniu
                    1. Las Stu Kotów
                        1. Blada Polana
                       1. Martwy Portal
                    1. Obrzeża
                        1. Ogródki Działkowe
                            1. Sad Zielonej Myszy
                        1. Osiedle Polne
                            1. Klub Włóczykij
                                1. Klub Dare Shiver
                            1. Pustostan Żelazny
                        1. Skład Budowlany Żuczek
                            1. Ukryta Kwatera Dla Skrzydła Terminusów Świecy
                            1. Ukryta Kwatera Świecy
                                1. Biostanowisko Dowodzenia
                                1. Koszary
                                1. System Detekcyjny
                1. Pszenino
                1. Wtorek Śląski
                    1. Obrzeża
                        1. Skałki Bajeczne
                        1. Stary Skup Zboża
                    1. Ośrodek Historyczny
                        1. Ruina Zamku
                    1. Ośrodek Rolniczy
                        1. Sześciokrąg Plantacji
                            1. Plantacja Krawędzi
                                1. Domostwo Krawędzi
            1. Powiat Leere
                1. Dzierżniów
                    1. Ulica Centralna
                        1. Gospoda Smalec
                    1. Ulica Skarbowa
                        1. Firma Przewozowa Trustport
                1. Gęsilot
                    1. Chata Pirzeców
                    1. Chata Sołtysa
                    1. Las Skrzacki
                    1. Plebania Gęsilocka
                        1. Kościół Nowoczesny
                1. Przodek
                    1. Centrum
                        1. Główna Biblioteka
                        1. Główny Komisariat Policji
                            1. Parking Policyjny
                        1. Klub "Czarny Dwór
                        1. Klub Czarny Dwór
                        1. Szpital Gotycki
                    1. Osiedle Metalowe
                        1. Dojo Faim De Loup
                        1. Garaże Osiedlowe
                        1. Klub Rakieta
                    1. Równia Słoneczna
                        1. "Mrówkowiec"
                            1. Wynajęte Mieszkanie Marii I Pauliny
                        1. Apartamentowiec "Słoneczko"
                        1. Apartamentowiec Słoneczko
                1. Starogłaz
                    1. Sad Jabłonka
                    1. Zamknięta Kopalnia
                1. Wykop Mały
                    1. Aleja Przodkowska
                        1. Motel Mięsiwo
                    1. Królestwo
                        1. Ekskluzywne Domki
                        1. Komenda Policji W Wykopie
                            1. Areszt
                    1. Nieużytki Wykopskie
                        1. Awaryjny Depot Świecy
                        1. Karłowaty Lasek
                            1. Zrujnowany Barakowóz
                    1. Osiedle Ludowe
                        1. Domki
                        1. Familoki
                        1. Hostel Opatrzność
                        1. Kafejka U Sępa
                        1. Klub Klimat Nocy
                        1. Szkoła Zawodowa Kucharska
                        1. Ulica Szczęśliwa
                    1. Zewnętrze
                        1. Gęsty Las
            1. Powiat Myśliński
                1. Bażantów
                    1. Klub Poszukiwaczy Szczęścia
                    1. Nieużytek Wschodni
                        1. Skrzydłoróg
                    1. Południew
                        1. Zpw Chrumkacz
                    1. Strefa Gospodarcza
                        1. Sieć Handlowa Rumcajs
                    1. Wielki Mrówkowiec Socrealistyczny
                1. Bobrów
                    1. Centrum
                        1. Schronisko Dla Psów Przyjaciel
                    1. Obrzeża
                        1. Siedziba Firmy Legioquant
                            1. Lapisowana Piwnica
                            1. Tajne Laboratorium I Baza
                1. Myślin
                    1. Centrum
                        1. Hotel "Luksus"
                    1. Grota Z Rdzeniem Interradiacyjnym
                    1. Ligota
                        1. Biały Dom Senatorski
                        1. Jesionowy Dworek
                    1. Lokalizacja "Lorda Jonatana"
                    1. Okolice
                        1. Bieda-Szyb (Nielegalne Wysypisko Śmieci)
                            1. Grota Z Rdzeniem Interradiacyjnym
                            1. Lokalizacja "Lorda Jonatana"
                        1. Kopalnia Odkrywkowa
            1. Powiat Okólno-Trocin
                1. Czapkowik
                    1. Klub Paintballa Snajper
                    1. Stara Opera
                1. Markucik
                    1. Las Obfity
                        1. Ruina Tartaku
                1. Stokrotki
                    1. Centralne Gospodarstwa
                        1. Gospodarstwo Kamratów
                        1. Gospodarstwo Wieciszków
                    1. Gminna Komenda Policji
                    1. Las Stokrocki
                    1. Remiza
                    1. Urząd Gminny
                    1. Wielkie Ściernisko
                    1. Zachód
                        1. Gospodarstwo Kiry I Franciszka
                        1. Nieużytki
                            1. Ziemianka Kiry
                                1. Eksperymentarium Podziemne
                1. Trocin
                    1. Centrum
                        1. Klub Neonowa Jaszczurka
                        1. Kompleks Srebrnej Świecy
                            1. Pion Ogólnodostępny
                                1. Sąd Magów
                    1. Obrzeża
                        1. Generator Zapasowy Mikro-Riftów
                        1. Parking Z Motelem 'Tarcica'
                    1. Żywy Las
                        1. Placówka Strażnicza 1.3
                        1. Placówka Strażnicza 1.5
            1. Powiat Rymaski
                1. Kartuszewo
                    1. Ulica Paszczaka
                1. Krupniok
                    1. Las Rusałek
                1. Myszeczkowo
                    1. Remiza Strażacka
            1. Powiat Żytowy
                1. Germanja
                    1. Ulica Piroska
                        1. Przybytek Szczęścia
    1. Todo
        1. Bóbr
            1. Bobrzański Zamek
                1. Rift
            1. Todo