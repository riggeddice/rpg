---
layout: post
title: "On mage pattern"
date: 2015-10-11
---

**WARNING! THIS TEXT IS CREATED FROM SPEECH-TO-TEXT! IT IS RAMBLING!**

The pattern of a mage.

When we talk about bloodlines we usually say about inheritance for example I inherit some biological quantities from my parents. In case of mage it’s a bit different. Only some of the biological and psychological quantities of a mage comes from the genetic makeup. A lot of those come from the pattern of the mage.
To understand the pattern I need to explain the concept of magical tissue first. The basic blocks of human body is the DNA. If magical energy touches human’s body the body starts to deteriorate. In the same time something different appears there. A modified entity receptive to magical power which has exactly the same biological structure like the DNA. This something is called magical tissue.

Think of it like of a radiation. In normal situations radiation especially impulse radiation will kill the person. Yet when we talk about magical energy it may mutate the person. Magical energy is commanded using the mind and the willpower; this is what differs magical energy form nuclear radiation. If the magical energy approaches the person who is naturally receptive to it and who has sufficient amount of willpower, parts of the body of the person may get replaced by magical tissue. This is – simplified - how a mage is created.
Normal human body cannot really influence the magical energies. In the same time magical tissue is able to interact with magical energies and it can be a catalyst for the mage’s will. In other words, the more magical tissue is in the body of a person the stronger will be the power of the mage.
As magical tissue is receptive to both magical energy and the willpower it starts to mutate. To be exact magical tissue will follow whatever the mage thinks of himself, of other what the general paradigm will think of a mage.

A known case is when you have a very good person who lives in seclusion; for example a village herbalist, and who generally helps people. This person has some of magical tissue and so has some of magical abilities. Generally people around will consider this person to be a witch. Now because of how magical tissue works if a lot of people believe that a person is a witch and a lot of people believe that witches have certain qualities, the resultant herbalist will inherit some of witches properties simply because the magical tissue will adopt the general paradigm of thinking of a witch. For example she will start looking like a witch, or her magical powers shift to witchy-like.

And it is a problem actually. Imagine a younger boy who has some magical tissue who is a born mage who enters the early lifestage with depression and all of that. If he has some magical powers and if he strongly believes that magical powers are evil, for example because of influence of the church, then this person’s magical tissue will start to mutate. It will start becoming… basically make him look like a demon because he considers himself to be a demon. If I look like a demon and if I am at demon, then because the general public considers demons to be impulsive entities, the boys biological impulses will also go into more impulsive and more fury. And with time the good boy may actually become the role he was kind of assigned.

This means that in general mages living in the human society without help of other mages have it too hard. This is why in the past there were so many fantastic magical creatures around: nymphs, dragons, demons… Those were mages who got assigned a particular role and became that role.

To combat this, present-day’s mages have created something called the bloodlines. The bloodline is in effect a template. For example a mage of a Zajcew bloodline is fueled by passion. This passion usually comes from anger. Zajcew is usually kind of a bruiser, quite frontloaded and very friendly. Both aggressive and friendly.
So let’s take our younger boy. He has been injected with the Zajcew template. Now the template of his magical tissue would actively lead towards itself. This means that not the general opinion on him or his own opinion on him will be the primary determination of what he would become but the template of the bloodline will have more priority in determining his future.

So: the mage’s pattern is exactly the combination of his own natural magical tissue, characteristics, personality and the template from the bloodline. In a way the pattern inherits from both the mage’s environment and the general paradigm and on the other hand it inherits from the bloodline template the mage comes from.

It is possible to modify a mage’s pattern by injecting him with a different pattern. This would be either a bloodline, that is a pattern coming from a bloodline, or this can be a specifically tailored pattern not coming from any bloodline. Think of this as of a blood transfusion or as of organ transplantation. It is a very invasive thing and it actually can kill a mage.
Normal organ transplants deal only with blood relation to each other, but in case of a mage and pattern transplantation it will influence the behavior of the mage, his magical abilities, his perception… basically, everything can change.

So the mage kind of has two different areas of biology. One of them is the normal human biology inherit from the parents, the other one is the “magical biology” which comes from the pattern.
