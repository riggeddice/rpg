---
layout: post
title: "Caine's fangs cleaners sect"
date: 2015-12-19
---
The sect called „Caine’s Fangs” is a special example of how Magi tend to clear the problems that are created in the first place. Sometimes the cleaning operations – operations which is supposed to make sure that nobody knows of magic existence – will not really work anymore. Maybe too many people have died. Maybe too many people know something. Maybe the magic cannot be used in this particular area.

Enter “Caine’s Fangs”. This group was manufactured by some very resourceful Zajcew magi who cooperated with Diakon Magi. They have found a small sect, very brutal, very satanic and they have actually grown it into something powerful and resourceful. They don’t have it under their control, but it does not really matter; the creed of this group is to revel in the pain of other people and to glorify the human sacrifices and very wrongly understood “freedom of will”.

“Caine’s Fangs” take the name from the vampires. Yes, seriously, vampires. They drink blood. It’s ritualistic. Of course, there are some Zajcew and Diakon magi who watch over this group from time to time in case a genuine defiler would appear among them, but that’s why terminus is created, isn’t it?

Overall, sometimes the world of the Magi require a different type of cleaners – ax-crazed fanatics who will kill and maim people – at least to simply distract from the news that magic has appeared – and that’s when this sect is introduced. Sometimes magi take over particular actions of the group, but very often this group is simply left alone to do what they want.

And it was all nice and lovely before the Eclipse. And then before the Invasion. Right now “Caine’s Fangs” got quite autonomous and they operate in the terrains which are lost to normal Magi, causing havoc and mayhem for the normal, good people over there. Magi? They do not really care. Those areas are outside their jurisdiction.

This sect has a lot of splinter sects. Of course, every prophet is the only real one, every prophet is the one who really understands the Satan and everybody else is a false prophet which has to be exterminated. This factor only helps Magi manipulate them and create their own splinter factions when needed. Very often, the members of the sect are being reinforced by artificial humans from Diakon magi, simply to make sure they will never die out and that this sect can be turned against whoever is the current “opponent of the week”.
