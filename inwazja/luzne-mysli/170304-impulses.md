---
layout: post
title: "Impulses - strategic and tactical"
date: 2017-03-04
---

# About

The primary purpose of an impulse is to inform a game master and a player about the character – it motivations, its goals, its approaches to particular problems.

This means, that we needed two different types of an impulse.

# Strategic impulse

## Purpose

Strategic impulse is supposed to be a kind of a goal for a character. If a character died tomorrow, what would make the character die with a smile on its face? A strategic impulse is the purpose of a character in the reality. It is something which drives a character, because that character wants to achieve something.

## Examples

Several examples of a strategic impulse would be:

* I want to have a financial independence at the age of 40.
* I want my beliefs to be the dominant beliefs in my village.
* I want to make sure I will be remembered as a great scientist.
* I want my children to have the best possible start in the world; much better than I used to have.
* I need to complete my collection of all rare songs from the 70s.

## Usage

### Player character

A well-made player character requires **two** strategic impulses. 

### NPC

A NPC does not require to have strategic impulses. A major NPC (being a force, having a drive...) requires at least one.

## Testing - how to check if it is made well?

Several good tests of a strategic impulse: 

* Can you create a situation in which feels compelled to act because the character progresses the goal?
* Do you know what your character needs to do before it can retire peacefully?
* Is it possible to entangle your character into a role-playing session by means other than a game master coercing your character to do it?
* Is your strategic impulse making your character proactive? If both are reactive, the only way a game master can do anything is to “kidnap the Princess and kill your cats”.

## Additional gains

A strategic impulse should also provide:

* At least one interesting contact or interesting resource; if a character has a long-term goal of… something, then that character should start amassing either contacts or resources which lead towards that goal
* at least one interesting skill, potentially at an expert level. If a character tries to achieve something, it is very likely that character picks up skills in the areas connected to that “something”.
 * At least one story. It is impossible to pursue a goal and not get some opponents, or some events… It should, technically, click into place on its own.

# Tactical impulse

## Purpose

Tactical impulse is supposed to be a snapshot of the personality of a character. It should show the main components of the personality of a particular character, a unique set of personality and approaches to the problems which make this particular character what it is.

## Examples

Several examples of a tactical impulse would be:

* womanizer; I am good in talking to women and I cannot resist a pretty lady in the room
* I don’t want to ever be in the spotlight; I operate from the shadows
* I am loyal; I will do everything for my team. My team is far more important than me.
* I am aggressive. This is the best way to handle conflict – head on and let the mightier win.

## Usage

### Player character

A well-made player character requires **two** tactical impulses. 

### NPC

A NPC requires at least **three** tactical impulses. Otherwise, the personality of a NPC will come from GM's personality, especially after 10 sessions without that NPC (or after 6 months).

## Testing - how to check if it is made well?

Several good tests of a tactical impulse:

* Can you make an example in which the character’s personality is an advantage? And can you make an example in which it is a disadvantage?
* Looking at this set of your tactical impulses, can you predict how the character will act in a particular situation?
* Can you create a situation in which your personality compels you to do X while it would be more beneficial to do Y?

## Additional gains

None.

# Reason for the split

The separation of impulses came from one of our sessions; to be frank, from a lot of sessions but we did not notice that yet.

The main problem – if we have a new player or if we have a player who is not calibrated for the group, we don’t really know what types of sessions would that player like to play.

If a player creates a character having impulses like “seductress” and skills in this area, it is quite possible that that player is interested in playing a character lesson to fighting and more into intrigue… Or simple bed fighting? This is a problem.

Having overarching, strategic impulses means that a game master does not really have to guess what would the character want. Also, the stupid “so you sit in the inn” start of a session expires. Every session is able to progress the goals of a character - or to put something a character achieved at the risk. This means a character is motivated into doing things. 

And this means a game master is able to give a player interesting choices: if you do X, you progress your goals. If you do Y, you do what is “right”. Choose one. Then, a player very often thinks of solutions no one would expect – and a story is created. Game master as an obstacle, not as a force of oppression.

In the same time, strategic impulses are not everything. The initial need which spawned impulses as a mechanic were a result of the fact that all the characters converge into player personality. So impulses need to preserve the unique personality or a character.

Enter tactical impulses. Some characters prefer to work from the shadows, while others tackle the problems head on. Some characters are aggressive while other characters are easier to be intimidated. Some players would like to play a character bound by a particular code (like a paladin) and not have to act “lawful stupid”. And this is why tactical impulses are created.

Both serve their purpose.

Strategic – they show what types of sessions are the characters interested in. Tactical – they show how are the problems solved by the characters, what is the character’s personality.
