---
layout: post
title: "Map of Silesia"
date: 2016-04-11
---

**Kopalin** is a main city where we have our missions. This city was initially devised as a defense complex of Silver Candle Guild and as a primary heart of influence of this guild and magi upon South of Poland. In the beginning in the city there was a Central Complex of Silver Candle with the gate, barracks, watchtower etc. but people tend to go where magi are located, especially when natural resources were found northeast of Kopalin. The city has expanded and it became a central place of both Silesian magi and human population.

**Koty** is a historical center of human chain of command in Silesia. This city was supposed to be the one where human population would be located and and it was supposed to be the point of interaction between Magi from Kopalin and human economy, but the dangers of the past and the riches of underground made it so, that Koty were relegated to be a small village. It is positioned next to the very important river “Flisaczka” and it was supposed to be the main way of transport of goods – because the southern forest was so extremely dangerous for humans.

**Piróg Górny** is a historically important agricultural center. Also positioned on the river “Flisaczka”, it was the main point of contact between the cities even higher (even more in the South) and it had cleaner water. It is a long, very long village located around the river. Because it was the first and because they were able to be gatekeepers, it was the richer of two Piróg.

**Piróg Dolny** was also an agricultural place, but it came after Piróg Górny. They have never had the position to bargain and between the two villages there were often feuds; especially when they tried to put some dams or stop the higher positioned village to send goods uninterrupted. At this moment this place is slowly decaying, but it has some industry like waste recycling and fertilizer stuff.

**Czelimin** is a small city located quite close to Kopalin which was created because of resources. It has never grown into something powerful, just a small city with a lot of industry and especially an ironworks (smelter). Currently the ironworks is inactive; although the city may not prosper much, it is still functional. It is very well communicated using the trains and roads which can be used to transport heavy stuff.

TODO:

- Myślin
- Wykop Mały

