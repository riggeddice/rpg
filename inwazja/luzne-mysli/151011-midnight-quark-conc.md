---
layout: post
title: "Midnight, Quark Crystals, Concentration"
date: 2015-10-11
---

Earlier on in the system it used to be at exactly midnight every spell stopped functioning. It was actually a relic of previous systems we use to have to create some tactical advantages. However the problem with this is quite apparent somewhere on this planet there is midnight.

After visiting London we have decided that this particular rule became artificial and constraining it doesn’t help us anymore it only stops us from doing job at sessions. So this particular rule got eliminated.

How to determine when a spell stops being maintained? The obvious answer is whenever a mage stops maintaining the spell. But this opens another can of worms: what if the mage is asleep? It is actually a very interesting situation; it is completely possible that a mage would forget that he should maintain a spell and go to sleep. What happens then? What if the mage forgets is a dream the spell shall be influenced by the current dream of a mage.

Between a mage and an effect of the spell does still exists a strong link. Only a mage can usually influence he spell or dismiss it. This means that the mage needs to actively work on a spell while it is up. You can imagine that a very concentrated mage is able to control every single component of the spell like every single component of the car in his head all the time.
And here and you advantage appears a mage might want to have some apprentices some interns. He can delegate that apprentices focus on particular components of the spell for example like with that car one apprentice thinks of an engine another thinks of a steering wheel yet another thinks of an exhaust pipe. Then the mage combines all those components into one car. If something does not fit he orders a particular apprentice to modify the component she is responsible for.

That way the mage can worry only of the integration.

When everything fits together then it is possible to use the quark crystals to fix the existing structure and reduce the load from the apprentices and from himself. That way the energy required for the spell to be eternal comes from quark crystals not from the apprentices and the mage.

So the role of quark crystals is to change the energy source from the live mage to them. As a cost it is impossible to modify an existing spell if it is fixed with the quark Crystal.
Now let’s talk about concentration. A spell not fixed with quark crystals will exists only as long as the mage’s support it with the concentration and energy. If you got a mage who cast flying spell and he goes into terrible pain for example because of an arrow to the knee, he may lose his concentration and the spell may dissipate; the cause of death would be falling down.

If for example a wizard would like to create an illusion of beautiful woman and he gets distracted by a cat he may accidentally change the spell and he may create an illusion of a partially cat partially woman entity.

Concentration is only one of the resources a mage requires to make a spell happen. The other one is the energy. Magical energy to be precise. It can come from quark crystals but it also can come from the mage’s own body. By manipulating the energy around the mage, the ambient energy, the mage can accumulate the energy inside his body and use the body as the focus. This is tiresome.

In one time a mage can only maintain several spells. This is the result of the need to concentrate and of the need of having energy. For example a good way to destroy a powerful mage who has created several layers of shielding with different magical forcefields, even if you have several weaker mages is to force the mage to adapt several shields at a time. Even if the power is not enough to pierce the shields, it may be enough to force the mage to lose concentration; this might create an opening all simply lead to the general spell failure. So when does the spell stop being maintained? The answer is when the mage is not able or not willing to support it. The midnight rule is not important anymore.

This also means that a natural weak spot for a mage is about 3 AM. Reason? About 3 AM human is at his worst. The concentration is the worst. This also means that a mage can maintain more spells using some kind of stimulants. This will wrack the body though; allowing very interesting gameplay and decisions.
