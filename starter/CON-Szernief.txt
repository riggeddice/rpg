Magia nie istnieje i wszyscy o tym wiedzą.
Tylko, że Wy wiecie, że prawda jest inna... I że ludzie nie mogą jej poznać, bo jeśli tak się stanie, świat stanie na głowie.

CON Szernief. to stacja kosmiczna nieco na uboczu.
Kiedyś cud ludzkiej inżynierii, dziś podupadła placówka skazana na śmierć... to, jak szybką, zależy tylko od graczy.

Stacja Szernief to obecnie stacja górnicza, mocno niedoinwestowana  i mająca pewne problemy finansowe ale - na razie jeszcze - samowystarczalna.
Daleko jej do luksusu, warunki są mocno ascetyczne.
Jej populacja to około 20 000 ludzi, główną jej część (ok 60%) stanowią savaranie, ludzie wyjątkowo oszczędni w ruchach, działaniu i sposobie myślenia. Nie marnują żadnych zasobów, a zasobem jest wysztko, od kalorii po Twoje własne ciało.
Około 25% populacji to drakolici, kultyści boga Saitaera, wierzący w wieczną ewolucję i ekstremalne dostosowanie się do warunków, w jakich przyszło im żyć. Cyborgizacja, bioinżynieria, wszczepy - to wszystko jest dla nich bardzo pożądane. 
Pozostałe 15% to mieszanka różnych kultur i przekonań.

Stacja ma strukturę cylindra o długości około 4 kilometrów i średnicy 1 km.

Powłoka zewnętrzna

Powłoka wewnętrzna

Oś

Powierzchnia / poza cylindrem



Potrzeby: woda, surowce do farm bioponicznych, sprzęt specjalistyczny i medyczny, ludzie.
Źródło dochodów: energia (baterie), produkty z nanofabrykatorów z orbity, rudy, chcieliby rozwinąć usługi turystyczne.

Problemy: brak finansów, zbyt duża populacja ludzi wierzących, lokalizacja stacji w podniesionym polu magicznym

Magia: magia istnieje i odpowiada na emocje i wolę ludzi. Im więcej ludzi w coś wierzy, tym większa szansa, że to się stanie, szczególnie w obliczu podniesionego tła magicznego.

Frakcje na stacji:

Savaranie: oszczędni, cisi, wydajni, efektywność ponad wszystko, mają na pieńku z Radą stacji 
 * Izolacjoniści, dowódca: Wit-421
 * Radykaliści, dowódczyni: Sia-03; ich zdaniem wszyscy muszą oszczędzać, to jedyny sposób na przetrwanie i poprawę finansów
 * Kooperacjoniści: Ikta-04; są skłonni współpracować z frakcją Terimana
 
Drakolici: kultyści ekstremalnej ewolucji, mają na pieńku z Radą stacji 
 * Wojownicy, dowódca: Mawir Hong; ewolucja poprzez starcie z przeciwnikiem, nawet zbyt silnym
 * Kooperacjoniści, dowódca: Termian Skarun; ewolucja poprzez stopniowe dostosowanie się i współpracę
 
Arystokraci: zabawa, nauka, akcje propagandowe
 * Artur Tavit - młody, walczący o prawa biosyntów. 
 * Aerina Cavalis - młoda, chce wszystko naprawić i by stacja działała jak najlepiej. Sojuszniczka Agencji
 * Kalista Surilik - dziennikarka z dobrego domu, próbuje odkryć prawdę o magii. Często coś wie, często wchodzi w szkodę Agencji.
 
Rada: 
 * 
 
Kompania Teraquid


Biosynty


Agencja: tu są gracze. 
Agencja Lux Umbrarum, zwykle operująca w okolicy stacji ze statku Luminarius.
Agencja oficjalnie zajmuje się rybołóstwem i rolnictwem, w praktyce usuwa magię i ogranicza jej efekty.
Zwykle nie ma problemów ze współpracą, ma uprawnienia i świetne wyszkolenie i sprzęt.